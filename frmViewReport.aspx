﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmViewReport.aspx.cs" Inherits="EnterpriserWebFinal.frmViewReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/jquery.number.js" type="text/javascript"></script>
    <script src="../js/nicescroll/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui.js" type="text/javascript"></script>
    <script src="../js/jsCommon.js" type="text/javascript"></script>
    <script src="../js/combinationkey.js" type="text/javascript"></script>
    <script src="../js/shortcut.js" type="text/javascript"></script>
    <script src="../js/jquery.toast.js" type="text/javascript"></script>
    <script src="../js/jquery.timepicker.js" type="text/javascript"></script>
    <script src="../js/jquerysession.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/print.js"></script> 

    <link rel="icon" type="image/gif" href="../images/favicon.jpg" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
    <link href="../css/common-styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/pagination.css" rel="stylesheet" type="text/css" />
    <link href="../css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../css/ValidationEngine.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../css/chosen.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.toast.css" rel="stylesheet" type="text/css" />
    <link href="../css/jquery.timepicker.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">

        $(function () {
            if ($('#<%=hidNewFolderName.ClientID%>').val() == "") {
                var links = document.getElementsByTagName("LINK");
                var link = document.getElementsByTagName("a");
                $(links).each(function (index) {
                    if (index == 12)
                        $('#<%=hidNewFolderName.ClientID%>').val($(this).attr("href").split('/')[3]);
                });
                $('#<%=btnFolder.ClientID%>').click();
            }
         });
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <asp:HiddenField ID="hidNewFolderName" runat="server" ClientIDMode="Static" Value="" />
        <div>
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" EnableDatabaseLogonPrompt="False"
                GroupTreeImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/tree/"
                ToolbarImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/toolbar/"
                DisplayGroupTree="False" Width="100%" BestFitPage="True" />
            <asp:Button runat="server" ID="btnFolder" OnClick="btnFolderRename_Clik" />
        </div>
    </form>
</body>
</html>
