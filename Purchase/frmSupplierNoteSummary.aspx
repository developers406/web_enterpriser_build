﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmSupplierNoteSummary.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmSupplierNoteSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }

    
    .supplierNote_lblvendorcode {
    float: left;
    width: 33%;

     }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .supplierNote td:nth-child(1), .supplierNote th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
            text-align:left;
        }

        .supplierNote td:nth-child(2), .supplierNote th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
            text-align:left;
            display:none;
        }

        .supplierNote td:nth-child(3), .supplierNote th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
            text-align:left;
        }

        .supplierNote td:nth-child(4), .supplierNote th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
            text-align:left;
        }

        .supplierNote td:nth-child(5), .supplierNote th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
            text-align:left;
        }

        .supplierNote td:nth-child(6), .supplierNote th:nth-child(6) {
            min-width: 275px;
            max-width: 275px;
            text-align:left;
        }

        .supplierNote td:nth-child(7), .supplierNote th:nth-child(7) {
            min-width: 125px;
            max-width: 125px;
            text-align:left;
        }

        .supplierNote td:nth-child(8), .supplierNote th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
            text-align:left;
        }

        .supplierNote td:nth-child(9), .supplierNote th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
            text-align:left;
        }

        .supplierNote td:nth-child(10), .supplierNote th:nth-child(10) {
            min-width: 90px;
            max-width: 90px;
            text-align:right !important;
        }

        .supplierNote td:nth-child(11), .supplierNote th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
            text-align:left;
        }

        .supplierNote td:nth-child(12), .supplierNote th:nth-child(12) {
            min-width: 150px;
            max-width: 150px;
            text-align:left;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         $(document).ready(function (e) {
             $('#<% =txtTran_no.ClientID %>').keydown(function (e) {
                   if (e.keyCode == 13) {
                       $('#<%=lnkrefresh.ClientID %>').focus();
                }
            });
           });
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SupplierNoteSummary');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SupplierNoteSummary";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        ///Page Load
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
            try {
                shortcut.add("Ctrl+A", function () {
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').removeClass('lowercase');
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').addClass('uppercase');
                    $('#lblCreditNote').addClass('uppercase');
                    $('#lblCreditNote').removeClass('lowercase');

                    $("#<%=hidbillType.ClientID%>").val('A');

                });
                shortcut.add("Ctrl+B", function () {
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').removeClass('uppercase');
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').addClass('lowercase');
                    $('#lblCreditNote').removeClass('uppercase');
                    $('#lblCreditNote').addClass('lowercase');
                    $("#<%=hidbillType.ClientID%>").val('B');

                });

                if ($("#<%=hidbillType.ClientID %>").val() == "A") {
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').removeClass('lowercase');
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').addClass('uppercase');
                    $('#lblCreditNote').addClass('uppercase');
                    $('#lblCreditNote').removeClass('lowercase');
                }
                else {
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').removeClass('uppercase');
                    $('#ContentPlaceHolder1_lblCreditandDebitNote').addClass('lowercase');
                    $('#lblCreditNote').removeClass('uppercase');
                    $('#lblCreditNote').addClass('lowercase');
                }
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkNewButton.ClientID %>').css("display", "inline-block");
                }
                else {
                    $('#<%=lnkNewButton.ClientID %>').css("display", "none");
                }
                if ($("#<%=txtfromdate.ClientID %>").val() == "") {
                $("#<%=txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
                $("#<%=txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

                $("#<%=txtTranDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                $("#<%=txtRefInvDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                $("#<%=txtVendorCDDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                $("#<%=txtAddress.ClientID %>").attr("readonly", true);
                $("#<%=txtCountry.ClientID %>").attr("readonly", true);
                   
                }
                fncGetSupplierNoteObj();
            }
                
            
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Create SupplierNote Table
        function fncGetSupplierNoteObj() {
            debugger;
            try {
                var obj = {};
                obj.search = $("#<%=txtNoofSearchRecord.ClientID %>").val();
                obj.vendorCode = $("#<%=txtVendor.ClientID %>").val();
                obj.locatioCode = $("#<%=ddlLocationcode.ClientID %>").val();
                obj.tranType = $("#<%=ddlTranType.ClientID %>").val();
                obj.tranno = $("#<%=txtTran_no.ClientID %>").val();

                if ($("#<%=cbAllRecord.ClientID %>").is(":checked")) {
                    obj.frmDate = "";
                    obj.toDate = "";
                }
                else {
                    obj.frmDate = $.trim($("#<%=txtfromdate.ClientID %>").val());
                    obj.toDate = $.trim($("#<%=txttodate.ClientID %>").val());
                } 
                obj.billtype = $("#<%=hidbillType.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: "frmSupplierNoteSummary.aspx/fncGetSupplierNote",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncCreateSupplierNoteTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Create Supplier Table
        function fncCreateSupplierNoteTable(msg) {
            try {
                var objSupplierNote, tblSupplierNotetbody;
                objSupplierNote = jQuery.parseJSON(msg.d);
                //ImageUrl = "~/images/print1.png"
                tblSupplierNotetbody = $("#tblSupplierNote tbody");
                tblSupplierNotetbody.children().remove();
                for (var i = 0; i < objSupplierNote.length; i++) {
                    row = "<tr><td>" +
                        objSupplierNote[i]["RowNo"] + "</td><td  id='tdEditDelete_" + i + "'>" +
                        "<input id=\"hide\" type=\"image\" src=\"../images/edit-icon.png\" onclick=\"fncCreditAndDebitEditDelete(this,'Edit');return false;\" />" + "</td><td>" +
                        "<input type=\"image\" src=\"../images/print1.png\" onclick=\"fncprint(this);return false;\" />" + "</td><td>" +
                        "<input type=\"image\" src=\"../images/No.png\" onclick=\"fncCreditAndDebitEditDelete(this,'Delete');return false;\" />" + "</td><td>" +
                        objSupplierNote[i]["Vendorcode"] + "</td><td>" +
                        objSupplierNote[i]["VendorName"] + "</td><td id='tdTranNo_" + i + "' >" +
                        objSupplierNote[i]["DebitNoteNo"] + "</td><td>" +
                        objSupplierNote[i]["DebitNoteDate"] + "</td><td> " +
                        objSupplierNote[i]["TranType"] + "</td><td> " +
                        objSupplierNote[i]["NetAmount"].toFixed(2) + "</td><td id='tdStatus_" + i + "' >" +
                        objSupplierNote[i]["Status"] + "</td><td id='tdType_" + i + "' >" +
                        objSupplierNote[i]["Type"] +
                        "</td></tr>";
                    //if(objSupplierNote[i]["Status"] =="Paid")
                    //    $('td input[id*=hide]').attr("disabled", true);
                    //else
                    //    $('td input[id*=hide]').removeAttr("disabled");
                    tblSupplierNotetbody.append(row);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncprint(source) {
            var obj;
            try {

                obj = $(source).parent().parent();
                $('#<%=hidTranno.ClientID%>').val(obj.find('td[id*=tdTranNo]').text());
                $('#<%=btnPrint.ClientID%>').click();

            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        //Refresh
        function fncRefresh() {
            try {
                fncGetSupplierNoteObj();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Get VendorName from 
        function fncVendorChange() {
            try {

                if ($('#<%=txtVendor.ClientID %>').val() == "") {
                    $("#<%=txtvendorname.ClientID %>").val('');
                    fncGetSupplierNoteObj();
                }
                else if ($('#<%=txtVendor.ClientID %>').val() != "") {
                    $("#<%=txtvendorname.ClientID %>").val(Description);
                    fncGetSupplierNoteObj();
                } 
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Initialize Credit and Debit Note
        function fncInitializeCreditandDebitNote() {
            try {
                $("#divCreditandDebitNote").dialog({
                    resizable: false,
                    closeOnEscape: false,
                    height: 640,
                    width: 400,
                    modal: true,
                    title: "Credit and Debit Note",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        //Clear All textbox
        function fncClear() {
            try {
                $("#<%=txtTranNo.ClientID %>").val('');

                $('#<%=ddlCDLocationCode.ClientID %>')[0].selectedIndex = 0;
                $('#<%=txtPopUpVendor.ClientID %>').val('');
                $("#<%=ddlReason.ClientID %>")[0].selectedIndex = 0;
                $("#<%=ddlTaxPerc.ClientID %>")[0].selectedIndex = 0;

                $("#<%=ddlCDLocationCode.ClientID %>").trigger("liszt:updated");
                <%--$("#<%=ddlCDVendorCode.ClientID %>").trigger("liszt:updated");--%>
                $("#<%=ddlReason.ClientID %>").trigger("liszt:updated");
                $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");

                $("#<%=txtAddress.ClientID %>").val('');
                $("#<%=txtCountry.ClientID %>").val('');
                $("#<%=txtRefInvNo.ClientID %>").val('');
                $("#<%=txtRefInvDate.ClientID %>").val(currentDateformat_Master);
                $("#<%=txtvendorCDNo.ClientID %>").val('');
                $("#<%=txtVendorCDDate.ClientID %>").val(currentDateformat_Master);
                $("#<%=txtDesc.ClientID %>").val('');

                $("#<%=txtTotalAmt.ClientID %>").val('');
                $("#<%=txtNetAmt.ClientID %>").val('');
                $("#<%=txtTaxAmt.ClientID %>").val('');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSaveCreditAndDebitNote() {
            try {
                var obj = {};
                var tax = {};

                var locCode, vendorCode, vendorName, totalAmt, taxPer, taxAmt, netAmt, reason;

                locCode = $('#<%=ddlCDLocationCode.ClientID %>').val();
                vendorCode = $('#<%=txtPopUpVendor.ClientID %>').val();
                vendorName = $('#<%=hidVendorName.ClientID %>').val();<%--$('#<%=ddlCDVendorCode.ClientID %>').find('option:selected').text();--%>
                totalAmt = $('#<%=txtTotalAmt.ClientID %>').val();
                <%--taxPer = $('#<%=ddlTaxPerc.ClientID %>').val();--%>
                tax = $('#<%=ddlTaxPerc.ClientID %>').find('option:selected').text().split('-');
                taxPer = $.trim(tax[1]);
                reason = $('#<%=ddlReason.ClientID %>').val();

                if (locCode == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=ddlCDLocationCode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.alert_Location%>'); 
                    return false;
                }
                else if (vendorCode == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtPopUpVendor.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.alert_vendorcode%>');


                    return false;
                }
                else if (totalAmt == "" || parseFloat(totalAmt) == 0) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtTotalAmt.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Amount%>');
                    return false;
                }
                else if (taxPer == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=ddlTaxPerc.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.alert_tax%>');
                    return;
                }
                else if (reason == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=ddlReason.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.alert_reasoncode%>');
                    return false;
                }

                obj.tranNo = $.trim($('#<%=txtTranNo.ClientID %>').val());
                obj.tranDate = $.trim($('#<%=txtTranDate.ClientID %>').val());
                obj.refNo = $.trim($('#<%=txtRefInvNo.ClientID %>').val());
                obj.refDate = $.trim($('#<%=txtRefInvDate.ClientID %>').val());
                obj.vendorCDNo = $.trim($('#<%=txtvendorCDNo.ClientID %>').val());
                obj.vendorCDDate = $.trim($('#<%=txtVendorCDDate.ClientID %>').val());
                obj.vendorCode = vendorCode;
                obj.vendorName = vendorName;
                obj.totalAmt = totalAmt;
                obj.taxPerc = taxPer;
                obj.taxAmt = $('#<%=txtTaxAmt.ClientID %>').val();
                obj.netAmt = $('#<%=txtNetAmt.ClientID %>').val();
                obj.desc = $('#<%=txtDesc.ClientID %>').val();
                obj.reasonCode = reason;
                obj.billtype = $('#<%=hidbillType.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: "frmSupplierNoteSummary.aspx/fncSaveCreditandDebitNote",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            ShowPopupMessageBox(msg.d + '<%=Resources.LabelCaption.save_Transaction%>');
                            $("#divCreditandDebitNote").dialog('close');
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get Vendor Detail
        function fncGetVendorDetail() {
            try {
                var objVendorDetail;
                var obj = {};
                obj.vendorCode = $('#<%=txtPopUpVendor.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: "frmSupplierNoteSummary.aspx/fncGetVendorDetail",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            objVendorDetail = jQuery.parseJSON(msg.d);

                            $('#<%=txtAddress.ClientID %>').val(objVendorDetail[0]["Address1"]);
                            $('#<%=txtCountry.ClientID %>').val(objVendorDetail[0]["Country"]);
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Amount Calculation
        function fncAmtCalc() {
            try {

                var totalAmt, taxper, netAmt, taxAmt = 0;
                var taxSplit = {};

                totalAmt = $('#<%=txtTotalAmt.ClientID %>').val();
                taxSplit = $('#<%=ddlTaxPerc.ClientID %> option:selected').text().split('-'); 
                taxper = taxSplit[1]; 

                if (totalAmt == "" || parseFloat(totalAmt) == 0 || taxper == "") {
                    return;
                }

                if (parseFloat(totalAmt) > 0)
                    $('#<%=lblCreditandDebitNote.ClientID %>').text('<%=Resources.LabelCaption.lbl_CreditNote%>');
                else if (parseFloat(totalAmt) < 0)
                    $('#<%=lblCreditandDebitNote.ClientID %>').text('<%=Resources.LabelCaption.lbl_DebitNote%>');

            if ($("#<%=rbnExclusive.ClientID %>").is(":checked")) {
                    $("#<%=ddlTaxPerc.ClientID%>").attr("disabled", false);
                    $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");
                    taxAmt = (parseFloat(totalAmt) * parseFloat(taxper)) / (100);
                    netAmt = parseFloat(taxAmt) + parseFloat(totalAmt);
                }
                else if ($("#<%=rbnInclusive.ClientID %>").is(":checked")) {
                    $("#<%=ddlTaxPerc.ClientID%>").attr("disabled", false);
                    $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");
                    taxAmt = (parseFloat(totalAmt) * parseFloat(taxper)) / (100 + parseFloat(taxper));
                    netAmt = parseFloat(totalAmt);
                }
                else {
                    netAmt = parseFloat(totalAmt);
                    $('#<%=ddlTaxPerc.ClientID %>').val('0.00');
                    $("#<%=ddlTaxPerc.ClientID%>").attr("disabled", true);
                    $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");
                }

            $('#<%=txtTaxAmt.ClientID %>').val(taxAmt.toFixed(2));
                $('#<%=txtNetAmt.ClientID %>').val(netAmt.toFixed(2));




            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Edit Saved Transaction
        function fncCreditAndDebitEditDelete(source, value) {
            try {

                var rowobj;
                rowobj = $(source).parent().parent();

                var obj = {};
                var objEdit;
                obj.tranNo = rowobj.find('td[id*=tdTranNo]').text();
                var status = rowobj.find('td[id*=tdStatus]').text();
                var type = rowobj.find('td[id*=tdType]').text();
                obj.mode = value;
                if (value == "Edit") {
                    if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                        ShowPopupMessageBox("You have no permission to Edit this");
                        return false;
                    }
                }
                if (value == "Delete") {
                    if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                        ShowPopupMessageBox("You have no permission to Delete this");
                        return false;
                    }
                }
                if (value == "Edit" || value == "Delete") {
                    if (status == "Paid") {
                        ShowPopupMessageBox("Paid Completed.");
                        return false;
                    }
                    else if (type == "Auto Debit Note") {
                        ShowPopupMessageBox("Auto Debit Note Cannot be Edit or Delete.");
                        return false;
                    }
                    else if (value == "Delete") {
                    obj.mode = value;
                    Delete(rowobj, value);
                    return false;
                }
            }
                $.ajax({
                    type: "POST",
                    url: "frmSupplierNoteSummary.aspx/fncEditAndDelete",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {

                            objEdit = jQuery.parseJSON(msg.d);

                            if (value == "Edit") {
                                $('#<%=txtTranNo.ClientID %>').val(obj.tranNo);
                                $('#<%=txtPopUpVendor.ClientID %>').val(objEdit[0]["Vendorcode"]);
                                <%--$("#<%=ddlCDVendorCode.ClientID %>").trigger("liszt:updated");--%>

                                $('#<%=ddlCDLocationCode.ClientID %>').val(objEdit[0]["LocationCode"]);
                                $("#<%=ddlCDLocationCode.ClientID %>").trigger("liszt:updated");

                                $('#<%=txtRefInvNo.ClientID %>').val(objEdit[0]["RefInvoiceNo"]);
                                $('#<%=txtRefInvDate.ClientID %>').val(objEdit[0]["RefInvoiceDate"]);
                                $('#<%=txtvendorCDNo.ClientID %>').val(objEdit[0]["vendorCNNO"]);
                                $('#<%=txtVendorCDDate.ClientID %>').val(objEdit[0]["VendorCNDate"]);
                                $('#<%=txtDesc.ClientID %>').val(objEdit[0]["Remarks"]);

                                $('#<%=ddlReason.ClientID %>').val(objEdit[0]["ReasonCode"]);
                                $("#<%=ddlReason.ClientID %>").trigger("liszt:updated");

                                $('#<%=txtTotalAmt.ClientID %>').val(objEdit[0]["TotalAmount"].toFixed(2));


                                $('#<%=ddlTaxPerc.ClientID %>').val(objEdit[0]["TaxCode"].toFixed(2));
                                $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");

                                $('#<%=txtTaxAmt.ClientID %>').val(objEdit[0]["TaxAmt"].toFixed(2));
                                $('#<%=txtNetAmt.ClientID %>').val(objEdit[0]["NetAmount"].toFixed(2));
                                fncGetVendorDetail();
                                fncInitializeCreditandDebitNote();
                            }
                            else if (objEdit[0]["Status"] == "Deleted") {
                                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_TranDelete%>');
                                fncGetSupplierNoteObj();
                            }
                            else {
                                ShowPopupMessageBox('<%=Resources.LabelCaption.msg_TranPaid%>');
                            }
                    }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);

        }
    }
    function Delete(source, value) {
        var obj = {};
        var objEdit;
        obj.tranNo = source.find('td[id*=tdTranNo]').text();
        obj.mode = value;

        $("#dialog-confirm").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "YES": function () {
                    $(this).dialog("close");
                    $.ajax({
                        type: "POST",
                        url: "frmSupplierNoteSummary.aspx/fncEditAndDelete",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != "") {

                                objEdit = jQuery.parseJSON(msg.d);

                                if (objEdit[0]["Status"] == "Deleted") {
                                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_TranDelete%>');
                                 fncGetSupplierNoteObj();
                             }
                             else {
                                 ShowPopupMessageBox('<%=Resources.LabelCaption.msg_TranPaid%>');
                             }
                         }
                    },
                          error: function (data) {
                              ShowPopupMessageBox(data.message);
                          }
                      });
                    },
                    "NO": function () {
                        $(this).dialog("close");
                    }
                }
         });
 }

 //Create New Debit and Credit Note
 function fncNewentry() {
     try {
         $('#<%=txtTranNo.ClientID %>').val('');
                $('#<%= txtPopUpVendor.ClientID %>').val('');
                <%--$("#<%=ddlCDVendorCode.ClientID %>").trigger("liszt:updated");--%>


                $('#<%= ddlCDLocationCode.ClientID %>')[0].selectedIndex = 0;
                $("#<%=ddlCDLocationCode.ClientID %>").trigger("liszt:updated");

                $('#<%=txtRefInvNo.ClientID %>').val('');
                $('#<%=txtRefInvDate.ClientID %>').val(currentDateformat_Master);
                $('#<%=txtvendorCDNo.ClientID %>').val('');
                $('#<%=txtVendorCDDate.ClientID %>').val(currentDateformat_Master);
                $('#<%=txtDesc.ClientID %>').val('');

                $('#<%= ddlReason.ClientID %>')[0].selectedIndex = 0;
                $("#<%=ddlReason.ClientID %>").trigger("liszt:updated");

                $('#<%=txtTotalAmt.ClientID %>').val('0.00');


                $('#<%= ddlTaxPerc.ClientID %>')[0].selectedIndex = 0;
                $("#<%=ddlTaxPerc.ClientID %>").trigger("liszt:updated");

                $('#<%=txtTaxAmt.ClientID %>').val('0.00');
                $('#<%=txtNetAmt.ClientID %>').val('0.00');

                fncInitializeCreditandDebitNote();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncTaxPercChange() {
            try {
                fncAmtCalc();
                setTimeout(function () {
                    $("#<%=ddlReason.ClientID %>").trigger("liszt:open");
                }, 50);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetValue() {
            try {
                if (ControlID == "txtPopUpVendor") {
                    $("#<%=txtPopUpVendor.ClientID %>").val(Code);
                    $("#<%=hidVendorName.ClientID %>").val(Description);
                    $("#<%=txtAddress.ClientID %>").val(Address1);
                    $("#<%=txtCountry.ClientID %>").val(Country);
                }
                else { 
                    fncVendorChange();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page uppercase" id="lblCreditNote"  >
                    <%=Resources.LabelCaption.lbl_ViewSupplierNote%>
                </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full">
            <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
            </div>
            <div>
                <div class="supplierNote_header">
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblvendorcode" runat="server" Text='<%$ Resources:LabelCaption,lblVendorCode %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                    </div>
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblVendorname" runat="server" Text='<%$ Resources:LabelCaption,lblVendorName %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:TextBox ID="txtvendorname" runat="server" CssClass="form-control-res bind-name"></asp:TextBox>
                    </div>
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblTran_no" runat="server" Text='Tran No'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:TextBox ID="txtTran_no" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="supplierNote_header">
                    <div class="supplierNote_lblvendorcode" style="/* width: 33%; */"  >
                        <asp:Label ID="lblLocationcode" runat="server" Text='<%$ Resources:LabelCaption,lblLocationCode %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:DropDownList ID="ddlLocationcode" runat="server" Style="width: 100%" onchange="fncGetSupplierNoteObj()">
                        </asp:DropDownList>
                    </div>
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblTranType" runat="server" Text='<%$ Resources:LabelCaption,lbl_TranType %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:DropDownList ID="ddlTranType" runat="server" Style="width: 100%" onchange="fncGetSupplierNoteObj()">
                            <asp:ListItem Text="ALL" Value="ALL"></asp:ListItem>
                            <asp:ListItem Text="CREDIT" Value="CREDIT"></asp:ListItem>
                            <asp:ListItem Text="DEBIT" Value="DEBIT"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="supplierNote_header">
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblfromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="supplierNote_lblvendorcode">
                        <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                    </div>
                    <div class="supplierNote_txtvendorcode">
                        <asp:TextBox ID="txttodate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="supplierNote_header">
                    <div class="supplierNote_searchlbl">
                        <asp:Label ID="lblSearchRecord" runat="server" Text='<%$ Resources:LabelCaption,lblRecordSearchDays %>'></asp:Label>
                    </div>
                    <div class="supplierNote_searchtxt">
                        <asp:TextBox ID="txtNoofSearchRecord" runat="server" CssClass="form-control-res">100</asp:TextBox>
                    </div>
                    <div class="supplierNote_searchlbl">
                        <asp:CheckBox ID="cbAllRecord" runat="server" Text='<%$ Resources:LabelCaption,cbGetAllRecord %>' />
                    </div>
                    <div class="supplierNote_searchtxt">
                        <asp:LinkButton ID="lnkrefresh" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_refresh %>'
                            OnClientClick="fncRefresh();return false;"></asp:LinkButton>
                    </div>
                </div>
                <div class="supplierNote_All">
                    <asp:LinkButton ID="lnkNewButton" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_New %>'
                        OnClientClick="fncNewentry();return false;"></asp:LinkButton>
                </div>
            </div>
            <div class="Payment_fixed_headers supplierNote">
                <table id="tblSupplierNote" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col" class="hiddencol">Edit
                            </th>
                            <th scope="col">Print
                            </th>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">VendorCode
                            </th>
                            <th scope="col">VendorName
                            </th>
                            <th scope="col">TranNo
                            </th>
                            <th scope="col">TranDate
                            </th>
                            <th scope="col">TranType
                            </th>
                            <th scope="col">Amount
                            </th>
                            <th scope="col">Status
                            </th>
                            <th scope="col">Type
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <div id="divCreditandDebitNote">
            <div class="creditandDebitNote_hdr">
                <asp:Label ID="lblCreditandDebitNote" runat="server" CssClass ="uppercase" Text='<%$ Resources:LabelCaption,lbl_CreditNote %>'></asp:Label>
            </div>
            <div class="creditandDebir_radiobtn">
                <div class="creditandDebit_checkbox">
                    <asp:RadioButton ID="rbnExclusive" runat="server" Checked="true" GroupName="tax"
                        onchange="fncAmtCalc();" Text='<%$ Resources:LabelCaption,lbl_Exclusive %>' />
                </div>
                <div class="creditandDebit_checkbox">
                    <asp:RadioButton ID="rbnInclusive" runat="server" GroupName="tax" onchange="fncAmtCalc();"
                        Text='<%$ Resources:LabelCaption,lbl_Inclusive %>' />
                </div>
                <div class="creditandDebit_checkbox">
                    <asp:RadioButton ID="rbnZero" runat="server" GroupName="tax" onchange="fncAmtCalc();"
                        Text='<%$ Resources:LabelCaption,lbl_Zero %>' />
                </div>
            </div>
            <div class="supplierNote_Body">
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblTranNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_TransactionNo %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtTranNo" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblTranDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_TranDate %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtTranDate" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblCDLocationcode" runat="server" Text='<%$ Resources:LabelCaption,lblLocationCode %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:DropDownList ID="ddlCDLocationCode" runat="server" Style="width: 200px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblCDvendorCode" runat="server" Text='<%$ Resources:LabelCaption,lblVendorCode %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <%--<asp:DropDownList ID="ddlCDVendorCode" runat="server" onchange="fncGetVendorDetail();return false;"
                            Style="width: 200px">
                        </asp:DropDownList>--%>
                        
                     <asp:TextBox ID="txtPopUpVendor" runat="server" MaxLength="50" CssClass="form-control-res" 
                           onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtPopUpVendor', 'txtRefInvNo');"></asp:TextBox> <%--onchange="fncGetVendorDetail();return false;"--%>
                    </div>
                </div>
                <%--  <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblCDVendorName" runat="server" Text='<%$ Resources:LabelCaption,lblVendorName %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtCDVendorName" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>--%>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblAddress" runat="server" Text='<%$ Resources:LabelCaption,lbl_Address %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblCountry" runat="server" Text='<%$ Resources:LabelCaption,lbl_Country %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblRefInvNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefInvoiceNo %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtRefInvNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblRefInvDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefInvoiceDate %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtRefInvDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblvendorCDNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_VendorCDNo %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtvendorCDNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblVendorCDDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_VendorCDDate %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtVendorCDDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblDecs" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblTotalAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalAmount %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtTotalAmt" runat="server" CssClass="form-control-res" onchange="fncAmtCalc();"
                            onkeypress="return isNumberKeywithMin(event)" MaxLength="18"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblTaxPerc" runat="server" Text='<%$ Resources:LabelCaption,lbl_taxPerc %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:DropDownList ID="ddlTaxPerc" runat="server" Style="width: 200px" onchange="fncTaxPercChange();return false;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblTaxAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_TaxAmt %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtTaxAmt" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblNetAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetAmt %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:TextBox ID="txtNetAmt" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <div class="creditandDebit_control_lbl">
                        <asp:Label ID="lblReason" runat="server" Text='<%$ Resources:LabelCaption,lbl_Reason %>'></asp:Label>
                    </div>
                    <div class="creditandDebit_control_txt">
                        <asp:DropDownList ID="ddlReason" runat="server" Style="width: 200px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lnkSave %>'
                            OnClientClick= "return fncSaveCreditAndDebitNote();"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                            OnClientClick="fncClear();return false;"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hidTranno" runat="server" Value="" />
        <asp:HiddenField ID="hidVendorName" runat="server" Value="" />
        <asp:Button ID="btnPrint" runat="server" Text="Button" OnClick="btnPrint_click" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
        <asp:HiddenField ID="hidbillType" runat="server" Value ="A"/>
    </div>

</asp:Content>
