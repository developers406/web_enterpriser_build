﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTextileBarcodePrint.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmTextileBarcodePrint" %>

<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        /*Attribute Count Table */
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        #grdAttributeSize {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeSize td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeSize tr:nth-child(even) {
                background-color: white;
            }

            #grdAttributeSize tr:hover {
                background-color: white;
            }

            #grdAttributeSize th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: indigo;
                font-weight: bolder;
                color: white;
            }

        /*Item Detials*/

        #grdItemDetails {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
        }

            #grdItemDetails td, #grdItemDetails th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdItemDetails tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdItemDetails tr:hover {
                background-color: lavender;
            }

            #grdItemDetails th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: green;
                font-weight: bolder;
                color: white;
                width: 100%;
            }

        #tblStyleCode {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
            height: auto;
        }

            #tblStyleCode td, #tblStyleCode th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #tblStyleCode tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #tblStyleCode tr:hover {
                background-color: pink;
            }

            #tblStyleCode th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: red;
                font-weight: bolder;
                color: white;
            }

        .divtblStyleCode th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: red;
            font-weight: bolder;
            color: white;
        }

            .divtblStyleCode td:nth-child(2), .divtblStyleCode th:nth-child(2) {
                min-width: 110px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblStyleCode td:nth-child(3), .divtblStyleCode th:nth-child(3) {
                min-width: 110px;
                max-width: 65px;
                text-align: right !important;
            }

            .divtblStyleCode td:nth-child(4), .divtblStyleCode th:nth-child(4) {
                min-width: 110px;
                max-width: 65px;
                text-align: right !important;
            }


        .divtblItemdetail th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: indigo;
            font-weight: bolder;
            color: white;
        }

            .divtblItemdetail td:nth-child(1), .divtblItemdetail th:nth-child(1) {
                min-width: 50px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(2), .divtblItemdetail th:nth-child(2) {
                min-width: 85px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(3), .divtblItemdetail th:nth-child(3) {
                min-width: 85px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(4), .divtblItemdetail th:nth-child(4) {
                min-width: 310px;
                max-width: 65px;
                text-align: left !important;
            }

            .divtblItemdetail td:nth-child(5), .divtblItemdetail th:nth-child(5) {
                min-width: 65px;
                max-width: 60px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(6), .divtblItemdetail th:nth-child(6) {
                min-width: 75px;
                max-width: 55px;
                text-align: right !important;
            }

            .divtblItemdetail td:nth-child(7), .divtblItemdetail th:nth-child(7) {
                min-width: 75px;
                max-width: 65px;
                text-align: right !important;
            }

            .divtblItemdetail td:nth-child(8), .divtblItemdetail th:nth-child(8) {
                min-width: 75px;
                max-width: 65px;
                text-align: center !important;
            }

            .divtblItemdetail td:nth-child(9), .divtblItemdetail th:nth-child(9) {
                min-width: 75px;
                max-width: 65px;
                text-align: center !important;
            }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'TextileBarcodePrint');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "TextileBarcodePrint";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">

        var gRowno = 0;
        var GRNNo = "";

        var pkdDate;
        var expiredDate;
        var dateFormat;
        var hideMRP;
        var hidePrice;
        var hidepkdDate;
        var hideexpDate;
        var hidecompany;
        var template;
        var size;
        var print;
        //Get Barcode User Comtrol
        $(function () {

            try {

                pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
                expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
                dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
                hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
                hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
                hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
                hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
                hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
                template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
                size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
                print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");
                print.hide();

            }
            catch (err) {
                fncToastError(err.message);
            }

        });

        function pageLoad() {

            pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            debugger;
            GRNNo = getParameterByName("GRNNo");
            console.log(GRNNo);
            LoadDynamicAttribute(GRNNo);
            //alert(GRNNo);
        }

        ///Change days and months
        function fncDateMonth() {
            try {

                if ($(DateRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Days)');
                }
                else if ($(MonthRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Months)');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Template
        function fncTemplateChange() {
            try {
                debugger;

                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

        ///Open Barcode Popup
        function ShowPopupBarcode() {

            dateFormat[0].selectedIndex = 1;
            dateFormat.trigger("liszt:updated");

            $("#divBarcode").dialog({
                appendTo: 'form:first',
                title: "Barcode Print",
                width: 500,
                modal: true

            });
            setTimeout(function () {
                expiredDate.select().focus();
            }, 50);
        }

        ///Barcode Validation
        function fncBarcodeValidation() {
            try {


                if (size.val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    return false;
                }
                    
                else if ($("#grdItemDetails tbody").children().length == 0) {
                    ShowPopupMessageBox('Add Items to Print..!');
                    return false;
                }
                else {
                    return true;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncPriceCalc(evt) {
            try {

                var amountTD = null;
                var Qtytxt = null;
                var Stocktxt = null;
                var Costtxt = null;
                amountTD = $(evt.target).closest("td").next().next();
                Costtxt = $(evt.target).closest("td").next();
                Stocktxt = $(evt.target).closest("td").prev();
                Qtytxt = $(evt.target);

                console.log(Qtytxt.val())
                console.log(Stocktxt.text())

                var Qty = parseFloat(Qtytxt.val());
                var Stock = parseFloat(Stocktxt.text());
                var Cost = parseFloat(Costtxt.text());
                var OldQty = parseFloat(amountTD.text()) / parseFloat(Costtxt.text());
                debugger;

                if (Qty == OldQty)
                    Qtytxt.css('border-color', 'white');

                if (Qty == 0) {
                    //ShowPopupMessageBox('Invalid Transfer Quantity !');
                    fncToastInformation('Invalid Transfer Quantity !');
                    Qtytxt.val(OldQty);
                    return;
                }

                if (Qty > Stock) {
                    //ShowPopupMessageBox('Please Check Stock !');
                    fncToastInformation('Please Check Stock !');
                    Qtytxt.val(OldQty);
                    Qtytxt.focus().select();
                    Qty = OldQty;
                    Qtytxt.css('border-color', 'red');
                }


                var dTotalamount = parseFloat(Costtxt.text()) * Qty;
                amountTD.text(dTotalamount.toFixed(2))
                console.log(amountTD.text())
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetFocustoNextRow(evt, source) {
            try {
                var rowobj = $(source).parent().parent();
                var amountTD = null;
                var Qtytxt = null;
                var Costtxt = null;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //alert(charCode);
                //GSStock = rowobj.find('td input[id*="txtQty"]').val();

                if (charCode == 13 || charCode == 40) {


                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {

                        NextRowobj.find('td input[id*="txtQty"]').focus();
                        NextRowobj.find('td input[id*="txtQty"]').select();
                    }
                    else {

                        rowobj.siblings().find('td input[id*="txtQty"]').focus();
                        rowobj.siblings().find('td input[id*="txtQty"]').select();
                    }

                }
                else if (charCode == 38) {

                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtQty"]').focus();
                        prevrowobj.find('td input[id*="txtQty"]').select();
                    }
                    else {
                        rowobj.siblings().last().find('td input[id*="txtQty"]').focus();
                        rowobj.siblings().last().find('td input[id*="txtQty"]').select();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        // Get Attributes
        function fncGetAttributes(mode, attributecode) {
            var obj = {};
            try {
                obj.mode = mode;
                obj.attributecode = attributecode;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmCategoryAttributeMapping.aspx/fncGetAttributes") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBindStyleCode(jQuery.parseJSON(msg.d));
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBindStyleCode(jsonObj) {
            var StyleObj, row, rowNo = 0;
            var StyleBody;
            try {

                StyleObj = jsonObj;
                StyleBody = $("#tblStyleCode tbody");
                StyleBody.children().remove();

                for (var i = 0; i < StyleObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr onclick='javascript:showRow(this);' id='trStyleCode_" + i + "' tabindex='" + i + "' >"
                                            + "<td id='tdRowNo'  class='hiddencol' >" + rowNo + "</td>"
                                           + "<td id='tdStyleCode' >" + StyleObj[i]["StyleCode"] + "</td>"
                                           + "<td id='tdBalanceqty' align='right' >" + parseFloat(StyleObj[i]["Stock"]).toFixed(2) + "</td>"
                                           + "<td id='tdSellingPrice' align='right'>" + parseFloat(StyleObj[i]["SellingPrice"]).toFixed(2) + "</td>"
                                           + "</tr>";
                    StyleBody.append(row);
                    //StyleBody.children().dblclick(fncStyleRowDoubleClick);
                    StyleBody.children().click(fncSearchRowClick);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindInventory(jsonObj) {
            var StyleObj, row, rowNo = 0;
            var StyleBody;
            try {

                StyleObj = jsonObj;
                StyleBody = $("#tblInventory tbody");
                StyleBody.children().remove();
                console.log(jsonObj);
                for (var i = 0; i < StyleObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    //row = "<tr onclick='javascript:showRow(this);' id='trInventory_" + i + "' tabindex='" + i + "' >"
                    row = "<tr id='trInventory_" + i + "' tabindex='" + i + "' >"
                                            + "<td id='tdSize' >" + StyleObj[i]["ValueName"] + "</td>"
                                           + "<td id='tdCode' >" + StyleObj[i]["InventoryCode"] + "</td>"
                                           + "<td id='tdName' align='right' >" + StyleObj[i]["Description"] + "</td>"
                                           + "<td id='tdMRP' align='right'>" + parseFloat(StyleObj[i]["MRP"]).toFixed(2) + "</td>"
                                           + "<td id='tdSellingPrice' align='right'> " + parseFloat(StyleObj[i]["SellingPrice"]).toFixed(2) + "</td>"
                                           + "<td id='tdSDAmt' align='right'> " + parseFloat(StyleObj[i]["SDAmt"]).toFixed(2) + "</td>"
                                           + "<td id='tdDiscAmt' align='right'> " + parseFloat(StyleObj[i]["DiscAmt"]).toFixed(2) + "</td>"
                                           + "<td id='tdVendorCode' align='right'> " + StyleObj[i]["VendorCode"] + "</td>"
                                           + "<td id='tdStyleCode' align='right'> " + StyleObj[i]["StyleCode"] + "</td>"
                                           + "</tr>";
                    StyleBody.append(row);
                    //StyleBody.children().dblclick(fncStyleRowDoubleClick);
                    //StyleBody.children().click(fncSearchRowClick);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function showRow(row) {
            var x = row.cells;
            //alert(x[1].innerHTML);
            batchNo = $.trim(x[1].innerHTML);
            console.log(batchNo);
            $('#<%=hiddenStylecode.ClientID%>').val('');
            LoadDynamicAttribute(batchNo);
            $('#<%=hiddenStylecode.ClientID%>').val(batchNo);
        }


        function fncStyleRowDoubleClick() {
            try {

                var obj, batchNo;
                batchNo = $.trim($(this).find('td[id*=tdStyleCode]').text().trim());
                //console.log(batchNo); 
                LoadDynamicAttribute("TEST");
                //fncGetInventoryDetailForTransferOnEnter(batchNo);
                event.preventDefault();
                event.stopPropagation();

                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// Show Save Dailog 
        function fncShowSaveDailog(message) {
            $(function () {
                $("#Save-dialog").html(message);
                $("#Save-dialog").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                            window.location = "frmTransferOut.aspx";
                        }
                    },
                    modal: true
                });
            });
        };
        /// to show back ground color on click
        function fncSearchRowClick() {
            try {

                $(this).css("background-color", "pink");
                $(this).siblings().css("background-color", "transparent");
                $("tr:even").css("background-color", "#f2f2f2");

                $(this).css("background-color", "#f2f2f2").filter(function (index) {
                    return $(this).css("background-color") === "rgb(242, 242, 242)";
                }).css("background-color", "pink");

                //console.log($(this).css("background-color"));               
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncClear() {
            var itemName = "";
            //$('#grdControlId tr').each(function () {
            //    if (!this.rowIndex) return;
            //    var controlId = $(this).find("td").eq(1).html();
            //    console.log(controlId);
            //    itemName = itemName + "-" + $("[id$='" + controlId + "']").find('option:selected').text();
            //});
            //var selectedVal = $("[id$=ddlProduct]").find('option:selected').text();
            //itemName = selectedVal + itemName;
            //console.log(itemName);
            XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));

            return false;
        }

        function LoadDynamicAttribute(ProductCode) {
            var obj = {};
            obj.Code = ProductCode;

            $.ajax({
                type: "POST",
                url: "frmTextileBarcodePrint.aspx/GetAttributes",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var objvendor = jQuery.parseJSON(msg.d);
                    console.log(objvendor);
                    //console.log(Object.keys(objvendor).length);
                    numofTable = Object.keys(objvendor).length;
                    //debugger;

                    $('#grdAttributeSize').find("tr").remove();
                    if (numofTable > 0) {

                        if (objvendor.Table.length > 0) {
                            var bSizeCheck = false;
                            var bSleeveCheck = false;

                            funBindSizeGrid(objvendor.Table);
                            fncBindInventory(objvendor.Table1);
                            $("[id*=divAddButtons]").show();
                            fncDisableStockRows();

                            return false;
                        }
                        else {

                            return false;
                        }
                    }

                    return false;

                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });
        }


        function fncAddTransDetail() {
            try {

                var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
                //tblSearchData = $("#grdItemDetails");
                tblSearchData = $("#TempgrdItemDetails tbody");
                var itemName = "";
                var CheckSize = true;
                var iTranQty = 0;
                var sSize = "";
                var sItemCode = "";
                var sDescription = "";
                var sCost = "";
                var sStock = "";
                var sSP = "";
                var sMRP = "";
                var sSDAmt = "";
                var sDiscAmt = "";
                var iTotalCost = 0;
                var StyleCodeCheck = false;
                var iTxtboxQty = 0;
                var QuantityCheck = false;

                $("#grdAttributeSize tr:even").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                    QuantityCheck = true;
                                    return;
                                }
                                else {
                                    var isnum = /^\d+$/.test($("input:text", cells.eq(j)).val());
                                    if (isnum) {
                                        iTxtboxQty = iTxtboxQty + 1;
                                    }
                                }
                            }
                        }
                    }

                    iTxtboxQty = cells.length - iTxtboxQty;
                    $("input:text", cells.eq(iTxtboxQty)).select().focus();
                });

                if (QuantityCheck == false) {
                    fncToastInformation('Please Enter Print Quantity !');
                }

                if (StyleCodeCheck == false && QuantityCheck == true) {

                    var tablerow = $("#grdAttributeSize tr").length - 1;
                    tablerow = tablerow / 2;
                    var bitemAdded = false;
                    //for (var p = 1; p <= tablerow; ++p) {
                    // $("#grdAttributeSize tr:nth-last-child("+ p +")").each(function () {
                    $("#grdAttributeSize tr:even").each(function () {

                        var cells = $("td", this);
                        var Sleeve = "";
                        if (cells.length > 0) {
                            for (var j = 0; j < cells.length; ++j) {
                                if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() == 'SLEEVE') {
                                    Sleeve = cells.eq(j).text();
                                }
                                if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                    if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                        var Header = $(this).parents('table:first').find('th').eq(j).text().toUpperCase();
                                        Header = Header.replace('(', '').replace(')', '');
                                        console.log(Header + '--' + $("input:text", cells.eq(j)).val());
                                        iTranQty = $("input:text", cells.eq(j)).val();

                                        $("#tblInventory tr").each(function () {
                                            if (!this.rowIndex) return;
                                            debugger;
                                            sSize = $(this).find("td").eq(0).html();
                                            sItemCode = $(this).find("td").eq(1).html();
                                            sDescription = $(this).find("td").eq(2).html();
                                            sMRP = $(this).find("td").eq(3).html();
                                            sSP = $(this).find("td").eq(4).html();
                                            sSDAmt = $(this).find("td").eq(5).html();
                                            sDiscAmt = $(this).find("td").eq(6).html();
                                            sVendorCode = $(this).find("td").eq(7).html();
                                            sStyleCode = $(this).find("td").eq(8).html();

                                            isSleeve = sDescription.includes(Sleeve);
                                            if (isSleeve) {
                                                if (Header.toUpperCase() == sSize.toUpperCase()) {
                                                    console.log(sDescription);
                                                    gRowno = parseFloat(gRowno) + parseFloat(1);
                                                    iTotalCost = iTranQty * parseFloat(sCost);
                                                    row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                            '<td align="center">' + gRowno + '</td>' +
                                                            '<td>' + sStyleCode + '</td>' +
                                                                '<td>' + sItemCode + '</td>' +
                                                                '<td align="Left">' + sDescription + '</td>' +
                                                                '<td align="right">' + iTranQty + '</td>' +
                                                                '<td align="right">' + sMRP + '</td>' +
                                                                '<td align="right">' + sSP + '</td>' +
                                                                '<td align="right">' + sSDAmt + '</td>' +
                                                                '<td align="right">' + sDiscAmt + '</td>' +
                                                                '<td align="right">' + sVendorCode + '</td>' +
                                                                '</tr>';
                                                    tblSearchData.append(row);
                                                    bitemAdded = true;
                                                }
                                                else if (Header.toUpperCase() == 'Quantity') {
                                                    console.log(sDescription);
                                                    gRowno = parseFloat(gRowno) + parseFloat(1);
                                                    row = "<tr id='trTOItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                            '<td align="center">' + gRowno + '</td>' +
                                                            '<td>' + sStyleCode + '</td>' +
                                                                '<td>' + sItemCode + '</td>' +
                                                                '<td align="Left">' + sDescription + '</td>' +
                                                                '<td align="right">' + iTranQty + '</td>' +
                                                                '<td align="right">' + sMRP + '</td>' +
                                                                '<td align="right">' + sSP + '</td>' +
                                                                '<td align="right">' + sSDAmt + '</td>' +
                                                                '<td align="right">' + sDiscAmt + '</td>' +
                                                                '<td align="right">' + sVendorCode + '</td>' +
                                                                '</tr>';
                                                    tblSearchData.append(row);
                                                    bitemAdded = true;
                                                }

                                                //$("[id*=divTransferBtn]").show();
                                            }

                                            //break;
                                        });
                                    }
                                }
                            }
                        }
                    });
                    //}

                    fncUpdateTransDetailTable();
                    XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));
                    fncClear();

                    //if (bitemAdded == false) {
                    //    fncToastInformation('Enter Print Quantity!');
                    //}
                    //else {

                    //}
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUpdateTransDetailTable() {
            try {

                debugger;
                var tblSearchData = $("#grdItemDetails");
                var sItemCode = '';
                var bCheckItemCode = false;

                $("#TempgrdItemDetails tr").each(function () {
                    if (!this.rowIndex) return;
                    debugger;

                    sTempStyleCode = $(this).find("td").eq(1).html();
                    sTempItemCode = $(this).find("td").eq(2).html();
                    sTempDescription = $(this).find("td").eq(3).html();
                    iTranQty = $(this).find("td").eq(4).html();
                    sTempMRP = $(this).find("td").eq(5).html();
                    sTempSP = $(this).find("td").eq(6).html();
                    sTempSD = $(this).find("td").eq(7).html();
                    sTempDISC = $(this).find("td").eq(8).html();
                    sTempVendor = $(this).find("td").eq(9).html();

                    gRowno = $("#grdItemDetails tbody").children().length + 1;

                    $("#grdItemDetails tr").each(function () {
                        if (!this.rowIndex) return;
                        debugger;
                        sItemCode = $(this).find("td").eq(2).html();
                        if (sItemCode == sTempItemCode) {
                            $(this).find('td').eq(4).find('input').val(iTranQty);
                            $(this).find('td').eq(4).find('input').focus().select();
                            $(this).remove();
                            fncUpdateRowNow();
                            bCheckItemCode = true;
                        }
                    });

                    gRowno = $("#grdItemDetails tbody").children().length + 1;
                    row = "<tr id='trTOoItem_" + gRowno + "' onkeydown='return fncGRNRowKeydown(event,this);' tabindex='" + gRowno + "' >" +
                                                           '<td align="center">' + gRowno + '</td>' +
                                                           '<td>' + sTempStyleCode + '</td>' +
                                                               '<td>' + sTempItemCode + '</td>' +
                                                               '<td align="Left">' + sTempDescription + '</td>' +
                                                               '<td align="center"> <input type="text" id="txtQty" MaxLength="3" onfocus="this.select();" class="inputs" onkeydown="return fncSetFocustoNextRow(event,this);" oninput="return fncPriceCalc1(event)"  onkeypress= "return isNumberKeyGA(event);" style="width:100%; text-align:center;" value=' + iTranQty + '> </td>' +
                                                               '<td align="right">' + sTempMRP + '</td>' +
                                                               '<td align="right">' + sTempSP + '</td>' +
                                                               '<td align="right"  class= "hiddencol">' + sTempSD + '</td>' +
                                                               '<td align="right"  class= "hiddencol">' + sTempDISC + '</td>' +
                                                               '<td align="right" class= "hiddencol">' + sTempVendor + '</td>' +
                                                               '</tr>';

                    tblSearchData.append(row);
                    console.log('Row' + gRowno);

                });

                $('#TempgrdItemDetails').find("tr:gt(0)").remove();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncClear() {

            $("#grdAttributeSize tr:even").each(function () {
                var cells = $("td", this);
                var Sleeve = "";
                if (cells.length > 0) {
                    for (var j = 0; j < cells.length; ++j) {

                        if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {

                            if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                $("input:text", cells.eq(j)).val('0');
                            }
                        }
                    }
                }
            });

            return false;
        }


        function ValidateForm() {

            var rows = 0;
            rows = $('#grdItemDetails tbody tr').length;
            if (rows <= 0) {
             <%--   popUpObjectForSetFocusandOpen = $('#<%=txtSearch.ClientID %>');--%>
                ShowPopupMessageBoxandOpentoObject('No items to Proceed Transfer!');
                return false;
            }
            else {

                XmlGridValue($('#grdItemDetails tr'), $('#<%=GRNXmldata.ClientID %>'));
                return true;
            }
        }

        /// delete  Added Row
        function fncGRNRowKeydown(evt, source) {
            var charCode, rowobj, scrollheight;
            try {
                rowobj = $(source);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 46) {
                    $(source).remove();
                    fncUpdateRowNow();

                }
                //else if (charCode == 113) {
                //    fncOpenItemhistory(rowobj.find('td input[id*="txtItemcode"]').val());
                //}
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUpdateRowNow() {
            var rowNo = 0;
            try {

                $("#grdItemDetails tbody").children().each(function () {
                    rowNo = parseInt(rowNo) + 1;
                    debugger;
                    //$(this).find('td input[id*="txtSNo"]').val(rowNo);
                    $(this).find("td").eq(0).html(rowNo);
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncDisableStockRows() {
            try {
                // Disable Odd Rows in Table
                $("#grdAttributeSize tr:odd").each(function () {
                    var cells = $("td", this);
                    var Sleeve = "";
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {

                                $("input:text", cells.eq(j)).attr('readonly', 'true');
                                $("input:text", cells.eq(j)).css('background-color', 'lavender');
                                $("input:text", cells.eq(j)).css('font-weight', 'bold');

                            }
                        }
                    }
                });

                return false;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {

                fncToastInformation('Sending to Printer');

            }
            catch (err) {
                fncToastError(err.message);
                //console.log(err);
            }
        }

        function XmlGridValue(tableid, XmlID) {
            try {

                var xml = '<NewDataSet>';

                // $('#grdAttribute tr').each(function () {
                tableid.each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {
                            if ($(this).parents('table:first').find('th').eq(j).text().trim() == 'Qty')
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            else
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';

                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                console.log(xml);
                //alert(xml);

                XmlID.val(escape(xml));
                //alert(XmlID.val());
                //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function isNumberKeyGA(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
                evt.preventDefault();
            } // prevent if 

            if (charCode == 13) {
                if ($(evt.target).closest("td").next().length == 0) {
                    // is last  
                    Qtytxt = $(evt.target).closest("tr").children('td:first-child').find("input");
                    Qtytxt.focus().select();
                }

                Qtytxt = $(evt.target).closest("td").next().find("input");
                Qtytxt.focus().select();
            }


            return true;
        }



        var sortByProperty = function (property) {

            return function (x, y) {

                return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));

            };

        };

        /// Bind Size Grid
        function funBindSizeGrid(jsonObj) {

            try {

                if (jsonObj == null)
                    return;

                debugger;
                // Call Sort By Name
                //jsonObj = jsonObj.sort();
                jsonObj = jsonObj.sort(sortByProperty('SLEEVE'));
                ////Creating table rows 
                var table1 = document.getElementById("grdAttributeSize");
                //$('#grdAttributeSize').find("tr").remove();
                var BodyElement = document.createElement("tbody");

                if (jsonObj.length > 0) {
                    var keys = Object.keys(jsonObj[0]);
                    //keys = keys.filter(e => e !== 'SLEEVE');
                    //keys.splice(0, 0, "SLEEVE");
                    //console.log(arr.join());
                    var rowth = document.createElement("thead");
                    var rowHeader = document.createElement("tr");
                    var cellHeader = null;
                    for (var i = 0; i < keys.length; i++) {
                        cellHeader = document.createElement("th");
                        cellHeader.appendChild(document.createTextNode(keys[i]));
                        rowHeader.appendChild(cellHeader);
                    }
                    rowth.appendChild(rowHeader);
                    table1.appendChild(rowth);
                }


                //Creating table rows
                for (var j = 0; j < jsonObj.length; j++) {
                    var tableRowElement = document.createElement("tr");

                    var tableRowId = "grdAttributeSize_row" + j;
                    tableRowElement.setAttribute("id", tableRowId);
                    //tableElement.setAttribute("class", tableclass);

                    console.log(jsonObj[j]);
                    for (var i = 0; i < keys.length; i++) {
                        var tableCellElement = document.createElement("td");
                        var key = Object.keys(jsonObj[j])[i];
                        var cellText;
                        var value = jsonObj[j][key];
                        console.log(key);
                        console.log(value);

                        //cellText = document.createTextNode(value);
                        //tableCellElement.contentEditable = "true";
                        //cellText.innerHTML = "<input type='text' class='Margin'onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;'/>";
                        if (key.toUpperCase() != "SLEEVE") {
                            var el = document.createElement('input');
                            el.type = 'text';
                            el.name = key;
                            el.id = key;
                            el.setAttribute("style", "width:100%; text-align:center;");
                            el.setAttribute("MaxLength", "3");
                            el.value = value;
                            //el.setAttribute("onkeypress", "return isNumberKeyGA(this);");
                            el.onkeypress = function (event) { return isNumberKeyGA(event) }
                            el.onfocus = function (event) { event.target.select() }

                            tableCellElement.appendChild(el);
                        }
                        else {
                            cellText = document.createTextNode(value);
                            tableCellElement.appendChild(cellText);
                        }

                        tableRowElement.appendChild(tableCellElement);
                    }
                    BodyElement.appendChild(tableRowElement);
                }

                table1.appendChild(BodyElement);

            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Textile Item bind Margin Fix table
        function fncBindGRNTable() {
            try {

                //$('#grdControlId').find("tr:gt(0)").remove();
            }
            catch (err) {
                fncToastError(err.message);
            }

        }


        function fncShowItemEditDailog(Gidno) {
            $(function () {

                $("#ItemEdit").html("GRN Created Successfully, " + "<H1><B>Style Code:" + Gidno + "</B></H1>");
                $("#ItemEdit").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Ok": function () {
                            $(this).dialog("destroy");
                            window.location.href = "frmGoodsAcknowledgement.aspx";
                        }
                    },
                    modal: true
                });
            });
        };

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="breadcrumbs" class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">GRN</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">Barcode Print</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="container-group-price">

        <div class="container-right-grn" style="width: 100%; margin-left: 0%; height: auto; padding: 5px;" id="HideFilter_ContainerRight" runat="server">
            <div class="control-group-single-res" id="dynamicInput1">
                <table id="grdAttributeSize" cellspacing="0" rules="all" border="1">
                </table>
                <table id="tblInventory" cellspacing="0" rules="all" border="1" style="display: none;">
                    <thead>
                        <tr>
                            <th scope="col">Size</th>
                            <th scope="col">InventoryCode </th>
                            <th scope="col">Description</th>
                            <th scope="col">MRP</th>
                            <th scope="col">SellingPrice</th>
                            <th scope="col">SDAmount</th>
                            <th scope="col">DiscAmt</th>
                            <th scope="col">StyleCode</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <table id="TempgrdItemDetails" cellspacing="0" rules="all" border="1" style="display: none;">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">StyleCode</th>
                            <th scope="col">ItemCode</th>
                            <th scope="col">Description</th>
                            <th scope="col">Qty</th>
                            <th scope="col">MRP</th>
                            <th scope="col">SellingPrice</th>
                            <th scope="col">SDAmt</th>
                            <th scope="col">DiscAmt</th>
                            <th scope="col">VendorCode</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="button-contol" id="divAddButtons" style="display: none; margin-top: 10px;">
                <div class="control-button">
                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" OnClientClick="fncAddTransDetail(); return false;"><i class="icon-play"></i>Add Item</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="return fncClear();" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="container-right-grn" style="width: 100%; margin-left: 0%; height: 300px; padding: 5px;" id="div1" runat="server">
            <div class="container-right-grn" style="width: 58%; margin-left: 0%; margin-top: 0%; height: auto; padding: 5px;" id="divTransDetail" runat="server">

                <table class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">StyleCode</th>
                            <th scope="col">ItemCode</th>
                            <th scope="col">Description</th>
                            <th scope="col">Qty</th>
                            <th scope="col">MRP</th>
                            <th scope="col">Price</th>
                            <th scope="col" class="hiddencol">SDAmount</th>
                            <th scope="col" class="hiddencol">DiscAmt</th>
                            <th scope="col" class="hiddencol">VendorCode</th>
                        </tr>
                    </thead>
                </table>

                <div style="overflow: auto; height: 200px;">
                    <table id="grdItemDetails" class="divtblItemdetail" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr class="hiddencol">
                                <th scope="col">S.No</th>
                                <th scope="col">StyleCode</th>
                                <th scope="col">ItemCode</th>
                                <th scope="col">Description</th>
                                <th scope="col">Qty</th>
                                <th scope="col">MRP</th>
                                <th scope="col">SellingPrice</th>
                                <th scope="col">SDAmt</th>
                                <th scope="col">DiscAmt</th>
                                <th scope="col">VendorCode</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                <div class="button-contol" style="display: none; margin-top: 10px;">
                    <div class="control-button">
                        <asp:LinkButton ID="LinkButton3" runat="server" class="button-red" OnClientClick="ValidateForm();"><i class="icon-play"></i>Print Barcode</asp:LinkButton>
                    </div>
                    <div class="control-button" style="display: none;">
                        <asp:LinkButton ID="lnkbtnPrint1" runat="server" OnClick="lnkbtnPrint_Click" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>

            </div>
            <div class="container-right-grn" style="width: 39%; background-color: powderblue; margin-left: 2%; margin-top: 4%; height: auto; padding: 5px;" id="div2" runat="server">
                <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl"
                    OnPrintButtonClientClick="return fncBarcodeValidation();" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="control-button" style="display: block; float: right;">
                            <asp:LinkButton ID="lnkbtnPrint" runat="server" OnClientClick="return fncBarcodeValidation();" OnClick="lnkbtnPrint_Click" class="button-red"><i class="icon-play"></i>Print Barcode</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="ItemEdit" class="display_none">
            <asp:HiddenField ID="GRNXmldata" runat="server" Value="" />
            <asp:HiddenField ID="hiddenGRNNo" runat="server" Value="" />
            <asp:HiddenField ID="hiddenStylecode" runat="server" Value="" />
            <%--<asp:Button ID="lnkbtnPrint" runat="server" OnClick="lnkbtnPrint_Click" />--%>
        </div>
        <div id="Save-dialog" style="display: none">
        </div>

        <div id="divBarcode" style="display: none">
        </div>
    </div>

</asp:Content>
