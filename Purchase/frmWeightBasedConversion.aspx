﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmWeightBasedConversion.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmWeightBasedConversion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'WeightBasedConversion');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "WeightBasedConversion";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                     <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        </ul></div></div>
    
   

</asp:Content>
