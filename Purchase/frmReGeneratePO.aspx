﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmReGeneratePO.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmReGeneratePO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .Generate_PO td:nth-child(1), .Generate_PO th:nth-child(1)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(2), .Generate_PO th:nth-child(2)
        {
            min-width: 50px;
            max-width: 50px;
        }
        .Generate_PO td:nth-child(3), .Generate_PO th:nth-child(3)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(4), .Generate_PO th:nth-child(4)
        {
            min-width: 300px;
            max-width: 300px;
        }
        .Generate_PO td:nth-child(5), .Generate_PO th:nth-child(5)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(6), .Generate_PO th:nth-child(6)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(7), .Generate_PO th:nth-child(7)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(8), .Generate_PO th:nth-child(8)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(9), .Generate_PO th:nth-child(9)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(10), .Generate_PO th:nth-child(10)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(11), .Generate_PO th:nth-child(11)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(12), .Generate_PO th:nth-child(12)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(13), .Generate_PO th:nth-child(13)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(14), .Generate_PO th:nth-child(14)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(15), .Generate_PO th:nth-child(15)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(16), .Generate_PO th:nth-child(16)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(17), .Generate_PO th:nth-child(17)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(18), .Generate_PO th:nth-child(18)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(19), .Generate_PO th:nth-child(19)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Generate_PO td:nth-child(20), .Generate_PO th:nth-child(20)
        {
            min-width: 100px;
            max-width: 100px;
        }
         .Generate_PO td:nth-child(21), .Generate_PO th:nth-child(21)
        {
            min-width: 100px;
            max-width: 100px;
        }
        
         <%--VetDetailRepeater--%>
        .gid_vetDetail td:nth-child(1), .gid_vetDetail th:nth-child(1)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_vetDetail td:nth-child(2), .gid_vetDetail th:nth-child(2)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_vetDetail td:nth-child(3), .gid_vetDetail th:nth-child(3)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_vetDetail td:nth-child(4), .gid_vetDetail th:nth-child(4)
        {
            min-width: 120px;
            max-width: 120px;
        }
        .gid_vetDetail td:nth-child(5), .gid_vetDetail th:nth-child(5)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_vetDetail td:nth-child(6), .gid_vetDetail th:nth-child(6)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_vetDetail td:nth-child(7), .gid_vetDetail th:nth-child(7)
        {
            min-width: 100px;
            max-width: 100px;
        }
         .gid_vetDetail td:nth-child(8), .gid_vetDetail th:nth-child(8)
        {
            min-width: 100px;
            max-width: 100px;
        }
        
        
        <%--ItemHistory--%>
        .ReGeneratePO_Itemhistory td:nth-child(1), .ReGeneratePO_Itemhistory th:nth-child(1)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(2), .ReGeneratePO_Itemhistory th:nth-child(2)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(3), .ReGeneratePO_Itemhistory th:nth-child(3)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(4), .ReGeneratePO_Itemhistory th:nth-child(4)
        {
            min-width: 120px;
            max-width: 120px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(5), .ReGeneratePO_Itemhistory th:nth-child(5)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(6), .ReGeneratePO_Itemhistory th:nth-child(6)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .ReGeneratePO_Itemhistory td:nth-child(7), .ReGeneratePO_Itemhistory th:nth-child(7)
        {
            min-width: 100px;
            max-width: 100px;
        }       
        
        .body
        {
            background-color: #f4f4f4;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'RegeneratePO');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "RegeneratePO";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">

        function pageLoad() {
            try {
                fncAssignNetValuestoTextBox();
                $("#<%= txtPoOrderDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $("#<%= txtPODeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Vendor Name Change
        function fncVendorNameChange() {
            try {
                $('#<%=txtNewVendorCode.ClientID %>').val($('#<%=ddlNewVendorName.ClientID %>').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Calculate Net Values
        function fncAssignNetValuestoTextBox() {
            try {
                var rowObj, totalValue = 0, totalDiscAmt = 0, subtotal = 0, totalGSTAmt = 0, totalCess = 0;
                var nettotal = 0, qty;

                $("#tblPoReGenerate [id*=PoReGenerateBody]").each(function () {
                    rowObj = $(this);
                    qty = parseFloat(rowObj.find('td span[id*="lblREVISEQTY"]').text());
                    totalValue = parseFloat(qty) * (parseFloat(totalValue) + parseFloat(rowObj.find('td span[id*="lblUnitCost"]').text()));
                    totalDiscAmt = parseFloat(qty) * (parseFloat(totalDiscAmt) + parseFloat(rowObj.find('td span[id*="lblDiscAmount"]').text()));
                    subtotal = parseFloat(qty) * (parseFloat(subtotal) + parseFloat(rowObj.find('td span[id*="lblGrossCost"]').text()));
                    totalGSTAmt = parseFloat(rowObj.find('td span[id*="lblitaxamt1"]').text()) + parseFloat(rowObj.find('td span[id*="lblitaxamt2"]').text());
                    totalGSTAmt = parseFloat(qty) * parseFloat(totalGSTAmt);
                    totalCess = parseFloat(qty) * (parseFloat(totalCess) + parseFloat(rowObj.find('td span[id*="lblitaxamt4"]').text()));
                    nettotal = parseFloat(qty) * (parseFloat(nettotal) + parseFloat(rowObj.find('td span[id*="lblNetCost"]').text()));
                });

                $('#<%=txtTotal.ClientID %>').val(totalValue.toFixed(2));
                $('#<%=txtDiscAmt.ClientID %>').val(totalDiscAmt.toFixed(2));
                $('#<%=txtSubTotal.ClientID %>').val(subtotal.toFixed(2));
                $('#<%=txtVat.ClientID %>').val(totalGSTAmt.toFixed(2));
                $('#<%=txtCessAmt.ClientID %>').val(totalCess.toFixed(2));
                $('#<%=txtNetTotal.ClientID %>').val(nettotal.toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Save Validation
        function fncSaveValidation() {
            try {

                var poDeliverDate, dateStatus;
                poDeliverDate = $('#<%=txtPODeliveryDate.ClientID %>').val();
                poDeliverDate = poDeliverDate.split("/").reverse().join("-");

                if ($('#<%=txtNewVendorCode.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_ChooseNewvendor%>');
                    return false;
                }
                else if (new Date(poDeliverDate) < new Date(currentDatesqlformat_Master)) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_Deliverydate%>');
                    return;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Initialize VetDetail Dialog
        function fncInitializeVetDetailDialog() {
            try {
                fncCreateVatDetailTable();
                $("#divVatDetail").dialog({
                    resizable: false,
                    height: 250,
                    width: 657,
                    modal: true,
                    title: "Vet Detail",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        ///Create VetDetail Table
        function fncCreateVatDetailTable() {
            try {

                var vatDetailBody, vatRowNo = 0, taxperc, cessper, rowObj;
                var locTaxPerc, locCessper, qty, grosscost, taxableValue;
                var totalTaxtAmt1, totalTaxtAmt2, totalCessAmt, totalPurValue;
                var localtaxablePurValue, localtaxAmt1, localCessAmt, localtotalPurValue, status = "init";

                var ftotalTaxablePurValue = 0, ftotalTaxtAmt1 = 0, ftotalTaxtAmt2 = 0, ftotalCessAmt = 0, ftotalPurValue = 0;


                vatDetailBody = $("#tblVetDetail tbody");

                vatDetailBody.children().remove();

                $("#tblPoReGenerate [id*=PoReGenerateBody]").each(function () {
                    rowObj = $(this);
                    taxPerc = parseFloat(rowObj.find('td span[id*="lblitaxper1"]').text()) + parseFloat(rowObj.find('td span[id*="lblitaxper2"]').text());
                    cessper = parseFloat(rowObj.find('td span[id*="lblitaxper4"]').text()).toFixed(2);
                    qty = rowObj.find('td span[id*="lblLQty"]').text();
                    grosscost = rowObj.find('td span[id*="lblGrossCost"]').text();
                    totalTaxtAmt1 = (parseFloat(qty) * parseFloat(rowObj.find('td span[id*="lblitaxamt1"]').text())).toFixed(2);
                    totalTaxtAmt2 = (parseFloat(qty) * parseFloat(rowObj.find('td span[id*="lblitaxamt2"]').text())).toFixed(2);
                    totalCessAmt = (parseFloat(qty) * parseFloat(rowObj.find('td span[id*="lblitaxamt4"]').text())).toFixed(2);
                    totalPurValue = (parseFloat(qty) * parseFloat(rowObj.find('td span[id*="lblNetCost"]').text())).toFixed(2);
                    taxableValue = (parseFloat(qty) * parseFloat(grosscost)).toFixed(2);
                    taxPerc = taxPerc.toFixed(2);

                    vatDetailBody.children().each(function () {
                        status = "";
                        localtaxablePurValue = $(this).find('td[id*=tdTaxablePurValue]').text();
                        localTaxPerc = $(this).find('td[id*="tdTaxPerc"]').text();
                        localCessper = $(this).find('td[id*="tdCessPerc"]').text();
                        localtaxAmt1 = $(this).find('td[id*=tdTaxAmt1]').text();
                        localtaxAmt2 = $(this).find('td[id*=tdTaxAmt2]').text();
                        localCessAmt = $(this).find('td[id*=tdCessAmt]').text();
                        localtotalPurValue = $(this).find('td[id*=tdTotalvalue]').text();

                        if (taxPerc == localTaxPerc && cessper == localCessper) {

                            ftotalTaxablePurValue = parseFloat(ftotalTaxablePurValue) + (parseFloat(localtaxablePurValue) + parseFloat(taxableValue)).toFixed(2);
                            ftotalTaxtAmt1 = parseFloat(ftotalTaxtAmt1) + (parseFloat(localtaxAmt1) + parseFloat(totalTaxtAmt1)).toFixed(2);
                            ftotalTaxtAmt2 = parseFloat(ftotalTaxtAmt2) + (parseFloat(localtaxAmt2) + parseFloat(totalTaxtAmt2)).toFixed(2);
                            ftotalCessAmt = parseFloat(ftotalCessAmt) + (parseFloat(localCessAmt) + parseFloat(totalCessAmt)).toFixed(2);
                            ftotalPurValue = parseFloat(ftotalPurValue) + (parseFloat(localtotalPurValue) + parseFloat(totalPurValue)).toFixed(2);

                            $(this).find('td[id*=tdTaxablePurValue]').text((parseFloat(localtaxablePurValue) + parseFloat(taxableValue)).toFixed(2));
                            $(this).find('td[id*=tdTaxAmt1]').text((parseFloat(localtaxAmt1) + parseFloat(totalTaxtAmt1)).toFixed(2));
                            $(this).find('td[id*=tdTaxAmt2]').text((parseFloat(localtaxAmt2) + parseFloat(totalTaxtAmt2)).toFixed(2));
                            $(this).find('td[id*=tdCessAmt]').text((parseFloat(localCessAmt) + parseFloat(totalCessAmt)).toFixed(2));
                            $(this).find('td[id*=tdTotalvalue]').text((parseFloat(localtotalPurValue) + parseFloat(totalPurValue)).toFixed(2));
                        }
                        else {
                            status = "NotAvailable";
                        }
                    });

                    if (status == "NotAvailable" || status == "init") {

                        ftotalTaxablePurValue = parseFloat(ftotalTaxablePurValue) + parseFloat(taxableValue);
                        ftotalTaxtAmt1 = parseFloat(ftotalTaxtAmt1) + parseFloat(totalTaxtAmt1);
                        ftotalTaxtAmt2 = parseFloat(ftotalTaxtAmt2) + parseFloat(totalTaxtAmt2);
                        ftotalCessAmt = parseFloat(ftotalCessAmt) + parseFloat(totalCessAmt);
                        ftotalPurValue = parseFloat(ftotalPurValue) + parseFloat(totalPurValue);

                        vatRowNo = parseInt(vatRowNo) + 1;
                        vatDetailBody.append("<tr><td>" + vatRowNo
                                            + "</td><td id='tdTaxPerc_" + vatRowNo + "'>" + taxPerc
                                            + "</td><td id='tdCessPerc_" + vatRowNo + "'>" + cessper
                                            + "</td><td id='tdTaxablePurValue_" + vatRowNo + "'>" + taxableValue
                                            + "</td><td id='tdTaxAmt1_" + vatRowNo + "'>" + totalTaxtAmt1
                                            + "</td><td id='tdTaxAmt2_" + vatRowNo + "'>" + totalTaxtAmt2
                                            + "</td><td id='tdCessAmt_" + vatRowNo + "'>" + totalCessAmt
                                            + "</td><td id='tdTotalvalue_" + vatRowNo + "'>" + totalPurValue
                                            + "</td></tr>");
                    }

                });

                $("#tblVetDetail [id*=lblTotalTaxablePurValue]").text(ftotalTaxablePurValue.toFixed(2));
                $("#tblVetDetail [id*=lblTotalTaxAmt1]").text(ftotalTaxtAmt1.toFixed(2));
                $("#tblVetDetail [id*=lblTotalTaxAmt2]").text(ftotalTaxtAmt2.toFixed(2));
                $("#tblVetDetail [id*=lblTotalCessAmt]").text(ftotalCessAmt.toFixed(2));
                $("#tblVetDetail [id*=lblTotalPurValue]").text(ftotalPurValue.toFixed(2));

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///View Item History
        function fncViewItemHistory(source) {
            try {

                var rowObj, itemCode, itemHistorybody, itemObj;
                var obj = {};
                rowObj = $(source).parent().parent();
                itemCode = rowObj.find('td span[id*="lblItemcode"]').text();
                obj.Itemcode = itemCode;
                itemHistorybody = $("#tblItemhistory tbody");
                $.ajax({
                    type: "POST",
                    //url: "frmReGeneratePO.aspx/fncGetItemHistory",
                    url: '<%=ResolveUrl("frmReGeneratePO.aspx/fncGetItemHistory")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        itemHistorybody.children().remove();
                        var itemObj = jQuery.parseJSON(msg.d);
                        if (itemObj.length > 0) {
                            for (var i = 0; i < itemObj.length; i++) {
                                itemHistorybody.append("<tr><td>" + itemObj[i]["MNName"] + "</td><td>" + itemObj[i]["Week1"].toFixed(2) +
                                "</td><td>" + itemObj[i]["Week2"].toFixed(2) + "</td><td>" + itemObj[i]["Week3"].toFixed(2) +
                                "</td><td>" + itemObj[i]["Week4"].toFixed(2) + "</td><td>" + itemObj[i]["Week5"].toFixed(2) +
                                "</td><td>" + itemObj[i]["TotalQty"].toFixed(2) + "</td></tr>");
                            }
                            fncInitializeItemHistoryDialog();
                        }
                        else {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.lblNotSales%>');
                        }

                    },
                    error: function (data) {                        
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Initialize Item History Dialog
        function fncInitializeItemHistoryDialog() {
            try {
                $("#divItemhistory").dialog({
                    resizable: false,
                    height: 250,
                    width: 760,
                    modal: true,
                    title: "Item History",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Initialize Save Dialog
        function fncInitializeSaveDialog(SaveMsg) {
            try {
                if (SaveMsg == "Fail") {
                    $('#<%=hidStatus.ClientID %>').val(SaveMsg);
                    SaveMsg = '<%=Resources.LabelCaption.msg_POnotSave%>';
                }
                else {
                    SaveMsg = SaveMsg + '-' + '<%=Resources.LabelCaption.msg_Posave%>';
                    $('#<%=hidStatus.ClientID %>').val(SaveMsg);
                }

                $('#<%=lblPOSave.ClientID %>').text(SaveMsg);
                $("#ReGePOSave").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncCheckSaveStatus() {
            try {
                if ($('#<%=hidStatus.ClientID %>').val() == "Fail") {
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_ReGeneratePO%>
                </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full EnableScroll">
            <div class="ReGenerate_Po_No">
                <div>
                    <div class="ReGenerate_Po_lblWidth">
                        <asp:Label ID="lblPoNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_PONo %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txtWidth">
                        <asp:TextBox ID="txtPoNo" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="display_table">
                   <%-- <asp:LinkButton ID="lnkItemHistory" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemHistory %>'>
                    </asp:LinkButton>--%>
                    <asp:Label ID="Label1" style="margin-top:3px;" class="button-red" Font-Bold="true" runat="server" Text="<a href='../Purchase/frmPoStatus.aspx' style='color:white;'>BACK</a>"></asp:Label>                      
                </div>
            </div>
            <div class="ReGenerate_Po_No">
                <div>
                    <div class="ReGenerate_Po_lbl">
                        <asp:Label ID="lblOldVendorCode" runat="server" Text='<%$ Resources:LabelCaption,lbl_OldVendorCode %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt">
                        <asp:TextBox ID="txtOldVendorCode" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="display_table">
                    <div class="ReGenerate_Po_lbl">
                        <asp:Label ID="lblNewVendor" runat="server" Text='<%$ Resources:LabelCaption,lbl_NewVendor %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt">
                        <asp:TextBox ID="txtNewVendorCode" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="ReGenerate_Po_vendor_Name">
                <div>
                    <div class="ReGenerate_Po_lbl_VNWidth">
                        <asp:Label ID="lblOldVendorName" runat="server" Text='<%$ Resources:LabelCaption,lbl_OldVendorName %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt_VNWidth">
                        <asp:TextBox ID="txtOldVendorName" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="display_table">
                    <div class="ReGenerate_Po_lbl_VNWidth">
                        <asp:Label ID="lblNewVendorName" runat="server" Text='<%$ Resources:LabelCaption,lbl_NewVendorName %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt_VNWidth">
                        <asp:DropDownList ID="ddlNewVendorName" runat="server" Style="width: 100%" onchange="fncVendorNameChange()">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="ReGenerate_Po_No">
                <div>
                    <div class="ReGenerate_Po_lbl">
                        <asp:Label ID="lblPoOrderDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_PoOrder %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt">
                        <asp:TextBox ID="txtPoOrderDate" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="display_table">
                    <div class="ReGenerate_Po_lbl">
                        <asp:Label ID="lblPoDeliveryDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_PoDeliveryDate %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_Po_txt">
                        <asp:TextBox ID="txtPODeliveryDate" runat="server" CssClass="form-control-res" onkeypress="return false;"
                            onkeydown="return false;" oncut="return flase;" onpaste="return false;"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Payment_fixed_headers Generate_PO">
                <table id="tblPoReGenerate" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">
                                S.No
                            </th>
                            <th scope="col">
                                View
                            </th>
                            <th scope="col">
                                Inventorycode
                            </th>
                            <th scope="col">
                                Description
                            </th>
                            <th scope="col">
                                UOM
                            </th>
                            <th scope="col">
                                Order Qty
                            </th>
                            <th scope="col">
                                Received Qty
                            </th>
                            <th scope="col">
                                Revise Qty
                            </th>
                            <th scope="col">
                                Foc
                            </th>
                            <th scope="col">
                                Basic Cost
                            </th>
                            <th scope="col">
                                Disc Amt
                            </th>
                            <th scope="col">
                                Gross Cost
                            </th>
                            <th scope="col">
                                SGST Per
                            </th>
                            <th scope="col">
                                CGST Per
                            </th>
                            <th scope="col">
                                Cess Per
                            </th>
                            <th scope="col">
                                SGST Amt
                            </th>
                            <th scope="col">
                                CGST Amt
                            </th>
                            <th scope="col">
                                Cess Amt
                            </th>
                            <th scope="col">
                                NetCost
                            </th>
                            <th scope="col">
                                SellingPrice
                            </th>
                            <th scope="col">
                                MRP
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrReGeneratePo" runat="server">
                        <HeaderTemplate>
                            <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="PoReGenerateBody" runat="server">
                                <td>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                </td>
                                <td runat="server" id="tdItemhistory">
                                    <asp:Button ID="btnItemview" runat="server" Text='<%$ Resources:LabelCaption,btn_View%>'
                                        OnClientClick="fncViewItemHistory(this);return false;" />
                                </td>
                                <td>
                                    <asp:Label ID="lblItemcode" runat="server" Text='<%# Eval("Itemcode") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblItemDesc" runat="server" Text='<%# Eval("ItemDesc") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblLUomCode" runat="server" Text='<%# Eval("LUomCode") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblLQty" runat="server" Text='<%# Eval("LQty") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblLReceiptQty" runat="server" Text='<%# Eval("LReceiptQty") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblREVISEQTY" runat="server" Text='<%# Eval("REVISEQTY") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblFocQty" runat="server" Text='<%# Eval("FocQty") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblUnitCost" runat="server" Text='<%# Eval("UnitCost") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblDiscAmount" runat="server" Text='<%# Eval("DiscAmount") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblGrossCost" runat="server" Text='<%# Eval("GrossCost") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxper1" runat="server" Text='<%# Eval("itaxper1") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxper2" runat="server" Text='<%# Eval("itaxper2") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxper4" runat="server" Text='<%# Eval("itaxper4") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxamt1" runat="server" Text='<%# Eval("itaxamt1") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxamt2" runat="server" Text='<%# Eval("itaxamt2") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblitaxamt4" runat="server" Text='<%# Eval("itaxamt4") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblNetCost" runat="server" Text='<%# Eval("NetCost") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div>
                <div class="ReGenerate_PO_Netvalues1">
                    <div class="ReGenerate_PO_totalvalue">
                        <asp:Label ID="lblTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Totalvalue %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="ReGenerate_PO_Netvalues">
                    <div class="ReGenerate_PO_total">
                        <asp:Label ID="lblDiscAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_DiscAmt %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="ReGenerate_PO_Netvalues">
                    <div class="ReGenerate_PO_totalvalue">
                        <asp:Label ID="lblSubTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Subtotal %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="ReGenerate_PO_GST_Total">
                    <div class="ReGenerate_PO_GST">
                        <asp:LinkButton ID="lnkVat" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vat %>'
                            OnClientClick="fncInitializeVetDetailDialog();return false;"></asp:LinkButton>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtVat" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="ReGenerate_PO_Netvalues1">
                    <div class="ReGenerate_PO_total">
                        <asp:Label ID="lblCess" runat="server" Text='<%$ Resources:LabelCaption,lblCessAmt %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtCessAmt" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
                <div class="ReGenerate_PO_Netvalues1">
                    <div class="ReGenerate_PO_totalvalue">
                        <asp:Label ID="lblNetTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetTotal %>'></asp:Label>
                    </div>
                    <div class="ReGenerate_PO_NetValues_txt">
                        <asp:TextBox ID="txtNetTotal" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false" ></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="control-button Regenerate_PO_Save">
                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lnkSave %>'
                    OnClientClick="return fncSaveValidation()" OnClick="lnkSave_Click"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <div id="divVatDetail">
            <div class="Payment_fixed_headers gid_vetDetail">
                <table id="tblVetDetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">
                                S.No
                            </th>
                            <th scope="col">
                                GST (%)
                            </th>
                            <th scope="col">
                                CESS (%)
                            </th>
                            <th scope="col">
                                Taxable Pur.Value
                            </th>
                            <th scope="col">
                                SGST
                            </th>
                            <th scope="col">
                                CGST
                            </th>
                            <th scope="col">
                                CESS
                            </th>
                            <th scope="col">
                                Total Pur.Value
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr class="Paymentfooter_Color">
                            <td class="paymentfooter_total">
                                Total
                            </td>
                            <td class="paymentfooter_total">
                            </td>
                            <td class="paymentfooter_total">
                            </td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lblTotalTaxablePurValue" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lblTotalTaxAmt1" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lblTotalTaxAmt2" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lblTotalCessAmt" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_balancetotal">
                                <asp:Label ID="lblTotalPurValue" runat="server" Text="0.00" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div id="divItemhistory">
            <div class="Payment_fixed_headers ReGeneratePO_Itemhistory">
                <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">
                                Month
                            </th>
                            <th scope="col">
                                1st Week
                            </th>
                            <th scope="col">
                                2st Week
                            </th>
                            <th scope="col">
                                3st Week
                            </th>
                            <th scope="col">
                                4st Week
                            </th>
                            <th scope="col">
                                5st Week
                            </th>
                            <th scope="col">
                                Total
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
        <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div id="ReGePOSave">
            <div>
                <asp:Label ID="lblPOSave" runat="server"></asp:Label>
            </div>
            <div class="dialog_center">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                    OnClick="lnkbtnOk_Click" OnClientClick="return fncCheckSaveStatus();"> </asp:LinkButton>
            </div>
        </div>
        <asp:HiddenField ID="hidBillType" runat="server" />
        <asp:HiddenField ID="hidWarehouse" runat="server" />
        <asp:HiddenField ID="hidStatus" runat="server" />
    </div>
</asp:Content>
