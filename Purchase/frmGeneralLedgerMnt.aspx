﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGeneralLedgerMnt.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmGeneralLedgerMnt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .GL_rptr_textbox_width {
    text-align: center !important;
    border: none;
    background-color: aliceblue;
}
        .GLPaymentTable td:nth-child(1), .GLPaymentTable th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(2), .GLPaymentTable th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(3), .GLPaymentTable th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(4), .GLPaymentTable th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(5), .GLPaymentTable th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(6), .GLPaymentTable th:nth-child(6) {
            min-width: 360px;
            max-width: 360px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(7), .GLPaymentTable th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .GLPaymentTable td:nth-child(8), .GLPaymentTable th:nth-child(8) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GeneralLedgerMnt');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GeneralLedgerMnt";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
         $(document).ready(function () {
            <%--$("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");--%>
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        });
         </script>

    <script type="text/javascript">

        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            <%--var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            $("#<%= txtFromDate.ClientID %>").val(firstDay);--%>
            $('#<%=txtOutStandingTotal.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtDebit.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtCredit.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtGLCode.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtVendorName.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtCustomerName.ClientID%>').attr("readonly", "readonly");
            if ($('#<%=txtGLCode.ClientID%>').val() != '')
            {
                fncGetDataforLoadGL();
            }
            //fnccaldemo();
        }
        
        function fncGetDataforLoadGL()
        {
            var Credit = 0;
            var Debit = 0;
            try {
                var Rno = 0;
                var obj = {};
                $("tbody").children().remove();
                fncAppendRow(Rno);
                var SNo, TranNo, TranDate, RefNo, RefDate, Description;
                obj.GLCode = $('#<%=txtGLCode.ClientID %>').val();
                obj.FromDate = $('#<%=txtFromDate.ClientID %>').val();
                obj.ToDate = $('#<%=txtToDate.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: 'frmGeneralLedgerMnt.aspx/GetDataforLoadGLdata',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {

                        var objBindData = jQuery.parseJSON(data.d);

                        if (objBindData.Table.length > 0) {
                            tblbody = $("#tblGLpayment tbody");
                            for (var i = 0; i < objBindData.Table.length; i++) {
                                Rno = parseFloat(Rno) + parseFloat(1);
                                row = "<tr>" +
                            "<td>" + objBindData.Table[i]["SNo"] + "</td>" +
                            "<td>" + objBindData.Table[i]["TranNo"] + "</td>" +
                            "<td>" + objBindData.Table[i]["TranDate"] + "</td>" +
                            "<td>" + objBindData.Table[i]["RefNo"] + "</td>" +
                            "<td>" + objBindData.Table[i]["RefDate"] + "</td>" +
                            "<td>" + objBindData.Table[i]["Description"] + "</td>" +
                            "<td style='text-align: right !important'>" + objBindData.Table[i]["Debit"].toFixed(2) + "</td>" +
                            "<td style='text-align: right !important'>" + objBindData.Table[i]["Credit"].toFixed(2) + "</td>" +
                            "</tr>";
                                Debit = Debit + parseFloat(objBindData.Table[i]["Debit"]);
                                Credit = Credit + parseFloat(objBindData.Table[i]["Credit"]);
                                tblbody.append(row);
                            }
                            Debit = Debit + parseFloat($('#<%=hdDebit.ClientID %>').val());
                            Credit = Credit + parseFloat($('#<%=hdCredit.ClientID %>').val());
                            $('#<%=txtDebit.ClientID %>').val(parseFloat(Debit).toFixed(2));
                            $('#<%=txtCredit.ClientID %>').val(parseFloat(Credit).toFixed(2));
                            fncCalTotal();
                        }
                        else {
                             Debit = Debit + parseFloat($('#<%=hdDebit.ClientID %>').val());
                            Credit = Credit + parseFloat($('#<%=hdCredit.ClientID %>').val());
                            $('#<%=txtDebit.ClientID %>').val(parseFloat(Debit).toFixed(2));
                            $('#<%=txtCredit.ClientID %>').val(parseFloat(Credit).toFixed(2));
                            fncCalTotal();
                        }
                    }
                });
               
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fnccaldemo()
        {
            var TotalBottles = 0;
            var outstbal = 0;
            var Credit = 0, Debit = 0;
            try {
            $("#tblGLpayment [id*=tblGLpayment]").each(function () {
                var rowObj = $(this);
                Debit = parseFloat(Debit) + parseFloat(rowObj.find('td input[id*="txtDebit"]').val());
                Credit = parseFloat(Credit) + parseFloat(rowObj.find('td input[id*="txtCredit"]').val());
                
                
            });
            outstbal = parseFloat(Credit) - parseFloat(Debit);
            //alert(outstbal);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAppendRow(Rno)
        {
            try {
                var tblbody, row;
                tblbody = $("#tblGLpayment tbody");

                row = "<tr>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td></td>" +
                            "<td>" + "Opening Balance :" + "</td>" +
                            "<td style='text-align: right !important'>" + $('#<%=hdDebit.ClientID %>').val() + "</td>" +
                            "<td style='text-align: right !important'>" + $('#<%=hdCredit.ClientID %>').val() + "</td>" +
                            "</tr>";
                tblbody.append(row);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncCalTotal()
        {
            var outstbal = 0;
            var Credit = 0, Debit = 0;
            
            try {
                Credit = parseFloat($('#<%=txtCredit.ClientID %>').val());
                Debit = parseFloat($('#<%=txtDebit.ClientID %>').val());
                //If Val(txtDebit) < Val(txtCredit) Then
                //lblOutstanding.Caption = "TOTAL PAYABLE IS "
                //ElseIf Val(txtDebit) > Val(txtCredit) Then
                //lblOutstanding.Caption = "TOTAL RECEIVABLE IS "
                //ElseIf Val(txtDebit) = Val(txtCredit) Then
                //lblOutstanding.Caption = "NO OUTSTANDING"
                //End If
                if (Debit < Credit)
                {
                    $('#<%=lblTotalAmount.ClientID %>').text('TOTAL PAYABLE IS');
                }
                else if (Debit < Credit)
                {
                    $('#<%=lblTotalAmount.ClientID %>').text('TOTAL RECEIVABLE IS');
                }
                else if (Debit == Credit)
                {
                    $('#<%=lblTotalAmount.ClientID %>').text('NO OUTSTANDING');
                }
                outstbal = parseFloat(Credit) - parseFloat(Debit);
                
                $('#<%=txtOutStandingTotal.ClientID %>').val(parseFloat(outstbal).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveValidation() {
            try {
                var OutStandingTotal, Amount, Msg;
                OutStandingTotal = parseFloat($('#<%=txtOutStandingTotal.ClientID %>').val());
                Amount = $('#<%=txtAmount.ClientID %>').val();
                //alert(OutStandingTotal)
                var status = true;
                if ($('#<%=txtPayMode.ClientID %>').val() == '') {
                        ShowPopupMessageBox('Please Choose valid Paymode');
                        $('#<%=txtPayMode.ClientID %>').focus();
                        status = false;
                }
                else if ($('#<%=txtGLPaymentType.ClientID %>').val() == '') {
                        ShowPopupMessageBox('Please Choose valid Payment type');
                        $('#<%=txtGLPaymentType.ClientID %>').focus();
                        status = false;
                    }
                else if ($('#<%=txtAmount.ClientID %>').val() == '0.00') {
                    ShowPopupMessageBox('Please enter the valid amount');
                    $('#<%=txtAmount.ClientID %>').focus();
                        status = false;

                    }
                    
                else if (parseFloat(OutStandingTotal) > 0) {
                        if ($('#<%=txtGLPaymentType.ClientID %>').val() == 'PAYMENT' && (parseFloat(OutStandingTotal) < parseFloat(Amount))) {
                            Msg = 'Payment amount is higher than actual outstanding. Do you want to continue.?';
                            fncInitializeSaveValidationDialog(Msg);
                            status = false;
                        }
                        
                    }
                        else if (parseFloat(OutStandingTotal) < 0) {
                            if ($('#<%=txtGLPaymentType.ClientID %>').val() == 'RECEIPT' && (parseFloat(OutStandingTotal) < parseFloat(Amount)))
                            {
                                Msg = 'Receipt amount is higher than actual outstanding. Do you want to continue.?';
                                fncInitializeSaveValidationDialog(Msg);
                                status = false;
                            }
                    
                }
    return status;
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
        //Initialize Save Dialog
function fncInitializeSaveDialog() {
    try {
        var SaveMsg = ' Updated Successfully';
        $('#<%=lblGLTransactionSave.ClientID %>').text(SaveMsg);
                $("#GLTransactionSave").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Save Dialog
        function fncSaveDialogClose() {
            $("#GLTransactionSave").dialog('close');
            fncSubCrear();
        }
        function fncSubCrear()
        {
            try {
                $('#<%=txtPayMode.ClientID %>').val('')
                $('#<%=txtGLPaymentType.ClientID %>').val('')
                $('#<%=txtRefNo.ClientID %>').val('')
                $('#<%=txtDescription.ClientID %>').val('')
                $('#<%=txtAmount.ClientID %>').val('0.00')
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Initialize Save Validation Dialog 
        function fncInitializeSaveValidationDialog(Msg) {
    try {
        $('#<%=lblGLTransactionSaveValidation.ClientID %>').text(Msg);
        $("#GLTransactionSaveValidation").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Save Dialog
        function fncInitializeSaveValidationDialogClose() {
            $("#GLTransactionSaveValidation").dialog('close');
            fncSubCrear();
        }
        function fncSaveDialogCancel()
        {
            $("#GLTransactionSaveValidation").dialog('close');
        }
        function fncBacktoPreviousform() {
            try {
                window.location = "frmGeneralLedger.aspx";
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function numericOnly(elementRef) {

            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {

                return true;

            }

                // '.' decimal point...  

            else if (keyCodeEntered == 46) {

                // Allow only 1 decimal point ('.')...  

                if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))

                    return false;

                else

                    return true;

            }

            return false;

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">General Ledger Payment</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="control-group-split" style="width: 35%; float: left;">
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 60%;">
                            <div class="label-left">
                                <asp:Label ID="Label26" runat="server" Text="GL Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtGLCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 60%;">
                            <div class="label-left">
                                <asp:Label ID="Label32" runat="server" Text="Vendor Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendorName" Style="width: 276%;" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 60%;">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Customer Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCustomerName" Style="width: 276%;" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group-split" style="width: 50%; float: right">
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 35%;">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 35%;">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-middle" style="width: 35%;">
                            <div class="control-button" style="float: right">
                                <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" Text="Fetch"></asp:LinkButton>
                                <%--OnClick="lnkFetch_Click"--%>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdDebit" runat="server" />
    <asp:HiddenField ID="hdCredit" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottom-purchase-container">
            <div class="panel panel-default">
                <div>

                    <div id="divGLPaymentTable" class="GLPaymentTable Payment_fixed_headers">
                        <table rules="all" border="1" id="tblGLpayment">
                            <thead>
                                <tr>
                                    <th style="text-align: center">S.No</th>
                                    <th style="text-align: center">Tran No</th>
                                    <th style="text-align: center">Tran Date</th>
                                    <th style="text-align: center">Reference No</th>
                                    <th style="text-align: center">Reference Date</th>
                                    <th style="text-align: center">Description</th>
                                    <th style="text-align: center">Debit</th>
                                    <th style="text-align: center">Credit</th>
                                </tr>
                            </thead>
                            <tbody id="divtblGLpayment">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="control-group-split" style="border: 1px solid;">
            <div class="control-group-split" style="float: left; width: 50%;">
                <div class="control-group-middle" style="width:45% !important">
                    <div class="label-left">
                        <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" style="font-weight:bolder"></asp:Label>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtOutStandingTotal" runat="server" style="text-align: right !important;" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="control-group-split" style="float: right; width: 30%;">
                <div class="control-group-middle" style="width:45% !important">
                    <div class="label-left">
                        <asp:Label ID="Label3" runat="server" Text="Total Debit" style="font-weight:bolder"></asp:Label>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtDebit" runat="server" style="text-align: right !important;" Text="0.00" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-middle" style="width:45% !important">
                    <div class="label-left">
                        <asp:Label ID="Label4" runat="server" Text="Total Credit" style="font-weight:bolder"></asp:Label>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtCredit" runat="server" style="text-align: right !important;" Text="0.00" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>

        </div>

        <div id="divItemadd">
            <asp:Panel runat="server" ID="pnlItemadd">
                <asp:UpdatePanel ID="updtPnlButton1" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="bottom-purchase-container-add gid_top_border" id="addInv">
                            <div class="bottom-purchase-container-add-Text">
                                <div class="control-group-split">
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text="PayMode"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPayMode" onkeydown="return fncShowSearchDialogCommon(event, 'Paymode',  'txtPayMode', 'txtGLPaymentType');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label18" runat="server" Text="Payment Type"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtGLPaymentType" onkeydown="return fncShowSearchDialogCommon(event, 'GLPaymentType',  'txtGLPaymentType', 'txtRefNo');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7" style="width:14%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Ref.No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtRefNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            <%--<asp:HiddenField ID="hfInvCode" runat="server" />--%>
                                        </div>
                                    </div>
                                    <div class="PO_container3" style="width:44%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Description"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    <div class="container2" style="width: 10%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text="Amount"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAmount" runat="server" Text="0.00" CssClass="form-control-res-right" onkeypress="return numericOnly(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_Click" OnClientClick="return fncSaveValidation()"
                                                Text="Save"></asp:LinkButton><%--OnClick="lnkSave_Click"--%>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClear" runat="server" Text="Clear" class="button-blue" OnClientClick="return fncSubCrear()"></asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkBack" runat="server" Text="Back" class="button-blue" OnClientClick="return fncBacktoPreviousform()"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </div>
    <div class="hiddencol">
    
    <div id="GLTransactionSave">
                <div>
                    <asp:Label ID="lblGLTransactionSave" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:UpdatePanel ID="upsave" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                OnClientClick="fncSaveDialogClose()" > </asp:LinkButton><%--OnClick="lnkbtnOk_Click"--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        <div id="GLTransactionSaveValidation">
                <div>
                    <asp:Label ID="lblGLTransactionSaveValidation" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:UpdatePanel ID="upSaveValidation" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkbtnGLTranOK" runat="server" class="button-blue" Text="OK"
                                OnClientClick="fncInitializeSaveValidationDialogClose()" > </asp:LinkButton><%--OnClick="lnkbtnOk_Click"--%>
                            <asp:LinkButton ID="lnkbtnCancel" runat="server" class="button-blue" Text="Cancel"
                                OnClientClick="fncSaveDialogCancel();return false" > </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    
</asp:Content>
