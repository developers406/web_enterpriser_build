﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmDisplayandContracts.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmDisplayandContracts" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'DisplayandContracts');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "DisplayandContracts";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript" language="Javascript">
        var todatectrl, fromdatectrl;
        $(document).ready(function () {
            $("#<%= txtPaymentDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtReferenceDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtChequeDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");

        });
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                <%--$('#<%=lnkUpdate.ClientID %>').css("display", "block");--%>
            }
            else {
                $('#<%=lnkUpdate.ClientID %>').css("display", "none");
            }
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            }
            else {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            }
        }
        function isNumberKeyAmtToPay(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                debugger;
                //alert($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');

                var Amt = tr.find('span[id *= txtAmount]').text();

                var AmtToPay = tr.find('input[id *= txtAmountToPay]').val();

                var PayStatus = tr.find('input[id *= lblVendorPayStatus]').val();

                if (parseFloat(AmtToPay) > parseFloat(Amt)) {
                    alert("Invalid Amount");
                    tr.find('input[id *= chkPaySingle]').prop('checked', false);
                } else {
                    tr.find('input[id *= chkPaySingle]').prop('checked', true);
                    tr.find('input[id *= lblBalance]').val(parseFloat(Amt) - parseFloat(AmtToPay));
                    var Bal = tr.find('input[id *= lblBalance]').val();
                    tr.next().find('input[id *= txtAmountToPay]').focus();
                    var PaytotalStatic = $('#<%=txtPayAmountStatic.ClientID %>').val();
                    if (Bal == '' || Bal == 0) {
                        tr.find('input[id *= lblVendorPayStatus]').val('Full');
                    } else {
                        //$('#<%=txtPayAmount.ClientID %>').val((parseFloat(PaytotalStatic)) - parseFloat(Bal));
                        tr.find('input[id *= lblVendorPayStatus]').val('Partial');
                    }

                }
                checkTotalAmount();

                return false;
            }


            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function checkTotalAmount() {
            var TotSum = 0;
            $("#<%=gvPayment.ClientID %> tr").each(function () {

                if ($(this).find('input[id *= txtAmountToPay]').length > 0) {
                    var AmtToPay = parseFloat($(this).find('input[id *= txtAmountToPay]').val());
                    //alert(AmtToPay);
                    TotSum = parseFloat(TotSum) + parseFloat(AmtToPay);
                }

            });
            $("#<%=txtPayAmount.ClientID %>").val(TotSum);
        }
        function Pay_Button_Click() {
            $("#<%=txtRefVoucherNo.ClientID %>").val('');
            $("#<%=txtRemarks.ClientID %>").val('');
            InitializeDialogPay_Button();
            $("#dialog-PaymentsFromVendor").dialog("open");
            $("#<%=txtRefVoucherNo.ClientID %>").focus();
        }
        function InitializeDialogPay_Button() {
            try {
                $("#dialog-PaymentsFromVendor").dialog({
                    appendTo: 'form:first',
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 850,
                    modal: true,

                    buttons: {
                        //                        "Select All": function () {

                        //                        fncValidatePaymentCheckAll();return false;
                        //                         //__doPostBack('ctl00$ContentPlaceHolder1$lnkSelectAllChang', '');
                        //                            //$(this).dialog("close");

                        //                        },

                        //                        "Save": function () {

                        //                            //                            if ($('#<%=txtRefVoucherNo.ClientID %>').val() == "") {
                        //                            //                                alert('Enter Voucher No')
                        //                            //                                return false;
                        //                            //                            }


                        //                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSaveChangeMem', '');

                        //                            $(this).dialog("close");

                        //                        },
                        //                        Cancel: function () {
                        //                            $(this).dialog("close");
                        //                        }
                    }
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function EnableOrDisableDropdown(element, isEnable) {
            //alert(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        $(function () {


            BankCode = $('#<%=ddlBankCode.ClientID %>');
            Paymode = $('#<%=ddlPaymode.ClientID %>');

            BankCode.on("change", function () {
                debugger;
                //alert(Deparmentctrl[0].selectedIndex)
                if (BankCode[0].selectedIndex == 0) {
                    $('#<%=txtBankName.ClientID %>').val('');
                } else {
                    $('#<%=txtBankName.ClientID %>').val($('#<%=ddlBankCode.ClientID %>').val());

                }

            });
            Paymode.on("change", function () {
                debugger;
                //alert(Deparmentctrl[0].selectedIndex)
                if (Paymode[0].selectedIndex == 0) {

                    $('#<%=txtBankName.ClientID %>').val('');
                    $('#<%=txtChequeNo.ClientID %>').val('');
                    EnableOrDisableDropdown(BankCode, false);

                } else if (Paymode[0].selectedIndex == 1) {

                    $('#<%=txtBankName.ClientID %>').val('CASH ACCOUNT');
                    $('#<%=txtChequeNo.ClientID %>').val('CASH');
                    EnableOrDisableDropdown(BankCode, true);
                } else {
                    EnableOrDisableDropdown(BankCode, false);
                    $('#<%=txtBankName.ClientID %>').val('');
                    $('#<%=txtChequeNo.ClientID %>').val('');
                }

            });
            //console.log(fromdatectrl.parent());
            //console.log(todatectrl.parent());
            //alert(fromdatectrl);
        });

        function fncValidatePaymentCheckAll() {
            try {
                var gridtr = $("#<%= gvPayment.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
            if (gridtr.length == '0') {
                //AlertNoItems
                fncShowAlertNoItemsMessage();
            }
            else {

                if ($("#<%=lnkSelectAllChang.ClientID %>").text() == "Select All") {

                    $("#<%=lnkSelectAllChang.ClientID %>").text("De-Select All");
                    $("#<%=gvPayment.ClientID %> [id*=chkPaySingle]").prop('checked', true);


                }
                else {

                    $("#<%=lnkSelectAllChang.ClientID %>").text("Select All");
                    $("#<%=gvPayment.ClientID %> [id*=chkPaySingle]").prop('checked', false);


                    }

                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function fncValidateCheckAll() {

            try {
                var gridtr = $("#<%= gvDisplay.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvDisplay.ClientID %> [id*=chkSingle]").prop('checked', true);


                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvDisplay.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncValidateSave() {
            try {
                checkTotalAmount();
                BankCode = $('#<%=ddlBankCode.ClientID %>');
                Paymode = $('#<%=ddlPaymode.ClientID %>');
                ChequeNo = $('#<%=txtChequeNo.ClientID %>');
                debugger;
                if ($("#<%=txtRefVoucherNo.ClientID %>").val() == "") {
                    alert("Please Enter Referance Voucher Number");
                }
                <%--else if (Paymode.val() == "--Select--") {
                    alert("Please Select Paymode");
                }
                else if ((Paymode.val() == "CHEQUE") || (Paymode.val() == "VOUCHER")) {

                    if ($("#<%=txtBankName.ClientID %>").val() == "") {
                        alert("Please Select BankCode");
                    } else if (ChequeNo.val() == "") {
                        alert("Please Select Cheque Number");
                    } else {
                        PaymentGridValidation();
                    }

                }--%>
                else {
                    PaymentGridValidation();

                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function PaymentGridValidation() {

            var gridtr = $("#<%= gvPayment.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
    if (gridtr.length == '0') {
        //AlertNoItems
        fncShowAlertNoItemsMessage();
    }
    else {
        debugger;
        //alert($("#<%=gvPayment.ClientID %> [id*=lnkSelectAllChang]").is(":checked"));
        if ($("#<%=gvPayment.ClientID %> [id*=chkPaySingle]").is(":checked") == '1') {

                    fncShowConfirmSaveMessage();
                } else {
                    fncShowMessage();
                }
            }
        }
        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvDisplay.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvDisplay.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvDisplay.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete1() {
            try {
                // alert("sdfg");
                $("#DisplayDelete").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //alert("message");
                InitializeDialogDelete1();
                $("#DisplayDelete").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncCloseDeleteDialog() {
            try {
                $("#DisplayDelete").dialog('close');


            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdateSelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdateSelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdateSelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteConfirm").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteConfirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirm").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirm").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }


        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
        }
        function fnfrmtoDate() {
            try {
                $("#<%=hidFromdate.ClientID %>").val($("#<%= txtFromDate.ClientID %>").val());
                $("#<%=hidTodate.ClientID %>").val($("#<%= txtToDate.ClientID %>").val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_DisplayAndContracts%></li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="EnableScroll">
            <div class="container-group-price">
                <div class="container-left-price" id="pnlFilter" runat="server">
                    <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                        EnableVendorDropDown="true"
                        EnableDisplayCodeDropDown="true" />
                    <div class="control-group-split" style="width: 100%; float: left">
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left" style="width: 39%">
                                <asp:Label ID="Label11" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right" style="width: 60%; margin-left: 1%">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left" style="width: 39%">
                                <asp:Label ID="Label13" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right" style="width: 60%; margin-left: 1%">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 button-contol">

                        <div class="control-button" style="float: right">
                            <div class="col-md-6">
                                <div class="label-left">
                                    <asp:LinkButton ID="lnkLoad" runat="server" class="button-red" OnClientClick="fnfrmtoDate();" OnClick="lnkLoadFilter_Click"><i class="icon-play"></i>Filter</asp:LinkButton>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="label-right">
                                    <asp:LinkButton ID="LinkButton7" runat="server" class="button-red" OnClientClick="clearForm() return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                style="height: 500px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="gvDisplay" />
                    </Triggers>
                    <ContentTemplate>
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 490px">
                            <asp:GridView ID="gvDisplay" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                OnRowCommand="gvDisplay_OnRowCommand" CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" Visible='<%# (Eval("Pay").ToString() == "Pay") %>'
                                                runat="server" ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit"
                                                CommandName="EditRow" CommandArgument='<%# Eval("DisplayNo").ToString() %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DisplayNo" HeaderText="Display No"></asp:BoundField>
                                    <asp:BoundField DataField="DisplayCode" HeaderText="Display Code"></asp:BoundField>
                                    <asp:BoundField DataField="FromDate" HeaderText="From Date" />
                                    <asp:BoundField DataField="ToDate" HeaderText="To Date"></asp:BoundField>
                                    <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code"></asp:BoundField>
                                    <asp:BoundField DataField="AmountToPay" HeaderText="AmtToPay" />
                                    <asp:BoundField DataField="Paidamount" HeaderText="Paid Amount" />
                                    <asp:TemplateField HeaderText="Payment Status" ItemStyle-Width="75">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayStatus" Font-Bold="true" Visible='<%# (Eval("Pay").ToString() == "Paid") %>'
                                                runat="server" Text='<%# Eval("Pay") %>'></asp:Label>
                                            <asp:Button ID="btnPay" CommandName="sbtnPay" CommandArgument='<%# Eval("VendorCode").ToString() %>'
                                                Visible='<%# (Eval("Pay").ToString() == "Pay") %>' Text='<%# Eval("Pay") %>'
                                                runat="server" Font-Bold="true" ForeColor="SeaGreen" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="Pay" ItemStyle-Width="65">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtPay" runat="server" onkeypress="return isNumberKey(event)" CssClass="grid-textbox"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="float: right; display: table">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                            Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkAdd_Click"
                            Text='<%$ Resources:LabelCaption,btnAdd %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                            Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                            Text="Delete"></asp:LinkButton>
                    </div>
                    <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
                </div>
            </div>
        </div>
        <div id="dialog-PaymentsFromVendor" style="display: none" title="Payment From Vendor">
            <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                <ContentTemplate>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label22" runat="server" Text="Payment No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPaymentNo" onMouseDown="return false" Enable="false" runat="server"
                                    CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendorCode" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label23" runat="server" Text="Payment Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Vendor Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendorName" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="* Ref Voucher No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRefVoucherNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Remarks"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRemarks" runat="server"   CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Ref Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtReferenceDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="control-group-split display_none">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Paymode"></asp:Label>
                            </div>
                            <div class="label-right" style="width: 40%">
                                <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="--Select--"></asp:ListItem>
                                    <asp:ListItem Value="CASH"></asp:ListItem>
                                    <asp:ListItem Value="VOUCHER"></asp:ListItem>
                                    <asp:ListItem Value="CHEQUE"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="BankCode"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlBankCode" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split display_none">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Bank Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBankName" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="ChequeNo"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split display_none">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="txtCheque" runat="server" Text="Cheque Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Amount"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPayAmount" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                <asp:TextBox ID="txtPayAmountStatic" runat="server" Visible="false" onMouseDown="return false"
                                    CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="gridDetails grid-overflow" id="Div1" runat="server" style="height: 200px">
                        <asp:GridView ID="gvPayment" runat="server" Width="100%" AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                            OnRowCommand="gvDisplay_OnRowCommand" CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkPaySingle" runat="server" Width="40px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DisplayNo" HeaderText="Display No"></asp:BoundField>
                                <asp:BoundField DataField="DisplayCode" HeaderText="Display Code"></asp:BoundField>
                                <asp:BoundField DataField="RefNo" HeaderText="Ref No" />
                                <%--<asp:BoundField DataField="AmountToPay" HeaderText="Amount"></asp:BoundField>--%>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label18" runat="server" Text="Amount"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="txtAmount" Width="100px" runat="server" Text='<%# Eval("AmountToPay") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="AmountToPay" HeaderText="AmtToPay" />--%>
                                <asp:TemplateField HeaderText="AmountToPay" ItemStyle-Width="65">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtAmountToPay" runat="server" Text='<%# Eval("AmountToPay") %>'
                                            onkeypress="return isNumberKeyAmtToPay(event)" CssClass="grid-textbox"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Balance" ItemStyle-Width="75">
                                    <ItemTemplate>
                                        <asp:TextBox ID="lblBalance" Font-Bold="true" Style="width: 100%" CssClass="grid-textbox" onMouseDown="return false" runat="server" Text='<%# Eval("Balance") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--  <asp:BoundField DataField="Balance" HeaderText="Balance" />--%>
                                <asp:TemplateField HeaderText="Payment Status" ItemStyle-Width="75">
                                    <ItemTemplate>
                                        <asp:TextBox ID="lblVendorPayStatus" Font-Bold="true" Style="width: 100%" CssClass="grid-textbox" onMouseDown="return false" runat="server" Text='<%# Eval("Pay") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div style="float: right; display: table">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSelectAllChang" runat="server" class="button-blue" OnClientClick="fncValidatePaymentCheckAll();return false;"
                                Text="Select All"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="InkSave" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'
                                OnClientClick="fncValidateSave();return false;" OnClick="lnkConfirmSavebtnOk_Click"
                                Width="100px"></asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="container-group-full" style="display: none">
            <div id="StockUpdateSelectAny">
                <p>
                    <%=Resources.LabelCaption.Alert_Select_Any%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="DeleteConfirm">
                <p>
                    <%=Resources.LabelCaption.Alert_DeleteSure%>
                </p>
                <div style="width: 150px; margin: auto">
                    <div style="float: left">
                        <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                            Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                    </div>
                    <div style="float: right">
                        <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="ConfirmSaveDialog">
                <p>
                    <%=Resources.LabelCaption.Alert_Confirm_Save%>
                </p>
                <div style="width: 150px; margin: auto">
                    <div style="float: left">
                        <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="CloseConfirmSave(); return true;"
                            Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                    </div>
                    <div style="float: right">
                        <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="CloseConfirmSave(); return false; "
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="DisplayDelete">
                <p>
                    <%=Resources.LabelCaption.Alert_Delete%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="StockUpdateSave">
                <p>
                    <%=Resources.LabelCaption.Alert_Save%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="AlertNoItems">
                <p>
                    <%=Resources.LabelCaption.Alert_No_Items%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full">
            <asp:HiddenField ID="hidFromdate" runat="server" Value="" />
            <asp:HiddenField ID="hidTodate" runat="server" Value="" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>
    </div>
</asp:Content>
