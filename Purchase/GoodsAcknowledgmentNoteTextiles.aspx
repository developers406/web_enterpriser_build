﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="GoodsAcknowledgmentNoteTextiles.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.GoodsAcknowledgmentNoteTextiles" %>

<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <link type="text/css" href="../css/grn-styles.css" rel="stylesheet" />
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'GoodsAcknowledgementNoteTextiles');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "GoodsAcknowledgementNoteTextiles";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">

        //Header Row Click
        function fncrptrHeaderclick(source) {
            try {
                //cbSearchheader
                //cbrow
                if (($(source).is(":checked"))) {
                    $("#tblItemDetail [id*=cbrow]").attr("checked", "checked");
                }
                else {
                    $("#tblItemDetail [id*=cbrow]").removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncNetCostKeyPress(event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {

                if (keyCode == 13) {

                    if ($('#<%=txtNetCostT.ClientID%>').val() > 0) {
                        fncItemForMarginGrid();
                        $('#<%=txtMarginT.ClientID %>').focus().select();
                    }

                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncTextBoxCalc(mode) {
            var grossAmt = "0";
            if (mode == 'Discount') {
                var discAmt = $('#<%=txtCostPriceT.ClientID %>').val() * $('#<%=txtDiscountPercT.ClientID %>').val() / 100;
                $('#<%=txtDiscountAmtT.ClientID %>').val(discAmt.toFixed(2));
                grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));
                $('#<%=txtGrossCostT.ClientID %>').val(grossAmt);
            }
            else if (mode == 'DiscountAmt') {
                totaldiscount = $('#<%=txtCostPriceT.ClientID %>').val() - $('#<%=txtDiscountAmtT.ClientID %>').val();
                discountpercent = ($('#<%=txtDiscountAmtT.ClientID %>').val() * 100) / parseFloat($('#<%=txtCostPriceT.ClientID %>').val());
                $('#<%=txtDiscountPercT.ClientID %>').val(discountpercent.toFixed(2));
                grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));
                $('#<%=txtGrossCostT.ClientID %>').val(grossAmt);
            }
            else if (mode == 'SD') {
                var SDamt = $('#<%=txtCostPriceT.ClientID %>').val() * $('#<%=txtSDPercT.ClientID %>').val() / 100;
                $('#<%=txtSDAmtT.ClientID %>').val(SDamt.toFixed(2));
                var grosscost = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - parseFloat($('#<%=txtSDAmtT.ClientID %>').val()) + parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val());
                $('#<%=txtGrossCostT.ClientID %>').val(grosscost);
                grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));
                $('#<%=txtGrossCostT.ClientID %>').val(grossAmt);
            }
            else if (mode == 'OTax') {
                var OTaxAmt = $('#<%=txtGrossCostT.ClientID %>').val() * $('#<%=txtOTaxPerc.ClientID %>').val() / 100;
                $('#<%=txtOTaxAmt.ClientID %>').val(OTaxAmt.toFixed(2));
                var dSubTotal = parseFloat($('#<%=txtGrossCostT.ClientID %>').val()) + parseFloat($('#<%=txtOTaxAmt.ClientID %>').val());
                $('#<%=hidSubTotal.ClientID %>').val(dSubTotal.toFixed(2));
                grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));
                $('#<%=txtGrossCostT.ClientID %>').val(grossAmt);
            }
            else if (mode == 'Gross') {
                grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));
                $('#<%=txtGrossCostT.ClientID %>').val(grossAmt);
            }
            else if (mode == 'GST') {
                var gstAmt = parseFloat($('#<%=hidSubTotal.ClientID %>').val()) * $('#<%=txtGSTPercT.ClientID %>').val() / 100;
                $('#<%=txtGSTAmtT.ClientID %>').val(gstAmt.toFixed(4));
                var netCost = parseFloat($('#<%=hidSubTotal.ClientID %>').val()) + parseFloat(gstAmt);
                $('#<%=txtNetCostT.ClientID %>').val(netCost.toFixed(4));

                var sgstper = $('#<%=txtGSTPercT.ClientID %>').val() / 2;
                fncTaxAmtCalculation($('#<%=hidSubTotal.ClientID %>').val(), sgstper, sgstper);
            }
            else if (mode == 'Margin') {
                var MRP = (parseFloat($('#<%=txtNetCostT.ClientID %>').val()) * parseFloat($('#<%=txtMarginT.ClientID %>').val()) / 100) + parseFloat($('#<%=txtNetCostT.ClientID %>').val());
                $('#<%=txtMrpT.ClientID %>').val(MRP);
                $('#<%=txtSellingPriceT.ClientID %>').val(MRP);
                $('#<%=txtMarginSP.ClientID %>').val(0);
            }
            else if (mode == 'MarginSP') {
                var MarginSP = parseFloat($('#<%=txtMarginSP.ClientID %>').val()); 
                if (MarginSP > 100) {
                    //ShowPopupMessageBox("Percentage must be lessthan 100");
                    $('#<%=txtMarginSP.ClientID %>').focus();
                }
                else {
                    var SP = $('#<%=txtMrpT.ClientID %>').val() - ($('#<%=txtMrpT.ClientID %>').val() * parseFloat($('#<%=txtMarginSP.ClientID %>').val()) / 100);
                    $('#<%=txtSellingPriceT.ClientID %>').val(SP);
                }
            }

}

var glSRTotalInvQty, glSRTotalBasicAmt, glSRTotalGrossAmt, glSRTotalVatAmt, glSRTotalCessAmt, glSRTotalDiscAmt, glSRTotalShDiscAmt;
var vatRowNo = 0, glSRtaxAmt1, glSRtaxAmt2, glSRCessAmt, glSRTotalvalue, glMissValueStatus = 0, glwcpforcost = 0;
var excessSupplyStatus = "", shortSupplyStatus = "", shortSupplyAmt = 0, excessSupplyAmt = 0;
var MBatchStatus = "", vatDetailtbody;//,commanKey="";
var maxItemCountNo = 0;
var ItemcodeColorch = "";
var MrpColorch = "";
var SNoColorch = "";
var StyleColorch = "";

var changeRow = [];
///Page Load
function pageLoad() {
    try {

        //$("select").show().removeClass('chzn-done');
        //$("select").next().remove();
        if ($("#<%=hidSagarSilks.ClientID %>").val() == "Y") {
            $('#divSpDis').css('display','block');
        } 
        if (focusflag == 0) {
            var fooElementverify = document.getElementById("grdVerifydiv");
            var fooElemententyr = document.getElementById("grdEntrydiv");
            var fooElementcost = document.getElementById("grdCostdiv");
            var fooElementstock = document.getElementById("grdStockdiv");
            fooElementverify.style.backgroundColor = "#1e82c5";
            fooElemententyr.style.backgroundColor = "red";
            fooElementcost.style.backgroundColor = "#1e82c5";
            fooElementstock.style.backgroundColor = "#1e82c5";
        }
        if ($("#<%=hidbillType.ClientID %>").val() == "A") {
            $('#breadcrumbs_text_GRN').removeClass('lowercase');
            $('#breadcrumbs_text_GRN').addClass('uppercase');
        }
        else {
            $('#breadcrumbs_text_GRN').removeClass('uppercase');
            $('#breadcrumbs_text_GRN').addClass('lowercase');
        }

        pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
        expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");

        $("#<%=txtfocFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        $("#<%=txtFocToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "1");
        $("#<%=txtExpireDate.ClientID %>").datepicker({ dateFormat: "mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "1");
        $('#<%=hidRepeaterValueStatus.ClientID%>').val('');
        fncInitizeVatDetailBody();
        fncInitializeVetDetailDialog();
        fncInitializePOPendingDetailDialog();
        fncDecimal();


        // Vendor Search    
        $("[id$=txtVendor]").autocomplete({
            source: function (request, response) {
                var obj = {};
                obj.searchData = request.term.replace("'", "''");
                obj.mode = "Vendor";

                $.ajax({
                    url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncCommonSearch")%>',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                valitemcode: item.split('|')[1],
                                valAddress: item.split('|')[2]
                            }
                        }))
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.message);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.message);
                    }
                });
            },
            select: function (e, i) {

                $('#<%=txtVendor.ClientID %>').val($.trim(i.item.valitemcode));
                return false;
            },
            minLength: 2
        });

        //Department Search   
            $("[id$=txtDepartment]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term.replace("'", "''");
                    obj.mode = "Department";

                    $.ajax({
                        url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncCommonSearch")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                select: function (e, i) {

                    $('#<%=txtDepartment.ClientID %>').val($.trim(i.item.valitemcode));
                    return false;
                },
                minLength: 1
            });

        // Category Search   
                $("[id$=txtCategory]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.searchData = request.term.replace("'", "''");
                        obj.mode = "Category";

                        $.ajax({
                            url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncCommonSearch")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    select: function (e, i) {

                        $('#<%=txtCategory.ClientID %>').val($.trim(i.item.valitemcode));
                        return false;
                    },
                    minLength: 1
                });

        // Class Search   
                    $("[id$=txtClass]").autocomplete({
                        source: function (request, response) {
                            var obj = {};
                            obj.searchData = request.term.replace("'", "''");
                            obj.mode = "Class";

                            $.ajax({
                                url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncCommonSearch")%>',
                                data: JSON.stringify(obj),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.message);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.message);
                                }
                            });
                        },
                        select: function (e, i) {

                            $('#<%=txtClass.ClientID %>').val($.trim(i.item.valitemcode));
                            return false;
                        },
                        minLength: 1
                    });

        // Brand Search   
                        $("[id$=txtBrand]").autocomplete({
                            source: function (request, response) {
                                var obj = {};
                                obj.searchData = request.term.replace("'", "''");
                                obj.mode = "Brand";

                                $.ajax({
                                    url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncCommonSearch")%>',
                                    data: JSON.stringify(obj),
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('|')[0],
                                                valitemcode: item.split('|')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        ShowPopupMessageBox(response.message);
                                    },
                                    failure: function (response) {
                                        ShowPopupMessageBox(response.message);
                                    }
                                });
                            },
                            select: function (e, i) {

                                $('#<%=txtBrand.ClientID %>').val($.trim(i.item.valitemcode));
                                return false;
                            },
                            minLength: 1
                        });

                            shortcut.add("Alt+F", function () {
                                var item = $("#<%=txtItemCode.ClientID%>").val().trim();
                                item = item + "-" + $("#<%=txtDescription.ClientID%>").val().trim();
                                if ($("#<%=txtItemCode.ClientID%>").val().trim() != "" && parseFloat($("#<%=txtInvQty.ClientID%>").val()) > 0) {
                                    fncOpenFOCScreen(item);
                                }
                            });

                            shortcut.add("Alt+W", function () {
                                var item = $("#<%=txtItemCode.ClientID%>").val().trim();
                                item = item + "-" + $("#<%=txtDescription.ClientID%>").val().trim();
                                if ($("#<%=txtItemCode.ClientID%>").val().trim() != "" && $("#<%=hidwbgrn.ClientID %>").val().trim() == "Y") {
                                    fncOpenWeightconQtyScreen(item);
                                }
                            });

                            if ($("#<%=hidError.ClientID%>").val() == "Error") {
            fncLoadHoldClosedGid('');
        }


        $(function () {
            $("#dialog-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 350,
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        $("#<%=hdfPendingPO.ClientID%>").val('Y')
                        $('#<%=btnSave.ClientID%>').click();
                    },
                    No: function () {
                        $(this).dialog("close");
                        $("#<%=hdfPendingPO.ClientID%>").val('N')
                          $('#<%=btnSave.ClientID%>').click();
                    }
                }
            });
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}


function fncHideFilter() {
    try {

        if ($('#<%=lnkHidefilter.ClientID%>').html() == "Hide Filter") {

            $("[id*=divAttribute]").hide();
            $("[id*=divFilter]").hide();
            $('#<%=lnkHidefilter.ClientID%>').html("Show Filter");
        }
        else {

            $("[id*=divAttribute]").show();
            $("[id*=divFilter]").show();
            $('#<%=lnkHidefilter.ClientID%>').html("Hide Filter");
        }

        return false;
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

$(document).ready(function () {

    fncGRNShortCut();
});

function fncGRNShortCut() {
    try {

        shortcut.add("Alt+G", function () {
            if ($("#divVatDetail").dialog('isOpen') == true) {
                $("#divVatDetail").dialog('close');
            }
            else {
                $("#divVatDetail").dialog('open');
            }

        });

        shortcut.add("Alt+P", function () {
            if ($("#divPOPendings").dialog('isOpen') == true) {
                $("#divPOPendings").dialog('close');
            }
            else {
                fncBindPendingPO();
                fncInitializePOPendingDetailDialog();
                $("#divPOPendings").dialog('open');
            }

        });
        shortcut.add("Alt+S", function () {
            if ($('#<%=txtMRP.ClientID %>').val() != "" && parseFloat($('#<%=txtMRP.ClientID %>').val()) > 0) {
                fncInitializeSPricePer();
                if ($("#divSellingPer").dialog('isOpen') == true) {
                    $("#divSellingPer").dialog('close');
                }
                else {
                    $('#<%=txtSellPricePercentage.ClientID %>').select();
                    $("#divSellingPer").dialog('open');
                }
            }
        }); 
    }
    catch (err) {
        fncToastError(err.message);
    }
        }
        function fncInitializeSPricePer() {
            try {
                $("#divSellingPer").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    title: "SPrice Percentage Discount",
                    appendTo: 'form:first'
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

function fncclientclickwcpok() {

    try {

        $("#<%=txtInvQty.ClientID%>").val($("#<%=txtQtyinUnits.ClientID%>").val());
        $("#<%=txtBCost.ClientID%>").val($("#<%=txtUnitCost.ClientID%>").val());
        fncInvQtyFocusOut();
        glwcpforcost = 1;
        fncclientclickwcpclear();
        $("#divWCP").dialog("close");
        $("#<%=txtInvQty.ClientID%>").focus();

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncclientclickwcpclear() {

    try {
        $("#<%=txtWeightQty.ClientID%>").val("0.00");
        $("#<%=txtKgRate.ClientID%>").val("0.00");
        $("#<%=txtTotalValue.ClientID%>").val("0.00");
        $("#<%=txtQtyinUnits.ClientID%>").val("0.00");
        $("#<%=txtUnitCost.ClientID%>").val("0.00");
        $("#<%=txtNetUnitCost.ClientID%>").val("0.00");
        $("#<%=txtWeightQty.ClientID%>").focus();

    }
    catch (err) {
        fncToastError(err.message);
    }
}

// Weight convertion
function fncWeightconerttoqty() {
    try {

        $('#<%=txtTotalValue.ClientID%>').val(($('#<%=txtWeightQty.ClientID%>').val() * $('#<%=txtKgRate.ClientID%>').val()).toFixed(2));
        if (($('#<%=txtQtyinUnits.ClientID%>').val()) > 0) {
            $('#<%=txtUnitCost.ClientID%>').val(($('#<%=txtTotalValue.ClientID%>').val() / $('#<%=txtQtyinUnits.ClientID%>').val()).toFixed(2));
        }
        $('#<%=txtNetUnitCost.ClientID%>').val(($('#<%=txtQtyinUnits.ClientID%>').val() * $('#<%=txtUnitCost.ClientID%>').val()).toFixed(2));



    }
    catch (err) {
        fncToastError(err.message);
    }
}

$(function () {
    SetAutoCompleteFoc();
});



function disableFunctionKeys(e) {

    var charCode = (e.which) ? e.which : e.keyCode;

    if (charCode == 27) {
        fncEnablePriceChangeAlert("Disable", "");
        if ($("#divItemSearch").css("display") == "block") {
            $("#divGRN").css("display", "block");
            $("#divItemSearch").css("display", "none");
            $("#<%=txtItemCode.ClientID%>").select();
        }

    }

    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

        if (e.keyCode == 117) {
            fncInvQty_KeyPress(e);
            e.preventDefault();
        }
        else if (e.keyCode == 114) {//// F3 Last Purchase Detail
            fncGetLastPurchaseDetail();
            e.preventDefault();
        }
        else if (e.keyCode == 123) {
            if ($('#<%=cbVendorItem.ClientID%>').is(':checked')) {
                $("#<%=cbVendorItem.ClientID%>").removeAttr("checked");
            }
            else {
                $("#<%=cbVendorItem.ClientID%>").attr("checked", "checked");
            }
            e.preventDefault();
        }
        else if (e.keyCode == 115) {
            fncSaveValidation();
            e.preventDefault();
        }
        else if (e.keyCode == 119) {
            $("#<%=btnclose.ClientID%>").click();
                e.preventDefault();
            }
            else if (e.keyCode == 121) {
                fncBarcodeQtyValidation();
                e.preventDefault();
            }
            else {
                e.preventDefault();
            }
}
};

$(document).ready(function () {
    $(document).on('keydown', disableFunctionKeys);
    if ($("#<%=hidGidGSTFlag.ClientID%>").val() == "2") {
        $(".Payment_fixed_headers thead").css("background-color", "red");
    }
});


//VetDetail Body Creation
function fncInitizeVatDetailBody() {
    vatDetailtbody = $("#tblVetDetail tbody");
}



// Textiles Item Search 
function fncItemSearchTextiles() {
    try {


        $("#<%=txtVendor.ClientID%>").val($("#<%=txtVendorCode.ClientID%>").val().trim());
        fncSubClearTextBox();
        fncShowItemSearchTableTextiles();

    }
    catch (err) {
        fncToastError(err.message);
    }
}

//// Item Search 
function fncItemSearch() {
    try {
        fncItemSearchForGrid($("#<%=txtItemCode.ClientID%>").val().trim());
    }
    catch (err) {
        fncToastError(err.message);
    }
}

// Textile Margin validation
function fncSelectValidationMargin() {
    var status = true;

    if ($('#<%=txtVSerialNo.ClientID%>').val().trim() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtVSerialNo.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Serial No..!");
        status = false;
    }
    else if ($('#<%=txtMrpT.ClientID%>').val() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter MRP..!");
        status = false;
    }
    else if ($('#<%=txtSellingPriceT.ClientID%>').val() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtSellingPriceT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Selling Price..!");
        status = false;
    }
    else if ($('#<%=txtNetCostT.ClientID%>').val() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtNetCostT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Net Cost..!");
        status = false;
    }

    var rowCount = $("#tblMarginFix tr").length;

    if (status = true) {
        fncBindGRNTableMargin();
        $('#divMarginFixing').dialog('close');
        $('#divItemSearchTextiles').dialog('close');
        //XmlGridValue(); 
    }


     <%--   $('#<%=txtItemCode.ClientID%>').val('500220');
            var e = jQuery.Event("keypress");
            e.which = 13; //enter keycode
            e.keyCode = 13;
            $("#ContentPlaceHolder1_txtItemCode").keypress(e);--%>
    //fncGetInventoryDetailForGID('500220', "", '');

    return false;
}

function fncSelectValidation() {
    var status = true;
   
    if ($('#<%=txtVSerialNo.ClientID%>').val().trim() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtVSerialNo.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Serial No..!");
        status = false;
    }
    else if ($('#<%=txtMrpT.ClientID%>').val() == "" || $('#<%=txtMrpT.ClientID%>').val() <= 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter MRP..!");
        status = false;
    }
    else if ($('#<%=txtSellingPriceT.ClientID%>').val() == "" || $('#<%=txtSellingPriceT.ClientID%>').val() <= 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtSellingPriceT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Selling Price..!");
        status = false;
    }
    else if ($('#<%=txtNetCostT.ClientID%>').val() == "" || $('#<%=txtNetCostT.ClientID%>').val() <= 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtNetCostT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Please Enter Net Cost..!");
        status = false;
    } else if (parseFloat($('#<%=txtNetCostT.ClientID%>').val()) > parseFloat($('#<%=txtMrpT.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("MRP must be Greater than NetCost");
        status = false;
    }
     else if (parseFloat($('#<%=txtNetCostT.ClientID%>').val()) > parseFloat($('#<%=txtMrpT.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("MRP must be Greater than NetCost");
        status = false;
     }
     else if (parseFloat($('#<%=txtCostPriceT.ClientID%>').val()) > parseFloat($('#<%=txtMrpT.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("MRP must be Greater than Cost");
        status = false;
     }
     else if (parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) > parseFloat($('#<%=txtMrpT.ClientID%>').val()) ) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("MRP must be Greater than Selling Price");
        status = false;
    }
	 else if (parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) < parseFloat($('#<%=txtNetCostT.ClientID%>').val()) ) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrpT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject("Selling Price must be Greater than NetCost");
        status = false;
    }

    if (status == true) {
	 $("#tblItemDetail tr").each(function () {
	     var rowObj = $("td", this);
	     var source = $(this);
	     var del = source;
        if (rowObj.length > 0) {
            if (rowObj.find("input").is(":checked")) {
                if (fncCheckStyleCode(rowObj.eq(2).text().trim(), $('#<%=txtStyleCode.ClientID%>').val().trim(), $('#<%=txtMrpT.ClientID%>').val(), 'F1', rowObj.eq(0).text().trim()) == false) {
                    status = false;
                    return false;
                }
            $("#tblGRN [id*=GRNBodyrow]").each(function () {
       var rowObjgrn = $(this);
       var glRowObj = rowObjgrn;
        if (rowObjgrn.find('td input[id*="txtItemcode"]').val().trim() == rowObj.eq(2).text().trim() &&
          rowObjgrn.find('td input[id*="txtStyleCode"]').val().trim() == $('#<%=txtStyleCode.ClientID%>').val().trim()) {  
             fncToastError("This Item Already Exists" +' - ' +rowObj.eq(2).text().trim()); 
            del.remove();
                }

         });
		 }
		 }
     });

        if (status == false)
            return false;
        else {
            $('#<%=hidCostEditAllow.ClientID %>').val("1");
            //if (status == true) {
            fncBindGRNTable();
            //}
            //XmlGridValue(); 
            //$("#divItemSearchTextiles").css("display", "none");
            $('#divItemSearchTextiles').dialog('close');
        }
    }

     <%--   $('#<%=txtItemCode.ClientID%>').val('500220');
            var e = jQuery.Event("keypress");
            e.which = 13; //enter keycode
            e.keyCode = 13;
            $("#ContentPlaceHolder1_txtItemCode").keypress(e);--%>
    //fncGetInventoryDetailForGID('500220', "", '');
    //if (status == true)
    //    return true;
    //else
    return status;
}


/// Data Bind to Margin Table
function fncItemForMarginGrid() {
    try {

        var obj = {};
        var bActive = true;
        var bExistAnd = false;
        var bAttribSel = false;
        var dAttribVal = 0;
        var bSuccess = false;
        var sQuery = 'delete from tblSizewiseMarginFixing ';

        $("#tblItemDetail tr").each(function () {
            var rowObj = $("td", this);
            if (rowObj.length > 0) {
                if (rowObj.find("input").is(":checked")) {

                    sQuery += " exec sp_SizewiseMarginFixing_Web  'USER','" + rowObj.eq(2).text().trim() + "' ";
                }
            }
        });

        //sQuery += "  SELECT TerminalCode, ItemCode, isnull(Size,'') Size, SizeCode FROM tblSizewiseMarginFixing order by Size";
        sQuery += "SELECT TerminalCode, ItemCode, isnull(Size,'') Size, SizeCode ,(Select Description from tblinventory where inventorycode =ItemCode ) as Name  FROM tblSizewiseMarginFixing order by Size";
        //alert(sQuery); 
        //alert(fncSelectValidation());

        $('#<%=txtMarginCost.ClientID %>').val($('#<%=txtNetCostT.ClientID %>').val());

        if ($('#<%=txtNetCostT.ClientID%>').val() == '') {
            bSuccess = true;
        }

        //if (fncSelectValidation() == false)
        //    bSuccess = true;

        //if (bSuccess) {

        //    return false;
        //}

        console.log(sQuery);
        obj.query = sQuery;

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncItemSearchForGridtextiles")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                fncTextileItemMarginBindtoTable(msg);
                fncShowItemMarginTable();
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });

        return false;
    }
    catch (err) {
        fncToastError(err.message);
    }

}

/// Data Bind to Textiles Item Grid
function fncItemSearchForGridTextiles() {
    try {

        var obj = {};
        var bActive = true;
        var bExistAnd = false;
        var bAttribSel = false;
        var dAttribVal = 0;
        var bSuccess = false;

        $("#<%=grdAttribute.ClientID %> tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {
                for (var k = 0; k < cells.length; ++k) {

                    if (cells.find("select").eq(k).val() != '0') {

                        dAttribVal = dAttribVal + 1;
                        bSuccess = true;
                    }
                }
            }
        });

        if ($('#<%=txtVendor.ClientID%>').val() != '')
            bSuccess = true;

        if ($('#<%=txtDepartment.ClientID%>').val() != '')
            bSuccess = true;

        if ($('#<%=txtCategory.ClientID%>').val() != '')
            bSuccess = true;

        if ($('#<%=txtClass.ClientID%>').val() != '')
            bSuccess = true;

        if ($('#<%=txtBrand.ClientID%>').val() != '')
            bSuccess = true;

        if ($('#<%=txtItemSearchT.ClientID%>').val() != '')
            bSuccess = true;

        var sQuery = '  SELECT TOP 100 A.InventoryCode,Description,ITaxPer3,UnitCost,GrossCost,NetCost,B.MRP,MarginPer,sellingprice,Uom,DepartmentCode,CategoryCode,BrandCode,Class,VendorCode,isnull(AverageCost,0) AverageCost,BasicSelling,Allowexpiredate,Batch,ItemType,PricingType, MarkDownPerc,EarnedMargin,MFPer,MFAmt FROM tblinventory A INNER JOIN tblinventorypricing B  ON A.Inventorycode = B.Inventorycode';
        //Vijay 20190207-MNS
        if (bSuccess) {

            sQuery = 'SELECT TOP 400 A.InventoryCode,Description,UnitCost,GrossCost,NetCost,B.MRP,MarginPer,sellingprice,Uom,';
            sQuery += 'DepartmentCode,CategoryCode,BrandCode,Class,VendorCode,ITaxPer3,isnull(AverageCost,0) AverageCost,BasicSelling,Allowexpiredate,Batch,ItemType,PricingType, MarkDownPerc,EarnedMargin,MFPer,MFAmt FROM tblinventory A INNER JOIN tblinventorypricing B  ON A.Inventorycode = B.Inventorycode WHERE  ';

            
            if ($('#<%=txtItemSearchT.ClientID%>').val().trim() == '') {

                $("#<%=grdAttribute.ClientID %> tr").each(function () {

                    var cells = $("td", this);
                    if (cells.length > 0) {

                        if (dAttribVal > 0) {
                            sQuery += '  A.inventorycode IN ( SELECT A.inventorycode FROM ( ';

                            for (var j = 0; j < cells.length; ++j) {
                                if (cells.find("select").eq(j).val() != '0') {
                                    if (bActive == true) {
                                        sQuery += " select * from view_inventoryAttribute where AttributeName = '" + $(this).parents('table:first').find('th').eq(j).text().trim() + "' and valueName = '" + cells.find("select").eq(j).val() + "'";
                                        bActive = false;
                                    }
                                    else {
                                        sQuery += " UNION ALL select * from view_inventoryAttribute where AttributeName = '" + $(this).parents('table:first').find('th').eq(j).text().trim() + "' and valueName = '" + cells.find("select").eq(j).val() + "'";
                                    }
                                }
                            }

                            sQuery += ' ) as a group by a.inventorycode having count(a.inventorycode) = ' + dAttribVal + ' )';

                            bExistAnd = true;
                        }

                    }
                });


                if ($('#<%=chkonlytextile.ClientID%>').is(':checked')) {
                    if (bExistAnd == true) {
                        sQuery += " and  Model = 'TEXTILE' ";
                    }
                    else {
                        sQuery += "  Model = 'TEXTILE' ";
                        bExistAnd = true;
                    }
                }

                if ($('#<%=txtVendor.ClientID%>').val() != '') {
                    if (bExistAnd == true) {
                        sQuery += " and  VendorCode = '" + $('#<%=txtVendor.ClientID%>').val() + "' ";
                    }
                    else {
                        sQuery += "  VendorCode = '" + $('#<%=txtVendor.ClientID%>').val() + "' ";
                        bExistAnd = true;
                    }
                }

                if ($('#<%=txtDepartment.ClientID%>').val() != '') {
                    if (bExistAnd == true) {
                        sQuery += " and  DepartmentCode = '" + $('#<%=txtDepartment.ClientID%>').val() + "' ";
                    }
                    else {
                        sQuery += "  DepartmentCode = '" + $('#<%=txtDepartment.ClientID%>').val() + "' ";
                        bExistAnd = true;
                    }
                }

                if ($('#<%=txtCategory.ClientID%>').val() != '') {
                    if (bExistAnd == true) {
                        sQuery += " and  CategoryCode = '" + $('#<%=txtCategory.ClientID%>').val() + "' ";
                    }
                    else {
                        sQuery += "  CategoryCode = '" + $('#<%=txtCategory.ClientID%>').val() + "' ";
                        bExistAnd = true;
                    }
                }

                if ($('#<%=txtClass.ClientID%>').val() != '') {
                    if (bExistAnd == true) {
                        sQuery += " and  Class = '" + $('#<%=txtClass.ClientID%>').val() + "' ";
                    }
                    else {
                        sQuery += "  Class = '" + $('#<%=txtClass.ClientID%>').val() + "' ";
                        bExistAnd = true;
                    }
                }

                if ($('#<%=txtBrand.ClientID%>').val() != '') {
                    if (bExistAnd == true) {
                        sQuery += " and  BrandCode = '" + $('#<%=txtBrand.ClientID%>').val() + "' ";
                    }
                    else {
                        sQuery += "  BrandCode = '" + $('#<%=txtBrand.ClientID%>').val() + "' ";
                        bExistAnd = true;
                    }
                }
            }
            else {
                sQuery += " A.InventoryCode = '" + $('#<%=txtItemSearchT.ClientID%>').val() + "' ";
            }
        }

        sQuery += " order by Description ";

        //alert(sQuery);
        console.log(sQuery);

        obj.query = sQuery;

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncItemSearchForGridtextiles")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                fncSubClearTextBox();
                fncTextileItemBindtoTable(msg);

                $('#<%=lnkHidefilter.ClientID%>').click();
                $('#<%=txtVSerialNo.ClientID%>').focus();

            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });

        return false;
    }
    catch (err) {
        fncToastError(err.message);
    }

}



/// Data Bind to Grid
function fncItemSearchForGrid(value) {
    try {
        var obj = {};

        if ($('#<%=cbVendorItem.ClientID%>').is(':checked')) {
            obj.vendorcode = $("#<%=txtVendorCode.ClientID%>").val().trim();
        }
        else {
            obj.vendorcode = "";
        }

        obj.searchData = value;
        obj.franchise = $("#<%=hidFranValue.ClientID%>").val();
        obj.gidLocation = $("#<%=hidGidLocationcode.ClientID%>").val();

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncItemSearchForGrid")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                fncItemBindtoTable(msg);

            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }

}

function XmlGridValue() {
    try {

        var xml = '<NewDataSet>';
        $("#tblGRNBulk tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {

                xml += "<Table>";
                for (var j = 0; j < cells.length; ++j) {

                    xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';

                }
                xml += "</Table>";
            }
        });

        xml = xml + '</NewDataSet>'

        $("#<%=HiddenXmldata.ClientID %>").val(escape(xml));

    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

/// Textile Item bind Margin Fix table

function fncBindGRNTable() {

    try {

        var objSearchData, tblSearchData, batchNo = "";
        //objSearchData = jQuery.parseJSON(msg.d);
        //tblSearchData = $("#tblGRNBulk tbody");
        //tblSearchData.children().remove();

        var Rowno = 0;
        var GrossBasicAmount = 0;
        var TotalValues = 0;
        var GrossGSTAmount = 0;
        var Qty = parseFloat($('#<%=txtQtyT.ClientID%>').val());
        var CostPrice = parseFloat($('#<%=txtCostPriceT.ClientID%>').val());
        var GSTAmount = parseFloat($('#<%=txtGSTAmtT.ClientID%>').val());
        var SGSTperc = parseFloat($('#<%=txtGSTPercT.ClientID%>').val()) / 2;
        var grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));

        GrossBasicAmount = Qty * parseFloat($('#<%=txtCostPriceT.ClientID %>').val());
        TotalValues = GrossBasicAmount + (GSTAmount * Qty);
        //((parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) -  parseFloat($('#<%=txtNetCostT.ClientID%>').val())) / parseFloat($('#<%=txtNetCostT.ClientID%>').val()) * 100).toFixed(2)
        var minvalSP = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
        var profitMargin = minvalSP / parseFloat($('#<%=txtNetCostT.ClientID%>').val()) * 100;

        var minvalMrp = parseFloat($('#<%=txtMrpT.ClientID%>').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
        var PurchaseProfitper = minvalMrp / parseFloat($('#<%=txtMrpT.ClientID%>').val()) * 100;

        var itemcode, ItemDesc, POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
           WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
           GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
           GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
           PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
           PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, TotalCharges, LandingCost, PurchaseProfitper,
           NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
           AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
           FTCBatchNo, FTCInputQty, StyleCode, SerialNo;

        POQty = "0";
        InvQty = Qty;
        lQty = Qty;
        Foc = "0";
        MRP = parseFloat($('#<%=txtMrpT.ClientID%>').val()).toFixed(2);
        BasicCost = parseFloat($('#<%=txtCostPriceT.ClientID%>').val()).toFixed(2);
        SellingPrice = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
        WPrice1 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
        WPrice2 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
        WPrice3 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
        GrossBasicAmt = GrossBasicAmount.toFixed(2);
        DiscPer = parseFloat($('#<%=txtDiscountPercT.ClientID%>').val()).toFixed(2);
        DiscAmt = parseFloat($('#<%=txtDiscountAmtT.ClientID%>').val()).toFixed(2);
        GrossDiscAmt = (Qty * parseFloat($('#<%=txtDiscountAmtT.ClientID%>').val())).toFixed(2);
        SDper = parseFloat($('#<%=txtSDPercT.ClientID%>').val()).toFixed(2);
        SDAmt = parseFloat($('#<%=txtSDAmtT.ClientID%>').val()).toFixed(2);
        GrossShDiscAmt = (Qty * parseFloat($('#<%=txtSDAmtT.ClientID%>').val())).toFixed(2);
        GrossCost = parseFloat($('#<%=txtGrossCostT.ClientID%>').val()).toFixed(2);
        TotalGrossCost = (Qty * parseFloat($('#<%=txtGrossCostT.ClientID%>').val())).toFixed(2);
        if ($('#<%=hidVendorStatus.ClientID%>').val() == "1") {
            ITaxperc1 = SGSTperc.toFixed(2);
            ITaxperc2 = SGSTperc.toFixed(2);
            ITaxAmt1 = $('#<%=hidITaxAmt1.ClientID %>').val();
            ITaxAmt2 = $('#<%=hidITaxAmt2.ClientID %>').val();
            ITaxt1 = SGSTperc.toFixed(2);
            ITax2 = SGSTperc.toFixed(2);

        }
        else {
            ITaxperc1 = parseFloat($('#<%=txtGSTPercT.ClientID%>').val());
            ITaxperc2 = "0";
            ITaxAmt1 = GSTAmount.toFixed(4);
            ITaxAmt2 = "0";
            ITaxt1 = parseFloat($('#<%=txtGSTPercT.ClientID%>').val());
            ITax2 = "0";

        }//vijay

        Cessper = $('#<%=txtCessPer.ClientID %>').val();
        CessAmt = $('#<%=hidCessAmt.ClientID %>').val();
        AddCessAmt = $('#<%=txtAddCess.ClientID %>').val();
        GrossVATAmt = GSTAmount.toFixed(4) * Qty;
        GrocessCessAmt = "0";
        GRAddCessAmt = "0";
        NetCost = parseFloat($('#<%=txtNetCostT.ClientID%>').val()).toFixed(2);
        TotalValue = TotalValues.toFixed(2);
        FTotalValue = "0";
        EarnedMarPerc = $('#<%=hidMFPer.ClientID %>').val();
        EarMarAmt = $('#<%=hidMFAmt.ClientID %>').val();
        Marginfixed = $('#<%=hidMarginFixAmt.ClientID %>').val();
        PrintQty = "0";
        ITaxt1 = SGSTperc.toFixed(2);
        ITax2 = SGSTperc.toFixed(2);
        MedBatch = "0";
        ExpDate = "0";
        OldEarnedMarg = "0";
        ProfitMargin = profitMargin.toFixed(2);
        FOCItem = "";
        FOCBatch = "";
        PurchaseCharges = "0";
        FreightCharges = "0";
        VendorMargin = "0";
        TotalCharges = "0";
        LandingCost = "0";
        PurchaseProfitper = PurchaseProfitper.toFixed(2);
        NetCostAffect = "0";
        Batch = "1";
        ItemType = $('#<%=hidItemType.ClientID %>').val();


        if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M" && $('#<%=hidMarginFixType.ClientID %>').val() == "M") {
            PricingType = "1";/// AlWays MRP
        }
        else if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M") {
            PricingType = "2";// Purchase Only MRP
        }
        else {
            PricingType = "";
        }
        MarkDownPerc = $('#<%=hidMarkDownPerc.ClientID %>').val();
        OldSellingPrice = $('#<%=hidOldSellingPrice.ClientID %>').val();
        AvgCost = $('#<%=hidAvgCost.ClientID %>').val();
        BasicSelling = $('#<%=hidBasicSellingPrice.ClientID %>').val();
        AllowExpireDate = '0'; //$('#<%=hidAllowExpireDate.ClientID %>').val();

        ItemBatchNo = AddZeros($('#<%=txtVSerialNo.ClientID%>').val(), 3); //fncCreateBatchNo($('#<%=txtMRP.ClientID %>').val());                 

        if ($('#<%=hidFocStatus.ClientID %>').val() == "FTC") {
            FTCFromdate = $('#<%=txtfocFromDate.ClientID %>').val();
            FTCToDate = $('#<%=txtFocToDate.ClientID %>').val();
            FTCBuyQty = $('#<%=txtFocBuyQty.ClientID %>').val();
            FTCFreeQty = $('#<%=txtFocFreeQty.ClientID %>').val();
        }
        else {
            FTCFromdate = '';
            FTCToDate = '';
            FTCBuyQty = "0";
            FTCFreeQty = "0";
        }

        if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBreakpricestatus.ClientID %>').val() == "1") // Barcode , KG Item and Break Price
            GridRowStatus = "BarKGBre";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS") // Barcode and KG Item 
            GridRowStatus = "BarKG";
        else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")//KG Item and Break price
            GridRowStatus = "KGBre";
        else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS")//KG Item 
            GridRowStatus = "KG";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode , pieces Item and Break Price
            GridRowStatus = "BarPCSBre";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS") // Barcode and pieces Item 
                     GridRowStatus = "BarPCS";
                 else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")
            GridRowStatus = "PCSBre";
        else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS")
                    GridRowStatus = "PCS";
                else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode Only
                     GridRowStatus = "Bar";
                 else
                     GridRowStatus = "";

    if ($('#<%=hidCostEditAllow.ClientID %>').val() == "0") {
            GridRowStatus = GridRowStatus + "CEF";
        }

        PrvData = $('#<%=hidPrvMRP.ClientID %>').val() + "," + $('#<%=hidOldCost.ClientID %>').val() + ",";
        PrvData = PrvData + $('#<%=hidoldMarFixed.ClientID %>').val() + "," + $('#<%=hidoldMarEarned.ClientID %>').val() + "," + $('#<%=hidPrvUnitCost.ClientID %>').val();

        FTCItemcode = $('#<%=hidFTCItemcode.ClientID %>').val();
        FTCBatchNo = $('#<%=hidFTCBatchNo.ClientID %>').val();
        FTCInputQty = $('#<%=hidFTCInputQty.ClientID %>').val();
        WQty = $('#<%=txtWQty.ClientID %>').val();
        if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
            WUOM = $('#<%=ddlMultipleUOM.ClientID %>').find("option:selected").text();
        }
        else {
            WUOM = $('#<%=txtwuom.ClientID %>').val();
        }

        StyleCode = $('#<%=txtStyleCode.ClientID %>').val();
        SerialNo = $('#<%=txtVSerialNo.ClientID%>').val();

        //alert(minval + '-' + profitMargin); 
        $("#tblItemDetail tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {

                if (cells.find("input").is(":checked")) {
                    
                        maxItemCountNo = parseFloat(maxItemCountNo) + parseFloat(1);

                        fncAddNewRowsToGrid(maxItemCountNo, cells.eq(2).text().trim(), cells.eq(3).text().trim(), POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
                        WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
                        GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
                        GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
                        PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
                        PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, LandingCost, PurchaseProfitper,
                        NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
                        AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
                        FTCBatchNo, FTCInputQty, WQty, WUOM, StyleCode, SerialNo);

                        //maxItemCountNo = parseInt(maxItemCountNo) + 1; 
                     
                }
            }
        });

        maxItemCountNo = $("#tblGRN tbody").children().length;

        fncSubClear('clr');
        fncRowArrayClear();

        fncNetValueCalculation();
        fncSaveGRNEntryTemporarly();

    }
    catch (err) {
        fncToastError(err.message);
    }

}


function AddZeros(num, size) {
    var s = String(num);
    while (s.length < (size || 2)) { s = "0" + s; }
    //alert(s);
    return s;
}

function fncBindGRNTableMargin() {
    try {

        var objSearchData, tblSearchData, batchNo = "";
        //objSearchData = jQuery.parseJSON(msg.d);
        //tblSearchData = $("#tblGRNBulk tbody");
        //tblSearchData.children().remove(); 

        var Rowno = 0;
        var GrossBasicAmount = 0;
        var TotalValues = 0;
        var GrossGSTAmount = 0;
        var Qty = parseFloat($('#<%=txtQtyT.ClientID%>').val());
        var CostPrice = parseFloat($('#<%=txtCostPriceT.ClientID%>').val());
        var GSTAmount = parseFloat($('#<%=txtGSTAmtT.ClientID%>').val());
        var SGSTperc = parseFloat($('#<%=txtGSTPercT.ClientID%>').val()) / 2;
        var grossAmt = parseFloat($('#<%=txtCostPriceT.ClientID %>').val()) - (parseFloat($('#<%=txtDiscountAmtT.ClientID %>').val()) + parseFloat($('#<%=txtSDAmtT.ClientID %>').val()));

        GrossBasicAmount = Qty * parseFloat($('#<%=txtCostPriceT.ClientID %>').val());
        TotalValues = GrossBasicAmount + (GSTAmount * Qty);
        //((parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) -  parseFloat($('#<%=txtNetCostT.ClientID%>').val())) / parseFloat($('#<%=txtNetCostT.ClientID%>').val()) * 100).toFixed(2)
        var minvalSP = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
        var profitMargin = minvalSP / parseFloat($('#<%=txtNetCostT.ClientID%>').val()) * 100;

        var minvalMrp = parseFloat($('#<%=txtMrpT.ClientID%>').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
        var PurchaseProfitper = minvalMrp / parseFloat($('#<%=txtMrpT.ClientID%>').val()) * 100;

        var itemcode, ItemDesc, POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
           WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
           GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
           GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
           PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, FOCItem, FOCBatch,
           PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, TotalCharges, LandingCost,
           NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
           AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
           FTCBatchNo, FTCInputQty, StyleCode, SerialNo;

        POQty = "0";
        InvQty = Qty;
        lQty = Qty;
        Foc = "0";
        MRP = parseFloat($('#<%=txtMrpT.ClientID%>').val()).toFixed(2);
        BasicCost = parseFloat($('#<%=txtCostPriceT.ClientID%>').val()).toFixed(2);
            <%--SellingPrice = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
            WPrice1 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
            WPrice2 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);
            WPrice3 = parseFloat($('#<%=txtSellingPriceT.ClientID%>').val()).toFixed(2);--%>
        GrossBasicAmt = GrossBasicAmount.toFixed(2);
        DiscPer = parseFloat($('#<%=txtDiscountPercT.ClientID%>').val()).toFixed(2);
        DiscAmt = parseFloat($('#<%=txtDiscountAmtT.ClientID%>').val()).toFixed(2);
        GrossDiscAmt = (Qty * parseFloat($('#<%=txtDiscountAmtT.ClientID%>').val())).toFixed(2);
        SDper = parseFloat($('#<%=txtSDPercT.ClientID%>').val()).toFixed(2);
        SDAmt = parseFloat($('#<%=txtSDAmtT.ClientID%>').val()).toFixed(2);
        GrossShDiscAmt = (Qty * parseFloat($('#<%=txtSDAmtT.ClientID%>').val())).toFixed(2);
        GrossCost = parseFloat($('#<%=txtGrossCostT.ClientID%>').val()).toFixed(2);
        TotalGrossCost = (Qty * parseFloat($('#<%=txtGrossCostT.ClientID%>').val())).toFixed(2);
        //ITaxperc1 = SGSTperc.toFixed(2);
        //ITaxperc2 = SGSTperc.toFixed(2);
        if ($('#<%=hidVendorStatus.ClientID%>').val() == "1") {
            ITaxperc1 = SGSTperc.toFixed(2);
            ITaxperc2 = SGSTperc.toFixed(2);
            ITaxAmt1 = $('#<%=hidITaxAmt1.ClientID %>').val();
            ITaxAmt2 = $('#<%=hidITaxAmt2.ClientID %>').val();
            ITaxt1 = SGSTperc.toFixed(2);
            ITax2 = SGSTperc.toFixed(2);

        }
        else {
            ITaxperc1 = parseFloat($('#<%=txtGSTPercT.ClientID%>').val());
            ITaxperc2 = "0";
            ITaxAmt1 = GSTAmount.toFixed(4);
            ITaxAmt2 = "0";
            ITaxt1 = parseFloat($('#<%=txtGSTPercT.ClientID%>').val());
            ITax2 = "0";

        }//vijay 20190207
        Cessper = $('#<%=txtCessPer.ClientID %>').val();
       <%-- ITaxAmt1 = $('#<%=hidITaxAmt1.ClientID %>').val();
        ITaxAmt2 = $('#<%=hidITaxAmt2.ClientID %>').val();--%>
        CessAmt = $('#<%=hidCessAmt.ClientID %>').val();

        AddCessAmt = $('#<%=txtAddCess.ClientID %>').val();
        GrossVATAmt = (GSTAmount.toFixed(4) * Qty);
        GrocessCessAmt = "0";
        GRAddCessAmt = "0";
        NetCost = parseFloat($('#<%=txtNetCostT.ClientID%>').val()).toFixed(2);
        TotalValue = TotalValues.toFixed(2);
        FTotalValue = "0";
        EarnedMarPerc = $('#<%=hidMFPer.ClientID %>').val();
        EarMarAmt = $('#<%=hidMFAmt.ClientID %>').val();
        Marginfixed = $('#<%=hidMarginFixAmt.ClientID %>').val();
        PrintQty = "0";
        //ITaxt1 = SGSTperc.toFixed(2);
        //ITax2 = SGSTperc.toFixed(2);
        MedBatch = "0";
        ExpDate = "0";
        OldEarnedMarg = "0";
        // ProfitMargin = profitMargin.toFixed(2);			
        FOCItem = "";
        FOCBatch = "";
        PurchaseCharges = "0";
        FreightCharges = "0";
        VendorMargin = "0";
        TotalCharges = "0";
        LandingCost = "0";
        //PurchaseProfitper = PurchaseProfitper.toFixed(2);
        NetCostAffect = "0";
        Batch = "1";
        ItemType = $('#<%=hidItemType.ClientID %>').val();

        if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M" && $('#<%=hidMarginFixType.ClientID %>').val() == "M") {
            PricingType = "1";/// AlWays MRP
        }
        else if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M") {
            PricingType = "2";// Purchase Only MRP
        }
        else {
            PricingType = "";
        }
        MarkDownPerc = $('#<%=hidMarkDownPerc.ClientID %>').val();
        OldSellingPrice = $('#<%=hidOldSellingPrice.ClientID %>').val();
        AvgCost = $('#<%=hidAvgCost.ClientID %>').val();
        BasicSelling = $('#<%=hidBasicSellingPrice.ClientID %>').val();
        AllowExpireDate = '0'; //$('#<%=hidAllowExpireDate.ClientID %>').val();

        ItemBatchNo = AddZeros($('#<%=txtVSerialNo.ClientID%>').val(), 3);//fncCreateBatchNo($('#<%=txtMRP.ClientID %>').val());                 

        if ($('#<%=hidFocStatus.ClientID %>').val() == "FTC") {
            FTCFromdate = $('#<%=txtfocFromDate.ClientID %>').val();
            FTCToDate = $('#<%=txtFocToDate.ClientID %>').val();
            FTCBuyQty = $('#<%=txtFocBuyQty.ClientID %>').val();
            FTCFreeQty = $('#<%=txtFocFreeQty.ClientID %>').val();
        }
        else {
            FTCFromdate = '';
            FTCToDate = '';
            FTCBuyQty = "0";
            FTCFreeQty = "0";
        }

        if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBreakpricestatus.ClientID %>').val() == "1") // Barcode , KG Item and Break Price
            GridRowStatus = "BarKGBre";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS") // Barcode and KG Item 
            GridRowStatus = "BarKG";
        else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")//KG Item and Break price
            GridRowStatus = "KGBre";
        else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS")//KG Item 
                     GridRowStatus = "KG";
                 else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode , pieces Item and Break Price
            GridRowStatus = "BarPCSBre";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS") // Barcode and pieces Item 
                    GridRowStatus = "BarPCS";
                else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")
                     GridRowStatus = "PCSBre";
                 else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS")
            GridRowStatus = "PCS";
        else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode Only
                    GridRowStatus = "Bar";
                else
                    GridRowStatus = "";

    if ($('#<%=hidCostEditAllow.ClientID %>').val() == "0") {
            GridRowStatus = GridRowStatus + "CEF";
        }

        PrvData = $('#<%=hidPrvMRP.ClientID %>').val() + "," + $('#<%=hidOldCost.ClientID %>').val() + ",";
        PrvData = PrvData + $('#<%=hidoldMarFixed.ClientID %>').val() + "," + $('#<%=hidoldMarEarned.ClientID %>').val() + "," + $('#<%=hidPrvUnitCost.ClientID %>').val();

        FTCItemcode = $('#<%=hidFTCItemcode.ClientID %>').val();
        FTCBatchNo = $('#<%=hidFTCBatchNo.ClientID %>').val();
        FTCInputQty = $('#<%=hidFTCInputQty.ClientID %>').val();
        WQty = $('#<%=txtWQty.ClientID %>').val();
        if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
            WUOM = $('#<%=ddlMultipleUOM.ClientID %>').find("option:selected").text();
        }
        else {
            WUOM = $('#<%=txtwuom.ClientID %>').val();
        }

        StyleCode = $('#<%=txtStyleCode.ClientID %>').val();
        SerialNo = $('#<%=txtVSerialNo.ClientID%>').val();

        $("#tblMarginFix tr").each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {

                //if (cells.find("input").is(":checked")) {                      

                var minvalSP = parseFloat(cells.eq(8).find('input').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
                var ProfitMargin = minvalSP / parseFloat($('#<%=txtNetCostT.ClientID%>').val()) * 100;

                var minvalMrp = parseFloat(cells.eq(6).find('input').val()) - parseFloat($('#<%=txtNetCostT.ClientID%>').val());
                var PurchaseProfitper = minvalMrp / parseFloat(cells.eq(6).find('input').val()) * 100;

                //alert(cells.eq(9).text().trim() + " - " + profitMargin.toFixed(2) + " - " + PurchaseProfitper.toFixed(2));
                maxItemCountNo = parseFloat(maxItemCountNo) + parseFloat(1);

                fncAddNewRowsToGrid(maxItemCountNo, cells.eq(2).text().trim(), cells.eq(9).text().trim(), POQty, InvQty, lQty, Foc, parseFloat(cells.eq(6).find('input').val()).toFixed(2), BasicCost,
                parseFloat(cells.eq(8).find('input').val()).toFixed(2),
                parseFloat(cells.eq(8).find('input').val()).toFixed(2), parseFloat(cells.eq(8).find('input').val()).toFixed(2),
                parseFloat(cells.eq(8).find('input').val()).toFixed(2),
                GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
                GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
                GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
                PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
                PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, LandingCost, PurchaseProfitper,
                NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
                AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
                FTCBatchNo, FTCInputQty, WQty, WUOM, StyleCode, SerialNo);

            }
        });

        maxItemCountNo = $("#tblGRN tbody").children().length;
        fncSubClear('clr');
        fncRowArrayClear();

        fncNetValueCalculation();
        fncSaveGRNEntryTemporarly();
    }
    catch (err) {
        fncToastError(err.message);
    }

}

/// Textile Item bind Margin Fix table
function fncTextileItemMarginBindtoTable(msg) {
    try {

        var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
        objSearchData = jQuery.parseJSON(msg.d);

        tblSearchData = $("#tblMarginFix tbody");
        tblSearchData.children().remove();


        if (objSearchData.length > 0) {
            for (var i = 0; i < objSearchData.length; i++) {

                Rowno = parseFloat(i) + parseFloat(1);
                row = "<tr id='trMarginRow_" + i + "' tabindex='" + i + "' >" +
                    "<td id='tdRowNo_" + i + "' >" + Rowno + "</td>" +
                    '<td><input type="checkbox" class="chkFix" onchange= handleChange(this); id="ckMargFix" name= "' + Rowno + '" /></td>' +
                    "<td id='tdItemcode_" + i + "' > " + objSearchData[i]["ItemCode"] + "</td>" +
                    "<td >" + objSearchData[i]["Size"] + "</td>" +
                    "<td ><input type='text' class='Margin' onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;' id='txtMargPerc_" + i + "'/></td>" +
                    "<td ><input type='text' class='InDec' onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;' id='txtIDPerc_" + i + "'/></td>" +
                    "<td ><input type='text' class='Mrp' onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;' id='txtMarginMrp_" + i + "'/></td>" +
                    "<td ><input type='text' class='MarginSPPer' onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;' id='txtMargSPPerc_" + i + "'/></td>" +
                    "<td ><input type='text' class='MarginSP' onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;' id='txtMargSP_" + i + "'/></td>" +
                    "<td id='tdItemName_" + i + "' style='display:none;' > " + objSearchData[i]["Name"] + "</td>" +
                    "</tr>";
                tblSearchData.append(row);
            }

            $("#tblMarginFix").find("input:text").attr("disabled", "disabled");
            //console.log(tblSearchData.children());
            //tblSearchData.children().dblclick(fncSearchRowDoubleClick);
            //tblSearchData.children().click(fncSearchRowClick);
            //fncAssignBackgroundColor();
        }
    }
    catch (err) {
        fncToastError(err.message);
    }

}

//$(function () {
//    $('input').on('input', function () {
//        match = (/(\d{0,2})[^.]*((?:\.\d{0,4})?)/g).exec(this.value.replace(/[^\d.]/g, ''));
//        this.value = match[1] + match[2];
//    });
//});

function isNumberKeyGA(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
        evt.preventDefault();
    } // prevent if already dot

    if (charCode == 13) {
        //event.preventDefault(); 
        //$('#<%=txtNetCostT.ClientID %>').val(196);

        var ValidMarginTD = $(evt.target).attr('id').indexOf("txtMargPerc_");
        var ValidInDecTD = $(evt.target).attr('id').indexOf("txtIDPerc_");
        var ValidMargSPPercTD = $(evt.target).attr('id').indexOf("txtMargSPPerc_");

        Pricetxt = $(evt.target).closest("td").next().find("input");
        Mrptxt = $(evt.target).closest("td").next().next().find("input");
        SellPricetxt = $(evt.target).closest("td").next().next().next().next().find("input");
        Pricetxt.focus().select();
        var netcost = parseFloat($('#<%=txtNetCostT.ClientID %>').val());

                 if (ValidMarginTD != '-1') {
                     var Margin = parseFloat($(evt.target).val());
                     dOrginalSellAmt = Math.ceil((netcost * Margin / 100) + netcost);
                     Mrptxt.val(dOrginalSellAmt.toFixed(2));
                     SellPricetxt.val(dOrginalSellAmt.toFixed(2));
                 }

                 if (ValidInDecTD != '-1') {
                     Pricetxt = $(evt.target).closest("td").next().next().find("input");
                     Mrptxt = $(evt.target).closest("td").next().find("input");
                     SellPricetxt = $(evt.target).closest("td").next().next().next().find("input");
                     var InDecPer = parseFloat($(evt.target).val());

                     if (Mrptxt.val() == '') {
                         $(evt.target).focus().select();
                         return false;
                     }

                     //Mrptxt.val(dOrginalSellAmt);
                     //SellPricetxt.val(dOrginalSellAmt);
                     Pricetxt.focus().select();
                     var Check = false;
                     var Check1 = false;
                     var dNegativecount = 0;

                     $("#tblMarginFix tr").each(function () {
                         var cells = $("td", this);
                         if (cells.length > 0) {
                             objMargin = $(this).find(".Margin");
                             if (objMargin.val() == '') {
                                 if (Check1 == false)
                                     dNegativecount = dNegativecount + 1;
                             }
                             else {
                                 Check1 = true;
                             }
                         }
                     });

                     $("#tblMarginFix tr").each(function () {
                         var cells = $("td", this);
                         if (cells.length > 0) {

                             objMargin = $(this).find(".Margin");
                             objMarginSP = $(this).find(".MarginSP");
                             objMrp = $(this).find(".Mrp");
                             dOrginalSellAmt = Math.ceil((netcost * objMargin.val() / 100) + netcost);

                             if (objMargin.val() == '') {
                                 if (Check == true) {
                                     dOrginalSellAmt = Math.ceil(dOrginalSellAmt + (dOrginalSellAmt * InDecPer / 100))
                                     objMrp.val(dOrginalSellAmt.toFixed(2));
                                     objMarginSP.val(dOrginalSellAmt.toFixed(2));
                                 }
                                 else {
                                     InDecPer1 = InDecPer * dNegativecount;
                                     dOrginalSellAmt = Math.ceil(Mrptxt.val() - (Mrptxt.val() * InDecPer1 / 100))
                                     objMrp.val(dOrginalSellAmt.toFixed(2));
                                     objMarginSP.val(dOrginalSellAmt.toFixed(2));

                                     dNegativecount = dNegativecount - 1;
                                 }
                             }
                             else {
                                 Check = true;
                             }

                             netcost = parseFloat(objMrp.val());
                         }
                     });
                 }

                 if (ValidMargSPPercTD != '-1') {

                     var MarginPerc = parseFloat($(evt.target).val());

                     $("#tblMarginFix tr").each(function () {
                         var cells = $("td", this);
                         if (cells.length > 0) {
                             objMarginSP = $(this).find(".MarginSP");
                             objMarginSPPer = $(this).find(".MarginSPPer");
                             objMrp = $(this).find(".Mrp");

                             dOrginalSellAmt = objMrp.val();
                             dOrginalSellAmt = Math.ceil(dOrginalSellAmt - (dOrginalSellAmt * MarginPerc / 100))
                             objMarginSP.val(dOrginalSellAmt.toFixed(2));
                         }
                     });

                 }

                 return false;

             }
             return true;
         }

         function fncClearMargin() {
             $("#tblMarginFix").find("input:text").val('');
         }

         function handleChange(checkbox) {
             rowid = $(checkbox).closest('tr');
             var selected = new Array();
             if (checkbox.checked == true) {
                 $('#tblMarginFix input[type="checkbox"]:checked').each(function () {
                     selected.push($(this).attr('id'));
                 });
                 console.log(selected.length);
                 if (selected.length == 1 || selected.length == 0) {
                     rowid.find("input").removeAttr("disabled");
                     $("#tblMarginFix").find("input:checkbox").attr("disabled", "disabled");
                     $(checkbox).removeAttr("disabled");
                 }
             }
             else {
                 rowid.find("input:text").attr("disabled", "disabled");
                 $("#tblMarginFix").find("input:checkbox").removeAttr("disabled");
                 $("#tblMarginFix").find("input:text").val('');
             }
         }

         /// Textile Item bind Search table
         function fncTextileItemBindtoTable(msg) {
             try {

                 var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
                 objSearchData = jQuery.parseJSON(msg.d);

                 tblSearchData = $("#tblItemDetail tbody");
                 tblSearchData.children().remove();


                 if (objSearchData.length > 0) {
                     for (var i = 0; i < objSearchData.length; i++) {

                         //if (objSearchData[i]["BatchNo"] == null) {
                         //    batchNo = "";
                         //}
                         //else {
                         //    batchNo = objSearchData[i]["BatchNo"];
                         //} 

                         Rowno = parseFloat(i) + parseFloat(1);
                         row = "<tr id='trDetailRow_" + i + "' tabindex='" + i + "' >" +
                             "<td id='tdRowNo_" + i + "' >" + Rowno + "</td>" +
                             '<td><input type="checkbox" id="cbrow" checked name= "' + Rowno + '" /></td>' +
                             "<td id='tdItemcode_" + i + "' > " + objSearchData[i]["InventoryCode"] + "</td>" +
                             "<td >" + objSearchData[i]["Description"].replace('<', '(').replace('>', ')') + "</td>" + //Sivaraj 28122018
                             "<td >" + objSearchData[i]["UnitCost"].toFixed(2) + "</td>" +
                             "<td >" + objSearchData[i]["GrossCost"].toFixed(2) + "</td>" +
                             "<td >" + objSearchData[i]["NetCost"].toFixed(2) + "</td>" +
                             "<td >" + objSearchData[i]["MRP"].toFixed(2) + "</td>" +
                             "<td  >" + objSearchData[i]["MarginPer"].toFixed(2) + "</td>" +
                             "<td  >" + objSearchData[i]["sellingprice"].toFixed(2) + "</td>" +
                             "<td  >" + objSearchData[i]["Uom"] + "</td>" +
                             "<td  >" + objSearchData[i]["DepartmentCode"] + "</td>" +
                             "<td  >" + objSearchData[i]["CategoryCode"] + "</td>" +
                             "<td >" + objSearchData[i]["BrandCode"] + "</td>" +
                             "<td >" + objSearchData[i]["Class"] + "</td>" +
                             "<td >" + objSearchData[i]["VendorCode"] + "</td>" +
                             "</tr>";
                         tblSearchData.append(row);

                         $('#<%=hidMFAmt.ClientID%>').val(objSearchData[i]["MFAmt"]);
                        $('#<%=hidMarginFixAmt.ClientID%>').val(objSearchData[i]["EarnedMargin"]);
                        $('#<%=hidMFPer.ClientID%>').val(objSearchData[i]["MFPer"]);
                        $('#<%=hidAllowExpireDate.ClientID%>').val(objSearchData[i]["Allowexpiredate"]);
                        $('#<%=hidItemType.ClientID%>').val(objSearchData[i]["ItemType"]);
                        $('#<%=hidPricingType.ClientID%>').val(objSearchData[i]["PricingType"]);
                        $('#<%=hidMarkDownPerc.ClientID%>').val(objSearchData[i]["MarkDownPerc"]);
                        $('#<%=hidAvgCost.ClientID%>').val(objSearchData[i]["AverageCost"]);
                        $('#<%=hidBasicSellingPrice.ClientID%>').val(objSearchData[i]["BasicSelling"]);
                        $('#<%=hidisbatch.ClientID%>').val(objSearchData[i]["Batch"]);
                        //$('#<%=txtGSTPercT.ClientID %>').val(objSearchData[i]["ITaxPer3"]);  
                        $('#<%=hidOldSellingPrice.ClientID%>').val(objSearchData[i]["sellingprice"]);

                            <%--$('#<%=hidMFAmt.ClientID%>').val(objInventory.Table2[0]["MFAmt"]);  // Earned Margin Amount
                            $('#<%=hidItemType.ClientID%>').val(objInventory.Table2[0]["ItemType"]);
                            $('#<%=hidPricingType.ClientID%>').val(objInventory.Table2[0]["PricingType"]);
                            $('#<%=hidPurchasedBy.ClientID%>').val(objInventory.Table2[0]["PurchasedBy"]);
                            $('#<%=hidMarginFixType.ClientID%>').val(objInventory.Table2[0]["MarginFixType"]);
                            $('#<%=hidMarkDownPerc.ClientID%>').val(objInventory.Table2[0]["MarkDownPerc"]);
                            $('#<%=hidAvgCost.ClientID%>').val(objInventory.Table2[0]["AverageCost"]);
                            $('#<%=hidBasicSellingPrice.ClientID%>').val(objInventory.Table2[0]["BasicSelling"]);
                            $('#<%=hidAllowExpireDate.ClientID%>').val(objInventory.Table2[0]["Allowexpiredate"]);
                            $('#<%=txtcess.ClientID%>').val(objInventory.Table2[0]["ITaxPer4"]);
                            $('#<%=txtCessPer.ClientID%>').val(objInventory.Table2[0]["ITaxPer4"]);
                            $('#<%=hidisbatch.ClientID%>').val(objInventory.Table2[0]["Batch"]);
                            $('#<%=txtAddCess.ClientID%>').val(objInventory.Table2[0]["ITaxamt5"]);--%>
                    }

                    //console.log(tblSearchData.children());

                    //tblSearchData.children().dblclick(fncSearchRowDoubleClick);
                    //tblSearchData.children().click(fncSearchRowClick);
                    //fncAssignBackgroundColor();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        /// Item bind Search table
        function fncItemBindtoTable(msg) {
            try {

                var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
                objSearchData = jQuery.parseJSON(msg.d);

                tblSearchData = $("#tblItemSearch tbody");
                tblSearchData.children().remove();


                if (objSearchData.length > 0) {
                    for (var i = 0; i < objSearchData.length; i++) {

                        if (objSearchData[i]["BatchNo"] == null) {
                            batchNo = "";
                        }
                        else {
                            batchNo = objSearchData[i]["BatchNo"];
                        }


                        Rowno = parseFloat(i) + parseFloat(1);
                        row = "<tr id='trSearchRow_" + i + "' tabindex='" + i + "' >" +
                            "<td id='tdRowNo_" + i + "' >" + Rowno + "</td>" +
                            "<td id='tdItemcode_" + i + "' > " + objSearchData[i]["Inventorycode"] + "</td>" +
                            "<td >" + objSearchData[i]["Description"] + "</td>" +
                            "<td >" + objSearchData[i]["Compatible"] + "</td>" +
                             "<td >" + objSearchData[i]["ActQty"].toFixed(2) + "</td>" +
                            "<td >" + objSearchData[i]["PRReqQty"].toFixed(2) + "</td>" +
                        "<td >" + objSearchData[i]["ActMRP"].toFixed(2) + "</td>" +
                        "<td  >" + objSearchData[i]["ActSellingPrice"].toFixed(2) + "</td>" +
                        "<td  >" + objSearchData[i]["Itaxper3"].toFixed(2) + "</td>" +
                        "<td  >" + objSearchData[i]["ActNet"].toFixed(2) + "</td>" +
                        "<td  >" + objSearchData[i]["CashDiscount"].toFixed(2) + "</td>" +
                        "<td  >" + objSearchData[i]["SchemeDiscount"].toFixed(2) + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td  >" + "" + "</td>" +
                        "<td id='tdBatchNo_" + i + "' >" + batchNo + "</td>" +
                        "</tr>";
                        tblSearchData.append(row);
                    }

                    //console.log(tblSearchData.children());

                    tblSearchData.children().dblclick(fncSearchRowDoubleClick);
                    tblSearchData.children().click(fncSearchRowClick);
                    fncAssignBackgroundColor();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        /// to show back ground color on click
        function fncSearchRowClick() {
            try {
                $(this).css("background-color", "#80b3ff");
                $(this).siblings().css("background-color", "white");

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// Assign Background color for Search grid
        function fncAssignBackgroundColor() {

            try {
                $("#tblItemSearch tbody > tr").first().css("background-color", "#80b3ff");
                $("#tblItemSearch tbody > tr").first().focus();



                $("#tblItemSearch tbody > tr").on('keydown', function (evt) {
                    var rowobj = $(this);
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    var scrollheight = 0;

                    if (charCode == 13) {
                        rowobj.dblclick();
                        return false;
                    }
                    else if (charCode == 40) {

                        var NextRowobj = rowobj.next(); //$(this).closest('tr').next('tr'); //rowobj.next();                        
                        if (NextRowobj.length > 0) {
                            NextRowobj.css("background-color", "#80b3ff");
                            NextRowobj.siblings().css("background-color", "white");
                            NextRowobj.select().focus();

                            setTimeout(function () {
                                scrollheight = $("#tblItemSearch tbody").scrollTop();
                                if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                    $("#tblItemSearch tbody").scrollTop(0);
                                }
                                else if (parseFloat(scrollheight) > 10) {
                                    scrollheight = parseFloat(scrollheight) - 10;
                                    $("#tblItemSearch tbody").scrollTop(scrollheight);
                                }
                            }, 50);


                        }

                    }
                    else if (charCode == 38) {
                        var prevrowobj = rowobj.prev();
                        if (prevrowobj.length > 0) {
                            prevrowobj.css("background-color", "#80b3ff");
                            prevrowobj.siblings().css("background-color", "white");
                            prevrowobj.select().focus();

                            setTimeout(function () {
                                scrollheight = $("#tblItemSearch tbody").scrollTop();
                                if (parseFloat(scrollheight) > 3) {
                                    scrollheight = parseFloat(scrollheight) + 1;
                                    $("#tblItemSearch tbody").scrollTop(scrollheight);
                                }
                            }, 50);


                        }

                    }

                });
            }
            catch (err) {
                fncToastError(err.message);
            }

        }



        ///Get GID Hold Record
        function SetGIDHoldRecord() {
            $("[id$=txtGidHold]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncGetGidHoldRecord")%>',
                data: "{ 'search': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            holdGidNo: item.split('|')[1],
                            invoiceNo: item.split('|')[2],
                            vendorCode: item.split('|')[3]
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
        select: function (e, i) {
            $('#<%=hidGidHoldNo.ClientID %>').val($.trim(i.item.holdGidNo));
            $('#<%=hidInvoiceNo.ClientID %>').val($.trim(i.item.invoiceNo));
            $('#<%=hidVendorCode.ClientID %>').val($.trim(i.item.vendorCode));
            $("#divGidHold").dialog('close');
            $('#<%=btnShowGidHold.ClientID %>').click();

            return false;
        },
        minLength: 1
    });
}

///Inventory Search For Foc
function SetAutoCompleteFoc() {
    $("[id$=txtFocItem]").autocomplete({
        source: function (request, response) {
            var obj = {};
            obj.searchData = request.term;
            obj.franchisevalue = $('#<%=hidFranValue.ClientID %>').val();
            obj.mode = "3";
            $.ajax({
                url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncFocItemSearch")%>',
                //data: "{ 'prefix': '" + request.term + "'}",
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            valitemcode: item.split('|')[1],
                            valName: item.split('|')[2]
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
        select: function (e, i) {
            $('#<%=txtFocItem.ClientID %>').val($.trim(i.item.valitemcode) + "-" + $.trim(i.item.valName));
            fncGetFOCBatch();
            return false;
        },
        minLength: 1
    });
    }

    //Get Inventory code
    function fncGetInventoryCode_onEnter(event) {
        try {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            var Itemcode, vendorcode;
            if (keyCode == 13) {

                Itemcode = $('#<%=txtItemCode.ClientID %>').val().trim();

                if ($('#<%=cbVendorItem.ClientID %>').is(":checked"))
                    vendorcode = $('#<%=txtVendorCode.ClientID %>').val().trim();
                else
                    vendorcode = "";

                fncGetInventoryDetailForGID(Itemcode, "", vendorcode);

                event.preventDefault();
                event.stopPropagation();
                return false;
            }
            else if (keyCode == 116 && $('#<%=hidCurLocation.ClientID%>').val() == "HQ") {
                fncOpenItem();
            }
            else if (keyCode == 35) {
                fncItemSearch();
                $("#divGRN").css("display", "none");
                $("#divItemSearch").css("display", "block");
            }
            else if (keyCode == 112) {
                fncItemSearchTextiles();
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    var poItem = "N", poNo = "0";


    ///Assign Value to Text Box
        var FootWearVal;
    function fncGetInventoryDetailForGID(Itemcode, batchNo, vendorcode) {
        try {
            var obj = {}, objInventory;
            obj.Itemcode = Itemcode;
            obj.batchNo = batchNo;
            obj.vendorcode = vendorcode;
            obj.franchise = $('#<%=hidFranValue.ClientID%>').val();
            obj.gidLocation = $('#<%=hidGidLocationcode.ClientID%>').val();

            if ($('#<%=hidScanedWithPO.ClientID%>').val() == "Y") {
                obj.gaNo = $('#<%=txtgaNo.ClientID%>').val()
            }
            else {
                obj.gaNo = "";
            }


            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncGetInventoryDetailForGidSearch")%>',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {

                            objInventory = jQuery.parseJSON(msg.d);

                            if (objInventory.Table.length > 0) {
                                if (objInventory.Table[0]["InventoryStatus"] == "0") {
                                    fncSubClear('clr');
                                    //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
                                    $('#<%=txtItemCode.ClientID%>').select();
                                    fncToastInformation('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                                    return;
                                }
                            }
                            if (objInventory.Table1.length > 0) {
                                if (objInventory.Table1[0]["Vendorstatus"] == "0") {
                                    fncSubClear('clr');
                                    $('#<%=txtItemCode.ClientID%>').select();
                                    fncToastInformation('<%=Resources.LabelCaption.alert_DifferenVendor%>');
                                    return;
                                }
                            }

                            if (objInventory.Table2.length > 0) {

                                $('#<%=txtItemCode.ClientID%>').val(objInventory.Table2[0]["inventorycode"].trim());
                                $('#<%=txtDescription.ClientID%>').val(objInventory.Table2[0]["Description"]);
                                $('#<%=txtMRP.ClientID%>').val(objInventory.Table2[0]["MRP"]);
                                $('#<%=txtBCost.ClientID%>').val(objInventory.Table2[0]["UnitCost"]);


                                /// basic cost enable and disable
                                $('#<%=hidCostEditAllow.ClientID%>').val(objInventory.Table2[0]["CostEdit"]);
                                if (objInventory.Table2[0]["CostEdit"] == "0" || objInventory.Table2[0]["PurchasedBy"].trim() == "M") {
                                    $('#<%=txtBCost.ClientID%>').attr("disabled", "disabled");
                                    $('#<%=txtDiscPer.ClientID%>').attr("disabled", "disabled");
                                    $('#<%=txtDiscAmt.ClientID%>').attr("disabled", "disabled");
                                    $('#<%=txtSDPer.ClientID%>').attr("disabled", "disabled");
                                    $('#<%=txtSchemeAmt.ClientID%>').attr("disabled", "disabled");
                                }
                                else {
                                    $('#<%=txtBCost.ClientID%>').removeAttr("disabled");
                                    $('#<%=txtDiscPer.ClientID%>').removeAttr("disabled");
                                    $('#<%=txtDiscAmt.ClientID%>').removeAttr("disabled");
                                    $('#<%=txtSDPer.ClientID%>').removeAttr("disabled");
                                    $('#<%=txtSchemeAmt.ClientID%>').removeAttr("disabled");
                                }


                                if ($('#<%=hidVendorStatus.ClientID %>').val() == "1") {

                                    $('#<%=txtITax1.ClientID%>').val(objInventory.Table2[0]["ITaxCode1"]);
                            $('#<%=txtITax2.ClientID%>').val(objInventory.Table2[0]["ITaxCode2"]);

                            $('#<%=txtITaxper1.ClientID%>').val(objInventory.Table2[0]["ITaxPer1"]);
                            $('#<%=txtITaxper2.ClientID%>').val(objInventory.Table2[0]["ITaxPer2"]);
                        }
                        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "4") {
                            $('#<%=txtITax1.ClientID%>').val("NoTax");
                                    $('#<%=txtITax2.ClientID%>').val("NoTax");

                                    $('#<%=txtITaxper1.ClientID%>').val("0.00");
                                    $('#<%=txtITaxper2.ClientID%>').val("0.00");
                                }
                                else {
                                    $('#<%=txtITax1.ClientID%>').val(objInventory.Table2[0]["ITaxCode3"]);
                                    $('#<%=txtITaxper1.ClientID%>').val(objInventory.Table2[0]["ITaxPer3"]);
                                }
                            $('#<%=txtGST.ClientID%>').val(objInventory.Table2[0]["ITaxCode3"]);
                                $('#<%=ddlGST.ClientID%>').val(parseFloat(objInventory.Table2[0]["ITaxCode3"]).toFixed(2));
                                $('#<%=ddlGST.ClientID%>').trigger("liszt:updated");

                                $('#<%=hidAddCessAmt.ClientID%>').val(objInventory.Table2[0]["AdditionalCess"]);

                                $('#<%=txtSPrice.ClientID%>').val(objInventory.Table2[0]["SellingPrice"]);
                                $('#<%=txtWpirce1.ClientID%>').val(objInventory.Table2[0]["WPrice1"]);
                                $('#<%=txtWpirce2.ClientID%>').val(objInventory.Table2[0]["WPrice2"]);
                                $('#<%=txtWpirce3.ClientID%>').val(objInventory.Table2[0]["WPrice3"]);

                                $('#<%=txtluom.ClientID%>').val(objInventory.Table2[0]["UOM"]);
                                $('#<%=txtwuom.ClientID%>').val(objInventory.Table2[0]["Compatible"]);
                                $('#<%=txtMFixed.ClientID%>').val(objInventory.Table2[0]["EarnedMargin"]); //Margin Fixed Amount
                                $('#<%=txtEarned.ClientID%>').val(objInventory.Table2[0]["MFPer"]);  // Earned Margin Per
                                $('#<%=hidMFAmt.ClientID%>').val(objInventory.Table2[0]["MFAmt"]);  // Earned Margin Amount

                                $('#<%=hidItemType.ClientID%>').val(objInventory.Table2[0]["ItemType"]);

                                $('#<%=hidPricingType.ClientID%>').val(objInventory.Table2[0]["PricingType"]);
                                $('#<%=hidPurchasedBy.ClientID%>').val(objInventory.Table2[0]["PurchasedBy"]);
                                $('#<%=hidMarginFixType.ClientID%>').val(objInventory.Table2[0]["MarginFixType"]);

                                $('#<%=hidMarkDownPerc.ClientID%>').val(fncConvertFloatJS(objInventory.Table2[0]["MarkDownPerc"]));
                                $('#<%=hidAvgCost.ClientID%>').val(objInventory.Table2[0]["AverageCost"]);
                                $('#<%=hidBasicSellingPrice.ClientID%>').val(objInventory.Table2[0]["BasicSelling"]);



                                $('#<%=hidAllowExpireDate.ClientID%>').val(objInventory.Table2[0]["Allowexpiredate"]);
                                $('#<%=txtcess.ClientID%>').val(objInventory.Table2[0]["ITaxPer4"]);
                                $('#<%=txtCessPer.ClientID%>').val(objInventory.Table2[0]["ITaxPer4"]);
                                $('#<%=hidisbatch.ClientID%>').val(objInventory.Table2[0]["Batch"]);
                                $('#<%=txtAddCess.ClientID%>').val(objInventory.Table2[0]["ITaxamt5"]);
                                $('#<%=txtDiscPer.ClientID%>').val(objInventory.Table2[0]["DiscountBasicPer"]);
                                $('#<%=txtDiscAmt.ClientID%>').val(objInventory.Table2[0]["DiscountBasicAmt"]);



                                $('#<%=txtSDPer.ClientID%>').val('0.00');
                                $('#<%=txtSchemeAmt.ClientID%>').val('0.00');


                                $('#<%=hidoldMarFixed.ClientID%>').val(objInventory.Table2[0]["EarnedMargin"]);
                                $('#<%=hidoldMarEarned.ClientID%>').val(objInventory.Table2[0]["MFPer"]);
                                $('#<%=hidoldvendorMargin.ClientID%>').val('0.00');




                            }

                            if (objInventory.Table3.length > 0) {
                                $('#<%=hidBreakpricestatus.ClientID%>').val(objInventory.Table3[0]["breakpricestatus"]);
                            }
                            if (objInventory.Table4.length > 0) {
                                $('#<%=hidBarcodeStatus.ClientID%>').val(objInventory.Table4[0]["BarcodeStatus"]);
                            }

                            if (objInventory.Table5.length > 0) {
                                $('#<%=hidUOMConv.ClientID%>').val(objInventory.Table5[0]["UomConv"]);
                            }
                            if (objInventory.Table6.length > 0) {
                                $('#<%=hidPrvMRP.ClientID%>').val(objInventory.Table6[0]["mrp"]);
                                $('#<%=hidPrvUnitCost.ClientID%>').val(objInventory.Table6[0]["UnitCost"]);
                                $('#<%=hidOldCost.ClientID%>').val(objInventory.Table2[0]["NetCost"]);
                                $('#<%=hidOldMRP.ClientID%>').val(objInventory.Table2[0]["MRP"]);
                                $('#<%=hidOldSellingPrice.ClientID%>').val(objInventory.Table2[0]["SellingPrice"]);
                            }
                            else {
                                $('#<%=hidPrvMRP.ClientID%>').val("0.00");
                                $('#<%=hidPrvUnitCost.ClientID%>').val("0.00");
                                $('#<%=hidOldCost.ClientID%>').val("0.00");
                                $('#<%=hidOldMRP.ClientID%>').val("0.00");
                                $('#<%=hidOldSellingPrice.ClientID%>').val("0.00");
                            }

                            if (objInventory.Table7.length > 0)
                            {
                                var deActiveMRP = [];
                                for (var i = 0; i < objInventory.Table7.length; i++) {
                                    deActiveMRP.push(objInventory.Table7[i]["MRP"]);
                                    $('#<%=hidDeActiveMRP.ClientID%>').val(deActiveMRP);
                                }
                            }
                            FootWearVal = '';
                            if (objInventory.Table13 != undefined) {

                                if (objInventory.Table13.length > 0) {
                                    var FootwearMrp = [];
                                    for (var i = 0; i < objInventory.Table13.length; i++) {
                                        FootwearMrp.push(objInventory.Table13[i]["FootWear"]);
                                        FootWearVal = FootwearMrp;
                                    }
                                    console.log(FootWearVal);
                                }  
                            }
                           
                            fncTextDecimalRestrictionwithUOM();
                            fncItemcodeboxDisable(true);



                            if ($('#<%=txtwuom.ClientID%>').val().trim() != "") {
                                $('#<%=txtWQty.ClientID%>').select();
                                $('#<%=txtWQty.ClientID%>').removeAttr("readonly");
                            }
                            else if ($('#<%=txtwuom.ClientID%>').val().trim() == "") {
                                // Style Code focus
                                $('#<%=txtESerialNo.ClientID%>').select();
                                $('#<%=txtWQty.ClientID%>').attr("readonly", "readonly");
                            }


                            if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
                                fncBindMultipleUOM(objInventory.Table8);
                            }

                            if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y" && $("select[id$=ddlMultipleUOM] > option").length > 0) {
                                fncOpenDropdownlist($('#<%=ddlMultipleUOM.ClientID %>'));
                                $('#<%=txtWQty.ClientID%>').removeAttr("readonly");
                            }

                            if ($('#<%=hidScanedWithPO.ClientID%>').val() == "Y" && objInventory.Table9.length == 0) {
                                poItem = "N";
                                fncShowSaveConfirmation_master("This Item Not Associated Wiht This PO.  Do You Want to Add?");
                            }
                            else if ($('#<%=hidScanedWithPO.ClientID%>').val() == "Y" && objInventory.Table9.length > 0) {
                                poItem = "Y";
                                $('#<%=txtInvQty.ClientID%>').val(objInventory.Table9[0]["POQty"]);
                                $('#<%=txtLQty.ClientID%>').val(objInventory.Table9[0]["POQty"]);
                                poNo = objInventory.Table9[0]["PoNo"];
                                fncInvQtyFocusOut();
                                fncAddValidation();
                            }
                             



                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncYesClick_master() {
            var totalQty = 0, rowObj;
            try {

                cmDialog_Master = "Close";
                if (AddMode == "AlreadyExists") {
                    rowObj = glRowObj
                    $("#saveConfirmation_master").dialog("destroy");
                    totalQty = parseFloat($('#<%=txtInvQty.ClientID%>').val()) + parseFloat(rowObj.find('td input[id*="txtInvQty"]').val());
                    rowObj.find('td input[id*="txtInvQty"]').val(totalQty);
                    totalQty = parseFloat($('#<%=txtLQty.ClientID%>').val()) + parseFloat(rowObj.find('td input[id*="txtlQty"]').val());
            rowObj.find('td input[id*="txtlQty"]').val(totalQty);
            fncRepeaterColumnValueChange(rowObj, 'InvQty', 'LoadQty');
            fncSubClear('clr');
                    //fncItemcodeboxDisable(false);
                    //fncAddEnterItems();
        }
        else {
            $('#<%=txtInvQty.ClientID%>').select();
                    $("#saveConfirmation_master").dialog("destroy");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncAfterNoClick_Master() {
            try {
                fncSubClear('clr');
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        // Itemcode box disable and Enable
        function fncItemcodeboxDisable(value) {
            try {
                $('#<%=txtItemCode.ClientID%>').prop('disabled', value);
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Clear  Search Filter
function fncSubClearFilter() {
    try {

        $('#<%=txtDepartment.ClientID%>').val('');
                $('#<%=txtCategory.ClientID%>').val('');
                $('#<%=txtClass.ClientID%>').val('');
                $('#<%=txtBrand.ClientID%>').val('');
                $('#<%=txtItemSearchT.ClientID%>').val('');
                $('#<%=txtVendor.ClientID%>').val('');
                $('#<%=txtVendor.ClientID%>').focus();

                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Clear  Search Filter
        function fncSubClearTextBox() {
            try {

                $('#<%=txtSellingPriceT.ClientID%>').val('');
                $('#<%=txtVSerialNo.ClientID%>').val('');
                $('#<%=txtStyleCode.ClientID%>').val('');
                $('#<%=txtQtyT.ClientID%>').val('');
                $('#<%=txtOTaxAmt.ClientID%>').val('');
                $('#<%=txtOTaxPerc.ClientID%>').val('0');
                $('#<%=txtCostPriceT.ClientID%>').val('');

                $('#<%=txtDiscountPercT.ClientID%>').val('0');
                $('#<%=txtDiscountAmtT.ClientID%>').val('');
                $('#<%=txtSDPercT.ClientID%>').val('0');
                $('#<%=txtSDAmtT.ClientID%>').val('');
                $('#<%=txtGrossCostT.ClientID%>').val('');
                $('#<%=txtGSTPercT.ClientID%>').val('0');
                $('#<%=txtGSTAmtT.ClientID%>').val('');

                $('#<%=txtNetCostT.ClientID%>').val('');
                $('#<%=txtMrpT.ClientID%>').val('0.00');
                $('#<%=txtMarginT.ClientID%>').val('0.00');
                $('#<%=txtMarginSP.ClientID%>').val('0.00');

                $('#<%=txtVSerialNo.ClientID%>').focus();
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        ///Clear Single Inventory Detail
        function fncSubClear(btn) {
            try {
                fncEnablePriceChangeAlert("Disable", "");
                fncItemcodeboxDisable(false);
                $('#<%=txtInvQty.ClientID%>').val('0');
        $('#<%=txtWQty.ClientID%>').val('0');
        $('#<%=txtItemCode.ClientID%>').val('');
        $('#<%=txtDescription.ClientID%>').val('');
        $('#<%=txtLQty.ClientID%>').val('0');
        $('#<%=txtMRP.ClientID%>').val('0');
        $('#<%=txtBCost.ClientID%>').val('0');
        $('#<%=txtDiscPer.ClientID%>').val('');
        $('#<%=txtDiscAmt.ClientID%>').val('0');
        $('#<%=txtSDPer.ClientID%>').val('0');
        $('#<%=txtSchemeAmt.ClientID%>').val('0');
        $('#<%=txtGrossCost.ClientID%>').val('0');

        $('#<%=txtITax1.ClientID%>').val('0');
        $('#<%=txtITax2.ClientID%>').val('0');
        $('#<%=txtcess.ClientID%>').val('0');

        $('#<%=txtNetCost.ClientID%>').val('0');
        $('#<%=txtSPrice.ClientID%>').val('0');

        $('#<%=txtMFixed.ClientID%>').val('0');
        $('#<%=txtEarned.ClientID%>').val('0');
        $('#<%=txtwuom.ClientID%>').val('');
        $('#<%=txtluom.ClientID%>').val('');
        $('#<%=txtTotQty.ClientID%>').val('0');

        $('#<%=txtITaxper1.ClientID%>').val('0');
        $('#<%=txtITaxper2.ClientID%>').val('0');
        $('#<%=txtCessPer.ClientID%>').val('0');

        $('#<%=txtTotalGBCost.ClientID%>').val('0');
        $('#<%=txtGRDisc.ClientID%>').val('0');
        $('#<%=txtGRShDiscAmt.ClientID%>').val('0');
        $('#<%=txtGRVat.ClientID%>').val('0');
        $('#<%=txtGRCessAmt.ClientID%>').val('0');
        $('#<%=txtTotal.ClientID%>').val('0');
        $('#<%=txtWpirce1.ClientID%>').val('0');
        $('#<%=txtWpirce2.ClientID%>').val('0');
        $('#<%=txtWpirce3.ClientID%>').val('0');
        $('#<%=txtTotalGCost.ClientID%>').val('0');
        $('#<%=txtAddCess.ClientID%>').val('0');
        $('#<%=txtGRAddCess.ClientID%>').val('0');
        $('#<%=hidFocItem.ClientID%>').val('');
        $('#<%=hidFocBatch.ClientID%>').val('');
        $('#<%=hidFocQty.ClientID%>').val('0');
        $('#<%=hidFTCItemcode.ClientID %>').val('');
        $('#<%=hidFTCBatchNo.ClientID %>').val('');
        $('#<%=hidFTCInputQty.ClientID %>').val('0');
        $('#<%=txtGST%>').val('0');
        $('#<%=lnkAdd.ClientID%>').css("display", "block");
        $("select[id$=ddlMultipleUOM] > option").remove();
        $('#<%=ddlMultipleUOM.ClientID %>').trigger("liszt:updated");
        $('#<%=ddlGST.ClientID%>').val("0.00");
        $('#<%=ddlGST.ClientID%>').trigger("liszt:updated");
        $('#<%=txtESerialNo.ClientID%>').val('');
        $('#<%=txtStyleCodeT.ClientID%>').val('');

        //txtTotalGCost

        if (btn == "clr" || btn == "row")
            $('#<%=txtItemCode.ClientID%>').select().focus();

        return false;
    }
    catch (err) {
        fncToastError(err.message);
    }
}
////Calculate 
function fncInvQtyFocusOut() {
    try {

        if ($('#<%=txtItemCode.ClientID%>').val().trim() == "" || parseInt($('#<%=txtItemCode.ClientID%>').val()) == 0)
            return;

        var invQty, basicCost, discAmt, shDiscAmt, GrossCost, NetCost, iTaxper1, iTaxper2, ActualGST;
        var SGSTAmt, CGSTAmt, IGstAmt, cessper, cessAmt, AddCessAmt = 0;

        if ($('#<%=txtInvQty.ClientID%>').val() != "") {
            invQty = parseFloat($('#<%=txtInvQty.ClientID%>').val()).toFixed(2);
        }



        basicCost = $('#<%=txtBCost.ClientID%>').val();
        discAmt = $('#<%=txtDiscAmt.ClientID%>').val();
        shDiscAmt = $('#<%=txtSchemeAmt.ClientID%>').val();

        $('#<%=txtInvQty.ClientID%>').val(invQty);
        $('#<%=txtLQty.ClientID%>').val(invQty);

        ///Gross Cost Calculation
        GrossCost = parseFloat(basicCost) - parseFloat(discAmt) - parseFloat(shDiscAmt);
        $('#<%=txtGrossCost.ClientID%>').val(GrossCost.toFixed(4));

        iTaxper1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val()); //!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0";
        iTaxper2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val());//!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0";//$('#<%=txtITaxper2.ClientID%>').val();
        cessper = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();

        ActualGST = fncTaxAmtCalculation(GrossCost, iTaxper1, iTaxper2);
        cessAmt = fncCessCalc(GrossCost, cessper);
        NetCost = parseFloat(GrossCost) + parseFloat(ActualGST) + parseFloat(cessAmt);
        AddCessAmt = $('#<%=txtAddCess.ClientID%>').val();
        NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
        $('#<%=txtNetCost.ClientID%>').val(NetCost.toFixed(4));

        fncSubTotalCalculation(basicCost, GrossCost, discAmt, shDiscAmt, ActualGST, NetCost, cessAmt, AddCessAmt)
    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Gst Calculation
function fncTaxAmtCalculation(GrossCost, iTaxper1, iTaxper2) {
    try {
       
        var ActualGST, itaxper;
        ///GST Calculation
        if ($('#<%=hidVendorStatus.ClientID %>').val() == "1") {

            ///Intra State Vendor
            itaxper = parseFloat(iTaxper1) + parseFloat(iTaxper2);
            if ($('#<%=hidVendorGSTNo.ClientID %>').val() != "") {
                ActualGST = (parseFloat(GrossCost) * parseFloat(itaxper)) / 100;

                $('#<%=hidITaxAmt1.ClientID%>').val((parseFloat(ActualGST) / 2).toFixed(4));
                        $('#<%=hidITaxAmt2.ClientID%>').val((parseFloat(ActualGST) / 2).toFixed(4));


            }
            else {
                ActualGST = (parseFloat(GrossCost) * parseFloat(itaxper)) / (parseFloat(itaxper) + 100);
                $('#<%=hidITaxAmt1.ClientID%>').val((parseFloat(ActualGST) / 2).toFixed(4));
                        $('#<%=hidITaxAmt2.ClientID%>').val((parseFloat(ActualGST) / 2).toFixed(4));
                $('#<%=hidGidGSTFlag.ClientID%>').val('5');
            }

        }
        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "2") {
            //Inter State Vendor
            if ($('#<%=hidVendorGSTNo.ClientID %>').val() != "") {
                        ActualGST = ((parseFloat(GrossCost) * parseFloat(iTaxper1)) / 100).toFixed(4);

                        $('#<%=hidITaxAmt1.ClientID%>').val(ActualGST);
                        $('#<%=hidITaxAmt2.ClientID%>').val("0.00");
            }
            else {
                ActualGST = ((parseFloat(GrossCost) * parseFloat(iTaxper1)) / (parseFloat(iTaxper1) + 100)).toFixed(4);

                $('#<%=hidITaxAmt1.ClientID%>').val(ActualGST);
                        $('#<%=hidITaxAmt2.ClientID%>').val("0.00");
                $('#<%=hidGidGSTFlag.ClientID%>').val('6');
            }
        }
        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "3") {
                    //Import State Vemdor
                    ActualGST = ((parseFloat(GrossCost) * parseFloat(iTaxper1)) / (parseFloat(iTaxper1) + 100));

                    $('#<%=hidITaxAmt1.ClientID%>').val(ActualGST.toFixed(4));
                    $('#<%=hidITaxAmt2.ClientID%>').val("0.0000");
                }
                else if ($('#<%=hidVendorStatus.ClientID %>').val() == "4") {
                    ActualGST = "0.0000";
                    $('#<%=hidITaxAmt1.ClientID%>').val("0.0000");
                    $('#<%=hidITaxAmt2.ClientID%>').val("0.0000");
                }
    return ActualGST;
}
    catch (err) {
        fncToastError(err.message);
    }
}

///Cess Amount Calculation
function fncCessCalc(GrossCost, cessper) {
    try {
        var cessAmt;



        ///GST Calculation
        if ($('#<%=hidVendorStatus.ClientID %>').val() == "1" || $('#<%=hidVendorStatus.ClientID %>').val() == "2") {

            ///Intra State Vendor

            if ($('#<%=hidVendorGSTNo.ClientID %>').val() != "") {
                cessAmt = (parseFloat(GrossCost) * parseFloat(cessper)) / 100;
                $('#<%=hidCessAmt.ClientID%>').val(parseFloat(cessAmt).toFixed(4));
            }
            else {
                cessAmt = (parseFloat(GrossCost) * parseFloat(cessper)) / (parseFloat(cessper) + 100);
                $('#<%=hidCessAmt.ClientID%>').val(parseFloat(cessAmt).toFixed(4));

            }

        }
        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "3") {
            //Import State Vemdor
            cessAmt = (parseFloat(GrossCost) * parseFloat(cessper)) / (parseFloat(cessper) + 100);
            $('#<%=hidCessAmt.ClientID%>').val(parseFloat(cessAmt).toFixed(4));
        }
        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "4") {
            cessAmt = "0.0000";
        }

    return cessAmt;

}
    catch (err) {
        fncToastError(err.message);
    }
}

///SubTotal Calculation
function fncSubTotalCalculation(basicCost, GrossCost, discAmt, shDiscAmt, ActualGST, NetCost, CessAmt, AddCessAmt) {
    try {
        var invQty;
        invQty = $('#<%=txtInvQty.ClientID%>').val();

        $('#<%=txtTotalGBCost.ClientID%>').val((parseFloat(invQty) * parseFloat(basicCost)).toFixed(4));
        $('#<%=txtTotalGCost.ClientID%>').val((parseFloat(invQty) * parseFloat(GrossCost)).toFixed(4));
        $('#<%=txtGRDisc.ClientID%>').val((parseFloat(invQty) * parseFloat(discAmt)).toFixed(4));
        $('#<%=txtGRShDiscAmt.ClientID%>').val((parseFloat(invQty) * parseFloat(shDiscAmt)).toFixed(4));
        $('#<%=txtGRVat.ClientID%>').val((parseFloat(invQty) * parseFloat(ActualGST)).toFixed(4));
        $('#<%=txtGRCessAmt.ClientID%>').val((parseFloat(invQty) * parseFloat(CessAmt)).toFixed(4));
        $('#<%=txtTotal.ClientID%>').val((parseFloat(invQty) * parseFloat(NetCost)).toFixed(4));
        $('#<%=txtGRAddCess.ClientID%>').val((parseFloat(invQty) * parseFloat(AddCessAmt)).toFixed(4));


        $('#<%=txtTotQty.ClientID%>').val(invQty);
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Discount and ShemeDiscount Calculation
function fncDiscountandShemeDiscCalc(code) {
    try {


        var basicCost, discPer, discAmt = 0, shDiscAmt = 0, invQty = 0, grossCost;
        var ActualGST, iTaxper1, iTaxper2, cessper, netCost, addCessAmt = 0;
        invQty = $('#<%=txtInvQty.ClientID%>').val();
        basicCost = $('#<%=txtBCost.ClientID%>').val();
        addCessAmt = $('#<%=txtAddCess.ClientID%>').val();

        iTaxper1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val());//!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0"; //$('#<%=txtITaxper1.ClientID%>').val();
        iTaxper2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val());//!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0";//$('#<%=txtITaxper2.ClientID%>').val();
        cessper = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();
        if (code == "Discper") {
            if ($('#<%=txtDiscPer.ClientID%>').val() == "")
                return;

            discPer = $('#<%=txtDiscPer.ClientID%>').val();
            discAmt = (parseFloat(basicCost) * parseFloat(discPer)) / 100;
            $('#<%=txtDiscAmt.ClientID%>').val(parseFloat(discAmt).toFixed(4));

           <%-- if (parseFloat(discPer) > 0)
                $('#<%=txtSDPer.ClientID%>').select();--%>

        }
        else if (code == "DiscAmt") {
            if ($('#<%=txtDiscAmt.ClientID%>').val() == "")
                return;


            discAmt = $('#<%=txtDiscAmt.ClientID%>').val();
            discAmt = parseFloat(discAmt) / parseFloat(invQty);
            $('#<%=txtDiscAmt.ClientID%>').val(discAmt);
            discPer = (parseFloat(discAmt) / parseFloat(basicCost)) * 100;
            $('#<%=txtDiscPer.ClientID%>').val(parseFloat(discPer).toFixed(2));

        }

    if (code == "ShDiscper" || code == "Discper") {
        if ($('#<%=txtSDPer.ClientID%>').val() == "")
            return;



        discAmt = $('#<%=txtDiscAmt.ClientID%>').val();
        if ($('#<%=cbSDOnGrossCost.ClientID%>').is(":checked")) {
            basicCost = parseFloat(basicCost) - parseFloat(discAmt);
        }


        discPer = $('#<%=txtSDPer.ClientID%>').val();
        shDiscAmt = (parseFloat(basicCost) * parseFloat(discPer)) / 100;
        $('#<%=txtSchemeAmt.ClientID%>').val(parseFloat(shDiscAmt).toFixed(2));
    }
    else if (code == "ShAmt" || code == "DiscAmt") {

        if ($('#<%=txtSchemeAmt.ClientID%>').val() == "")
            return;

        shDiscAmt = $('#<%=txtSchemeAmt.ClientID%>').val();
        shDiscAmt = parseFloat(shDiscAmt) / parseFloat(invQty);
        $('#<%=txtSchemeAmt.ClientID%>').val(shDiscAmt);


        discAmt = $('#<%=txtDiscAmt.ClientID%>').val();


        if ($('#<%=cbSDOnGrossCost.ClientID%>').is(":checked")) {
            basicCost = parseFloat(basicCost) - parseFloat(discAmt);
        }

        shDiscAmt = $('#<%=txtSchemeAmt.ClientID%>').val();
        discPer = (parseFloat(shDiscAmt) / parseFloat(basicCost)) * 100;
        $('#<%=txtSDPer.ClientID%>').val(parseFloat(discPer).toFixed(2));

           <%-- setTimeout(function () {
                $('#<%=txtSPrice.ClientID%>').select();
            }, 50);--%>

    }

        //Gross Cost Calculation
        basicCost = $('#<%=txtBCost.ClientID%>').val();
        $('#<%=txtGrossCost.ClientID%>').val((parseFloat(basicCost).toFixed(4) - parseFloat(discAmt).toFixed(4) - parseFloat(shDiscAmt).toFixed(4)));
        grossCost = $('#<%=txtGrossCost.ClientID%>').val();
        //Gross Discount Calculation
        $('#<%=txtGRDisc.ClientID%>').val((parseFloat(discAmt) * parseFloat(invQty)).toFixed(4));
        //Scheme Discount Calculation
        $('#<%=txtGRShDiscAmt.ClientID%>').val((parseFloat(shDiscAmt) * parseFloat(invQty)).toFixed(4));
        //Total Gross Cost
        $('#<%=txtTotalGCost.ClientID%>').val((parseFloat(grossCost) * parseFloat(invQty)).toFixed(4));

        ///GST Calculation

        ActualGST = fncTaxAmtCalculation(grossCost, iTaxper1, iTaxper2);
        cessAmt = fncCessCalc(grossCost, cessper);

        netCost = (parseFloat(grossCost) + parseFloat(ActualGST) + parseFloat(cessAmt) + parseFloat(addCessAmt)).toFixed(4);
        $('#<%=txtNetCost.ClientID%>').val(netCost);

        fncSubTotalCalculation(basicCost, grossCost, discAmt, shDiscAmt, ActualGST, netCost, cessAmt, addCessAmt);

    }
    catch (err) {
        fncToastError(err.message);
    }
}


var AddMode = "", glRowObj;
///Add Validation
function fncAddValidation() {
    var status = true, medBatch = "0";
    var totalQty = 0;
    try {

        $('#<%=lnkAdd.ClientID%>').css("display", "none");
        $("#tblGRN [id*=GRNBodyrow]").each(function () {
            rowObj = $(this);
            glRowObj = rowObj;

            if ($('#<%=txtMBatchNo.ClientID%>').val().trim() == "")
                medBatch = "0";
            else
                medBatch = $('#<%=txtMBatchNo.ClientID%>').val().trim();
              if (rowObj.find('td input[id*="txtItemcode"]').val().trim() == $('#<%=txtItemCode.ClientID%>').val().trim() &&
                parseFloat(rowObj.find('td input[id*="txtStyleCode"]').val()) == parseFloat($('#<%=txtStyleCodeT.ClientID%>').val()))
              {
                  ShowPopupMessageBox("This Item Already Exists" +' - '+ $('#<%=txtItemCode.ClientID%>').val().trim());
                  status = false;
                  fncSubClear('clr');
                  return false;
              }
           else  if (rowObj.find('td input[id*="txtItemcode"]').val().trim() == $('#<%=txtItemCode.ClientID%>').val().trim() &&
                parseFloat(rowObj.find('td input[id*="txtMRP"]').val()) == parseFloat($('#<%=txtMRP.ClientID%>').val()) &&
                rowObj.find('td input[id*="txtMedBatch"]').val().trim() == medBatch
                && parseFloat(rowObj.find('td input[id*="txtBasicCost"]').val()) == parseFloat($('#<%=txtBCost.ClientID%>').val())
                && parseFloat(rowObj.find('td input[id*="txtSerialNo"]').val()) == parseFloat($('#<%=txtESerialNo.ClientID%>').val())
                && parseFloat(rowObj.find('td input[id*="txtStyleCode"]').val()) == parseFloat($('#<%=txtStyleCodeT.ClientID%>').val())) {
                {
                    if (parseFloat(rowObj.find('td input[id*="txtPONo"]').val()) != 0 && $('#<%=hidGidExistVal.ClientID%>').val() == "Y") {
                        totalQty = parseFloat($('#<%=txtInvQty.ClientID%>').val()) + parseFloat(rowObj.find('td input[id*="txtInvQty"]').val());
                        rowObj.find('td input[id*="txtInvQty"]').val(totalQty);
                        totalQty = parseFloat($('#<%=txtLQty.ClientID%>').val()) + parseFloat(rowObj.find('td input[id*="txtlQty"]').val());
                        rowObj.find('td input[id*="txtlQty"]').val(totalQty);
                        fncRepeaterColumnValueChange(this, 'InvQty', 'LoadQty');
                        fncSubClear('clr');
                        status = false;
                        return;
                    }
                    else if (parseFloat(rowObj.find('td input[id*="txtPONo"]').val()) == 0 && $('#<%=hidGidExistVal.ClientID%>').val() == "Y") {
                        AddMode = "AlreadyExists";
                        fncShowSaveConfirmation_master("This Item Already Exists .Do you want to Add?.");
                        ItemcodeColorch = rowObj.find('td input[id*="txtItemcode"]').val().trim();
                        MrpColorch = rowObj.find('td input[id*="txtMRP"]').val();
                        SNoColorch = rowObj.find('td input[id*="txtSerialNo"]').val();
                        StyleColorch = rowObj.find('td input[id*="txtStyleCode"]').val();
                        status = false;
                        return;
                    }
                    else {
                        status = false;
                        popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_AddedList%>');
                        fncSubClear('add');
                        return;
                    }
            }
        }
        });

    if (status == false) {
        fncItemcodeboxDisable(false);
        $('#<%=lnkAdd.ClientID%>').css("display", "block");
        return false;
    }
    if ($('#<%=txtItemCode.ClientID%>').val().trim() == "") {
            popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_EnterItem%>');
        status = false;
    }
    else if (fncCheckDeActiveStatus() == false) {
        status = false;
    }
    else if ($('#<%=txtDescription.ClientID%>').val() == "") {
        ShowPopupMessageBox('<%=Resources.LabelCaption.alert_InvalidDesc%>');
        status = false;
    }
    else if ($('#<%=txtESerialNo.ClientID%>').val() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtESerialNo.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('Please Enter Serial Number !');
        status = false;
    }
    else if ($('#<%=txtStyleCodeT.ClientID%>').val() == "") {
        popUpObjectForSetFocusandOpen = $('#<%=txtStyleCodeT.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('Please Enter Stylecode !');
        status = false;
    } 
    else if (fncCheckStyleCode($('#<%=txtItemCode.ClientID%>').val(), $('#<%=txtStyleCodeT.ClientID%>').val(), $('#<%=txtMRP.ClientID%>').val(),'Normal',0) == false) {
        status = false;
    }
    else if ($('#<%=txtInvQty.ClientID%>').val() == "" || parseFloat($('#<%=txtInvQty.ClientID%>').val()) == 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtInvQty.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterQty%>');
        status = false;
    }
    else if ($('#<%=txtLQty.ClientID%>').val() == "" || parseFloat($('#<%=txtLQty.ClientID%>').val()) == 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtLQty.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterQty%>');
        status = false;
    }
    else if ($('#<%=txtMRP.ClientID%>').val() == "" || parseFloat($('#<%=txtMRP.ClientID%>').val()) == 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMRP.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Invalidmrp%>');
        status = false;
    }
    else if ($('#<%=txtBCost.ClientID%>').val() == "" || parseFloat($('#<%=txtBCost.ClientID%>').val()) == 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtBCost.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_BasicCost%>');
        status = false;
    }
    else if (parseFloat($('#<%=txtBCost.ClientID%>').val()) > parseFloat($('#<%=txtMRP.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtBCost.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_BasicLessthenMrp%>');
        status = false;
    }
    else if ($('#<%=txtSPrice.ClientID%>').val() == "" || parseFloat($('#<%=txtSPrice.ClientID%>').val()) == 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtSPrice.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_InvalidSellingPrice%>');
        status = false;
    }
    else if (parseFloat($('#<%=txtSPrice.ClientID%>').val()) < parseFloat($('#<%=txtNetCost.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtSPrice.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_SellingPriceGreaterthenNetCost%>');
        status = false;
    }
    else if (parseFloat($('#<%=txtSPrice.ClientID%>').val()) > parseFloat($('#<%=txtMRP.ClientID%>').val())) {
        popUpObjectForSetFocusandOpen = $('#<%=txtSPrice.ClientID%>');
        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_SellingPriceLessthenMrp%>');
        status = false;
    }
    
    if (status == false) {
        $('#<%=lnkAdd.ClientID%>').css("display", "block");
        return status;
    }

        ///Whole Sale Price
    if ($('#<%=hidWholeSale.ClientID %>').val() == "Y") {
            if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
            $('#<%=txtWpirce2.ClientID%>').val($('#<%=txtWpirce1.ClientID%>').val());
                $('#<%=txtWpirce3.ClientID%>').val($('#<%=txtWpirce1.ClientID%>').val());
                if ($('#<%=txtWpirce1.ClientID%>').val() == "" || parseFloat($('#<%=txtWpirce1.ClientID%>').val()) == 0) {

                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce1.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');
                    status = false;
                }
                if (parseFloat($('#<%=txtWpirce1.ClientID%>').val()) < parseFloat($('#<%=txtNetCost.ClientID%>').val())) {

                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce1.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');
                    status = false;
                }
                else if (($('#<%=hidMBatchNo.ClientID%>').val() == "" || $('#<%=hidExpireDate.ClientID%>').val() == "") && $('#<%=hidAllowExpireDate.ClientID%>').val() == "1") {
                    fncInitializeMedicalBatchDialog('Add');
                    status = false;
                }


            }
            else {


                if ($('#<%=txtWpirce1.ClientID%>').val() == "" || parseFloat($('#<%=txtWpirce1.ClientID%>').val()) == 0) {

                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce1.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');
                    status = false;
                }
                else if ($('#<%=txtWpirce2.ClientID%>').val() == "" || parseFloat($('#<%=txtWpirce2.ClientID%>').val()) == 0) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce2.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');
                    status = false;
                }
                else if ($('#<%=txtWpirce3.ClientID%>').val() == "" || parseFloat($('#<%=txtWpirce3.ClientID%>').val()) == 0) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce3.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');
                    status = false;
                }

        if (parseFloat($('#<%=txtWpirce1.ClientID%>').val()) < parseFloat($('#<%=txtNetCost.ClientID%>').val())) {

                    popUpObjectForSetFocusandOpen = $('#<%=txtWpirce1.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');
            status = false;
        }
        else if (parseFloat($('#<%=txtWpirce2.ClientID%>').val()) < parseFloat($('#<%=txtNetCost.ClientID%>').val())) {
            popUpObjectForSetFocusandOpen = $('#<%=txtWpirce2.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');
            status = false;
        }
        else if (parseFloat($('#<%=txtWpirce3.ClientID%>').val()) < parseFloat($('#<%=txtNetCost.ClientID%>').val())) {
            popUpObjectForSetFocusandOpen = $('#<%=txtWpirce3.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');
            status = false;
        }
        else if (($('#<%=hidMBatchNo.ClientID%>').val() == "" || $('#<%=hidExpireDate.ClientID%>').val() == "") && $('#<%=hidAllowExpireDate.ClientID%>').val() == "1") {
            fncInitializeMedicalBatchDialog('Add');
            status = false;
        }


}

}

    if (status == true) {
        fncItemcodeboxDisable(false);
        fncAddEnterItems();
    }
    else
        $('#<%=lnkAdd.ClientID%>').css("display", "block");
    return status;


}
    catch (err) {
        fncToastError(err.message);
    }
}
///Add Vat Detail
function fncAddVetDetail() {
    try {
        var invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost;
        var taxper1, taxper2;

        invQty = $('#<%=txtInvQty.ClientID%>').val();
        taxAmt1 = fncConvertFloatJS($('#<%=hidITaxAmt1.ClientID%>').val()); //fncConvertFloat($('#<%=hidITaxAmt1.ClientID%>').val());//fncCheckFloatOrNot($('#<%=hidITaxAmt1.ClientID%>').val());//!

        ($('#<%=hidITaxAmt1.ClientID%>').val()) ? $('#<%=hidITaxAmt1.ClientID%>').val() : 0; //$('#<%=hidITaxAmt1.ClientID%>').val();
        taxAmt2 = fncConvertFloatJS($('#<%=hidITaxAmt2.ClientID%>').val());//!isNaN($('#<%=hidITaxAmt2.ClientID%>').val()) ? $('#<%=hidITaxAmt2.ClientID%>').val() : 0;//$('#<%=hidITaxAmt2.ClientID%>').val();
        cessAmt = fncConvertFloatJS($('#<%=hidCessAmt.ClientID%>').val());//!isNaN($('#<%=hidCessAmt.ClientID%>').val()) ? $('#<%=hidCessAmt.ClientID%>').val() : 0;//$('#<%=hidCessAmt.ClientID%>').val(); 

        taxper1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val());//!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0"; //$('#<%=txtITaxper1.ClientID%>').val() == '' ? '0' : $('#<%=txtITaxper1.ClientID%>').val();
        taxper2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val()); //!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0";//$('#<%=txtITaxper2.ClientID%>').val() == '' ? '0' : $('#<%=txtITaxper2.ClientID%>').val();

        taxPerc = (parseFloat(taxper1) + parseFloat(taxper2)).toFixed(2);
        cessPer = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();
        totalgrosscost = $('#<%=txtTotalGCost.ClientID%>').val();
        fncCreateVatDetailTable(invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost);
    }
    catch (err) {
        fncToastError(err.message);
    }
}




///AddVetDetail
function fncAddVetDetailForPoandHold() {
    try {

        var invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost;
        vatRowNo = 0, rowNo = 0;

        var totGrocessAmt = 0, totGST = 0, totCessAmt = 0, totQty = 0, totBasicAmt = 0;
        var totdiscountAmt = 0, totShDiscAmt = 0, totBillAmt = 0, Addition = 0, deduction = 0;
        var totAddCessAmt = 0;

        vatDetailtbody = $("#tblVetDetail tbody");
        vatDetailtbody.children().remove();
        $("#tblGRN [id*=GRNBodyrow]").each(function () {
            rowObj = $(this);
            rowNo = parseInt(rowNo) + parseFloat(1);



            rowObj.find('td input[id*="txtSNo"]').val(rowNo);
            rowObj.find('td input[id*="txtSerialNo"]').val(rowNo);
            invQty = rowObj.find('td input[id*="txtInvQty"]').val();
            taxAmt1 = rowObj.find('td input[id*="txtITaxAmt1"]').val();
            taxAmt2 = rowObj.find('td input[id*="txtITaxAmt2"]').val();
            cessAmt = rowObj.find('td input[id*="txtCessAmt"]').val();
            taxPerc = parseFloat(rowObj.find('td input[id*="txtITaxperc1"]').val()) + parseFloat(rowObj.find('td input[id*="txtITaxperc2"]').val());
            cessPer = rowObj.find('td input[id*="txtCessper"]').val();
            totalgrosscost = rowObj.find('td input[id*="txtTotalGrossCost"]').val();


            rowObj.find('td input[id*="txtFoc_"]').number(true, 2);
            rowObj.find('td input[id*="txtMRP_"]').number(true, 2);
            rowObj.find('td input[id*="txtSellingPrice_"]').number(true, 2);
            rowObj.find('td input[id*="txtWPrice1_"]').number(true, 2);
            rowObj.find('td input[id*="txtWPrice2_"]').number(true, 2);
            rowObj.find('td input[id*="txtWPrice3_"]').number(true, 2);
            rowObj.find('td input[id*="txtPrintQty_"]').number(true, 0);

            if (rowObj.find('td span[id*="lblGridRowStatus"]').text().indexOf("KG") > -1) {
                rowObj.find('td input[id*="txtInvQty"]').number(true, 2);
                rowObj.find('td input[id*="txtlQty"]').number(true, 2);
                fncGRNGridRowTextColorChange(rowObj);
            }
            else if (rowObj.find('td span[id*="lblGridRowStatus"]').text().indexOf("PCS") > -1) {
                rowObj.find('td input[id*="txtInvQty"]').number(true, 0);
                rowObj.find('td input[id*="txtlQty"]').number(true, 0);
                fncGRNGridRowTextColorChange(rowObj);
            }

            if (rowObj.find('td span[id*="lblGridRowStatus"]').text().indexOf("Bar") > -1) {
                fncGRNGridRowTextColorChange(rowObj);
            }



            if (rowObj.find('td span[id*="PricingType"]').text() == "1" || rowObj.find('td span[id*="PricingType"]').text() == "2" || rowObj.find('td span[id*="lblGridRowStatus"]').text().indexOf("CEF") > -1) {
                rowObj.find('td input[id*="txtBasicCost"]').attr("readonly", "readonly");
                rowObj.find('td input[id*="txtBasicCost"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtBasicCost"]').addClass("border_nonewith_yellow");
                rowObj.find('td input[id*="txtDiscPer"]').attr("readonly", "readonly");
                rowObj.find('td input[id*="txtDiscPer"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtDiscPer"]').addClass("border_nonewith_yellow");
                rowObj.find('td input[id*="txtDiscAmt"]').attr("readonly", "readonly");
                rowObj.find('td input[id*="txtDiscAmt"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtDiscAmt"]').addClass("border_nonewith_yellow");
                rowObj.find('td input[id*="txtSDper"]').attr("readonly", "readonly");
                rowObj.find('td input[id*="txtSDper"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtSDper"]').addClass("border_nonewith_yellow");
                rowObj.find('td input[id*="txtSDAmt"]').attr("readonly", "readonly");
                rowObj.find('td input[id*="txtSDAmt"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtSDAmt"]').addClass("border_nonewith_yellow");
                rowObj.find('td input[id*="txtSDAmt"]').on('keypress', fnctblColEnableFalse);
                rowObj.find('td input[id*="txtSDAmt"]').addClass("border_nonewith_yellow");

            }
            else {
                rowObj.find('td input[id*="txtBasicCost"]').number(true, 4);
                rowObj.find('td input[id*="txtDiscPer"]').number(true, 2);
                rowObj.find('td input[id*="txtDiscAmt"]').number(true, 4);
                rowObj.find('td input[id*="txtSDper"]').number(true, 2);
                rowObj.find('td input[id*="txtSDAmt"]').number(true, 4);
            }

            fncCreateVatDetailTable(invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost);
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncGRNGridRowTextColorChange(rowObj) {
    try {
        //rowObj.find('td input[id*="txtSNo"]').css("background-color", "gainsboro");
        //rowObj.find('td input[id*="txtPONo"]').css("background-color", "gainsboro");
        //rowObj.find('td input[id*="txtItemcode"]').css("background-color", "gainsboro");
        //rowObj.find('td input[id*="txtItemDesc"]').css("background-color", "gainsboro");
        //rowObj.find('td input[id*="txtPOQty"]').css("background-color", "gainsboro");
        $("#tblGRN tbody").children().each(function () {
            var cells = $("td", this);
            //console.log(cells);
            //console.log(cells.eq(0));
            var txtSerno = $("td:eq(0) input[type='text']", this);
            var txtPoNo = $("td:eq(2) input[type='text']", this);
            var txtCode = $("td:eq(3) input[type='text']", this);
            var txtDes = $("td:eq(4) input[type='text']", this);
            var PoQty = $("td:eq(5) input[type='text']", this);
            var txtMrp = $("td:eq(9) input[type='text']", this);
            var txtSno = $("td:eq(54) input[type='text']", this);
            var txtStyle = $("td:eq(55) input[type='text']", this);
            if ($(txtCode).css('background-color') == "rgb(253, 253, 3)") {
            }
            else if (ItemcodeColorch == txtCode.val() && MrpColorch == txtMrp.val()
                && SNoColorch == txtSno.val() && StyleColorch == txtStyle.val()) {
                txtSerno.css('background-color', 'rgb(253, 253, 3)');
                txtPoNo.css('background-color', 'rgb(253, 253, 3)');
                txtCode.css('background-color', 'rgb(253, 253, 3)');
                txtDes.css('background-color', 'rgb(253, 253, 3)');
                PoQty.css('background-color', 'rgb(253, 253, 3)');
            }
            else {
                txtSerno.css('background-color', 'gainsboro');
                txtPoNo.css('background-color', 'gainsboro');
                txtCode.css('background-color', 'gainsboro');
                txtDes.css('background-color', 'gainsboro');
                PoQty.css('background-color', 'gainsboro');
            }
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Create VetDetail Table
function fncCreateVatDetailTable(invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost) {
    try {

        var totalTaxtAmt1 = 0, totalTaxtAmt2 = 0, localTaxPerc, localCessper, status = "NotAvailable";
        var totalPurValue, totalCessAmt, rowObj;

        totalTaxtAmt1 = (parseFloat(invQty) * parseFloat(taxAmt1)).toFixed(4);
        totalTaxtAmt2 = (parseFloat(invQty) * parseFloat(taxAmt2)).toFixed(4);
        totalCessAmt = (parseFloat(invQty) * parseFloat(cessAmt)).toFixed(4);

        totalPurValue = (parseFloat(totalTaxtAmt1) + parseFloat(totalTaxtAmt2) + parseFloat(totalgrosscost)).toFixed(2);

        vatDetailtbody.children().each(function () {
            localTaxPerc = $(this).find('td[id*="tdTaxPerc"]').text();
            localCessper = $(this).find('td[id*="tdCessPerc"]').text();

            if (parseFloat(taxPerc) == parseFloat(localTaxPerc) && parseFloat(cessPer) == parseFloat(localCessper)) {
                status = "Available";
            }
        });
        if (status == "NotAvailable") {
            vatRowNo = parseInt(vatRowNo) + 1;
            rowObj = "<tr><td>" + vatRowNo
                                    + "</td><td id='tdTaxPerc_" + vatRowNo + "'>" + parseFloat(taxPerc).toFixed(2)
                                    + "</td><td id='tdCessPerc_" + vatRowNo + "'>" + parseFloat(cessPer).toFixed(2)
                                    + "</td><td id='tdTaxablePurValue_" + vatRowNo + "'>" + parseFloat(totalgrosscost).toFixed(2)
                                    + "</td><td id='tdTaxAmt1_" + vatRowNo + "'>" + parseFloat(totalTaxtAmt1).toFixed(4)
                                    + "</td><td id='tdTaxAmt2_" + vatRowNo + "'>" + parseFloat(totalTaxtAmt2).toFixed(4)
                                    + "</td><td id='tdCessAmt_" + vatRowNo + "'>" + parseFloat(totalCessAmt).toFixed(4)
                                    + "</td><td id='tdTotalvalue_" + vatRowNo + "'>" + (parseFloat(totalPurValue) + parseFloat(totalCessAmt)).toFixed(2)
                                    + "</td></tr>";
            vatDetailtbody.append(rowObj);

        }
        else {
            fncUpdateVatDetail(invQty, taxPerc, cessPer, totalgrosscost, taxAmt1, taxAmt2, cessAmt);
        }
        fncVatSubTotalCalcForFooter();
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Vet Sub Total Calculation
function fncVatSubTotalCalcForFooter() {
    try {
        var totalTaxablePurValue = 0, totalPurValue = 0, totalTaxtAmt1 = 0, totalTaxtAmt2 = 0, totalCessAmt = 0;

        vatDetailtbody = $("#tblVetDetail tbody");

        vatDetailtbody.children().each(function () {
            totalTaxablePurValue = parseFloat(totalTaxablePurValue) + parseFloat($(this).find('td[id*=tdTaxablePurValue]').text());
            totalTaxtAmt1 = parseFloat(totalTaxtAmt1) + parseFloat($(this).find('td[id*=tdTaxAmt1]').text());
            totalTaxtAmt2 = parseFloat(totalTaxtAmt2) + parseFloat($(this).find('td[id*="tdTaxAmt2"]').text());
            totalCessAmt = parseFloat(totalCessAmt) + parseFloat($(this).find('td[id*="tdCessAmt"]').text());
            totalPurValue = parseFloat(totalPurValue) + parseFloat($(this).find('td[id*="tdTotalvalue"]').text());
        });

        //hide GST Column Based on Vendor Status 
        if ($('#<%=hidGidGSTFlag.ClientID%>').val() == "2" || $('#<%=hidGidGSTFlag.ClientID%>').val() == "3") {
            vatDetailtbody.find('td[id*="tdTaxAmt2"]').addClass("hiddencol");
            $("#tblVetDetail [id*=lblTotalTaxAmt2]").addClass("hiddencol");
        }

        $("#tblVetDetail [id*=lblTotalTaxablePurValue]").text(totalTaxablePurValue.toFixed(2));
        $("#tblVetDetail [id*=lblTotalTaxAmt1]").text(totalTaxtAmt1.toFixed(4));
        $("#tblVetDetail [id*=lblTotalTaxAmt2]").text(totalTaxtAmt2.toFixed(4));
        $("#tblVetDetail [id*=lblTotalCessAmt]").text(totalCessAmt.toFixed(4));
        $("#tblVetDetail [id*=lblTotalPurValue]").text(totalPurValue.toFixed(2));
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Update Vat Detail when Repeater Valued Changed
function fncUpdateVatDetail(invQty, taxPerc, cessPer, totalgrosscost, taxAmt1, taxAmt2, cessAmt, Status) {
    try {

        var taxLocalPerc, totalValue, localtaxablePurValue, localtaxAmt1, localtaxAmt2, localCessAmt;
        var localTotalValue, totaltaxAmt1, totaltaxAmt2, totalCessAmt, cesslocaPer;
        //var vatDetailtbody = $("#tblVetDetail tbody");
        vatDetailtbody.children().each(function () {
            taxLocalPerc = $(this).find('td[id*="tdTaxPerc"]').text();
            cesslocaPer = $(this).find('td[id*="tdCessPerc"]').text();
            if (cessPer == "0") {
                cessPer = "0.00"
            }
            else if (cessPer == "12") {
                cessPer = "12.00"
            }

            if (taxLocalPerc == taxPerc && cesslocaPer == cessPer) {
                localtaxablePurValue = $(this).find('td[id*=tdTaxablePurValue]').text();
                localtaxAmt1 = $(this).find('td[id*=tdTaxAmt1]').text();
                localtaxAmt2 = $(this).find('td[id*=tdTaxAmt2]').text();
                localCessAmt = $(this).find('td[id*=tdCessAmt]').text();
                localTotalValue = $(this).find('td[id*=tdTotalvalue]').text();
                totalValue = parseFloat(totalgrosscost) + (parseFloat(taxAmt1) * parseFloat(invQty)) + (parseFloat(taxAmt2) * parseFloat(invQty))
                totaltaxAmt1 = parseFloat(taxAmt1) * parseFloat(invQty);
                totaltaxAmt2 = parseFloat(taxAmt2) * parseFloat(invQty);
                totalCessAmt = parseFloat(cessAmt) * parseFloat(invQty);

                if (Status == "Edit" || Status == "Delete") {
                    localtaxablePurValue = parseFloat(localtaxablePurValue) - parseFloat(glSRTotalGrossAmt);
                    localtaxAmt1 = parseFloat(localtaxAmt1) - parseFloat(glSRtaxAmt1);
                    localtaxAmt2 = parseFloat(localtaxAmt2) - parseFloat(glSRtaxAmt2);
                    localCessAmt = parseFloat(localCessAmt) - parseFloat(glSRCessAmt);
                    localTotalValue = parseFloat(localTotalValue) - parseFloat(glSRTotalvalue);
                }
                if (Status == "Delete") {
                    $(this).find('td[id*=tdTaxablePurValue]').text(localtaxablePurValue.toFixed(2));
                    $(this).find('td[id*=tdTaxAmt1]').text(localtaxAmt1.toFixed(4));
                    $(this).find('td[id*=tdTaxAmt2]').text(localtaxAmt2.toFixed(4));
                    $(this).find('td[id*=tdCessAmt]').text(localCessAmt.toFixed(4));
                    $(this).find('td[id*=tdTotalvalue]').text(localTotalValue.toFixed(2));
                }
                else {
                    $(this).find('td[id*=tdTaxablePurValue]').text((parseFloat(localtaxablePurValue) + parseFloat(totalgrosscost)).toFixed(2));
                    $(this).find('td[id*=tdTaxAmt1]').text((parseFloat(localtaxAmt1) + parseFloat(totaltaxAmt1)).toFixed(4));
                    $(this).find('td[id*=tdTaxAmt2]').text((parseFloat(localtaxAmt2) + parseFloat(totaltaxAmt2)).toFixed(4));
                    $(this).find('td[id*=tdCessAmt]').text((parseFloat(localCessAmt) + parseFloat(totalCessAmt)).toFixed(4));
                    $(this).find('td[id*=tdTotalvalue]').text((parseFloat(localTotalValue) + parseFloat(totalValue) + parseFloat(totalCessAmt)).toFixed(2));
                }

            }
        });

        fncVatSubTotalCalcForFooter();

    }
    catch (err) {
        fncToastError(err.message);
    }
}


///Deete and Update Vat Detail when Repeater Valued Changed
function fncDeleteandUpdateVetDetail(invQty, taxPerc, cessPer, totalgrosscost, taxAmt1, taxAmt2, cessAmt, status) {
    try {
        var taxLocalPerc, localCessper;
        //var vatDetailtbody = $("#tblVetDetail tbody");
        if (status == "Available") {

            taxLocalPerc = $(this).find('td[id*="tdTaxPerc"]').text();
            fncUpdateVatDetail(invQty, taxPerc, cessPer, totalgrosscost, taxAmt1, taxAmt2, cessAmt, 'Delete');
        }
        else {
            vatDetailtbody.children().each(function () {
                taxLocalPerc = $(this).find('td[id*="tdTaxPerc"]').text();
                localCessper = $(this).find('td[id*="tdCessPerc"]').text();
                if (taxLocalPerc == taxPerc && localCessper == cessPer) {
                    $(this).remove();

                }
            });
        }

        fncVatSubTotalCalcForFooter();
    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Row Delete
function fncDeleteClick(source) {
    try {


        var rowObj, rowId;

        rowobj = $(source);//.parent().parent();


        rowId = rowobj.find('td input[id*="txtSNo"]').val();
        //rowId = parseFloat(rowId) - parseFloat(1);

        $('#<%=hidItemcode.ClientID%>').val(rowobj.find('td input[id*="txtItemcode"]').val());
        $('#<%=hidMRP.ClientID%>').val(rowobj.find('td input[id*="txtMRP"]').val());
        $('#<%=hidBasiccost.ClientID%>').val(rowobj.find('td input[id*="txtBasicCost"]').val());
        $('#<%=btnDeleteRow.ClientID%>').click();

    }
    catch (err) {
        fncToastError(err.message);
    }
}




//Repeater Column Value Change Process
function fncRepeaterColumnValueChange(source, value, mode) {
    try {

        var rowObj, invQty, basicCost, discAmt, shDiscAmt, GrossCost, NetCost, grossVatAmt = 0, taxAmt1, taxAmt2, cessAmt;
        var grossBasicAmt, totalgrosscost, grossVatAmt, grossCessAmt, grossDiscAmt;
        var discper, shDiscper, MarkDownPerc, sumITaxPerc, taxPerc1, taxPerc2, cessPer;
        var refNetCost, earnedMargin, outPutGSt, basicSelling, sellingprice, ouputcessAmt = 0;
        var outputgstAmt, dBatchValue, sBatchValue, actualGST = 0, gstPer, AddCessamt = 0, total = 0;
        var prvMRP = 0, prvNetCost = 0, prvSelling = 0, prvMarginFixed = 0, prvEarnedMargin = 0;
        var SFixedMargin = 0, mrp = 0;
        var serialno, stylecode;

        if (mode == "LoadQty")
            rowObj = $(source);
        else {
            rowObj = $(source).parent().parent();
            var status = $.inArray(rowObj.find('td input[id*="txtSNo"]').val(), changeRow) > -1

            if (status == false) {
                changeRow.push(rowObj.find('td input[id*="txtSNo"]').val());
                $("#<%=hidChangeRowval.ClientID %>").val(changeRow);
            }
        }

        glInvCode = rowObj.find('td input[id*="txtItemcode"]').val();
        glBatchNo = rowObj.find('td input[id*="hidItemBatchNo"]').val();

        glmrp = rowObj.find('td input[id*="txtMRP"]').val();
        glsprice = rowObj.find('td input[id*="txtMRP"]').val();
        glNetCost = rowObj.find('td input[id*="txtNetCost"]').val();


        invQty = rowObj.find('td input[id*="txtInvQty"]').val();
        basicCost = rowObj.find('td input[id*="txtBasicCost"]').val();
        mrp = rowObj.find('td input[id*="txtMRP"]').val();
        sellingprice = rowObj.find('td input[id*="txtSellingPrice"]').val();
        wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val();
        wprice2 = rowObj.find('td input[id*="txtWPrice2"]').val();
        wprice3 = rowObj.find('td input[id*="txtWPrice3"]').val();
        discper = rowObj.find('td input[id*="txtDiscPer"]').val();
        discAmt = rowObj.find('td input[id*="txtDiscAmt"]').val();
        shDiscper = rowObj.find('td input[id*="txtSDper"]').val();
        shDiscAmt = rowObj.find('td input[id*="txtSDAmt"]').val();
        GrossCost = rowObj.find('td input[id*="txtGrossCost"]').val();
        NetCost = rowObj.find('td input[id*="txtNetCost"]').val();
        taxAmt1 = rowObj.find('td input[id*="txtITaxAmt1"]').val();
        taxAmt2 = rowObj.find('td input[id*="txtITaxAmt2"]').val();
        cessAmt = rowObj.find('td input[id*="txtCessAmt"]').val();
        earnedMargin = rowObj.find('td input[id*="txtEarnedMarPerc"]').val();
        SFixedMargin = rowObj.find('td input[id*="txtMarginfixed"]').val();
        sellingprice = rowObj.find('td input[id*="txtSellingPrice"]').val();
        taxPerc1 = rowObj.find('td input[id*="txtITaxperc1"]').val();
        taxPerc2 = rowObj.find('td input[id*="txtITaxperc2"]').val();
        cessPer = rowObj.find('td input[id*="txtCessper"]').val();
        MarkDownPerc = rowObj.find('td input[id*="txtMarkDownPerc"]').val();
        addCessAmt = rowObj.find('td input[id*="txtAddCessAmt"]').val();
        basicSelling = rowObj.find('td input[id*="hidBasicSelling"]').val();
        serialno = rowObj.find('td input[id*="txtSerialNo"]').val();
        stylecode = rowObj.find('td input[id*="txtStyleCode"]').val();


        grossBasicAmt = parseFloat(basicCost) * parseFloat(invQty);
        grossDiscAmt = parseFloat(discAmt) * parseFloat(invQty);
        grossShDiscAmt = parseFloat(shDiscAmt) * parseFloat(invQty);
        totalgrosscost = parseFloat(GrossCost) * parseFloat(invQty);
        actualGST = parseFloat(taxAmt1) + parseFloat(taxAmt2)
        grossVatAmt = parseFloat(actualGST) * parseFloat(invQty);
        grossCessAmt = parseFloat(cessAmt) * parseFloat(invQty);

        if (value == "MRP") {

            rowObj.find('td input[id*="txtBasicCost"]').css("background-color", "aliceblue");

            //Create Batch No
            if (rowObj.find('td span[id*="lblAllowExpireDate"]').text() == "0") {
                if (rowObj.find('td span[id*="lblBatch"]').text() == "1") {
                    if (parseFloat(rowObj.find('td input[id*="txtMRP"]').val()) > 9999) {
                        dBatchValue = Math.trunc(parseFloat(rowObj.find('td input[id*="txtMRP"]').val())) / 1000000;
                        sBatchValue = dBatchValue + "00000000";
                    }
                    else {
                        dBatchValue = Math.trunc(parseFloat(rowObj.find('td input[id*="txtMRP"]').val()) * 100) / 1000000;
                        //dBatchValue = parseFloat(rowObj.find('td input[id*="txtMRP"]').val() * 100) / 1000000;                                
                        sBatchValue = dBatchValue + "00000000";

                    }
                    rowObj.find('td input[id*="lblItemBatchNo"]').val(sBatchValue.substr(2, 6));
                    rowObj.find('td input[id*="hidItemBatchNo"]').val(sBatchValue.substr(2, 6));
                }
                else {
                    rowObj.find('td input[id*="lblItemBatchNo"]').val('');
                    rowObj.find('td input[id*="hidItemBatchNo"]').val('');
                }
            }
            else {
                rowObj.find('td input[id*="lblItemBatchNo"]').val('');
                rowObj.find('td input[id*="hidItemBatchNo"]').val('');
            }


            if (rowObj.find('td span[id*="lblPricingType"]').text() == "1" || rowObj.find('td span[id*="lblPricingType"]').text() == "2") {// Purchase by MRP
                var locNetCost = 0;
                sumITaxPerc = parseFloat(taxPerc1) + parseFloat(taxPerc2) + parseFloat(cessPer);
                NetCost = parseFloat(mrp) - (parseFloat(mrp) * parseFloat(MarkDownPerc)) / 100;
                locNetCost = parseFloat(NetCost) - parseFloat(addCessAmt);
                //GrossCost = (parseFloat(netCost) / (100 + parseFloat(sumITaxPerc))) * 100;

                actualGST = (parseFloat(locNetCost) * parseFloat(sumITaxPerc)) / (100 + parseFloat(sumITaxPerc));
                if (parseFloat(sumITaxPerc) > 0) {
                    cessAmt = (parseFloat(actualGST) / parseFloat(sumITaxPerc)) * parseFloat(cessper);
                }

                grossCost = parseFloat(locNetCost) - parseFloat(actualGST);
                actualGST = parseFloat(actualGST) - parseFloat(cessAmt);
                basicCost = parseFloat(grossCost) + parseFloat(discAmt) + parseFloat(shDiscAmt);

                if (rowObj.find('td span[id*="lblPricingType"]').text() == "1") {
                    var outPutGSt = 0;
                    sellingprice = parseFloat(mrp) - (parseFloat(mrp) * parseFloat(rowObj.find('td input[id*="txtMarginfixed"]').val())) / 100;
                    outPutGSt = fncArriveInclusiveTax(sellingprice, taxPerc1, taxPerc2, cessper);
                    basicSelling = parseFloat(sellingprice) - parseFloat(outPutGSt) - parseFloat(addCessAmt);
                    rowObj.find('td input[id*="hidBasicSelling"]').val(basicSelling.toFixed(2));
                }
                else {
                    sellingprice = parseFloat(NetCost) + (parseFloat(NetCost) * parseFloat(rowObj.find('td input[id*="txtMarginfixed"]').val())) / 100;
                    basicSelling = parseFloat(sellingprice) - parseFloat(actualGST) - parseFloat(cessAmt) - parseFloat(addCessAmt);
                    rowObj.find('td input[id*="hidBasicSelling"]').val(basicSelling.toFixed(2));
                }
            }
            else {

                prvMRP = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[0];
                prvUnitCost = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[4];
                prvSelling = rowObj.find('td span[id*="lblOldSellingPrice"]').text();
                if (parseFloat(prvMRP) > 0) {

                    prvMargin = parseFloat(prvMRP) / parseFloat(prvUnitCost) * 100;
                    basicCost = parseFloat(mrp) / parseFloat(prvMargin) * 100;
                    rowObj.find('td input[id*="txtBasicCost"]').val(basicCost.toFixed(2));

                    if (parseFloat(mrp) != parseFloat(prvMRP)) {
                        if ((parseFloat(mrp) - parseFloat(prvMRP)) > 0 && parseFloat(prvMRP) != 0 && parseFloat(mrp) != 0) {
                            sellingprice = parseFloat(prvSelling) + ((parseFloat(prvSelling) * ((parseFloat(mrp) - parseFloat(prvMRP)) / parseFloat(mrp)) * 100) / 100);
                        }

                        if ((parseFloat(mrp) - parseFloat(prvMRP)) < 0 && parseFloat(prvMRP) != 0 && parseFloat(mrp) != 0) {
                            if ($('#<%=hidsellingCalFromPrvMRP.ClientID%>').val() == "Y") {
                                sellingprice = parseFloat(mrp) - ((((parseFloat(prvMRP) - parseFloat(prvSelling)) / parseFloat(prvMRP) * 100) * parseFloat(mrp)) / 100);
                            }
                            else {
                                sellingprice = parseFloat(prvSelling) - (parseFloat(prvSelling) * (((parseFloat(prvMRP) - parseFloat(mrp)) / parseFloat(mrp)) * 100) / 100);
                            }
                        }
                    }

                    rowObj.find('td input[id*="txtSellingPrice"]').val(SellingPriceRoundOff(sellingprice));

                    rowObj.find('td input[id*="txtBasicCost"]').css("background-color", "lightgreen");
                    value = "BasicCost";
                }
            }

            fncBindMultipleUOMExisitingRecord(mrp, NetCost);
            fncShowMultipUOM(glInvCode);
        }

        if (value == "BasicCost") {




            basicCost = rowObj.find('td input[id*="txtBasicCost"]').val();
            GrossCost = parseFloat(basicCost) - parseFloat(discAmt) - parseFloat(shDiscAmt);
            actualGST = fncTaxAmtCalculation(GrossCost, taxPerc1, taxPerc2);
            cessAmt = fncCessCalc(GrossCost, cessPer);
            NetCost = parseFloat(GrossCost) + parseFloat(actualGST) + parseFloat(cessAmt);// + parseFloat(addCessAmt);

            if (parseFloat(SFixedMargin) > 0) {
                basicSelling = parseFloat(GrossCost) + (parseFloat(GrossCost) * parseFloat(SFixedMargin) / (100));
                outPutGSt = fncTaxAmtCalculation(basicSelling, taxPerc1, taxPerc2);
                ouputcessAmt = fncCessCalc(basicSelling, cessPer);
                sellingprice = parseFloat(basicSelling) + parseFloat(outPutGSt) + parseFloat(ouputcessAmt);
                sellingprice = parseFloat(sellingprice) + parseFloat(addCessAmt);
            }
            else {
                basicSelling = parseFloat(basicSelling);
                sellingprice = parseFloat(sellingprice);
            }

            value = "DiscPer"
        }

        if (value == "DiscPer" || value == "DiscAmt" || value == "ShDiscPer" || value == "ShDiscAmt") {
            if (value == "DiscPer") {
                discAmt = (parseFloat(basicCost) * parseFloat(discper)) / 100;
                rowObj.find('td input[id*="txtDiscAmt"]').val(discAmt.toFixed(4));
            }
            else if (value == "DiscAmt") {

                discAmt = rowObj.find('td input[id*="txtDiscAmt"]').val();
                discAmt = parseFloat(discAmt) / parseFloat(invQty);
                rowObj.find('td input[id*="txtDiscAmt"]').val(discAmt.toFixed(4));
                discPer = (parseFloat(discAmt) / parseFloat(basicCost)) * 100;
                rowObj.find('td input[id*="txtDiscPer"]').val(parseFloat(discPer).toFixed(2));
            }

            if (value == "DiscPer" || value == "ShDiscPer") {

                if ($('#<%=cbSDOnGrossCost.ClientID%>').is(":checked")) {
                    basicCost = parseFloat(basicCost) - parseFloat(discAmt);
                }

                discPer = rowObj.find('td input[id*="txtSDper"]').val();
                shDiscAmt = (parseFloat(basicCost) * parseFloat(discPer)) / 100;
                rowObj.find('td input[id*="txtSDAmt"]').val(shDiscAmt.toFixed(4));


            }
            else if (value == "DiscAmt" || value == "ShDiscAmt") {

                shDiscAmt = rowObj.find('td input[id*="txtSDAmt"]').val();
                shDiscAmt = parseFloat(shDiscAmt) / parseFloat(invQty);
                rowObj.find('td input[id*="txtSDAmt"]').val(shDiscAmt);

                //discAmt = $('#<%=txtDiscAmt.ClientID%>').val();


                if ($('#<%=cbSDOnGrossCost.ClientID%>').is(":checked")) {
                    basicCost = parseFloat(basicCost) - parseFloat(discAmt);
                }

                shDiscAmt = rowObj.find('td input[id*="txtSDAmt"]').val();
                discPer = (parseFloat(shDiscAmt) / parseFloat(basicCost)) * 100;
                rowObj.find('td input[id*="txtSDper"]').val(discPer);
            }

            //GrossCost = parseFloat(basicCost) - parseFloat(discAmt) - parseFloat(shDiscAmt);
        basicCost = rowObj.find('td input[id*="txtBasicCost"]').val();
        GrossCost = parseFloat(basicCost) - parseFloat(discAmt) - parseFloat(shDiscAmt);
        actualGST = fncTaxAmtCalculation(GrossCost, taxPerc1, taxPerc2);
        cessAmt = fncCessCalc(GrossCost, cessPer);
            //taxAmt1 = parseFloat(actualGST) / 2;
            //taxAmt2 = parseFloat(actualGST) / 2;
        if ($('#<%=hidVendorStatus.ClientID%>').val() == "1") {
            taxAmt1 = parseFloat(actualGST) / 2;
            taxAmt2 = parseFloat(actualGST) / 2;
        }
        else {
            taxAmt1 = parseFloat(actualGST);
            taxAmt2 = "0";
        }//Vijay 20190207


    }
    else if (value == "sellingprice") {

        prvMRP = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[0];
        prvNetCost = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[1];
        prvSelling = rowObj.find('td span[id*="lblOldSellingPrice"]').text();
        prvMarginFixed = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[2];
        prvEarnedMargin = rowObj.find('td span[id*="lblPrvData"]').text().split(',')[3];

        outputgstAmt = fncArriveInclusiveTax(sellingprice, taxPerc1, taxPerc2, cessPer);
        basicSelling = parseFloat(sellingprice) - parseFloat(outputgstAmt);
        rowObj.find('td input[id*="txtBasicSelling"]').val(basicSelling.toFixed(2));



        if (fncRepeaterColumnValidation(rowObj, basicCost, mrp, sellingprice, NetCost, wprice1, wprice2, wprice3) == false) {
            return false;
        }

        if (parseFloat(sellingprice) != parseFloat(prvSelling)) {

            $('#<%=txtoldCost.ClientID%>').val(prvNetCost);
            $('#<%=txtOldMRP.ClientID%>').val(prvMRP);
            $('#<%=txtOldSelling.ClientID%>').val(prvSelling);
            $('#<%=txtoldMarginfixed.ClientID%>').val(prvMarginFixed);
            $('#<%=txtoldMarginEarned.ClientID%>').val(prvEarnedMargin);


            $('#<%=txtNewCost.ClientID%>').val(basicCost);
            $('#<%=txtNewMRP.ClientID%>').val(mrp);
            $('#<%=txtNewSelling.ClientID%>').val(sellingprice);
            $('#<%=txtNewMarginFixed.ClientID%>').val(earnedMargin);
            $('#<%=txtNewMarginEarned.ClientID%>').val(rowObj.find('td input[id*="txtMarginfixed"]').val());

            fncEnablePriceChangeAlert("Enable", rowObj.find('td input[id*="txtItemDesc"]').val());

            if (rowObj.find('td span[id*="lblGridRowStatus"]').text().indexOf("Bre") > -1) {
                fncOpenItemhistory(rowObj.find('td input[id*="txtItemcode"]').val());
            }
            else if (rowObj.find('td span[id*="lblItemType"]').text() == "BULK" && $('#<%=hidChildPriceChangeAllLocation.ClientID%>').val() == "Y") {
                fncOpenQuickPriceChange(rowObj.find('td input[id*="txtItemcode"]').val());
            }
            else if (rowObj.find('td span[id*="lblItemType"]').text() == "BULK") {
                $('#<%=txtBulItemcode.ClientID%>').val(rowObj.find('td input[id*="txtItemcode"]').val());
                $('#<%=txtBulkItemDesc.ClientID%>').val(rowObj.find('td input[id*="txtItemDesc"]').val());
                $('#<%=txtBulkMRP.ClientID%>').val(rowObj.find('td input[id*="txtMRP"]').val());
                $('#<%=txtBulkCost.ClientID%>').val(rowObj.find('td input[id*="txtNetCost"]').val());
                $('#<%=txtBulkSPrice.ClientID%>').val(rowObj.find('td input[id*="txtSellingPrice"]').val());
                $('#<%=txtBulkNewSPrice.ClientID%>').val(rowObj.find('td input[id*="txtSellingPrice"]').val());
                $('#<%=txtBulkWPrice1.ClientID%>').val(rowObj.find('td input[id*="txtWPrice1"]').val());
                $('#<%=txtBulkWPrice2.ClientID%>').val(rowObj.find('td input[id*="txtWPrice2"]').val());
                $('#<%=txtBulkWPrice3.ClientID%>').val(rowObj.find('td input[id*="txtWPrice3"]').val());
                fncGetChildItemList();
            }

            //fncPriceChangeAlertNew();
}
else {
    fncEnablePriceChangeAlert("Disable", "");
}

}
else if (value == "AddCessAmt") {
    rowObj.find('td input[id*="txtNetCost"]').val((parseFloat(GrossCost) + parseFloat(taxAmt1) + parseFloat(taxAmt2) + parseFloat(cessAmt)).toFixed(4));
    NetCost = rowObj.find('td input[id*="txtNetCost"]').val();
    rowObj.find('td input[id*="txtTotalValue"]').val((parseFloat(invQty) * parseFloat(NetCost)).toFixed(2));

    /// Total Addiotional Cess Amt
    AddCessamt = rowObj.find('td input[id*="txtAddCessAmt"]').val();
    total = rowObj.find('td input[id*="txtTotalValue"]').val();
    rowObj.find('td input[id*="txtTotalValue"]').val(parseFloat(AddCessamt) + parseFloat(total));
    rowObj.find('td input[id*="txtGRAddCessAmt"]').val(parseFloat(AddCessamt).toFixed(2));

    // Unit of Addtional Cess Amt
    AddCessamt = parseFloat(AddCessamt) / parseFloat(invQty)
    rowObj.find('td input[id*="txtAddCessAmt"]').val(parseFloat(AddCessamt).toFixed(2));

}

    if (value == "SerialNo") {

        rowObj.find('td input[id*="hidItemBatchNo"]').val(AddZeros(serialno, 3));
        rowObj.find('td input[id*="txtSerialNo"]').val(serialno);

    }
    if (value == "StyleCode") {
        rowObj.find('td input[id*="txtStyleCode"]').val(stylecode)
    }


    grossDiscAmt = parseFloat(discAmt) * parseFloat(invQty);
    grossShDiscAmt = parseFloat(shDiscAmt) * parseFloat(invQty);
    basicCost = rowObj.find('td input[id*="txtBasicCost"]').val();
    grossBasicAmt = parseFloat(basicCost) * parseFloat(invQty);
    totalgrosscost = parseFloat(GrossCost) * parseFloat(invQty);
    grossVatAmt = parseFloat(actualGST) * parseFloat(invQty);
    grossCessAmt = parseFloat(cessAmt) * parseFloat(invQty);
    NetCost = parseFloat(GrossCost) + parseFloat(actualGST) + parseFloat(cessAmt) + parseFloat(addCessAmt);

    rowObj.find('td input[id*="txtlQty"]').val(invQty)
    rowObj.find('td input[id*="txtITaxAmt1"]').val(taxAmt1);
    rowObj.find('td input[id*="txtITaxAmt2"]').val(taxAmt2);
    rowObj.find('td input[id*="txtCessAmt"]').val(cessAmt);
    rowObj.find('td input[id*="txtNetCost"]').val(NetCost);
    rowObj.find('td input[id*="txtBasicSelling"]').val(basicSelling);
    rowObj.find('td input[id*="txtSellingPrice"]').val(sellingprice);
    rowObj.find('td input[id*="txtGrossVATAmt"]').val(grossVatAmt.toFixed(4));
    rowObj.find('td input[id*="txtGrocessCessAmt"]').val(grossCessAmt.toFixed(4));
    rowObj.find('td input[id*="txtGrossCost"]').val(GrossCost);
    rowObj.find('td input[id*="txtTotalGrossCost"]').val(totalgrosscost);
    rowObj.find('td input[id*="txtBasicCost"]').val(basicCost);
    rowObj.find('td input[id*="txtGrossBasicAmt"]').val(grossBasicAmt.toFixed(4));
    rowObj.find('td input[id*="txtGrossDiscAmt"]').val(grossDiscAmt);
    rowObj.find('td input[id*="txtGrossShDiscAmt"]').val(grossShDiscAmt);
    rowObj.find('td input[id*="txtTotalGrossCost"]').val(totalgrosscost.toFixed(2));
    rowObj.find('td input[id*="txtTotalValue"]').val((parseFloat(NetCost) * parseFloat(invQty)).toFixed(2));
    rowObj.find('td input[id*="txtGRAddCessAmt"]').val((parseFloat(addCessAmt) * parseFloat(invQty)).toFixed(2));




    gstPer = parseFloat(taxPerc1) + parseFloat(taxPerc2);


    if (fncNetValueCalculation() == true) {
        fncRepeaterColumnValidation(rowObj, basicCost, mrp, sellingprice, NetCost, wprice1, wprice2, wprice3);
        ///Update VetDetail
        fncAddVetDetailForPoandHold();
    }

}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncBindMultipleUOMExisitingRecord(mrp, netCost) {
    var uomObj, row, rowNo = 0;
    var mulUOMBody, xmlObj, localBatch;
    try {

        mulUOMBody = $("#tblMultipleUOM tbody");
        mulUOMBody.children().remove();
        localBatch = glBatchNo;
        if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {

            $("#tblMultipleUOMSave tbody").children().each(function () {// your outer tag of xml
                if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode.trim() && $(this).find('td[id*="tdBatachNo"]').text() == localBatch.trim()) {
                    //$(this).remove();
                    glBatchNo = fncCreateBatchNo(mrp);
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr>"
                        + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                        + "<td id='tdUOMCode_" + rowNo + "' >" + $(this).find('td[id*="tdUOMcode"]').text() + "</td>"
                        + "<td id='tdMRP_" + rowNo + "' >" + parseFloat(mrp).toFixed(2) + "</td>"
                        + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(netCost).toFixed(2) + "</td>"
                        + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                        + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                        //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                        + "<td id='tdWholeSalePricePer1_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                        + "<td id='tdWPrice1_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                        //+ "<td id='tdWholeSalePricePer2_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                        //+ "<td id='tdWPrice2_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                        // + "<td id='tdWholeSalePricePer3_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                        //+ "<td id='tdWPrice3_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                        ////+ "<td id='tdWPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdWPrice"]').text()).toFixed(2) + "</td>"
                        + "<td id='tdUOMconv_" + rowNo + "' >" + $(this).find('td[id*="tdUOMconv"]').text() + "</td>"
                        + "</tr>";
                    mulUOMBody.append(row);

                    $("#txtSellingPer_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                    //$("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                    //$("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                    $("#txtSPrice_" + rowNo + "").number(true, 2);
                    $("#txtWPrice1_" + rowNo + "").number(true, 2);
                    //$("#txtWPrice2_" + rowNo + "").number(true, 2);
                    //$("#txtWPrice3_" + rowNo + "").number(true, 2);
                }

            });

        }
        else {
            $("#tblMultipleUOMSave tbody").children().each(function () {// your outer tag of xml
                if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode.trim() && $(this).find('td[id*="tdBatachNo"]').text() == localBatch.trim()) {
                    //$(this).remove();
                    glBatchNo = fncCreateBatchNo(mrp);
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr>"
                        + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                        + "<td id='tdUOMCode_" + rowNo + "' >" + $(this).find('td[id*="tdUOMcode"]').text() + "</td>"
                        + "<td id='tdMRP_" + rowNo + "' >" + parseFloat(mrp).toFixed(2) + "</td>"
                        + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(netCost).toFixed(2) + "</td>"
                        + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                        + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                        //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                        + "<td id='tdWholeSalePricePer1_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                        + "<td id='tdWPrice1_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                        + "<td id='tdWholeSalePricePer2_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                        + "<td id='tdWPrice2_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                         + "<td id='tdWholeSalePricePer3_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                        + "<td id='tdWPrice3_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                        //+ "<td id='tdWPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdWPrice"]').text()).toFixed(2) + "</td>"
                        + "<td id='tdUOMconv_" + rowNo + "' >" + $(this).find('td[id*="tdUOMconv"]').text() + "</td>"
                        + "</tr>";
                    mulUOMBody.append(row);

                    $("#txtSellingPer_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                    $("#txtSPrice_" + rowNo + "").number(true, 2);
                    $("#txtWPrice1_" + rowNo + "").number(true, 2);
                    $("#txtWPrice2_" + rowNo + "").number(true, 2);
                    $("#txtWPrice3_" + rowNo + "").number(true, 2);
                }

            });
        }

        $("#tblMultipleUOMSave tbody").children().each(function () {// your outer tag of xml
            if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode && $(this).find('td[id*="tdBatachNo"]').text() == localBatch) {
                $(this).remove();
            }
        });

        $(setTimeout(function () {
            $("#txtSellingPer_1").select();
        }), 10);
    }
    catch (err) {
        fncToastError(err.message);
    }
}


/// Net Value Calculation
function fncNetValueCalculation() {

    var totGrocessAmt = 0, totGST = 0, totCessAmt = 0, totQty = 0, totBasicAmt = 0;
    var totdiscountAmt = 0, totShDiscAmt = 0, totBillAmt = 0, Addition = 0, deduction = 0;
    var totAddCessAmt = 0, totalRow = 0;

    try {

        totalRow = $("#tblGRN tbody").children().length;

        $("#tblGRN [id*=GRNBodyrow]").each(function () {

            rowObj = $(this);
            totGrocessAmt = parseFloat(totGrocessAmt) + parseFloat(rowObj.find('td input[id*="txtTotalGrossCost"]').val());
            totGST = parseFloat(totGST) + parseFloat(rowObj.find('td input[id*="txtGrossVATAmt"]').val());
            totCessAmt = parseFloat(totCessAmt) + parseFloat(rowObj.find('td input[id*="txtGrocessCessAmt"]').val());
            totQty = parseFloat(totQty) + parseFloat(rowObj.find('td input[id*="txtlQty"]').val());
            totBasicAmt = parseFloat(totBasicAmt) + parseFloat(rowObj.find('td input[id*="txtGrossBasicAmt"]').val()); 
            totdiscountAmt = parseFloat(totdiscountAmt) + parseFloat(rowObj.find('td input[id*="txtGrossDiscAmt"]').val());
            totShDiscAmt = parseFloat(totShDiscAmt) + parseFloat(rowObj.find('td input[id*="txtGrossShDiscAmt"]').val());
            totAddCessAmt = parseFloat(totAddCessAmt) + parseFloat(rowObj.find('td input[id*="txtGRAddCessAmt"]').val());

        });

        $('#<%=txtNetGrossAmt.ClientID%>').val(totGrocessAmt.toFixed(2));
        $('#<%=txtNetVat.ClientID%>').val(totGST.toFixed(2));
        $('#<%=txtNetCessAmt.ClientID%>').val((parseFloat(totCessAmt) + parseFloat(totAddCessAmt)).toFixed(2));
        //$('#<%=txtTotalGSTCessAmt.ClientID%>').val((parseFloat(totGST) + parseFloat(totCessAmt) + parseFloat(totAddCessAmt)).toFixed(2));
        $('#<%=txtTotalGSTCessAmt.ClientID%>').val((parseFloat($('#<%=txtNetVat.ClientID%>').val()) + parseFloat($('#<%=txtNetCessAmt.ClientID%>').val())).toFixed(2));

        //$('#<%=txtTotalBillAmt.ClientID%>').val((parseFloat(totGrocessAmt) + parseFloat(totGST) + parseFloat(totCessAmt) + parseFloat(totAddCessAmt)).toFixed(2));
        $('#<%=txtTotalBillAmt.ClientID%>').val((parseFloat($('#<%=txtNetGrossAmt.ClientID%>').val()) + parseFloat($('#<%=txtTotalGSTCessAmt.ClientID%>').val())).toFixed(2));


        totBillAmt = $('#<%=txtTotalBillAmt.ClientID%>').val();
        deduction = $('#<%=txtDeduction.ClientID%>').val();
        Addition = $('#<%=txtAddition.ClientID%>').val();

        $('#<%=txtTotalPayable.ClientID%>').val((parseFloat(totBillAmt) - parseFloat(deduction) + parseFloat(Addition)).toFixed(2));

        $('#<%=txtTotalRow.ClientID%>').val(totalRow);
        $('#<%=txtTotalQty.ClientID%>').val(totQty.toFixed(2));
        $('#<%=txtNetBasicCost.ClientID%>').val(totBasicAmt.toFixed(2));
        $('#<%=txtNetDiscount.ClientID%>').val(totdiscountAmt.toFixed(2));
        $('#<%=txtNetShDiscAmt.ClientID%>').val(totShDiscAmt.toFixed(2));
        $('#<%=txtNetGrossCost.ClientID%>').val((parseFloat(totBasicAmt) - parseFloat(totdiscountAmt) - parseFloat(totShDiscAmt)).toFixed(2));

        return true;

    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Validation on Repeater Column Value Change
function fncRepeaterColumnValidation(rowObj, basicCost, mrp, sellingprice, netcost, wprice1, wprice2, wprice3) {
    try {

        if (basicCost == "" || parseFloat(basicCost) == 0) {

            //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtBasicCost"]');
            rowObj.find('td input[id*="txtBasicCost"]').select();
            fncToastInformation('<%=Resources.LabelCaption.alert_BasicCost%>');

            return false;
        }
        else if (parseFloat(basicCost) > parseFloat(mrp)) {

            //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtBasicCost"]');
            setTimeout(function () {
                rowObj.find('td input[id*="txtBasicCost"]').select();
            }, 20);
            fncToastInformation('<%=Resources.LabelCaption.alert_BasicLessthenMrp%>');
            return false;
        }
        else if (sellingprice == "" || sellingprice == 0) {

            //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtSellingPrice"]');
            setTimeout(function () {
                rowObj.find('td input[id*="txtSellingPrice"]').select();
            }, 20);
            fncToastInformation('<%=Resources.LabelCaption.alert_InvalidSellingPrice%>');

            return false;
        }
        else if (parseFloat(sellingprice) < parseFloat(netcost)) {
            //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtSellingPrice"]');
            setTimeout(function () {
                rowObj.find('td input[id*="txtSellingPrice"]').select();
            }, 20);
            fncToastInformation('<%=Resources.LabelCaption.alert_SellingPriceGreaterthenNetCost%>');

            return false;
        }
        else if (parseFloat(sellingprice) > parseFloat(mrp)) {
            //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtSellingPrice"]');
            setTimeout(function () {
                rowObj.find('td input[id*="txtSellingPrice"]').select();
            }, 20);
            fncToastInformation('<%=Resources.LabelCaption.alert_SellingPriceLessthenMrp%>');

            return false;
        }

    if ($('#<%=hidWholeSale.ClientID %>').val() == "Y") {
            if (wprice1 = "" || parseFloat(wprice1) == 0) {
                //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice1"]');
                setTimeout(function () {
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                }, 20);
                fncToastInformation('<%=Resources.LabelCaption.alert_invalidWproce%>');

                return false;
            }
            else if (parseFloat(wprice1) < parseFloat(netcost)) {

                //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice1"]');
                setTimeout(function () {
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                }, 20);
                fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

                return false;
            }
            else if (parseFloat(wprice1) > parseFloat(mrp)) {
                //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice1"]');
                setTimeout(function () {
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                }, 20);
                fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

                return false;
            }

    if (wprice2 == "" || parseFloat(wprice2) == 0) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice2"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice2"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_invalidWproce%>');

        return false;
    }
    else if (parseFloat(wprice2) < parseFloat(netcost)) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice2"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice2"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

        return false;;
    }
    else if (parseFloat(wprice2) > parseFloat(mrp)) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice2"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice2"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

        return false;
    }

    if (wprice3 == "" || parseFloat(wprice3) == 0) {

        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice3"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice3"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_invalidWproce%>');

        return false;
    }
    else if (parseFloat(wprice2) < parseFloat(wprice3)) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice3"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice2"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

        return false;
    }

    else if (parseFloat(wprice1) < parseFloat(wprice2)) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice3"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice1"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

        return false;
    }
    else if (parseFloat(wprice3) > parseFloat(mrp)) {
        //popUpObjectForSetFocusandOpen = rowObj.find('td input[id*="txtWPrice3"]');
        setTimeout(function () {
            rowObj.find('td input[id*="txtWPrice3"]').select();
        }, 20);
        fncToastInformation('<%=Resources.LabelCaption.alert_WpricegreaterthenNetcost%>');

        return false;
    }
}

    return true;
}
    catch (err) {
        fncToastError(err.message);
    }
}

var saveStatus = false;
//Save Validation
function fncSaveValidation() {
    try {
        $('#<%=lnkSave.ClientID %>').css("display", "none");
        saveStatus = false;
        var result = false, misQty = 0, misValue = 0;
        var rowObj, basicCost, mrp, sellingprice, netCost, wprice1, wprice2, wprice3, invQty, qty;
        var msg, totalGSTAmt;
        $('#<%=hidExcessSupplyAmt.ClientID%>').val('0.00');
        $('#<%=hidShortSupplyAmt.ClientID%>').val('0.00');

        if (parseFloat($('#<%=txtNetGrossAmt.ClientID%>').val()) == 0) {
            fncToastInformation('<%=Resources.LabelCaption.msg_grossAmt%>');
            $('#<%=txtNetGrossAmt.ClientID%>').select();
            $('#<%=lnkSave.ClientID %>').css("display", "block");
            return false;
        }

        if ($('#<%=hidGidGSTFlag.ClientID%>').val() == "3" || $('#<%=hidGidGSTFlag.ClientID%>').val() == "5" || $('#<%=hidGidGSTFlag.ClientID%>').val() == "6") {
            if (parseFloat($('#<%=txtBasicAmt.ClientID%>').val()) != parseFloat($('#<%=txtNetGrossAmt.ClientID%>').val())) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_GSTUnRegisteredValidation%>');
            $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
        }
        else {
            //totalGSTAmt = (parseFloat($('#<%=txtNetVat.ClientID%>').val()) + parseFloat($('#<%=txtNetCessAmt.ClientID%>').val())).toFixed(2);
            totalGSTAmt = $('#<%=txtTotalGSTCessAmt.ClientID%>').val();

            if (parseFloat($('#<%=txtBasicAmt.ClientID%>').val()) != parseFloat($('#<%=txtNetGrossAmt.ClientID%>').val())) {
                fncToastInformation('<%=Resources.LabelCaption.alert_GAGrossAmtEquqlToGRNGrossAmt%>');
                $('#<%=txtBasicAmt.ClientID%>').select();
            $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else if (parseFloat($('#<%=txtVatAmt.ClientID%>').val()) != totalGSTAmt) {
                fncToastInformation('<%=Resources.LabelCaption.alert_GAVatAmtEquqlToGRNVatAmt%>');
                $('#<%=txtVatAmt.ClientID%>').select();
            $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else if (parseFloat($('#<%=txtBillAmt.ClientID%>').val()) != parseFloat($('#<%=txtTotalBillAmt.ClientID%>').val())) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_GABillAmtEquqlToGRNBillAmt%>');
            $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            else if (parseFloat($('#<%=txttotPayable.ClientID%>').val()) != parseFloat($('#<%=txtTotalPayable.ClientID%>').val())) {
                fncToastInformation('GA Total Payable Amount should be equal to GRN Total Payable Amount ');
                $('#<%=txttotPayable.ClientID%>').select();
            $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
}


    $("#tblGRN [id*=GRNBodyrow]").each(function () {
        rowObj = $(this);
        basicCost = rowObj.find('td input[id*="txtBasicCost"]').val();
        netCost = rowObj.find('td input[id*="txtNetCost"]').val();
        mrp = rowObj.find('td input[id*="txtMRP"]').val();
        sellingprice = rowObj.find('td input[id*="txtSellingPrice"]').val();
        wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val();
        wprice2 = rowObj.find('td input[id*="txtWPrice2"]').val();
        wprice3 = rowObj.find('td input[id*="txtWPrice3"]').val();
        result = fncRepeaterColumnValidation(rowObj, basicCost, mrp, sellingprice, netCost, wprice1, wprice2, wprice3);

        if (result == false) { 
            $('#<%=lnkSave.ClientID %>').css("display", "block");
            return false;
        }


        invQty = rowObj.find('td input[id*="txtInvQty"]').val();
        qty = rowObj.find('td input[id*="txtlQty"]').val();

        //Create Credit and Debit Note
        if (parseFloat(qty) > parseFloat(invQty)) {
            misQty = parseFloat(qty) - parseFloat(invQty);
            misValue = parseFloat(netCost) * parseFloat(misQty);
            excessSupplyAmt = parseFloat(excessSupplyAmt) + parseFloat(misValue)
            $('#<%=hidExcessSupplyAmt.ClientID%>').val(excessSupplyAmt.toFixed(2));
            }
            else if (parseFloat(qty) < parseFloat(invQty)) {
                misQty = parseFloat(qty) - parseFloat(invQty);
                misValue = parseFloat(netCost) * parseFloat(misQty);
                shortSupplyAmt = parseFloat(shortSupplyAmt) + parseFloat(misValue)
                $('#<%=hidShortSupplyAmt.ClientID%>').val(shortSupplyAmt.toFixed(2));
        }

        });

    if (result == false)
        return result;

    result = fncShowCreditAndDebitNoteDialog();

    if (result == false)
        return result;

    if ($('#<%=hidCurLocation.ClientID%>').val() == "HQ" && $('#<%=hidGidLocationcode.ClientID%>').val() != $('#<%=hidCurLocation.ClientID%>').val()) {
        fncCreateTransferOut();
    }
    else {
        fncGetGridValuesForSave();
        fncGetMultiUOMForSave();
        if (saveStatus == false) {
            saveStatus = true;

            //=====================> Check Pending PO For Create New PO

            
            if ($('#<%=hdfPendingPOReCreate.ClientID%>').val() == 'Y') {
                var IsPendingPO = false;
                $("#tblGRN tbody tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        var PoQty = cells.eq(5).find('input').val().trim();
                        var InvQty = cells.eq(6).find('input').val().trim();
                        if (PoQty != InvQty) {
                            IsPendingPO = true;
                            return;
                        }
                    }
                });

                if (IsPendingPO == true) {
                    $("#dialog-confirm").dialog("open");  // Open Popup    
                    return false;
                }
                else {
                    $('#<%=btnSave.ClientID%>').click();
            }
        }
        else {
            $('#<%=btnSave.ClientID%>').click();
    }

}

}
    return false;

}
    catch (err) {
        fncToastError(err.message);
    }
}

///Create Transfer Out
function fncCreateTransferOut() {
    try {
        $(function () {
            $("#dialog-Transferout").html('<%=Resources.LabelCaption.msg_CreateGidTransferOut%>');
            $("#dialog-Transferout").dialog({
                title: "Enterpriser Web",
                buttons: {
                    Yes: function () {
                        $(this).dialog("destroy");
                        $('#<%=hidTransferout.ClientID%>').val('Y');
                        fncGetGridValuesForSave();
                        fncGetMultiUOMForSave();
                        if (saveStatus == false) {
                            saveStatus = true;
                            $('#<%=btnSave.ClientID%>').click();
                        }
                        //$('#<%=btnSave.ClientID%>').click();
                    },
                    No: function () {
                        $(this).dialog("destroy");
                        $('#<%=hidTransferout.ClientID%>').val('N');
                        fncGetGridValuesForSave();
                        fncGetMultiUOMForSave();
                        if (saveStatus == false) {
                            saveStatus = true;
                            $('#<%=btnSave.ClientID%>').click();
                        }
                        //$('#<%=btnSave.ClientID%>').click();
                    }
                },
                modal: true
            });
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncShowSerialNumberConfirm(message) {

    $("#dialog-MessageBox").html(message);
    $("#dialog-MessageBox").dialog({
        title: "Enterpriser Web",
        buttons: {
            Yes: function () {
                $(this).dialog("destroy");
                fncOpenSerialNumber();
            },
            No: function () {
                $(this).dialog("destroy");
                __doPostBack('ctl00$ContentPlaceHolder1$lnkbtnOk', '');
            }
        },
        modal: true

    });
};

function fncOpenSerialNumber() {
    try {
        var page = '<%=ResolveUrl("~/Purchase/frmSerialNumberEntry.aspx") %>';
        page = page + "?GIDNO=" + $("#<%=txtGrnNo.ClientID%>").val() + "&VendorCode=" + $("#<%=txtVendorCode.ClientID%>").val();
        page = page + "&Date=" + $("#<%=txtgrndate.ClientID%>").val()
        window.location = page;

    }
    catch (err) {
        fncToastError(err.message);
    }

}

///Credit and Debit Confirmation
function fncShowCreditAndDebitNoteDialog() {
    try {
        ///Confirmation for Creadit Nort
        if (parseFloat(excessSupplyAmt) > 0 && excessSupplyStatus == "") {
            msg = excessSupplyAmt.toFixed(2) + '<%=Resources.LabelCaption.alert_ExcessSupply%>';
            $('#<%=lblCreditandDebitNote.ClientID%>').text(msg);
            fncInitializeCreditandDebitNote();
            excessSupplyStatus = "Called";
            return false;
        }

        ///Confirmation For Debit Note
        if (parseFloat(shortSupplyAmt) < 0 && shortSupplyStatus == "") {
            msg = shortSupplyAmt.toFixed(2) + '<%=Resources.LabelCaption.alert_ShortSupply%>';
            $('#<%=lblCreditandDebitNote.ClientID%>').text(msg);
            fncInitializeCreditandDebitNote();
            shortSupplyStatus = "Called";
            return false;
        }

        return true;
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Reverse Calculation for MarDown
function fncMRPFocusOut() {
    try {

        var mrp, MarkDownPerc, taxPerc1, taxPerc2, sumITaxPerc;
        var taxAmt1, taxAmt2, netCost, grossCost, refNetCost, netSellingPrice;
        var basicCost, discAmt, shDiscAmt, actualGST, basicSelling, outPutGSt;
        var cessper, cessAmt = 0, prvMargin = 0, addCessAmt = 0, locNetCost = 0, prvMRP = 0;
        var prvSelling = 0;


        basicCost = $('#<%=txtBCost.ClientID%>').val();
        mrp = $('#<%=txtMRP.ClientID%>').val();
        prvMRP = $('#<%=hidPrvMRP.ClientID%>').val();
        netSellingPrice = $('#<%=txtSPrice.ClientID%>').val();
        prvSelling = $('#<%=hidOldSellingPrice.ClientID%>').val();


        if ($('#<%=hidPurchasedBy.ClientID%>').val().trim() == "M") {
            MarkDownPerc = $('#<%=hidMarkDownPerc.ClientID%>').val();
            taxPerc1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val());//!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0";// $('#<%=txtITaxper1.ClientID%>').val();
            taxPerc2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val());//!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0";//$('#<%=txtITaxper2.ClientID%>').val();
            cessper = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();
            discAmt = $('#<%=txtDiscAmt.ClientID%>').val();
            shDiscAmt = $('#<%=txtSchemeAmt.ClientID%>').val();
            addCessAmt = $('#<%=txtAddCess.ClientID%>').val();

            sumITaxPerc = parseFloat(taxPerc1) + parseFloat(taxPerc2) + parseFloat(cessper);

            netCost = parseFloat(mrp) - (parseFloat(mrp) * parseFloat(MarkDownPerc)) / 100;
            locNetCost = parseFloat(netCost) - parseFloat(addCessAmt);
            actualGST = (parseFloat(locNetCost) * parseFloat(sumITaxPerc)) / (100 + parseFloat(sumITaxPerc));
            if (parseFloat(sumITaxPerc) > 0) {
                cessAmt = (parseFloat(actualGST) / parseFloat(sumITaxPerc)) * parseFloat(cessper);
            }

            actualGST = parseFloat(actualGST) - parseFloat(cessAmt);
            grossCost = parseFloat(locNetCost) - (parseFloat(actualGST) + parseFloat(cessAmt));
            basicCost = parseFloat(grossCost) + parseFloat(discAmt) + parseFloat(shDiscAmt);
            //actualGST = fncTaxAmtCalculation(grossCost, taxPerc1, taxPerc2);

            //refNetCost = parseFloat(grossCost) + parseFloat(actualGST);
            //grossCost = parseFloat(grossCost) + parseFloat(netCost) - parseFloat(refNetCost);
            //basicCost = parseFloat(grossCost) + parseFloat(discAmt) + parseFloat(shDiscAmt);

            if ($('#<%=hidMarginFixType.ClientID%>').val() == "M") {
                netSellingPrice = parseFloat(mrp) - (parseFloat(mrp) * parseFloat($('#<%=txtMFixed.ClientID%>').val())) / 100;
                outPutGSt = fncArriveInclusiveTax(netSellingPrice, taxPerc1, taxPerc2, cessper);
                basicSelling = parseFloat(netSellingPrice) - parseFloat(outPutGSt) - parseFloat(addCessAmt);
                $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));

            }
            else {
                netSellingPrice = parseFloat(netCost) + (parseFloat(netCost) * parseFloat($('#<%=txtMFixed.ClientID%>').val())) / 100;
                basicSelling = parseFloat(netSellingPrice) - parseFloat(actualGST) - parseFloat(addCessAmt);
                $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));

            }

            //netCost = parseFloat(netCost) + parseFloat(addCessAmt);
            $('#<%=txtBCost.ClientID%>').val(basicCost.toFixed(4));
            $('#<%=txtGrossCost.ClientID%>').val(grossCost.toFixed(4));
            $('#<%=txtNetCost.ClientID%>').val(netCost.toFixed(4));
            $('#<%=txtSPrice.ClientID%>').val(SellingPriceRoundOff(netSellingPrice));
            $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));

            fncSubTotalCalculation(basicCost, grossCost, discAmt, shDiscAmt, actualGST, netCost, cessAmt, addCessAmt)

        }
        else {
            if (parseFloat($('#<%=hidPrvMRP.ClientID%>').val()) > 0) {

                // Arrive Basic Cost from Previous Purchase
                prvMargin = parseFloat($('#<%=hidPrvMRP.ClientID%>').val()) / parseFloat($('#<%=hidPrvUnitCost.ClientID%>').val()) * 100;
                if (glwcpforcost == 0) {
                    basicCost = parseFloat($('#<%=txtMRP.ClientID%>').val()) / parseFloat(prvMargin) * 100;
                }
                else {
                    basicCost = parseFloat(basicCost);
                    glwcpforcost = 0;
                }
                $('#<%=txtBCost.ClientID%>').val(basicCost.toFixed(4));

                //// Arrive Selling Price From Previous Purchase
                //hidsellingCalFromPrvMRP

                if (parseFloat(mrp) != parseFloat(prvMRP)) {
                    if ((parseFloat(mrp) - parseFloat(prvMRP)) > 0 && parseFloat(prvMRP) != 0 && parseFloat(mrp) != 0) {
                        netSellingPrice = parseFloat(prvSelling) + ((parseFloat(prvSelling) * ((parseFloat(mrp) - parseFloat(prvMRP)) / parseFloat(mrp)) * 100) / 100);
                    }

                    if ((parseFloat(mrp) - parseFloat(prvMRP)) < 0 && parseFloat(prvMRP) != 0 && parseFloat(mrp) != 0) {
                        if ($('#<%=hidsellingCalFromPrvMRP.ClientID%>').val() == "Y") {
                            netSellingPrice = parseFloat(mrp) - ((((parseFloat(prvMRP) - parseFloat(prvSelling)) / parseFloat(prvMRP) * 100) * parseFloat(mrp)) / 100);
                        }
                        else {
                            netSellingPrice = parseFloat(prvSelling) - (parseFloat(prvSelling) * (((parseFloat(prvMRP) - parseFloat(mrp)) / parseFloat(mrp)) * 100) / 100);
                        }
                    }
                }

                $('#<%=txtSPrice.ClientID%>').val(SellingPriceRoundOff(netSellingPrice));

                fncBasicCostFocusOut();
                $('#<%=txtBCost.ClientID%>').css("background-color", "lightgreen");
            }
        }


    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Calculation for Basic Cost Change
function fncBasicCostFocusOut() {
    try {

        var basicCost, discAmt, shDiscAmt, grossCost, taxAmt1, taxAmt2;
        var taxPerc1, taxPerc2, netCost = 0, actualGST, basicSelling, earnedMarginPerc;
        var sellingPrice, cessper, cessAmt, actualTaxForCess;
        var outputGST = 0, ouputcessAmt = 0, addCessAmt = 0;



        basicCost = $('#<%=txtBCost.ClientID%>').val();
        discAmt = $('#<%=txtDiscAmt.ClientID%>').val();
        shDiscAmt = $('#<%=txtSchemeAmt.ClientID%>').val();
        taxPerc1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val());//!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0"; //$('#<%=txtITaxper1.ClientID%>').val();
        taxPerc2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val());//!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0"; //$('#<%=txtITaxper2.ClientID%>').val();
        cessper = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();
        earnedMarginPerc = $('#<%=txtMFixed.ClientID%>').val();
        sellingPrice = $('#<%=txtSPrice.ClientID%>').val();
        basicSelling = $('#<%=hidBasicSellingPrice.ClientID%>').val();
        addCessAmt = $('#<%=txtAddCess.ClientID%>').val();


        grossCost = parseFloat(basicCost) - parseFloat(discAmt) - parseFloat(shDiscAmt);
        actualGST = fncTaxAmtCalculation(grossCost, taxPerc1, taxPerc2);
        cessAmt = fncCessCalc(grossCost, cessper);

        netCost = parseFloat(grossCost) + parseFloat(actualGST) + parseFloat(cessAmt);// + parseFloat(addCessAmt);

        if (parseFloat(earnedMarginPerc) > 0) {

            basicSelling = parseFloat(grossCost) + (parseFloat(grossCost) * parseFloat(earnedMarginPerc) / (100));

            outputGST = fncTaxAmtCalculation(basicSelling, taxPerc1, taxPerc2);
            ouputcessAmt = fncCessCalc(basicSelling, cessper);
            sellingPrice = parseFloat(basicSelling) + parseFloat(outputGST) + parseFloat(ouputcessAmt);
            sellingPrice = parseFloat(sellingPrice) + parseFloat(addCessAmt);
        }
        else {
            basicSelling = parseFloat(basicSelling);
            sellingPrice = parseFloat(sellingPrice);
        }

        netCost = parseFloat(netCost) + parseFloat(addCessAmt)
        $('#<%=txtGrossCost.ClientID%>').val(grossCost.toFixed(4));
        $('#<%=txtNetCost.ClientID%>').val(netCost.toFixed(4));
        $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));
        $('#<%=txtSPrice.ClientID%>').val(SellingPriceRoundOff(sellingPrice));

        fncSubTotalCalculation(basicCost, grossCost, discAmt, shDiscAmt, actualGST, netCost, cessAmt, addCessAmt)

        fncDiscountandShemeDiscCalc('Discper');
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Alter GA Amount Calculation
function fncGAAmountCalculation() {
    var grossAmt, gstAmt, billAmt, addDeductionAmt;
    try {
        grossAmt = $('#<%=txtBasicAmt.ClientID%>').val();
        gstAmt = $('#<%=txtVatAmt.ClientID%>').val();
        billAmt = parseFloat(grossAmt) + parseFloat(gstAmt);
        addDeductionAmt = $('#<%=txtAD.ClientID%>').val();

        $('#<%=txtBillAmt.ClientID%>').val(billAmt.toFixed(2));
        $('#<%=txttotPayable.ClientID%>').val((parseFloat(billAmt) + parseFloat(addDeductionAmt)).toFixed(2));
        return false;
    }
    catch (err) {
        fncToastError(err.message);
    }
}
function fncArriveBasicSellingPrice() {
    try {
        var basicSelling, netSelling, taxPerc1, taxPerc2, cessPer, gstAmt;
        var SellingEarnedPerc = 0, SellingEarnedAmount = 0;
        var vatliabilty = 0, actualGST = 0, cessAmt = 0, grossCost = 0;
        var addCessAmt = 0;

        addCessAmt = $('#<%=txtAddCess.ClientID%>').val();
        netSelling = $('#<%=txtSPrice.ClientID%>').val();
        taxPerc1 = fncConvertFloatJS($('#<%=txtITaxper1.ClientID%>').val()); //!isNaN($('#<%=txtITaxper1.ClientID%>').val()) ? $('#<%=txtITaxper1.ClientID%>').val() : "0";//$('#<%=txtITaxper1.ClientID%>').val();
        taxPerc2 = fncConvertFloatJS($('#<%=txtITaxper2.ClientID%>').val());//!isNaN($('#<%=txtITaxper2.ClientID%>').val()) ? $('#<%=txtITaxper2.ClientID%>').val() : "0";//$('#<%=txtITaxper2.ClientID%>').val();
        cessPer = fncConvertFloatJS($('#<%=txtcess.ClientID%>').val());//!isNaN($('#<%=txtcess.ClientID%>').val()) ? $('#<%=txtcess.ClientID%>').val() : "0";//$('#<%=txtcess.ClientID%>').val();
        gstAmt = fncArriveInclusiveTax(netSelling, taxPerc1, taxPerc2, cessPer);
        basicSelling = parseFloat(netSelling) - parseFloat(gstAmt) - parseFloat(addCessAmt);
        $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));


        ///Arrive Earned Margin Percentage and Amount
        grossCost = $('#<%=txtGrossCost.ClientID%>').val();

        actualGST = fncTaxAmtCalculation(grossCost, taxPerc1, taxPerc2);
        cessAmt = fncCessCalc(grossCost, cessPer);

        vatliabilty = (parseFloat(gstAmt) - parseFloat(actualGST) - parseFloat(cessAmt)).toFixed(2);
        SellingEarnedAmount = (parseFloat(netSelling) - parseFloat($('#<%=txtNetCost.ClientID%>').val()) - parseFloat(vatliabilty)).toFixed(2);
        SellingEarnedPerc = (parseFloat(SellingEarnedAmount) / 100 * parseFloat(netSelling)).toFixed(2);


        $('#<%=txtEarned.ClientID%>').val(SellingEarnedPerc); // Earned Margin Per
        $('#<%=hidMFAmt.ClientID%>').val(SellingEarnedAmount);  // Earned Margin Amount

    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Selling Price Round Off
function SellingPriceRoundOff(SellingPrice) {

    if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '1') {
        return (Math.round(SellingPrice * 2)).toFixed(2); //1 Paise RoundOff
    }
    else if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '50') {
        return (Math.round(SellingPrice * 2) / 2).toFixed(2); //50 Paise RoundOff
    }
    else if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '25') {
        return (Math.round(SellingPrice * 4) / 4).toFixed(2); //25 Paise RoundOff
    }
    else {
        return SellingPrice;

    }
}

function fncEnablePriceChangeAlert(mode, itemname) {
    try {
        if (mode == "Enable") {
            $('#divgaDet1').css("display", "none");
            $('#divgaDet2').css("display", "none");
            $('#divBarcode1').css("display", "none");
            $('#divVendor').css("display", "none");
            $('#divPriceChaneAlert').css("display", "block");
            $('#divPrcItemname').css("display", "block");
            $('#<%=lblPrcItemname.ClientID%>').text(itemname);
        }
        else {
            $('#divgaDet1').css("display", "block");
            $('#divgaDet2').css("display", "block");
            $('#divBarcode1').css("display", "block");
            $('#divVendor').css("display", "block");
            $('#divPriceChaneAlert').css("display", "none");
            $('#divPrcItemname').css("display", "none");
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Arrive Basic Selling from Net Selling
function fncSellingPriceFocusOut() {
    try {

        fncArriveBasicSellingPrice();
        if (parseFloat($('#<%=txtSPrice.ClientID%>').val()) != parseFloat($('#<%=hidOldSellingPrice.ClientID%>').val())) {

            fncEnablePriceChangeAlert("Enable", $('#<%=txtDescription.ClientID%>').val());

            if ($('#<%=hidBreakpricestatus.ClientID%>').val() == "1") {
                fncOpenItemhistory($('#<%=txtItemCode.ClientID %>').val());
                fncPriceChangeAlertNew();
            }
            else if ($('#<%=hidItemType.ClientID%>').val().trim() == "BULK" && $('#<%=hidChildPriceChangeAllLocation.ClientID%>').val() == "Y") {
                fncOpenQuickPriceChange($('#<%=txtItemCode.ClientID %>').val().trim());
            }
            else if ($('#<%=hidItemType.ClientID%>').val().trim() == "BULK") {
                $('#<%=txtBulItemcode.ClientID%>').val($('#<%=txtItemCode.ClientID%>').val());
                $('#<%=txtBulkItemDesc.ClientID%>').val($('#<%=txtDescription.ClientID%>').val());
                $('#<%=txtBulkMRP.ClientID%>').val(parseFloat($('#<%=txtMRP.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkCost.ClientID%>').val(parseFloat($('#<%=txtBCost.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkSPrice.ClientID%>').val(parseFloat($('#<%=txtSPrice.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkNewSPrice.ClientID%>').val(parseFloat($('#<%=txtSPrice.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkWPrice1.ClientID%>').val(parseFloat($('#<%=txtWpirce1.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkWPrice2.ClientID%>').val(parseFloat($('#<%=txtWpirce2.ClientID%>').val()).toFixed(2));
                $('#<%=txtBulkWPrice3.ClientID%>').val(parseFloat($('#<%=txtWpirce3.ClientID%>').val()).toFixed(2));
                fncGetChildItemList();
                setTimeout(function () {
                    $('#<%=txtBulkSPrice.ClientID%>').select();
                }, 20);

            }
            else {


                fncPriceChangeAlertNew();
            }
}
}
    catch (err) {
        fncToastError(err.message);
    }

}
function fncPriceChangeAlertNew() {
    try {
        $('#<%=txtoldCost.ClientID%>').val($('#<%=hidOldCost.ClientID%>').val());
        $('#<%=txtOldMRP.ClientID%>').val($('#<%=hidOldMRP.ClientID%>').val());
        $('#<%=txtOldSelling.ClientID%>').val($('#<%=hidOldSellingPrice.ClientID%>').val());
        $('#<%=txtoldMarginfixed.ClientID%>').val($('#<%=hidoldMarFixed.ClientID%>').val());
        $('#<%=txtoldMarginEarned.ClientID%>').val($('#<%=hidoldMarEarned.ClientID%>').val());


        $('#<%=txtNewCost.ClientID%>').val($('#<%=txtBCost.ClientID%>').val());
        $('#<%=txtNewMRP.ClientID%>').val($('#<%=txtMRP.ClientID%>').val());
        $('#<%=txtNewSelling.ClientID%>').val($('#<%=txtSPrice.ClientID%>').val());
        $('#<%=txtNewMarginFixed.ClientID%>').val($('#<%=txtMFixed.ClientID%>').val());
        $('#<%=txtNewMarginEarned.ClientID%>').val($('#<%=txtEarned.ClientID%>').val());
    }
    catch (err) {
        fncToastError(err.message);
    }
}


/// Open Item 
function fncOpenItem() {
    var page = '<%=ResolveUrl("~/Inventory/frmInventoryMastertextiles.aspx") %>';
    var page = page + "?Status=dailog";
    var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 630,
        width: 1200,
        title: "Inventory Master"
    });
    $dialog.dialog('open');
}

/// Open Item History
function fncOpenItemhistory(itemcode) {
    var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
    var page = page + "?InvCode=" + itemcode + "&Status=dailog";
    var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 630,
        width: 1200,
        title: "Inventory History"
    });
    $dialog.dialog('open');
}

/// Open Item 
function fncOpenQuickPriceChange(itemcode) {
    var page = '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx") %>';
    var page = page + "?Itemcode=" + itemcode + "&Status=dialog";
    var $dialog = $('<div id="popupQuickPriceChange" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 630,
        width: 1200,
        title: "Quick Price Change"
    });
    $dialog.dialog('open');
}

///Arrive Inclusive Tax
function fncArriveInclusiveTax(value, taxperc1, txtperc2, cessper) {
    try {

        if (txtperc2 == "")
            txtperc2 = 0;

        var totalGstPerc = parseFloat(taxperc1) + parseFloat(txtperc2) + parseFloat(cessper);
        return parseFloat(value) * parseFloat(totalGstPerc) / (100 + parseFloat(totalGstPerc));
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Initialize Save Dialog
function fncInitializeSaveDialog(SaveMsg, mode, serialNumCount) {
    try {

        fncSubClear('clr');

        if (mode == "SerialNum") {
            SaveMsg = SaveMsg + '<%=Resources.LabelCaption.save_Gid%>' + '.';
            SaveMsg = SaveMsg + '<br /> ' + serialNumCount + ' Items Required IMEI Details ';
            SaveMsg = SaveMsg + '<br /> Do you want to Enter Now? ';
            fncShowSerialNumberConfirm(SaveMsg);
        }
        else {

            if (mode == "GidSave")
                SaveMsg = SaveMsg + '<%=Resources.LabelCaption.save_Gid%>';
            else
                SaveMsg = SaveMsg + '<%=Resources.LabelCaption.save_GidHold%>';

            $('#<%=lblGidSave.ClientID %>').text(SaveMsg);
            $("#GidSave").dialog({
                resizable: false,
                height: 130,
                width: 325,
                modal: true,
                title: "Enterpriser Web"
            });
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

///Calculation for 
function fncLoadHoldClosedGid(mode) {
    try {
        
        fncLoadDiscountinueHoldClosed();

        if (mode == "PDA") {
            $("#tblGRN [id*=GRNBodyrow]").each(function () {
                fncRepeaterColumnValueChange(this, 'InvQty', 'LoadQty');
            });
        }


    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Load Po Qty
function fncLoadQtyclick() {
    try {

        var status, qty

        $("#tblGRN [id*=GRNBodyrow]").each(function () {
            rowObj = $(this);
            if (rowObj.find('td input[id*="txtPONo"]').val() != "0") {

                status = $.inArray(rowObj.find('td input[id*="txtSNo"]').val(), changeRow) > -1
                if (status == false) {
                    changeRow.push(rowObj.find('td input[id*="txtSNo"]').val());
                    $("#<%=hidChangeRowval.ClientID %>").val(changeRow);
                }

                qty = rowObj.find('td input[id*="txtPOQty"]').val();
                rowObj.find('td input[id*="txtInvQty"]').val(qty);
                fncRepeaterColumnValueChange(this, 'InvQty', 'LoadQty');

            }

        });



    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Foc Work
function fncInvQty_KeyPress(evt) {


    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var item = $("#<%=txtItemCode.ClientID%>").val().trim();

    item = item + "-" + $("#<%=txtDescription.ClientID%>").val();
    if (charCode == 117) {
        if ($("#<%=txtGrnNo.ClientID%>").val() != "") {
            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_cannothold%>');
            return;
        }
        else if ($("#tblGRN [id*=GRNBodyrow]").length > 0) {
            fncGetGridValuesForSave();
            fncGetMultiUOMForSave();
            $("#<%=btnHold.ClientID%>").click();
        }
        else {
            fncInitializeGidHold();
            SetGIDHoldRecord();
        }
}

else if (charCode != 46 && charCode > 31
&& (charCode < 48 || charCode > 57)) {
    return false;
}
    return true;
}

function fncOpenFOCScreen(item) {
    try {

        setTimeout(function () {
            $("#<%=cbSalesNotAffectNetCost.ClientID%>").focus();
        }, 10);

        $("#<%=cbSalesNotAffectNetCost.ClientID%>").prop('checked', true);
        $("#<%=cbFreetoCustomer.ClientID%>").prop('checked', false);
        fncEnableDisableFTC(true)

        $("#<%=txtQualifyingItem.ClientID%>").val(item);
        $("#<%=txtQualifyingQty.ClientID%>").val(parseFloat($("#<%=txtInvQty.ClientID%>").val()).toFixed(2));

        $("#<%=txtFocItem.ClientID%>").val('');
        $("#<%=txtFocTotalQty.ClientID%>").val('');
        $("select[id$=ddlfocBatch] > option").remove();

        //$('#<%=ddlfocBatch.ClientID %>').trigger("liszt:updated");

        $("#<%=hidFocItem.ClientID%>").val('');
        $("#<%=hidFocBatch.ClientID%>").val('');
        $("#<%=hidFocQty.ClientID%>").val('0.00');
        $("#<%=hidFocStatus.ClientID%>").val('');

        $("#<%=cbSalesNotAffectNetCost.ClientID%>").attr("checked", "checked");

        fncEnableDisableFTC(true);
        focStatus = "Open";
        $("#divFoc").dialog({
            resizable: false,
            height: 230,
            width: 600,
            modal: true,
            title: "Foc",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}
//Weight based
function fncOpenWeightconQtyScreen(item) {
    try {

        $("#<%=txtwcpitem.ClientID%>").val(item);
        $("#divWCP").dialog({
            resizable: false,
            height: 240,
            width: 600,
            modal: true,
            title: "Weight Convert To PCS",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Enable and Disable for Free to Customer Control
function fncEnableDisableFTC(status) {
    try {
        $("#<%=txtfocFromDate.ClientID%>").attr("disabled", status);
        $("#<%=txtFocToDate.ClientID%>").attr("disabled", status);
        $("#<%=txtFocBuyQty.ClientID%>").attr("disabled", status);
        $("#<%=txtFocFreeQty.ClientID%>").attr("disabled", status);

        if (status == true) {
            $("#<%=txtFocBuyQty.ClientID%>").val(0);
            $("#<%=txtFocFreeQty.ClientID%>").val(0);
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Foc Calculation
function fncFocModeClick(value) {
    try {

        if (value == "salesNotAffect") {
            $("#<%=cbFreetoCustomer.ClientID%>").removeAttr("checked");
            fncEnableDisableFTC(true);

        }
        else {
            fncEnableDisableFTC(false);
            $("#<%=cbSalesNotAffectNetCost.ClientID%>").removeAttr("checked");

        }

    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Get Batch Detail
function fncGetBatchDetail() {
    try {
        //var charCode = (evt.which) ? evt.which : evt.keyCode;
        //if (charCode == 13) {

            <%--if ($('#<%=txtFocItem.ClientID%>').val().trim() == "") {
                $("#divFoc").dialog('destroy');
                $('#<%=txtMRP.ClientID%>').select();
            }
            else {
                fncGetFOCBatch();
            }--%>
        var obj = {};
        var items;
        var focItem;
        focItem = $('#<%=txtFocItem.ClientID%>').val();
        obj.searchData = focItem.split('-')[0];


        obj.franchisevalue = $('#<%=hidFranValue.ClientID %>').val();
        obj.mode = "Itemcode";

        $.ajax({
            url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncFocItemSearch")%>',
            //data: "{ 'prefix': '" + request.term + "'}",
            data: JSON.stringify(obj),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data.d != "") {
                    items = data.d.toString();
                    $('#<%=txtFocItem.ClientID %>').val(items.split('|')[1] + "-" + items.split('|')[2]);
                    fncGetFOCBatch();
                }
                else {
                    $('#<%=txtFocItem.ClientID %>').val('');
                    var ddlBactno = $('#<%=ddlfocBatch.ClientID%>');
                    $("select[id$=ddlfocBatch] > option").remove();
                    $('#<%=txtFocItem.ClientID %>').select();
                }
            },
            error: function (response) {
                ShowPopupMessageBox(response.message);
            }
        });


        return false;

        //}
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncFOCItemEnter(evt) {
    try {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            if ($('#<%=txtFocItem.ClientID%>').val().trim() == "") {
                $("#divFoc").dialog('destroy');
                $('#<%=txtMRP.ClientID%>').select();
            }
            else if ($('#<%=ddlfocBatch.ClientID%>').val() != null) {
                fncOpenDropdownlist($('#<%=ddlfocBatch.ClientID %>'));
                //$('#<%=ddlfocBatch.ClientID %>').select().focus();
            }
            else {
                $('#<%=txtFocTotalQty.ClientID %>').select().focus();
            }
        return false;
    }
}
    catch (err) {
        fncToastError(err.message);
    }
}

//// Get Foc Batch Detail
function fncGetFOCBatch() {
    try {
        var focItem;
        focItem = $('#<%=txtFocItem.ClientID%>').val();
        focItem = focItem.split('-')[0];

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("~/WebService.asmx/GetBatchNo")%>',
            data: "{'sInvCode':'" + focItem + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                var ddlBactno = $('#<%=ddlfocBatch.ClientID%>');
                $("select[id$=ddlfocBatch] > option").remove();

                $.each(msg.d, function () {
                    ddlBactno.append($("<option></option>").val(this['Value']).html(this['Text']));
                });

                $('#<%=ddlfocBatch.ClientID %>').trigger("liszt:updated");
                if ($('#<%=ddlfocBatch.ClientID%>').val() != null) {
                    fncOpenDropdownlist($('#<%=ddlfocBatch.ClientID %>'));
                    //$('#<%=ddlfocBatch.ClientID %>').select().focus();
                }
                else {
                    $('#<%=txtFocTotalQty.ClientID %>').select().focus();
                }

            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Set Focus To Foc Qty
function fncFocBatchSelect() {
    try {

        //setTimeout(function () {
        $('#<%=txtFocTotalQty.ClientID %>').select().focus();
        //}, 10);

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncCloseFocDialog(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var focQty, focItem, focBatch;
    try {

        if (charCode == 13) {

            focQty = $('#<%=txtFocTotalQty.ClientID %>').val();

            if (focQty == "" || parseFloat(focQty) == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_InvalidFocQty%>');
                return;
            }
            else if ($('#<%=txtFocItem.ClientID %>').val() == "") {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_FocItem%>');
                return;
            }

        focItem = $('#<%=txtFocItem.ClientID%>').val();
            focItem = focItem.split('-')[0];
            focBatch = $('#<%=ddlfocBatch.ClientID%>').val();

            if ($("#<%=cbSalesNotAffectNetCost.ClientID%>").is(":checked")) {
                $('#<%=hidFocStatus.ClientID %>').val('SalesNotAffectNC');
                $('#<%=hidFocItem.ClientID %>').val(focItem);
                $('#<%=hidFocBatch.ClientID %>').val(focBatch);
                $('#<%=hidFocQty.ClientID%>').val(focQty);
                fncClearFOCItem();
            }

            else if ($("#<%=cbFreetoCustomer.ClientID%>").is(":checked")) {
                $('#<%=txtfocFromDate.ClientID %>').select();
            }

        return false;
    }
}

    catch (err) {
        fncToastError(err.message);
    }
}

function fncFTCFreeQtyEnterEvent(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var focItem = "", focBatch = "", focQty = 0;
    try {
        if (charCode == 13) {

            focQty = $('#<%=txtFocTotalQty.ClientID %>').val();

            if (focQty == "") {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_InvalidFocQty%>');
                return;
            }
            else if (new Date($('#<%=txtfocFromDate.ClientID %>').val()) < new Date(currentDateformat_Master)) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_fromdategreaterthencurDate%>');
                    return;
                }
                else if (new Date($('#<%=txtFocToDate.ClientID %>').val()) < new Date($('#<%=txtfocFromDate.ClientID %>').val())) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_TodategreaterthenFromDate%>');
                    return;
                }
                else if ($('#<%=txtFocBuyQty.ClientID %>').val() == "" || parseFloat($('#<%=txtFocBuyQty.ClientID %>').val()) == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BuyQty%>');
                return;
            }
            else if ($('#<%=txtFocFreeQty.ClientID %>').val() == "" || parseFloat($('#<%=txtFocFreeQty.ClientID %>').val()) == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_FreeQty%>');
                return;
            }

    focItem = $('#<%=txtFocItem.ClientID%>').val();
            focItem = focItem.split('-')[0];
            focBatch = $('#<%=ddlfocBatch.ClientID%>').val();

            $('#<%=hidFocStatus.ClientID %>').val('FTC');
            $('#<%=hidFTCItemcode.ClientID %>').val(focItem);
            $('#<%=hidFTCBatchNo.ClientID %>').val(focBatch);
            $('#<%=hidFTCInputQty.ClientID%>').val(focQty);

            $("#divFoc").dialog('destroy');
            $('#<%=txtMRP.ClientID%>').select();

            return false;

        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncClearFOCItem() {
    try {

        $("#<%=cbFreetoCustomer.ClientID%>").prop('checked', true);
            $("#<%=cbSalesNotAffectNetCost.ClientID%>").prop('checked', false);
            $('#<%=txtFocItem.ClientID%>').val('');
            $('#<%=txtFocTotalQty.ClientID%>').val('0');
            $("select[id$=ddlfocBatch] > option").remove();
            $('#<%=txtFocItem.ClientID%>').select();
            fncFocModeClick('FTC');

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    ///Initialize Gid Hold Dialog
    function fncInitializeGidHold() {
        try {
            $("#divGidHold").dialog({
                resizable: false,
                height: 150,
                width: 250,
                modal: true,
                title: "GRN Hold Record",
                appendTo: 'form:first'
            });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    ///Initialize Price Change Alert
    function fncPriceChangeAlert() {
        try {
            $("#divPriceChaneAlert").dialog({
                resizable: false,
                height: 290,
                width: 600,
                modal: true,
                title: "Price Chart",
                appendTo: 'form:first',
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");

                        if ($('#<%=hidWholeSale.ClientID%>').val() == "Y") {
                        $('#<%=txtWpirce1.ClientID%>').select();
                    }
                    else {
                        $('#<%=lnkAdd.ClientID%>').focus();
                    }
                }
            },
        });

        setTimeout(function () {
            $("#divPriceChaneAlert").parent().find("button:eq(1)").focus();
        }, 50);


    }
    catch (err) {
        fncToastError(err.message);
    }
}

//// Enable All Disable Columns/// 
function fncEnableAllColumns() {

    try {
        $('#thBasicCost').css("display", "");
        $('[id*="tdBasicCost_"]').css("display", "");
        $('#thSellingPrice').css("display", "");
        $('[id*="tdSellingPrice_"]').css("display", "");
        $('#thSellingPrice').css("display", "");
        $('[id*="tdSellingPrice_"]').css("display", "");
        $('#thWPrice1').css("display", "");
        $('[id*="tdWPrice1_"]').css("display", "");
        $('#thWPrice2').css("display", "");
        $('[id*="tdWPrice2_"]').css("display", "");
        $('#thWPrice3').css("display", "");
        $('[id*="tdWPrice3_"]').css("display", "");
        $('#thGrossBasicAmt').css("display", "");
        $('[id*="tdGrossBasicAmt_"]').css("display", "");
        $('#thDiscPer').css("display", "");
        $('[id*="tdDiscPer_"]').css("display", "");
        $('#thDiscAmt').css("display", "");
        $('[id*="tdDiscAmt_"]').css("display", "");
        $('#thGrossDiscAmt').css("display", "");
        $('[id*="tdGrossDiscAmt_"]').css("display", "");
        $('#thSDPer').css("display", "");
        $('[id*="tdSDPer_"]').css("display", "");
        $('#thSDAmt').css("display", "");
        $('[id*="tdSDAmt_"]').css("display", "");
        $('#thGrossShDiscAmt').css("display", "");
        $('[id*="tdGrossShDiscAmt_"]').css("display", "");
        $('#thGrossCost').css("display", "");
        $('[id*="tdGrossCost_"]').css("display", "");
        $('#thTotalGrossCost').css("display", "");
        $('[id*="tdTotalGrossCost_"]').css("display", "");
        $('#thSGstPerc').css("display", "");
        $('[id*="tdITaxperc1_"]').css("display", "");
        $('#thCGstPerc').css("display", "");
        $('[id*="tdITaxperc2_"]').css("display", "");
        $('#thCessPer').css("display", "");
        $('[id*="tdCessper_"]').css("display", "");
        $('#thSGstAmt').css("display", "");
        $('[id*="tdITaxAmt1_"]').css("display", "");
        $('#thCGstAmt').css("display", "");
        $('[id*="tdITaxAmt2_"]').css("display", "");
        $('#thCESSAmt').css("display", "");
        $('[id*="tdCessAmt_"]').css("display", "");
        $('#thGrossGSTAmt').css("display", "");
        $('[id*="tdGrossVATAmt_"]').css("display", "");
        $('#thGrossCessAmt').css("display", "");
        $('[id*="tdGrocessCessAmt_"]').css("display", "");
        $('#thNetCost').css("display", "");
        $('[id*="tdtNetCost_"]').css("display", "");
        $('#thTotalValue').css("display", "");
        $('[id*="tdTotalValue_"]').css("display", "");
        $('#thFTotalValue').css("display", "");
        $('[id*="tdtFTotalValue_"]').css("display", "");
        $('#thEarnedMarPer').css("display", "");
        $('[id*="tdEarnedMarPerc_"]').css("display", "");
        $('#thEarMarAmt').css("display", "");
        $('[id*="tdEarMarAmt_"]').css("display", "");
        $('#thMarginFixed').css("display", "");
        $('[id*="tdMarginfixed_"]').css("display", "");
        $('#thPrintQty').css("display", "");
        $('[id*="tdPrintQty_"]').css("display", "");
        $('#thSGSt').css("display", "");
        $('[id*="tdITaxt1_"]').css("display", "");
        $('#thCGst').css("display", "");
        $('[id*="tdITaxt2_"]').css("display", "");
        $('#thMEDBATCH').css("display", "");
        $('[id*="tdMedBatch_"]').css("display", "");
        $('#thEXPDATE').css("display", "");
        $('[id*="tdExpDate_"]').css("display", "");
        $('#thProfitMargin').css("display", "");
        $('[id*="tdProfitMargin_"]').css("display", "");
        $('#thFocItem').css("display", "");
        $('[id*="tdFOCItem_"]').css("display", "");
        $('#thFocBatch').css("display", "");
        $('[id*="tdFOCBatch_"]').css("display", "");
        $('#thPurCharges').css("display", "");
        $('[id*="tdPurchaseCharges_"]').css("display", "");
        $('#thFrieghtCharges').css("display", "");
        $('[id*="tdFreightCharges_"]').css("display", "");
        $('#thAddCESSAmt').css("display", "");
        $('[id*="tdAddCessAmt_"]').css("display", "");
        $('#thGRAddCessAmt').css("display", "");
        $('[id*="tdGRAddCessAmt_"]').css("display", "");

        //$('#tblGRNHorizontalBar').css("left", "30px");
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncTest(evt) {
    charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 13)
        charCode = 9;

    return charCode;

}

///Set Focus Next Row and Next Column
function fncSetFocusNextRowandCol(evt, source, value) {
    var rowobj, rowId = 0, NextRowobj, prevrowobj;
    try {

        rowobj = $(source).parent().parent();
        rowId = rowobj.find('td input[id*="txtSNo"]').val();

        rowId = parseFloat(rowId) - parseFloat(1);
        charCode = (evt.which) ? evt.which : evt.keyCode;
        //// Enter and Right Arrow
        if (charCode == 13 || charCode == 39) {
            if (focusflag == 1) {
                if (value == "RowNo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPONo"]'));
                    return false;
                }
                else if (value == "PONo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemcode"]'));
                    return false;
                }
                else if (value == "ItemCode") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemDesc"]'));
                    return false;
                }
                else if (value == "ItemDesc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPOQty"]'));
                    return false;
                }
                else if (value == "POQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtInvQty"]'));
                    return false;
                }
                else if (value == "InvQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtlQty"]'));
                    //fncSetSelectAndFocus($('#txtlQty_' + rowId));
                    return false;
                }
                else if (value == "LQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFoc"]'));
                    //fncSetSelectAndFocus($('#txtFoc_' + rowId));
                    return false;
                }
                else if (value == "Foc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtMRP"]'));
                    //fncSetSelectAndFocus($('#txtMRP_' + rowId));
                    return false;
                }
                else if (value == "MRP") { 
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtStyleCode"]'));
                    return false;
                }
                else if (value == "StyleCode") { 
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtSerialNo"]'));
                    return false;
                }
                else if (value == "SerialNo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemcode"]'));
                    return false;
                }

            }
        else{
            if (value == "RowNo") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtPONo"]'));
                return false;
            }
            else if (value == "PONo") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemcode"]'));
                return false;
            }
            else if (value == "ItemCode") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemDesc"]'));
                return false;
            }
            else if (value == "ItemDesc") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtPOQty"]'));
                return false;
            }
            else if (value == "POQty") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtInvQty"]'));
                return false;
            }
            else if (value == "InvQty") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtlQty"]'));
                //fncSetSelectAndFocus($('#txtlQty_' + rowId));
                return false;
            }
            else if (value == "LQty") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFoc"]'));
                //fncSetSelectAndFocus($('#txtFoc_' + rowId));
                return false;
            }
            else if (value == "Foc") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtMRP"]'));
                //fncSetSelectAndFocus($('#txtMRP_' + rowId));
                return false;
            }
            else if (value == "MRP") {

                if ($('#thBasicCost').css("display") == "none") {
                    $('#thBasicCost').css("display", "");
                    $('[id*="tdBasicCost_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtBasicCost"]'));
                return false;
            }
            else if (value == "BasicCost") {

                if ($('#thSellingPrice').css("display") == "none") {
                    $('#thSellingPrice').css("display", "");
                    $('[id*="tdSellingPrice_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSellingPrice"]'));
                return false;
            }
            else if (value == "SellingPrice") {

                $('#thBasicCost').css("display", "none");
                $('[id*="tdBasicCost_"]').css("display", "none");

                if ($('#thWPrice1').css("display") == "none") {
                    $('#thWPrice1').css("display", "");
                    $('[id*="tdWPrice1_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice1"]'));

                return false;
            }
            else if (value == "Wprice1") {

                $('#thSellingPrice').css("display", "none");
                $('[id*="tdSellingPrice_"]').css("display", "none");


                if ($('#thWPrice2').css("display") == "none") {
                    $('#thWPrice2').css("display", "");
                    $('[id*="tdWPrice2_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice2"]'));
                return false;
            }
            else if (value == "Wprice2") {

                $('#thWPrice1').css("display", "none");
                $('[id*="tdWPrice1_"]').css("display", "none");

                if ($('#thWPrice3').css("display") == "none") {
                    $('#thWPrice3').css("display", "");
                    $('[id*="tdWPrice3_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice3"]'));
                return false;
            }
            else if (value == "Wprice3") {

                $('#thWPrice2').css("display", "none");
                $('[id*="tdWPrice2_"]').css("display", "none");

                if ($('#thGrossBasicAmt').css("display") == "none") {
                    $('#thGrossBasicAmt').css("display", "");
                    $('[id*="tdGrossBasicAmt_"]').css("display", "");
                }


                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossBasicAmt"]'));

                return false;
            }
            else if (value == "GrossBasicAmt") {

                $('#thWPrice3').css("display", "none");
                $('[id*="tdWPrice3_"]').css("display", "none");


                if ($('#thDiscPer').css("display") == "none") {
                    $('#thDiscPer').css("display", "");
                    $('[id*="tdDiscPer_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscPer"]'));
                return false;
            }


            else if (value == "DiscPer") {
                $('#thGrossBasicAmt').css("display", "none");
                $('[id*="tdGrossBasicAmt_"]').css("display", "none");

                if ($('#thDiscAmt').css("display") == "none") {
                    $('#thDiscAmt').css("display", "");
                    $('[id*="tdDiscAmt_"]').css("display", "");

                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscAmt"]'));


                return false;
            }
            else if (value == "DiscAmt") {

                $('#thDiscPer').css("display", "none");
                $('[id*="tdDiscPer_"]').css("display", "none");

                if ($('#thGrossDiscAmt').css("display") == "none") {
                    $('#thGrossDiscAmt').css("display", "");
                    $('[id*="tdGrossDiscAmt_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossDiscAmt"]'));
                return false;
            }
            else if (value == "GrossDiscAmt") {

                $('#thDiscAmt').css("display", "none");
                $('[id*="tdDiscAmt_"]').css("display", "none");

                if ($('#thSDPer').css("display") == "none") {
                    $('#thSDPer').css("display", "");
                    $('[id*="tdSDPer_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSDper"]'));
                return false;
            }
            else if (value == "SDper") {

                $('#thGrossDiscAmt').css("display", "none");
                $('[id*="tdGrossDiscAmt_"]').css("display", "none");

                if ($('#thSDAmt').css("display") == "none") {
                    $('#thSDAmt').css("display", "");
                    $('[id*="tdSDAmt_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSDAmt"]'));

                return false;
            }
            else if (value == "SDAmt") {

                $('#thSDPer').css("display", "none");
                $('[id*="tdSDPer_"]').css("display", "none");

                if ($('#thGrossShDiscAmt').css("display") == "none") {
                    $('#thGrossShDiscAmt').css("display", "");
                    $('[id*="tdGrossShDiscAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossShDiscAmt"]'));
                return false;
            }
            else if (value == "GrossShDiscAmt") {

                $('#thSDAmt').css("display", "none");
                $('[id*="tdSDAmt_"]').css("display", "none");

                if ($('#thGrossCost').css("display") == "none") {
                    $('#thGrossCost').css("display", "");
                    $('[id*="tdGrossCost_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossCost"]'));
                return false;
            }
            else if (value == "GrossCost") {

                $('#thGrossShDiscAmt').css("display", "none");
                $('[id*="tdGrossShDiscAmt_"]').css("display", "none");

                if ($('#thTotalGrossCost').css("display") == "none") {
                    $('#thTotalGrossCost').css("display", "");
                    $('[id*="tdTotalGrossCost_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalGrossCost"]'));
                return false;
            }
            else if (value == "TotalGrossCost") {

                $('#thGrossCost').css("display", "none");
                $('[id*="tdGrossCost_"]').css("display", "none");

                if ($('#thSGstPerc').css("display") == "none") {
                    $('#thSGstPerc').css("display", "");
                    $('[id*="tdITaxperc1_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxperc1"]'));
                return false;
            }
            else if (value == "ITaxperc1") {

                $('#thTotalGrossCost').css("display", "none");
                $('[id*="tdTotalGrossCost_"]').css("display", "none");

                if ($('#thCGstPerc').css("display") == "none") {
                    $('#thCGstPerc').css("display", "");
                    $('[id*="tdITaxperc2_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxperc2"]'));
                return false;
            }
            else if (value == "ITaxperc2") {

                $('#thSGstPerc').css("display", "none");
                $('[id*="tdITaxperc1_"]').css("display", "none");

                if ($('#thCessPer').css("display") == "none") {
                    $('#thCessPer').css("display", "");
                    $('[id*="tdCessper_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtCessper"]'));
                return false;
            }
            else if (value == "Cessper") {

                $('#thCGstPerc').css("display", "none");
                $('[id*="tdITaxperc2_"]').css("display", "none");

                if ($('#thSGstAmt').css("display") == "none") {
                    $('#thSGstAmt').css("display", "");
                    $('[id*="tdITaxAmt1_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxAmt1"]'));
                return false;
            }
            else if (value == "ITaxAmt1") {

                $('#thCessPer').css("display", "none");
                $('[id*="tdCessper_"]').css("display", "none");

                if ($('#thCGstAmt').css("display") == "none") {
                    $('#thCGstAmt').css("display", "");
                    $('[id*="tdITaxAmt2_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxAmt2"]'));
                return false;
            }
            else if (value == "ITaxAmt2") {

                $('#thSGstAmt').css("display", "none");
                $('[id*="tdITaxAmt1_"]').css("display", "none");

                if ($('#thCESSAmt').css("display") == "none") {
                    $('#thCESSAmt').css("display", "");
                    $('[id*="tdCessAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtCessAmt"]'));
                return false;
            }
            else if (value == "CessAmt") {

                $('#thCGstAmt').css("display", "none");
                $('[id*="tdITaxAmt2_"]').css("display", "none");

                if ($('#thAddCESSAmt').css("display") == "none") {
                    $('#thAddCESSAmt').css("display", "");
                    $('[id*="tdAddCessAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtAddCessAmt"]'));
                return false;
            }
            else if (value == "AddCessAmt") {

                $('#thCESSAmt').css("display", "none");
                $('[id*="tdCessAmt_"]').css("display", "none");

                if ($('#thGrossCessAmt').css("display") == "none") {
                    $('#thGrossCessAmt').css("display", "");
                    $('[id*="tdGrocessCessAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossVATAmt"]'));
                return false;
            }

            else if (value == "GrossVATAmt") {

                $('#thAddCESSAmt').css("display", "none");
                $('[id*="tdAddCessAmt_"]').css("display", "none");

                if ($('#thGrossCessAmt').css("display") == "none") {
                    $('#thGrossCessAmt').css("display", "");
                    $('[id*="tdGrocessCessAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrocessCessAmt"]'));
                return false;
            }
            else if (value == "GrocessCessAmt") {

                $('#thGrossGSTAmt').css("display", "none");
                $('[id*="tdGrossVATAmt_"]').css("display", "none");

                if ($('#thGRAddCessAmt').css("display") == "none") {
                    $('#thGRAddCessAmt').css("display", "");
                    $('[id*="tdGRAddCessAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGRAddCessAmt"]'));
                return false;
            }
            else if (value == "GRAddCessAmt") {

                $('#thGrossCessAmt').css("display", "none");
                $('[id*="tdGrocessCessAmt_"]').css("display", "none");

                if ($('#thNetCost').css("display") == "none") {
                    $('#thNetCost').css("display", "");
                    $('[id*="tdtNetCost_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetCost"]'));
                return false;
            }
            else if (value == "NetCost") {

                $('#thGRAddCessAmt').css("display", "none");
                $('[id*="tdGRAddCessAmt_"]').css("display", "none");

                if ($('#thTotalValue').css("display") == "none") {
                    $('#thTotalValue').css("display", "");
                    $('[id*="tdTotalValue_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalValue"]'));
                return false;
            }
            else if (value == "TotalValue") {

                $('#thNetCost').css("display", "none");
                $('[id*="tdtNetCost_"]').css("display", "none");

                if ($('#thFTotalValue').css("display") == "none") {
                    $('#thFTotalValue').css("display", "");
                    $('[id*="tdtFTotalValue_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFTotalValue"]'));
                return false;
            }
            else if (value == "FTotalValue") {

                $('#thTotalValue').css("display", "none");
                $('[id*="tdTotalValue_"]').css("display", "none");

                if ($('#thEarnedMarPer').css("display") == "none") {
                    $('#thEarnedMarPer').css("display", "");
                    $('[id*="tdEarnedMarPerc_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtEarnedMarPerc"]'));
                return false;
            }
            else if (value == "EarnedMarPerc") {

                $('#thFTotalValue').css("display", "none");
                $('[id*="tdtFTotalValue_"]').css("display", "none");

                if ($('#thEarMarAmt').css("display") == "none") {
                    $('#thEarMarAmt').css("display", "");
                    $('[id*="tdEarMarAmt_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtEarMarAmt"]'));
                return false;
            }
            else if (value == "EarMarAmt") {

                $('#thEarnedMarPer').css("display", "none");
                $('[id*="tdEarnedMarPerc_"]').css("display", "none");

                if ($('#thMarginFixed').css("display") == "none") {
                    $('#thMarginFixed').css("display", "");
                    $('[id*="tdMarginfixed_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtMarginfixed"]'));
                return false;
            }
            else if (value == "Marginfixed") {

                $('#thEarMarAmt').css("display", "none");
                $('[id*="tdEarMarAmt_"]').css("display", "none");

                if ($('#thPrintQty').css("display") == "none") {
                    $('#thPrintQty').css("display", "");
                    $('[id*="tdPrintQty_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtPrintQty"]'));
                return false;
            }
            else if (value == "PrintQty") {
                $('#thMarginFixed').css("display", "none");
                $('[id*="tdMarginfixed_"]').css("display", "none");

                if ($('#thSGSt').css("display") == "none") {
                    $('#thSGSt').css("display", "");
                    $('[id*="tdITaxt1_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxt1"]'));
                return false;
            }
            else if (value == "ITaxt1") {

                $('#thPrintQty').css("display", "none");
                $('[id*="tdPrintQty_"]').css("display", "none");

                if ($('#thCGst').css("display") == "none") {
                    $('#thCGst').css("display", "");
                    $('[id*="tdITaxt2_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITax2"]'));
                return false;
            }
            else if (value == "ITaxt2") {

                $('#thSGSt').css("display", "none");
                $('[id*="tdITaxt1_"]').css("display", "none");

                if ($('#thMEDBATCH').css("display") == "none") {
                    $('#thMEDBATCH').css("display", "");
                    $('[id*="tdMedBatch_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtMedBatch"]'));
                return false;
            }
            else if (value == "MedBatch") {

                $('#thCGst').css("display", "none");
                $('[id*="tdITaxt2_"]').css("display", "none");

                if ($('#thEXPDATE').css("display") == "none") {
                    $('#thEXPDATE').css("display", "");
                    $('[id*="tdExpDate_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtExpDate"]'));
                return false;
            }
            else if (value == "ExpDate") {

                $('#thMEDBATCH').css("display", "none");
                $('[id*="tdMedBatch_"]').css("display", "none");

                if ($('#thProfitMargin').css("display") == "none") {
                    $('#thProfitMargin').css("display", "");
                    $('[id*="tdProfitMargin_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtProfitMargin"]'));
                return false;
            }
            else if (value == "ProfitMargin") {

                $('#thEXPDATE').css("display", "none");
                $('[id*="tdExpDate_"]').css("display", "none");

                if ($('#thFocItem').css("display") == "none") {
                    $('#thFocItem').css("display", "");
                    $('[id*="tdFOCItem_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFOCItem"]'));
                return false;
            }
            else if (value == "FOCItem") {

                $('#thProfitMargin').css("display", "none");
                $('[id*="tdProfitMargin_"]').css("display", "none");

                if ($('#thFocBatch').css("display") == "none") {
                    $('#thFocBatch').css("display", "");
                    $('[id*="tdFOCBatch_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFOCBatch"]'));
                return false;
            }
            else if (value == "FOCBatch") {

                $('#thFocItem').css("display", "none");
                $('[id*="tdFOCItem_"]').css("display", "none");

                if ($('#thPurCharges').css("display") == "none") {
                    $('#thPurCharges').css("display", "");
                    $('[id*="tdPurchaseCharges_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtPurchaseCharges"]'));
                return false;
            }
            else if (value == "PurchaseCharges") {

                $('#thFocBatch').css("display", "none");
                $('[id*="tdFOCBatch_"]').css("display", "none");

                if ($('#thFrieghtCharges').css("display") == "none") {
                    $('#thFrieghtCharges').css("display", "");
                    $('[id*="tdFreightCharges_"]').css("display", "");
                }

                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFreightCharges"]'));
                return false;
            }
            else if (value == "FreightCharges") {

                $('#thPurCharges').css("display", "none");
                $('[id*="tdPurchaseCharges_"]').css("display", "none");

                if ($('#thVendorMargin').css("display") == "none") {
                    $('#thVendorMargin').css("display", "");
                    $('[id*="tdVendorMargin_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtVendorMargin"]'));
                return false;
            }
            else if (value == "VendorMargin") {

                $('#thFrieghtCharges').css("display", "none");
                $('[id*="tdFreightCharges_"]').css("display", "none");

                if ($('#thTotalCharges').css("display") == "none") {
                    $('#thTotalCharges').css("display", "");
                    $('[id*="tdTotalCharges_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalCharges"]'));
                return false;
            }
            else if (value == "TotalCharges") {

                $('#thVendorMargin').css("display", "none");
                $('[id*="tdVendorMargin_"]').css("display", "none");

                if ($('#thLandingCost').css("display") == "none") {
                    $('#thLandingCost').css("display", "");
                    $('[id*="tdLandingCost_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtLandingCost"]'));
                //fncSetSelectAndFocus($('#txtLandingCost_' + rowId));
                return false;
            }
            else if (value == "LandingCost") {

                $('#thTotalCharges').css("display", "none");
                $('[id*="tdTotalCharges_"]').css("display", "none");

                if ($('#thStyleCode').css("display") == "none") {
                    $('#thStyleCode').css("display", "");
                    $('[id*="tdStyleCode_"]').css("display", "");
                }
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtStyleCode"]'));
                return false;
            }
            else if (value == "StyleCode") {
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSerialNo"]'));
                return false;
            }
            else if (value == "SerialNo") {
                return false;
            }

        }
    }
            ///// Left Arrow
        else if (charCode == 37) {
            if (focusflag == 1) {
                if (value == "SerialNo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtStyleCode"]'));
                    return false;
                }
                else if (value == "StyleCode") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtMRP"]'));
                    return false;
                }
                else if (value == "MRP") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFoc"]'));
                    return false;
                }
                else if (value == "Foc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtlQty"]'));
                    return false;
                }
                else if (value == "LQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtInvQty"]'));
                    return false;
                }
                else if (value == "InvQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPOQty"]'));
                    //fncSetSelectAndFocus($('#txtlQty_' + rowId));
                    return false;
                }
                else if (value == "POQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemDesc"]'));
                    //fncSetSelectAndFocus($('#txtFoc_' + rowId));
                    return false;
                }
                else if (value == "ItemDesc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemcode"]'));
                    //fncSetSelectAndFocus($('#txtMRP_' + rowId));
                    return false;
                }
                else if (value == "ItemCode") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPONo"]'));
                    return false;
                }
                else if (value == "PONo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtSNo"]'));
                    return false;
                }
            }
            else
            {
                if (value == "PONo") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtSNo"]'));
                    return false;
                }
                else if (value == "ItemCode") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPONo"]'));
                    return false;
                }
                else if (value == "ItemDesc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemcode"]'));
                    return false;
                }
                else if (value == "POQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtItemDesc"]'));
                    return false;
                }
                if (value == "InvQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPOQty"]'));
                    return false;
                }
                else if (value == "StyleCode") {
                    $('#thLandingCost').css("display", "");
                    $('[id*="tdLandingCost_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtLandingCost"]'));
                    //fncSetSelectAndFocus($('#txtTotalCharges_' + rowId));
                }
                else if (value == "SerialNo") {
                    $('#thStyleCode').css("display", "");
                    $('[id*="tdStyleCode_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtStyleCode"]'));
                    //fncSetSelectAndFocus($('#txtTotalCharges_' + rowId));
                }
                else if (value == "LandingCost") {
                    $('#thTotalCharges').css("display", "");
                    $('[id*="tdTotalCharges_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalCharges"]'));
                    //fncSetSelectAndFocus($('#txtTotalCharges_' + rowId));
                }
                else if (value == "TotalCharges") {
                    $('#thVendorMargin').css("display", "");
                    $('[id*="tdVendorMargin_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtVendorMargin"]'));
                    //fncSetSelectAndFocus($('#txtVendorMargin_' + rowId));
                }
                else if (value == "VendorMargin") {
                    $('#thFrieghtCharges').css("display", "");
                    $('[id*="tdFreightCharges_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFreightCharges"]'));
                    //fncSetSelectAndFocus($('#txtFreightCharges_' + rowId));
                }
                else if (value == "FreightCharges") {
                    $('#thPurCharges').css("display", "");
                    $('[id*="tdPurchaseCharges_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtPurchaseCharges"]'));
                    //fncSetSelectAndFocus($('#txtPurchaseCharges_' + rowId));
                }
                else if (value == "PurchaseCharges") {
                    $('#thFocBatch').css("display", "");
                    $('[id*="tdFOCBatch_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFOCBatch"]'));
                    //fncSetSelectAndFocus($('#txtFOCBatch_' + rowId));
                }
                else if (value == "FOCBatch") {
                    $('#thFocItem').css("display", "");
                    $('[id*="tdFOCItem_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFOCItem"]'));
                    //fncSetSelectAndFocus($('#txtFOCItem_' + rowId));
                }
                else if (value == "FOCItem") {
                    $('#thProfitMargin').css("display", "");
                    $('[id*="tdProfitMargin_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtProfitMargin"]'));
                    //fncSetSelectAndFocus($('#txtProfitMargin_' + rowId));
                }
                else if (value == "ProfitMargin") {
                    $('#thEXPDATE').css("display", "");
                    $('[id*="tdExpDate_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtExpDate"]'));
                    //fncSetSelectAndFocus($('#txtExpDate_' + rowId));
                }
                else if (value == "ExpDate") {
                    $('#thMEDBATCH').css("display", "");
                    $('[id*="tdMedBatch_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtMedBatch"]'));
                    //fncSetSelectAndFocus($('#txtMedBatch_' + rowId));

            }
            else if (value == "MedBatch") {
                $('#thCGst').css("display", "");
                $('[id*="tdITaxt2_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITax2"]'));
                //fncSetSelectAndFocus($('#txtITax2_' + rowId));

            }
            else if (value == "ITaxt2") {
                $('#thSGSt').css("display", "");
                $('[id*="tdITaxt1_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxt1"]'));
                //fncSetSelectAndFocus($('#txtITaxt1_' + rowId));

            }
            else if (value == "ITaxt1") {
                $('#thPrintQty').css("display", "");
                $('[id*="tdPrintQty_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtPrintQty"]'));
                //fncSetSelectAndFocus($('#txtPrintQty_' + rowId));
            }
            else if (value == "PrintQty") {
                $('#thMarginFixed').css("display", "");
                $('[id*="tdMarginfixed_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtMarginfixed"]'));
                //fncSetSelectAndFocus($('#txtMarginfixed_' + rowId));
            }
            else if (value == "Marginfixed") {
                $('#thEarMarAmt').css("display", "");
                $('[id*="tdEarMarAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtEarMarAmt"]'));
                //fncSetSelectAndFocus($('#txtEarMarAmt_' + rowId));
            }
            else if (value == "EarMarAmt") {
                $('#thEarnedMarPer').css("display", "");
                $('[id*="tdEarnedMarPerc_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtEarnedMarPerc"]'));
                //fncSetSelectAndFocus($('#txtEarnedMarPerc_' + rowId));
            }
            else if (value == "EarnedMarPerc") {
                $('#thFTotalValue').css("display", "");
                $('[id*="tdtFTotalValue_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtFTotalValue"]'));
                //fncSetSelectAndFocus($('#txtFTotalValue_' + rowId));
            }
            else if (value == "FTotalValue") {
                $('#thTotalValue').css("display", "");
                $('[id*="tdTotalValue_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalValue"]'));
                //fncSetSelectAndFocus($('#txtTotalValue_' + rowId));
            }
            else if (value == "TotalValue") {
                $('#thNetCost').css("display", "");
                $('[id*="tdtNetCost_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetCost"]'));
                //fncSetSelectAndFocus($('#txtNetCost_' + rowId));
            }
            else if (value == "NetCost") {
                $('#thGRAddCessAmt').css("display", "");
                $('[id*="tdGRAddCessAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGRAddCessAmt"]'));
                //fncSetSelectAndFocus($('#txtGRAddCessAmt_' + rowId));
            }
            else if (value == "GRAddCessAmt") {
                $('#thGrossCessAmt').css("display", "");
                $('[id*="tdGrocessCessAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrocessCessAmt"]'));
                //fncSetSelectAndFocus($('#txtGrocessCessAmt_' + rowId));
            }
            else if (value == "GrocessCessAmt") {
                $('#thGrossGSTAmt').css("display", "");
                $('[id*="tdGrossVATAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossVATAmt"]'));
                //fncSetSelectAndFocus($('#txtGrossVATAmt_' + rowId));
            }
            else if (value == "GrossVATAmt") {
                $('#thCESSAmt').css("display", "");
                $('[id*="tdCessAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtCessAmt"]'));
                //fncSetSelectAndFocus($('#txtCessAmt_' + rowId));
            }
            else if (value == "AddCessAmt") {
                $('#thAddCESSAmt').css("display", "");
                $('[id*="tdAddCessAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtAddCessAmt"]'));
                //fncSetSelectAndFocus($('#txtAddCessAmt_' + rowId));
            }
            else if (value == "CessAmt") {
                $('#thCGstAmt').css("display", "");
                $('[id*="tdITaxAmt2_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxAmt2"]'));
                //fncSetSelectAndFocus($('#txtITaxAmt2_' + rowId));
            }
            else if (value == "ITaxAmt2") {
                $('#thSGstAmt').css("display", "");
                $('[id*="tdITaxAmt1_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxAmt1"]'));
                //fncSetSelectAndFocus($('#txtITaxAmt1_' + rowId));
            }
            else if (value == "ITaxAmt1") {
                $('#thCessPer').css("display", "");
                $('[id*="tdCessper_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtCessper"]'));
                //fncSetSelectAndFocus($('#txtCessper_' + rowId));
            }
            else if (value == "Cessper") {
                $('#thCGstPerc').css("display", "");
                $('[id*="tdITaxperc2_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxperc2"]'));
                //fncSetSelectAndFocus($('#txtITaxperc2_' + rowId));
            }
            else if (value == "ITaxperc2") {
                $('#thSGstPerc').css("display", "");
                $('[id*="tdITaxperc1_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtITaxperc1"]'));
                //fncSetSelectAndFocus($('#txtITaxperc1_' + rowId));
            }
            else if (value == "ITaxperc1") {
                $('#thTotalGrossCost').css("display", "");
                $('[id*="tdTotalGrossCost_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtTotalGrossCost"]'));
                //fncSetSelectAndFocus($('#txtTotalGrossCost_' + rowId));
            }
            else if (value == "TotalGrossCost") {
                $('#thGrossCost').css("display", "");
                $('[id*="tdGrossCost_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossCost"]'));
                //fncSetSelectAndFocus($('#txtGrossCost_' + rowId));
            }
            else if (value == "GrossCost") {
                $('#thGrossShDiscAmt').css("display", "");
                $('[id*="tdGrossShDiscAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossShDiscAmt"]'));
                //fncSetSelectAndFocus($('#txtGrossShDiscAmt_' + rowId));
            }
            else if (value == "GrossShDiscAmt") {
                $('#thSDAmt').css("display", "");
                $('[id*="tdSDAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSDAmt"]'));
                //fncSetSelectAndFocus($('#txtSDAmt_' + rowId));
            }
            else if (value == "SDAmt") {
                $('#thSDPer').css("display", "");
                $('[id*="tdSDPer_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtSDper"]'));
                //fncSetSelectAndFocus($('#txtSDper_' + rowId));
            }
            else if (value == "SDper") {
                $('#thGrossDiscAmt').css("display", "");
                $('[id*="tdGrossDiscAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossDiscAmt"]'));
                //fncSetSelectAndFocus($('#txtGrossDiscAmt_' + rowId));
            }
            else if (value == "GrossDiscAmt") {
                $('#thDiscAmt').css("display", "");
                $('[id*="tdDiscAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscAmt"]'));
                //fncSetSelectAndFocus($('#txtDiscAmt_' + rowId));
            }
            else if (value == "DiscAmt") {
                $('#thDiscPer').css("display", "");
                $('[id*="tdDiscPer_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscPer"]'));
                //fncSetSelectAndFocus($('#txtDiscPer_' + rowId));
            }
            else if (value == "DiscPer") {
                $('#thGrossBasicAmt').css("display", "");
                $('[id*="tdGrossBasicAmt_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtGrossBasicAmt"]'));
                //fncSetSelectAndFocus($('#txtGrossBasicAmt_' + rowId));
            }
            else if (value == "GrossBasicAmt") {
                $('#thGrossBasicAmt').css("display", "");
                $('[id*="tdWPrice3_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice3"]'));
                //fncSetSelectAndFocus($('#txtWPrice3_' + rowId));
            }
            else if (value == "Wprice3") {
                $('#thWPrice2').css("display", "");
                $('[id*="tdWPrice2_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice2"]'));
                //fncSetSelectAndFocus($('#txtWPrice2_' + rowId));
            }
            else if (value == "Wprice2") {
                $('#thWPrice1').css("display", "");
                $('[id*="tdWPrice1_"]').css("display", "");
                fncSetSelectAndFocus(rowobj.find('td input[id*="txtWPrice1"]'));
                //fncSetSelectAndFocus($('#txtWPrice1_' + rowId));

                }
                else if (value == "Wprice1") {
                    $('#thSellingPrice').css("display", "");
                    $('[id*="tdSellingPrice_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtSellingPrice"]'));
                    //fncSetSelectAndFocus($('#txtSellingPrice_' + rowId));
                }
                else if (value == "SellingPrice") {
                    $('#thBasicCost').css("display", "");
                    $('[id*="tdBasicCost_"]').css("display", "");
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtBasicCost"]'));
                    //fncSetSelectAndFocus($('#txtBasicCost_' + rowId));
                }
                else if (value == "BasicCost") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtMRP"]'));
                    //fncSetSelectAndFocus($('#txtMRP_' + rowId));
                }
                else if (value == "MRP") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtFoc"]'));
                    //fncSetSelectAndFocus($('#txtFoc_' + rowId));
                }
                else if (value == "Foc") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtlQty"]'));
                    //fncSetSelectAndFocus($('#txtlQty_' + rowId));
                }
                else if (value == "LQty") {
                    fncSetSelectAndFocus(rowobj.find('td input[id*="txtInvQty"]'));
                    //fncSetSelectAndFocus($('#txtInvQty_' + rowId));
                }
            }
        }
        else if (charCode == 40) {
            NextRowobj = rowobj.next();
            if (value == "RowNo") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSNo"]'));
            }
            else if (value == "PONo") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtPONo"]'));
            }
            else if (value == "ItemCode") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtItemcode"]'));
            }
            else if (value == "ItemDesc") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtItemDesc"]'));
            }
            else if (value == "POQty") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtPOQty"]'));
            }
            else if (value == "InvQty") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtInvQty"]'));

            }
            else if (value == "LQty") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtlQty"]'));
            }
            else if (value == "Foc") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFoc"]'));
            }
            else if (value == "MRP") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMRP"]'));
            }
            else if (value == "BasicCost") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBasicCost"]'));


            }
            else if (value == "SellingPrice") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSellingPrice"]'));


            }
            else if (value == "Wprice1") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtWPrice1"]'));


            }
            else if (value == "Wprice2") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtWPrice2"]'));


            }
            else if (value == "Wprice3") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtWPrice3"]'));


            }
            else if (value == "GrossBasicAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGrossBasicAmt"]'));
            }
            else if (value == "DiscPer") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtDiscPer"]'));


            }
            else if (value == "DiscAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtDiscAmt"]'));


            }
            else if (value == "GrossDiscAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGrossDiscAmt"]'));


            }
            else if (value == "SDper") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSDper"]'));


            }
            else if (value == "SDAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSDAmt"]'));


            }
            else if (value == "GrossShDiscAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="GrossShDiscAmt"]'));


            }
            else if (value == "GrossCost") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGrossCost"]'));


            }
            else if (value == "TotalGrossCost") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtTotalGrossCost"]'));
            }
            else if (value == "ITaxperc1") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITaxperc1"]'));
            }
            else if (value == "ITaxperc2") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITaxperc2"]'));

            }
            else if (value == "Cessper") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtCessper"]'));

            }
            else if (value == "ITaxAmt1") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITaxAmt1"]'));

            }
            else if (value == "ITaxAmt2") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITaxAmt2"]'));

            }
            else if (value == "CessAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtCessAmt"]'));

            }
            else if (value == "AddCessAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtAddCessAmt"]'));

            }
            else if (value == "GrossVATAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGrossVATAmt"]'));

            }
            else if (value == "GrocessCessAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGrocessCessAmt"]'));

            }
            else if (value == "GRAddCessAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGRAddCessAmt"]'));

            }
            else if (value == "NetCost") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtNetCost"]'));

            }
            else if (value == "TotalValue") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtTotalValue"]'));

            }
            else if (value == "FTotalValue") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFTotalValue"]'));

            }
            else if (value == "EarnedMarPerc") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtEarnedMarPerc"]'));

            }
            else if (value == "EarMarAmt") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtEarMarAmt"]'));

            }
            else if (value == "Marginfixed") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMarginfixed"]'));

            }
            else if (value == "PrintQty") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtPrintQty"]'));


            }
            else if (value == "ITaxt1") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITaxt1"]'));

            }
            else if (value == "ITaxt2") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtITax2"]'));

            }
            else if (value == "MedBatch") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMedBatch"]'));

            }
            else if (value == "ExpDate") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtExpDate"]'));

            }
            else if (value == "ProfitMargin") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtProfitMargin"]'));

            }
            else if (value == "FOCItem") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFOCItem"]'));

            }
            else if (value == "FOCBatch") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFOCBatch"]'));

            }
            else if (value == "PurchaseCharges") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtPurchaseCharges"]'));

            }
            else if (value == "FreightCharges") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFreightCharges"]'));

            }
            else if (value == "VendorMargin") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtVendorMargin"]'));

            }
            else if (value == "TotalCharges") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtTotalCharges"]'));

            }
            else if (value == "LandingCost") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtLandingCost"]'));

            }
            else if (value == "StyleCode") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtStyleCode"]'));

            }
            else if (value == "SerialNo") {
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSerialNo"]'));

            }

        }
        else if (charCode == 38) {
            prevrowobj = rowobj.prev();
            if (value == "RowNo") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSNo"]'));

            }

            else if (value == "PONo") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtPONo"]'));

            }
            else if (value == "ItemCode") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtItemcode"]'));


            }
            else if (value == "ItemDesc") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtItemDesc"]'));

            }
            else if (value == "POQty") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtPOQty"]'));

            }
            else if (value == "InvQty") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtInvQty"]'));
            }
            else if (value == "LQty") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtlQty"]'));

            }
            else if (value == "Foc") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFoc"]'));


            }
            else if (value == "MRP") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtMRP"]'));


            }
            else if (value == "BasicCost") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtBasicCost"]'));


            }
            else if (value == "SellingPrice") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSellingPrice"]'));


            }
            else if (value == "Wprice1") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtWPrice1"]'));


            }
            else if (value == "Wprice2") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtWPrice2"]'));


            }
            else if (value == "Wprice3") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtWPrice3"]'));


            }
            else if (value == "GrossBasicAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGrossBasicAmt"]'));


            }
            else if (value == "DiscPer") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtDiscPer"]'));


            }
            else if (value == "DiscAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtDiscAmt"]'));


            }
            else if (value == "GrossDiscAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGrossDiscAmt"]'));


            }
            else if (value == "SDper") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSDper"]'));


            }
            else if (value == "SDAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSDAmt"]'));


            }
            else if (value == "GrossShDiscAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="GrossShDiscAmt"]'));


            }
            else if (value == "GrossCost") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGrossCost"]'));


            }
            else if (value == "TotalGrossCost") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtTotalGrossCost"]'));

            }
            else if (value == "ITaxperc1") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITaxperc1"]'));

            }
            else if (value == "ITaxperc2") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITaxperc2"]'));

            }
            else if (value == "Cessper") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtCessper"]'));

            }
            else if (value == "ITaxAmt1") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITaxAmt1"]'));
            }
            else if (value == "ITaxAmt2") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITaxAmt2"]'));

            }
            else if (value == "CessAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtCessAmt"]'));

            }
            else if (value == "AddCessAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtAddCessAmt"]'));

            }
            else if (value == "GrossVATAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGrossVATAmt"]'));

            }
            else if (value == "GrocessCessAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGrocessCessAmt"]'));

            }
            else if (value == "GRAddCessAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGRAddCessAmt"]'));

            }
            else if (value == "NetCost") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtNetCost"]'));

            }
            else if (value == "TotalValue") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtTotalValue"]'));

            }
            else if (value == "FTotalValue") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFTotalValue"]'));

            }
            else if (value == "EarnedMarPerc") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtEarnedMarPerc"]'));

            }
            else if (value == "EarMarAmt") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtEarMarAmt"]'));

            }
            else if (value == "Marginfixed") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtMarginfixed"]'));

            }
            else if (value == "PrintQty") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtPrintQty"]'));


            }
            else if (value == "ITaxt1") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITaxt1"]'));

            }
            else if (value == "ITaxt2") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtITax2"]'));

            }
            else if (value == "MedBatch") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtMedBatch"]'));

            }
            else if (value == "ExpDate") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtExpDate"]'));

            }
            else if (value == "ProfitMargin") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtProfitMargin"]'));

            }
            else if (value == "FOCItem") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFOCItem"]'));

            }
            else if (value == "FOCBatch") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFOCBatch"]'));

            }
            else if (value == "PurchaseCharges") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtPurchaseCharges"]'));

            }
            else if (value == "FreightCharges") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFreightCharges"]'));

            }
            else if (value == "VendorMargin") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtVendorMargin"]'));

            }
            else if (value == "TotalCharges") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtTotalCharges"]'));

            }
            else if (value == "LandingCost") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtLandingCost"]'));
            }
            else if (value == "StyleCode") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtStyleCode"]'));
            }
            else if (value == "SerialNo") {
                fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSerialNo"]'));
            }
        }
        else if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57) &&
            !(charCode >= 96 && charCode <= 105) && charCode != 110 && charCode != 190) {
            event.preventDefault();
            return false;
        }
        else {
            if (charCode == 8) {
                if (value == "RowNo" || value == "PONo" || value == "ItemCode" || value == "ItemDesc"
                    || value == "POQty" || value == "GrossBasicAmt" || value == "GrossDiscAmt"
                    || value == "GrossShDiscAmt" || value == "GrossCost" || value == "TotalGrossCost"
                    || value == "ITaxperc1" || value == "ITaxperc2" || value == "Cessper" || value == "ITaxAmt1"
                    || value == "ITaxAmt2" || value == "CessAmt" || value == "AddCessAmt" || value == "GrossVATAmt"
                    || value == "GrocessCessAmt" || value == "GrossVATAmt" || value == "GrocessCessAmt"
                    || value == "GRAddCessAmt" || value == "NetCost" || value == "TotalValue" || value == "FTotalValue"
                    || value == "EarnedMarPerc" || value == "EarMarAmt" || value == "ITaxt1" || value == "ITaxt2"
                    || value == "MedBatch" || value == "ExpDate" || value == "ProfitMargin" || value == "FOCItem"
                    || value == "FOCBatch" || value == "PurchaseCharges" || value == "FreightCharges" || value == "VendorMargin"
                    || value == "TotalCharges" || value == "LandingCost" || value == "Marginfixed" || value == "StyleCode" || value == "SerialNo") {
                    return false;
                }
                else {
                    return true;
                }
            }

        }
    }

    catch (err) {
        fncToastError(err.message);
    }
}

function fncSetSelectAndFocus(obj) {
    try {

        setTimeout(function () {
            obj.focus().select();
        }, 10);

        fncRowClick($(obj).parent().parent());

    }
    catch (err) {
        fncToastError(err.message);
    }
}


//Focus Set to Next Row
function fncSetFocustoNextRow(evt, source) {
    try {
        var rowobj = $(source).parent().parent();
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13 || charCode == 40) {
            var NextRowobj = rowobj.next();
            if (NextRowobj.length > 0) {
                NextRowobj.find('td input[id*="txtAmtToPay"]').select().focus();
            }
            else {
                rowobj.siblings().first().find('td input[id*="txtAmtToPay"]').select().focus();

            }
        }
        else if (charCode == 38) {
            var prevrowobj = rowobj.prev();
            if (prevrowobj.length > 0) {
                prevrowobj.find('td input[id*="txtAmtToPay"]').select().focus();

            }
            else {
                rowobj.siblings().last().find('td input[id*="txtAmtToPay"]').focus();

            }
        }
        else {
            if (rowobj.find('td span[id*="lblTranType"]').html() != "PURINV") {
                return false;
            }
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Initialize VetDetail Dialog
function fncInitializeVetDetailDialog() {
    try {
        fncHideCGSTColumninVatSummery();
        $("#divVatDetail").dialog({
            autoOpen: false,
            resizable: false,
            height: 250,
            width: 657,
            modal: true,
            title: "GST Detail",
            appendTo: 'form:first'
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Initialize PO Pending Dialog
function fncInitializePOPendingDetailDialog() {
    try {
        $("#divPOPendings").dialog({
            autoOpen: false,
            resizable: false,
            height: 331,
            width: 750,
            modal: true,
            title: "PO Pending / Excess Items",
            appendTo: 'form:first'
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}

/// Hide GST Column based on Vendor Status
function fncHideCGSTColumninVatSummery() {
    try {
        if ($('#<%=hidGidGSTFlag.ClientID%>').val() == "2" || $('#<%=hidGidGSTFlag.ClientID%>').val() == "3") {
            $("#tblVetDetail [id*=vatSummerySGST]").text("IGST");
            $("#tblVetDetail [id*=vatSummeryCGST]").addClass("hiddencol");
            $("#tblVetDetail [id*=vatSummaryCGST]").addClass("hiddencol");
        }


    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Initialize Deduction Dialog
function fncInitializeDeduction() {
    try {
        $("#divDeduction").dialog({
            resizable: false,
            height: "auto",
            width: "auto",
            modal: true,
            title: "Deduction",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Initialize Addition Dialog
function fncInitializeAddition() {
    try {
        $("#divAddition").dialog({
            resizable: false,
            height: 220,
            width: 536,
            modal: true,
            title: "Addition",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}


//Deduction and Addiotion Calculation
function fncDeductionAmtCalculation(source, mode, value) {
    try {

        var rowObj, billValue, discperc, Amt, netValue, enterValue = 0, vatAmt, deductionAmt, additionAmt;
        billValue = $('#<%=txtTotalBillAmt.ClientID %>').val();
        totalPayableValue = $('#<%=txtTotalPayable.ClientID %>').val();

        rowObj = $(source).parent().parent();

        if (mode == "Deduction") {
            if (value == "perc") {
                discperc = rowObj.find('td input[id*="txtDiscperc"]').val();
                enterValue = discperc;
                Amt = (parseFloat(billValue) * parseFloat(discperc)) / parseFloat(100);
                rowObj.find('td input[id*="txtDiscAmt"]').val(Amt.toFixed(4));
            }
            else if (value == "Amt") {
                Amt = rowObj.find('td input[id*="txtDiscAmt"]').val();
                enterValue = Amt;
                discperc = (parseFloat(Amt) / parseFloat(billValue)) * parseFloat(100);
                rowObj.find('td input[id*="txtDiscperc"]').val(discperc.toFixed(2));
            }

            //tblDeduction
            enterValue = 0;
            $("#tblDeduction tbody").children().each(function () {
                enterValue = parseFloat(enterValue) + parseFloat($(this).find('td input[id*="txtDiscAmt"]').val());
            });


            $('#<%=txtDeduction.ClientID %>').val(parseFloat(enterValue).toFixed(2));

        }
        else {
            if (value == "perc") {
                discperc = rowObj.find('td input[id*="txtAddPerc"]').val();
                enterValue = discperc;
                Amt = (parseFloat(billValue) * parseFloat(discperc)) / parseFloat(100);
                rowObj.find('td input[id*="txtAddAmt"]').val(Amt.toFixed(2));
            }
            else if (value == "Amt") {
                Amt = rowObj.find('td input[id*="txtAddAmt"]').val();
                enterValue = Amt;
                discperc = (parseFloat(Amt) / parseFloat(billValue)) * parseFloat(100);
                rowObj.find('td input[id*="txtAddPerc"]').val(discperc.toFixed(2));
            }

            //tblAddition
            enterValue = 0;
            $("#tblAddition tbody").children().each(function () {
                enterValue = parseFloat(enterValue) + parseFloat($(this).find('td input[id*="txtAddAmt"]').val());
            });
            $('#<%=txtAddition.ClientID %>').val(parseFloat(enterValue).toFixed(2));
            fncGetAndSetFrieghtValue();
        }

        deductionAmt = $('#<%=txtDeduction.ClientID %>').val();
        additionAmt = $('#<%=txtAddition.ClientID %>').val();

        totalPayableValue = parseFloat(billValue) + parseFloat(additionAmt) - parseFloat(deductionAmt);
        $('#<%=txtTotalPayable.ClientID %>').val(totalPayableValue.toFixed(2));
    }
    catch (err) {
        fncToastError(err.message);
    }
}

var Totalfrieght = 0;
function fncGetAndSetFrieghtValue() {
    var frieghtPer = 0, netCost, unitFrieght = 0;
    try {
        Totalfrieght = 0;
        $("#tblAddition tbody").children().each(function () {
            if ($(this).find('td span[id*="lblMode"]').text() == "L") {
                Totalfrieght = parseFloat(Totalfrieght) + parseFloat($(this).find('td input[id*="txtAddAmt"]').val());
            }
        });


        frieghtPer = Totalfrieght * 100 / $('#<%=txtTotalBillAmt.ClientID%>').val();
        $("#tblGRN tbody").children().each(function () {
            netCost = $(this).find('td input[id*="txtNetCost"]').val();
            unitFrieght = parseFloat(netCost) + parseFloat(netCost * frieghtPer / 100);
            $(this).find('td input[id*="txtLandingCost"]').val(parseFloat(unitFrieght).toFixed(4));
        });



    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Deduction and Addiotion EnterFocus
function fncEnterFocusAddDed(event, source, mode, value) {
    try {
        var rowObj, keychar;
        keychar = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        rowObj = $(source).parent().parent();
        if (keychar == 13) {
            if (mode == "Deduction") {
                $("#divDeduction").dialog("destroy");
            }
            else {
                $("#divAddition").dialog("destroy");
            }
            return false;
        }
        else if (keychar == 39)/// right arrow
        {
            if (mode == "Deduction") {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.find('td input[id*="txtDiscAmt"]'));
            }
            else {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.find('td input[id*="txtAddAmt"]'));
            }
        }
        else if (keychar == 37)/// left arrow
        {
            if (mode == "Deduction") {
                if (value == "Amt")
                    fncSetFocusCommon(rowObj.find('td input[id*="txtDiscperc"]'));
            }
            else {
                if (value == "Amt")
                    fncSetFocusCommon(rowObj.find('td input[id*="txtAddPerc"]'));
            }
        }
        else if (keychar == 40)/// Down arrow
        {
            if (mode == "Deduction") {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.next().find('td input[id*="txtDiscperc"]'));
                else
                    fncSetFocusCommon(rowObj.next().find('td input[id*="txtDiscAmt"]'));
            }
            else {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.next().find('td input[id*="txtAddPerc"]'));
                else
                    fncSetFocusCommon(rowObj.next().find('td input[id*="txtAddAmt"]'));
            }
        }
        else if (keychar == 38)/// Up arrow
        {
            if (mode == "Deduction") {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.prev().find('td input[id*="txtDiscperc"]'));
                else
                    fncSetFocusCommon(rowObj.prev().find('td input[id*="txtDiscAmt"]'));
            }
            else {
                if (value == "perc")
                    fncSetFocusCommon(rowObj.prev().find('td input[id*="txtAddPerc"]'));
                else
                    fncSetFocusCommon(rowObj.prev().find('td input[id*="txtAddAmt"]'));
            }
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Initialize Child Item List
function fncInitializeChildItemList() {
    try {
        $("#divChildandParentItemlist").dialog({
            resizable: false,
            height: 425,
            width: 1093,
            modal: true,
            title: "Child Item Price Change",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Show Last Purchase Detail
function fncShowLastPurchaseDetail() {
    try {
        $("#divLastPurchaseDetail").dialog({
            resizable: false,
            height: 340,
            width: 1093,
            modal: true,
            title: "Last Purchase Detail",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Show Item Search Table Textiles
function fncShowItemSearchTableTextiles() {
    try {
        $("#divItemSearchTextiles").dialog({
            resizable: false,
            height: "auto",
            width: "auto",
            modal: true,
            title: "Item Search - Textiles",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Show Margin Fix Table Textiles
function fncShowItemMarginTable() {
    try {
        $("#divMarginFixing").dialog({
            resizable: false,
            height: "auto",
            width: "auto",
            modal: true,
            title: "Margin Fixing",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Show Item Search Table
function fncShowItemSearchTable() {
    try {
        $("#divItemSearch").dialog({
            resizable: false,
            height: "auto",
            width: 1093,
            modal: true,
            title: "Enterpriser Web",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Get Child Item List        
function fncGetChildItemList() {
    try {
        var itemcode, row, objChildList, tblChildItemListbody;
        itemcode = $('#<%=txtItemCode.ClientID %>').val().trim();
        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncGetChildItemList")%>',
            data: "{'sInvCode':'" + itemcode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                objChildList = jQuery.parseJSON(msg.d);
                tblChildItemListbody = $("#tblChildItemList tbody");
                tblChildItemListbody.children().remove();
                for (var i = 0; i < objChildList.length; i++) {
                    row = "<tr><td>" + objChildList[i]["RowNo"] + "</td><td id='tdItemCode_" + i + "'>" + objChildList[i]["inventorycode"] +
                        "</td><td id='tdDesc_" + i + "'>" + objChildList[i]["description"] + "</td><td  id='tduom_" + i + "'>" +
                        objChildList[i]["uom"] + "</td><td id='tdWeight_" + i + "'>" + objChildList[i]["Weight"].toFixed(2) + "</td><td id='tdNewMRP_" + i + "'> 0.00</td>" +
                         "<td id='tdNewCost_" + i + "'>0.00</td><td id='tdNewSelling_" + i + "'>0.00</td><td id='tdWprice1_" + i + "'>0.00</td>" +
                         "<td id='tdWprice2_" + i + "'>0.00</td><td id='tdWprice3_" + i + "'>0.00<td id='tdPrvMRP_" + i + "'> " + objChildList[i]["mrp"].toFixed(2) + "</td>" +
                         "<td id='tdPrvCost_" + i + "'>" + objChildList[i]["NetCost"].toFixed(2) + "</td><td id='tdPrvSelling_" + i + "'>" + objChildList[i]["NetSellingPrice"].toFixed(2) + "</td></tr>";

                    tblChildItemListbody.append(row);
                }
                fncInitializeChildItemList();
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}
//Get Last Purchase Detail
function fncGetLastPurchaseDetail() {
    try {
        var itemcode, row, objLastPurchaseList, tblLastPurchasetbody;
        itemcode = $('#<%=txtItemCode.ClientID %>').val().trim();
        if (itemcode == "")
            return;
        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("GoodsAcknowledgmentNote.aspx/fncGetLastPurchaseDetail")%>',
            data: "{'sInvCode':'" + itemcode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                objLastPurchaseList = jQuery.parseJSON(msg.d);
                tblLastPurchasetbody = $("#tblLastPurchase tbody");
                tblLastPurchasetbody.children().remove();
                for (var i = 0; i < objLastPurchaseList.length; i++) {
                    row = "<tr><td>" + objLastPurchaseList[i]["RowNo"] + "</td><td>" + objLastPurchaseList[i]["VendorName"] +
                        "</td><td>" + objLastPurchaseList[i]["Dono"] +
                        "</td><td>" + objLastPurchaseList[i]["Dodate"] + "</td><td>" + objLastPurchaseList[i]["InvoiceQty"] +
                        "</td><td >" + objLastPurchaseList[i]["LQty"] + "</td><td>" + objLastPurchaseList[i]["UnitCost"].toFixed(2) +
                        "</td><td>" + objLastPurchaseList[i]["Disc"].toFixed(2) + "</td><td>" + objLastPurchaseList[i]["sd"].toFixed(2) +
                        "</td><td>" + objLastPurchaseList[i]["ITaxPer3"] + "</td><td>" + objLastPurchaseList[i]["ITaxPer4"] +
                        "</td><td>" + objLastPurchaseList[i]["NetCost"].toFixed(2) + "</td><td>" + objLastPurchaseList[i]["MRP"].toFixed(2) +
                        "</td><td>" + objLastPurchaseList[i]["SellingPrice"].toFixed(2) + "</td><td>" + objLastPurchaseList[i]["GidNo"] +
                        "</td></tr>";
                    tblLastPurchasetbody.append(row);
                }
                fncShowLastPurchaseDetail();
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}
///child Item Price Calculation
function fncChildItemPriceChange() {
    try {
        var bulkMRP, bulkCost, bulkSelling, weight, bulkwprice1, bulkwprice2, bulkwprice3;
        bulkMRP = $('#<%=txtBulkMRP.ClientID %>').val();
        bulkCost = $('#<%=txtBulkCost.ClientID %>').val();
        bulkSelling = $('#<%=txtBulkNewSPrice.ClientID %>').val();
        bulkwprice1 = $('#<%=txtBulkWPrice1.ClientID %>').val();
        bulkwprice2 = $('#<%=txtBulkWPrice2.ClientID %>').val();
        bulkwprice3 = $('#<%=txtBulkWPrice3.ClientID %>').val();

        if (bulkSelling == "" || parseFloat(bulkSelling) == 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkNewSPrice.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_InvalidSellingPrice%>');

            return false;
        }
        else if (parseFloat(bulkSelling) > parseFloat(bulkMRP)) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkNewSPrice.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_SellingPriceLessthenMrp%>');

            return false;
        }
        else if (bulkwprice1 == "" || parseFloat(bulkwprice1) == 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice1.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');

            return false;
        }
        else if (parseFloat(bulkwprice1) > parseFloat(bulkMRP)) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice1.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpriceLessthenMRP%>');

            return false;
        }
        else if (bulkwprice2 == "" || parseFloat(bulkwprice2) == 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice2.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');

            return false;
        }
        else if (parseFloat(bulkwprice2) > parseFloat(bulkMRP)) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice2.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpriceLessthenMRP%>');

            return false;
        }
        else if (bulkwprice3 == "" || parseFloat(bulkwprice3) == 0) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice3.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_invalidWproce%>');

            return false;
        }
        else if (parseFloat(bulkwprice3) > parseFloat(bulkMRP)) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBulkWPrice3.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_WpriceLessthenMRP%>');

            return false;
        }

    if ($("#tblChildItemList tbody").length > 0) {
        $("#tblChildItemList tbody").children().each(function () {
            weight = $(this).find('td[id*=tdWeight]').text();
            $(this).find('td[id*=tdNewMRP]').text((parseFloat(weight) * parseFloat(bulkMRP)).toFixed(2));
            $(this).find('td[id*=tdNewCost]').text((parseFloat(weight) * parseFloat(bulkCost)).toFixed(2));
            $(this).find('td[id*=tdNewSelling]').text((parseFloat(weight) * parseFloat(bulkSelling)).toFixed(2));
            $(this).find('td[id*=tdWprice1]').text((parseFloat(weight) * parseFloat(bulkwprice1)).toFixed(2));
            $(this).find('td[id*=tdWprice2]').text((parseFloat(weight) * parseFloat(bulkwprice2)).toFixed(2));
            $(this).find('td[id*=tdWprice3]').text((parseFloat(weight) * parseFloat(bulkwprice3)).toFixed(2));
        });
    }
}
    catch (err) {
        fncToastError(err.message);
    }
}

///ChildItem Save
function fncChildItemSave() {

    var row, gaNo, pItemcode, pCost, cItemCode, cOldCost, cNewCost;
    var cOldselling, cNewSelling, cOldMRP, cNewMRP, Wprice1, Wprice2, Wprice3;
    try {
        if ($("#tblChildItemList tbody").children().length > 0) {
            row = "<table>";
            pItemcode = $.trim($('#<%=txtBulItemcode.ClientID %>').val());
            pCost = $('#<%=txtBulkCost.ClientID %>').val();
            gaNo = $.trim($('#<%=txtgaNo.ClientID %>').val());
            $("#tblChildItemList tbody").children().each(function () {
                cItemCode = $.trim($(this).find('td[id*=tdItemCode]').text());
                cOldCost = $(this).find('td[id*=tdPrvCost]').text();
                cNewCost = $(this).find('td[id*=tdNewCost]').text();
                cOldselling = $(this).find('td[id*=tdPrvSelling]').text();
                cNewSelling = $(this).find('td[id*=tdNewSelling]').text();
                cOldMRP = $(this).find('td[id*=tdPrvMRP]').text();
                cNewMRP = $(this).find('td[id*=tdNewMRP]').text();
                Wprice1 = $(this).find('td[id*=tdWprice1]').text();
                Wprice2 = $(this).find('td[id*=tdWprice2]').text();
                Wprice3 = $(this).find('td[id*=tdWprice3]').text();
                row = row + "<child gaNo='" + gaNo + "'";
                row = row + " pItemcode='" + pItemcode + "' pCost='" + pCost + "' cItemCode='" + cItemCode + "' cOldCost='" + cOldCost + "'";
                row = row + " cNewCost='" + cNewCost + "' cOldselling='" + cOldselling + "' cNewSelling='" + cNewSelling + "'"
                row = row + " cOldMRP = '" + cOldMRP + "' cNewMRP='" + cNewMRP + "' Wprice1='" + Wprice1 + "' Wprice2='" + Wprice2 + "' Wprice3='" + Wprice3 + "'"
                row = row + "></child>";
            });
            row = row + "</table>";
            $('#<%=hidChildItemPriceList.ClientID %>').val(escape(row));
            $("#divChildandParentItemlist").dialog("destroy");

            $('#<%=txtSPrice.ClientID %>').val(SellingPriceRoundOff($('#<%=txtBulkNewSPrice.ClientID %>').val()));
            $('#<%=txtWpirce1.ClientID %>').val($('#<%=txtBulkWPrice1.ClientID %>').val());
            $('#<%=txtWpirce2.ClientID %>').val($('#<%=txtBulkWPrice2.ClientID %>').val());
            $('#<%=txtWpirce3.ClientID %>').val($('#<%=txtBulkWPrice3.ClientID %>').val());

            fncArriveBasicSellingPrice();
            //fncAddEnterItems();
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}


///Initialize Credit and Debit Note
function fncInitializeCreditandDebitNote() {
    try {
        $("#divCreditandDebitNote").dialog({
            resizable: false,
            height: "auto",
            width: "auto",
            modal: true,
            title: "Credit and Debit Note Alert",
            appendTo: 'form:first',
            closeOnEscape: false,
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            }
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Confirmation to Save Credit and Debit Note
function fncCreditAndDebitNoteConfirmation() {
    try {
        if (parseFloat($('#<%=hidShortSupplyAmt.ClientID%>').val()) < 0 && parseFloat($('#<%=hidExcessSupplyAmt.ClientID%>').val()) > 0)
            glMissValueStatus = "2";
        else if (parseFloat($('#<%=hidShortSupplyAmt.ClientID%>').val()) < 0 || parseFloat($('#<%=hidExcessSupplyAmt.ClientID%>').val()) > 0)
            glMissValueStatus = "1";

        if (glMissValueStatus == "2" && $('#<%=hidCreditandDebitNoteStatus.ClientID%>').val() != "1") {
            $('#<%=hidCreditandDebitNoteStatus.ClientID%>').val('1');
            fncShowCreditAndDebitNoteDialog();
        }
        else if (glMissValueStatus == "1") {
            $("#divCreditandDebitNote").dialog('destroy');
            //$('#<%=btnCDSave.ClientID%>').click();
            if ($('#<%=hidCurLocation.ClientID%>').val() == "HQ" && $('#<%=hidGidLocationcode.ClientID%>').val() != $('#<%=hidCurLocation.ClientID%>').val()) {
                fncCreateTransferOut();
            }
            else {
                fncGetGridValuesForSave();
                fncGetMultiUOMForSave();
                if (saveStatus == false) {
                    saveStatus = true;
                    $('#<%=btnSave.ClientID%>').click();
                }
            }
        }
        else if (glMissValueStatus == "2" && $('#<%=hidCreditandDebitNoteStatus.ClientID%>').val() == "1") {
            $("#divCreditandDebitNote").dialog('destroy');
            //$('#<%=btnCDSave.ClientID%>').click();
            if ($('#<%=hidCurLocation.ClientID%>').val() == "HQ" && $('#<%=hidGidLocationcode.ClientID%>').val() != $('#<%=hidCurLocation.ClientID%>').val()) {
                fncCreateTransferOut();
            }
            else {
                fncGetGridValuesForSave();
                fncGetMultiUOMForSave();
                if (saveStatus == false) {
                    saveStatus = true;
                    $('#<%=btnSave.ClientID%>').click();
                }
            }
        }
}
    catch (err) {
        fncToastError(err.message);
    }
}
///Close Credit and Debit Note
function fncCloseCreditAndDebitNoteDialog() {
    try {
        $("#divCreditandDebitNote").dialog('destroy');
        shortSupplyStatus = "";
        excessSupplyStatus = "";
        shortSupplyAmt = 0;
        excessSupplyAmt = 0;
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Open Barcode Popup
function ShowPopupBarcode() {

    dateFormat[0].selectedIndex = 1;
    dateFormat.trigger("liszt:updated");

    $("#divBarcode").dialog({
        appendTo: 'form:first',
        title: "Barcode Print",
        width: 500,
        modal: true

    });
    setTimeout(function () {
        expiredDate.select().focus();
    }, 50);
}

///Barcode Validation
function fncBarcodeQtyValidation() {
    try {
        var status = "";
        if ($("#tblGRN tbody").length > 0) {
            $("#tblGRN tbody").children().each(function () {
                if ($(this).find('td input[id*=txtPrintQty]').val() != "" && parseFloat($(this).find('td input[id*=txtPrintQty]').val()) != 0) {
                    status = "Qty"
                }
            });

            if (status == "") {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_GRNPrint%>');
                return false;
            }
            else if (status == "Qty") {
                ShowPopupBarcode();
                return true;
            }


        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

var pkdDate;
var expiredDate;
var dateFormat;
var hideMRP;
var hidePrice;
var hidepkdDate;
var hideexpDate;
var hidecompany;
var template;
var size;
var print;
//Get Barcode User Comtrol
$(function () {

    try {

        pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
        expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
        dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
        hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
        hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
        hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
        hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
        hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
        template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
        size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
        print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");


    }
    catch (err) {
        fncToastError(err.message);
    }

});

///Change days and months
function fncDateMonth() {
    try {

        if ($(DateRadioButton).is(':checked')) {
            $(DateLable).text('Best Before (Days)');
        }
        else if ($(MonthRadioButton).is(':checked')) {
            $(DateLable).text('Best Before (Months)');
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Template
function fncTemplateChange() {
    try {
        

        var value;
        value = template.val();
        size.val(value.split('#')[1]);
    }
    catch (err) {
        fncToastError(err.message);
    }
}



//Save Dialog Initialation
function fncBarcodeInitialization() {
    try {

        $(function () {
            $("#barcodeprinting").html('<%=Resources.LabelCaption.Save_Barcodeprinting%>');
            $("#barcodeprinting").dialog({
                title: "Enterpriser Web",
                buttons: {
                    Ok: function () {
                        //$(this).dialog('close');                            
                        $(this).dialog("destroy");
                    }
                },
                modal: true
            });
        });


    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Barcode Validation
function fncBarcodeValidation() {
    try {
        if (size.val() == "") {
            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
            return false;
        }
        else {
            fncGetGridValuesForSave();
            $("#<%=lnkbtnPrint.ClientID %>").click();
            return false;
        }

    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Repeater Column Value Changed status
function fncRepeaterValueChanged(source) {
    try {
        var rowObj;
        rowObj = $(source).parent().parent();

        var status = $.inArray(rowObj.find('td input[id*="txtSNo"]').val(), changeRow) > -1

        if (status == false) {
            changeRow.push(rowObj.find('td input[id*="txtSNo"]').val());
            $("#<%=hidChangeRowval.ClientID %>").val(changeRow);
        }

    }
    catch (err) {
        fncToastError(err.message);
    }
}

//Open Medical Batch Dia
function fncInitializeMedicalBatchDialog(value) {
    try {
        var lQty;

        if ($('#<%=txtLQty.ClientID%>').val() != "") {
            lQty = parseFloat($('#<%=txtLQty.ClientID%>').val()).toFixed(2);
            $('#<%=txtLQty.ClientID%>').val(lQty);
        }


        MBatchStatus = value;
        if ($('#<%=hidAllowExpireDate.ClientID%>').val() == "1") {

            $('#<%=txtMBatchNo.ClientID%>').val('');
            $('#<%=hidMBatchNo.ClientID %>').val('');
            $('#<%=hidExpireDate.ClientID %>').val('')

            $("#divMedicalBatch").dialog({
                resizable: false,
                height: 146,
                width: 300,
                modal: true,
                title: "Medical Batch"
            });

            setTimeout(function () {
                $('#<%=txtMBatchNo.ClientID%>').select();
            }, 50);

            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    ///Medical Batch Validation
    function fncMedicalBatchOkClick() {
        try {
            //var currentDatesqlformat_Master = $.datepicker.formatDate('yy-mm-dd', new Date());
            var expireDate;
            expireDate = $('#<%=txtExpireDate.ClientID %>').val();
            expireDate = expireDate.split("/").reverse().join("-");

            if ($('#<%=txtMBatchNo.ClientID%>').val() == "") {
                fncToastInformation('<%=Resources.LabelCaption.alert_medicalBatchNo%>');
                $('#<%=txtMBatchNo.ClientID%>').select();
                return;
            }

            $("#divMedicalBatch").dialog('close');
            $('#<%=hidMBatchNo.ClientID %>').val($('#<%=txtMBatchNo.ClientID%>').val());
            $('#<%=hidExpireDate.ClientID %>').val(expireDate)

            if (MBatchStatus == "LQty")
                $('#<%=txtMRP.ClientID %>').select().focus();
            else if (MBatchStatus == "Add") {
                if (fncAddValidation() != false)
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                    fncAddEnterItems();
            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncGridHeaderColorChange() {
        try {
            if ($("#<%=hidGidGSTFlag.ClientID%>").val() == "2") {
                $(".Payment_fixed_headers thead").css("background-color", "red");
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    //Row Array Clear
    function fncRowArrayClear() {
        try {

            fncGridHeaderColorChange();
            fncAddVetDetailForPoandHold();


            changeRow = [];
            $("#<%=hidChangeRowval.ClientID %>").val('');
            fncSubClear('row');

            /// to move scroll bottom
            var scroll = $("#divGRNtbl")[0].scrollHeight;
            $("#divGRNtbl").scrollTop(scroll);


        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    ///Load Print Qty
    $(document).ready(function () {
        $("#<%=cbPrintBarcodeAllItem.ClientID %>").click(function () {
            try {
                var checked = $(this).is(':checked');
                var rowObj, qty;
                if (checked) {
                    $("#tblGRN [id*=GRNBodyrow]").each(function () {
                        rowObj = $(this);
                        qty = rowObj.find('td input[id*="txtlQty"]').val();
                        rowObj.find('td input[id*="txtPrintQty"]').val(qty);
                    });
                } else {
                    $("#tblGRN [id*=GRNBodyrow]").each(function () {
                        rowObj = $(this);
                        rowObj.find('td input[id*="txtPrintQty"]').val(0);
                    });
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        });
    });



    //Show Popup After Save
    function fncShowBarcodeSuccessMessage() {
        try {

            fncToastInformation('<%=Resources.LabelCaption.Save_Barcodeprinting%>');

        }
        catch (err) {
            fncToastError(err.message);
            //console.log(err);
        }
    }


    //close Price change alert
    function fncClosePriceAlert(event) {
        try {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

            if (keyCode == 27) {
                return true;
            }
            else
                return false;
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// Set Focus next control based on basic cost 
    //enable and disable
    function fncMRPEnterTraversal(evt) {
        try {
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13) {

                if ($('#<%=txtBCost.ClientID%>').is(':disabled')) {
                    fncSetFocustoObject($('#<%=txtSPrice.ClientID%>'), evt);
                }
                else {
                    fncSetFocustoObject($('#<%=txtBCost.ClientID%>'), evt);
                }
                return false;
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }



    ///Show Selected Row with color
    function fncRowClick(source) {
        try {

            var obj;
            obj = $(source);
            //$(obj).css("background-color", "#80b3ff");
            //$(obj).siblings().css("background-color", "white");
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// Search Row Click
    function fncSearchRowDoubleClick() {
        try {

            var obj, Itemcode, batchNo, vendorcode
            $("#divItemSearch").css("display", "none");
            $("#divGRN").css("display", "block");


            Itemcode = $.trim($(this).find('td[id*=tdItemcode]').text());
            $('#<%=txtItemCode.ClientID%>').val($.trim($(this).find('td[id*=tdItemcode]').text()));

            batchNo = $.trim($(this).find('td[id*=tdBatchNo]').text())

            if ($('#<%=cbVendorItem.ClientID %>').is(":checked"))
                vendorcode = $('#<%=txtVendorCode.ClientID %>').val();
            else
                vendorcode = "";


            fncGetInventoryDetailForGID(Itemcode, batchNo, vendorcode);



        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    //// GRN Row Double Click
    function fncGRNRowdblClick(source) {
        try {
            fncEnableAllColumns();
        }
        catch (err) {
            fncToastError(err.message);
        }

    }

    function fncVendorNameKeyPress(event) {
        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        try {
            if (keyCode == 39 || keyCode == 37) {
                return true;
            }
            else {
                return false;
            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// delete  Added Row
    function fncGRNRowKeydown(evt, source) {
        var charCode, rowobj, scrollheight;
        try {
            rowobj = $(source);
            charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 46) {
                $(source).remove();
                fncUpdateRowNow();

            }
            else if (charCode == 113) {
                fncOpenItemhistory(rowobj.find('td input[id*="txtItemcode"]').val());
            }


        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    //// Numeric Validation
    function fncDecimal() {

        try {
            $('#<%=txtWQty.ClientID%>').number(true, 0);
            $('#<%=txtLQty.ClientID%>').number(true, 2);
            $('#<%=txtMRP.ClientID%>').number(true, 2);
            $('#<%=txtBCost.ClientID%>').number(true, 4);
            $('#<%=txtDiscPer.ClientID%>').number(true, 2);
            $('#<%=txtDiscAmt.ClientID%>').number(true, 4);
            $('#<%=txtSDPer.ClientID%>').number(true, 2);
            $('#<%=txtSchemeAmt.ClientID%>').number(true, 4);
            $('#<%=txtGrossCost.ClientID%>').number(true, 4);
            $('#<%=txtITax1.ClientID%>').number(true, 2);
            $('#<%=txtITax2.ClientID%>').number(true, 2);
            $('#<%=txtcess.ClientID%>').number(true, 2);
            $('#<%=txtAddCess.ClientID%>').number(true, 2);
            $('#<%=txtNetCost.ClientID%>').number(true, 4);
            $('#<%=txtSPrice.ClientID%>').number(true, 2);
            $('#<%=txtMFixed.ClientID%>').number(true, 2);
            $('#<%=txtEarned.ClientID%>').number(true, 2);
            $('#<%=txtTotQty.ClientID%>').number(true, 2);
            $('#<%=txtITaxper1.ClientID%>').number(true, 2);
            $('#<%=txtITaxper2.ClientID%>').number(true, 2);
            $('#<%=txtCessPer.ClientID%>').number(true, 2);
            $('#<%=txtTotalGBCost.ClientID%>').number(true, 4);
            $('#<%=txtTotalGCost.ClientID%>').number(true, 4);
            $('#<%=txtGRDisc.ClientID%>').number(true, 4);
            $('#<%=txtGRShDiscAmt.ClientID%>').number(true, 4);
            $('#<%=txtGRVat.ClientID%>').number(true, 4);
            $('#<%=txtGRCessAmt.ClientID%>').number(true, 4);
            $('#<%=txtGRAddCess.ClientID%>').number(true, 2);
            $('#<%=txtTotal.ClientID%>').number(true, 4);
            $('#<%=txtWpirce1.ClientID%>').number(true, 2);
            $('#<%=txtWpirce2.ClientID%>').number(true, 2);
            $('#<%=txtWpirce3.ClientID%>').number(true, 2);
            $('#<%=txtoldCost.ClientID%>').number(true, 2);
            $('#<%=txtOldMRP.ClientID%>').number(true, 2);
            $('#<%=txtOldSelling.ClientID%>').number(true, 2);
            $('#<%=txtoldMarginfixed.ClientID%>').number(true, 2);
            $('#<%=txtoldMarginEarned.ClientID%>').number(true, 2);
            $('#<%=txtNewCost.ClientID%>').number(true, 2);
            $('#<%=txtNewMRP.ClientID%>').number(true, 2);
            $('#<%=txtNewSelling.ClientID%>').number(true, 2);
            $('#<%=txtNewMarginFixed.ClientID%>').number(true, 2);
            $('#<%=txtTotalRow.ClientID%>').number(true, 0);
            $('#<%=txtFocTotalQty.ClientID%>').number(true, 2);
            $('#<%=txtFocBuyQty.ClientID%>').number(true, 2);
            $('#<%=txtFocFreeQty.ClientID%>').number(true, 2);
            $('#<%=txtGST.ClientID%>').number(true, 2);
            $('#<%=txtMrpT.ClientID%>').number(true, 2);
            $('#<%=txtSellingPriceT.ClientID%>').number(true, 2);
            fncGRNGridDecimal();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    function fncGRNGridDecimal() {
        try {
            $('td input[id*="txtFoc_"]').number(true, 2);
            $('td input[id*="txtMRP_"]').number(true, 2);
            $('td input[id*="txtSellingPrice_"]').number(true, 2);
            $('td input[id*="txtWPrice1_"]').number(true, 2);
            $('td input[id*="txtWPrice2_"]').number(true, 2);
            $('td input[id*="txtWPrice3_"]').number(true, 2);
            $('td input[id*="txtPrintQty_"]').number(true, 0);
            $('td input[id*="txtLandingCost_"]').number(true, 4);

        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    function fncTextDecimalRestrictionwithUOM() {
        try {
            if ($('#<%=txtluom.ClientID%>').val().trim() == "PCS") {
                $('#<%=txtInvQty.ClientID%>').number(true, 0);
                $('#<%=txtLQty.ClientID%>').number(true, 0);

            }
            else {
                $('#<%=txtInvQty.ClientID%>').number(true, 2);
                $('#<%=txtLQty.ClientID%>').number(true, 2);
            }

        }
        catch (err) {
            fncToastError(err.message);
        }

    }

    function fncEnterkeyTraversal(evt, value) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        try {

            if (charCode == 13) {
                if (value == "SchemeAmt") {
                    if ($('#<%=txtAddCess.ClientID%>').is(":disabled")) {
                        if ($('#<%=hidGSTEdit.ClientID%>').val() == "Y") {
                            setTimeout(function () {
                                $('#<%=ddlGST.ClientID %>').trigger("liszt:open");
                            }, 100);
                            //fncOpenDropdownlist();
                        }
                        else {
                            $('#<%=txtSPrice.ClientID%>').select();
                        }

                    }
                    else {
                        $('#<%=txtAddCess.ClientID%>').select();

                    }
                }
                return false;
            }


        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncAdditionalCessAmtCal() {
        var AddCessamt = 0, InvQty = 0, NetCost = 0, total = 0;
        var iTaxper1 = 0, iTaxper2 = 0, cessper = 0, grossCost = 0;
        var ActualGST = 0, cessAmt = 0, netSellingPrice = 0, mrp = 0;
        var outPutGSt = 0, basicSelling = 0;
        try {

            iTaxper1 = $('#<%=txtITaxper1.ClientID%>').val();
            iTaxper2 = $('#<%=txtITaxper2.ClientID%>').val();
            cessper = $('#<%=txtcess.ClientID%>').val();
            grossCost = $('#<%=txtGrossCost.ClientID%>').val();

            ActualGST = fncTaxAmtCalculation(grossCost, iTaxper1, iTaxper2);
            cessAmt = fncCessCalc(grossCost, cessper);

            NetCost = (parseFloat(grossCost) + parseFloat(ActualGST) + parseFloat(cessAmt)).toFixed(4);
            $('#<%=txtNetCost.ClientID%>').val(NetCost);

            $('#<%=txtTotal.ClientID%>').val((parseFloat($('#<%=txtInvQty.ClientID%>').val()) * parseFloat(NetCost)).toFixed(2));

            /// Total Addiotional Cess Amt
            AddCessamt = $('#<%=txtAddCess.ClientID%>').val();
            total = $('#<%=txtTotal.ClientID%>').val();
            $('#<%=txtTotal.ClientID%>').val(parseFloat(AddCessamt) + parseFloat(total));
            $('#<%=txtGRAddCess.ClientID%>').val(AddCessamt)

            // Unit of Addtional Cess Amt
            InvQty = $('#<%=txtInvQty.ClientID%>').val();
            AddCessamt = parseFloat(AddCessamt) / parseFloat(InvQty);
            $('#<%=txtAddCess.ClientID%>').val(AddCessamt)

            NetCost = $('#<%=txtNetCost.ClientID%>').val();
            $('#<%=txtNetCost.ClientID%>').val(parseFloat(NetCost) + parseFloat(AddCessamt));

            NetCost = $('#<%=txtNetCost.ClientID%>').val();

            if ($('#<%=hidMarginFixType.ClientID%>').val() == "M") {
                netSellingPrice = parseFloat(mrp) - (parseFloat(mrp) * parseFloat($('#<%=txtMFixed.ClientID%>').val())) / 100;
                outPutGSt = fncArriveInclusiveTax(netSellingPrice, iTaxper1, iTaxper2, cessper);
                basicSelling = parseFloat(netSellingPrice) - parseFloat(outPutGSt);
                $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));

            }
            else {
                netSellingPrice = parseFloat(NetCost) + (parseFloat(NetCost) * parseFloat($('#<%=txtMFixed.ClientID%>').val())) / 100;
                basicSelling = parseFloat(netSellingPrice) - parseFloat(ActualGST) - parseFloat(cessAmt);
                $('#<%=hidBasicSellingPrice.ClientID%>').val(basicSelling.toFixed(2));

            }

            $('#<%=txtSPrice.ClientID%>').val(netSellingPrice.toFixed(2));

        }
        catch (err) {
            fncToastError(err.message);
        }

    }

    function fncWholeQtyCal() {
        try {

            if ($('#<%=hidMultiUOM.ClientID%>').val() == "Y") {
                $('#<%=hidUOMConv.ClientID%>').val($('#<%=ddlMultipleUOM.ClientID%>').val());
            }

            $('#<%=txtInvQty.ClientID%>').val(parseFloat($('#<%=txtWQty.ClientID%>').val()) * parseFloat($('#<%=hidUOMConv.ClientID%>').val()));
            //$('#<%=txtMRP.ClientID%>').select();
            fncInvQtyFocusOut();
        }
        catch (err) {
            fncToastError(err.message);
        }

    }

    function fncMedicalBatchEnterKey(evt, value) {

        var charCode = (evt.which) ? evt.which : evt.keyCode;
        try {
            if (charCode == 13) {
                if (value == "batchno") {
                    $('#<%=txtExpireDate.ClientID%>').select();
                    return false;
                }
                else {
                    fncMedicalBatchOkClick();
                    return false;
                }
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncCheckDeActiveStatus() {
        var infor = "";
        var deActiveMRP = [];

        try {
            if ($('#<%=hidisbatch.ClientID%>').val() == "1") {
                deActiveMRP = $('#<%=hidDeActiveMRP.ClientID%>').val().split(',');

                for (var i = 0; i < deActiveMRP.length; i++) {
                    if (parseFloat(deActiveMRP[i]) == parseFloat($('#<%=txtMRP.ClientID%>').val())) {
                        infor = "Please activate this item.<br/> InventoryCode :" + $('#<%=txtItemCode.ClientID%>').val().trim();
                        infor = infor + "  MRP :" + $('#<%=txtMRP.ClientID%>').val();
                        popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject(infor);
                        fncSubClear('clr');
                        return false;
                    }

                }

            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    /// Add New To Grid
    function fncAddNewRowsToGrid(maxRowNo, itemcode, ItemDesc, POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
    WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
    GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
    GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
    PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
    PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, LandingCost, PurchaseProfitper,
    NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
    AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
    FTCBatchNo, FTCInputQty, WQty, WUOM, StyleCode, SerialNo) {
        var tblGRNBody, row;
        try {
            LandingCost = $('#<%=hidLanaCost.ClientID%>').val() == "Y" ? NetCost : "0"; // "0";
                tblGRNBody = $("#tblGRN tbody");
                var rowno = "rowno";
                row = "<tr id='GRNBodyrow_" + maxRowNo + "' onkeydown=' return fncGRNRowKeydown(event,this);' ondblclick='fncGRNRowdblClick(this);' onclick='fncRowClick(this);' tabindex='" + maxRowNo + "'  >"
                    + "<td><input type='text' id='txtSNo_" + maxRowNo + "' value='" + maxRowNo + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"RowNo\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                   + "<td></td>"
                   + "<td><input type='text' id='txtPONo_" + maxRowNo + "' value='" + poNo + "'  "
                + "onfocus='this.select();' Class='border_nonewith_yellow form-control-res txt_align_left'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"PONo\");' onkeypress='return fnctblColEnableFalse(event);'/></td>"
                + "<td><input type='text' id='txtItemcode_" + maxRowNo + "' value='" + itemcode + "'  "
                + "onfocus='this.select();' Class='border_nonewith_yellow form-control-res txt_align_left' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ItemCode\");' onkeypress='return fnctblColEnableFalse(event);'/></td>"
                + "<td><input type='text' id='txtItemDesc_" + maxRowNo + "' value='" + ItemDesc + "'  "
                + "onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' Class='border_nonewith_yellow form-control-res txt_align_left' "
                + "onkeydown='return fncSetFocusNextRowandCol(event,this,\"ItemDesc\");'/></td>"
                + "<td><input type='text' id='txtPOQty_" + maxRowNo + "' value='" + POQty + "'  "
                + "onfocus='this.select();'  onkeypress='return fnctblColEnableFalse(event);' Class='border_nonewith_yellow form-control-res' "
                + "onkeydown='return fncSetFocusNextRowandCol(event,this,\"POQty\");'/></td>"
                + "<td><input type='text' id='txtInvQty_" + maxRowNo + "' value='" + InvQty + "'  "
                + "Class='grn_rptr_textbox_width form-control-res'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"InvQty\");' onchange='fncRepeaterColumnValueChange(this,\"InvQty\");' onkeypress='return isNumberKeyWithDecimalNew(event);' onfocus='this.select();'/></td>"
                + "<td><input type='text' id='txtlQty_" + maxRowNo + "' value='" + lQty + "'  "
                + "Class='grn_rptr_textbox_width form-control-res'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"LQty\");' onFocus='this.select();' onchange='fncRepeaterValueChanged(this)' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td><input type='text'  id='txtFoc_" + maxRowNo + "'  value='" + parseFloat(Foc).toFixed(2) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res'  "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Foc\");' onfocus='this.select();' onchange='fncRepeaterValueChanged(this)' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td><input type='text' id='txtMRP_" + maxRowNo + "' value='" + parseFloat(MRP).toFixed(2) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res' onfocus='this.select();' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"MRP\");' onchange='fncRepeaterColumnValueChange(this,\"MRP\")' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td id='tdBasicCost_" + maxRowNo + "' ><input type='text' id='txtBasicCost_" + maxRowNo + "' value='" + BasicCost + "' onfocus='this.select();'  "
                + "Class='grn_rptr_textbox_width form-control-res' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"BasicCost\");'"
                + "onchange='fncRepeaterColumnValueChange(this,\"BasicCost\")'/></td>"
                + "<td id='tdSellingPrice_" + maxRowNo + "' ><input type='text' id='txtSellingPrice_" + maxRowNo + "' value='" + parseFloat(SellingPrice).toFixed(2) + "' onfocus='this.select();' "
                + "Class='grn_rptr_textbox_width form-control-res'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"SellingPrice\");'"
                + "onchange='fncRepeaterColumnValueChange(this,'sellingprice');' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td id='tdWPrice1_" + maxRowNo + "' ><input type='text' id='txtWPrice1_" + maxRowNo + "' value='" + parseFloat(WPrice1).toFixed(2) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Wprice1\");'"
                + "onfocus='this.select();' onchange='fncRepeaterValueChanged(this)' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td id='tdWPrice2_" + maxRowNo + "' ><input type='text' id='txtWPrice2_" + maxRowNo + "' value='" + parseFloat(WPrice2).toFixed(2) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Wprice2\");' onfocus='this.select();' onchange='fncRepeaterValueChanged(this)' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td id='tdWPrice3_" + maxRowNo + "' ><input type='text' id='txtWPrice3_" + maxRowNo + "' value='" + parseFloat(WPrice3).toFixed(2) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Wprice3\");' onfocus='this.select();'"
                + "onchange='fncRepeaterValueChanged(this)' onkeypress='return isNumberKeyWithDecimalNew(event);'/></td>"
                + "<td id='tdGrossBasicAmt_" + maxRowNo + "' ><input type='text' id='txtGrossBasicAmt_" + maxRowNo + "' value='" + parseFloat(GrossBasicAmt).toFixed(4) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossBasicAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdDiscPer_" + maxRowNo + "' ><input type='text' id='txtDiscPer_" + maxRowNo + "' value='" + DiscPer + "' onfocus='this.select();' "
                + "Class='grn_rptr_textbox_width form-control-res' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DiscPer\");' onchange='fncRepeaterColumnValueChange(this,\"DiscPer\")' onkeypress='return isNumberKeyWithDecimalNew(event);' /></td>"
                + "<td id='tdDiscAmt_" + maxRowNo + "' ><input type='text' id='txtDiscAmt_" + maxRowNo + "' value='" + DiscAmt + "'  "
                + "Class='grn_rptr_textbox_width form-control-res' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DiscAmt\");' onchange='fncRepeaterColumnValueChange(this,\"DiscAmt\")' onkeypress='return isNumberKeyWithDecimalNew(event);' /></td>"
                + "<td id='tdGrossDiscAmt_" + maxRowNo + "' ><input type='text' id='txtGrossDiscAmt_" + maxRowNo + "' value='" + parseFloat(GrossDiscAmt).toFixed(2) + "' onfocus='this.select();' "
                + "Class='grn_rptr_textbox_width form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossDiscAmt\");' "
                + " onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdSDPer_" + maxRowNo + "' ><input type='text' id='txtSDper_" + maxRowNo + "' value='" + SDper + "' onfocus='this.select();' "
                + "Class='grn_rptr_textbox_width form-control-res'  "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"SDper\");' onchange='fncRepeaterColumnValueChange(this,\"ShDiscPer\")' onkeypress='return isNumberKeyWithDecimalNew(event);' /></td>"
                + "<td id='tdSDAmt_" + maxRowNo + "' ><input type='text' id='txtSDAmt_" + maxRowNo + "' value='" + SDAmt + "' onfocus='this.select();' "
                + "Class='grn_rptr_textbox_width form-control-res ' "
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"SDAmt\");' onchange='fncRepeaterColumnValueChange(this,\"ShDiscAmt\")' onkeypress='return isNumberKeyWithDecimalNew(event);' /></td>"
                + "<td id='tdGrossShDiscAmt_" + maxRowNo + "' ><input type='text' id='txtGrossShDiscAmt_" + maxRowNo + "' value='" + parseFloat(GrossShDiscAmt).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'   onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossShDiscAmt\");' onkeypress='return fnctblColEnableFalse(event);' onfocus='this.select();' /></td>"
                + "<td id='tdGrossCost_" + maxRowNo + "' ><input type='text' id='txtGrossCost_" + maxRowNo + "' value='" + parseFloat(GrossCost).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossCost\");'"
                + "onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdTotalGrossCost_" + maxRowNo + "' ><input type='text' id='txtTotalGrossCost_" + maxRowNo + "' value='" + parseFloat(TotalGrossCost).toFixed(2) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'   onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TotalGrossCost\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdITaxperc1_" + maxRowNo + "' ><input type='text' id='txtITaxperc1_" + maxRowNo + "' value='" + ITaxperc1 + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxperc1\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdITaxperc2_" + maxRowNo + "' ><input type='text' id='txtITaxperc2_" + maxRowNo + "' value='" + ITaxperc2 + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxperc2\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdCessper_" + maxRowNo + "' ><input type='text' id='txtCessper_" + maxRowNo + "' value='" + Cessper + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Cessper\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdITaxAmt1_" + maxRowNo + "' ><input type='text' id='txtITaxAmt1_" + maxRowNo + "' value='" + ITaxAmt1 + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxAmt1\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdITaxAmt2_" + maxRowNo + "' ><input type='text' id='txtITaxAmt2_" + maxRowNo + "' value='" + ITaxAmt2 + "'  "
                + " Class='border_nonewith_yellow form-control-res-right'onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxAmt2\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdCessAmt_" + maxRowNo + "' ><input id='txtCessAmt_" + maxRowNo + "' value='" + CessAmt + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"CessAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdAddCessAmt_" + maxRowNo + "' ><input type='text' id='txtAddCessAmt_" + maxRowNo + "' value='" + parseFloat(AddCessAmt).toFixed(2) + "'  "
                + " Class='grn_rptr_textbox_width form-control-res-right border_nonewith_yellow' onchange='fncRepeaterColumnValueChange(this,\"AddCessAmt\")'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"AddCessAmt\");' onkeypress='return fnctblColEnableFalse(event);' onfocus='this.select();' /></td>"
                + "<td id='tdGrossVATAmt_" + maxRowNo + "' ><input type='text' id='txtGrossVATAmt_" + maxRowNo + "' value='" + parseFloat(GrossVATAmt).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'   onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossVATAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdGrocessCessAmt_" + maxRowNo + "' ><input type='text' id='txtGrocessCessAmt_" + maxRowNo + "' value='" + parseFloat(GrocessCessAmt).toFixed(4) + "'  "
                + "Class='grn_rptr_textbox_width form-control-res-right border_nonewith_yellow' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrocessCessAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdGRAddCessAmt_" + maxRowNo + "' ><input type='text' id='txtGRAddCessAmt_" + maxRowNo + "' value='" + parseFloat(GRAddCessAmt).toFixed(2) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GRAddCessAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdtNetCost_" + maxRowNo + "' ><input type='text' id='txtNetCost_" + maxRowNo + "' value='" + parseFloat(NetCost).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"NetCost\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdTotalValue_" + maxRowNo + "' ><input type='text' id='txtTotalValue_" + maxRowNo + "' value='" + parseFloat(TotalValue).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TotalValue\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdtFTotalValue_" + maxRowNo + "' ><input type='text' id='txtFTotalValue_" + maxRowNo + "' value='" + FTotalValue + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"FTotalValue\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdEarnedMarPerc_" + maxRowNo + "' ><input type='text' id='txtEarnedMarPerc_" + maxRowNo + "' value='" + EarnedMarPerc + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"EarnedMarPerc\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdEarMarAmt_" + maxRowNo + "' ><input type='text' id='txtEarMarAmt_" + maxRowNo + "' value='" + EarMarAmt + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"EarMarAmt\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdMarginfixed_" + maxRowNo + "' ><input type='text' id='txtMarginfixed_" + maxRowNo + "' value='" + parseFloat(Marginfixed).toFixed(2) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'   onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Marginfixed\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdPrintQty_" + maxRowNo + "' ><input id='txtPrintQty_" + maxRowNo + "' value='" + PrintQty + "'  "
                + "Class='grn_rptr_textbox_width form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"PrintQty\");' onFocus='this.select()' /></td>"
                + "<td id='tdITaxt1_" + maxRowNo + "' ><input type='text' id='txtITaxt1_" + maxRowNo + "' value='" + ITaxt1 + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxt1\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdITaxt2_" + maxRowNo + "' ><input type='text' id='txtITax2_" + maxRowNo + "' value='" + ITax2 + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ITaxt2\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdMedBatch_" + maxRowNo + "' ><input type='text' id='txtMedBatch_" + maxRowNo + "' value='" + MedBatch + "'  "
                + "Class='border_nonewith_yellow form-control-res'   onkeydown=' return fncSetFocusNextRowandCol(event,this,\"MedBatch\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdExpDate_" + maxRowNo + "' ><input type='text' id='txtExpDate_" + maxRowNo + "' value='" + ExpDate + "'  "
                + "Class='border_nonewith_yellow form-control-res'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ExpDate\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td ><input id='txtOldEarnedMarg_" + maxRowNo + "' value='" + OldEarnedMarg + "'  "
                + "Class='grn_rptr_textbox_width form-control-res-right'  onfocus='this.select();' ReadOnly='true' /></td>"
                + "<td id='tdProfitMargin_" + maxRowNo + "'  ><input type='text' id='txtProfitMargin_" + maxRowNo + "' value='" + parseFloat(ProfitMargin).toFixed(2) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"ProfitMargin\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdFOCItem_" + maxRowNo + "'  ><input type='text' id='txtFOCItem_" + maxRowNo + "' value='" + FOCItem + "'  "
                + "Class='border_nonewith_yellow form-control-res' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"FOCItem\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdFOCBatch_" + maxRowNo + "'  ><input type='text' id='txtFOCBatch_" + maxRowNo + "' value='" + FOCBatch + "'  "
                + "Class='border_nonewith_yellow form-control-res'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"FOCBatch\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdPurchaseCharges_" + maxRowNo + "'  ><input type='text' id='txtPurchaseCharges_" + maxRowNo + "' value='" + PurchaseCharges + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"PurchaseCharges\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdFreightCharges_" + maxRowNo + "'  ><input type='text' id='txtFreightCharges_" + maxRowNo + "' value='" + FreightCharges + "'  "
                + " Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"FreightCharges\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdVendorMargin_" + maxRowNo + "'  ><input type='text' id='txtVendorMargin_" + maxRowNo + "' value='" + VendorMargin + "'  "
                + "Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"VendorMargin\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdTotalCharges_" + maxRowNo + "'  ><input type='text' id='txtTotalCharges_" + maxRowNo + "' value='" + TotalCharges + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TotalCharges\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"
                + "<td id='tdLandingCost_" + maxRowNo + "'  ><input type='text' id='txtLandingCost_" + maxRowNo + "' value='" + parseFloat(LandingCost).toFixed(4) + "'  "
                + "Class='border_nonewith_yellow form-control-res-right'  onkeydown=' return fncSetFocusNextRowandCol(event,this,\"LandingCost\");' onfocus='this.select();' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                + "<td id='tdStyleCode_" + maxRowNo + "'  ><input type='text' id='txtStyleCode_" + maxRowNo + "' value='" + StyleCode + "'  "
                + "Class='grn_rptr_textbox_width form-control-res'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"StyleCode\");' onchange='fncRepeaterColumnValueChange(this,\"StyleCode\");' onkeypress='return isNumberKeyWithDecimalNew(event);' onfocus='this.select();'/></td>"

                    + "<td id='tdSerialNo_" + maxRowNo + "'  ><input type='text' id='txtSerialNo_" + maxRowNo + "' value='" + maxRowNo + "'  "
                + "Class='grn_rptr_textbox_width form-control-res'"
                + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"SerialNo\");' onchange='fncRepeaterColumnValueChange(this,\"SerialNo\");' onkeypress='return isNumberKeyWithDecimalNew(event);' onfocus='this.select();'/></td>"

                + "<td ><span id='lblPurchaseProfitper_" + maxRowNo + "'>" + PurchaseProfitper + "</span></td>"
                + "<td ><span id='lblNetCostAffect_" + maxRowNo + "'>" + NetCostAffect + "</span></td>"
                + "<td ><span id='lblBatch_" + maxRowNo + "'>" + Batch + "</span></td>"
                + "<td ><span id='lblItemType_" + maxRowNo + "'>" + ItemType + "</span></td>"
                + "<td ><span id='lblPricingType_" + maxRowNo + "'>" + PricingType + "</span></td>"
                + "<td ><span id='lblMarkDownPerc_" + maxRowNo + "'>" + MarkDownPerc + "</span></td>"
                + "<td ><span id='lblOldSellingPrice_" + maxRowNo + "'>" + OldSellingPrice + "</span></td>"
                + "<td ><span id='lblAvgCost_" + maxRowNo + "'>" + AvgCost + "</span></td>"
                + "<td ><input type='hidden' id='hidBasicSelling_" + maxRowNo + "' value='" + BasicSelling + "' /></td>"
                + "<td ><input type='hidden' id='hidItemBatchNo_" + maxRowNo + "' value='" + ItemBatchNo + "' /></td>"
                + "<td ><span id='lblAllowExpireDate_" + maxRowNo + "'>" + AllowExpireDate + "</span></td>"
                + "<td ><span id='lblFTCFromdate_" + maxRowNo + "'>" + FTCFromdate + "</span></td>"
                + "<td ><span id='lblFTCToDate_" + maxRowNo + "'>" + FTCToDate + "</span></td>"
                + "<td ><span id='lblFTCBuyQty_" + maxRowNo + "'>" + FTCBuyQty + "</span></td>"
                + "<td ><span id='lblFTCFreeQty_" + maxRowNo + "'>" + FTCFreeQty + "</span></td>"
                + "<td ><span id='lblGridRowStatus_" + maxRowNo + "'>" + GridRowStatus + "</span></td>"
                + "<td ><span id='lblPrvData_" + maxRowNo + "'>" + PrvData + "</span></td>"
                + "<td ><span id='lblFTCItemcode_" + maxRowNo + "'>" + FTCItemcode + "</span></td>"
                + "<td ><span id='lblFTCBatchNo_" + maxRowNo + "'>" + FTCBatchNo + "</span></td>"
                + "<td ><span id='lblFTCInputQty_" + maxRowNo + "'> " + FTCInputQty + " </span></td>"
                + "<td ><span id='lblWQty_" + maxRowNo + "'> " + WQty + " </span></td>"
                + "<td ><span id='lblWUOM_" + maxRowNo + "'> " + WUOM + " </span></td>"
                  + "</tr>";
                
                tblGRNBody.append(row);

                if (poItem == "Y") {
                    $("#GRNBodyrow_" + maxRowNo + "").css("background-color", "green");
                }
                poNo = "0";


            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncAddEnterItems() {
            var itemcode, ItemDesc, POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
            WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
            GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
            GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
            PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
            PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, TotalCharges, LandingCost, PurchaseProfitper,
            NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
            AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
            FTCBatchNo, FTCInputQty, WQty, WUOM, StyleCode, SerialNo;
            try {
                itemcode = $('#<%=txtItemCode.ClientID %>').val();
                ItemDesc = $('#<%=txtDescription.ClientID %>').val().replace('<', '(').replace('>', ')');
                POQty = "0";
                InvQty = $('#<%=txtInvQty.ClientID %>').val();
                lQty = $('#<%=txtLQty.ClientID %>').val();
                Foc = $('#<%=hidFocQty.ClientID %>').val();
                MRP = $('#<%=txtMRP.ClientID %>').val();
                BasicCost = $('#<%=txtBCost.ClientID %>').val();
                SellingPrice = $('#<%=txtSPrice.ClientID %>').val();
                WPrice1 = $('#<%=txtWpirce1.ClientID %>').val();
                WPrice2 = $('#<%=txtWpirce2.ClientID %>').val();
                WPrice3 = $('#<%=txtWpirce3.ClientID %>').val();
                GrossBasicAmt = $('#<%=txtTotalGBCost.ClientID %>').val();
                DiscPer = $('#<%=txtDiscPer.ClientID %>').val();
                DiscAmt = $('#<%=txtDiscAmt.ClientID %>').val();
                GrossDiscAmt = $('#<%=txtGRDisc.ClientID %>').val();
                SDper = $('#<%=txtSDPer.ClientID %>').val();
                SDAmt = $('#<%=txtSchemeAmt.ClientID %>').val();
                GrossShDiscAmt = $('#<%=txtGRShDiscAmt.ClientID %>').val();
                GrossCost = $('#<%=txtGrossCost.ClientID %>').val();
                TotalGrossCost = $('#<%=txtTotalGCost.ClientID %>').val();
                ITaxperc1 = $('#<%=txtITaxper1.ClientID %>').val();
                ITaxperc2 = $('#<%=txtITaxper2.ClientID %>').val();
                Cessper = $('#<%=txtCessPer.ClientID %>').val();
                ITaxAmt1 = $('#<%=hidITaxAmt1.ClientID %>').val();
                ITaxAmt2 = $('#<%=hidITaxAmt2.ClientID %>').val();
                CessAmt = $('#<%=hidCessAmt.ClientID %>').val();
                AddCessAmt = $('#<%=txtAddCess.ClientID %>').val();
                GrossVATAmt = $('#<%=txtGRVat.ClientID %>').val();
                GrocessCessAmt = $('#<%=txtGRCessAmt.ClientID %>').val();
                GRAddCessAmt = $('#<%=txtGRAddCess.ClientID %>').val();
                NetCost = $('#<%=txtNetCost.ClientID %>').val();
                TotalValue = $('#<%=txtTotal.ClientID %>').val();
                FTotalValue = "0";
                EarnedMarPerc = $('#<%=txtEarned.ClientID %>').val();
                EarMarAmt = $('#<%=hidMFAmt.ClientID %>').val();
                Marginfixed = $('#<%=txtMFixed.ClientID %>').val();
                PrintQty = "0";
                ITaxt1 = $('#<%=txtITax1.ClientID %>').val();
                ITax2 = $('#<%=txtITax2.ClientID %>').val();

                if ($('#<%=hidAllowExpireDate.ClientID %>').val() == "1") {
                    MedBatch = $('#<%=hidMBatchNo.ClientID %>').val();
                    ExpDate = $('#<%=hidExpireDate.ClientID %>').val();
                }
                else {
                    MedBatch = "0";
                    ExpDate = "0";
                }
                OldEarnedMarg = $('#<%=txtMFixed.ClientID %>').val();
                ProfitMargin = (((parseFloat($('#<%=txtSPrice.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val())) * 100).toFixed(2);//38 //$('#<%=txtLQty.ClientID %>').val();
                FOCItem = $('#<%=hidFocItem.ClientID %>').val();
                FOCBatch = $('#<%=hidFocBatch.ClientID %>').val();
                PurchaseCharges = "0";
                FreightCharges = "0";
                VendorMargin = "0";
                TotalCharges = "0";
                //LandingCost = $('#<%=hidLanaCost.ClientID%>').val() == "Y" ? NetCost : "0"; // "0";
                LandingCost = "0";
                PurchaseProfitper = (((parseFloat($('#<%=txtMRP.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtMRP.ClientID %>').val())) * 100).toFixed(2);//46;
                NetCostAffect = "";
                Batch = $('#<%=hidisbatch.ClientID %>').val();
                ItemType = $('#<%=hidItemType.ClientID %>').val();

                if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M" && $('#<%=hidMarginFixType.ClientID %>').val() == "M") {
                    PricingType = "1";/// AlWays MRP
                }
                else if ($('#<%=hidPurchasedBy.ClientID %>').val() == "M") {
                    PricingType = "2";// Purchase Only MRP
                }
                else {
                    PricingType = "";
                }
                MarkDownPerc = $('#<%=hidMarkDownPerc.ClientID %>').val();
                OldSellingPrice = $('#<%=hidOldSellingPrice.ClientID %>').val();
                AvgCost = $('#<%=hidAvgCost.ClientID %>').val();
                BasicSelling = $('#<%=hidBasicSellingPrice.ClientID %>').val();
                AllowExpireDate = $('#<%=hidAllowExpireDate.ClientID %>').val();

                ItemBatchNo = AddZeros($('#<%=txtESerialNo.ClientID%>').val(), 3); //fncCreateBatchNo($('#<%=txtMRP.ClientID %>').val());

            <%--if ($('#<%=hidAllowExpireDate.ClientID %>').val() == "0") {
                if ($('#<%=hidisbatch.ClientID %>').val() == "1") {
                    if (parseFloat($('#<%=txtMRP.ClientID %>').val()) > 9999) {
                        ItemBatchNo = Math.trunc(parseFloat($('#<%=txtMRP.ClientID %>').val())) / 1000000;
                        ItemBatchNo = ItemBatchNo + "00000000";
                        ItemBatchNo = ItemBatchNo.substring(2, 8);
                    }
                    else {
                        ItemBatchNo = Math.trunc(parseFloat($('#<%=txtMRP.ClientID %>').val()) * 100) / 1000000;
                        ItemBatchNo = ItemBatchNo + "00000000";
                        ItemBatchNo = ItemBatchNo.substring(2, 8);
                    }
                }
                else {
                    ItemBatchNo = "";
                }
            }
            else {
                ItemBatchNo = $('#<%=hidMBatchNo.ClientID %>').val();
            }--%>

                if ($('#<%=hidFocStatus.ClientID %>').val() == "FTC") {
                    FTCFromdate = $('#<%=txtfocFromDate.ClientID %>').val();
                    FTCToDate = $('#<%=txtFocToDate.ClientID %>').val();
                    FTCBuyQty = $('#<%=txtFocBuyQty.ClientID %>').val();
                    FTCFreeQty = $('#<%=txtFocFreeQty.ClientID %>').val();
                }
                else {
                    FTCBuyQty = "0";
                    FTCFreeQty = "0";
                }

                if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBreakpricestatus.ClientID %>').val() == "1") // Barcode , KG Item and Break Price
                    GridRowStatus = "BarKGBre";
                else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() != "PCS") // Barcode and KG Item 
                    GridRowStatus = "BarKG";
                else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")//KG Item and Break price
                    GridRowStatus = "KGBre";
                else if ($('#<%=txtluom.ClientID %>').val().trim() != "PCS")//KG Item 
                    GridRowStatus = "KG";
                else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode , pieces Item and Break Price
                    GridRowStatus = "BarPCSBre";
                else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1" && $('#<%=txtluom.ClientID %>').val().trim() == "PCS") // Barcode and pieces Item 
                    GridRowStatus = "BarPCS";
                else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS" && $('#<%=hidBarcodeStatus.ClientID %>').val() == "1")
                    GridRowStatus = "PCSBre";
                else if ($('#<%=txtluom.ClientID %>').val().trim() == "PCS")
                    GridRowStatus = "PCS";
                else if ($('#<%=hidBarcodeStatus.ClientID %>').val() == "1") // Barcode Only
                    GridRowStatus = "Bar";
                else
                    GridRowStatus = "";

    if ($('#<%=hidCostEditAllow.ClientID %>').val() == "0") {
                    GridRowStatus = GridRowStatus + "CEF";
                }

                PrvData = $('#<%=hidPrvMRP.ClientID %>').val() + "," + $('#<%=hidOldCost.ClientID %>').val() + ",";
                PrvData = PrvData + $('#<%=hidoldMarFixed.ClientID %>').val() + "," + $('#<%=hidoldMarEarned.ClientID %>').val() + "," + $('#<%=hidPrvUnitCost.ClientID %>').val();

                FTCItemcode = $('#<%=hidFTCItemcode.ClientID %>').val();
                FTCBatchNo = $('#<%=hidFTCBatchNo.ClientID %>').val();
                FTCInputQty = $('#<%=hidFTCInputQty.ClientID %>').val();
                WQty = $('#<%=txtWQty.ClientID %>').val();
                if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
                    WUOM = $('#<%=ddlMultipleUOM.ClientID %>').find("option:selected").text();
                }
                else {
                    WUOM = $('#<%=txtwuom.ClientID %>').val();
                }

                StyleCode = $('#<%=txtStyleCodeT.ClientID %>').val();
                SerialNo = $('#<%=txtESerialNo.ClientID %>').val();

                
                fncAddNewRowsToGrid(maxItemCountNo, itemcode, ItemDesc, POQty, InvQty, lQty, Foc, MRP, BasicCost, SellingPrice,
            WPrice1, WPrice2, WPrice3, GrossBasicAmt, DiscPer, DiscAmt, GrossDiscAmt, SDper, SDAmt, GrossShDiscAmt,
            GrossCost, TotalGrossCost, ITaxperc1, ITaxperc2, Cessper, ITaxAmt1, ITaxAmt2, CessAmt, AddCessAmt, GrossVATAmt,
            GrocessCessAmt, GRAddCessAmt, NetCost, TotalValue, FTotalValue, EarnedMarPerc, EarMarAmt, Marginfixed,
            PrintQty, ITaxt1, ITax2, MedBatch, ExpDate, OldEarnedMarg, ProfitMargin, FOCItem, FOCBatch,
            PurchaseCharges, FreightCharges, VendorMargin, TotalCharges, LandingCost, PurchaseProfitper,
            NetCostAffect, Batch, ItemType, PricingType, MarkDownPerc, OldSellingPrice, AvgCost, BasicSelling, ItemBatchNo,
            AllowExpireDate, FTCFromdate, FTCToDate, FTCBuyQty, FTCFreeQty, GridRowStatus, PrvData, FTCItemcode,
            FTCBatchNo, FTCInputQty, WQty, WUOM, StyleCode, SerialNo);

                //maxItemCountNo = parseInt(maxItemCountNo) + 1;
                maxItemCountNo = $("#tblGRN tbody").children().length;

                fncItemcodeboxDisable(true);
                $('#<%=lnkAdd.ClientID%>').css("display", "block");
                fncSubClear('clr');
                fncRowArrayClear();

                fncNetValueCalculation();
                fncSaveGRNEntryTemporarly();

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncCreateBatchNo(mrp) {
            try {
                if ($('#<%=hidAllowExpireDate.ClientID %>').val() == "0") {
                    if ($('#<%=hidisbatch.ClientID %>').val() == "1") {
                        if (parseFloat(mrp) > 9999) {
                            ItemBatchNo = Math.trunc(parseFloat(mrp)) / 1000000;
                            ItemBatchNo = ItemBatchNo + "00000000";
                            ItemBatchNo = ItemBatchNo.substring(2, 8);
                        }
                        else {
                            ItemBatchNo = Math.trunc(parseFloat(mrp) * 100) / 1000000;
                            ItemBatchNo = ItemBatchNo + "00000000";
                            ItemBatchNo = ItemBatchNo.substring(2, 8);
                        }
                    }
                    else {
                        ItemBatchNo = "";
                    }
                }
                else {
                    ItemBatchNo = $('#<%=hidMBatchNo.ClientID %>').val();
                }

                return ItemBatchNo;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncGetGridValuesForSave() {
            var obj;
            try {
                var xml = '<NewDataSet>';
                $("#tblGRN tbody").children().each(function () {
                    obj = $(this);

                    if (parseFloat(obj.find('td input[id*=txtlQty]').val()) > 0 || parseFloat(obj.find('td input[id*=txtInvQty]').val()) > 0
                        || parseFloat(obj.find('td input[id*=txtFoc]').val()) > 0) {

                        xml += "<Table>";
                        xml += "<RowNo>" + obj.find('td input[id*=txtSNo]').val().trim() + "</RowNo>";
                        xml += "<PONo>" + obj.find('td input[id*=txtPONo]').val().trim() + "</PONo>";
                        xml += "<ItemCode>" + obj.find('td input[id*=txtItemcode]').val().trim() + "</ItemCode>";
                        xml += "<ItemDescription>" + obj.find('td input[id*=txtItemDesc]').val().trim() + "</ItemDescription>";
                        xml += "<POQty>" + obj.find('td input[id*=txtPOQty]').val().trim() + "</POQty>";
                        xml += "<InvQty>" + obj.find('td input[id*=txtInvQty]').val().trim() + "</InvQty>";
                        xml += "<Qty>" + obj.find('td input[id*=txtlQty]').val().trim() + "</Qty>";
                        xml += "<Foc>" + obj.find('td input[id*=txtFoc]').val().trim() + "</Foc>";
                        xml += "<MRP>" + obj.find('td input[id*=txtMRP]').val().trim() + "</MRP>";
                        xml += "<BasicCost>" + obj.find('td input[id*=txtBasicCost]').val().trim() + "</BasicCost>";
                        xml += "<SellingPrice>" + obj.find('td input[id*=txtSellingPrice]').val().trim() + "</SellingPrice>";
                        xml += "<Wprc1>" + obj.find('td input[id*=txtWPrice1]').val().trim() + "</Wprc1>";
                        xml += "<Wprc2>" + obj.find('td input[id*=txtWPrice2]').val().trim() + "</Wprc2>";
                        xml += "<Wprc3>" + obj.find('td input[id*=txtWPrice3]').val().trim() + "</Wprc3>";
                        xml += "<GrossBasicAmt>" + obj.find('td input[id*=txtGrossBasicAmt]').val().trim() + "</GrossBasicAmt>";
                        xml += "<Discper>" + obj.find('td input[id*=txtDiscPer]').val().trim() + "</Discper>";
                        xml += "<DiscAmt>" + obj.find('td input[id*=txtDiscAmt]').val().trim() + "</DiscAmt>";
                        xml += "<GrossDiscAmt>" + obj.find('td input[id*=txtGrossDiscAmt]').val().trim() + "</GrossDiscAmt>";
                        xml += "<SDper>" + obj.find('td input[id*=txtSDper]').val().trim() + "</SDper>";
                        xml += "<SDAmt>" + obj.find('td input[id*=txtSDAmt]').val().trim() + "</SDAmt>";
                        xml += "<GrossShDiscAmt>" + obj.find('td input[id*=txtGrossShDiscAmt]').val().trim() + "</GrossShDiscAmt>";
                        xml += "<GrossCost>" + obj.find('td input[id*=txtGrossCost]').val().trim() + "</GrossCost>";
                        xml += "<TotalGrossCost>" + obj.find('td input[id*=txtTotalGrossCost]').val().trim() + "</TotalGrossCost>";
                        xml += "<ITaxperc1>" + obj.find('td input[id*=txtITaxperc1]').val().trim() + "</ITaxperc1>";
                        xml += "<ITaxperc2>" + obj.find('td input[id*=txtITaxperc2]').val().trim() + "</ITaxperc2>";
                        xml += "<ITaxperc4>" + obj.find('td input[id*=txtCessper]').val().trim() + "</ITaxperc4>";
                        xml += "<ITaxAmt1>" + obj.find('td input[id*=txtITaxAmt1]').val().trim() + "</ITaxAmt1>";
                        xml += "<ITaxAmt2>" + obj.find('td input[id*=txtITaxAmt2]').val().trim() + "</ITaxAmt2>";
                        xml += "<ITaxAmt4>" + obj.find('td input[id*=txtCessAmt]').val().trim() + "</ITaxAmt4>";
                        xml += "<ITaxAmt5>" + obj.find('td input[id*=txtAddCessAmt]').val().trim() + "</ITaxAmt5>";
                        xml += "<GrossVATAmt>" + obj.find('td input[id*=txtGrossVATAmt]').val().trim() + "</GrossVATAmt>";
                        xml += "<GrossCessAmt>" + obj.find('td input[id*=txtGrocessCessAmt]').val().trim() + "</GrossCessAmt>";
                        xml += "<GRAddCessAmt>" + obj.find('td input[id*=txtGRAddCessAmt]').val().trim() + "</GRAddCessAmt>";
                        xml += "<NetCost>" + obj.find('td input[id*=txtNetCost]').val().trim() + "</NetCost>";
                        xml += "<TotalValue>" + obj.find('td input[id*=txtTotalValue]').val().trim() + "</TotalValue>";
                        xml += "<FTotalValue>" + obj.find('td input[id*=txtFTotalValue]').val().trim() + "</FTotalValue>";
                        xml += "<EarnedMarPerc>" + obj.find('td input[id*=txtEarnedMarPerc]').val().trim() + "</EarnedMarPerc>";
                        xml += "<EarMarAmt>" + obj.find('td input[id*=txtEarMarAmt]').val().trim() + "</EarMarAmt>";
                        xml += "<Marginfixed>" + obj.find('td input[id*=txtMarginfixed]').val().trim() + "</Marginfixed>";
                        xml += "<PrintQty>" + obj.find('td input[id*=txtPrintQty]').val().trim() + "</PrintQty>";
                        xml += "<ITaxt1>" + obj.find('td input[id*=txtITaxt1]').val().trim() + "</ITaxt1>";
                        xml += "<ITaxt2>" + obj.find('td input[id*=txtITax2]').val().trim() + "</ITaxt2>";
                        xml += "<MedBatch>" + obj.find('td input[id*=txtMedBatch]').val().trim() + "</MedBatch>";
                        xml += "<ExpDate>" + obj.find('td input[id*=txtExpDate]').val().trim() + "</ExpDate>";
                        xml += "<OldEarnedMarg>" + obj.find('td input[id*=txtOldEarnedMarg]').val().trim() + "</OldEarnedMarg>";
                        xml += "<ProfitMargin>" + obj.find('td input[id*=txtProfitMargin]').val().trim().replace('NaN', '0') + "</ProfitMargin>";
                        xml += "<FOCItem>" + obj.find('td input[id*=txtFOCItem]').val().trim() + "</FOCItem>";
                        xml += "<FOCBatch>" + obj.find('td input[id*=txtFOCBatch]').val().trim() + "</FOCBatch>";
                        xml += "<PurchaseCharges>" + obj.find('td input[id*=txtPurchaseCharges]').val().trim() + "</PurchaseCharges>";
                        xml += "<FreightCharges>" + obj.find('td input[id*=txtFreightCharges]').val().trim() + "</FreightCharges>";
                        xml += "<VendorMargin>" + obj.find('td input[id*=txtVendorMargin]').val().trim() + "</VendorMargin>";
                        xml += "<TotalCharges>" + obj.find('td input[id*=txtTotalCharges]').val().trim() + "</TotalCharges>";
                        xml += "<LandingCost>" + obj.find('td input[id*=txtLandingCost]').val().trim() + "</LandingCost>";
                        xml += "<PurchaseProfitper>" + 0 + "</PurchaseProfitper>";
                        xml += "<NetCostAffect>" + obj.find('td span[id*=lblNetCostAffect]').text().trim() + "</NetCostAffect>";
                        xml += "<Batch>" + obj.find('td span[id*=lblBatch]').text().trim() + "</Batch>";
                        xml += "<ItemType>" + obj.find('td span[id*=lblItemType]').text().trim() + "</ItemType>";
                        xml += "<PricingType>" + obj.find('td span[id*=lblPricingType]').text().trim() + "</PricingType>";
                        xml += "<MarkDownPerc>" + obj.find('td span[id*=lblMarkDownPerc]').text() + "</MarkDownPerc>";
                        xml += "<oldSellingPrice>" + obj.find('td span[id*=lblOldSellingPrice]').text().trim() + "</oldSellingPrice>";
                        xml += "<AvgCost>" + obj.find('td span[id*=lblAvgCost]').text().trim() + "</AvgCost>";
                        xml += "<BasicSelling>" + obj.find('td input[id*=hidBasicSelling]').val().trim() + "</BasicSelling>";
                        xml += "<ItemBatchNo>" + obj.find('td input[id*=hidItemBatchNo]').val().trim() + "</ItemBatchNo>";
                        xml += "<AllowExpireDate>" + obj.find('td span[id*=lblAllowExpireDate]').text().trim() + "</AllowExpireDate>";
                        xml += "<FTCFromDate>" + obj.find('td span[id*=lblFTCFromdate]').text().trim() + "</FTCFromDate>";
                        xml += "<FTCToDate>" + obj.find('td span[id*=lblFTCToDate]').text().trim() + "</FTCToDate>";
                        xml += "<FTCBuyQty>" + obj.find('td span[id*=lblFTCBuyQty]').text().trim() + "</FTCBuyQty>";
                        xml += "<FTCFreeQty>" + obj.find('td span[id*=lblFTCFreeQty]').text().trim() + "</FTCFreeQty>";
                        xml += "<GridRowStatus>" + obj.find('td span[id*=lblGridRowStatus]').text().trim() + "</GridRowStatus>";
                        xml += "<FTCItemcode>" + obj.find('td span[id*=lblFTCItemcode]').text().trim() + "</FTCItemcode>";
                        xml += "<FTCBatchNo>" + obj.find('td span[id*=lblFTCBatchNo]').text().trim() + "</FTCBatchNo>";
                        xml += "<FTCInputQty>" + obj.find('td span[id*="lblFTCInputQty"]').text().trim() + "</FTCInputQty>";
                        xml += "<WQty>" + obj.find('td span[id*="lblWQty"]').text().trim() + "</WQty>"; 
                        xml += "<WUOM>" + obj.find('td span[id*="lblWUOM"]').text().trim() + "</WUOM>";
                        xml += "<StyleCode>" + obj.find('td input[id*=txtStyleCode]').val().trim() + "</StyleCode>";
                        xml += "<SerialNo>" + obj.find('td input[id*=txtSerialNo]').val().trim() + "</SerialNo>";
                        xml += "</Table>";
                    }
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
                $('#<%=GRNXmldata.ClientID %>').val(xml);
                return xml;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetMultiUOMForSave() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            var json;
            try {

                $.each($("#tblMultipleUOMSave thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblMultipleUOMSave tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                json = JSON.stringify(tableObj);
                $('#<%=hidMultiUOMSave.ClientID %>').val(json);
                return json;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncLoadDiscountinueHoldClosed() {
            var allObj, obj;
            var obj;
            try {
                allObj = jQuery.parseJSON($('#<%=hidGRNJSONData.ClientID %>').val());
                obj = allObj.Table;
                if (allObj.Table1 != null && allObj.Table1.length > 0) {
                    fncLoadDiscontinueMultipleUOM(allObj.Table1);
                }
                
                for (var i = 0; i < obj.length; i++) {
                    poNo = obj[i]["PONo"];
                    $('#<%=hdfPoNo.ClientID %>').val(poNo);
                    fncAddNewRowsToGrid(obj[i]["RowNo"], obj[i]["ItemCode"], obj[i]["ItemDescription"],
                        obj[i]["POQty"], obj[i]["InvQty"], obj[i]["Qty"], obj[i]["Foc"], obj[i]["MRP"], obj[i]["BasicCost"],
                        obj[i]["SellingPrice"], obj[i]["Wprc1"], obj[i]["Wprc2"], obj[i]["Wprc3"], obj[i]["GrossBasicAmt"], obj[i]["Discper"],
                        obj[i]["DiscAmt"], obj[i]["GrossDiscAmt"], obj[i]["SDper"], obj[i]["SDAmt"], obj[i]["GrossShDiscAmt"],
                obj[i]["GrossCost"], obj[i]["TotalGrossCost"], obj[i]["ITaxperc1"], obj[i]["ITaxperc2"], obj[i]["ITaxperc4"],
                obj[i]["ITaxAmt1"], obj[i]["ITaxAmt2"], obj[i]["ITaxAmt4"], obj[i]["ITaxAmt5"], obj[i]["GrossVATAmt"],
                obj[i]["GrossCessAmt"], obj[i]["GRAddCessAmt"], obj[i]["NetCost"], obj[i]["TotalValue"], obj[i]["FTotalValue"],
                obj[i]["EarnedMarPerc"], obj[i]["EarMarAmt"], obj[i]["Marginfixed"], obj[i]["PrintQty"], obj[i]["ITaxt1"],
                obj[i]["ITaxt2"], obj[i]["MedBatch"], obj[i]["ExpDate"], obj[i]["OldEarnedMarg"], obj[i]["ProfitMargin"], obj[i]["FOCItem"],
                obj[i]["FOCBatch"], obj[i]["PurchaseCharges"], obj[i]["FreightCharges"], obj[i]["VendorMargin"], obj[i]["TotalCharges"],
                obj[i]["LandingCost"], obj[i]["PurchaseProfitper"], obj[i]["NetCostAffect"], obj[i]["Batch"], obj[i]["ItemType"], obj[i]["PricingType"],
                fncConvertFloatJS(obj[i]["MarkDownPerc"]), obj[i]["oldSellingPrice"], obj[i]["AvgCost"], obj[i]["BasicSelling"], obj[i]["ItemBatchNo"],
                obj[i]["AllowExpireDate"], obj[i]["FTCFromDate"], obj[i]["FTCToDate"], obj[i]["FTCBuyQty"], obj[i]["FTCFreeQty"],
                obj[i]["GridRowStatus"], obj[i]["PrvData"], obj[i]["FTCItemcode"], obj[i]["FTCBatchNo"], obj[i]["FTCInputQty"],
                obj[i]["WQty"], obj[i]["WUomCode"], obj[i]["StyleCode"], obj[i]["SerialNo"]);
                }

                fncNetValueCalculation();
                fncInitizeVatDetailBody();
                fncAddVetDetailForPoandHold();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// fnc Save PO Entry
        function fncSaveGRNEntryTemporarly() {
            var obj = {};
            try {

                obj.xmlData = fncGetGridValuesForSave();
                obj.gaNo = $('#<%=txtgaNo.ClientID %>').val().trim();
            obj.MultiUOMXML = fncGetMultiUOMForSave();

            $.ajax({
                type: "POST",
                url: "GoodsAcknowledgmentNote.aspx/fncSaveGRNEntryTemporarily",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    return false;
                },
                error: function (data) {
                    fncToastError(data.message);
                    return false;
                }
            });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncUpdateRowNow() {
        var rowNo = 0;
        try {
            $("#tblGRN tbody").children().each(function () {
                rowNo = parseInt(rowNo) + 1;
                $(this).find('td input[id*="txtSNo"]').val(rowNo);

            });
            fncNetValueCalculation();
            fncAddVetDetailForPoandHold();
            fncSaveGRNEntryTemporarly();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }


    function fncAddItemsDirectly(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        try {

            if (charCode == 13) {
                if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y" && $("select[id$=ddlMultipleUOM] > option").length > 0) {
                    glmrp = $('#<%=txtMRP.ClientID %>').val();
                    glsprice = $('#<%=txtMRP.ClientID %>').val();
                    glInvCode = $('#<%=txtItemCode.ClientID %>').val();
                    glNetCost = $('#<%=txtNetCost.ClientID %>').val();
                    glBatchNo = fncCreateBatchNo($('#<%=txtMRP.ClientID %>').val());


                    fncShowMultipUOM($('#<%=txtItemCode.ClientID %>').val());

                }
                else {
                    fncSellingPriceFocusOut();
                    fncAddValidation();
                }

                return false;
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncBindMultipleUOM(uomObj) {
        var uomObj, row, rowNo = 0;
        var mulUOMBody;
        try {

            mulUOMBody = $("#tblMultipleUOM tbody");
            mulUOMBody.children().remove();
            if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                $("select[id$=ddlMultipleUOM] > option").remove();

                for (var i = 0; i < uomObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr>"
                        + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                        + "<td id='tdUOMCode_" + rowNo + "' >" + uomObj[i]["UOM"] + "</td>"
                                           + "<td id='tdMRP_" + rowNo + "' >0</td>"
                                           + "<td id='tdNetCost_" + rowNo + "' >0</td>"
                                           + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                                           + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                                           //+ "<td id='tdSPrice_" + rowNo + "' >0</td>"
                                           + "<td id='tdWholeSalePricePer1_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                                           + "<td id='tdWPrice1_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                                           //+ "<td id='tdWholeSalePricePer2_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");'Enabled='false'; Class='form-control-res-right[readonly]' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                                           //   + "<td id='tdWPrice2_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='aspNetDisabled form-control-res' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                                           //   + "<td id='tdWholeSalePricePer3_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='aspNetDisabled form-control-res' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                                           //      + "<td id='tdWPrice3_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='aspNetDisabled form-control-res' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                                           //+ "<td id='tdWPrice_" + rowNo + "' >0</td>"
                                           + "<td id='tdUOMconv_" + rowNo + "' >" + uomObj[i]["ConvQty"] + "</td>"
                                           + "</tr>";
                    mulUOMBody.append(row);
                    $('#<%=ddlMultipleUOM.ClientID%>').append($("<option></option>").val(uomObj[i]["ConvQty"]).html(uomObj[i]["UOM"]));
                    $('#<%=ddlMultipleUOM.ClientID %>').trigger("liszt:updated");

                    $("#txtSellingPer_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                    //$("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                    //$("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                    $("#txtSPrice_" + rowNo + "").number(true, 2);
                    $("#txtWPrice1_" + rowNo + "").number(true, 2);
                    //$("#txtWPrice2_" + rowNo + "").number(true, 2);
                    //$("#txtWPrice3_" + rowNo + "").number(true, 2);
                }
            }
            else {


                $("select[id$=ddlMultipleUOM] > option").remove();

                for (var i = 0; i < uomObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr>"
                        + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                        + "<td id='tdUOMCode_" + rowNo + "' >" + uomObj[i]["UOM"] + "</td>"
                                           + "<td id='tdMRP_" + rowNo + "' >0</td>"
                                           + "<td id='tdNetCost_" + rowNo + "' >0</td>"
                                           + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                                           + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                                           //+ "<td id='tdSPrice_" + rowNo + "' >0</td>"
                                           + "<td id='tdWholeSalePricePer1_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                                           + "<td id='tdWPrice1_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice1\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                                           + "<td id='tdWholeSalePricePer2_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                                              + "<td id='tdWholeSalePricePer3_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                                                 + "<td id='tdWPrice3_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='0' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                                           //+ "<td id='tdWPrice_" + rowNo + "' >0</td>"
                                           + "<td id='tdUOMconv_" + rowNo + "' >" + uomObj[i]["ConvQty"] + "</td>"
                                           + "</tr>";
                    mulUOMBody.append(row);
                    $('#<%=ddlMultipleUOM.ClientID%>').append($("<option></option>").val(uomObj[i]["ConvQty"]).html(uomObj[i]["UOM"]));
                    $('#<%=ddlMultipleUOM.ClientID %>').trigger("liszt:updated");

                    $("#txtSellingPer_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                    $("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                    $("#txtSPrice_" + rowNo + "").number(true, 2);
                    $("#txtWPrice1_" + rowNo + "").number(true, 2);
                    $("#txtWPrice2_" + rowNo + "").number(true, 2);
                    $("#txtWPrice3_" + rowNo + "").number(true, 2);
                }
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncShowMultipUOM(itemcode) {
        try {

            if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
                fncArriveMultipleUOMSPAndWP()
                $("#divMultipleUOM").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true,
                    title: itemcode
                });
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    var glmrp = 0, glsprice = 0, glNetCost = 0;
    function fncArriveMultipleUOMSPAndWP() {
        var convQty = 0, obj;
        var wholeMrp = 0, WholeSPrice = 0, wholeNetCost = 0;
        var sellingprice = 0, per = 0;

        try {
           <%-- glmrp = $('#<%=txtMRP.ClientID %>').val();
            glsprice = $('#<%=txtMRP.ClientID %>').val();--%>

            $("#tblMultipleUOM tbody").children().each(function () {
                obj = $(this);

                convQty = obj.find('td[id*="tdUOMconv"]').text();
                wholeMrp = convQty * glmrp;
                WholeSPrice = convQty * glsprice;
                wholeNetCost = convQty * glNetCost;



                obj.find('td[id*="tdMRP"]').text(wholeMrp.toFixed(2));
                obj.find('td input[id*="txtSPrice"]').val(WholeSPrice.toFixed(2));
                obj.find('td input[id*="txtWPrice"]').val(WholeSPrice.toFixed(2));
                obj.find('td[id*="tdNetCost"]').text(wholeNetCost.toFixed(2));

                per = obj.find('td input[id*="txtSellingPer"]').val();
                sellingprice = obj.find('td[id*="tdMRP"]').text() - (obj.find('td[id*="tdMRP"]').text() * per / 100);
                obj.find('td input[id*="txtSPrice"]').val(sellingprice.toFixed(2));

                per = obj.find('td input[id*="txtWholeSalePricePer1"]').val();
                sellingprice = obj.find('td input[id*="txtSPrice"]').val() - (obj.find('td input[id*="txtSPrice"]').val() * per / 100);
                obj.find('td input[id*="txtWPrice1"]').val(sellingprice.toFixed(2));
                per = obj.find('td input[id*="txtWholeSalePricePer2"]').val();
                sellingprice = obj.find('td input[id*="txtWPrice1"]').val() - (obj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                obj.find('td input[id*="txtWPrice2"]').val(sellingprice.toFixed(2));
                sellingprice = obj.find('td input[id*="txtWPrice2"]').val() - (obj.find('td input[id*="txtWPrice2"]').val() * per / 100);
                obj.find('td input[id*="txtWPrice3"]').val(sellingprice.toFixed(2));

                //obj.find('td [id*="txtSellingPer"]').text(WholeSPrice.toFixed(2));
                //obj.find('td[id*="txtWPricePer"]').text(WholeSPrice.toFixed(2));
            });

            $("#txtSellingPer_1").select();
            return true;
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    var uomStatus = true;
    function fncArriveSellingpriceAndWpriceForMulUOM(sorce, value) {
        var per = 0, sellingprice = 0, mrp = 0;
        var rowObj, discAmt = 0, discPer = 0, netCost = 0;
        var wprice1 = 0, wprice2 = 0, wprice3 = 0;

        try {

            rowObj = $(sorce).parent().parent();
            netCost = rowObj.find('td[id*="tdNetCost"]').text();
            if (value == "SPricePer") {
                per = rowObj.find('td input[id*="txtSellingPer"]').val();
                sellingprice = rowObj.find('td[id*="tdMRP"]').text() - (rowObj.find('td[id*="tdMRP"]').text() * per / 100);
                rowObj.find('td input[id*="txtSPrice"]').val(sellingprice.toFixed(2));

                /// WPrice Calculation
                fncArriveSellingpriceAndWpriceForMulUOM(sorce, "WPricePer");
            }
            else if (value == "WPricePer") {
                per = rowObj.find('td input[id*="txtWholeSalePricePer"]').val();
                sellingprice = rowObj.find('td input[id*="txtSPrice"]').val() - (rowObj.find('td input[id*="txtSPrice"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice1"]').val(sellingprice.toFixed(2));
                //wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                //rowObj.find('td input[id*="txtWPrice1"]').val(wprice1.toFixed(2));
                wprice2 = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice2"]').val(wprice2.toFixed(2));
                wprice3 = rowObj.find('td input[id*="txtWPrice2"]').val() - (rowObj.find('td input[id*="txtWPrice2"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice3"]').val(wprice3.toFixed(2));
            }
            else if (value == "WPricePer1") {
                per = rowObj.find('td input[id*="txtWholeSalePricePer1"]').val();
                sellingprice = rowObj.find('td input[id*="txtSPrice"]').val() - (rowObj.find('td input[id*="txtSPrice"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice1"]').val(sellingprice.toFixed(2));
                //wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                //rowObj.find('td input[id*="txtWPrice1"]').val(wprice1.toFixed(2));
                wprice2 = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice2"]').val(wprice2.toFixed(2));
                wprice3 = rowObj.find('td input[id*="txtWPrice2"]').val() - (rowObj.find('td input[id*="txtWPrice2"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice3"]').val(wprice3.toFixed(2));
            }
            else if (value == "WPricePer2") {
                per = rowObj.find('td input[id*="txtWholeSalePricePer2"]').val();

                wprice2 = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice2"]').val(wprice2.toFixed(2));

            }
            else if (value == "WPricePer3") {
                per = rowObj.find('td input[id*="txtWholeSalePricePer3"]').val();

                wprice3 = rowObj.find('td input[id*="txtWPrice2"]').val() - (rowObj.find('td input[id*="txtWPrice2"]').val() * per / 100);
                rowObj.find('td input[id*="txtWPrice3"]').val(wprice3.toFixed(2));
            }
            else if (value == "SPrice") {
                mrp = rowObj.find('td[id*="tdMRP"]').text();
                sellingprice = rowObj.find('td input[id*="txtSPrice"]').val();

                discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                discPer = discAmt * 100 / mrp;
                rowObj.find('td input[id*="txtSellingPer"]').val(discPer.toFixed(4));

                /// Wprice Calculation
                fncArriveSellingpriceAndWpriceForMulUOM(sorce, "WPricePer");
            }
            else if (value == "WPrice1") {
                mrp = rowObj.find('td input[id*="txtSPrice"]').val();
                sellingprice = rowObj.find('td input[id*="txtWPrice1"]').val();
                //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                discPer = discAmt * 100 / mrp;
                rowObj.find('td input[id*="txtWholeSalePricePer1"]').val(discPer.toFixed(4));
            }
            else if (value == "WPrice2") {
                mrp = rowObj.find('td input[id*="txtWPrice1"]').val();
                sellingprice = rowObj.find('td input[id*="txtWPrice2"]').val();
                //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                discPer = discAmt * 100 / mrp;
                rowObj.find('td input[id*="txtWholeSalePricePer2"]').val(discPer.toFixed(4));
            }
            else if (value == "WPrice3") {
                mrp = rowObj.find('td input[id*="txtWPrice2"]').val();
                sellingprice = rowObj.find('td input[id*="txtWPrice3"]').val();
                //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                discPer = discAmt * 100 / mrp;
                rowObj.find('td input[id*="txtWholeSalePricePer3"]').val(discPer.toFixed(4));
            }
            //// Validation Part////
            sellingprice = rowObj.find('td input[id*="txtSPrice"]').val();
            wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val();
            mrp = rowObj.find('td[id*="tdMRP"]').text();
            wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val();
            wprice2 = rowObj.find('td input[id*="txtWPrice2"]').val();
            wprice3 = rowObj.find('td input[id*="txtWPrice3"]').val();
            if (parseFloat(sellingprice) > parseFloat(mrp)) {
                fncToastInformation("Selling Price must less then Or equal MRP. ");
                rowObj.find('td input[id*="txtSPrice"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(sellingprice) < parseFloat(netCost)) {
                fncToastInformation("Selling Price must greater then  Netcost. ");
                rowObj.find('td input[id*="txtSPrice"]').select();
                uomStatus = false;
                return;
            }
                //else if ( parseFloat(wprice) > parseFloat(sellingprice)) {
                //    fncToastInformation("WPrice must less then Or equal Selling Price. ");
                //    rowObj.find('td input[id*="txtWPrice"]').select();
                //    uomStatus = false;
                //    return;
                //}
            else if (parseFloat(wprice1) < parseFloat(netCost)) {
                fncToastInformation("WPrice1 must greater then Netcost. ");
                rowObj.find('td input[id*="txtWPrice1"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice1) > parseFloat(sellingprice)) {
                fncToastInformation("WPrice1 must less then Or equal Selling Price. ");
                rowObj.find('td input[id*="txtWPrice1"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice2) > parseFloat(wprice1)) {
                fncToastInformation("WPrice2 must less then Or equal wprice1. ");
                rowObj.find('td input[id*="txtWPrice2"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice3) > parseFloat(wprice1)) {
                fncToastInformation("WPrice3 must less then Or equal wprice1. ");
                rowObj.find('td input[id*="txtWPrice3"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice3) > parseFloat(wprice2)) {
                fncToastInformation("WPrice3 must less then Or equal wprice2. ");
                rowObj.find('td input[id*="txtWPrice3"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice2) > parseFloat(sellingprice)) {
                fncToastInformation("WPrice2 must less then Or equal Selling Price. ");
                rowObj.find('td input[id*="txtWPrice2"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice3) > parseFloat(sellingprice)) {
                fncToastInformation("WPrice3 must less then Or equal Selling Price. ");
                rowObj.find('td input[id*="txtWPrice3"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice1) < parseFloat(netCost)) {
                fncToastInformation("WPrice1 must greater then Netcost. ");
                rowObj.find('td input[id*="txtWPrice1"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice2) < parseFloat(netCost)) {
                fncToastInformation("WPrice2 must greater then Netcost. ");
                rowObj.find('td input[id*="txtWPrice2"]').select();
                uomStatus = false;
                return;
            }
            else if (parseFloat(wprice3) < parseFloat(netCost)) {
                fncToastInformation("WPrice3 must greater then Netcost. ");
                rowObj.find('td input[id*="txtWPrice3"]').select();
                uomStatus = false;
                return;
            }


            uomStatus = true;



        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    var glInvCode = "", glBatchNo = "";
    function fncMultiUOMtoXmlformat() {
        var tblUOMBodySave, row, rowNo = 0;
        try {

            if (uomStatus == false) {
                fncToastInformation("Please Check Entered Values.");
                return;
            }

            <%--glInvCode = $('#<%=txtItemCode.ClientID %>').val();
                glBatchNo = fncCreateBatchNo($('#<%=txtMRP.ClientID %>').val());--%>

            tblUOMBodySave = $("#tblMultipleUOMSave tbody");
            // Delete Exisiting UOM
            tblUOMBodySave.children().each(function () {
                if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode && $(this).find('td[id*="tdBatachNo"]').text() == glBatchNo) {
                    $(this).remove();
                }
            });
            if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                $("#tblMultipleUOM tbody").children().each(function () {
                    rowNo = parseInt(rowNo) + 1;
                    var obj = $(this);

                    row = "<tr>"
                           + "<td id='tdInventorycode_" + rowNo + "' >" + glInvCode + "</td>"
                           + "<td id='tdBatachNo_" + rowNo + "' >" + glBatchNo + "</td>"
                                              + "<td id='tdUOMcode_" + rowNo + "' >" + obj.find('td[id*="tdUOMCode"]').text().trim() + "</td>"
                                              + "<td id='tdMRP_" + rowNo + "' >" + obj.find('td[id*="tdMRP"]').text().trim() + "</td>"
                                              + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj.find('td input[id*="txtSellingPer"]').val().trim() + "</td>"
                                              + "<td id='tdSPrice_" + rowNo + "' >" + obj.find('td input[id*="txtSPrice"]').val().trim() + "</td>"
                                              + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice1_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                              + "<td id='tdUOMconv_" + rowNo + "' >" + obj.find('td[id*="tdUOMconv"]').text().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice3_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                              + "</tr>";
                    tblUOMBodySave.append(row);

                });

            }
            else {
                $("#tblMultipleUOM tbody").children().each(function () {
                    rowNo = parseInt(rowNo) + 1;
                    var obj = $(this);

                    row = "<tr>"
                           + "<td id='tdInventorycode_" + rowNo + "' >" + glInvCode + "</td>"
                           + "<td id='tdBatachNo_" + rowNo + "' >" + glBatchNo + "</td>"
                                              + "<td id='tdUOMcode_" + rowNo + "' >" + obj.find('td[id*="tdUOMCode"]').text().trim() + "</td>"
                                              + "<td id='tdMRP_" + rowNo + "' >" + obj.find('td[id*="tdMRP"]').text().trim() + "</td>"
                                              + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj.find('td input[id*="txtSellingPer"]').val().trim() + "</td>"
                                              + "<td id='tdSPrice_" + rowNo + "' >" + obj.find('td input[id*="txtSPrice"]').val().trim() + "</td>"
                                              + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice1_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                              + "<td id='tdUOMconv_" + rowNo + "' >" + obj.find('td[id*="tdUOMconv"]').text().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer2"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice2"]').val().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer3"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice3_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice3"]').val().trim() + "</td>"
                                              + "</tr>";
                    tblUOMBodySave.append(row);

                });
            }

            $("#divMultipleUOM").dialog("destroy");
            if ($('#<%=txtItemCode.ClientID %>').val() != "") {
                fncAddValidation();
            }
            else {
                $('#<%=txtItemCode.ClientID %>').select();
            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncLoadDiscontinueMultipleUOM(obj) {
        var row, tblUOMBodySave, rowNo = 0;
        try {

            tblUOMBodySave = $("#tblMultipleUOMSave tbody");
            tblUOMBodySave.children().remove();

            for (var i = 0; i < obj.length; i++) {
                rowNo = parseInt(rowNo) + 1;
                if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                    row = "<tr>"
                          + "<td id='tdInventorycode_" + rowNo + "' >" + obj[i]["Inventorycode"] + "</td>"
                          + "<td id='tdBatachNo_" + rowNo + "' >" + obj[i]["BatachNo"] + "</td>"
                                             + "<td id='tdUOMcode_" + rowNo + "' >" + obj[i]["UOMcode"] + "</td>"
                                             + "<td id='tdMRP_" + rowNo + "' >" + obj[i]["MRP"] + "</td>"
                                             + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj[i]["SPricePer"] + "</td>"
                                             + "<td id='tdSPrice_" + rowNo + "' >" + obj[i]["Sellingprice"] + "</td>"
                                             + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj[i]["WPricePer1"] + "</td>"
                                             + "<td id='tdWPrice1_" + rowNo + "' >" + obj[i]["WPrice1"] + "</td>"
                                             + "<td id='tdUOMconv_" + rowNo + "' >" + obj[i]["UOMConv"] + "</td>"
                                             + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj[i]["WPricePer1"] + "</td>"
                                             + "<td id='tdWPrice2_" + rowNo + "' >" + obj[i]["WPrice1"] + "</td>"
                                             + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj[i]["WPricePer1"] + "</td>"
                                             + "<td id='tdWPrice3_" + rowNo + "' >" + obj[i]["WPrice1"] + "</td>"

                                             + "</tr>";
                }
                else {
                    row = "<tr>"
                           + "<td id='tdInventorycode_" + rowNo + "' >" + obj[i]["Inventorycode"] + "</td>"
                           + "<td id='tdBatachNo_" + rowNo + "' >" + obj[i]["BatachNo"] + "</td>"
                                              + "<td id='tdUOMcode_" + rowNo + "' >" + obj[i]["UOMcode"] + "</td>"
                                              + "<td id='tdMRP_" + rowNo + "' >" + obj[i]["MRP"] + "</td>"
                                              + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj[i]["SPricePer"] + "</td>"
                                              + "<td id='tdSPrice_" + rowNo + "' >" + obj[i]["Sellingprice"] + "</td>"
                                              + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj[i]["WPricePer1"] + "</td>"
                                              + "<td id='tdWPrice1_" + rowNo + "' >" + obj[i]["WPrice1"] + "</td>"
                                              + "<td id='tdUOMconv_" + rowNo + "' >" + obj[i]["UOMConv"] + "</td>"
                                              + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj[i]["WPricePer2"] + "</td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' >" + obj[i]["WPrice2"] + "</td>"
                                              + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj[i]["WPricePer3"] + "</td>"
                                              + "<td id='tdWPrice3_" + rowNo + "' >" + obj[i]["WPrice3"] + "</td>"

                                              + "</tr>";
                }
                tblUOMBodySave.append(row);
            }


        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    $(function () {
        $("#ContentPlaceHolder1_ddlMultipleUOM_chzn").bind('keyup', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13) {
                $('#<%=txtWQty.ClientID %>').select();
            }
        });
        $("#ContentPlaceHolder1_ddlGST_chzn").bind('keyup', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13) {
                $('#<%=txtSPrice.ClientID %>').select();
            }
        });
        $("#ContentPlaceHolder1_ddlfocBatch_chzn").bind('keyup', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13) {
                $('#<%=txtFocTotalQty.ClientID %>').select();
            }
        });
    });

    function fncMultiUOMTraversal(source, evt, value) {
        var obj;
        try {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            obj = $(source).parent().parent();
            if (charCode == 13) {
                if (value == "SellingPer") {
                    obj.find('td input[id*="txtSPrice"]').select();
                }
                else if (value == "WPricePer1") {
                    obj.find('td input[id*="txtWPrice1"]').select();
                }
                else if (value == "WPrice1") {
                    obj.find('td input[id*="txtWholeSalePricePer2"]').select();
                }
                else if (value == "WPricePer2") {
                    obj.find('td input[id*="txtWPrice2"]').select();
                }
                else if (value == "WPrice2") {
                    obj.find('td input[id*="txtWholeSalePricePer3"]').select();
                }

                else if (value == "txtWholeSalePricePer1") {
                    if (obj.next().length > 0) {
                        obj.next().find('td input[id*="txtWholeSalePricePer2"]').select();
                    }
                    else {
                        $('#<%=lnkOk.ClientID %>').focus();
                    }
                }
                else if (value == "Selling") {
                    obj.find('td input[id*="txtWholeSalePricePer1"]').select();
                }
                else if (value == "WPrice1") {
                    if (obj.next().length > 0) {
                        obj.next().find('td input[id*="WPrice2"]').select();
                    }

                }
                else if (value == "WPrice2") {
                    if (obj.next().length > 0) {
                        obj.next().find('td input[id*="WPrice3"]').select();
                    }
                    else {
                        $('#<%=lnkOk.ClientID %>').focus();
                    }
                }
                else if (value == "WPrice3") {
                    if (obj.next().length > 0) {
                        obj.next().find('td input[id*="txtSellingPer"]').select();
                    }
                    else {
                        $('#<%=lnkOk.ClientID %>').focus();
                    }
                }
                else if (value == "WPricePer3") {
                    obj.find('td input[id*="txtWPrice3"]').select();

                }

}

}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncGSTChange() {
    var taxPer = 0;
    try {
        //alert('fncGSTChange');
        <%-- taxPer = parseFloat($('#<%=ddlGST.ClientID%>').val()) / 2;
        $('#<%=txtITax1.ClientID%>').val(taxPer);
        $('#<%=txtITax2.ClientID%>').val(taxPer);
        $('#<%=txtITaxper1.ClientID%>').val(taxPer);
        $('#<%=txtITaxper2.ClientID%>').val(taxPer);
        fncInvQtyFocusOut();--%>
        if ($('#<%=hidVendorStatus.ClientID %>').val() == "1") {
            taxPer = parseFloat($('#<%=ddlGST.ClientID%>').val()) / 2;
            $('#<%=txtITax1.ClientID%>').val(taxPer);
            $('#<%=txtITax2.ClientID%>').val(taxPer);
            $('#<%=txtITaxper1.ClientID%>').val(taxPer);
            $('#<%=txtITaxper2.ClientID%>').val(taxPer);
        }
        else if ($('#<%=hidVendorStatus.ClientID %>').val() == "4") {
            $('#<%=txtITax1.ClientID%>').val("NoTax");
            $('#<%=txtITax2.ClientID%>').val("NoTax");

            $('#<%=txtITaxper1.ClientID%>').val("0.00");
            $('#<%=txtITaxper2.ClientID%>').val("0.00");
        }
        else {
            $('#<%=txtITax1.ClientID%>').val(parseFloat($('#<%=ddlGST.ClientID%>').val()));
            $('#<%=txtITaxper1.ClientID%>').val(parseFloat($('#<%=ddlGST.ClientID%>').val()));
        }

    fncInvQtyFocusOut();
        //Vijay 20190107- MNS
}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncBindPendingPO() {
    var itemcode, GRNQty = 0, PoQty = 0, bal = 0;
    var obj, POPendingBody, row, rowNo = 0;
    var itemAvailable = false;
    try {
        POPendingBody = $("#tblPendingPO tbody");
        POPendingBody.children().remove();
        obj = $.parseJSON($('#<%=hidGRNJSONData.ClientID%>').val());

        for (var i = 0; i < obj.Table.length ; i++) {
            itemAvailable = false;
            GRNQty = 0;
            $("#tblGRN tbody").children().each(function () {
                if (obj.Table[i]["ItemCode"].trim() == $(this).find('td input[id*="txtItemcode"]').val() && parseFloat(obj.Table[i]["POQty"]) == parseFloat($(this).find('td input[id*="txtlQty"]').val())) {
                    itemAvailable = true;
                }

                if (obj.Table[i]["ItemCode"].trim() == $(this).find('td input[id*="txtItemcode"]').val() && parseFloat(obj.Table[i]["POQty"]) != parseFloat($(this).find('td input[id*="txtlQty"]').val())) {
                    GRNQty = $(this).find('td input[id*="txtlQty"]').val();
                }
            });

            if (itemAvailable == false) {
                bal = parseFloat(GRNQty) - parseFloat(obj.Table[i]["POQty"]);
                rowNo = parseFloat(rowNo) + 1;
                row = "<tr>"
                    + "<td> " + rowNo + " </td>"
                    + "<td> " + obj.Table[i]["ItemCode"] + " </td>"
                     + "<td> " + obj.Table[i]["ItemDescription"] + " </td>"
                    + "<td> " + obj.Table[i]["POQty"] + " </td>"
                    + "<td> " + GRNQty + " </td>"
                    + "<td> " + bal + " </td>"
                    + "</tr>"
                POPendingBody.append(row);
            }

        }


    }
    catch (err) {
        fncToastInformation(err.message);
    }
}


//================================> Open Image
function fncOpenImageScreen() {
    var page = '<%=ResolveUrl("~/Purchase/frmGRNImage.aspx") %>';
    var page = page + "?PoNo=" + $('#<%=hdfPoNo.ClientID%>').val();
    var $dialog = $('<div id="PopupGRNImage" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        height: 570,
        width: 1000,
        title: "GRN Image",
        appendTo: 'form:first'
    });
    $dialog.dialog('open');
}

        var focusflag = 0;
        function fncGridselectedcolumn() {

            try {

                focusflag = 1;

                if (focusflag == 1) {
                    var fooElementverify = document.getElementById("grdVerifydiv");
                    var fooElemententyr = document.getElementById("grdEntrydiv");
                    var fooElementcost = document.getElementById("grdCostdiv");
                    var fooElementstock = document.getElementById("grdStockdiv");
                    fooElementverify.style.backgroundColor = "red";
                    fooElemententyr.style.backgroundColor = "#1e82c5";
                    fooElementcost.style.backgroundColor = "#1e82c5";
                    fooElementstock.style.backgroundColor = "#1e82c5";
                }

                $('#thPono').css("display", "");
                $('[id*="tdPono_"]').css("display", "");


                $('#thCode').css("display", "");
                $('[id*="tdItemcode_"]').css("display", "");

                $('#thPoqty').css("display", "");
                $('[id*="tdPoqty_"]').css("display", "");

                $('#thBasicCost').css("display", "none");
                $('[id*="tdBasicCost_"]').css("display", "none");

                $('#thWPrice1').css("display", "none");
                $('[id*="tdWPrice1_"]').css("display", "none");

                $('#thWPrice2').css("display", "none");
                $('[id*="tdWPrice2_"]').css("display", "none");

                $('#thWPrice3').css("display", "none");
                $('[id*="tdWPrice3_"]').css("display", "none");

                $('#thGrossBasicAmt').css("display", "none");
                $('[id*="tdGrossBasicAmt_"]').css("display", "none");

                $('#thDiscPer').css("display", "none");
                $('[id*="tdDiscPer_"]').css("display", "none");

                $('#thDiscAmt').css("display", "none");
                $('[id*="tdDiscAmt_"]').css("display", "none");

                $('#thGrossDiscAmt').css("display", "none");
                $('[id*="tdGrossDiscAmt_"]').css("display", "none");

                $('#thSDPer').css("display", "none");
                $('[id*="tdSDPer_"]').css("display", "none");

                $('#thSDAmt').css("display", "none");
                $('[id*="tdSDAmt_"]').css("display", "none");

                $('#thGrossShDiscAmt').css("display", "none");
                $('[id*="tdGrossShDiscAmt_"]').css("display", "none");

                $('#thGrossCost').css("display", "none");
                $('[id*="tdGrossCost_"]').css("display", "none");

                $('#thTotalGrossCost').css("display", "none");
                $('[id*="tdTotalGrossCost_"]').css("display", "none");

                $('#thAddCESSAmt').css("display", "none");
                $('[id*="tdAddCessAmt_"]').css("display", "none");

                $('#thGrossGSTAmt').css("display", "none");
                $('[id*="tdGrossVATAmt_"]').css("display", "none");

                $('#thGrossCessAmt').css("display", "none");
                $('[id*="tdGrocessCessAmt_"]').css("display", "none");

                $('#thGRAddCessAmt').css("display", "none");
                $('[id*="tdGRAddCessAmt_"]').css("display", "none");

                $('#thSellingPrice').css("display", "none");
                $('[id*="tdSellingPrice_"]').css("display", "none");


                $('#thStyleCode').css("display", "");
                $('[id*="tdStyleCode_"]').css("display", "");

                $('#thSerialNo').css("display", "");
                $('[id*="tdStyleCode_"]').css("display", "");

                $('#thSGstPerc').css("display", "none");
                $('[id*="tdITaxperc1_"]').css("display", "none");


                $('#thCGstPerc').css("display", "none");
                $('[id*="tdITaxperc2_"]').css("display", "none");


                $('#thCessPer').css("display", "none");
                $('[id*="tdCessper_"]').css("display", "none");


                $('#thSGstAmt').css("display", "none");
                $('[id*="tdITaxAmt1_"]').css("display", "none");

                $('#thCGstAmt').css("display", "none");
                $('[id*="tdITaxAmt2_"]').css("display", "none");

                $('#thCESSAmt').css("display", "none");
                $('[id*="tdCessAmt_"]').css("display", "none");

                $('#thNetCost').css("display", "none");
                $('[id*="tdtNetCost_"]').css("display", "none");

                $('#thTotalValue').css("display", "none");
                $('[id*="tdTotalValue_"]').css("display", "none");

                $('#thFTotalValue').css("display", "none");
                $('[id*="tdtFTotalValue_"]').css("display", "none");

                $('#thEarnedMarPer').css("display", "none");
                $('[id*="tdEarnedMarPerc_"]').css("display", "none");

                $('#thEarMarAmt').css("display", "none");
                $('[id*="tdEarMarAmt_"]').css("display", "none");

                $('#thMarginFixed').css("display", "none");
                $('[id*="tdMarginfixed_"]').css("display", "none");

                $('#thPrintQty').css("display", "none");
                $('[id*="tdPrintQty_"]').css("display", "none");

                $('#thSGSt').css("display", "none");
                $('[id*="tdITaxt1_"]').css("display", "none");

                $('#thCGst').css("display", "none");
                $('[id*="tdITaxt2_"]').css("display", "none");

                $('#thMEDBATCH').css("display", "none");
                $('[id*="tdMedBatch_"]').css("display", "none");

                $('#thEXPDATE').css("display", "none");
                $('[id*="tdExpDate_"]').css("display", "none");

                $('#thProfitMargin').css("display", "none");
                $('[id*="tdProfitMargin_"]').css("display", "none");

                $('#thFocItem').css("display", "none");
                $('[id*="tdFOCItem_"]').css("display", "none");


                $('#thFocBatch').css("display", "none");
                $('[id*="tdFOCBatch_"]').css("display", "none");

                $('#thPurCharges').css("display", "none");
                $('[id*="tdPurchaseCharges_"]').css("display", "none");

                $('#thFrieghtCharges').css("display", "none");
                $('[id*="tdFreightCharges_"]').css("display", "none");

                $('#thVendorMargin').css("display", "none");
                $('[id*="tdVendorMargin_"]').css("display", "none");

                $('#thTotalCharges').css("display", "none");
                $('[id*="tdTotalCharges_"]').css("display", "none");

                $('#thLandingCost').css("display", "none");
                $('[id*="tdLandingCost_"]').css("display", "none");

            }
            catch (err) {
                fncToastInformation(err.message);
            }
        }
        function fncGridEntrycolumn() {
            focusflag = 0;

            if (focusflag == 0) {
                var fooElementverify = document.getElementById("grdVerifydiv");
                var fooElemententyr = document.getElementById("grdEntrydiv");
                var fooElementcost = document.getElementById("grdCostdiv");
                var fooElementstock = document.getElementById("grdStockdiv");
                fooElementverify.style.backgroundColor = "#1e82c5";
                fooElemententyr.style.backgroundColor = "red";
                fooElementcost.style.backgroundColor = "#1e82c5";
                fooElementstock.style.backgroundColor = "#1e82c5";
            }
            fncEnableAllColumns();
        }

        $(function () {
            SetAutoComplete();
        });

        
        function SetAutoComplete() {
            $('#<%=txtStyleCode.ClientID %>').focusout(function () {
                var obj = {};
                obj.Mode = 'FootWear';
                obj.invCode = $('#<%=txtItemCode.ClientID%>').val();
                obj.Prefix = $('#<%=txtStyleCode.ClientID%>').val();
            $.ajax({
                url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncStyleCodeSearch")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var temp = data.d;
                    if (temp != undefined && temp.length > 0)
                    FootWearVal = temp; 
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
            });
        $("[id$=txtStyleCodeT]").autocomplete({
            source: function (request, response) {
                  var obj = {};
            obj.Mode = 'Inv';
            obj.invCode = $('#<%=txtItemCode.ClientID%>').val();
            obj.Prefix = $('#<%=txtStyleCodeT.ClientID%>').val();
            $.ajax({
                url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncStyleCodeSearch")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[1],
                            valitemcode: item.split('|')[1] 
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
         focus: function (event, i) {
             $('#<%=txtStyleCodeT.ClientID %>').val($.trim(i.item.valitemcode));  
                        event.preventDefault();
                    },
        select: function (e, i) { 
                <%--$('#<%=txtStyleCodeT.ClientID%>').attr("readonly", "readonly");--%>
            $('#<%=txtStyleCodeT.ClientID %>').val($.trim(i.item.valitemcode));  
            return false;
        },
        minLength: 1
        });


            $("[id$=txtStyleCode]").autocomplete({
            source: function (request, response) {
                  var obj = {};
                  obj.Mode = 'Bat';
            obj.invCode = '';
            obj.Prefix = $('#<%=txtStyleCode.ClientID%>').val();
            $.ajax({
                url: '<%=ResolveUrl("GoodsAcknowledgmentNoteTextiles.aspx/fncStyleCodeSearch")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[1],
                            valitemcode: item.split('|')[1] 
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
         focus: function (event, i) {
             $('#<%=txtStyleCode.ClientID %>').val($.trim(i.item.valitemcode));  
                        event.preventDefault();
                    },
        select: function (e, i) {  
            $('#<%=txtStyleCode.ClientID %>').val($.trim(i.item.valitemcode));  
            return false;
        },
        minLength: 1
    });
        }

        function fncAlertAlredyGA(GaNo) {
            ShowPopupMessageBox("This GA Already Saved.The GRN No is: " + GaNo);
            return false;
        }

        function fncCheckStyleCode(Inventorycode,Batch,Mrp,Mode,Sno) {
            var infor = "";  
            try {
                var Batch = Batch;
                var Mrp = Mrp;
                var ItemCode = Inventorycode;

                if (FootWearVal != undefined && FootWearVal != '') {
                    if (Mode == 'Normal') { 
                        for (var i = 0; i < FootWearVal.length; i++) {
                            var Spitval = FootWearVal[i].split('||');
                            if (Spitval[0].trim() == Batch.trim() && parseFloat(Spitval[1].trim()) != parseFloat(Mrp.trim())) {
                                infor = "Mrp is different in InvCode :" + $('#<%=txtItemCode.ClientID %>').val() + " Batch : " + Batch.trim();
                                infor = infor + " and  MRP :" + Spitval[1].trim();
                                popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
                                ShowPopupMessageBoxandFocustoObject(infor);
                                fncSubClear('clr');
                                return false;
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < FootWearVal.length; i++) {
                            var Spitval = FootWearVal[i].split('|');
                            if (Spitval[0].trim() == ItemCode.trim() && parseFloat(Spitval[1].trim()) != parseFloat(Mrp.trim())) {
                                infor = "Mrp is different in InvCode :" + ItemCode + " Batch : " + Batch.trim();
                                infor = infor + " MRP :" + Spitval[1].trim();
                                infor = infor + " and  S.No :" + Sno;
                                 popUpObjectForSetFocusandOpen = $('#<%=txtStyleCode.ClientID%>');
                                 ShowPopupMessageBoxandFocustoObject(infor);
                                 fncSubClear('clr');
                                 return false;
                             }
                         }
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncCalculateSPrice(event,mode) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                var SPrice = 0;
                if (mode == "Normal") {
                    SPrice = $('#<%=txtSellPricePercentage.ClientID %>').val();
                    if ($('#<%=txtMRP.ClientID %>').val().trim() != "" && $('#<%=txtMRP.ClientID %>').val().trim() != "" && parseFloat($('#<%=txtMRP.ClientID %>').val()) > 0 && SPrice != "" && parseFloat(SPrice) > 0) {
                    var SPerValue = parseFloat($('#<%=txtMRP.ClientID %>').val()) * (SPrice / 100);
                    SPerValue = parseFloat($('#<%=txtMRP.ClientID %>').val()) - parseFloat(SPerValue);
                         $('#<%=txtSPrice.ClientID %>').val(parseFloat(SPerValue).toFixed(2));
                    }
                    $("#divSellingPer").dialog('close');
                } 
                
                <%--$('#<%=txtSellPricePercentage.ClientID %>').val('');
                $('#<%=txtSPriceDis.ClientID %>').val('');--%>
                return false;
            }
            if (mode == "F1") {
                SPrice = $('#<%=txtSPriceDis.ClientID %>').val();
                if ($('#<%=txtMrpT.ClientID %>').val().trim() != "" && $('#<%=txtMrpT.ClientID %>').val().trim() != "" && parseFloat($('#<%=txtMrpT.ClientID %>').val()) > 0 && SPrice != "" && parseFloat(SPrice) > 0) {
                    var SPerValue = parseFloat($('#<%=txtMrpT.ClientID %>').val()) * (SPrice / 100);
                    SPerValue = parseFloat($('#<%=txtMrpT.ClientID %>').val()) - parseFloat(SPerValue);
                    $('#<%=txtSellingPriceT.ClientID %>').val(parseFloat(SPerValue).toFixed(2));
                }
            } 
        }

        function fncHoldValidation() {
    <%--if ($('#<%=hidWeightbasedConversion.ClientID%>').val() == "Y") {//velu
                fncSaveConversionChildTable();
            }--%>
            if (NetCostCheck() == false) {
                fncToastInformation("Net Cost cannot be Zero or Empty");
                return false;
            }
            var totalRow = $("#tblGRN tbody").children().length;
            if (totalRow == 0) {
                ShowPopupMessageBox("No Entries to Hold");
                return false;
            } else {
                if ($("#<%=txtGrnNo.ClientID%>").val() != "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_cannothold%>');
                    return false;
                }
                else if ($("#tblGRN [id*=GRNBodyrow]").length > 0) {
                    <%--if ($('#<%=hidWeightbasedConversion.ClientID%>').val() == "Y") {//velu
                        fncSaveConversionChildTable();
                    }--%>
                    fncGetGridValuesForSave();
                    fncGetMultiUOMForSave();
                    return true;
                }
            }
        }
        //function NetCostCheck() {
        //    var status = true;
        //    $("#tblGRN tbody").children().each(function () {
        //        if ($(this).find('td input[id*=txtNetCost]').val() == "" || parseFloat($(this).find('td input[id*=txtNetCost]').val()) == 0) {
        //            setTimeout(function () {
        //                rowObj.find('td input[id*="txtNetCost"]').select();
        //            }, 20);
        //            fncToastInformation("Net Cost cannot be Zero or Empty");
        //            status = false;
        //        }
        //    });
        //    return status;
        //}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <asp:UpdatePanel ID="upgrn" runat="server">
            <ContentTemplate>
                <div class="barcodepahe_color">
                    <div class="main-container">
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                                </li>
                                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                                <li>
                                    <a href="../Purchase/frmGoodsAcknowledgement.aspx">
                                        <%=Resources.LabelCaption.lblViewGoodsAcknowledgement%>
                                    </a>
                                </li>
                                <li class="active-page uppercase" id="breadcrumbs_text_GRN">View GRN Maintenance
                                </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                            </ul>
                            <div class="price_change_itemname" id="divPrcItemname">
                                <asp:Label ID="lblPrcItemname" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <div class="container-group-full gid_top_border">
                            <div class="gid-split gid-split3">
                                <div class="left">
                                    <asp:Label ID="lblGrnNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtGrnNo" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblInvNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_InvNo %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtInvNo" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblVendorCode" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtVendorCode" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="gid-split">
                                <div class="left">
                                    <asp:Label ID="lblGrnDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNDate %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtgrndate" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblInvDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_InvDate %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtInvDate" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control-res background_yellow" onkeydown="return fncVendorNameKeyPress(event);"></asp:TextBox>
                                </div>

                            </div>
                            <div class="gid-split">
                                <div class="left">
                                    <asp:Label ID="lblgaNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_GANumber %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtgaNo" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblgaDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_GADate %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtgaDate" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblRemark" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="gid-split" id="divgaDet1">
                                <div class="left">
                                    <asp:Label ID="lblGrossAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_GrossAmt %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtBasicAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                          onFocus="this.select()" onblur="fncGAAmountCalculation()"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblVatAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_VATAmt %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtVatAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        onkeypress="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()" onblur="fncGAAmountCalculation()"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblBillAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_BillAmt %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:Panel ID="Panel14" runat="server">
                                        <asp:TextBox ID="txtBillAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()"></asp:TextBox>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="gid-split gid-split1" id="divgaDet2">
                                <div class="left">
                                    <asp:Label ID="lblAd" runat="server" Text='<%$ Resources:LabelCaption,lbl_AD %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:TextBox ID="txtAD" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        onkeypress="return isNumberKeywithMin(event)" onFocus="this.select()" onblur="fncGAAmountCalculation()"></asp:TextBox>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblschmamt" runat="server" Text='<%$ Resources:LabelCaption,lbl_SchmAmt %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:Panel ID="Panel16" runat="server">
                                        <asp:TextBox ID="txtGASchemeDisAmt" runat="server" CssClass="form-control-res-right" Text="0.00"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return fncReturnfalse(event);" onFocus="this.select()"></asp:TextBox>
                                    </asp:Panel>
                                </div>
                                <div class="left">
                                    <asp:Label ID="lblTotalPay" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalPayable %>'></asp:Label>
                                </div>
                                <div class="right">
                                    <asp:Panel ID="Panel17" runat="server">
                                        <asp:TextBox ID="txttotPayable" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return fncReturnfalse(event);" onFocus="this.select()"></asp:TextBox>
                                    </asp:Panel>
                                </div>
                            </div>

                            <div class="gid-split2" id="divBarcode1">
                                <div class="control-button1 ">
                                    <asp:LinkButton ID="LinkBarCode" runat="server" class="button-blue grn_barcode_btn" OnClientClick="fncBarcodeQtyValidation();return false;" Text="BarCode(F10)"></asp:LinkButton>
                                </div>
                                <div class="control-button1">
                                    <asp:LinkButton ID="lnkLoadQty" runat="server" class="button-blue" OnClientClick="fncLoadQtyclick();return false;">LoadQty</asp:LinkButton>
                                </div>
                            </div>
                            <div class="gid-split2" style="width: auto; margin: 7px;" id="divVendor">
                                <div runat="server" id="vendorItem">
                                    <asp:CheckBox ID="cbVendorItem" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="radioboxlistgreen" Checked="True" Text='<%$ Resources:LabelCaption,lblVendorItems %>' />
                                </div>
                                <div runat="server" id="divSDonGrosscost">
                                    <asp:CheckBox ID="cbSDOnGrossCost" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="radioboxlistgreen" Checked="True" Text='SD On G.Cost' />
                                </div>
                                <div runat="server" id="printbarcode">
                                    <asp:CheckBox ID="cbPrintBarcodeAllItem" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="radioboxlistgreen" Text='<%$ Resources:LabelCaption,lblPrintAllItems %>' />
                                </div>
                            </div>
                            <div id="divPriceChaneAlert" class="price_alert_gid">

                                <div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblOldCost" runat="server" Text='Prv.Netcost'></asp:Label>
                                        <asp:TextBox ID="txtoldCost" runat="server" CssClass="form-control-res-right" onkeydown="return fncClosePriceAlert(event);"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblOldMrp" runat="server" Text='Prv.MRP'></asp:Label>
                                        <asp:TextBox ID="txtOldMRP" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblOldSelling" runat="server" Text='Prv.Sellingprice'></asp:Label>
                                        <asp:TextBox ID="txtOldSelling" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lbloldMarginfixed" runat="server" Text='Prv.Mar.Fixed'></asp:Label>
                                        <asp:TextBox ID="txtoldMarginfixed" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblOldMarginEarned" runat="server" Text='Prv.Mar.Earned'></asp:Label>
                                        <asp:TextBox ID="txtoldMarginEarned" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                </div>

                                <div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblNewCost" runat="server" Text='Cur.Netcost'></asp:Label>
                                        <asp:TextBox ID="txtNewCost" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblNewMrp" runat="server" Text='Cur.MRP'></asp:Label>
                                        <asp:TextBox ID="txtNewMRP" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblNewSelling" runat="server" Text='Cur.SellingPrice'></asp:Label>
                                        <asp:TextBox ID="txtNewSelling" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblNewMarginFixed" runat="server" Text='Cur.Mar.Fixed'></asp:Label>
                                        <asp:TextBox ID="txtNewMarginFixed" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                    <div class="gid_pricechange_alert">
                                        <asp:Label ID="lblNewMarginEarned" runat="server" Text='Cur.Mar.Earned'></asp:Label>
                                        <asp:TextBox ID="txtNewMarginEarned" runat="server" CssClass="form-control-res-right" onkeydown="return false;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="divItemSearch" class="Payment_fixed_headers gid_Itemsearch gidItem_Searchtbl display_none">
                            <table id="tblItemSearch" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr id="itemsearch" runat="server">
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Code
                                        </th>
                                        <th scope="col">Item Description
                                        </th>
                                        <th scope="col">Size
                                        </th>
                                        <th scope="col">Qty in Hand
                                        </th>
                                        <th scope="col">PRRQty
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th scope="col">SellingPrice
                                        </th>
                                        <th scope="col">GST(%)
                                        </th>
                                        <th scope="col">Nett
                                        </th>
                                        <th scope="col">C.D
                                        </th>
                                        <th scope="col">S.D
                                        </th>
                                        <th scope="col">Margin
                                        </th>
                                        <th scope="col">Pur.Date
                                        </th>
                                        <th scope="col">PO Qty
                                        </th>
                                        <th scope="col">Pur.Qty
                                        </th>
                                        <th scope="col">Sold.Qty
                                        </th>
                                        <th scope="col">SINO
                                        </th>
                                        <th scope="col">Batch No
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        
                        <div id="grdcolumndis">
                            <div class="containergrd">
                                <div id="grdEntrydiv" class="containergrd1">
                                    <asp:LinkButton ID="grdEntry" runat="server" Text="Entry" Style="color: white" OnClientClick="fncGridEntrycolumn();return false;"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="containergrd">
                                <div id="grdVerifydiv" class="containergrd1">
                                    <asp:LinkButton ID="grdVerify" runat="server" Text="Verify" Style="color: white" OnClientClick="fncGridselectedcolumn();return false;"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="containergrd display_none">
                                <div id="grdCostdiv" class="containergrd1">
                                    <asp:LinkButton ID="grdCost" runat="server" Text="Cost" Style="color: white" OnClientClick="fncGridCostcolumn();return false;"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="containergrd display_none">
                                <div id="grdStockdiv" class="containergrd1">
                                    <asp:LinkButton ID="grdStock" runat="server" Text="Stock" Style="color: white" OnClientClick="fncGridStockcolumn();return false;"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div id="divGRN" class="Payment_fixed_headers grn">
                            <table id="tblGRN" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Delete
                                        </th>
                                        <th scope="col">Po.No
                                        </th>
                                        <th scope="col">Code
                                        </th>
                                        <th scope="col">Item Decsription
                                        </th>
                                        <th scope="col">Po Qty
                                        </th>
                                        <th scope="col">Inv.Qty
                                        </th>
                                        <th scope="col">LQty
                                        </th>
                                        <th scope="col">Foc
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th id="thBasicCost" scope="col">Basic Cost
                                        </th>
                                        <th id="thSellingPrice" scope="col">Selling Price
                                        </th>
                                        <th id="thWPrice1" scope="col">W.Price1
                                        </th>
                                        <th id="thWPrice2" scope="col">W.Price2
                                        </th>
                                        <th id="thWPrice3" scope="col">W.Price3
                                        </th>
                                        <th id="thGrossBasicAmt" scope="col">Gross Basic Amt
                                        </th>
                                        <th id="thDiscPer" scope="col">Disc (%)
                                        </th>
                                        <th id="thDiscAmt" scope="col">Disc Amt
                                        </th>
                                        <th id="thGrossDiscAmt" scope="col">Gross Disc Amt
                                        </th>
                                        <th id="thSDPer" scope="col">SD (%)
                                        </th>
                                        <th id="thSDAmt" scope="col">S.D Amt
                                        </th>
                                        <th id="thGrossShDiscAmt" scope="col">Gross ShDisc Amt
                                        </th>
                                        <th id="thGrossCost" scope="col">Gross Cost
                                        </th>
                                        <th id="thTotalGrossCost" scope="col">Total Gross Cost
                                        </th>
                                        <th scope="col" id="thSGstPerc">SGST (%)
                                        </th>
                                        <th scope="col" id="thCGstPerc">CGST (%)
                                        </th>
                                        <th scope="col" id="thCessPer">CESS (%)
                                        </th>
                                        <th scope="col" id="thSGstAmt">SGST Amt
                                        </th>
                                        <th scope="col" id="thCGstAmt">CGST Amt
                                        </th>
                                        <th scope="col" id="thCESSAmt">CESS Amt </th>
                                        <th scope="col" id="thAddCESSAmt">Add CESS Amt </th>
                                        <th id="thGrossGSTAmt" scope="col">Gross GST Amt
                                        </th>
                                        <th id="thGrossCessAmt" scope="col">Gross Cess Amt</th>
                                        <th id="thGRAddCessAmt" scope="col">GR Add Cess Amt</th>
                                        <th id="thNetCost" scope="col">Net Cost
                                        </th>
                                        <th id="thTotalValue" scope="col">Total Value
                                        </th>
                                        <th id="thFTotalValue" scope="col">F.Total Value
                                        </th>
                                        <th id="thEarnedMarPer" scope="col">Earned Mar(%)
                                        </th>
                                        <th id="thEarMarAmt" scope="col">Ear.Mar Amt
                                        </th>
                                        <th id="thMarginFixed" scope="col">Margin Fixed
                                        </th>
                                        <th id="thPrintQty" scope="col">Print Qty
                                        </th>
                                        <th scope="col" id="thSGSt">SGST
                                        </th>
                                        <th scope="col" id="thCGst">CGST
                                        </th>
                                        <th id="thMEDBATCH" scope="col">MEDBATCH
                                        </th>
                                        <th id="thEXPDATE" scope="col">EXPDATE
                                        </th>
                                        <th scope="col">Old Earned Mar
                                        </th>
                                        <th id="thProfitMargin" scope="col">Profit Margin
                                        </th>
                                        <th id="thFocItem" scope="col">Foc Item
                                        </th>
                                        <th id="thFocBatch" scope="col">Foc Batch
                                        </th>
                                        <th id="thPurCharges" scope="col">Pur Charges
                                        </th>
                                        <th id="thFrieghtCharges" scope="col">Frieght Charges
                                        </th>
                                        <th id="thVendorMargin" scope="col">Vendor Margin
                                        </th>
                                        <th id="thTotalCharges" scope="col">Total Charges
                                        </th>
                                        <th id="thLandingCost" scope="col">Landing Cost
                                        </th>
                                        <th id="thStyleCode" scope="col">Style Code
                                        </th>
                                        <th id="thSerialNo" scope="col">Serial No
                                        </th>
                                        <th scope="col">Purchase Profit
                                        </th>
                                        <th scope="col">NetCostAffect
                                        </th>
                                        <th scope="col">Batch
                                        </th>
                                        <th scope="col">ItemType
                                        </th>
                                        <th scope="col">PricingType
                                        </th>
                                        <th scope="col">MarkDownPerc
                                        </th>
                                        <th scope="col">OldSellingPrice
                                        </th>
                                        <th scope="col">AvgCost
                                        </th>
                                        <th scope="col">BasicSelling
                                        </th>
                                        <th scope="col">ItemBatchNo
                                        </th>
                                        <th scope="col">AllowExpireDate
                                        </th>
                                        <th scope="col">FTCFromDate
                                        </th>
                                        <th scope="col">FTCToDate
                                        </th>
                                        <th scope="col">BuyQty
                                        </th>
                                        <th scope="col">FreeQty
                                        </th>
                                        <th scope="col">GridRowStatus
                                        </th>
                                        <th scope="col">OldMRP
                                        </th>
                                        <th scope="col">FTCItemcode
                                        </th>
                                        <th scope="col">FTCBatchno
                                        </th>
                                        <th scope="col">FTCInputQty
                                        </th>
                                        <th scope="col">WQty
                                        </th>
                                        <th scope="col">WUOM
                                        </th>
                                        <th scope="col">LCost
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="divGRNtbl">
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
                            <ProgressTemplate>
                                <div class="modal-loader">
                                    <div class="center-loader">
                                        <img alt="" src="../images/loading_spinner.gif" />
                                    </div>
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <div runat="server" id="divgidbottomcontrol" class="gid_bottom_control">
                            <div class="display_table">
                                <div class="container7" style="width: 4%;">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblItemCode" runat="server" Text='I.Code'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown=" return fncGetInventoryCode_onEnter(event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res" Enabled="false"
                                            onkeydown="return fncReturnfalse(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container7" style="width: 3%;">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblEsno" runat="server" Text="S.NO"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtESerialNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2" style="width: 4%;">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblStyleCode" runat="server" Text="StyleCode"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtStyleCodeT" runat="server" CssClass="form-control-res" MaxLength ="15"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblWQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_WQty %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtWQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onchange="fncWholeQtyCal();" onkeypress="return isNumberKeyWithDecimalNew(event);" Text="0"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblInvQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_InvQty %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtInvQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeypress="return fncInvQty_KeyPress(event)" onchange="fncInvQtyFocusOut();" onfocus="this.select();">0</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblLQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_lQty %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeypress="return isNumberKey(event)" onblur="fncInitializeMedicalBatchDialog('LQty');return false;">0</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeydown="return fncMRPEnterTraversal(event);" onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncMRPFocusOut();"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblBCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_BCost %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Panel ID="Panel5" runat="server">
                                            <asp:TextBox ID="txtBCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                                onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncBasicCostFocusOut()"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="container2 container6" style="width: 4%;">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblDiscper" runat="server" Text='<%$ Resources:LabelCaption,lbl_Discper %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Panel ID="Panel6" runat="server">
                                            <asp:TextBox ID="txtDiscPer" runat="server" CssClass="form-control-res" Style="text-align: right"
                                                onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncDiscountandShemeDiscCalc('Discper')"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt gid_foc_lable_center">
                                        <asp:Label ID="lblAmt" runat="server" Text='<%$ Resources:LabelCaption,lbl_Amt %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Panel ID="Panel7" runat="server">
                                            <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                                onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncDiscountandShemeDiscCalc('DiscAmt')"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="container2 container6">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblSDper" runat="server" Text='<%$ Resources:LabelCaption,lbl_SDper %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Panel ID="Panel8" runat="server">
                                            <asp:TextBox ID="txtSDPer" runat="server" CssClass="form-control-res" Style="text-align: right"
                                                onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncDiscountandShemeDiscCalc('ShDiscper')"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblAmt1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Amt %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Panel ID="Panel9" runat="server">
                                            <asp:TextBox ID="txtSchemeAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                                onkeydown="return fncEnterkeyTraversal(event,'SchemeAmt');" onkeypress="return isNumberKeyWithDecimalNew(event)" onchange="fncDiscountandShemeDiscCalc('ShAmt')"></asp:TextBox>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblGCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_GCost %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGrossCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container1" style="width: 5%">
                                    <div runat="server">
                                        <div class="spilt gid_foc_lable_center">
                                            <asp:Label ID="lblGST" runat="server" Text='GST'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtGST" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                            <div id="dropGST" runat="server">
                                                <asp:DropDownList ID="ddlGST" runat="server" onchange="fncGSTChange();" Width="100%"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container2 container6" style="display: none">
                                    <div id="IGST" runat="server">
                                        <div class="spilt gid_foc_lable_center">
                                            <asp:Label ID="lblSGST" runat="server" Text='<%$ Resources:LabelCaption,lblSGST %>'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtITax1" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="container2 container6" style="display: none">
                                    <div id="SGST" runat="server">
                                        <div class="spilt gid_foc_lable_center">
                                            <asp:Label ID="lblCGST" runat="server" Text='<%$ Resources:LabelCaption,lblCGST %>'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtITax2" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="container2 container6" style="width: 4%">
                                    <div id="cess" runat="server">
                                        <div class="spilt gid_foc_lable_center">
                                            <asp:Label ID="lblcess" runat="server" Text='<%$ Resources:LabelCaption,lblCESS %>'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtcess" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div id="Div1" runat="server">
                                        <div class="spilt gid_foc_lable_center">
                                            <asp:Label ID="lblAddCess" runat="server" Text='Add Cess'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtAddCess" runat="server" onkeypress="return isNumberKeyWithDecimalNew(event);" onchange="fncAdditionalCessAmtCal();" CssClass="form-control-res-right" Text="0.00" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblNetCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetCost %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtNetCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblSPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellPrice %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)"
                                            onkeydown="return fncAddItemsDirectly(event);"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="gid_Add_button">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue grn_add_btn" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                            OnClientClick="fncAddValidation();return false;"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="display_table">
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblMFixed" runat="server" Text='<%$ Resources:LabelCaption,lbl_VenMargin %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMFixed" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container6">
                                    <div align="center">
                                        <asp:Label ID="lblEarned" runat="server" Text='<%$ Resources:LabelCaption,lbl_MEarned %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtEarned" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblWUom" runat="server" Text='<%$ Resources:LabelCaption,lbl_WUOM %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtwuom" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                        <div id="divMultiplie" runat="server">
                                            <asp:DropDownList ID="ddlMultipleUOM" runat="server" Width="100%"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="container6">
                                    <div align="center">
                                        <asp:Label ID="lblLuom" runat="server" Text='<%$ Resources:LabelCaption,lbl_LUOM %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtluom" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblTotQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotQty %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtTotQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container9">
                                    <div align="center">
                                        <asp:Label ID="lblSGSTper" runat="server" Text='<%$ Resources:LabelCaption,lblSGSTper %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtITaxper1" runat="server" CssClass="form-control-res" Style="text-align: right" Text="0"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container9">
                                    <div align="center">
                                        <asp:Label ID="lblCGSTper" runat="server" Text='<%$ Resources:LabelCaption,lblCGSTper %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtITaxper2" runat="server" CssClass="form-control-res" Style="text-align: right" Text="0"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container9">
                                    <div align="center">
                                        <asp:Label ID="lblCessPer" runat="server" Text='<%$ Resources:LabelCaption,lblCessper %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtCessPer" runat="server" CssClass="form-control-res" Style="text-align: right" Text="0"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblGrBasic" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRBasic %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtTotalGBCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblGrCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRCost %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtTotalGCost" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblgrDisc" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRDisc %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGRDisc" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblgrSchm" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRSchm %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGRShDiscAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblgrVat" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRVat %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGRVat" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2">
                                    <div align="center">
                                        <asp:Label ID="lblGRCessAmt" runat="server" Text='<%$ Resources:LabelCaption,lblGRCessAmt %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGRCessAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2 container10">
                                    <div align="center">
                                        <asp:Label ID="lblGRAddCess" runat="server" Text='GR.Add.Cess'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGRAddCess" runat="server" CssClass="form-control-res-right" Text="0.00"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container2 container8">
                                    <div align="center">
                                        <asp:Label ID="lblTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Total %>'></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="wprice" runat="server">
                                    <div class="container2">
                                        <div align="center">
                                            <asp:Label ID="lblWPrice1" runat="server" Text='<%$ Resources:LabelCaption,lblWPrice1 %>'></asp:Label>
                                        </div>
                                        <div>
                                            <asp:TextBox ID="txtWpirce1" runat="server" CssClass="form-control-res" Style="text-align: right" onkeydown="return fncAddItemsDirectly(event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div id="wprice1" runat="server">
                                        <div class="container2">
                                            <div align="center">
                                                <asp:Label ID="lblWPrice2" runat="server" Text='<%$ Resources:LabelCaption,lblWPrice2 %>'></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtWpirce2" runat="server" CssClass="form-control-res" Style="text-align: right"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div align="center">
                                                <asp:Label ID="lblWPrice3" runat="server" Text='<%$ Resources:LabelCaption,lblWPrice3 %>'></asp:Label>
                                            </div>
                                            <div>
                                                <asp:TextBox ID="txtWpirce3" runat="server" CssClass="form-control-res" Style="text-align: right" onkeydown="return fncAddItemsDirectly(event);"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gid_Add_button">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue grn_add_btn" Text='Clear'
                                            OnClientClick="return fncSubClear('clr')"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gid_bottom_control">
                            <div class="grn_net_value">
                                <div class="grn_net_lbl">
                                    <asp:Label ID="lblGrossAmount" runat="server" Text='Gr.Amt'></asp:Label>
                                </div>
                                <div class="grn_net_txt">
                                    <asp:TextBox ID="txtNetGrossAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="bottom-split1">
                                <div class="grn_net_lbl3">
                                    <asp:LinkButton ID="btnVat" runat="server" OnClientClick="fncInitializeVetDetailDialog();return false;"
                                        Text='GST(Alt+G)'></asp:LinkButton>
                                </div>
                                <div class="grn_net_lbl3">
                                    <asp:TextBox ID="txtNetVat" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value3">
                                <div class="grn_net_txt2">
                                    <asp:Label ID="lblNetCessAmt" runat="server" Text='Total Cess Amt'></asp:Label>
                                </div>
                                <div class="grn_net_lbl2">
                                    <asp:TextBox ID="txtNetCessAmt" runat="server" CssClass="form-control-res-right"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value4">
                                <div class="grn_net_lbl4">
                                    <asp:Label ID="lblGSTCess" runat="server" Text='GST+Cess Amt'></asp:Label>
                                </div>
                                <div class="grn_net_txt4">
                                    <asp:TextBox ID="txtTotalGSTCessAmt" runat="server" CssClass="form-control-res-right" Text="0.00"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);"></asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value3">
                                <div class="grn_net_lbl">
                                    <asp:Label ID="lblBillAmount" runat="server" Text='Bill Amt'></asp:Label>
                                </div>
                                <div class="grn_net_txt">
                                    <asp:TextBox ID="txtTotalBillAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value1">
                                <div class="grn_net_txt4">
                                    <asp:LinkButton ID="btnDeduction" runat="server" OnClientClick="fncInitializeDeduction();return false;"
                                        Text='<%$ Resources:LabelCaption,lbl_Deduction %>'></asp:LinkButton>
                                </div>
                                <div class="grn_net_txt5">
                                    <asp:TextBox ID="txtDeduction" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        Enabled="false">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value1">
                                <div class="grn_net_lbl2">
                                    <asp:LinkButton ID="btnAddition" runat="server" OnClientClick="fncInitializeAddition();return false;"
                                        Text='<%$ Resources:LabelCaption,lbl_Addition %>'></asp:LinkButton>
                                </div>
                                <div class="grn_net_txt2">
                                    <asp:TextBox ID="txtAddition" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        Enabled="false">0.00</asp:TextBox>
                                </div>
                            </div>
                            <div class="grn_net_value4">
                                <div class="grn_net_txt4">
                                    <asp:Label ID="lblTotalPayable" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalPayable %>'></asp:Label>
                                </div>
                                <div class="grn_net_txt5">
                                    <asp:TextBox ID="txtTotalPayable" runat="server" CssClass="form-control-res" Style="text-align: right"
                                        ReadOnly="true" onkeydown="return fncReturnfalse(event);">0.00</asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="gid_bottom_control">
                            <div class="bottom-split2-left">
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lbltotal1" runat="server" Text='<%$ Resources:LabelCaption,lbl_TRow %>'></asp:Label>
                                    </div>
                                    <div class="spilt">

                                        <asp:TextBox ID="txtTotalRow" runat="server" CssClass="form-control-res-right" Enabled="false" Text="0.00"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="container2">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_TQty %>'></asp:Label>
                                    </div>
                                    <div class="spilt">
                                        <asp:TextBox ID="txtTotalQty" runat="server" CssClass="form-control-res-right" Enabled="false" Text="0.00"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblTotBCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotBasicCost %>'></asp:Label>
                                    </div>
                                    <div class="spilt">
                                        <asp:TextBox ID="txtNetBasicCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false">0.00</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblTotDisc" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotDisc %>'></asp:Label>
                                    </div>
                                    <div class="spilt">
                                        <asp:TextBox ID="txtNetDiscount" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false">0.00</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblTotSchmDisc" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotSchmDisc %>'></asp:Label>
                                    </div>
                                    <div class="spilt">
                                        <asp:TextBox ID="txtNetShDiscAmt" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false">0.00</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt" align="center">
                                        <asp:Label ID="lblTotGrCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotGrCost %>'></asp:Label>
                                    </div>
                                    <div class="spilt">
                                        <asp:TextBox ID="txtNetGrossCost" runat="server" CssClass="form-control-res" Style="text-align: right"
                                            Enabled="false">0.00</asp:TextBox>
                                    </div>
                                </div>
                                <div class="container3">
                                    <div class="spilt">
                                        <asp:Label ID="lblRemark1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                                    </div>
                                    <div class="spilt" style="width: 400px">
                                        <asp:TextBox ID="txtGIDRemarks" runat="server" CssClass="form-control-res" onkeydown="return fncReturnfalse(event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-container">
                                <div class="gid-control grn_save_btn">
                                    <asp:LinkButton ID="lnkClose" runat="server" class="button-blue"
                                        OnClick="lnkbtnOk_Click" Text="Close(F8)"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-container">
                                <div class="gid-control grn_save_btn">
                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return fncSaveValidation()"
                                        OnClick="lnkSave_Click" Text="Save(F4)"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-container">
                                <div class="gid-control grn_save_btn">
                                    <asp:LinkButton ID="lnkViewImages" runat="server" class="button-red" OnClientClick="fncHoldValidation();return false;" OnClick="btnHold_Click"
                                        Text="Hold"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hiddencol">
                        <asp:HiddenField ID="hidisbatch" runat="server" />
                        <asp:HiddenField ID="hidVendorStatus" runat="server" />
                        <asp:HiddenField ID="hidWholeSale" runat="server" />
                        <asp:HiddenField ID="hidWholeSaleWprice1Only" runat="server" />
                        <asp:HiddenField ID="hidITaxAmt1" runat="server" />
                        <asp:HiddenField ID="hidITaxAmt2" runat="server" />
                        <asp:HiddenField ID="hidMFAmt" runat="server" />
                        <asp:HiddenField ID="hidVendorGSTNo" runat="server" />

                        <asp:HiddenField ID="hidItemType" runat="server" />
                        <asp:HiddenField ID="hidPricingType" runat="server" />
                        <asp:HiddenField ID="hidMarkDownPerc" runat="server" />
                        <asp:HiddenField ID="hidOldSellingPrice" runat="server" />
                        <asp:HiddenField ID="hidAvgCost" runat="server" />
                        <asp:HiddenField ID="hidBasicSellingPrice" runat="server" />
                        <asp:HiddenField ID="hidFocItem" runat="server" />
                        <asp:HiddenField ID="hidFocBatch" runat="server" />
                        <asp:HiddenField ID="hidFocQty" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidFocStatus" runat="server" />
                        <asp:HiddenField ID="hidAllowExpireDate" runat="server" />
                        <asp:HiddenField ID="hidftcFromDate" runat="server" />
                        <asp:HiddenField ID="hidftcToDate" runat="server" />
                        <asp:HiddenField ID="hidftcBuyQty" runat="server" />
                        <asp:HiddenField ID="hidftcFreeQty" runat="server" />
                        <asp:HiddenField ID="hidGidHoldNo" runat="server" />
                        <asp:HiddenField ID="hidInvoiceNo" runat="server" />
                        <asp:HiddenField ID="hidVendorCode" runat="server" />
                        <asp:HiddenField ID="hidOldCost" runat="server" />
                        <asp:HiddenField ID="hidOldMRP" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidPrvUnitCost" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidPrvMRP" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidDeActiveStatus" runat="server" Value="0" />

                        <asp:HiddenField ID="hidoldMarFixed" runat="server" />
                        <asp:HiddenField ID="hidoldMarEarned" runat="server" />
                        <asp:HiddenField ID="hidoldvendorMargin" runat="server" />
                        <asp:HiddenField ID="hidShortSupplyAmt" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidExcessSupplyAmt" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidCreditandDebitNoteStatus" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidChildItemPriceList" runat="server" />
                        <asp:HiddenField ID="hidGidGSTFlag" runat="server" />
                        <asp:HiddenField ID="hidRepeaterValueStatus" runat="server" Value="" />
                        <asp:HiddenField ID="hidMBatchNo" runat="server" />
                        <asp:HiddenField ID="hidExpireDate" runat="server" Value="" />
                        <asp:HiddenField ID="hidCessAmt" runat="server" Value="0" />
                        <asp:HiddenField ID="hidbillType" runat="server" />
                        <asp:HiddenField ID="hidChangeRowval" runat="server" />
                        <asp:HiddenField ID="hidfSellingPriceRoundOff" runat="server" />
                        <asp:HiddenField ID="hidFranValue" runat="server" />
                        <asp:HiddenField ID="hidGidLocationcode" runat="server" />

                        <asp:HiddenField ID="hidMRP" runat="server" />
                        <asp:HiddenField ID="hidItemcode" runat="server" />
                        <asp:HiddenField ID="hidBasiccost" runat="server" />
                        <asp:HiddenField ID="hidBreakpricestatus" runat="server" />
                        <asp:HiddenField ID="hidBarcodeStatus" runat="server" />
                        <asp:HiddenField ID="hidTransferout" runat="server" Value="N" />
                        <asp:HiddenField ID="hidAddCessAmt" runat="server" />
                        <asp:HiddenField ID="hidPurchasedBy" runat="server" />
                        <asp:HiddenField ID="hidMarginFixType" runat="server" />
                        <asp:HiddenField ID="hidUOMConv" runat="server" />
                        <asp:HiddenField ID="hidDeActiveMRP" runat="server" Value="0.00" />
                        <asp:HiddenField ID="hidChildPriceChangeAllLocation" runat="server" />
                        <asp:HiddenField ID="hidsellingCalFromPrvMRP" runat="server" />
                        <asp:HiddenField ID="hidCostEditAllow" runat="server" Value="0" />
                        <asp:HiddenField ID="hidFTCItemcode" runat="server" Value="" />
                        <asp:HiddenField ID="hidFTCBatchNo" runat="server" Value="" />
                        <asp:HiddenField ID="hidFTCInputQty" runat="server" Value="0" />
                        <asp:HiddenField ID="hidCurLocation" runat="server" Value="0" />
                        <asp:HiddenField ID="GRNXmldata" runat="server" Value="" />
                        <asp:HiddenField ID="hidGRNJSONData" runat="server" Value="" />
                        <asp:HiddenField ID="hidError" runat="server" Value="NoError" />
                        <asp:HiddenField ID="hidMultiUOM" runat="server" Value="N" />
                        <asp:HiddenField ID="hidMultiUOMSave" runat="server" Value="" />
                        <asp:HiddenField ID="hidScanedWithPO" runat="server" Value="N" />
                        <asp:HiddenField ID="hidGidExistVal" runat="server" Value="N" />
                        <asp:HiddenField ID="hidGSTEdit" runat="server" Value="N" />
                        <asp:HiddenField ID="hidLanaCost" runat="server" Value="N" />
                        <asp:HiddenField ID="hidwbgrn" runat="server" Value="N" />
                        <asp:HiddenField ID="hdfPoNo" runat="server" />

                        <%--Textiles Value--%>
                        <asp:HiddenField ID="hidSubTotal" runat="server" Value="0" />
                        <asp:HiddenField ID="hidMarginFixAmt" runat="server" Value="0" />
                        <asp:HiddenField ID="hidMFPer" runat="server" Value="0" />
                        <asp:HiddenField ID="hdfPendingPO" runat="server" Value="N" />
                        <asp:HiddenField ID="hdfPendingPOReCreate" runat="server" Value="N" />
                        <asp:HiddenField ID="hidSagarSilks" runat="server" Value="N" />
                        <input type="hidden" id="HiddenXmldata" runat="server" />

                        <asp:Button ID="btnHold" runat="server" OnClick="btnHold_Click" />
                        <asp:Button ID="btnShowGidHold" runat="server" OnClick="btnShowGidHold_Click" />
                        <asp:Button ID="btnSave" runat="server" OnClick="lnkSave_Click" />
                        <asp:Button ID="btnCDSave" runat="server" OnClick="btnCDSave_Click" />
                        <asp:Button ID="btnDeleteRow" runat="server" />
                        <asp:Button ID="btnclose" runat="server" OnClick="lnkbtnOk_Click" />
                        <asp:Button ID="lnkbtnPrint" runat="server" OnClick="lnkbtnPrint_Click" />



                        <div id="divCreditandDebitNote">
                            <div>
                                <asp:Label ID="lblCreditandDebitNote" runat="server"></asp:Label>
                            </div>
                            <div class="paymentDialog_Center">
                                <div class="PaymentDialog_yes">
                                    <asp:LinkButton ID="lnkCDYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                        OnClientClick="fncCreditAndDebitNoteConfirmation();return false;"> </asp:LinkButton>
                                </div>
                                <div class="PaymentDialog_No">
                                    <asp:LinkButton ID="lnkCDNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                                        OnClientClick="fncCloseCreditAndDebitNoteDialog();return false;"> </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div id="GidSave">
                            <div>
                                <asp:Label ID="lblGidSave" runat="server"></asp:Label>
                            </div>
                            <div class="dialog_center">
                                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                    OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                            </div>
                        </div>
                        <div id="divFoc">
                            <div>

                                <div class="gid_foc_Affectnetcost">
                                    <asp:CheckBox ID="cbSalesNotAffectNetCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesNotAffectNetCost %>'
                                        onclick="fncFocModeClick('salesNotAffect')" />
                                </div>
                                <div class="gid_foc_NotAffectnetcost">
                                    <asp:CheckBox ID="cbFreetoCustomer" runat="server" Text='<%$ Resources:LabelCaption,lbl_FreeToCustomer %>'
                                        onclick="fncFocModeClick('FTC')" />
                                </div>
                            </div>
                            <div class="gid_foc_Qitem_top ">
                                <div class="gid_foc_qualifingItem">
                                    <asp:Label ID="lblQualifyingItem" runat="server" Text='<%$ Resources:LabelCaption,lbl_QualifyingItem %>'></asp:Label>
                                    <asp:TextBox ID="txtQualifyingItem" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_foc_Qty gid_foc_lable_center">
                                    <asp:Label ID="lblQualifyingQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                                    <asp:TextBox ID="txtQualifyingQty" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="gid_foc_item_top ">
                                <div class="gid_foc_Item">
                                    <asp:Label ID="lblFocItem" runat="server" Text='<%$ Resources:LabelCaption,lbl_FocItem %>'></asp:Label>
                                    <asp:TextBox ID="txtFocItem" runat="server" CssClass="form-control-res" onchange="fncGetBatchDetail();" onkeydown="return fncFOCItemEnter(event);"></asp:TextBox>
                                </div>
                                <div class="gid_foc_Qty gid_foc_lable_center">
                                    <asp:Label ID="lblFocBatch" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batch %>'></asp:Label>
                                    <asp:DropDownList ID="ddlfocBatch" runat="server" Width="100%" onchange="fncFocBatchSelect()">
                                    </asp:DropDownList>
                                </div>
                                <div class="gid_foc_Qty gid_foc_lable_center">
                                    <asp:Label ID="lblTotalQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalQty %>'></asp:Label>
                                    <asp:TextBox ID="txtFocTotalQty" runat="server" CssClass="form-control-res-right" onkeydown="return fncCloseFocDialog(event);"></asp:TextBox>
                                </div>
                            </div>
                            <div class="gid_foc_Date_top ">
                                <div class="gid_foc_date ">
                                    <asp:Label ID="lblfromdate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                    <asp:TextBox ID="txtfocFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                                <div class="gid_foc_date gid_foc_lable_center">
                                    <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                    <asp:TextBox ID="txtFocToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                                <div class="gid_foc_date gid_foc_lable_center">
                                    <asp:Label ID="lblBuyQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_BuyQty %>'></asp:Label>
                                    <asp:TextBox ID="txtFocBuyQty" runat="server" Text="0.00" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                                <div class="gid_foc_date gid_foc_lable_center">
                                    <asp:Label ID="lblfreeQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_FreeQty %>'></asp:Label>
                                    <asp:TextBox ID="txtFocFreeQty" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return fncFTCFreeQtyEnterEvent(event);"
                                        onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="divWCP">

                            <div class="WCP-header">

                                <div class="WCP-price-detail">

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblweight" runat="server" Text='<%$ Resources:LabelCaption,lblweight %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtWeightQty" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1"
                                            onchange="fncWeightconerttoqty();"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return fncReturnfalse(event);" onFocus="this.select()"></asp:TextBox>
                                    </div>

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblRate" runat="server" Text='<%$ Resources:LabelCaption,lblRate %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtKgRate" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1"
                                            onchange="fncWeightconerttoqty();"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return fncReturnfalse(event);" onFocus="this.select()"></asp:TextBox>
                                    </div>

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblTotalValue" runat="server" Text='<%$ Resources:LabelCaption,lblTotalValue %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtTotalValue" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1" onkeydown="return fncReturnfalse(event);"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="WCP-price-detail">

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblQtyinUnits" runat="server" Text='<%$ Resources:LabelCaption,lblQtyinUnits %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtQtyinUnits" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1"
                                            onchange="fncWeightconerttoqty();"
                                            onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return fncReturnfalse(event);" onFocus="this.select()"></asp:TextBox>
                                    </div>

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblUnitCost" runat="server" Text='<%$ Resources:LabelCaption,lblUnitCost %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtUnitCost" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1" onkeydown="return fncReturnfalse(event);"></asp:TextBox>
                                    </div>

                                    <div class="Weight_Convert_PCS_1">
                                        <div class="Weight_Convert_PCS_1">
                                            <asp:Label ID="lblNetUnitCost" runat="server" Text='<%$ Resources:LabelCaption,lblTotalValue %>'></asp:Label>
                                        </div>
                                        <asp:TextBox ID="txtNetUnitCost" runat="server" Text="0.00" CssClass="form-control-res-right-WCP-1" onkeydown="return fncReturnfalse(event);"></asp:TextBox>
                                    </div>

                                </div>

                                <div class="WCP-price-detail-footer">
                                    <asp:TextBox ID="txtwcpitem" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                    <div class="control-container-wcp">

                                        <div class="gid-control grn_save_btn">
                                            <asp:LinkButton ID="lnkOkwcp" runat="server" class="button-blue"
                                                OnClientClick="fncclientclickwcpok(); return false;" Text="OK"></asp:LinkButton>
                                        </div>
                                        <div class="gid-control grn_save_btn">
                                            <asp:LinkButton ID="lnkClearwcp" runat="server" class="button-blue"
                                                OnClientClick="fncclientclickwcpclear(); return false" Text="Clear"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="divGidHold">
                            <asp:Label ID="lblGidHoldVendorName" runat="server" Text='<%$ Resources:LabelCaption,lblVendorName %>'></asp:Label>
                            <asp:TextBox ID="txtGidHold" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div id="divVatDetail">
                            <div class="Payment_fixed_headers gid_vetDetail">
                                <table id="tblVetDetail" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">GST (%)
                                            </th>
                                            <th scope="col">CESS (%)
                                            </th>
                                            <th scope="col">Taxable Pur.Value
                                            </th>
                                            <th scope="col" id="vatSummerySGST">SGST
                                            </th>
                                            <th scope="col" id="vatSummeryCGST">CGST
                                            </th>
                                            <th scope="col">CESS
                                            </th>
                                            <th scope="col">Total Pur.Value
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr class="Paymentfooter_Color">
                                            <td class="paymentfooter_total">Total
                                            </td>
                                            <td class="paymentfooter_total"></td>
                                            <td class="paymentfooter_total"></td>
                                            <td class="paymentfooter_total">
                                                <asp:Label ID="lblTotalTaxablePurValue" runat="server" Text="0.00" />
                                            </td>
                                            <td class="paymentfooter_total">
                                                <asp:Label ID="lblTotalTaxAmt1" runat="server" Text="0.00" />
                                            </td>
                                            <td id="vatSummaryCGST" class="paymentfooter_total">
                                                <asp:Label ID="lblTotalTaxAmt2" runat="server" Text="0.00" />
                                            </td>
                                            <td class="paymentfooter_total">
                                                <asp:Label ID="lblTotalCessAmt" runat="server" Text="0.00" />
                                            </td>
                                            <td class="paymentfooter_balancetotal">
                                                <asp:Label ID="lblTotalPurValue" runat="server" Text="0.00" />
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="divDeduction">
                            <div class="Payment_fixed_headers gid_Deduction">
                                <table id="tblDeduction" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Disc.Code
                                            </th>
                                            <th scope="col">Desc
                                            </th>
                                            <th scope="col">Desc(%)
                                            </th>
                                            <th scope="col">Desc Amt
                                            </th>
                                            <th scope="col">Mode
                                            </th>
                                        </tr>
                                    </thead>
                                    <asp:Repeater ID="rptrDeduction" runat="server">
                                        <HeaderTemplate>
                                            <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="DeductionBodyRow" runat="server">
                                                <td>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDiscCode" runat="server" Text='<%# Eval("DiscCode") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDiscDesc" runat="server" Text='<%# Eval("Desc") %>' />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscperc" runat="server" CssClass="grn_rptr_textbox_width form-control-res grn_edit_txt" Text='<%# Eval("DiscPer") %>'
                                                        onchange="fncDeductionAmtCalculation(this, 'Deduction', 'perc'); " onfocus="this.select();" onkeydown="return fncEnterFocusAddDed(event, this, 'Deduction', 'perc')"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="grn_rptr_textbox_width form-control-res grn_edit_txt" Text='<%# Eval("DiscAmount") %>'
                                                        onchange="fncDeductionAmtCalculation(this, 'Deduction', 'Amt'); " onfocus="this.select();" onkeydown="return fncEnterFocusAddDed(event,this, 'Deduction', 'Amt')"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMode" runat="server" Text='<%# Eval("Type") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="divAddition">
                            <div class="Payment_fixed_headers gid_Deduction">
                                <table id="tblAddition" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Add.Code
                                            </th>
                                            <th scope="col">Desc
                                            </th>
                                            <th scope="col">Add(%)
                                            </th>
                                            <th scope="col">Add Amt
                                            </th>
                                            <th scope="col">Mode
                                            </th>
                                        </tr>
                                    </thead>
                                    <asp:Repeater ID="rptrAddition" runat="server">
                                        <HeaderTemplate>
                                            <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="AdditionBodyRow" runat="server">
                                                <td>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAddCode" runat="server" Text='<%# Eval("AddCode") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAddDesc" runat="server" Text='<%# Eval("Desc") %>' />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddPerc" runat="server" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("AddPer") %>'
                                                        onchange="fncDeductionAmtCalculation(this, 'Addition', 'perc'); " onfocus="this.select();" onkeydown="return fncEnterFocusAddDed(event, this, 'Addition', 'perc');return false;"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddAmt" runat="server" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("AddAmount") %>'
                                                        onchange="fncDeductionAmtCalculation(this, 'Addition', 'Amt'); " onfocus="this.select();" onkeydown="return fncEnterFocusAddDed(event,this, 'Addition', 'Amt');return false;"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMode" runat="server" Text='<%# Eval("Type") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="divChildandParentItemlist">
                            <div>
                                <div class="gid_bulitemPrice_itemcode">
                                    <asp:Label ID="lblBulItemcode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                    <asp:TextBox ID="txtBulItemcode" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_ItemDesc">
                                    <asp:Label ID="lblBulkItemDesc" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkItemDesc" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkMRP" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkCost" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkSPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellingPrice %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkSPrice" runat="server" CssClass="form-control-res-right" Enabled="false"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulNSPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_NewSellingPrice %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkNewSPrice" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkWPrice1" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice1 %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkWPrice1" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkWPrice2" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice2 %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkWPrice2" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price">
                                    <asp:Label ID="lblBulkWPrice3" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice3 %>'></asp:Label>
                                    <asp:TextBox ID="txtBulkWPrice3" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="gid_bulitemPrice_Price gi_child_apply">
                                    <asp:LinkButton ID="lnkApply" runat="server" class="ui-button ui-corner-all ui-widget" Text='<%$ Resources:LabelCaption,lnk_apply %>'
                                        OnClientClick="fncChildItemPriceChange();return false;"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="Payment_fixed_headers gid_ParentandChild">
                                <table id="tblChildItemList" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">ItemCode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">UOM
                                            </th>
                                            <th scope="col">Weight
                                            </th>
                                            <th scope="col">New MRP
                                            </th>
                                            <th scope="col">New Cost
                                            </th>
                                            <th scope="col">New Selling
                                            </th>
                                            <th scope="col">WPrice1
                                            </th>
                                            <th scope="col">WPrice2
                                            </th>
                                            <th scope="col">WPrice3
                                            </th>
                                            <th scope="col">Prv.MRP
                                            </th>
                                            <th scope="col">Prv.Cost
                                            </th>
                                            <th scope="col">Prv.Selling
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="gid_child_item_save">
                                <asp:LinkButton ID="lnkChildItemSave" runat="server" class="ui-button ui-corner-all ui-widget" Text='<%$ Resources:LabelCaption,btnSave %>'
                                    OnClientClick="fncChildItemSave();return false;"></asp:LinkButton>
                            </div>
                        </div>
                        <div id="divBarcode">
                            <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl"
                                OnPrintButtonClientClick="return fncBarcodeValidation();" />
                        </div>
                    </div>
                    <div id="divMedicalBatch" class="display_none">
                        <div>
                            <div class="gid_medicalbatch_lable">
                                <asp:Label ID="lblMBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_MBatchNo %>'></asp:Label>
                            </div>
                            <div class="gid_medicalbatch_textbox">
                                <asp:TextBox ID="txtMBatchNo" runat="server" onkeydown=" return fncMedicalBatchEnterKey(event,'batchno');" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div>
                            <div class="gid_medicalbatch_lable">
                                <div class="float_left">
                                    <asp:Label ID="lblExpireDate" runat="server" Text='Expire Date'></asp:Label>
                                </div>
                                <div class="float_left background_red">
                                    <asp:Label ID="Label1" runat="server" Text='(mm/yyyy)'></asp:Label>
                                </div>

                            </div>
                            <div class="gid_medicalbatch_textbox">
                                <asp:TextBox ID="txtExpireDate" runat="server" CssClass="form-control-res" onkeypress="return false;" onkeydown=" return fncMedicalBatchEnterKey(event,'expiredate');"
                                    oncut="return flase;" onpaste="return false;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="gid_medicalbatch_ok">
                            <asp:LinkButton ID="lnkMBOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblOk %>'
                                OnClientClick="fncMedicalBatchOkClick();return false;"></asp:LinkButton>
                        </div>
                    </div>
                    <div id="divLastPurchaseDetail" class="display_none">
                        <div class="Payment_fixed_headers gid_LastPurchase">
                            <table id="tblLastPurchase" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Supplier
                                        </th>
                                        <th scope="col">DO/INV
                                        </th>
                                        <th scope="col">DO/INV Date
                                        </th>
                                        <th scope="col">INV.QTY
                                        </th>
                                        <th scope="col">L.QTY
                                        </th>
                                        <th scope="col">Basic Cast
                                        </th>
                                        <th scope="col">DISC
                                        </th>
                                        <th scope="col">SD
                                        </th>
                                        <th scope="col">GST
                                        </th>
                                        <th scope="col">CESS
                                        </th>
                                        <th scope="col">Netcost
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th scope="col">Selling Price
                                        </th>
                                        <th scope="col">GIDNo
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div id="divMultipleUOM" class="display_none">
                        <div class="Payment_fixed_headers multipleUOM">
                            <table id="tblMultipleUOM" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">UOMCode</th>
                                        <th scope="col">MRP</th>
                                        <th scope="col">Net Cost</th>
                                        <th scope="col">Selling(%)
                                        </th>
                                        <th scope="col">Selling Price
                                        </th>
                                        <th scope="col">W.Price1(%)
                                        </th>
                                        <th scope="col">W.Price1
                                        </th>
                                        <th id="thRMUOMWprice2Per" runat="server" scope="col">W.Price2(%)   
                                        </th>
                                        <th id="thRMUOMWprice2" runat="server" scope="col">W.Price2
                                        </th>
                                        <th id="thRMUOMWprice3Per" runat="server" scope="col">W.Price3(%)
                                        </th>
                                        <th id="thRMUOMWprice3" runat="server" scope="col">W.Price3
                                        </th>
                                        <th scope="col">UOMConv
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                        <div class="float_right">
                            <asp:LinkButton ID="lnkOk" runat="server" class="ui-button ui-corner-all ui-widget" Text='Ok'
                                OnClientClick="fncMultiUOMtoXmlformat();return false;"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="Payment_fixed_headers gid_LastPurchase display_none">
                        <table id="tblMultipleUOMSave" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">Inventorycode
                                    </th>
                                    <th scope="col">BatachNo</th>
                                    <th scope="col">UOMcode</th>
                                    <th scope="col">MRP</th>
                                    <th scope="col">SPricePer</th>
                                    <th scope="col">Sellingprice</th>
                                    <th scope="col">WPricePer1</th>
                                    <th scope="col">WPrice1</th>
                                    <th scope="col">UOMConv</th>
                                    <th id="thRMUOMWprice2Persave" runat="server" scope="col">WPricePer2   
                                    </th>
                                    <th id="thRMUOMWprice2save" runat="server" scope="col">WPrice2
                                    </th>
                                    <th id="thRMUOMWprice3Persave" runat="server" scope="col">WPricePer3
                                    </th>
                                    <th id="thRMUOMWprice3save" runat="server" scope="col">WPrice3
                                    </th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div id="divPOPendings" class="Payment_fixed_headers POPendings display_none">
                        <table id="tblPendingPO" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No</th>
                                    <th scope="col">Item Code</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">PO Qty</th>
                                    <th scope="col">GRN Qty</th>
                                    <th scope="col">Bal Qty</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                      <div id="divSellingPer" class="container2 display_none">
                          <div class="spilt" align="center">
                                        <asp:Label ID="Label4" runat="server" Text="Selling Price%"></asp:Label>
                                    </div>
                                    <div>
                          <asp:TextBox ID="txtSellPricePercentage" runat="server" CssClass="form-control-res" Style="text-align: right"
                                             onkeypress="return isNumberKeyWithDecimalNew(event)"  onkeydown="return fncCalculateSPrice(event,'Normal');"></asp:TextBox>
                                        </div>
                      </div> 
                </div>

                <div class="hiddencol">
                    <div id="dialog-Transferout">
                    </div>
                </div> 
                <div class="hiddencol">
                    <div id="divItemSearchTextiles">
                        <div class="container welltextiles" id="divFilter">
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Vendor</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtVendor" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Department</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtDepartment" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Category</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtCategory" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Class</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtClass" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Brand</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtBrand" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-3" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-sm-3 col-form-label">Item Code</label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtItemSearchT" runat="server" onkeydown="return fncReturnfalse(event);" CssClass="form-control-plaintext"></asp:TextBox>
                                </div>

                            </div>
                            <div class="form-group row col-md-5" style="margin-bottom: 5px;">
                                <asp:LinkButton ID="lnkbtClearFilter" runat="server" OnClientClick="return fncSubClearFilter();" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Clear"></asp:LinkButton>
                            </div>
                        </div>

                        <div class="container welltextiles" id="divAttribute">
                            <div class="barcode-header">
                                Attributes
                            </div>
                            <div style="height: auto; border: solid">
                                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                                    ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <Columns>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="col-md-2">
                                <asp:CheckBox ID="chkonlytextile" Text="OnlyTextile" CssClass="radioboxlistgreen" runat="server" />
                            </div>
                            <div class="col-md-9">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <asp:LinkButton ID="lnkFetchdata" runat="server" OnClientClick="fncItemSearchForGridTextiles(); return false;" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Fetch Item"></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="container welltextiles" style="padding-top: 10px;">

                            <div style="border: solid; margin-bottom: 10px;">
                                <div class="Payment_fixed_headers gid_TextileItemsearch ">
                                    <table id="tblItemDetail" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr id="Tr2" runat="server">
                                                <th scope="col">S.No
                                                </th>
                                                <th scope="col">
                                                    <input type="checkbox" id="cbSearchheader" checked="checked" value="Select" name="ChkHeader" onclick="fncrptrHeaderclick(this)" />
                                                </th>
                                                <th scope="col">Item Code
                                                </th>
                                                <th scope="col">Item Name
                                                </th> 
                                                <th scope="col">B.Cost
                                                </th>
                                                <th scope="col">G.Cost
                                                </th>
                                                <th scope="col">Net Cost
                                                </th>
                                                <th scope="col">MRP
                                                </th>
                                                <th scope="col">Margin Per
                                                </th>
                                                <th scope="col">S.Price
                                                </th>
                                                <th scope="col">UOM
                                                </th>
                                                <th scope="col">Department
                                                </th>
                                                <th scope="col">Category
                                                </th>
                                                <th scope="col">Brand
                                                </th>
                                                <th scope="col">Class
                                                </th>
                                                <th scope="col">Vendor
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div>
                                <div class="qualifyingItem_lbl_text" style="width: 3%;">
                                    <div>
                                        <asp:Label ID="lblFItemCode" runat="server" Text="SI.NO"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtVSerialNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 12%;">
                                    <div>
                                        <asp:Label ID="lblFDesc" runat="server" Text="StyleCode"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtStyleCode" runat="server" CssClass="form-control-res" MaxLength ="15"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4%;">
                                    <div>
                                        <asp:Label ID="lblFBatchNo" runat="server" Text="Quantity"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtQtyT" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 6%;">
                                    <div>
                                        <asp:Label ID="lblFQty" runat="server" Text="Cost"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtCostPriceT" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4.5%;">
                                    <div>
                                        <asp:Label ID="lblLimitQty" runat="server" Text="Disc %"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDiscountPercT" runat="server" onblur="fncTextBoxCalc('Discount')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label7" runat="server" Text="Disc Amt"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtDiscountAmtT" runat="server" onblur="fncTextBoxCalc('DiscountAmt')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4.5%;">
                                    <div>
                                        <asp:Label ID="Label8" runat="server" Text="SD %"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSDPercT" runat="server" onblur="fncTextBoxCalc('SD')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label9" runat="server" Text="SD Amt"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSDAmtT" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 6%;">
                                    <div>
                                        <asp:Label ID="Label10" runat="server" Text="G.Cost"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGrossCostT" runat="server" onblur="fncTextBoxCalc('Gross')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4.5%;">
                                    <div>
                                        <asp:Label ID="Label12" runat="server" Text="OTax %"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtOTaxPerc" runat="server" onblur="fncTextBoxCalc('OTax')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label13" runat="server" Text="OTaxAmt"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtOTaxAmt" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4.5%;">
                                    <div>
                                        <asp:Label ID="Label11" runat="server" Text="GST %"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGSTPercT" runat="server" onblur="fncTextBoxCalc('GST')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label14" runat="server" Text="GST Amt"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtGSTAmtT" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 6%;">
                                    <div>
                                        <asp:Label ID="Label15" runat="server" Text="Net Cost"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtNetCostT" runat="server" onkeydown="return fncNetCostKeyPress(event);" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 4.9%;">
                                    <div>
                                        <asp:Label ID="Label16" runat="server" Text="Margin %"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMarginT" runat="server" onblur="fncTextBoxCalc('Margin')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 6%;">
                                    <div>
                                        <asp:Label ID="Label17" runat="server" Text="MRP"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMrpT" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label18" runat="server" Text="MarginSP%"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtMarginSP" runat="server" onblur="fncTextBoxCalc('MarginSP')" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text" style="width: 6%;">
                                    <div>
                                        <asp:Label ID="Label19" runat="server" Text="S.Price"></asp:Label>
                                    </div>
                                    <div>
                                        <asp:TextBox ID="txtSellingPriceT" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="qualifyingItem_lbl_text">
                                    <div>
                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top: 5px;">
                                    <div class="col-md-3">
                                        <asp:LinkButton ID="lnkHidefilter" OnClientClick="return fncHideFilter();" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: left" Text="Hide Filter"></asp:LinkButton>
                                    </div>  
                                     <div class="col-md-5"></div>
                                    <div class="col-md-2">
                                        <div class="col-md-6">
                                            <asp:LinkButton ID="lnkCleartxt" OnClientClick="return fncSubClearTextBox();" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Clear"></asp:LinkButton>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:LinkButton ID="lnkLoadToGrid" runat="server" OnClientClick="fncSelectValidation();  return false;" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="OK"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-md-2 display_none" id="divSpDis"> 
                                        <div class="col-md-5" > 
                                            <asp:Label ID="Label5" runat="server" Text="SPDisc %"></asp:Label>
                                        </div>
                                        <div class="col-md-7" >
                                            <asp:TextBox ID="txtSPriceDis" runat="server"  CssClass="form-control-res" Style="text-align: right"
                                                 onkeypress="return isNumberKeyWithDecimalNew(event)"  onblur="return fncCalculateSPrice(event,'F1');"></asp:TextBox>
                                        </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="dialog-confirm" title="Enterpriser">
                    <p>
                        <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0px;"></span>Do you want Pending item to Create New PO?
                    </p>
                </div>

                <div class="hiddencol">
                    <div id="divMarginFixing">
                        <div style="border: solid; height: auto; margin-bottom: 10px;">
                            <div class="Payment_fixed_headers gid_MarginItem ">
                                <table id="tblMarginFix" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr id="Tr3" runat="server">
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Select                                                   
                                            </th>
                                            <th scope="col">Item Code
                                            </th>
                                            <th scope="col">Size
                                            </th>
                                            <th scope="col">Margin %
                                            </th>
                                            <th scope="col">I / D %
                                            </th>
                                            <th scope="col">MRP
                                            </th>
                                            <th scope="col">SP Margin %
                                            </th>
                                            <th scope="col">S.Price 
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-6">
                                <div class="col-md-6">
                                    <asp:Label ID="Label3" runat="server" Text="Net Cost"></asp:Label>
                                </div>
                                <div class="col-md-6">
                                    <asp:TextBox ID="txtMarginCost" runat="server" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <asp:LinkButton ID="lnkClearMarginFix" runat="server" OnClientClick="fncClearMargin(); return false;" class="ui-button ui-corner-all ui-widget" Style="float: left" Text="Clear"></asp:LinkButton>
                            </div>
                            <div class="col-md-6">
                                <asp:LinkButton ID="lnkMarginFix" runat="server" OnClientClick=" fncSelectValidationMargin(); return false;" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Select"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hiddencol">
                    <div id="divGRNTable">
                        <table id="tblGRNBulk" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">SNo</th>
                                    <th scope="col">PoNo</th>
                                    <th scope="col">ItemCode</th>
                                    <th scope="col">ItemDescription</th>
                                    <th scope="col">PoQty</th>
                                    <th scope="col">InvQty</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Foc</th>
                                    <th scope="col">MRP</th>
                                    <th scope="col">BasicCost</th>
                                    <th scope="col">SellingPrice</th>
                                    <th scope="col">Wprc1</th>
                                    <th scope="col">Wprc2</th>
                                    <th scope="col">Wprc3</th>
                                    <th scope="col">GrossBasicAmt</th>
                                    <th scope="col">Discper</th>
                                    <th scope="col">DiscAmt</th>
                                    <th scope="col">GrossDiscAmt</th>
                                    <th scope="col">SDPer</th>
                                    <th scope="col">SDAmt</th>
                                    <th scope="col">GrossShDiscAmt</th>
                                    <th scope="col">GrossCost</th>
                                    <th scope="col">TotalGrossCost</th>
                                    <th scope="col">SGSTPerc</th>
                                    <th scope="col">CGSTPerc</th>
                                    <th scope="col">CESSPerc</th>
                                    <th scope="col">SGSTAmt</th>
                                    <th scope="col">CGSTAmt</th>
                                    <th scope="col">CESSAmt</th>
                                    <th scope="col">AddCESSAmt</th>
                                    <th scope="col">GrossGSTAmt</th>
                                    <th scope="col">GrossCessAmt</th>
                                    <th scope="col">GRAddCessAmt</th>
                                    <th scope="col">NetCost</th>
                                    <th scope="col">TotalValue</th>
                                    <th scope="col">FTotalValue</th>
                                    <th scope="col">EarnedMarPerc</th>
                                    <th scope="col">EarMarAmt</th>
                                    <th scope="col">Marginfixed</th>
                                    <th scope="col">PrintQty</th>
                                    <th scope="col">ITaxt1</th>
                                    <th scope="col">ITaxt2</th>
                                    <th scope="col">MEDBATCH</th>
                                    <th scope="col">EXPDATE</th>
                                    <th scope="col">OldEarnedMar</th>
                                    <th scope="col">ProfitMargin</th>
                                    <th scope="col">FocItem</th>
                                    <th scope="col">FocBatch</th>
                                    <th scope="col">PurCharges</th>
                                    <th scope="col">FrieghtCharges</th>
                                    <th scope="col">VendorMargin</th>
                                    <th scope="col">TotalCharges</th>
                                    <th scope="col">LandingCost</th>
                                    <th scope="col">PurchaseProfitper</th>
                                    <th scope="col">NetCostAffect</th>
                                    <th scope="col">Batch</th>
                                    <th scope="col">ItemType</th>
                                    <th scope="col">PricingType</th>
                                    <th scope="col">MarkDownPerc</th>
                                    <th scope="col">OldSellingPrice</th>
                                    <th scope="col">AvgCost</th>
                                    <th scope="col">BasicSelling</th>
                                    <th scope="col">ItemBatchNo</th>
                                    <th scope="col">AllowExpireDate</th>
                                    <th scope="col">FTCFromDate</th>
                                    <th scope="col">FTCToDate</th>
                                    <th scope="col">BuyQty</th>
                                    <th scope="col">FreeQty</th>
                                    <th scope="col">GridRowStatus</th>
                                    <th scope="col">SerialNo</th>
                                    <th scope="col">StyleCode</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
