﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseOrderPlanning.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrderPlanning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        /**************************** GridView Fixed header ****************************/

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 330px;
            max-width: 330px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 90px;
            max-width: 90px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }


        /**************************** Item History ****************************/

        .Itemhistory td:nth-child(1), .Itemhistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .Itemhistory td:nth-child(2), .Itemhistory th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(2) {
            text-align: right !important;
        }

        .Itemhistory td:nth-child(3), .Itemhistory th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(3) {
            text-align: right !important;
        }

        .Itemhistory td:nth-child(4), .Itemhistory th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(4) {
            text-align: right !important;
        }

        .Itemhistory td:nth-child(5), .Itemhistory th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(5) {
            text-align: right !important;
        }

        .Itemhistory td:nth-child(6), .Itemhistory th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(6) {
            text-align: right !important;
        }

        .Itemhistory td:nth-child(7), .Itemhistory th:nth-child(7) {
            min-width: 60px;
            max-width: 60px;
        }

        .Itemhistory td:nth-child(7) {
            text-align: left !important;
        }
    </style>

    <style type="text/css">
        .textboxOrder {
            background-color: transparent;
            font-size: 13px;
            width: 100%;
            height: 100%;
            font-weight: bold;
            text-align: right;
            padding-right: 5px;
        }

        .left_align {
            padding-left: 5px;
        }

        .right_align {
            text-align: right;
            padding-right: 5px;
        }

        .textbox-right-align {
            text-align: right;
        }

        /********** For Accordion **********/
        .ui-accordion .ui-accordion-content {
            padding: 0px 10px !important;
        }

        .ui-accordion .ui-accordion-header {
            font-weight: bold !important;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'PurchaseOrderPlanning');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "PurchaseOrderPlanning";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        function pageLoad() {

            fncDecimal();

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            // Set Width
            $("#<%=lstLocation.ClientID %>_chzn").css("width", '100%');

            // Accordion
            $(function () {
                var icons = {
                    header: "ui-icon-circle-arrow-e",
                    activeHeader: "ui-icon-circle-arrow-s"
                };
                $("#accordion").accordion({
                    collapsible: true,
                    icons: icons
                });
                $("#toggle").button().on("click", function () {
                    if ($("#accordion").accordion("option", "icons")) {
                        $("#accordion").accordion("option", "icons", null);
                    } else {
                        $("#accordion").accordion("option", "icons", icons);
                    }
                });
            });
        }

        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function disableFunctionKeys(e) { 
            try {
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
              catch (err) {
                    fncToastError(err.message);
                }
            }
        function fncDecimal() {
            try {
                $('#<%=txtQty.ClientID%>').number(true, 2);
                $('#<%=txtSPrice.ClientID%>').number(true, 2);
                $('#<%=txtQtyIncreasedByPrc.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function clearForm() {
            try {
                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtItemName.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtFloor.ClientID %>').val('');
                $('#<%= txtShelf.ClientID %>').val('');
                $('#<%= lstLocation.ClientID %>').val('');
                $('#<%= txtClass.ClientID %>').val('');
                $("select").trigger("liszt:updated");
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
            return false;
        }

        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%").height(200);
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncOpenItemHistory() {
            $("#dialog-History").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source, curcell) {
            var verHight;
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    fncSetFocustoObject(NextRowobj.find('td input[id*="' + curcell + '"]'));
                    return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncSetFocustoObject(prevrowobj.find('td input[id*="' + curcell + '"]'));
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        // Open Inventory Master
        function fncPurchasePlanningRowdblClk(rowObj) {
            try {
                rowObj = $(rowObj);
                fncOpenItemhistory($.trim($("td", rowObj).eq(0).text()));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        // Open Item History
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 630,
                width: 1200,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        // Sales History
        function fncGetSalesHistory(source) {
            var itemcode;
            try {
                rowObj = $(source).closest("tr");
                itemcode = $("td", rowObj).eq(0).text().replace(/&nbsp;/g, '');
                fncBindSalesHistory(itemcode);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        // Bind Sales history to table
        function fncBindSalesHistory(itemcode) {
            debugger;
            var obj = {};
            try {
                obj.itemcode = itemcode;
                $.ajax({
                    type: "POST",
                    url: "frmAutoPO.aspx/fncGetSalesHistory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var tblLocSalesHis;
                        tblLocSalesHis = $('#tblItemhistory tbody');
                        tblLocSalesHis.children().remove();
                        var objItem = jQuery.parseJSON(msg.d);
                        if (objItem.length > 0) {

                            for (var i = 0; i < objItem.length; i++) {
                                tblLocSalesHis.append("<tr>"
                                    + "<td>" + objItem[i]["MNName"].substring(0, 3) + "</td>"
                                    + "<td >" + objItem[i]["Week1"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week2"].toFixed(2) + "</td>"
                                    + "<td >" + objItem[i]["Week3"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week4"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week5"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["TotalQty"].toFixed(2) + "</td>"
                                    + "</tr>");
                            }
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        // Change Qty Increased By Prc
        function fncChangeQtyIncreasedByPrc() {
            var Prc = $('#<%=txtQtyIncreasedByPrc.ClientID %>').val();
            $("#<%=gvItemDetails.ClientID%> tr:has(td)").each(function () {
                var cells = $("td", this);
                var SuggestQty = cells.eq(2).text();
                var OrderQtyInc = (parseFloat(SuggestQty) * parseFloat(Prc)) / 100;
                var OrderQty = parseFloat(SuggestQty) + parseFloat(OrderQtyInc);
                cells.eq(3).children(":text").val(Math.round(OrderQty).toFixed(2));
            });
            return false;
        }
    </script>

    <style type="text/css">
        input:checked + label {
            color: white;
            background: red;
        }
    </style>
    <script type="text/javascript">
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <script type="text/javascript">

        <%------------------------------------------- Validation -------------------------------------------%>

        function fncAddValidation() {

            var Show = '';
            if (document.getElementById("<%=txtPlanningItemNameAdd.ClientID%>").value == "") {
                Show = Show + '\n  Please Enter Plan Inv Name';
                document.getElementById("<%=txtPlanningItemNameAdd.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtOrderQty.ClientID%>").value == "") {
                Show = Show + '\n  Please Enter Order Qty';
                document.getElementById("<%=txtOrderQty.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                return true;
            }
        }

        function fncSaveValidation() {

            var Show = '';
            if (document.getElementById("<%=txtPlanCode.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Plan';
                document.getElementById("<%=txtPlanCode.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                return true;
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Purchase Order Planning </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div id="accordion">
                            <h1>Filtration</h1>
                            <div>
                                <div class="control-group-single-res" style="margin-top: 10px">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocation" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Class"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtFromDate');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="SubCategory"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Merchendise"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtMerchendise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchendise', 'txtManufacture');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label17" runat="server" Text="Manufature"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label20" runat="server" Text="Floor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Shelf"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemName');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="Item Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemName', 'txtItemCode');">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label16" runat="server" Text="Item Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtClass');">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="lblFromDate" runat="server" Text="FromDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="lblToDate" runat="server" Text="ToDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res col-md-12">
                                    <div class="col-md-5">
                                        <div class="label-left">
                                            <asp:Label ID="lblQty" runat="server" Text="Qty < "></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="label-left">
                                            <asp:Label ID="lblSPrice" runat="server" Text="S.Price <= "></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3>Ordering Methods</h3>
                            <div>
                                <div class="control-group-single-res" style="margin-top: 10px">
                                    <asp:RadioButton ID="RdoMinMaxQty" runat="server" GroupName="priceMenu" Checked="true" Text="Min & Max Quantity Level" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res">
                                    <asp:RadioButton ID="RdoPreSupQty" runat="server" GroupName="priceMenu" Text="Previous Supply Wise" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res" style="display: none">
                                    <asp:RadioButton ID="RdoWQty" runat="server" GroupName="priceMenu" Text="Min & Max W-Quantity" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res">
                                    <asp:RadioButton ID="RdoSQtyLasPur" runat="server" GroupName="priceMenu" Text="Sold Qty in Last Purchase" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res">
                                    <asp:RadioButton ID="RdoSQtyDate" runat="server" GroupName="priceMenu" Text="Sold Qty in Date Range" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res">
                                    <asp:RadioButton ID="RdoCycDate" runat="server" GroupName="priceMenu" Visible="true" Text="OrderBy Re.Qty / Cyc.Dates" Class="radioboxlist" />
                                </div>
                                <div class="control-group-single-res">
                                    <div style="float: left">
                                        <asp:RadioButton ID="RdoReorder" runat="server" GroupName="priceMenu" Visible="true" Text="ReOrder By No.of Days" Class="radioboxlist" />
                                    </div>
                                    <div style="float: right">
                                        <asp:Label ID="lblNoOfDays" runat="server" Text="No.of Days"></asp:Label>
                                        <asp:TextBox ID="txtNoOfDays" runat="server" CssClass="form-control-res" Width="50px"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <asp:RadioButton ID="rdoBaseInventory" runat="server" GroupName="priceMenu" Visible="true" Text="Based On Inventory" Class="radioboxlist" />
                                </div>
                                 
                            <div class="control-group-single-res">
                                <asp:CheckBox ID="RdoAllowZero" runat="server" GroupName="allMenu" Text="Allow Zero Qty" Class="radioboxlistgreen" />
                            </div> 
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" OnClick="lnkLoadFilter_Click"><i class="icon-play" ></i>Load Data</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick=" return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                        <div class="GridDetails">

                            <table rules="all" id="tblHead" runat="server" class="grdLoad fixed_header" style="display: block">
                                <tr>
                                    <td style="text-align: center">ItemCode</td>
                                    <td style="text-align: center">Item Name</td>
                                    <td style="text-align: center">Suggest Qty</td>
                                    <td style="text-align: center">OrderQty</td>
                                    <td style="text-align: center">QtyInHand</td>
                                    <td style="text-align: center">MRP</td>
                                    <td style="text-align: center">SellingPrice</td>
                                    <td style="text-align: center">Cost</td>
                                    <td style="text-align: center">NetCost</td>
                                </tr>
                            </table>

                            <div class="grid-overflow gridAlign" id="HideFilter_GridOverFlow" runat="server" style="height: 352px">
                                <asp:GridView ID="gvItemDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True" ShowHeader="false"
                                    CssClass="grdLoad" OnRowDataBound="gvItemDetails_RowDataBound">
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <Columns>
                                        <asp:BoundField DataField="InventoryCode" HeaderText="Item Code" ItemStyle-CssClass="left_align"></asp:BoundField>
                                        <asp:BoundField DataField="Description" HeaderText="Item Name" ItemStyle-CssClass="left_align"></asp:BoundField>
                                        <asp:BoundField DataField="SuggestQty" HeaderText="Suggest Qty" ItemStyle-CssClass="right_align" />
                                        <asp:TemplateField HeaderText="Order Qty" ItemStyle-Width="100">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtOrderQty" runat="server" Text='<%# Eval("SuggestQty") %>' onkeydown=" return fncSetFocustoNextRow(event,this,'txtOrderQty');"
                                                    onkeypress="return isNumberKey(event)" CssClass="textboxOrder" onfocusin="return fncGetSalesHistory(this);"></asp:TextBox>
                                            </ItemTemplate>
                                            <ControlStyle Height="100%" />
                                            <ItemStyle Height="100%"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="QtyonHand" HeaderText="QtyInHand" ItemStyle-CssClass="right_align"></asp:BoundField>
                                        <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="right_align"></asp:BoundField>
                                        <asp:BoundField DataField="SellingPrice" HeaderText="SellingPrice" ItemStyle-CssClass="right_align"></asp:BoundField>
                                        <asp:BoundField DataField="CurrPrice" HeaderText="Cost" ItemStyle-CssClass="right_align"></asp:BoundField>
                                        <asp:BoundField DataField="NetCost" HeaderText="NetCost" ItemStyle-CssClass="right_align"></asp:BoundField>
                                        <%--<asp:BoundField DataField="POQty" HeaderText="PO.Qty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField DataField="PurDate" HeaderText="Purchase Date" ItemStyle-CssClass="right_align"></asp:BoundField>                                                                              
                                                <asp:BoundField DataField="PurQty" HeaderText="Purchase Qty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                 <asp:BoundField DataField="SoldQty" HeaderText="Sold Qty" ItemStyle-CssClass="right_align"></asp:BoundField>--%>
                                        <asp:BoundField DataField="VendorCode" HeaderText="VendorCode" HeaderStyle-CssClass="hiddencol"
                                            ItemStyle-CssClass="hiddencol"></asp:BoundField>
                                        <asp:BoundField DataField="SortNo" HeaderText="SortNo" HeaderStyle-CssClass="hiddencol"
                                            ItemStyle-CssClass="hiddencol"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                </asp:GridView>
                            </div>
                        </div>

                        <div class="col-md-12" style="border: 1px solid black; padding: 5px">
                            <div class="col-md-5">
                                <asp:Label ID="Label7" runat="server" Text="Planning Item Name"></asp:Label>
                                <asp:TextBox ID="txtPlanningItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="Label9" runat="server" Text="Deparment Name"></asp:Label>
                                <asp:TextBox ID="txtDepartmentAdd" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartmentAdd', 'txtOrderQty');"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:Label ID="Label13" runat="server" Text="Order Qty"></asp:Label>
                                <asp:TextBox ID="txtOrderQty" runat="server" CssClass="form-control-res textbox-right-align"></asp:TextBox>
                            </div>
                            <div style="margin-top: 14px">
                                <div class="col-md-2">
                                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text="Add" OnClientClick="return fncAddValidation();" OnClick="lnkAdd_Click"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkClearText" runat="server" Text="Clear" class="button-blue" OnClientClick="fncClearEntervalue();return false;"></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4" style="float: left; margin-top: 5px">
                            <div class="col-md-4">
                                <asp:Label ID="Label15" runat="server" Text="Plan Name"></asp:Label>
                            </div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtPlanCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PurchasePlan', 'txtPlanCode', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3" style="float: left; margin-top: 5px">
                            <div class="col-md-6">
                                <asp:Label ID="Label10" runat="server" Text="Qty Increase by %"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtQtyIncreasedByPrc" runat="server" CssClass="form-control-res" onchange="return fncChangeQtyIncreasedByPrc();" Style="text-align: right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div style="float: right">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick="return fncSaveValidation();" OnClick="lnkUpdate_Click">Create Purchase Planning</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClick="lnkClearAll_Click">Clear</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="Payment_fixed_headers Itemhistory PO_Itemhistory col-md-6" style="width: 420px;">
                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">Month
                                    </th>
                                    <th scope="col">1st Week
                                    </th>
                                    <th scope="col">2st Week
                                    </th>
                                    <th scope="col">3st Week
                                    </th>
                                    <th scope="col">4st Week
                                    </th>
                                    <th scope="col">5st Week
                                    </th>
                                    <th scope="col">Total
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
