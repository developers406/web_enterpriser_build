﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmGoodsAcknowledgement.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmGoodsAcknowledgement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4) {
            min-width: 40px;
            max-width: 40px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5) {
            min-width: 50px;
            max-width: 50px;
        }

        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9) {
            min-width: 350px;
            max-width: 350px;
        }

        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
        }

        .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(11) {
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(12) {
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(13) {
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(15), .Barcode_fixed_headers th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(16), .Barcode_fixed_headers th:nth-child(16) {
            min-width: 110px;
            max-width: 110px;
        }

        .Barcode_fixed_headers td:nth-child(17), .Barcode_fixed_headers th:nth-child(17) {
            min-width: 110px;
            max-width: 110px;
        }

        .Barcode_fixed_headers td:nth-child(18), .Barcode_fixed_headers th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(19), .Barcode_fixed_headers th:nth-child(19) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(20), .Barcode_fixed_headers th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GoodsAcknowledgement');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GoodsAcknowledgement";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">
        shortcut.add("Ctrl+A", function () {
            $('#breadcrumbs_text').removeClass('lowercase');
            $('#breadcrumbs_text').addClass('uppercase');
            $("#<%=hidBillType.ClientID%>").val('A');
        });
        shortcut.add("Ctrl+B", function () {
            $('#breadcrumbs_text').removeClass('uppercase');
            $('#breadcrumbs_text').addClass('lowercase');
            $("#<%=hidBillType.ClientID%>").val('B');
        });
        var rowDeleteobj;
        function pageLoad() {
            <%-- if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkAdd.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkAdd.ClientID %>').css("display", "none");
             }--%>

            if ($("#<%= hidUserNameFilter.ClientID %>").val() == "Y") {
                $('#divUserNameText').show();
                $('#divUserName').show();
            }
            else {
                $('#divUserNameText').hide();
                $('#divUserName').hide();
            }
            fncSearchGidNumber();
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            showhidecontrol();
            //$("#<%=hidgaNo.ClientID%>").val('');
            $("#<%=hidVendorcode.ClientID%>").val('');
            //$("#<%=hidGid.ClientID%>").val('');
        }

        $(function () {
            fncSearchGidNumber();
        });
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            //var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
            //$("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
        });
        //Show and Hide Data,pending GRN
        function showhidecontrol() {
            try {
                if ($("#<%=rbnGrnpending.ClientID %>").is(":checked") || $("#<%=rbnHold.ClientID %>").is(":checked")
                    || $("#<%=rbnDis.ClientID %>").is(":checked")) {
                    $('#divgaholdunhold').hide();
                    $('#divdate').hide();
                    $('#divGidNo').hide();
                    $("#<%=lnkGRNHoldPrint.ClientID %>").css("display", "block");
                    $("#<%=lnkgaprint.ClientID %>").css("display", "block");
                    $("#<%=lnkgrnprint.ClientID %>").css("display", "none");
                    $("#<%=lnkMemoPrint.ClientID %>").css("display", "none");
                    $("#<%=lnkGRNFOC.ClientID %>").css("display", "none");
                    $("#<%=lnkGRNAddDed.ClientID %>").css("display", "none");
                    $("#<%=txtWarehouse.ClientID %>").prop("disabled", true);

                }
                else {
                    $('#divgaholdunhold').hide();
                    $('#divdate').show();
                    $('#divGidNo').show();
                    $("#<%=lnkgrnprint.ClientID %>").css("display", "block");
                    $("#<%=lnkMemoPrint.ClientID %>").css("display", "block");
                    $("#<%=lnkGRNFOC.ClientID %>").css("display", "block");
                    $("#<%=lnkGRNAddDed.ClientID %>").css("display", "block");

                   <%-- $("#<%=lnkgaprint.ClientID %>").css("display", "none");--%>
                    $("#<%=lnkGRNHoldPrint.ClientID %>").css("display", "none");

                    $("#<%=txtWarehouse.ClientID %>").prop("disabled", false);
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowDiscontinuEntry() {
            $(function () {
                $("#dialog-DiscontinueEntry").html('<%=Resources.LabelCaption.lbl_loadDiscontinueEntry%>');
                $("#dialog-DiscontinueEntry").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            $("#<%=btnGrnGo.ClientID%>").click();
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                            $("#<%=hidGidMode.ClientID%>").val('New');
                            $("#<%=btnGrnGo.ClientID%>").click();
                        }
                    },
                    modal: true
                });
            });
        };

        //Open Delete Dialoag
        function fncGADleteDialog() {
            try {
                $("#deleteGA").dialog({
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Close Delete Dialog
        function fncCloseDeleteDialog() {
            $("#deleteGA").dialog('close');
        }

        //Open GidHold Dialog
        function fncGidHoldDialogInitialize() {
            try {
                $("#divGidHold").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //$(function () {
        //    fncViewClosedGRN
        //});

        function fncViewClosedGRN(source) {
            try {
                var obj;
                obj = $(source).parent().parent();

                fncRowClick(obj);

                $("#<%=hidGidMode.ClientID%>").val('View');

                var page = '';
                if ($("#<%=hidTextileBrand.ClientID%>").val() == 'Y' && $("#<%=hidTextileMode.ClientID%>").val() == 'N1') {
                    page = '<%=ResolveUrl("~/Purchase/frmTextileBarcodePrint.aspx") %>';
                }
                else if ($("#<%=hidTextileLoc.ClientID%>").val() == 'Y' && $("#<%=hidTextileMode.ClientID%>").val() == 'N1') {
                    page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNoteTextiles.aspx") %>';
                }
                else {
                    page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                }
                var page = page + "?GRNNo=" + $("#<%=hidGid.ClientID%>").val() + "&GANo=" + $("#<%=hidgaNo.ClientID%>").val();
                var page = page + "&BillNo=" + $("#<%=hidBillNo.ClientID%>").val() + "&VendorCode=" + $("#<%=hidVendorcode.ClientID%>").val();
                var page = page + "&Status=" + $("#<%=hidGidMode.ClientID%>").val() + "&BillType=" + $("#<%=hidBillType.ClientID%>").val();
                var page = page + "&LoadPoQty=N" + "&locationcode=" + $("#<%=hidLocation.ClientID%>").val();
                var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "VIEW GRN MAINTENANCE",
                    buttons: [
                        {
                            text: "Close",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });

                //alert(page);
                $dialog.dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var btnValue = '';
        function fncGADeleteAndEdit_New(source, btnvalue) { 
            rowDeleteobj = $(source);
            btnValue = btnvalue;
            if (btnValue == 'Delete')
            fncConfirmDeleteGA_Alert('A');
        }

        //Delete ga from table
        function fncGADeleteAndEdit(source, btnvalue) {
            try {
                if (btnvalue == "Delete") {
                    if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                        if ($("#<%=rbnGrnClosed.ClientID %>").is(":checked"))
                            ShowPopupMessageBox("You have no permission to Delete this GRN");
                        else
                            ShowPopupMessageBox("You have no permission to Delete this GA");
                        return false;
                    }
                }
                else if (btnvalue == "Edit") {
                    if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                        ShowPopupMessageBox("You have no permission to Edit this GA");
                        return false;
                    }
                }

                rowDeleteobj = $(source).parent().parent();
                fncGADeleteAndEditNew(rowDeleteobj, btnvalue);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGADeleteAndEditNew(source, btnvalue) {
            var gidNo;
            var obj = {};
            try {
                $("#<%=hidBillType.ClientID%>").val($.trim(rowDeleteobj.find('td span[id*="lblBilltype"]').text()));
                rowDeleteobj = source;
                obj.billNo = $.trim(rowDeleteobj.find('td span[id*="lblBillNo"]').text());
                obj.vendorCode = $.trim(rowDeleteobj.find('td span[id*="lblVendorcode"]').text());
                obj.billType = $.trim(rowDeleteobj.find('td span[id*="lblBilltype"]').text());
                obj.gaNo = $.trim(rowDeleteobj.find('td span[id*="lblGANo"]').text());
                obj.Mode = btnvalue;
                gidNo = $.trim(rowDeleteobj.find('td span[id*="lblGidNo"]').text());
                if (gidNo != "" && btnvalue == "Delete") {
                    $("#<%=hidGid.ClientID %>").val(gidNo);
                fncShowSaveConfirmation_master('<%=Resources.LabelCaption.msg_GRNDelete%>' + gidNo + " ?.");
                    return;
                }

                $("#<%=hidBillNo.ClientID %>").val(obj.billNo);
                $.ajax({
                    type: "POST",
                    url: "frmGoodsAcknowledgement.aspx/fncGetGADeleteStatus",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (btnvalue == "Delete")
                            ProcessGADelete(msg);
                        else if (btnvalue == "Edit")
                            ProcessGAEdit(msg);
                        else
                            fncGrnGoClick(source, msg);


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncYesClick_master() {
            try {
                $("#saveConfirmation_master").dialog("destroy"); 
                fncGRNDelete($("#<%=hidGid.ClientID %>").val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function ProcessGAEdit(msg) {
            try {
                if (msg.d.indexOf("Hold") != -1) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alter_gaEditHold%>');
                }
                else if (msg.d != "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_gaAlreadyProcess%>' + msg.d);
                }
                else {
                    $("#<%=btnEdit.ClientID %>").click();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function ProcessGADelete(msg) {
            try {
                if (msg.d.indexOf("Hold") != -1) {
                    $("#<%=lblGaDelete.ClientID %>").text('<%=Resources.LabelCaption.msg_GAhold%>');
                    fncGADleteDialog();
                }
                else if (msg.d != "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_gaAlreadyProcess%>' + msg.d);
                    return false;
                }
                else {
                    $("#<%=lblGaDelete.ClientID %>").text('<%=Resources.LabelCaption.msg_deletega%>' + $("#<%=hidBillNo.ClientID %>").val());
                    fncGADleteDialog();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                //console.log(err.message);
            }
        }

        var deleteflag = 'N';
        function fncConfirmDeleteGA_Alert(value) {
            try {
                deleteflag = value;
                $('#lblCountPrint').css('display', 'none');
                fncCheckPrintLog();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncConfirmDeleteGA() {
            try { 
                var obj = {};
                obj.billNo = $.trim(rowDeleteobj.find('td span[id*="lblBillNo"]').text());
                obj.billDate = $.trim(rowDeleteobj.find('td span[id*="lblBillDate"]').text());
                obj.gaNo = $.trim(rowDeleteobj.find('td span[id*="lblGANo"]').text());
                //obj.grnNo = $.trim(rowDeleteobj.find('td span[id*="lblGidNo"]').html());
                obj.billType = $("#<%=hidBillType.ClientID%>").val();
                obj.grnNo = $.trim(rowDeleteobj.find('td span[id*="lblVendorcode"]').text()) + "-" + obj.billNo;


                $.ajax({
                    type: "POST",
                    url: "frmGoodsAcknowledgement.aspx/fncDeleteGA",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "Success") {
                            var rowParentObj = rowDeleteobj.parent();
                            rowDeleteobj.remove();
                            rowParentObj.children().each(function (index) {
                                $(this).find('td span[id*=lblSNo]').html(index + 1);
                            });
                            rowDeleteobj = null;
                            fncCloseDeleteDialog();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        //console.log(data);
                        return false;
                    }
                });



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }
        ///Get value for GA and GRN Report
        var PrintCount = 0;
        function fncRowClick(source) {
            try {

                $("#<%=hidgaNo.ClientID%>").val($.trim($(source).find('td span[id*="lblGANo"]').text()));
                $("#<%=hidgaDate.ClientID%>").val($.trim($(source).find('td span[id*="lblGADate"]').text()));
                $("#<%=hidgaAmt.ClientID%>").val($.trim($(source).find('td span[id*="lblNetAmount"]').text()));
                $("#<%=hidVendorcode.ClientID%>").val($.trim($(source).find('td span[id*="lblVendorcode"]').text()));
                $("#<%=hidGid.ClientID%>").val($.trim($(source).find('td span[id*="lblGidNo"]').text()));
                $("#<%=hidBillNo.ClientID %>").val($.trim($(source).find('td span[id*="lblBillNo"]').text()));
                $("#<%=hidLocation.ClientID%>").val($.trim($(source).find('td span[id*="lblLocationCode"]').text()));
                PrintCount = $.trim($(source).find('td span[id*="lblPrintCount"]').text());
                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "aliceblue");
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //New GRN Entry
        function fncGrnGoClick(source, msg) {
            try {
                var holdGidNo;
                var rowObj = source;//$(source).parent().parent();
                $("#<%=hidLocation.ClientID%>").val($.trim(rowObj.find('td span[id*="lblLocationCode"]').html()));
                $("#<%=hidBillNo.ClientID %>").val($.trim(rowObj.find('td span[id*="lblBillNo"]').html()));
                holdGidNo = $.trim(rowObj.find('td span[id*="lblBillNo"]').text()) + "-" + $.trim(rowObj.find('td span[id*="lblVendorcode"]').text());
                var Split = msg.d.split("||");

                if ($.trim(Split[1]) == $("#<%=hidComputerName.ClientID%>").val().trim()) {
                    $("#<%=hidGidMode.ClientID%>").val('New');
            $("#<%=btnGrnGo.ClientID%>").click();
        }
        else if ($.trim(Split[1]) == "Hold") {
            $("#<%=lblGidHold.ClientID%>").text("This Invoice (" + holdGidNo + ") is on Hold.Do you want to open?");<%--'<%=Resources.LabelCaption.alert_Invoicehold%>'--%>
            fncGidHoldDialogInitialize();
        }
        else if ($.trim(Split[1]) == "PDA") {
            $("#<%=hidGidMode.ClientID%>").val('PDA');
            $("#<%=btnGrnGo.ClientID%>").click();
        }
        else if ($.trim(Split[1]) != "" && $.trim(Split[1]) != $("#<%=hidComputerName.ClientID%>").val().trim()) {
            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_alreadyProcess%>' + 'another computer');
        }

        else if (Split[0].indexOf("JSONFiles") != -1) {/// check Contain Operation
            $("#<%=hidDiscontinueEntry.ClientID %>").val($.trim(Split[0]));
            $("#<%=hidGidMode.ClientID%>").val('DiscontinueEntry');
            fncShowDiscontinuEntry();
        }

        else if (msg.d == "Inter") {
            $("#<%=hidGidMode.ClientID%>").val('Inter');
            $("#<%=btnGrnGo.ClientID%>").click();
        }
            <%--  else if (msg.d == "") {
            $("#<%=hidGidMode.ClientID%>").val('New');
                 $("#<%=btnGrnGo.ClientID%>").click();
             }--%>
        else {
            <%--ShowPopupMessageBox('<%=Resources.LabelCaption.msg_alreadyProcess%>' + msg.d);--%>
            <%--ShowPopupMessageBox('<%=Resources.LabelCaption.msg_alreadyProcess%>' + 'another computer');--%>
            $("#<%=hidGidMode.ClientID%>").val('New');
            $("#<%=btnGrnGo.ClientID%>").click();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncConfirmToOpenHoldInvoice(value) {
            try {
                if (value == "Yes") {
                    $("#<%=hidGidMode.ClientID%>").val('Hold');
                    $("#<%=btnGrnGo.ClientID%>").click();
                }
                else
                    $("#divGidHold").dialog('close');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Report Validation
        function fncReportValidation(Mode) {
            try {
                if ($("#tblga tbody").children().length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false;
                }
                else if ($("#<%=hidgaNo.ClientID%>").val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_anyOneRow%>');
                    return false;
                }
                else if (Mode == 'GRNPrint' && parseFloat(PrintCount) > 0 && $("#<%=hidGRNPrintCount.ClientID%>").val() == "Y") {
                    $("#<%=lblCountPrint.ClientID%>").text('This Invoice Already Printed.Print Count = ' + PrintCount + '.');
                    fncCheckPrintLog();
                    return false;
                }
              

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///GRN Delete
        function fncGRNDelete(gidNo) {
            try {
                $.ajax({
                    type: "POST",
                    url: "frmGoodsAcknowledgement.aspx/fncGRNDelete",
                    data: "{ 'GidNo': '" + gidNo + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "TransferOut") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_GidTransferOut%>');
                        }
                        else if (msg.d == "SalesPromotion") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_SalesPromotion%>');
                        }
                        else if (msg.d == "InterPurchase") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_gidInterpurchase%>');
                        }
                        else if (msg.d == "ACCFlag") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_gidPosted%>');
                        }
                        else if (msg.d == "Success") {
                            fncConfirmGRNDelete('Yes');
                       <%-- $("#<%=lblGRNDelete.ClientID%>").text('<%=Resources.LabelCaption.msg_GRNDelete%>' + gidNo + " ?.");
                        fncShowDeletedGRN();--%>
                        }
                        else {
                            ShowPopupMessageBox(msg.d);
                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        //console.log(data);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Show Deleted GRN
        function fncShowDeletedGRN() {
            try {
                $("#divGRNDelete").dialog({
                    resizable: false,
                    height: 130,
                    width: 240,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// GRN Delete Confirmation
        function fncConfirmGRNDelete(value) {
            try {
                if (value == "Yes") {
                    var rowParentObj = rowDeleteobj.parent();
                    rowDeleteobj.remove();
                    rowParentObj.children().each(function (index) {
                        $(this).find('td span[id*=lblSNo]').html(index + 1);
                    });
                    rowDeleteobj = null;
                    //$("#divGRNDelete").dialog('close');
                }
                //else {
                //    //$("#divGRNDelete").dialog('close');
                //}
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        ///Gid Number Search 
        function fncSearchGidNumber() {
            $("[id$=txtGidNo]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetGidNumber")%>',
                        data: "{ 'searchtext': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1]

                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));
            $("#<%=btngidno.ClientID %>").click();
                    return false;
                },
                minLength: 1
            });
        }

        /// GA Row Click
        function fncGARowKeydown(evt, source) {
            var rowobj, charCode, scrollheight;
            try {
                rowobj = $(source);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    return false;
                }
                else if (charCode == 113) {
                    fncGADeleteAndEditNew(rowobj, "Edit");
                }
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    fncRowClick(NextRowobj)
                    if (NextRowobj.length > 0) {
                        //NextRowobj.css("background-color", "#80b3ff");
                        //NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblga tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td span[id*="lblSNo"]').text()) < 3) {
                                $("#tblga tbody").scrollTop(0);
                            }
                            //else if (parseFloat(scrollheight) > 10) {
                            //    scrollheight = parseFloat(scrollheight) - 10;
                            //    $("#tblga tbody").scrollTop(scrollheight);
                            //}
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncRowClick(prevrowobj);
                    if (prevrowobj.length > 0) {
                        //prevrowobj.css("background-color", "#80b3ff");
                        //prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblga tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblga tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            //$("#tblga tbody > tr").first().css("background-color", "#80b3ff");
            $("#tblga tbody > tr").first().focus();
            fncRowClick($("#tblga tbody > tr").first());
        });
        function fncSetValue() {
            try {
                if (SearchTableName == "User") {
                    $("#<%=txtUserSearch.ClientID %>").val($.trim(Description));
                    $("#<%=hidUserCode.ClientID %>").val($.trim(Code));
                }
                if (SearchTableName == "GidNo") {
                    $("#<%=btngidno.ClientID %>").click();
                }
                else
                    __doPostBack('ctl00$ContentPlaceHolder1$LinkRefresh', '');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSearchDialogVendor(event, Vendor, txtVendor, Next) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            // alert(charCode);
            if (charCode == 13) {
                return false;
            }
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Vendor, txtVendor, Next);
                event.preventDefault();
            }
            else {
                fncShowSearchDialogCommon(event, Vendor, txtVendor, Next);
            }
        }

        function fncClear() {
            $("#<%=rbnGrnpending.ClientID %>").attr('checked', 'checked');
            return true;
        }

        function fncCheckPrintLog() {
            try {
                $("#adminauthentication").dialog({ 
                    height: 120,
                    width: "auto",
                    modal: true
                });
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //To Open Authentication Dialog
        function fncAdminAuthentication(evt) {
            try {

                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {

                    var obj = {};
                    obj.password = $("#<%=txtPassword.ClientID %>").val();


                    if (obj.password == "") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Enterpassword%>');
                        return;
                    }

                    $.ajax({
                        type: "POST",
                        url: "frmGoodsAcknowledgement.aspx/fncPasswordAuthantication",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {

                            if (msg.d == "InvalidPassword") {
                                ShowPopupMessageBox('<%=Resources.LabelCaption.msgInvalidPassword%>');
                            }
                            else if (msg.d == "Success") {
                                fncAuthenticationDialogClose();
                            }

                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAuthenticationDialogClose() {
            try {
                $("#adminauthentication").dialog('close');
                $("#<%=txtPassword.ClientID %>").val('');
                if (deleteflag == 'N')
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkgrnprint', '');
                else if (deleteflag == 'B')
                    fncConfirmDeleteGA();
                else
                    fncGADeleteAndEdit(rowDeleteobj,btnValue);
                //deleteflag = 'N';
                //rowDeleteobj = '';
                //btnValue = '';

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClick() {
            try {
                if ($("#<%=hidTextileBrand.ClientID%>").val() == 'Y') {
                    page = '<%=ResolveUrl("~/Purchase/frmTextileBarcodePrint.aspx") %>';
                  }
                  else if ($("#<%=hidTextileLoc.ClientID%>").val() == 'Y') {
                      page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNoteTextiles.aspx") %>';
                  }
                  else {
                      page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                }
                var page = page + "?GRNNo=" + $("#<%=hidGid.ClientID%>").val() + "&GANo=" + $("#<%=hidgaNo.ClientID%>").val();
                var page = page + "&BillNo=" + $("#<%=hidBillNo.ClientID%>").val() + "&VendorCode=" + $("#<%=hidVendorcode.ClientID%>").val();
                var page = page + "&Status=" + $("#<%=hidGidMode.ClientID%>").val() + "&BillType=" + $("#<%=hidBillType.ClientID%>").val();
                var page = page + "&LoadPoQty=N" + "&locationcode=" + $("#<%=hidLocation.ClientID%>").val();

                window.location.href = page;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
         
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="barcodepahe_color">
        <div class="main-container">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page uppercase" id="breadcrumbs_text">
                        <%=Resources.LabelCaption.lblViewGoodsAcknowledgement%>
                    </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
            <div class="container-group-full">
                <div class="purchase-order-header barcodepahe_color">
                    <div class="control-container" style="float: left; clear: both; display: table;">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" OnClick="lnkAdd_Click"
                                Text='<%$ Resources:LabelCaption,btnAdd %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="fncClear();" class="button-blue" OnClick="lnkClear_Click">Clear</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:UpdatePanel ID="uprefresh" runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="LinkRefresh" runat="server" class="button-blue" OnClick="LinkRefresh_Click"
                                        Text='<%$ Resources:LabelCaption,btn_refresh %>'></asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="control-button ga_po_qty" style="display: none">
                            <asp:CheckBox ID="cbLoadPoQty" runat="server" Checked="True" Text='<%$ Resources:LabelCaption,lblLoadPOQty %>' />
                        </div>
                    </div>
                    <div class="control-container" id="divdate">
                        <div class="control-button" id="divUserName">
                            <asp:Label ID="Label1" runat="server" Text="User Name"></asp:Label>
                        </div>
                        <div class="control-button" id="divUserNameText">
                            <asp:TextBox ID="txtUserSearch" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'User', 'txtUserSearch', '');" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="control-button">
                            <asp:Label ID="lblfromdate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                        </div>
                        <div class="control-button">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="control-button">
                            <asp:Label ID="lbltodate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                        </div>
                        <div class="control-button">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="control-group-split">
                    <asp:UpdatePanel ID="upgrnview" runat="server">
                        <ContentTemplate>
                            <div class="ga_view_dropdown">
                                <div class="ga_view_dropdown_split">
                                    <div class="ga_label">
                                        <asp:Label ID="lblvendor" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <%--<asp:DropDownList ID="ddlVendor" runat="server" CssClass="full_width" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogVendor(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="ga_view_dropdown_split">
                                    <div class="ga_label">
                                        <asp:Label ID="lblwarehouse" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <%--<asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="full_width" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlWarehouse_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtWarehouse" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogCommon(event, 'Warehouse', 'txtWarehouse', '');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="ga_view_dropdown_split">
                                    <div class="ga_label">
                                        <asp:Label ID="lbllocation" runat="server" Text='<%$ Resources:LabelCaption,lblLocationCode %>'></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <%--<asp:DropDownList ID="ddllocation" runat="server" CssClass="full_width" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddllocation_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="ga_view_dropdown_split" id="divGidNo">
                                    <div class="ga_label">
                                        <asp:Label ID="lblGidNo" runat="server" Text='<%$ Resources:LabelCaption,lblGidNo %>'></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <%--<asp:TextBox ID="txtGidNo" onkeydown="return fncShowSearchDialogCommon(event, 'GidNo',  'txtGidNo', '');" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtGidNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'GidNo', 'txtGidNo', '');"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="ga_view_radiobutton">
                                <div class="control-button">
                                    <asp:RadioButton ID="rbnGrnpending" runat="server" Text='<%$ Resources:LabelCaption,lblGRNPending %>'
                                        Visible="true" GroupName="grnpendingclosed" Checked="true" AutoPostBack="True"
                                        OnCheckedChanged="rbnGrnpending_CheckedChanged" />
                                    <asp:RadioButton ID="rbnGrnClosed" runat="server" Text='<%$ Resources:LabelCaption,lblGRNClosed %>'
                                        Visible="true" GroupName="grnpendingclosed" AutoPostBack="True" OnCheckedChanged="rbnGrnClosed_CheckedChanged" />
                                    <asp:RadioButton ID="rbnHold" runat="server" Text='<%$ Resources:LabelCaption,lblHold %>'
                                        Visible="true" GroupName="grnpendingclosed" AutoPostBack="true" OnCheckedChanged="rbnHold_CheckedChanged" />
                                    <asp:RadioButton ID="rbnDis" runat="server" Text="DisContinue"
                                        Visible="true" GroupName="grnpendingclosed" AutoPostBack="true" OnCheckedChanged="rbtDis_CheckedChanged" />
                                </div>
                                <div class="control-button" id="divgaholdunhold" style="display: none;">
                                    <asp:RadioButton ID="rbnUnhold" runat="server" Text='<%$ Resources:LabelCaption,lblUnHold %>'
                                        Visible="true" GroupName="grnpending" AutoPostBack="true" OnCheckedChanged="rbnUnhold_CheckedChanged" />
                                    <asp:RadioButton ID="rbnAll" runat="server" Text='<%$ Resources:LabelCaption,lblAll %>'
                                        Visible="true" GroupName="grnpending" AutoPostBack="true" Checked="True" OnCheckedChanged="rbnAll_CheckedChanged" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="divrepeater" class="Barcode_fixed_headers">
                <asp:UpdatePanel ID="uprptrga" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblga" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col" runat="server" id="thDelete">Delete
                                    </th>
                                    <th scope="col" runat="server" id="thEdit">Edit GA
                                    </th>
                                    <th scope="col" runat="server" id="thGrn">GRN
                                    </th>
                                    <th scope="col" runat="server" id="thGrnView">GRN
                                    </th>
                                    <th scope="col">BillNo/RefNo
                                    </th>
                                    <th scope="col">BillDate/RefDate
                                    </th>
                                    <th scope="col">VendorCode
                                    </th>
                                    <th scope="col">VendorName
                                    </th>
                                    <th scope="col">GSTIN No
                                    </th>
                                    <th scope="col">Gross Amount
                                    </th>
                                    <th scope="col">GST
                                    </th>
                                    <th scope="col">Total Amount
                                    </th>
                                    <th scope="col">G.A.No
                                    </th>
                                    <th scope="col">G.A.Date
                                    </th>
                                    <th scope="col">PO.No
                                    </th>
                                    <th scope="col">GRN.No
                                    </th>
                                    <th scope="col">Location
                                    </th>
                                    <th scope="col">CreateDate
                                    </th>
                                    <th scope="col">CreateUser
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrGA" runat="server" OnItemDataBound="rptrGA_ItemDataBound">
                                <HeaderTemplate>
                                    <tbody id="BulkBarcodebody" style="height:386px">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="gaRow" tabindex='<%#(Container.ItemIndex)%>' runat="server"
                                        onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                        </td>
                                        <td runat="server" id="tdDelete">
                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png"
                                                OnClientClick="fncGADeleteAndEdit(this,'Delete');return false;" />
                                        </td>
                                        <td runat="server" id="tdEdit">
                                            <asp:ImageButton ID="btnImgEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                OnClientClick="fncGADeleteAndEdit(this,'Edit')" />
                                        </td>
                                        <td runat="server" id="tdGrn">
                                            <asp:Button ID="btnGrn" runat="server" Text='<%$ Resources:LabelCaption,lblGo%>'
                                                OnClientClick="fncGADeleteAndEdit(this,'Go');return false;" />
                                        </td>
                                        <td runat="server" id="tdGrnView">
                                            <asp:Button ID="btnGrnView" runat="server" Text='<%$ Resources:LabelCaption,btn_View%>'
                                                OnClientClick="fncViewClosedGRN(this); return false; " />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillNo" runat="server" Text='<%# Eval("DOInvNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillDate" runat="server" Text='<%# Eval("BillDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorcode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTinNo" runat="server" Text='<%# Eval("CSTNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillAmount" runat="server" Text='<%# Eval("BillAmount") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltax" runat="server" Text='<%# Eval("tax") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%# Eval("NetAmount") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGANo" runat="server" Text='<%# Eval("GANo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGADate" runat="server" Text='<%# Eval("GADate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPONo" runat="server" Text='<%# Eval("PONo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGidNo" runat="server" Text='<%# Eval("GidNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLocationCode" runat="server" Text='<%# Eval("LocationCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateUser" runat="server" Text='<%# Eval("CreateUser") %>' />
                                        </td>
                                        <td style="display: none">
                                            <asp:Label ID="lblBilltype" runat="server" Text='<%# Eval("BillType") %>' />
                                        </td>
                                        <td style="display: none">
                                            <asp:Label ID="lblPrintCount" runat="server" Text='<%# Eval("GRNCloasedPrint") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="purchase-order-header barcodepahe_color">
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
            <div class="control-button">
                <asp:LinkButton ID="lnkgaprint" runat="server" class="button-blue" OnClick="lnkgaprint_Click"
                    Text='<%$ Resources:LabelCaption,btnGAPrint %>' OnClientClick="return fncReportValidation('')"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkGRNHoldPrint" runat="server" class="button-blue"
                    Text='<%$ Resources:LabelCaption,btn_GRNHoldPrint %>' OnClientClick="return fncReportValidation('')" OnClick="lnkGRNHoldPrint_Click"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkgrnprint" runat="server" class="button-blue" OnClick="lnkgrnprint_Click"
                    Text='<%$ Resources:LabelCaption,lblGRNPrint %>' OnClientClick="return fncReportValidation('GRNPrint')"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkMemoPrint" runat="server" class="button-blue"
                    Text='<%$ Resources:LabelCaption,lbl_MemoPrint %>' OnClientClick="return fncReportValidation('Memo')" OnClick="lnkMemoPrint_Click"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkGRNFOC" runat="server" class="button-blue"
                    Text='<%$ Resources:LabelCaption,lbl_FocPrint %>' OnClientClick="return fncReportValidation('')" OnClick="lnkGRNFOC_Click"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="lnkGRNAddDed" runat="server" class="button-blue"
                    Text='<%$ Resources:LabelCaption,lbl_GRNAddDed %>' OnClientClick="return fncReportValidation()" OnClick="lnkGRNAddDed_Click"></asp:LinkButton>
            </div>
                                    </ContentTemplate>
                 </asp:UpdatePanel>
        </div>
        <div class="hiddencol">
            <div id="deleteGA">
                <div>
                    <asp:Label ID="lblGaDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:UpdatePanel ID="updelete" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkbtnYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                    OnClientClick="return fncConfirmDeleteGA();"> </asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnlbtnNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncCloseDeleteDialog();return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="GAEdit">
                <div>
                    <asp:Label ID="lbleditStatus" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                        OnClientClick="fncCloseEditDialog();return false;"> </asp:LinkButton>
                </div>
            </div>
            <div id="divGidHold">
                <div>
                    <asp:Label ID="lblGidHold" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkgidHoldYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkgidHoldNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="divGRNDelete">
                <div>
                    <asp:Label ID="lblGRNDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkGRNYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>' OnClientClick="fncConfirmGRNDelete('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkGRNNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>' OnClientClick="fncConfirmGRNDelete('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="adminauthentication" title="Authentication">
                <div>
                    <asp:Label ID="lblCountPrint" runat="server" ></asp:Label>
                </div>
                <div>
                    <asp:Label ID="lblPassword" runat="server" Text="Please Enter Admin Password"></asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control-res"
                        onkeydown="fncAdminAuthentication(event);"></asp:TextBox>
                </div>
            </div>
            <asp:UpdatePanel ID="upgidno" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btngidno" runat="server" OnClick="btngidno_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:HiddenField ID="hidgaNo" runat="server" />
            <asp:HiddenField ID="hidVendorcode" runat="server" />
            <asp:HiddenField ID="hidGid" runat="server" />
            <asp:HiddenField ID="hidBillNo" runat="server" />
            <asp:Button ID="btnEdit" runat="server" OnClick="btnEdit_Click" />
            <asp:Button ID="btnGrnGo" runat="server"   OnClick="btnGrnGo_Click"/> <%----%>
            <asp:HiddenField ID="hidLocation" runat="server" />
            <asp:HiddenField ID="hidGidMode" runat="server" />
            <asp:HiddenField ID="hidgaDate" runat="server" />
            <asp:HiddenField ID="hidgaAmt" runat="server" />
            <asp:HiddenField ID="hidBillType" runat="server" />
            <asp:HiddenField ID="hidComputerName" runat="server" />
            <asp:HiddenField ID="hidDiscontinueEntry" runat="server" />
            <asp:HiddenField ID="hidTextileBrand" runat="server" />
            <asp:HiddenField ID="hidTextileLoc" runat="server" />
            <asp:HiddenField ID="hidInterPurchase" runat="server" />

            <div id="dialog-DiscontinueEntry">
            </div>

        </div>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="uprptrga">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidUserCode" runat="server" />
    <asp:HiddenField ID="hidUserNameFilter" runat="server" />
    <asp:HiddenField ID="hidGRNPrintCount" runat="server" />
    <asp:HiddenField ID="hidTextileMode" runat="server" Value="N0"/>
</asp:Content>
