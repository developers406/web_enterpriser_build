﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frmNewPO.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmNewPO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--   <link type="text/css" href="../css/autopo.css" rel="stylesheet" />--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            

        });
        //var flag = null;
        function disableFunctionKeys(e) {
            var functionKeys = new Array(40, 38, 13, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };
        function pageLoad() {
            //fncsort();
            <%--if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                $('#<%=lnkStock.ClientID %>').css("display", "block");
                $('#<%=lnkExport.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                $('#<%=lnkStock.ClientID %>').css("display", "none");
                $('#<%=lnkExport.ClientID %>').css("display", "none");
            }--%>
            $("#<%=txtOrderValue.ClientID %>").number(true, 2);
            var newOption = {};
            var option = '';
            $("#<%=ddlSort.ClientID %>").empty();

            $('#tblHead th').each(function (e) {
                var index = $(this).index();
                var table = $(this).closest('table');
                var val = table.find('.click th').eq(index).text();

                if ($(this).is(":visible")) {
                    if (val != "OrderQty") {
                        if (val != "PO.Date")
                            option += '<option value="' + val + '">' + val + '</option>';
                    }
                }
            });
            $("#<%=ddlSort.ClientID %>").append(option);
            $("#<%=ddlSort.ClientID %>").prop('selectedIndex', 2);
            $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");

         
            $("select").chosen({ width: '100%' });

            $('#lblUp').live('click', function (event) {
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "green");
                var length = $("#<%=grdItemDetails.ClientID %>").length;
                var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
                //alert(columnIndex);
                if (length > 0) {
                    var tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
                    if (columnIndex == 2) {
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text().toUpperCase());
                            var nData = $.trim($(n).text().toUpperCase());
                            return pData < nData ? -1 : 1;
                        });
                    }
                    else if (columnIndex == 5 || columnIndex == 6 || columnIndex == 7 || columnIndex == 8 || columnIndex == 9 || columnIndex == 10) {
                        tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text());
                            var nData = $.trim($(n).text());
                            return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                        });
                    }
                    else if (columnIndex == 11 || columnIndex == 12 || columnIndex == 13) {
                        tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 12) + ")");
                        tdArray.sort(function (p, n) {
                            var pData = $.trim($(p).text());
                            var nData = $.trim($(n).text());
                            return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                        });
                    }
                    else if (columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17 || columnIndex == 18) {
                        tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 15) + ")");
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData < nData ? -1 : 1;
                            });
                        }
                        else {
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text());
                                var nData = $.trim($(n).text());
                                return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                            });
                        }
            tdArray.each(function () {
                var row = $(this).parent();
                $("#<%=grdItemDetails.ClientID %>").append(row);
            });
        }
        else {
            $('#lblDown').css("color", "");
            $('#lblUp').css("color", "");
            ShowPopupMessageBox("Invalid Records");
            return false;
        }


            });
    $('#lblDown').live('click', function (event) {
        $('#lblDown').css("color", "green");
        $('#lblUp').css("color", "");
        var length = $("#<%=grdItemDetails.ClientID %>").length;
        var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
        var tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
        if (length > 0) {

            var tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
            if (columnIndex == 2) {
                tdArray.sort(function (p, n) {
                    var pData = $.trim($(p).text().toUpperCase());
                    var nData = $.trim($(n).text().toUpperCase());
                    return pData > nData ? -1 : 1;
                });
            }
            else if (columnIndex == 5 || columnIndex == 6 || columnIndex == 7 || columnIndex == 8 || columnIndex == 9 || columnIndex == 10) {
                tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                tdArray.sort(function (p, n) {
                    var pData = $.trim($(p).text());
                    var nData = $.trim($(n).text());
                    return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                });
            }
            else if (columnIndex == 11 || columnIndex == 12 || columnIndex == 13) {
                tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 12) + ")");
                tdArray.sort(function (p, n) {
                    var pData = $.trim($(p).text());
                    var nData = $.trim($(n).text());
                    return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                });
            }
            else if (columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17 || columnIndex == 18) {
                tdArray = $("#<%=grdItemDetails.ClientID %>").closest("table").find("tr td:nth-child(" + (columnIndex + 15) + ")");
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text().toUpperCase());
                        var nData = $.trim($(n).text().toUpperCase());
                        return pData > nData ? -1 : 1;
                    });
                }
                else {
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text());
                        var nData = $.trim($(n).text());
                        return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                    });
                }
    tdArray.each(function () {
        var row = $(this).parent();
        $("#<%=grdItemDetails.ClientID %>").append(row);
    });
}
else {
    $('#lblDown').css("color", "");
    $('#lblUp').css("color", "");
    ShowPopupMessageBox("Invalid Records");
    return false;
}

    });

    $(function () {
        $("#spLeftArrow").removeClass('ui-icon-circle-arrow-s');
        $("#spLeftArrow").addClass('ui-icon-circle-arrow-e');
        var icons = {
            header: "ui-icon-circle-arrow-e",
            activeHeader: "ui-icon-circle-arrow-s"
        };
        $("#accordion").accordion({
            collapsible: true,
            icons: icons
        });
        $("#toggle").button().on("click", function () {
            if ($("#accordion").accordion("option", "icons")) {
                $("#accordion").accordion("option", "icons", null);
            } else {
                $("#accordion").accordion("option", "icons", icons);
            }

        });

        $("#spLeftArrow").removeClass('ui-icon-circle-arrow-s');
        $("#spLeftArrow").addClass('ui-icon-circle-arrow-e');

    });
    $("#<%=chKMonth.ClientID %>").click(function () { //Vijay Check                 
        var checked = $(this).is(':checked');
        if (checked == true) {
            $('#lblItemCode').text('');
            $('#lblItemDescription').text('');
            $("#divMonth").css("display", "block");
            $("#<%=HideFilter_GridOverFlow.ClientID %>").height(290);
            $("#<%=divGrd2.ClientID %>").height(285);
        }
        else {
            $('#lblItemCode').text('');
            $('#lblItemDescription').text('');
            $("#divMonth").css("display", "none");
            $("#<%=HideFilter_GridOverFlow.ClientID %>").height(415);
            $("#<%=divGrd2.ClientID %>").height(410);
        }
    });
            //$("label[for='ContentPlaceHolder1_RdoReorder']").css("width", "84%");
    fncDecimal();
    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            if ($("#<%=RdoReorder.ClientID %>").is(':checked')) {
                $('#ContentPlaceHolder1_txtNoOfDays').show();
                $('#ContentPlaceHolder1_lblNoOfDays').show();
            }
            else {
                $('#ContentPlaceHolder1_txtNoOfDays').hide();
                $('#ContentPlaceHolder1_lblNoOfDays').hide();
            }
        }

        $(document).ready(function () {
            $("input:radio").change(function () {
                if ($("#<%=RdoReorder.ClientID %>").is(':checked') ) {
                    $('#ContentPlaceHolder1_txtNoOfDays').show();
                    $('#ContentPlaceHolder1_lblNoOfDays').show();
                }
                else {
                    $('#ContentPlaceHolder1_txtNoOfDays').hide();
                    $('#ContentPlaceHolder1_lblNoOfDays').hide();
                }
            });

            $("#<%=RdoReorder.ClientID %>").click(function () {
                if ($("#<%=RdoReorder.ClientID %>").is(':checked')) {
                    $('#ContentPlaceHolder1_txtNoOfDays').show();
                    $('#ContentPlaceHolder1_lblNoOfDays').show();
                }
                else {
                    $('#ContentPlaceHolder1_txtNoOfDays').hide();
                    $('#ContentPlaceHolder1_lblNoOfDays').hide();
                }
            });
            
        });

        $(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function fncDecimal() {
            try {
                $('#<%=txtQty.ClientID%>').number(true, 2);
                $('#<%=txtSPrice.ClientID%>').number(true, 2);
                <%--$('#<%=txtNoOfDays.ClientID%>').number(true, 0);--%>
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function clearForm() {
            try {

                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtItemName.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtFloor.ClientID %>').val('');
                $('#<%= txtShelf.ClientID %>').val('');
                $('#<%= txtLocation.ClientID %>').val('HQ');

                $('#<%= txtBin.ClientID %>').val('');
                $('#<%= txtMerchendise.ClientID %>').val('');
                $('#<%= txtManufacture.ClientID %>').val('');
                $('#<%= txtSection.ClientID %>').val('');
                $('#<%= txtClass.ClientID %>').val('');
                $(':checkbox, :radio').prop('checked', false);
                //$("select").val(0);
                $("select").trigger("liszt:updated");
            <%--    $('#<%= txtNoOfDays.ClientID %>').val('');--%>
                $('#ContentPlaceHolder1_txtNoOfDays').hide();
                $('#ContentPlaceHolder1_lblNoOfDays').hide();
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
            return false;
        }

        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    // $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'height:40%; width:100%');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%");
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("1325px");
                    $("#<%=divGrd2.ClientID %>").width("179%");

                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:77%');
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("1035px");
                    $("select").trigger("liszt:updated");
                    $("#<%=divGrd2.ClientID %>").width("231%");
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function fncOpenItemHistory() {
            $("#dialog-History").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        function ShowPopupPO(name) {
            clearForm();
            $("#dvPo").dialog({
                title: name,
                width: 350,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }
        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source, curcell) {
            var verHight;
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    //alert('td input[id*="' + curcell + '"]');
                    fncSetFocustoObject(NextRowobj.find('td input[id*="' + curcell + '"]'));
                    return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncSetFocustoObject(prevrowobj.find('td input[id*="' + curcell + '"]'));
                    return false;
                }

                //if (charCode == 13) {
                //    verHight = $("#tblQuickPriceChange_Regular").scrollTop();
                //    $("#tblQuickPriceChange_Regular").scrollTop(verHight + 10);
                //    return false;
                //}

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Get Sales history
        var sum = 0;
        function fncGetSalesHistory(source) {
            var itemcode;
            var itemname;
            var cost = 0, OrderQty = 0;
            try {
                rowObj = $(source).closest("tr");
                if ($("#<%=chKMonth.ClientID %>").is(':checked')) {
                    //fncVenItemRowClk((source).closest("tr"));

                    itemcode = $("td", rowObj).eq(2).text().replace(/&nbsp;/g, '');
                    itemname = $("td", rowObj).eq(3).text().replace(/&nbsp;/g, '');
                    //desc = $("td", rowObj).eq(2).text().replace(/&nbsp;/g, '');
                    fncBindSalesHistory(itemcode, itemname);
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetOrderValue(source) {
            try {
                sum = 0;
                $("#<%=grdItemDetails.ClientID %>  tr").each(function () {
                    rowObj = $(this);
                    sum = parseFloat(sum) + parseFloat(rowObj.find('td input[id*="txtOrderQty"]').val() * rowObj.find('td').eq(12).text());
                    rowObj.find('td').eq(11).text(parseFloat(parseFloat(rowObj.find('td input[id*="txtOrderQty"]').val() * rowObj.find('td').eq(12).text())).toFixed(2));
                });
                $("#<%=txtOrderValue.ClientID %>").val(parseFloat(sum).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncVenItemRowClk(rowObj) {
            var siblingObj;
            try {
                //$(rowObj).css("background-color", "#fff2e6");
                //$(rowObj).siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Bind Sales history to table
        function fncBindSalesHistory(itemcode, itemname) {
            var obj = {};
            try {
                obj.itemcode = itemcode; 
                obj.location = $('#<%= txtLocation.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: "frmNewPO.aspx/fncGetSalesHistory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var tblLocSalesHis;
                        tblLocSalesHis = $('#tblItemhistory tbody');
                        tblLocSalesHis.children().remove();
                        var objItem = jQuery.parseJSON(msg.d);

                        //alert('');
                        //console.log(msg.d);
                        if (objItem.length > 0) {
                            // fncMonthdialogopen();
                            for (var i = 0; i < objItem.length; i++) {
                                tblLocSalesHis.append("<tr>"
                                    + "<td>" + objItem[i]["MNName"] + "</td>"
                                    + "<td >" + objItem[i]["Week1"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week2"].toFixed(2) + "</td>"
                                    + "<td >" + objItem[i]["Week3"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week4"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["Week5"].toFixed(2) + "</td>"
                                    + "<td>" + objItem[i]["TotalQty"].toFixed(2) + "</td>"
                                    + "</tr>");
                            }
                        }
                        $('#lblItemCode').text("ItemCode: " + itemcode + '  -   ');
                        $('#lblItemDescription').text("ItemName: " + itemname);

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncExcelExport() {
            try {
                $('#<%=btnExcelExport.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message)
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncVenItemRowdblClk(itemcode) {
            try {
                // rowObj = $(rowObj);
                fncOpenItemhistory($.trim(itemcode));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }
        function fncVendorclick() {
            fncVedorViewClick();
        }

        function fncVedorViewClick() {
            fncPendingLoad();
        }

        function fncPendingLoad() {
            try {
                var obj = {};
                obj.Mode = "Pending";
                obj.Days = $('#<%=ddlVendorDays.ClientID %>').find('option:selected').text();
                obj.Dayvalue = $('#<%=ddlVendorDays.ClientID %>').find('option:selected').val();
                $.ajax({
                    type: "POST",
                    url: "frmNewPO.aspx/fncGetPendigDays",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        AssignTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });
            }
            catch (err) {
                err.message();
            }
        }

        function AssignTable(msg) {
            var obj = jQuery.parseJSON(msg.d);
            var tblPendingBody = $("#tblPendnigValue tbody");
            var tblProcessBody = $("#tblProcessValue tbody");
            tblPendingBody.children().remove();
            for (var i = 0; i < obj.Table.length; i++) {
                var row = "<tr id='PendingRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                     (parseFloat(i) + 1) + "</td><td id='tdvendorCode_" + i + "' >" +
                     $.trim(obj.Table[i]["VendorCode"]) + "</td><td id='tdvendorname_" + i + "' >" +
                     $.trim(obj.Table[i]["vendorname"]) + "</td><td id='tdPodate_" + i + "' >" +
                     obj.Table[i]["Podate"] + "</td><td id='tdGRNDate_" + i + "' >" +
                     obj.Table[i]["GRNDate"] + "</td></tr>";
                tblPendingBody.append(row);
            }

            tblProcessBody.children().remove();
            if (obj.Table1.length > 0) {
                for (var i = 0; i < obj.Table1.length; i++) {
                    var row = "<tr id='PendingRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                         (parseFloat(i) + 1) + "</td><td id='tdvendorCode_" + i + "' >" +
                         $.trim(obj.Table1[i]["VendorCode"]) + "</td><td id='tdvendorname_" + i + "' >" +
                         $.trim(obj.Table1[i]["vendorname"]) + "</td><td id='tdPodate_" + i + "' >" +
                         obj.Table1[i]["Podate"] + "</td><td id='tdNetValue_" + i + "' >" +
                         $.trim(obj.Table1[i]["PoNetValue"]) + "</td><td id='tdTotalValue_" + i + "' >" +
                         $.trim(obj.Table1[i]["PoTotalValue"]) + "</td><td id='tdPoNo_" + i + "' >" +
                         $.trim(obj.Table1[i]["PoNo"]) + "</td></tr>";
                    tblProcessBody.append(row);
                }
            }
            tblPendingBody.children().dblclick(fncPendingClick);

            tblProcessBody.children().dblclick(fncProcessClick);

            $('#divVendorView').dialog({
                autoOpen: false,
                modal: true,
                height: 600,
                width: 860,
                title: "Daywise Vendor"
            });
            $('#divVendorView').dialog('open');
        }

        function fncPendingClick() {
            try {
                $('#divVendorView').dialog('close');
                $('#<%= txtVendor.ClientID %>').val($.trim($(this).find('td[id*="tdvendorCode"]').text()));
               
                __doPostBack('ctl00$ContentPlaceHolder1$lnkFilter', '');
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncProcessClick() {
            $('#<%= hidPoNo.ClientID %>').val($.trim($(this).find('td[id*="tdPoNo"]').text()));
            $('#<%= btnPo.ClientID %>').click();
        }

        $(function () {
            $('#<%=ddlVendorDays.ClientID %>').change(function () {
                //alert($('option:selected', this).text());
                fncPendingLoad();
            });
        });
        function fncClear() {
            clearForm();
            $("#divMonth").css("display", "none");
            sum = 0;
            return true;
        }
      
        
            $(document).ready(function () {
                try {



                    var obj = {};
                    obj.Mode = "loc";
                    $.ajax({
                        type: "POST",
                        url: "frmNewPO.aspx/Getloc",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {

                            fncBindloc(response.d);

                        },
                        failure: function (response) {

                        }
                    });
                }


                catch (err) {

                }

            });
        
         
        

          
        
        function fncBindloc(obj) {
          
                var tblHeader = $("#tblHeader tbody");

                var sno = 0;
                var row = "";
                
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                        sno = parseFloat(sno) + 1;
                        var split = obj[i].split('|');
                        row = "<tr class='text-center'>";

                        row += "<td style='width:40px'>" + sno + "</td>";
                        row += "<td class='row' style='display:none'><input type='text' id='txtcode' value='" + split[0] + "' autocomplete='off' /></td>";
                        row += "<td class='row'  >" + split[1] + "</td>";
                        row += "<td ><input type='text' id='txtdays' value='" + "" + "' autocomplete='off' OnKeydown='return fncnext(event,this);' /></td>";
                        row += "</tr>";


                        tblHeader.append(row);

                    }

                }
            
            
            
        }
        
        function fncdata(e) {
            try {
              
                    
                    var result = true;
                    var xml = '<NewDataSet>';
                    var num = 0;
                    $("#tblHeader tbody").children().each(function () {

                        obj = $(this);

                        var no = obj.find('td input[id*=txtdays]').val();
                        
                        if (no > 0) {
                            xml += "<Table>";
                            xml += "<Code>" + obj.find('td input[id*=txtcode]').val() + "</Code>";
                            xml += "<NoDay>" + obj.find('td input[id*=txtdays]').val() + "</NoDay>";
                            xml += "</Table>";
                            num++;
                        }

                    });
                    if (num != 0) {
                        xml = xml + '</NewDataSet>';
                        xml = escape(xml);
                        $('#<%=days.ClientID %>').val(xml);
                        $('#<%=btnload.ClientID %>').click();
                        event.stopPropagation();
                        event.preventDefault();
                        return false;
                        //return result;
                    }
                if (num == 0) {
                    event.stopPropagation();
                    event.preventDefault();
                    return false;
                        //result = false;
                    }
                    //return result;
               
            }
            
          catch (err) {

          }
        }
      
        function fncnext(evt, source) {
            //$("#tblHeader tbody").find('#txtdays').focus();
            var rowobj = $(source).parent().parent();
            var key = evt.keyCode ? evt.keyCode : evt.which;
            var field = rowobj.closest('tr').next().find('td input[id*=txtdays]');
                if (key == 13) {
                    field.focus();
                    return false;
                }
           
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

            <div class="main-container" style="overflow-x:scroll;overflow-y:scroll">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Purchase (New PO) </li>
                    </ul>
                </div>
             
                    
                    <div class="container-left-price" id="pnlFilter" runat="server" style="width: 22% !important;">

                                                           <div class="GridDetails" style="width: 100%" runat="server">

                                    <div style ="height:100px; width:325px; ">
                                        <table cellspacing="0" cellpadding = "0" rules="all" border="1" id="tblHeader"style="font-family:Arial;border-collapse:collapse;height:25%;">
                                            <thead>
                                            <tr style="background-color:#4163e1">
                                                <th style="color:white">S.No</th>
                                                <th class="hiddencol">LocationCode</th>
                                                <th style="padding:2px 3px;color:white">LocationName</th>
                                                <th style="padding:2px 48px;color:white">No.Of.Days</th>
                                            </tr>
                                                </thead>
                                            <tbody style="height: 78px;width: 316px;overflow-y: auto;position: absolute;">
                                                 
                                            </tbody>

</table>

                                    </div>
                        <div id="acc">
                            <h1 style="font-size: 17px;padding: 3px 3px 6px 107px;background: #4163e1;color: white;">Filtration 
                           <%-- <span id="spLeftArrow" onclick="return fncHideFilter();" title="Click to Hide Filter" style="margin-left: 115px;" class="ui-accordion-header-icon ui-icon ui-icon-circle-arrow-e"></span>--%><%--<img alt="Uniprotech" src="images/go_arrow.jpg" />--%> </h1>
                            <div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text="Location"></asp:Label>
                                        <%--</div>--%>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtVendor');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                               
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                                    </div>
                                </div>
                                 
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                       
                                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                       
                                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                     
                                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtItemCode');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="SubCategory"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                      
                                        <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Merchendise"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                     
                                        <asp:TextBox ID="txtMerchendise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchendise', 'txtManufacture');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="Manufature"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                    
                                        <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                                    </div>
                                </div>
                             
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label20" runat="server" Text="Floor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                      
                                        <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="Section"></asp:Label>
                                    </div>
                                    <div class="label-right">

                                        <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label13" runat="server" Text="Bin"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                     
                                        <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch" style="display:none">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Shelf"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                   
                                        <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemCode');"></asp:TextBox>
                                    </div>
                                </div>


                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label16" runat="server" Text="Item Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                                            onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="Item Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Class"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtFromDate');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="Lblfrod" runat="server" Text="FromDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res FilterSearch">
                                    <div class="label-left">
                                        <asp:Label ID="lblto" runat="server" Text="ToDate"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res col-md-12 FilterSearch">
                                    <div class="col-md-5">
                                        <div class="label-left">
                                            <asp:Label ID="lblQty" runat="server" Text="Qty < "></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="col-md-6">
                                            <asp:Label ID="lblSPrice" runat="server" Text="S.Price<="></asp:Label>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                                        </div>
                                         <div class="control-group-single-res col-md-12 RadioButtonFilter" style="position: inherit;left: -135px;bottom: -26px;display:none">
                                    <div class="col-md-8 RadioWidthLeft" style="margin-right:65px">
                                        <asp:RadioButton ID="RdoSQtyDate" runat="server" GroupName="priceMenu"  Text="Sold Qty in Date Range" Class="radioboxlist" />
                                    </div>
                              
                                </div>
                                <div class="control-group-single-res col-md-12 RadioWidthRight" style="position: inherit;right: 142px;bottom: -1px;display:none">
                                    <div class="col-md-8 RadioWidthLeft" style="margin-right: 57px;">
                                        <asp:RadioButton ID="RdoReorder" runat="server" GroupName="priceMenu" Visible="true" Text="ReOrderByNo.ofDays" Checked="true" Class="radioboxlist" />
                                    </div>

                                </div>
                                    </div>
                                </div>
                            </div>
                      
                                    </div>
                   
                        <div class="control-container RadioButtonFilter">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server"   class="button-red"  OnClientClick="return fncdata(event);" ><i class="icon-play" ></i>Load Data</asp:LinkButton>
                                
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick=" return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                                                            </div>
                    </div>
                        
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                        style="width: 78% !important; margin-left: 0% !important">
                        
  
                        <asp:UpdatePanel ID="updatePnlAutoPo" UpdateMode="Conditional" runat="Server">
                            <ContentTemplate>
                  
                                    <div class="col-md-12">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-2">
                                            <div class="control-button"style="position: inherit;left: 432px;display:none">
                                                <asp:LinkButton ID="lnkVendorView" runat="server" class="button-blue" OnClientClick="fncVedorViewClick();return false;">Daywise Vendor</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-md-4 sort_by" style="display:none">
                                            <div class="col-md-3">
                                                <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                                            </div>
                                            <div class="col-md-7">
                                                <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-1">
                                                <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                            </div>
                                            <div class="col-md-1">
                                                <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: scroll; height: 453px; background-color: aliceblue;"
                                        id="HideFilter_GridOverFlow" runat="server">
                                        <div class="grdLoad">
                                            <table rules="all" id="tblHead" class="Payment_fixed_headers">
                                             
                                            </table>
                                          <div class="grdbodyAp" id="divGrd2" runat="server">
                                                <asp:GridView ID="grdItemDetails" runat="server" AutoGenerateColumns="false" OnPageIndexChanging="OnPageIndexChanging"
                                                    OnRowDataBound="grdItemDetails_RowDataBound"  OnDataBound="grdItemDetails_DataBound"
                                                    PageSize="14" ShowHeaderWhenEmpty="True" ShowHeader="true" CssClass="pshro_GridDgn">

                                                    <PagerStyle CssClass="pshro_text" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                   

                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        PageButtonCount="5" Position="Bottom" />
                                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                </asp:GridView>
                                            </div>

                                    </div>
                                    </div>
                                
                        
                       
                                <%--<ups:PaginationUserControl runat="server" ID="AutoPo" Visible="false" OnPaginationButtonClick="AutoPo_PaginationButtonClick" />--%>
                                <div class="col-md-12">
                                    <div class="col-md-9">
                                    </div>
                                   <%-- <asp:Label ID="lblformula1" style ="color:red;" runat="server" Text="Average = SoldQty / DayRange "></asp:Label>--%>
                                     <asp:Label ID="lblformula2" style ="color:red;margin-left:-449px" runat="server" Text="*StockRelease = Average - Location(Qty On Hand) "></asp:Label>
                                    <div class="col-md-3" style="display:none">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <asp:Label ID="Label19" runat="server" Text="OrderValue" Font-Bold="true"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtOrderValue" runat="server" Text="0" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-8">
                                        <div class="control-button" style="display:none">
                                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click">Create PO</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="return fncClear();" OnClick="lnkClearAll_Click1">Clear</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" OnClientClick="fncExcelExport();return false;">Export</asp:LinkButton>
                                        </div>
                                        <div class="control-button" style="display:none">
                                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click">Print</asp:LinkButton>
                                        </div>
                                        <div class="control-button" style="display:none">
                                            <asp:LinkButton ID="lnkStock" runat="server" class="button-blue" OnClick="lnkStock_Click">Stock Request</asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="col-md-2" style="padding-top: 8px">
                                        <div class="control-group-single-res">
                                            <asp:CheckBox ID="RdoAllowZero" runat="server" GroupName="allMenu" Text="Allow Zero Qty" Class="radioboxlistgreen" />
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 8px">
                                        <div class="control-group-single-res">
                                            <asp:CheckBox ID="chKMonth" runat="server" GroupName="allMenu" Text="Month wise Sales" Class="radioboxlistgreen" />
                                        </div>
                                    </div>
                                    </di>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-top: 10px;display:none">
                                        <div class="col-md-12" style="margin-top: 5px;">
                                            <div class="col-md-4">
                                                <asp:Label ID="lblWareHouse" runat="server" Text="WareHouse"></asp:Label>
                                            </div>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtWareHouse" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse',  'txtWareHouse', '');" CssClass="form-control-res"> 
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="Payment_fixed_headers Itemhistory PO_Itemhistory col-md-6" style="display: none;" id="divMonth">
                                            <div class="col-md-12">
                                                <label id="lblItemCode" style="font-weight: 700;"></label>
                                                <label id="lblItemDescription" style="font-weight: 700;"></label>
                                            </div>
                                            <div class="col-md-12">
                                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Month
                                                            </th>
                                                            <th scope="col">1st Week
                                                            </th>
                                                            <th scope="col">2st Week
                                                            </th>
                                                            <th scope="col">3st Week
                                                            </th>
                                                            <th scope="col">4st Week
                                                            </th>
                                                            <th scope="col">5st Week
                                                            </th>
                                                            <th scope="col">Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                   <div id="dvPo" style="display: none;">
                        <asp:GridView ID="grdPOCreated" runat="server" CellPadding="10" AutoGenerateColumns="False"
                            ForeColor="#333333">
                            <Columns>
                                <asp:BoundField DataField="PONO" HeaderText="PO-No" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="VendorCode" HeaderText="Vendor" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <AlternatingRowStyle BackColor="White" />
                            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                            <RowStyle BackColor="#FFFBD6" BorderColor="Black" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                            <SortedAscendingCellStyle BackColor="#FDF5AC" />
                            <SortedAscendingHeaderStyle BackColor="#4D0000" />
                            <SortedDescendingCellStyle BackColor="#FCF6C0" />
                            <SortedDescendingHeaderStyle BackColor="#820000" />
                        </asp:GridView>

                    </div>
                                    </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lnkExport" />
                                <asp:PostBackTrigger ControlID="btnExcelExport" />
                                </Triggers>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lnkFilter" />
                                <asp:AsyncPostBackTrigger ControlID="btnload" />
                            </Triggers>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnExcelExport" />
                                    
                            </Triggers>
                        </asp:UpdatePanel>
                </div>
                
                    <div id="dialog-History" style="display: none">
                        <asp:GridView ID="grdItemHistory" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="MNName" HeaderText="Month" ItemStyle-Width="80">
                                    <ItemStyle Width="80px"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Week1" HeaderText="Week1" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Week2" HeaderText="Week2" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Week3" HeaderText="Week3" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Week4" HeaderText="Week4" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Week5" HeaderText="Week5" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="TotalQty" HeaderText="Total Qty" ItemStyle-Width="150">
                                    <ItemStyle Width="150px" HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundField>
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" BorderColor="Black" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                    </div>
                 
                </div>
                <div class="display_none">
                    <asp:Button ID="btnExcelExport" runat="server" OnClick="lnkExport_Click" />
                    <asp:Button ID="btnload" runat="server" OnClick="lnkLoadFilter_Click" />
                    <asp:HiddenField ID="hidSort" runat="server" />
                    <asp:Button ID="btnPo" runat="server" OnClick="lnkPo_Click" />
                </div>
           
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidCurrentdate" runat="server" />
    <asp:HiddenField ID="hidPoNo" runat="server" />
    <div class="display_none">
        <div class="col-md-12" id="divVendorView">
            <div class="col-md-12">
                <div class="col-md-4 sort_by" style="width: 20%;">
                    <div class="col-md-2">
                        <asp:Label ID="lblDays" Text="Days" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-10" style="margin-left: 40px; margin-top: -20px;">
                        <asp:DropDownList runat="server" Width="100%" ID="ddlVendorDays">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-8">
                </div>

            </div>
            <div class="col-md-12" style="margin-top: 5px;">
                <div class="col-md-12">
                    <div class="dialog-inv-container-set-price-header">
                        Pending
                    </div>
                    <div class="pendig Vendor_margin_border">
                        <div class="col-md-12">
                            <table id="tblPendnigValuehead" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Vendor Code
                                        </th>
                                        <th scope="col">Vendor Name
                                        </th>
                                        <th scope="col">Last PO Date
                                        </th>
                                        <th scope="col">Last GRN Date
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 200px; width: 102% !important">
                            <table id="tblPendnigValue" cellspacing="0" rules="all" border="1">
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="dialog-inv-container-set-price-header">
                        Process
                    </div>
                    <div class="process Vendor_margin_border">
                        <div class="col-md-12">
                            <table id="tblProcessValuehead" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Vendor Code
                                        </th>
                                        <th scope="col">Vendor Name
                                        </th>
                                        <th scope="col">PO Date
                                        </th>
                                        <th scope="col">Po Value
                                        </th>
                                        <th scope="col">Total
                                        </th>
                                        <th scope="col">PO.No
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-12" style="overflow-y: scroll; overflow-x: hidden; height: 200px; width: 102% !important">
                            <table id="tblProcessValue" cellspacing="0" rules="all" border="1">
                                <tbody></tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="days" runat="server" Value="" />
    <asp:HiddenField ID="Length" runat="server" Value="" />
     
</asp:Content>
