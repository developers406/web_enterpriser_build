﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="GoodsAcknowledgmentReceiptTextiles.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.GoodsAcknowledgmentReceiptTextiles" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .rptr_polist td:nth-child(1), .rptr_polist th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .rptr_polist td:nth-child(2), .rptr_polist th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .rptr_polist td:nth-child(2) {
            text-align: center !important;
        }

        .rptr_polist td:nth-child(3), .rptr_polist th:nth-child(3) {
            min-width: 182px;
            max-width: 182px;
        }

        .rptr_polist td:nth-child(4), .rptr_polist th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .rptr_polist td:nth-child(5), .rptr_polist th:nth-child(5) {
            min-width: 140px;
            max-width: 140px;
        }

        .rptr_polist td:nth-child(5) {
            text-align: right !important;
        }

        .rptr_polist td:nth-child(6), .rptr_polist th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_polist td:nth-child(6) {
            text-align: right !important;
        }

        .rptr_polist td:nth-child(7), .rptr_polist th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_polist td:nth-child(7) {
            text-align: right !important;
        }

        .rptr_PurchaseRetReq td:nth-child(1), .rptr_PurchaseRetReq th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .rptr_PurchaseRetReq td:nth-child(2), .rptr_PurchaseRetReq th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
        }

        .rptr_PurchaseRetReq td:nth-child(3), .rptr_PurchaseRetReq th:nth-child(3) {
            min-width: 514px;
            max-width: 514px;
        }

        .rptr_PurchaseRetReq td:nth-child(4), .rptr_PurchaseRetReq th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_InterPurchase td:nth-child(1), .rptr_InterPurchase th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .rptr_InterPurchase td:nth-child(2), .rptr_InterPurchase th:nth-child(2) {
            min-width: 223px;
            max-width: 223px;
        }

        .rptr_InterPurchase td:nth-child(3), .rptr_InterPurchase th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_InterPurchase td:nth-child(4), .rptr_InterPurchase th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .rptr_InterPurchase td:nth-child(5), .rptr_InterPurchase th:nth-child(5) {
            min-width: 200px;
            max-width: 200px;
        }

        .rptr_InterPurchase td:nth-child(6), .rptr_InterPurchase th:nth-child(6) {
            display: none;
        }

        .rptr_poselected td:nth-child(1), .rptr_poselected th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .rptr_poselected td:nth-child(2), .rptr_poselected th:nth-child(2) {
            min-width: 322px;
            max-width: 322px;
        }

        .rptr_poselected td:nth-child(3), .rptr_poselected th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_poselected td:nth-child(4), .rptr_poselected th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_poselected td:nth-child(4) {
            text-align: right !important;
        }

        .rptr_poselected td:nth-child(5), .rptr_poselected th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
        }

        .rptr_poselected td:nth-child(5) {
            text-align: right !important;
        }


        .body {
            background: #f3f3f3;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'GoodsAcknowledgementReceiptTextiles');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "GoodsAcknowledgementReceiptTextiles";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        var glpoListtbody, glpurRerReqtbody, glinterPurchasetbody, glLorrytrantbody;
        var arrPONo = [];

        function pageLoad() {
            try {

                $("#<%=ddlLRStatus.ClientID %>, #<%=ddlLRStatus.ClientID %>_chzn, #<%=ddlLRStatus.ClientID %>_chzn > div, #<%=ddlLRStatus.ClientID %>_chzn > div > div > input").css("width", '100%');
                $("#<%=DropDownTransport.ClientID %>, #<%=DropDownTransport.ClientID %>_chzn, #<%=DropDownTransport.ClientID %>_chzn > div, #<%=DropDownTransport.ClientID %>_chzn > div > div > input").css("width", '100%');

                shortcut.add("Ctrl+A", function () {
                    $('#breadcrumbs_text_GAR').removeClass('lowercase');
                    $('#breadcrumbs_text_GAR').addClass('uppercase');
                    $("#<%=hidbillType.ClientID%>").val('A');

                });
                shortcut.add("Ctrl+B", function () {
                    $('#breadcrumbs_text_GAR').removeClass('uppercase');
                    $('#breadcrumbs_text_GAR').addClass('lowercase');
                    $("#<%=hidbillType.ClientID%>").val('B');

                });

                if ($("#<%=hidbillType.ClientID %>").val() == "A") {
                    $('#breadcrumbs_text_GAR').removeClass('lowercase');
                    $('#breadcrumbs_text_GAR').addClass('uppercase');
                }
                else {
                    $('#breadcrumbs_text_GAR').removeClass('uppercase');
                    $('#breadcrumbs_text_GAR').addClass('lowercase');
                }

                $("select").chosen({ width: '100%' }); // width in px, %, em, etc 

                //var ddlCustomers = document.getElementById("DropDownLrNo"); 
                $("#<%=DropDownLrNo.ClientID%>").show().removeClass('chzn-done');
                $("#<%=DropDownLrNo.ClientID%>").next().remove();



                $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });//.datepicker("setDate", "0");

                $("#<%= txtReceiptDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });//.datepicker("setDate", "0");
                $("#<%= txtLRDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });//.datepicker("setDate", "0");

                $("#<%=hidPoNo.ClientID%>").val('');
                $("#<%=hidVendorBillNo.ClientID %>").val('');


                disableVendorandLocation();



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncQtyFocusOut(source) {
            try {

                var ReceivedQty = parseInt($(source).val());
                var BaleQty = parseInt($("#<%=txtLRBaleQty.ClientID%>").val());

                if (ReceivedQty == 0) {
                    $("#<%=ddlLRStatus.ClientID%>").val('0')
                    $("#<%=ddlLRStatus.ClientID%>").trigger("liszt:updated");
                }
                else if (ReceivedQty >= BaleQty) {
                    $("#<%=ddlLRStatus.ClientID%>").val('2')
                    $("#<%=ddlLRStatus.ClientID%>").trigger("liszt:updated");
                }
                else if (ReceivedQty < BaleQty) {
                    $("#<%=ddlLRStatus.ClientID%>").val('1')
                    $("#<%=ddlLRStatus.ClientID%>").trigger("liszt:updated");
                }
    }
    catch (err) {
        alert(err.message);
    }
}

function fncinitializeinventoryCal() {
    try {
        $("#dialog-Lorry").dialog({
            appendTo: 'form:first',
            autoOpen: false,
            resizable: false,
            height: "300",
            width: 1050,
            modal: true,
            //show: {
            //    effect: "blind",
            //    duration: 1000
            //},
            //hide: {
            //    effect: "blind",
            //    duration: 1000
            //},
            buttons: {
                "Update": function () {
                    $("#<%=hidLRUpdate.ClientID%>").val('1');
                    $(this).dialog("close");
                },
                //Clear: function () {
                //    //$("#dialog-clear-confirm").dialog("open"); 
                //},
                Close: function () {                    
                    $("#<%=hidLRUpdate.ClientID%>").val('0');
                    $(this).dialog("close");
                }
            }
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncInitLoad() {
    try {
        $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" }).datepicker("setDate", "0");
        $("#<%=txtGADate.ClientID %>").val($.datepicker.formatDate('dd/mm/yy', new Date()));

        $("#<%= txtReceiptDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" }).datepicker("setDate", "0");
        $("#<%=txtLRDate.ClientID %>").val($.datepicker.formatDate('dd/mm/yy', new Date()));
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

//Disable vendor code and Location code
function disableVendorandLocation() {
    try {
        if ($("#<%=txtGA.ClientID %>").val() != "") {
            $("#<%=ddlHqCode.ClientID %>").prop("disabled", true);
            $("#<%=txtvendorname.ClientID %>").prop("disabled", true);

            $("#<%=ddlHqCode.ClientID %>").trigger("liszt:updated");
            $("#<%=txtvendorname.ClientID %>").trigger("liszt:updated");

            fncGetVendorDetail();
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

//Data Initialize
$(document).ready(function () {
    //$("#<%=txtBillDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            glpoListtbody = $("#tblPoList tbody");
            glpurRerReqtbody = $("#tblPurRetReq tbody");
            glinterPurchasetbody = $("#tblInterPurchase tbody");
            glLorrytrantbody = $("#tblLRTrans tbody");
            fncdecimal();
            fncinitializeinventoryCal();
            $("#dialog-Lorry").parent().appendTo($("form:first"));
        });

        function fncdecimal() {
            try {
                $("#<%=txtBasicAmt.ClientID %>").number(true, 2);
                $("#<%=txtSchemeDis.ClientID %>").number(true, 2);
                $("#<%=txtTaxAmount.ClientID %>").number(true, 2);
                $("#<%=txtBillAmt.ClientID %>").number(true, 2);
                //$("#<%=txtAddDed.ClientID %>").number(true, 2);
                $("#<%=txtTotalPayable.ClientID %>").number(true, 2);

                $("#<%=txtWeight.ClientID %>").number(true, 2);
                $("#<%=txtLorryAmount.ClientID %>").number(true, 2);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        function SetSelectedText(ddlFruits) {
            var selectedText = ddlFruits.options[ddlFruits.selectedIndex].innerHTML;
            var selectedValue = ddlFruits.options[ddlFruits.selectedIndex].value;
            //document.getElementById("hfFruitName").value = selectedText;          

            if (selectedValue != '0')
            {
                $("#<%=hidLRNo.ClientID%>").val(selectedText);      
                $("#dialog-Lorry").dialog("open");
            }
            else {
                $("#<%=hidLRNo.ClientID%>").val('0');    
            }
        }

        //Get Vendor Detail
        function fncGetVendorDetail() {
            try {
                var numofTable, row;
                $("#<%=hidVendorBillNo.ClientID %>").val('');
                var obj = {};
                obj.vendorCode = $("#<%=txtvendorcode.ClientID %>").val();
                obj.locationCode = $("#<%=ddlHqCode.ClientID %>").val();

                if (obj.vendorCode == "")
                    return;

                if (obj.locationCode == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Location%>');
                }

                setTimeout(function () {
                    $("#<%=txtBillNo.ClientID %>").focus().select();
                }, 50);

                $.ajax({
                    type: "POST",
                    url: "GoodsAcknowledgmentReceiptTextiles.aspx/fncGetVendorDetail",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        glpoListtbody.children().remove();
                        glpurRerReqtbody.children().remove();
                        glinterPurchasetbody.children().remove();
                        glLorrytrantbody.children().remove();

                        var objvendor = jQuery.parseJSON(msg.d);
                        //console.log(objvendor);
                        //console.log(Object.keys(objvendor).length);
                        numofTable = Object.keys(objvendor).length;
                        //debugger;
                        if (numofTable > 0) {
                            if (objvendor.Table.length > 0) {

                                $("#<%=txtAddress1.ClientID %>").val(objvendor.Table[0]["Address1"]);
                                $("#<%=txtTINNo.ClientID %>").val(objvendor.Table[0]["CSTNo"]);

                                if (objvendor.Table[0]["CST"] == "1") {
                                    $("#<%=txtGSTType.ClientID %>").val('Intra State');
                                    }
                                    else if (objvendor.Table[0]["CST"] == "2") {
                                        $("#<%=txtGSTType.ClientID %>").val('Inter State');
                                }
                                else if (objvendor.Table[0]["CST"] == "3") {
                                    $("#<%=txtGSTType.ClientID %>").val('Import');
                                }
                                else {
                                    $("#<%=txtGSTType.ClientID %>").val('Zero GST');
                                }

                        if (objvendor.Table[0]["CST"] == "4") {
                            $("#<%=hidbillType.ClientID %>").val('B');
                            $('#breadcrumbs_text_GAR').removeClass('uppercase');
                            $('#breadcrumbs_text_GAR').addClass('lowercase');
                        }
                        else {
                            $("#<%=hidbillType.ClientID %>").val($("#<%=hideActualBillMode.ClientID %>").val());

                            if ($("#<%=hidbillType.ClientID %>").val() == "A") {
                                $('#breadcrumbs_text_GAR').removeClass('lowercase');
                                $('#breadcrumbs_text_GAR').addClass('uppercase');
                            }
                        }
                    }
                }

                        if (numofTable > 1) {
                            if (objvendor.Table1.length > 0) {
                                for (var i = 0; i < objvendor.Table1.length; i++) {
                                    row = "<tr><td>" + objvendor.Table1[i]["RowNo"] + "</td>"
                                        + "<td><input id='cbSelect_" + i + "' type='checkbox' onclick='fncPOListRowClick(this);' /></td>"
                                        + "<td id='tdPoNo_" + i + "'>" + objvendor.Table1[i]["PoNo"] + "</td>"
                                        + "<td id='tdPoDate_" + i + "'>" + objvendor.Table1[i]["PODate"] + "</td>"
                                        + "<td id='tdPoQty_" + i + "' >" + objvendor.Table1[i]["LQty"] + "</td>"
                                        + "<td id='tdNetTotal_" + i + "'>" + objvendor.Table1[i]["PoNetValue"].toFixed(2) + "</td>"
                                        + "<td id='tdPotax_" + i + "'>" + objvendor.Table1[i]["Gst"].toFixed(2) + "</td>"
                                        + "</tr>";
                                    glpoListtbody.append(row);
                                }
                                //glpoListtbody.children().click(fncPoListRowClick);
                            }
                        }

                        if (numofTable > 2) {
                            if (objvendor.Table2.length > 0) {
                                for (var i = 0; i < objvendor.Table2.length; i++) {
                                    glpurRerReqtbody.append("<tr><td>" + objvendor.Table2[i]["RowNo"] + "</td><td id='tdInventorycode_" + i + "'>" +
                                objvendor.Table2[i]["InventoryCode"] + "</td><td>" + objvendor.Table2[i]["Description"] + "</td><td>" +
                                objvendor.Table2[i]["MRP"] + "</td></tr>");
                                }
                            }
                        }

                        if (numofTable > 3) {
                            if (objvendor.Table3.length > 0) {
                                $("#<%=hidVendorBillNo.ClientID %>").val(objvendor.Table3[0]["BillNo"]);
                                }
                            }

                        if (numofTable > 4) {
                            if (objvendor.Table4.length > 0) {
                                for (var i = 0; i < objvendor.Table4.length; i++) {
                                    glinterPurchasetbody.append("<tr><td>" + objvendor.Table4[i]["RowNo"] + "</td><td id='tdInvoiceno_" + i + "'>" +
                                objvendor.Table4[i]["Invoiceno"] + "</td><td>" + objvendor.Table4[i]["InvoiceDate"] + "</td><td id='tdTotalQty_" + i + "'>" +
                                objvendor.Table4[i]["TotalQty"] + "</td><td id='tdNetTotal_" + i + "'>" + objvendor.Table4[i]["NetTotal"] +
                                "</td><td id='tdGst_" + i + "'>" + objvendor.Table4[i]["Gst"] + "</td></tr>");
                                }
                                glinterPurchasetbody.children().click(fncInterPurchaseRowClick);

                                var ddlCustomers = document.getElementById("ContentPlaceHolder1_DropDownLrNo");

                                //alert($("#<%=hidEdit.ClientID%>").val());

                                    if ($("#<%=hidEdit.ClientID%>").val() == '0') {
                                        var option1 = document.createElement("OPTION");
                                        option1.innerHTML = '--SELECT--';
                                        option1.value = '0';
                                        ddlCustomers.options.add(option1);
                                    }

                                    for (var i = 0; i < objvendor.Table4.length; i++) {
                                        var option = document.createElement("OPTION");

                                        //Set Customer Name in Text part.
                                        option.innerHTML = objvendor.Table4[i]["LRNo"];

                                        //Set CustomerId in Value part.
                                        option.value = objvendor.Table4[i]["LRSerialNo"];

                                        //Add the Option element to DropDownList.
                                        ddlCustomers.options.add(option);

                                        $('#<%=txtLRBaleQty.ClientID %>').val(objvendor.Table4[i]["BaleQty"]);

                                    $('#<%=hidLRSerial.ClientID %>').val(objvendor.Table4[i]["LRSerialNo"]);

                                }

                            }
                        }

                        if (numofTable > 5) {

                            if (objvendor.Table5.length > 0) {
                                for (var i = 0; i < objvendor.Table5.length; i++) {
                                    glLorrytrantbody.append("<tr><td>" + objvendor.Table5[i]["LRDate"] + "</td><td>" +
                                objvendor.Table5[i]["LRNO"] + "</td><td>" + objvendor.Table5[i]["LRSerialNo"] + "</td><td>" +
                                objvendor.Table5[i]["LRAmount"] + "</td><td>" + objvendor.Table5[i]["LRReceiptDate"] + "</td></tr>");
                                }
                            }
                        }



                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Clear All Text Control
        function fncClearAll() {
            try {

                glpoListtbody.children().remove();
                glpurRerReqtbody.children().remove();
                glinterPurchasetbody.children().remove();
                $("#tblPoSelected tbody").children().remove();

                $("#<%=hidPoNo.ClientID%>").val('');
                arrPONo = [];

                $('#<%=txtvendorcode.ClientID %>').val('');
                $('#<%=txtvendorname.ClientID %>').val('');

                $('#<%=txtGA.ClientID %>').val('');
                $('#<%=txtAddress1.ClientID %>').val('');
                $('#<%=txtTINNo.ClientID %>').val('');
                $('#<%=txtBillNo.ClientID %>').val('');
                $('#<%=txtBillDate.ClientID %>').val($.datepicker.formatDate('dd/mm/yy', new Date()));
                $('#<%=txtBasicAmt.ClientID %>').val('0.00');
                $('#<%=txtSchemeDis.ClientID %>').val('0.00');
                $('#<%=txtTaxAmount.ClientID %>').val('0.00');
                $('#<%=txtBillAmt.ClientID %>').val('0.00');
                $('#<%=txtAddDed.ClientID %>').val('0.00');
                $('#<%=txtTotalPayable.ClientID %>').val('0.00');
                $('#<%=txtRemarks.ClientID %>').val('');
                $('#<%=txtGSTType.ClientID %>').val('');

                $("#<%=ddlHqCode.ClientID %>").prop("disabled", false);
                $("#<%=txtvendorname.ClientID %>").prop("disabled", false);
                $("#<%=ddlHqCode.ClientID %>").trigger("liszt:updated");

                $("#<%=txtvendorname.ClientID %>").focus();





            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Clear All Text Control
        function fncClearAllLR() {
            try {

                $("#<%=hidPoNo.ClientID%>").val('');
                $('#<%=txtLorryNo.ClientID %>').val('');
                $('#<%=txtLorryAmount.ClientID %>').val('');
                $('#<%=txtLorryNo.ClientID %>').val('');
                $('#<%=txtVehicleNo.ClientID %>').val('');
                $('#<%=txtLrvendorcode.ClientID %>').val('');
                $('#<%=txtLrvendorname.ClientID %>').val('');
                $('#<%=txtReceiptDate.ClientID %>').val($.datepicker.formatDate('dd/mm/yy', new Date()));
                $('#<%=txtLRDate.ClientID %>').val($.datepicker.formatDate('dd/mm/yy', new Date()));

                $('#<%=txtBaleQty.ClientID %>').val('0');
                $('#<%=txtDeclareValue.ClientID %>').val('0.00');
                $('#<%=txtWeight.ClientID %>').val('0.00');

                $("#<%=DropDownLrNo.ClientID %>").trigger("liszt:updated");

                $("#<%=txtLrvendorname.ClientID %>").focus();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Validation
        function fncSaveValidation() {
            try {

               
                var billNo = $.trim($('#<%=txtBillNo.ClientID %>').val());
                if ($('#<%=ddlHqCode.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=ddlHqCode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.alert_Location%>');
                    return false;
                }
                else if ($('#<%=txtvendorcode.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtvendorcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Vendor%>');
                    return false;
                }
                else if (billNo == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBillNo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alter_BillNo%>');
                    return false;
                }
                else if ($('#<%=txtBillDate.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBillDate.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alter_BillNo%>');
                    return false;
                }
                else if (parseFloat($('#<%=txtBasicAmt.ClientID %>').val()) <= 0) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBasicAmt.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_BasicAmount%>');
                    return false;
                }
                else if ($('#<%=hidVendorBillNo.ClientID %>').val().indexOf(billNo) > -1 && $('#<%=txtGA.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBillNo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_DuplicateInvoiceNo%>');
                    return false;
                }
                else if ($('#<%=hidLRNo.ClientID %>').val() != "0")
                {
                    if ($('#<%=hidLRUpdate.ClientID %>').val() == "0")
                    {
                        $("#dialog-Lorry").dialog("open");
                        return false;
                    }
                    else
                    {
                        if ($('#<%=DropDownLrNo.ClientID %>').val() == "0") {
                            popUpObjectForSetFocusandOpen = $('#<%=DropDownLrNo.ClientID %>');
                            return false;
                        }                        
                    } 
                }
                else if ($('#<%=hidLRNo.ClientID %>').val() == "0") {

                    if ($('#<%=hidLRUpdate.ClientID %>').val() == "1") {
                         
                            alert($('#<%=hidLRNo.ClientID %>').val());
                            popUpObjectForSetFocusandOpen = $('#<%=DropDownLrNo.ClientID %>');
                            ShowPopupMessageBoxandFocustoObject("Select LR No.");
                            return false;
                   
                    }
                }

                
                $('#<%=lnkSave.ClientID %>').css("display", "none");
                $('#<%=hidPoNo.ClientID %>').val(arrPONo);
                return true;


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Lorry Receipt Save Validation
        function fncSaveValidationLR() {
            try {
                var billNo = $.trim($('#<%=txtBillNo.ClientID %>').val());

                if ($('#<%=DropDownTransport.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=DropDownTransport.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('Select Transport Name');
                    return false;
                }
                else if ($('#<%=txtLrvendorcode.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtvendorcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Vendor%>');
                    return false;
                }
                else if ($('#<%=txtLorryNo.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtLorryNo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('Enter LR No');
                    return false;
                }
                else if (parseFloat($('#<%=txtBaleQty.ClientID %>').val()) <= 0) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBaleQty.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('Enter Bale Qty');
                    return false;
                }



                $('#<%=lnkSave.ClientID %>').css("display", "none");
                //$('#<%=hidPoNo.ClientID %>').val(arrPONo);
                return true;


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //Amount Calculation
        function fncAmtCalculation() {
            try {
                var BasicAmt = 0, ShDiscount = 0, TaxAmt = 0, AddDed = 0, BillAmt = 0;

                if ($('#<%=txtBasicAmt.ClientID %>').val() == "")
                    $('#<%=txtBasicAmt.ClientID %>').val('0.00');

                if ($('#<%=txtTaxAmount.ClientID %>').val() == "")
                    $('#<%=txtTaxAmount.ClientID %>').val('0.00');

                if ($('#<%=txtSchemeDis.ClientID %>').val() == "")
                    $('#<%=txtSchemeDis.ClientID %>').val('0.00');

                if ($('#<%=txtAddDed.ClientID %>').val() == "")
                    $('#<%=txtAddDed.ClientID %>').val('0.00');

                BasicAmt = $('#<%=txtBasicAmt.ClientID %>').val();
                TaxAmt = $('#<%=txtTaxAmount.ClientID %>').val();
                ShDiscount = $('#<%=txtSchemeDis.ClientID %>').val();
                AddDed = $('#<%=txtAddDed.ClientID %>').val();

                BillAmt = parseFloat(BasicAmt) + parseFloat(TaxAmt) - parseFloat(ShDiscount);

                $('#<%=txtBillAmt.ClientID %>').val(BillAmt.toFixed(2));
                $('#<%=txtTotalPayable.ClientID %>').val((parseFloat(BillAmt) + parseFloat(AddDed)).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Initialize Save Dialog
        function fncInitializeSaveDialog(SaveMsg) {
            try {
                SaveMsg = SaveMsg + '<%=Resources.LabelCaption.alert_gaSave%>';
                $('#<%=lblgaReceiptSave.ClientID %>').text(SaveMsg);
                $("#GASave").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Save Dialog
        function fncSaveDialogClose() {
            $("#GASave").dialog('close');
            fncClearAll();
        }

        function fncPOListRowClick(source) {
            var rowobj, taxAmt, totalAmt, taxAmtgird, totalAmtgird;
            var poNo, poDate, poQty, netValue, mode;
            try {
                rowobj = $(source).parent().parent();

                taxAmt = $('#<%=txtTaxAmount.ClientID %>').val();
                totalAmt = $('#<%=txtTotalPayable.ClientID %>').val();


                taxAmtgird = rowobj.find('td[id*=tdPotax]').text();
                totalAmtgird = rowobj.find('td[id*=tdNetTotal]').text();
                poNo = rowobj.find('td[id*=tdPoNo]').text().trim();
                poDate = rowobj.find('td[id*=tdPoDate]').text();
                poQty = rowobj.find('td[id*=tdPoQty]').text();
                netValue = rowobj.find('td[id*=tdNetTotal]').text();

                if (($(source).is(":checked"))) {
                    taxAmt = parseFloat(taxAmt) + parseFloat(taxAmtgird);
                    totalAmt = parseFloat(totalAmt) + parseFloat(totalAmtgird);
                    mode = "Add";
                    arrPONo.push(poNo);

                }
                else {
                    taxAmt = parseFloat(taxAmt) - parseFloat(taxAmtgird);
                    totalAmt = parseFloat(totalAmt) - parseFloat(totalAmtgird);
                    mode = "Remove";
                    arrPONo.splice($.inArray(poNo, arrPONo), 1);
                }

                if (glpoListtbody.children().length == $("#tblPoList [id*=cbSelect]:checked").length) {
                    $("#tblPoList [id*=cbAllSelect]").attr("checked", "checked");
                }
                else {
                    $("#tblPoList [id*=cbAllSelect]").removeAttr("checked");
                }

                fncAssignvaluestotextbox(taxAmt, totalAmt);
                fncBindPOSelected(poNo, poDate, poQty, netValue, mode);

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBindPOSelected(poNo, poDate, poQty, netValue, mode) {
            var glpoSelectedtbody = $("#tblPoSelected tbody");
            var rowNo = 0, row;
            try {
                debugger;
                if (mode == "Remove") {
                    $("#tblPoSelected tbody").children().each(function () {
                        if ($(this).find('td[id*=tdPoNo]').text() == poNo) {
                            $(this).remove();
                            return;
                        }
                    });

                    rowNo = 0;
                    $("#tblPoSelected tbody").children().each(function () {
                        rowNo = parseInt(rowNo) + 1;
                        $(this).find('td [id*=tdRowNo]').text(rowNo);
                    });
                }
                else {
                    if (glpoSelectedtbody.children().length > 0) {
                        rowNo = $("#tblPoSelected tbody > tr").last().find('td[id*=tdRowNo]').text();
                    }
                    rowNo = parseInt(rowNo) + 1;

                    row = "<tr><td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                                          + "<td id='tdPoNo_" + rowNo + "'>" + poNo + "</td>"
                                          + "<td id='tdPoDate_" + rowNo + "'>" + poDate + "</td>"
                                          + "<td id='tdPoQty_" + rowNo + "' >" + poQty + "</td>"
                                          + "<td  id='tdNetTotal_" + rowNo + "'>" + netValue + "</td>"
                                          + "</tr>";
                    glpoSelectedtbody.append(row);
                }


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


<%--//Polist Row Click
function fncPoListRowClick() {
    try {
        var taxAmt = $(this).find('td[id*=tdPotax]').text();
        var totalAmt = $(this).find('td[id*=tdNetTotal]').text();
        $("#<%=hidPoNo.ClientID%>").val($(this).find('td[id*=tdPoNo]').text());
                fncAssignvaluestotextbox(taxAmt, totalAmt, this);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>

        //Inter Purchase Row Click
        function fncInterPurchaseRowClick() {
            try {
                var taxAmt = $(this).find('td[id*=tdGst]').html();
                var totalAmt = $(this).find('td[id*=tdNetTotal]').html();
                $("#<%=hidPoNo.ClientID%>").val($(this).find('td[id*=tdPoNo]').html());
                fncAssignvaluestotextbox(taxAmt, totalAmt, this);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Assign Po and InterPurchse Values to text box
        function fncAssignvaluestotextbox(taxAmt, totalAmt, source) {
            try {
                //$("#<%=txtBillNo.ClientID%>").val($("#<%=hidPoNo.ClientID%>").val());
                $("#<%=txtBasicAmt.ClientID%>").val((parseFloat(totalAmt) - parseFloat(taxAmt)).toFixed(2));
                $("#<%=txtTaxAmount.ClientID%>").val(parseFloat(taxAmt).toFixed(2));
                $("#<%=txtBillAmt.ClientID%>").val(parseFloat(totalAmt).toFixed(2));
                $("#<%=txtTotalPayable.ClientID%>").val(parseFloat(totalAmt).toFixed(2));

                //$(source).css("background-color", "#8080ff");
                //$(source).siblings().each(function () {
                //    $(source).css("background-color", "white");
                //});

                $("#<%=txtBillNo.ClientID%>").select();
            }
            catch (err) {

            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function isNumberKeywithMin(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 45 && charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }


        function fncShowVendorSearchDailog(event) {
            try {

                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

                if (keyCode == 13 || keyCode == 115 || keyCode == 117) {
                    return false;
                }
                else if (keyCode == 17) {
                    $("#<%=hidCombination.ClientID%>").val('ctrl_Yes');
                    return false;
                }
                else if ($("#<%=hidCombination.ClientID%>").val() == "ctrl_Yes" || keyCode == 65 || keyCode == 66) {
                    $("#<%=hidCombination.ClientID%>").val('ctrl_No');
                    return false;
                }


        setTimeout(function () {
            $('#<%=txtVendorSearch1.ClientID%>').val($('#<%=txtvendorname.ClientID%>').val());
            fncShowVendorSearch();
        }, 50);

    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncShowVendorSearchDailogLorry(event) {
    try {

        var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

        if (keyCode == 13 || keyCode == 115 || keyCode == 117) {
            return false;
        }
        else if (keyCode == 17) {
            $("#<%=hidCombination.ClientID%>").val('ctrl_Yes');
            return false;
        }
        else if ($("#<%=hidCombination.ClientID%>").val() == "ctrl_Yes" || keyCode == 65 || keyCode == 66) {
            $("#<%=hidCombination.ClientID%>").val('ctrl_No');
            return false;
        }


    setTimeout(function () {
        $('#<%=txtVendorSearch1.ClientID%>').val($('#<%=txtLrvendorname.ClientID%>').val());
        fncShowVendorSearch();
    }, 50);

}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

///Show Item Search Table
function fncShowVendorSearch() {
    try {
        $("#dialog-Vendor1").dialog({
            appendTo: 'form:first',
            resizable: false,
            height: "auto",
            width: 500,
            modal: true,
            title: "Vendor Search"
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

$(function () {
    $("[id$=txtVendorSearch1]").autocomplete({

        source: function (request, response) {
            var obj = {};
            obj.searchData = request.term.replace("'", "''");
            obj.mode = "Vendor";

            $.ajax({
                url: '<%=ResolveUrl("GoodsAcknowledgmentReceipt.aspx/fncCommonSearch")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            valitemcode: item.split('|')[1],
                            valAddress: item.split('|')[2]
                        }

                    }))

                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
        focus: function (event, i) {
            $('#<%=txtVendorSearch1.ClientID %>').val($.trim(i.item.valitemcode));
            $('#<%=txtvendorAddress.ClientID %>').val($.trim(i.item.valAddress));
            event.preventDefault();
        },
        select: function (e, i) {
            $("#dialog-Vendor1").dialog("destroy");
            $('#<%=txtvendorname.ClientID %>').val(i.item.label.split('-')[1]);
            $('#<%=txtvendorcode.ClientID %>').val($.trim(i.item.valitemcode));
            $('#<%=txtvendorAddress.ClientID %>').val("");
            $('#<%=txtBillNo.ClientID %>').focus();

            $('#<%=txtLrvendorname.ClientID %>').val(i.item.label.split('-')[1]);
            $('#<%=txtLrvendorcode.ClientID %>').val($.trim(i.item.valitemcode));
            if ($('#<%=txtLrvendorname.ClientID %>').val() != "")
                $('#<%=txtLorryNo.ClientID %>').focus();
            fncGetVendorDetail();
            return false;
        },
        appendTo: $("#dialog-Vendor1"),
        minLength: 2
    });
});

$(document).ready(function () {
    $(document).on('keydown', disableFunctionKeys);
});

//// Disable Function Keys
function disableFunctionKeys(e) {
    try {
        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
            if (e.keyCode == 115) {
                if (fncSaveValidation() == true) {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                }
            }
            else if (e.keyCode == 117) {
                fncClearAll();
            }

            e.preventDefault();
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncPoListHeaderclick(source) {
    try {
        if (($(source).is(":checked"))) {
            $("#tblPoList tbody").children().each(function () {
                $(this).find('td [id*=cbSelect]').attr("checked", "checked");
                var rowObj = $(this).find('td [id*=cbSelect]');
                fncPOListRowClick(rowObj);
            });
        }
        else {
            $("#tblPoList tbody").children().each(function () {
                $(this).find('td [id*=cbSelect]').removeAttr("checked");
                var rowObj = $(this).find('td [id*=cbSelect]');
                fncPOListRowClick(rowObj);
            });

        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li>
                    <a href="../Purchase/frmGoodsAcknowledgement.aspx">
                        <%=Resources.LabelCaption.lblViewGoodsAcknowledgement%>
                    </a>
                </li>
                <li class="active-page uppercase" id="breadcrumbs_text_GAR">
                    <%=Resources.LabelCaption.lblGoodsAcknowledgementReceipt%>
                </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="container-group-full">
            <div style="width: 100%; margin-bottom: 0px; display: table">
                <div class="panel panel-default" style="width: 100%; margin-bottom: 0px; display: table">
                    <div id="Tabs" role="tabpanel">
                        <ul class="nav nav-tabs custnav" role="tablist">
                            <li class="active ga_tab_header"><a href="#GAForm" aria-controls="GAForm" role="tab"
                                data-toggle="tab">GOODS ACKNOWLEDGEMENT RECEIPT</a></li>
                            <li class="ga_tab_header"><a href="#Lorry" aria-controls="Lorry" role="tab"
                                data-toggle="tab">LORRY RECEIPT</a></li>
                        </ul>
                        <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto;">
                            <div class="tab-pane active" role="tabpanel" id="GAForm">
                                <div class="left-container ga_left-container" style="width: 33%;">
                                    <div class="left-container-detail">
                                        <div class="left-container-header">
                                            <%=Resources.LabelCaption.lblGoodsAcknowledgementReceipt%>
                                        </div>
                                        <div class="left-top-container" style="width: 100%; margin-top: 5px;">
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_GANumber %>'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtGA" runat="server" CssClass="form-control-res" onmouseDown="return false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lblLocationCode %>'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:DropDownList ID="ddlHqCode" runat="server" CssClass="form-control-res" onchange="fncGetVendorDetail();return false;">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label3" runat="server" Text='Vendor Code'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtvendorcode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="lblVendorname" runat="server" Text='Vendor Name'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtvendorname" runat="server" onkeydown="return fncShowVendorSearchDailog(event);" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="control-group-single" style="display: none;">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblVendorAddress %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control-res" TextMode="MultiLine"
                                                        Height="50px" onmouseDown="return false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label27" runat="server" Text="LR NO"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:DropDownList ID="DropDownLrNo" runat="server" onchange="SetSelectedText(this)" CssClass="form-control-res">
                                                        <%-- <asp:ListItem Value="0">--Select--</asp:ListItem>--%>
                                                        <%-- onchange="fncGetVendorDetail();return false;">--%>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblTinNo %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtTINNo" runat="server" CssClass="form-control-res" onmouseDown="return false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="lblGstType" runat="server" Text='<%$ Resources:LabelCaption,lbl_GstType %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtGSTType" runat="server" CssClass="form-control-res" onmouseDown="return false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblGADate %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtGADate" runat="server" CssClass="form-control-res" onmouseDown="return false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblBillNo %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control-res" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBillDate %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtBillDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lblBasicAmount %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtBasicAmt" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)"
                                                        onblur="fncAmtCalculation()" MaxLength="10">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblSchemeDisc %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtSchemeDis" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)"
                                                        onblur="fncAmtCalculation()" MaxLength="10">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lblTAX %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtTaxAmount" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)"
                                                        onblur="fncAmtCalculation()" MaxLength="10">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lblBillAmount %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtBillAmt" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)"
                                                        onblur="fncAmtCalculation()" MaxLength="10">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblAddDed %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtAddDed" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKeywithMin(event)"
                                                        onblur="fncAmtCalculation()" MaxLength="10">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblTotalPayable %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtTotalPayable" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-container" style="margin-right: 150px">
                                        <div class="control-button">
                                            <asp:UpdatePanel ID="uplnksave" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return fncSaveValidation();"
                                                        OnClick="lnkSave_Click" Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="fncClearAll();return false"
                                                Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-righttab ga-right-container">
                                    <div style="width: 100%; margin-bottom: 0px; margin-left: 10px; display: table">
                                        <div class="panel panel-default" style="width: 100%; margin-bottom: 0px; display: table">
                                            <div id="Tabs" role="tabpanel">
                                                <ul class="nav nav-tabs custnav" role="tablist">
                                                    <li class="active ga_tab_header"><a href="#POList" aria-controls="POList" role="tab"
                                                        data-toggle="tab">PO List</a></li>
                                                    <li class="ga_tab_header"><a href="#POSelected" aria-controls="POList" role="tab"
                                                        data-toggle="tab">PO Selected</a></li>
                                                    <li class="ga_tab_header"><a href="#PurchaseReturnReq" aria-controls="PurchaseReturnReq"
                                                        role="tab" data-toggle="tab">PurchaseReturnReq</a></li>
                                                    <li class="ga_tab_header"><a href="#InterPurchase" aria-controls="InterPurchase"
                                                        role="tab" data-toggle="tab">InterPurchase</a></li>
                                                </ul>
                                                <div class="tab-content" style="padding-top: 5px; overflow: auto; height: 500px">
                                                    <div class="tab-pane active" role="tabpanel" id="POList">
                                                        <div class="Payment_fixed_headers rptr_polist">
                                                            <table id="tblPoList" cellspacing="0" rules="all" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">S.No
                                                                        </th>
                                                                        <th scope="col">
                                                                            <input id="cbAllSelect" type="checkbox" onclick="fncPoListHeaderclick(this);" />
                                                                        </th>
                                                                        <th scope="col">PO No
                                                                        </th>
                                                                        <th scope="col">PO Date
                                                                        </th>
                                                                        <th scope="col">No.Items
                                                                        </th>
                                                                        <th scope="col">NetValue
                                                                        </th>
                                                                        <th scope="col">Tax
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" role="tabpanel" id="POSelected">
                                                        <div class="Payment_fixed_headers  rptr_poselected">
                                                            <table id="tblPoSelected" cellspacing="0" rules="all" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">S.No
                                                                        </th>
                                                                        <th scope="col">PO No
                                                                        </th>
                                                                        <th scope="col">PO Date
                                                                        </th>
                                                                        <th scope="col">No.Items
                                                                        </th>
                                                                        <th scope="col">NetValue
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" role="tabpanel" id="PurchaseReturnReq">
                                                        <div class="Payment_fixed_headers  rptr_PurchaseRetReq">
                                                            <table id="tblPurRetReq" cellspacing="0" rules="all" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">S.No
                                                                        </th>
                                                                        <th scope="col">InventoryCode
                                                                        </th>
                                                                        <th scope="col">Description
                                                                        </th>
                                                                        <th scope="col">MRP
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" role="tabpanel" id="InterPurchase">
                                                        <div class="tab-pane" role="tabpanel" id="Div1">
                                                            <div class="Payment_fixed_headers rptr_InterPurchase">
                                                                <table id="tblInterPurchase" cellspacing="0" rules="all" border="1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">S.No
                                                                            </th>
                                                                            <th scope="col">Invoice No
                                                                            </th>
                                                                            <th scope="col">Invoice Date
                                                                            </th>
                                                                            <th scope="col">No.Items
                                                                            </th>
                                                                            <th scope="col">NetValue
                                                                            </th>
                                                                            <th scope="col">Tax
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" role="tabpanel" id="Lorry">
                                <div class="left-container ga_left-container" style="width: 33%;">
                                    <div class="left-container-detail">
                                        <div class="left-container-header">
                                            Lorry Receipt                                          
                                        </div>
                                        <div class="left-top-container" style="width: 100%; margin-top: 5px;">
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label7" runat="server" Text="LR Serial.No"
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLRSerialNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label33" runat="server" Text="Date"
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtReceiptDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label17" runat="server" Text="TransPort Name"
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:DropDownList ID="DropDownTransport" runat="server" CssClass="form-control-res" onchange="fncGetVendorDetail();return false;">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label18" runat="server" Text='Vendor Code'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLrvendorcode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label19" runat="server" Text='Vendor Name'
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLrvendorname" runat="server" onkeydown="return fncShowVendorSearchDailog(event);" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label21" runat="server" Text="LR No"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLorryNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label22" runat="server" Text="LR Date"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLRDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label20" runat="server" Text="LR Amount"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtLorryAmount" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control-res">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_right" style="float: right;">
                                                    <asp:CheckBox ID="chkPayto" runat="server" Text="To Pay" Class="radioboxlistgreen" />
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label23" runat="server" Text="Vehicle No"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtVehicleNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label25" runat="server" Text="Bale Qty"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtBaleQty" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control-res-right">0</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label26" runat="server" Text="Weight(KG)"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label24" runat="server" Text="Declared Value"
                                                        Font-Bold="True"></asp:Label>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:TextBox ID="txtDeclareValue" runat="server" CssClass="form-control-res-right" onkeypress="return isNumberKey(event)">0.00</asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-container" style="margin-right: 150px">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkSaveLR" runat="server" class="button-red" OnClientClick="return fncSaveValidationLR();"
                                                Text='<%$ Resources:LabelCaption,btnSave %>' OnClick="lnkSaveLR_Click"></asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearLR" runat="server" class="button-red" OnClientClick="fncClearAllLR();return false"
                                                Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-righttab ga-right-container">
                                    <div style="width: 100%; margin-bottom: 0px; margin-left: 10px; display: table">
                                        <div class="panel panel-default" style="width: 100%; margin-bottom: 0px; display: table">
                                            <div id="Tabs" role="tabpanel">
                                                <ul class="nav nav-tabs custnav" role="tablist">
                                                    <li class="active ga_tab_header"><a href="#Transaction" aria-controls="Transaction" role="tab"
                                                        data-toggle="tab">Transaction</a></li>
                                                    <%--      <li class="ga_tab_header"><a href="#LRStatus" aria-controls="LRStatus" role="tab"
                                                        data-toggle="tab">Lorry Status</a></li>--%>
                                                </ul>
                                                <div class="tab-content" style="padding-top: 5px; overflow: auto; height: 500px">

                                                    <div class="tab-pane active" role="tabpanel" id="Transaction">
                                                        <div class="Payment_fixed_headers rptr_poselected" style="width: auto;">
                                                            <table id="tblLRTrans" cellspacing="0" rules="all" border="1">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">LR Date
                                                                        </th>
                                                                        <th scope="col">LR.No 
                                                                        </th>
                                                                        <th scope="col">SERIAL NO
                                                                        </th>
                                                                        <th scope="col">LR Amount
                                                                        </th>
                                                                        <th scope="col">SERIAL NO DATE
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                                <tfoot>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <%--<div class="tab-pane" role="tabpanel" id="LRStatus">
                                                        <div class="Payment_fixed_headers  rptr_poselected">
                                                           <asp:GridView ID="grdLRStatus" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <EditRowStyle BackColor="#2461BF" />
                                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                <RowStyle BackColor="#EFF3FB" />
                                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hiddencol2">
                <div id="dialog-Lorry" class="dialog-inv-tax">
                    <div class="dialog-Inv-tax-header">
                        Update Lorry Receipt Status
                    </div>
                    <div class="dialog-Inv-tax-detail">
                        <div class="control-group-split">
                            <div class="control-group-left1 inv_purchaseby_width">
                                <div class="label-left">
                                    <asp:Label ID="Label95" runat="server" Text="Received Bale Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtReceivedQty" runat="server" onblur="fncQtyFocusOut(this)" onkeypress="return isNumberKey(event)" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-middle inv_purchaseby_width">
                                <div class="label-left">
                                </div>
                                <div class="label-right">
                                    <asp:CheckBox ID="chkPaidstatus" runat="server" Text="Paid Status" Class="radioboxlistgreen" />
                                </div>
                            </div>
                            <div class="control-group-right1 inv_purchasebyPartial_width">
                                <div class="label-left">
                                    <asp:Label ID="Label28" runat="server" Text="LR Status"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlLRStatus" runat="server" CssClass="form-control-res">
                                        <asp:ListItem Text="Pending" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Partialy Received" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Received" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Lost" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dialog-Inv-tax-detail">
                        <div class="control-group-split">
                            <div class="control-group-left1 inv_purchaseby_width">
                                <div class="label-left">
                                    <asp:Label ID="Label29" runat="server" Text="Wages Amount"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtWagesAmt" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-middle inv_purchaseby_width">
                                <div class="label-left">
                                    <asp:Label ID="Label30" runat="server" Text="LR Location"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLRLocation" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right1 inv_purchasebyPartial_width">

                                <div class="label-left">
                                    <asp:Label ID="Label32" runat="server" Text="Bale Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLRBaleQty" runat="server" onmouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dialog-Inv-tax-detail">
                        <div class="control-group-split">
                            <div class="control-group-left1 inv_purchaseby_width">

                                <div class="label-right">
                                </div>
                            </div>

                            <div class="control-group-middle" style="width: 600px;">
                                <div class="label-left">
                                    <asp:Label ID="Label31" runat="server" Text="Remarks"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLRRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="hiddencol">
        <asp:HiddenField ID="hidPoNo" runat="server" />
        <asp:HiddenField ID="hidVendorBillNo" runat="server" />
        <asp:HiddenField ID="hidbillType" runat="server" />
        <asp:HiddenField ID="hideActualBillMode" runat="server" />
        <asp:HiddenField ID="hidCombination" runat="server" Value="ctrl_No" />
        <asp:HiddenField ID="hidLRNo" runat="server" Value="0" />
        <asp:HiddenField ID="hidEdit" runat="server" Value="0" />
        <asp:HiddenField ID="hidLRSerial" runat="server" Value="0" />
        <asp:HiddenField ID="hidLRUpdate" runat="server" Value="0" />


        <div id="GASave">
            <div>
                <asp:Label ID="lblgaReceiptSave" runat="server"></asp:Label>
            </div>
            <div class="dialog_center">
                <asp:UpdatePanel ID="upsave" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                            OnClientClick="fncSaveDialogClose()" OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="dialog-MessageBoxGAReceipt" style="display: none">
        </div>
        <div id="dialog-Vendor1" style="display: none">
            <div>
                <div class="float_left">
                    <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                </div>
                <div class="gidItem_Search">
                    <asp:TextBox ID="txtVendorSearch1" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="vendor_Searchaddress">
                <asp:TextBox ID="txtvendorAddress" runat="server" TextMode="MultiLine"
                    class="form-control-res vendor_Searchaddress_height"></asp:TextBox>
            </div>

        </div>

    </div>

</asp:Content>
