﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmConsolidatedPOCreation.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmConsolidatedPOCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .ConPOCreation td:nth-child(1), .ConPOCreation th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .ConPOCreation td:nth-child(2), .ConPOCreation th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .ConPOCreation td:nth-child(3), .ConPOCreation th:nth-child(3) {
            min-width: 447px;
            max-width: 447px;
        }

        .ConPOCreation td:nth-child(4), .ConPOCreation th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .ConPOCreation td:nth-child(5), .ConPOCreation th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(6), .ConPOCreation th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(6) {
            text-align: right !important;
            padding-right: 5px;
        }

        .ConPOCreation td:nth-child(7), .ConPOCreation th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(7) {
            text-align: right !important;
            padding-right: 5px;
        }

        .ConPOCreation td:nth-child(8), .ConPOCreation th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(8) {
            text-align: right !important;
            padding-right: 5px;
        }

        .ConPOCreation td:nth-child(9), .ConPOCreation th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(9) {
            text-align: right !important;
            padding-right: 5px;
        }

        .ConPOCreation td:nth-child(10), .ConPOCreation th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(10) {
            text-align: right !important;
            padding-right: 5px;
        }

        .ConPOCreation td:nth-child(11), .ConPOCreation th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .ConPOCreation td:nth-child(11) {
            text-align: right !important;
            padding-right: 5px;
        }

        .poSummary td:nth-child(1), .poSummary th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .poSummary td:nth-child(2), .poSummary th:nth-child(2) {
            min-width: 400px;
            max-width: 400px;
        }

        .poSummary td:nth-child(3), .poSummary th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(3) {
            text-align: right !important;
            padding-right: 5px;
        }

        .poSummary td:nth-child(4), .poSummary th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(4) {
            text-align: right !important;
            padding-right: 5px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'ConsolidatedPOCreation');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "ConsolidatedPOCreation";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        ///Show Item Search Table
        function fncShowVendorDetail() {
            try {
                $("#venSummary").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true,
                    title: "PO Detail Summary",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                    if ($('#<%=lnkCreatePO.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkCreatePO', '');
                        e.preventDefault();
                        return false;
                    }
                    else if (e.keyCode == 119) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkShowVendDet', '');
                        e.preventDefault();
                        return false;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkCreatePO.ClientID %>').css("display", "block");
                 }
                 else {
                     $('#<%=lnkCreatePO.ClientID %>').css("display", "none");
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Consolidated PO</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full" style="padding-left: 10px;">
            <div style="display: table">
                <div class="control-button">
                    <asp:LinkButton ID="lnkShowVendDet" runat="server" class="button-blue" OnClientClick="fncShowVendorDetail();return false;"
                        Text="Summary (F8)"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkCreatePO" runat="server" class="button-blue"
                        Text="Proceed (F4)" OnClick="lnkCreatePO_Click"></asp:LinkButton>
                </div>
                <div class="control-button" style="padding-left: 10px;">
                    <asp:CheckBox ID="cbMappedVendor" runat="server" Checked="true" Text="Mapped Vendor" class="radioboxlist" AutoPostBack="true" OnCheckedChanged="cbMappedVendor_changed" />
                </div>
            </div>
            <div class="over_flowhorizontal_New">
                <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header ConPOCreation">
                    <tr>
                        <th>S.No</th>
                        <th>Itemcode</th>
                        <th>Description</th>
                        <th>Size</th>
                        <th>W.Qty</th>
                        <th>ReqQty</th>
                        <th>OrderQty</th>
                        <th>Stock</th>
                        <th>MRP</th>
                        <th>Netcost</th>
                        <th>GST(%)</th>
                    </tr>
                </table>
                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 430px; width: 1345px; background-color: aliceblue;">
                    <asp:GridView ID="gvConsolidatedPO" runat="server" AllowSorting="True" Width="100%" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="True" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff" ShowHeader="false"
                        CssClass="pshro_GridDgn ConPOCreation">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                        <PagerStyle CssClass="pshro_text" />
                        <Columns>
                            <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                            <asp:BoundField DataField="InventoryCode" HeaderText="Itemcode"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                            <asp:BoundField DataField="Compatible" HeaderText="Size"></asp:BoundField>
                            <asp:BoundField DataField="WQty" HeaderText="W.Qty"></asp:BoundField>
                            <asp:BoundField DataField="ReqQty" HeaderText="ReqQty"></asp:BoundField>
                            <asp:BoundField DataField="OrderQty" HeaderText="OrderQty"></asp:BoundField>
                            <asp:BoundField DataField="QtyOnHand" HeaderText="Stock"></asp:BoundField>
                            <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                            <asp:BoundField DataField="NetCost" HeaderText="Netcost"></asp:BoundField>
                            <asp:BoundField DataField="ITaxPer3" HeaderText="GST(%)"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <div id="venSummary" class="over_flowhorizontal">
            <table rules="all" border="1" id="Table1" runat="server" class="fixed_header poSummary">
                <tr>
                    <th>S.No</th>
                    <th>VendorName</th>
                    <th>No Of Item</th>
                    <th>Total</th>
                </tr>
            </table>
            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 300px; width: 668px; background-color: aliceblue;">
                <asp:GridView ID="gvVendorDet" runat="server" AllowSorting="True" Width="100%" AutoGenerateColumns="False" ShowHeader="false"
                    ShowHeaderWhenEmpty="True" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                    CssClass="pshro_GridDgn poSummary">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <Columns>
                        <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                        <asp:BoundField DataField="VendorName" HeaderText="VendorName"></asp:BoundField>
                        <asp:BoundField DataField="NoOfItem" HeaderText="No Of Item"></asp:BoundField>
                        <asp:BoundField DataField="totalvalue" HeaderText="Total" DataFormatString="{0:0.00}"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
