﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseOrderImage.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrderImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
           .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        #imgViewer {
            display: block;
            overflow: auto;
            height: 310px;
        }

        input[type="file"] {
            display: block;
        }

        .imageThumb {
            width: 130px;
            height: 100px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
        }

        .pip {
            display: inline-block;
            margin: 0 1px;
        }

        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

            .remove:hover {
                background: white;
                color: black;
            }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'PurchaseOrderImage');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "PurchaseOrderImage";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">

        function showpreview(source) {
            readURL(source);
        }

        function readURL(input) {
            if (input.files && input.files.length > 0) {
                var Count = 0;
                for (var i = 0 ; i < input.files.length; i++) {
                    var reader = new FileReader();
                    reader.onload = function (e) {

                        // Add Multiple Images for Single Browse
                        //$('#imgViewer').attr('src', e.target.result);
                        //$('#imgViewer').append($('<img>', { src: e.target.result, width: '100px', height: '100px' }));

                        var fp = $("#ContentPlaceHolder1_FileUpload1");
                        var items = fp[0].files;
                        var Name = items[Count].name;
                        var Size = items[Count].size;
                        var Type = items[Count].type;
                        Size = parseFloat(Size) * parseFloat(0.001);

                        $("<span class=\"pip\">" +
                        "<img class=\"imageThumb\" src=\"" + e.target.result + "\" name = \"" + Name + "\" title = \"" + Size.toFixed(2) + " Kb" + "\"/>" +
                        "<br/><span class=\"remove\">Remove image</span>" +
                        "</span>").appendTo("#imgViewer");

                        Count = Count + 1;

                        // Remove Image 
                        $(".remove").click(function () {
                            $(this).parent(".pip").remove();
                        });
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        function fncSetNameToHidden() {
            var byteData = "";
            var length = $("#imgViewer img").length;
            for (var i = 0; i < length; i++) {

                if (byteData == '') {
                    byteData = $('#imgViewer').find('.pip').eq(i).find('.imageThumb').attr('src').split(';')[1].replace("base64,", "");
                    byteData = byteData + "@" + $('#imgViewer').find('.pip').eq(i).find('.imageThumb').attr('name');
                }
                else {
                    byteData = byteData + "|" + $('#imgViewer').find('.pip').eq(i).find('.imageThumb').attr('src').split(';')[1].replace("base64,", "");
                    byteData = byteData + "@" + $('#imgViewer').find('.pip').eq(i).find('.imageThumb').attr('name');
                }
                console.log($('#imgViewer').find('.pip').eq(i).find('.imageThumb').attr('name'));
            }
            $('#<%=hdfFileUploadName.ClientID %>').val(byteData);
        }

        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupBatchInfo').dialog('close');
                window.parent.$('#popupBatchInfo').remove();
            }
        });

        function ValidateSave() {

            var Show = '';
            if (document.getElementById("<%=txtImageGroupName.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Image Group Name';
                document.getElementById("<%=txtImageGroupName.ClientID %>").focus();
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                fncSetNameToHidden();
                return true;
            }
        }

    </script>

    <script type="text/javascript">

        //================================> After Common Search
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>

    <script type="text/javascript">

        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                window.parent.jQuery('#PopupPoImage').dialog('close');
                window.parent.$('#PopupPoImage').remove();
            }
        });

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>

            <div class="main-container">
                <div class="breadcrumbs">
                    <ul><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        </ul></div>
                <div class="container-image">

                    <div style="margin-left: 15px">
                        <span style="font: bold; font-size: 18px; color: red">Po No : </span>
                        <asp:Label ID="lblPoNo" runat="server" Text="" Style="font: bold; font-size: 18px"></asp:Label>
                    </div>

                    <div class="top-purchase-container top-padding">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text="Image Group Name"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtImageGroupName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label1" runat="server" Text="Image"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" onchange="showpreview(this);" />
                                            <asp:HiddenField ID="hdfFileUploadName" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-container">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return ValidateSave()" OnClick="lnkSave_Click" Text="Save"></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text="Clear" OnClientClick="clearForm()"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="clear: both">
                                <div id="imgViewer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
