﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGRNImage.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmGRNImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        #dialog > img {
            width: 100% !important;
            height: 100% !important;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GRNImage');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GRNImage";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">

        //$(document).keyup(function (e) {
        //    if (e.keyCode == 27) {
        //        alert($('#dialog').dialog('isOpen'));
        //        if ($('#dialog').dialog('isOpen') == false) {
        //            window.parent.jQuery('#PopupGRNImage').dialog('close');
        //            window.parent.$('#PopupGRNImage').remove();
        //        }
        //    }
        //});

    </script>

    <script type="text/javascript">

        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                modal: true,
                height: 480,
                width: 850,
                title: "Zoomed Image",
                buttons: {
                    "Print": function () {
                        PrintImage(this);
                    }
                }
            });
            $("[id*=gvGRNImages] img").click(function () {
                $('#dialog').html('');
                $('#dialog').append($(this).clone());
                $('#dialog').dialog('open');
            });
        });

        function PrintImage(source) {           
            printWindow = window.open("", "mywindow", "location=1,status=1,scrollbars=1,height=400,width=800");
            printWindow.document.write("<div style='width:100%;'>");
            printWindow.document.write("<img id='img' height=400,width=800 src='" + document.getElementById($(source).find('img').attr("id")).src + "'/>");
            printWindow.document.write("</div>");
            printWindow.document.close();
            printWindow.print();
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
         <div class="breadcrumbs">
                <ul><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
             </div>
        <div class="container-image">
            <div class="top-purchase-container top-padding">
                <div class="control-group-single-res" style="width: 50%">
                    <div class="label-left">
                        <asp:Label ID="Label2" runat="server" Text="Group Name"></asp:Label>
                    </div>
                    <div class="label-right">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="height: 412px; overflow: auto">

                <asp:GridView ID="gvGRNImages" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="True" ShowHeader="True"
                    CssClass="pshro_GridDgn" OnRowDataBound="gvGRNImages_OnRowDataBound">
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <Columns>
                        <asp:BoundField DataField="SNo" HeaderText="SNo"></asp:BoundField>
                        <asp:BoundField DataField="CategoryCode" HeaderText="Group Name"></asp:BoundField>
                        <asp:BoundField DataField="ImageName" HeaderText="ImageName" ItemStyle-CssClass="left_align"></asp:BoundField>
                        <asp:TemplateField HeaderText="Image">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" Width="200px" Height="100px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div id="dialog" style="display: none">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
