﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmDisplayandContractsAdd.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmDisplayandContractsAdd" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .control-group-single .label-left
        {
            width: 40%;
        }
        
        .control-group-single .label-right
        {
            width: 60%;
        }
        
    /*.fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
     }*/


    .no-close .ui-dialog-titlebar-close {
            display:none;
     }
    </style>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'DisplayandContractsAdd');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "DisplayandContractsAdd";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>

    <script type="text/javascript" language="Javascript">

        var fromdatectrl, todatectrl;

        function pageLoad() {
            try {
                <%--$('#<%=txtDisAmt.ClientID %>').val(0);
                $('#<%=txtxDisPerc.ClientID %>').val(0);--%>
                 if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                <%--$('#<%=lnkUpdate.ClientID %>').css("display", "block");--%>
             }
             else {
                 $('#<%=lnkUpdate.ClientID %>').css("display", "none");
             }
                //setAutocomplete();
                if ($('#<%=txtFromDate.ClientID %>').val() == "") {
                    fromdatectrl = $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                   
                    todatectrl = $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                }
                else {
                    fromdatectrl = $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                    todatectrl = $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function isNumberKeyDiscPerc(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) { 
                DiscPercCalculation();
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKeyAmt(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                var Amount = $('#<%=txtamt.ClientID %>').val();
                var DisPer = $('#<%=txtxDisPerc.ClientID %>').val();
                if (Amount == '0' || Amount == '') {
                    alert("Invalid Amount");
                } else {
                    if (DisPer != '') {
                        var Disamt = parseFloat((Amount)) * parseFloat((DisPer)) / 100;
                        $('#<%=txtDisAmt.ClientID %>').val(Disamt);
                        $('#<%=txttotalamt.ClientID %>').val(parseFloat(Amount) - parseFloat(Disamt));
                        $('#<%=txtxDisPerc.ClientID %>').select();
                        return false;
                    }
                }
            }
        }
        function DiscPercCalculation() {
            var Amount = $('#<%=txtamt.ClientID %>').val();
            var DisPer = $('#<%=txtxDisPerc.ClientID %>').val();
            if (Amount == '0' || Amount == '') {
                alert("Invalid Amount");
            } else {
                if (DisPer != '') {
                    var Disamt = parseFloat((Amount)) * parseFloat((DisPer)) / 100;
                    $('#<%=txtDisAmt.ClientID %>').val(Disamt);
                    $('#<%=txttotalamt.ClientID %>').val(parseFloat(Amount) - parseFloat(Disamt));
                    $('#<%=txtDisAmt.ClientID %>').select();
                    return false;
                } else {
                    ShowPopupMessageBox("Please Enter Disc(%)");
                }
            }
        }
        function isNumberKeyDiscAmt(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) { 
                DisAmtCalc();
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function DisAmtCalc() {

            var Amount = $('#<%=txtamt.ClientID %>').val();
            //var DisPer = $('#<%=txtxDisPerc.ClientID %>').val();
            var Disamt = $('#<%=txtDisAmt.ClientID %>').val();
            if (Amount == '0' || Amount == '') {
                alert("Invalid Amount");
            } else {
                if (Disamt != '') {
                    // $('#<%=txtDisAmt.ClientID %>').val(Disamt);
                    $('#<%=txttotalamt.ClientID %>').val(parseFloat(Amount) - parseFloat(Disamt));
                    $('#<%=txtxDisPerc.ClientID %>').val((parseFloat(Disamt) / parseFloat(Amount)) * 100);
                    $('#<%=txttotalamt.ClientID %>').focus();

                    return false;
                } else {
                    alert("Please Enter DiscAmt")
                    return false;
                }
            }
        }
        function fncQtyFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=lnkAdd.ClientID %>").focus();
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncClear(event) {
            try {

                $('#<%=txtInventoryCode.ClientID %>').val('');
                $('#<%=txtItemNameAdd.ClientID %>').val('');
                $('#<%=txtMrp.ClientID%>').val('');
                $('#<%=txtSellingPrice.ClientID%>').val('');
                $('#<%=txtBatchno.ClientID%>').val('');
                $('#<%=txtQty.ClientID%>').val('');
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtInventoryCode.ClientID %>').val(itemcode);
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                alert(err.Message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCodeEnter(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    <%--$("#ContentPlaceHolder1_searchFilterUserControl_txtItemCode").val($("#<%=txtInventoryCode.ClientID %>").val());--%>
                <%--$('#ContentPlaceHolder1_searchFilterUserControl_txtItemCode').val($("#<%=txtInventoryCode.ClientID %>").val());--%>

                <%--$("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($("#<%=txtInventoryCode.ClientID %>").val());--%>
                    DispalyContract = "1";
                    fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }
        //Get BatchStatus
        function fncInventoryCodeEnterkey_Master_Display() {
            try {
                DispalyContract = "1";
                fncGetBatchStatus_Master($('#<%=txtInventoryCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                alert(err.Message);
            }

        }

        //Get Inventory Detail  
        function fncGetBatchStatus_Display() {
            try {
                DispalyContract = "1";
                fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
            }
            catch (err) {
                alert(err.Message);
            }
        }

        //Assign values to text box
        function fncAssignInventoryDetailToControls_Master_Display(inventorydata) {
            try {
                //alert(inventorydata[9]);
                $('#<%=txtItemNameAdd.ClientID%>').val(inventorydata[0]);
                $('#<%=hidvendorcode.ClientID %>').val(inventorydata[9]);


                if ($('#<%=hidisbatch.ClientID%>').val() == "1") {
                    fncGetBatchDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
                }
                else {
                    $('#<%=txtSellingPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtMrp.ClientID%>').val(inventorydata[6]);
                    $('#<%=txtQty.ClientID%>').focus();

                }

            }
            catch (err) {
                alert(err.message);
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock) {
            try {

                $('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtSellingPrice.ClientID%>').val(sprice);
                $('#<%=txtBatchno.ClientID%>').val(batchno);
                $('#<%=txtQty.ClientID%>').focus();

            }
            catch (err) {
                alert(err.Messagek);
            }
        }
        function EnableOrDisableDropdown(element, isEnable) {
            //alert(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        $(function () {


            var Deparmentctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DepartmentDropDown).ClientID %>");

            var Categoryctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.CategoryDropDown).ClientID %>");

            var Brandctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BrandDropDown).ClientID %>");



            Deparmentctrl.on("change", function () {
                //alert(Deparmentctrl[0].selectedIndex)
                if (Deparmentctrl[0].selectedIndex == 0) {
                    EnableOrDisableDropdown(Categoryctrl, false);
                    EnableOrDisableDropdown(Brandctrl, false);
                } else {
                    EnableOrDisableDropdown(Categoryctrl, true);
                    EnableOrDisableDropdown(Brandctrl, true);
                }

            });
            Categoryctrl.on("change", function () {
                //alert(Deparmentctrl[0].selectedIndex)
                if (Categoryctrl[0].selectedIndex == 0) {
                    EnableOrDisableDropdown(Deparmentctrl, false);
                    EnableOrDisableDropdown(Brandctrl, false);
                } else {
                    EnableOrDisableDropdown(Deparmentctrl, true);
                    EnableOrDisableDropdown(Brandctrl, true);
                }

            });
            Brandctrl.on("change", function () {
                //alert(Deparmentctrl[0].selectedIndex)
                if (Brandctrl[0].selectedIndex == 0) {
                    EnableOrDisableDropdown(Deparmentctrl, false);
                    EnableOrDisableDropdown(Categoryctrl, false);
                } else {
                    EnableOrDisableDropdown(Deparmentctrl, true);
                    EnableOrDisableDropdown(Categoryctrl, true);
                }

            });



            displaytcodectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DisplayCodeDropDown).ClientID %>");
            displaytcodectrl.change(function () {
                DateChangesUI(fromdatectrl.val(), todatectrl.val());
            });
            //console.log(fromdatectrl.parent());
            //console.log(todatectrl.parent());
            //alert(fromdatectrl);

            //fromdatectrl.datepicker({
            //    dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true,
            //    onSelect: function (dateText) {
            //        alert('test');
            //        //display("Selected date: " + dateText + "; input's current value: " + this.value);
            //        DateChangesUI(fromdatectrl.val(), todatectrl.val());
            //    }
            //});
            //todatectrl.datepicker({
            //    dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true,
            //    onSelect: function (dateText) {
            //        DateChangesUI(fromdatectrl.val(), todatectrl.val());
            //        //display("Selected date: " + dateText + "; input's current value: " + this.value);
            //    }
            //});
        });
          
            $(document).ready(function () {
                $('#<%=txtFromDate.ClientID %>').change(function () {
                    DateChangesUI(fromdatectrl.val(), todatectrl.val());    
                });
                $('#<%=txtToDate.ClientID %>').change(function () {
                    DateChangesUI(fromdatectrl.val(), todatectrl.val());    
                });
                <%--$('#<%=txtFromDate.ClientID %>').on("keydown", function () {
                    return false;
                });
                $('#<%=txtToDate.ClientID %>').on("keydown", function () {
                    return false;
                });--%>
            });
            function DateChangesUI(frmdate, Todate) {
                
                displaytcodectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DisplayCodeDropDown).ClientID %>");
                if (displaytcodectrl.val() == '') {
                    ShowPopupMessageBox("Please Select DisplayCode");

                } else {

                    var values = displaytcodectrl.val().split('|');
                    var code = values[0];
                    var PerDay = values[1];
                    var PerMonth = values[2];

                    var date1 = frmdate;
                    var date2 = Todate;


                    date1 = date1.split('-');
                    date2 = date2.split('-');


                    date1 = new Date(date1[2], date1[1], date1[0]);
                    date2 = new Date(date2[2], date2[1], date2[0]);

                    //                date1_unixtime = parseInt(date1.getTime() / 1000);
                    //                date2_unixtime = parseInt(date2.getTime() / 1000);

                    //                //  in seconds
                    //                var timeDifference = date2_unixtime - date1_unixtime;

                    //                // in Hours
                    //                var timeDifferenceInHours = timeDifference / 60 / 60;

                    //                // in days 
                    //                var timeDifferenceInDays = timeDifferenceInHours / 24;
                    var _MS_PER_DAY = 1000 * 60 * 60 * 24;
                    var utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
                    var utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());
                    //alert( Math.floor((utc2 - utc1) / _MS_PER_DAY));
                    var timeDifferenceInDays = Math.floor((utc2 - utc1) / _MS_PER_DAY);
                    // alert(timeDifferenceInDays);
                    res = timeDifferenceInDays % 30;
                    var fromDate = new Date(frmdate);
                    var toDate = new Date(Todate);

                    // alert(monthDiff(fromDate, toDate));
                    //var months = monthDiff(fromDate, toDate)
                    var months = "";
                    if (PerDay != "0.00") {
                        months = "0";
                    }
                    else {
                        months = parseInt(timeDifferenceInDays / 30);
                    }
                
                    //alert(monthDiff(fromDate, toDate));
                    // var months = monthDiff(fromDate, toDate);
                    //                var months = 0;
                    //                months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
                    //                months -= fromDate.getMonth();
                    //                months += toDate.getMonth();
                    //                if (toDate.getDate() < fromDate.getDate()) {
                    //                    months--;
                    //                }

                    // alert(months);
                    if (months == 0) {
                        if (PerDay > 0) {
                            $('#<%=txtamt.ClientID%>').val(PerDay * timeDifferenceInDays);
                        }
                    }
                    else if (months != 0) {
                        if (PerMonth > 0) {
                            var Amt;
                            //alert(res);
                            if (res != 0) {
                                Amt = PerMonth / 30;
                                Amt = Amt * res;
                                // alert(Amt);
                            } else {
                                Amt = 0;
                            }
                            $('#<%=txtamt.ClientID%>').val((PerMonth * months) + Amt);
                        }
                    }

                }

            }
            function monthDiff(d1, d2) {
                var months;
                months = (d2.getFullYear() - d1.getFullYear()) * 12;
                months -= d1.getMonth() + 1;
                months += d2.getMonth();
                return months <= 0 ? 0 : months;
            }
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }

            function fncShowAlertNoItemsMessage() {
                try {
                    InitializeDialogAlertNoItems();
                    $("#AlertNoItems").dialog('open');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            //Close Save Dialog
            function fncCloseAlertNoItemsDialog() {
                try {
                    $("#AlertNoItems").dialog('close');
                    return false;

                }
                catch (err) {
                    alert(err.message);
                }
            }
            //Save Dialog Initialation
            function InitializeDialogAlertNoItems() {
                try {
                    $("#AlertNoItems").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    alert(err.message);
                }
            }
            function fncValidateSave() {
                
                var Show = '';
                try {
                    var vendorctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.VendorDropDown).ClientID %>");
                    var displatcodectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DisplayCodeDropDown).ClientID %>");
                    var Deparmentctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DepartmentDropDown).ClientID %>");

                    var Categoryctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.CategoryDropDown).ClientID %>");

                    var Brandctrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BrandDropDown).ClientID %>");


                    //alert(vendorctrl.val());
                    //alert(displatcodectrl.val());
                    if (vendorctrl.val() == '') {
                        Show = Show + 'Please Select Vendor'
                    }  if (displatcodectrl.val() == '') {
                        Show = Show + '<br />Please Select DisplayCode';
                    }  if (Deparmentctrl.val() == '' && Categoryctrl.val() == '' && Brandctrl.val() == '') {
                        DateChangesUI(fromdatectrl.val(), todatectrl.val());
                        Show = Show + '<br />Please Choose any One (Department, Category, Brand)';
                       
                       
                    }  if ($('#<%=txttotalamt.ClientID%>').val() == '') {
                        DateChangesUI(fromdatectrl.val(), todatectrl.val());
                        Show = Show + '<br />Invalid Total Amount';
                    }
                    if (Show != '') {
                        ShowPopupMessageBox(Show);
                        return false;
                    }
                    else {
                        var gridtr = $("#<%= gvDisplay.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                        if (gridtr.length == '0') {
                            //AlertNoItems
                            fncShowAlertNoItemsMessage();
                        }
                        else {
                            
                            var Amount = $('#<%=txtamt.ClientID %>').val();
                            var DisPer = $('#<%=txtxDisPerc.ClientID %>').val();
                            if (DisPer == '') {
                                $('#<%=txtxDisPerc.ClientID %>').val("0");
                                var Disamt = parseFloat((Amount)) * parseFloat($('#<%=txtxDisPerc.ClientID %>').val()) / 100;
                                $('#<%=txtDisAmt.ClientID %>').val(Disamt);
                                $('#<%=txttotalamt.ClientID %>').val(parseFloat(Amount) - parseFloat(Disamt));

                            }
                            var Amount1 = $('#<%=txtamt.ClientID %>').val();

                            var Disamt1 = $('#<%=txtDisAmt.ClientID %>').val();
                            if (Disamt1 == '') {
                                $('#<%=txtDisAmt.ClientID %>').val("0");
                                $('#<%=txttotalamt.ClientID %>').val(parseFloat(Amount1) - parseFloat($('#<%=txtDisAmt.ClientID %>').val()));
                                $('#<%=txtxDisPerc.ClientID %>').val((parseFloat($('#<%=txtDisAmt.ClientID %>').val()) / parseFloat(Amount1)) * 100);
                            }
                            fncShowConfirmSaveMessage();
                            //alert($("#<%=gvDisplay.ClientID %> [id*=chkSingle]").is(":checked"));
                            //                    if ($("#<%=gvDisplay.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                            //                        fncShowConfirmSaveMessage();
                            //                    } else {
                            //                        fncShowMessage();
                            //                    }
                        }
                    }
                   
                }
                catch (err) {
                    alert(err.message);
                    console.log(err);
                }
            }
            function fncValidateAdd() {
                try {
                    
                    var itemcode = $('#<%=txtInventoryCode.ClientID %>').val();
                    var itemdesc = $('#<%=txtItemNameAdd.ClientID %>').val();
                    var qty = $('#<%=txtQty.ClientID%>').val();
                    if (itemcode == '' && itemdesc == '' && qty == '') {
                        ShowPopupMessageBox("Please Enter Details");
                        return false;
                    }
                    else if (qty == '') {
                        ShowPopupMessageBox("Please Enter Qty");
                        $('#<%=txtQty.ClientID%>').focus();
                        return false;

                    } else {
                        return true;
                    }

                }
                catch (err) {
                    alert(err.message);
                    console.log(err);
                }
            }
            function fncShowSuccessDeleteMessage() {
                try {
                    //alert("message");
                    InitializeDialog1();
                    $("#StockUpdatePosting").dialog('open');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            function InitializeDialog1() {
                try {
                    $("#StockUpdatePosting").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function fncCloseDeleteDialog() {
                try {
                    $("#StockUpdatePosting").dialog('close');


                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            //Show Popup After Save
            function fncShowMessage() {
                try {
                    InitializeDialog();
                    $("#SelectAny").dialog('open');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            //Close Save Dialog
            function fncCloseDialog() {
                try {
                    $("#SelectAny").dialog('close');
                    return false;

                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            //Save Dialog Initialation
            function InitializeDialog() {
                try {
                    $("#SelectAny").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            function fncShowConfirmSaveMessage() {
                try {
                    InitializeDialogConfirmSave();
                    $("#ConfirmSaveDialog").dialog('open');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            function InitializeDialogConfirmSave() {
                try {
                    $("#ConfirmSaveDialog").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function CloseConfirmSave() {
                try {
                    $("#ConfirmSaveDialog").dialog('close');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            function fncShowConfirmDeleteMessage() {
                try {
                    InitializeDialogDelete();
                    $("#DeleteStockUpdatePosting").dialog('open');
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            function InitializeDialogDelete() {
                try {
                    $("#DeleteStockUpdatePosting").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function CloseConfirmDelete() {
                try {
                    $("#DeleteStockUpdatePosting").dialog('close');

                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function Clear() {
                try {
                    $("#DeleteStockUpdatePosting").dialog('close'); return false;
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function InitializeDialog1() {
                try {
                    $("#StockUpdateSave").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 150,
                        width: 370,
                        modal: true
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
            function fncShowSuccessMessage() {
                try {
                    $("#divSave").dialog({
                        autoOpen: false,
                        resizable: false,
                        height: 'auto',
                        width: 'auto',
                        modal: true
                    });
                   // ShowPopupMessageBox("Saved Successfully" +'-'+$('#<%=hidSuccess.ClientID%>').val());
                    //InitializeDialog1();
                    $("#divSave").dialog('open');
                    //clearForm();
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                    console.log(err);
                }
            }
            //Close Save Dialog
            function fncCloseSaveDialog() {
                try {
                    $("#StockUpdateSave").dialog('close');
                    return false;
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            } 
        
        
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $('#<%=txtamt.ClientID%>').val('');
            $('#<%=txtxDisPerc.ClientID %>').val('0');
            $('#<%=txtDisAmt.ClientID %>').val('0');
            $('#<%=txttotalamt.ClientID %>').val('');
            $('#<%=txtInventoryCode.ClientID %>').focus();

        }
        $(document).ready(function () {
            setAutocomplete();
        });
        function setAutocomplete() {
            try{
                $("[id$=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmDisplayandContractsAdd.aspx/GetInventoryValue",
                        data: "{ 'sCode': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label:item,
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                    focus: function (event, i) {
                        $('#<%=txtInventoryCode.ClientID %>').val(i.item.label.split('-')[0]);
                        $('#<%=txtItemNameAdd.ClientID %>').val(i.item.val);


                        event.preventDefault();
                    },
                select: function (e, i) {

                    $('#<%=txtInventoryCode.ClientID %>').val(i.item.label.split('-')[0]);
                    $('#<%=txtItemNameAdd.ClientID %>').val(i.item.val);

                    return false;
                },

                minLength: 1
                });
                $(window).scroll(function (event) {
                    $('.ui-autocomplete.ui-menu').position({
                        my: 'left bottom',
                        at: 'left top',
                        of: '#ContentPlaceHolder1_txtInventoryCode'
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkUpdate.ClientID %>').is(":visible"))
                    $('#<%=lnkUpdate.ClientID %>').click();
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmDisplayandContracts.aspx">
                    <%=Resources.LabelCaption.lbl_DisplayAndContracts%></a> <i class="fa fa-angle-right">
                    </i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_DisplayAndContractsAdd%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        
        <div class="container-group-price EnableScroll">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl" 
                            EnableVendorDropDown = "true"
                            EnableDepartmentDropDown = "true"
                            EnableCategoryDropDown = "true"
                            EnableBrandDropDown = "true"
                            EnableDisplayCodeDropDown = "true"
                            
                    SectionHeader="Display Settings" AutoLoadControlData="false" />
                <div class="control-group-split" style="width: 100%; float: left">
                     <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                     <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_amount %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txtamt" runat="server" onkeypress="return isNumberKeyAmt(event);" 
                                CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_Discperc %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txtxDisPerc" runat="server" onkeypress="return isNumberKeyDiscPerc(event)" Text="0"
                                CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Discount_Amt %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txtDisAmt" runat="server" onkeypress="return isNumberKeyDiscAmt(event)" Text="0"
                                CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 100%">
                        <div class="label-left" style="width: 39%">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalAmount %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%; margin-left: 1%">
                            <asp:TextBox ID="txttotalamt" runat="server" onkeypress="return isNumberKey(event)"
                                CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div style="width: 100%">
                    <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                        <div class="control-button">
                            <div class="label-right" style="width: 100%">
                                <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="clearForm();"><i class="icon-play"></i>Clear</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                style="height: 450px">
                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                    <ContentTemplate>
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 420px">
                            <asp:GridView ID="gvDisplay" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" OnRowDeleting="gv_OnRowDeleting"
                                OnRowDataBound="gv_OnRowDataBound" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn"
                                EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Remove" />
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Visible="false" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Inventorycode" HeaderText="Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Item Description"></asp:BoundField>
                                    <asp:BoundField DataField="Qty" HeaderText="Qty"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="Selling" HeaderText="Selling Price"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <div class="bottom-purchase-container-add">
                    <div class="bottom-purchase-container-add-Text">
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 8%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 5px">
                                    <asp:TextBox ID="txtInventoryCode" runat="server" onkeydown="return fncGetInventoryCodeEnter(event)"
                                        CssClass="form-control" Style="width: 100px;"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 3%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label14" runat="server" Text=" "></asp:Label>
                                </div>
                                <div class="control-group-left" style="width: 3%">
                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                        <div class="container5">
                                            <%-- <asp:ImageButton ID="btnImage" runat="server" ImageUrl="../images/SearchImage.png"
                                            OnClick="btnImage_Click" Height="16px" Width="16px" Visible="False" />--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 20%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                    <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res" onMouseDown="return false"
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 5%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                    <asp:TextBox ID="txtQty" runat="server" onkeypress="return isNumberKey(event)" onkeydown="fncQtyFocus(event)"
                                        CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 6%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label161" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                    <asp:TextBox ID="txtMrp" runat="server" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                        CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 8%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                    <asp:TextBox ID="txtSellingPrice" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                        runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 20%">
                                <div class="label-left" style="width: 100%">
                                    <asp:Label ID="lblBatchno" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                    <asp:TextBox ID="txtBatchno" runat="server" onMouseDown="return false" CssClass="form-control-res"
                                        Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                            <div style="width: 100%">
                                <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                    <div class="control-button">
                                        <div class="label-left" style="width: 100%">
                                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                                OnClick="lnkAdd_Click" OnClientClick="return fncValidateAdd();"><i class="icon-play"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%--<div class="control-button">
                            <asp:LinkButton ID="lnkDelete" runat="server" class="button-red-small"><i class="icon-play"></i>Delete</asp:LinkButton>
                        </div>--%>
                                    <div class="control-button">
                                        <div class="label-right" style="width: 100%">
                                            <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                                OnClientClick="fncClear();return false;"><i class="icon-play"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="float: right; display: table">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                            Visible="False">Hide Display Settings</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'
                            OnClientClick="fncValidateSave();return false;" Width="150px"></asp:LinkButton>
                    </div>
                    <%--<div class="control-button">
                        <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                            Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                    </div>--%>
                    <div class="control-button">
                        <asp:LinkButton ID="Delete" runat="server" class="button-blue"  PostBackUrl="~/Purchase/frmDisplayandContracts.aspx"
                            Text="Close"></asp:LinkButton> <%--OnClientClick="fncValidateDelete();return false;"--%>
                    </div>
                    <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
                </div>
                      </ContentTemplate>
                    </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="SelectAny">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteStockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_Delete%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
         <div id="divSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkSaveView" runat="server" class="button-blue"  PostBackUrl="~/Purchase/frmDisplayandContracts.aspx"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full">
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        <asp:HiddenField ID="hidSuccess" runat="server" Value="" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
                  
</asp:Content>
 