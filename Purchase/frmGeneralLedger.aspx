﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGeneralLedger.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmGeneralLedger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
               .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .GeneralLedger td:nth-child(1), .GeneralLedger th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(2), .GeneralLedger th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(3), .GeneralLedger th:nth-child(3) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(4), .GeneralLedger th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(5), .GeneralLedger th:nth-child(5) {
            min-width: 400px;
            max-width: 400px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(6), .GeneralLedger th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(7), .GeneralLedger th:nth-child(7) {
            min-width: 335px;
            max-width: 335px;
            text-align: center !important;
        }

        .GeneralLedger td:nth-child(8) {
            text-align: right !important;
        }

        .GeneralLedger td:nth-child(8), .GeneralLedger th:nth-child(8) {
            min-width: 120px;
            max-width: 120px;
        }

        
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'GeneralLedger');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "GeneralLedger";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        function pageLoad() {
            $('#<%=txtGLCode.ClientID%>').attr("readonly", "readonly");
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Member") {
                    $('#<%=txtCustomerCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtCustomerName.ClientID %>').val($.trim(Description));
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkGetGLCode', '')
                    //fncGetGLCode(Code);
                }
                if (SearchTableName == "Vendor") {
                    $('#<%=txtVendorCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtVendorName.ClientID %>').val($.trim(Description));
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkGetGLCode', '')
                    //fncGetGLCode(Code);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        <%--function fncGetGLCode(Code) {
            try {
                var val = Code;
                var obj = {};
                obj.Data = val;
                $.ajax({
                    type: "POST",
                    url: 'frmGeneralLedger.aspx/GetGLCode',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {

                        var objdata = jQuery.parseJSON(data.d);

                        if (objdata.Table.length > 0) {
                            $("#<%=txtGLCode.ClientID %>").val(objdata.Table[0]["GLCode"]);
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>
        function fncPaybuttonclick(source)
        {
            try{
                var rowPayObj, GLcode, CustomerName, VendorName;
                rowPayObj = $(source).parent().parent();
                GLcode = $.trim(rowPayObj.find('td span[id*="lblGLcode"]').text());
                CustomerName = $.trim(rowPayObj.find('td span[id*="lblCustomerName"]').text());
                VendorName = $.trim(rowPayObj.find('td span[id*="lblVendorName"]').text());
                $('#<%=hdGLcode.ClientID %>').val(GLcode);
                $('#<%=hdCustomerName.ClientID %>').val(CustomerName);
                $('#<%=hdVendorName.ClientID %>').val(VendorName);
                //window.location = "./frmGeneralLedgerMnt.aspx?GLCode=" + GLcode + "&CustomerName=" + CustomerName + "&VendorName=" + VendorName;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">General Ledger</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="purchase-order-header">
                        <div>
                            <div class="control-group-split">

                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label32" runat="server" Text="Customer Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCustomerCode" onkeydown="return fncShowSearchDialogCommon(event, 'Member',  'txtCustomerCode', 'txtVendorCode');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="form-control-res" Style="width: 165%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="GL Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtGLCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">

                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Vendor Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendorCode" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor',  'txtVendorCode', '');" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control-res" Style="width: 165%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="control-button PO_Summary_border" style="width: 100%">
                                        <div class="control-button" style="width: 125px">
                                            <asp:RadioButton ID="rbtOutStanding" runat="server" AutoPostBack="true" OnCheckedChanged="rbtOutStanding_CheckedChanged" Checked="true" Text="OutStanding" GroupName="GL" />
                                        </div>
                                        <div class="control-button">
                                            <asp:RadioButton ID="rbtAll" runat="server" AutoPostBack="true" OnCheckedChanged="rbtAll_CheckedChanged" Text="All" GroupName="GL" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width: 20%;">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" Text="Fetch"></asp:LinkButton>
                                        <%--OnClick="lnkLoad_Click"--%>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" OnClick="lnkClear_Click" class="button-blue" Text="Clear"></asp:LinkButton>
                                        <%--OnClick="lnkLoad_Click"--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div style="height: 100px">--%>
                            <div id="divRepeater" class="Barcode_fixed_headers">
                            <div class="purchase-order-detail">
                                <asp:Panel ID="Panel2" runat="server">
                                    <div id="divrepeater" class="Payment_fixed_headers GeneralLedger">
                                        <asp:UpdatePanel ID="uprptrGL" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                                        <table id="tblGeneralLedger" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr>
                                                    <th scope="col">S.No
                                                    </th>
                                                    <th scope="col">Payment
                                                    </th>
                                                    <th scope="col">GLCode
                                                    </th>
                                                    <th scope="col">Vendor Code
                                                    </th>
                                                    <th scope="col">Vendor Name
                                                    </th>
                                                    <th scope="col">Customer Code
                                                    </th>
                                                    <th scope="col">Customer Name
                                                    </th>
                                                    <th scope="col">Total Amount
                                                    </th>

                                                </tr>
                                            </thead>
                                            <asp:Repeater ID="rptrGLdata" runat="server" OnItemDataBound="rptrGL_ItemDataBound">
                                                <%--OnItemDataBound="rptrGL_ItemDataBound"--%>
                                                <HeaderTemplate>
                                                    <tbody id="BulkBarcodebody">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr id="GLRow" tabindex='<%#(Container.ItemIndex)%>' runat="server" onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                                        <td>
                                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNo") %>' />
                                                        </td>
                                                        <td runat="server" id="tdPay">
                                                            <asp:Button ID="btnPay" runat="server" Text="Pay" OnClick="imgPay_Click" OnClientClick="fncPaybuttonclick(this)" /><%--OnClick="btnPay_Click"--%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblGLcode" runat="server" Text='<%# Eval("GLCode") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblVendorCode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCustomerCode" runat="server" Text='<%# Eval("CustomerCode") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCurrentBalance" runat="server" Text='<%# Eval("CurrentBalance") %>' />
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </tbody>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <tfoot>
                                            </tfoot>
                                        </table>
                        </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </div>
                                </asp:Panel>
                            </div>
                                </div>
                        <%--</div>--%>
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="hiddencol">
    <asp:LinkButton ID="lnkGetGLCode" runat="server" OnClick="lnkGetGLCode_Click" style="display:none;" class="button-blue" Text="Clear"></asp:LinkButton>
        <asp:HiddenField ID="hdGLcode" runat="server" />
        <asp:HiddenField ID="hdCustomerName" runat="server" />
        <asp:HiddenField ID="hdVendorName" runat="server" />
        </div>
</asp:Content>
