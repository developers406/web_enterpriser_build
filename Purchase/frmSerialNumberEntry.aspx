﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSerialNumberEntry.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmSerialNumberEntry" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
          
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

          .gvVendorLoadItemdummy td:nth-child(1) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvVendorLoadItemdummy td:nth-child(2) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvVendorLoadItemdummy td:nth-child(3) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvVendorLoadItemdummy td:nth-child(4) {
              min-width: 841px;
              max-width: 750px;
          }

          .gvVendorLoadItemdummy td:nth-child(5) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvVendorLoadItemdummy td:nth-child(6) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvVendorLoadItemdummy td:nth-child(7) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(1) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(2) {
              min-width: 855px;
              max-width: 150px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(3) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(4) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(5) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(6) {
              min-width: 100px;
              max-width: 100px;
          }

          .gvPendingPurchaseBalancedummy td:nth-child(7) {
              min-width: 100px;
              max-width: 100px;
          }

          td {
              cursor: pointer;
              transition: all .25s ease-in-out;
          }

          .selected {
              background-color: #4F84C4 !important;
              font-weight: bold;
              color: #fff;
          }

          #tblImeiDetail tr:hover td {
              background-color: #ff4c4c;
              font-weight: bold;
              color: #fff;
          }
      </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SerialNumberEntry');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SerialNumberEntry";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        var IMEINoforDelete = "";
        var SerialNoforDelete = "";
        var deletethis = null;
        var gidlistviewthis = null;

        function fncImeiDetailListView() {
            var objImei, ImeiRow;
            imeiBody = $("#tblImeiDetail tbody");
            try {
                objImei = jQuery.parseJSON($("#<%=hdnImeiList.ClientID %>").val());
                imeiBody.children().remove();
                if (objImei.length > 0) {
                    for (var i = 0; i < objImei.length; i++) {
                        ImeiRow = "<tr class='tbl_left'>"
                    + "<td id='tdImeiDetails_" + i + "' >" + objImei[i]["IMEINumber"] + "</td>"
                    + " <td >" + objImei[i]["SerialNo"] + "</td>"
                    + "</tr>";
                        imeiBody.append(ImeiRow);

                    }

                }
                else {
                    fncToastInformation("No Record Found");
                }
                $("#dialog-popEnter").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    title: $('#<%=hdnInvertoryDesc.ClientID %>').val(),
                    modal: true

                });
                $("#<%=txtInventoryCode.ClientID %>").val();
                $('#<% =txtSerialNumber.ClientID %>').focus();

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBuildIMEIList() {
            try {
                var obj = {};
                obj.Invertory = $('#<%=hdnInvertory.ClientID %>').val();
                obj.GidNo = $('#<% =hdnGidNo.ClientID %>').val();
                obj.VendorCode = $('#<% =txtVendorCode.ClientID %>').val();
                obj.Date = $('#<% =hdnDate.ClientID %>').val();

                obj.IMEINumber = $('#<%=txtIMEINumber.ClientID %>').val();
                obj.SerialNumber = $('#<% =txtSerialNumber.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmSerialNumberEntry.aspx/fncBuildIMEIList")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $("#<%=hdnImeiList.ClientID %>").val(msg.d);
                        debugger;
                        if (msg.d == "1") {
                            fncToastError("Duplicate numbers not allowed in Serial No");
                        } else if (msg.d == "2") {
                            fncToastError("Duplicate numbers not allowed in IMEI Number");
                        } else if (msg.d == "3") {
                            var countexist1 = 0;
                            <%--$('#<%=hdnInvertory.ClientID %>').val('');
                            $('#<% =hdnGidNo.ClientID %>').val('');
                            $('#<% =txtVendorCode.ClientID %>').val('');
                            $('#<% =hdnDate.ClientID %>').val('');--%>



                            for (var i = 0; i < $("#tblImeiDetail").find('tr').length; i++) {
                                if ($("#tblImeiDetail").find("tr").eq(i).find("td").eq(1).text() == $('#<%=txtSerialNumber.ClientID %>').val()) {
                                    countexist1++
                                }
                            }

                            if (countexist1 == 0) {

                                var tempQty = $('#<%=txtOf.ClientID %>').val();
                                gidlistviewthis.find('td').eq(4).html(parseInt(tempQty) + 1);
                                gidlistviewthis.find('td').eq(5).html(parseInt(gidlistviewthis.find('td').eq(5).text()) - 1);
                                $('#<%=txtOf.ClientID %>').val(parseInt(tempQty) + 1);

                            }
                            $("#tblImeiDetail").each(function () {
                                var tds = "<tr ALIGN='CENTER'>";
                                tds += "<td>" + $('#<%=txtIMEINumber.ClientID %>').val() + "</td>"
                                      + " <td>" + $('#<%=txtSerialNumber.ClientID %>').val() + "</td>"
                                tds += '</tr>';
                                if ($('tbody', this).length > 0) {
                                    $('tbody', this).append(tds);
                                } else {
                                    $(this).append(tds);
                                }
                            });
                            $('#<%=txtIMEINumber.ClientID %>').val('');
                            $('#<% =txtSerialNumber.ClientID %>').val('');
                        }
                        else {
                            fncToastError(msg.d);
                        }
                    },
                    error: function (data) {
                        fncToastError(data.message);
                    }
                });

    }
    catch (err) {
        fncToastError(err.message);
    }
}
function fncDeleteIMEIList() {
    try {
        var obj = {};
        var countexist1 = 0;
        obj.Invertory = $('#<%=hdnInvertory.ClientID %>').val();
                obj.GidNo = $('#<% =hdnGidNo.ClientID %>').val();
                obj.VendorCode = $('#<% =txtVendorCode.ClientID %>').val();
                obj.Date = $('#<% =hdnDate.ClientID %>').val();

                obj.IMEINumber = IMEINoforDelete;
                obj.SerialNumber = SerialNoforDelete;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmSerialNumberEntry.aspx/fncDeleteIMEIList")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "True") {

                            deletethis.remove();
                            for (var i = 0; i < $("#tblImeiDetail").find('tr').length; i++) {
                                if ($("#tblImeiDetail").find("tr").eq(i).find("td").eq(1).text() == deletethis.find('td:nth-child(2)').text()) {
                                    countexist1++
                                }
                            }

                            if (countexist1 == 0) {

                                var tempQty = $('#<%=txtOf.ClientID %>').val();
                                $('#<%=txtOf.ClientID %>').val(parseInt(tempQty) - 1);
                                gidlistviewthis.find('td').eq(4).html(parseInt(tempQty) - 1);
                                gidlistviewthis.find('td').eq(5).html(parseInt(gidlistviewthis.find('td').eq(5).text()) + 1);
                            }
                            IMEINoforDelete = "";
                            SerialNoforDelete = "";
                            deletethis = null;
                            //gidlistviewthis = null;


                            fncToastInformation("Delete Successfully");
                        } else if (msg.d == "False") {
                            fncToastError("Some Thing Went Wrong");
                        } else {
                            fncToastError("Some Thing Went Wrong");
                        }

                    },
                    error: function (data) {
                        fncToastError(data.message);
                    }
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncGetIemi() {
            try {
                var obj = {};
                obj.Invertory = $('#<%=hdnInvertory.ClientID %>').val();
        obj.GidNo = $('#<% =hdnGidNo.ClientID %>').val();
        obj.VendorCode = $('#<% =txtVendorCode.ClientID %>').val();
        obj.Date = $('#<% =hdnDate.ClientID %>').val();

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("frmSerialNumberEntry.aspx/fncGetIemi")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $("#<%=hdnImeiList.ClientID %>").val(msg.d);
                        fncImeiDetailListView();
                    },
                    error: function (data) {
                        fncToastError(data.message);
                    }
                });

                }
                catch (err) {
                    fncToastError(err.message);
                }
            }
            function fncsetDate(date) {

                $('#<%=txtDate.ClientID %>').val($('#<% =hdnDate.ClientID %>').val());
            console.log($('#<% =hdnDate.ClientID %>').val());
        }
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                        $('#<%=btInsert.ClientID %>').css("display", "block");
                    }
                    else {
                        $('#<%=btInsert.ClientID %>').css("display", "none");
                }
                 if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                        $('#<%=btDelete.ClientID %>').css("display", "block");
                    }
                    else {
                        $('#<%=btDelete.ClientID %>').css("display", "none");
                 }

                    $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                    if ($("#<%= txtDate.ClientID %>").val() === '') {
                        $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                    }


                    $('#<%= gvPendingPurchase.ClientID %> ').find('tr').click(function () {
                        IMEINoforDelete = "";
                        SerialNoforDelete = "";
                        deletethis = null;
                        gidlistviewthis = null;

                        $('#<%=txtGIDNo.ClientID %>').val($(this).find('td:nth-child(1)').text());
                        $('#<%=txtDate.ClientID %>').val($(this).find('td:nth-child(2)').text());
                        $('#<%=txtVendorCode.ClientID %>').val($(this).find('td:nth-child(3)').text());
                        $('#<%=txtName.ClientID %>').val($(this).find('td:nth-child(4)').text());
                        $('#<% =hdnGidNo.ClientID %>').val($(this).find('td:nth-child(1)').text());
                        $('#<% =hdnDate.ClientID %>').val($(this).find('td:nth-child(2)').text());
                        $('#<% =hdnVendorCode.ClientID %>').val($(this).find('td:nth-child(3)').text());
                        $('#<% =BtGetPendingStatus.ClientID %>').click();
                    });
                    $('#<%= gvPendingPurchaseBalance.ClientID %>').find('tr').dblclick(function () {
                        //if (!this.rowIndex) return;
                        IMEINoforDelete = "";
                        SerialNoforDelete = "";
                        deletethis = null;
                        gidlistviewthis = null;

                        gidlistviewthis = $(this);
                        $('#<%=hdnInvertory.ClientID %>').val($(this).find('td:nth-child(1)').text());
                        $('#<%=hdnInvertoryDesc.ClientID %>').val($(this).find('td:nth-child(2)').text());
                        fncGetIemi();
                        $('#<%=txtInventoryCode.ClientID %>').val($(this).find('td:nth-child(1)').text());
                        $('#<%=txtDescription.ClientID %>').val($(this).find('td:nth-child(2)').text());
                        $('#<%=txtQty.ClientID %>').val($(this).find('td:nth-child(4)').text());
                        $('#<%=txtOf.ClientID %>').val($(this).find('td:nth-child(5)').text());


                    });
                    $("#ContentPlaceHolder1_gvPendingPurchase tbody tr").hover(
                               function () {

                                   $(this).addClass("selected");
                               },
                               function () {
                                   //if (!this.rowIndex) return;
                                   $(this).removeClass("selected");
                               }
                             );
                    $("#ContentPlaceHolder1_gvPendingPurchaseBalance tbody tr").hover(
                              function () {

                                  $(this).addClass("selected");
                              },
                              function () {
                                  //if (!this.rowIndex) return;
                                  $(this).removeClass("selected");
                              }
                            );

                    $("#tblImeiDetail").on("dblclick", "tr", function () {


                        $("#tblImeiDetail").find("tr").removeClass("selected");
                        $(this).addClass("selected");
                        deletethis = $(this);
                        fncToastInformation("IMEI Number " + $(this).find('td:nth-child(1)').text() + " Serial No " + $(this).find('td:nth-child(2)').text() + " Is Selected");
                        IMEINoforDelete = $(this).find('td:nth-child(1)').text();
                        SerialNoforDelete = $(this).find('td:nth-child(2)').text();
                    });
                    $('#<%=btInsert.ClientID %>').click(function () {
                        IMEINoforDelete = "";
                        SerialNoforDelete = "";
                        deletethis = null;
                        //gidlistviewthis = null;
                        var countexist = 0;
                        if ($('#<%=txtSerialNumber.ClientID %>').val() == "" && $('#<%=txtIMEINumber.ClientID %>').val() == "") {
                        fncToastError("Please Fill The Column");
                        $('#<%=txtSerialNumber.ClientID %>').css({ "border-color": "Red" });
                        $('#<%=txtIMEINumber.ClientID %>').css({ "border-color": "Red" });
                        $('#<% =txtSerialNumber.ClientID %>').focus();
                    }
                    else {

                        for (var i = 0; i < $("#tblImeiDetail").find('tr').length; i++) {
                            if ($("#tblImeiDetail").find("tr").eq(i).find("td").eq(1).text() == $('#<%=txtSerialNumber.ClientID %>').val()) {
                                countexist++
                            }
                        }
                        if (countexist == 0) {
                            if (parseInt($('#<%=txtQty.ClientID %>').val()) > parseInt($('#<%=txtOf.ClientID %>').val())) {
                                for (var i = 0; i < $("#tblImeiDetail").find('tr').length; i++) {
                                    if ($("#tblImeiDetail").find("tr").eq(i).find("td").eq(0).text() == $('#<%=txtIMEINumber.ClientID %>').val()) {

                                        fncToastError("Duplicate numbers not allowed");
                                        $('#<% =txtIMEINumber.ClientID %>').focus();
                                        return;
                                    }
                                }
                            }
                            else {
                                fncToastError("Total Scanned Quantity cannot exceed Actual Quantity !");
                                $('#<%=txtIMEINumber.ClientID %>').val('');
                                $('#<% =txtIMEINumber.ClientID %>').focus();
                                return;
                            }

                        }
                        else {
                            for (var i = 0; i < $("#tblImeiDetail").find('tr').length; i++) {
                                if ($("#tblImeiDetail").find("tr").eq(i).find("td").eq(0).text() == $('#<%=txtIMEINumber.ClientID %>').val()) {

                                    fncToastError("Duplicate numbers not allowed");
                                    return;
                                }
                            }
                        }

                        fncBuildIMEIList();
                    }

                    });
                $('#<%=btDelete.ClientID %>').click(function () {
                        if (!(IMEINoforDelete == "" && SerialNoforDelete == "")) {
                            $("#dialog-confirm").dialog({
                                resizable: false,
                                height: "auto",
                                width: 400,
                                modal: true,
                                buttons: {
                                    "YES": function () {
                                        fncDeleteIMEIList();
                                        $(this).dialog("close");
                                    },
                                    "NO": function () {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                        else {
                            fncToastError("Please Select a IMEI For Delete");
                        }
                    });
                }
                catch (err) {
                    fncToastError(err.message);
                }
            }
        </script>
   
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
        <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 12px 12px 20px 0;"></span>Are you sure,Do You Want To Delete This Item ?</p>
    </div>
    
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase </a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Serial Number Entry</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        

        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <table rules="all" border="1" id="tblgvVendorLoadItemdummy" runat="server" class="gvVendorLoadItemdummy fixed_header">
                                            <tr>
                                                <td style="text-align: center">PBill No</td>
                                                <td style="text-align: center">Purchase Date</td>
                                                <td style="text-align: center">Vendor Code</td>
                                                <td style="text-align: center">Vendor Name</td>
                                                <td style="text-align: center">Create User</td>
                                                <td style="text-align: center">Purchase Type</td>
                                                <%--<td style="text-align: center">WQty</td>--%>
                                            </tr>
                                        </table>
                <div class="GridDetails" id="divgvVendorLoadItemdummy" runat="server" style=" overflow-x: hidden; overflow-y: scroll; height: 150px; width: 1360px; background-color: aliceblue;">
                    <asp:GridView ID="gvPendingPurchase" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="false" CssClass="pshro_GridDgn gvVendorLoadItemdummy myGrid" oncopy="return false" ShowHeader="false"
                        DataKeyNames="PBillNo">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter fixed_header" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        <Columns>
                           
                            <asp:BoundField DataField="PBillNo" HeaderText="PBill No"></asp:BoundField>
                            <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date"></asp:BoundField>
                            <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code"></asp:BoundField>

                            <asp:BoundField DataField="VendorName" HeaderText="Vendor Name"></asp:BoundField>
                            <asp:BoundField DataField="CreateUser" HeaderText="Create User"></asp:BoundField>
                            <asp:BoundField DataField="PurchaseType" HeaderText="Purchase Type"></asp:BoundField>

                        </Columns>
                    </asp:GridView>
                </div>

                    <center>
                    <div class="container-group" style="margin-top:10px;">
                        <div class="container-control">
                            <div class="container-left">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label1" runat="server" Text='GID No'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtGIDNo"  onkeypress="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label2" runat="server"  Text='Vendor Code'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtVendorCode" onkeypress="" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="container-Right">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label8" runat="server" Text='Date'></asp:Label>
                                    </div>
                                    <div style="float: right"">
                                        <asp:TextBox ID="txtDate" onkeypress=""  nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label6" runat="server" Text='Name'></asp:Label>
                                    </div>
                                    <div style="float: right"">
                                        <asp:TextBox ID="txtName" runat="server" onkeypress="return false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                           </div>
                     </div>
                   </div>
                        </center>
                    <table rules="all" border="1" id="Table1" runat="server" class="gvPendingPurchaseBalancedummy fixed_header">
                                            <tr>
                                                <td style="text-align: center">Inventory Code</td>
                                                <td style="text-align: center">Description</td>
                                                <td style="text-align: center">UOM</td>
                                                <td style="text-align: center">Qty</td>
                                                <td style="text-align: center">Scanned</td>
                                                <td style="text-align: center">Balance</td>
                                                <%--<td style="text-align: center">WQty</td>--%>
                                            </tr>
                                        </table>
                <div class="GridDetails" style="margin-bottom:10px; overflow-x: hidden; overflow-y: scroll; height: 150px; width: 1360px; background-color: aliceblue;">

                    <asp:GridView ID="gvPendingPurchaseBalance" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="false" ShowHeader="false" CssClass="pshro_GridDgn gvPendingPurchaseBalancedummy myGrid" oncopy="return false"
                        DataKeyNames="InvyCode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter fixed_header" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        <Columns>
                           
                            <asp:BoundField DataField="InvyCode" HeaderText="Inventory Code"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="UOM"></asp:BoundField>
                            <asp:BoundField DataField="Qty" HeaderText="Qty"></asp:BoundField>
                            <asp:BoundField DataField="Scanned" HeaderText="Scanned"></asp:BoundField>
                            <asp:BoundField DataField="Balance" HeaderText="Balance"></asp:BoundField>
                          

                        </Columns>
                    </asp:GridView>
                </div>
                <div class="hidden">
                    <asp:HiddenField ID="hdnGidNo" Value="" runat="server" />
                    <asp:HiddenField ID="hdnVendorCode" Value="" runat="server" />
                    <asp:HiddenField ID="hdnDate" Value="" runat="server" />
                    <asp:HiddenField ID="hdnInvertory" Value="" runat="server" />
                    <asp:HiddenField ID="hdnImeiList" Value="" runat="server" />
                    <asp:HiddenField ID="hdnInvertoryDesc" Value="" runat="server" />
                    <asp:HiddenField ID="hdnImeiStatusResult" Value="" runat="server" />
                    <asp:Button ID="BtGetPendingStatus" runat="server" OnClick="BtGetPendingStatus_Click"/>
                </div>
                <div id="dialog-popEnter" style="display: none;" title="Enterpriser Web">
                    <center>
                    <div class="container-group">
                        <div class="container-control">
                            <div class="container-left">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label4" runat="server" Text='Inventory Code'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtInventoryCode" ReadOnly="true" onkeypress="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label5" runat="server"  Text='Description'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtDescription" ReadOnly="true" onkeypress="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="container-Right">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label7" runat="server" Text='Qty'></asp:Label>
                                    </div>
                                    <div style="float: right"">
                                        <asp:TextBox ID="txtQty" ReadOnly="true" onkeypress="return false"  nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label9" runat="server" Text='Of'></asp:Label>
                                    </div>
                                    <div style="float: right"">
                                        <asp:TextBox ID="txtOf" ReadOnly="true" runat="server" onkeypress="return false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                           </div>
                     </div>
                   </div>
                        
                    <div class="gid_Itemsearch gidItem_Searchtbl  " style="padding-bottom:10px" >
                    <table id="tblImeiDetail" width="100%"   rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">IMEI Number</th>
                            <th scope="col">Serial No</th>
                          </tr>
                    </thead>
                    <tbody >
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
                        </div>
                        <div class="container-group">
                        <div class="container-control">
                            <div class="container-left">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label10" runat="server" Text='Serial Number'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtSerialNumber"  runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Label ID="Label11" runat="server"  Text='IMEI Number'></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:TextBox ID="txtIMEINumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="container-Right">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:Button ID="btInsert" runat="server" Text="Insert" class="button-red" /> 
                                    </div>
                                    <div style="float: left">
                                        <asp:Button ID="btDelete" runat="server" Text="Delete" class="button-red" />
                                    </div>
                                </div>
                                
                           </div>
                     </div>
                   </div>
                        </center>

         </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </div> 
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" /> 
</asp:Content>
