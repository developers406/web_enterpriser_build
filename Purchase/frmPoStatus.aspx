﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPoStatus.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPoStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .po_status td:nth-child(1), .po_status th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .po_status td:nth-child(2), .po_status th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .po_status td:nth-child(3), .po_status th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .po_status td:nth-child(4), .po_status th:nth-child(4) {
            min-width: 90px;
            max-width: 90px;
        }

        .po_status td:nth-child(5), .po_status th:nth-child(5) {
            min-width: 200px;
            max-width: 200px;
        }

        .po_status td:nth-child(6), .po_status th:nth-child(6) {
            min-width: 90px;
            max-width: 90px;
        }

        .po_status td:nth-child(7), .po_status th:nth-child(7) {
            min-width: 360px;
            max-width: 360px;
        }

        .po_status td:nth-child(8), .po_status th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .po_status td:nth-child(9), .po_status th:nth-child(9) {
            min-width: 150px;
            max-width: 150px;
        }

        .po_status td:nth-child(10), .po_status th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PoStatus');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PoStatus";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
            try {

                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetPoNo(source) {
            try {
                var rowobj;
                rowobj = $(source).parent().parent();
                $("#<%=hidPONo.ClientID%>").val($.trim(rowobj.find('td span[id*="lblPoNo"]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get value for GA and GRN Report
        function fncRowClick(source) {
            try {
                $("#<%=hidPONo.ClientID%>").val($.trim($(source).find('td span[id*="lblPoNo"]').text()));
                $(source).css("background-color", "#8080ff");
                $(source).siblings().css("background-color", "white");
                //                $(source).siblings().each(function () {
                //                    $(this).css("background-color", "white");
                //                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Print Validation
        function fncPrintValidation() {
            try {

                if ($("#<%=hidPONo.ClientID%>").val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_SelectPO%>');
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_PoStatus%>
                </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full EnableScroll">
            <div class="po_Status_header">
                <div>
                    <div class="Po_Status_vendor_lbl">
                        <asp:Label ID="lblvendor" runat="server" Text='<%$ Resources:LabelCaption,lblVendorCode %>'></asp:Label>
                    </div>
                    <div class="Po_Status_vendor_ddl">
                        <asp:UpdatePanel ID="upvendor" runat="server">
                            <ContentTemplate>
                                <%--<asp:DropDownList ID="ddlVendor" runat="server" CssClass="full_width" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"
                                    AutoPostBack="True">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div>
                    <div class="Po_Status_vendor_lbl po_Status">
                        <asp:Label ID="lblStaus" runat="server" Text='<%$ Resources:LabelCaption,lblStatus %>'></asp:Label>
                    </div>
                    <div class="Po_Status_vendor_ddl">
                        <asp:UpdatePanel ID="upstatus" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="full_width" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                    <asp:ListItem Text="UnDelivered" Value="UnDelivered">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Pending" Value="Pending">
                                    </asp:ListItem>
                                </asp:DropDownList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div id="print" class="PO_Status_print">
                    <%--  <div class="control-button">
                        <asp:RadioButton ID="rbnInternal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Internal %>'
                            GroupName="print" Checked="true" />
                        <asp:RadioButton ID="rbnExternal" runat="server" Text='<%$ Resources:LabelCaption,lbl_External %>'
                            GroupName="print" />
                    </div>--%>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Print %>'
                            OnClick="lnkPrint_Click" OnClientClick="return fncPrintValidation();"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="Payment_fixed_headers po_status">
                <asp:UpdatePanel runat="server" ID="upPoStatus" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblPoStatus" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">View
                                    </th>
                                    <th scope="col">PO No
                                    </th>
                                    <th scope="col">PO Date
                                    </th>
                                    <th scope="col">Gid No
                                    </th>
                                    <th scope="col">Gid Date
                                    </th>
                                    <th scope="col">Vendor Name
                                    </th>
                                    <th scope="col">Create User
                                    </th>
                                    <th scope="col">Approved By
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrPoStatus" runat="server">
                                <HeaderTemplate>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnView" runat="server" Text="View" OnClientClick="fncGetPoNo(this)"
                                                OnClick="btnView_Click" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPoNo" runat="server" Text='<%# Eval("PoNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPODate" runat="server" Text='<%# Eval("PoDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGidNo" runat="server" Text='<%# Eval("GidNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGidDate" runat="server" Text='<%# Eval("GidDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateUser" runat="server" Text='<%# Eval("CreatedBy") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblApprovedBy" runat="server" Text='<%# Eval("ApprovedBy") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <div class="hiddencol">
            <asp:HiddenField ID="hidPONo" runat="server" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>
    </div>
</asp:Content>
