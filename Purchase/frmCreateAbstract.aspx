﻿        <%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCreateAbstract.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmCreateAbstract" %>

        <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
            <style type="text/css">
                
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
                .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1) {
                    min-width: 50px;
                    max-width: 50px;
                    text-align: center;
                }

                .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2) {
                    min-width: 50px;
                    max-width: 50px;
                    text-align: center;
                }

                .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3) {
                    min-width: 50px;
                    max-width: 50px;
                    text-align: center;
                    display: none;
                }

                .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4) {
                    display: none;
                }

                .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5) {
                    display: none;
                }

                .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6) {
                    min-width: 50px;
                    max-width: 50px;
                }

                .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7) {
                    min-width: 150px;
                    max-width: 150px;
                }

                .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8) {
                    display: none;
                }

                .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10) {
                    min-width: 350px;
                    max-width: 350px;
                }

                .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11) {
                    min-width: 150px;
                    max-width: 150px;
                }

                .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(12) {
                    text-align: right;
                }

                .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(13) {
                    text-align: right;
                }

                .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(14) {
                    text-align: right;
                }

                .Barcode_fixed_headers td:nth-child(15), .Barcode_fixed_headers th:nth-child(15) {
                    min-width: 100px;
                    max-width: 100px;
                }
                .Barcode_fixed_headers td:nth-child(15) {
                    text-align: right;
                }
                .Barcode_fixed_headers td:nth-child(16), .Barcode_fixed_headers th:nth-child(16) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(17), .Barcode_fixed_headers th:nth-child(17) {
                    min-width: 110px;
                    max-width: 110px;
                }

                .Barcode_fixed_headers td:nth-child(18), .Barcode_fixed_headers th:nth-child(18) {
                    min-width: 110px;
                    max-width: 110px;
                }

                .Barcode_fixed_headers td:nth-child(19), .Barcode_fixed_headers th:nth-child(19) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(20), .Barcode_fixed_headers th:nth-child(20) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(21), .Barcode_fixed_headers th:nth-child(21) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(22), .Barcode_fixed_headers th:nth-child(22) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(23), .Barcode_fixed_headers th:nth-child(23) {
                    display: none;
                }

                .Barcode_fixed_headers td:nth-child(24), .Barcode_fixed_headers th:nth-child(24) {
                    min-width: 100px;
                    max-width: 100px;
                }

                .Barcode_fixed_headers td:nth-child(25), .Barcode_fixed_headers th:nth-child(25) {
                    min-width: 100px;
                    max-width: 100px;
                    text-align: right !important;
                }
                 .Barcode_fixed_headers td:nth-child(26), .Barcode_fixed_headers th:nth-child(26) {
                    min-width: 100px;
                    max-width: 100px;
                    text-align: right !important;
                }
                  .Barcode_fixed_headers td:nth-child(27), .Barcode_fixed_headers th:nth-child(27) {
                    min-width: 100px;
                    max-width: 100px;
                    text-align: right !important;
                }
            </style>
            
   <script type="text/javascript">
       var d1;
       var d2;
       var Opendate;
       var dur;
       function fncGetUrl() {
           fncSaveHelpVideoDetail('', '', 'CreateAbstract');
       }
       function fncOpenvideo() {

           document.getElementById("ifHelpVideo").src = HelpVideoUrl;

           var Mode = "CreateAbstract";
           var d = new Date($.now());
           Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
           d1 = new Date($.now()).getTime();



           $("#dialog-Open").dialog({
               autoOpen: true,
               resizable: false,
               height: "auto",
               width: 1093,
               modal: true,
               dialogClass: "no-close",
               buttons: {
                   Close: function () {
                       $(this).dialog("destroy");
                       var d = new Date($.now());
                       var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                       d2 = new Date($.now()).getTime();
                       var Diff = Math.floor((d2 - d1) / 1000);
                       //alert(Diff);
                       if (Diff >= 60) {
                           fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                       }

                   }
               }
           });
       }


   </script>
            <script type="text/javascript">
                shortcut.add("Ctrl+A", function () {
                    $('#breadcrumbs_text').removeClass('lowercase');
                    $('#breadcrumbs_text').addClass('uppercase');
                    $("#<%=hidBillType.ClientID%>").val('A');
                });
                shortcut.add("Ctrl+B", function () {
                    $('#breadcrumbs_text').removeClass('uppercase');
                    $('#breadcrumbs_text').addClass('lowercase');
                    $("#<%=hidBillType.ClientID%>").val('B');
                });
                function pageLoad() {
                    $('#divGidNo').css('visibility', 'hidden');
                    $("select").chosen({ width: '100%' });
                    $("#<%= txtAbstractno.ClientID %>").hide();
                    $("#<%= txtAbstractno.ClientID %>").attr('disabled', 'disabled');
                    if ($("#<%= hidGidMode.ClientID %>").val() == "Edit") {
                        $("#tblga [id*=gaRow]").find('td input[id*="chkSingle"]').attr('checked', 'checked');
                        $('.Barcode_fixed_headers td:nth-child(2)').css("display", "none");
                        $('.Barcode_fixed_headers th:nth-child(2)').css("display", "none");
                        $('.Barcode_fixed_headers td:nth-child(3)').css("display", "table-cell");
                        $('.Barcode_fixed_headers th:nth-child(3)').css("display", "table-cell");
                        fncGidSearch();
                        $('#divdate').hide();
                        $("#<%= lblvendor.ClientID %>").text('Abstract No');
                        $("#<%= txtVendor.ClientID %>").hide();
                        $("#<%= Label1.ClientID %>").text('AbstractValue');
                        $('#divGidNo').css('visibility', 'visible');
                        $('.inv_margin_border').hide();
                        $("#<%= txtAbstractno.ClientID %>").show();
                        $("#<%= txtLocation.ClientID %>").attr('disabled', 'disabled');
                        $('#breadcrumbs_text').text('Edit Abstract');
                    }
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                }

                $(document).ready(function () {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                    var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
                    $("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
                });

                //Open Delete Dialoag
                function fncGADleteDialog() {
                    try {
                        $("#deleteGA").dialog({
                            resizable: false,
                            height: 130,
                            width: 240,
                            modal: true,
                            title: "Enterpriser Web"
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                //Close Delete Dialog
                function fncCloseDeleteDialog() {
                    $("#deleteGA").dialog('close');
                }

                //Open GidHold Dialog
                function fncGidHoldDialogInitialize() {
                    try {
                        $("#divGidHold").dialog({
                            resizable: false,
                            height: 130,
                            width: 240,
                            modal: true,
                            title: "Enterpriser Web"
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                function fncViewClosedGRN(source) {
                    try {
                        var obj;
                        obj = $(source).parent().parent();


                        $("#<%=hidGidMode.ClientID%>").val('View');

                        var page = '';
                        if ($("#<%=hidTextileBrand.ClientID%>").val() == 'Y') {
                            page = '<%=ResolveUrl("~/Purchase/frmTextileBarcodePrint.aspx") %>';
                        }
                        else if ($("#<%=hidTextileLoc.ClientID%>").val() == 'Y') {
                            page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNoteTextiles.aspx") %>';
                        }
                        else {
                            page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                        }

                    var page = page + "?GRNNo=" + $.trim(obj.find('td span[id*="Label2"]').text()) + "&GANo=" + $.trim(obj.find('td span[id*="lblGANo"]').text());
                    var page = page + "&BillNo=" + $.trim(obj.find('td span[id*="lblBillNo"]').text()) + "&VendorCode=" + $.trim(obj.find('td span[id*="lblVendorcode"]').text());
                    var page = page + "&Status=" + $("#<%=hidGidMode.ClientID%>").val() + "&BillType=" + $("#<%=hidBillType.ClientID%>").val();
                        var page = page + "&LoadPoQty=N" + "&locationcode=" + $.trim(obj.find('td span[id*="lblLocationCode"]').text());
                        var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                            autoOpen: false,
                            modal: true,
                            height: 645,
                            width: 1300,
                            title: "VIEW GRN MAINTENANCE",
                            buttons: [
                                           {
                                               text: "Close",
                                               click: function () {
                                                   $(this).dialog("close");
                                               }
                                           }
                            ]
                        });

                        $dialog.dialog('open');
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }


                //Show Deleted GRN
                function fncShowDeletedGRN() {
                    try {
                        $("#divGRNDelete").dialog({
                            resizable: false,
                            height: 130,
                            width: 240,
                            modal: true,
                            title: "Enterpriser Web"
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }


                /// GA Row Click
                function fncGARowKeydown(evt, source) {
                    var rowobj, charCode, scrollheight;
                    try {
                        rowobj = $(source);
                        charCode = (evt.which) ? evt.which : evt.keyCode;
                        if (charCode == 13) {
                            return false;
                        }
                        else if (charCode == 113) {
                            fncGADeleteAndEditNew(rowobj, "Edit");
                        }
                        else if (charCode == 40) {
                            var NextRowobj = rowobj.next();
                            fncRowClick(NextRowobj)
                            if (NextRowobj.length > 0) {
                                NextRowobj.select().focus();

                                setTimeout(function () {
                                    scrollheight = $("#tblga tbody").scrollTop();
                                    if (parseInt(NextRowobj.find('td span[id*="lblSNo"]').text()) < 3) {
                                        $("#tblga tbody").scrollTop(0);
                                    }
                                }, 50);
                            }
                        }
                        else if (charCode == 38) {
                            var prevrowobj = rowobj.prev();
                            fncRowClick(prevrowobj);
                            if (prevrowobj.length > 0) {
                                prevrowobj.select().focus();

                                setTimeout(function () {
                                    scrollheight = $("#tblga tbody").scrollTop();
                                    if (parseFloat(scrollheight) > 3) {
                                        scrollheight = parseFloat(scrollheight) + 1;
                                        $("#tblga tbody").scrollTop(scrollheight);
                                    }
                                }, 50);
                            }
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                $(document).ready(function () {
                    $("#tblga tbody > tr").first().focus();
                    fncRowClick($("#tblga tbody > tr").first());
                });
                function fncShowSearchDialogVendor(event, Vendor, txtVendor, Next) {
                    var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

                    if (charCode == 112) {
                        fncShowSearchDialogCommon(event, Vendor, txtVendor, Next);
                        event.preventDefault();
                    }
                    else {
                        fncShowSearchDialogCommon(event, Vendor, txtVendor, Next);
                    }
                }

                function fncClear() {
                    $("#<%=rbnGRNDate.ClientID %>").attr('checked', 'checked');
            return true;
        }
        function fncSelectUnselectrow(event) {
            try {
                var selVal = 0;
                if (($(event).is(":checked"))) {
                    $("#tblga [id*=gaRow]").each(function () {
                        $(this).find('td input[id*="chkSingle"]').attr("checked", "checked");
                        selVal = parseFloat(selVal) + parseFloat($(this).find('td span[id*="lblPayValue"]').text());
                    });
                    $("#<%=txtSelectValue.ClientID %>").val(parseFloat(selVal).toFixed(2));
                    $("#<%=hidViewbtn.ClientID %>").val(parseFloat(selVal).toFixed(2));
                }
                else {
                    $("#tblga [id*=gaRow] input[id*='chkSingle']:checkbox").removeAttr("checked");
                    $("#<%=txtSelectValue.ClientID %>").val('0');
                    $("#<%=hidViewbtn.ClientID %>").val('0');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSelectSinglerow(event) {
            try {
                var selVal = 0, count = 0;
                $("#tblga [id*=gaRow]").each(function () {
                    if ($(this).find('td input[id*="chkSingle"]').is(':checked')) {
                        //selVal = parseFloat(selVal) + parseFloat($(this).find('td span[id*="lblNetAmount"]').text()) + parseFloat($(this).find('td span[id*="lblDebitValue"]').text());
                        selVal = parseFloat(selVal) + parseFloat($(this).find('td span[id*="lblPayValue"]').text());
                        count = parseFloat(count) + 1;
                    }
                });
                $("#<%=txtSelectValue.ClientID %>").val(parseFloat(selVal).toFixed(2));
                $("#<%=hidViewbtn.ClientID %>").val(parseFloat(selVal).toFixed(2));
                if (!($(event).is(":checked")))
                    $("#<%=cbProfilterheader.ClientID %>").removeAttr("checked");
                else if (parseFloat(count) == $("#tblga [id*=gaRow]").length)
                    $("#<%=cbProfilterheader.ClientID %>").attr("checked", "checked");

        }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSaveAbstract.ClientID %>').is(":visible"))
                        ValidateForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClose.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function ValidateForm() {
            try {
                $('#<%= lnkSaveAbstract.ClientID %>').css('display', 'none');
                if ($("#tblga [id*=gaRow]").length == 0) {
                    $('#<%= lnkSaveAbstract.ClientID %>').css('display', 'block');
                    ShowPopupMessageBox("Abstract Detail is Empty");
                    return false;
                }
                else if (parseFloat($('#<%= txtSelectValue.ClientID %>').val()) <= 0) {
                    $('#<%= lnkSaveAbstract.ClientID %>').css('display', 'block');
                    ShowPopupMessageBox("Abstract Value  is Must be Greater than Zero");
                    return false;
                }
                else {
                    fncGetGridValuesForSave();
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSaveAbstract', '');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncRowClick(source) {
            try {
                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "aliceblue");
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetGridValuesForSave() {
            var obj, count = 0;
            try {
                var xml = '<NewDataSet>';
                $("#tblga [id*=gaRow]").each(function () {
                    obj = $(this);
                    if (obj.find('td input[id*="chkSingle"]').is(':checked')) {
                        count = parseFloat(count) + 1;
                        xml += "<Table>";
                        xml += "<ReferenceNo>" + "" + "</ReferenceNo>";
                        xml += "<BillNo>" + obj.find('td span[id*=lblBillNo]').text().trim() + "</BillNo>";
                        xml += "<BillDate>" + obj.find('td span[id*=lblBillDate]').text().trim() + "</BillDate>";
                        xml += "<VendorCode>" + obj.find('td span[id*=lblVendorcode]').text().trim() + "</VendorCode>";
                        xml += "<VendorName>" + obj.find('td span[id*=lblVendorName]').text().trim() + "</VendorName>";
                        xml += "<GSTNo>" + obj.find('td span[id*=lblTinNo]').text().trim() + "</GSTNo>";
                        xml += "<BillAmt>" + obj.find('td span[id*=lblBillAmount]').text().trim() + "</BillAmt>";
                        xml += "<GST>" + obj.find('td span[id*=lbltax]').text().trim() + "</GST>";
                        xml += "<NetAmount>" + obj.find('td span[id*=lblPayValue]').text().trim() + "</NetAmount>";
                        xml += "<GANo>" + obj.find('td span[id*=lblGANo]').text().trim() + "</GANo>";
                        xml += "<GADate>" + obj.find('td span[id*=lblGADate]').text().trim() + "</GADate>";
                        xml += "<PONo>" + obj.find('td span[id*=lblPONo]').text().trim() + "</PONo>";
                        xml += "<GIDNo>" + obj.find('td span[id*=Label2]').text().trim() + "</GIDNo>";
                        xml += "<LocationCode>" + obj.find('td span[id*=lblLocationCode]').text().trim() + "</LocationCode>";
                        xml += "<GACreateUser>" + obj.find('td span[id*=lblCreateUser]').text().trim() + "</GACreateUser>";
                        xml += "<BillType>" + obj.find('td span[id*=lblBilltype]').text().trim() + "</BillType>";
                        xml += "<DebitNo>" + obj.find('td span[id*=Label3]').text().trim() + "</DebitNo>";
                        xml += "<DebitValue>" + obj.find('td span[id*=lblDebitValue]').text().trim() + "</DebitValue>";
                        xml += "<CreaditNo>" + obj.find('td span[id*=lblCreaditValue]').text().trim() + "</CreaditNo>";
                        xml += "<CreadiValue>" + obj.find('td span[id*=lblValue]').text().trim() + "</CreadiValue>";
                        xml += "</Table>";
                    }
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
                $('#<%=hidAbstractValue.ClientID %>').val(xml);
                $('#<%=hidCount.ClientID %>').val(count);
                return xml;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGidSearch() {
            try {
                $("[id$=txtGidNo]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        obj.location = $('#<%=txtLocation.ClientID %>').val();
                             $.ajax({
                                 url: '../Transfer/frmTransferOutAdd.aspx/fncGetGidNo',
                                 data: JSON.stringify(obj),
                                 dataType: "json",
                                 type: "POST",
                                 contentType: "application/json; charset=utf-8",
                                 success: function (data) {
                                     response($.map(data.d, function (item) {
                                         return {
                                             label: item.split('|')[0],
                                             valitemcode: item.split('|')[1]
                                         }
                                     }))
                                 },
                                 error: function (response) {
                                     ShowPopupMessageBox(response.message);
                                 },
                                 failure: function (response) {
                                     ShowPopupMessageBox(response.message);
                                 }
                             });
                         },
                         open: function (event, ui) {
                             var $input = $(event.target);
                             var $results = $input.autocomplete("widget");
                             var scrollTop = $(window).scrollTop();
                             var top = $results.position().top;
                             var height = $results.outerHeight();
                             if (top + height > $(window).innerHeight() + scrollTop) {
                                 newTop = top - height - $input.outerHeight();
                                 if (newTop > scrollTop)
                                     $results.css("top", newTop + "px");
                             }
                         },
                         focus: function (event, i) {
                             $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));

                            event.preventDefault();
                        },
                        select: function (e, i) {
                            $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));
                            return false;
                        },
                        minLength: 0
                     });
                        $(window).scroll(function (event) {
                            $('.ui-autocomplete.ui-menu').position({
                                my: 'left bottom',
                                at: 'left top',
                                of: '#ContentPlaceHolder1_txtGidNo'
                            });
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                function validateGidNo() {
                    try {
                        var status = true;
                        if ($('#<%=txtGidNo.ClientID %>').val() == "") {
                            ShowPopupMessageBox("PLease Enter Gid Number");
                            return false;
                        }
                        else {
                            $("#tblga [id*=gaRow]").each(function () {
                                obj = $(this);
                                if (obj.find('td span[id*=Label2]').text().trim() == $('#<%=txtGidNo.ClientID %>').val().trim())
                                        status = false;
                                });

                                if (status == false) {
                                    ShowPopupMessageBox("This GID Number is Already Exists - " + $('#<%=txtGidNo.ClientID %>').val());
                                $('#<%=txtGidNo.ClientID %>').val('');
                                return false;
                            }
                            else
                                return true;
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }
                var source;
                function fncAbsDeleteAndEdit(rowDelete, value) {
                    source = $(rowDelete).parent().parent();
                    if (value == 'Delete') {
                        fncShowSaveConfirmation_master("Do You Want Delete this Gid Number in this Transaction -" + source.find('td span[id*="Label2"]').text() + " ?.");
                        return false;
                    }
                }
                function fncYesClick_master() {
                    try {
                        $("#saveConfirmation_master").dialog("destroy");
                        //fncAbsDelete(source.find('td span[id*="Label2"]').text());  
                        var rowParentObj = source, selVal = 0, count =0;
                        rowParentObj.remove();
                        $("#tblga [id*=gaRow]").each(function (index) {
                            $(this).find('td span[id*=lblSNo]').html(index + 1);
                            selVal = parseFloat(selVal) + parseFloat($(this).find('td span[id*="lblPayValue"]').text());
                            count = parseFloat(count) + 1;
                        });
                $("#<%=txtSelectValue.ClientID %>").val(parseFloat(selVal).toFixed(2));
                $("#<%=hidViewbtn.ClientID %>").val(parseFloat(selVal).toFixed(2));
                        source = null;
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }
                function fncNoClick_master() {
                    $("#saveConfirmation_master").dialog("destroy");
                    source = null;
                }
                function fncAbsDelete(gidNo) {
                    try {
                        $.ajax({
                            type: "POST",
                            url: "frmCreateAbstract.aspx/fncAbsDelete",
                            data: "{ 'GidNo': '" + gidNo + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                if (msg.d != "") {
                                    var rowParentObj = source;
                                    rowParentObj.remove();
                                    $("#tblga [id*=gaRow]").each(function (index) {
                                        $(this).find('td span[id*=lblSNo]').html(index + 1);
                                    });
                                    source = null;
                                }
                                else {
                                    ShowPopupMessageBox(msg.d);
                                }

                            },
                            error: function (data) {
                                ShowPopupMessageBox(data.message);
                                return false;
                            }
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }
            </script>
        </asp:Content>
        <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <div class="barcodepahe_color">
                <div class="main-container">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                            <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                            <li><a style="text-decoration: none;">Abstract</a><i class="fa fa-angle-right"></i></li>
                            <li class="active-page uppercase" id="breadcrumbs_text">Create Abstract
                            </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        </ul>
                    </div>
                    <div class="container-group-full">
                        <div class="purchase-order-header barcodepahe_color">
                        </div>
                        <div class="control-group-split">
                            <asp:UpdatePanel ID="upgrnview" runat="server">
                                <ContentTemplate>
                                    <div class="ga_view_dropdown">
                                        <div class="ga_view_dropdown_split" style="width: 22% !important;">
                                            <div class="ga_label">
                                                <asp:Label ID="lblvendor" runat="server" Text="Vendor"></asp:Label>
                                            </div>
                                            <div class="ga_dropdown">
                                                <asp:TextBox ID="txtVendor" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogVendor(event, 'Vendor', 'txtVendor', 'txtLocation');"></asp:TextBox>
                                                <asp:TextBox ID="txtAbstractno" runat="server" Style="display: none;" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="ga_view_dropdown_split" style="width: 22% !important;">
                                            <div class="ga_label">
                                                <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>
                                            </div>
                                            <div class="ga_dropdown">
                                                <asp:TextBox ID="txtLocation" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogVendor(event, 'Location', 'txtLocation', 'txtFromDate');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-container" id="divdate" style="width: 56% !important;">
                                            <div class="col-md-2">
                                                <div class="control-button">
                                                    <asp:Label ID="lblfromdate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-button">
                                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="control-button">
                                                    <asp:Label ID="lbltodate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="control-button">
                                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" OnClick="lnkRefresh_Click"
                                                    Text="Refresh" OnClientClick="return fncReportValidation()"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ga_view_radiobutton">
                                        <div class="col-md-6">
                                            <div class="control-button inv_margin_border" style="height: 25px !important;">
                                                <asp:RadioButton ID="rbnGRNDate" runat="server" Text="GRN Date"
                                                    Visible="true" GroupName="grnpendingclosed" Checked="true" AutoPostBack="True" />
                                                <asp:RadioButton ID="rbnGADate" runat="server" Text="GA Date"
                                                    Visible="true" GroupName="grnpendingclosed" AutoPostBack="True" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                                <asp:Label ID="Label1" runat="server" Text="SelectValue" Style="font-weight: bold;"></asp:Label>
                                            </div>

                                            <div class="col-md-6">
                                                <asp:TextBox ID="txtSelectValue" runat="server" CssClass="form-control-res" Text="0" Style="text-align: right;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div id="divrepeater" class="Barcode_fixed_headers">
                        <asp:UpdatePanel ID="uprptrga" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table id="tblga" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">
                                                <asp:CheckBox ID="cbProfilterheader" onclick="fncSelectUnselectrow(this)" runat="server" />
                                            </th>
                                            <th scope="col" runat="server" id="thDelete">Delete
                                            </th>
                                            <th scope="col" runat="server" id="thEdit">Edit GA
                                            </th>
                                            <th scope="col" runat="server" id="thGrn">GRN
                                            </th>
                                            <th scope="col" runat="server" id="thGrnView">GRN
                                            </th>
                                            <th scope="col">BillNo/RefNo
                                            </th>
                                            <th scope="col">BillDate/RefDate
                                            </th>
                                            <th scope="col">VendorCode
                                            </th>
                                            <th scope="col">VendorName
                                            </th>
                                            <th scope="col">GSTIN No
                                            </th>
                                            <th scope="col">Gross Amount
                                            </th>
                                            <th scope="col">GST
                                            </th>
                                            <th scope="col">Total Amount
                                            </th>
                                            <th scope="col">Pay Amount
                                            </th>
                                            <th scope="col">G.A.No
                                            </th>
                                            <th scope="col">G.A.Date
                                            </th>
                                            <th scope="col">PO.No
                                            </th>
                                            <th scope="col">GRN.No
                                            </th>
                                            <th scope="col">Location
                                            </th>
                                            <th scope="col">CreateDate
                                            </th>
                                            <th scope="col">CreateUser
                                            </th>
                                            <th scope="col">Bill Type
                                            </th>
                                            <th scope="col">DebitNo
                                            </th>
                                            <th scope="col">Debit Value
                                            </th>
                                            <th scope="col">CreaditNo
                                            </th>
                                            <th scope="col">Creadit Value
                                            </th>
                                        </tr>
                                    </thead>
                                    <asp:Repeater ID="rptrGA" runat="server">
                                        <HeaderTemplate>
                                            <tbody id="BulkBarcodebody">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="gaRow" tabindex='<%#(Container.ItemIndex)%>' runat="server"
                                                onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                                <td>
                                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                                </td>
                                                <td runat="server" id="td1">
                                                    <asp:CheckBox ID="chkSingle" runat="server" onclick="fncSelectSinglerow(this)" />
                                                </td>
                                                <td runat="server" id="tdDelete">
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png"
                                                        OnClientClick="fncAbsDeleteAndEdit(this,'Delete');return false;" />
                                                </td>
                                                <td runat="server" id="tdEdit">
                                                    <asp:ImageButton ID="btnImgEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                        OnClientClick="fncAbsDeleteAndEdit(this,'Edit')" />
                                                </td>
                                                <td runat="server" id="tdGrn">
                                                    <asp:Button ID="btnGrn" runat="server" Text='<%$ Resources:LabelCaption,lblGo%>'
                                                        OnClientClick="fncAbsDeleteAndEdit(this,'Go');return false;" />
                                                </td>
                                                <td runat="server" id="tdGrnView">
                                                    <asp:Button ID="btnGrnView" runat="server" Text='<%$ Resources:LabelCaption,btn_View%>'
                                                        OnClientClick="fncViewClosedGRN(this); return false; " />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBillNo" runat="server" Text='<%# Eval("DOInvNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBillDate" runat="server" Text='<%# Eval("BillDate") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVendorcode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblTinNo" runat="server" Text='<%# Eval("CSTNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBillAmount" runat="server" Text='<%# Eval("BillAmount") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lbltax" runat="server" Text='<%# Eval("tax") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNetAmount" runat="server" Text='<%# Eval("NetAmount") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPayValue" runat="server" Text='<%# Eval("PayValue") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblGANo" runat="server" Text='<%# Eval("GANo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblGADate" runat="server" Text='<%# Eval("GADate") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPONo" runat="server" Text='<%# Eval("PONo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("GidNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblLocationCode" runat="server" Text='<%# Eval("LocationCode") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCreateUser" runat="server" Text='<%# Eval("CreateUser") %>' />
                                                </td>
                                                <td style="display: none">
                                                    <asp:Label ID="lblBilltype" runat="server" Text='<%# Eval("BillType") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("DebitNoteNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDebitValue" runat="server" Text='<%# Eval("Value") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCreaditValue" runat="server" Text='<%# Eval("CreaditNo") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblValue" runat="server" Text='<%# Eval("CreaditValue") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="purchase-order-header barcodepahe_color">
                    <div class="col-md-6" id="divGidNo">
                        <div class="col-md-2">
                            <asp:Label ID="Label4" runat="server" Text="Sel.GID No" Style="font-weight: bold;"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtGidNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" Text="Add" OnClick="lnkAdd_Click"
                                OnClientClick="return validateGidNo();"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="control-button" style="float: right !important;">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" PostBackUrl="../Masters/frmMain.aspx" Text="Close(F6)"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right !important;">
                            <asp:LinkButton ID="lnkSaveAbstract" runat="server" class="button-blue" OnClick="lnkSaveAbstract_Click"
                                Text="Make Abstract(F4)" OnClientClick="return ValidateForm();"></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="hiddencol">
                    <div id="deleteGA">
                        <div>
                            <asp:Label ID="lblGaDelete" runat="server"></asp:Label>
                        </div>
                        <div class="paymentDialog_Center">
                            <div class="PaymentDialog_yes">
                                <asp:UpdatePanel ID="updelete" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lnkbtnYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                            OnClientClick="return fncConfirmDeleteGA()"> </asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="PaymentDialog_No">
                                <asp:LinkButton ID="lnlbtnNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                                    OnClientClick="fncCloseDeleteDialog();return false;"> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div id="GAEdit">
                        <div>
                            <asp:Label ID="lbleditStatus" runat="server"></asp:Label>
                        </div>
                        <div class="dialog_center">
                            <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                OnClientClick="fncCloseEditDialog();return false;"> </asp:LinkButton>
                        </div>
                    </div>
                    <div id="divGidHold">
                        <div>
                            <asp:Label ID="lblGidHold" runat="server"></asp:Label>
                        </div>
                        <div class="paymentDialog_Center">
                            <div class="PaymentDialog_yes">
                                <asp:LinkButton ID="lnkgidHoldYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                    OnClientClick="fncConfirmToOpenHoldInvoice('Yes');return false;"> </asp:LinkButton>
                            </div>
                            <div class="PaymentDialog_No">
                                <asp:LinkButton ID="lnkgidHoldNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                                    OnClientClick="fncConfirmToOpenHoldInvoice('No');return false;"> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div id="divGRNDelete">
                        <div>
                            <asp:Label ID="lblGRNDelete" runat="server"></asp:Label>
                        </div>
                        <div class="paymentDialog_Center">
                            <div class="PaymentDialog_yes">
                                <asp:LinkButton ID="lnkGRNYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>' OnClientClick="fncConfirmGRNDelete('Yes');return false;"> </asp:LinkButton>
                            </div>
                            <div class="PaymentDialog_No">
                                <asp:LinkButton ID="lnkGRNNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>' OnClientClick="fncConfirmGRNDelete('No');return false;"> </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="upgidno" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btngidno" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:HiddenField ID="hidgaNo" runat="server" />
                    <asp:HiddenField ID="hidVendorcode" runat="server" />
                    <asp:HiddenField ID="hidGid" runat="server" />
                    <asp:HiddenField ID="hidBillNo" runat="server" />
                    <asp:Button ID="btnEdit" runat="server" />
                    <asp:Button ID="btnGrnGo" runat="server" />
                    <asp:HiddenField ID="hidLocation" runat="server" />
                    <asp:HiddenField ID="hidGidMode" runat="server" Value="" />
                    <asp:HiddenField ID="hidgaDate" runat="server" />
                    <asp:HiddenField ID="hidgaAmt" runat="server" />
                    <asp:HiddenField ID="hidBillType" runat="server" Value="A" />
                    <asp:HiddenField ID="hidComputerName" runat="server" />
                    <asp:HiddenField ID="hidDiscontinueEntry" runat="server" />

                    <asp:HiddenField ID="hidTextileBrand" runat="server" />
                    <asp:HiddenField ID="hidTextileLoc" runat="server" />

                    <div id="dialog-DiscontinueEntry">
                    </div>

                </div>

                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="uprptrga">
                    <ProgressTemplate>
                        <div class="modal-loader">
                            <div class="center-loader">
                                <img alt="" src="../images/loading_spinner.gif" />
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidAbstractValue" runat="server" />
            <asp:HiddenField ID="hidCount" runat="server" />
        </asp:Content>
