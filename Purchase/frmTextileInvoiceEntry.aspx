﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTextileInvoiceEntry.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmTextileInvoiceEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="../css/CategoryAttribute.css" rel="stylesheet" />--%>
    <style type="text/css">
      
    </style>
   
    <script type="text/javascript">
        function pageLoad() {
            try {
                //$("#thsize").hide();
                //$("#thcolour").hide();
                //$("#thvariety").hide();
                //$("#thcbrand").hide();
                //$("#thdesign").hide();
                //$("#thtype").hide();

                $('#<%=txtSNo.ClientID%>').focus();
                $(".chzn-container").css('display', 'none');
                $(".form-control-res").css('height', '25px');
                $(".form-control-res").css('display', 'block');
                //fncGetAttributes();
                if ($.session.get('height') != null && $.session.get('height') != "") {
                    var height = $.session.get('height');
                    height = parseFloat(height) - 310;
                    $("#tblAttribute tbody").css('height', '' + height + "px" + '');
                }
                fncDecimal();
                if ($('#<%=hidStatus.ClientID%>').val() == "1") {
                    //fncGetGADetails();
                }
                $('#<%=ddlProduct.ClientID%>').change(function () {
                    fncGetAttributes($(this).val());
                });
            
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $("#<%=chckGA.ClientID %>").click(function () {
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $('#<%=lblItemValue.ClientID%>').text(parseFloat($('#<%=txtGRcost.ClientID%>').val()));
                    $('#<%=lblTot.ClientID%>').text(parseFloat($('#<%=txttotal.ClientID%>').val()));
                }
                else {
                    $('#<%=lblItemValue.ClientID%>').text(parseFloat($('#<%=hiditem.ClientID%>').val()));
                    $('#<%=lblTot.ClientID%>').text(parseFloat($('#<%=hidtot.ClientID%>').val()));
                }
            });
        });
        function fncGetGADetails() {
            try {
                $.ajax({
                    url: "frmTextileInvoiceEntry.aspx/GetGaDetails",
                    data: "{ 'Code': '" + status + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        // fncBindData(jQuery.parseJSON(data.d));
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDecimal() {
            $('#<%=txtSNo.ClientID%>').number(true, 0);
            $('#<%=txtHsnCode.ClientID%>').number(true, 0);
            $('#<%=txtQty.ClientID%>').number(true, 2);
            $('#<%=txtCost.ClientID%>').number(true, 2);
            $('#<%=txtMargin.ClientID%>').number(true, 2);
            $('#<%=txtMRP.ClientID%>').number(true, 2);
            $('#<%=txtDisPer.ClientID%>').number(true, 2);
            $('#<%=txtSale.ClientID%>').number(true, 2);
            $('#<%=txtFinal.ClientID%>').number(true, 2);
        }
        function fncGetAttributes(Code) {
            $.ajax({
                url: "frmTextileInvoiceEntry.aspx/GetExistingCode",
                data: "{ 'Code': '" + Code + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    debugger
                    fncBindData(jQuery.parseJSON(data.d), Code);
                },
                error: function (response) {
                    ShowPopupMessageBox(response.responseText);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.responseText);
                }
            });
        }
        var select = "-1";
        var ddlselect = "---Select---"
        function fncBindData(obj, ProductCode) {
            try {
                var vars = {};
                var divstart = "";
                $("#divDynamic").empty();
                for (var i = 0; i < obj.Table.length - 1; i++) {
                    divstart = divstart + "<div class='col-md-12 Cat_Margin'><div class='col-md-6 Cat_Margin'><span class='labelorange'>" + obj.Table[i]["AttributeName"] + "</span></div><div class='col-md-6 Cat_Margin'><span class='labelorange'>" + obj.Table[i + 1]["AttributeName"] + "</span></div> <div class='col-md-6 Cat_Margin'><select id ='ddl_" + obj.Table[i]["AttributeName"] + "' class='form-control-res'>"
                    var Option = "";
                    Option = Option + "<option value=" + select + ">" + ddlselect + "</option>";
                    for (var j = 0; j < obj.Table1.length - 1; j++) {
                        if (obj.Table1[j]["AttributeCode"] == obj.Table[i]["AttributeCode"]) {
                            Option = Option + "<option value='" + obj.Table1[j]["ValueCode"] + "'>" + obj.Table1[j]["ValueName"] + "</option>";
                        }
                    }
                    divstart = divstart + Option + "</select></div><div class='col-md-6 Cat_Margin'><select id ='ddl_" + obj.Table[i + 1]["AttributeName"] + "'  class='form-control-res'>"
                    Option = "";
                    Option = Option + "<option value=" + select + ">" + ddlselect + "</option>";
                    for (var j = 0; j < obj.Table1.length - 1; j++) {
                        if (obj.Table1[j]["AttributeCode"] == obj.Table[i + 1]["AttributeCode"]) {
                            Option = Option + "<option value='" + obj.Table1[j]["ValueCode"] + "'>" + obj.Table1[j]["ValueName"] + "</option>";
                        }
                    }
                    divstart = divstart + Option + "</select></div></div>";
                    i = i + 1;
                }
                //divstart = divstart + "</div>";
                divstart = divstart.replace('<option></option>', '');
                $("#divDynamic").append(divstart);
                //$(".form-control-res").css('height', '25px');
                //$(".form-control-res").css('display', 'block');
                $.ajax({
                    url: "frmTextileInvoiceEntry.aspx/GetBindData",
                    data: "{ 'Code': '" + ProductCode + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d != null) {
                            var dt = data.d;
                            $('#<%=ddlItaxper.ClientID%>').val(dt);
                            var ddltax = $('#<%=ddlItaxper.ClientID%> option:selected').text();
                            var taxs = ddltax.split('-');
                            $('#<%=txttax.ClientID%>').val(taxs[1]);
                            //$("#ddl_COLOUR").val();OtaxCode3
                            //$("#ddl_VARIETY NAME").val();
                            //$("#ddl_SIZE").val();
                            //$("#ddl_CBRAND").val();
                            //$("#ddl_DESIGN").val();
                            //$("#ddl_TYPE").val();

                        }
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });

            }
            catch (ex) {
                ShowPopupMessageBox(ex.message);
            }
        }
        function fncClearAttribute() {
            $('#<%=txtSNo.ClientID%>').val('');
            $('#<%=ddlSize.ClientID%>').val('1');
            $('#<%=txtDesign.ClientID%>').val('');
            $('#<%=txtQty.ClientID%>').val(0);
            $('#<%=txtCost.ClientID%>').val(0);
            $('#<%=txtMargin.ClientID%>').val(0);
            $('#<%=txtSale.ClientID%>').val(0);
            $('#<%=txtMRP.ClientID%>').val(0);
            $('#<%=txtDisPer.ClientID%>').val(0);
            $('#<%=txtFinal.ClientID%>').val(0);
            $('#<%=txtBasicCost.ClientID%>').val(0);
            $('#<%=txtGrossCost.ClientID%>').val(0);
            $('#<%=txtTaxAmount.ClientID%>').val(0);
            $('#<%=txtnetcost.ClientID%>').val(0);
           
            $('#<%=txtSNo.ClientID%>').focus();
        }
        var SIZE = "";
        var COLOUR = "";
        var VARIETY = "";
        var CBRAND = "";
        var DESIGN = "";
        var TYPE = "";
        function fncAdd() {
            try {
                debugger
                var Show = '';
                if ($('#<%=ddlItaxper.ClientID%>').val() == "") {
                    Show = Show + 'Please Select Tax Percentage.';
                }
                <%--if ($('#<%=ddlSize.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Select Size.';
                }--%>
                if ($('#<%=txtSNo.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Serial Number.';
                }
                if ($('#<%=txtDesign.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Design Number.';
                }
                if ($('#<%=txtQty.ClientID%>').val() == "" || parseFloat($('#<%=txtQty.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Qty.';
                }
                if ($('#<%=txtBasicCost.ClientID%>').val() == "" || parseFloat($('#<%=txtBasicCost.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Cost.';
                }
              <%--  if ($('#<%=txtSale.ClientID%>').val() == "" || parseFloat($('#<%=txtSale.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Sale Price.';
                }--%>
              <%--  if ($('#<%=txtMRP.ClientID%>').val() == "" || parseFloat($('#<%=txtMRP.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid MRP.';
                }--%>
               <%-- if (parseFloat($('#<%=txtMRP.ClientID%>').val()) < parseFloat($('#<%=txtBasicCost.ClientID%>').val())) {
                    Show = Show + '<br /> Cost Must be Less than or Equal to MRP.';
                }--%>
                if (parseFloat($('#<%=txtMRP.ClientID%>').val()) < parseFloat($('#<%=txtSale.ClientID%>').val())) {
                    Show = Show + '<br /> Sale Price Must be Less than or Equal to MRP.';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                if ($('#ddl_SIZE option:selected').text() == "---Select---") {
                    SIZE = "";
                    //$("#thsize").hide();
                }
                else {
                    $("#thsize").show();
                    SIZE = $('#ddl_SIZE option:selected').text();
                }


                if ($('#ddl_COLOUR option:selected').text() == "---Select---") {
                    COLOUR = "";
                    //$("#thcolour").hide();
                }
                else {
                    $("#thcolour").show();
                    COLOUR = $('#ddl_COLOUR option:selected').text();
                }

                if ($('#ddl_VARIETY_NAME option:selected').text() == "---Select---") {
                   // $("#thvariety").hide();
                    VARIETY = "";
                }
                else {
                    $("#thvariety").show();
                    VARIETY = $('#ddl_VARIETY_NAME option:selected').text();
                }

                if ($('#ddl_CBRAND option:selected').text() == "---Select---") {
                   // $("#thcbrand").hide();
                    CBRAND = "";
                }
                else {
                    $("#thcbrand").show();
                    CBRAND = $('#ddl_CBRAND option:selected').text();
                }

                if ($('#ddl_DESIGN option:selected').text() == "---Select---") {
                    DESIGN = "";
                    //$("#thdesign").hide();
                }
                else {
                    $("#thdesign").show();
                    DESIGN = $('#ddl_DESIGN option:selected').text();
                }

                if ($('#ddl_TYPE option:selected').text() == "---Select---") {
                    TYPE = "";
                   // $("#thtype").hide();
                }
                else {
                    $("#thtype").show();
                    TYPE = $('#ddl_TYPE option:selected').text();
                }
                var tblGRNBody = $("#tblAttribute tbody");
                var maxRowNo = tblGRNBody.children().length;
                var rowno = "rowno";
                var row = "<tr id='GRNBodyrow_" + maxRowNo + "'  tabindex='" + maxRowNo + "'  >"

                    + "<td><input type='text' id='txtSNo_" + maxRowNo + "' value='" + $('#<%=txtSNo.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"RowNo\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                    + "<td><input type='text' id='txtProduct_" + maxRowNo + "' value='" + $('#<%=ddlProduct.ClientID%> option:selected').text() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' "
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Product\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                    + "<td><input type='text' id='txtDesign_" + maxRowNo + "' value='" + $('#<%=txtDesign.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Design\");'  /></td>"

                    + "<td><input type='text' id='txtHSN_" + maxRowNo + "' value='" + $('#<%=txtHsnCode.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"HSN\");'  onchange='fncRepeaterColumnValueChange(this,\"HSN\");'/></td>"

                    + "<td><input type='text' id='txtQty_" + maxRowNo + "' value='" + $('#<%=txtQty.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onchange='fncRepeaterColumnValueChange(this,\"Qty\");'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Qty\");' /></td>"

                    + "<td><input type='text' id='txtPrice_" + maxRowNo + "' value='" + $('#<%=txtBasicCost.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onchange='fncRepeaterColumnValueChange(this,\"BasicCost\");'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"BasicCost\");'  /></td>"

                    + "<td><input type='text' id='txtDisPer_" + maxRowNo + "' value='" + $('#<%=txtDisPer.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DisPer\");'  /></td>"

                    + "<td><input type='text' id='txttax" + maxRowNo + "' value='" + $('#<%=txttax.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"tax\");'  /></td>"

                    + "<td><input type='text' id='txtGrossCost" + maxRowNo + "' value='" + $('#<%=txtGrossCost.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"GrossCost\");'  /></td>"

                    + "<td><input type='text' id='txtNetCost" + maxRowNo + "' value='" + $('#<%=txtFinal.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"NetCost\");'  /></td>"
                    
                    + "<td><input type='text' id='ddl_SIZE" + maxRowNo + "' value='" + SIZE + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"SIZE\");'  onchange='fncRepeaterColumnValueChange(this,\"SIZE\");'/></td>"
                    //}
                    //if ($('#ddl_VARIETY NAME option:selected').text() != "---Select---") {
                    + "<td><input type='text' id='ddl_VARIETY" + maxRowNo + "' value='" + VARIETY + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"VARIETY\");'  onchange='fncRepeaterColumnValueChange(this,\"VARIETY\");'/></td>"
                    //}
                    //if ($('#ddl_CBRAND option:selected').text() != "--Select--") {
                    + "<td><input type='text' id='ddl_CBRAND" + maxRowNo + "' value='" + CBRAND + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"CBRAND\");'  onchange='fncRepeaterColumnValueChange(this,\"CBRAND\");'/></td>"
                    //}
                    //if ($('#ddl_DESIGN option:selected').text() != "--Select--") {
                    + "<td><input type='text' id='ddl_DESIGN" + maxRowNo + "' value='" + DESIGN + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DESIGN\");'  onchange='fncRepeaterColumnValueChange(this,\"DESIGN\");'/></td>"
                    //}
                    //if ($('#ddl_TYPE option:selected').text() != "--Select--") {
                    + "<td><input type='text' id='ddl_TYPE" + maxRowNo + "' value='" + TYPE + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TYPE\");'  onchange='fncRepeaterColumnValueChange(this,\"TYPE\");'/></td>"
                    //}
                    //if ($('#ddl_COLOUR option:selected').text() != "---Select---") {
                    + "<td><input type='text' id='ddl_COLOUR" + maxRowNo + "' value='" + COLOUR + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"COLOUR\");'  onchange='fncRepeaterColumnValueChange(this,\"COLOUR\");'/></td>"
                    //}
                    + "<td><input type='text' id='txBarcode_" + maxRowNo + "' value='" + Math.floor(100000 + Math.random() * 900000) + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + " onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Barcode\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                    <%--+ "<td><input type='text' id='txtFinal_" + maxRowNo + "' value='" + $('#<%=txtMRP.ClientID%>').val() + "'  "
                        + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                        + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Final\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                        + "<td><input type='text' id='txtNetCost_" + maxRowNo + "' value='" + parseFloat($('#<%=txtQty.ClientID%>').val() * $('#<%=txtSale.ClientID%>').val()).toFixed(2) + "'  "
                        + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                        + " onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Amount\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"--%>


                    + "<td  onclick ='DeleteRow(this);return false;' style='text-align: center !important;' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Del\");' ><span id='tdAction_" + maxRowNo + "' class ='glyphicon glyphicon-remove-circle' style ='font-size: 20px;color: red;cursor: pointer;' /></td>"

                    + "<td id ='tdProductID_" + maxRowNo + "' style='display:none !important;'>" + $('#<%=ddlProduct.ClientID%>').val() + "</td>"
                    + "<td id ='txDisPer_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtDisPer.ClientID%>').val() + "</td>"
                    + "<td id ='txDisAmt_" + maxRowNo + "' style='display:none !important'>" + $('#<%=hidDisAmt.ClientID%>').val() + "</td>"
                    + "<td id ='txMarginPer_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtMargin.ClientID%>').val() + "</td>"
                    + "<td id ='tdSPrice_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtGrossCost.ClientID%>').val() + "</td>"
                    + "<td id ='tdCost_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtCost.ClientID%>').val() + "</td>"
                    + "<td id ='tdGST_" + maxRowNo + "' style='display:none !important'>" + $('#<%=ddlItaxper.ClientID%> option:selected').val() + "</td>"
                    + "<td id ='tdPerGst_" + maxRowNo + "' style='display:none !important'>" + $('#<%=ddlItaxper.ClientID%> option:selected').text().split('-')[1] + "</td>"
                    + "<td id ='tdSize_" + maxRowNo + "' style='display:none !important'>" + $('#<%=ddlSize.ClientID%>').val() + "</td>" 
              

                + "<td><input type='text' id='txtTaxAmount_" + maxRowNo + "' value='" + $('#<%=txtTaxAmount.ClientID%>').val() + "'  "
                    +"' style='display:none !important'/></td ></tr >";
                tblGRNBody.append(row);                
                fncTotalCalculation();
                fncClearAttribute();
               
                //console.log(row);
                //$('#tblAttribute').parent().find('td').css("width", "69.4px !important");
                //$('#tblAttribute').parent().find('th').css("width", "69.4px !important");
                //document.getElementById("tblAttribute").style.td = "300px !important";
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Repeater Column Value Change Process
        function fncRepeaterColumnValueChange(source, value) {
            try {
                debugger
                var rowObj = $(source).parent().parent();
                if (value == 'HSN') {
                    if (rowObj.find('td input[id*="txtHSN"]').val() != '') {
                        var HSNCode_len = rowObj.find('td input[id*="txtHSN"]').val().length;
                        if (HSNCode_len != 0 && HSNCode_len != 2 && HSNCode_len != 4 && HSNCode_len != 8) {
                            rowObj.find('td input[id*="txtHSN"]').val('');
                            fncToastError('Enter Valid HSN Code(2 or 4 or 8 Digits Length or Empty)');
                            setTimeout(function () {
                                rowObj.find('td input[id*="txtHSN"]').focus().select();
                            }, 10);
                        }
                    }
                    return false;
                }

                var Qty = rowObj.find('td input[id*="txtQty"]').val();
                var Mrp = rowObj.find('td input[id*="txtMrp"]').val();
                var Price = rowObj.find('td input[id*="txtBasicCost"]').val();
                var DisPer = rowObj.find('td[id*="txDisPer"]').text();
                var OldPrice = rowObj.find('td[id*="tdSPrice"]').text();
                var MarginPer = rowObj.find('td[id*="txMarginPer"]').text();
                var Amount = 0;

                if (value == 'Qty') {
                    Amount = Qty * Price;
                }
                else if (value == 'BasicCost') {
                    if (parseFloat(Mrp) < parseFloat(Price)) {
                        rowObj.find('td input[id*="txtPrice"]').val(OldPrice);
                        setTimeout(function () {
                            rowObj.find('td input[id*="txtPrice"]').focus().select();
                        }, 10);
                        fncToastError('Sale Price Must be Less than or Equal to MRP.');
                    }
                    Amount = Price * (Price * DisPer / 100);
                    Amount = Qty * Amount;
                }
                rowObj.find('td input[id*="txtNetCost"]').val(parseFloat(Amount).toFixed(2));
                fncTotalCalculation();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetSelectAndFocus(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        ///Set Focus Next Row and Next Column
        function fncSetFocusNextRowandCol(evt, source, value) {
            var rowobj, rowId = 0, NextRowobj, prevrowobj;
            try {
                debugger
                rowobj = $(source).parent().parent();
                rowId = rowobj.find('td input[id*="txtSNo"]').val();
                rowId = parseFloat(rowId) - parseFloat(1);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                rowobj.css("background-color", "#ffffff");
                //// Enter and Right Arrow
                if (charCode == 13 || charCode == 39) {
                    rowobj.css("background-color", "#999999");
                    if (value == "RowNo") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtProduct"]'));
                        return false;
                    }
                    else if (value == "Product") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDesign"]'));
                        return false;
                    }
                    else if (value == "Design") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtHSN"]'));
                        return false;
                    }
                    else if (value == "HSN") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtQty"]'));
                        return false;
                    }
                    else if (value == "Qty") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtPrice"]'));
                        return false;
                    }
                    else if (value == "BasicCost") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtMrp"]'));
                        return false;
                    }
                    else if (value == "DisPer") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtSize"]'));
                        return false;
                    }
                    else if (value == "SIZE") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtFinal"]'));
                        return false;
                    }
                    else if (value == "Final") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetCost"]'));
                        return false;
                    }
                    else if (value == "Amount") {
                        fncSetSelectAndFocus(rowobj.find('td span[id*="tdAction"]'));
                        return false;
                    }
                    else if (value == "tax") {
                        fncSetSelectAndFocus(rowobj.find('td span[id*="txttax"]'));
                        return false;
                    }
                }
                ///// Left Arrow
                else if (charCode == 37) {
                    rowobj.css("background-color", "#999999");
                    if (value == "Product") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtSNo"]'));
                        return false;
                    }
                    else if (value == "Design") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtProduct"]'));
                        return false;
                    }
                    else if (value == "HSN") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDesign"]'));
                        return false;
                    }
                    else if (value == "Qty") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtHSN"]'));
                        return false;
                    }
                    else if (value == "Price") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtQty"]'));
                        return false;
                    }
                    else if (value == "MRP") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtPrice"]'));
                        return false;
                    }
                    else if (value == "SIZE") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtMrp"]'));
                        return false;
                    }
                    else if (value == "Final") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtSize"]'));
                        return false;
                    }
                    else if (value == "Amount") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtFinal"]'));
                        return false;
                    }
                    else if (value == "Del") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetCost"]'));
                        return false;
                    }
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    NextRowobj.css("background-color", "#999999");
                    if (NextRowobj.length == 0) {
                        rowobj.css("background-color", "#999999");
                        return false;
                    }
                    if (value == "RowNo") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSNo"]'));
                        return false;
                    }
                    else if (value == "Product") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtProduct"]'));
                        return false;
                    }
                    else if (value == "Design") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtDesign"]'));
                        return false;
                    }
                    else if (value == "HSN") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtHSN"]'));
                        return false;
                    }
                    else if (value == "Qty") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtQty"]'));
                        return false;
                    }
                    else if (value == "Price") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtPrice"]'));
                        return false;
                    }
                    else if (value == "MRP") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMrp"]'));
                        return false;
                    }
                    else if (value == "SIZE") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSize"]'));
                        return false;
                    }
                    else if (value == "Final") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtFinal"]'));
                        return false;
                    }
                    else if (value == "Amount") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtNetCost"]'));
                        return false;
                    }
                    else if (value == "Del") {
                        fncSetSelectAndFocus(NextRowobj.find('td span[id*="tdAction"]'));
                        return false;
                    }

                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    prevrowobj.css("background-color", "#999999");
                    if (prevrowobj.length == 0) {
                        return false;
                    }
                    if (value == "RowNo") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSNo"]'));
                        return false;
                    }
                    else if (value == "Product") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtProduct"]'));
                        return false;
                    }
                    else if (value == "Design") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtDesign"]'));
                        return false;
                    }
                    else if (value == "HSN") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtHSN"]'));
                        return false;
                    }
                    else if (value == "Qty") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtQty"]'));
                        return false;
                    }
                    else if (value == "Price") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtPrice"]'));
                        return false;
                    }
                    else if (value == "MRP") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtMrp"]'));
                        return false;
                    }
                    else if (value == "SIZE") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtSize"]'));
                        return false;
                    }
                    else if (value == "Final") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtFinal"]'));
                        return false;
                    }
                    else if (value == "Amount") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtNetCost"]'));
                        return false;
                    }
                    else if (value == "Del") {
                        fncSetSelectAndFocus(prevrowobj.find('td span[id*="tdAction"]'));
                        return false;
                    }
                }
                else {
                    return true;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncTotalCalculation() {
            try {
                debugger
                var Total = 0, Qty = 0, DisPer = 0, Tax = 0, Cost = 0,TotGr=0,TotTax=0;
                $("#tblAttribute tbody").children().each(function () {
                    Total = parseFloat(Total) + parseFloat($(this).find('td input[id*=txtNetCost]').val());
                    Qty = parseFloat(Qty) + parseFloat($(this).find('td input[id*=txtQty]').val());
                    DisPer = parseFloat(DisPer) + parseFloat($(this).find('td input[id*=txtDisPer]').val());
                    //parseFloat(DisPer) + parseFloat(parseFloat($(this).find('td input[id*=txtQty]').val()) * $(this).find('td[id*=txtDisPer]').text());
                    Cost = parseFloat(Cost) + parseFloat($(this).find('td input[id*=txtGrossCost]').val()); //parseFloat(Cost) + parseFloat(parseFloat($(this).find('td input[id*=txtQty]').val()) * $(this).find('td[id*=tdCost]').text());
                    Tax = parseFloat(Tax) + parseFloat($(this).find('td input[id*=txtTaxAmount]').val());
                        //parseFloat(Tax) + parseFloat($(this).find('td input[id*=tdTaxAmount_]').val());//parseFloat(Tax) + parseFloat($(this).find('td input[id*=txtQty]').val()) * parseFloat($(this).find('td[id*=tdCost]').text() * parseFloat($(this).find('td[id*=tdPerGst]').text()) / 100).toFixed(2);
                    TotTax = parseFloat(TotTax) + (parseFloat($(this).find('td input[id*=txtQty]').val()) * parseFloat($(this).find('td input[id*=txtTaxAmount]').val()));
                    TotGr = parseFloat(TotGr) + (parseFloat($(this).find('td input[id*=txtQty]').val()) * parseFloat($(this).find('td input[id*=txtGrossCost]').val()));
                });
                $('#<%=txtGRcost.ClientID%>').val(parseFloat(TotGr).toFixed(2));
                $('#<%=txtGST.ClientID%>').val(parseFloat(TotTax).toFixed(2));
                $('#<%=txttotal.ClientID%>').val(parseFloat(Total).toFixed(2));
                $('#<%=lblTotal.ClientID%>').text((parseFloat(Total)).toFixed(2));
                $('#<%=lblQty.ClientID%>').text(parseFloat(Qty).toFixed(2));
                $('#<%=lblDiscount.ClientID%>').text(parseFloat(DisPer).toFixed(2));
                $('#<%=lbltaxable.ClientID%>').text(parseFloat(Cost).toFixed(2));
                $('#<%=lblTax.ClientID%>').text(parseFloat(Tax).toFixed(2));
             

                $('#<%=hidItemTotal.ClientID%>').val(parseFloat(Total).toFixed(2));
                $('#<%=hidTotQty.ClientID%>').val(parseFloat(Qty).toFixed(2));
                $('#<%=hidTotDiscount.ClientID%>').val(parseFloat(DisPer).toFixed(2));
                $('#<%=hidtaxable.ClientID%>').text(parseFloat(Cost).toFixed(2));
                $('#<%=hidTax.ClientID%>').text(parseFloat(Tax).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncChangeValue(value) {
            try {
                debugger
                // if (value == 'Cost' || value == 'Margin') {
                var CostMargin = parseFloat($('#<%=txtCost.ClientID%>').val()) + (parseFloat($('#<%=txtCost.ClientID%>').val()) * parseFloat($('#<%=txtMargin.ClientID%>').val()) / 100);
                $('#<%=txtGrossCost.ClientID%>').val(parseFloat(CostMargin).toFixed(2));
                //}
                // else if (value == 'Sell' || value == 'Dis') {
                var DisAmt = (parseFloat($('#<%=txtGrossCost.ClientID%>').val()) * parseFloat($('#<%=txtDisPer.ClientID%>').val()) / 100)
                var SellMar = parseFloat($('#<%=txtGrossCost.ClientID%>').val()) - parseFloat(DisAmt);
                $('#<%=txtGrossCost.ClientID%>').val(parseFloat(SellMar).toFixed(2));
                $('#<%=hidDisAmt.ClientID%>').val(parseFloat(DisAmt).toFixed(2));
                //  }

                var tax = 0;
                if ($('#ContentPlaceHolder1_ddlItaxper option:selected').text() != "---Select---")
                    tax = parseFloat(CostMargin * parseFloat($('#<%=ddlItaxper.ClientID%> option:selected').text().split('-')[1]) / 100).toFixed(2);

                $('#<%=txtGrossCost.ClientID%>').val(parseFloat($('#<%=txtGrossCost.ClientID%>').val()) + parseFloat(tax));

                var Finalval = parseFloat($('#<%=txtQty.ClientID%>').val()) * parseFloat($('#<%=txtGrossCost.ClientID%>').val());

                $('#<%=txtFinal.ClientID%>').val(parseFloat(Finalval).toFixed(2));
                var ddltax = $('#<%=ddlItaxper.ClientID%> option:selected').text();
                var taxs = ddltax.split('-');
                $('#<%=txttax.ClientID%>').val(taxs[1]);
                funSumValue();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function DeleteRow(source) {
            var Itemcode = $(source).closest('tr').find('td input[id*=txtSNo]').val();
            var tblVat = 0;
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                open: function () {
                    var markup = 'Do you want Delete this S.No? -' + Itemcode;
                    $(this).html(markup);
                },
                buttons: {
                    "NO": function () {
                        $(this).dialog("close");
                    },
                    "YES": function () {
                        $(this).dialog("close");
                        $(source).closest('tr').remove();
                        fncTotalCalculation();
                    }
                }
            });
        }
        $(document).ready(function () {
            $('#tblAttribute tbody tr').live('click', function (event) {
                $('#tblAttribute tbody tr').css("background-color", "white");
                $(this).closest('tr').css("background-color", "#999999");
            });
        });
        //// GRN Row Double Click
        function fncGRNRowdblClick(source) {
            try {
                fncEnableAllColumns();
            }
            catch (err) {
                fncToastError(err.message);
            }

        }
        function fncSave() {            debugger
            try {
                var xml = '<NewDataSet>';
                var rowNo = 0;

                var totalRow = $("#tblAttribute tbody").children().length;
                if (totalRow < 1) {
                    ShowPopupMessageBox('Invalid Details.')
                    return false;
                }
                if (parseFloat($('#<%=lblTot.ClientID%>').text()) != parseFloat($('#<%=lblTotal.ClientID%>').text())) {
                    ShowPopupMessageBox('GA Total and Purchase Total are MisMatch.')
                    return false;
                }

                $("#tblAttribute tbody").children().each(function () {
                    var obj = $(this);
                    var Final1 = parseFloat(obj.find('td input[id*=txtQty]').val().trim())
                    var Final2 = parseFloat(obj.find('td input[id*=txtNetCost]').val().trim());
                    var Final3 = parseFloat(Final1) * parseFloat(Final2);
                    xml += "<Table>";
                    xml += "<SNo>" + obj.find('td input[id*=txtSNo]').val().trim() + "</SNo>";
                    xml += "<Product>" + obj.find('td input[id*=txtProduct]').val().trim() + "</Product>";
                    xml += "<Design>" + obj.find('td input[id*=txtDesign]').val().trim() + "</Design>";
                    xml += "<HsnCode>" + obj.find('td input[id*=txtHSN]').val().trim() + "</HsnCode>";
                    xml += "<Qty>" + obj.find('td input[id*=txtQty]').val().trim() + "</Qty>";
                    xml += "<SPrice>" + obj.find('td input[id*=txtPrice]').val().trim() + "</SPrice>";
                    xml += "<Mrp>" + obj.find('td input[id*=txtPrice]').val().trim() + "</Mrp>";
                    xml += "<Size>" + $('#ddl_SIZE option:selected').text().trim() + "</Size>";
                    xml += "<FPrice>" + Final3 + "</FPrice>";
                    xml += "<NetAmount>" + obj.find('td input[id*=txtNetCost]').val().trim() + "</NetAmount>";
                    xml += "<DiscountPer>" + obj.find('td[id*=txDisPer]').text().trim() + "</DiscountPer>";
                    xml += "<DisAmt>" + obj.find('td[id*=txDisPer]').text().trim() + "</DisAmt>";
                    xml += "<Margin>" + obj.find('td[id*=txMarginPer]').text().trim() + "</Margin>";
                    xml += "<Remarks>" + "PUR" + "</Remarks>";
                    xml += "<Cost>" + obj.find('td input[id*=txtNetCost]').val().trim() + "</Cost>";
                    xml += "<Gst>" + obj.find('td[id*=tdGST]').text().trim() + "</Gst>";
                    xml += "<GstPer>" + obj.find('td[id*=tdPerGst]').text().trim() + "</GstPer>";
                    xml += "</Table>";
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
                $('#<%=hidSaveSata.ClientID %>').val(xml);
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

       <%-- function funBCSumValue() {
            var GCAmount = parseFloat($('#<%=txtBasicCost.ClientID%>').val()) - (parseFloat($('#<%=txtDisPer.ClientID%>').val()));
            if (isNaN(GCAmount)) {
                GCAmount = parseFloat(0).toFixed(2);
            }
            $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GCAmount).toFixed(2));
        }--%>
        function funSumValue() {

            var GCAmount = parseFloat($('#<%=txtBasicCost.ClientID%>').val()) - (parseFloat($('#<%=txtDisPer.ClientID%>').val()));
            if (isNaN(GCAmount)) {
                GCAmount = parseFloat(0).toFixed(2);
            }
            $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GCAmount).toFixed(2));




            var TaxAmount = parseFloat($('#<%=txtGrossCost.ClientID%>').val()) * (parseFloat($('#<%=txttax.ClientID%>').val())/100);
            if (isNaN(TaxAmount)) {
                TaxAmount = parseFloat(0).toFixed(2);
            }
            $('#<%=txtTaxAmount.ClientID%>').val(parseFloat(TaxAmount).toFixed(2));

            var NA = parseFloat($('#<%=txtGrossCost.ClientID%>').val()) + (parseFloat($('#<%=txtTaxAmount.ClientID%>').val()));
            if (isNaN(NA)) {
                NA = parseFloat(0).toFixed(2);
            }
            $('#<%=txtnetcost.ClientID%>').val(parseFloat(NA).toFixed(2));


            var final = parseFloat($('#<%=txtnetcost.ClientID%>').val()) * (parseFloat($('#<%=txtQty.ClientID%>').val()));
            if (isNaN(final)) {
                final = parseFloat(0).toFixed(2);
            }
            $('#<%=txtFinal.ClientID%>').val(parseFloat(final).toFixed(2));


        }
      <%--  function txtTaxAmount() {
            var final = parseFloat($('#<%=txtnetcost.ClientID%>').val()) * (parseFloat($('#<%=txtQty.ClientID%>').val()));
            if (isNaN(final)) {
                final = parseFloat(0).toFixed(2);
            }
            $('#<%=txtFinal.ClientID%>').val(parseFloat(final).toFixed(2));
        };--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Purchase</a> <i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmTextileGA.aspx">Textile GA</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Invoice Entry</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="EnableScroll">
            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-6">
                    <span class="glyphicon glyphicon-arrow-left hedertext"></span><span class="whole-price-header">Invoice Entry</span>
                </div>
                <div class="col-md-6">
<%--                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="glyphicon glyphicon-floppy-saved" OnClientClick="return fncSave();" OnClick="lnkSave_Click">
                           <span class="btnSpan">Save</span></asp:LinkButton>
                            <div class="glyphicon glyphicon-chevron-left gylSeperator"></div>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="glyphicon glyphicon-floppy-remove" OnClientClick="fncClear();return false;">
                             <span class="btnSpan">Clear</span></asp:LinkButton>
                        </div>
                    </div>--%>
                </div>
                <div class="col-md-12 headerborder">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-12 Cat_Margin">
                        <span class="labelorange">Product</span>
                    </div>
                    <div class="col-md-12 Cat_Margin">
                        <asp:DropDownList ID="ddlProduct" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="labelorange">HSN Code</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <span class="labelorange">S.Tax</span>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtHsnCode" runat="server" placeholder="HSN Code" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:DropDownList ID="ddlItaxper" runat="server" CssClass="form-control-res" onchange="fncChangeValue('');return false;">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12" id="divDynamic">
                </div>
            </div>
            <div class="col-sm-10">
                <div class="transfer-order-header" style="background-color: #e6e6e6; margin-top: 3px;">
                    <div class="col-md-12 Cat_Margins">
                        <div class="col-md-1 Cat_Margins">
                            <span class="lblBlack">SUPPLIER</span>
                        </div>
                        <div class="col-md-3 Cat_Margins">
                            <asp:Label ID="lblSuppiler" runat="server" Text="Default" Style="font-weight: 700;"></asp:Label>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <span class="lblBlack">GA No</span>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <asp:Label ID="lblGANo" runat="server" Text="Default" Style="font-weight: 700;"></asp:Label>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <span class="lblBlack">GR Amt</span>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <asp:Label ID="lblItemValue" runat="server" CssClass="labelorange" Text="0.00"></asp:Label>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <span class="lblBlack">Total Payable</span>
                        </div>
                        <div class="col-md-1 Cat_Margins">
                            <asp:Label ID="lblTot" runat="server" CssClass="labelorange" Text="0"></asp:Label>
                        </div>
                        <div class="col-md-2 Cat_Margins">
                            <asp:CheckBox ID="chckGA" runat="server" />
                            <span>Fill GA </span>
                        </div>
                    </div>
                </div>
                <div class="transfer-order-header" style="background-color: #e6e6e6; margin-top: 3px;height:100px" >
                    <div class="col-md-12 ">
                        <div class="containertext" style="margin-right: 3px">
                            <div class="spilt lblblue">
                                <asp:Label ID="lblItemCode" runat="server" Text="S.No"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtSNo" runat="server" Style="border-radius: 4px;" CssClass="form-control-res" onfocus="this.select();"></asp:TextBox>
                            </div>
                        </div>
                        <div style="display: none; margin-right: 3px;" class="container3text">
                            <div class="spilt lblred">
                                <asp:Label ID="lbSize" runat="server" Text="Size"></asp:Label>
                            </div>
                            <div>
                                <asp:DropDownList ID="ddlSize" runat="server" CssClass="">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="container3text" style="margin-right: 3px;">
                            <div class="spilt lblred">
                                <asp:Label ID="Label1" runat="server" Text="Design No"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtDesign" Style="text-align: right; border-radius: 4px;" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="containertext" style="margin-right: 3px">
                            <div class="spilt lblblue">
                                <asp:Label ID="Label4" runat="server" Text="Qty"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtQty" runat="server" onfocus="this.select();" CssClass="form-control-res" Style="text-align: right; border-radius: 4px;"
                                    onchange="funSumValue(); return false;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="containertext" style="margin-right: 3px">
                            <div class="spilt lblblue">
                                <asp:Label ID="Label5" runat="server" Text="Basic Cost"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtBasicCost" runat="server" onfocus="this.select();" Text="0.00" Style="text-align: right; border-radius: 4px;"
                                    CssClass="form-control-res" onchange="funSumValue(); return false;"></asp:TextBox>
                            </div>
                            <div style="display: none">
                                <asp:TextBox ID="txtCost" runat="server" onfocus="this.select();" Style="text-align: right; border-radius: 4px;"
                                    CssClass="form-control-res" onchange="fncChangeValue('Cost');return false;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="container3text" style="margin-right: 3px;">
                            <div style="display: none" class="spilt lblblue">
                                <asp:Label ID="Label6" runat="server" Text="Margin %"></asp:Label>
                            </div>
                            <div style="display: none">
                                <asp:TextBox ID="txtMargin" runat="server" onfocus="this.select();" Text="0" Style="text-align: right; border-radius: 4px;"
                                    CssClass="form-control-res" onchange="fncChangeValue('Margin');return false;"></asp:TextBox>
                            </div>
                            <div class="spilt lblblue">
                                <asp:Label ID="lblDis" runat="server" Text="Discount (₹)"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtDisPer" runat="server" onfocus="this.select();" CssClass="form-control-res" Text="0" Style="text-align: right; border-radius: 4px;" onchange="funSumValue(); return false;"></asp:TextBox><%--onchange="fncChangeValue('Dis');return false;"--%>
                            </div>
                        </div>
                        <div class="container3text" style="margin-right: 3px;">
                            <div style="display: none">
                                <div class="spilt lblblue">
                                    <asp:Label ID="Label7" runat="server" Text="Sale"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtSale" runat="server" onfocus="this.select();" Style="text-align: right; border-radius: 4px;"
                                        CssClass="form-control-res" onchange="fncChangeValue('Sell');return false;"></asp:TextBox>
                                </div>
                            </div>
                            <div class="spilt lblblue">
                                <asp:Label ID="Label16" runat="server" Text="Gross Cost"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtGrossCost" runat="server" ReadOnly="true" onfocus="this.select();" Text="0.00" Style="text-align: right; border-radius: 4px; border-radius: 4px;"
                                    CssClass="form-control-res" onchange="fncChangeValue('GrossCost');return false;"></asp:TextBox>
                            </div>


                        </div>
                        <div class="containertext" style="width: 7% !important; margin-right: 3px;">
                            <div style="display: none;" class="spilt lblblue">
                                <asp:Label ID="Label8" runat="server" onfocus="this.select();" Text="MRP"></asp:Label>
                            </div>
                            <div style="display: none;">
                                <asp:TextBox ID="txtMRP" runat="server" onfocus="this.select();" Style="text-align: right; border-radius: 4px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="spilt lblblue">
                                <asp:Label ID="Label2" runat="server" onfocus="this.select();" Text="Tax %"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txttax" Text="0" runat="server" onfocus="this.select();" Style="text-align: right; border-radius: 4px;" onchange="funSumValue(); return false;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="containertext" style="margin-right: 3px">
                            <%--<div class="spilt lblblue">
                            <asp:Label ID="lblDis" runat="server" Text="Dis%"></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtDisPer" runat="server" onfocus="this.select();" CssClass="form-control-res" Text="0"
                                onchange="fncChangeValue('Dis');return false;"></asp:TextBox>
                        </div>--%>
                            <div class="spilt lblblue">
                                <asp:Label ID="Label9" runat="server" onfocus="this.select();" Text="Tax Amount"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtTaxAmount" runat="server" onfocus="this.select();" ReadOnly="true" Style="text-align: right; border-radius: 4px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="containertext" style="margin-right: 3px">

                            <div class="spilt lblblue">
                                <asp:Label ID="Label10" runat="server" onfocus="this.select();" Text="Net Cost"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtnetcost" runat="server" onfocus="this.select();" ReadOnly="true" Style="text-align: right; border-radius: 4px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="containertext" style="margin-right: 3px;">
                            <div class="spilt lblblue">
                                <asp:Label ID="lblFinal" runat="server" Text="Final"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFinal" runat="server" onfocus="this.select();" ReadOnly="true" Style="text-align: right; border-radius: 4px;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="container12">
                            <%-- <div class="spilt lblblue">
                                <asp:Label ID="Label3" Style="visibility: hidden;" runat="server" Text="Final"></asp:Label>
                            </div>--%>
                          
                        </div>
                    </div>
                    
                        <div class="col-md-12 ">
                            <div class="containertext" style="margin-right: 2px">
                                <div class="spilt lblblue">
                                    <asp:Label ID="lblGR" runat="server" Text="GRAmt"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtGRcost" runat="server" Style="border-radius: 4px;" CssClass="form-control-res" ReadOnly="true" Text="0.00"></asp:TextBox>
                                </div>
                            </div>
                           
                           <div class="containertext" style="margin-right: 3px;">
                                <div class="spilt lblblue">
                                    <asp:Label ID="lblGST" runat="server" Text="GST"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txtGST" Style="text-align: right; border-radius: 4px;" runat="server" CssClass="form-control-res" ReadOnly="true" Text="0.00"></asp:TextBox>
                                </div>
                           </div>
                            <div class="containertext" style="margin-right: 3px;">
                                <div class="spilt lblblue">
                                    <asp:Label ID="lblTotalPay" runat="server" Text="TotalPayable"></asp:Label>
                                </div>
                                <div>
                                    <asp:TextBox ID="txttotal" Style="text-align: right; border-radius: 4px;" runat="server" CssClass="form-control-res" ReadOnly="true" Text="0.00"></asp:TextBox>
                                </div>
                            </div>
                          <div>
                             <br />
                                <div class="float_right">
                                    <asp:LinkButton ID="lnlkAdd" runat="server" OnClientClick="fncAdd();return false;" CssClass="button-blue" >Add
                                    </asp:LinkButton>
                           
                                    <asp:LinkButton ID="lnkClearAttribute" runat="server"  class="button-green"  OnClientClick="fncClearAttribute();return false;">Clear
                                    </asp:LinkButton>
                          
                               
                                </div></div>
                           
                     </div>
                    </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="Payment_fixed_headers Cat_Margin">
                        <table id="tblAttribute" cellspacing="0" rules="all" border="1" style="width: 100%; overflow-y: scroll; overflow-x: scroll">
                            <thead>
                                <tr>
                                    <th style="width:63px">SI.</th>
                                    <th style="width:65px">Product</th>
                                    <th style="width:63px">Design No</th>
                                    <th style="width:62px">HSN</th>
                                    <th style="width:65px">Qty</th>
                                    <th style="width:63px">B.Cost</th>
                                    <th>Discount</th>
                                    <th style="width:66px">Tax</th>
                                    <th style="width:62px">GR.Cost</th>
                                    <th style="width:64px">NetCost</th>
                                    <th id="thsize" style="width:63px">Size</th>
                                    <th id="thvariety" style="width:62px">Variety</th>
                                    <th id="thcbrand" style="width:66px">Cbrand</th>
                                    <th id="thdesign" style="width:63px">Design  </th>
                                    <th id="thtype" style="width:61px">Type  </th>
                                    <th id="thcolour" style="width:65px">Colour</th>
                                    <th>Barcode</th>
                                    <%--<th >F.Price</th>--%>
                                    <%-- <th >Amount</th>--%>
                                    <th style="width:32px"></th>
                                    <th style="display: none !important;">ProductID_</th>
                                    <th style="display: none !important;">DisPer_</th>
                                    <th style="display: none !important;">DisAmt_</th>
                                    <th style="display: none !important;">MarginPer_</th>
                                    <th style="display: none !important;">SPrice_</th>
                                    <th style="display: none !important;">Cost_</th>
                                    <th style="display: none !important;">GST_</th>
                                    <th style="display: none !important;">PerGst_</th>
                                    <th style="display: none !important;">Size_</th>
                                    <th style="display: none !important;">TaxAmount_</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="col-md-12 Cat_Margin" style="display:none">
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">Total</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:Label ID="lblTotal" runat="server" Text="0.00" Style="font-weight: 900;"></asp:Label>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblblue">Taxable</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:Label ID="lbltaxable" runat="server" CssClass="labelorange" Text="0.00"></asp:Label>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblblue">Tax</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:Label ID="lblTax" runat="server" CssClass="labelorange" Text="0.00"></asp:Label>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblblue">Discount</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:Label ID="lblDiscount" runat="server" CssClass="labelorange" Text="0.00"></asp:Label>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblblue">Qty</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:Label ID="lblQty" runat="server" CssClass="labelorange" Text="0.00"></asp:Label>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control-res">
                            <asp:ListItem Value="">Status</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                  <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return fncSave();" OnClick="lnkSave_Click">Save
                          </asp:LinkButton>
                      
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="fncClear();return false;">Clear
                             </asp:LinkButton>
                        </div>
                    </div>
            </div>
        </div>
        <div class="hidden">
            <div id="dialog-confirm" style="display: none;" title="Enterpriser Web"></div>
            <asp:HiddenField ID="hidDisAmt" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidSaveSata" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidTotDiscount" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidItemTotal" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidTax" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidtaxable" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidTotQty" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidDoNo" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidGATranNo" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidStatus" runat="server" Value="0"></asp:HiddenField>
            <asp:HiddenField ID="hidtot" runat="server" />
             <asp:HiddenField ID="hiditem" runat="server" />
        </div>
    </div>
</asp:Content>
