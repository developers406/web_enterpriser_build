﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCategoryPurchase.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmCategoryPurchase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />
    <script type="text/javascript">
        function pageLoad() {
            $(".chzn-container").css('display', 'none');
            $(".form-control-res").css('height', '25px');
            $(".form-control-res").css('display', 'block');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Purchase</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transport Entry</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Do You Want Clear ?</p>
        </div>
        <div id="dialog-Save" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Saved Successfully. Do You Want Clear ?</p>
        </div>
        <div class="EnableScroll" style="width: 100%; background-color: white;"> 
            <div class="col-md-6">
                <span class="glyphicon glyphicon-arrow-left hedertext"></span><span class="whole-price-header">Warehouse/Transport Entry</span>
            </div>
            <div class="col-md-6">
                <div class="button-contol">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="glyphicon glyphicon-floppy-saved"   OnClientClick="return fncSave();">
                           <span class="btnSpan">Save(F4)</span></asp:LinkButton>
                        <div class="glyphicon glyphicon-chevron-left gylSeperator" ></div>
                    </div>
                    
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="glyphicon glyphicon-remove" OnClientClick="fncClear();return false;">
                             <span class="btnSpan">Clear(F6)</span></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-md-12 headerborder"  >
            </div>

            <div class="col-md-6">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblLRMode" runat="server" Text="LR Mode/No"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlDelivery" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtLrNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblLRLDate" runat="server" Text="LR Date"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtLRDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblRecDate" runat="server" Text="Received Date"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtRecDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblSupplier" runat="server" Text="Supplier"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlSuplier" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblAgen" runat="server" Text="Agent/Commission"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlAgent" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtCommision" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblTransport" runat="server" Text="Transport"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlTransport" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblFromCity" runat="server" Text="From City"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlFromCity" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblToCity" runat="server" Text="Receiving City"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlReceivingCity" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblAuto" runat="server" Text="Auto Transfer Location"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlAuto" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblPurchaseManager" runat="server" Text="Purchase Manage"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlPurchaseManager" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblStock" runat="server" Text="Stock Holding Period(days)"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtStockHoldingdays" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblMargin" runat="server" Text="Margin"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtMargin" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblBUndles" runat="server" Text="No of Bundles"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtBundles" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblBoxes" runat="server" Text="No of Boxes"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtBoxes" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblPieces" runat="server" Text="No of Pieces"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtPieces" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblWeight" runat="server" Text="A.Wgt/C.Wgt"></asp:Label>
                    </div>
                    <div class="col-md-1" style="width: 12.5%;">
                        <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1" style="width: 12.5%;">
                        <asp:TextBox ID="txtCWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblFreightCharge" runat="server" Text="Freight Charge"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:CheckBox ID="chkFreightCharge" runat="server" Class="radioboxlist" Font-Bold="True" Checked="true" />
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblLrEntry" runat="server" Text="LR Entry Date"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtLREntryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblLREntryNo" runat="server" Text="LR Entry No"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtLREntryNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblDueDate" runat="server" Text="Due Date"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtDueDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblPayMode" runat="server" Text="Pay Mode"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice No"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtInvoiceNO" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblInvdate" runat="server" Text="Inv Date"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtInvDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblPackageSkip" runat="server" Text="PackageSkip No"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtPackageSkipNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                        <asp:Label ID="lblSkipDate" runat="server" Text="Skip Date"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtSkipDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <span style="font-size: 18px; font-weight: 700;">File Attachments</span>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="Label1" runat="server" Text="Type"></asp:Label>
                    </div>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-3">
                        <asp:Label ID="lblImages" runat="server" Text="Image"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:Image ID="ImgUpload" runat="server" Height="100px" Width="165px" Style="display: none;" />
                        <asp:FileUpload ID="ImageUpload" runat="server" onchange="showpreview(this);" />
                    </div>
                    <div class="col-md-3">
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button_Add" OnClientClick="return fncSave();">
                            <i class="icon-play"></i>Add</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="Payment_fixed_headers Cat_Margin">
                        <table id="tblAttribute" cellspacing="0" rules="all" border="1" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width: 150px;" scope="col">Image Type
                                    </th>
                                    <th style="width: 505px;" scope="col">Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody style="height: 170px; overflow-y: scroll;">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
