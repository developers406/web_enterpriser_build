﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmVendorCataLog.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmVendorCataLog" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .ui-tooltip, .arrow:after {
            background: black;
            border: 2px solid white;
        }

        .ui-tooltip {
            padding: 10px 20px;
            color: white;
            border-radius: 20px;
            font: bold 14px "Helvetica Neue", Sans-Serif;
            text-transform: uppercase;
            box-shadow: 0 0 7px black;
        }

        .arrow {
            width: 70px;
            height: 16px;
            overflow: hidden;
            position: absolute;
            left: 50%;
            margin-left: -35px;
            bottom: -16px;
        }

            .arrow.top {
                top: -16px;
                bottom: auto;
            }

            .arrow.left {
                left: 20%;
            }

            .arrow:after {
                content: "";
                position: absolute;
                left: 20px;
                top: -20px;
                width: 25px;
                height: 25px;
                box-shadow: 6px 5px 9px -9px black;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            .arrow.top:after {
                bottom: -20px;
                top: auto;
            }

        .content :hover {
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function pageLoad() {
            $('#ContentPlaceHolder1_searchFilterUserControl_Label1').html("Vendor <span class='mandatory'>*</span>");
        }
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'VendorCatalog');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "VendorCatalog";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript" language="Javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //alert($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.find('input[id *= chkSingle]').prop('checked', true);
                tr.next().find('input[id *= txtNetSellingPrice]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function fncValidateCheckAll() {
            try {
                var gridtr = $("#<%= gvVendorCat.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvVendorCat.ClientID %> [id*=chkSingle]").prop('checked', true);

                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvVendorCat.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateExportNoItems() {
            try {
                var gridtr = $("#<%= gvVendorCat.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems

                    fncShowAlertNoItemsMessage();
                    return false;

                }
                else {
                    return true;

                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateAllZero() {
            try {
                var gridtr = $("#<%= gvVendorCat.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    $("#<%=gvVendorCat.ClientID %> [id*=txtNewQty]").val('0.000');
                    //alert($("#<%=gvVendorCat.ClientID %> [id*=chkSingle]").is(":checked"));
                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvVendorCat.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvVendorCat.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvVendorCat.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#<%=LinkButton3.ClientID %>").css('display', 'none');
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdate").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdate").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdate").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        function clearForm() {
            //alert("Clear");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("#<%= gvVendorCat.ClientID %>").remove();
            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }

        function txtSellingPrice_TextChanged(lnk) {
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var sMRP = row.cells[4].getElementsByTagName("input")[0].value;
            var sSell = row.cells[16].getElementsByTagName("input")[0].value;
            if (parseFloat(sSell) > parseFloat(sMRP)) {
                row.cells[16].getElementsByTagName("input")[0].value = row.cells[16].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                validation = true;
                ShowPopupMessageBox("MRP must be Greater than or Equal to Selling Price", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                return;
            }
        }
        //txtBasicCostNew_TextChanged

        function txtBasicCostNew_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sBasicCost = row.cells[5].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[4].getElementsByTagName("input")[0].value;
                var sNetCost = row.cells[13].innerHTML;
                var sSell = row.cells[16].getElementsByTagName("input")[0].value;

                if (sMRP != "") {
                    if (parseFloat(sBasicCost) > parseFloat(sMRP)) {
                        row.cells[4].getElementsByTagName("input")[0].value = row.cells[4].getElementsByTagName("span")[0].innerHTML;
                        row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to BasicCost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                        return;
                    }
                    else if (parseFloat(sNetCost) > parseFloat(sMRP)) {
                        row.cells[4].getElementsByTagName("input")[0].value = row.cells[4].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                        validation = true;
                        ShowPopupMessageBox("MRP must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                        return;
                    }

                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;

                var Dis1 = row.cells[6].getElementsByTagName("input")[0].value;
                var Dis2 = row.cells[7].getElementsByTagName("input")[0].value;
                var Dis3 = row.cells[8].getElementsByTagName("input")[0].value;
                var InputVat = row.cells[11].innerHTML;
                var Margin = row.cells[18].innerHTML;
                var OutputVatPer = row.cells[14].innerHTML;
                if (Dis1 == "")
                    Dis1 = 0;
                if (Dis2 == "")
                    Dis2 = 0;
                if (Dis3 == "")
                    Dis3 = 0;
                if (InputVat == "")
                    InputVat = 0;
                if (sBasicCost == "")
                    sBasicCost = 0;
                if (OutputVatPer == "")
                    OutputVatPer = 0;
                if (Margin == "")
                    Margin = 0;

                var VatAmt = 0;
                var TotDiscAmt;
                var DiscountedBasicCost;
                var DiscAmt1 = (sBasicCost * parseFloat(Dis1).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = DiscAmt1;
                DiscountedBasicCost = (parseFloat(sBasicCost) - parseFloat(Dis1)).toFixed(2)
                var DiscAmt2 = (DiscountedBasicCost * parseFloat(Dis2).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                var DiscAmt3 = (DiscountedBasicCost * parseFloat(Dis3).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                var GrossCost = parseFloat(sBasicCost) - parseFloat(TotDiscAmt);
                VatAmt = (GrossCost * parseFloat(InputVat).toFixed(2) / 100).toFixed(2);
                var NetCost = parseFloat(GrossCost) + parseFloat(VatAmt);

                lnk.parentNode.parentNode.cells[9].value = parseFloat(TotDiscAmt).toFixed(2);
                row.cells[9].innerHTML = parseFloat(TotDiscAmt).toFixed(2);
                row.cells[10].innerHTML = parseFloat(GrossCost).toFixed(2);
                row.cells[13].innerHTML = parseFloat(NetCost).toFixed(2);
                row.cells[12].innerHTML = parseFloat(VatAmt).toFixed(2);

                var BasicSelling = parseFloat(parseFloat(GrossCost) + parseFloat(GrossCost * Margin / 100)).toFixed(2);

                //var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                //var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                //var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                //var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                //var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer);

                var TotalTaxAmt = parseFloat(BasicSelling * OutputVatPer / 100).toFixed(2);

                //var OutputCGSTAmt = parseFloat(TotalTaxAmt / 2).toFixed(2);
                //var OutputSGSTAmt = OutputCGSTAmt;
                //var OutputCESSAmt = parseFloat(BasicSelling * OutputCESSPer / 100).toFixed(2);

                var NetSellingPrice = parseFloat(BasicSelling) + parseFloat(TotalTaxAmt);

                //txtBasicSelling.val(BasicSelling);
                //txtOutputCGSTAmt.val(OutputCGSTAmt);
                //txtOutputSGSTAmt.val(OutputSGSTAmt);
                //txtOutputCESSAmt.val(OutputCESSAmt);
                //txtSellingPrice.val(NetSellingPrice);
                row.cells[14].innerHTML = parseFloat(TotalTaxAmt).toFixed(2);
                row.cells[16].getElementsByTagName("input")[0].value = parseFloat(NetSellingPrice).toFixed(2);
                //txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));
                sSell = parseFloat(NetSellingPrice).toFixed(2);

                if (parseFloat(row.cells[16].getElementsByTagName("input")[0].value) > parseFloat(row.cells[4].getElementsByTagName("input")[0].value)) {
                    row.cells[16].getElementsByTagName("input")[0].value = row.cells[16].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                    validation = true;
                    ShowPopupMessageBox("MRP must be Greater than or Equal to Selling Price - " + sSell, "#ContentPlaceHolder1_gvVendorCat_txtNetSellingPrice_" + rowIndex);
                    return;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function txtDiscountBasicPer_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sBasicCost = row.cells[3].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[2].getElementsByTagName("input")[0].value;
                var sNetCost = row.cells[13].innerHTML;

                if (sMRP != "") {
                    if (parseFloat(sBasicCost) > parseFloat(sMRP)) // SellingPrice > MRP
                    {
                        row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to BasicCost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                        return;
                    }
                    //else if (parseFloat(sNetCost) > parseFloat(sMRP)) {
                    //    row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                    //    validation = true;
                    //    ShowPopupMessageBox("MRP must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex); 
                    //    return;
                    //}
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False

                var Dis1 = row.cells[6].getElementsByTagName("input")[0].value;
                var Dis2 = row.cells[7].getElementsByTagName("input")[0].value;
                var Dis3 = row.cells[8].getElementsByTagName("input")[0].value;
                var InputVat = row.cells[11].innerHTML;

                //fncBasicCalculation(sBasicCost, Dis1, Dis2, Dis3, InputVat);

                var VatAmt = 0;
                var TotDiscAmt;
                var DiscountedBasicCost;
                var DiscAmt1 = (sBasicCost * parseFloat(Dis1).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = DiscAmt1;
                DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(Dis1)).toFixed(2)
                var DiscAmt2 = (DiscountedBasicCost * parseFloat(Dis2).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                var DiscAmt3 = (DiscountedBasicCost * parseFloat(Dis3).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                var GrossCost = parseFloat(sBasicCost) - parseFloat(TotDiscAmt);
                VatAmt = (GrossCost * parseFloat(InputVat).toFixed(2) / 100).toFixed(2);
                var NetCost = parseFloat(GrossCost) + parseFloat(VatAmt);

                row.cells[9].innerHTML = parseFloat(TotDiscAmt).toFixed(2);
                row.cells[10].innerHTML = parseFloat(GrossCost).toFixed(2);
                row.cells[13].innerHTML = parseFloat(NetCost).toFixed(2);


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function txtDiscountBasicPer2_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sBasicCost = row.cells[3].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[2].getElementsByTagName("input")[0].value;
                var sNetCost = row.cells[13].innerHTML;

                if (sMRP != "") {
                    if (parseFloat(sBasicCost) > parseFloat(sMRP)) // SellingPrice > MRP
                    {
                        row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to BasicCost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                        return;
                    }
                    //else if (parseFloat(sNetCost) > parseFloat(sMRP)) {
                    //    row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                    //    validation = true;
                    //    ShowPopupMessageBox("MRP must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex); 
                    //    return;
                    //}
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False

                var Dis1 = row.cells[6].getElementsByTagName("input")[0].value;
                var Dis2 = row.cells[7].getElementsByTagName("input")[0].value;
                var Dis3 = row.cells[8].getElementsByTagName("input")[0].value;
                var InputVat = row.cells[11].innerHTML;

                //fncBasicCalculation(sBasicCost, Dis1, Dis2, Dis3, InputVat);

                var VatAmt = 0;
                var TotDiscAmt;
                var DiscountedBasicCost;
                var DiscAmt1 = (sBasicCost * parseFloat(Dis1).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = DiscAmt1;
                DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(Dis1)).toFixed(2)
                var DiscAmt2 = (DiscountedBasicCost * parseFloat(Dis2).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                var DiscAmt3 = (DiscountedBasicCost * parseFloat(Dis3).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                var GrossCost = parseFloat(sBasicCost) - parseFloat(TotDiscAmt);
                VatAmt = (GrossCost * parseFloat(InputVat).toFixed(2) / 100).toFixed(2);
                var NetCost = parseFloat(GrossCost) + parseFloat(VatAmt);

                row.cells[9].innerHTML = parseFloat(TotDiscAmt).toFixed(2);
                row.cells[10].innerHTML = parseFloat(GrossCost).toFixed(2);
                row.cells[13].innerHTML = parseFloat(NetCost).toFixed(2);


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function txtDiscountBasicPer3_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sBasicCost = row.cells[3].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[2].getElementsByTagName("input")[0].value;
                var sNetCost = row.cells[13].innerHTML;

                if (sMRP != "") {
                    if (parseFloat(sBasicCost) > parseFloat(sMRP)) // SellingPrice > MRP
                    {
                        row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to BasicCost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex);
                        return;
                    }
                    //else if (parseFloat(sNetCost) > parseFloat(sMRP)) {
                    //    row.cells[3].getElementsByTagName("input")[0].value = row.cells[3].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                    //    validation = true;
                    //    ShowPopupMessageBox("MRP must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvVendorCat_txtBasicCostNew_" + rowIndex); 
                    //    return;
                    //}
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False

                var Dis1 = row.cells[6].getElementsByTagName("input")[0].value;
                var Dis2 = row.cells[7].getElementsByTagName("input")[0].value;
                var Dis3 = row.cells[8].getElementsByTagName("input")[0].value;
                var InputVat = row.cells[11].innerHTML;

                //fncBasicCalculation(sBasicCost, Dis1, Dis2, Dis3, InputVat);

                var VatAmt = 0;
                var TotDiscAmt;
                var DiscountedBasicCost;
                var DiscAmt1 = (sBasicCost * parseFloat(Dis1).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = DiscAmt1;
                DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(Dis1)).toFixed(2)
                var DiscAmt2 = (DiscountedBasicCost * parseFloat(Dis2).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                var DiscAmt3 = (DiscountedBasicCost * parseFloat(Dis3).toFixed(2) / 100).toFixed(2);
                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                var GrossCost = parseFloat(sBasicCost) - parseFloat(TotDiscAmt);
                VatAmt = (GrossCost * parseFloat(InputVat).toFixed(2) / 100).toFixed(2);
                var NetCost = parseFloat(GrossCost) + parseFloat(VatAmt);

                row.cells[9].innerHTML = parseFloat(TotDiscAmt).toFixed(2);
                row.cells[10].innerHTML = parseFloat(GrossCost).toFixed(2);
                row.cells[13].innerHTML = parseFloat(NetCost).toFixed(2);


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtMrp_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtMrp"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[5].getElementsByTagName("input")[0].select();
            }
            if (charCode == 37) {
                row.cells[16].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtMrp"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtMrp"]').select();

                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtMrp"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtMrp"]').select();
                }
            }
            return true;
        }
        function txtBasicCostNew_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtBasicCostNew"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[6].getElementsByTagName("input")[0].select();
            }
            if (charCode == 37) {
                row.cells[4].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtBasicCostNew"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtBasicCostNew"]').select();

                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtBasicCostNew"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtBasicCostNew"]').select();
                }
            }
            return true;
        }
        function txtDiscountBasicPer_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtDiscountBasicPerNew"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 37) {
                row.cells[5].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDiscountBasicPerNew"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDiscountBasicPerNew"]').select();
                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDiscountBasicPerNew"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDiscountBasicPerNew"]').select();
                }
            }
            return true;
        }
        function txtDiscountBasicPer2_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtDiscountBasicPer2"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 37) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDiscountBasicPer2"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDiscountBasicPer2"]').select();

                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDiscountBasicPer2"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDiscountBasicPer2"]').select();
                }
            }
            return true;
        }
        function txtDiscountBasicPer3_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtDiscountBasicPer3"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[16].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 37) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDiscountBasicPer3"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDiscountBasicPer3"]').select();
                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDiscountBasicPer3"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDiscountBasicPer3"]').select();
                }
            }
            return true;
        }
        function txtNetSellingPrice_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                var NextRowobj = rowobj.next();
                NextRowobj.find('td input[id*="txtNetSellingPrice"]').select();
                event.preventDefault();
            }
            if (charCode == 39) {
                row.cells[5].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 37) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtNetSellingPrice"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtNetSellingPrice"]').select();
                }
            }
            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtNetSellingPrice"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtNetSellingPrice"]').select();
                }
            }
            return true;
        }

        function fncVenItemRowdblClk(itemcode) {
            try {
                fncOpenItemhistory($.trim(itemcode));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function fncGetCatlogItem(ItemCode, Description) {
            var obj = {};
            try {
                obj.sMode = "Catlog";
                obj.sItemcode = ItemCode;
                $.ajax({
                    type: "POST",
                    url: "frmVendorCataLog.aspx/fncGetCatlogDetail",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var objItem = jQuery.parseJSON(msg.d);
                        //alert(Description);
                        $('#lblDescription').html(Description);
                        $("#<%=txtCatMrp.ClientID %>").val(objItem.Table[0]["MRP"]);
                        $("#<%=txtBasicCost.ClientID %>").val(objItem.Table[0]["BasicCost"]);
                        $("#<%=txtNetCost.ClientID %>").val(objItem.Table[0]["NetCost"]);
                        $("#<%=txtSellingPrice.ClientID %>").val(objItem.Table[0]["NetSellingPrice"]);

                        $('#<%=txtCatMrp.ClientID%>').number(true, 2);
                        $('#<%=txtBasicCost.ClientID%>').number(true, 2);
                        $('#<%=txtNetCost.ClientID%>').number(true, 2);
                        $('#<%=txtSellingPrice.ClientID%>').number(true, 2);

                        $("#divVendorCatelog").dialog({
                            resizable: false,
                            height: 190,
                            width: 400,
                            modal: true,
                            title: "Enterpriser Web - " + ItemCode//,
                            //open: function (event, ui) {
                            //    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                            //}
                        });
                        $("#divVendorCatelog").dialog('open');
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                            <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">
                            <%=Resources.LabelCaption.lbl_Vendor_Catalog%></li>
                        <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
                    </ul>
                    <a href="#" class="trigger">TEST</a>
                </div>
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                            EnableVendorDropDown="true"
                            EnableDepartmentDropDown="true"
                            EnableCategoryDropDown="true"
                            EnableSubCategoryDropDown="true"
                            EnableBrandDropDown="true"
                            EnableMerchandiseDropDown="true"
                            EnableManufactureDropDown="true"
                            EnableFloorDropDown="true"
                            EnableSectionDropDown="true"
                            EnableBinDropDown="true"
                            EnableShelfDropDown="true"
                            EnableWarehouseDropDown="true"
                            EnableBatchNoTextBox="false"
                            EnableBarcodeTextBox="false"
                            EnableItemCodeTextBox="true"
                            EnableItemNameTextBox="true"
                            EnableFilterButton="true"
                            EnableClearButton="true"
                            OnClearButtonClientClick="clearForm(); return false;"
                            OnFilterButtonClick="lnkLoadFilter_Click" />
                    </div>
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                        style="height: 500px">
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 420px">
                            <asp:GridView ID="gvVendorCat" runat="server" Width="100%" AutoGenerateColumns="False" OnRowDataBound="grdItemDetails_RowDataBound"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <%--<RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />--%>
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:TemplateField><%--0--%>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VendorCode" HeaderText="VendorCode"></asp:BoundField>
                                    <%--1--%>
                                    <asp:BoundField DataField="InventoryCode" HeaderText="InventoryCode"></asp:BoundField>
                                    <%--2--%>
                                    <asp:TemplateField HeaderText="Description" ItemStyle-Width="150"><%--3--%>
                                        <ItemTemplate>
                                            <asp:Label ID="txtDescription" Width="150px" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-Width="95"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="MRP" ItemStyle-Width="150"><%--4--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMrp" Width="70px" runat="server" Text='<%# Eval("MRP") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtMrp" Width="70px" runat="server" Text='<%# Eval("MRP") %>'
                                                Style="text-align: right" onkeyup="return txtMrp_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtBasicCostNew_TextChanged(this);" CssClass="grid-textbox" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--<asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="BasicCost" ItemStyle-Width="150"><%--5--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBasicCost" Width="70px" runat="server" Text='<%# Eval("BasicCost") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtBasicCostNew" Width="70px" runat="server" Text='<%# Eval("BasicCost") %>'
                                                Style="text-align: right" onkeyup="return txtBasicCostNew_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtBasicCostNew_TextChanged(this);" CssClass="grid-textbox" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DiscountBasicPer" ItemStyle-Width="150"><%--6--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscountBasicPer" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtDiscountBasicPerNew" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer") %>'
                                                onkeyup="return txtDiscountBasicPer_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtBasicCostNew_TextChanged(this);" CssClass="grid-textbox" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DiscountBasicPer2" ItemStyle-Width="150"><%--7--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscountBasicPer2" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer2") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtDiscountBasicPer2" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer2") %>'
                                                onkeyup="return txtDiscountBasicPer2_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtBasicCostNew_TextChanged(this);" CssClass="grid-textbox" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DiscountBasicPer3" ItemStyle-Width="150"><%--8--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiscountBasicPer3" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer3") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtDiscountBasicPer3" Width="70px" runat="server" Text='<%# Eval("DiscountBasicPer3") %>'
                                                onkeyup="return txtDiscountBasicPer3_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtBasicCostNew_TextChanged(this);" CssClass="grid-textbox" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DiscountBasicAmt" HeaderText="Discount Amt 1"></asp:BoundField>
                                    <%--9--%>
                                    <asp:BoundField DataField="GrossCost" HeaderText="GrossCost" />
                                    <%--10--%>
                                    <asp:BoundField DataField="InputVatPer" HeaderText="Input Vat %" />
                                    <%--11--%>
                                    <asp:BoundField DataField="InputVatAmt" HeaderText="Input Vat Amt" />
                                    <%--12--%>
                                    <asp:BoundField DataField="NetCost" HeaderText="NetCost" />
                                    <%--13--%>
                                    <asp:BoundField DataField="OutPutVatPer" HeaderText="OutPut Vat %" />
                                    <%--14--%>
                                    <asp:BoundField DataField="OutPutVatAmt" HeaderText="OutPut Vat Amt" />
                                    <%--15--%>
                                    <asp:TemplateField HeaderText="Net Selling Price" ItemStyle-Width="85"><%--16--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSellingPrice" Width="70px" runat="server" Text='<%# Eval("NetSellingPrice") %>'
                                                Style="display: none !important;"></asp:Label>
                                            <asp:TextBox ID="txtNetSellingPrice" Width="70px" runat="server" Text='<%# Eval("NetSellingPrice") %>'
                                                onkeypress="return isNumberKey(event)" CssClass="grid-textbox"
                                                onkeyup="return txtNetSellingPrice_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="return txtSellingPrice_TextChanged(this);" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="InvVen" ItemStyle-CssClass="display_none" />
                                    <%--17--%>
                                    <asp:BoundField DataField="EarnedMargin" ItemStyle-CssClass="display_none" />
                                    <%--18--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="float: right; display: table">
        <div class="control-button">
            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick="fncValidateSave();return false;"
                Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
        </div>
        <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdate">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'>
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
            </div>
        </div>
        <div id="divVendorCatelog">
            <label id="lblDescription" style="color: #5f609c; font-size: 14px; font-weight: bold;"></label>
            <div class="gid_medicalbatch_lable">
                <asp:Label ID="Label45" runat="server" Text="MRP"></asp:Label>
            </div>
            <div class="gid_medicalbatch_textbox">
                <asp:TextBox ID="txtCatMrp" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="gid_medicalbatch_lable">
                <asp:Label ID="Label46" runat="server" Text="Basic Cost"></asp:Label>
            </div>
            <div class="gid_medicalbatch_textbox">
                <asp:TextBox ID="txtBasicCost" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
            </div>

            <div class="gid_medicalbatch_lable">
                <asp:Label ID="Label1" runat="server" Text="NetCost"></asp:Label>
            </div>
            <div class="gid_medicalbatch_textbox">
                <asp:TextBox ID="txtNetCost" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
            </div>
            <div class="gid_medicalbatch_lable">
                <asp:Label ID="Label3" runat="server" Text="Selling Price"></asp:Label>
            </div>
            <div class="gid_medicalbatch_textbox">
                <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
            </div>
        </div>
    </div>

</asp:Content>
