﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmPurchaseOrder.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrder"
    MaintainScrollPositionOnPostback="true" %>

<%--<%@ Register TagPrefix="ups" TagName="ItemHistory" Src="~/UserControls/ItemHistory.ascx" %>--%>
<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
        
        
       .gvVendorLoadItemdummy td:nth-child(14){     /* musaraf 30112022*/
                                                      
           min-width:80px !important;
           max-width:80px !important;

       }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <link type="text/css" href="../css/autopo.css" rel="stylesheet" />
    <link type="text/css" href="../css/grnnote.css" rel="stylesheet" /> 
    <%------------------------------------------- DateTimePicker -------------------------------------------%>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'PurchaseOrder');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "PurchaseOrder";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        var vatDetailtbody, vatRowNo;
        $(document).ready(function () {
            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $(document).on('keydown', disableFunctionKeys);
            //fncChangeGridColor();
        });

        function fncVendorSearch(event) {
            if ($("#<%= hidVendorCode.ClientID %>").val() == '')
                fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');
            else
                return false;
        }

    </script>
    <script type="text/javascript">
        var txtItemCodeAdd;
        var txtItemNameAdd;
        var txtTotalDiscPrc;
        var txtTotalDiscRs;
        var txtTotal;
        var txtSubTotal;
        var txtGST;
        var txtNetTotal;
        var sStatus;
        var hdfTaxType;
    </script>
    <%------------------------------------------- Page Load -------------------------------------------%>
    <script type="text/javascript">


        function fncDisableSaleshistory() {
            try {
                $('#divItemhistory').css('display', 'none');
                $('#locWiseSaleshis').css('display', 'none');
                $('#divPoPending').css('display', 'none');
                $('#divLastFoc').css('display', 'none');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);

            }
        }

        function fncInitLoad() {
            try {
                fncDisableSaleshistory();
                //fncDisableAndEnableOnVendorChange(true);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncDisableAndEnableOnVendorChange(status) {
            try {
                if (status == true) {
                    $('#<%=txtBrand.ClientID %>').attr("readonly", "readonly");
                    $('#<%=chkVendorItems.ClientID %>').attr("disabled", "disabled");
                    $('#<%=txtItemCodeAdd.ClientID %>').attr("readonly", "readonly");
                }
                else {
                    $('#<%=txtBrand.ClientID %>').removeAttr("readonly");
                    $('#<%=chkVendorItems.ClientID %>').removeAttr("disabled");
                    $('#<%=txtItemCodeAdd.ClientID %>').removeAttr("readonly");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function pageLoad() {
            if ($('#<%=hidSearchMRP.ClientID %>').val() != "") {
                $('#txtmrp').val($('#<%=hidSearchMRP.ClientID %>').val());
            }
            fncDisableSaleshistory();

            if ($("[id*=hdfTabName]").val().trim() == "ViewAddInv") {
                $('#<%=hidSave.ClientID %>').val('save');
            }
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            if ($("#<%=hidSave.ClientID %>").val() == "Show") {
                $('#divVendorItem').show();
                $('#divSelectedItem').hide();
            }
            else {
                $('#divVendorItem').hide();
                $('#divSelectedItem').show();
            }
            <%-- var newOption = {};
            var option = '';
            $("#<%=ddlSort.ClientID %>").empty();

            $("[id*=tblgvVendorLoadItemdummy] th").each(function (e) {
                var index = $(this).index();
                var table = $(this).closest('table');
                var val = table.find('.click th').eq(index).text();

                if ($(this).is(":visible")) {
                    option += '<option value="' + val + '">' + val + '</option>';
                }
            });
            $("#<%=ddlSort.ClientID %>").append(option);
            $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");--%>
           <%-- if ($("#<%=hidsort.ClientID %>").val() == "ASC") {
                ('#lblDown').css("color", "");
                $('#lblUp').css("color", "green");
            }
            else {
                $('#lblUp').css("color", "");
                $('#lblDown').css("color", "green");
            }--%>
            $('#lblUp').live('click', function (event) {
                $("#<%=btnAsending.ClientID %>").click();
            });
            $('#lblDown').live('click', function (event) {
                $("#<%=btnDescending.ClientID %>").click();
            });
            $('#lblUP2').live('click', function (event) {
                $("#<%=btnPoLoadAse.ClientID %>").click();
            });
            $('#lblDown2').live('click', function (event) {
                $("#<%=btnPoLoaddes.ClientID %>").click();
            });
            $("select").chosen({ width: '100%' });
            $('#divDisc1').hide();
            $('#divDisc2').hide();
            $('#divDisc3').hide();
            $('#divMDown').hide();
            $('#divAdMDown').hide();
            $('#<%=txtDisc1Per.ClientID %>').attr("readonly", "readonly");
            $('#<%=txtDisc2Per.ClientID %>').attr("readonly", "readonly");
            $('#<%=txtDisc3Per.ClientID %>').attr("readonly", "readonly");
            $('#<%=txtMDownPer.ClientID %>').attr("readonly", "readonly");
            $('#<%=txtAddMDown.ClientID %>').attr("readonly", "readonly");
            fncDecimal();

            if ($('#<%=txtVendor.ClientID %>').val() != "") {
                $('#<%=txtVendor.ClientID %>').attr("readonly", "readonly");
                $('#<%=txtVendor.ClientID %>').attr("onfocus", "this.blur();");
            }



            pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");


            //fncvendordetailLoad();
            //============================  Assign Variable for Simply =========================//

            txtItemCodeAdd = $('#<%=txtItemCodeAdd.ClientID %>');
            txtItemNameAdd = $('#<%=txtItemNameAdd.ClientID %>');
            txtTotalDiscPrc = $('#<%=txtTotalDiscPrc.ClientID %>');
            txtTotalDiscRs = $('#<%=txtTotalDiscRs.ClientID %>');
            txtTotal = $('#<%=txtTotal.ClientID %>');
            txtSubTotal = $('#<%=txtSubTotal.ClientID %>');
            txtNetTotal = $('#<%=txtNetTotal.ClientID %>');
            txtGST = $('#<%=txtGST.ClientID %>');
            hdfTaxType = $('#<%=hdfTaxType.ClientID %>');

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });

            //------------------------------------------- Get Item Name ----------------------------------------//
            $("[id$=txtItemCodeAdd]").autocomplete({
                source: function (request, response) {

                    if ($('#<%=txtVendor.ClientID %>').val() == "") {
                        popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('Please Select Vendor Code');
                        $("[id$=txtItemCodeAdd]").val('');
                        //$("[id$=ddlVendorCode]").focus();
                        //$("[id$=ddlVendorCode]").trigger("liszt:open");
                        return false;
                    }

                    var obj = {};

                    obj.prefix = request.term.trim();
                    if ($('#<%=chkVendorItems.ClientID %>').is(':checked')) {
                        obj.vendorcode = $('#<%=txtVendor.ClientID %>').val();
                    }
                    else {
                        obj.vendorcode = "";
                    }
                    $.ajax({
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/GetFilterValue") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2],
                                    batch: item.split('/')[3]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });

                },

                focus: function (event, i) {
                    $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(i.item.val));
                    event.preventDefault();
                },

                //==========================> After Select Value
                select: function (e, i) {
                    $("[id$=hfInvCode]").val(i.item.val);
                    $("[id$=txtItemCodeAdd]").val(i.item.val);
                    $("[id$=txtItemNameAdd]").val(i.item.desc);
                    //$("[id$=txtItemNameAdd]").val(i.item.batch);


                    $('#<%=txtLQty.ClientID %>').focus().select();

                    return false;
                },

                minLength: 2
            });

            //============================> For Maintain Current Tab while postback
            $(function () {
                var tabName = $("[id*=hdfTabName]").val() != "" ? $("[id*=hdfTabName]").val() : "VendorWiseInv";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs a").click(function () {

                    $("[id*=hdfTabName]").val($(this).attr("href").replace("#", ""));

                    //============================> For Change Tab                    
                    var target = $("[id*=hdfTabName]").val();

                    if (target == "ViewAddInv") {
                        $("#<%=btnAddGrid.ClientID %>").click();
                    }

                });
            });


            $('#<%=txtMrp.ClientID%>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                if (key == '13') {
                    if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                        || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                        fncMRP34_GrossCost();
                    } else {
                        fncMRP1_NetCost();
                    }
                }
            });
            //------------------------------------------- L Qty Lost Focus -------------------------------------------------//

            $('#<%=txtLQty.ClientID %>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                try {
                    if (key == '13') {
                        var LQty = $('#<%=txtLQty.ClientID%>').val();
                        $('#<%=txtNQty.ClientID%>').val(LQty);

                        if ($(' #<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                            fncMarkup_Netcost_Calculation();
                        } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                            || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                            fncMRP34_GrossCost();
                        } else {
                            fncMRP1_NetCost();
                        }
                    }
                    //fncCalcNetAmount(); //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- N Qty Lost Focus -------------------------------------------------//

            $('#<%=txtNQty.ClientID %>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                try {
                    if (key == '13') {
                        var NQty = $('#<%=txtNQty.ClientID%>').val();
                        $('#<%=txtLQty.ClientID%>').val(NQty);
                        if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                            fncMarkup_Netcost_Calculation();
                        } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                            || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                            fncMRP34_GrossCost();
                        } else {
                            fncMRP1_NetCost();
                        }
                    }
                    // fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- Basic Cost Lost Focus -------------------------------------------------//

            $('#<%=txtBasicCost.ClientID %>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                try {
                    if (key == '13') {
                        var BasicCost = $('#<%=txtBasicCost.ClientID%>').val();
                        if (BasicCost != '') {
                        <%-- $('#<%=txtBasicCost.ClientID%>').val(parseFloat(BasicCost).toFixed(2));
                        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(BasicCost).toFixed(2));--%>
                            if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                                fncMarkup_Netcost_Calculation();
                            } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                                || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                                fncMRP34_GrossCost();
                            } else {
                                fncMRP1_NetCost();
                            }
                        }
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- Discount Prc Lost Focus -------------------------------------------------//

            $('#<%=txtDiscPrc.ClientID %>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                try {
                    if (key == '13') {
                        var DiscPrc = $('#<%=txtDiscPrc.ClientID%>').val();
                        if (DiscPrc == '') {
                            return false;
                        }
                   <%-- $('#<%=txtDiscPrc.ClientID%>').val(parseFloat(DiscPrc).toFixed(2));
                    var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                    var DiscAmt = ((GrossCost * DiscPrc) / (100));
                    $('#<%=txtDisAmt.ClientID%>').val(parseFloat(DiscAmt).toFixed(2));
                    var GrossCostFinal = parseFloat(GrossCost).toFixed(2) - parseFloat(DiscAmt).toFixed(2);
                    $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCostFinal).toFixed(2));--%>

                        if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                            fncMarkup_Netcost_Calculation();
                        } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                            || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                            fncMRP34_GrossCost();
                        } else {
                            fncMRP1_NetCost();
                        }
                    }
                    //fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- SD PRC Cost Lost Focus -------------------------------------------------//

            $('#<%=txtSDPrc.ClientID %>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                try {
                    if (key == '13') {
                        var SDPrc = $('#<%=txtSDPrc.ClientID%>').val();
                        if (SDPrc == '') {
                            return false;
                        }
                        $('#<%=txtSDPrc.ClientID%>').val(parseFloat(SDPrc).toFixed(2));
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var SDAmt = ((GrossCost * SDPrc) / (100));
                        $('#<%=txtSDAmt.ClientID%>').val(parseFloat(SDAmt).toFixed(2));
                        var GrossCostFinal = parseFloat(GrossCost).toFixed(2) - parseFloat(SDAmt).toFixed(2);
                        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCostFinal).toFixed(2));

                    <%-- if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else {
                        fncMRP1_NetCost();
                    }--%>
                        debugger;
                        fncCalcNetAmount();  //Calc Net Amount
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });


            function fncCalcNetAmount() {

                var NQty = $('#<%=txtNQty.ClientID%>').val();
                if (NQty != "" && NQty != "0") {
                    //===============> Net Amt Calc

                    if (hdfTaxType.val() == 'S') {
                        var txtSGST = $('#<%=txtSGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtSGST.ClientID%>').val();
                        var txtCGST = $('#<%=txtCGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtCGST.ClientID%>').val()
                        var TotalGstPerc = parseFloat(txtSGST) + parseFloat(txtCGST);
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var TotalGstAmt = ((GrossCost * TotalGstPerc) / (100));
                        var CessPer = $('#<%=txtCESS.ClientID %>').val();
                        var CessAmt = GrossCost * CessPer / 100;

                        NetAmt = parseFloat(GrossCost) + parseFloat(TotalGstAmt) + parseFloat(CessAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));
                        var SGST = parseFloat(TotalGstAmt) / 2;
                        $('#<%=txtSGSTAmt.ClientID%>').val(parseFloat(SGST).toFixed(2));
                        $('#<%=txtCGSTAmt.ClientID%>').val(parseFloat(SGST).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else if (hdfTaxType.val() == 'I') {

                        var txtIGST = $('#<%=txtIGST.ClientID%>').val();
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
                        }
                        var CessPer = $('#<%=txtCESS.ClientID %>').val();
                        var CessAmt = GrossCost * CessPer / 100;

                        NetAmt = parseFloat(GrossCost) + parseFloat(IGSTAmt) + parseFloat(CessAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else if (hdfTaxType.val() == 'Import') {
                        var txtIGST = $('#<%=txtIGST.ClientID%>').val();
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
                        }

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else {
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();

                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }

                }
            }



            //------------------------------------------ Total Discount Focus Out --------------------------------------------//
            txtTotalDiscPrc.keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                //txtTotalDiscPrc.val(txtTotalDiscPrc.val() == '' ? '0' : parseFloat(txtTotalDiscPrc.val()).toFixed(2));
                //var Total = txtTotal.val() == '' ? '0' : txtTotal.val();
                //var TotalDiscPrc = txtTotalDiscPrc.val() == '' ? '0' : txtTotalDiscPrc.val();
                //var TotalDiscRs = (Total * TotalDiscPrc) / 100;
                //txtTotalDiscRs.val(parseFloat(TotalDiscRs).toFixed(2));
                //var SubTotal = parseFloat(Total).toFixed(2) - parseFloat(TotalDiscRs).toFixed(2);
                //txtSubTotal.val(SubTotal);
                //var GST = txtGST.val() == '' ? '0' : txtGST.val();
                //var NetTotal = parseFloat(SubTotal).toFixed(2) + parseFloat(GST).toFixed(2);
                //txtNetTotal.val(parseFloat(NetTotal).toFixed(2));
                if (key == '13') {
                    if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                        || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                        fncMRP34_GrossCost();
                    } else {
                        fncMRP1_NetCost();
                    }
                }
            });

            ////Select Brandcode 

            $("[id$=txtBrand]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term.replace("'", "''");
                    obj.mode = "Brand";

                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtBrand.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtBrand.ClientID %>').val($.trim(i.item.valitemcode));
            $('#<%=btnVendorchange.ClientID %>').click();
                    return false;
                },
                minLength: 1
            });

        }

        $(document).ready(function () {
    <%-- $('#<%=txtSDPrc.ClientID %>').focusout(function () {
                try {
                    var SDPrc = $('#<%=txtSDPrc.ClientID%>').val();
                    if (SDPrc == '') {
                        return false;
                    }
                    <%--$('#<%=txtSDPrc.ClientID%>').val(parseFloat(SDPrc).toFixed(2));
                    var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                    var SDAmt = ((GrossCost * SDPrc) / (100));
                    $('#<%=txtSDAmt.ClientID%>').val(parseFloat(SDAmt).toFixed(2));
                    var GrossCostFinal = parseFloat(GrossCost).toFixed(2) - parseFloat(SDAmt).toFixed(2);
                    $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCostFinal).toFixed(2));--%>

    <%-- if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else {
                        fncMRP1_NetCost();
                    }
                    ///fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });--%>

            $('#lblUp').live('click', function (event) {
                $("#<%=btnAsending.ClientID %>").click();
    });
            $('#lblDown').live('click', function (event) {
                $("#<%=btnDescending.ClientID %>").click();
    });
            $('#lblUP2').live('click', function (event) {
                $("#<%=btnPoLoadAse.ClientID %>").click();
    });
            $('#lblDown2').live('click', function (event) {
                $("#<%=btnPoLoaddes.ClientID %>").click();
    });
        });
    </script>
    <%------------------------------------------- Add Inventory Button Click -------------------------------------------%>
    <script type="text/javascript">
        function AddbuttonClick() {
            var status = true;
            try {


                var MRP = parseFloat($('#<%=txtMrp.ClientID %>').val());
                var SP = parseFloat($('#<%=txtSellingPrice.ClientID %>').val());
                var BC = parseFloat($('#<%=txtBasicCost.ClientID %>').val());
                var Nc = parseFloat($('#<%=txtNet.ClientID %>').val());


                if (MRP < SP) {
                    fncToastInformation("MRP Should Greater than Sellingprice");
                    return false;
                }
                else if (SP < BC) {
                    fncToastInformation("Selling Should Greater than Basic");
                    return false;
                }
                else if (Nc > SP) {
                    fncToastInformation("Selling Should Greater than NetCost");
                    return false;
                }
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "ViewAddInv";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');

                $('#<%=lnkAddInv.ClientID%>').css("display", "none");

                if ($('#<%=txtItemCodeAdd.ClientID %>').val() == '') {
                    fncToastInformation("Please Enter ItemCode");
                    $('#<%=txtItemCodeAdd.ClientID %>').focus().select();
                    status = false;
                }
                else if ($('#<%=txtLQty.ClientID %>').val() == '' || $('#<%=txtLQty.ClientID %>').val() == '0') {
                    fncToastInformation("Please Enter Qty");
                    $('#<%=txtLQty.ClientID %>').focus().select();
                    status = false;
                }

                if (status == false) {
                    //fncClearEntervalue();
                    $('#<%=lnkAddInv.ClientID%>').css("display", "block");
                    return status;
                }


                var ItemCode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                $("#<%=gvSelectItemList.ClientID%> tbody").children().each(function () {

                    var gitem = $.trim($("td", $(this)).eq(2).text());
                    if ($.trim(gitem) == $.trim(ItemCode)) {
                        fncToastInformation("This Item Already added.");
                        fncClearEntervalue();
                        status = false;
                        return status;
                    }
                });

                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }
    </script>
    <script type="text/javascript">
        function showConfirm(ItemCode) {
            if (confirm("ItemCode " + ItemCode + " Already Exist, Do U want to Add?") == true) {
                return true;
            }
            return false;
        }
    </script>
    <%------------------------------------------- PO Pending Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenPoPending() {
            $("#dialog-POPending").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("destroy");
                    }
                }
            });

            fncChangeGridColor();
        }
    </script>
    <%------------------------------------------- Last FOC Detail Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenLastFocDetail() {
            $("#dialog-LastFocDetail").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Item History Detail Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenItemHistory() {
            $("#dialog-ItemHistory").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 600,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Clear Textbox After Add Inventory -------------------------------------------%>
    <script type="text/javascript">
        /// enable and Disable number key press base on UOM
        function fncDisableAndEnableNumKeyOnDiscontinueEntry() {
            try {
                fncClearAfterAddInv();
                fncChangeGridColor();
                fncChangeNumberFormartForQty();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncClearAfterAddInv() {
            fncClearEntervalue();
            $('#Tabs a[href="#ViewAddInv"]').tab('show');
            $("[id*=hdfTabName]").val("ViewAddInv");
            //fncChangeGridColor();
            fncSavePOEntryTemporarly();
        }

        function fncClearEntervalue() {
            try {
                $("div[id*=addInv]").find("input[type=text]").val("");
                $('#<%=txtWQty.ClientID %>').val("0");
                $('#<%=txtLQty.ClientID %>').val("0");
                $('#<%=txtMrp.ClientID %>').val("0");
                $('#<%=txtNQty.ClientID %>').val("0");
                $('#<%=txtMrp.ClientID %>').val("0");
                $('#<%=txtBasicCost.ClientID %>').val("0");
                $('#<%=txtDiscPrc.ClientID %>').val("0");
                $('#<%=txtDisAmt.ClientID %>').val("0");
                $('#<%=txtSDPrc.ClientID %>').val("0");
                $('#<%=txtSDAmt.ClientID %>').val("0");
                $('#<%=txtGrossCost.ClientID %>').val("0");
                $('#<%=txtNet.ClientID %>').val("0");
                $('#<%=txtNetTotalAdd.ClientID %>').val("0");
                $('#<%=txtFoc.ClientID %>').val("0");
                $('#<%=txtAddCess.ClientID %>').val("0");
                $('#<%=lnkAddInv.ClientID%>').css("display", "block");
                $('#<%=lblPurchaseType.ClientID%>').css("visibility", "hidden");
                $('#<%=lblBatch.ClientID%>').css("visibility", "hidden");
                $('#divDisc1').hide();
                $('#divDisc2').hide();
                $('#divDisc3').hide();
                $('#divMDown').hide();
                $('#divAdMDown').hide();
                //$('#<%=txtItemCodeAdd.ClientID %>').removeAttr("readonly");
                fncDisableAndEnableOnVendorChange(false);
                setTimeout(function () {
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    </script>
    <%------------------------------------------- Validation -------------------------------------------%>
    <script type="text/javascript">

        var glitemhistorybody, DisContinueEntry = "";

        function Validate() {

          <%--  if ($("#<%=hidSaveClick.ClientID %>").val() == "true")
                return false;--%>
            $("#<%=lnkSave.ClientID %>").css("display", "none");
            var Show = '';
            if (document.getElementById("<%=txtVendor.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Vendor Name';
                document.getElementById("<%=txtVendor.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlLocation.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Location Name';
                document.getElementById("<%=ddlLocation.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtPurchaseDate.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Purchase Date';
                document.getElementById("<%=txtPurchaseDate.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtDeliveryDate.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Delivery Date';
                document.getElementById("<%=txtDeliveryDate.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtSubTotal.ClientID%>").value <= 0) {
                Show = Show + '\n  Please Add Item To Proceed..!';
                document.getElementById("<%=txtBrand.ClientID %>").focus();
            }



            

            if (Show != '') {
                ShowPopupMessageBox(Show);
                $("#<%=lnkSave.ClientID %>").css("display", "block");
               <%--// $("#<%=hidSaveClick.ClientID %>").val("false");--%>
                return false;
            }
            else {

               
                return true;
                <%--$("#<%=hidSaveClick.ClientID %>").val("true");--%>






                //     $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                //    obj = $(this);
                //    venItemcode = $.trim($("td", obj).eq(1).text());
                //    if (obj.find('td input[id*=txtQty]').val() == "NaN") {
                //        obj.find('td input[id*=txtQty]').val("0");
                //    }

                //    GetQty = obj.find('td input[id*=txtQty]').val();
                //    if (GetQty == "") {
                //        GetQty = "0.00";
                //    }

                //    if (parseFloat(GetQty) != 0) {
                //        if (ItemCode == "") {
                //            ItemCode = $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val();
                //        }
                //        else {
                //            ItemCode = ItemCode + "|" + $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val();
                //        }
                //    }
                //    //Add Inventory Calculation
                //    Itemstatus = false;
                //    $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                //        addObj = $(this);
                //        addItemcode = $.trim($("td", addObj).eq(2).text());
                //        if (venItemcode == addItemcode) {
                //            addObj.find('td input[id*=txtQty]').val(GetQty);
                //            Itemstatus = true;                      
                //        }
                //    });                
                //}); 
                //  return true;
            }
        }

        /// Work start by saravanan
        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source, mode) {
            try {

                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var verHight;


                //if ((charCode > 95 && charCode < 106) || charCode == 37 || charCode == 38 || charCode == 39  || charCode == 190) {
                //    return true;
                //    evt.preventDefault();
                //}
                //else if ((charCode != 46 && charCode > 31) && (charCode < 48 || charCode > 57) && charCode != 110) {
                //    return false;
                //    evt.preventDefault();
                //}                 


                if (charCode == 112) {
                    if (mode == "venItem")
                        fncOpenItemhistory($.trim($("td", rowobj).eq(1).text()));
                    else if (mode == "AddItem")
                        fncOpenItemhistory($.trim($("td", rowobj).eq(2).text()));
                }
                else if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        if (mode == "venItem" || mode == "WQty") {
                            verHight = $("#gvItemLoad").scrollTop();
                            $("#gvItemLoad").scrollTop(verHight + 14);
                        }
                        else {
                            verHight = $("#gvAddItem").scrollTop();
                            $("#gvAddItem").scrollTop(verHight + 14);
                        }

                        if (mode == "WQty") {
                            fncSetFocustoObject(NextRowobj.find('td input[id*="txtWQty"]'));
                        }
                        else {
                            fncSetFocustoObject(NextRowobj.find('td input[id*="txtQty"]'));
                        }
                    }
                    else {
                        if (charCode == 13 || charCode == 40) {

                            if (mode == "AddItem") {
                                if ($('#ContentPlaceHolder1_gvSelectItemList tbody').children().length == 1) {
                                    $('#<%=lnkSave.ClientID %>').focus();
                                }
                                else {
                                    fncSetFocustoObject(rowobj.siblings().first().find('td input[id*="txtQty"]'));
                                }
                            }
                            else if (mode == "venItem") {
                                if ($('#ContentPlaceHolder1_gvVendorLoadItemList tbody').children().length == 1) {

                                    $('#<%=lnkSave.ClientID %>').focus();
                                }
                                else {
                                    fncSetFocustoObject(rowobj.siblings().first().find('td input[id*="txtQty"]'));
                                }
                            }
                        }

                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {

                        if (mode == "venItem" || mode == "WQty") {
                            verHight = $("#gvItemLoad").scrollTop();
                            $("#gvItemLoad").scrollTop(verHight - 10);
                        }
                        else if ("AddItem") {
                            verHight = $("#gvAddItem").scrollTop();
                            $("#gvAddItem").scrollTop(verHight - 10);
                        }

                        if (mode == "WQty") {
                            fncSetFocustoObject(prevrowobj.find('td input[id*="txtWQty"]'));
                        }
                        else {
                            fncSetFocustoObject(prevrowobj.find('td input[id*="txtQty"]'));
                        }
                    }
                }

                if (charCode == 13) {
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Open Item History
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 630,
                width: 1200,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        /// Show and Hide Sales history
        /// 16-02-2018
        function fncShowandHideSaleshistory() {
            try {
                //fncColorChangeOnSelectionForFOCPOSalesHis("salesHistory");
                if ($('#divItemhistory').css('display') == 'none') {
                    $('#divItemhistory').css('display', 'block');
                    $('#addInv').css('display', 'none');
                    $('#<%=lnkSaleshistory.ClientID%>').css("background-color", "#f44336");
                }
                else {
                    $('#divItemhistory').css('display', 'none');
                    $('#<%=lnkSaleshistory.ClientID%>').css("background-color", "blueviolet");
                    fncEnableAddInv();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncOpenLastFOCDet() {
            try {
                //fncColorChangeOnSelectionForFOCPOSalesHis("FOC");
                if ($('#divLastFoc').css('display') == 'none') {
                    $('#divLastFoc').css('display', 'block');
                    $('#addInv').css('display', 'none');
                    $('#<%=lnkLastFocDetail.ClientID%>').css("background-color", "#f44336");
                }
                else {
                    $('#divLastFoc').css('display', 'none');
                    $('#<%=lnkLastFocDetail.ClientID%>').css("background-color", "blueviolet");
                    fncEnableAddInv();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncEnableAddInv() {
            try {
                if ($('#divItemhistory').css('display') == 'none' && $('#divLastFoc').css('display') == 'none' && $('#divPoPending').css('display') == 'none') {
                    $('#addInv').css('display', 'block');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //Data Initialize
        $(document).ready(function () {
            glitemhistorybody = $("#tblItemhistory tbody");
    <%--  $('#<%=txtMrp.ClientID%>').keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($('#<%=txtBasicCost.ClientID%>').is(':disabled')) {
                $('#<%=txtSDPrc.ClientID%>').select();
            }
            else {
                $('#<%=txtBasicCost.ClientID%>').select();
            }
        }
         return false;
    }); --%>
            $('#<%=txtMrp.ClientID%>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                if (key == '13') {
                    if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                        || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                        fncMRP34_GrossCost();
                    } else {
                        fncMRP1_NetCost();
                    }
                }
            });

            $('#<%=txtBasicCost.ClientID%>').keydown(function (event) {
                var key = (event.keyCode ? event.keyCode : event.which);
                if (key == '13') {
                    if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                        fncMarkup_Netcost_Calculation();
                    } else if ($('#<%=lblPurchaseType.ClientID%>').text() == "Partial MarkDown(G.C)"
                        || $('#<%=lblPurchaseType.ClientID%>').text() == "Full MarkDown(G.C)") {
                        fncMRP34_GrossCost();
                    } else {
                        fncMRP1_NetCost();
                    }
                }
            });
        });



        /// Get Sales history
        /// work done by saravnan 16-02-2018
        function fncGetSalesHistory(source) {
            var itemcode, desc, verHight;
            try {

                //if ($('#divItemhistory').css('display') == 'none')
                //    return;

                fncVenItemRowClk((source).closest("tr"));
                rowObj = $(source).closest("tr");

                itemcode = $("td", rowObj).eq(1).text().replace(/&nbsp;/g, '');
                desc = $("td", rowObj).eq(2).text().replace(/&nbsp;/g, '');
                fncBindSalesHistory(itemcode, desc, "");
                rowObj.find('td input[id*=txtQty]').select();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Bind Sales history to table
        function fncBindSalesHistory(itemcode, desc, month) {
            var obj = {};
            try {
                $('#<%=code.ClientID %>').text(itemcode);
                $('#<%=txtDesc.ClientID %>').val(desc);

                $('#<%=loc_Code.ClientID %>').text(itemcode);
                $('#<%=txtlocName.ClientID %>').val(desc);

                $('#<%=lblFOcItem.ClientID %>').text(itemcode);
                $('#<%=txtFocItem.ClientID %>').val(desc);

                $('#<%=lblPOpendingItem.ClientID %>').text(itemcode);
                $('#<%=txtPOPenCode.ClientID %>').val(desc);

                obj.itemcode = itemcode;
                obj.location = $('#<%=ddlLocation.ClientID %>').val();
                obj.month = month//$('#<%=ddlLocation.ClientID %>').val();
                obj.vendorcode = $('#<%=hidVendor.ClientID %>').val();


                $.ajax({
                    type: "POST",
                    url: "frmPurchaseOrder.aspx/fncGetSalesHistory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {



                        if (month == "") {
                            glitemhistorybody.children().remove();
                            var objItemhistory = jQuery.parseJSON(msg.d);
                            //console.log(objItemhistory);                         
                            if (objItemhistory.Table.length > 0) {
                                for (var i = 0; i < objItemhistory.Table.length; i++) {
                                    glitemhistorybody.append("<tr>"
                                        + "<td>" + objItemhistory.Table[i]["RowNo"] + "</td>"
                                        + "<td style='font-size: 11px;' id='tdMNName_" + i + "' >" + objItemhistory.Table[i]["MNName"] + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK1"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK2"].toFixed(2) + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK3"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK4"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK5"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK6"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Total"].toFixed(2) + "</td>"
                                        + "</tr>");
                                }
                                glitemhistorybody.children().dblclick(fncGetLocationwiseSaleshistory);

                                if ($('#divItemhistory').css('display') == 'block') {
                                    $('#locWiseSaleshis').css('display', 'none');
                                    //$('#divItemhistory').css('display', 'block');
                                }


                            }
                        }
                        else {

                            var tblLocSalesHis;
                            tblLocSalesHis = $('#tblLocwiseSaleshistory tbody');
                            tblLocSalesHis.children().remove();
                            var objItemhistory = jQuery.parseJSON(msg.d);

                            if (objItemhistory.Table.length > 0) {
                                for (var i = 0; i < objItemhistory.Table.length; i++) {
                                    tblLocSalesHis.append("<tr>"
                                        + "<td>" + objItemhistory.Table[i]["RowNo"] + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Location"] + "</td>"
                                        + "<td id='tdMNName_" + i + "' >" + objItemhistory.Table[i]["MNName"] + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK1"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK2"].toFixed(2) + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK3"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK4"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK5"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK6"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Total"].toFixed(2) + "</td>"
                                        + "</tr>");
                                }
                                $('#divItemhistory').css('display', 'none');
                                $('#locWiseSaleshis').css('display', 'block');
                            }
                        }

                        if (objItemhistory.Table1.length > 0) {
                            fncShowItemwisePOPendings(objItemhistory.Table1);

                        }
                        else {
                            $('#divPoPending').css('display', 'none');
                            fncEnableItemPanel();
                            //fncCloseLocationSaleshistory();
                        }

                        if (objItemhistory.Table2.length > 0) {
                            fncShowLastFOCDetail(objItemhistory.Table2);
                        }
                        else {
                            $('#tblLastFOC tbody').children().remove();
                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        // Show Last Purchase Detail
        function fncShowItemwisePOPendings(objPOPending) {
            var potbodyItemwisePending;
            try {
                potbodyItemwisePending = $('#tblPOItemwisePending tbody');
                potbodyItemwisePending.children().remove();
                if (objPOPending.length > 0) {
                    for (var i = 0; i < objPOPending.length; i++) {
                        potbodyItemwisePending.append("<tr>"
                            + "<td>" + objPOPending[i]["RowNo"] + "</td>"
                            + "<td>" + objPOPending[i]["PoNo"] + "</td>"
                            + "<td>" + objPOPending[i]["PoDate"] + "</td>"
                            + "<td >" + objPOPending[i]["ItemCode"] + "</td>"
                            + "<td>" + objPOPending[i]["LQty"] + "</td>"
                            + "<td >" + objPOPending[i]["LReceiptQty"] + "</td>"
                            + "</tr>");
                    }
                    $('#addInv').css('display', 'none');
                    $('#divPoPending').css('display', 'block');
                }
                else {
                    $('#divPoPending').css('display', 'none');
                    fncEnableAddInv();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //// Show Last Foc Detail
        function fncShowLastFOCDetail(objLastFOCDet) {
            var lastFOCDet;
            try {
                lastFOCDet = $('#tblLastFOC tbody');
                lastFOCDet.children().remove();
                if (objLastFOCDet.length > 0) {
                    for (var i = 0; i < objLastFOCDet.length; i++) {
                        lastFOCDet.append("<tr>"
                            + "<td>" + objLastFOCDet[i]["RowNo"] + "</td>"
                            + "<td>" + objLastFOCDet[i]["ItemCode"] + "</td>"
                            + "<td>" + objLastFOCDet[i]["Lqty"] + "</td>"
                            + "<td>" + objLastFOCDet[i]["FOCItemCode"] + "</td>"
                            + "<td >" + objLastFOCDet[i]["FOCQty"] + "</td>"
                            + "<td>" + objLastFOCDet[i]["GIDNo"] + "</td>"
                            + "<td >" + objLastFOCDet[i]["GIDDate"] + "</td>"
                            + "</tr>");
                    }
                    $('#addInv').css('display', 'none');
                    $('#divLastFoc').css('display', 'block');
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetLocationwiseSaleshistory() {
            try {
                fncBindSalesHistory($('#<%=code.ClientID %>').text(), $('#<%=txtDesc.ClientID %>').val(), $(this).find('td[id*=tdMNName]').text());
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //=================> After Common Search
        function fncSetValue() {
            try {

                if (SearchTableName == "Vendor") {
                    $('#<%=hidVendor.ClientID %>').val(Code);
                    $('#<%=txtVendor.ClientID %>').val(Code);
                    $('#<%=txtVendorDetails.ClientID %>').val(VendorAddress);
                    fncCheckPODiscontinueEntry();
                    fncDisableAndEnableOnVendorChange(false);
                }

                if (SearchTableName == "Inventory") {
                    fncGetInventoryDetail();
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //=================> Repeater Column Value Change Process
        function fncRepeaterColumnValueChange(value) {//Vijay Change
            try {
                var Code = [];
                var TotalQty = 0, GetQty = 0, ItemsCount = 0, TotalCost = 0, ItemCode = "", GST = 0;
                var taxAmt1 = 0, taxAmt2 = 0, cessAmt = 0, totalgrosscost = 0;
                var ITaxPerc1 = 0, ITaxPerc2 = 0, ITaxPerc3 = 0, ITaxPerc4 = 0;
                var poTemporarlyData = "", venItemcode = "", addItemcode = "", Itemstatus = false;
                var obj, jsonObj = "", addObj, GetQty1;
                //var itemArry = {};
                if (value == "NewQty") {
                    var xml = '<NewDataSet>';
                    $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                        obj = $(this);

                        venItemcode = $.trim($("td", obj).eq(1).text());
                        GetQty = obj.find('td input[id*=txtQty]').val();

                        if (parseFloat(GetQty) == 0) {
                            var code = $.trim($("td", obj).eq(1).text());

                            xml += "<Table>";
                            xml += "<InventoryCode>" + code + "</InventoryCode>";
                            xml += "<Qty>" + obj.find('td input[id*=txtQty]').val() + "</Qty>";
                            xml += "</Table>";
                        }

                        if (parseFloat(obj.find('td input[id*=txtQty]').val()) > 0) {
                            if (obj.find('td input[id*=txtQty]').val() == "NaN") {
                                obj.find('td input[id*=txtQty]').val("0");
                            }

                            GetQty = obj.find('td input[id*=txtQty]').val();
                            if (GetQty == "") {
                                GetQty = "0.00";
                            }

                            if (parseFloat(GetQty) != 0) {
                                if (ItemCode == "") {
                                    ItemCode = $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val();
                                }
                                else {
                                    ItemCode = ItemCode + "|" + $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val();
                                }
                            }


                            //Add Inventory Calculation
                            Itemstatus = false;
                            $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                                addObj = $(this);
                                addItemcode = $.trim($("td", addObj).eq(2).text());
                                if (venItemcode == addItemcode) {
                                    addObj.find('td input[id*=txtQty]').val(GetQty);
                                    Itemstatus = true;
                                    //itemArry.push(venItemcode);
                                }
                            });

                            if (Itemstatus == false) {

                                if (parseFloat(GetQty) != 0) {
                                    TotalQty = parseFloat(TotalQty) + parseFloat(GetQty);
                                    ItemsCount = ItemsCount + 1;

                                    //Total Cost
                                    var Cost = $.trim($("td", obj).eq(10).text());
                                    TotalCost = parseFloat(TotalCost) + parseFloat(GetQty * Cost)

                                    ITaxPerc1 = fncConvertFloatJS($.trim($("td", obj).eq(17).text()));
                                    ITaxPerc2 = fncConvertFloatJS($.trim($("td", obj).eq(18).text()));
                                    ITaxPerc3 = fncConvertFloatJS($.trim($("td", obj).eq(19).text()));
                                    ITaxPerc4 = fncConvertFloatJS($.trim($("td", obj).eq(25).text()));

                                    if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                                        GST = GST + ((GetQty * Cost * ITaxPerc1) / 100);
                                        GST = GST + ((GetQty * Cost * ITaxPerc2) / 100);
                                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                                    }
                                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                                        GST = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                                    }
                                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                                        GST = GST + ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                                    }

                                    ////// To Save PO Temporarly value get from Vendor Grid
                                    if (poTemporarlyData == "") {
                                        poTemporarlyData = $.trim($("td", obj).eq(1).text()) + ";";//itemcode
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(2).text()) + ";";//itemdesc
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//Size
                                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Foc
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//MRP
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Selling
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//Cost
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//DisAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";////O.DisPer
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//O.DisAmt
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//Gross
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//SGST
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//CGST
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//IGST
                                        poTemporarlyData = poTemporarlyData + $("td", obj).eq(20).text() + ";";//Net
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Total
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//SGSTAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//CGSTAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//IGSTAmt
                                        poTemporarlyData = poTemporarlyData + "" + ";";//Checked
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Po Qty
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//CESSPer
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//CessAmt
                                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//UOM
                                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtWQty]').val(); //WQty
                                    }
                                    else {
                                        poTemporarlyData = poTemporarlyData + "|" + $.trim($("td", obj).eq(1).text()) + ";";// itemcode                            
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(2).text()) + ";";//itemdesc
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//Size
                                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Foc
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//MRP
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Selling
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//Cost
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//DisAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";////O.DisPer
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//O.DisAmt
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//Gross
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//SGST
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//CGST
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//IGST
                                        poTemporarlyData = poTemporarlyData + $("td", obj).eq(20).text() + ";";//Net
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Total
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//SGSTAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//CGSTAmt
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//IGSTAmt
                                        poTemporarlyData = poTemporarlyData + "" + ";";//Checked
                                        poTemporarlyData = poTemporarlyData + "0" + ";";//Po Qty
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//CESSPer
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//CessAmt
                                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//UOM
                                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtWQty]').val(); //WQty
                                    }
                                }
                            }
                        }
                    });

                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    $("#<%=hidPo.ClientID%>").val(xml);
                    $('#<%=hdfItemCode.ClientID%>').val(ItemCode); // Get ItemCode  

                    ///CalCulate Added Total ValueContentPlaceHolder1_gvSelectItemList
                    $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                        obj = $(this);
                        GetQty = $.trim(obj.find('td input[id*=txtQty]').val());


                        if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {

                            if (ItemCode == "") {
                                ItemCode = $.trim($("td", obj).eq(2).text()) + ";" + GetQty + ";" + $.trim($("td", obj).eq(27).text());
                            }
                            else {
                                ItemCode = ItemCode + "|" + $.trim($("td", obj).eq(2).text()) + ";" + GetQty + ";" + $.trim($("td", obj).eq(27).text());
                            }

                            TotalQty = parseFloat(TotalQty) + parseFloat(obj.find('td input[id*=txtQty]').val());
                            ItemsCount = ItemsCount + 1;

                            //Total Cost
                            var Cost = $.trim($("td", obj).eq(13).text());
                            TotalCost = parseFloat(TotalCost) + parseFloat(GetQty * Cost)

                            ITaxPerc1 = fncConvertFloatJS($.trim($("td", obj).eq(14).text()));
                            ITaxPerc2 = fncConvertFloatJS($.trim($("td", obj).eq(15).text()));
                            ITaxPerc3 = fncConvertFloatJS($.trim($("td", obj).eq(16).text()));
                            ITaxPerc4 = fncConvertFloatJS($.trim($("td", obj).eq(24).text()));

                            if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                        //taxAmt1 =  ((GetQty * Cost * ITaxPerc1) / 100);
                        //taxAmt2 = ((GetQty * Cost * ITaxPerc1) / 100);
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc1) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc2) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }
                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                        //taxAmt1 = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }
                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                        //taxAmt1 = ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }



                    ////// To Save PO Temporarly 
                    if (poTemporarlyData == "") {
                        poTemporarlyData = $.trim($("td", obj).eq(2).text()) + ";";//itemcode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//itemdesc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(4).text()) + ";";//Size
                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(6).text()) + ";";//Foc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(7).text()) + ";";//MRP
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//Selling
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Cost
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(11).text()) + ";";////O.DisPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(12).text()) + ";";//O.DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(13).text()) + ";";//Gross
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(14).text()) + ";";//SGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(15).text()) + ";";//CGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//IGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//Net
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//Total
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//SGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(20).text()) + ";";//CGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(21).text()) + ";";//IGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Checked
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Po Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(23).text()) + ";";//CESSPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(24).text()) + ";";//CessAmt
                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//UOM
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) //WQty
                    }
                    else {
                        poTemporarlyData = poTemporarlyData + "|" + $.trim($("td", obj).eq(2).text()) + ";";// itemcode
                        ///poTemporarlyData = $.trim($("td", obj).eq(2).text()) + ";";//itemcode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//itemdesc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(4).text()) + ";";//Size
                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(6).text()) + ";";//Foc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(7).text()) + ";";//MRP
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//Selling
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Cost
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(11).text()) + ";";////O.DisPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(12).text()) + ";";//O.DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(13).text()) + ";";//Gross
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(14).text()) + ";";//SGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(15).text()) + ";";//CGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//IGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//Net
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//Total
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//SGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(20).text()) + ";";//CGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(21).text()) + ";";//IGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Checked
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Po Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(23).text()) + ";";//CESSPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(24).text()) + ";";//CessAmt
                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//UOM
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) //WQty
                    }

                    /// PO Temporarly Save End
                }

            });
                    ///End

                    fncNetValuesCal(TotalQty, ItemsCount, TotalCost, GST);
        // fncSavePOEntryTemporarly(poTemporarlyData);
       <%-- // $("#<%=btnAddGrid.ClientID %>").click();--%>
                }

                else if (value == "AddQty") {

                    $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                        obj = $(this);
                        GetQty = $.trim(obj.find('td input[id*=txtQty]').val());
                        if (GetQty == "") {
                            GetQty = "0.00";
                        }
                        //if (GetQty == "0.00") {
                        //    $(this).closest('tr').remove();
                        //}
                        if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {

                            if (ItemCode == "") {
                                ItemCode = $.trim($("td", obj).eq(2).text()) + ";" + GetQty + ";" + $.trim($("td", obj).eq(27).text());
                            }
                            else {
                                ItemCode = ItemCode + "|" + $.trim($("td", obj).eq(2).text()) + ";" + GetQty + ";" + $.trim($("td", obj).eq(27).text());
                            }

                            TotalQty = parseFloat(TotalQty) + parseFloat(obj.find('td input[id*=txtQty]').val());
                            ItemsCount = ItemsCount + 1;

                            //Total Cost
                            var Cost = $.trim($("td", obj).eq(9).text());
                            TotalCost = parseFloat(TotalCost) + parseFloat(GetQty * Cost)

                            ITaxPerc1 = fncConvertFloatJS($.trim($("td", obj).eq(14).text()));
                            ITaxPerc2 = fncConvertFloatJS($.trim($("td", obj).eq(15).text()));
                            ITaxPerc3 = fncConvertFloatJS($.trim($("td", obj).eq(16).text()));
                            ITaxPerc4 = fncConvertFloatJS($.trim($("td", obj).eq(24).text()));

                            if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                        //taxAmt1 =  ((GetQty * Cost * ITaxPerc1) / 100);
                        //taxAmt2 = ((GetQty * Cost * ITaxPerc1) / 100);
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc1) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc2) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }
                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                        //taxAmt1 = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }
                    else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                        //taxAmt1 = ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                        //cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                        GST = GST + ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                        GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                    }



                    ////// To Save PO Temporarly 
                    if (poTemporarlyData == "") {
                        poTemporarlyData = $.trim($("td", obj).eq(2).text()) + ";";//itemcode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//itemdesc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(4).text()) + ";";//Size
                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(6).text()) + ";";//Foc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(7).text()) + ";";//MRP
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//Selling
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Cost
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(11).text()) + ";";////O.DisPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(12).text()) + ";";//O.DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(13).text()) + ";";//Gross
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(14).text()) + ";";//SGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(15).text()) + ";";//CGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//IGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//Net
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//Total
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//SGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(20).text()) + ";";//CGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(21).text()) + ";";//IGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Checked
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(23).text()) + ";";//Po Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(24).text()) + ";";//CESSPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//CessAmt
                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//UOM
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(27).text()) //WQty
                    }
                    else {
                        poTemporarlyData = poTemporarlyData + "|" + $.trim($("td", obj).eq(2).text()) + ";";// itemcode
                        poTemporarlyData = $.trim($("td", obj).eq(2).text()) + ";";//itemcode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(3).text()) + ";";//itemdesc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(4).text()) + ";";//Size
                        poTemporarlyData = poTemporarlyData + obj.find('td input[id*=txtQty]').val() + ";";//Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(6).text()) + ";";//Foc
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(7).text()) + ";";//MRP
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(8).text()) + ";";//Selling
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(9).text()) + ";";//Cost
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(10).text()) + ";";//DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(11).text()) + ";";////O.DisPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(12).text()) + ";";//O.DisAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(13).text()) + ";";//Gross
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(14).text()) + ";";//SGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(15).text()) + ";";//CGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(16).text()) + ";";//IGST
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(17).text()) + ";";//Net
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(18).text()) + ";";//Total
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(19).text()) + ";";//SGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(20).text()) + ";";//CGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(21).text()) + ";";//IGSTAmt
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(22).text()) + ";";//Checked
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(23).text()) + ";";//Po Qty
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(24).text()) + ";";//CESSPer
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(25).text()) + ";";//CessAmt
                        poTemporarlyData = poTemporarlyData + "I" + ";";//Mode
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(26).text()) + ";";//UOM
                        poTemporarlyData = poTemporarlyData + $.trim($("td", obj).eq(27).text()) //WQty
                    }
                    /// PO Temporarly Save End
                }
                else {
                    __doPostBack('ctl00$ContentPlaceHolder1$gvSelectItemList', 'Delete$1')
                }

            });

                    $('#<%=hdfAddItemCode.ClientID%>').val(ItemCode); // Get ItemCode
                    fncNetValuesCal(TotalQty, ItemsCount, TotalCost, GST);


                    fncSavePOEntryTemporarly(poTemporarlyData);



                }

                //fncCreateVatDetailTable(TotalQty, taxAmt1, taxAmt2, cessAmt, ITaxPerc3, ITaxPerc4, TotalCost);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// Net Value Calculation
        function fncNetValuesCal(TotalQty, ItemsCount, TotalCost, GST) {
            var SubTotal = 0, DiscAmt = 0;
            try {
                //============> Set Value                
                $('#<%=txtQty.ClientID%>').val(TotalQty); // Total Qty
                $('#<%=txtItems.ClientID%>').val(ItemsCount); // Total Item
                $('#<%=txtTaxableAmt.ClientID%>').val(TotalCost); // Total Cost                       
                $('#<%=txtGST.ClientID%>').val(GST); // GST  

                if ($('#<%=hdfItemCode.ClientID%>').val() == "Import" || $('#<%=hdfItemCode.ClientID%>').val() == "NoGst") {
                    $('#<%=txtSubTotal.ClientID%>').val(TotalCost); // Sub Total Cost                         
        }
        else {
            var NetCost = parseFloat(TotalCost) + parseFloat(GST);
            $('#<%=txtSubTotal.ClientID%>').val(NetCost); // Sub Total Cost                        
                }

                SubTotal = $('#<%=txtSubTotal.ClientID%>').val();
                DiscAmt = $('#<%=txtDiscountAmt.ClientID%>').val();

                $('#<%=txtNetTotal.ClientID%>').val(parseFloat(SubTotal) - parseFloat(DiscAmt));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDiscountAmtCal(value) {
            var taxableAmt = 0, discPerc = 0, discAmt = 0;
            var netAmt = 0;

            try {
                taxableAmt = $('#<%=txtTaxableAmt.ClientID%>').val();
                discPerc = $('#<%=txtDiscountPerc.ClientID%>').val();
                discAmt = $('#<%=txtDiscountAmt.ClientID%>').val();
                if (value == "DisPrc") {
                    discAmt = (taxableAmt * discPerc) / 100;
                }
                else {
                    discPerc = (discAmt * 100) / taxableAmt;
                }

                netAmt = parseFloat(taxableAmt) - parseFloat(discAmt);

                $('#<%=txtDiscountAmt.ClientID%>').val(discAmt);
                $('#<%=txtDiscountPerc.ClientID%>').val(discPerc);
                $('#<%=txtNetTotal.ClientID%>').val(netAmt);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// fnc Save PO Entry
        function fncSavePOEntryTemporarly() {
            var obj = {};
            try {
                obj.vendorcode = $('#<%=hidVendor.ClientID%>').val();
                if ($('#<%=chkVendorItems.ClientID%>').is(':checked')) {
                    obj.value = "1";
                }
                else {
                    obj.value = "0";
                }
                obj.xmlData = fncGetGridValuesForSave();
                $.ajax({
                    type: "POST",
                    url: "frmPurchaseOrder.aspx/fncSavePOEntryTemporarily",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        return false;
                    },
                    error: function (data) {
                        fncToastError(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function disableFunctionKeys(e) {
            try {

                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 114) {
                        if ($("#<%=hdfTabName.ClientID %>").val() == "" || $("#<%=hdfTabName.ClientID %>").val() == "VendorWiseInv") {
                            $('#Tabs a[href="#ViewAddInv"]').tab('show');
                            $('#divVendorItem').hide();
                            $('#divSelectedItem').show();
                            fncRepeaterColumnValueChange('NewQty');
                            //setTimeout(function () {
                            $("#<%=btnAddGrid.ClientID %>").click();
                            //}, 100);
                            $("[id*=hdfTabName]").val("ViewAddInv");
                            if ($('#<%=txtVendor.ClientID %>').val() != "") {
                                $('#<%=txtVendor.ClientID %>').attr("readonly", "readonly");
                            }
                            //fncRepeaterColumnValueChange('NewQty');
                            // fncSavePOEntryTemporarly();
                        }
                        else {
                            $('#<%=hidSave.ClientID %>').val("Show");
                            $('#divVendorItem').show();
                            $('#divSelectedItem').hide();
                            $("select").chosen({ width: '100%' });
                            $('#Tabs a[href="#VendorWiseInv"]').tab('show');
                            $("[id*=hdfTabName]").val("VendorWiseInv");
                            fncSetGridQty();    //vijay click
                            if ($('#<%=txtVendor.ClientID %>').val() != "") {
                                $('#<%=txtVendor.ClientID %>').attr("readonly", "readonly");
                            }
                        }
                        e.preventDefault();
                        fncDisableSaleshistory();
                        $('#addInv').css('display', 'block');


                    }
                    else if (e.keyCode == 115) {
                        if (Validate() == true) {
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        }
                    }
                    else if (e.keyCode == 117) {
                        window.location.href = "../Purchase/frmPurchaseOrder.aspx";
                    }
                    else if (e.keyCode == 119) {
                        window.location.href = "../Purchase/frmPurchaseOrderView.aspx";
                    }
                    else if (e.keyCode == 121) {
                        fncBarcodeQtyValidation();
                    }

                    e.preventDefault();
                }

                if (e.keyCode == 27) {
                    fncCloseLocationSaleshistory();
                }
                if (e.keyCode == 123) {
                    if ($('#<%=chkVendorItems.ClientID%>').is(':checked')) {
                $("#<%=chkVendorItems.ClientID%>").removeAttr("checked");
            }
            else {
                $("#<%=chkVendorItems.ClientID%>").attr("checked", "checked");
            }
            $("#<%=txtItemCodeAdd.ClientID%>").select();

                    e.preventDefault();
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncCloseLocationSaleshistory() {
            try {

                if ($('#locWiseSaleshis').css('display') == 'block') {
                    $('#locWiseSaleshis').css('display', 'none');
                    $('#divItemhistory').css('display', 'block');
                }

                if ($('#divPoPending').css('display') == 'block') {
                    $('#divPoPending').css('display', 'none');
                    if ($('#divItemhistory').css('display') == 'none') {
                        $('#addInv').css('display', 'block');
                    }
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncEnableItemPanel() {
            try {
                if ($('#divItemhistory').css('display') == 'none' && $('#locWiseSaleshis').css('display') == 'none' &&
                    $('#divLastFoc').css('display') == 'none' && $('#divPoPending').css('display') == 'none') {
                    $('#addInv').css('display', 'block');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        ///Barcode Validation
        function fncBarcodeQtyValidation() {
            try {
                var status = "";
                if ($("#ContentPlaceHolder1_gvSelectItemList tbody").children().length == 1) {
                    if ($("#ContentPlaceHolder1_gvSelectItemList_Label3").text() == "No Records Found") {
                        return false;
                    }
                }

                if ($("#ContentPlaceHolder1_gvSelectItemList tbody").children().length > 0) {
                    ShowPopupBarcode();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        ///Open Barcode Popup
        function ShowPopupBarcode() {

            dateFormat[0].selectedIndex = 1;
            dateFormat.trigger("liszt:updated");

            $("#divBarcode").dialog({
                appendTo: 'form:first',
                title: "Barcode Print",
                width: 500,
                modal: true

            });
            setTimeout(function () {
                expiredDate.select().focus();
            }, 50);
        }

        //Get Barcode User Comtrol
        $(function () {
            try {

                pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
                expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
                dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
                hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
                hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
                hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
                hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
                hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
                template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
                size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
                printName = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.printName).ClientID%>");
                print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");
            }
            catch (err) {
                fncToastError(err.message);
            }

        });

        function fncShowBarcodeSuccessMessage() {
            try {
                fncDisableSaleshistory();
                fncToastInformation('<%=Resources.LabelCaption.Save_Barcodeprinting%>');
            }
            catch (err) {
                fncToastInformation();
            }
        }

        function fncTemplateChange() {
            try {

                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Barcode Validation
        function fncBarcodeValidation() {
            try {
                if (size.val() == "") {
                    fncToastInformation('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    return false;
                }
                else if ($('#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName').val() == '' ||
                    $('#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName').val() == '-- Select --') {
                    ShowPopupMessageBox("Please Choose Printer Name");
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtTaxableAmt.ClientID%>').number(true, 2);
                $('#<%=txtGST.ClientID%>').number(true, 2);
                $('#<%=txtSubTotal.ClientID%>').number(true, 2);
                $('#<%=txtDiscountPerc.ClientID%>').number(true, 2);
                $('#<%=txtDiscountAmt.ClientID%>').number(true, 2);
                $('#<%=txtNetTotal.ClientID%>').number(true, 2);
                $('#<%=txtMrp.ClientID%>').number(true, 2);
                $('#<%=txtBasicCost.ClientID%>').number(true, 2);
                $('#<%=txtSDPrc.ClientID%>').number(true, 2);
                $('#<%=txtDisAmt.ClientID%>').number(true, 2);
                $('#<%=txtSDPrc.ClientID%>').number(true, 2);
                $('#<%=txtSDAmt.ClientID%>').number(true, 2);
                $('#<%=txtGrossCost.ClientID%>').number(true, 2);
                $('#<%=txtNet.ClientID%>').number(true, 2);
                $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
                $('#<%=txtNetTotalAdd.ClientID%>').number(true, 2);
                $('#<%=txtSGST.ClientID%>').number(true, 2);
                $('#<%=txtCGST.ClientID%>').number(true, 2);
                $('#<%=txtIGST.ClientID%>').number(true, 2);
                $('#<%=txtCESS.ClientID%>').number(true, 2);
                $('#<%=txtAddCess.ClientID%>').number(true, 2);
                $('#<%=txtDisc1Per.ClientID%>').number(true, 2);
                $('#<%=txtDisc2Per.ClientID%>').number(true, 2);
                $('#<%=txtDisc3Per.ClientID%>').number(true, 2);
                $('#<%=txtMDownPer.ClientID%>').number(true, 2);
                $('#<%=txtAddMDown.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetAllRecords() {
            try {
                if ($('#<%=chkVendorItems.ClientID %>').is(':checked')) {
                    <%--$('#<%=txtVendor.ClientID %>').removeAttr("disabled");--%>
            $('#<%=txtVendor.ClientID %>').focus();
            if ($('#<%=txtVendor.ClientID %>').val() != "")
                $('#<%=btnVendorchange.ClientID%>').click();
            return true;
        }
        else {
                    <%--$('#<%=txtVendor.ClientID %>').attr("disabled", "disabled");--%>
            $('#<%=txtVendor.ClientID %>').focus();
                    return false;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncGetVendorItems() {
            try {
                if ($('#<%=txtVendor.ClientID%>').val() != "") {
            $('#<%=btnVendorchange.ClientID%>').click();
                    return true;
                }
                else {
                    ShowPopupMessageBox("Please Enter Vendor Name");
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Grid View Search
        function fncItemSearch(itemObj) {
            var rowObj, SearchData;
            try {
                SearchData = $(itemObj).val().trim();
                SearchData = SearchData.toLowerCase();
                if (SearchData == "") {
                    $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().removeClass("display_none");
                }
                else {
                    $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                        rowObj = $(this);
                        if ($.trim($("td", rowObj).eq(2).text()).toLowerCase().indexOf(SearchData) < 0) {
                            rowObj.addClass("display_none");
                        }
                        else {
                            rowObj.removeClass("display_none");
                        }

                    });
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncVenItemRowdblClk(rowObj) {
            try {
                fncOpenItemhistory($.trim(rowObj));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncVenItemRowClk(rowObj) {
            var siblingObj;
            try {
                $(rowObj).css("background-color", "#fff2e6");
                $(rowObj).siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Initialize VetDetail Dialog
        function fncInitializeVetDetailDialog() {
            try {
                fncBindGSTTable();
                //fncHideCGSTColumninVatSummery();
                $("#divVatDetail").dialog({
                    resizable: false,
                    height: 250,
                    width: 657,
                    modal: true,
                    title: "GST Detail",
                    appendTo: 'form:first'
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindGSTTable() {
            var invQty = 0, ITaxPerc = 0, ITaxPerc4 = 0, Cost = 0, obj;
            var taxAmt = 0, totalgrosscost = 0, emptyGrid = "", cessAmt = 0;
            vatRowNo = 0;
            try {
                vatDetailtbody = $("#tblVetDetail tbody");
                vatDetailtbody.children().remove();

                if ($("#ContentPlaceHolder1_gvSelectItemList tbody").children().length == 1) {
                    if ($("#ContentPlaceHolder1_gvSelectItemList_Label3").text() == "No Records Found") {
                        emptyGrid = "SelectItem+";
                    }
                }
                if ($("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().length == 1) {
                    if ($("#ContentPlaceHolder1_gvVendorLoadItemList_Label3").text() == "No Records Found") {
                        emptyGrid = emptyGrid + "VenItem";
                    }
                }

                debugger;
                if (emptyGrid.indexOf("SelectItem") != -1 && emptyGrid.indexOf("VenItem") != -1)
                    return false;

                if ($("#ContentPlaceHolder1_gvSelectItemList tbody").children().length > 0 && emptyGrid.indexOf("SelectItem") == -1) {
                    debugger;
                    $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                        obj = $(this);

                        debugger;

                        invQty = $.trim(obj.find('td input[id*=txtQty]').val());
                        ITaxPerc = fncConvertFloatJS($.trim($("td", obj).eq(16).text()));
                        ITaxPerc4 = fncConvertFloatJS($.trim($("td", obj).eq(24).text()));
                        Cost = $.trim($("td", obj).eq(13).text());
                        if ($('#<%=hdfTaxType.ClientID%>').val() == "S" || $('#<%=hdfTaxType.ClientID%>').val() == "I") {
                    taxAmt = ((invQty * Cost * ITaxPerc) / 100);
                    cessAmt = ((invQty * Cost * ITaxPerc4) / 100);
                    Cost = invQty * Cost;
                }
                else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                    taxAmt = ((GetQty * Cost * ITaxPerc) / (100 + ITaxPerc3));
                    cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                    Cost = GetQty * Cost;
                }
                totalgrosscost = Cost + taxAmt + cessAmt;
                if (parseFloat(invQty) > 0) {
                    fncCreateVatDetailTable(invQty, taxAmt / 2, taxAmt / 2, cessAmt, ITaxPerc, ITaxPerc4, totalgrosscost, Cost);
                }

            });
        }
        else if ($("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().length > 0 && emptyGrid.indexOf("VenItem") == -1) {
            $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                obj = $(this);
                invQty = $.trim(obj.find('td input[id*=txtQty]').val());
                ITaxPerc = fncConvertFloatJS($.trim($("td", obj).eq(19).text()));
                ITaxPerc4 = fncConvertFloatJS($.trim($("td", obj).eq(25).text()));
                Cost = $.trim($("td", obj).eq(10).text());
                if ($('#<%=hdfTaxType.ClientID%>').val() == "S" || $('#<%=hdfTaxType.ClientID%>').val() == "I") {
                    taxAmt = ((invQty * Cost * ITaxPerc) / 100);
                    cessAmt = ((invQty * Cost * ITaxPerc4) / 100);
                    Cost = invQty * Cost;
                }
                else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                    taxAmt = ((GetQty * Cost * ITaxPerc) / (100 + ITaxPerc3));
                    cessAmt = ((GetQty * Cost * ITaxPerc4) / 100);
                    Cost = GetQty * Cost;
                }
                totalgrosscost = Cost + taxAmt + cessAmt;
                if (parseFloat(invQty) > 0) {
                    fncCreateVatDetailTable(invQty, taxAmt / 2, taxAmt / 2, cessAmt, ITaxPerc, ITaxPerc4, totalgrosscost, Cost);
                }
            });
                }

                fncVatSubTotalCalcForFooter();

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Create VetDetail Table
        function fncCreateVatDetailTable(invQty, taxAmt1, taxAmt2, cessAmt, taxPerc, cessPer, totalgrosscost, taxableAmt) {
            try {
                var status = "NotAvailable", rowObj, localTaxPerc, localCessper, totalPurValue = 0;
                var loctotgrosscost = 0, localtaxAmt1 = 0, localtaxAmt2 = 0, localCessAmt = 0;
                var loctotPurValue = 0;
                debugger;
                totalPurValue = (parseFloat(taxAmt1) + parseFloat(taxAmt2) + cessAmt + parseFloat(taxableAmt));//.toFixed(2);

                vatDetailtbody.children().each(function () {
                    localTaxPerc = $(this).find('td[id*="tdTaxPerc"]').text();
                    localCessper = $(this).find('td[id*="tdCessPerc"]').text();
                    debugger;
                    if (parseFloat(taxPerc) == parseFloat(localTaxPerc) && parseFloat(cessPer) == parseFloat(localCessper)) {
                        loctotgrosscost = $(this).find('td[id*=tdTaxablePurValue]').text();
                        localtaxAmt1 = $(this).find('td[id*=tdTaxAmt1]').text();
                        localtaxAmt2 = $(this).find('td[id*=tdTaxAmt2]').text();
                        localCessAmt = $(this).find('td[id*=tdCessAmt]').text();
                        loctotPurValue = $(this).find('td[id*=tdTotalvalue]').text();
                        debugger;
                        $(this).find('td[id*=tdTaxablePurValue]').text((parseFloat(loctotgrosscost) + parseFloat(taxableAmt)).toFixed(2));
                        $(this).find('td[id*=tdTaxAmt1]').text((parseFloat(localtaxAmt1) + parseFloat(taxAmt1)).toFixed(4));
                        $(this).find('td[id*=tdTaxAmt2]').text((parseFloat(localtaxAmt2) + parseFloat(taxAmt2)).toFixed(4));
                        $(this).find('td[id*=tdCessAmt]').text((parseFloat(localCessAmt) + parseFloat(cessAmt)).toFixed(4));
                        $(this).find('td[id*=tdTotalvalue]').text((parseFloat(loctotPurValue) + parseFloat(totalPurValue)).toFixed(2));
                        status = "Available";
                    }
                });
                if (status == "NotAvailable") {
                    vatRowNo = parseInt(vatRowNo) + 1;
                    debugger;
                    rowObj = "<tr><td>" + vatRowNo
                        + "</td><td id='tdTaxPerc_" + vatRowNo + "'>" + parseFloat(taxPerc).toFixed(2)
                        + "</td><td id='tdCessPerc_" + vatRowNo + "'>" + parseFloat(cessPer).toFixed(2)
                        //+ "</td><td id='tdTaxablePurValue_" + vatRowNo + "'>" + parseFloat(totalgrosscost).toFixed(2)
                        + "</td><td id='tdTaxablePurValue_" + vatRowNo + "'>" + parseFloat(taxableAmt).toFixed(2)
                        + "</td><td id='tdTaxAmt1_" + vatRowNo + "'>" + parseFloat(taxAmt1).toFixed(4)
                        + "</td><td id='tdTaxAmt2_" + vatRowNo + "'>" + parseFloat(taxAmt2).toFixed(4)
                        + "</td><td id='tdCessAmt_" + vatRowNo + "'>" + parseFloat(cessAmt).toFixed(4)
                        + "</td><td id='tdTotalvalue_" + vatRowNo + "'>" + parseFloat(totalPurValue)//.toFixed(2)
                        + "</td></tr>";
                    vatDetailtbody.append(rowObj);

                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }




        ///Vet Sub Total Calculation
        function fncVatSubTotalCalcForFooter() {
            try {
                var totalTaxablePurValue = 0, totalPurValue = 0, totalTaxtAmt1 = 0, totalTaxtAmt2 = 0, totalCessAmt = 0;

                vatDetailtbody = $("#tblVetDetail tbody");

                vatDetailtbody.children().each(function () {
                    totalTaxablePurValue = parseFloat(totalTaxablePurValue) + parseFloat($(this).find('td[id*=tdTaxablePurValue]').text());
                    totalTaxtAmt1 = parseFloat(totalTaxtAmt1) + parseFloat($(this).find('td[id*=tdTaxAmt1]').text());
                    totalTaxtAmt2 = parseFloat(totalTaxtAmt2) + parseFloat($(this).find('td[id*="tdTaxAmt2"]').text());
                    totalCessAmt = parseFloat(totalCessAmt) + parseFloat($(this).find('td[id*="tdCessAmt"]').text());
                    totalPurValue = parseFloat(totalPurValue) + parseFloat($(this).find('td[id*="tdTotalvalue"]').text());
                });

       <%-- //hide GST Column Based on Vendor Status 
        if ($('#<%=hidGidGSTFlag.ClientID%>').val() == "2" || $('#<%=hidGidGSTFlag.ClientID%>').val() == "3") {
            vatDetailtbody.find('td[id*="tdTaxAmt2"]').addClass("hiddencol");
            $("#tblVetDetail [id*=lblTotalTaxAmt2]").addClass("hiddencol");
        }--%>

                $("#tblVetDetail [id*=lblTotalTaxablePurValue]").text(totalTaxablePurValue.toFixed(2));
                $("#tblVetDetail [id*=lblTotalTaxAmt1]").text(totalTaxtAmt1.toFixed(4));
                $("#tblVetDetail [id*=lblTotalTaxAmt2]").text(totalTaxtAmt2.toFixed(4));
                $("#tblVetDetail [id*=lblTotalCessAmt]").text(totalCessAmt.toFixed(4));
                $("#tblVetDetail [id*=lblTotalPurValue]").text(totalPurValue.toFixed(2));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Change Grid Color
        function fncChangeGridColor() {
            var obj;
            try {
                $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                    obj = $(this);
                    $("td", obj).eq(11).css("background-color", "#fff2e6");
                    $("td", obj).eq(12).css("background-color", "#fff2e6");
                    $("td", obj).eq(13).css("background-color", "#fff2e6");
                    $("td", obj).eq(14).css("background-color", "#fff2e6");


                    if ($("td", obj).eq(28).text().trim() == "PCS") {
                        obj.find('td input[id*="txtQty"]').number(true, 0);
                        obj.find('td input[id*="txtWQty"]').number(true, 0);
                    }
                    else {
                        obj.find('td input[id*="txtQty"]').number(true, 2);
                        obj.find('td input[id*="txtWQty"]').number(true, 2);
                    }

                });

                if ($('#<%=txtVendor.ClientID %>').val() != "") {
                    $('#<%=txtVendor.ClientID %>').attr("readonly", "readonly");
                     
                }
               

      <%--  if ($('#<%=txtBrand.ClientID %>').val() != "") {
            $('#<%=txtBrand.ClientID %>').attr("readonly", "readonly");
        }
        else {
            $('#<%=txtBrand.ClientID %>').removeAttr("readonly");
        }--%>
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncChangeNumberFormartForQty() {
            var obj;
            try {
                $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                    obj = $(this);
                    if ($("td", obj).eq(26).text().trim() == "PCS") {
                        obj.find('td input[id*="txtQty"]').number(true, 0);
                    }
                    else {
                        obj.find('td input[id*="txtQty"]').number(true, 2);

                    }
                });
                fncSavePOEntryTemporarly();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetPurAndSaleshisData() {
            try {
                $('#<%=btnVendorchange.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncWholeQtyCal(source) {
            var rowObj, qty = 0;
            try {
                rowObj = $(source).closest("tr");
                if ($("td", rowObj).eq(27).text() != "") {
                    qty = parseFloat(rowObj.find('td input[id*="txtWQty"]').val()) * parseFloat($("td", rowObj).eq(27).text());
                    rowObj.find('td input[id*="txtQty"]').val(qty);
                    fncRepeaterColumnValueChange('NewQty');
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncWQtyOnFocus(source) {
            try {
                fncVenItemRowClk((source).closest("tr"));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncCheckPODiscontinueEntry() {
            var obj = {};

            try {
                obj.vendorcode = $('#<%=hidVendor.ClientID %>').val();
        $.ajax({
            type: "POST",
            url: "frmPurchaseOrder.aspx/fncCheckDiscontinueEntryAvailableOrNot",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d != "") {
                    DisContinueEntry = msg.d;
                   
                    PODisContinueEntry("Do you want load Discontinue Entry?");
                }
                else {
                    $('#<%=btnVendorchange.ClientID %>').click();
                }
            },
            error: function (data) {
                fncToastError(data.message);
                return false;
            }
        });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncYesClick_master() {
            try {
                $("#PODisContinueEntry").dialog("destroy");
                $('#<%=hidDiscontinueEntry.ClientID %>').val(DisContinueEntry)
        $('#<%=btnLoadDiscontinueEntry.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncNoClick() {

            try {
                $("#PODisContinueEntry").dialog("close");
                $('#<%=btnVendorchange.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function PODisContinueEntry(msg) {
            $(function () {
                $('#<%=lblSavemsg.ClientID%>').text(msg);
         $("#PODisContinueEntry").dialog({
             title: "Enterpriser Web",
             modal: true,
             width: "auto"
         });
     });
        };

        function fncGetInventoryDetail() {
            try {
                if ($('#<%=chkVendorItems.ClientID %>').is(':checked')) {
            if ($('#<%=txtVendor.ClientID %>').val() == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                ShowPopupMessageBoxandFocustoObject('Please Select Vendor Code');
                $("[id$=txtItemCodeAdd]").val('');
                return false;
            }
        }

        var obj = {};
        var jsonObj;

        obj.InvCode = txtItemCodeAdd.val().trim();
        if ($('#<%=chkVendorItems.ClientID %>').is(':checked')) {
            obj.VendorCode = $('#<%=hidVendor.ClientID %>').val().trim();
        }
        else {
            obj.VendorCode = "";
        }

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/GetAddInvDetail") %>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) { 

                jsonObj = JSON.parse(msg.d); 

                if (jsonObj.Table.length > 0) {
                    if (jsonObj.Table[0]["inventoryStatus"] == "0") {
                        fncToastInformation("Invalid Inventorycode");
                        fncClearEntervalue();
                        return false;
                    }
                }
                if (jsonObj.Table1.length > 0) {
                    if (jsonObj.Table1[0]["vendorStatus"] == "0") {
                        fncToastInformation('<%=Resources.LabelCaption.alert_DifferenVendor%>');
                        fncClearEntervalue()
                        return false;
                    }
                } 
                if (jsonObj.Table2.length > 0) {
                    $('#<%=txtMrp.ClientID %>').val(jsonObj.Table2[0]["MRP"]);
                    $('#<%=txtBasicCost.ClientID %>').val(jsonObj.Table2[0]["BasicCost"]);
                    $('#<%=txtDiscPrc.ClientID %>').val(jsonObj.Table2[0]["DiscountBasicPer"]);
                    $('#<%=txtDisAmt.ClientID %>').val(jsonObj.Table2[0]["DiscountBasicAmt"]);
                    $('#<%=txtGrossCost.ClientID %>').val(jsonObj.Table2[0]["GrossCost"]);
                    $('#<%=txtSellingPrice.ClientID %>').val(jsonObj.Table2[0]["NetSellingPrice"]);
                    $('#<%=txtSGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer1"]);
                    $('#<%=txtCGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer2"]);
                    $('#<%=txtIGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer3"]);
                    $('#<%=txtItemCodeAdd.ClientID %>').val(jsonObj.Table2[0]["InventoryCode"]);
                    $('#<%=txtItemNameAdd.ClientID %>').val(jsonObj.Table2[0]["Description"]);
                    $('#<%=txtCESS.ClientID %>').val(jsonObj.Table2[0]["ITaxPer4"]);
                    $('#<%=txtLUOM.ClientID %>').val(jsonObj.Table2[0]["UOM"]);
                    $('#<%=txtWUOM.ClientID %>').val(jsonObj.Table2[0]["Compatible"]);
                    $('#<%=hidWQtyCon.ClientID %>').val(jsonObj.Table2[0]["UomConv"]);

                    //UOM
                    if (jsonObj.Table2[0]["UOM"].trim() == "PCS") {
                        $('#<%=txtNQty.ClientID %>').number(true, 0);
                        $('#<%=txtLQty.ClientID %>').number(true, 0);
                        $('#<%=txtWQty.ClientID %>').number(true, 0);
                    }
                    else {
                        $('#<%=txtNQty.ClientID %>').number(true, 2);
                        $('#<%=txtLQty.ClientID %>').number(true, 2);
                        $('#<%=txtWQty.ClientID %>').number(true, 2);
                    }

                    $('#<%=txtNet.ClientID %>').val('0.00');
                    $('#<%=txtNetTotalAdd.ClientID %>').val('0.00');
                }

                if (jsonObj.Table3.length > 0) {
                    $('#<%=txtSDPrc.ClientID %>').val(jsonObj.Table3[0]["SchemediscPerc"]);
                    $('#<%=txtSDAmt.ClientID %>').val(jsonObj.Table3[0]["SD"]);
                }

                if ($('#<%=txtWUOM.ClientID %>').val() != "") {
                    $('#<%=txtWQty.ClientID %>').focus().select();
                }
                else {
                    $('#<%=txtLQty.ClientID %>').focus().select();
                }
                $('#<%=lblBatch.ClientID%>').css("visibility", "visible");
                $('#<%=lblBatch.ClientID%>').css("background-color", "green");
                if (jsonObj.Table2[0]["Batch"] == "0") {
                    $('#<%=lblBatch.ClientID%>').text("N.Batch");
                    $('#<%=Label6.ClientID%>').css("font-size", "12px");
                    $('#<%=lblBatch.ClientID%>').css("font-size", "10px");
                }
                else
                    $('#<%=lblBatch.ClientID%>').text("Batch");

                $('#<%=lblPurchaseType.ClientID%>').css("visibility", "visible");
                $('#<%=lblPurchaseType.ClientID%>').css("background-color", "red");
                if ((jsonObj.Table2[0]["PurchasedBy"].trim() == "M" || jsonObj.Table2[0]["PurchasedBy"].trim() == "M1")
                    && jsonObj.Table2[0]["PricingType"].trim() == "MRP") {
                    $('#<%=lblPurchaseType.ClientID%>').text('Full MarkDown(N.C)');
                    $('#<%=lblPurchaseType.ClientID%>').css("margin-left", "15px");
                    $('#divDisc1').hide();
                    $('#divDisc2').hide();
                    $('#divDisc3').hide();
                    $('#divMDown').show();
                    $('#divAdMDown').show();
                    $('#<%=txtMDownPer.ClientID%>').val(jsonObj.Table2[0]["MarkDownPerc"]);
                    $('#<%=txtAddMDown.ClientID%>').val(jsonObj.Table2[0]["VatLiabilityPer"]);
                    $('#<%=txtBasicCost.ClientID%>').attr("disabled", "disabled");
                }
                else if ((jsonObj.Table2[0]["PurchasedBy"].trim() == "M" || jsonObj.Table2[0]["PurchasedBy"].trim() == "M2")
                    && jsonObj.Table2[0]["PricingType"].trim() == "SELLINGPRICE") {
                    $('#<%=lblPurchaseType.ClientID%>').text("Partial MarkDown(N.C)");
                    $('#<%=lblPurchaseType.ClientID%>').css("margin-left", "0px");
                    $('#divDisc1').hide();
                    $('#divDisc2').hide();
                    $('#divDisc3').hide();
                    $('#divMDown').show();
                    $('#divAdMDown').show();
                    $('#<%=txtMDownPer.ClientID%>').val(jsonObj.Table2[0]["MarkDownPerc"]);
                    $('#<%=txtAddMDown.ClientID%>').val(jsonObj.Table2[0]["VatLiabilityPer"]);
                    $('#<%=txtBasicCost.ClientID%>').attr("disabled", "disabled");
                }
                else if (jsonObj.Table2[0]["PurchasedBy"].trim() == "M3") {
                    $('#<%=lblPurchaseType.ClientID%>').text("Full MarkDown(G.C)");
                    $('#<%=lblPurchaseType.ClientID%>').css("margin-left", "0px");
                    $('#divDisc1').hide();
                    $('#divDisc2').hide();
                    $('#divDisc3').hide();
                    $('#divMDown').show();
                    $('#divAdMDown').show();
                    $('#<%=txtMDownPer.ClientID%>').val(jsonObj.Table2[0]["MarkDownPerc"]);
                    $('#<%=txtAddMDown.ClientID%>').val(jsonObj.Table2[0]["VatLiabilityPer"]);
                    $('#<%=txtBasicCost.ClientID%>').attr("disabled", "disabled");
                }
                else if (jsonObj.Table2[0]["PurchasedBy"].trim() == "M4") {
                    $('#<%=lblPurchaseType.ClientID%>').text("Partial MarkDown(G.C)");
                    $('#<%=lblPurchaseType.ClientID%>').css("margin-left", "0px");
                    $('#divDisc1').hide();
                    $('#divDisc2').hide();
                    $('#divDisc3').hide();
                    $('#divMDown').show();
                    $('#divAdMDown').show();
                    $('#<%=txtMDownPer.ClientID%>').val(jsonObj.Table2[0]["MarkDownPerc"]);
                    $('#<%=txtAddMDown.ClientID%>').val(jsonObj.Table2[0]["VatLiabilityPer"]);
                    $('#<%=txtBasicCost.ClientID%>').attr("disabled", "disabled");
                }
                else if (jsonObj.Table2[0]["PurchasedBy"].trim() == "C" || jsonObj.Table2[0]["PurchasedBy"].trim() == "CS"
                    && jsonObj.Table2[0]["PricingType"].trim() == "SELLINGPRICE") {
                    $('#<%=lblPurchaseType.ClientID%>').text("Mark Up");
                    $('#<%=lblPurchaseType.ClientID%>').css("margin-left", "15px");
                    $('#divDisc1').show();
                    $('#divDisc2').show();
                    $('#divDisc3').show();
                    $('#divMDown').hide();
                    $('#divAdMDown').hide();
                    $('#<%=txtDisc1Per.ClientID%>').val(jsonObj.Table2[0]["DiscountBasicPer"]);
                    $('#<%=txtDisc2Per.ClientID%>').val(jsonObj.Table2[0]["DiscountBasicPer2"]);
                    $('#<%=txtDisc3Per.ClientID%>').val(jsonObj.Table2[0]["DiscountBasicPer3"]);
                    $('#<%=txtBasicCost.ClientID%>').removeAttr("disabled");
                }
                if (parseFloat(jsonObj.Table2[0]["ITaxPer4"]) == 0 || jsonObj.Table2[0]["ITaxPer4"] == "") {
                    $('#<%=txtAddCess.ClientID%>').attr("disabled", "disabled");
                }
                else {
                    $('#<%=txtAddCess.ClientID%>').removeAttr("disabled");
                }
                if (jsonObj.Table2[0]["VenCatStatus"] == "Y") {   
                    $('#<%=txtCatMrp.ClientID%>').val(jsonObj.Table2[0]["CatMrp"]);
                    $('#<%=txtCatCost.ClientID%>').val(jsonObj.Table2[0]["CatCost"]);
                    $('#<%=txtCatNetCost.ClientID%>').val(jsonObj.Table2[0]["CatNet"]);
                    $('#<%=txtCatSellPrice.ClientID%>').val(jsonObj.Table2[0]["CatSPrice"]); 

                    $('#<%=txtCatMrp.ClientID%>').number(true, 2);
                    $('#<%=txtCatCost.ClientID%>').number(true, 2);
                    $('#<%=txtCatNetCost.ClientID%>').number(true, 2); 
                    $('#<%=txtCatSellPrice.ClientID%>').number(true, 2);

                    $("#divVendorcatelogAlert").dialog({ 
                        resizable: false,
                        height: "auto",
                        width: 800,
                        modal: true,
                        dialogClass: "no-close",
                        title:"Vendor Catalog Detail"
                    });

                    $('#divVendorcatelogAlert').dialog('open');
                    $('#btnAdd').focus();  
                }
                else {
                    $('#divVendorcatelogAlert').hide();
                } 
                fncDisableAndEnableOnVendorChange(true); 
            },
            error: function (data) {
                fncToastError(data.message);
            }
        });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncWQtyCal() {
            var lQty = 0;
            try {
                lQty = $('#<%=txtWQty.ClientID %>').val() * $('#<%=hidWQtyCon.ClientID %>').val();
        $('#<%=txtLQty.ClientID %>').val(lQty);

        if ($('#<%=lblPurchaseType.ClientID%>').text() == "Mark Up") {
                    fncMarkup_Netcost_Calculation();
                } else {
                    fncMRP1_NetCost();
                }
                //fncCalcNetAmount();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }



        function fncCalcNetAmount() {

            var NQty = $('#<%=txtNQty.ClientID%>').val();
    if (NQty != "" && NQty != "0") {
        //===============> Net Amt Calc

        if (hdfTaxType.val() == 'S') {
            var txtSGST = $('#<%=txtSGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtSGST.ClientID%>').val();
            var txtCGST = $('#<%=txtCGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtCGST.ClientID%>').val()
            var TotalGstPerc = parseFloat(txtSGST) + parseFloat(txtCGST);
            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
            var TotalGstAmt = ((GrossCost * TotalGstPerc) / (100));

            NetAmt = parseFloat(GrossCost) + parseFloat(TotalGstAmt);
            $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

            //==========> Net Total Amt Calc
            NetAmt = $('#<%=txtNet.ClientID%>').val();
            var SGST = parseFloat(TotalGstAmt) / 2;
            $('#<%=txtSGSTAmt.ClientID%>').val(parseFloat(SGST).toFixed(2));
            $('#<%=txtCGSTAmt.ClientID%>').val(parseFloat(SGST).toFixed(2));
            var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
            $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
        }
        else if (hdfTaxType.val() == 'I') {
            var txtIGST = $('#<%=txtIGST.ClientID%>').val();
            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
            var IGSTAmt = 0;
            if (txtIGST != "" && txtSGST != "0.00") {
                var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                var IGSTAmt = ((GrossCost * txtIGST) / (100));
            }
            else {
                var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
            }

            NetAmt = parseFloat(GrossCost) + parseFloat(IGSTAmt);
            $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

            //==========> Net Total Amt Calc
            NetAmt = $('#<%=txtNet.ClientID%>').val();
            var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
            $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
        }
        else if (hdfTaxType.val() == 'Import') {
            var txtIGST = $('#<%=txtIGST.ClientID%>').val();
            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
            var IGSTAmt = 0;
            if (txtIGST != "" && txtSGST != "0.00") {
                var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                var IGSTAmt = ((GrossCost * txtIGST) / (100));
            }
            else {
                var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
            }

            NetAmt = parseFloat(GrossCost);
            $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

            //==========> Net Total Amt Calc
            NetAmt = $('#<%=txtNet.ClientID%>').val();
            var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
            $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
        }
        else {
            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();

            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));

            NetAmt = parseFloat(GrossCost);
            $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

            //==========> Net Total Amt Calc
            NetAmt = $('#<%=txtNet.ClientID%>').val();
            var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
            $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                }

            }
        }

        function fncColorChangeOnSelectionForFOCPOSalesHis() {
            try {
                if ($('#divItemhistory').css('display') == 'block') {
                    $('#<%=lnkSaleshistory.ClientID%>').css("background-color", "#f44336");
        }
        if ($('#divLastFoc').css('display') == 'block') {
            $('#<%=lnkLastFocDetail.ClientID%>').css("background-color", "#f44336");
                }
                fncOpenPoPending();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //Open Image
        function fncOpenImageScreen() {
            var page = '<%=ResolveUrl("~/Purchase/frmPurchaseOrderImage.aspx") %>';
    var page = page + "?PoNo=" + $('#<%=txtPONO.ClientID%>').val()
            var $dialog = $('<div id="PopupPoImage" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                height: 500,
                width: 900,
                title: "Purchase Order Image",
                appendTo: 'form:first'
            });
            $dialog.dialog('open');
        }

        function fncSetGridQty() {//Vijay Qty
            $("#ContentPlaceHolder1_gvVendorLoadItemList tbody").children().each(function () {
                var obj = $(this);
                var GetQty = $.trim(obj.find('td input[id*=txtQty]').val());
                var ItemCode = $.trim($("td", obj).eq(1).text());
                if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {

                    var split, splitItemcode, splitagian = {};
                    split = $('#<%=hidZeroQty.ClientID%>').val().split(',');
            splitItemcode = $('#<%=hdfItemCode.ClientID%>').val().split('|');
            if (split.length > 0) {
                for (var i = 0; split.length - 1 > i; i++) {
                    if (split[i] == ItemCode) {
                        obj.find('td input[id*=txtQty]').val("0.00");
                    }
                    for (var j = 0; j < splitItemcode.length; j++) {
                        splitagian = splitItemcode[j].split(';');
                        if (splitagian[0] != split[i]) {
                            $('#<%=hdfItemCode.ClientID%>').val(splitItemcode[j] + '|');
                        }
                    }
                }
            }

        }
        else {
            $("#ContentPlaceHolder1_gvSelectItemList tbody").children().each(function () {
                var Item = $.trim($("td", $(this)).eq(2).text());
                if (ItemCode == Item) {
                    obj.find('td input[id*=txtQty]').val($(this).find('td input[id*=txtQty]').val());
                }

            });
        }
    });
    $('#<%=hidZeroQty.ClientID%>').val('');
        }
        //MarkUp Calculation
        function fncMarkup_Netcost_Calculation() {
            try {
                var NQty = $('#<%=txtNQty.ClientID%>').val();
        var BasicCost = parseFloat($('#<%=txtBasicCost.ClientID%>').val()).toFixed(2);
        var TotDiscAmt;
        var DiscountedBasicCost;
        var DiscAmt1 = (BasicCost * parseFloat($('#<%=txtDisc1Per.ClientID%>').val()).toFixed(2) / 100).toFixed(2);
        TotDiscAmt = DiscAmt1;
        DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(DiscAmt1)).toFixed(2)
        var DiscAmt2 = (DiscountedBasicCost * parseFloat($('#<%=txtDisc2Per.ClientID%>').val()).toFixed(2) / 100).toFixed(2);
        TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
        DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
        var DiscAmt3 = (DiscountedBasicCost * parseFloat($('#<%=txtDisc3Per.ClientID%>').val()).toFixed(2) / 100).toFixed(2);
        TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

        var GrossCost = (parseFloat($('#<%=txtBasicCost.ClientID%>').val()).toFixed(2) - TotDiscAmt).toFixed(2);

        var CGSTAmt = (GrossCost * parseFloat($('#<%=txtCGST.ClientID%>').val()) / 100).toFixed(2);
        var SGSTAmt = CGSTAmt;
        var CESSAmt = (GrossCost * parseFloat($('#<%=txtCESS.ClientID%>').val()).toFixed(2) / 100).toFixed(2);

        var NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
            parseFloat(CESSAmt) + parseFloat($('#<%=txtAddCess.ClientID%>').val())).toFixed(2);
        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetCost).toFixed(2);

        $('#<%=txtDiscountAmt.ClientID%>').val(parseFloat(TotDiscAmt).toFixed(2));
        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
        <%--$('#<%=txtCGST.ClientID%>').val(parseFloat(CGSTAmt).toFixed(2));
        $('#<%=txtSGST.ClientID%>').val(parseFloat(SGSTAmt).toFixed(2));
        $('#<%=txtCESS.ClientID%>').val(parseFloat(CESSAmt).toFixed(2));--%>
        $('#<%=txtNet.ClientID%>').val(parseFloat(NetCost).toFixed(2));
        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Full  && Partial MarkDown Calculation
        function fncMRP1_NetCost() {
            try {
                var NQty = $('#<%=txtNQty.ClientID%>').val();
        var MRP1 = parseFloat($('#<%=txtMrp.ClientID%>').val()).toFixed(2);
        var MRP1_MDP = parseFloat($('#<%=txtMDownPer.ClientID%>').val()).toFixed(2);
        var MRP1_MDAP = parseFloat($('#<%=txtAddMDown.ClientID%>').val()).toFixed(2);
        //var NetCost = MRP1 - MRP1 * MRP1_MDP / 100;
        var NetCost = parseFloat(MRP1 - MRP1 * MRP1_MDP / 100).toFixed(2);
        NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2);

        var CGST = parseFloat($('#<%=txtCGST.ClientID%>').val()).toFixed(2);
        var SGST = parseFloat($('#<%=txtSGST.ClientID%>').val()).toFixed(2);
        var CESS = parseFloat($('#<%=txtCESS.ClientID%>').val()).toFixed(2);

        var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat($('#<%=txtAddCess.ClientID%>').val()).toFixed(2);
        var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) +
            parseFloat(CESS)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)))).toFixed(2);

        var SGSTAmt;
        var CGSTAmt;
        var CESSAmt;
        if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
            SGSTAmt = 0;
            CGSTAmt = 0;
            CESSAmt = 0;
        }
        else {
            SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) * parseFloat(SGST)).toFixed(2);
            CGSTAmt = SGSTAmt;
            CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
        }

        var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

        if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
            GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
            CESSAmt = 0.00;
        }
        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetCost).toFixed(2);
       <%-- $('#<%=txtCGST.ClientID%>').val(parseFloat(CGSTAmt).toFixed(2));
        $('#<%=txtSGST.ClientID%>').val(parseFloat(SGSTAmt).toFixed(2));
        $('#<%=txtCESS.ClientID%>').val(parseFloat(CESSAmt).toFixed(2));--%>
        $('#<%=txtSGSTAmt.ClientID%>').val(parseFloat(SGSTAmt).toFixed(2));
        $('#<%=txtCGSTAmt.ClientID%>').val(parseFloat(CGSTAmt).toFixed(2));
        $('#<%=txtBasicCost.ClientID%>').val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
        $('#<%=txtNet.ClientID%>').val(parseFloat(NetCost).toFixed(2));
        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncMRP34_GrossCost() {
            try {

                var NQty = $('#<%=txtNQty.ClientID%>').val();
        var MRP34 = parseFloat($('#<%=txtMrp.ClientID%>').val()).toFixed(2);
        var MRP34_MDP = parseFloat($('#<%=txtMDownPer.ClientID%>').val()).toFixed(2);
        var MRP34_MDAP = parseFloat($('#<%=txtAddMDown.ClientID%>').val()).toFixed(2);

        var GrossCost = parseFloat(MRP34 - MRP34 * MRP34_MDP / 100).toFixed(2);
        GrossCost = GrossCost - parseFloat(GrossCost * MRP34_MDAP / 100).toFixed(2);

        var CGST = parseFloat($('#<%=txtCGST.ClientID%>').val()).toFixed(2);
        var SGST = parseFloat($('#<%=txtSGST.ClientID%>').val()).toFixed(2);
        var CESS = parseFloat($('#<%=txtCESS.ClientID%>').val()).toFixed(2);
        var AdCess = parseFloat($('#<%=txtAddCess.ClientID%>').val()).toFixed(2)

        var SGSTAmt;
        var CGSTAmt;
        var CESSAmt;
        if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
            SGSTAmt = 0;
            CGSTAmt = 0;
            CESSAmt = 0;
        }
        else {
            SGSTAmt = (parseFloat(GrossCost) * parseFloat(SGST) / 100).toFixed(2);
            CGSTAmt = SGSTAmt;
            CESSAmt = (parseFloat(GrossCost) * parseFloat(CESS) / 100).toFixed(2);
        }

        var NetCost = (parseFloat(parseFloat(GrossCost) + parseFloat(SGSTAmt) + parseFloat(CGSTAmt)
            + parseFloat(CESSAmt) + parseFloat(AdCess))).toFixed(2);

        if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
            GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
            CESSAmt = 0.00;
        }
        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetCost).toFixed(2);
        //txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
        //txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
        //txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
        //txtBasicCost.val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
        //txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
        //txtNetCost.val(parseFloat(NetCost).toFixed(2));
        $('#<%=txtSGSTAmt.ClientID%>').val(parseFloat(SGSTAmt).toFixed(2));
        $('#<%=txtCGSTAmt.ClientID%>').val(parseFloat(CGSTAmt).toFixed(2));
        $('#<%=txtBasicCost.ClientID%>').val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
        $('#<%=txtNet.ClientID%>').val(parseFloat(NetCost).toFixed(2));
        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncPurchaseKeyPress(evt, value) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 9) {
                    /// MRP Key Press
                    if (value == "MRP") {
                        setTimeout(function () {
                            if ($('#<%=txtBasicCost.ClientID%>').is(':disabled')) {
                        $('#<%=txtSDPrc.ClientID%>').select();
                    }
                    else {
                        $('#<%=txtBasicCost.ClientID%>').select();
                    }
                }, 50);
            }
            else if (value == "SDprc") {
                setTimeout(function () {
                    if ($('#<%=txtAddCess.ClientID%>').is(':disabled')) {
                        $('#<%=txtSellingPrice.ClientID%>').select();
                    }
                    else {
                        $('#<%=txtAddCess.ClientID%>').select();
                    }
                }, 50);
                    }
                    return false;
                }

            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
            }
            if (charCode == 13) {
                fncGetInventoryDetail();
                return false;
            }
        }


        function fncGetGridValuesForSave() {
            var obj;
            try {
                var xml = '<NewDataSet>';
                $('#<%=gvSelectItemList.ClientID%>').find('tr').each(function () {
                
                obj = $(this);
                if (parseFloat(obj.find('td input[id*=txtQty]').val()) > 0) {
                    xml += "<Table>";
                    xml += "<RowNo>" + obj.find("td:nth-child(2)").html() + "</RowNo>";
                    xml += "<InventoryCode>" + obj.find("td:nth-child(3)").html() + "</InventoryCode>";
                    xml += "<Description>" + obj.find("td:nth-child(4)").html().trim().replace('>', ')').replace('<', '(') + "</Description>";
                    xml += "<Size>" + obj.find("td:nth-child(5)").html() + "</Size>";
                    xml += "<Qty>" + obj.find('td input[id*=txtQty]').val() + "</Qty>";
                    xml += "<Foc>" + obj.find("td:nth-child(7)").html() + "</Foc>";
                    xml += "<MRP>" + obj.find("td:nth-child(8)").html() + "</MRP>";
                    xml += "<SellingPrice>" + obj.find("td:nth-child(9)").html() + "</SellingPrice>";
                    xml += "<Cost>" + obj.find("td:nth-child(10)").html() + "</Cost>";
                    xml += "<DisAmt>" + obj.find("td:nth-child(11)").html() + "</DisAmt>";
                    xml += "<ODiscPRC>" + obj.find("td:nth-child(12)").html() + "</ODiscPRC>";
                    xml += "<ODiscAmt>" + obj.find("td:nth-child(13)").html() + "</ODiscAmt>";
                    xml += "<Gross>" + obj.find("td:nth-child(14)").html() + "</Gross>";
                    xml += "<SGSTPrc>" + obj.find("td:nth-child(15)").html() + "</SGSTPrc>";
                    xml += "<CGSTPrc>" + obj.find("td:nth-child(16)").html() + "</CGSTPrc>";
                    xml += "<IGSTPrc>" + obj.find("td:nth-child(17)").html() + "</IGSTPrc>";
                    xml += "<Net>" + obj.find("td:nth-child(18)").html() + "</Net>";
                    xml += "<Total>" + obj.find("td:nth-child(19)").html() + "</Total>";
                    xml += "<SGSTAmt>" + obj.find("td:nth-child(20)").html() + "</SGSTAmt>";
                    xml += "<CGSTAmt>" + obj.find("td:nth-child(21)").html() + "</CGSTAmt>";
                    xml += "<IGSTAmt>" + obj.find("td:nth-child(22)").html() + "</IGSTAmt>";
                    xml += "<Checked>" + obj.find("td:nth-child(23)").html() + "</Checked>";
                    xml += "<PoQty>" + obj.find("td:nth-child(24)").html() + "</PoQty>";
                    xml += "<CESSPrc>" + obj.find("td:nth-child(25)").html() + "</CESSPrc>";
                    xml += "<CESSAmt>" + obj.find("td:nth-child(26)").html() + "</CESSAmt>";
                    xml += "<Mode>" + "I" + "</Mode>";
                    xml += "<UOM>" + obj.find("td:nth-child(27)").html() + "</UOM>";
                    xml += "<WQty>" + obj.find("td:nth-child(28)").html() + "</WQty>";
                    //xml += "<TranInQty>" + obj.find("td:nth-child(29)").html() + "</TranInQty>"
                    xml += "</Table>";
                }
            });
            xml = xml + '</NewDataSet>'
            xml = escape(xml);
            <%--$('#<%=GRNXmldata.ClientID %>').val(xml);--%>

                console.log(xml);
                return xml;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function StopPurchaseToast(Vendorname) {
            $('#<%=txtVendor.ClientID%>').val('');
            ShowPopupMessageBox("This Vendor in Stop Purchase Mode.Please Activate the Purchase for - " + $.trim(Vendorname));
            return false;
        }

        function fncVendorCatlogClick() {
            $('#<%=txtMrp.ClientID%>').val($('#<%=txtCatMrp.ClientID%>').val());
            $('#<%=txtBasicCost.ClientID%>').val($('#<%=txtCatCost.ClientID%>').val());
            $('#<%=txtNet.ClientID%>').val($('#<%=txtCatNetCost.ClientID%>').val());
            $('#<%=txtSellingPrice.ClientID%>').val($('#<%=txtCatSellPrice.ClientID%>').val());
            $('#<%=txtLQty.ClientID%>').select();
            $('#divVendorcatelogAlert').dialog('close'); 
        }
        function fncVenCatClose() {
            $('#divVendorcatelogAlert').dialog('close'); 
            $('#<%=txtLQty.ClientID%>').select();
        }

        //search with mrp
        function fncItemBindwithmrpfiltertoTable(e, val) { 
            var charCode = (e.which) ? e.which : e.keyCode;
            try {
                if (charCode == 13) {
                    if ($('#<%=txtVendor.ClientID%>').val() != "") {
                        if (val != '') {

                            $('#<%=hidSearchMRP.ClientID%>').val(val);
                            //__doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');
                            //$("#btnsearchgrid").click();
                            $('#ContentPlaceHolder1_gvVendorLoadItemList tbody').children().each(function () {
                                obj = $(this);

                                if (obj.find('td').eq(8).html() == val + '.00') {
                                    obj.show();
                                }
                                else {
                                    obj.hide();
                                }
                            });

                        }
                        else {
                            $('#ContentPlaceHolder1_gvVendorLoadItemList tbody').children().each(function () {
                                $(this).show();
                            });
                        }
                        
                    }
                    else {
                        $('#txtmrp').val('');
                        ShowPopupMessageBox("Please Enter Vendor Code !.");
                    }
                    return false;
                }
                else {
                    return true;
                }
            } 
            catch (err) {
                fncToastError(err.message);
            } 
        }

        function showAll() {
          
            $("#dialog-POPending").dialog({           //musaraf 19112022
                dialogClass: "no-close",
                title: "Enterpriser Web",
                width: 400,
                modal: true,
                buttons: {
                    "OK": function () {
                          
                        $("#<%=btnsave.ClientID %>").click();
                        
                    }
                    //Cancel: function () {
                    //    $(this).dialog("close");
                    //}
                }
            }).html("Purchase Order Saved Successfully..!")
        }




    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmPurchaseOrderView.aspx">View Purchase Order</a> </li>
                <li class="active-page">Purchase Order </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <asp:UpdatePanel ID="updtPnlTop" runat="Server">
                <ContentTemplate>
                    <div class="top-purchase-container">

                        <div class="top-purchase-left-container-po po_top_left">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="PO No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPONO" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendor" runat="server" onkeydown="return fncVendorSearch(event);" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblSearchItem" runat="server" Text="Search Items in Grid"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSearchItem" runat="server" CssClass="form-control-res" oninput="fncItemSearch(this)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Brand"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:CheckBox ID="chkVendorLoad" Checked="true" onchange="return fncGetVendorItems();" runat="server" Text="500 Items"></asp:CheckBox>
                                    </div>
                                    <div class="label-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-middle-container-po">
                            <div class="control-group-single">
                                <div class="label-right" style="width: 100%">
                                    <asp:TextBox ID="txtVendorDetails" runat="server" CssClass="form-control-res" TextMode="MultiLine"
                                        Style="height: 53px" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-right-container-po float_left ">
                            <div class="control-group-split">
                                <div class="control-group-left1">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-middle">
                                    <div class="label-left" style="width: 55%">
                                        <asp:Label ID="Label9" runat="server" Text="Purchase Date"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 45%">
                                        <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right1">
                                    <div class="label-left" style="width: 55%">
                                        <asp:Label ID="Label10" runat="server" Text="Delivery Date"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 45%">
                                        <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="label-left" style="width: 100%">
                                <div class="control-container" style="float: left">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkLastFocDetail" runat="server" class="button-link" OnClientClick="fncOpenLastFOCDet();return false;">Last Foc Detail</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkPOPending" runat="server" class="button-link" OnClick="lnkPOPending_Click" Text="PO Pending"></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkSaleshistory" runat="server" class="button-link" OnClientClick="fncShowandHideSaleshistory();return false;">Sales History</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:CheckBox ID="chkVendorItems" onchange="return fncGetAllRecords();" runat="server"
                                            Checked="true" Text="VendorItems(F12)" />
                                        <asp:CheckBox ID="chkShowPurSaleshis" runat="server" onchange="fncGetPurAndSaleshisData();" Text="Pur/Sales History" />

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12" id="divVendorItem" style="margin-top: -20px;">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3 sort_by">
                                <div class="col-md-3">
                                    <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                                </div>
                                <div class="col-md-7">
                                    <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                        <asp:ListItem Value="S.No" Text="S.No"></asp:ListItem>
                                        <asp:ListItem Value="ItemName" Text="Description"></asp:ListItem>
                                        <asp:ListItem Value="SuggestQty" Text="Stock"></asp:ListItem>
                                        <asp:ListItem Value="MRP" Text="MRP"></asp:ListItem>
                                        <asp:ListItem Value="SellingPrice" Text="SellingPrice"></asp:ListItem>
                                        <asp:ListItem Value="Cost" Text="CurrPrice"></asp:ListItem>
                                        <asp:ListItem Value="NetCost" Text="NetCost"></asp:ListItem>
                                        <asp:ListItem Value="SoldQty" Text="SoldQty"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                </div>
                            </div>

                        </div>
                        <div id="divSelectedItem" class="col-md-12" style="margin-top: -20px;">
                            <div class="col-md-9">
                            </div>
                            <div class="col-md-3 sort_by">
                                <div class="col-md-3">
                                    <label id="lblSortItem" style="margin-top: 5px;">Sort by:</label>
                                </div>
                                <div class="col-md-7">
                                    <asp:DropDownList runat="server" Width="100%" ID="ddlSelectedItemSort">
                                        <asp:ListItem Value="S.No" Text="S.No"></asp:ListItem>
                                        <asp:ListItem Value="ItemName" Text="Description"></asp:ListItem>
                                        <asp:ListItem Value="SuggestQty" Text="Stock"></asp:ListItem>
                                        <asp:ListItem Value="MRP" Text="MRP"></asp:ListItem>
                                        <asp:ListItem Value="SellingPrice" Text="SellingPrice"></asp:ListItem>
                                        <asp:ListItem Value="Cost" Text="CurrPrice"></asp:ListItem>
                                        <asp:ListItem Value="NetCost" Text="NetCost"></asp:ListItem>
                                        <asp:ListItem Value="SoldQty" Text="SoldQty"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblUP2" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblDown2" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="bottom-purchase-container">
                <div class="panel panel-default">
                    <div id="Tabs" role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs custnav custnav-po display_none" role="tablist">
                            <li id="liVendorwise"><a href="#VendorWiseInv" aria-controls="VendorWiseInv" role="tab" data-toggle="tab">Vendor Wise Inventory </a></li>
                            <li id="liVendorAddInv"><a href="#ViewAddInv" aria-controls="ViewAddInv" role="tab" data-toggle="tab">View
                                Add Inventory</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" >
                            <div class="tab-pane active" role="tabpanel" id="VendorWiseInv" style="overflow-x:scroll">
                                <asp:UpdatePanel ID="updtPnlGrid1" UpdateMode="Always" runat="Server">
                                    <ContentTemplate>
                                        <script type="text/javascript">
                                            jQuery(".main-table").clone(true).appendTo("#<%= tblgvVendorLoadItemdummy.ClientID %>").addClass('clone');
                                        </script>
                                        <table rules="all" border="1" id="tblgvVendorLoadItemdummy" runat="server" class="gvVendorLoadItemdummy fixed_header th">
                                            <tr class="click">
                                                <td>S.No</td>
                                                <td>Code</td>
                                                <td>Description</td>
                                                <td>Size</td>
                                                <td>W.Qty</td>
                                                <td>Qty</td>
                                                <td>Stock</td>
                                                <td>PRR</td>
                                                <td>MRP <input type="text" id ="txtmrp" style="width:100%;color: black;" onfocus="this.select();"  onkeydown="return fncItemBindwithmrpfiltertoTable(event,value);" /></td>
                                                <td>Selling</td>
                                                <td>BasicCost</td>
                                                <td>POQty</td>
                                                <td>TranInQty</td>
                                                <td>Pur-Date</td>
                                                <td>PurQty</td>
                                                <td>SoldQty</td>
                                                <td>PRPrice</td>
                                                <td>DiscAmt</td>
                                                <td>SGST</td>
                                                <td>CGST</td>
                                                <td>IGST</td>
                                                <td>NetCost</td>
                                                <td>CD</td>
                                                <td>SD</td>
                                                <td>Margin</td>
                                                <td>BarcodeQty</td>
                                                <td>CESSPrc</td>
                                                <td>CESSAmt</td>
                                                <td>UOMConv</td>
                                                <td>UOM</td>
                                                <%--<td style="text-align: center">WQty</td>--%>
                                            </tr>
                                        </table>
                                        <div class="GridDetails" id="gvItemLoad" style="overflow-x: hidden; overflow-y: scroll; height: 200px; width: 2583px; background-color: aliceblue;">
                                            <asp:GridView ID="gvVendorLoadItemList" runat="server" AutoGenerateColumns="False"
                                                ShowHeaderWhenEmpty="false" CssClass="pshro_GridDgn gvVendorLoadItemdummy myGrid" ShowHeader="false"
                                                OnRowDataBound="gvVendorLoadItemList_RowDataBound">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField DataField="RowNo" HeaderText="S.No" />
                                                    <asp:BoundField DataField="Inventorycode" HeaderText="Code"></asp:BoundField>
                                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                    <asp:BoundField DataField="Compatible" HeaderText="Size"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="W.Qty">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtWQty" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'WQty');"
                                                                CssClass="grid-textbox_right" onkeypress="isNumberKeyWithDecimalNew(event);" Text='<%# Eval("WQty") %>'
                                                                onfocus="fncWQtyOnFocus(this);" onchange="fncWholeQtyCal(this);"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQty" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'venItem');"
                                                                CssClass="grid-textbox_right" onkeypress="isNumberKeyWithDecimalNew(event);" Text='<%# Eval("InitQty") %>'
                                                                onfocus="fncGetSalesHistory(this);" onfocusout="fncRepeaterColumnValueChange('NewQty')"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Qtyonhand" HeaderText="Stock" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="PRReqQty" HeaderText="PRR" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="SellingPrice" HeaderText="Selling" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="CurrPrice" HeaderText="Cost" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="poQty" HeaderText="PO Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="AcceptQty" HeaderText="TranInQty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="PurDate" HeaderText="Pur-Date"></asp:BoundField>
                                                    <asp:BoundField DataField="purQty" HeaderText="Pur Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="SoldQty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField NullDisplayText="" HeaderText="PR Price" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="DiscountBasicAmt" HeaderText="Disc Amt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="ITaxPer1" HeaderText="SGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="ITaxPer2" HeaderText="CGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="ITaxPer3" HeaderText="IGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="NetCost" HeaderText="Net" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField NullDisplayText="" HeaderText="CD" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField NullDisplayText="" HeaderText="SD" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField NullDisplayText="" HeaderText="Margin" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField NullDisplayText="" HeaderText="Barcode Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="ITaxPer4" HeaderText="CESS Prc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="ITaxamt4" HeaderText="CESS Amt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="UomConv" HeaderText="UOMConv" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="UOM" HeaderText="UOM" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="PurchasedBy" HeaderText="Purchased By"></asp:BoundField>
                                                    <asp:BoundField DataField="VendorCatalog" HeaderText="VendorCatalog" ItemStyle-CssClass="hidden"></asp:BoundField>
                                                    <%--<asp:BoundField DataField="WQty" HeaderText="WQty" ItemStyle-CssClass="text-right"></asp:BoundField>--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="ViewAddInv">
                                <asp:UpdatePanel ID="updtPnlGrid2" UpdateMode="Conditional" runat="Server" style="overflow-x:scroll">
                                    <ContentTemplate>
                                        <table rules="all" border="1" id="Table1" runat="server" class="gvSelectItemListdummy fixed_header">
                                            <tr class="backClr_chocolate">
                                                <td scope="col">Delete</td>
                                                <td scope="col">S.No</td>
                                                <td scope="col">Code</td>
                                                <td scope="col">Description</td>
                                                <td scope="col">Size</td>
                                                <td scope="col">Qty</td>
                                                <td scope="col">Foc</td>
                                                <td scope="col">MRP</td>
                                                <td scope="col">Selling</td>
                                                <td id="thCost1" scope="col">Basic Cost</td>
                                                <td id="thDiscAmt1" scope="col">Disc Amt</td>
                                                <td id="thODisc1" scope="col">O.Disc(%)</td>
                                                <td id="thODiscAmt1" scope="col">O.DiscAmt</td>
                                                <td id="thGross1" scope="col">Gross Cost</td>
                                                <td id="thSGST1" scope="col">SGST</td>
                                                <td id="thCGST1" scope="col">CGST</td>
                                                <td id="thIGST1" scope="col">IGST</td>
                                                <td id="thNet1" scope="col">Net Cost</td>
                                                <td id="thTotal1" scope="col">Total</td>
                                                <td id="thSGSTAmt1" scope="col">SGST Amt</td>
                                                <td id="thCGSTAmt1" scope="col">CGST Amt</td>
                                                <td id="thIGSTAmt1" scope="col">IGST Amt</td>
                                                <td id="thChecked1" scope="col">Checked</td>
                                                <td id="thPoQty1" scope="col">Po Qty</td>
                                                <td id="thCESSPrc1" scope="col">CESS Prc</td>
                                                <td id="thCESSAmt1" scope="col">CESS Amt</td>
                                                <td id="thUOM" scope="col">UOM</td>
                                                <td id="thWQty" scope="col">W.Qty</td>
                                            </tr>
                                        </table>
                                        <div class="GridDetails" id="gvAddItem" style="overflow-x: hidden; overflow-y: scroll; height: 200px; width: 2198px; background-color: aliceblue;">
                                            <asp:GridView ID="gvSelectItemList" runat="server" AllowSorting="true" ShowHeader="false"
                                                AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn gvSelectItemListdummy "
                                                OnRowDeleting="gvSelectItemList_OnRowDeleting">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Delete" />
                                                    <%--1--%>
                                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                                    <%--2--%>
                                                    <asp:BoundField DataField="InventoryCode" HeaderText="Code"></asp:BoundField>
                                                    <%--3--%>
                                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                    <%--4--%>
                                                    <asp:BoundField DataField="Size" HeaderText="Size"></asp:BoundField>
                                                    <%--5--%>
                                                    <%-- <asp:BoundField DataField="Qty" HeaderText="Qty" ItemStyle-CssClass="text-right"></asp:BoundField>--%>
                                                    <asp:TemplateField HeaderText="Qty">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQty" runat="server" Text='<%# Eval("Qty") %>' ReadOnly="true" onkeydown="return fncSetFocustoNextRow(event,this,'AddItem');" onkeypress="isNumberKeyWithDecimalNew(event);"
                                                                CssClass="grid-textbox_right" onchange="fncRepeaterColumnValueChange('AddQty');"></asp:TextBox><%--6--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Foc" HeaderText="Foc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--7--%>
                                                    <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--8--%>
                                                    <asp:BoundField DataField="SellingPrice" HeaderText="Selling" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--9--%>
                                                    <asp:BoundField DataField="Cost" HeaderText="Cost" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--10--%>
                                                    <asp:BoundField DataField="DisAmt" HeaderText="Disc Amt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--11--%>
                                                    <asp:BoundField DataField="ODiscPRC" HeaderText="O.Disc(%)" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--12--%>
                                                    <asp:BoundField DataField="ODiscAmt" HeaderText="O.DiscAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--13--%>
                                                    <asp:BoundField DataField="Gross" HeaderText="Gross" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--14--%>
                                                    <asp:BoundField DataField="SGSTPrc" HeaderText="SGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--15--%>
                                                    <asp:BoundField DataField="CGSTPrc" HeaderText="CGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--16--%>
                                                    <asp:BoundField DataField="IGSTPrc" HeaderText="IGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--17--%>
                                                    <asp:BoundField DataField="Net" HeaderText="Net" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--18--%>
                                                    <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--19--%>
                                                    <asp:BoundField DataField="SGSTAmt" HeaderText="SGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--20--%>
                                                    <asp:BoundField DataField="CGSTAmt" HeaderText="CGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--21--%>
                                                    <asp:BoundField DataField="IGSTAmt" HeaderText="IGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--22--%>
                                                    <asp:BoundField DataField="Checked" HeaderText="Checked"></asp:BoundField>
                                                    <%--23--%>
                                                    <asp:BoundField DataField="PoQty" HeaderText="PoQty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--<asp:BoundField DataField="AcceptQty" HeaderText="TranInQty" ItemStyle-CssClass="text-right"></asp:BoundField>--%>
                                                    <%--24--%>
                                                    <asp:BoundField DataField="CESSPrc" HeaderText="CESS Prc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--25--%>
                                                    <asp:BoundField DataField="CESSAmt" HeaderText="CESS Amt" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                    <%--26--%>
                                                    <asp:BoundField DataField="UOM" HeaderText="UOM" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <asp:BoundField DataField="WQty" HeaderText="WQty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                    <%--27--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="updtPnlPOPending" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div id="dialog-POPending" title="PO Pending Details" style="display: none">
                            <div class="grid-popup">
                                <asp:GridView ID="gvPoPending" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                    CssClass="pshro_GridDgn">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="PoNo" HeaderText="Po No"></asp:BoundField>
                                        <asp:BoundField DataField="PoDate" HeaderText="Po Date"></asp:BoundField>
                                        <asp:BoundField DataField="PoTotalValue" HeaderText="Po Total Value"></asp:BoundField>
                                        <asp:BoundField DataField="Process" HeaderText="Po Process"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="dialog-LastFocDetail" title="Last Foc Detail" style="display: none">
                            <div class="grid-popup">
                                <asp:GridView ID="gvLastFocDetail" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                    CssClass="pshro_GridDgn">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="ItemCode" HeaderText="Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="Lqty" HeaderText="Lqty"></asp:BoundField>
                                        <asp:BoundField DataField="FOCItemCode" HeaderText="FOC Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="FOCQty" HeaderText="FOC Qty"></asp:BoundField>
                                        <asp:BoundField DataField="GIDNo" HeaderText="GID No"></asp:BoundField>
                                        <asp:BoundField DataField="GIDDate" HeaderText="GID Date"></asp:BoundField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <div class="float_left">
                    <div id="divItemhistory">
                        <div class="display_table">
                            <div class="po_saleshistory">
                                <asp:Label ID="code" runat="server"></asp:Label>
                            </div>
                            <div class="po_saleshistory">
                                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control-res border_none" Style="width: 379px;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Payment_fixed_headers ReGeneratePO_Itemhistory PO_Itemhistory" style="width: 550px;">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Month
                                        </th>
                                        <th scope="col">1st Week
                                        </th>
                                        <th scope="col">2nd Week
                                        </th>
                                        <th scope="col">3rd Week
                                        </th>
                                        <th scope="col">4th Week
                                        </th>
                                        <th scope="col">5th Week
                                        </th>
                                        <th scope="col">6th Week
                                        </th>
                                        <th scope="col">Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div id="locWiseSaleshis">
                        <div class="display_table">
                            <div class="po_saleshistory">
                                <asp:Label ID="loc_Code" runat="server"></asp:Label>
                            </div>
                            <div class="po_saleshistory">
                                <%-- <asp:Label ID="loc_Name" runat="server"></asp:Label>--%>
                                <asp:TextBox ID="txtlocName" runat="server" CssClass="form-control-res border_none"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Payment_fixed_headers ReGeneratePO_Itemhistory PO_Itemhistory" style="width: 510px;">
                            <table id="tblLocwiseSaleshistory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Location
                                        </th>
                                        <th scope="col">Month
                                        </th>
                                        <th scope="col">1st Week
                                        </th>
                                        <th scope="col">2st Week
                                        </th>
                                        <th scope="col">3st Week
                                        </th>
                                        <th scope="col">4st Week
                                        </th>
                                        <th scope="col">5st Week
                                        </th>
                                        <th scope="col">6st Week
                                        </th>
                                        <th scope="col">Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="divLastFoc" class="float_left">
                    <div class="display_table">
                        <div class="po_saleshistory">
                            <asp:Label ID="lblFOcItem" runat="server"></asp:Label>
                        </div>
                        <div class="po_saleshistory">
                            <asp:TextBox ID="txtFocItem" runat="server" CssClass="form-control-res border_none" Style="width: 341px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Payment_fixed_headers lastFocDet PO_Itemhistory" style="width: 425px;">
                        <table id="tblLastFOC" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Itemcode
                                    </th>
                                    <th scope="col">Qty
                                    </th>
                                    <th scope="col">F.Itemcode
                                    </th>
                                    <th scope="col">F.Qty
                                    </th>
                                    <th scope="col">GID No
                                    </th>
                                    <th scope="col">GID Date
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="divPoPending" class="float_left">
                    <div class="display_table">
                        <div class="po_saleshistory">
                            <asp:Label ID="lblPOpendingItem" runat="server"></asp:Label>
                        </div>
                        <div class="po_saleshistory">
                            <asp:TextBox ID="txtPOPenCode" runat="server" CssClass="form-control-res border_none" Style="width: 270px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Payment_fixed_headers POItemwisePending PO_Itemhistory" style="width: 404px;">
                        <table id="tblPOItemwisePending" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">PO No
                                    </th>
                                    <th scope="col">PO Date
                                    </th>
                                    <th scope="col">Item code
                                    </th>
                                    <th scope="col">PO Qty
                                    </th>
                                    <th scope="col">PO R.Qty
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div id="divItemadd">
                <asp:Panel runat="server" ID="pnlItemadd">
                    <asp:UpdatePanel ID="updtPnlButton1" UpdateMode="Conditional" runat="Server">
                        <ContentTemplate>
                            <div class="bottom-purchase-container-add gid_top_border" id="addInv">
                                <div class="bottom-purchase-container-add-Text">
                                    <div class="control-group-split">
                                        <div class="container7">
                                            <div class="label-left">
                                                <asp:Label ID="Label6" runat="server" Text="Code"></asp:Label>
                                                <asp:Label ID="lblBatch" runat="server" Style="color: white; padding: 2px;"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtItemCodeAdd" runat="server" CssClass="form-control-res"
                                                    onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemCodeAdd', 'txtWQty');"></asp:TextBox>
                                                <asp:HiddenField ID="hfInvCode" runat="server" />
                                            </div>
                                        </div>
                                        <div class="PO_container3">
                                            <div class="label-left">
                                                <asp:Label ID="Label7" runat="server" Text="ItemDesc"></asp:Label>
                                                <asp:Label ID="lblPurchaseType" runat="server" Style="color: white; font-weight: normal; padding: 2px;"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label14" runat="server" Text="W.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtWQty" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)" onchange="fncWQtyCal();" onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label26" runat="server" Text="L.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label27" runat="server" Text="Foc"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtFoc" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label15" runat="server" Text="N.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNQty" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label16" runat="server" Text="MRP"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMrp" runat="server" Text="0.00" CssClass="form-control-res-right"
                                                    onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown=" return fncPurchaseKeyPress(event,'MRP'); " onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label17" runat="server" Text="BasicCost"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtBasicCost" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select()"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2" style="display: none;">
                                            <div class="label-left">
                                                <asp:Label ID="Label18" runat="server" Text="Dis(%)"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtDiscPrc" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label19" runat="server" Text="Dis Amt"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtDisAmt" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)" Text="0.00"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label20" runat="server" Text="S.D(%)"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtSDPrc" runat="server" Text="0.00" CssClass="form-control-res-right"
                                                    onkeypress="return isNumberKeyWithDecimalNew(event)" onkeydown=" return fncPurchaseKeyPress(event,'SDprc');"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label21" runat="server" Text="SD Amt"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtSDAmt" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label22" runat="server" Text="G.Cost"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtGrossCost" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label34" runat="server" Text="AddCess"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtAddCess" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label23" runat="server" Text="Net Cost"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNet" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label28" runat="server" Text="Sell Price"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtSellingPrice" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label24" runat="server" Text="Net Total"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNetTotalAdd" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-purchase-container-add-Text">
                                    <div class="bottom-purchase-container-blns">
                                        <div class="control-group-split">
                                            <asp:Panel ID="pnlCGST" runat="server">
                                                <div class="container2">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label25" runat="server" Text="L.Uom"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtLUOM" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="container2">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label13" runat="server" Text="W.Uom"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtWUOM" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="PO_container1">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label11" runat="server" Text="SGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtSGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfSGSTCode" runat="server" />
                                                        <asp:HiddenField ID="hidSave" runat="server" Value="Show" />
                                                    </div>
                                                </div>
                                                <div class="PO_container1">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label36" runat="server" Text="CGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfCGSTCode" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="PO_container1">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label45" runat="server" Text="SGSTAmt"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtSGSTAmt" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hidSgstAmt" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="PO_container1">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label46" runat="server" Text="CGSTAmt"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCGSTAmt" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hidCGSTAmt" runat="server" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlIGST" runat="server" Style="display: none;">
                                                <div class="PO_container1">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label5" runat="server" Text="IGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtIGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfIGSTCode" runat="server" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <div class="PO_container1">
                                                <div class="label-left">
                                                    <asp:Label ID="Label37" runat="server" Text="CESS(%)"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtCESS" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 25%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label8" runat="server" Text="Remarks"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 100%;">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="PO_container1" id="divDisc1">
                                                <div class="label-left">
                                                    <asp:Label ID="Label40" runat="server" Text="Disc1%"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDisc1Per" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="PO_container1" id="divDisc2">
                                                <div class="label-left">
                                                    <asp:Label ID="Label41" runat="server" Text="Disc2%"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDisc2Per" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="PO_container1" id="divDisc3">
                                                <div class="label-left">
                                                    <asp:Label ID="Label42" runat="server" Text="Disc3%"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDisc3Per" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="PO_container1" id="divMDown">
                                                <div class="label-left">
                                                    <asp:Label ID="Label43" runat="server" Text="MDown%"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtMDownPer" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="PO_container1" id="divAdMDown">
                                                <div class="label-left">
                                                    <asp:Label ID="Label44" runat="server" Text="Ad.MDown%"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtAddMDown" runat="server" CssClass="form-control-res-right"
                                                        onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hdfTaxType" runat="server" />
                                    <asp:HiddenField ID="hidsort" runat="server" Value="Sort" />
                                    <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                        <div class="control-button" id="divInsertImage" runat="server" style="display: none">
                                            <asp:LinkButton ID="lnkInsertImage" runat="server" class="button-link btn-img" OnClientClick="fncOpenImageScreen();return false;">Insert Image</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkAddInv" runat="server" class="button-blue" OnClientClick="return AddbuttonClick()"
                                                OnClick="lnkAddInv_Click" Text="Add"></asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearAdd" runat="server" Text="Clear" class="button-blue" OnClientClick="fncClearEntervalue();return false;"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-purchase-container-total gid_top_border">
                                <div class="control-group-split">
                                    <div class="control-group-left" style="width: 8%">
                                        <div class="label-left" style="width: 50%">
                                            <asp:Label ID="Label29" runat="server" Text="Items"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtItems" runat="server" Text="0" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left" style="width: 8%">
                                        <div class="label-left" style="width: 35%; padding-left: 5px;">
                                            <asp:Label ID="Label30" runat="server" Text="Qty"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 65%">
                                            <asp:TextBox ID="txtQty" runat="server" Text="0" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol " style="width: 12%">
                                        <div class="label-left" style="width: 40%">
                                            <asp:Label ID="Label2" runat="server" Text="Total"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 60%">
                                            <asp:TextBox ID="txtTotal" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol" style="width: 15%">
                                        <div class="label-left" style="width: 70%">
                                            <asp:Label ID="Label31" runat="server" Text="Discount By%"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 28%">
                                            <asp:TextBox ID="txtTotalDiscPrc" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"
                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol" style="width: 15%">
                                        <div class="label-left" style="width: 50%">
                                            <asp:Label ID="Label32" runat="server" Text="By Rs"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtTotalDiscRs" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_txt3">
                                            <asp:Label ID="lblTaxableAmt" runat="server" Text="TaxableAmt"></asp:Label>
                                        </div>
                                        <div class="grn_net_txt3">
                                            <asp:TextBox ID="txtTaxableAmt" runat="server" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_lbl2">
                                            <asp:LinkButton ID="lnkGST" runat="server" Text="GST(F11)" OnClientClick="fncInitializeVetDetailDialog();return false;"></asp:LinkButton>
                                        </div>
                                        <div class="grn_net_txt2">
                                            <asp:TextBox ID="txtGST" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_lbl2">
                                            <asp:Label ID="Label33" runat="server" Text="Net Total"></asp:Label>
                                        </div>
                                        <div class="grn_net_txt2">
                                            <asp:TextBox ID="txtSubTotal" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_txt3">
                                            <asp:Label ID="Label38" runat="server" Visible="false" Text="Discount(%)"></asp:Label>
                                        </div>
                                        <div class="grn_net_txt3">
                                            <asp:TextBox ID="txtDiscountPerc" Visible="false" onchange="fncDiscountAmtCal('DisPrc');" runat="server" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_txt3">
                                            <asp:Label ID="Label39" runat="server" Visible="false" Text="Discount(Rs)"></asp:Label>
                                        </div>
                                        <div class="grn_net_txt3">
                                            <asp:TextBox ID="txtDiscountAmt" runat="server" Visible="false" onchange="fncDiscountAmtCal('DisAmt');" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="grn_net_value3 po_NetPadding">
                                        <div class="grn_net_lbl2">
                                            <asp:Label ID="Label35" Visible="false" runat="server" Text="Net Total"></asp:Label>
                                        </div>
                                        <div class="grn_net_txt2">
                                            <asp:TextBox ID="txtNetTotal" Visible="false" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
            <asp:UpdatePanel ID="updtPnlButton2" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div>
                        <div class="float_left">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkBarcode" runat="server" class="button-blue"
                                    Text="Barcode(F10)" OnClientClick="fncBarcodeQtyValidation();return false;"></asp:LinkButton>
                            </div>
                        </div>

                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return Validate()"
                                    OnClick="lnkSave_Click" Text="Save(F4)"></asp:LinkButton>       
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-blue"
                                    Text="Clear(F6)" OnClick="lnkClear_Click"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkViewPo" runat="server" class="button-blue" Text="PO List(F8)" PostBackUrl="~/Purchase/frmPurchaseOrderView.aspx"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClick="lnkClose_Click" Text="Close"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="updtPnlButton2">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

        </div>
    </div>
    <div class="hiddencol">
        <div id="divVatDetail">
            <div class="Payment_fixed_headers gid_vetDetail">
                <table id="tblVetDetail" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">GST (%)
                            </th>
                            <th scope="col">CESS (%)
                            </th>
                            <th scope="col">Taxable
                            </th>
                            <th scope="col" id="vatSummerySGST">SGST
                            </th>
                            <th scope="col" id="vatSummeryCGST">CGST
                            </th>
                            <th scope="col">CESS
                            </th>
                            <th scope="col">Total Pur.Value
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr class="Paymentfooter_Color">
                            <td class="paymentfooter_total">Total</td>
                            <td class="paymentfooter_total" style="font-size: 13px; font-weight: bold;"></td>
                            <td class="paymentfooter_total" style="font-size: 13px; font-weight: bold;"></td>
                            <td class="paymentfooter_total" style="font-size: 13px; font-weight: bold;">
                                <asp:Label ID="lblTotalTaxablePurValue" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_total" style="font-size: 13px; font-weight: bold;">
                                <asp:Label ID="lblTotalTaxAmt1" runat="server" Text="0.00" />
                            </td>
                            <td id="vatSummaryCGST" class="paymentfooter_total" style="font-size: 13px; font-weight: bold;">
                                <asp:Label ID="lblTotalTaxAmt2" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_total" style="font-size: 13px; font-weight: bold;">
                                <asp:Label ID="lblTotalCessAmt" runat="server" Text="0.00" />
                            </td>
                            <td class="paymentfooter_balancetotal">
                                <asp:Label ID="lblTotalPurValue" runat="server" Text="0.00" />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div id="divBarcode">
            <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl" OnPrintButtonClick="lnkbtnPrint_Click"
                OnPrintButtonClientClick="return fncBarcodeValidation(); " />
        </div>

        <div id="divVendorcatelogAlert" class="price_alert_gid"> 
            <div> 
                <div class="gid_pricechange_alert">
                    <asp:Label ID="lblOldCost" runat="server" Text='Cat.Mrp'></asp:Label>
                    <asp:TextBox ID="txtCatMrp" runat="server" CssClass="form-control-res-right" ReadOnly="true" onkeydown="return fncClosePriceAlert(event);"></asp:TextBox>
                </div>
                <div class="gid_pricechange_alert">
                    <asp:Label ID="lblOldMrp" runat="server" Text='Cat.BasicCost'></asp:Label>
                    <asp:TextBox ID="txtCatCost" runat="server" CssClass="form-control-res-right" ReadOnly="true" onkeydown="return false;"></asp:TextBox>
                </div>
                <div class="gid_pricechange_alert">
                    <asp:Label ID="lblOldSelling" runat="server" Text='Cat.NetCost'></asp:Label>
                    <asp:TextBox ID="txtCatNetCost" runat="server" CssClass="form-control-res-right" ReadOnly="true" onkeydown="return false;"></asp:TextBox>
                </div>
                <div class="gid_pricechange_alert">
                    <asp:Label ID="lbloldMarginfixed" runat="server" Text='Cat.SellingPrice'></asp:Label>
                    <asp:TextBox ID="txtCatSellPrice" runat="server" CssClass="form-control-res-right" ReadOnly="true" onkeydown="return false;"></asp:TextBox>
                </div> 
                 <div class="col-md-1" style="margin-top:15px;"> 
                    <button id="btnAdd" class ="btn-danger" onclick="fncVendorCatlogClick();return false;">Add</button> 
                </div>
                 <div class="col-md-1" style="margin-top:15px;"> 
                    <button id="btnClose" class ="btn-danger" onclick="fncVenCatClose();return false;">Close</button> 
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hidWQtyCon" runat="server" Value="0" />
        <asp:HiddenField ID="hdfTabName" runat="server" />
        <asp:HiddenField ID="hdfItemCode" runat="server" />
        <asp:HiddenField ID="hdfAddItemCode" runat="server" />
        <asp:HiddenField ID="hidDiscontinueEntry" runat="server" Value="" />
        <asp:HiddenField ID="hidSaveClick" runat="server" Value="false" />
        <asp:HiddenField ID="hidVendorCode" runat="server" Value="" />


        <div class="display_none">
            <div id="PODisContinueEntry" class="display_none">
                <div>
                    <asp:Label ID="lblSavemsg" runat="server"></asp:Label>
                </div>
                <div class="float_right">
                    <div class="inv_dailog_btn">
                        <asp:LinkButton ID="lnkNewItemYes" runat="server" Text="Yes" class="button-blue-withoutanim" OnClientClick="fncYesClick_master();return false;"></asp:LinkButton>
                    </div>
                    <div class="inv_dailog_btn">
                        <asp:LinkButton ID="lnkNewItemNo" runat="server" Text="No" class="button-blue-withoutanim" OnClientClick="fncNoClick();return false;"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="dialog-Vendor" style="display: none">
                <div>
                    <div class="float_left">
                        <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                    </div>
                    <div class="gidItem_Search">
                        <asp:TextBox ID="txtVendorSearch" runat="server" class="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="vendor_Searchaddress">
                    <asp:TextBox ID="txtVendorAddress" runat="server" TextMode="MultiLine"
                        class="form-control-res vendor_Searchaddress_height"></asp:TextBox>
                </div>

            </div>
        </div>
             <asp:Button ID="btnsave" runat="server" style="display:none"  OnClick="btn_save_Click" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <asp:Button ID="btnVendorchange" runat="server" OnClick="btnVendorchange_click" />
                <asp:Button ID="btnLoadDiscontinueEntry" runat="server" OnClick="btnLoadDiscontinueEntry_click" />
                <asp:Button ID="btnAddGrid" runat="server" OnClick="btnAddGrid_Click" />
                <asp:HiddenField ID="venListTab" runat="server" Value="1" />
                <asp:HiddenField ID="hidZeroQty" runat="server" Value="1" />
                <asp:HiddenField ID="hidVendor" runat="server" Value="1" />
                <asp:Button ID="btnAsending" runat="server" OnClick="btnAsendinge_click" />
                <asp:Button ID="btnDescending" runat="server" OnClick="btnDescending_click" />
                <asp:Button ID="btnPoLoadAse" runat="server" OnClick="btnAsendingePO_click" />
                <asp:Button ID="btnPoLoaddes" runat="server" OnClick="btnDescendingPO_click" />
                
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" /> 
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidSearchMRP" runat="server" />
    <asp:HiddenField ID="hidPo" runat="server" value=""/>
    <asp:hiddenField ID="hidEdit" runat="server" />
<%--    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="150px" Style="visibility: hidden"
                        OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>--%>
    <asp:Button  ID="btnsearchgrid" runat="server" OnClick="lnkSearchGrid_Click" Style="visibility: hidden"/>
</asp:Content>
