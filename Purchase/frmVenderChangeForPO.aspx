﻿<%@ Page Title="Vender Change For PO" Language="C#" AutoEventWireup="true" MasterPageFile="~/MainMaster.master" CodeBehind="frmVenderChangeForPO.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmVenderChangeForPO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'VenderChangeForPO');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "VenderChangeForPO";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>



    <script type="text/javascript">


        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };


        function ValidateForm() {
            var Show = '';
            if (document.getElementById("<%=txtPONo.ClientID%>").value == '') {
                //Show = Show + '\n  Please Select Po Number';
                fncToastInformation('Please Select Po Number');

                document.getElementById("<%=txtPONo.ClientID %>").focus();
                return false;
            }
            else if (document.getElementById("<%=txtNewVendor.ClientID%>").value == '') {
                //Show = Show + '\n  Please Select New Vendor';
                fncToastInformation('Please Select New Vendor');
                document.getElementById("<%=txtNewVendor.ClientID %>").focus();
                return false;
            }
            else {
                fncConfirm();
            }

    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkUpdate.ClientID %>').css("display", "none");
              }
              $("select").chosen({ width: '100%' });
          }
          function clearForm() {

              $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
              $(':checkbox, :radio').prop('checked', false);
              $('#<%=txtPONo.ClientID %>').val('');
            $('#<%=txtNewVendor.ClientID %>').val('');

        }
        function fncGetVendorDetail() {

            var obj = {};
            try {
                obj.pono = $('#<%=txtPONo.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Purchase/frmVenderChangeForPO.aspx/fncGetVendorDetail") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        var objItem = jQuery.parseJSON(msg.d);

                        if (objItem.length > 0) {
                            // alert('Check');
                            $("[id*=txtPoVendorName]").val(objItem[0]["VendorName"]);
                            $("[id*=txtPoVendorCode]").val(objItem[0]["VendorCode"]);
                        }
                    },
                    error: function (data) {

                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        };

        function funGetNewVendor() {

            try {
                $('#<%=txtNewVendorName.ClientID %>').val($('#<%=txtNewVendor.ClientID %>').val());
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncConfirm() {
            $(function () {
                $("#dialog-confirm").html('Do you want Update?');
                $("#dialog-confirm").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Yes: function () {
                            $(this).dialog("destroy");
                            $('#<%=btnUpdate.ClientID %>').click();
                            //fncCheckPono.call(this);
                            //btnUpdate
                        },
                        No: function () {
                            $(this).dialog("destroy");
                        }
                    },
                    modal: true
                });
            });
        };



        function fncSetValue() {
            try {
                if (SearchTableName == "PoNo") {
                    fncGetVendorDetail();
                }
                if (SearchTableName == "Vendor") {
                    funGetNewVendor();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="frmMain.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Vender Change For PO</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div style="padding: 10px 20px 0px 35px">
                    <div class="container-group-small">
                        <div class="container-control">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Po No"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <%--<asp:DropDownList ID="ddlpono" runat="server" onchange="fncGetVendorDetail();"  readonly = 'true'
                                        CssClass="form-control-res" 
                                        >
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtPONo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PoNo', 'txtPONo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Po Vendor Name"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPoVendorName" runat="server" ReadOnly='true' CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Po Vendor Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPoVendorCode" runat="server" ReadOnly='true' CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>


                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label9" runat="server" Text="New Vendor"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <%--<asp:DropDownList ID="ddlNewVendor" runat="server" readonly = 'true' onchange="funGetNewVendor();"
                                        CssClass="form-control-res" 
                                         >
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtNewVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtNewVendor', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="New Vendor Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtNewVendorName" runat="server" ReadOnly='true' CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>


                        </div>
                        <div class="button-contol" style="margin-left: 175px">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkUpdate" runat="server" class="button-red"
                                    OnClientClick=" ValidateForm(); return false;">Update</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm();return false;"
                                    Text='<%$ Resources:LabelCaption,lnkClear %>'></asp:LinkButton>
                            </div>

                        </div>

                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="display_none">
            <asp:Button ID="btnUpdate" runat="server" Text="Button" OnClick="btnUpdate_Click" />
            <div id="dialog-confirm">
            </div>
        </div>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>

