﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmJobOrder.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmJobOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4) {
            min-width: 140px;
            max-width: 140px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7) {
            min-width: 240px;
            max-width: 240px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8) {
            min-width: 240px;
            max-width: 240px;
            text-align:center;
        }
        .Barcode_fixed_headers th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
            text-align:center;
        }
        .Barcode_fixed_headers td:nth-child(9) {
        min-width: 100px;
            max-width: 100px;
            text-align:right;
        }
        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10) {
            min-width: 250px;
            max-width: 250px;
            text-align:center;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'JobOrder');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "JobOrder";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


     <script type="text/javascript">
         $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
             $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
             $(document).on('keydown', disableFunctionKeys);
         });
         //Initialize Job Oredr Delete Dialog
         function fncInitializeJobOrderDeleteDialog() {
    try {
        var Msg;
        Msg = "Are you sure want to Delete this order.?";
        $('#<%=lblDeleteDialog.ClientID %>').text(Msg);
        $("#divJobOrderDelete-Dialog").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Order Complete Dialog
         function fncJobOrderDeleteDialogClose() {
            $("#divJobOrderDelete-Dialog").dialog('close');
        }
         function disableFunctionKeys(e) {
             try {
                 var charCode = (e.which) ? e.which : e.keyCode;
                 var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                 if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                     if (e.keyCode == 118) {//F6 Refresh
                         __doPostBack('ctl00$ContentPlaceHolder1$lnkLoad', '')
                         e.preventDefault();
                     }
                     else if (e.keyCode == 116) {//F5 for Add Button
                         document.getElementById('<%= lnkAdd.ClientID %>').click()
                         e.preventDefault();
                     }
                     else if (e.keyCode == 113) {//F3 for Fetch Button
                         __doPostBack('ctl00$ContentPlaceHolder1$lnkFetch','')
                         e.preventDefault();
                     }
                 }

             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         </script>
    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
        }
        function fncSetValue() {
            try{
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncJobOrderEdit(source) {
            try {
                var rowEditobj,OrderId;
                rowEditobj = $(source).parent().parent();
                OrderId = $.trim(rowEditobj.find('td span[id*="lblOrderId"]').text());
                $('#<%=hdOrderId.ClientID %>').val(OrderId);
                
                //window.location = "./frmJobOrderMnt.aspx?OrderId=" + OrderId;
                //window.open('../Reports/frmSalesReportFilters.aspx?ReportName=' + reportname);
                //alert(OrderId);
                //fncGADeleteAndEditNew(rowDeleteobj, btnvalue);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncJobOrderDelete(source)
        {
            try {
                debugger;
                var rowEditobj, OrderId;
                rowEditobj = $(source).parent().parent();
                OrderId = $.trim(rowEditobj.find('td span[id*="lblOrderId"]').text());
                $('#<%=hdOrderId.ClientID %>').val(OrderId);
                <%--if ($('#<%=hdDeleteOrder.ClientID %>').val()=='Yes') {
                    fncInitializeJobOrderDeleteDialog();
                }
                else {
                    ShowPopupMessageBox('Pending Orders Only allow to Delete..!');
                }--%>
                if ($("#<%=rbtPending.ClientID %>").is(':checked')) {
                    fncInitializeJobOrderDeleteDialog();
                }
                else {
                    ShowPopupMessageBox('Pending Orders Only allow to Delete..!');
                }
                
                <%--$("#<%=btnDelete.ClientID %>").click();--%>
                //alert(OrderId);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Job Order </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="purchase-order-header">
                        <div>
                            <div class="control-group-split float_left" style="width: 50%; clear: both">
                                <div class="control-group-middle" style="width:48%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label38" runat="server" Text="Order From"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtOrderFrom" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Member',  'txtOrderFrom', 'txtOrderTo');" CssClass="form-control-res" ></asp:TextBox> <%--onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"--%>
                                    </div>
                                </div>
                                <div class="control-group-middle" style="width:48%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label40" runat="server" Text="Order To"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtOrderTo" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor',  'txtOrderTo', '');" runat="server" CssClass="form-control-res" ></asp:TextBox> <%--onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"--%>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split float_left" style="width: 50%;">
                                
                                    <div class="control-group-middle" style="width:48%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label26" runat="server" Text="From Date"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                <div class="control-group-middle" style="width:48%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label32" runat="server" Text="To Date"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                   
                                </div>
                        </div>
                        <div class="control-container" style="float: left; clear: both; display: table; width: 50%">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" PostBackUrl="~/Purchase/frmJobOrderMnt.aspx" Text="Add(F5)"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkLoad" runat="server" class="button-blue" OnClick="lnkLoad_Click" Text="Refresh(F7)"></asp:LinkButton> <%--OnClick="lnkLoad_Click"--%>
                            </div>
                            <div class="control-button PO_Summary_border">
                                <asp:RadioButton ID="rbtPending" runat="server" AutoPostBack="true" OnCheckedChanged="rbtPending_CheckedChanged" Checked="true" Text="Pending" GroupName="JobOrder" />
                                <asp:RadioButton ID="rbtReceived" runat="server" AutoPostBack="true" OnCheckedChanged="rbtReceived_CheckedChanged" Text="Received" GroupName="JobOrder" />
                                <asp:RadioButton ID="rbtDeleted" runat="server" AutoPostBack="true" OnCheckedChanged="rbtDeleted_CheckedChanged" Text="Deleted" GroupName="JobOrder" />
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkPrintList" runat="server" OnClick="lnkPrintListReport_Click" class="button-blue"  Text="Print List"></asp:LinkButton> <%--OnClick="lnkLoad_Click"--%>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" Text="Fetch(F2)"></asp:LinkButton> <%--OnClick="lnkLoad_Click"--%>
                            </div>
                        </div>
                        </div>
                    <div id="divRepeater" class="Barcode_fixed_headers">
                     <%--<div class="purchase-order-detail over_flowhorizontal">--%>
                         <asp:UpdatePanel ID="uprptrJobOrder" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                         <table rules="all" border="1" id="tblJobOrderDetails">
                             <thead>
                            <tr>
                                
                                <th scope="col">S.No</th>
                                <th scope="col" runat="server" id="thEdit">Edit</th>
                                <th scope="col" runat="server" id="thDelete">Delete</th>
                                <th scope="col">Order ID</th>
                                <th scope="col">Order Date</th>
                                <th scope="col">Delivery Date</th>
                                <th scope="col">Order From</th>
                                <th scope="col">Order To</th>
                                <th scope="col">Total Value</th>
                                <th scope="col">Remarks</th>
                            </tr>
                                 </thead>
                             <asp:Repeater ID="rptrJobOrder" runat="server" OnItemDataBound="rptrJobOrder_ItemDataBound"><%--OnItemDataBound="rptrGA_ItemDataBound"--%>
                                <HeaderTemplate>
                                    <tbody id="BulkBarcodebody">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="gaRow" tabindex='<%#(Container.ItemIndex)%>' runat="server" onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNo") %>' />
                                        </td>
                                        <td runat="server" id="tdEdit">
                                            <asp:ImageButton ID="btnImgEdit" runat="server" OnClick="imgEdit_Click" ImageUrl="~/images/edit-icon.png"
                                                OnClientClick="fncJobOrderEdit(this)" />
                                        </td>
                                        <td runat="server" id="tdDelete">
                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png" OnClientClick="fncJobOrderDelete(this);return false;" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrderId" runat="server" Text='<%# Eval("OrderId") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrderDate" runat="server" Text='<%# Eval("JobOrderDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDeliveryDate" runat="server" Text='<%# Eval("JobDeliveryDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrderFrom" runat="server" Text='<%# Eval("OrderFrom") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrderTo" runat="server" Text='<%# Eval("OrderTo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotalValue" runat="server" Text='<%# Eval("TotalValue") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>
                         </div>
                        </ContentTemplate>
                             </asp:UpdatePanel>
                        <%--</div>--%>

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="hiddencol">
            <div id="divJobOrderDelete-Dialog">
                <div>
                    <asp:Label ID="lblDeleteDialog" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                <div class="PaymentDialog_yes">
                    <asp:UpdatePanel ID="upJobOrderDelete" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkJobOrderDeleteOk" runat="server" class="button-blue" Text="Yes"
                                OnClientClick="fncJobOrderDeleteDialogClose()" OnClick="lnkDelete_Click"> </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkJobOrderDeleteNo" runat="server" class="button-blue" Text='No'
                            OnClientClick="fncJobOrderDeleteDialogClose();return false;"> </asp:LinkButton>
                    </div>
</div>

            </div>
            <%--<asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" />--%>
            <asp:HiddenField ID="hdOrderId" runat="server" />
            <%--<asp:HiddenField ID="hdDeleteOrder" runat="server" />--%>
            </div>
    </div>
</asp:Content>
