﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAbstractPayment.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmAbstractPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8) {
            min-width: 150px;
            max-width: 150px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9) {
            min-width: 130px;
            max-width: 130px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11) {
            min-width: 320px;
            max-width: 320px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12) {
            min-width: 130px;
            max-width: 130px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .Barcode_fixed_headers td:nth-child(13) {
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }


        .Barcode_fixed_headers td:nth-child(15), .Barcode_fixed_headers th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(16), .Barcode_fixed_headers th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(17), .Barcode_fixed_headers th:nth-child(17) {
            min-width: 110px;
            max-width: 110px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(18), .Barcode_fixed_headers th:nth-child(18) {
            min-width: 110px;
            max-width: 110px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(19), .Barcode_fixed_headers th:nth-child(19) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(20), .Barcode_fixed_headers th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(21), .Barcode_fixed_headers th:nth-child(21) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(22), .Barcode_fixed_headers th:nth-child(22) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(23), .Barcode_fixed_headers th:nth-child(23) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(24), .Barcode_fixed_headers th:nth-child(24) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .Barcode_fixed_headers td:nth-child(25), .Barcode_fixed_headers th:nth-child(25) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }
         .Barcode_fixed_headers td:nth-child(26), .Barcode_fixed_headers th:nth-child(26) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }
          .Barcode_fixed_headers td:nth-child(27), .Barcode_fixed_headers th:nth-child(27) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AbstractPayment');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AbstractPayment";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtAbstract.ClientID %>").attr('disabled', 'disabled');
            $("#<%= txtLocation.ClientID %>").attr('disabled', 'disabled');
        }
        function fncGARowKeydown(evt, source) {
            var rowobj, charCode, scrollheight;
            try {
                rowobj = $(source);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    fncRowClick(NextRowobj)
                    if (NextRowobj.length > 0) {
                        NextRowobj.select().focus();
                        setTimeout(function () {
                            //scrollheight = $("#tblga tbody").scrollTop();
                            //if (parseInt(NextRowobj.find('td span[id*="lblSNo"]').text()) < 3) {
                            //    $("#tblga tbody").scrollTop(0); 
                            //} 
                            NextRowobj.find('td input[id*="txtreferenceNo"]').select();
                        }, 100);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncRowClick(prevrowobj);
                    if (prevrowobj.length > 0) {
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            //scrollheight = $("#tblga tbody").scrollTop();
                            //if (parseFloat(scrollheight) > 3) {
                            //    scrollheight = parseFloat(scrollheight) + 1;
                            //    $("#tblga tbody").scrollTop(scrollheight);
                            //}
                            prevrowobj.find('td input[id*="txtreferenceNo"]').select();
                        }, 100);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $("#tblga tbody > tr").first().focus();
            setTimeout(function () {
                $("#tblga tbody > tr").first().find('td input[id*="txtreferenceNo"]').select();
            }, 50);
            fncRowClick($("#tblga tbody > tr").first());
        });

        function fncRowClick(source) {
            try {
                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "aliceblue");
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtreferenceNo_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); //GET CURRENT ROW FOR UP AND DOWN KEY PRESS
            var row = lnk.parentNode.parentNode; //GET CURRENT ROW FOR LEFT AND RIGHT KEY PRESS
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            var NextRowobj = rowobj.next();
            if (charCode == 13) {
                NextRowobj.find('td input[id*="txtreferenceNo"]').select();
                event.preventDefault();
            }
            else if (charCode == 39) {
                NextRowobj.find('td input[id*="txtreferenceNo"]').select();
                event.preventDefault();

            }
            else if (charCode == 37) {
                NextRowobj.find('td input[id*="txtreferenceNo"]').select();
                event.preventDefault();

            }
            else if (charCode == 40) {

                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtreferenceNo"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtreferenceNo"]').select();
                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtreferenceNo"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtreferenceNo"]').select();
                }
            }
            return true;
        }

        function ValidateForm() {
            try {
                var status = true, rowno = 0;;
                if ($("#<%= txtAbstract.ClientID %>").val() == "") {
                    ShowPopupMessageBox("Abstract Number is Invalid !.")
                    return false;
                }
                else {
                    $("#tblga [id*=gaRow]").each(function (index) {
                        obj = $(this);
                        if (obj.find('td input[id*="txtreferenceNo"]').val() == "") {
                            status = false;
                            rowno = index + 1;
                            return false;
                        }
                    });
                    if (status == true) {
                        fncGetGridValuesForSave();
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSaveAbstract', '');
                    }
                    else {
                        ShowPopupMessageBox("Please Enter Reference Number in this Row - " + rowno);
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetGridValuesForSave() {
            var obj, count = 0;
            try {
                var xml = '<NewDataSet>';
                $("#tblga [id*=gaRow]").each(function () {
                    obj = $(this);
                    if (obj.find('td input[id*="txtreferenceNo"]').val() != "") {
                        count = parseFloat(count) + 1;
                        xml += "<Table>";
                        xml += "<ReferenceNo>" + obj.find('td input[id*=txtreferenceNo]').val().trim() + "</ReferenceNo>";
                        xml += "<BillNo>" + obj.find('td span[id*=lblBillNo]').text().trim() + "</BillNo>";
                        xml += "<BillDate>" + obj.find('td span[id*=lblBillDate]').text().trim() + "</BillDate>";
                        xml += "<VendorCode>" + obj.find('td span[id*=lblVendorcode]').text().trim() + "</VendorCode>";
                        xml += "<VendorName>" + obj.find('td span[id*=lblVendorName]').text().trim() + "</VendorName>";
                        xml += "<GSTNo>" + obj.find('td span[id*=lblTinNo]').text().trim() + "</GSTNo>";
                        xml += "<BillAmt>" + obj.find('td span[id*=lblBillAmount]').text().trim() + "</BillAmt>";
                        xml += "<GST>" + obj.find('td span[id*=lbltax]').text().trim() + "</GST>";
                        xml += "<NetAmount>" + obj.find('td span[id*=lblNetAmount]').text().trim() + "</NetAmount>";
                        xml += "<GANo>" + obj.find('td span[id*=lblGANo]').text().trim() + "</GANo>";
                        xml += "<GADate>" + obj.find('td span[id*=lblGADate]').text().trim() + "</GADate>";
                        xml += "<PONo>" + obj.find('td span[id*=lblPONo]').text().trim() + "</PONo>";
                        xml += "<GIDNo>" + obj.find('td span[id*=Label2]').text().trim() + "</GIDNo>";
                        xml += "<LocationCode>" + obj.find('td span[id*=lblLocationCode]').text().trim() + "</LocationCode>";
                        xml += "<GACreateUser>" + obj.find('td span[id*=lblCreateUser]').text().trim() + "</GACreateUser>";
                        xml += "<BillType>" + obj.find('td span[id*=lblBilltype]').text().trim() + "</BillType>";
                        xml += "<DebitNo>" + obj.find('td span[id*=Label3]').text().trim() + "</DebitNo>";
                        xml += "<DebitValue>" + obj.find('td span[id*=lblDebitValue]').text().trim() + "</DebitValue>";
                        xml += "<CreaditNo>" + obj.find('td span[id*=lblCreaditValue]').text().trim() + "</CreaditNo>";
                        xml += "<CreadiValue>" + obj.find('td span[id*=lblValue]').text().trim() + "</CreadiValue>";
                        xml += "</Table>";
                    }
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
                $('#<%=hidAbstractValue.ClientID %>').val(xml);
                $('#<%=hidCount.ClientID %>').val(count);
                return xml;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    ValidateForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClose.ClientID %>').click();
                        e.preventDefault();
               }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="main-container">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Abstract</a><i class="fa fa-angle-right"></i></li>
                    <li><a href="../Purchase/frmSummaryAbstract.aspx">Summary Abstract</a> <i class="fa fa-angle-right"></i></li>
                    <li class="active-page uppercase" id="breadcrumbs_text">Pay Abstract
                    </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
            <div class="container-group-full">
                <div class="purchase-order-header barcodepahe_color">
                </div>
                <div class="control-group-split">
                    <asp:UpdatePanel ID="upgrnview" runat="server">
                        <ContentTemplate>
                            <div class="ga_view_dropdown">
                                <div class="ga_view_dropdown_split" style="width: 22% !important;">
                                    <div class="ga_label">
                                        <asp:Label ID="lblvendor" runat="server" Text="Abstract No"></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <asp:TextBox ID="txtAbstract" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogVendor(event, 'Vendor', 'txtVendor', 'txtLocation');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="ga_view_dropdown_split" style="width: 22% !important;">
                                    <div class="ga_label">
                                        <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="full_width" onkeydown="return fncShowSearchDialogVendor(event, 'Location', '', '');"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="divrepeater" class="Barcode_fixed_headers">
                <asp:UpdatePanel ID="uprptrga" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblga" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">
                                        <asp:CheckBox ID="cbProfilterheader" onclick="fncSelectUnselectrow(this)" runat="server" />
                                    </th>
                                    <th scope="col" runat="server" id="thDelete">Delete
                                    </th>
                                    <th scope="col" runat="server" id="thEdit">Edit GA
                                    </th>
                                    <th scope="col" runat="server" id="thGrn">GRN
                                    </th>
                                    <th scope="col" runat="server" id="thGrnView">GRN
                                    </th>
                                    <th scope="col">Reference No
                                    </th>
                                    <th scope="col">BillNo/RefNo
                                    </th>
                                    <th scope="col">BillDate/RefDate
                                    </th>
                                    <th scope="col">VendorCode
                                    </th>
                                    <th scope="col">VendorName
                                    </th>
                                    <th scope="col">GSTIN No
                                    </th>
                                    <th scope="col">Gross Amount
                                    </th>
                                    <th scope="col">GST
                                    </th>
                                    <th scope="col">Total Amount
                                    </th>
                                    <th scope="col">G.A.No
                                    </th>
                                    <th scope="col">G.A.Date
                                    </th>
                                    <th scope="col">PO.No
                                    </th>
                                    <th scope="col">GRN.No
                                    </th>
                                    <th scope="col">Location
                                    </th>
                                    <th scope="col">CreateDate
                                    </th>
                                    <th scope="col">CreateUser
                                    </th>
                                    <th scope="col">Bill Type
                                    </th>
                                    <th scope="col">DebitNo
                                    </th>
                                    <th scope="col">Value
                                    </th>
                                    <th scope="col">CreaditNo
                                    </th>
                                    <th scope="col">Creadit Value
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrGA" runat="server">
                                <HeaderTemplate>
                                    <tbody id="BulkBarcodebody">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="gaRow" tabindex='<%#(Container.ItemIndex)%>' runat="server"
                                        onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                        </td>
                                        <td runat="server" id="td1">
                                            <asp:CheckBox ID="chkSingle" runat="server" onclick="fncSelectSinglerow(this)" />
                                        </td>
                                        <td runat="server" id="tdDelete">
                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png"
                                                OnClientClick="fncAbsDeleteAndEdit(this,'Delete');return false;" />
                                        </td>
                                        <td runat="server" id="tdEdit">
                                            <asp:ImageButton ID="btnImgEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                OnClientClick="fncAbsDeleteAndEdit(this,'Edit')" />
                                        </td>
                                        <td runat="server" id="tdGrn">
                                            <asp:Button ID="btnGrn" runat="server" Text='<%$ Resources:LabelCaption,lblGo%>'
                                                OnClientClick="fncAbsDeleteAndEdit(this,'Go');return false;" />
                                        </td>
                                        <td runat="server" id="tdGrnView">
                                            <asp:Button ID="btnGrnView" runat="server" Text='<%$ Resources:LabelCaption,btn_View%>'
                                                OnClientClick="fncViewClosedGRN(this); return false; " />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtreferenceNo" runat="server" Width="100%" onkeyup="return txtreferenceNo_Keystroke(event,this);" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillNo" runat="server" Text='<%# Eval("DOInvNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillDate" runat="server" Text='<%# Eval("BillDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorcode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTinNo" runat="server" Text='<%# Eval("CSTNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillAmount" runat="server" Text='<%# Eval("BillAmount") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltax" runat="server" Text='<%# Eval("tax") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNetAmount" runat="server" Text='<%# Eval("NetAmount") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGANo" runat="server" Text='<%# Eval("GANo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGADate" runat="server" Text='<%# Eval("GADate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPONo" runat="server" Text='<%# Eval("PONo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("GidNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLocationCode" runat="server" Text='<%# Eval("LocationCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateUser" runat="server" Text='<%# Eval("CreateUser") %>' />
                                        </td>
                                        <td style="display: none">
                                            <asp:Label ID="lblBilltype" runat="server" Text='<%# Eval("BillType") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("DebitNoteNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDebitValue" runat="server" Text='<%# Eval("Value") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreaditValue" runat="server" Text='<%# Eval("CreaditNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("CreaditValue") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="purchase-order-header barcodepahe_color">
            <div class="col-md-6" id="divGidNo">
            </div>
            <div class="col-md-6">
                <div class="control-button" style="float: right !important;">
                    <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" PostBackUrl="../Purchase/frmSummaryAbstract.aspx" Text="Close(F6)"></asp:LinkButton>
                </div>
                <div class="control-button" style="float: right !important;">
                    <asp:LinkButton ID="lnkSaveAbstract" runat="server" class="button-blue" OnClick="lnkSave_Click"
                        Text="Save(F4)" OnClientClick="return ValidateForm();"></asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="hiddencol">
            <div id="deleteGA">
                <div>
                    <asp:Label ID="lblGaDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:UpdatePanel ID="updelete" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkbtnYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                    OnClientClick="return fncConfirmDeleteGA()"> </asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnlbtnNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncCloseDeleteDialog();return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="GAEdit">
                <div>
                    <asp:Label ID="lbleditStatus" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                        OnClientClick="fncCloseEditDialog();return false;"> </asp:LinkButton>
                </div>
            </div>
            <div id="divGidHold">
                <div>
                    <asp:Label ID="lblGidHold" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkgidHoldYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkgidHoldNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="divGRNDelete">
                <div>
                    <asp:Label ID="lblGRNDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkGRNYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>' OnClientClick="fncConfirmGRNDelete('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkGRNNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>' OnClientClick="fncConfirmGRNDelete('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>

            <asp:UpdatePanel ID="upgidno" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btngidno" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:HiddenField ID="hidgaNo" runat="server" />
            <asp:HiddenField ID="hidVendorcode" runat="server" />
            <asp:HiddenField ID="hidGid" runat="server" />
            <asp:HiddenField ID="hidBillNo" runat="server" />
            <asp:Button ID="btnEdit" runat="server" />
            <asp:Button ID="btnGrnGo" runat="server" />
            <asp:HiddenField ID="hidLocation" runat="server" />
            <asp:HiddenField ID="hidGidMode" runat="server" Value="" />
            <asp:HiddenField ID="hidgaDate" runat="server" />
            <asp:HiddenField ID="hidgaAmt" runat="server" />
            <asp:HiddenField ID="hidBillType" runat="server" Value="A" />
            <asp:HiddenField ID="hidComputerName" runat="server" />
            <asp:HiddenField ID="hidDiscontinueEntry" runat="server" />

            <asp:HiddenField ID="hidTextileBrand" runat="server" />
            <asp:HiddenField ID="hidTextileLoc" runat="server" />

            <div id="dialog-DiscontinueEntry">
            </div>

        </div>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="uprptrga">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidAbstractValue" runat="server" />
    <asp:HiddenField ID="hidCount" runat="server" />
</asp:Content>
