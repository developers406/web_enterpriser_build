﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPurchaseOrderView.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrderView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .poSummary td:nth-child(1), .poSummary th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .poSummary td:nth-child(2), .poSummary th:nth-child(2) {
            min-width: 40px;
            max-width: 40px;
        }

        .poSummary td:nth-child(3), .poSummary th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
        }

        .poSummary td:nth-child(4), .poSummary th:nth-child(4) {
            min-width: 40px;
            max-width: 40px;
        }

        .poSummary td:nth-child(5), .poSummary th:nth-child(5) {
            min-width: 40px;
            max-width: 40px;
        }

        .poSummary td:nth-child(6), .poSummary th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(7), .poSummary th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .poSummary td:nth-child(8), .poSummary th:nth-child(8) {
            min-width: 90px;
            max-width: 90px;
        }

        .poSummary td:nth-child(9), .poSummary th:nth-child(9) {
            min-width: 382px;
            max-width: 382px;
        }

        .poSummary td:nth-child(9) {
            text-align: left;
        }

        .poSummary td:nth-child(10), .poSummary th:nth-child(10) {
            min-width: 90px;
            max-width: 90px;
        }

        .poSummary td:nth-child(10) {
            text-align: right;
        }

        .poSummary td:nth-child(11), .poSummary th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
        }

        .poSummary td:nth-child(12), .poSummary th:nth-child(12) {
            min-width: 65px;
            max-width: 65px;
        }

        .poSummary td:nth-child(13), .poSummary th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .poSummary td:nth-child(14), .poSummary th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
        }

        .poSummary td:nth-child(15), .poSummary th:nth-child(15) {
            min-width: 70px;
            max-width: 70px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'PurchaseOrderView');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "PurchaseOrderView";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

      
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkAdd.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkAdd.ClientID %>').css("display", "none");
              }
              $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            if ($("#<%= txtFromDate.ClientID %>").val() != "") {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            }
            else {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

            fncHideShowApproveCol();

        }

        ///Get PO Status GRN Closed Or Not
        function fncGetPOStatus() {
            try {
              //  var Val = $("#<%=hidAppprovedval.ClientID%>").val($("#<%=chkApporved.Checked%>").val());

              <%-- alert( $('#chkApporved.').text($('#chkApporved').val()));


                if ($("#<%=chkApporved.Checked%>").val() == "True") {
                    alert($("#<%=chkApporved.Checked%>").val());
                }--%>
                var DeleteUnApprovePo = $("td", rowObj).eq(14).text().replace(/&nbsp;/g, '').trim(); //For check Approved PO surya12112021
                console.log(DeleteUnApprovePo)
                if (DeleteUnApprovePo == "UnApprove") {
                    ShowPopupMessageBox("Cannot Delete ApprovedPo");
                    return false;
                }
                var obj = {};
                obj.poNo = $("#<%=hidPONo.ClientID %>").val();
                obj.mode = "10";
              
                $.ajax({
                    type: "POST",
                    url: "frmPurchaseOrderView.aspx/fncGetPOStatus",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "Y") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_PODelete%>');
                        }
                        else if (msg.d == "N") {
                            $("#<%= btnPODelete.ClientID %>").click();

                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Get Approve Status
    function fncGetPODetail(source, value) {
        try {

            rowObj = $(source).closest("tr");
            $("#<%=hidPONo.ClientID %>").val($("td", rowObj).eq(5).html().replace(/&nbsp;/g, ''));
            
            debugger;
            if (value == "approveclick") {
                fncShowApproveDailog();
            }
            else if (value == "email") {

                var Values = $("td", rowObj).eq(12).html().replace(/&nbsp;/g, '');
                if (Values.trim() == "") {
                    console.log(Values.trim());
                    ShowPopupMessageBox('PO is not Approved..!');
                    return false;
                }
            }
            else {
                if ($("#<%=chkApporved.ClientID %>").is(":checked")) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_CannotPO%>');
                    return false;
                }
                else {
                    fncGetPOApproveStatus();
                }
            }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
/// Get PO Approve Status
function fncGetPOApproveStatus() {
    try {
        var obj = {};
        obj.poNo = $("#<%=hidPONo.ClientID %>").val();
            obj.mode = "2";
            $.ajax({
                type: "POST",
                url: "frmPurchaseOrderView.aspx/fncGetPOStatus",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == "Y") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.msg_CannotPO%>');
                        }
                        else if (msg.d == "N") {
                            $("#<%=btnEdit.ClientID %>").click();
                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    /// Get PO Approve Status for Email
    function fncGetPOApproveStatusEmail() {
        try {
            var obj = {};
            obj.poNo = $("#<%=hidPONo.ClientID %>").val();
            obj.mode = "2";
            $.ajax({
                type: "POST",
                url: "frmPurchaseOrderView.aspx/fncGetPOStatus",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == "Y") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.msg_CannotPO%>');
                        }
                        else if (msg.d == "N") {
                            $("#<%=btnEdit.ClientID %>").click();
                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Show Approve Dailog
    function fncShowApproveDailog() {
        var msg = "";
        $(function () {

            if ($('#<%=chkApporved.ClientID%>').is(':checked')) {
                msg = "Do You Want UnApprove?"
            }
            else {
                msg = "Do You Want Approve?"
            }

            $("#dialog-MessageBox").html(msg);
            $("#dialog-MessageBox").dialog({
                title: "Enterpriser Web",

                buttons: {
                    Yes: function () {
                        $(this).dialog("destroy");
                        $("#<%=btnApprove.ClientID %>").click();
                    },
                    No: function () {
                        $(this).dialog("destroy");
                    },
                },
                modal: true
            });
        });
    };

    ///Delete Confirmation //05-12-2017
    function fncPODeleteConfirm(source) {

        try {
            rowObj = $(source).closest("tr");
            $("#<%=hidPONo.ClientID %>").val($("td", rowObj).eq(5).html().replace(/&nbsp;/g, ''));
            var DeleteUnApprovePo = $("td", rowObj).eq(14).html().replace(/&nbsp;/g, ''); //For check Approved PO surya12112021

            $(function () {
                $("#dialog-MessageBox").html($("#<%=hidPONo.ClientID %>").val() + '<%=Resources.LabelCaption.msg_PODeleteConfirm%>');
                    $("#dialog-MessageBox").dialog({
                        title: "Enterpriser Web",

                        buttons: {
                            Yes: function () {
                                $(this).dialog("destroy");
                                fncGetPOStatus();
                            },
                            No: function () {
                                $(this).dialog("destroy");
                            },
                        },
                        modal: true
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        };


        function fncHideShowApproveCol() {
            try {
                if ($("#<%=chkApporved.ClientID %>").is(":checked")) {
                    $("#ContentPlaceHolder1_approve").text("UnApprove");
                    $("#ContentPlaceHolder1_gvPO tbody tr").find('td a[id*="lnkApporve"]').text("UnApprove");
                }
                else {
                    $("#ContentPlaceHolder1_approve").text("Approve");
                    $("#ContentPlaceHolder1_gvPO tbody tr").find('td a[id*="lnkApporve"]').text("Approve");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncMailSuccessMessage() {
            try {
                fncHideShowApproveCol();
                fncToastInformation("Mail send Successfully");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSetValue() {
            try {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkLoad', '');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Purchase Order </li>
                <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="purchase-order-header">
                        <div>
                            <div class="control-group-split float_left" style="width: 55%; clear: both">
                                <div class="control-group-left1">
                                    <div class="label-left">
                                        <asp:Label ID="Label38" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-middle">
                                    <div class="label-left">
                                        <asp:Label ID="Label40" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-right1">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Po No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlPoNo" runat="server" CssClass="form-control-res" OnSelectedIndexChanged="ddlPoNo_SelectedIndexChanged"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="purchase-option-container float_left" style="width: 45%;">
                                <div class="control-group-split">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                        </div>
                                        <div class="label-right" style="float: right">
                                            <asp:CheckBox ID="chkApporved" runat="server" OnCheckedChanged="chkApporved_CheckedChanged"
                                                AutoPostBack="true" />
                                            Approved
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label26" runat="server" Text="From Date"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label32" runat="server" Text="To Date"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-container" style="float: left; clear: both; display: table; width: 50%">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" PostBackUrl="~/Purchase/frmPurchaseOrder.aspx" Text="New"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkLoad" runat="server" class="button-blue" OnClick="lnkLoad_Click" Text="Refresh"></asp:LinkButton>
                            </div>
                            <div class="control-button PO_Summary_border">
                                <asp:RadioButton ID="rbtSummary" runat="server" Checked="true" Text="Internal" GroupName="Print" />
                                <asp:RadioButton ID="rbtDetail" runat="server" Text="External" GroupName="Print" />
                                <asp:RadioButton ID="rbtSmallPrint" runat="server" Text="Small" GroupName="Print" />
                            </div>
                            <div class="control-button PO_Summary_border">
                                <asp:RadioButton ID="rbnExcel" runat="server" Checked="true" Text="Excel" GroupName="Email" />
                                <asp:RadioButton ID="rbnPDF" runat="server" Text="PDF" GroupName="Email" />
                            </div>
                            <div id="divJobOrder" runat="server">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkJobOrder" runat="server" class="button-blue" PostBackUrl="~/Purchase/frmJobOrder.aspx" Text="Job Order"></asp:LinkButton>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="purchase-order-detail over_flowhorizontal">
                        <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header poSummary">
                            <tr>
                                <th>S.No</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Print</th>
                                <th>Email</th>
                                <th>PoNo</th>
                                <th>PoDate</th>
                                <th>DeliveryDate</th>
                                <th id="idvendorName">VendorName</th>
                                <th>PoNetValue</th>
                                <th>CreatedBy</th>
                                <th>Apporved</th>
                                <th>ApprovedBy</th>
                                <th>Location</th>
                                <th id="approve">Apporve</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 400px; width: 1356px; background-color: aliceblue;">
                            <asp:GridView ID="gvPO" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                CssClass="pshro_GridDgn poSummary">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />

                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClientClick="return fncGetPODetail(this,'editclick');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                ToolTip="Click here to Delete" OnClientClick=" return fncPODeleteConfirm(this);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                                ToolTip="Click here to Print" OnClick="lnkPrint_Click" OnClientClick="fncHideShowApproveCol" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEmail" runat="server" ImageUrl="~/images/email_New.png"
                                                ToolTip="Click here to Email" OnClientClick="return fncGetPODetail(this,'email');" OnClick="imgbtnEmail_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="PoNo" HeaderText="PO NO"></asp:BoundField>
                                    <asp:BoundField DataField="PoDate" HeaderText="PO Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                                    <asp:BoundField DataField="DeliveryDate" HeaderText="Delivery On" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                                    <asp:BoundField DataField="VendorName" HeaderText="Vendor Name"></asp:BoundField>
                                    <asp:BoundField DataField="PoNetValue" HeaderText="Net Total"></asp:BoundField>
                                    <asp:BoundField DataField="CreatedBy" HeaderText="Created By"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Apporved">
                                        <ItemTemplate>
                                            <asp:Image ID="imgStatus" runat="server" ImageUrl='<%# GetImage((string)Eval("Approved")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ApprovedBy" HeaderText="Apporved By"></asp:BoundField>
                                    <asp:BoundField DataField="CurrCode" HeaderText="Loc"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Apporve">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkApporve" runat="server" OnClientClick="return fncGetPODetail(this,'approveclick');">Approve</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hidPONo" runat="server" Value="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="hiddencol">
        <asp:Button ID="btnApprove" runat="server" OnClick="btnApprove_Click" />
        <asp:Button ID="btnPODelete" runat="server" OnClick="btnPODelete_Click" />
        <asp:Button ID="btnEdit" runat="server" OnClick="btnEdit_Click" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" /> 
        <asp:HiddenField ID="hidEdit" runat="server" />
         <asp:HiddenField ID="hidAppprovedval" runat="server" /> 
    </div>
</asp:Content>
