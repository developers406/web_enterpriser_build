﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTextileGADashboard.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmTextileGADashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />
    <script type="text/javascript">
        var Status = 0;
        function pageLoad() {
            try {
                $(".chzn-container").css('display', 'none');
                $(".form-control-res").css('height', '25px');
                $(".form-control-res").css('display', 'block');
                if ($("#<%=txtFromDate.ClientID %>").val() == '') {
                    $("#<%=txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                    $("#<%=txtTODate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                }
                else {
                    $("#<%=txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                    $("#<%=txtTODate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                }

                if ($.session.get('height') != null && $.session.get('height') != "") {
                    var height = $.session.get('height');
                    height = parseFloat(height) - 175;
                    $("#tblInvoice tbody").css('height', '' + height + "px" + '');
                }
                fncGetGADetails('N');
                $("#<%=chkGid.ClientID %>").change(function () {
                    var tblGABody = $("#tblInvoice tbody");
                    if (this.checked) {
                        Status = 1;
                        fncGetGADetails('Y');
                        tblGABody.css('background-color', 'aliceblue');
                    }
                    else {
                        Status = 0;
                        fncGetGADetails('N');
                        tblGABody.css('background-color', 'white');
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetGADetails(status) {
            try {
                $.ajax({
                    url: "frmTextileGA.aspx/GetGaDetails",
                    data: "{ 'Code': '" + status + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        fncBindData(jQuery.parseJSON(data.d));
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBindData(obj) {
            try {
                var vars = {};
                var divstart = "";
                var tblGABody = $("#tblInvoice tbody");
                tblGABody.children().remove();
                for (var i = 0; i < obj.Table.length; i++) {
                    var row = "<tr id='GRNBodyrow_" + i + "'  tabindex='" + i + "'  >"
                        + "<td id ='tdCompanyCode_" + i + "'  >" + obj.Table[i]["CompanyCode"] + "</td>"
                        + "<td id ='tdSupplierName_" + i + "'  >" + obj.Table[i]["SupplierName"] + "</td>"
                        + "<td id ='tdDoNo_" + i + "'  >" + obj.Table[i]["DoNo"] + "</td>"
                        + "<td id ='tdDoDate_" + i + "'  >" + obj.Table[i]["DoDate"] + "</td>"
                        + "<td style='text-align: right !important;' id ='tdNOfPcs_" + i + "'  >" + obj.Table[i]["NOfPcs"] + "</td>"
                        + "<td style='text-align: right !important;' id ='tdBillValue_" + i + "' >" +parseFloat(obj.Table[i]["BillValue"]).toFixed(2) + "</td>"
                        + "<td style='text-align: right !important;' id ='tdGstPer_" + i + "' >" + +parseFloat(obj.Table[i]["GstPer"]).toFixed(2) + "</td>"
                        + "<td style='text-align: right !important;' id ='tdDisPer_" + i + "' >" + +parseFloat(obj.Table[i]["DisPer"]).toFixed(2) + "</td>"
                        + "<td style='text-align: right !important;' id ='tdNet_" + i + "' >" + +parseFloat(obj.Table[i]["NetAmt"]).toFixed(2) + "</td>"
                        + "<td id ='tdTran_" + i + "' style='display:none !important'>" + obj.Table[i]["TranNo"] + "</td>"
                        + "<td id ='tdVendodCode_" + i + "' style='display:none !important'>" + obj.Table[i]["VendorName"] + "</td>"
                        + "<td  onclick ='fncGo(this);return false;' style='text-align: center !important;' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Del\");' ><span id='tdremove_" + i + "' class ='glyphicon glyphicon-remove-circle' style ='font-size: 24px;color: red;cursor: pointer;' />&nbsp&nbsp&nbsp<span id='tdsign_" + i + "' class ='glyphicon glyphicon-ok-sign' style ='font-size: 24px;color: green;cursor: pointer;' />&nbsp&nbsp&nbsp<span id='tdEdit_" + i + "' class ='glyphicon glyphicon-edit' style ='font-size: 24px;color: dark blue;cursor: pointer;' /></td></tr > ";
                    tblGABody.append(row);
                }

            }
            catch (ex) {
                ShowPopupMessageBox(ex.message);
            }
        }

        function fncGo(source) {
            try {
                $('#<%=hidTranNo.ClientID %>').val($(source).closest('tr').find('td[id*=tdTran]').text());
                $('#<%=hidVendor.ClientID %>').val($(source).closest('tr').find('td[id*=tdVendodCode]').text());
                $('#<%=hidNetAmt.ClientID %>').val($(source).closest('tr').find('td[id*=tdNet]').text());
                $('#<%=hidInvoiceNo.ClientID %>').val($(source).closest('tr').find('td[id*=tdDoNo]').text());
                $('#<%=hidBillValue.ClientID %>').val($(source).closest('tr').find('td[id*=tdBillValue]').text());
                $('#<%=hidStatus.ClientID %>').val(Status);
                $('#<%=btnClick.ClientID %>').click();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    <style type="text/css">
        #tblInvoice th, td {
            width: 149.5px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Purchase</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">GA Entry</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="col-md-12 Cat_Margin">
            <div class="col-md-1 Cat_Margin">
                <span class="lblBlack">Supplier</span>
            </div>
            <div class="col-md-2 Cat_Margin">
                <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control-res">
                </asp:DropDownList>
            </div>
            <div class="col-md-1 Cat_Margin">
                <asp:TextBox ID="txtTranSearch" runat="server" CssClass="form-control-res" placeholder="Tran No to Search"></asp:TextBox>
            </div>
            <div class="col-md-1 Cat_Margin">
                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" placeholder="From Date"></asp:TextBox>
            </div>
            <div class="col-md-1 Cat_Margin">
                <asp:TextBox ID="txtTODate" runat="server" CssClass="form-control-res" placeholder="To Date"></asp:TextBox>
            </div>

            <div class="col-md-1">
                <asp:LinkButton ID="lnkSearch" runat="server" class="button-red" Style="float: right;"
                    OnClientClick="fncAdd();return false;"><i class="icon-play"></i>Refresh</asp:LinkButton>
            </div>
              <div class="col-md-1">
                <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Style="float: right;" PostBackUrl="~/Purchase/frmTextileGA.aspx" ><i class="icon-play"></i>New</asp:LinkButton>
            </div>
            <div class="col-md-4 Cat_Margin">
                <asp:CheckBox ID="chkGid" runat="server" Style="float: right;" Class="radioboxlist" Text="Closed GID" />
            </div>
            <div class="Payment_fixed_headers Cat_Margin">
                <table id="tblInvoice" cellspacing="0" rules="all" border="1" style="width: 100%;" class="Invoice">
                    <thead>
                        <tr>
                            <th scope="col">Company
                            </th>
                            <th scope="col">Supplier Name
                            </th>
                            <th scope="col">Invoice No
                            </th>
                            <th scope="col">Date
                            </th>
                            <th scope="col">Bundles  
                            </th>
                            <th scope="col">Base
                            </th>
                            <th scope="col">Tax  
                            </th>
                            <th scope="col">Expense
                            </th>
                            <th scope="col">Total
                            </th>
                            <th scope="col"  >Action
                            </th>
                        </tr>
                    </thead>
                    <tbody style="overflow-y: scroll; height: auto;">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
        <asp:HiddenField ID="hidTaxAmt" runat="server" />
        <asp:HiddenField ID="hidNetAmt" runat="server" />
        <asp:HiddenField ID="hidfSellingPriceRoundOff" runat="server" />
        <asp:HiddenField ID="hidSaveSata" runat="server" />
        <asp:HiddenField ID="hidVendor" runat="server" />
        <asp:HiddenField ID="hidTranNo" runat="server" />
        <asp:HiddenField ID="hidInvoiceNo" runat="server" />
        <asp:HiddenField ID="hidBillValue" runat="server" />
        <asp:HiddenField ID="hidStatus" runat="server" />
    <div class="display_none">
            <asp:Button ID="btnClick" runat="server" OnClick="lnkGo_Click" />
        </div>
</asp:Content>
