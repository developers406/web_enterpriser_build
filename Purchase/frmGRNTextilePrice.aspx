﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGRNTextilePrice.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmGRNTextilePrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .form-control-res[readonly] {
            background-color: lightpink;
        }

        /*Attribute Count Table */
        #grdAttributeSize {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeSize td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeSize tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdAttributeSize tr:hover {
                background-color: #ddd;
            }

            #grdAttributeSize th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: crimson;
                font-weight: bolder;
                color: white;
            }

        /*Item Detials*/
        #grdItemDetails {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdItemDetails td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdItemDetails tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #grdItemDetails tr:hover {
                background-color: #e9dcf7;
            }

            #grdItemDetails th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: rebeccapurple;
                font-weight: bolder;
                color: white;
            }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GRNTextilePrice');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GRNTextilePrice";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        function pageLoad() {

            $("[id$=ddlProduct]").change(function () {
                var selectedVal = $("[id$=ddlProduct]").val();
                //alert(selectedVal);
                LoadDynamicAttribute(selectedVal);

            });


            $("[id$=ddlPricingType]").change(function () {
                var selectedVal = $("[id$=ddlPricingType]").val();
                if (selectedVal == 1) {
                    $("[id$=divRangeFrom]").show();
                    //$("[id$=divRangeTo]").hide();
                    //$("[id$=divRangeTo]").hide();
                }
                else if (selectedVal == 2) {
                    $("[id$=divRangeFrom]").show();
                    //$("[id$=divRangeTo]").show();
                }
                else if (selectedVal == 3) {
                    $("[id$=divRangeFrom]").show();
                    //$("[id$=divRangeTo]").hide();
                }
                else {
                    $("[id$=divRangeFrom]").hide();
                    $("[id$=divRangeTo]").hide();
                }

            });
        }

        function fncClear() {
            var itemName = "";
            $('#grdControlId tr').each(function () {
                if (!this.rowIndex) return;
                var controlId = $(this).find("td").eq(1).html();
                console.log(controlId);
                itemName = itemName + "-" + $("[id$='" + controlId + "']").find('option:selected').text();
            });
            var selectedVal = $("[id$=ddlProduct]").find('option:selected').text();
            itemName = selectedVal + itemName;
            console.log(itemName);
            return false;
        }

        function LoadDynamicAttribute(ProductCode) {
            var obj = {};
            obj.Code = ProductCode;

            $.ajax({
                type: "POST",
                url: "frmGRNTextilePrice.aspx/GetAttributes",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var objvendor = jQuery.parseJSON(msg.d);
                    console.log(objvendor);
                    //console.log(Object.keys(objvendor).length);
                    numofTable = Object.keys(objvendor).length;
                    debugger;

                    $('#grdAttributeSize').find("tr").remove();
                    if (numofTable > 0) {

                        $('#grdControlId').find("tr:gt(0)").remove();
                        $('#grdAttributes').find("tr:gt(0)").remove();
                        $('#grdSizeValue').find("tr:gt(0)").remove();
                        $("#dynamicInput").empty();
                        if (objvendor.Table.length > 0) {
                            //$("div.control-group-single-res").remove();    
                            LoadProductCost(objvendor.Table);
                            fncSetZero();
                            var bSizeCheck = false;
                            var bSleeveCheck = false;
                            for (var i = 0; i < objvendor.Table.length; i++) {
                                console.log(objvendor.Table[i]["AttributeName"]);
                                createAttributeTable(objvendor.Table[i]["AttributeCode"], objvendor.Table[i]["AttributeName"], 'grdControlId');
                                //if (objvendor.Table[i]["AttributeName"] != 'SIZE' && objvendor.Table[i]["AttributeName"] != 'SLEEVE') {                                    
                                var newDiv = document.createElement('div');
                                newDiv.className = 'control-group-single-res';
                                var selectHTML = "";
                                selectHTML = "<div class='label-left'><label >" + objvendor.Table[i]["AttributeName"] + "</label></div>";
                                selectHTML += "<div class='label-right'> <select  class='form-control-res' id = '" + objvendor.Table[i]["AttributeName"] + "'>";
                                attributecode = objvendor.Table[i]["AttributeCode"];
                                attributeName = objvendor.Table[i]["AttributeName"];
                                if (objvendor.Table1.length > 0) {

                                    var res = $.grep(objvendor.Table1, function (v) {
                                        return objvendor.Table[i]["AttributeName"].indexOf(v.AttributeName) > -1;
                                    });

                                    //console.log(res);  
                                    selectHTML += "<option value='0'>NONE</option>";
                                    $.each(res, function (i, obj) {
                                        //console.log(obj.ValueCode);
                                        //console.log(obj.ValueName);  
                                        if (attributeName.toUpperCase() == 'SIZE' || attributeName.toUpperCase() == 'SLEEVE') {
                                            createAttributeSize(attributecode, attributeName, obj.ValueCode, obj.ValueName, 'grdSizeValue')
                                        }
                                        selectHTML += "<option value='" + obj.ValueCode + "'>" + obj.ValueName + "</option>";
                                    });
                                }

                                selectHTML += "</select></div>";
                                newDiv.innerHTML = selectHTML;

                                if (objvendor.Table[i]["AttributeName"].toUpperCase() != 'SIZE' && objvendor.Table[i]["AttributeName"].toUpperCase() != 'SLEEVE') {

                                    document.getElementById('dynamicInput').appendChild(newDiv);
                                    //console.log(newDiv.innerHTML);
                                    $("select").chosen({ width: '100%' });

                                }
                                else if (objvendor.Table[i]["AttributeName"].toUpperCase() == 'SIZE') {
                                    if (objvendor.Table1.length > 0) {
                                        funBindSizeGrid(objvendor.Table2);
                                        //console.log(res);                                        
                                        bSizeCheck = true;
                                    }
                                }
                                else if (objvendor.Table[i]["AttributeName"].toUpperCase() == 'SLEEVE') {
                                    bSleeveCheck = true;

                                }
                            }
                            debugger;
                            var targetDiv = document.getElementById('priceHrd');
                            if (bSizeCheck) {
                                $("[id*=divQty]").hide();
                                $("[id*=divFull]").show();
                                $("[id*=divHalf]").show();
                                targetDiv.innerHTML = 'Half Sleeve';
                            }
                            else {
                                $("[id*=divQty]").show();
                                $("[id*=divFull]").hide();
                                $("[id*=divHalf]").show();
                                targetDiv.innerHTML = 'Price Setting';
                            }

                            if (bSleeveCheck == false && bSizeCheck == true) {
                                $("[id*=divFull]").hide();
                                $("[id*=divHalf]").show();
                                targetDiv.innerHTML = 'Price Setting';
                            }

                            $("[id*=lnkAdd]").show();

                        }
                        else {

                            fncSetZero();
                            console.log('No mapping');
                            $("[id*=lnkAdd]").hide();
                            $("[id*=divQty]").hide();
                            $("[id*=divFull]").hide();
                            $("[id*=divHalf]").hide();
                        }
                    }

                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });
        }

        function createAttributeTable(Code, Name, TableId) {
            var table1 = document.getElementById(TableId);
            var newRow = table1.insertRow(table1.length),
                       cell1 = newRow.insertCell(0),
                       cell2 = newRow.insertCell(1);
            // add values to the cells
            cell1.innerHTML = Code;
            cell2.innerHTML = Name;
        }

        function createAttributeSize(AttbCode, AttbName, valueCode, valueName, TableId) {
            var table1 = document.getElementById(TableId);
            var newRow = table1.insertRow(table1.length),
                       cell1 = newRow.insertCell(0),
                       cell2 = newRow.insertCell(1),
                       cell3 = newRow.insertCell(2),
                       cell4 = newRow.insertCell(3);
            // add values to the cells
            cell1.innerHTML = AttbCode;
            cell2.innerHTML = AttbName;
            cell3.innerHTML = valueCode;
            cell4.innerHTML = valueName;
        }

        function createAttributeValueTable(SNo, Code, Value) {
            var table1 = document.getElementById("grdAttributes");
            var newRow = table1.insertRow(table1.length),
                       cell1 = newRow.insertCell(0),
                       cell2 = newRow.insertCell(1),
                       cell3 = newRow.insertCell(2);
            // add values to the cells
            cell1.innerHTML = SNo;
            cell2.innerHTML = Code;
            cell3.innerHTML = Value;
        }

        function LoadProductCost(jsonObj) {
            var obj;
            for (var i = 0; i < jsonObj.length; i++) {
                if (jsonObj[i].ProductCost > 0) {
                    obj = jsonObj[i];
                    break;
                }
            }
            //console.log(obj);
            $(obj).each(function (index, element) {
                //console.log(element.ProductCost);
                $('#<%=txtProductCost.ClientID%>').val(element.ProductCost.toFixed(2));
                $('#<%=txtMrp.ClientID%>').val(element.MRP.toFixed(2));
                $('#<%=txtMiscelenious.ClientID%>').val(element.Miscelenious.toFixed(2));
                $('#<%=txtMargin.ClientID%>').val(element.Margin.toFixed(2));
                $('#<%=txtGSTPerc.ClientID%>').val(element.GSTPerc.toFixed(2));
                $('#<%=txtGSTAmt.ClientID%>').val(element.GSTAmt.toFixed(2));
                //$('#<%=txtProductCost.ClientID%>').focus().select();
            });
        }

        function ValidateForm() {

            var SelectionCheck = false;
            var Checksubcategory = false;
            var subcategoryValue = "";

            $('#grdControlId tr').each(function () {
                if (!this.rowIndex) return;
                var controlId = $(this).find("td").eq(1).html();
                var attibCode = $(this).find("td").eq(0).html();
                if (controlId.toUpperCase() != 'SIZE' && controlId.toUpperCase() != 'SLEEVE') {
                    value = $("[id$='" + controlId + "']").find('option:selected').text();
                    if (value != 'NONE') {
                        SelectionCheck = true;
                    }
                    if (controlId.toUpperCase() == 'SUB CATEGORY') {
                        value = $("[id$='" + controlId + "']").find('option:selected').text();
                        subcategoryValue = value;
                        if (value != 'NONE') {
                            Checksubcategory = true;
                        }
                    }
                }
            });

            debugger;

            if (subcategoryValue != "") {
                if (Checksubcategory == false) {
                    popUpObjectForSetFocusandOpen = $("[id$='SUB CATEGORY']");
                    fncToastInformation('Please select Sub Category!');
                    return false;
                }
            }

            if (SelectionCheck == false) {
                popUpObjectForSetFocusandOpen = $('#<%=ddlProduct.ClientID %>');
                fncToastInformation('Please select Attributes!');
                return false;
            }
            else if ($('#<%=ddlProduct.ClientID %>').val() == '') {
                popUpObjectForSetFocusandOpen = $('#<%=ddlProduct.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please select Product Name!');
                return false;
            }
            else if ($('#<%=txtProductCost.ClientID %>').val() <= 0) {
                popUpObjectForSetFocusandOpen = $('#<%=txtProductCost.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please enter Product Cost!');
                return false;
            }
            else if ($('#<%=txtMrp.ClientID %>').val() <= 0) {
                popUpObjectForSetFocusandOpen = $('#<%=txtMrp.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please enter Mrp!');
                return false;
            }
            else {

                //fncBindGRNTable();
                XmlGridValue($('#grdItemDetails tr'), $('#<%=hiddenXml.ClientID %>'));
                XmlGridValue($('#grdAttributes tr'), $('#<%=hiddenAttributes.ClientID %>'));

                return true;
            }
}


function ValidateAddItem() {

    var SelectionCheck = false;
    var Checksubcategory = false;
    var subcategoryValue = "";

    $('#grdControlId tr').each(function () {
        if (!this.rowIndex) return;
        var controlId = $(this).find("td").eq(1).html();
        var attibCode = $(this).find("td").eq(0).html();
        if (controlId.toUpperCase() != 'SIZE' && controlId.toUpperCase() != 'SLEEVE') {
            value = $("[id$='" + controlId + "']").find('option:selected').text();
            if (value != 'NONE') {
                SelectionCheck = true;
            }
            if (controlId.toUpperCase() == 'SUB CATEGORY') {
                value = $("[id$='" + controlId + "']").find('option:selected').text();
                subcategoryValue = value;
                if (value != 'NONE') {
                    Checksubcategory = true;
                }
            }
        }
    });

    debugger;

    if (subcategoryValue != "") {
        if (Checksubcategory == false) {
            popUpObjectForSetFocusandOpen = $("[id$='SUB CATEGORY']");
            fncToastInformation('Please select Sub Category!');
            return false;
        }
    }

    if (SelectionCheck == false) {
        popUpObjectForSetFocusandOpen = $('#<%=ddlProduct.ClientID %>');
        fncToastInformation('Please select Attributes!');
        return false;
    }
    else if ($('#<%=ddlProduct.ClientID %>').val() == '') {
        popUpObjectForSetFocusandOpen = $('#<%=ddlProduct.ClientID %>');
        ShowPopupMessageBoxandOpentoObject('Please select Product Name!');
        return false;
    }
    else if ($('#<%=txtProductCost.ClientID %>').val() <= 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtProductCost.ClientID %>');
        ShowPopupMessageBoxandOpentoObject('Please enter Product Cost!');
        return false;
    }
    else if ($('#<%=txtMrp.ClientID %>').val() <= 0) {
        popUpObjectForSetFocusandOpen = $('#<%=txtMrp.ClientID %>');
        ShowPopupMessageBoxandOpentoObject('Please enter Mrp!');
        return false;
    }
    else {

        fncBindGRNTable();
        XmlGridValue($('#grdItemDetails tr'), $('#<%=hiddenXml.ClientID %>'));
        XmlGridValue($('#grdAttributes tr'), $('#<%=hiddenAttributes.ClientID %>'));
        //return true;
    }
}

function XmlGridValue(tableid, XmlID) {
    try {

        var xml = '<NewDataSet>';

        // $('#grdAttribute tr').each(function () {
        tableid.each(function () {
            var cells = $("td", this);
            if (cells.length > 0) {

                xml += "<Table>";
                for (var j = 0; j < cells.length; ++j) {

                    xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';

                }
                xml += "</Table>";
            }
        });

        xml = xml + '</NewDataSet>'
        console.log(xml);
        //alert(xml);

        XmlID.val(escape(xml));
        //alert(XmlID.val());
        //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

function isNumberKeyGA(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 46 && charCode > 31
      && (charCode < 48 || charCode > 57))
        return false;

    if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
        evt.preventDefault();
    } // prevent if 

    if (charCode == 13) {
        if ($(evt.target).closest("td").next().length == 0) {
            // is last  
            Qtytxt = $(evt.target).closest("tr").children('td:first-child').find("input");
            Qtytxt.focus().select();
        }

        Qtytxt = $(evt.target).closest("td").next().find("input");
        Qtytxt.focus().select();
    }
    return true;
}

function fncSetZero() {
    $('#<%=txtProductCost.ClientID%>').val('0');
    $('#<%=txtMrp.ClientID%>').val('0');
    $('#<%=txtMiscelenious.ClientID%>').val('0');
    $('#<%=txtMargin.ClientID%>').val('0');
    $('#<%=txtMarginAmt.ClientID%>').val('0');
    $('#<%=txtGSTPerc.ClientID%>').val('0');
    $('#<%=txtGSTAmt.ClientID%>').val('0');


    $('#<%=txtProductCost1.ClientID%>').val('0');
    $('#<%=txtMrp1.ClientID%>').val('0');
    $('#<%=txtMiscelenious1.ClientID%>').val('0');
    $('#<%=txtMargin1.ClientID%>').val('0');
    $('#<%=txtMarginAmt1.ClientID%>').val('0');
    $('#<%=txtGSTPerc1.ClientID%>').val('0');
    $('#<%=txtGSTAmt1.ClientID%>').val('0');
    //$('#<%=txtProductCost.ClientID%>').focus().select();
}

        function getSortedKeys(obj) {
            var keys = []; for (var key in obj) keys.push(key);
            return keys.sort(function (a, b) { return obj[b] - obj[a] });
        }
        /// Bind Size Grid
        function funBindSizeGrid(jsonObj) {

            try {

                if (jsonObj == null)
                    return;

                debugger;
                // Call Sort By Name
                jsonObj = jsonObj.sort();

                ////Creating table rows 
                var table1 = document.getElementById("grdAttributeSize");
                //$('#grdAttributeSize').find("tr").remove();
                var BodyElement = document.createElement("tbody");

                if (jsonObj.length > 0) {
                    var keys = Object.keys(jsonObj[0]);
                    //keys = keys.filter(e => e !== 'SLEEVE');
                    //keys.splice(0, 0, "SLEEVE");
                    //console.log(arr.join());
                    var rowth = document.createElement("thead");
                    var rowHeader = document.createElement("tr");
                    var cellHeader = null;
                    for (var i = 0; i < keys.length; i++) {
                        cellHeader = document.createElement("th");
                        cellHeader.appendChild(document.createTextNode(keys[i]));
                        rowHeader.appendChild(cellHeader);
                    }
                    rowth.appendChild(rowHeader);
                    table1.appendChild(rowth);
                }


                //Creating table rows
                for (var j = 0; j < jsonObj.length; j++) {
                    var tableRowElement = document.createElement("tr");

                    var tableRowId = "grdAttributeSize_row" + j;
                    tableRowElement.setAttribute("id", tableRowId);
                    //tableElement.setAttribute("class", tableclass);

                    console.log(jsonObj[j]);
                    for (var i = 0; i < keys.length; i++) {
                        var tableCellElement = document.createElement("td");
                        var key = Object.keys(jsonObj[j])[i];
                        var cellText;
                        var value = jsonObj[j][key];
                        //console.log(key);

                        //cellText = document.createTextNode(value);
                        //tableCellElement.contentEditable = "true";
                        //cellText.innerHTML = "<input type='text' class='Margin'onkeypress='return isNumberKeyGA(event);' style='width:100%; text-align:center;'/>";
                        if (key.toUpperCase() != "SLEEVE") {
                            var el = document.createElement('input');
                            el.type = 'text';
                            el.name = key;
                            el.id = key;
                            el.setAttribute("style", "width:100%; text-align:center;");
                            el.setAttribute("MaxLength", "3");
                            //el.setAttribute("onkeypress", "return isNumberKeyGA(this);");
                            el.onkeypress = function (event) { return isNumberKeyGA(event) }

                            tableCellElement.appendChild(el);
                        }
                        else {
                            cellText = document.createTextNode(value);
                            tableCellElement.appendChild(cellText);
                        }

                        tableRowElement.appendChild(tableCellElement);
                    }
                    BodyElement.appendChild(tableRowElement);
                }

                table1.appendChild(BodyElement);

            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Textile Item bind Margin Fix table
        function fncBindGRNTable() {
            try {

                var objSearchData, tblSearchData, Rowno = 0, batchNo = "";
                //debugger;
                tblSearchData = $("#grdItemDetails");
                //tblSearchData.children().remove();
                var Range = 0;
                var Rowno = 0;
                var GrossBasicAmount = 0;
                var TotalValues = 0;
                var GrossGSTAmount = 0;
                var CostPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                var GSTAmount = parseFloat($('#<%=txtGSTAmt.ClientID%>').val());
                var GSTPerc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val());
                var SGSTperc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val()) / 2;
                var Miscelenious = parseFloat($('#<%=txtMiscelenious.ClientID%>').val());
                var Margin = parseFloat($('#<%=txtMargin.ClientID%>').val());
                var MRP = parseFloat($('#<%=txtMrp.ClientID%>').val());

                var CostPrice1 = parseFloat($('#<%=txtProductCost1.ClientID%>').val());
                var GSTAmount1 = parseFloat($('#<%=txtGSTAmt1.ClientID%>').val());
                var GSTPerc1 = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val());
                var SGSTperc1 = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val()) / 2;
                var Miscelenious1 = parseFloat($('#<%=txtMiscelenious1.ClientID%>').val());
                var Margin1 = parseFloat($('#<%=txtMargin1.ClientID%>').val());
                var MRP1 = parseFloat($('#<%=txtMrp1.ClientID%>').val());

                var itemName = "";
                var CheckSize = true;

                $('#grdControlId tr').each(function () {
                    if (!this.rowIndex) return;
                    var controlId = $(this).find("td").eq(1).html();
                    var attibCode = $(this).find("td").eq(0).html();
                    //console.log(controlId);
                    if (controlId.toUpperCase() != 'SIZE' && controlId.toUpperCase() != 'SLEEVE') {
                        value = $("[id$='" + controlId + "']").find('option:selected').text();
                        if (value != 'NONE') {
                            itemName = itemName + "-" + $("[id$='" + controlId + "']").find('option:selected').text();
                            valueCode = $("[id$='" + controlId + "']").find('option:selected').val();
                            createAttributeValueTable('0', attibCode, valueCode);
                        }
                    }
                    if (controlId.toUpperCase() == 'SIZE') {
                        CheckSize = false;
                    }
                });
                var selectedVal = $("[id$=ddlProduct]").find('option:selected').text();
                itemName = selectedVal + itemName;
                console.log(itemName);

                if (CheckSize) {
                    Rowno = parseFloat(Rowno) + parseFloat(1);
                    row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                        '<td align="center">' + Rowno + '</td>' +
                        '<td>' + itemName + '</td>' +
                        '<td align="center">' + $('#<%=txtGRNQuantity.ClientID%>').val() + '</td>' +
                        '<td align="right">' + CostPrice.toFixed(2) + '</td>' +
                        '<td align="right">' + Miscelenious.toFixed(2) + '</td>' +
                        '<td align="right">' + Margin.toFixed(2) + '</td>' +
                        '<td align="right">' + GSTPerc.toFixed(2) + '</td>' +
                        '<td align="right">' + GSTAmount.toFixed(2) + '</td>' +
                        '<td align="right">' + MRP.toFixed(2) + '</td>' +
                        '</tr>';
                    tblSearchData.append(row);
                }
                var icount = 0;
                var dFromPrice = 0;
                var dToPrice = 0;
                var dRange = 0;
                var dMaxRange = 0;
                debugger;

                var InvCount = 1;
                $("#grdAttributeSize tr:gt(0)").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {
                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {

                                    InvCount = j + 1;
                                }
                            }
                        }
                    }
                });



                var selectedVal = $("[id$=ddlPricingType]").val();
                if (selectedVal == 2) {
                    InvCount = InvCount - 1;
                    dFromPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                    dToPrice = parseFloat($('#<%=txtFromRange.ClientID%>').val());
                    Range = (dToPrice - dFromPrice) / InvCount; //CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                    $('#<%=txtToRange.ClientID%>').val(Range);
                    dToPrice = Range;
                }

                if (selectedVal == 2 || selectedVal == 1) {

                    $("#grdAttributeSize tr:gt(0)").each(function () {
                        //$("#grdAttributeSize tr").each(function () {
                        var cells = $("td", this);
                        if (cells.length > 0) {
                            for (var j = 0; j < cells.length; ++j) {
                                //debugger;
                                if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                    if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                        //console.log($("input:text", cells.eq(j)).val());
                                        //console.log(cells.eq(0).text().trim());
                                        // itemCheck = itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text()
                                        header = $(this).parents('table:first').find('th').eq(j).text();
                                        $('#grdSizeValue tr').each(function () {
                                            if (!this.rowIndex) return;
                                            var controlId = $(this).find("td").eq(3).html();
                                            //debugger;
                                            header = header.replace('(', '').replace(')', '');
                                            if (controlId == header) {
                                                console.log(header);
                                                if (cells.eq(0).text().trim() == '') {
                                                    createAttributeValueTable(itemName + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                                }
                                                else {
                                                    createAttributeValueTable(itemName + '-' + cells.eq(0).text().trim() + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                                }
                                            }
                                            if (cells.eq(0).text().trim() == controlId) {
                                                console.log(controlId);
                                                createAttributeValueTable(itemName + '-' + cells.eq(0).text().trim() + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                            }
                                        });

                                        if (cells.eq(0).text().trim().toUpperCase() == 'HALF') {

                                            fncCalculateMrp();
                                            GSTAmount = parseFloat($('#<%=txtGSTAmt.ClientID%>').val());
                                            GSTPerc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val());
                                            SGSTperc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val()) / 2;
                                            Miscelenious = parseFloat($('#<%=txtMiscelenious.ClientID%>').val());
                                            Margin = parseFloat($('#<%=txtMargin.ClientID%>').val());
                                            MRP = parseFloat($('#<%=txtMrp.ClientID%>').val());
                                            Rowno = parseFloat(Rowno) + parseFloat(1);
                                            row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                                '<td align="center">' + Rowno + '</td>' +
                                                '<td>' + itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                                '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                                '<td align="right">' + CostPrice.toFixed(2) + '</td>' +
                                                '<td align="right">' + Miscelenious.toFixed(2) + '</td>' +
                                                '<td align="right">' + Margin.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTPerc.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTAmount.toFixed(2) + '</td>' +
                                                '<td align="right">' + MRP.toFixed(2) + '</td>' +
                                                '</tr>';
                                            tblSearchData.append(row);
                                            CostPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                                            var selectedVal = $("[id$=ddlPricingType]").val();
                                            if (selectedVal == 1) {
                                                Range = CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                            }
                                            else if (selectedVal == 2) {
                                                Range = CostPrice + dToPrice;
                                            }
                                            CostPrice = parseFloat(Range);
                                            $('#<%=txtProductCost.ClientID%>').val(Range);
                                        }
                                        else if (cells.eq(0).text().trim().toUpperCase() == 'FULL') {
                                            debugger;
                                            <%--    InvCount = InvCount - 1;
                                            dFromPrice = parseFloat($('#<%=txtProductCost1.ClientID%>').val());
                                            dToPrice = parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                            Range = (dToPrice - dFromPrice) / InvCount; //CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                            $('#<%=txtToRange.ClientID%>').val(Range);
                                            dToPrice = Range;--%>

                                            fncCalculateMrp2();
                                            GSTAmount1 = parseFloat($('#<%=txtGSTAmt1.ClientID%>').val());
                                            GSTPerc1 = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val());
                                            SGSTperc = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val()) / 2;
                                            Miscelenious = parseFloat($('#<%=txtMiscelenious1.ClientID%>').val());
                                            Margin1 = parseFloat($('#<%=txtMargin1.ClientID%>').val());
                                            MRP1 = parseFloat($('#<%=txtMrp1.ClientID%>').val());

                                            Rowno = parseFloat(Rowno) + parseFloat(1);
                                            row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                                '<td align="center">' + Rowno + '</td>' +
                                                '<td>' + itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                                '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                                '<td align="right">' + CostPrice1.toFixed(2) + '</td>' +
                                                '<td align="right">' + Miscelenious1.toFixed(2) + '</td>' +
                                                '<td align="right">' + Margin1.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTPerc1.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTAmount1.toFixed(2) + '</td>' +
                                                '<td align="right">' + MRP1.toFixed(2) + '</td>' +
                                                '</tr>';
                                            tblSearchData.append(row);
                                            CostPrice1 = parseFloat($('#<%=txtProductCost1.ClientID%>').val());
                                            var selectedVal = $("[id$=ddlPricingType]").val();
                                            if (selectedVal == 1) {
                                                Range = CostPrice1 + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                            }
                                            else if (selectedVal == 2) {
                                                Range = CostPrice1 + dToPrice;
                                            }
                                            CostPrice1 = parseFloat(Range);
                                            $('#<%=txtProductCost1.ClientID%>').val(Range);
                                        }
                                        else {
                                            //debugger;
                                            fncCalculateMrp();
                                            GSTAmount = parseFloat($('#<%=txtGSTAmt.ClientID%>').val());
                                            GSTPerc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val());
                                            SGSTperc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val()) / 2;
                                            Miscelenious = parseFloat($('#<%=txtMiscelenious.ClientID%>').val());
                                            Margin = parseFloat($('#<%=txtMargin.ClientID%>').val());
                                            MRP = parseFloat($('#<%=txtMrp.ClientID%>').val());

                                            Rowno = parseFloat(Rowno) + parseFloat(1);
                                            row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                                '<td align="center">' + Rowno + '</td>' +
                                                '<td>' + itemName + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                                '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                                '<td align="right">' + CostPrice.toFixed(2) + '</td>' +
                                                '<td align="right">' + Miscelenious.toFixed(2) + '</td>' +
                                                '<td align="right">' + Margin.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTPerc.toFixed(2) + '</td>' +
                                                '<td align="right">' + GSTAmount.toFixed(2) + '</td>' +
                                                '<td align="right">' + MRP.toFixed(2) + '</td>' +
                                                '</tr>';
                                            tblSearchData.append(row);

                                            CostPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                                            var selectedVal = $("[id$=ddlPricingType]").val();
                                            if (selectedVal == 1) {
                                                Range = CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                            }
                                            else if (selectedVal == 2) {
                                                Range = CostPrice + dToPrice;
                                            }
                                            CostPrice = parseFloat(Range);
                                            $('#<%=txtProductCost.ClientID%>').val(Range);
                                        }

                                }
                            }
                        }
                    }
                    });
            }
            else if (selectedVal == 3) {

                var Center = 0;
                Center = Math.round(InvCount / 2);
                var GrdRowNo = 1;
                $("#grdAttributeSize tr:gt(0)").each(function () {
                    //$("#grdAttributeSize tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        for (var j = 0; j < cells.length; ++j) {
                            //debugger;
                            if ($(this).parents('table:first').find('th').eq(j).text().toUpperCase() != 'SLEEVE') {
                                if ($("input:text", cells.eq(j)).val() > 0 && $("input:text", cells.eq(j)).val() != '') {
                                    //console.log($("input:text", cells.eq(j)).val());
                                    //console.log(cells.eq(0).text().trim());
                                    // itemCheck = itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text()
                                    header = $(this).parents('table:first').find('th').eq(j).text();
                                    $('#grdSizeValue tr').each(function () {
                                        if (!this.rowIndex) return;
                                        var controlId = $(this).find("td").eq(3).html();
                                        //debugger;
                                        header = header.replace('(', '').replace(')', '');
                                        if (controlId == header) {
                                            console.log(header);
                                            if (cells.eq(0).text().trim() == '') {
                                                createAttributeValueTable(itemName + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                            }
                                            else {
                                                createAttributeValueTable(itemName + '-' + cells.eq(0).text().trim() + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                            }
                                        }
                                        if (cells.eq(0).text().trim() == controlId) {
                                            console.log(controlId);
                                            createAttributeValueTable(itemName + '-' + cells.eq(0).text().trim() + '-' + header, $(this).find("td").eq(0).html(), $(this).find("td").eq(2).html());
                                        }
                                    });

                                    if (cells.eq(0).text().trim().toUpperCase() == 'HALF') {

                                        fncCalculateMrp();
                                        GSTAmount = parseFloat($('#<%=txtGSTAmt.ClientID%>').val());
                                        GSTPerc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val());
                                        SGSTperc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val()) / 2;
                                        Miscelenious = parseFloat($('#<%=txtMiscelenious.ClientID%>').val());
                                        Margin = parseFloat($('#<%=txtMargin.ClientID%>').val());
                                        MRP = parseFloat($('#<%=txtMrp.ClientID%>').val());
                                        Rowno = parseFloat(Rowno) + parseFloat(1);
                                        row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                            '<td align="center">' + Rowno + '</td>' +
                                            '<td>' + itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                            '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                            '<td align="right">' + CostPrice.toFixed(2) + '</td>' +
                                            '<td align="right">' + Miscelenious.toFixed(2) + '</td>' +
                                            '<td align="right">' + Margin.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTPerc.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTAmount.toFixed(2) + '</td>' +
                                            '<td align="right">' + MRP.toFixed(2) + '</td>' +
                                            '</tr>';
                                        tblSearchData.append(row);
                                        CostPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                                        var selectedVal = $("[id$=ddlPricingType]").val();
                                        if (selectedVal == 1) {
                                            Range = CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                        }
                                        else if (selectedVal == 2) {
                                            Range = CostPrice + dToPrice;
                                        }
                                        CostPrice = parseFloat(Range);
                                        $('#<%=txtProductCost.ClientID%>').val(Range);
                                    }
                                    else if (cells.eq(0).text().trim().toUpperCase() == 'FULL') {
                                        debugger;
                                        fncCalculateMrp2();
                                        GSTAmount1 = parseFloat($('#<%=txtGSTAmt1.ClientID%>').val());
                                        GSTPerc1 = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val());
                                        SGSTperc = parseFloat($('#<%=txtGSTPerc1.ClientID%>').val()) / 2;
                                        Miscelenious = parseFloat($('#<%=txtMiscelenious1.ClientID%>').val());
                                        Margin1 = parseFloat($('#<%=txtMargin1.ClientID%>').val());
                                        MRP1 = parseFloat($('#<%=txtMrp1.ClientID%>').val());

                                        Rowno = parseFloat(Rowno) + parseFloat(1);
                                        row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                            '<td align="center">' + Rowno + '</td>' +
                                            '<td>' + itemName + '-' + cells.eq(0).text().trim() + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                            '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                            '<td align="right">' + CostPrice1.toFixed(2) + '</td>' +
                                            '<td align="right">' + Miscelenious1.toFixed(2) + '</td>' +
                                            '<td align="right">' + Margin1.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTPerc1.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTAmount1.toFixed(2) + '</td>' +
                                            '<td align="right">' + MRP1.toFixed(2) + '</td>' +
                                            '</tr>';
                                        tblSearchData.append(row);
                                        CostPrice1 = parseFloat($('#<%=txtProductCost1.ClientID%>').val());
                                        var selectedVal = $("[id$=ddlPricingType]").val();
                                        if (selectedVal == 1) {
                                            Range = CostPrice1 + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                        }
                                        else if (selectedVal == 2) {
                                            Range = CostPrice1 + dToPrice;
                                        }
                                        CostPrice1 = parseFloat(Range);
                                        $('#<%=txtProductCost1.ClientID%>').val(Range);
                                    }
                                    else {
                                        //debugger;

                                        if (GrdRowNo < Center)
                                        {
                                            CostPrice = CostPrice - Range;
                                        }                                            
                                        else if (GrdRowNo > Center)
                                        {
                                            CostPrice = CostPrice + Range;
                                        }                                           
                                        
                                        fncCalculateMrp();
                                        GSTAmount = parseFloat($('#<%=txtGSTAmt.ClientID%>').val());
                                        GSTPerc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val());
                                        SGSTperc = parseFloat($('#<%=txtGSTPerc.ClientID%>').val()) / 2;
                                        Miscelenious = parseFloat($('#<%=txtMiscelenious.ClientID%>').val());
                                        Margin = parseFloat($('#<%=txtMargin.ClientID%>').val());
                                        MRP = parseFloat($('#<%=txtMrp.ClientID%>').val());

                                        Rowno = parseFloat(Rowno) + parseFloat(1);
                                        row = "<tr id='trGRNItem_" + Rowno + "' tabindex='" + Rowno + "' >" +
                                            '<td align="center">' + Rowno + '</td>' +
                                            '<td>' + itemName + '-' + $(this).parents('table:first').find('th').eq(j).text().replace("(", "").replace(")", "") + '</td>' +
                                            '<td align="center">' + $("input:text", cells.eq(j)).val() + '</td>' +
                                            '<td align="right">' + CostPrice.toFixed(2) + '</td>' +
                                            '<td align="right">' + Miscelenious.toFixed(2) + '</td>' +
                                            '<td align="right">' + Margin.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTPerc.toFixed(2) + '</td>' +
                                            '<td align="right">' + GSTAmount.toFixed(2) + '</td>' +
                                            '<td align="right">' + MRP.toFixed(2) + '</td>' +
                                            '</tr>';
                                        tblSearchData.append(row);

                                        CostPrice = parseFloat($('#<%=txtProductCost.ClientID%>').val());
                                        var selectedVal = $("[id$=ddlPricingType]").val();
                                        if (selectedVal == 1) {
                                            Range = CostPrice + parseFloat($('#<%=txtFromRange.ClientID%>').val());
                                        }
                                        else if (selectedVal == 2) {
                                            Range = CostPrice + dToPrice;
                                        }
                                        CostPrice = parseFloat(Range);
                                        $('#<%=txtProductCost.ClientID%>').val(Range);

                                        GrdRowNo = GrdRowNo + 1;
                                    }

                            }

                        }

                    }

                }

                });
             }

            fncSetZero();
            //$('#grdControlId').find("tr:gt(0)").remove();
    }
    catch (err) {
        fncToastError(err.message);
    }

}


function showpreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
        }
    }

    function fncShowItemEditDailog(Gidno) {
        $(function () {

            $("#ItemEdit").html("GRN Created Successfully, " + "<H1><B>Style Code:" + Gidno + "</B></H1>");
            $("#ItemEdit").dialog({
                title: "Enterpriser Web",
                buttons: {
                    "Ok": function () {
                        $(this).dialog("destroy");
                        window.location.href = "frmGoodsAcknowledgement.aspx";
                    }
                },
                modal: true
            });
        });
    };


    function roundOff(figure, precision) {
        debugger;
        figure = parseInt(figure);
        reminder = figure % 5;
        var stringval = figure + ""
        var lastChar = stringval.slice(-1);
        if (stringval.length > 1) {
            if (lastChar >= 5) {
                refs = (figure - reminder) + 5;
            }
            else {
                refs = (figure - reminder);
            }
        }
        else
            refs = figure;

        // Round Up to 5 when 2.5
        //if (figure == 0)
        //{
        //    refs = figure;
        //}
        //else
        //{
        //    if (value < 2.5) {
        //        figure = figure - value;
        //    }
        //    else {
        //        figure = (figure + 5) - value;
        //    }
        //}

        // Round Up to 5 
        //if (value % 5 == 0) {
        //    refs = parseInt(Math.floor(value / 5)) * 5;
        //} else {
        //    refs = (parseInt(Math.floor(value / 5)) * 5) + 5;
        //}

        return refs;
    };

    function fncCalculateMrp() {

        var productcost = 0;
        var miscelenious = 0;
        var margin = 0;
        var marginAmt = 0;
        var gstperc = 0;
        var gstamt = 0;
        var mrp = 0;

        var objProductcost = $('#<%=txtProductCost.ClientID%>');
        var objMrp = $('#<%=txtMrp.ClientID%>');
        var objMiscelenious = $('#<%=txtMiscelenious.ClientID%>');
        var objMargin = $('#<%=txtMargin.ClientID%>');
        var objGSTperc = $('#<%=txtGSTPerc.ClientID%>');
        var objGSTamount = $('#<%=txtGSTAmt.ClientID%>');
        var objMarginamount = $('#<%=txtMarginAmt.ClientID%>');

        productcost = objProductcost.val();
        miscelenious = objMiscelenious.val();
        margin = objMargin.val();

        miscelenious = parseFloat(productcost) + parseFloat(miscelenious)
        marginAmt = (miscelenious * parseFloat(margin)) / 100;
        marginAmt = marginAmt + miscelenious;
        objMarginamount.val(marginAmt.toFixed(2));
        debugger;
        if (marginAmt > 1000) {
            objGSTperc.val('12.00');
            gstamt = (marginAmt * 12) / 100;
            objGSTamount.val(gstamt.toFixed(2));
            mrp = gstamt + marginAmt;
            mrp = roundOff(mrp.toFixed(2), 5);
            objMrp.val(mrp.toFixed(2));
        }
        else {
            objGSTperc.val('5.00');
            gstamt = (marginAmt * 5) / 100;
            objGSTamount.val(gstamt.toFixed(2));
            mrp = gstamt + marginAmt;
            mrp = roundOff(mrp.toFixed(2), 5);
            objMrp.val(mrp.toFixed(2));
        }

        console.log(mrp);

    }

    function fncCalculateMrp2() {

        var productcost = 0;
        var miscelenious = 0;
        var margin = 0;
        var marginAmt = 0;
        var gstperc = 0;
        var gstamt = 0;
        var mrp = 0;

        var objProductcost = $('#<%=txtProductCost1.ClientID%>');
        var objMrp = $('#<%=txtMrp1.ClientID%>');
        var objMiscelenious = $('#<%=txtMiscelenious1.ClientID%>');
        var objMargin = $('#<%=txtMargin1.ClientID%>');
        var objGSTperc = $('#<%=txtGSTPerc1.ClientID%>');
        var objGSTamount = $('#<%=txtGSTAmt1.ClientID%>');
        var objMarginamount = $('#<%=txtMarginAmt1.ClientID%>');

        productcost = objProductcost.val();
        miscelenious = objMiscelenious.val();
        margin = objMargin.val();

        miscelenious = parseFloat(productcost) + parseFloat(miscelenious)
        marginAmt = (miscelenious * parseFloat(margin)) / 100;
        marginAmt = marginAmt + miscelenious;
        objMarginamount.val(marginAmt.toFixed(2));

        if (marginAmt > 1000) {
            objGSTperc.val('12.00');
            gstamt = (marginAmt * 12) / 100;
            objGSTamount.val(gstamt.toFixed(2));
            mrp = gstamt + marginAmt;
            mrp = roundOff(mrp.toFixed(2), 5);
            objMrp.val(mrp.toFixed(2));
        }
        else {
            objGSTperc.val('5.00');
            gstamt = (marginAmt * 5) / 100;
            objGSTamount.val(gstamt.toFixed(2));
            mrp = gstamt + marginAmt;
            mrp = roundOff(mrp.toFixed(2), 5);
            objMrp.val(mrp.toFixed(2));
        }

        console.log(marginAmt);

    }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="breadcrumbs" class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">GRN Entry</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="container-group-price">
        <div class="container-left-grn" id="pnlFilter" runat="server" style="width: 45%;">
            <div class="container-group-small" style="margin-top: 5px; width: 100%;">
                <div class="container-control">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label2" runat="server" Text="CATEGORY NAME"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:DropDownList ID="ddlProduct" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group-single-res" id="dynamicInput">
                    </div>
                    <div class="control-group-single-res" id="dynamicInput1">
                        <table id="grdAttributeSize" cellspacing="0" rules="all" border="1">
                        </table>
                    </div>
                    <div class="control-group-single-res" id="divQty" style="display: none;">
                        <div class="label-left">
                            <asp:Label ID="Label7" runat="server" Text="QUANTITY"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGRNQuantity" runat="server" onkeypress="return isNumberKeyGA(event)" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res" style="display: none;" id="dynamicControls">
                        <table id="grdControlId" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">AttributeCode
                                    </th>
                                    <th scope="col">AttributeName
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <table id="grdAttributes" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">SNo</th>
                                    <th scope="col">AttributeCode</th>
                                    <th scope="col">ValueCode</th>
                                </tr>
                            </thead>
                        </table>
                        <table id="grdSizeValue" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">AttributeCode
                                    </th>
                                    <th scope="col">AttributeName
                                    </th>
                                    <th scope="col">ValueCode
                                    </th>
                                    <th scope="col">ValueName
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div class="control-group-single-res">
                        <asp:Image ID="imgpreview" runat="server" ImageAlign="AbsMiddle" Height="150px" Width="250px" ImageUrl="~/images/No-image-available.jpg" />
                        <%--<asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />--%>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                            <Triggers>
                                <asp:PostBackTrigger ControlID="lnkAdd" />
                            </Triggers>
                            <ContentTemplate>
                                <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>




        </div>
        <div class="container-right-grn" style="width: 54%; height: 100%;" id="HideFilter_ContainerRight" runat="server">
            <div class="control-group-single-res" style="padding: 4px 7px 2px 4px;">

                <div class="col-md-6" style="padding: 10px; display: none; text-align: left; border: 1px solid #d8d9d8; background-color: #f3f3f3;" id="divHalf">
                    <div class="textiles-price-header" id="priceHrd">
                        Half Sleeve
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="blss" runat="server" Text="Cost"></asp:Label>
                            <span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtProductCost" runat="server" onkeypress="return isNumberKeyGA(event)" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text="Others"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMiscelenious" runat="server" onkeypress="return isNumberKeyGA(event)" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Margin %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMargin" runat="server" onkeypress="return isNumberKeyGA(event)" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label8" runat="server" Text="Basic"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMarginAmt" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label5" runat="server" Text="GST %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTPerc" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text="GST"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTAmt" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label6" runat="server" Text="MRP"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMrp" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-md-6" style="padding: 10px; display: none; text-align: left; border: 1px solid #d8d9d8; background-color: #f3f3f3;" id="divFull">
                    <div class="textiles-price-header">
                        Full Sleeve
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text="Cost"></asp:Label>
                            <span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtProductCost1" runat="server" oninput="fncCalculateMrp2()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label10" runat="server" Text="Others"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMiscelenious1" runat="server" oninput="fncCalculateMrp2()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label11" runat="server" Text="Margin %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMargin1" runat="server" oninput="fncCalculateMrp2()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label12" runat="server" Text="Basic"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMarginAmt1" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label13" runat="server" Text="GST %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTPerc1" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label14" runat="server" Text="GST"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTAmt1" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label15" runat="server" Text="MRP"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMrp1" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>

            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12" style="padding: 10px; display: block; text-align: left; border: 1px solid #d8d9d8; background-color: #f3f3f3;" id="divHeader">
                        <div class="col-md-3">
                            <div class="label-left" style="margin-left: 3px;">
                                <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Price Setting"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlPricingType" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Text="None" Value="0" />
                                    <asp:ListItem Text="Variation" Value="1" />
                                    <asp:ListItem Text="Maximum Range" Value="2" />
                                    <asp:ListItem Text="Center" Value="3" />
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2" id="divRangeFrom" style="display: none;">
                            <div class="label-left" style="margin-left: 3px;">
                                <asp:Label ID="Label17" runat="server" Font-Bold="true" Text="To Range"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromRange" runat="server" Style="text-align: right;" onkeypress="return isNumberKeyGA(this);" Text="0" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2" id="divRangeTo" style="display: none;">
                            <div class="label-left" style="margin-left: 3px;">
                                <asp:Label ID="Label19" runat="server" Font-Bold="true" Text="To Range"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToRange" runat="server" Style="text-align: right;" onkeypress="return isNumberKeyGA(this);" Text="0" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2" id="divJumper" style="display: none;">
                            <div class="label-left" style="margin-left: 3px;">
                                <asp:Label ID="Label18" runat="server" Font-Bold="true" Text="Jumper Value"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtJumper" runat="server" onkeypress="return isNumberKeyGA(this);" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAddItem" runat="server" class="button-red" OnClientClick="ValidateAddItem(); return false;"><i class="icon-play"></i>Add Item</asp:LinkButton>
                                <asp:LinkButton ID="lnkAdd" Style="display: none; margin-left: 5px;" runat="server" class="button-red" OnClientClick="return ValidateForm();" OnClick="lnkSave_Click"><i class="icon-play"></i>Create GRN</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="attr_fixed_headers gid_LastPurchase">
                <table id="grdItemDetails" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <%--  <th scope="col" class="hiddencol">AttributeCode
                            </th>--%>
                            <th scope="col">S.No</th>
                            <th scope="col">ItemName</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Cost</th>
                            <th scope="col">Miscelenious</th>
                            <th scope="col">Margin</th>
                            <th scope="col">GST</th>
                            <th scope="col">GSTAmount</th>
                            <th scope="col">MRP</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="button-contol" style="display: none;">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="ValidateForm();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="return fncClear(); return false;" class="button-red"><i class="icon-play"></i>Clear</asp:LinkButton>
                </div>
            </div>

        </div>

        <div id="ItemEdit" class="display_none">
        </div>
        <asp:HiddenField ID="hiddenXml" runat="server" Value="" />
        <asp:HiddenField ID="hiddenAttributes" runat="server" Value="" />
        <asp:HiddenField ID="hiddenjson" runat="server" Value="" />
        <asp:HiddenField ID="hidbillType" runat="server" Value="" />
        <asp:HiddenField ID="hidGidLocationcode" runat="server" Value="" />
        <asp:HiddenField ID="hidVendorCode" runat="server" Value="" />
        <asp:HiddenField ID="hidInvoiceNo" runat="server" Value="" />
        <asp:HiddenField ID="hidDONo" runat="server" Value="" />
        <asp:HiddenField ID="hidGANo" runat="server" Value="" />
        <asp:HiddenField ID="hidGIDNo" runat="server" Value="" />

    </div>

</asp:Content>
