﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseOrderEntry.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrderEntry" %>

<%@ Register TagPrefix="ups" TagName="ItemHistory" Src="~/UserControls/ItemHistory.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        #dialog-ViewItemHistory {
            width: 100% !important;
        }

        .container-inv-history {
            width: 100% !important;
        }

        .cust-pages {
            display: none !important;
        }


        /****************Vendor Inventory CSS Style****************/

        .po td:nth-child(1), .po th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .po td:nth-child(2), .po th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .po td:nth-child(3), .po th:nth-child(3) {
            min-width: 300px;
            max-width: 300px;
        }

        .po td:nth-child(4), .po th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .po td:nth-child(5), .po th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .po td:nth-child(6), .po th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(6) {
            text-align: right !important;
        }


        .po td:nth-child(7), .po th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(8), .po th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(9), .po th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(10), .po th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(11), .po th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(12), .po th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(13), .po th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(14), .po th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(15), .po th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(16), .po th:nth-child(16) {
            min-width: 111px;
            max-width: 111px;
        }

        .po td:nth-child(17), .po th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(18), .po th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(19), .po th:nth-child(19) {
            min-width: 104px;
            max-width: 104px;
        }

        .po td:nth-child(20), .po th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(21), .po th:nth-child(21) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(22), .po th:nth-child(22) {
            min-width: 120px;
            max-width: 120px;
        }

        .po td:nth-child(23), .po th:nth-child(23) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(24), .po th:nth-child(24) {
            min-width: 118px;
            max-width: 118px;
        }

        .po td:nth-child(25), .po th:nth-child(25) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(26), .po th:nth-child(26) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(27), .po th:nth-child(27) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(28), .po th:nth-child(28) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(29), .po th:nth-child(29) {
            min-width: 100px;
            max-width: 100px;
        }

        .po td:nth-child(30), .po th:nth-child(30) {
            min-width: 100px;
            max-width: 100px;
        }


        /****************Selected Inventory CSS Style****************/

        .poSelect td:nth-child(1), .poSelect th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
            text-align: center !important;
        }

        .poSelect td:nth-child(2), .poSelect th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .poSelect td:nth-child(3), .poSelect th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(4), .poSelect th:nth-child(4) {
            min-width: 300px;
            max-width: 300px;
        }

        .poSelect td:nth-child(5), .poSelect th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(6), .poSelect th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(6) {
            text-align: right !important;
        }


        .poSelect td:nth-child(7), .poSelect th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(8), .poSelect th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(9), .poSelect th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(10), .poSelect th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(11), .poSelect th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(12), .poSelect th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(13), .poSelect th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(14), .poSelect th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(15), .poSelect th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(16), .poSelect th:nth-child(16) {
            min-width: 111px;
            max-width: 111px;
        }

        .poSelect td:nth-child(17), .poSelect th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(18), .poSelect th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(19), .poSelect th:nth-child(19) {
            min-width: 104px;
            max-width: 104px;
        }

        .poSelect td:nth-child(20), .poSelect th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(21), .poSelect th:nth-child(21) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(22), .poSelect th:nth-child(22) {
            min-width: 120px;
            max-width: 120px;
        }

        .poSelect td:nth-child(23), .poSelect th:nth-child(23) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(24), .poSelect th:nth-child(24) {
            min-width: 118px;
            max-width: 118px;
        }

        .poSelect td:nth-child(25), .poSelect th:nth-child(25) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(26), .poSelect th:nth-child(26) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(27), .poSelect th:nth-child(27) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(28), .poSelect th:nth-child(28) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(29), .poSelect th:nth-child(29) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSelect td:nth-child(30), .poSelect th:nth-child(30) {
            min-width: 100px;
            max-width: 100px;
        }


        /**************** Item History CSS Style****************/

        .ReGeneratePO_Itemhistory td:nth-child(1), .ReGeneratePO_Itemhistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(2), .ReGeneratePO_Itemhistory th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(3), .ReGeneratePO_Itemhistory th:nth-child(3) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(4), .ReGeneratePO_Itemhistory th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(5), .ReGeneratePO_Itemhistory th:nth-child(5) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(6), .ReGeneratePO_Itemhistory th:nth-child(6) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(7), .ReGeneratePO_Itemhistory th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(8), .ReGeneratePO_Itemhistory th:nth-child(8) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(9), .ReGeneratePO_Itemhistory th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchaseOrderEntry');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchaseOrderEntry";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <%------------------------------------------- DateTimePicker -------------------------------------------%>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        });


    </script>
    <script type="text/javascript">
        var txtItemCodeAdd;
        var txtItemNameAdd;
        var txtTotalDiscPrc;
        var txtTotalDiscRs;
        var txtTotal;
        var txtSubTotal;
        var txtGST;
        var txtNetTotal;
        var sStatus;
        var hdfTaxType;
    </script>
    <%------------------------------------------- Page Load -------------------------------------------%>
    <script type="text/javascript">

        function pageLoad() {

            //$('#emptyrow').css("display","none");

            fncVendorDetailLoad();
            //============================  Assign Variable for Simply =========================//

            txtItemCodeAdd = $('#<%=txtItemCodeAdd.ClientID %>');
            txtItemNameAdd = $('#<%=txtItemNameAdd.ClientID %>');
            txtTotalDiscPrc = $('#<%=txtTotalDiscPrc.ClientID %>');
            txtTotalDiscRs = $('#<%=txtTotalDiscRs.ClientID %>');
            txtTotal = $('#<%=txtTotal.ClientID %>');
            txtSubTotal = $('#<%=txtSubTotal.ClientID %>');
            txtNetTotal = $('#<%=txtNetTotal.ClientID %>');
            txtGST = $('#<%=txtGST.ClientID %>');
            hdfTaxType = $('#<%=hdfTaxType.ClientID %>');

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });

            //------------------------------------------- Get Item Name ----------------------------------------//
            $("[id$=txtItemCodeAdd]").autocomplete({
                source: function (request, response) {

                    if ($('#<%=txtVendor.ClientID %>').val() == "") {
                        popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('Please Select Vendor Code');
                        $("[id$=txtItemCodeAdd]").val('');
                        //$("[id$=ddlVendorCode]").focus();
                        //$("[id$=ddlVendorCode]").trigger("liszt:open");
                        return false;
                    }

                    $.ajax({
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/GetFilterValue") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });

                },

                focus: function (event, i) {
                    $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(i.item.val));
                    event.preventDefault();
                },

                //==========================> After Select Value
                select: function (e, i) {
                    $("[id$=hfInvCode]").val(i.item.val);
                    $("[id$=txtItemCodeAdd]").val(i.item.val);
                    $("[id$=txtItemNameAdd]").val(i.item.desc);

                    $('#<%=txtLQty.ClientID %>').focus().select();

                    return false;
                },

                minLength: 2
            });

            //------------------------------------------- Item Code Focus Out ----------------------------------------//
            txtItemCodeAdd.focusout(function () {
                if ($('#<%=txtVendor.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('Please Select Vendor Code');
                    $("[id$=txtItemCodeAdd]").val('');
                    //$("[id$=ddlVendorCode]").focus();
                    //$("[id$=ddlVendorCode]").trigger("liszt:open");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/GetAddInvDetail") %>',
                    data: "{'InvCode':'" + txtItemCodeAdd.val() + "','VendorCode':'" + $('#<%=txtVendor.ClientID %>').val() + "','FromDate':'" + $('#<%=txtPurchaseDate.ClientID %>').val() + "','ToDate':'" + $('#<%=txtPurchaseDate.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $('#<%=txtMrp.ClientID %>').val(msg.d[0]);
                        $('#<%=txtBasicCost.ClientID %>').val(msg.d[1]);
                        $('#<%=txtDiscPrc.ClientID %>').val(msg.d[2]);
                        $('#<%=txtDiscAmt.ClientID %>').val(msg.d[3]);
                        $('#<%=txtGrossCost.ClientID %>').val(msg.d[4]);
                        $('#<%=txtSellingPrice.ClientID %>').val(msg.d[5]);
                        $('#<%=txtSDPrc.ClientID %>').val(msg.d[6]);
                        $('#<%=txtSDAmt.ClientID %>').val(msg.d[7]);
                        $('#<%=txtSGST.ClientID %>').val(msg.d[8]);
                        $('#<%=txtCGST.ClientID %>').val(msg.d[9]);
                        $('#<%=txtIGST.ClientID %>').val(msg.d[10]);
                        $('#<%=hdfSGSTCode.ClientID %>').val(msg.d[11]);
                        $('#<%=hdfCGSTCode.ClientID %>').val(msg.d[12]);
                        $('#<%=hdfIGSTCode.ClientID %>').val(msg.d[13]);
                        $('#<%=ddlWUom.ClientID %>').val(msg.d[14]);
                        $('#<%=ddlWUom.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlLUOM.ClientID %>').val(msg.d[14]);
                        $('#<%=ddlLUOM.ClientID %>').trigger("liszt:updated");
                        $('#<%=txtItemCodeAdd.ClientID %>').val(msg.d[15]);
                        $('#<%=txtItemNameAdd.ClientID %>').val(msg.d[16]);
                        $('#<%=txtCESS.ClientID %>').val(msg.d[17]);

                        $('#<%=txtNQty.ClientID %>').val('0');
                        $('#<%=txtLQty.ClientID %>').val('0');
                        $('#<%=txtNet.ClientID %>').val('0.00');
                        $('#<%=txtNetTotalAdd.ClientID %>').val('0.00');

                        $('#<%=txtLQty.ClientID %>').focus().select();
                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });
            });


            //============================> For Maintain Current Tab while postback
            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=hdfTabName]").val() : "VendorWiseInv";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs a").click(function () {
                    $("[id*=hdfTabName]").val($(this).attr("href").replace("#", ""));

                    //============================> For Change Tab                    
                    var target = $("[id*=hdfTabName]").val();

                    if (target == "ViewAddInv") {
                        $("#<%=btnAddGrid.ClientID %>").click();
                }

                });
            });

            //------------------------------------------- L Qty Lost Focus -------------------------------------------------//

        $('#<%=txtLQty.ClientID %>').focusout(function () {
                try {
                    var LQty = $('#<%=txtLQty.ClientID%>').val();
                    $('#<%=txtNQty.ClientID%>').val(LQty);

                    fncCalcNetAmount(); //Calc Net Amount
                }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        });

            //------------------------------------------- N Qty Lost Focus -------------------------------------------------//

            $('#<%=txtNQty.ClientID %>').focusout(function () {
                try {
                    var NQty = $('#<%=txtNQty.ClientID%>').val();
                    $('#<%=txtLQty.ClientID%>').val(NQty);

                    fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- Basic Cost Lost Focus -------------------------------------------------//

            $('#<%=txtBasicCost.ClientID %>').focusout(function () {
                try {
                    var BasicCost = $('#<%=txtBasicCost.ClientID%>').val();
                    if (BasicCost != '') {
                        $('#<%=txtBasicCost.ClientID%>').val(parseFloat(BasicCost).toFixed(2));
                        $('#<%=txtGrossCost.ClientID%>').val(parseFloat(BasicCost).toFixed(2));
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- Discount Prc Lost Focus -------------------------------------------------//

            $('#<%=txtDiscPrc.ClientID %>').focusout(function () {
                try {
                    var DiscPrc = $('#<%=txtDiscPrc.ClientID%>').val();
                    if (DiscPrc == '') {
                        return false;
                    }
                    $('#<%=txtDiscPrc.ClientID%>').val(parseFloat(DiscPrc).toFixed(2));
                    var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                    var DiscAmt = ((GrossCost * DiscPrc) / (100));
                    $('#<%=txtDiscAmt.ClientID%>').val(parseFloat(DiscAmt).toFixed(2));
                    var GrossCostFinal = parseFloat(GrossCost).toFixed(2) - parseFloat(DiscAmt).toFixed(2);
                    $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCostFinal).toFixed(2));

                    fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });

            //------------------------------------------- SD PRC Cost Lost Focus -------------------------------------------------//

            $('#<%=txtSDPrc.ClientID %>').focusout(function () {
                try {
                    var SDPrc = $('#<%=txtSDPrc.ClientID%>').val();
                    if (SDPrc == '') {
                        return false;
                    }
                    $('#<%=txtSDPrc.ClientID%>').val(parseFloat(SDPrc).toFixed(2));
                    var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                    var SDAmt = ((GrossCost * SDPrc) / (100));
                    $('#<%=txtSDAmt.ClientID%>').val(parseFloat(SDAmt).toFixed(2));
                    var GrossCostFinal = parseFloat(GrossCost).toFixed(2) - parseFloat(SDAmt).toFixed(2);
                    $('#<%=txtGrossCost.ClientID%>').val(parseFloat(GrossCostFinal).toFixed(2));

                    fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });


            function fncCalcNetAmount() {
                debugger;
                var NQty = $('#<%=txtNQty.ClientID%>').val();
                if (NQty != "" && NQty != "0") {
                    //===============> Net Amt Calc

                    if (hdfTaxType.val() == 'S') {
                        var txtSGST = $('#<%=txtSGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtSGST.ClientID%>').val();
                        var txtCGST = $('#<%=txtCGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtCGST.ClientID%>').val()
                        var TotalGstPerc = parseFloat(txtSGST) + parseFloat(txtCGST);
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var TotalGstAmt = ((GrossCost * TotalGstPerc) / (100));

                        NetAmt = parseFloat(GrossCost) + parseFloat(TotalGstAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else if (hdfTaxType.val() == 'I') {
                        var txtIGST = $('#<%=txtIGST.ClientID%>').val();
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
                        }

                        NetAmt = parseFloat(GrossCost) + parseFloat(IGSTAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else if (hdfTaxType.val() == 'Import') {
                        var txtIGST = $('#<%=txtIGST.ClientID%>').val();
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));
                        }

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else {
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();

                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(2));

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(2));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2);
                        $('#<%=txtNetTotalAdd.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }

        }
    }

            //------------------------------------------- Brand Change Event -------------------------------------------------//

    $('#<%=ddlBrandCode.ClientID %>').change(function () {

                var sVendorCode = $('#<%=txtVendor.ClientID%>').val();
        if (sVendorCode == "-- Select --") {
            ShowPopupMessageBox('Please Select Vendor Code');
            $('#<%=ddlBrandCode.ClientID %>').val(0);
        }
    });

            //------------------------------------------ Total Discount Focus Out --------------------------------------------//
    txtTotalDiscPrc.focusout(function () {
        txtTotalDiscPrc.val(txtTotalDiscPrc.val() == '' ? '0' : parseFloat(txtTotalDiscPrc.val()).toFixed(2));
        var Total = txtTotal.val() == '' ? '0' : txtTotal.val();
        var TotalDiscPrc = txtTotalDiscPrc.val() == '' ? '0' : txtTotalDiscPrc.val();
        var TotalDiscRs = (Total * TotalDiscPrc) / 100;
        txtTotalDiscRs.val(parseFloat(TotalDiscRs).toFixed(2));
        var SubTotal = parseFloat(Total).toFixed(2) - parseFloat(TotalDiscRs).toFixed(2);
        txtSubTotal.val(SubTotal);
        var GST = txtGST.val() == '' ? '0' : txtGST.val();
        var NetTotal = parseFloat(SubTotal).toFixed(2) + parseFloat(GST).toFixed(2);
        txtNetTotal.val(parseFloat(NetTotal).toFixed(2));
    });

}
    </script>
    <%------------------------------------------- Add Inventory Button Click -------------------------------------------%>
    <script type="text/javascript">
        function AddbuttonClick() {
            try {
                if ($('#<%=txtItemCodeAdd.ClientID %>').val() == '') {
                    ShowPopupMessageBox("Please Enter ItemCode");
                    $('#<%=txtItemCodeAdd.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtLQty.ClientID %>').val() == '' || $('#<%=txtLQty.ClientID %>').val() == '0') {
                    ShowPopupMessageBox("Please Enter Qty");
                    $('#<%=txtLQty.ClientID %>').focus().select();
                    return false;
                }

                var checked = false;
                var ItemCode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                var gridItem = "";

                //===============> Check ItemCode If already exists
                $("#tblAddInv tbody").children().each(function () {
                    gridItem = $(this).find('td input[id*=txtItemcode]').val().trim();
                    if ($.trim(gridItem) == $.trim(ItemCode)) {
                        if (confirm("ItemCode " + ItemCode + " Already Exist, Do U want to Add ?")) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }
    </script>
    <script type="text/javascript">
        function showConfirm(ItemCode) {
            if (confirm("ItemCode " + ItemCode + " Already Exist, Do U want to Add?") == true) {
                return true;
            }
            return false;
        }
    </script>
    <%------------------------------------------- PO Pending Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenPoPending() {
            $("#dialog-POPending").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Last FOC Detail Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenLastFocDetail() {
            $("#dialog-LastFocDetail").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Item History Detail Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenItemHistory() {
            $("#dialog-ItemHistory").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 600,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 0
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Clear Textbox After Add Inventory -------------------------------------------%>
    <script type="text/javascript">
        function fncClearAfterAddInv() {
            $("div[id*=addInv]").find("input[type=text]").val("");
        }
    </script>

    <%------------------------------------------- Table Calculation -------------------------------------------%>

    <script type="text/javascript">

        //==================> Set Focus Next Row and Next Column
        function fncSetFocusNextRowandCol(evt, source, value, curcell, rightTarget, leftTarget, isFreeze, thDispNone, tdDispNone, thDispBlock, tdDispBlock) {
            debugger;
            var rowobj, rowId = 0, charCode, NextRowobj, prevrowobj;
            try {
                rowobj = $(source).parent().parent();
                charCode = (evt.which) ? evt.which : evt.keyCode;

                //======> Enter and Down Arrow
                if (charCode == 13 || charCode == 40) {
                    NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="' + curcell + '"]'));
                    }
                    else {
                        rowobj.siblings().first().find('td input[id*="' + curcell + '"]').select();
                    }
                    return false;
                }

                //======> Up Arrow
                if (charCode == 38) {
                    prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="' + curcell + '"]'));
                    }
                    else {
                        rowobj.siblings().last().find('td input[id*="' + curcell + '"]').select();
                    }
                    return false;
                }


                //======> Right Arrow
                if (charCode == 39) {
                    debugger;
                    if (isFreeze == '1') {
                        $('#' + thDispNone + '').css("display", "none");
                        $('[id*="' + tdDispNone + '"]').css("display", "none");
                    }
                    fncSetSelectAndFocus(rowobj.find('td input[id*="' + rightTarget + '"]'));
                    return false;
                }
                //======> left Arrow
                if (charCode == 37) {
                    debugger;
                    if (isFreeze == '1') {
                        if ($('#' + thDispBlock + '').css("display") == "none") {
                            $('#' + thDispBlock + '').css("display", "");
                            $('[id*="' + tdDispBlock + '"]').css("display", "");
                        }
                    }
                    fncSetSelectAndFocus(rowobj.find('td input[id*="' + leftTarget + '"]'));
                    return false;
                }
            }

            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncSetSelectAndFocus(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        //=================> Repeater Column Value Change Process
        function fncRepeaterColumnValueChange(value) {
            debugger;
            try {
                var TotalQty = 0, GetQty = 0, ItemsCount = 0, TotalCost = 0, ItemCode = "", GST = 0;

                if (value == "NewQty") {
                    $("#tblVenwiseInv tbody").children().each(function () {
                        GetQty = $(this).find('td input[id*=txtNewQty]').val().trim();
                        if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {

                            if (ItemCode == "") {
                                ItemCode = $(this).find('td input[id*=txtItemcode]').val().trim() + "/" + GetQty;
                            }
                            else {
                                ItemCode = ItemCode + "|" + $(this).find('td input[id*=txtItemcode]').val().trim() + "/" + GetQty;
                            }

                            TotalQty = parseFloat(TotalQty) + parseFloat($(this).find('td input[id*=txtNewQty]').val().trim());
                            ItemsCount = ItemsCount + 1;

                            //Total Cost
                            var Cost = $(this).find('td input[id*=txtCurrPrice]').val().trim();
                            TotalCost = parseFloat(TotalCost) + parseFloat(GetQty * Cost)

                            var ITaxPerc1 = fncConvertFloatJS($(this).find('td input[id*=txtITaxPer1]').val().trim());
                            var ITaxPerc2 = fncConvertFloatJS($(this).find('td input[id*=txtITaxPer2]').val().trim());
                            var ITaxPerc3 = fncConvertFloatJS($(this).find('td input[id*=txtITaxPer3]').val().trim());
                            var ITaxPerc4 = fncConvertFloatJS($(this).find('td input[id*=txtITaxPer4]').val().trim());

                            if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                                GST = GST + ((GetQty * Cost * ITaxPerc1) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc2) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                            else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                                GST = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                            else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                                GST = GST + ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                    }

                    });

                    //============> Set Value
                $('#<%=hdfItemCode.ClientID%>').val(ItemCode); // Get ItemCode
                    $('#<%=txtQty.ClientID%>').val(TotalQty); // Total Qty
                    $('#<%=txtItems.ClientID%>').val(ItemsCount); // Total Item
                    $('#<%=txtSubTotal.ClientID%>').val(TotalCost); // Total Cost  
                    $('#<%=txtGST.ClientID%>').val(GST); // GST  

                    if ($('#<%=hdfTaxType.ClientID%>').val() == "Import" || $('#<%=hdfTaxType.ClientID%>').val() == "NoGst") {
                        $('#<%=txtNetTotal.ClientID%>').val(TotalCost);
                    }
                    else {
                        var NetCost = TotalCost + GST;
                        $('#<%=txtNetTotal.ClientID%>').val(NetCost);
                    }
                }

                else if (value == "AddQty") {

                    $("#tblAddInv tbody").children().each(function () {
                        GetQty = $(this).find('td input[id*=txtAddQty]').val().trim();
                        if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {

                            if (ItemCode == "") {
                                ItemCode = $(this).find('td input[id*=txtItemcode]').val().trim() + "/" + GetQty;
                            }
                            else {
                                ItemCode = ItemCode + "|" + $(this).find('td input[id*=txtItemcode]').val().trim() + "/" + GetQty;
                            }

                            TotalQty = parseFloat(TotalQty) + parseFloat($(this).find('td input[id*=txtAddQty]').val().trim());
                            ItemsCount = ItemsCount + 1;

                            //Total Cost
                            var Cost = $(this).find('td input[id*=txtNetCost]').val().trim();
                            TotalCost = parseFloat(TotalCost) + parseFloat(GetQty * Cost)

                            var ITaxPerc1 = fncConvertFloatJS($(this).find('td input[id*=txtSGSTPrc]').val().trim());
                            var ITaxPerc2 = fncConvertFloatJS($(this).find('td input[id*=txtCGSTPrc]').val().trim());
                            var ITaxPerc3 = fncConvertFloatJS($(this).find('td input[id*=txtIGSTPrc]').val().trim());
                            var ITaxPerc4 = fncConvertFloatJS($(this).find('td input[id*=txtCESSPrc]').val().trim());

                            if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                                GST = GST + ((GetQty * Cost * ITaxPerc1) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc2) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                            else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                                GST = GST + ((GetQty * Cost * ITaxPerc3) / 100);
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                            else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                                GST = GST + ((GetQty * Cost * ITaxPerc3) / (100 + ITaxPerc3));
                                GST = GST + ((GetQty * Cost * ITaxPerc4) / 100);
                            }
                    }

                    });

                    //============> Set Value
                $('#<%=hdfAddItemCode.ClientID%>').val(ItemCode); // Get ItemCode
                    $('#<%=txtQty.ClientID%>').val(TotalQty); // Total Qty
                    $('#<%=txtItems.ClientID%>').val(ItemsCount); // Total Item
                    $('#<%=txtSubTotal.ClientID%>').val(TotalCost); // Total Cost  
                    $('#<%=txtGST.ClientID%>').val(GST); // GST  

                    if ($('#<%=hdfItemCode.ClientID%>').val() == "Import" || $('#<%=hdfItemCode.ClientID%>').val() == "NoGst") {
                        $('#<%=txtNetTotal.ClientID%>').val(TotalCost);
                    }
                    else {
                        var NetCost = TotalCost + GST;
                        $('#<%=txtNetTotal.ClientID%>').val(NetCost);
                    }

                }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncDeleteClick(source) {
        try {
            debugger;
            var rowObj, rowId;

            rowobj = $(source).parent().parent();


            rowId = rowobj.find('td input[id*="txtSNo"]').val();
            //rowId = parseFloat(rowId) - parseFloat(1);

            $('#<%=hdfItemCode.ClientID%>').val(rowobj.find('td input[id*="txtItemcode"]').val());
            $('#<%=btnDeleteRow.ClientID%>').click();
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    </script>

    <script type="text/javascript ">

        var glitemhistorybody;

        /// Show and Hide Sales history
        /// 16-02-2018
        function fncShowandHideSaleshistory() {
            try {
                if ($('#divItemhistory').css('display') == 'none') {
                    $('#divItemhistory').css('display', 'block');
                    $('#addInv').css('display', 'none');
                }
                else {
                    $('#divItemhistory').css('display', 'none');
                    $('#addInv').css('display', 'block');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //Data Initialize
        $(document).ready(function () {

            glitemhistorybody = $("#tblItemhistory tbody");

        });

        /// Get Sales history
        /// work done by saravnan 16-02-2018
        function fncGetSalesHistory(source) {
            var obj = {};
            try {
                if ($('#divItemhistory').css('display') == 'none')
                    return;

                rowObj = $(source).closest("tr");

                obj.itemcode = $("td", rowObj).eq(1).text().replace(/&nbsp;/g, '');

                $('#<%=code.ClientID %>').text(obj.itemcode);
                $('#<%=Desc.ClientID %>').text($("td", rowObj).eq(2).text().replace(/&nbsp;/g, ''));


                $.ajax({
                    type: "POST",
                    url: "frmPurchaseOrderEntry.aspx/fncGetSalesHistory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        glitemhistorybody.children().remove();
                        var objItemhistory = jQuery.parseJSON(msg.d);

                        if (objItemhistory.length > 0) {
                            for (var i = 0; i < objItemhistory.length; i++) {
                                glitemhistorybody.append("<tr>"
                                    + "<td>" + objItemhistory[i]["RowNo"] + "</td>"
                                    + "<td>" + objItemhistory[i]["MNName"] + "</td>"
                                    + "<td>" + objItemhistory[i]["WK1"].toFixed(2) + "</td>"
                                    + "<td>" + objItemhistory[i]["WK2"].toFixed(2) + "</td>"
                                    + "<td >" + objItemhistory[i]["WK3"].toFixed(2) + "</td>"
                                    + "<td>" + objItemhistory[i]["WK4"].toFixed(2) + "</td>"
                                    + "<td>" + objItemhistory[i]["WK5"].toFixed(2) + "</td>"
                                    + "<td>" + objItemhistory[i]["WK6"].toFixed(2) + "</td>"
                                    + "<td>" + objItemhistory[i]["Total"].toFixed(2) + "</td>"
                                    + "</tr>");
                            }

                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncVendorDetailLoad() {
            $("[id$=txtVendor]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term;
                    $.ajax({
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/fncGetVendorDetail") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[1],
                                    vendorcode: item.split('/')[0],
                                    vendoraddress1: item.split('/')[2],
                                    vendoraddress2: item.split('/')[3],
                                    GSTNo: item.split('/')[4]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });

                },

                focus: function (event, i) {
                    var vendordet = "";
                    $('#<%=txtVendor.ClientID %>').val($.trim(i.item.vendorcode));
                    vendordet = $.trim(i.item.vendoraddress1) + $.trim(i.item.vendoraddress2) + $.trim(i.item.GSTNo);
                    $('#<%=txtVendorDetails.ClientID %>').val(vendordet);

                    event.preventDefault();
                },

                //==========================> After Select Value
                select: function (e, i) {
                    var vendordet = "";
                    $('#<%=txtVendor.ClientID %>').val($.trim(i.item.vendorcode));
                    vendordet = $.trim(i.item.label) + $.trim(i.item.vendoraddress1) + $.trim(i.item.vendoraddress2) + $.trim(i.item.GSTNo);
                    $('#<%=txtVendorDetails.ClientID %>').val(vendordet);
                    $('#<%=btnVendorchange.ClientID %>').click();
                    return false;
                },

                minLength: 1
            });
        }

        ///Show Item Search Table
        function fncShowVendorSearch() {
            try {
                $("#dialog-Vendor").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Vendor Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowVendorSearchDailog(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                setTimeout(function () {
                    $('#<%=txtVendorSearch.ClientID%>').val($('#<%=txtVendor.ClientID%>').val());
                    fncShowVendorSearch();
                }, 50);
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            $(document).ready(function () {
                fncVendorSearch();
            });

            //// AutoComplete Search for Vendor
            function fncVendorSearch() {
                try {
                    $("[id$=txtVendorSearch]").autocomplete({
                        source: function (request, response) {
                            var obj = {};
                            obj.searchData = escape(request.term);
                            obj.mode = "Vendor";

                            $.ajax({
                                url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                                data: JSON.stringify(obj),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1],
                                            valAddress: item.split('|')[2]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.message);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.message);
                                }
                            });
                        },
                        focus: function (event, i) {
                            $('#<%=txtVendorSearch.ClientID %>').val($.trim(i.item.valitemcode));
                            $('#<%=txtVendorAddress.ClientID %>').val($.trim(i.item.valAddress));
                            event.preventDefault();
                        },
                        select: function (e, i) {
                            $('#emptyrow').css("display", "none");
                            $("#dialog-Vendor").dialog("destroy");
                            var vendordet = "";
                            $('#<%=txtVendor.ClientID %>').val($.trim(i.item.valitemcode));
                            vendordet = $.trim(i.item.label) + $.trim(i.item.valAddress);
                            $('#<%=txtVendorDetails.ClientID %>').val(vendordet);
                            //$('#<%=btnVendorchange.ClientID %>').click();
                            fncGetVendorWiseItemList();
                            return false;
                        },
                        appendTo: $("#dialog-Vendor"),
                        minLength: 2
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

        function fncGetVendorWiseItemList()
        {
            try
            {
                 $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrderEntry.aspx/fncGetVendorWiseItemList") %>',
                     data: "{'Vendorcode':'" + $('#<%=txtVendor.ClientID %>').val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            fncBindVendorwiseItem(jQuery.parseJSON(msg.d));
                        },
                        error: function (data) {
                            fncToastError(data.message);
                        }
                 });
            }
            catch (err)
            {
                fncToastError(err.message);
            }
        }


        function fncBindVendorwiseItem(objvendor)
        {
            var tbodyVendorwise,row;
            try
            {    

                tbodyVendorwise = $("#tblVenwiseInv tbody");

                tbodyVendorwise.children().remove();
                
                for (var i = 0; i < objvendor.Table1.length; i++) {
                   
                    row = "<tr><td id='tdRowNo_" + row + "' > " + objvendor.Table1[i]["RowNo"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["Inventorycode"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["Description"] + "</td>"
                    + "<td>" + "" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + objvendor.Table1[i]["PRReqQty"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["MRP"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["SellingPrice"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["CurrPrice"] + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + objvendor.Table1[i]["DiscountBasicAmt"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxcode1"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxcode2"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxCode3"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxPer1"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxPer2"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxPer3"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["NetCost"] + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + "0.00" + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxPer4"] + "</td>"
                    + "<td>" + objvendor.Table1[i]["ITaxamt4"] + "</td>"
                    + "</tr>";
                    tbodyVendorwise.append(row);
                   
                }
              
            }
            catch (err)
            {
                fncToastError(err.message);
            }
        }

    </script>


    <%------------------------------------------- Validation -------------------------------------------%>
    <script type="text/javascript">

        function Validate() {
            var Show = '';
            if (document.getElementById("<%=txtVendor.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Vendor Name';
                document.getElementById("<%=txtVendor.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlLocation.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Location Name';
                document.getElementById("<%=ddlLocation.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtPurchaseDate.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Purchase Date';
                document.getElementById("<%=txtPurchaseDate.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtDeliveryDate.ClientID%>").value == "") {
                Show = Show + '\n  Please Select Delivery Date';
                document.getElementById("<%=txtDeliveryDate.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                return true;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>

            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="../Purchase/frmPurchaseOrderView.aspx">View Purchase Order</a> </li>
                        <li class="active-page">Purchase Order </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-full">

                    <div class="top-purchase-container">
                        <div class="top-purchase-left-container-po po_top_left">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="PO No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPONO" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendor" runat="server" onkeydown="return fncShowVendorSearchDailog(event);" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Brand"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlBrandCode" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlBrandCode_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-middle-container-po">
                            <div class="control-group-single">
                                <div class="label-right" style="width: 100%">
                                    <asp:TextBox ID="txtVendorDetails" runat="server" CssClass="form-control-res" TextMode="MultiLine"
                                        Style="height: 53px" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-right-container-po ">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="Purchase Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="Delivery Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left" style="width: 44%">
                                    Vendor Items
                                    <asp:CheckBox ID="chkVendorItems" runat="server" />
                                    &nbsp;
                                    <asp:RadioButton ID="rbtMRP" runat="server" GroupName="PriceType" />
                                    MRP
                                    <asp:RadioButton ID="rbtSPrice" runat="server" GroupName="PriceType" />S.Price
                                </div>
                                <div class="label-right" style="width: 56%">
                                    <div class="control-container" style="float: right">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkLastFocDetail" runat="server" class="button-link" OnClick="lnkLastFocDetail_Click">Last Foc Detail</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkPOPending" runat="server" class="button-link" OnClick="lnkPOPending_Click">PO Pending</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkSaleshistory" runat="server" class="button-link" OnClientClick="fncShowandHideSaleshistory();return false;">Sales History</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="bottom-purchase-container">
                        <div class="panel panel-default">
                            <div id="Tabs" role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs custnav custnav-po" role="tablist">
                                    <li><a href="#VendorWiseInv" aria-controls="VendorWiseInv" role="tab" data-toggle="tab">Vendor Wise Inventory </a></li>
                                    <li><a href="#ViewAddInv" aria-controls="ViewAddInv" role="tab" data-toggle="tab">View Add Inventory</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" role="tabpanel" id="VendorWiseInv">
                                        <div class="Payment_fixed_headers po">
                                            <table id="tblVenwiseInv" cellspacing="0" rules="all" border="1">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">S.No</th>
                                                        <th scope="col">Code</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Size</th>
                                                        <th scope="col">Qty</th>
                                                        <th scope="col">Stock</th>
                                                        <th scope="col">PRR</th>
                                                        <th scope="col">MRP</th>
                                                        <th scope="col">Selling</th>
                                                        <th id="thCost" scope="col">Cost</th>
                                                        <th id="thPoQty" scope="col">PO Qty</th>
                                                        <th id="thPurDate" scope="col">Pur-Date</th>
                                                        <th id="thSoldQty" scope="col">Sold Qty</th>
                                                        <th id="thPRPrice" scope="col">PR Price</th>
                                                        <th id="thPurQty" scope="col">Pur Qty</th>
                                                        <th id="thDiscAmt" scope="col">Disc Amt</th>
                                                        <th id="thSGST" scope="col">SGST</th>
                                                        <th id="thCGST" scope="col">CGST</th>
                                                        <th id="thIGST" scope="col">IGST</th>
                                                        <th id="thNet" scope="col">Net</th>
                                                        <th id="thCD" scope="col">CD</th>
                                                        <th id="thSD" scope="col">SD</th>
                                                        <th id="thMargin" scope="col">Margin</th>
                                                        <th id="thBarcodeQty" scope="col">Barcode Qty</th>
                                                        <th id="thCessPrc" scope="col">CESS Prc</th>
                                                        <th id="thCessAmt" scope="col">CESS Amt</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <%--<asp:Repeater ID="rptrVenwiseInv" runat="server">
                                                    <HeaderTemplate>
                                                        <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr tabindex='<%#(Container.ItemIndex)%>' runat="server">
                                                            <td>
                                                                <asp:TextBox ID="txtSNo" runat="server" Text='<%# Eval("RowNo") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtSNo', 'txtItemcode', 'txtItemcode', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtItemcode" runat="server" Text='<%# Eval("Inventorycode") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtItemcode', 'txtDesc', 'txtSNo', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Description") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtDesc', 'txtSize', 'txtItemcode', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSize" runat="server" onfocus="this.select();" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'NewQty', 'txtSize', 'txtNewQty', 'txtDesc', '0', '','','','');" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNewQty" runat="server" Text='0.00' CssClass="grn_rptr_textbox_width form-control-res"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtNewQty', 'txtStock', 'txtSize', '0', '','','','');" onchange="fncRepeaterColumnValueChange('NewQty')"
                                                                    onfocus="fncGetSalesHistory(this); this.select();"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtStock" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtStock', 'txtPRReqQty', 'txtNewQty', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPRReqQty" runat="server" Text='<%# Eval("PRReqQty") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtPRReqQty', 'txtMRP', 'txtStock', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMRP" runat="server" Text='<%# Eval("MRP") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtMRP', 'txtSPrice', 'txtPRReqQty', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSPrice" runat="server" Text='<%# Eval("SellingPrice") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtSPrice', 'txtCurrPrice', 'txtMRP', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCurrPrice">
                                                                <asp:TextBox ID="txtCurrPrice" runat="server" Text='<%# Eval("CurrPrice") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtCurrPrice', 'txtPOQty', 'txtSPrice', '1', 'thCost','ContentPlaceHolder1_rptrVenwiseInv_tdCurrPrice_','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdPOQty">
                                                                <asp:TextBox ID="txtPOQty" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtPOQty', 'txtPurDate', 'txtCurrPrice', '1', 'thPoQty', 'ContentPlaceHolder1_rptrVenwiseInv_tdPOQty_', 'thCost', 'ContentPlaceHolder1_rptrVenwiseInv_tdCurrPrice_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow"
                                                                    onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdPurDate">
                                                                <asp:TextBox ID="txtPurDate" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtPurDate', 'txtSoldQty', 'txtPOQty', '1', 'thPurDate', 'ContentPlaceHolder1_rptrVenwiseInv_tdPurDate_', 'thPoQty', 'ContentPlaceHolder1_rptrVenwiseInv_tdPOQty_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow"
                                                                    onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdSoldQty">
                                                                <asp:TextBox ID="txtSoldQty" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtSoldQty', 'txtPRPrice', 'txtPurDate', '1', 'thSoldQty','ContentPlaceHolder1_rptrVenwiseInv_tdSoldQty_','thPurDate','ContentPlaceHolder1_rptrVenwiseInv_tdPurDate_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdPRPrice">
                                                                <asp:TextBox ID="txtPRPrice" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtPRPrice', 'txtPurQty', 'txtSoldQty', '1', 'thPRPrice','ContentPlaceHolder1_rptrVenwiseInv_tdPRPrice_','thSoldQty','ContentPlaceHolder1_rptrVenwiseInv_tdSoldQty_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdPurQty">
                                                                <asp:TextBox ID="txtPurQty" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtPurQty', 'txtDiscAmt', 'txtPRPrice', '1', 'thPurQty','ContentPlaceHolder1_rptrVenwiseInv_tdPurQty_','thPRPrice','ContentPlaceHolder1_rptrVenwiseInv_tdPRPrice_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdDiscAmt">
                                                                <asp:TextBox ID="txtDiscAmt" runat="server" Text='<%# Eval("DiscountBasicAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtDiscAmt', 'txtITaxcode1', 'txtPurQty', '1', 'thDiscAmt','ContentPlaceHolder1_rptrVenwiseInv_tdDiscAmt_','thPurQty','ContentPlaceHolder1_rptrVenwiseInv_tdPurQty_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdITaxcode1">
                                                                <asp:TextBox ID="txtITaxcode1" runat="server" Text='<%# Eval("ITaxcode1") %>'
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxcode1', 'txtITaxcode2', 'txtDiscAmt', '1', 'thSGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxcode1_','thDiscAmt','ContentPlaceHolder1_rptrVenwiseInv_tdDiscAmt_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdITaxcode2">
                                                                <asp:TextBox ID="txtITaxcode2" runat="server" Text='<%# Eval("ITaxcode2") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxcode2', 'txtITaxCode3', 'txtITaxcode1', '1', 'thCGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxcode2_','thSGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxcode1_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdITaxCode3">
                                                                <asp:TextBox ID="txtITaxCode3" runat="server" Text='<%# Eval("ITaxCode3") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxCode3', 'txtNetCost', 'txtITaxcode2', '1', 'thIGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxCode3_','thCGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxcode2_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td class="hiddencol">
                                                                <asp:TextBox ID="txtITaxPer1" runat="server" Text='<%# Eval("ITaxPer1") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxPer1', 'txtITaxPer2', 'txtITaxCode3', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td class="hiddencol">
                                                                <asp:TextBox ID="txtITaxPer2" runat="server" Text='<%# Eval("ITaxPer2") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxPer2', 'txtITaxPer3', 'txtITaxCode3', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td class="hiddencol">
                                                                <asp:TextBox ID="txtITaxPer3" runat="server" Text='<%# Eval("ITaxPer3") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxPer3', 'txtNetCost', 'txtITaxCode3', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdNetCost">
                                                                <asp:TextBox ID="txtNetCost" runat="server" Text='<%# Eval("NetCost") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtNetCost', 'txtCD', 'txtITaxCode3', '1', 'thNet','ContentPlaceHolder1_rptrVenwiseInv_tdNetCost_','thIGST','ContentPlaceHolder1_rptrVenwiseInv_tdITaxCode3_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCD">
                                                                <asp:TextBox ID="txtCD" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtCD', 'txtSD', 'txtNetCost', '1', 'thCD','ContentPlaceHolder1_rptrVenwiseInv_tdCD_','thNet','ContentPlaceHolder1_rptrVenwiseInv_tdNetCost_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdSD">
                                                                <asp:TextBox ID="txtSD" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtSD', 'txtMargin', 'txtCD', '1', 'thSD','ContentPlaceHolder1_rptrVenwiseInv_tdSD_','thCD','ContentPlaceHolder1_rptrVenwiseInv_tdCD_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdMargin">
                                                                <asp:TextBox ID="txtMargin" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtMargin', 'txtBarcodeQty', 'txtSD', '1', 'thMargin','ContentPlaceHolder1_rptrVenwiseInv_tdMargin_','thSD','ContentPlaceHolder1_rptrVenwiseInv_tdSD_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdBarcodeQty">
                                                                <asp:TextBox ID="txtBarcodeQty" runat="server" Text='0.00' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtBarcodeQty', 'txtITaxPer4', 'txtMargin', '1', '','','thMargin','ContentPlaceHolder1_rptrVenwiseInv_tdMargin_');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdITaxPer4">
                                                                <asp:TextBox ID="txtITaxPer4" runat="server" Text='<%# Eval("ITaxPer4") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxPer4', 'txtITaxamt4', 'txtBarcodeQty', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdITaxamt4">
                                                                <asp:TextBox ID="txtITaxamt4" runat="server" Text='<%# Eval("ITaxamt4") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'VendorWiseInv', 'txtITaxamt4', 'txtITaxamt4', 'txtITaxPer4', '0', '','','','');" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                    </FooterTemplate>
                                                </asp:Repeater>--%>
                                                <tfoot>
                                                    <%-- <tr style="height: 235px" id="emptyrow">
                                                        <td colspan="30" class="repeater_td_align_center">
                                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                                Text="No items to display" />
                                                        </td>
                                                    </tr>--%>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="ViewAddInv">
                                        <div class="Payment_fixed_headers poSelect">
                                            <table id="tblAddInv" cellspacing="0" rules="all" border="1">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Delete</th>
                                                        <th scope="col">S.No</th>
                                                        <th scope="col">Code</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Size</th>
                                                        <th scope="col">Qty</th>
                                                        <th scope="col">Foc</th>
                                                        <th scope="col">MRP</th>
                                                        <th scope="col">Selling</th>
                                                        <th id="thCost1" scope="col">Cost</th>
                                                        <th id="thDiscAmt1" scope="col">Disc Amt</th>
                                                        <th id="thODisc1" scope="col">O.Disc(%)</th>
                                                        <th id="thODiscAmt1" scope="col">O.DiscAmt</th>
                                                        <th id="thGross1" scope="col">Gross</th>
                                                        <th id="thIGST1" scope="col">IGST</th>
                                                        <th id="thSGST1" scope="col">SGST</th>
                                                        <th id="thCGST1" scope="col">CGST</th>
                                                        <th id="thNet1" scope="col">Net</th>
                                                        <th id="thTotal1" scope="col">Total</th>
                                                        <th id="thIGSTCode1" scope="col">IGST Code</th>
                                                        <th id="thSGSTCode1" scope="col">SGST Code</th>
                                                        <th id="thCGSTCode1" scope="col">CGST Code</th>
                                                        <th id="thIGSTAmt1" scope="col">IGST Amt</th>
                                                        <th id="thSGSTAmt1" scope="col">SGST Amt</th>
                                                        <th id="thCGSTAmt1" scope="col">CGST Amt</th>
                                                        <th id="thChecked1" scope="col">Checked</th>
                                                        <th id="thPoQty1" scope="col">Po Qty</th>
                                                        <th id="thCESSPrc1" scope="col">CESS Prc</th>
                                                        <th id="thCESSAmt1" scope="col">CESS Amt</th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptrSelectedInv" runat="server">
                                                    <HeaderTemplate>
                                                        <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr tabindex='<%#(Container.ItemIndex)%>' runat="server">
                                                            <td>
                                                                <img src="../images/No.png" alt="Delete" onclick="fncDeleteClick(this);return false;" />
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSNo" runat="server" Text='<%# Eval("RowNo") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSNo', 'txtItemcode', 'txtItemcode', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtItemcode" runat="server" Text='<%# Eval("Inventorycode") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtItemcode', 'txtDesc', 'txtSNo', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDesc" runat="server" Text='<%# Eval("Description") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtDesc', 'txtSize', 'txtItemcode', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSize" runat="server" onfocus="this.select();" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSize', 'txtAddQty', 'txtDesc', '0', '','','','');"
                                                                    onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtAddQty" runat="server" Text='<%# Eval("Qty") %>' onfocus="this.select();" CssClass="grn_rptr_textbox_width form-control-res"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtAddQty', 'txtFoc', 'txtSize', '0', '','','','');" onchange="fncRepeaterColumnValueChange('AddQty')"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFoc" runat="server" Text='<%# Eval("Foc") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtFoc', 'txtMRP', 'txtAddQty', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtMRP" runat="server" Text='<%# Eval("MRP") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtMRP', 'txtSPrice', 'txtFoc', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtSPrice" runat="server" Text='<%# Eval("SellingPrice") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSPrice', 'txtNetCost', 'txtMRP', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdNetCost1">
                                                                <asp:TextBox ID="txtNetCost" runat="server" Text='<%# Eval("Cost") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtNetCost', 'txtDiscAmt', 'txtSPrice', '1', 'thCost1','ContentPlaceHolder1_rptrSelectedInv_tdNetCost1_','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdDiscAmt1">
                                                                <asp:TextBox ID="txtDiscAmt" runat="server" Text='<%# Eval("DisAmt") %>' onfocus="this.select();" CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtDiscAmt', 'txtODiscPRC', 'txtNetCost', '1', 'thDiscAmt1','ContentPlaceHolder1_rptrSelectedInv_tdDiscAmt1_','thCost1','ContentPlaceHolder1_rptrSelectedInv_tdNetCost1_');"
                                                                    onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdODiscPrc1">
                                                                <asp:TextBox ID="txtODiscPRC" runat="server" Text='<%# Eval("ODiscPRC") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtODiscPRC', 'txtODiscAmt', 'txtDiscAmt', '1', 'thODisc1','ContentPlaceHolder1_rptrSelectedInv_tdODiscPrc1_','thDiscAmt1','ContentPlaceHolder1_rptrSelectedInv_tdDiscAmt1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdODiscAmt1">
                                                                <asp:TextBox ID="txtODiscAmt" runat="server" Text='<%# Eval("ODiscAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtODiscAmt', 'txtGross', 'txtODiscPRC', '1', 'thODiscAmt1','ContentPlaceHolder1_rptrSelectedInv_tdODiscAmt1_','thODisc1','ContentPlaceHolder1_rptrSelectedInv_tdODiscPrc1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdGross1">
                                                                <asp:TextBox ID="txtGross" runat="server" Text='<%# Eval("Gross") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtGross', 'txtIGSTPrc', 'txtODiscAmt', '1', 'thGross1','ContentPlaceHolder1_rptrSelectedInv_tdGross1_','thODiscAmt1','ContentPlaceHolder1_rptrSelectedInv_tdODiscAmt1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdIGSTPrc1">
                                                                <asp:TextBox ID="txtIGSTPrc" runat="server" Text='<%# Eval("IGSTPrc") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtIGSTPrc', 'txtSGSTPrc', 'txtGross', '1', 'thIGST1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTPrc1_','thGross1','ContentPlaceHolder1_rptrSelectedInv_tdGross1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdSGSTPrc1">
                                                                <asp:TextBox ID="txtSGSTPrc" runat="server" Text='<%# Eval("SGSTPrc") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSGSTPrc', 'txtCGSTPrc', 'txtIGSTPrc', '1', 'thSGST1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTPrc1_','thIGST1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTPrc1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCGSTPrc1">
                                                                <asp:TextBox ID="txtCGSTPrc" runat="server" Text='<%# Eval("CGSTPrc") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtCGSTPrc', 'txtNet', 'txtSGSTPrc', '1', 'thCGST1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTPrc1_','thSGST1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTPrc1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdNet1">
                                                                <asp:TextBox ID="txtNet" runat="server" Text='<%# Eval("Net") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtNet', 'txtTotal', 'txtCGSTPrc', '1', 'thNet1','ContentPlaceHolder1_rptrSelectedInv_tdNet1_','thCGST1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTPrc1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdTotal1">
                                                                <asp:TextBox ID="txtTotal" runat="server" Text='<%# Eval("Total") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtTotal', 'txtIGSTCode', 'txtNet', '1', 'thTotal1','ContentPlaceHolder1_rptrSelectedInv_tdTotal1_','thNet1','ContentPlaceHolder1_rptrSelectedInv_tdNet1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdIGSTCode1">
                                                                <asp:TextBox ID="txtIGSTCode" runat="server" Text='<%# Eval("IGSTCode") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtIGSTCode', 'txtSGSTCode', 'txtTotal', '1', 'thIGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTCode1_','thTotal1','ContentPlaceHolder1_rptrSelectedInv_tdTotal1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdSGSTCode1">
                                                                <asp:TextBox ID="txtSGSTCode" runat="server" Text='<%# Eval("SGSTCode") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSGSTCode', 'txtCGSTCode', 'txtIGSTCode', '1', 'thSGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTCode1_','thIGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTCode1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCGSTCode1">
                                                                <asp:TextBox ID="txtCGSTCode" runat="server" Text='<%# Eval("CGSTCode") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtCGSTCode', 'txtIGSTAmt', 'txtSGSTCode', '1', 'thCGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTCode1_','thSGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTCode1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdIGSTAmt1">
                                                                <asp:TextBox ID="txtIGSTAmt" runat="server" Text='<%# Eval("IGSTAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtIGSTAmt', 'txtSGSTAmt', 'txtCGSTCode', '1', 'thIGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTAmt1_','thCGSTCode1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTCode1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdSGSTAmt1">
                                                                <asp:TextBox ID="txtSGSTAmt" runat="server" Text='<%# Eval("SGSTAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtSGSTAmt', 'txtCGSTAmt', 'txtIGSTAmt', '1', 'thSGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTAmt1_','thIGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdIGSTAmt1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCGSTAmt1">
                                                                <asp:TextBox ID="txtCGSTAmt" runat="server" Text='<%# Eval("CGSTAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtCGSTAmt', 'txtChecked', 'txtSGSTAmt', '1', 'thCGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTAmt1_','thSGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdSGSTAmt1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdChecked1">
                                                                <asp:TextBox ID="txtChecked" runat="server" Text='<%# Eval("Checked") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtChecked', 'txtPoQty', 'txtCGSTAmt', '1', 'thChecked1','ContentPlaceHolder1_rptrSelectedInv_tdChecked1_','thCGSTAmt1','ContentPlaceHolder1_rptrSelectedInv_tdCGSTAmt1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdPoQty1">
                                                                <asp:TextBox ID="txtPoQty" runat="server" Text='<%# Eval("PoQty") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtPoQty', 'txtCESSPrc', 'txtChecked', '1', '','','thChecked1','ContentPlaceHolder1_rptrSelectedInv_tdChecked1_');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCessPrc1">
                                                                <asp:TextBox ID="txtCESSPrc" runat="server" Text='<%# Eval("CESSPrc") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtCESSPrc', 'txtCESSAmt', 'txtPoQty', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                            <td id="tdCessAmt1">
                                                                <asp:TextBox ID="txtCESSAmt" runat="server" Text='<%# Eval("CESSAmt") %>' onfocus="this.select();"
                                                                    onkeydown=" return fncSetFocusNextRowandCol(event,this,'SelectedInv', 'txtCESSAmt', 'txtCESSAmt', 'txtCESSPrc', '0', '','','','');"
                                                                    CssClass="grn_rptr_textbox_width form-control-res border_nonewith_yellow" onkeypress="return fnctblColEnableFalse(event);"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <tfoot>
                                                    <tr style="height: 235px" id="emptyrow1">
                                                        <td colspan="30" class="repeater_td_align_center">
                                                            <asp:Label ID="Label38" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                                Text="No items to display" />
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hiddencol">
                            <asp:HiddenField ID="hdfTabName" runat="server" />
                            <asp:Button ID="btnAddGrid" runat="server" Text="AddGrid" OnClick="btnAddGrid_Click" />
                            <asp:Button ID="btnDeleteRow" runat="server" Text="AddGrid" OnClick="btnDeleteRow_Click" />
                            <asp:Button ID="btnVendorchange" runat="server" OnClick="btnVendorchange_click" />
                            <asp:HiddenField ID="hdfItemCode" runat="server" />
                            <asp:HiddenField ID="hdfAddItemCode" runat="server" />
                        </div>

                        <%--<asp:UpdatePanel ID="updtPnlPOPending" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>--%>
                        <div id="dialog-POPending" title="PO Pending Details" style="display: none">
                            <div class="grid-popup">
                                <asp:GridView ID="gvPoPending" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                    CssClass="pshro_GridDgn">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="PoNo" HeaderText="Po No"></asp:BoundField>
                                        <asp:BoundField DataField="PoDate" HeaderText="Po Date"></asp:BoundField>
                                        <asp:BoundField DataField="PoTotalValue" HeaderText="Po Total Value"></asp:BoundField>
                                        <asp:BoundField DataField="Process" HeaderText="Po Process"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div id="dialog-LastFocDetail" title="Last Foc Detail" style="display: none">
                            <div class="grid-popup">
                                <asp:GridView ID="gvLastFocDetail" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                    CssClass="pshro_GridDgn">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="ItemCode" HeaderText="Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="Lqty" HeaderText="Lqty"></asp:BoundField>
                                        <asp:BoundField DataField="FOCItemCode" HeaderText="FOC Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="FOCQty" HeaderText="FOC Qty"></asp:BoundField>
                                        <asp:BoundField DataField="GIDNo" HeaderText="GID No"></asp:BoundField>
                                        <asp:BoundField DataField="GIDDate" HeaderText="GID Date"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>


                    <div id="divItemhistory" style="display: none">
                        <div class="po_saleshistory">
                            <asp:Label ID="code" runat="server"></asp:Label>
                        </div>
                        <div class="po_saleshistory">
                            <asp:Label ID="Desc" runat="server"></asp:Label>
                        </div>
                        <div class="Payment_fixed_headers ReGeneratePO_Itemhistory PO_Itemhistory">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Month
                                        </th>
                                        <th scope="col">1st Week
                                        </th>
                                        <th scope="col">2st Week
                                        </th>
                                        <th scope="col">3st Week
                                        </th>
                                        <th scope="col">4st Week
                                        </th>
                                        <th scope="col">5st Week
                                        </th>
                                        <th scope="col">6st Week
                                        </th>
                                        <th scope="col">Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div id="divItemadd">
                        <asp:Panel runat="server" ID="pnlItemadd">
                            <%--    <asp:UpdatePanel ID="updtPnlButton1" UpdateMode="Conditional" runat="Server">
                        <ContentTemplate>--%>
                            <div class="bottom-purchase-container-add" id="addInv">
                                <div class="bottom-purchase-container-add-Text">
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 8%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label6" runat="server" Text="Item Code"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%">
                                                <asp:TextBox ID="txtItemCodeAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                <asp:HiddenField ID="hfInvCode" runat="server" />
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 18%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label7" runat="server" Text="ItemDesc"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 7%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label13" runat="server" Text="W.Uom"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:DropDownList ID="ddlWUom" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 5%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label14" runat="server" Text="W.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtWQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 5%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label15" runat="server" Text="N.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtNQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 6%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label16" runat="server" Text="MRP"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtMrp" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 6%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label17" runat="server" Text="BasicCost"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtBasicCost" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 5%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label18" runat="server" Text="Dis(%)"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtDiscPrc" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 6%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label19" runat="server" Text="Dis Amt"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 5%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label20" runat="server" Text="S.D(%)"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtSDPrc" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 7%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label21" runat="server" Text="SD Amt"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtSDAmt" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 7%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label22" runat="server" Text="G.Cost"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtGrossCost" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 7%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label23" runat="server" Text="Nett"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtNet" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-left" style="width: 8%">
                                            <div class="label-left" style="width: 100%">
                                                <asp:Label ID="Label24" runat="server" Text="Net Total"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                                <asp:TextBox ID="txtNetTotalAdd" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-purchase-container-add-Text">
                                    <div class="bottom-purchase-container-blns">
                                        <div class="control-group-split">
                                            <asp:Panel ID="pnlCGST" runat="server">
                                                <div class="control-group-left" style="width: 10%">
                                                    <div class="label-left" style="width: 100%">
                                                        <asp:Label ID="Label11" runat="server" Text="SGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                                        <asp:TextBox ID="txtSGST" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfSGSTCode" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-left" style="width: 10%">
                                                    <div class="label-left" style="width: 100%">
                                                        <asp:Label ID="Label36" runat="server" Text="CGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                                        <asp:TextBox ID="txtCGST" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfCGSTCode" runat="server" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlIGST" runat="server" Visible="false">
                                                <div class="control-group-left" style="width: 10%">
                                                    <div class="label-left" style="width: 100%">
                                                        <asp:Label ID="Label5" runat="server" Text="IGST(%)"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                                        <asp:TextBox ID="txtIGST" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                        <asp:HiddenField ID="hdfIGSTCode" runat="server" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <div class="control-group-left" style="width: 10%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label37" runat="server" Text="CESS(%)"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:TextBox ID="txtCESS" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 10%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label25" runat="server" Text="L.Uom"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:DropDownList ID="ddlLUOM" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 10%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label26" runat="server" Text="L.Qty"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 10%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label27" runat="server" Text="Foc"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:TextBox ID="txtFoc" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 15%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label28" runat="server" Text="Selling Price"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-left" style="width: 25%">
                                                <div class="label-left" style="width: 100%">
                                                    <asp:Label ID="Label8" runat="server" Text="Remarks"></asp:Label>
                                                </div>
                                                <div class="label-right" style="width: 99%; margin-left: 1%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hdfTaxType" runat="server" />
                                    <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkAddInv" runat="server" class="button-red" OnClientClick="return AddbuttonClick()" OnClick="lnkAddInv_Click">Add</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearAdd" runat="server" class="button-red" OnClientClick="return fncClearAfterAddInv();">Clear</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-purchase-container-total">
                                <div class="control-group-split">
                                    <div class="control-group-left" style="width: 8%">
                                        <div class="label-left" style="width: 50%">
                                            <asp:Label ID="Label29" runat="server" Text="Items"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtItems" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left" style="width: 8%">
                                        <div class="label-left" style="width: 35%; padding-left: 5px;">
                                            <asp:Label ID="Label30" runat="server" Text="Qty"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 65%">
                                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol " style="width: 12%">
                                        <div class="label-left" style="width: 40%">
                                            <asp:Label ID="Label2" runat="server" Text="Total"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 60%">
                                            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol" style="width: 15%">
                                        <div class="label-left" style="width: 70%">
                                            <asp:Label ID="Label31" runat="server" Text="Discount By%"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 28%">
                                            <asp:TextBox ID="txtTotalDiscPrc" runat="server" CssClass="form-control-res" Font-Bold="true"
                                                onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left hiddencol" style="width: 15%">
                                        <div class="label-left" style="width: 50%">
                                            <asp:Label ID="Label32" runat="server" Text="By Rs"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtTotalDiscRs" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left" style="width: 15%">
                                        <div class="label-left" style="width: 50%; padding-left: 30px;">
                                            <asp:Label ID="Label33" runat="server" Text="SubTotal"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left" style="width: 12%">
                                        <div class="label-left" style="width: 50%; padding-left: 30px;">
                                            <asp:Label ID="Label34" runat="server" Text="GST"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtGST" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left" style="width: 15%">
                                        <div class="label-left" style="width: 50%; padding-left: 30px;">
                                            <asp:Label ID="Label35" runat="server" Text="Net Total"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 48%">
                                            <asp:TextBox ID="txtNetTotal" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--  </ContentTemplate>
                    </asp:UpdatePanel>--%>
                        </asp:Panel>
                    </div>
                    <%--   <asp:UpdatePanel ID="updtPnlButton2" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>--%>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkViewPo" runat="server" class="button-red" PostBackUrl="~/Purchase/frmPurchaseOrderView.aspx">View PO</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return Validate()" OnClick="lnkSave_Click">Save</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                OnClick="lnkClear_Click">Clear</asp:LinkButton>
                        </div>
                    </div>
                    <%--  </ContentTemplate>
            </asp:UpdatePanel>--%>
                </div>
            </div>

            <div class="display_none">
                <div id="dialog-Vendor" style="display: none">
                    <div>
                        <div class="float_left">
                            <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                        </div>
                        <div class="gidItem_Search">
                            <asp:TextBox ID="txtVendorSearch" runat="server" class="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="vendor_Searchaddress">
                        <asp:TextBox ID="txtVendorAddress" runat="server" TextMode="MultiLine"
                            class="form-control-res vendor_Searchaddress_height"></asp:TextBox>
                    </div>

                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


