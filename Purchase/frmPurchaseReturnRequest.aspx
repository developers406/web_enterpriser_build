﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmPurchaseReturnRequest.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseReturnRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 350px;
            max-width: 350px;
        }

        .transfer td:nth-child(4) {
            text-align: left !important;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(6) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(8) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(9) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(10) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 200px;
            max-width: 200px;
        }

        .transfer td:nth-child(12), .transfer th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .ui-autocomplete {
            max-height: 200px !important;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 20px;
        }
    </style>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'PurchaseReturnRequest');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "PurchaseReturnRequest";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>
    <script type="text/javascript">

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            SetAutoComplete();
            fncDecimal();
        }

        $(function () {

            $("#<%= DropDownVendor.ClientID %>").change(function () {
                var status = this.value;
                setTimeout(function () {
                    $('#<%=txtPRRQty.ClientID %>').focus().select();
                }, 100);
                // if (status == "1")
                //   $("#icon_class, #background_class").hide(); // hide multiple sections
            });

        });

        $(function () {
            $('#<%=txtPRRQty.ClientID %>').focusout(function () {
                fncCalcTotalAmount();
            });
        });

        $(function () {
            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

            if ($("#<%= txtRequestdate.ClientID %>").val() === '') {
                $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }

        });

        function reloadPage() {
            window.location.reload();
            // window.location.href="~/Purchase/frmPurchaseReturnList.aspx" ;
        }

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function SetAutoComplete() {
            var obj = {};

            $("[id$=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    obj.prefix = request.term;
                    obj.mode = "PURCHASE";
                    $.ajax({
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseReturnRequest.aspx/GetInventoryDetails")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1],
                                    valDescription: item.split('|')[2],
                                    valMrp: item.split('|')[3],
                                    valSellprice: item.split('|')[4],
                                    valCost: item.split('|')[5],
                                    valBatch: item.split('|')[6]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                focus: function (event, i) {
                    $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.valCode));
                      event.preventDefault();
                  },
                  select: function (e, i) {
                      $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.valCode));
                    $('#<%=txtdescription.ClientID %>').val($.trim(i.item.valDescription));
                    $('#<%=txtMrp.ClientID %>').val($.trim(i.item.valMrp));
                    $('#<%=txtSellingPrice.ClientID %>').val($.trim(i.item.valSellprice));
                    $('#<%=txtCost.ClientID %>').val($.trim(i.item.valCost));
                    $("#HidenIsBatch").val($.trim(i.item.valBatch));
                    var Item = $('#<%=txtInventoryCode.ClientID %>').val();
                    $("#<%=HiddenItemCode.ClientID %>").val(Item);
                    $('#<%=txtInventoryCode.ClientID %>').attr('disabled', 'disabled');
                    //alert($("#HidenIsBatch").val());
                    if ($("#HidenIsBatch").val() == 'True')
                        fncGetBatchDetail_Master(i.item.valCode);
                    else
                        fncVendorDetails(i.item.valCode);
                    setTimeout(function () {
                        $('#<%=txtPRRQty.ClientID %>').select();
                    }, 10)


                    return false;
                },
                minLength: 2
            });
            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtInventoryCode'
                });
            });
        }

        function fncVendorDetails(itemCode) {
            try {
                $.ajax({
                    type: "POST",
                    url: "frmPurchaseReturnRequest.aspx/GetVendorDetails",
                    data: "{'sInvCode':'" + itemCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var ddlVendorsrc = $('#<%=DropDownVendor.ClientID%>');
                        //ddlBactno.empty().append('<option selected="selected" Value="0">Please select</option>');                        
                        $("select[id$=DropDownVendor] > option").remove();
                        //alert(msg.d.length);
                        //if (msg.d.length > 0)
                        //    ddlVendorsrc.empty().append('<option selected="selected" Value="0">Please select</option>');
                        $.each(msg.d, function () {
                            ddlVendorsrc.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                        $('#<%=DropDownVendor.ClientID %>').trigger("liszt:updated");
                        if (msg.d.length > 1)
                            $('#<%=DropDownVendor.ClientID %>').trigger("liszt:open");

                        $('#<%=txtPRRQty.ClientID%>').select();

                    },
                    error: function (data) {
                        alert('Something Went Wrong')
                    }
                });
            }
            catch (err) {
                alert(err.Message);
            }
        }

        //        //Get Vendor Code from DropDownList
        //        function fncVendorSelect() {
        //            try { 
        //                var selectedText = $('#<%=DropDownVendor.ClientID%>').find("option:selected").text();
        //                var selectedValue = $('#<%=DropDownVendor.ClientID%>').val();
        //                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
        //                $("select[id$=DropDownVendor] > option").remove();
        //                $('#<%=DropDownVendor.ClientID%>').append($("<option></option>").val(selectedValue).html(selectedText));

        //                $("#divVendor").dialog('close');
        //                return false;
        //            }
        //            catch (err) {
        //                alert(err.Message);
        //            }
        //        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock, netCost) {
            try {
                $('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtSellingPrice.ClientID%>').val(sprice);
                $('#<%=txtBatchNo.ClientID%>').val(batchno);
                $('#<%=txtStock.ClientID%>').val(stock);
                $('#<%=txtCost.ClientID%>').val(netCost);

                fncVendorDetails($('#<%=txtInventoryCode.ClientID%>').val());
                //ShowPopupBatch("Select Vendor");

            }
            catch (err) {
                alert(err.message);
            }
        }

        function ShowPopupBatch(name) {
            $("#divVendor").dialog({
                appendTo: 'form:first',
                closeOnEscape: false,
                title: name,
                width: 365,
                //                buttons: {
                //                    PRINT: function () {
                //                        $(this).dialog('close');
                //                    }
                //                },
                modal: true

            });
        }

        function fncValidateSave() {
            var totalRows = $("#<%=grdPurchaseReturnRequest.ClientID %> td").length;

            if (totalRows == 0) {
                alert('Please Add any Item to Proceed !');
                $('#<%=txtInventoryCode.ClientID %>').focus().select();
                return false;
            }
        }


        function AddbtnValidation() {

            try {

                if ($('#<%=txtInventoryCode.ClientID %>').val() == '') {

                    alert('Item Code should not be Empty !');
                    $('#<%=txtInventoryCode.ClientID %>').select();
                    return false;

                }

                if ($('#<%=txtPRRQty.ClientID %>').val() <= 0) {
                    alert('Invalid Request Qty !');
                    $('#<%=txtPRRQty.ClientID %>').focus().select();

                    return false;
                }

                if ($('#<%=txtAmount.ClientID %>').val() <= 0) {
                    alert('Invalid Amount !');
                    $('#<%=txtAmount.ClientID %>').focus().select();

                    return false;
                }

                var name = $('#<%=DropDownVendor.ClientID %>').find('option:selected').text();
                var code = $('#<%=DropDownVendor.ClientID %>').val();

                if (code == '') {
                    alert('Please select Vendor !');
                    $('#<%=DropDownVendor.ClientID %>').focus().select();

                    return false;
                }

                $("#<%=HiddenVendorName.ClientID %>").val(name);
                $("#<%=HiddenVendorCode.ClientID %>").val(code);
                //alert(name + '-' + code);
                debugger;
                var totalRows = $("#<%=grdPurchaseReturnRequest.ClientID %> tr").length;

                $("#<%=grdPurchaseReturnRequest.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitemcode = $(this).find("td.Itemcode").html();
                    var gBatchNo = $(this).find("td.BatchNo").html();
                    //alert(gitemcode + '-' + gBatchNo);
                    if ($.trim(gitemcode) == $.trim($('#<%=txtInventoryCode.ClientID %>').val()) && $.trim(gBatchNo) == $.trim($('#<%=txtBatchNo.ClientID %>').val())) {
                        var confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";
                        confirm_value.name = "confirm_value";
                        if (confirm("Item Already exists!. Do you want to update Return Qty?")) {
                            confirm_value.value = "Yes";

                        } else {
                            confirm_value.value = "No";
                        }
                        document.forms[0].appendChild(confirm_value);
                    }
                    else if ($.trim(gitemcode) == $.trim($('#<%=txtInventoryCode.ClientID %>').val())) {
                        var confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";
                        confirm_value.name = "confirm_value";
                        if (confirm("Item Already exists!. Do you want to update Return Qty?")) {
                            confirm_value.value = "Yes";

                        } else {
                            confirm_value.value = "No";
                        }
                        document.forms[0].appendChild(confirm_value);
                    }

                });

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //Get Calc Total amount
        function fnctxtReturnQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtPRRQty.ClientID %>').val() <= 0) {

                        alert('Invalid Weight Qty !');
                        $('#<%=txtPRRQty.ClientID %>').focus().select();

                        return false;
                    }

                    fncCalcTotalAmount();

                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    return false;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncCalcTotalAmount() {

            try {

                var ReturnQty = $('#<%=txtPRRQty.ClientID%>').val();
                var Cost = $('#<%=txtCost.ClientID%>').val();
                var Amount;
                Amount = ReturnQty * Cost;
                $('#<%=txtAmount.ClientID%>').val(Amount.toFixed(2));

            }
            catch (err) {
                alert(err.Message);
            }
        }

        function ClearTextBox() {
            //$('input[type="text"]').val('');
            $('#<%=txtInventoryCode.ClientID %>').removeAttr('disabled');
            $('#<%=txtInventoryCode.ClientID %>').val('');
            $('#<%=txtdescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtPRRQty.ClientID %>').val('0');
            $('#<%=txtMrp.ClientID %>').val('0');
            $('#<%=txtSellingPrice.ClientID %>').val('0');
            $('#<%=txtCost.ClientID %>').val('0');
            $('#<%=txtStock.ClientID %>').val('0');
            $('#<%=txtAmount.ClientID %>').val('0');

            $("#HiddenVendorName").val('');
            $('#HiddenVendorCode').val('');

            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            $('#<%=txtInventoryCode.ClientID %>').focus().select();
            return false;
        }

        function ClearForm() {
            //$('input[type="text"]').val('');
            $('#<%=txtInventoryCode.ClientID %>').val('');
            $('#<%=txtdescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtPRRQty.ClientID %>').val('0');
            $('#<%=txtMrp.ClientID %>').val('0');
            $('#<%=txtSellingPrice.ClientID %>').val('0');
            $('#<%=txtCost.ClientID %>').val('0');
            $('#<%=txtStock.ClientID %>').val('0');
            $('#<%=txtAmount.ClientID %>').val('0');

            $("table[id$='grdPurchaseReturnRequest']").html("");

            $("#HiddenVendorName").val('');
            $('#HiddenVendorCode').val('');

            $("#<%= txtRequestdate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            $('#<%=txtInventoryCode.ClientID %>').focus().select();
            return false;
        }

        $(function () {
            $("[id*=grdPurchaseReturnRequest]").on('click', 'td > img', function (e) {

                var r = confirm("Do you want to Delete?");
                if (r === false) {
                    return false;
                }
                $(this).closest("tr").remove();
                $('#<%=txtInventoryCode.ClientID %>').focus().select();
                return false;
            });

        });

        function fncGoToPurchaseReturnReq(value) {
            try {
                //window.location = "frmPurchaseReturnList.aspx";
                //setTimeout(function () {
                //    fncToastInformation(value + " Purchase Return Request Saved Successfully.");
                //}, 20)

                $(function () {
                    $("#dialog-Success").html(value + " Purchase Return Request Saved Successfully.");
                    $("#dialog-Success").dialog({
                        title: "Enterpriser Web",
                        buttons: {
                            Ok: function () {
                                $(this).dialog("destroy");
                                window.location = "frmPurchaseReturnList.aspx";
                            }
                        },
                        modal: true
                    });
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtPRRQty.ClientID%>').number(true, 2);
                $('#<%=txtCost.ClientID%>').number(true, 2);
                $('#<%=txtMrp.ClientID%>').number(true, 2);
                $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
                $('#<%=txtAmount.ClientID%>').number(true, 2);
                $('#<%=txtTotalQty.ClientID%>').number(true, 2);
                $('#<%=txtTotalAmt.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGetItemdetail_Enterkey(evt) {
            var item = {}, charCode;
            var obj = {};
            try {
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    obj.prefix = $('#<%=txtInventoryCode.ClientID %>').val();
                    obj.mode = "PURCHASE_code";
                    $.ajax({
                        url: '<%=ResolveUrl("~/Purchase/frmPurchaseReturnRequest.aspx/GetInventoryDetails")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var item = data.d.toString().split('|');

                            $('#<%=txtInventoryCode.ClientID %>').val($.trim(item[1]));
                            $('#<%=txtdescription.ClientID %>').val($.trim(item[2]));
                            $('#<%=txtMrp.ClientID %>').val($.trim(item[3]));
                            $('#<%=txtSellingPrice.ClientID %>').val($.trim(item[4]));
                            $('#<%=txtCost.ClientID %>').val($.trim(item[5]));
                            $("#HidenIsBatch").val($.trim(item[6]));
                            //alert($("#HidenIsBatch").val());
                            var ItemCode = $('#<%=txtInventoryCode.ClientID %>').val();
                            $("#<%=HiddenItemCode.ClientID %>").val(ItemCode);
                            $('#<%=txtInventoryCode.ClientID %>').attr('disabled', 'disabled');
                            if ($("#HidenIsBatch").val() == 'True')
                                fncGetBatchDetail_Master($.trim(item[1]));
                            else
                                fncVendorDetails($.trim(item[1]));
                            setTimeout(function () {
                                $('#<%=txtPRRQty.ClientID %>').select();
                            }, 10);

                            //response($.map(data.d, function (item) {
                            //    return {
                            //        label: item.split('|')[0],
                            //        valCode: item.split('|')[1],
                            //        valDescription: item.split('|')[2],
                            //        valMrp: item.split('|')[3],
                            //        valSellprice: item.split('|')[4],
                            //        valCost: item.split('|')[5],
                            //        valBatch: item.split('|')[6]
                            //    }
                            //}))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        }
                    });
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" id="form1">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmPurchaseReturnList.aspx">Purchase Retrun Requested List</a></li>
                <li class="active-page">Purchase Return Request Creation</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-pc1">
            <%--<div class="right-container-top-distribution">--%>
            <div class="right-container-bottom-detail">
                <div class="control-group-split">
                    <div class="control-group-left-Distb">
                        <div class="label-left-pc">
                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblRequestdate %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:TextBox ID="txtRequestdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left-pc">
                        <div class="label-left-pc">
                            <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_RequestNo %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:TextBox ID="txtRequestNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left-pc">
                        <div class="label-left-pc">
                            <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_ReturnType %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:DropDownList ID="ReturnTypeDropDown" runat="server" CssClass="form-control-res">
                                <asp:ListItem>RETURN</asp:ListItem>
                                <asp:ListItem>DAMAGE</asp:ListItem>
                                <asp:ListItem>EXCHANGE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="control-group-left-pc">
                        <div class="label-left-pc">
                            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Location %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <%--<asp:DropDownList ID="LocationDropdown" runat="server" CssClass="form-control-res">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', ''); " ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left-Distribut-txt">
                        <div class="label-left-pc">
                            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left-Distb">
                        <div class="label-left-pc">
                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_totalQty %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:TextBox ID="txtTotalQty" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left-Distb">
                        <div class="label-left-pc">
                            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_totalAmount %>'></asp:Label>
                        </div>
                        <div class="label-right-pc">
                            <asp:TextBox ID="txtTotalAmt" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-button" style="margin-left: 20px">
                        <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button-red" OnClientClick="return reloadPage()"
                            Text='<%$ Resources:LabelCaption,btn_refresh %>'> Delete </asp:LinkButton>
                    </div>
                </div>
            </div>
            <%--<div class="container-horiz-top-purreturn">--%>
            <%--<div class="right-container-top-header">
                        Purchase Return Request
                    </div>--%>
            <div style="padding-top: 5px;">
                <div class="GridDetails">
                    <div class="row">
                        <div class="over_flowhorizontal">
                            <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header transfer">
                                <tr>
                                    <th>S.No</th>
                                    <th>Delete</th>
                                    <th>Item Code</th>
                                    <th>Description</th>
                                    <th>BatchNo</th>
                                    <th>Return Qty</th>
                                    <th>Mrp</th>
                                    <th>SellingPrice</th>
                                    <th>Cost</th>
                                    <th>Amount</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Code</th>
                                </tr>
                            </table>
                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 260px; width: 1470px; background-color: aliceblue;">
                                <asp:GridView ID="grdPurchaseReturnRequest" runat="server" AutoGenerateColumns="false"
                                    PageSize="14" ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn transfer" OnRowDataBound="grdPurchaseReturnRequest_RowDataBound"
                                    OnRowDeleting="grdPurchaseReturnRequest_RowDeleting" ShowHeader="false">
                                    <PagerStyle CssClass="pshro_text" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <Columns>
                                        <asp:BoundField HeaderText="S.No" DataField="RowNo"></asp:BoundField>
                                        <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                        <asp:BoundField HeaderText="Item Code" DataField="ItemCode"></asp:BoundField>
                                        <asp:BoundField HeaderText="Description" DataField="Description"></asp:BoundField>
                                        <asp:BoundField HeaderText="BatchNo" DataField="BatchNo"></asp:BoundField>
                                        <asp:BoundField HeaderText="Return Qty" DataField="ReturnQty"></asp:BoundField>
                                        <asp:BoundField HeaderText="Mrp" DataField="Mrp"></asp:BoundField>
                                        <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice"></asp:BoundField>
                                        <asp:BoundField HeaderText="Cost" DataField="Cost"></asp:BoundField>
                                        <asp:BoundField HeaderText="Amount" DataField="Amount"></asp:BoundField>
                                        <asp:BoundField HeaderText="Vendor Name" DataField="VendorName"></asp:BoundField>
                                        <asp:BoundField HeaderText="Vendor Code" DataField="VendorCode"></asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        PageButtonCount="5" Position="Bottom" />
                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-horiz-top-purreturn">
                <div class="right-container-bottom-header">
                </div>
                <div class="right-container-bottom-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblitemcode %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtInventoryCode" runat="server" CssClass="form-control-res" onkeydown="return fncGetItemdetail_Enterkey(event);"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="control-group-left-Description-sm">
                            <div class="label-left-pc">
                                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label3" runat="server" Text="PRR Qty"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPRRQty" runat="server" onkeypress="return isNumberKey(event)"
                                    onkeydown="return fnctxtReturnQty(event)" CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Mrp %>'></asp:Label>
                            </div>
                            <div class="label-right-small">
                                <asp:TextBox ID="txtMrp" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellingPrice %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtSellingPrice" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_amount %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtAmount" runat="server" Text="0" onkeypress="return isNumberKey(event)"
                                    CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Description-vendor">
                            <div class="label-left-pc">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_vendor %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:DropDownList ID="DropDownVendor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Stock %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtStock" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_Cost %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCost" runat="server" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-small">
                            <div class="label-left-pc">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batchno %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtBatchNo" runat="server" Text="" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClientClick="return AddbtnValidation()"
                            Text='<%$ Resources:LabelCaption,btnadd %>' OnClick="lnkAdd_Click"> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                            Text='<%$ Resources:LabelCaption,btnclear %>'> Clear </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClientClick="return fncValidateSave();"
                            Text='<%$ Resources:LabelCaption,btnSave %>' OnClick="lnkSave_Click">  </asp:LinkButton>
                    </div>
                </div>
            </div>
            <%--</div>--%>
            <%--</div>--%>
            <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="hidenExists" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="HiddenVendorCode" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="HiddenVendorName" runat="server" ClientIDMode="Static" Value="" />
            <asp:HiddenField ID="HiddenItemCode" runat="server" ClientIDMode="Static" Value="" />
            <div class="container-group-pc" id="divVendor">
                <div class="label-right-pc">
                    <div class="label-left-pc">
                        <%--<asp:DropDownList ID="DropDownVendord" Style="width: 330px" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <div id="dialog-Success" style="display: none">
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
