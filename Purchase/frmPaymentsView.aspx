﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPaymentsView.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPaymentsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/shortcut.js" type="text/javascript"></script>
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .issuedpayheader td:nth-child(1), .issuedpayheader th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
        }

        .issuedpayheader td:nth-child(2), .issuedpayheader th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
        }

        .issuedpayheader td:nth-child(3), .issuedpayheader th:nth-child(3) {
            min-width: 65px;
            max-width: 65px;
        }

        .issuedpayheader td:nth-child(4), .issuedpayheader th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .issuedpayheader td:nth-child(5), .issuedpayheader th:nth-child(5) {
            min-width: 300px;
            max-width: 300px;
        }

        .issuedpayheader td:nth-child(6), .issuedpayheader th:nth-child(6) {
            min-width: 170px;
            max-width: 170px;
        }

        .issuedpayheader td:nth-child(7), .issuedpayheader th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .issuedpayheader td:nth-child(8), .issuedpayheader th:nth-child(8) {
            min-width: 150px;
            max-width: 150px;
        }

        .issuedpayheader td:nth-child(9), .issuedpayheader th:nth-child(9) {
            min-width: 150px;
            max-width: 150px;
        }

        .issuedpayheader td:nth-child(10), .issuedpayheader th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
        }


        .UnIssuedPayment td:nth-child(1), .UnIssuedPayment th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .UnIssuedPayment td:nth-child(2), .UnIssuedPayment th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
        }

        .UnIssuedPayment td:nth-child(3), .UnIssuedPayment th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .UnIssuedPayment td:nth-child(4), .UnIssuedPayment th:nth-child(4) {
            min-width: 240px;
            max-width: 240px;
        }

        .UnIssuedPayment td:nth-child(5), .UnIssuedPayment th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .UnIssuedPayment td:nth-child(5) {
            text-align: right !important;
        }

        .UnIssuedPayment td:nth-child(6), .UnIssuedPayment th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        }

        .UnIssuedPayment td:nth-child(6) {
            text-align: right !important;
        }

        .UnIssuedPayment td:nth-child(7), .UnIssuedPayment th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .UnIssuedPayment td:nth-child(8), .UnIssuedPayment th:nth-child(8) {
            min-width: 120px;
            max-width: 120px;
        }

        .UnIssuedPayment td:nth-child(8) {
            text-align: right !important;
        }

        .UnIssuedPayment td:nth-child(9), .UnIssuedPayment th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .UnIssuedPayment td:nth-child(10), .UnIssuedPayment th:nth-child(10) {
            min-width: 50px;
            max-width: 50px;
            text-align:center !important;
        }

        .UnIssuedPayment td:nth-child(11), .UnIssuedPayment th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
        }

        .UnIssuedPayment td:nth-child(12), .UnIssuedPayment th:nth-child(12) {
            display: none;
        }

        .UnIssuedPayment td:nth-child(13), .UnIssuedPayment th:nth-child(13) {
            display: none;
        }

        .UnIssuedPayment td:nth-child(14), .UnIssuedPayment th:nth-child(14) {
            display: none;
        }

        .UnIssuedPayment td:nth-child(15), .UnIssuedPayment th:nth-child(15) {
            min-width: 90px;
            max-width: 90px;
        }

        .chosen-container.chosen-container-single {
            width: 300px !important; /* or any value that fits your needs */
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'PaymentsView');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "PaymentsView";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
           
        });
    </script>
    <script type="text/javascript">
        var issueddate = "", locationcode = "";
        var rowDeleteobj;
        $(document).ready(function () {
            resizeChosen();
            jQuery(window).on('resize', resizeChosen);
        });

        function resizeChosen() {
            $(".chosen-container").each(function () {
                $(this).attr('style', 'width: 100%');
            });
        }
        function pageLoad() {
          if ($("#<%=txtFromDate.ClientID %>").val() == "" && $("#<%=txtToDate.ClientID %>").val() == "") {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            }
         
            resizeChosen();
            fncTotalchqIssue();
            //$("select").chosen({ width: '100%' });
            $("#tblPaymnet [id*=paymentBodyrow]").find('td[id*="tdPayNeft"]').hide();
            $('.UnIssuedPayment th:nth-child(15)').css("display", "none");
            if ($("#<% =hidPAYNeft.ClientID%>").val() == "Y") {
                $("#tblPaymnet [id*=paymentBodyrow]").find('td[id*="tdPayNeft"]').show();
                $('.UnIssuedPayment th:nth-child(15)').css("display", "table-cell");
                $("#tblPaymnet [id*=paymentBodyrow]").each(function () {
                    if ($(this).find('td span[id*="lblPaymenttype"]').text().indexOf("NEF") == -1)
                        $(this).find('td input[id*="btnPayNeft"]').hide();
                });
                if ($("#<% =hidBalance.ClientID%>").val() != "") {
                    $("#<% =lblBalance.ClientID%>").text("View Balance");
                    $("#<% =txtToAccountNo.ClientID%>").val($("#<% =hidVendor.ClientID%>").val());
                    $("#<% =txtIfscCode.ClientID%>").val($("#<% =hidIfscCode.ClientID%>").val());
                    $("#<% =txtPayeeName.ClientID%>").val($("#<% =hidPayee.ClientID%>").val());
                    $("#<% =lblBalance.ClientID%>").css("background-color", "pink");
                    $("#<% =lblBalance.ClientID%>").hover(function () {
                        $(this).text($("#<% =hidBalance.ClientID%>").val() + " Rs");
                        $(this).css("background-color", "yellow");
                    }, function () {
                        $(this).text("View Balance");
                        $(this).css("background-color", "pink");
                    });
                    $("#<% =txtRemarks.ClientID%>").focus();
                    $("#divNEFT").dialog({
                        resizable: false,
                        height: 400,
                        width: 500,
                        modal: true,
                        title: "ICICI Transaction",
                        appendTo: 'form:first'
                    });
                }
                else if ($("#<% =hidOTPStatus.ClientID%>").val() == "Success") {

                    $("#divOTP").dialog({
                        resizable: false,
                        height: 400,
                        width: 500,
                        modal: true,
                        title: "Verify Transaction",
                        appendTo: 'form:first'
                    });

                    $("#<% =txtVerifyOTP.ClientID%>").focus();
                }
        }

        $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });

            $('#<%=txtVendorName.ClientID %>').attr("readonly", true);
            $("#<%= txtFromDate.ClientID %>").removeAttr('disabled');
            $("#<%= txtToDate.ClientID %>").removeAttr('disabled');

            if ($("#<%= hidLocation.ClientID %>").val() == "Y")
                $('#divLocation').show();
            else
                $('#divLocation').hide();

            if ($("#<%= hidTextile.ClientID %>").val() == "Y")
                $('#divAgent').show();
            else
                $('#divAgent').hide();

            $(document).ready(function () {
                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            });

            fncInitializeRTGS();
            fncTotalforView();

            if ($("#<%=hidBillType.ClientID%>").val() == "B") {
                $('#breadcrumbs_text').removeClass('uppercase');
                $('#breadcrumbs_text').addClass('lowercase');
            }
            else if ($("#<%=hidBillType.ClientID%>").val() == "A") {
                $('#breadcrumbs_text').removeClass('lowercase');
                $('#breadcrumbs_text').addClass('uppercase');
            }
            else if ($("#<%=hidBillType.ClientID%>").val() == "C") {
                $('#breadcrumbs_text').removeClass('uppercase');
                $('#breadcrumbs_text').removeClass('lowercase');
                $('#breadcrumbs_text').text('View Payments');
            }
    }

    shortcut.add("Ctrl+A", function () {
        $('#breadcrumbs_text').removeClass('lowercase');
        $('#breadcrumbs_text').addClass('uppercase');
        $("#<%=hidBillType.ClientID%>").val('A');

    });
    shortcut.add("Ctrl+B", function () {
        $('#breadcrumbs_text').removeClass('uppercase');
        $('#breadcrumbs_text').addClass('lowercase');
        $("#<%=hidBillType.ClientID%>").val('B');
    });

    shortcut.add("Ctrl+C", function () {
        $('#breadcrumbs_text').removeClass('uppercase');
        $('#breadcrumbs_text').removeClass('lowercase');
        $('#breadcrumbs_text').text('View Payments');
        $("#<%=hidBillType.ClientID%>").val('C');
    });

    //Initialize RTGS Dialog
    function fncInitializeRTGS() {
        try {
            $("#rtgs").dialog({
                resizable: false,
                autoOpen: false,
                height: 200,
                width: 300,
                modal: true,
                title: "RTGS Excel Conversion",
                appendTo: 'form:first',
                draggable: false,

            });
            return false;
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Total Calculation
    function fncTotalforView() {
        try {
            var outStandingAmt = 0, Amttopay = 0, Chequesum = 0;
            $("#tblPaymnet [id*=paymentBodyrow]").each(function () {
                outStandingAmt = parseFloat(outStandingAmt) + parseFloat($(this).find('td span[id*="lblOutStanding"]').html());
                Amttopay = parseFloat(Amttopay) + parseFloat($(this).find('td span[id*="lblToPayAmount"]').html());
                var Cheque = $(this).find('td span[id*="lblChequeAmount"]').html();
                if (Cheque == "")
                    Cheque = 0;
                Chequesum = parseFloat(Chequesum) + parseFloat(Cheque);
                if ($(this).find('td span[id*="lblStatus"]').html() == "") {
                    $(this).find('td input[id*="cbIssue"]').attr('disabled', 'disabled');
                    $(this).find('td input[id*="btnPayNeft"]').css('display', 'none');

                }
                else {
                    $(this).find('td input[id*="cbIssue"]').removeAttr('disabled');
                }

            });

            outStandingAmt = outStandingAmt.toFixed(2);
            outStandingAmt = outStandingAmt.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            Amttopay = Amttopay.toFixed(2);
            Amttopay = Amttopay.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            Chequesum = Chequesum.toFixed(2);
            Chequesum = Chequesum.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            $("#tblPaymnet [id*=lblTotalsum]").html(outStandingAmt);
            $("#tblPaymnet [id*=lbltopaysum]").html(Amttopay);
            $("#tblPaymnet [id*=lblChequeAmtSum]").html(Chequesum);
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
        function fncTotalchqIssue() {
            try {
                var Chequesum = 0;

                $("#tblIssuedPayment [id*=IssuedPaymentBodyrow]").each(function () {
                    Chequesum = parseFloat(Chequesum) + parseFloat($(this).find('td span[id*="lblchequeAmt"]').html());
                });

                Chequesum = Chequesum.toFixed(2);
                Chequesum = Chequesum.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                $("#tblIssuedPayment [id*=lblsum]").html(Chequesum);
            }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            
        }
    function fncVendorPay(evt) {
        try {

            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13) {

                __doPostBack('ctl00$ContentPlaceHolder1$lnkrefresh', '');
                return false;
            }
            else {
                return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendorCode', '');
            }

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
        return true;
    }

    //Zero Payment Check
    function fncPaybuttonclick(source) {
        try {
            var rowobj = $(source).parent().parent();
            //CurRowObj.find('td input[id*="txtAmtToPay"]').val(ToPayAmt);
            var PayAmt = rowobj.find('td span[id*="lblToPayAmount"]').html();
            var vendorcode = rowobj.find('td span[id*="lblVendorCode"]').html();
            var chequeNo = rowobj.find('td span[id*="lblChequeNo"]').html();
            var chequeAmt = rowobj.find('td span[id*="lblChequeAmount"]').html();
            var PaymentNo = rowobj.find('td span[id*="lblPaymentNo"]').html();
            var F = $("#<%=txtFromDate.ClientID %>").val();
            var T = $("#<%=txtToDate.ClientID %>").val();
            $("#<%=hidFdate.ClientID %>").val(F);
            $("#<%=hidTdate.ClientID %>").val(T);
            if (chequeNo != "") {
                $("#<%= hidVendorcode.ClientID %>").val(vendorcode);
                $("#<%= hidToPayamt.ClientID %>").val(chequeAmt);
                $("#<%= hidPaymentNo.ClientID %>").val(PaymentNo);
            }
            <%--else if (parseFloat(PayAmt) == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_DueAmountEmpty%>');
                return false;
            }--%>
            else {
                $("#<%= hidVendorcode.ClientID %>").val(vendorcode);
                $("#<%= hidToPayamt.ClientID %>").val(PayAmt);
                $("#<%= hidPaymentNo.ClientID %>").val('');
            }

    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

//Initialize Save Dialog
function fncInitializeSaveDialog() {
    try {
        $("#PaymentSave").dialog({
            autoOpen: false,
            resizable: false,
            height: 130,
            width: 325,
            modal: true
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

//Open Save Dialog
function fncOpenSaveDialog() {
    try {
        if ($("#divOTP").is(":visible") == true) {
            $("#divOTP").dialog('close');
        }
        fncInitializeSaveDialog();
        $("#PaymentSave").dialog('open');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncOpenRTGS() {
    try {
        $("#<%=txtLocation.ClientID %>").val() == '';
        $("#rtgs").dialog('open');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncViewbuttonclick(source) {
    try {
        var rowobj = $(source).parent().parent();
        var vendorcode = rowobj.find('td span[id*="lblVendorCode"]').html();
        var PayAmt = rowobj.find('td span[id*="lblchequeAmt"]').html();
        var PaymentNo = rowobj.find('td span[id*="lblpaymentno"]').html();

        $("#<%= hidVendorcode.ClientID %>").val(vendorcode);
        $("#<%= hidToPayamt.ClientID %>").val(PayAmt);
        $("#<%= hidPaymentNo.ClientID %>").val(PaymentNo);

    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//Initialize Save Dialog
function fncInitializeAdminAuthenticationDialog(source) {
    try {
        rowDeleteobj = $(source).parent().parent();
        issueddate = rowDeleteobj.find('td span[id*="lblIssuedDate"]').html();
        locationcode = $("#<%=txtLocation.ClientID %>").val();

        $("#<%=hidPaymentNo.ClientID %>").val(rowDeleteobj.find('td span[id*="lblpaymentno"]').html());
        $("#adminauthentication").dialog({
            resizable: false,
            height: 120,
            width: 250,
            modal: true
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//To Open Authentication Dialog
function fncAdminAuthentication(evt) {
    try {

        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode == 13) {

            var obj = {};
            obj.password = $("#<%=txtPassword.ClientID %>").val();


            if (obj.password == "") {
                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Enterpassword%>');
                return;
            }

            obj.paymentNo = $("#<%=hidPaymentNo.ClientID %>").val();
            obj.issuedDate = issueddate;
            obj.locationcode = locationcode;

            $.ajax({
                type: "POST",
                url: "frmPaymentsView.aspx/fncGetPaymentStatus",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    if (msg.d == "InvalidPassword") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.msgInvalidPassword%>');
                    }
                    else if (msg.d == "DayEndCompleted") {
                        fncAuthenticationDialogClose();
                        ShowPopupMessageBox('<%=Resources.LabelCaption.msgafterDayend%>');
                    }
                    else if (msg.d == "RTGSComplete") {
                        fncAuthenticationDialogClose();
                        ShowPopupMessageBox('<%=Resources.LabelCaption.alertRTGSComplete%>');
                    }
                    else if (msg.d == "Success") {

                        var rowParentObj = rowDeleteobj.parent();
                        rowDeleteobj.remove();
                        rowParentObj.children().each(function (index) {
                            $(this).find('td span[id*=lblSNo]').html(index + 1);
                        });
                        rowDeleteobj = null;

                        fncAuthenticationDialogClose();
                        ShowPopupMessageBox('<%=Resources.LabelCaption.save_DeletePayment%>');

                    }
                    else if (msg.d == "Fail") {
                        fncAuthenticationDialogClose();
                        ShowPopupMessageBox('<%=Resources.LabelCaption.fail_Transaction%>');
                    }
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });
}
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncAuthenticationDialogClose() {
    try {
        $("#adminauthentication").dialog('close');
        $("#<%=txtPassword.ClientID %>").val('');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
///Empty Cheque Validation
function fncCheckNoValidation() {
    try {
        if ($("#<%=txtChequeNo.ClientID %>").val() == "") {
            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Enterchequeno%>');
            return false;
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//Cheque Save Validation
function fncSaveValidation() {
    try {
        if ($("#tblPaymnet [id*=paymentBodyrow]").length == 0) {
            ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
            return false;
        }
        else if ($("#tblPaymnet [id*=cbIssue]:checked").length == 0) {
            ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                return false;
            }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//Get All Record using 0 qty
function fncSetZerotosearchrecord() {
    try {
        if ($("#<%=txtRecordSearch.ClientID %>").val() == "") {
            $("#<%=txtRecordSearch.ClientID %>").val('0');
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

//Empty Bank Check
function fncBankcodecheck() {
    try {
        if ($("#<%=txtBankCode.ClientID %>").val() == '<%=Resources.LabelCaption.ddl_Empty%>') {
            ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_BankCode%>');
            return false;
        }
        else
            $("#rtgs").dialog('close');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//
function fncSaveDialogClose() {
    $("#PaymentSave").dialog('close');
}
function fncSetValue() {
    try {
        if (SearchTableName == "Vendor") {
            $('#<%=txtVendorCode.ClientID %>').val($.trim(Code));
            $('#<%=txtVendorName.ClientID %>').val($.trim(Description));
        }
        //__doPostBack('ctl00$ContentPlaceHolder1$lnkrefresh', '');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

$('#rbtnIssued').click(function () {
    alert('test');
});

function fncShowItemEditDailog(msg) {
    $(function () {

        $("#PaymentDialog").html("<H5><B>" + msg + "</B></H5>");
        $("#PaymentDialog").dialog({
            title: "Enterpriser Web",
            width: 200,
            buttons: {
                "Ok": function () {
                    $(this).dialog("destroy");
                    //window.location.href = "frmGoodsAcknowledgement.aspx";
                }
            },
            modal: true
        });
    });
};
function fncExcelExport() {
    try {
        $('#<%=btnExcelExport.ClientID %>').click();
    }
    catch (err) {
        fncToastError(err.message)
    }
}

function fncClear() {
    $(':input').not(':button, :submit, :reset, :hidden').val('');
    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
    $("#<%=txtRecordSearch.ClientID %>").val('200');

}

function fncPayNEFT(source) {
    try {
        var rowobj = $(source).parent().parent();
        //CurRowObj.find('td input[id*="txtAmtToPay"]').val(ToPayAmt);
        //var PayAmt = rowobj.find('td span[id*="lblToPayAmount"]').html();
        //var vendorcode = rowobj.find('td span[id*="lblVendorCode"]').html();
        //var chequeNo = rowobj.find('td span[id*="lblChequeNo"]').html();
        //var chequeAmt = rowobj.find('td span[id*="lblChequeAmount"]').html();
        //var PaymentNo = rowobj.find('td span[id*="lblPaymentNo"]').html();
        rowobj.find('td input[id*="cbIssue"]').attr('checked', 'checked');
        var obj = {};
        obj.PaymentNo = rowobj.find('td span[id*="lblPaymentNo"]').html();
        $('#<%=hidVendor.ClientID %>').val(rowobj.find('td span[id*="lblVendorCode"]').html());
        $('#<%=hidVendorcode.ClientID %>').val(rowobj.find('td span[id*="lblVendorCode"]').html());
        $('#<%=hidVendorName.ClientID %>').val(rowobj.find('td span[id*="lblVendorName"]').html());
        $('#<%=txtTrAmt.ClientID %>').val(parseFloat(rowobj.find('td span[id*="lblChequeAmount"]').html()).toFixed(2));
        obj.Mode = "1";
        $.ajax({
            type: "POST",
            url: "frmPaymentsView.aspx/fncCheckNEFT",
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.indexOf("NEFT") != -1) {
                    fncSetPayment();
                }
                else {
                    $("#tblPaymnet [id*=paymentBodyrow]").find('td input[id*="cbIssue"]').removeAttr('checked');
                    ShowPopupMessageBox("Only NEFT Payment is Allowed to Pay.This is " + msg.d + " Payment");
                }
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
                return false;
            }
        });

    }
    catch (e) {
        ShowPopupMessageBox(e.message);
    }
    return false;
}

function fncSetPayment() {
    try {
        $('#<%=lnkBalance.ClientID %>').text('');
        $('#<%=lnkBalance.ClientID %>').click();
    }
    catch (e) {
        ShowPopupMessageBox(e.message);
    }
}

function numericOnly(elementRef) {

    var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

    if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57) || (keyCodeEntered == 189) || (keyCodeEntered == 109)) {
        return true;
    }
        // '.' decimal point...   
    else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...   
        if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))
            return false;
        else

            return true;
    }
    return false;

}

function fncBank_Detail_Clear() {
    try {
        $('#<%=lnkBalance.ClientID %>').text('');
        $('#<%=txtTrAmt.ClientID %>').val('0');
        $("#<% =txtToAccountNo.ClientID%>").val('');
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncCheckBankDetail() {
    try {
        if ($('#<%=ddlAccoutnNo.ClientID %>').val() == "") {
            ShowPopupMessageBox("Please Choose From Account Number");
            return false;
        } else if ($('#<%=txtToAccountNo.ClientID %>').val() == "") {
            ShowPopupMessageBox("Please Enter To Account Number");
            return false;
        }
        else if ($('#<%=txtIfscCode.ClientID %>').val() == "") {
            ShowPopupMessageBox("Please Enter IFSC Code");
            return false;
        }
        else if ($('#<%=txtTrAmt.ClientID %>').val() == "" || parseFloat($('#<%=txtTrAmt.ClientID %>').val()) <= 0) {
            ShowPopupMessageBox("Please Enter Transaction Amount");
            return false;
        }
        else if ($('#<%=txtRemarks.ClientID %>').val() == "") {
            ShowPopupMessageBox("Please Enter Remarks");
            return false;
        }
        else {
            return true;
        }
}
    catch (e) {
        ShowPopupMessageBox(e.message);
    }
}

function Toast(msg) {
    try {
        //$('#ContentPlaceHolder1_ddlAccoutnNo_chzn').css("width", "135px !important"); 
        if ($("#tblPaymnet [id*=paymentBodyrow]").length > 0) {
            $("#tblPaymnet [id*=paymefntBodyrow]").find('td input[id*="cbIssue"]').removeAttr('checked');
        }
        ShowPopupMessageBox(msg);
        return false;
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncVerifyValidation() {
    try {
        if ($('#<%=txtVerifyOTP.ClientID %>').val() == "") {
            ShowPopupMessageBox("Please Enter OTP");
            return false;
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
        }

        function fncLoadPaymentSync() {
            try {
                window.location.href = "../GST_Web/frmGstrMothwiseSalesSync.aspx"
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <asp:UpdatePanel ID="upPayment" runat="Server">
            <ContentTemplate>
                <div class="barcodepahe_color">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                            <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                            <li class="active-page uppercase" id="breadcrumbs_text">
                                <%=Resources.LabelCaption.lblViewPayments%>
                            </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        </ul>
                    </div>
                    <div class="container-group-full_1">
                        <div class="purchase-order-header" style="background-color: #e6e6e6">
                            <div class="col-md-12">
                                <div class="col-md-5">
                                    <div class="control-button ">
                                        <asp:Label ID="lblChequeNo" runat="server" Text='<%$ Resources:LabelCaption,lblChequeNumber %>'></asp:Label>
                                    </div>
                                    <div class="control-button payment_chequeno">
                                        <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="control-button ">
                                        <asp:LinkButton ID="lblOk" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lblOk %>'
                                            OnClick="lblOk_Click" OnClientClick="return fncCheckNoValidation()"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-md-2 display_none">
                                    <asp:Button ID="btnPayment" runat="server" Text="Payments Sync" OnClientClick="fncLoadPaymentSync();return false;"/>
                                </div>
                                <div class="col-md-3">
                                    <asp:Label ID="lblLastSyncDate" runat="server" Style="float: right; margin-top: 5px; background-color: #4163e1; color: white;"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rbtnIssued" runat="server" GroupName="Status" AutoPostBack="True"
                                            OnCheckedChanged="rbtnIssued_CheckedChanged" OnClientClick="fncTotalchqIssue()" />
                                        <%=Resources.LabelCaption.lblIssued%>
                                    &nbsp;
                                    </div>
                                    <div class="control-button">
                                        <asp:RadioButton ID="rbtnUnIssued" runat="server" GroupName="Status" Checked="true"
                                            OnCheckedChanged="rbtnUnIssued_CheckedChanged" AutoPostBack="True" />
                                        <%=Resources.LabelCaption.lblUnIssed%>
                                    &nbsp;
                                    </div>
                                    <%--JK Starts--%>
                                    <div class="control-button">
                                        <asp:RadioButton ID="rbtnAll" runat="server" GroupName="Status"
                                            OnCheckedChanged="rbtnAll_CheckedChanged" AutoPostBack="True" />
                                        <%=Resources.LabelCaption.lblAll%>
                                    &nbsp;
                                    </div>
                                    <%--JK ends--%>
                                </div>
                            </div>
                        </div>
                        <div class="purchase-order-header" style="background-color: #e6e6e6">
                            <div class="purchase-option-container" style="width: 50%; float: left">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblVendorCode %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <%--                                            <asp:TextBox ID="txtVendorCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendorCode', ''); "  ></asp:TextBox>--%>
                                            <asp:TextBox ID="txtVendorCode" runat="server" CssClass="form-control-res" onkeydown="return fncVendorPay(event); "></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblVendorName %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblCity %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCity" runat="server" autocomplete="off" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Zone', 'txtCity', '');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblRecordSearchDays %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtRecordSearch" runat="server" onblur="fncSetZerotosearchrecord()"
                                                CssClass="form-control-res" onkeypress="return isNumberKey(event)">200</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                    </div>

                                    <div class="control-group-right" id="divLocation">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lblLocationCode %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="purchase-option-container" style="width: 49%; float: left; margin-left: 1%">
                                <div class="control-group-split">
                                    <asp:Panel ID="pnlPaymode" runat="server" Enabled="false">
                                        <div class="control-group-left" style="width: 40%">
                                            <div class="label-left" style="width: 27%">
                                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblPayMode %>'></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 73%">
                                                <asp:TextBox ID="txtPaymode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Paymode', 'txtPaymode', '');"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlLHC" runat="server">
                                        <div class="control-group-right" style="width: 50%; float: right">
                                            <asp:RadioButton ID="rbtCreditLimit" runat="server" GroupName="LDH" OnCheckedChanged="rbtCreditLimit_CheckedChanged"
                                                AutoPostBack="True" />
                                            Credit Limit &nbsp;
                                            <asp:RadioButton ID="rbtCreditDays" runat="server" GroupName="LDH" OnCheckedChanged="rbtCreditDays_CheckedChanged"
                                                AutoPostBack="True" />
                                            Credit Days &nbsp;
                                            <asp:RadioButton ID="rbtHoldInvoice" runat="server" GroupName="LDH" OnCheckedChanged="rbtHoldInvoice_CheckedChanged"
                                                AutoPostBack="True" />
                                            Hold Invoice &nbsp;
                                            <asp:RadioButton ID="rbAll" runat="server" GroupName="LDH" AutoPostBack="True" OnCheckedChanged="rbAll_CheckedChanged"
                                                Checked="True" />
                                            All &nbsp;
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left1" id="divAgent">
                                        <div class="label-left" style="width: 35%;">
                                            <asp:Label ID="Label3" runat="server" Text="Agent Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAgentCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Agent', 'txtAgentCode', '');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlDate" runat="server" Enabled="false">
                                        <div class="control-group-middle">
                                            <div class="label-left">
                                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right1">
                                            <div class="label-left">
                                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right1" style="width: 30% !important">
                                            <div class="control-button" style="margin: 0;">
                                                <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="fncClear();return false;">Clear</asp:LinkButton>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="purchase-order-detail">
                            <asp:Panel ID="Panel1" runat="server" Visible="false">
                                <div class="issuedpayheader Payment_fixed_headers ">
                                    <table id="tblIssuedPayment" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col">S.No
                                                </th>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Payment
                                                </th>
                                                <th scope="col">Vendor Code
                                                </th>
                                                <th scope="col">Vendor Name
                                                </th>
                                                <th scope="col">Payment No
                                                </th>
                                                <th scope="col">Cheque Amount
                                                </th>
                                                <th scope="col">Cheque Date
                                                </th>
                                                <th scope="col">Issued Date
                                                </th>
                                                <th scope="col">Cheque No
                                                </th>
                                            </tr>
                                        </thead>
                                        <asp:Repeater ID="rptrIssuedPayment" runat="server">
                                            <HeaderTemplate>
                                                <tbody id="IssuedPaymentBody">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="IssuedPaymentBodyrow" runat="server">
                                                    <td>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png" OnClientClick="fncInitializeAdminAuthenticationDialog(this);return false;" />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnview" runat="server" OnClick="btnview_Click" Text="View" OnClientClick="fncViewbuttonclick(this)" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendorCode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblpaymentno" runat="server" Text='<%# Eval("paymentno") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblchequeAmt" runat="server" Text='<%# Eval("chequeAmt") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblchequedate" runat="server" Text='<%# Eval("chequedate") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblIssuedDate" runat="server" Text='<%# Eval("IssuedDate") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblchequeno" runat="server" Text='<%# Eval("chequeno") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <tfoot>
                                             <tr class="Paymentfooter_Color">
                                                <td class="paymentfooter_total">Total
                                                </td>
                                                  <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total">
                                                     <asp:Label ID="lblsum" runat="server" />
                                                 </td>
                                                 <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total"></td>
                                                 <td class="paymentfooter_total"></td>
                                                 
                                        </tfoot>
                                    </table>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server">
                                <div id="divrepeater" class="Payment_fixed_headers UnIssuedPayment">
                                    <table id="tblPaymnet" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col">S.No
                                                </th>
                                                <th scope="col">Payment
                                                </th>
                                                <th scope="col">Vendor Code
                                                </th>
                                                <th scope="col">Vendor Name
                                                </th>
                                                <th scope="col">OutStanding
                                                </th>
                                                <th scope="col">ToPay Amount
                                                </th>
                                                <th scope="col">Cheque No
                                                </th>
                                                <th scope="col">Cheque Amount
                                                </th>
                                                <th scope="col">Cheque Date
                                                </th>
                                                <th scope="col">Issue
                                                </th>
                                                <th scope="col">Status
                                                </th>
                                                <th scope="col">PaymentNo
                                                </th>
                                                <th scope="col">StopPayment
                                                </th>
                                                <th scope="col">PaymentType
                                                </th>
                                                <th scope="col">Pay NEFT
                                                </th>
                                            </tr>
                                        </thead>
                                        <asp:Repeater ID="rptrPayment" runat="server" OnItemDataBound="rptrPayment_ItemDataBound">
                                            <HeaderTemplate>
                                                <tbody id="PaymentBody">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="paymentBodyrow" runat="server">
                                                    <td>
                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnPay" runat="server" OnClick="btnPay_Click" Text="Pay" OnClientClick="return fncPaybuttonclick(this)" />
                                                    </td>
                                                    <td id="rptVendorCode" runat="server">
                                                        <asp:Label ID="lblVendorCode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblOutStanding" runat="server" Text='<%# Eval("OutStanding") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblToPayAmount" runat="server" Text='<%# Eval("ToPayAmount") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblChequeNo" runat="server" Text='<%# Eval("ChequeNo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblChequeAmount" runat="server" Text='<%# Eval("ChequeAmt") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblChequeDate" runat="server" Text='<%# Eval("ChequeDate") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="cbIssue" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("ChequeStatus") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPaymentNo" runat="server" Text='<%# Eval("PaymentNo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblStopPayment" runat="server" Text='<%# Eval("StopPayment") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPaymenttype" runat="server" Text='<%# Eval("Paymenttype") %>' />
                                                    </td>
                                                    <td id="tdPayNeft">
                                                        <asp:Button ID="btnPayNeft" runat="server" Text="Pay NEFT" OnClientClick="return fncPayNEFT(this)" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <tfoot>
                                            <tr class="Paymentfooter_Color">
                                                <td class="paymentfooter_total">Total
                                                </td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total">
                                                    <asp:Label ID="lblTotalsum" runat="server" />
                                                </td>
                                                <td class="paymentfooter_total">
                                                    <asp:Label ID="lbltopaysum" runat="server" />
                                                </td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total">
                                                    <asp:Label ID="lblChequeAmtSum" runat="server" />
                                                </td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                                <td class="paymentfooter_total"></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </asp:Panel>
                            <%-- <div class="col-md-8">
                                <label style="color:white;background-color:#d64937">XXXX</label>
                                <label style="font-weight:700;">Under Stop Vendor Payment</label>
                            </div>--%>
                        </div>
                        <div class="payments-group-left1">
                            <div class="control-group-split">
                                <div class="control-group-middle" style="width: 35%;">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Bank Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <div id="divddlbankcode">
                                            <asp:DropDownList ID="ddlBankCode" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-button" style="margin: 0;">
                                    <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="fncExcelExport();return false;">NEFT/RTGS Export</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="control-container" style="width: 30% !important">

                            <div id="divGeneralLedgerBtn" runat="server">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkGeneralLedger" runat="server" class="button-red" PostBackUrl="~/Purchase/frmGeneralLedger.aspx" Text="General Ledger"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkrtgs" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_RTGS %>'
                                    OnClientClick="fncOpenRTGS();return false;" Visible="False"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Pay %>'
                                    OnClick="lnkSave_Click" OnClientClick="return fncSaveValidation()"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkrefresh" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_refresh %>'
                                    OnClick="lnkrefresh_Click"></asp:LinkButton>
                            </div>
                            <%--JK starts--%>
                            <div class="control-button" style="display: none;">
                                <asp:LinkButton ID="lnkclose" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,PaymentWindowClose %>' OnClientClick="window.close();return false"></asp:LinkButton>
                            </div>
                            <%--JK Ends--%>
                        </div>

                    </div>
                </div>

                <div id="PaymentDialog" class="display_none">
                </div>

                <div class="hiddencol">
                    <asp:HiddenField ID="hidVendorcode" runat="server" />
                    <asp:HiddenField ID="hidToPayamt" runat="server" />
                    <asp:HiddenField ID="hidPaymentNo" runat="server" />
                    <asp:HiddenField ID="hidBillType" runat="server" Value="A" />
                    <div id="PaymentSave">
                        <p>
                            <%=Resources.LabelCaption.save_ChequeIssueDate%>
                        </p>
                        <div class="dialog_center">
                            <asp:UpdatePanel ID="upsave" runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                        OnClick="lnkbtnOk_Click" OnClientClick="fncSaveDialogClose()"> </asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div id="adminauthentication" title="Authentication">
                        <div>
                            <asp:Label ID="lblPassword" runat="server" Text='<%$ Resources:LabelCaption,lblAdminPassword %>'></asp:Label>
                        </div>
                        <div>
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control-res"
                                onkeydown="fncAdminAuthentication(event);"></asp:TextBox>
                        </div>
                    </div>
                    <div id="rtgs">
                        <div class="PaymentDialog_yes">
                            <%--<asp:DropDownList ID="ddlBankCode" runat="server" Style="width: 150px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtBankCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'BankCode', 'txtBankCode', '');"></asp:TextBox>
                        </div>
                        <div class="payment_rtgs_button">
                            <asp:LinkButton ID="lnkrtgsok" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                OnClick="lnkrtgsok_Click" OnClientClick="return fncBankcodecheck()"> </asp:LinkButton>
                        </div>
                    </div>
                    <div id="divNEFT" style="display: none" title="Enterpriser Web">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="../images/icici_bank _logo_.jpg" style="width: 100%;" /><%--style ="background-image: url('../images/icici_bank_logo.jpg');"--%>
                                </div>
                                <div class="col-md-8"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label11" runat="server" Text="Balacne"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Label ID="lblBalance" runat="server" Style="font-weight: 900; font-size: 18px;" Text="0"></asp:Label>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label26" runat="server" Text="From Account No"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:DropDownList ID="ddlAccoutnNo" CssClass="form-control-res" runat="server" Width="100%"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label12" runat="server" Text="To Account No"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtToAccountNo" onkeypress="return numericOnly(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label18" runat="server" Text="IFSC Code"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtIfscCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label19" runat="server" Text="Payee Name"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtPayeeName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label27" runat="server" Text="Transaction Amount"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtTrAmt" onkeypress="return numericOnly(event);" runat="server" CssClass="form-control-res"
                                            Style="text-align: right;" Text="0"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label20" runat="server" Text="Remarks"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 5px;">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="control-button ">
                                            <asp:LinkButton ID="lnkPay" runat="server" class="button-red" Text="Pay" OnClientClick="return fncCheckBankDetail();"
                                                OnClick="lnkPay_Click"></asp:LinkButton>
                                        </div>
                                        <div class="control-button ">
                                            <asp:LinkButton ID="LinkButton2" runat="server" class="button-red" Text="Clear" OnClientClick="fncBank_Detail_Clear();return false;"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divOTP" style="display: none" title="Enterpriser Web">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="../images/icici_bank _logo_.jpg" style="width: 100%;" />
                                </div>
                                <div class="col-md-8"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label22" runat="server" Text="Payee Name"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtVerifyPayeeName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label14" runat="server" Text="From Acc.No"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtFrAccNo" onkeypress="return numericOnly(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label15" runat="server" Text="To Acc.No"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtToAccNo" onkeypress="return numericOnly(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label21" runat="server" Text="IFSC Code"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtVerifyIfscCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label16" runat="server" Text="Tran Amount"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtTraAmt" onkeypress="return numericOnly(event);" runat="server" Style="text-align: right;"
                                            CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label17" runat="server" Text="Tran ID"></asp:Label>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtTranID" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="control-group-single-res" style="margin-top: 10px;">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label13" runat="server" Text="OTP"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtVerifyOTP" onkeypress="return numericOnly(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                    <div class="control-button ">
                                        <asp:LinkButton ID="lnkVerify" runat="server" OnClientClick="return fncVerifyValidation();" class="button-red" Text="Verify" OnClick="lnkVerify_Click"></asp:LinkButton>
                                    </div>
                                    <div class="control-button ">
                                        <asp:LinkButton ID="lnkResend" runat="server" class="button-red" Text="Resend" OnClick="lnkPay_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rdoRtgs" runat="server" GroupName="Tran" AutoPostBack="false" Checked="true" />
                                        RTGS
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rdoNeft" runat="server" GroupName="Tran" AutoPostBack="false" />
                                        NEFT
                                    </div>
                                </div>
                                <div class="col-md-2" style="display: none;">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rdoImps" runat="server" GroupName="Tran" AutoPostBack="false" />IMPS
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rdoOwn" runat="server" GroupName="Tran" AutoPostBack="false" />OWN to OWN
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="control-button">
                                        <asp:RadioButton ID="rdoExternal" runat="server" GroupName="Tran" AutoPostBack="false" />Own to Ex
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hidPAYNeft" runat="server" />
                    <asp:HiddenField ID="hidBalance" runat="server" Value="" />
                    <asp:HiddenField ID="hidVendor" runat="server" Value="" />
                    <asp:HiddenField ID="hidOTPStatus" runat="server" Value="" />
                    <asp:HiddenField ID="hidIfscCode" runat="server" Value="" />
                    <asp:HiddenField ID="hidPayee" runat="server" Value="" />
                    <asp:HiddenField ID="hidRemarks" runat="server" Value="" />
                    <asp:HiddenField ID="hidVendorName" runat="server" Value="" />
                </div>
                <div style="display: none;">
                    <asp:Button ID="lnkBalance" runat="server" OnClick="lnkBalance_Click" />
                </div>

            </ContentTemplate>

        </asp:UpdatePanel>
        <div>
            <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="display_none">
                <asp:Button ID="btnExcelExport" runat="server" OnClick="lnkExport_Click" />
                <asp:HiddenField ID="hidSort" runat="server" />
            </div>
        </div>
        <asp:HiddenField ID="hidLocation" runat="server" />
        <asp:HiddenField ID="hidTextile" runat="server" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
        <asp:HiddenField ID="hidFdate" runat="server" />
        <asp:HiddenField ID="hidTdate" runat="server" />

    </div>
</asp:Content>
