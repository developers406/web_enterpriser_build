﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmJobOrderMnt.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmJobOrderMnt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .job_rptr_textbox_width {
            text-align: center !important;
            border: none;
            background-color: aliceblue;
        }

        #tblFinishedGoods th {
            background-color: #4163e1;
            color: white;
        }

        #tblRawMaterial tbody {
            height: 100px;
        }

        .gvVendorLoadItemdummy tbody {
            height: 100px;
        }

        .gvVendorLoadItemdummy td:nth-child(1), .gvVendorLoadItemdummy th:nth-child(1) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(2), .gvVendorLoadItemdummy th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(3), .gvVendorLoadItemdummy th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(4), .gvVendorLoadItemdummy th:nth-child(4) {
            min-width: 85px;
            max-width: 85px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(5), .gvVendorLoadItemdummy th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(6), .gvVendorLoadItemdummy th:nth-child(6) {
            min-width: 240px;
            max-width: 240px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(7), .gvVendorLoadItemdummy th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .gvVendorLoadItemdummy td:nth-child(8), .gvVendorLoadItemdummy th:nth-child(8) {
            min-width: 85px;
            max-width: 85px;
        }

        .gvVendorLoadItemdummy td:nth-child(8) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(9), .gvVendorLoadItemdummy th:nth-child(9) {
            min-width: 85px;
            max-width: 85px;
        }

        .gvVendorLoadItemdummy td:nth-child(9) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(10), .gvVendorLoadItemdummy th:nth-child(10) {
            min-width: 85px;
            max-width: 85px;
        }

        .gvVendorLoadItemdummy td:nth-child(10) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(11), .gvVendorLoadItemdummy th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .gvVendorLoadItemdummy td:nth-child(11) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(12), .gvVendorLoadItemdummy th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .gvVendorLoadItemdummy td:nth-child(12) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(13), .gvVendorLoadItemdummy th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .gvVendorLoadItemdummy td:nth-child(13) {
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(14), .gvVendorLoadItemdummy th:nth-child(14) {
            min-width: 70px;
            max-width: 70px;
        }

        .gvVendorLoadItemdummy td:nth-child(15), .gvVendorLoadItemdummy th:nth-child(15) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(16), .gvVendorLoadItemdummy th:nth-child(16) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(17), .gvVendorLoadItemdummy th:nth-child(17) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(18), .gvVendorLoadItemdummy th:nth-child(18) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(19), .gvVendorLoadItemdummy th:nth-child(19) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(20), .gvVendorLoadItemdummy th:nth-child(20) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(21), .gvVendorLoadItemdummy th:nth-child(21) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .gvVendorLoadItemdummy td:nth-child(21), .gvVendorLoadItemdummy th:nth-child(21) {
            display: none;
        }

        .poSummarytbl tbody {
            height: 100px;
        }

        .poSummary td:nth-child(1), .poSummary th:nth-child(1) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .poSummary td:nth-child(2), .poSummary th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .poSummary td:nth-child(3), .poSummary th:nth-child(3) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .poSummary td:nth-child(4), .poSummary th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .poSummary td:nth-child(5), .poSummary th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .poSummary td:nth-child(6), .poSummary th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .poSummary td:nth-child(7), .poSummary th:nth-child(7) {
            min-width: 280px;
            max-width: 280px;
            text-align: center !important;
        }

        .poSummary td:nth-child(8), .poSummary th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .poSummary td:nth-child(9), .poSummary th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(9) {
            text-align: right !important;
        }

        .poSummary td:nth-child(10), .poSummary th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(10) {
            text-align: right !important;
        }

        .poSummary td:nth-child(11), .poSummary th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(11) {
            text-align: right !important;
        }

        .poSummary td:nth-child(12), .poSummary th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .poSummary td:nth-child(12) {
            text-align: right !important;
        }

        .poSummary td:nth-child(13), .poSummary th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .poSummary td:nth-child(14), .poSummary th:nth-child(14) {
            min-width: 70px;
            max-width: 70px;
            display: none;
        }

        #tblProcessValue {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #tblProcessValue th {
                padding-top: 2px;
                padding-bottom: 2px;
                background-color: #E91E63;
                font-weight: bolder;
                color: white;
            }


        .poSummaryView td:nth-child(1), .poSummaryView th:nth-child(1) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .poSummaryView td:nth-child(2), .poSummaryView th:nth-child(2) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .poSummaryView td:nth-child(3), .poSummaryView th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .poSummaryView td:nth-child(4), .poSummaryView th:nth-child(4) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .poSummaryView td:nth-child(5), .poSummaryView th:nth-child(5) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .poSummaryView td:nth-child(6), .poSummaryView th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .poSummaryView td:nth-child(7), .poSummaryView th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .poSummaryView td:nth-child(8), .poSummaryView th:nth-child(8) {
            display: none;
        }

        .poSummaryView tbody {
            height: 100px;
        }

        #tblProcessValue td {
            padding: 2px !important;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'JobOrderMnt');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "JobOrderMnt";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
            $(document).on('keydown', disableFunctionKeys);
        });
        $(function () {
            var colId = $("#tblFinishedGoods td:first").attr('id');
        });
        function disableFunctionKeys(e) {
            try {
                var charCode = (e.which) ? e.which : e.keyCode;
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 117) {//F6 for clear form
                        clearForm();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 115) {//F4 for save function
                        fncSaveValidation();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 119) {//F8 for Bact to Previous Form
                        fncBacktoPreviousform();
                        e.preventDefault();
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBacktoPreviousform() {
            try {
                window.location = "frmJobOrder.aspx";
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function clearForm() {
            $("#tblFinishedGoods tbody").empty();
            $("#tblRawMaterial tbody").empty();
            fncSubCrear();
            $('#<%=txtTotalValue.ClientID%>').val('0.00');
            $('#<%=txtAdvancePaid.ClientID%>').val('0.00');
            $('#<%=txtBalanceAmount.ClientID%>').val('0.00');
            $('#<%=txtOrderFromCode.ClientID%>').val('');
            $('#<%=txtOrderFromName.ClientID%>').val('');
            $('#<%=txtOrderToCode.ClientID%>').val('');
            $('#<%=txtOrderToName.ClientID%>').val('');
            $('#<%=txtRemarks.ClientID%>').val('');
            return false;
        }
        //----------------->Get Current Date<--------------------
        var CurrentDate;
        function createDate() {
            try {
                CurrentDate = $.datepicker.formatDate("dd-mm-yy", new Date());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //------------------------------------------------------------------
    </script>
    <script type="text/javascript">
        var TranNo;
        function pageLoad() {
            $('#divGstPer').hide();
            $('#divGstAmt').hide();
            $('#divGSTDetails').hide();
            $('#<%=txtGstPer.ClientID %>').number(true, 2);
            $('#<%=txtGstAmt.ClientID %>').number(true, 2);
            $("#thEdit").hide();
            createDate();
            TranNo = MakeTranId();
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $('#<%=txtOrderId.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtOrderDate.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtBalanceAmount.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtItemNameAdd.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtBatch.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtTotQty.ClientID%>').attr("readonly", "readonly");
            $('#<%=txtCost.ClientID%>').attr("readonly", "readonly");

            <%--$('#<%=ddlMaterialType.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlTranType.ClientID %>').trigger("liszt:updated");--%>
            if ($('#<%=hdOrderId.ClientID %>').val() != '') {
                var ordrid = $('#<%=hdOrderId.ClientID %>').val();
                //$('#ContentPlaceHolder1_txtTranType').hide();

                fncGetDataforRawandFinishedTbl();
            }
            else {
                $('#<%=txtTranType.ClientID%>').val('Initial');
                $('#<%=txtTranType.ClientID%>').attr("readonly", "readonly");
            }
            $('#<%=txtItemCodeAdd.ClientID %>').select().focus();

            $('#<%=chkGst.ClientID %>').change(function () {
                if (this.checked) {
                    if ($("#tblFinishedGoods tbody").children().length > 1 || $("#tblRawMaterial tbody").children().length > 0) {
                        $('#<%=chkGst.ClientID %>').prop('checked', false);
                        ShowPopupMessageBox("Please Clear All Finished and Raw Materials Details.");
                    }
                }
                else {
                    if ($("#tblFinishedGoods tbody").children().length > 1 || $("#tblRawMaterial tbody").children().length > 0) {
                        $('#<%=chkGst.ClientID %>').prop('checked', true);
                        ShowPopupMessageBox("Please Clear All Finished and Raw Materials Details.");
                    }
                }
            });
        }

        function fncGetDataforRawandFinishedTbl() {
            try {
                $("tblFinishedGoods").children().remove();
                $("tblRawMaterial").children().remove();
                $("tbody").children().remove();
                var obj = {};
                var tblbody, row;
                var FN_Rno = 0;
                var RW_Rno = 0;
                obj.Itemcode = $('#<%=hdOrderId.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: 'frmJobOrderMnt.aspx/fncGetDataforRawandFinishedData',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {

                        var objBindData = jQuery.parseJSON(data.d);

                        if (objBindData.Table1.length > 0) {
                            //tblRawMaterial.children().remove(); 
                            $("#thEdit").show();
                            tblbody = $("#tblRawMaterial tbody");
                            //$("#tblFinishedGoods tr").addClass("Payment_fixed_headers");

                            for (var i = 0; i < objBindData.Table1.length; i++) {

                                RW_Rno = parseFloat(i) + parseFloat(1);
                                row = "<tr onkeydown='return RawMaterialReadOnlyColumn(event,this);'>" +

                                    "<td><input type='text' id='txtSNo_" + RW_Rno + "' value='" + objBindData.Table1[i]["SNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td id ='tdEdit' style ='display:block;'><img src='../images/edit-icon.png' onclick ='fncEdit(this);' style='cursor:pointer'></td>" +
                                    "<td ><img src='../images/No.png' onclick ='fncRemoveAddOns(this);' style='cursor:pointer'></td>" +
                                    "<td><input type='text' id='txtTranNo_" + RW_Rno + "' value='" + objBindData.Table1[i]["TranNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTranDate_" + RW_Rno + "' value='" + objBindData.Table1[i]["TranDate"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtItemCode_" + RW_Rno + "' value='" + objBindData.Table1[i]["Inventorycode"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtItemName_" + RW_Rno + "' title ='" + objBindData.Table1[i]["Description"] + "' value='" + objBindData.Table1[i]["Description"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtBatch_" + RW_Rno + "' value='" + objBindData.Table1[i]["BatchNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtAcQty_" + RW_Rno + "' value='" + objBindData.Table1[i]["OrderQty"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtWastage_" + RW_Rno + "' value='" + objBindData.Table1[i]["Wastage"] + "'" + "onkeydown='return fncSetFocusNextRowandCol(event,this);' onfocus ='this.select();' onchange='fncWastageChange(this);return false;' Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTotalQty_" + RW_Rno + "' value='" + objBindData.Table1[i]["TotalQty"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTotalCost_" + RW_Rno + "' value='" + objBindData.Table1[i]["TotalCost"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTranType_" + RW_Rno + "' value='" + objBindData.Table1[i]["TranType"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtflag_" + FN_Rno + "' value='Old'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "</tr>";
                                tblbody.append(row);
                            }
                        }
                        if (objBindData.Table.length > 0) {
                            //tblFinishedGoods.children().remove();
                            tblbody = $("#tblFinishedGoods tbody");
                            $('#divVendorLoadEntry').removeClass("gvVendorLoadItemdummy");

                            document.getElementById("tblFinishedGoods").deleteTHead();

                            if ($("#tblFinishedGoods tbody").children().length == 0) {
                                $('#tblFinishedGoods').find('th').remove();
                                row = "<tr class ='Payment_fixed_headers'><th>SNO</th>" + "<th>Delete</th>" + "<th>Tran No</th>" + "<th>Tran Date</th>" + "<th>Item Code</th>" + "<th>Item Name</th>" + "<th>Batch No</th>" + "<th>Ac.Qty</th>" + "<th>Al.Rec</th>" + "<th>Rec.Qty</th>" + "<th>WG(pc)</th>" + "<th>Bal.Qty</th>" + "<th>Tot.WG</th>" + "<th>Total Cost</th>" + "<th>Making Fee</th>" + "<th>SGST Per</th>" + "<th>CGST Per</th>" + "<th>SGST Amt</th>" + "<th>CGST Amt</th>" + "<th>Total GST</th>" + "<th>Total Fee</th>" + "<th style ='display:none'>Flag</th></tr>";
                            }
                            for (var i = 0; i < objBindData.Table.length; i++) {
                                FN_Rno = parseFloat(i) + parseFloat(1);
                                row += "<tr id='tblFinishedGoodsrow_" + FN_Rno + "' onkeypress=' return fncGetAcQty(event,this);' onkeydown='return FinishedGoodsReadOnlyColumn(event,this);'>" +
                                    "<td><input type='text' id='txtSNo_" + FN_Rno + "' value='" + objBindData.Table[i]["SNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td ><img src='../images/No.png' onclick ='fncRemoveAddOnsFinish(this);' style='cursor:pointer'></td>" +
                                    "<td><input type='text' id='txtTranNo_" + FN_Rno + "' value='" + objBindData.Table[i]["TranNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTranDate_" + FN_Rno + "' value='" + objBindData.Table[i]["TranDate"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtItemCode_" + FN_Rno + "' value='" + objBindData.Table[i]["Inventorycode"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtItemName_" + FN_Rno + "' title ='" + objBindData.Table[i]["Description"] + "' value='" + objBindData.Table[i]["Description"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtBatch_" + FN_Rno + "' value='" + objBindData.Table[i]["BatchNo"] + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtAcQty_" + FN_Rno + "' value='" + objBindData.Table[i]["OrderQty"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onkeydown='return FinishedGoodsReadOnlyColumn(event)'/></td>" +
                                    "<td><input type='text' id='txtAlrecs_" + FN_Rno + "' value='" + objBindData.Table[i]["ReceivedQty"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onkeydown='return FinishedGoodsReadOnlyColumn(event)'/></td>" +
                                    "<td><input type='text' id='txtRecQty_" + FN_Rno + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onchange='fncCalcBalQty(this,\"Bal\");' onkeypress='return numericOnly(event);' onFocus='this.select();' onkeydown=' return fncSetNextFocus(event,this,\"RecQty\");'/></td>" +
                                    "<td><input type='text' id='txtWG_" + FN_Rno + "' value='" + objBindData.Table[i]["Weight(pc)"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onchange='fncCalcBalQty(this,\"WG\");' onkeypress='return numericOnly(event);' onFocus='this.select();' onkeydown=' return fncSetNextFocus(event,this,\"WG\");'/></td>" +
                                    "<td><input type='text' id='txtBalanceQty_" + FN_Rno + "' value='" + objBindData.Table[i]["BalanceQty"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTotWG_" + FN_Rno + "'value='" + objBindData.Table[i]["TotalWeight(pc)"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtTotalCost_" + FN_Rno + "' value='" + objBindData.Table[i]["TotalCost"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtMakingFee_" + FN_Rno + "' value='" + objBindData.Table[i]["MakingCharge"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td><input type='text' id='txtSGSTPer_" + FN_Rno + "' value='" + objBindData.Table[i]["SGSTPer"] + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" +
                                    "<td><input type='text' id='txtCGSTPer_" + FN_Rno + "' value='" + objBindData.Table[i]["CGSTPer"] + "'" + "Class='grn_rptr_textbox_width form-control-res'readonly ='true' " + "'/></td>" +
                                    "<td><input type='text' id='txtSGSTAmt_" + FN_Rno + "' value='" + objBindData.Table[i]["SGSTAmt"] + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" +
                                    "<td><input type='text' id='txtCGSTAmt_" + FN_Rno + "' value='" + objBindData.Table[i]["CGSTAmt"] + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" +
                                    "<td><input type='text' id='txtTotalGST_" + FN_Rno + "' value='" + objBindData.Table[i]["TotalGST"] + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" +
                                    "<td><input type='text' id='txtMakingtotal_" + FN_Rno + "' value='" + objBindData.Table[i]["TotalValue"] + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "<td style ='display:none'><input type='text' id='txtflag_" + FN_Rno + "' value='Old'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                                    "</tr>";
                                tblbody.append(row);
                                row = '';
                            }
                            fncAddColinFin();
                            if (objBindData.Table2.length > 0) {
                                fncAddValues(objBindData.Table2);
                            }
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var AcQty;
        function fncGetAcQty(evt, source) {
            var charCode, rowobj, scrollheight;
            try {
                rowobj = $(source);
                var tweight;
                AcQty = rowobj.find('td input[id*="txtAcQty"]').val();
                var colId = $("#tblFinishedGoods td:first").attr('id');
                //alert(colId);
                //return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncCalcBalQty(source, val) {
            var rowobj, BalQty = 0, RecQty = 0, ActQty = 0, AlQty = 0;
            rowobj = $(source).parent().parent();
            try {
                RecQty = parseFloat(rowobj.find('td input[id*="txtRecQty"]').val());
                if (val == "Bal") {
                    BalQty = parseFloat(rowobj.find('td input[id*="txtBalanceQty"]').val());
                    ActQty = parseFloat(rowobj.find('td input[id*="txtAcQty"]').val());
                    AlQty = parseFloat(rowobj.find('td input[id*="txtAlrecs"]').val());

                    if (parseFloat(BalQty) == 0) {
                        BalQty = parseFloat(ActQty) - parseFloat(AlQty);
                        rowobj.find('td input[id*="txtBalanceQty"]').val(BalQty);
                    }

                    if (RecQty > BalQty) {
                        ShowPopupMessageBox('Received Qty should not Greater than Balance qty');
                        BalQty = parseFloat(ActQty) - parseFloat(AlQty);
                        rowobj.find('td input[id*="txtBalanceQty"]').val(BalQty);
                        rowobj.find('td input[id*="txtRecQty"]').val('0');
                        rowobj.find('td input[id*="txtRecQty"]').select().focus();
                    }
                    else {
                        BalQty = parseFloat(ActQty) - parseFloat(RecQty) - parseFloat(AlQty);
                        rowobj.find('td input[id*="txtBalanceQty"]').val(BalQty);
                    }
                }
                if (val == "WG" || val == "Bal") {
                    var WG = 0, TotalWG = 0, ActQty = 0, Rec = 0, Cost = 0;
                    WG = rowobj.find('td input[id*="txtWG"]').val();
                    if (RecQty == '')
                        RecQty = 0;

                    TotalWG = parseFloat(RecQty * WG).toFixed(2);
                    rowobj.find('td input[id*="txtTotalWg"]').val(TotalWG);
                }

                if (val == "PC" || val == "Bal") {
                    var Qty = rowobj.find('td input[id*="txtRecQty"]').val();
                    $("#tblFinishedGoods tbody").children().each(function () {
                        objtbl = $(this);
                        var Weight = objtbl.find('td input[id*="txtWG"]').val();
                        var totweight = 0;
                        if (objtbl.index() > 0) {
                            var tdQty = 0;
                            objtbl.find('td').each(function () {
                                if ($(this).find('input[id*=Raw]').val() != undefined) {
                                    var ItemQty = $(this).find('input[id*=Raw]').val();
                                    var tot = $(this).find('input[id*=WSG]').val();
                                    if (parseFloat(ItemQty) > 0) {
                                        tdQty = parseFloat(Qty * ItemQty).toFixed(2);
                                        totweight = parseFloat(totweight) + parseFloat(ItemQty);
                                    }
                                }
                                else if ($(this).find('input[id*=WSG]').val() != undefined) {
                                    $(this).find('input[id*=WSG]').val(tdQty);
                                    tdQty = 0;
                                }
                            });
                        }
                        if (parseFloat(totweight) > parseFloat(Weight)) {
                            ShowPopupMessageBox("Child Weight is Must be Lessar than or Equal to Weight");
                            objtbl.find('td input[id*="Raw"]').val('0');
                            objtbl.find('td input[id*="WSG"]').val('0');
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //------------------------->After Search Grid Set Value in Text Box<----------------------------------------------
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemNameAdd.ClientID %>').val($.trim(Description));
                    $('#<%=txtItemCodeAdd.ClientID %>').attr('disabled', 'disabled');
                    fncGetAverageCost();
                }
                if (SearchTableName == "Member") {
                    $('#<%=txtOrderFromCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtOrderFromName.ClientID %>').val($.trim(Description));
                }
                if (SearchTableName == "Vendor") {
                    $('#<%=txtOrderToCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtOrderToName.ClientID %>').val($.trim(Description));
                }
                if (SearchTableName == "MaterialType") {
                    if (Code == 'RAW') {
                        $('#<%=divMakingFee.ClientID %>').hide();
                        $('#<%=divWastage.ClientID %>').show();
                    }
                    if (Code == 'Finished') {
                        $('#<%=divMakingFee.ClientID %>').show();
                        $('#<%=divWastage.ClientID %>').hide();
                        $('#divGstPer').show();
                        $('#divGstAmt').show();
                        $('#divGSTDetails').show();
                        $('#<%=txtSGSTPer.ClientID %>').number(true, 2);
                        $('#<%=txtSGSTAmt.ClientID %>').number(true, 2);
                        $('#<%=txtCGSTPer.ClientID %>').number(true, 2);
                        $('#<%=txtCGSTAmt.ClientID %>').number(true, 2);
                    }
                    else {
                        $('#divGstPer').hide();
                        $('#divGstAmt').hide();
                        $('#divGSTDetails').hide();
                    }
                }

                if (SearchTableName == "TranType") {
                    $('#<%=lnkAddInv.ClientID%>').focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //
        //----------------------------------->Get Item Cost from DB<----------------------------------------------
        function fncGetAverageCost() {
            try {
                var InvCode;
                InvCode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                var obj = {};
                obj.Itemcode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: 'frmJobOrderMnt.aspx/fncGetAverageCost',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {

                        var objdata = jQuery.parseJSON(data.d);

                        if (objdata.Table.length > 0) {
                            $("#<%=txtCost.ClientID %>").val(objdata.Table[0]["AverageCost"]);
                            $("#<%=hdUOM.ClientID %>").val(objdata.Table[0]["UOM"]);
                            $("#HidenIsBatch").val(objdata.Table[0]["Batch"]);

                            $("#<%=txtGstPer.ClientID %>").val(objdata.Table[0]["ItaxPer3"]);
                            $("#<%=txtSGSTPer.ClientID %>").val(objdata.Table[0]["ItaxPer1"]);
                            $("#<%=txtCGSTPer.ClientID %>").val(objdata.Table[0]["ItaxPer2"]);
                            if ($("#HidenIsBatch").val() == 'true') {
                                fncGetBatchDetail_Master(InvCode);
                            }
                        }
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock, netCost) {
            try {
                <%--$('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtSellingPrice.ClientID%>').val(sprice);--%>
                $('#<%=txtBatch.ClientID%>').val(batchno);
                <%--$('#<%=txtStock.ClientID%>').val(stock);
                $('#<%=txtCost.ClientID%>').val(netCost);--%>

                <%--fncVendorDetails($('#<%=txtInventoryCode.ClientID%>').val());--%>
                //ShowPopupBatch("Select Vendor");
            }
            catch (err) {
                alert(err.message);
            }
        }
        //---------------------------------------->Set Enter Focus<-------------------------------------
        function fncSetEnterFocus(event, setFocusControl) {
            try {

                var FocusId;
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) //For Enter lnkAddInv
                {
                    if ($('#<%=txtMaterialType.ClientID %>').val() == 'Finished' && setFocusControl == 'txtWastage') {
                        FocusId = 'txtMakingFee';
                        $('#ContentPlaceHolder1_' + FocusId).select().focus();
                    }
                    else if ($('#<%=txtMaterialType.ClientID %>').val() == 'RAW' && setFocusControl == 'txtWastage') {
                        FocusId = 'txtWastage';
                        $('#ContentPlaceHolder1_' + FocusId).select().focus();
                    }
                    else if ($('#<%=txtOrderId.ClientID %>').val() == '' && setFocusControl == 'txtTranType') {
                        $('#<%=lnkAddInv.ClientID%>').focus();
                    }
                    else if (setFocusControl == 'lnkAddInv') {
                        if ($('#<%=txtTranType.ClientID %>').val() == '' || $('#<%=txtTranType.ClientID %>').val() != '') {
                            $('#<%=lnkAddInv.ClientID%>').focus();
                        }
                    }
                    else {
                        $('#ContentPlaceHolder1_' + setFocusControl).select().focus();
                    }
                    return false;
                }
                else {
                    if (setFocusControl == 'lnkAddInv') {
                        fncShowSearchDialogCommon(event, 'TranType', 'txtTranType', 'lnkAddInv');
                    }

                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //---------------------------------------------->Add Value Into Table<------------------------------------------------

        function fncAddValidation() {
            try {
                var status = true;
                if ($('#<%=txtItemCodeAdd.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Please Enter Valid Inventory Code');
                    $('#<%=txtItemCodeAdd.ClientID %>').focus().select();
                    status = false;
                }
                else if ($('#<%=txtMaterialType.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Please Select Material Type');
                    $('#<%=txtMaterialType.ClientID %>').focus().select();
                    status = false;
                }
                else if ($('#<%=txtGivenQty.ClientID %>').val() == '' || parseFloat($('#<%=txtGivenQty.ClientID %>').val()) <= 0) {
                    ShowPopupMessageBox('Please Enter Given Qty');
                    $('#<%=txtGivenQty.ClientID %>').focus().select();
                    status = false;
                }
                else if ($('#<%=txtTranType.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Please Select Tran Type');
                    $('#<%=txtTranType.ClientID %>').focus().select();
                    status = false;
                }
                else if (parseFloat($('#<%=txtGivenQty.ClientID %>').val()) < parseFloat($('#<%=txtWastage.ClientID %>').val())) {
                    ShowPopupMessageBox('Given Qty is Must be Grater than or Equal to Wastage Qty');
                    $('#<%=txtWastage.ClientID %>').focus().select();
                    status = false;
                }
                if (status) {
                    if (parseFloat($('#<%=txtGivenQty.ClientID %>').val()) > 0 && $('#<%=hdUOM.ClientID %>').val().trim() == 'PCS') {

                        var val, dec;
                        val = $('#<%=txtGivenQty.ClientID %>').val();
                        dec = val.split('.')[1];
                        if (parseFloat(dec) > 0) {
                            ShowPopupMessageBox('Pcs Qty Not Allow Decimal');
                            $('#<%=txtGivenQty.ClientID %>').focus().select();
                            status = false;
                        }
                    }
                }
                if (status) {
                    if ($('#<%=txtMaterialType.ClientID %>').val() == 'RAW' && parseFloat($('#<%=txtWastage.ClientID %>').val()) > 0 && $('#<%=hdUOM.ClientID %>').val().trim() == 'PCS') {
                        var val, dec;
                        val = $('#<%=txtWastage.ClientID %>').val();
                        dec = val.split('.')[1];
                        if (parseFloat(dec) > 0) {
                            ShowPopupMessageBox('Pcs Qty Not Allow Decimal');
                            $('#<%=txtWastage.ClientID %>').focus().select();
                            status = false;
                        }
                    }
                }
                if (status) {
                    status = fncAddtblvalidation();
                }
                if (status == true) {
                    AddValueintbl();
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                }
                fncSubCrear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAddtblvalidation() {
            var status = true, ItemCode;
            var rowObj;
            try {
                ItemCode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                if ($('#<%=txtMaterialType.ClientID %>').val() == 'Finished') {
                    $("#tblFinishedGoods tbody").children().each(function () {
                        rowObj = $(this);
                        if (rowObj.find('td input[id*="txtflag"]').val() == 'New' && rowObj.find('td input[id*="txtItemCode"]').val() == ItemCode) {
                            ShowPopupMessageBox('Item already exisits,Try some other item.');
                            fncSubCrear()
                            status = false;
                        }
                    });
                }
                if ($('#<%=txtMaterialType.ClientID %>').val() == 'RAW') {
                    $("#tblRawMaterial tbody").children().each(function () {
                        rowObj = $(this);
                        if (rowObj.find('td input[id*="txtflag"]').val() == 'New' && rowObj.find('td input[id*="txtItemCode"]').val() == ItemCode) {
                            ShowPopupMessageBox('Item already exisits,Try some other item.');
                            fncSubCrear()
                            status = false;
                        }
                    });
                }
                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var F_SNo = 0, R_SNO = 0;

        function AddValueintbl() {
            var tblValue, TranType;
            var totalcost = 0, flag = 'New', makingtotal = 0, TotalQty = 0;
            var recQty = 0, balanceQty = 0, tblLength = 0;
            try {
                balanceQty = $('#<%=txtGivenQty.ClientID %>').val();

                R_SNO = $('#tblRawMaterial').find('tr').length;
                F_SNo = $('#tblFinishedGoods').find('tr').length;
                if ($('#<%=hdOrderId.ClientID %>').val() != "") {
                    TranType = $('#<%=txtTranType.ClientID %>').val();
                }
                else {
                    TranType = $('#<%=txtTranType.ClientID %>').val();
                }
                if ($('#<%=txtMaterialType.ClientID %>').val() == 'RAW') {
                    totalcost = parseFloat($('#<%=txtTotQty.ClientID %>').val()) * parseFloat($('#<%=txtCost.ClientID %>').val());
                    TotalQty = $('#<%=txtTotQty.ClientID %>').val();
                    if (TranType == 'Addition' || TranType == 'Initial') {
                        TotalQty = parseFloat($('#<%=txtTotQty.ClientID %>').val()) * (-1);
                    }
                    tblValue = "<tr onkeydown='return RawMaterialReadOnlyColumn(event,this);'>" + "<td><input type='text' id='txtSNo_" + R_SNO + "' value='" + R_SNO + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td id ='tdEdit_" + R_SNO + "' style ='display:block;'><img src='../images/edit-icon.png' onclick ='fncEdit(this);' style='cursor:pointer'></td>"
                        + "<td><img src='../images/No.png' onclick ='fncRemoveAddOns(this);' style='cursor:pointer'></td>"
                        + "<td><input type='text' id='txtTranNo_" + R_SNO + "' value='" + TranNo + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTranDate_" + R_SNO + "' value='" + CurrentDate + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtItemCode_" + R_SNO + "' value='" + $('#<%=txtItemCodeAdd.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtItemName_" + R_SNO + "' title='" + $('#<%=txtItemNameAdd.ClientID %>').val() + "' value='" + $('#<%=txtItemNameAdd.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtBatch_" + R_SNO + "' value='" + $('#<%=txtBatch.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtAcQty_" + R_SNO + "' value='" + parseFloat($('#<%=txtGivenQty.ClientID %>').val()) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtWastage_" + R_SNO + "' value='" + parseFloat($('#<%=txtWastage.ClientID %>').val()) + "'" + "onkeydown=' return fncSetFocusNextRowandCol(event,this);' onfocus ='this.select();' onchange='fncWastageChange(this);return false;' Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTotalQty_" + R_SNO + "' value='" + TotalQty + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTotalCost_" + R_SNO + "' value='" + parseFloat(totalcost).toFixed(2) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTranType_" + R_SNO + "' value='" + TranType + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtflag_" + R_SNO + "' value='" + flag + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td></tr>";
                    $("#tblRawMaterial tbody").append(tblValue);
                    if ($('#<%=hdOrderId.ClientID %>').val() == '') {
                        $("#tdEdit_" + R_SNO + "").hide();
                        $('#thEdit').hide();
                    }

                    R_SNO = R_SNO + 1;
                }
                else {

                    makingtotal = parseFloat($('#<%=txtMakingFee.ClientID %>').val()) * parseFloat($('#<%=txtGivenQty.ClientID %>').val());
                    totalcost = parseFloat($('#<%=txtGivenQty.ClientID %>').val()) * parseFloat($('#<%=txtCost.ClientID %>').val());

                    $('#divVendorLoadEntry').removeClass("gvVendorLoadItemdummy");
                    document.getElementById("tblFinishedGoods").deleteTHead();
                    if ($("#tblFinishedGoods tbody").children().length == 0) {
                        $('#tblFinishedGoods').find('th').remove();
                        tblValue = "<tr class ='Payment_fixed_headers'><th>SNO</th>" + "<th>Delete</th>" + "<th>Tran No</th>" + "<th>Tran Date</th>" + "<th>Item Code</th>" + "<th>Item Name</th>" + "<th>Batch No</th>" + "<th>Ac.Qty</th>" + "<th>Al.Rec</th>" + "<th>Rec.Qty</th>" + "<th>WG(pc)</th>" + "<th>Bal.Qty</th>" + "<th>Tot.WG</th>" + "<th>Total Cost</th>" + "<th>Making Fee</th>" + "<th>SGST Per</th>" + "<th>CGST Per</th>" + "<th>SGST Amt</th>" + "<th>CGST Amt</th>" + "<th>Total GST</th>" + "<th>Total Fee</th>" + "<th style ='display:none'>Flag</th></tr>";
                    }
                    tblValue += "<tr onkeydown='return FinishedGoodsReadOnlyColumn(event,this);'>" + "<td><input type='text' id='txtSNo_" + F_SNo + "' value='" + F_SNo + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><img src='../images/No.png' onclick ='fncRemoveAddOnsFinish(this);' style='cursor:pointer'></td>"
                        + "<td><input type='text' id='txtTranNo_" + F_SNo + "' value='" + TranNo + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTranDate_" + F_SNo + "' value='" + CurrentDate + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtItemCode_" + F_SNo + "' value='" + $('#<%=txtItemCodeAdd.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtItemName_" + F_SNo + "' title='" + $('#<%=txtItemNameAdd.ClientID %>').val() + "' value='" + $('#<%=txtItemNameAdd.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtBatch_" + F_SNo + "' value='" + $('#<%=txtBatch.ClientID %>').val() + "'" + "Class='job_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtAcQty_" + F_SNo + "' value='" + parseFloat($('#<%=txtGivenQty.ClientID %>').val()) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "' readonly/></td>"
                        + "<td><input type='text' id='txtAllrec_" + F_SNo + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtRecQty_" + F_SNo + "' value='" + recQty + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onchange='fncCalcBalQty(this,\"Bal\");' onkeydown='return numericOnly(event)'/></td>"
                        + "<td><input type='text' id='txtWG_" + F_SNo + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onchange='fncCalcBalQty(this,\"WG\");' onkeydown='return numericOnly(event)'/></td>"
                        + "<td><input type='text' id='txtBalanceQty_" + F_SNo + "' value='" + balanceQty + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtTotWG_" + F_SNo + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "' readonly /></td>"
                        + "<td><input type='text' id='txtTotalCost_" + F_SNo + "' value='" + parseFloat(totalcost).toFixed(2) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtMakingFee_" + F_SNo + "' value='" + parseFloat($('#<%=txtMakingFee.ClientID %>').val()).toFixed(2) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td><input type='text' id='txtSGSTPer_" + F_SNo + "' value='" + $('#<%=txtSGSTPer.ClientID %>').val() + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>"
                        + "<td><input type='text' id='txtCGSTPer_" + F_SNo + "' value='" + $('#<%=txtSGSTPer.ClientID %>').val() + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" 
                        + "<td><input type='text' id='txtSGSTAmt_" + F_SNo + "' value='" + $('#<%=txtSGSTAmt.ClientID %>').val() + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" 
                        + "<td><input type='text' id='txtCGSTAmt_" + F_SNo + "' value='" + $('#<%=txtSGSTAmt.ClientID %>').val() + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" 
                        + "<td><input type='text' id='txtTotalGST_" + F_SNo + "' value='" + $('#<%=txtGstAmt.ClientID %>').val() + "'" + "Class='grn_rptr_textbox_width form-control-res' readonly ='true'" + "'/></td>" 
                        + "<td><input type='text' id='txtMakingtotal_" + F_SNo + "' value='" + parseFloat(makingtotal).toFixed(2) + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>"
                        + "<td style ='display:none'><input type='text' id='txtflag_" + F_SNo + "' value='" + flag + "'" + "Class='grn_rptr_textbox_width form-control-res'" + "'/></td>";
                    tblValue += "</tr>";

                    $("#tblFinishedGoods tbody").append(tblValue);
                    if ($('#<%=txtOrderId.ClientID %>').val() != "") {
                        fncAddColinFin();
                    }

                    F_SNo = F_SNo + 1;

                    fncTotalAmountCalc();

                }
                fncSubCrear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAddColinFin() {
            try {
                if ($("#tblRawMaterial tbody").children().length > 0 && $("#tblFinishedGoods tbody").children().length > 0) {
                    var count = document.getElementById('tblFinishedGoods').rows[0].cells.length - 1;
                    if (parseFloat(count) > 16) {
                        $("#tblFinishedGoods tbody").find('th[id*="thItemCode"]').remove();
                        $("#tblFinishedGoods tbody").find('td[id*="tdItemCode"]').remove();
                        $("#tblFinishedGoods tbody").find('th[id*="thItemCodeWG"]').remove();
                        $("#tblFinishedGoods tbody").find('td[id*="tdItemCodeWG"]').remove();
                    }

                    var item = 0, width = 1400, px = 100, pxTot = 0;
                    $("#tblRawMaterial tbody").children().each(function (index) {
                        objtbl = $(this);
                        item = parseFloat(item) + 1;
                        var ItemCode = objtbl.find('td input[id*=txtItemCode]').val();
                        var ItemName = objtbl.find('td input[id*=txtItemName]').val();
                        var tblValueth = "<th id ='thItemCode_'" + item + "'>" + ItemCode + "(PC)" + "</th>";
                        tblValueth += "<th id ='thItemCodeWG_'" + item + "'>" + ItemCode + "(WG)" + "</th>";
                        var tblValuetd = "<td id ='tdItemCode_'" + item + "'><input type='text' id='Raw_" + item + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "' onkeydown=' return fncSetNextFocus(event,this,\"PC\");' onchange='fncCalcBalQty(this,\"PC\");'/></td>";
                        tblValuetd += "<td id ='tdItemCodeWG_'" + item + "'><input type='text' id='WSG_" + item + "' value='0'" + "Class='grn_rptr_textbox_width form-control-res'" + "' readonly /></td>";
                        $("#tblFinishedGoods tbody tr:first").append(tblValueth);
                        $("#tblFinishedGoods tbody tr").append(tblValuetd);
                        pxTot = parseFloat(pxTot) + 1;
                        var Total = parseFloat(width) + parseFloat(px * pxTot);
                        $('#divVendorLoadEntry').width(Total);
                    });
                    $("#tblFinishedGoods tbody tr:first").children('td').remove();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAddValues(source) {
            var obj = source;
            for (var i = 0; i < obj.length; i++) {
                $("#tblFinishedGoods tbody").find('th').each(function () {
                    if ($.trim(obj[i]["Use_ItemCode"]) == $(this).text().match(/.{1,6}/g)[0]) {
                        var id = $(this).index();
                        if ($(this).text().indexOf("PC") != -1) {
                            $("#tblFinishedGoods tbody").children().each(function () {
                                var Rec_Itemcode = $(this).find('td input[id*=txtItemCode]').val();
                                if (Rec_Itemcode != undefined) {
                                    if ($.trim(obj[i]["Ref_ItemCode"]) == $.trim(Rec_Itemcode)) {
                                        var Weight = $("td:eq('" + id + "') input[type='text']", this);
                                        Weight.val(obj[i]["Weight"]);
                                    }
                                }
                            });
                        }
                        else {
                            $("#tblFinishedGoods tbody").children().each(function () {
                                var Rec_Itemcode = $(this).find('td input[id*=txtItemCode]').val();
                                if (Rec_Itemcode != undefined) {
                                    if ($.trim(obj[i]["Ref_ItemCode"]) == $.trim(Rec_Itemcode)) {
                                        var totWeight = $("td:eq('" + id + "') input[type='text']", this);
                                        totWeight.val(obj[i]["TotWeight"]);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }

        function fncTotalAmountCalc() {
            try {
                var Totalmakingfee = 0, MakingFee = 0, Balance = 0, GST = 0, TotalGSt = 0;
                $("#tblFinishedGoods tbody").children().each(function () {
                    if ($(this).index() > 0) {
                        MakingFee = $(this).find('td input[id*="txtMakingtotal"]').val();
                        GST = $(this).find('td input[id*="txtTotalGST"]').val();
                        Totalmakingfee = parseFloat(Totalmakingfee) + parseFloat(MakingFee);
                        TotalGSt = parseFloat(TotalGSt) + parseFloat(GST);
                    }
                });
                $('#<%=txtTotalValue.ClientID %>').val(parseFloat(Totalmakingfee).toFixed(2));
                $('#<%=hidTotalValue.ClientID %>').val(parseFloat(Totalmakingfee).toFixed(2));
                $('#<%=txtTotalGST.ClientID %>').val(parseFloat(TotalGSt).toFixed(2));
                $('#<%=hidTotalGST.ClientID %>').val(parseFloat(TotalGSt).toFixed(2));
                Balance = parseFloat($('#<%=txtTotalValue.ClientID %>').val()) + parseFloat($('#<%=txtTotalGST.ClientID %>').val()) - parseFloat($('#<%=txtAdvancePaid.ClientID %>').val());

                $('#<%=txtBalanceAmount.ClientID %>').val(parseFloat(Balance).toFixed(2))
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function MakeTranId() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }
        function fncRemoveAddOns(source) {
            try {
                var rowObj = $(source).parent().parent();
                var ItemCode = rowObj.find('td input[id*="txtItemCode"]').val();

                fncRemoveHeaderandBody(ItemCode);

                $(source).closest("tr").remove();
                var row = 0;
                $("#tblRawMaterial tbody").children().each(function () {
                    row = parseFloat(row) + 1;
                    objtbl = $(this);
                    objtbl.find('td input[id*=txtSNo]').val(row);
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncRemoveHeaderandBody(ItemCode) {
            try {
                var FItemCode = {};
                var item = 0, width = 1400, px = 50, pxTot = 0;
                FItemCode = $("#tblFinishedGoods tbody").find('th[id*="thItemCode"]').text().match(/.{1,6}/g);
                if (FItemCode != null) {
                    if (FItemCode.length >= 0) {
                        for (var i = 0; i < FItemCode.length; i++) {
                            if ($.trim(FItemCode[i]) == $.trim(ItemCode)) {
                                $("#tblFinishedGoods tbody").find('th').each(function () {
                                    if ($.trim(FItemCode[i]) == $(this).text()) {
                                        var rowIndex = $(this).index();
                                        $("#tblFinishedGoods tbody").find("th:eq(" + rowIndex + ")").remove();//th Remove
                                        $("#tblFinishedGoods tbody tr").find("td:eq(" + rowIndex + ")").remove();//td Remove
                                        pxTot = parseFloat(pxTot) + 1;
                                        var Total = parseFloat(width) - parseFloat(px * pxTot);
                                        $('#divVendorLoadEntry').width(Total);
                                    }
                                });
                            }
                        }
                    }
                }
            }
            catch (e) {
                ShowPopupMessageBox(e.message);
            }
        }
        //------------------------------------------->Total Calculations<-----------------------------------------------------------
        function fncCalcTotalQty() {
            try {
                var TotalQty = 0;
                TotalQty = parseFloat($('#<%=txtWastage.ClientID %>').val()) + parseFloat($('#<%=txtGivenQty.ClientID %>').val());
                $('#<%=txtTotQty.ClientID %>').val(TotalQty);
                if ($('#<%=txtMaterialType.ClientID %>').val() == 'Finished') {
                    fncGSTCalculation();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCalcTotalAmount() {
            try {
                var Balance = 0, TotalValue = 0, AdvancePaid = 0;
                TotalValue = $('#<%=txtTotalValue.ClientID %>').val();
                AdvancePaid = $('#<%=txtAdvancePaid.ClientID %>').val();
                Balance = parseFloat($('#<%=txtTotalValue.ClientID %>').val()) - parseFloat($('#<%=txtAdvancePaid.ClientID %>').val());
                Balance = parseFloat(Balance).toFixed(2);
                $('#<%=txtBalanceAmount.ClientID %>').val(Balance);
                $('#<%=txtTotalValue.ClientID %>').val(parseFloat(TotalValue).toFixed(2));
                $('#<%=txtAdvancePaid.ClientID %>').val(parseFloat(AdvancePaid).toFixed(2));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //-------------------------------->Clrae Function<-----------------------------
        function fncSubCrear() {
            try {
                $('#<%=txtItemCodeAdd.ClientID%>').val('');
                $('#<%=txtItemCodeAdd.ClientID%>').select().focus();
                $('#<%=txtItemNameAdd.ClientID%>').val('');
                $('#<%=txtBatch.ClientID%>').val('');
                $('#<%=txtGivenQty.ClientID%>').val('0');
                $('#<%=txtWastage.ClientID%>').val('0');
                $('#<%=txtTotQty.ClientID%>').val('0');
                $('#<%=txtCost.ClientID%>').val('0.00');
                $('#<%=txtMakingFee.ClientID%>').val('0.00');
                $('#<%=txtMaterialType.ClientID%>').val(''); 
                $('#<%=txtSGSTPer.ClientID %>').val('0.00');
                $('#<%=txtSGSTAmt.ClientID %>').val('0.00');
                $('#<%=txtCGSTPer.ClientID %>').val('0.00');
                $('#<%=txtCGSTAmt.ClientID %>').val('0.00');
                $('#<%=txtGstAmt.ClientID %>').val('0.00');
                $('#<%=txtGstPer.ClientID %>').val('0.00');
                $('#divGstPer').hide();
                $('#divGstAmt').hide();
                $('#divGSTDetails').hide();
                $('#<%=txtItemCodeAdd.ClientID %>').removeAttr('disabled');
                if ($('#<%=txtOrderId.ClientID%>').val() == '') {
                    $('#<%=txtTranType.ClientID%>').val('Initial');
                }
                else {
                    $('#<%=txtTranType.ClientID%>').val('');
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveValidation() {
            try {
                var status = true;
                var fintbllength = $("#tblFinishedGoods > tbody").children().length;
                var rawtbllength = $("#tblRawMaterial > tbody").children().length;
                if ($('#<%=txtOrderFromCode.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Please select valid Order From');
                    $('#<%=txtOrderFromCode.ClientID %>').focus();
                    status = false;

                }
                else if ($('#<%=txtOrderToCode.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Please select valid Order To');
                    $('#<%=txtOrderToCode.ClientID %>').focus();
                    status = false;
                }
                else if ($('#<%=txtTotalValue.ClientID %>').val() == 0) {
                    ShowPopupMessageBox('Total value should not zero');
                    $('#<%=txtTotalValue.ClientID %>').focus();
                    status = false;
                }
                else if (fintbllength == 0) {
                    ShowPopupMessageBox('Please add Finished items first');
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                    status = false;
                }
                else if (rawtbllength == 0) {
                    ShowPopupMessageBox('Please add Raw items first.');
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                    status = false;
                }
                if (status) {
                    status = fncSaveFinishedGoodsTbl();
                }
                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveFinishedGoodsTbl() {
            try {
                var status = false;
                var objtbl;
                var obj = {};
                var FinishedGoodsXml = '<NewDataSet>', ItemName, ItemQty = '', TranNo = '', ItemTotWeight = '';
                var obj_Raw = {};
                $("#tblFinishedGoods tbody").children().each(function () {
                    objtbl = $(this);
                    if (objtbl.index() > 0) {
                        FinishedGoodsXml += "<Table>";
                        FinishedGoodsXml += "<TranNo>" + objtbl.find('td input[id*=txtTranNo]').val() + "</TranNo>";
                        FinishedGoodsXml += "<TranDate>" + objtbl.find('td input[id*=txtTranDate]').val() + "</TranDate>";
                        FinishedGoodsXml += "<Inventorycode>" + objtbl.find('td input[id*=txtItemCode]').val() + "</Inventorycode>";
                        FinishedGoodsXml += "<Description>" + objtbl.find('td input[id*=txtItemName]').val() + "</Description>";
                        FinishedGoodsXml += "<BatchNo>" + objtbl.find('td input[id*=txtBatch]').val() + "</BatchNo>";
                        FinishedGoodsXml += "<OrderQty>" + objtbl.find('td input[id*=txtAcQty]').val() + "</OrderQty>";
                        FinishedGoodsXml += "<ReceivedQty>" + objtbl.find('td input[id*=txtRecQty]').val() + "</ReceivedQty>";
                        FinishedGoodsXml += "<BalanceQty>" + objtbl.find('td input[id*=txtBalanceQty]').val() + "</BalanceQty>";
                        FinishedGoodsXml += "<TotalCost>" + objtbl.find('td input[id*=txtTotalCost]').val() + "</TotalCost>";
                        FinishedGoodsXml += "<MakingCharge>" + objtbl.find('td input[id*=txtMakingFee]').val() + "</MakingCharge>";
                        FinishedGoodsXml += "<TotalValue>" + objtbl.find('td input[id*=txtMakingtotal]').val() + "</TotalValue>";
                        FinishedGoodsXml += "<Weight>" + objtbl.find('td input[id*=txtWG]').val() + "</Weight>";
                        FinishedGoodsXml += "<TotalWeight>" + objtbl.find('td input[id*=txtTotWG]').val() + "</TotalWeight>";
                        //GST Add -Vijay   20201017
                        FinishedGoodsXml += "<SGSTPer>" + objtbl.find('td input[id*=txtSGSTPer]').val() + "</SGSTPer>"; 
                        FinishedGoodsXml += "<SGSTAmt>" + objtbl.find('td input[id*=txtSGSTAmt]').val() + "</SGSTAmt>";  
                        FinishedGoodsXml += "<IGSTAmt>" + objtbl.find('td input[id*=txtTotalGST]').val() + "</IGSTAmt>";
                        FinishedGoodsXml += "</Table>";
                        if ($('#<%=txtOrderId.ClientID %>').val() != "") {
                            TranNo = objtbl.find('td input[id*=txtTranNo]').val();
                            objtbl.find('td').each(function () {
                                if ($(this).find('input[id*=Raw]').val() != undefined) {
                                    ItemQty = ItemQty + $(this).find('input[id*=Raw]').val() + '-';
                                }
                                else if ($(this).find('input[id*=WSG]').val() != undefined) {
                                    ItemTotWeight = ItemTotWeight + $(this).find('input[id*=WSG]').val() + '-';
                                }
                            });
                        }
                    }
                    else {
                        if ($('#<%=txtOrderId.ClientID %>').val() != "") {
                            obj_Raw = objtbl.find('th[id*=thItemCode]').text().match(/.{1,10}/g);
                            if (obj_Raw != null) {
                                var removeItem = [];
                                if (obj_Raw.length > 0) {
                                    for (var i = 0; i < obj_Raw.length; i++) {
                                        if (obj_Raw[i].indexOf("(WG)") != -1) {
                                            removeItem.push(obj_Raw[i]);
                                        }
                                    }
                                }
                                obj_Raw = obj_Raw.filter(function (el) {
                                    return !removeItem.includes(el);
                                });
                            }
                        }
                    }

                });
                var RawXml = '';
                if ($('#<%=txtOrderId.ClientID %>').val() != "") {
                    if (obj_Raw != null) {
                        RawXml = '<NewDataSet>';
                        var Qty = {}, Tot = {};
                        ItemQty = ItemQty.slice(0, -1);
                        ItemTotWeight = ItemTotWeight.slice(0, -1);
                        Qty = ItemQty.split('-');
                        Tot = ItemTotWeight.split('-');
                        $("#tblFinishedGoods tbody").children().each(function () {
                            objtbl = $(this);
                            var Rec_Itemcode = objtbl.find('td input[id*=txtItemCode]').val();
                            if (Rec_Itemcode != undefined) {
                                if (obj_Raw.length > 0) {
                                    var id1 = 16, id2 = 0;
                                    for (var i = 0; i < obj_Raw.length; i++) {

                                        var Qty = '', totQty = '', ival = 0;
                                        if (parseFloat(i) > 1)
                                            ival = 1;
                                        else
                                            ival = i;

                                        id1 = parseFloat(id1) + 1 + parseFloat(ival);
                                        id2 = parseFloat(id1) + 1;
                                        Qty = $("td:eq('" + id1 + "') input[type='text']", this);
                                        totQty = $("td:eq('" + id2 + "') input[type='text']", this);

                                        if (obj_Raw[i].indexOf("(WG)") == -1) {
                                            RawXml += "<Table>";
                                            RawXml += "<OrderID>" + $('#<%=txtOrderId.ClientID %>').val() + "</OrderID>";
                                    RawXml += "<TranNo>" + TranNo + "</TranNo>";
                                    RawXml += "<Ref_Itemcode>" + Rec_Itemcode + "</Ref_Itemcode>";
                                    RawXml += "<Use_Itemcode>" + $.trim(obj_Raw[i].replace("(PC)", " ")) + "</Use_Itemcode>";
                                    RawXml += "<Weight>" + Qty.val() + "</Weight>";
                                    RawXml += "<TotalWeight>" + totQty.val() + "</TotalWeight>";
                                    RawXml += "</Table>";
                                }
                            }
                        }
                    }
                });

                        RawXml = RawXml + '</NewDataSet>'
                        RawXml = escape(RawXml);
                    }

                }

                FinishedGoodsXml = FinishedGoodsXml + '</NewDataSet>'
                FinishedGoodsXml = escape(FinishedGoodsXml);


                var objtbl;
                var RawMaterialsTblXml = '<NewDataSet>';
                $("#tblRawMaterial tbody").children().each(function () {
                    objtbl = $(this);
                    if (objtbl.length > 0) {
                        if (objtbl.find('td input[id*=txtflag]').val() == 'New') {
                            RawMaterialsTblXml += "<Table>";
                            RawMaterialsTblXml += "<TranNo>" + objtbl.find('td input[id*=txtTranNo]').val() + "</TranNo>";
                            RawMaterialsTblXml += "<TranDate>" + objtbl.find('td input[id*=txtTranDate]').val() + "</TranDate>";
                            RawMaterialsTblXml += "<Inventorycode>" + objtbl.find('td input[id*=txtItemCode]').val() + "</Inventorycode>";
                            RawMaterialsTblXml += "<Description>" + objtbl.find('td input[id*=txtItemName]').val() + "</Description>";
                            RawMaterialsTblXml += "<BatchNo>" + objtbl.find('td input[id*=txtBatch]').val() + "</BatchNo>";
                            RawMaterialsTblXml += "<OrderQty>" + objtbl.find('td input[id*=txtAcQty]').val() + "</OrderQty>";
                            RawMaterialsTblXml += "<Wastage>" + objtbl.find('td input[id*=txtWastage]').val() + "</Wastage>";
                            RawMaterialsTblXml += "<TotalQty>" + objtbl.find('td input[id*=txtTotalQty]').val() + "</TotalQty>";
                            RawMaterialsTblXml += "<TotalCost>" + objtbl.find('td input[id*=txtTotalCost]').val() + "</TotalCost>";
                            RawMaterialsTblXml += "<TranType>" + objtbl.find('td input[id*=txtTranType]').val() + "</TranType>";
                            RawMaterialsTblXml += "</Table>";
                        }
                    }
                });

                RawMaterialsTblXml = RawMaterialsTblXml + '</NewDataSet>'
                RawMaterialsTblXml = escape(RawMaterialsTblXml);


                obj.RawMaterialsTblXml = RawMaterialsTblXml;
                obj.FinishedGoodsXml = FinishedGoodsXml;
                obj.RawXml = RawXml;

                $.ajax({
                    type: "POST",
                    url: 'frmJobOrderMnt.aspx/SaveFinishedGoodsTbl',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "0") {
                            status = true;
                        }
                    },
                    error: function (data) {
                        fncToastError('Something Went Wrong')
                    }
                });
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveOrderReceivedTbl() {
            try {
                var status = false;
                var objtbl;
                var ReceivedTblXml = '<NewDataSet>';
                $("#tblFinishedGoods tbody").children().each(function () {
                    objtbl = $(this);
                    if (objtbl.length > 0 && objtbl.find('td input[id*=txtTranNo]').val() != undefined) {

                        ReceivedTblXml += "<Table>";
                        ReceivedTblXml += "<TranNo>" + objtbl.find('td input[id*=txtTranNo]').val() + "</TranNo>";
                        ReceivedTblXml += "<TranDate>" + objtbl.find('td input[id*=txtTranDate]').val() + "</TranDate>";
                        ReceivedTblXml += "<Inventorycode>" + objtbl.find('td input[id*=txtItemCode]').val() + "</Inventorycode>";
                        ReceivedTblXml += "<Description>" + objtbl.find('td input[id*=txtItemName]').val() + "</Description>";
                        ReceivedTblXml += "<BatchNo>" + objtbl.find('td input[id*=txtBatch]').val() + "</BatchNo>";
                        ReceivedTblXml += "<OrderQty>" + objtbl.find('td input[id*=txtAcQty]').val() + "</OrderQty>";
                        ReceivedTblXml += "<ReceivedQty>" + objtbl.find('td input[id*=txtRecQty]').val() + "</ReceivedQty>";
                        ReceivedTblXml += "<BalanceQty>" + objtbl.find('td input[id*=txtBalanceQty]').val() + "</BalanceQty>";
                        ReceivedTblXml += "<TotalCost>" + objtbl.find('td input[id*=txtTotalCost]').val() + "</TotalCost>";
                        ReceivedTblXml += "<MakingCharge>" + objtbl.find('td input[id*=txtMakingFee]').val() + "</MakingCharge>";
                        ReceivedTblXml += "<TotalValue>" + objtbl.find('td input[id*=txtMakingtotal]').val() + "</TotalValue>";
                        ReceivedTblXml += "</Table>";
                    }
                });

                ReceivedTblXml = ReceivedTblXml + '</NewDataSet>'
                ReceivedTblXml = escape(ReceivedTblXml);
                var obj = {};
                obj.ReceivedTblXml = ReceivedTblXml;

                $.ajax({
                    type: "POST",
                    url: 'frmJobOrderMnt.aspx/fncOrderReceived',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "0") {
                            status = true;
                        }
                    },
                    error: function (data) {
                        fncToastError('Something Went Wrong')
                    }
                });
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Initialize Save Dialog
        function fncInitializeSaveDialog(SaveMsg) {
            try {
                $('#<%=lblgaReceiptSave.ClientID %>').text(SaveMsg);
                $("#JobOrderSave").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Save Dialog
        function fncSaveDialogClose() {
            $("#JobOrderSave").dialog('close');
            fncSubCrear();
        }
        //Initialize Order Completed Dialog
        function fncInitializeOrderCompletedDialog() {
            try {
                var Msg;
                Msg = "Are you sure want to complete this order.?";
                $('#<%=lblOrderComplete.ClientID %>').text(Msg);
                $("#OrderComplete").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Close Order Complete Dialog
        function fncOrderCompleteDialogClose() {
            $("#OrderComplete").dialog('close');
            fncSubCrear();
        }
        function fncOrderReceivedValidation() {
            try {
                var status = true;
                if ($('#<%=txtTotalValue.ClientID %>').val() == 0) {
                    ShowPopupMessageBox('Total value should not zero');
                    $('#<%=txtTotalValue.ClientID %>').focus();
                    status = false;

                }
                if (status) {
                    status = fncFintblvalidation();
                }
                if (status) {
                    status = fncBalRecQtyCalc();
                }

                if (status) {
                    status = fncSaveOrderReceivedTbl();
                }
                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncFintblvalidation() {
            var status = true;
            var rowObj;
            try {
                $("#tblFinishedGoods tbody").children().each(function () {
                    rowObj = $(this);
                    if (rowObj.find('td input[id*="txtflag"]').val() == 'New') {
                        ShowPopupMessageBox('Some item newly added. Please Save first then make received.');
                        status = false;
                    }
                    if (rowObj.find('td input[id*="txtRecQty"]').val() == 0) {
                        ShowPopupMessageBox('Please enter received Qty.');
                        status = false;
                    }
                });
                $("#tblRawMaterial tbody").children().each(function () {
                    rowObj = $(this);
                    if (rowObj.find('td input[id*="txtflag"]').val() == 'New') {
                        ShowPopupMessageBox('Some item newly added. Please Save first then make received.');
                        status = false;
                    }
                });
                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBalRecQtyCalc() {
            try {
                var RecQty = 0, BalQty = 0, chkRecQty = 0;
                var status = true;
                $("#tblFinishedGoods tbody").children().each(function () {
                    var rowObj = $(this);
                    chkRecQty = parseFloat(rowObj.find('td input[id*="txtBalanceQty"]').val());
                    if (chkRecQty == 0) {
                        status = false;
                        ShowPopupMessageBox('Please Enter Received Qty');
                    }
                    else {
                        BalQty = parseFloat(BalQty) + parseFloat(rowObj.find('td input[id*="txtBalanceQty"]').val());
                        RecQty = parseFloat(RecQty) + parseFloat(rowObj.find('td input[id*="txtRecQty"]').val());
                    }

                });
                if (RecQty > BalQty) {
                    status = false;
                    ShowPopupMessageBox('Received Qty should not Greater than Balance qty');

                }
                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function AllowNumbersOnly(event) {
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 190 || event.keyCode == 110) {
                // let it happen, don't do anything
            }
            else {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        }
        function FinishedGoodsReadOnlyColumn(event, source) {
            numericOnly(event);
            var rowobj = $(source);
            if ($('#<%=txtOrderId.ClientID %>').val() == '' || rowobj.find('td input[id*="txtflag"]').val() == 'New') {
                rowobj.find('td input[id*="txtRecQty"]').attr("readonly", true);
            }
            rowobj.find('td input[id*="txtTranNo"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTranDate"]').attr("readonly", true);
            rowobj.find('td input[id*="txtItemCode"]').attr("readonly", true);
            rowobj.find('td input[id*="txtItemName"]').attr("readonly", true);
            rowobj.find('td input[id*="txtBatch"]').attr("readonly", true);
            rowobj.find('td input[id*="txtAcQty"]').attr("readonly", true);
            rowobj.find('td input[id*="txtBalanceQty"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTotalCost"]').attr("readonly", true);
            rowobj.find('td input[id*="txtMakingFee"]').attr("readonly", true);
            rowobj.find('td input[id*="txtMakingtotal"]').attr("readonly", true);

            rowobj.find('td input[id*="txtTotalWg"]').attr("readonly", true);
            rowobj.find('td input[id*="txtAlrecs"]').attr("readonly", true);


        }
        function RawMaterialReadOnlyColumn(event, source) {
            var rowobj = $(source);

            numericOnly(event);
            rowobj.find('td input[id*="txtTranNo"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTranDate"]').attr("readonly", true);
            rowobj.find('td input[id*="txtItemCode"]').attr("readonly", true);
            rowobj.find('td input[id*="txtItemName"]').attr("readonly", true);
            rowobj.find('td input[id*="txtBatch"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTotalQty"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTotalCost"]').attr("readonly", true);
            rowobj.find('td input[id*="txtTranType"]').attr("readonly", true);
            rowobj.find('td input[id*="txtAcQty"]').attr("readonly", true);
        }
        function fncOnfocus(data) {
            try {
                if (data == "txtItemCodeAdd") {
                    $('#<%=txtItemCodeAdd.ClientID %>').focus().select();
                }
                if (data == "txtGivenQty") {
                    $('#<%=txtGivenQty.ClientID %>').focus();
                }
                if (data == "txtWastage") {
                    $('#<%=txtWastage.ClientID %>').focus();
                }
                if (data == "txtMakingFee") {
                    $('#<%=txtMakingFee.ClientID %>').focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function numericOnly(elementRef) {

            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {

                return true;

            }

            // '.' decimal point...  

            else if (keyCodeEntered == 46) {

                // Allow only 1 decimal point ('.')...  

                if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))

                    return false;

                else

                    return true;

            }

            return false;

        }
        //Set Enter Focus in Finished Goods Table
        var btnId, tblRowID = 0;
        function fncSetNextFocus(evt, source, value) {
            var rowobj, rowId = 0, NextRowobj, prevrowobj, rowId1 = 0;
            try {
                var tbllength = $("#tblFinishedGoods > tbody").children().length;

                rowobj = $(source).parent().parent();
                rowId = rowobj.find('td input[id*="txtSNo"]').val();
                tblRowID = parseFloat(rowId);
                rowId = parseFloat(rowId) - parseFloat(1);
                rowId1 = parseFloat(tbllength) - parseFloat(1);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 39) {//right arrow
                    if (value == "RecQty") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="txtWG"]'));
                        return false;
                    }
                    else if (value == "WG") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="Raw"]'));
                        return false;
                    }
                    else if (value == "PC") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="Raw"]'));
                        return false;
                    }
                }
                else if (charCode == 37) {//left arrow
                    if (value == "RecQty") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="txtWG"]'));
                        return false;
                    }
                    else if (value == "WG") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="txtRecQty"]'));
                        return false;
                    }
                    else if (value == "PC") {
                        fncSetSelectAndFocusonTbl(rowobj.find('td input[id*="txtWG"]'));
                        return false;
                    }
                }
                if (charCode == 38) {//up arrow
                    prevrowobj = rowobj.prev();
                    if (value == "RecQty") {
                        fncSetSelectAndFocusonTbl(prevrowobj.find('td input[id*="txtRecQty"]'));
                        return false;
                    }
                    else if (value == "WG") {
                        fncSetSelectAndFocusonTbl(prevrowobj.find('td input[id*="txtWG"]'));
                        return false;
                    }
                    else if (value == "PC") {
                        fncSetSelectAndFocusonTbl(prevrowobj.find('td input[id*="Raw"]'));
                        return false;
                    }
                }
                else if (charCode == 40 || charCode == 13) {//down arrow,Enter
                    NextRowobj = rowobj.next();
                    if (value == "RecQty") {
                        if (rowId == rowId1) {
                            $('#<%=lnkOrderReceived.ClientID%>').focus();
                            btnId = 'add';
                            if (charCode == 39) {
                                $('#<%=lnkOrderReceived.ClientID%>').focus();
                            }
                            if (charCode == 37) {
                                $('#<%=lnkOrderCompleted.ClientID%>').focus();
                            }
                        }
                        fncSetSelectAndFocusonTbl(NextRowobj.find('td input[id*="txtRecQty"]'));
                        return false;
                    }
                    else if (value == "WG") {
                        fncSetSelectAndFocusonTbl(NextRowobj.find('td input[id*="txtWG"]'));
                        return false;
                    }
                    else if (value == "PC") {
                        fncSetSelectAndFocusonTbl(NextRowobj.find('td input[id*="Raw"]'));
                        return false;
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //Set Focus
        function fncSetSelectAndFocusonTbl(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncWastageChange(source) {
            try {
                var rowObj = $(source).parent().parent();
                var wastage = rowObj.find('td input[id*="txtWastage"]').val();
                var actualQty = rowObj.find('td input[id*="txtAcQty"]').val();
                var preTotal = rowObj.find('td input[id*="txtTotalQty"]').val();
                var totCost = rowObj.find('td input[id*="txtTotalCost"]').val();
                var cost = 0;
                cost = parseFloat(parseFloat(totCost) / parseFloat(preTotal)).toFixed(2);
                var total = parseFloat(wastage) + parseFloat(actualQty);
                if (parseFloat(wastage) <= parseFloat(actualQty)) {
                    rowObj.find('td input[id*="txtTotalQty"]').val('-' + parseFloat(total).toFixed(2));
                    rowObj.find('td input[id*="txtTotalCost"]').val(parseFloat(cost * (-total)).toFixed(2));
                }
                else {
                    rowObj.find('td input[id*="txtWastage"]').val(actualQty);
                    ShowPopupMessageBox("Wastage is Must be Less than or Equal to Acctual Quantity");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetFocusNextRowandCol(evt, source) {
            var rowobj, rowId = 0, NextRowobj, prevrowobj;
            try {

                rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtWastage"]'));
                    return false;
                }
                else if (charCode == 38 || charCode == 39) {
                    var NextRowobj = rowobj.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtWastage"]'));
                    return false;
                }
            }
            catch (e) {
                ShowPopupMessageBox(e.message);
            }
        }

        function fncSetSelectAndFocus(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncEdit(source) {
            try {
                var rowObj = $(source).parent().parent();
                var tblProcessBody = $("#tblProcessValue tbody");

                var tranno = rowObj.find('td input[id*="txtTranNo"]').val();
                var ItemCode = rowObj.find('td input[id*="txtItemCode"]').val();
                var Batch = rowObj.find('td input[id*="txtBatch"]').val();
                var wastage = rowObj.find('td input[id*="txtWastage"]').val();
                var actualQty = rowObj.find('td input[id*="txtAcQty"]').val();
                var preTotal = rowObj.find('td input[id*="txtTotalQty"]').val();
                var totCost = rowObj.find('td input[id*="txtTotalCost"]').val();

                tblProcessBody.children().remove();

                var row = "<tr ><td id='tdtranno' > " +
                    tranno + "</td><td id='tdItemCode' >" +
                    $.trim(ItemCode) + "</td><td id='tdBatch'>" +
                    $.trim(Batch) + "</td><td id='tdactualQty'>" +
                    $.trim(actualQty) + "</td>" +
                    "<td><input type='text' id='txtWastage' value='" + wastage + "'" + "onfocus ='this.select();' onchange='fncWastageChangeView(this);return false;' Class='grn_rptr_textbox_width form-control-res'" + "'/></td>" +
                    "<td id='tdpreTotal'>" +
                    $.trim(preTotal) + "</td><td id='tdtotCost'>" +
                    $.trim(totCost) + "</td><td id='tdWastage'>" +
                    $.trim(wastage) + "</td></tr>";
                tblProcessBody.append(row);

                $("#divWastageView").dialog({
                    resizable: false,
                    height: "auto",
                    width: 600,
                    modal: true,
                    title: "Wastage Edit",
                    appendTo: 'form:first',
                    buttons: {
                        "Save": function () {
                            var Was = tblProcessBody.find('td input[id*="txtWastage"]').val();
                            var TotalCost = tblProcessBody.find('td[id*="tdtotCost"]').text();
                            var WastageDiff = tblProcessBody.find('td[id*="tdWastage"]').text();

                            fncSaveWastage(Was, tranno, ItemCode, Batch, actualQty, TotalCost, WastageDiff);
                            $(this).dialog("close");
                        },
                        "Close": function () {
                            $(this).dialog("close");
                        }
                    }
                });
            }
            catch (e) {
                ShowPopupMessageBox(e.message);
            }
            return false;
        }

        function fncWastageChangeView(source) {
            try {
                var rowObj = $(source).parent().parent();
                var wastage = rowObj.find('td input[id*="txtWastage"]').val();
                var actualQty = rowObj.find('td[id*="tdactualQty"]').text();
                var preTotal = rowObj.find('td[id*="tdpreTotal"]').text();
                var totCost = rowObj.find('td[id*="tdtotCost"]').text();
                var cost = 0;
                cost = parseFloat(parseFloat(totCost) / parseFloat(preTotal)).toFixed(2);
                var total = parseFloat(wastage) + parseFloat(actualQty);
                if (parseFloat(wastage) <= parseFloat(actualQty)) {
                    rowObj.find('td[id*="tdpreTotal"]').text('-' + parseFloat(total).toFixed(2));
                    rowObj.find('td[id*="tdtotCost"]').text(parseFloat(cost * (-total)).toFixed(2));
                    rowObj.find('td[id*="tdWastage"]').text(parseFloat(parseFloat(wastage) - parseFloat(rowObj.find('td[id*="tdWastage"]').text())).toFixed(2));
                }
                else {
                    rowObj.find('td input[id*="txtWastage"]').val(actualQty);
                    ShowPopupMessageBox("Wastage is Must be Less than or Equal to Acctual Quantity");
                }

            }
            catch (e) {
                ShowPopupMessageBox(e.message);
            }
        }
        function fncSaveWastage(wastage, tranno, Itemcode, Batch, Actualqty, TotalCost, WastageDiff) {
            try {
                var obj = {};
                obj.sOrderId = $('#<%=txtOrderId.ClientID%>').val();
                obj.waste = wastage;
                obj.tran = tranno;
                obj.Itemcode = Itemcode;
                obj.Actualqty = Actualqty;
                obj.Batch = Batch;
                obj.sTotalCost = TotalCost;
                obj.sWastDiff = WastageDiff;
                $.ajax({
                    type: "POST",
                    url: "frmJobOrderMnt.aspx/fncSaveWastageQty",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "Success") {
                            $("#tblRawMaterial tbody").children().each(function () {
                                var rowobj = $(this);
                                if ($.trim(rowobj.find('td input[id*="txtTranNo"]').val()) == $.trim(tranno))
                                    rowobj.find('td input[id*="txtWastage"]').val(wastage);
                                rowobj.find('td input[id*="txtTotalQty"]').val(parseFloat(Actualqty) + parseFloat(wastage));
                                rowobj.find('td input[id*="txtTotalCost"]').val(TotalCost);

                                rowobj.find('td input[id*="txtWastage"]').attr("readonly", true);
                                rowobj.find('td input[id*="txtTotalQty"]').attr("readonly", true);
                                rowobj.find('td input[id*="txtTotalCost"]').attr("readonly", true);
                            });
                            ShowPopupMessageBox("Quatity Updated Successfully");
                            return false;
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                err.message();
            }
        }

        function fncRemoveAddOnsFinish(source) {
            try {
                var rowObj = $(source).parent().parent();
                $(source).closest("tr").remove();

                if ($("#tblFinishedGoods tbody").children().length == 1)
                    $('#divVendorLoadEntry').addClass("gvVendorLoadItemdummy");
                var row = 0;
                $("#tblFinishedGoods tbody").children().each(function () {
                    objtbl = $(this);
                    if ($(this).index() > 0) {
                        row = parseFloat(row) + 1;
                        objtbl.find('td input[id*=txtSNo]').val(row);
                    }
                }); 
                fncTotalAmountCalc();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //GST Calculation Vijay -20201018
        function fncGSTCalculation() {
            try {
                if ($('#<%=chkGst.ClientID%>').is(':checked')) {
                    var Qty = $('#<%=txtGivenQty.ClientID%>').val();
                    var MakingFee = $('#<%=txtMakingFee.ClientID%>').val();
                    var SGSTPer = $('#<%=txtSGSTPer.ClientID%>').val();
                    var TotalAmt = parseFloat(Qty) * parseFloat(MakingFee);
                    var Total = parseFloat((TotalAmt * (parseFloat(SGSTPer) + parseFloat(SGSTPer)) / (parseFloat(100) + parseFloat(SGSTPer) + parseFloat(SGSTPer)))).toFixed(2);
                    var SGSTAmt = (parseFloat(Total) / (parseFloat(SGSTPer) + parseFloat(SGSTPer)) * parseFloat(SGSTPer)).toFixed(2);
                    $('#<%=txtCGSTAmt.ClientID%>').val(SGSTAmt);
                    $('#<%=txtSGSTAmt.ClientID%>').val(SGSTAmt)
                    $('#<%=txtGstAmt.ClientID%>').val(parseFloat(parseFloat(SGSTAmt) * 2).toFixed(2));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Job Order Maintenance</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="bottom-purchase-container">
                    <div class="panel panel-default">
                        <div class="container-group-full">
                            <div class="purchase-order-header">
                                <div class="control-group-split" style="border: 1px solid; width: 75%; float: left;">
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label26" runat="server" Text="Order ID"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderId" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label32" runat="server" Text="Order From"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderFromCode" onkeydown="return fncShowSearchDialogCommon(event, 'Member',  'txtOrderFromCode', 'txtOrderToCode');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderFromName" runat="server" CssClass="form-control-res" Style="width: 165%;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label1" runat="server" Text="Order Date"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label2" runat="server" Text="Order To"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderToCode" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor',  'txtOrderToCode', 'txtRemarks');" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-right">
                                                <asp:TextBox ID="txtOrderToName" runat="server" CssClass="form-control-res" Style="width: 165%;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label3" runat="server" Text="Delivery Date"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-middle" style="width: 31%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label4" runat="server" Text="Remarks"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res" onkeypress="return fncSetEnterFocus(event,'txtTotalValue')" Style="width: 276%;"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                          <div class="col-md-4 form_bootstyle">
                                        <asp:CheckBox ID="chkGst" runat="server" Text="With GST" Class="radioboxlist" Checked="true" Font-Bold="True" style ="float:right;"  />
                                    </div>
                                    </div>
                                </div>
                                <div class="control-group-split" style="border: 1px solid; width: 25%; float: right">
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 75%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label5" runat="server" Text="Total Value"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtTotalValue" Text="0.00" MaxLength="9" Style="text-align: right;" onFocus="this.select()" onchange="fncCalcTotalAmount();" runat="server" CssClass="form-control-res" ReadOnly="true" onkeypress="return fncSetEnterFocus(event,'txtAdvancePaid')" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 75%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label22" runat="server" Text="Total GSTAmt"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtTotalGST" Text="0.00" MaxLength="9" Style="text-align: right;" onFocus="this.select()" onchange="fncCalcTotalAmount();" runat="server" CssClass="form-control-res" ReadOnly="true" onkeypress="return fncSetEnterFocus(event,'txtAdvancePaid')" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 75%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label6" runat="server" Text="Advance Paid"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtAdvancePaid" Text="0.00" MaxLength="9" Style="text-align: right;" onFocus="this.select()" onchange="fncCalcTotalAmount();" runat="server" CssClass="form-control-res" onkeypress="return fncSetEnterFocus(event,'txtItemCodeAdd')" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-middle" style="width: 75%;">
                                            <div class="label-left">
                                                <asp:Label ID="Label7" runat="server" Text="Balance Amount"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtBalanceAmount" Text="0.00" Style="text-align: right;" runat="server" onkeypress="return fncSetEnterFocus(event,'txtItemCodeAdd')" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divJOBtns" runat="server" style="float: right;">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkJobOrderReport_Click" Text="Print"></asp:LinkButton>
                                        <%--OnClick="lnkJobOrderReport_Click"--%>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkOrderCompleted" runat="server" class="button-blue" OnClientClick="fncInitializeOrderCompletedDialog();return false" Text="Order Completed"></asp:LinkButton>
                                        <%--OnClick="lnkLoad_Click"--%>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkOrderReceived" runat="server" class="button-blue" OnClick="lnkOrderReceived_Click" OnClientClick="return fncOrderReceivedValidation();" Text="Order Received"></asp:LinkButton>
                                        <%--OnClick="lnkLoad_Click"--%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottom-purchase-container">
            <div class="panel panel-default">
                <div>
                    <div class="label-left">
                        <asp:Label ID="Label8" runat="server" Text="Finished Goods" Style="color: royalblue; font-weight: bolder"></asp:Label>
                    </div>
                    <%--<asp:UpdatePanel ID="updtPnlGrid1" UpdateMode="Conditional" runat="Server">
                                    <ContentTemplate>--%>
                    <div id="divFinishedGoodsTable" style="overflow-x: scroll; overflow-y: hidden; height: 165px !important; background-color: aliceblue;">
                        <div id="divVendorLoadEntry" class="gvVendorLoadItemdummy">
                            <table rules="all" border="1" id="tblFinishedGoods" class="Payment_fixed_headers">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">S.No</th>
                                        <th style="text-align: center">Delete</th>
                                        <th style="text-align: center">Tran No</th>
                                        <th style="text-align: center">Tran Date</th>
                                        <th style="text-align: center">Item Code</th>
                                        <th style="text-align: center">Item Name</th>
                                        <th style="text-align: center">Batch No</th>
                                        <th style="text-align: center">Ac.Qty</th>
                                        <th style="text-align: center">Al.Rec</th>
                                        <th style="text-align: center">Rec.Qty</th>
                                        <th style="text-align: center">WG(pc)</th>
                                        <th style="text-align: center">Bal.Qty</th>
                                        <th style="text-align: center">Tot.WG</th>
                                        <th style="text-align: center">Total Cost</th>
                                        <th style="text-align: center">Making Fee</th>
                                        <th style="text-align: center">SGST Per</th>
                                        <th style="text-align: center">CGST Per</th>
                                        <th style="text-align: center">SGST Amt</th>
                                        <th style="text-align: center">CGST Amt</th>
                                        <th style="text-align: center">Total GST</th>
                                        <th style="text-align: center">Total Fee</th>
                                        <th style="text-align: center">Flag</th>
                                    </tr>
                                </thead>
                                <tbody id="divtblFinishedGoods" style="height: 145px !important;">
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                </div>
                <div>
                    <div class="label-left">
                        <asp:Label ID="Label9" runat="server" Text="Raw Materials" Style="color: royalblue; font-weight: bolder"></asp:Label>
                    </div>
                    <%--<asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="Server">
                                    <ContentTemplate>--%>
                    <div id="divRawMaterialTable" class="poSummary Payment_fixed_headers">
                        <table rules="all" border="1" id="tblRawMaterial">
                            <thead>
                                <tr>
                                    <th style="text-align: center">S.No</th>
                                    <th style="text-align: center;" id="thEdit">Edit</th>
                                    <th style="text-align: center">Delete</th>
                                    <th style="text-align: center">Tran No</th>
                                    <th style="text-align: center">Tran Date</th>
                                    <th style="text-align: center">Item Code</th>
                                    <th style="text-align: center">Item Name</th>
                                    <th style="text-align: center">Batch No</th>
                                    <th style="text-align: center">Ac.Qty</th>
                                    <th style="text-align: center">Wastage</th>
                                    <th style="text-align: center">Total Qty</th>
                                    <th style="text-align: center">Total Cost</th>
                                    <th style="text-align: center">Tran Type</th>
                                    <th style="text-align: center">Flag</th>
                                </tr>
                            </thead>
                            <tbody id="divtblRawMaterial">
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
        <div id="divItemadd">
            <asp:Panel runat="server" ID="pnlItemadd">
                <asp:UpdatePanel ID="updtPnlButton1" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="bottom-purchase-container-add gid_top_border" id="addInv">
                            <div class="bottom-purchase-container-add-Text">
                                <div class="control-group-split">
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Item Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemCodeAdd" onFocus="this.select()" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCodeAdd', 'txtMaterialType');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            <asp:HiddenField ID="hfInvCode" runat="server" />
                                        </div>
                                    </div>
                                    <div class="PO_container3">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="ItemDesc"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label14" runat="server" Text="Batch"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBatch" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label16" runat="server" Text="Cost"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCost" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label18" runat="server" Text="Material Type"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMaterialType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'MaterialType',  'txtMaterialType', 'txtGivenQty');"></asp:TextBox>
                                        </div>
                                        <%--<div class="label-right">
                                            <asp:DropDownList ID="ddlMaterialType" runat="server" onkeypress="return fncSetEnterFocus(event,'ddlTranType_chzn')" CssClass="form-control-res">
                                                <asp:ListItem Value="1">RAW</asp:ListItem>
                                                <asp:ListItem Value="2">FIN</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                    </div>
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label12" runat="server" Text="Given Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtGivenQty" runat="server" MaxLength="9" CssClass="form-control-res-right" Text="0" onFocus="this.select()" onchange="return fncCalcTotalQty();" onkeypress="return fncSetEnterFocus(event,'txtWastage')" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div id="divWastage" runat="server" class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label27" runat="server" Text="Wastage"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWastage" runat="server" MaxLength="9" CssClass="form-control-res-right" Text="0" onFocus="this.select()" onchange="return fncCalcTotalQty();" onkeypress="return fncSetEnterFocus(event,'txtTranType')" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div id="divMakingFee" runat="server" class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text="Making Fee"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMakingFee" runat="server" Text="0.00" MaxLength="9" CssClass="form-control-res-right" onFocus="this.select()" onkeypress="return fncSetEnterFocus(event,'txtTranType')" onchange="fncGSTCalculation();return false;"
                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;" id="divGstPer">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text="GSTPer"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtGstPer" ReadOnly="true" onFocus="this.select()" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;" id="divGstAmt">
                                        <div class="label-left">
                                            <asp:Label ID="Label20" runat="server" Text="GSTAmt"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtGstAmt" runat="server" ReadOnly="true" onFocus="this.select()" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label15" runat="server" Text="Tot.Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTotQty" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7" style="width: 7%;">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text="Tran Type"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:DropDownList ID="ddlTranType" runat="server" CssClass="form-control-res">
                                                <asp:ListItem Value="1">Addition</asp:ListItem>
                                                <asp:ListItem Value="2">Return</asp:ListItem>
                                            </asp:DropDownList>--%>
                                            <asp:TextBox ID="txtTranType" runat="server" onkeydown="return fncSetEnterFocus(event,'lnkAddInv');" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkAddInv" runat="server" class="button-blue" OnClientClick="return fncAddValidation()"
                                                Text="Add"></asp:LinkButton><%--OnClick="lnkAddInv_Click"--%>
                                            <%--<asp:Button ID="btnDelete" runat="server" class="button-blue" OnClientClick="fncAddValidation()" />--%>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClearAdd" runat="server" Text="Clear" class="button-blue" OnClientClick="return fncSubCrear()"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split" id="divGSTDetails" style="margin-top: -30px">
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label21" runat="server" Text="SGST Per"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSGSTPer" onFocus="this.select()" ReadOnly="true" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label23" runat="server" Text="CGST Per"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCGSTPer" onFocus="this.select()" ReadOnly="true" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label24" runat="server" Text="SGST Amt"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSGSTAmt" onFocus="this.select()" Text="0" ReadOnly="true" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label25" runat="server" Text="CGST Amt"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCGSTAmt" Text="0" onFocus="this.select()" ReadOnly="true" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-container">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return fncSaveValidation()" OnClick="lnkSave_Click"
                                            Text="Save(F4)"></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="return clearForm()"
                                            Text="ClearAll(F6)"></asp:LinkButton><%--OnClick="lnkClear_Click"--%>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkBack" runat="server" class="button-blue" OnClientClick="return fncBacktoPreviousform()"
                                            Text="Back(F8)"></asp:LinkButton><%--OnClick="lnkClear_Click"--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
        <div class="hiddencol">
            <asp:HiddenField ID="hdOrderId" runat="server" Value="" />
            <asp:HiddenField ID="hdUOM" runat="server" Value="" />
            <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="" />
            <div id="JobOrderSave">
                <div>
                    <asp:Label ID="lblgaReceiptSave" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:UpdatePanel ID="upsave" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                OnClientClick="fncSaveDialogClose()" OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="OrderComplete">
                <div>
                    <asp:Label ID="lblOrderComplete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:UpdatePanel ID="upOrderComplete" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkOrderCompleteOk" runat="server" class="button-blue" Text="Yes"
                                    OnClientClick="fncOrderCompleteDialogClose()" OnClick="lnkOrderCompleteOk_Click"> </asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkOrderCompleteNo" runat="server" class="button-blue" Text='No'
                            OnClientClick="fncOrderCompleteDialogClose();return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="display_none">
            <div id="divWastageView" class="col-md-12 poSummaryView">
                <table id="tblProcessValue" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">Tran No
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Batch No
                            </th>
                            <th scope="col">Act Qty
                            </th>
                            <th scope="col">Wastage
                            </th>
                            <th scope="col">Total Qty
                            </th>
                            <th scope="col">Total Cost
                            </th>
                            <th scope="col">WastageConstant
                            </th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <asp:HiddenField ID="hidTotalValue" runat="server" Value ="0" />
        <asp:HiddenField ID="hidTotalGST" runat="server" Value ="0" />
    </div>
</asp:Content>
