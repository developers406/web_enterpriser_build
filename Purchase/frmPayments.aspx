﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPayments.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .Payment_fixed_headers td:nth-child(1), .Payment_fixed_headers th:nth-child(1) {
            min-width: 42px;
            max-width: 42px;
        }

        .Payment_fixed_headers td:nth-child(2), .Payment_fixed_headers th:nth-child(2) {
            min-width: 130px;
            max-width: 130px;
        }

        .Payment_fixed_headers td:nth-child(3), .Payment_fixed_headers th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
        }

        .Payment_fixed_headers td:nth-child(4), .Payment_fixed_headers th:nth-child(4) {
            min-width: 75px;
            max-width: 75px;
        }

        .Payment_fixed_headers td:nth-child(5), .Payment_fixed_headers th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
        }

        .Payment_fixed_headers td:nth-child(6), .Payment_fixed_headers th:nth-child(6) {
            min-width: 140px;
            max-width: 140px;
        }

        .Payment_fixed_headers td:nth-child(12), .Payment_fixed_headers th:nth-child(12) {
            min-width: 110px;
            max-width: 110px;
        }

        .Payment_fixed_headers td:nth-child(11), .Payment_fixed_headers th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .Payment_fixed_headers td:nth-child(13), .Payment_fixed_headers th:nth-child(13) {
            min-width: 99px;
            max-width: 99px;
        }

        .Payment_fixed_headers td:nth-child(14), .Payment_fixed_headers th:nth-child(14) {
            display: none;
        }

        .Payment_fixed_headers td:nth-child(15), .Payment_fixed_headers th:nth-child(15) {
            display: none;
            min-width: 99px;
            max-width: 99px;
            text-align: center !important;
        }

        .Payment_fixed_headers td:nth-child(6) {
            text-align: right !important;
        }

        .Payment_fixed_headers td:nth-child(11) {
            text-align: right !important;
        }

        .Payment_fixed_headers td:nth-child(12) {
            text-align: right !important;
        }

        .Payment_fixed_headers td:nth-child(7), .Payment_fixed_headers th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .Payment_fixed_headers td:nth-child(7) {
            text-align: right !important;
        }

        .Payment_fixed_headers td:nth-child(8), .Payment_fixed_headers th:nth-child(8) {
            min-width: 50px;
            max-width: 50px;
            text-align: left !important;
        }

        .Payment_fixed_headers td:nth-child(9), .Payment_fixed_headers th:nth-child(9) {
            min-width: 50px;
            max-width: 50px;
        }

        .Payment_fixed_headers td:nth-child(10), .Payment_fixed_headers th:nth-child(10) {
            min-width: 150px;
            max-width: 150px;
        }

        .Payment_fixed_headers td:nth-child(10) {
            text-align: right !important;
        }


        .payment_display_body td:nth-child(1), .payment_display_body th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .payment_display_body td:nth-child(2), .payment_display_body th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
        }

        .payment_display_body td:nth-child(3), .payment_display_body th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .payment_display_body td:nth-child(4), .payment_display_body th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_display_body td:nth-child(5), .payment_display_body th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_display_body td:nth-child(6), .payment_display_body th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(1), .payment_PRReq th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .payment_PRReq td:nth-child(2), .payment_PRReq th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(3), .payment_PRReq th:nth-child(3) {
            min-width: 250px;
            max-width: 250px;
        }

        .payment_PRReq td:nth-child(4), .payment_PRReq th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(4) {
            text-align: right !important;
        }

        .payment_PRReq td:nth-child(5), .payment_PRReq th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(5) {
            text-align: right !important;
        }

        .payment_PRReq td:nth-child(6), .payment_PRReq th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(6) {
            text-align: right !important;
        }

        .payment_PRReq td:nth-child(7), .payment_PRReq th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .payment_PRReq td:nth-child(7) {
            text-align: left !important;
        }

        .payment_PRReq td:nth-child(8), .payment_PRReq th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .table th {
            border-color: #ccc;
            padding: 5px 5px;
            color: white;
            background-color: #004bc6;
        }

        .table td {
            padding: 5px 5px;
        }

        .table > tbody > tr:last-child {
            background: yellow;
            font-weight: 900
        }

        .saleslabel {
            float: right;
            font-size: 15px;
            font-weight: 900;
            color: blue;
        }

        .salesData {
            font-size: 15px;
            font-weight: 900;
            color: green;
        }
    </style>
    <link href="../css/breakeven.css" rel="stylesheet" />
    <link href="../css/fonts/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="../css/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'Payments');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "Payments";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }
                    }
                }
            });
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#<%= hidView.ClientID %>").val() == "View") {
                $("#<%= txtPaymentDate.ClientID %>").val();
                $("#<%= txtRefDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });
                $("#<%= txtChequeDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            }
            else {
                $("#<%= txtPaymentDate.ClientID %>").val(currentDateformat_Master);
                $("#<%= txtRefDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $("#<%= txtChequeDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            }

        });
    </script>
    <script type="text/javascript">
        var CurRowObj, popupStatus = "";
        var load = "";
        var len = 0;
        var TranAmtFocusOut = 0;
        var AmttopayFocus = 0;

        function numericOnly(elementRef) {
            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;
            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57) || (keyCodeEntered == 189) || (keyCodeEntered == 109)) {
                return true;
            }
            else if (keyCodeEntered == 46) {
                if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))
                    return false;
                else
                    return true;
            }
            return false;
        }
        function fncLoadReverseGrid() {
            TranAmtFocusOut = $("#<%=txtAmount.ClientID %>").val();
            var fullAmt = $.trim($("#tblPaymnet [id*=lblTotalsum]").html().replace(/(,)/g, ''));
            if (parseFloat(fullAmt) < parseFloat(TranAmtFocusOut).toFixed(2)) {
                TranAmtFocusOut = 0;
                ShowPopupMessageBox("Enter Amount is Greater than OutStanding - " + fullAmt);
                return false;
            }
            var minus = 0;
            len = $("#tblPaymnet [id*=paymentBodyrow]").length;
            if (parseFloat(len) > 0) {
                $("#tblPaymnet [id*=paymentBodyrow]").each(function (i) {
                    if (parseFloat($(this).find('td span[id*="lblAmount"]').html()) < 0) {
                        minus = parseFloat(minus) + parseFloat($(this).find('td span[id*="lblAmount"]').html());
                        $(this).find('td input[id*="cbrowFull"]').attr("checked", "checked");
                        $(this).find('td input[id*="txtAmtToPay"]').val($(this).find('td span[id*="lblAmount"]').html());
                        $(this).find('td span[id*="lblBalance"]').html('0');
                    }
                    else {
                        $(this).find('td input[id*="cbrowFull"]').removeAttr("checked");
                        $(this).find('td input[id*="txtAmtToPay"]').val('0');
                        $(this).find('td span[id*="lblBalance"]').html('0');
                    }
                    $(this).find('td input[id*="cbrowPartial"]').removeAttr("checked");
                });
                var status = true;
                TranAmtFocusOut = parseFloat(TranAmtFocusOut) - parseFloat(minus);
                if (TranAmtFocusOut > 0) {
                    for (len; len > 0; len--) {
                        var Amount = $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(5) span").html();
                        if (parseFloat(Amount) > 0) {
                            if (parseFloat(TranAmtFocusOut) >= parseFloat(Amount)) {
                                TranAmtFocusOut = parseFloat(TranAmtFocusOut) - parseFloat(Amount);
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(6) input").val(parseFloat(Amount).toFixed(2));
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(8) input").removeAttr("checked");
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(7) input").attr("checked", "checked");
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(9) span").html('0');
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(9) input").val('0');
                                AmttopayFocus = parseFloat(AmttopayFocus) + parseFloat($("#tblPaymnet").find("tr:eq('" + len + "') td:eq(6) input").val());
                                $("#tblPaymnet [id*=lbltopaysum]").html(parseFloat(AmttopayFocus).toFixed(2));
                            }

                            else {
                                Amount = parseFloat(Amount) - parseFloat(TranAmtFocusOut);
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(6) input").val(parseFloat(TranAmtFocusOut).toFixed(2));
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(7) input").removeAttr("checked");
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(8) input").attr("checked", "checked");
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(9) span").html(parseFloat(Amount).toFixed(2));
                                $("#tblPaymnet").find("tr:eq('" + len + "') td:eq(9) input").val(parseFloat(Amount).toFixed(2));
                                TranAmtFocusOut = 0;
                                AmttopayFocus = parseFloat(AmttopayFocus) + parseFloat($("#tblPaymnet").find("tr:eq('" + len + "') td:eq(6) input").val());
                                $("#tblPaymnet [id*=lbltopaysum]").html(parseFloat(AmttopayFocus).toFixed(2));
                            }
                        }
                    }
                }
                fncTotalforView();
            }

        }
        function pageLoad() {
            //Vijay Hide or Show Margin Button - 20210512
            if ($("#<%=hidTextileLocation.ClientID %>").val() == "Y") {
                $('.Payment_fixed_headers td:nth-child(15)').css("display", "revert");
                $('.Payment_fixed_headers th:nth-child(15)').css("display", "revert");
                $('.Payment_fixed_headers td:nth-child(15)').css("text-align", "center !important");
            }
            $('#ContentPlaceHolder1_ddlbankCodeVendor_chzn').css("width", "116%");
            load = "true";

            if ($("#<%=hidBillType.ClientID %>").val() == "A") {
                $('#breadcrumbs_text_Payment').removeClass('lowercase');
                $('#breadcrumbs_text_Payment').addClass('uppercase');
            }
            else if ($("#<%=hidBillType.ClientID %>").val() == "B") {
                $('#breadcrumbs_text_Payment').removeClass('uppercase');
                $('#breadcrumbs_text_Payment').addClass('lowercase');
            }
            else if ($("#<%=hidBillType.ClientID%>").val() == "C") {
                $('#breadcrumbs_text_Payment').removeClass('uppercase');
                $('#breadcrumbs_text_Payment').removeClass('lowercase');
                $('#breadcrumbs_text_Payment').text('View Payments');
            }

            //            fncInitializeChequeprint();
            $("#<%= txtPaymentDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtRefDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });

            $(document).ready(function () {
                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            });

            fncTotalforView();
        }

        //Total sum Calculation for TotalToPayamt,Balance
        function fncTotalforView(rowobj) {
            try {

                var Amount = 0, Amttopay = 0, balance = 0;
                var tranNo, tranType, objGST;

                $("#tblPaymnet [id*=paymentBodyrow]").each(function () {
                    /// Work for GSTR Status .To Show GST Paid or Not
                   <%-- if (load == "true")
                    {
                        tranNo = $(this).find('td span[id*="lblTranNo"]').text().trim();
                        tranType = $(this).find('td span[id*="lblTranType"]').text().trim();
                        
                        if (tranType == "PURINV") {

                            $(this).css("background-color", "#ff4d4d");

                            objGST = jQuery.parseJSON($('#<%=hidGSTRPayment.ClientID %>').val());
                            for (var i = 0; i < objGST.length; i++) {
                                if (objGST[i]["gidno"].trim() == tranNo) {
                                    if (parseFloat(objGST[i]["RemGSTR"]) == 0) {
                                        $(this).css("background-color", "#99ff99");
                                    }
                                    else {
                                        $(this).css("background-color", "#ff4d4d");
                                    }
                                }
                                
                            }
                        }
                    }--%>

                    /// End 



                    Amount = parseFloat(Amount) + parseFloat($(this).find('td span[id*="lblAmount"]').html());
                    Amttopay = parseFloat(Amttopay) + parseFloat($(this).find('td input[id*="txtAmtToPay"]').val());
                    balance = parseFloat(balance) + parseFloat($(this).find('td span[id*="lblBalance"]').html());

                    if (parseFloat($(this).find('td input[id*="txtAmtToPay"]').val()) == 0) {
                        $(this).find('td input[id*="cbrowPartial"]').removeAttr("checked");
                        $(this).find('td input[id*="cbrowFull"]').removeAttr("checked");

                    }
                    else if (parseFloat($(this).find('td span[id*="lblBalance"]').html()) == 0) {
                        $(this).find('td input[id*="cbrowPartial"]').removeAttr("checked");
                        $(this).find('td input[id*="cbrowFull"]').attr("checked", "checked");
                    }
                    else if (parseFloat($(this).find('td span[id*="lblBalance"]').html()) > 0) {
                        $(this).find('td input[id*="cbrowPartial"]').attr("checked", "checked");
                        $(this).find('td input[id*="cbrowFull"]').removeAttr("checked");
                    }

                });



                $('#<%=hidtotalAmt.ClientID %>').val(Amount.toFixed(2));
                $('#<%=hidToPayAmt.ClientID %>').val(Amttopay.toFixed(2));
                $('#<%=hidBalance.ClientID %>').val(balance.toFixed(2));

                $('#<%=txtAmount.ClientID %>').val(Amttopay.toFixed(2));

                Amount = Amount.toFixed(2);
                Amttopay = Amttopay.toFixed(2);
                balance = balance.toFixed(2);

                Amount = Amount.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                Amttopay = Amttopay.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                balance = balance.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                $("#tblPaymnet [id*=lblTotalsum]").html(Amount);
                $("#tblPaymnet [id*=lbltopaysum]").html(Amttopay);
                $("#tblPaymnet [id*=lblbalancesum]").html(balance);

                if ($('#<%=hidCLExitStatus.ClientID %>').val() == "No") {

                    if (parseFloat(balance) < parseFloat($('#<%=txtCreditLimit.ClientID %>').val())) {
                        CurRowObj = rowobj;
                        InitializePaymentCreditLimitExitDialog();
                        //$("#PaymentCreditLimitExist").dialog('open');
                    }
                }

                load = "false";
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get Bank From ddlBank
        function fncGetBankName() {
            try {
                var bankname = $('#<%= ddlBankCode.ClientID %>').val();
                var code = $("#ContentPlaceHolder1_ddlBankCode option:selected").text();
                if (bankname != '<%=Resources.LabelCaption.ddl_Empty%>') {
                    $('#<%=txtBankName.ClientID %>').val(bankname);
                    if ($('#<%=ddlPaymode.ClientID %>').val() == "CHEQUE") {
                        var obj = {};
                        obj.BankCode = code;
                        $.ajax({
                            type: "POST",
                            url: "frmPayments.aspx/fncCheck",
                            data: JSON.stringify(obj),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                $('#<%=txtChequeNo.ClientID %>').val(msg.d);

                            },
                            error: function (data) {
                                fncToastError(data.message);
                                return false;
                            }
                        });

                        
                    }
                    else if ($('#<%=ddlPaymode.ClientID %>').val() == "RTGS") {
                        setTimeout(function () {
                            $('#<%=txtChequeDate.ClientID %>').focus();
                        }, 10);
                    }
                    else {
                        setTimeout(function () {
                            $('#<%=txtChequeNo.ClientID %>').focus();
                        }, 10);
                    }
                   
                }
                
                else
                    $('#<%=txtBankName.ClientID %>').val('');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignPaymodeDetail() {
            try {
                var paymodetype = $('#<%= ddlPaymode.ClientID %>').val();
                if (paymodetype == "CASH" || paymodetype == "CD/DN.Adj") {
                    fncShowHideBankcode();

                    if (paymodetype == "CASH") {
                        $('#<%=txtBankcode.ClientID %>').val('CASH');
                        $('#<%=txtBankName.ClientID %>').val('CASH ACCOUNT');
                        $('#<%=txtChequeNo.ClientID %>').val('CASH');
                        $('#<%=txtChequeNo.ClientID %>').on('mousedown', function () {
                            return false;
                        });
                    }
                    else {
                        $('#<%=txtBankcode.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtBankName.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtChequeNo.ClientID %>').val('CD/DN.Adj');
                        $('#<%=txtChequeNo.ClientID %>').off('mousedown');
                        $('#<%=txtAmount.ClientID %>').on('mousedown', function () {
                            return false;
                        });
                        $('#<%=cbhdrFull.ClientID %>').attr('disabled', 'disabled');

                        $("#tblPaymnet tbody").children().each(function () {
                            var rowobj = $(this);
                            rowobj.find('td input[id*="cbrowPartial"]').on('mousedown', function () {
                                return false;
                            });
                            rowobj.find('td input[id*="cbrowFull"]').attr('disabled', 'disabled');
                        });
                    }
                }
                else if (paymodetype == "RTGS") {
                    $('#<%=txtChequeNo.ClientID %>').val('RTGS');
                    $('#<%=txtChequeNo.ClientID %>').on('mousedown', function () {
                        return false;
                    });
                }
                else if (paymodetype == "CHEQUE")
                {
                    var code = $("#ContentPlaceHolder1_ddlBankCode option:selected").text();
                    if (code != "-- Select --") {
                        var obj = {};
                        obj.BankCode = code;
                        $.ajax({
                            type: "POST",
                            url: "frmPayments.aspx/fncCheck",
                            data: JSON.stringify(obj),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                $('#<%=txtChequeNo.ClientID %>').val(msg.d);

                            },
                            error: function (data) {
                                fncToastError(data.message);
                                return false;
                            }
                        });
                    }
                    $('#<%=txtChequeNo.ClientID %>').val('');
                    $('#<%=txtBankName.ClientID %>').val('');
                  
                    $('#divtxtBankcode').hide();
                    $('#divddlbankcode').show();
               
                    
                }
                else {
                    $('#divtxtBankcode').hide();
                    $('#divddlbankcode').show();
                    $('#<%=txtChequeNo.ClientID %>').off('mousedown');
                    $('#<%=txtChequeNo.ClientID %>').val('');
                    $('#<%=txtBankName.ClientID %>').val('');
                }
               
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowHideBankcode() {
            try {
                $('#divtxtBankcode').show();
                $('#divddlbankcode').hide();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Save Validation
        function fncSaveValidation() {
            try {
                //                var d = new Date(),
                //                date = ((d.getDate()) + '/' + (d.getMonth() + 1) + '/' + d.getFullYear());
                $('#<%= lnkSave.ClientID %>').css("display", "none");
                if ($('#<%= txtRefVoucherNo.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%= txtRefVoucherNo.ClientID %>')
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_RefNo%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if ($('#<%= ddlPaymode.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>') {
                    popUpObjectForSetFocusandOpen = $('#<%= ddlPaymode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_PayType%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if ($('#<%= ddlBankCode.ClientID %>').val() == '<%=Resources.LabelCaption.ddl_Empty%>' && $('#<%= ddlPaymode.ClientID %>').val() != "CASH"
                    && $('#<%= ddlPaymode.ClientID %>').val() != "CD/DN.Adj") {
                    popUpObjectForSetFocusandOpen = $('#<%= ddlBankCode.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.Alert_BankCode%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if ($('#<%= ddlPaymode.ClientID %>').val() == "CD/DN.Adj") {
                    if (parseFloat($('#<%= txtAmount.ClientID %>').val()) != 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_ZeroPayment%>');
                        $('#<%= lnkSave.ClientID %>').css("display", "block");
                        return false;
                    }
                }
                else if ($('#<%= txtChequeNo.ClientID %>').val() == "" && $("#<%=hidBillType.ClientID %>").val() != "B") {
                    popUpObjectForSetFocusandOpen = $('#<%= txtChequeNo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterCheque%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                else if (parseInt($('#<%= txtAmount.ClientID %>').val()) <= 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_InvalidAmt%>');
                    $('#<%= lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Balabce Amount Calculation
        function fncBalanceAmtCalc(source) {
            try {
                var rowobj = $(source).parent().parent();
                var Amttopay = 0, Amount = 0, balance = 0, initToPayAmt = 0;

                if ($(rowobj).find('td input[id*="txtAmtToPay"]').val() == "")
                    $(rowobj).find('td input[id*="txtAmtToPay"]').val('0.00');


                Amount = parseFloat($(rowobj).find('td span[id*="lblAmount"]').html());
                Amttopay = parseFloat($(rowobj).find('td input[id*="txtAmtToPay"]').val());

                balance = Amount - Amttopay;


                if (balance < 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Paymentlessthenzero%>');
                    $(rowobj).find('td input[id*="txtAmtToPay"]').val($(rowobj).find('td input[id*="hidAmtToPay"]').val());
                }
                else {
                    $(rowobj).find('td span[id*="lblBalance"]').html(balance.toFixed(2));
                    $(rowobj).find('td input[id*="hidBalance"]').val(balance.toFixed(2));

                }


                fncTotalforView(rowobj);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Assign full Amount
        function fncAssignFullAmt(source) {
            try {
                var rowobj = $(source).parent().parent();
                fncAssignFullAmtCom(rowobj);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAssignFullAmtCom(rowobj) {
            try {
                //if (rowobj.find('td span[id*="lblTranType"]').html() != "PURINV") {
                //    //$(source).attr("checked", "checked");
                //    rowobj.find('td input[id*="cbrowFull"]').attr("checked", "checked");
                //    return;
                //}
                //else

                if (rowobj.find('td input[id*="cbrowFull"]').is(":checked")) {
                    $(rowobj).find('td input[id*="txtAmtToPay"]').val($(rowobj).find('td span[id*="lblAmount"]').text());
                    $(rowobj).find('td span[id*="lblBalance"]').html('0.00');
                    $(rowobj).find('td input[id*="hidBalance"]').val('0.00');
                }
                else {
                    $(rowobj).find('td input[id*="txtAmtToPay"]').val('0.00');
                    $(rowobj).find('td span[id*="lblBalance"]').html($(rowobj).find('td span[id*="lblAmount"]').text());
                    $(rowobj).find('td input[id*="hidBalance"]').val($(rowobj).find('td span[id*="lblAmount"]').text());
                }
                fncTotalforView(rowobj);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source) {
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtAmtToPay"]').focus();
                        NextRowobj.find('td input[id*="txtAmtToPay"]').select();
                    }
                    else {
                        rowobj.siblings().first().find('td input[id*="txtAmtToPay"]').focus();
                        rowobj.siblings().first().find('td input[id*="txtAmtToPay"]').select();
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtAmtToPay"]').focus();
                        prevrowobj.find('td input[id*="txtAmtToPay"]').select();
                    }
                    else {
                        rowobj.siblings().last().find('td input[id*="txtAmtToPay"]').focus();
                        rowobj.siblings().last().find('td input[id*="txtAmtToPay"]').select();
                    }
                }
                else {
                    if (rowobj.find('td span[id*="lblTranType"]').html() != "PURINV") {
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Credit Limit Dialog Initialation
        function InitializePaymentCreditLimitExitDialog() {
            try {
                $("#PaymentCreditLimitExist").html('<%=Resources.LabelCaption.Alert_CreditLimitExit%>');
                $("#PaymentCreditLimitExist").dialog({
                    //autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true,
                    title: "Enterpriser Web",
                    buttons: {
                        Yes: function () {
                            $(this).dialog("destroy");
                            $('#<%=hidCLExitStatus.ClientID %>').val('OK');
                        },
                        No: function () {
                            $(this).dialog("destroy");
                            fncCLlimitNo();
                        }
                    },
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Credit Limit Exit Status Not Accept
        function fncCLlimitNo() {
            try {

                var ToPayAmt, totalAmt;

                $('#<%=hidCLExitStatus.ClientID %>').val('No');
                //totalAmt = CurRowObj.find('td span[id*="lblAmount"]').html();
                //ToPayAmt = CurRowObj.find('td input[id*="hidAmtToPay"]').val();


                //CurRowObj.find('td input[id*="txtAmtToPay"]').val(ToPayAmt);
                //CurRowObj.find('td span[id*="lblBalance"]').html((parseFloat(totalAmt) - parseFloat(ToPayAmt)).toFixed(2));
                //CurRowObj.find('td input[id*="hidBalance"]').val((parseFloat(totalAmt) - parseFloat(ToPayAmt)).toFixed(2));

                //fncTotalforView();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }




        //Initialize Save Dialog
        function fncInitializeSaveDialog() {
            try {
                $("#PaymentSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 130,
                    width: 380,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Initialize Cheque print Dialog
        function fncInitializeChequeprint() {
            try {
                if ($('#<%=ddlPaymode.ClientID %>').val() == "CHEQUE")
                    $('#<%=lblprint.ClientID %>').text('<%=Resources.LabelCaption.msgChequeprint%>');
                else if ($('#<%=ddlPaymode.ClientID %>').val() == "CASH")
                    $('#<%=lblprint.ClientID %>').text('<%=Resources.LabelCaption.msgCashprint%>');
                else if ($('#<%=ddlPaymode.ClientID %>').val().indexOf("NEFT") != -1)
                    $('#<%=lblprint.ClientID %>').text('<%=Resources.LabelCaption.msgNeftprint%>');
                else {
                    window.location.href = 'frmPaymentsView.aspx';
                    return;
                }


                $("#ChequePrint").dialog({
                    autoclose: false,
                    resizable: false,
                    height: 130,
                    width: 240,
                    modal: true,
                    title: "Print"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Open Save Dialog
        function fncOpenSaveDialog(SaveMsg) {
            try {
                fncInitializeSaveDialog();
                $('#<%=lblSave.ClientID %>').text(SaveMsg);
                $("#PaymentSave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Show Pop Message
        function ShowPopupMessageBoxPayment(message) {
            $(function () {
                $("#dialog-MessageBoxPayment").html(message);
                $("#dialog-MessageBoxPayment").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Ok: fncPopupconfirmation
                    },
                    modal: false
                });
            });
        };

        //Popup Confirmation
        function fncPopupconfirmation() {
            try {

                $("#dialog-MessageBoxPayment").dialog('close');

                if (popupStatus == "refVoucherNo") {
                    $('#<%=txtRefVoucherNo.ClientID %>').focus();
                }
                else if (popupStatus == "refVoucherNo") {
                    $('#<%= ddlPaymode.ClientID %>').focus();
                }

                popupStatus = "";
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Duplicate Voucher No Cheque
        function fncVocherNoCheck() {
            try {
                var obj = {};
                obj.voucherNo = $('#<%=txtRefVoucherNo.ClientID %>').val();
                obj.vendorCode = $('#<%=txtVendorCode.ClientID %>').val();
                if (obj.voucherNo != "") {
                    $.ajax({
                        type: "POST",
                        url: "frmPayments.aspx/fncCheckVoucherDuplication",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d > 0) {
                                popupStatus = "refVoucherNo";
                                ShowPopupMessageBoxPayment('<%=Resources.LabelCaption.Alert_Voucher%>');

                                return false;

                            }
                        },
                        error: function (data) {
                            ShowPopupMessageBoxPayment(data.message);
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Duplicate Cheque Check
        function fncDuplicateChequeNumberCheck() {
            try {
                if ($('#<%= ddlPaymode.ClientID %>').val() != "CASH" || $('#<%= ddlPaymode.ClientID %>').val() != "CD/DN.Adj" ||
                    $('#<%= ddlPaymode.ClientID %>').val() != "RTGS") {
                    var obj = {};
                    obj.chequeNo = $.trim($('#<%= txtChequeNo.ClientID %>').val());
                    obj.BankCode = $('#<%=ddlBankCode.ClientID %>').find("option:selected").text();
                    obj.Paymode = $('#<%=ddlPaymode.ClientID %>').val();

                    if (obj.chequeNo == "")
                        return;

                    $.ajax({
                        type: "POST",
                        url: "frmPayments.aspx/fncChequeChequeDuplication",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d > 0) {

                                popupStatus = "refVoucherNo";
                                ShowPopupMessageBoxPayment('<%=Resources.LabelCaption.Alert_Cheque%>');

                                return false;

                            }
                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Close Save Dialog
        function fncSaveDialogClose() {
            try {

                $("#PaymentSave").dialog('close');
                fncInitializeChequeprint();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncPrint() {
            try {
                if ($('#<%=txtPaymentNo.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_chequeprint%>');
                    return false;
                }
                else {
                    fncInitializeChequeprint();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Initailize Display Dialog
        function fncShowDisplayitem() {
            try {
                if ($("#tbldisplay [id*=displayBodyRow]").length > 0) {
                    $("#diplay").dialog({
                        resizable: false,
                        height: 200,
                        width: 680,
                        modal: true,
                        title: "Display Item"
                    });
                }
                else
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_display%>');


                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSelectAllPayment(source) {
            var rowobj;
            try {

                $("#tblPaymnet tbody").children().each(function () {
                    rowobj = $(this);
                    if ($(source).is(":checked")) {
                        $(rowobj).find('td input[id*="cbrowFull"]').attr("checked", "checked");
                        //$(rowobj).find('td input[id*="txtAmtToPay"]').val($(rowobj).find('td span[id*="lblAmount"]').text());
                        //$(rowobj).find('td span[id*="lblBalance"]').text('0.00');
                        //$(rowobj).find('td input[id*="hidBalance"]').val('0.00');
                    }
                    else {
                        $(rowobj).find('td input[id*="cbrowFull"]').removeAttr("checked");
                        //$(rowobj).find('td input[id*="txtAmtToPay"]').val('0.00');
                        //$(rowobj).find('td span[id*="lblBalance"]').text($(rowobj).find('td span[id*="lblAmount"]').text());
                        //$(rowobj).find('td input[id*="hidBalance"]').val($(rowobj).find('td span[id*="lblAmount"]').text());
                    }
                    //else if ($(this).find('td span[id*="lblTranType"]').text() != "PURINV" ) {
                    //    $(this).find('td input[id*="cbrowFull"]').removeAttr("checked");
                    //}
                    fncAssignFullAmtCom($(this));

                });

                //$("#tblPaymnet").children().each(function () {
                //    fncTotalforView($(this));
                //});

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $('[id*="ContentPlaceHolder1_rptrPayment_txtAmtToPay_"]').number(true, 2);
        
        });
     

        function fncGoToTranPage(source) {
            var obj, page, tranNo = "", gaNo = "", billNo = "";
            var locationCode = "", ReferenceDate = "";
            try {
                obj = $(source).parent().parent();
                tranNo = obj.find('td span[id*="lblTranNo"]').text();
                billNo = obj.find('td span[id*="lblRefNo"]').text();
                ReferenceDate = obj.find('td span[id*="lblRefDate"]').text();

                if (obj.find('td span[id*="lblTranType"]').text() == "PURINV") {
                    /// Go To GRN Page
                    page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                    page = page + "?GRNNo=" + tranNo + "&GANo=" + gaNo;
                    page = page + "&BillNo=" + billNo + "&VendorCode=" + $('#<%=txtVendorCode.ClientID%>').val();
                    page = page + "&Status=View&BillType=A";
                    page = page + "&LoadPoQty=N" + "&locationcode=" + locationCode ;
                    var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "VIEW GRN MAINTENANCE"
                    });

                    //alert(page);
                    $dialog.dialog('open');
                }
                else if (obj.find('td span[id*="lblTranType"]').text() == "PURRET") {
                    /// Go to Purchase Return Page
                    page = '<%=ResolveUrl("~/Purchase/frmPurchaseReturn.aspx") %>';
                    page = page + "?PRNo=" + tranNo;
                    var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Purchase Return"
                    });

                    $dialog.dialog('open');
                }


                else if (obj.find('td span[id*="lblTranType"]').text() == "CREDIT", "DEBIT") {
                    /// Go To CREDIT NOTE Page
                    page = '<%=ResolveUrl("~/Purchase/frmSupplierNoteSummary.aspx") %>';
                    page = page + "?GRNNo=" + tranNo + "&GANo=" + gaNo;
                    page = page + "&BillNo=" + billNo + "&VendorCode=" + $('#<%=txtVendorCode.ClientID%>').val() + "&VendorName=" + $('#<%=txtVendorName.ClientID%>').val();
                    page = page + "&Date=" + ReferenceDate;
                    page = page + "&Status=View&BillType=A";
                    page = page + "&LoadPoQty=N" + "&locationcode=" + locationCode;
                    var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "VIEW CREDIT DEBIT NOTE"
                    });

                    //alert(page);
                    $dialog.dialog('open');
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //Show Pop Message
        function fncShowPRReqDet() {
            $(function () {
                $("#divPRReqDet").dialog({
                    width: "auto",
                    modal: false
                });
            });
        };

        function fncShowItemEditDailog(msg) {
            $(function () {

                $("#PaymentDialog").html("<H5><B>" + msg + "</B></H5>");
                $("#PaymentDialog").dialog({
                    title: "Enterpriser Web",
                    width: 300,
                    buttons: {
                        "Ok": function () {
                            $(this).dialog("destroy");
                            //window.location.href = "frmGoodsAcknowledgement.aspx";
                        }
                    },
                    modal: true
                });
            });
        };

        function fncAmtFocusOut(evt, source) {
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    //var NextRowobj = rowobj.next();
                    fncLoadReverseGrid();
                    $('#<%= lnkSave.ClientID %>').focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncMargin(evt,source) {
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var obj = {};
                $('#<%=lblInvoiceNo.ClientID %>').text('Invoice No : ' + rowobj.find('td span[id*="lblTranNo"]').html());
                $('#<%=lblVendorName.ClientID %>').text('Vendor Name: ' + $('#<%=txtVendorName.ClientID %>').val());
                obj.tranNo = rowobj.find('td span[id*="lblTranNo"]').html();
                obj.vendorCode = $('#<%=txtVendorCode.ClientID %>').val();
                if (charCode == "1") {
                    obj.voucherNo = "";
                }
                if (obj.voucherNo != "") {
                    $.ajax({
                        type: "POST",
                        url: "frmPayments.aspx/fncGetMarginDetailS",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            fncBindSalesDetail(msg);
                        },
                        error: function (data) {
                            ShowPopupMessageBoxPayment(data.message);
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        function fncBindSalesDetail(source) {
            try {
                var obj = jQuery.parseJSON(source.d);
                var sumMargin = 0, sumNetCost = 0, sumSPrice = 0, sumLQty = 0, sumSQty = 0, agingDays = '';
               // console.log(obj);

                var tblInvoiceDetail = $("#tblMarginBody tbody");
                tblInvoiceDetail.children().remove();

                if (obj.Table.length > 0) {
                    for (var i = 0; i < obj.Table.length; i++) {
                        var ProfitMargin = parseFloat((parseFloat($.trim(obj.Table[i]["Price"])) - parseFloat($.trim(obj.Table[i]["NetCost"]))) / parseFloat($.trim(obj.Table[i]["NetCost"])) * parseFloat(100)).toFixed(2);
                        if (parseFloat(ProfitMargin) <= 0) {
                            ProfitMargin = '0.00';
                        }
                        ProfitMargin = ProfitMargin + '%';

                        var row = "<tr id='InvoiceRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                            $.trim(obj.Table[i]["RowNo"]) + "</td><td id='tdItemCode_" + i + "' >" +
                            $.trim(obj.Table[i]["ItemCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                            $.trim(obj.Table[i]["ItemDesc"]) + "</td><td style ='text-align: right;' id='tdLqty_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["Lqty"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdSSalQty_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["SalQty"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdNCost_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["MRP"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdNCost_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["NetCost"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdPrice_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["Price"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdMargin_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["Margin"])).toFixed(2) + "</td><td style ='text-align: right;' id='tdMargin_" + i + "' >" +
                            ProfitMargin + "</td><td  style ='display: none;' id='tdSPrice_" + i + "' >" +
                            parseFloat($.trim(obj.Table[i]["SellingPrice"])).toFixed(2) + "</td></tr>";
                        sumMargin = parseFloat($.trim(obj.Table[i]["Margin"])).toFixed(2);
                        sumNetCost = parseFloat($.trim(obj.Table[i]["NetCost"])).toFixed(2);
                        sumSPrice = parseFloat($.trim(obj.Table[i]["Price"])).toFixed(2);
                        sumLQty = parseFloat($.trim(obj.Table[i]["Lqty"])).toFixed(2);
                        sumSQty = parseFloat($.trim(obj.Table[i]["SalQty"])).toFixed(2);
                        if (agingDays == '')
                            agingDays = $.trim(obj.Table[i]["Aging"]);
                        tblInvoiceDetail.append(row);

                        $("#tblHeaderData tbody");
                    }
                }

                var tblHeaderData = $("#tblHeaderData tbody");
                tblHeaderData.children().remove();

                if (parseFloat(sumMargin) <= 0) {
                    sumMargin = 0;
                }

                var MarginPer = parseFloat((parseFloat(sumSPrice) - parseFloat(sumNetCost)) / parseFloat(sumNetCost) * parseFloat(100)).toFixed(2);
                if (parseFloat(MarginPer) <= 0) {
                    MarginPer = '0.00';
                }

                var BreakEven = (parseFloat(sumNetCost) - parseFloat(sumSPrice)) / (parseFloat(sumNetCost)) * 100;
                //parseFloat(sumNetCost) - parseFloat(sumSPrice);

                if (BreakEven == 'Infinity')
                    BreakEven = 0;
                if (BreakEven < 0) {
                    BreakEven = (parseFloat(sumSPrice) - parseFloat(sumNetCost));
                    BreakEven = BreakEven / (sumNetCost * 100);
                }

                //if (parseFloat(BreakEven) > 0) {
                // BreakEven = parseFloat(BreakEven).toFixed(2);
                // }
                //else { 
                //    BreakEven = 'Achieved';
                //} 

                var StockPer = (parseFloat(sumLQty) - parseFloat(sumSQty)) / (parseFloat(sumLQty)) * 100;

                var AvgSales = parseFloat(sumSPrice) / agingDays;

                var BalanceStock = parseFloat(sumLQty) - parseFloat(sumSQty);

                var AgingSum = parseFloat(sumSQty) / agingDays;

                if (AgingSum == 'Infinity')
                    AgingSum = 0;

                var BrekEvenDays = BalanceStock / AgingSum;

                if (BrekEvenDays == 'Infinity')
                    BrekEvenDays = 0;



                var SalesPer = (parseFloat(sumSPrice) / parseFloat(sumNetCost)) * 100;

                //var row = "<tr id='InvoiceRow_" + i + "' tabindex='" + i + "' ><td style ='text-align: right;' id='tdLqty_" + i + "' >" +
                //    parseFloat(sumLQty).toFixed(2) + "</td><td style ='text-align: right;' id='tdSSalQty_" + i + "' >" +
                //    parseFloat(sumSQty).toFixed(2) + "</td><td style ='text-align: right;' id='tdStockPer_" + i + "' >" +
                //    parseFloat(StockPer).toFixed(2) + "</td><td style ='text-align: right;' id='tdagingDays_" + i + "' >" +
                //    parseFloat(agingDays).toFixed(2) + "</td><td style ='text-align: right;' id='tdAvgSales_" + i + "' >" +
                //    parseFloat(AvgSales).toFixed(2) + "</td><td  style ='text-align: right;' id='tdBreakEven_" + i + "' >" +
                //    BreakEven + "</td><td  style ='text-align: right;' id='tdBreakEvenQty_" + i + "' >" +
                //    parseFloat(BrekEvenDays).toFixed(2) + "</td><td style ='text-align: right;' id='tdMarginPer_" + i + "' >" +
                //    parseFloat(MarginPer).toFixed(2) + "</td><td style ='text-align: right;' id='tdsumMargin_" + i + "' >" +
                //    parseFloat(sumMargin).toFixed(2) + "</td></tr>";
                //tblHeaderData.append(row);

                $("#divPrQty").html(parseFloat(sumLQty).toFixed(2));
                $("#divSalesQty").html(parseFloat(sumSQty).toFixed(2));
                $("#divStockPer").html(parseFloat(StockPer).toFixed(2));
                $("#divAgingDays").html(parseFloat(agingDays).toFixed(0));
                $("#divAvgSales").html(parseFloat(AvgSales).toFixed(2));
                $("#divAceive").html(parseFloat(SalesPer).toFixed(2));
                $("#divBreakEvenDays").html(parseFloat(BrekEvenDays).toFixed(0));
                $("#divProfitPer").html(parseFloat(MarginPer).toFixed(2));
                $("#divProfit").html(parseFloat(sumMargin).toFixed(2));
                $("#divSalesPer").html(parseFloat(SalesPer).toFixed(2));
                $("#divPurchase").html(parseFloat(sumNetCost).toFixed(2));

                $('#divInvoiceMargin').dialog({
                    autoOpen: false,
                    modal: true,
                    height: "auto",
                    width: 1150,
                    title: "BreakEven"
                });

                $('#divInvoiceMargin').dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
      
        $(document).keydown(function (event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == "115") {
                $('#<%= btnsav.ClientID %>').click();
            }
        });

    </script>
    <style type="text/css">
        .body {
            background: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmPaymentsView.aspx" ><%=Resources.LabelCaption.lblViewPayments%></a><i class="fa fa-angle-right"></i></li>
                <li class="active-page " id="breadcrumbs_text_Payment">
                    <%=Resources.LabelCaption.lblSupplierPayment%>
                </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
            <div class="label-left">
                <asp:Label ID="lblStoppayment" runat="server" Style="color: white; background-color: red; margin-left: 190px;"></asp:Label>
            </div>
        </div>
        <div class="container-group-full_1">
            <%-- <div class="control-button Payment_display">
                <asp:LinkButton ID="lnkbtnDisplay" runat="server" Text='<%$ Resources:LabelCaption,btn_display %>'
                    class="button-red" OnClientClick="return fncShowDisplayitem()"></asp:LinkButton>
            </div>--%>
            <div class="payments-group">
                <div class="payments-group-left">
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Payment No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:UpdatePanel ID="uppayment" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtPaymentNo" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Payment Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Ref Voucher No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRefVoucherNo" runat="server" CssClass="form-control-res" onblur="fncVocherNoCheck()"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 100%">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Ref Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRefDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payments-group-right">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <asp:Label ID="Label21" runat="server" Text="Vendor Code"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtVendorCode" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <asp:Label ID="Label22" runat="server" Text="PRReq Items"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtPRReqItems" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-5">
                                <asp:Label ID="Label23" runat="server" Text="User"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtUser" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <asp:Label ID="Label16" runat="server" Text="Bank Code"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:DropDownList ID="ddlbankCodeVendor" runat="server" Width="100%"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="col-md-3">
                                <asp:Label ID="Label12" runat="server" Text="Vendor Name"></asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-5">
                                <asp:Label ID="Label17" runat="server" Text="Bank Ac No"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtbancAcNo" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="col-md-6">
                                <asp:Label ID="Label18" runat="server" Text="IFSC Code"></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtIfscCode" runat="server" CssClass="form-control-res" Style="width: 116%" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-single">
                        <div class="label-left" style="width: 13%">
                            <asp:Label ID="Label4" runat="server" Text="Remarks"></asp:Label>
                        </div>
                        <div class="label-right" style="width: 60%;">
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res" MaxLength="100"></asp:TextBox>
                        </div>
                         <div class="label-right" style="width: 12%;">
                            <asp:Label ID="lblGst" runat="server" Text="GST No" Style="margin-left:23%"></asp:Label>
                        </div>
                        <div class="label-left" style="width: 15% ;">
                            <asp:TextBox ID="txtGst" runat="server" CssClass="form-control-res" onMouseDown="return false" ></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left1">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Credit Limit"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCreditLimit" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-middle">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Credit Days"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCreditDays" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right1">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Hold Bills"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtHoldBills" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="payments-group">
                <div class="payments-group-left1">
                    <div class="control-group-split">
                        <div class="control-group-left1">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Paymode"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlPaymode" runat="server" CssClass="form-control-res" onchange="fncAssignPaymodeDetail()">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-middle">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="Bank Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <div id="divddlbankcode">
                                    <asp:DropDownList ID="ddlBankCode" runat="server" CssClass="form-control-res" onchange="fncGetBankName()">
                                    </asp:DropDownList>
                                </div>
                                <div id="divtxtBankcode" style="display: none">
                                    <asp:TextBox ID="txtBankcode" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-right1">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="Bank Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="payments-group-right1">
                    <div class="control-group-split">
                        <div class="control-group-left1">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text="Cheque No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control-res" onblur="fncDuplicateChequeNumberCheck()"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-middle">
                            <div class="label-left">
                                <asp:Label ID="Label14" runat="server" Text="Cheque Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtChequeDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right1">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Tran Amount"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:UpdatePanel ID="upAmt" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAmount" runat="server" onkeypress="return numericOnly(event);" onkeydown="return fncAmtFocusOut(event,this);" CssClass="form-control-res"></asp:TextBox>
                                        <%--onMouseDown="return false"--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divrepeater" class="Payment_fixed_headers">
                <table id="tblPaymnet" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Tran No
                            </th>
                            <th scope="col">Ref Date
                            </th>
                            <th scope="col">Tran Type
                            </th>
                            <th scope="col">Ref No
                            </th>
                            <th scope="col">Amount
                            </th>
                            <th scope="col">Amt To Pay
                            </th>
                            <th scope="col">
                                <asp:CheckBox ID="cbhdrFull" runat="server" onclick="fncSelectAllPayment(this);" />
                                Full
                            </th>
                            <th scope="col">Partial   
                            </th>
                            <th scope="col">Balance
                            </th>
                            <th scope="col">GST
                            </th>
                            <th scope="col">Taxable
                            </th>
                            <th scope="col">CrDays
                            </th>
                            <th scope="col">CurrRate
                            </th>
                            <th scope="col">BreakEven
                            </th>
                        </tr>
                    </thead>
                    <asp:Repeater ID="rptrPayment" runat="server" OnItemDataBound="rptrPayment_ItemDataBound">
                        <HeaderTemplate>
                            <tbody id="PaymentBody">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr id="paymentBodyrow" runat="server" >
                                <td>
                                    <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                </td>
                                <td id="tdTranNo" runat="server">
                                    <asp:Label ID="lblTranNo" CssClass="pointer" onclick="fncGoToTranPage(this);" runat="server" Text='<%# Eval("TranNo") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblRefDate" runat="server"  Text='<%# Eval("ReferenceDate") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblTranType" runat="server" Text='<%# Eval("TranType") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblRefNo" runat="server" Text='<%# Eval("ReferenceNo") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>' />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAmtToPay" runat="server" CssClass="form-control-res-right" Text='<%# Eval("AmtToPay") %>'
                                        MaxLength="10" onblur="fncBalanceAmtCalc(this)" onkeydown="return fncSetFocustoNextRow(event,this)"
                                        onkeypress="return isNumberKeyWithDecimalNew(event)" />
                                    <asp:HiddenField ID="hidAmtToPay" runat="server" Value='<%# Eval("AmtToPay") %>' />
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbrowFull" runat="server" Checked='<%# Eval("Full") %>' onclick="fncAssignFullAmt(this)" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbrowPartial" runat="server" Checked='<%# Eval("Partial") %>' onclick="return false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblBalance" runat="server" Text='<%# Eval("Balance") %>' />
                                    <asp:HiddenField ID="hidBalance" runat="server" Value='<%# Eval("Balance") %>' />

                                </td>
                                <td>
                                    <asp:Label ID="lblGST" runat="server" Text='<%# Eval("GST") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblTaxable" runat="server" Text='<%# Eval("Taxable") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblCrDays" runat="server" Text='<%# Eval("CrDays") %>' />
                                </td>
                                <td style="display: none">
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("CurrRate") %>' />
                                </td>
                                <td id="tdMargin">
                                    <asp:Button ID="btnMargin" runat="server" Text="Go" OnClientClick="fncMargin(event,this);return false;" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </FooterTemplate>
                    </asp:Repeater>
                    <tfoot>
                        <%--<tr class="repeaterfooter_payment" runat="server" id="emptyrow">
                            <td colspan="9" class="repeater_td_align_center">
                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                    Text="No items to display" />
                            </td>
                        </tr>--%>
                        <tr class="Paymentfooter_Color">
                            <td class="paymentfooter_total">Total
                            </td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lblTotalsum" runat="server" />
                            </td>
                            <td class="paymentfooter_total">
                                <asp:Label ID="lbltopaysum" runat="server" />
                            </td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_balancetotal">
                                <asp:Label ID="lblbalancesum" runat="server" />
                            </td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                            <td class="paymentfooter_total"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="control-container">
            <div class="control-button">
                <asp:UpdatePanel ID="upSave" runat="server">
                    <ContentTemplate>
                        <div class="control-button ">
                            <asp:LinkButton ID="lnkClose" runat="server" Text='Close' class="button-blue" OnClick="lnkClose_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button ">
                            <asp:LinkButton ID="lnkprint" runat="server" Text='<%$ Resources:LabelCaption,btn_Print %>'
                                class="button-red" OnClientClick="return fncPrint()"></asp:LinkButton>
                        </div>
                        <div class="control-button ">
                            <asp:LinkButton ID="lnkPRReqDet" runat="server" Text='PRReq'
                                class="button-red" OnClientClick="fncShowPRReqDet();return false;"></asp:LinkButton>
                        </div>
                        <div class="control-button ">
                            <asp:LinkButton ID="lnkbtnDisplay" runat="server" Text='<%$ Resources:LabelCaption,btn_display %>'
                                class="button-red" OnClientClick="return fncShowDisplayitem()"></asp:LinkButton>
                        </div>
                        <div class="control-button " >
                            <asp:LinkButton ID="lnkSave" runat="server" Text='<%$ Resources:LabelCaption,btnSave %>'
                                class="button-red" OnClientClick="return fncSaveValidation()" OnClick="lnkSave_Click"></asp:LinkButton>
                        </div>
                        <asp:Button ID="btnsav" runat="server" OnClientClick="return fncSaveValidation()" OnClick="lnkSave_Click" Style="display:none" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="barcodesavedialog">
            <asp:HiddenField ID="hidCLExitStatus" runat="server" Value="No" />
            <asp:HiddenField ID="hidtotalAmt" runat="server" Value="0" />
            <asp:HiddenField ID="hidToPayAmt" runat="server" Value="0" />
            <asp:HiddenField ID="hidBalance" runat="server" Value="0" />
            <asp:HiddenField ID="hidBillType" runat="server" />
            <div id="PaymentCreditLimitExist">
                <%-- <p>
                    <%=Resources.LabelCaption.Alert_CreditLimitExit%>
                </p>--%>
                <%--<div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkbtnYes" runat="server" class="button-blue" OnClientClick="return fncCLlimitOK();"
                            Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lblNo" runat="server" class="button-blue" OnClientClick="return fncCLlimitNo();"
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>--%>
            </div>
            <div id="PaymentSave">
                <div>
                    <asp:Label ID="lblSave" runat="server" Text="" />
                </div>
                <div class="dialog_center">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                        OnClientClick="return fncSaveDialogClose()"> </asp:LinkButton>
                </div>
            </div>
            <div id="ChequePrint">
                <div>
                    <asp:Label ID="lblprint" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkbtnchYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                            OnClick="lnkbtnchYes_Click"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnlbtnchNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClick="lnlbtnchNo_Click"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="diplay" class="Payment_display">
                <div class="Payment_fixed_headers payment_display_body">
                    <table id="tbldisplay" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">Display No
                                </th>
                                <th scope="col">Display Name
                                </th>
                                <th scope="col">Amount
                                </th>
                                <th scope="col">FromDate
                                </th>
                                <th scope="col">ToDate
                                </th>
                            </tr>
                        </thead>
                        <asp:Repeater ID="rptrdisplay" runat="server">
                            <HeaderTemplate>
                                <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="displayBodyRow" runat="server">
                                    <td>
                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDisplayNo" runat="server" Text='<%# Eval("DisplayNo") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDisplayName" runat="server" Text='<%# Eval("DisplayName") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("AmountToPay") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFromDate" runat="server" Text='<%# Eval("FromDate") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblToDate" runat="server" Text='<%# Eval("ToDate") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                            </FooterTemplate>
                        </asp:Repeater>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <asp:UpdateProgress ID="upPaymentProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
     
    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidGSTRPayment" runat="server" />
        <div id="dialog-MessageBoxPayment" style="display: none">
        </div>
        <div id="divPRReqDet" title="Purchase Return Request Detail" class="Payment_fixed_headers payment_PRReq">
            <table id="tblPRReq" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">InventoryCode</th>
                        <th scope="col">Description</th>
                        <th scope="col">MRP</th>
                        <th scope="col">Selling Price</th>
                        <th scope="col">Return Qty</th>
                        <th scope="col">BatchNo</th>
                        <th scope="col">PR ReqNo</th>
                    </tr>
                </thead>
                <asp:Repeater ID="rptrPRReqDet" runat="server">
                    <HeaderTemplate>
                        <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="displayBodyRow" runat="server">
                            <td>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblInventoryCode" runat="server" Text='<%# Eval("InventoryCode") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSPrice" runat="server" Text='<%# Eval("SPrice") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblPRReqQty" runat="server" Text='<%# Eval("PRReqQty") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("BatchNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblPRReqNo" runat="server" Text='<%# Eval("PRReqNo") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    </FooterTemplate>
                </asp:Repeater>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <asp:HiddenField ID="hidView" runat="server" Value="" />
        <div id="PaymentDialog" class="display_none">
        </div>
    </div>

    <div class="display_none">
        <div id="divInvoiceMargin">
            <div class="col-md-12">
                <div class="dialog-inv-container-set-price-header">
                    BreakEven Report 
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                    <div class="col-md-8">
                        <asp:Label ID="lblInvoiceNo" runat="server" />
                    </div>
                    <div class="col-md-4">
                        <asp:Label ID="lblVendorName" runat="server" />
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 5px;">
                </div>
                <div class="panel-body">
                    <div class="text-center border-line">
                        <div class="col-md-12" style="margin-top: 5px;">
                            <div class="col-md-4">
                                <span class="header">Purchase Details</span>
                            </div>
                             <div class="col-md-1"></div>
                            <div class="col-md-2">
                                <span class="header">BreakEvenDetails</span>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <span class="header">Sales Details</span>
                            </div>
                        </div>
                        <div class="col-md-4 border-double">
                            <div class="col-md-6 col-xs-6 border-outset">
                                <div class="round round-lg green">
                                    <i class="fa fa-truck" aria-hidden="true"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divPrQty">0.00</b></div>
                                    <div class="col-md-12"><b>Purchase Qty</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset left">
                                <div class="round round-lg green">
                                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divStockPer">0.00</b></div>
                                    <div class="col-md-12"><b>StockIn %</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset top">
                                <div class="round round-lg green">
                                    <span class="glyphicon glyphicon-calendar"></span>

                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divAgingDays">0</b></div>
                                    <div class="col-md-12"><b>AgingDays</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset top left">
                                <div class="round round-lg green">
                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divPurchase">0</b></div>
                                    <div class="col-md-12"><b>Purchase Value</b></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-2 border-double">
                            <div class="col-md-12 col-xs-6 border-outset">
                                <div class="round round-lg green">
                                    <i class="fa fa-line-chart"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divAceive">0.00</b></div>
                                    <div class="col-md-12"><b>BreakEven %</b></div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-6 border-outset top">
                                <div class="round round-lg green">
                                    <span class="glyphicon glyphicon-calendar"></span>

                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divBreakEvenDays">0</b></div>
                                    <div class="col-md-12" style="padding: 1px 0px 0px 0px; font-size: 15px;"><b>BreakEvenQtyInDays</b></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 border-double" style="border-right: none;">
                            <div class="col-md-6 col-xs-6 border-outset">
                                <div class="round round-lg green">
                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divSalesQty">0.00</b></div>
                                    <div class="col-md-12"><b>Sales Qty</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset left display_none">
                                <div class="round round-lg green">
                                    <i class="fa fa-balance-scale"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divSalesPer">0.00</b></div>
                                    <div class="col-md-12"><b>Sales %</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset left" style="left: 2px;">
                                <div class="round round-lg green">
                                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divAvgSales">0.00</b></div>
                                    <div class="col-md-12"><b>Avg Sales</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset top">
                                <div class="round round-lg green">
                                    <span class="glyphicon glyphicon-thumbs-up"></span>

                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divProfitPer">0.00</b></div>
                                    <div class="col-md-12"><b>Profit %</b></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 border-outset left top">
                                <div class="round round-lg green">
                                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                </div>
                                <div class="glyphicontextcolor">
                                    <div class="col-md-12 breakeventext"><b id="divProfit">0.00</b></div>
                                    <div class="col-md-12"><b>ProfitMargin</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 display_none">
                    <table id="tblHeaderData" class="table" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col" style="text-align: right;">PurchaseQty
                                </th>
                                <th scope="col" style="text-align: right;">SalesQty
                                </th>
                                <th scope="col" style="text-align: right;">StockIn %
                                </th>
                                <th scope="col" style="text-align: right;">AgingDays
                                </th>
                                <th scope="col" style="text-align: right;">Avg Sales
                                </th>
                                <th scope="col" style="text-align: right;">BreakEvenAchieve
                                </th>
                                <th scope="col" style="text-align: right;">BreakEven QtyInDays
                                </th>
                                <th scope="col" style="text-align: right;">Profit %
                                </th>
                                <th scope="col" style="text-align: right;">ProfitMargin
                                </th>
                            </tr>
                        </thead>
                        <tbody style="overflow-y: scroll; overflow-x: hidden; height: 20px;">
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12" style="margin-top: 10px; overflow-y: scroll; overflow-x: hidden; height: 250px;">
                    <table id="tblMarginBody" class="table" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">ItemCode
                                </th>
                                <th scope="col">Description
                                </th>
                                <th scope="col" style="text-align: right;">Pur.Qty
                                </th>
                                <th scope="col" style="text-align: right;">Sal.Qty
                                </th>
                                <th scope="col" style="text-align: right;">MRP
                                </th>
                                <th scope="col" style="text-align: right;">NetCost
                                </th>
                                <th scope="col" style="text-align: right;">SalePrice
                                </th>
                                <th scope="col" style="text-align: right;">Margin
                                </th>
                                <th scope="col" style="text-align: right;">Margin %
                                </th>
                                <th scope="col" style="display: none;">InvSPrice
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="margin-top: 5px; display: none;">
                <div class="col-md-2">
                    <asp:Label ID="Label19" CssClass="saleslabel" runat="server" Text="PurChase Qty : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblPurchaseQty" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="Label24" CssClass="saleslabel" runat="server" Text="Sales Qty : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblSalesQty" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="Label25" CssClass="saleslabel" runat="server" Text="StockInPer: " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblSalesPer" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="Label20" CssClass="saleslabel" runat="server" Text="Aging Days : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblAgingDays" CssClass="salesData" Style="color: brown;" runat="server" />
                </div>
            </div>
            <div style="margin-top: 5px; display: none;">

                <div class="col-md-2">
                    <asp:Label ID="lblProfitPer" CssClass="saleslabel" runat="server" Text="Profit Perc : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblProfitPercentage" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblSalesMArgin" CssClass="saleslabel" runat="server" Text="Profit Margin : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblSalesData" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="Label30" CssClass="saleslabel" runat="server" Text="BreakEven : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblBreakEven" CssClass="salesData" runat="server" />
                </div>
                <div class="col-md-2">
                    <asp:Label ID="Label26" CssClass="saleslabel" runat="server" Text="Avg Sales : " />
                </div>
                <div class="col-md-1">
                    <asp:Label ID="lblAvgSales" CssClass="salesData" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidTextileLocation" runat="server" />
    <asp:HiddenField ID="hidbtnchq" runat="server" />

</asp:Content>
