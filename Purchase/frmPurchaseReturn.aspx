﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmPurchaseReturn.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseReturn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .purchaseReturn td:nth-child(1), .purchaseReturn th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .purchaseReturn td:nth-child(2), .purchaseReturn th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(3), .purchaseReturn th:nth-child(3) {
            min-width: 400px;
            max-width: 400px;
        }

        .purchaseReturn td:nth-child(4), .purchaseReturn th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(5), .purchaseReturn th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(6), .purchaseReturn th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(7), .purchaseReturn th:nth-child(7) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(8), .purchaseReturn th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(9), .purchaseReturn th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(10), .purchaseReturn th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(11), .purchaseReturn th:nth-child(11) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(12), .purchaseReturn th:nth-child(12) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(13), .purchaseReturn th:nth-child(13) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(14), .purchaseReturn th:nth-child(14) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(15), .purchaseReturn th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(16), .purchaseReturn th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(17), .purchaseReturn th:nth-child(17) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturn td:nth-child(18), .purchaseReturn th:nth-child(18) {
            min-width: 50px;
            max-width: 50px;
        }

        .purchaseReturn td:nth-child(19), .purchaseReturn th:nth-child(19) {
            min-width: 50px;
            max-width: 50px;
        }

        .purchaseReturn td:nth-child(20), .purchaseReturn th:nth-child(20) {
            min-width: 50px;
            max-width: 50px;
        }

        .purchaseReturn td:nth-child(21), .purchaseReturn th:nth-child(21) {
            min-width: 60px;
            max-width: 60px;
        }

        .purchaseReturn td:nth-child(22), .purchaseReturn th:nth-child(22) {
            display: none;
        }

        .purchaseReturn td:nth-child(23), .purchaseReturn th:nth-child(23) {
            display: none;
        }

        .purchaseReturn td:nth-child(24), .purchaseReturn th:nth-child(24) {
            display: none;
        }
        .purchaseReturn td:nth-child(25), .purchaseReturn th:nth-child(25) {
            display: none;
        }
        .purchaseReturn td:nth-child(26), .purchaseReturn th:nth-child(26) {
            display: none;
        } 

        .purchaseReturnAdd td:nth-child(1), .purchaseReturnAdd th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .purchaseReturnAdd td:nth-child(2), .purchaseReturnAdd th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(3), .purchaseReturnAdd th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(4), .purchaseReturnAdd th:nth-child(4) {
            min-width: 350px;
            max-width: 350px;
        }

        .purchaseReturnAdd td:nth-child(5), .purchaseReturnAdd th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(6), .purchaseReturnAdd th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(7), .purchaseReturnAdd th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(8), .purchaseReturnAdd th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(9), .purchaseReturnAdd th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(10), .purchaseReturnAdd th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(11), .purchaseReturnAdd th:nth-child(11) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(12), .purchaseReturnAdd th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(13), .purchaseReturnAdd th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(14), .purchaseReturnAdd th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(15), .purchaseReturnAdd th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(16), .purchaseReturnAdd th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(17), .purchaseReturnAdd th:nth-child(17) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(18), .purchaseReturnAdd th:nth-child(18) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(19), .purchaseReturnAdd th:nth-child(19) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(20), .purchaseReturnAdd th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(21), .purchaseReturnAdd th:nth-child(21) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(22), .purchaseReturnAdd th:nth-child(22) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(23), .purchaseReturnAdd th:nth-child(23) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(24), .purchaseReturnAdd th:nth-child(24) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(25), .purchaseReturnAdd th:nth-child(25) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(26), .purchaseReturnAdd th:nth-child(26) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(27), .purchaseReturnAdd th:nth-child(27) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(28), .purchaseReturnAdd th:nth-child(28) {
            min-width: 80px;
            max-width: 80px;
        }

        .purchaseReturnAdd td:nth-child(29), .purchaseReturnAdd th:nth-child(29) {
            display: none;
        }

        .purchaseReturnAdd td:nth-child(30), .purchaseReturnAdd th:nth-child(30) {
            display: none;
        }

        .purchaseReturnAdd td:nth-child(31), .purchaseReturnAdd th:nth-child(31) {
            display: none;
        }

        .purchaseReturnAdd td:nth-child(32), .purchaseReturnAdd th:nth-child(32) {
            display: none;
        }

        .PRReq td:nth-child(1), .PRReq th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .PRReq td:nth-child(2), .PRReq th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRReq td:nth-child(3), .PRReq th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRReq td:nth-child(4), .PRReq th:nth-child(4) {
            min-width: 435px;
            max-width: 435px;
        }

        .PRReq td:nth-child(5), .PRReq th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRReq td:nth-child(6), .PRReq th:nth-child(6) {
            display: none;
        }

        .PRReq td:nth-child(7), .PRReq th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRReq td:nth-child(8), .PRReq th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRReq td:nth-child(9), .PRReq th:nth-child(9) {
            display: none;
        }

        .PRReq td:nth-child(10), .PRReq th:nth-child(10) {
            display: none;
        }

        .PRReq td:nth-child(11), .PRReq th:nth-child(11) {
            display: none;
        }

        .PRReq td:nth-child(12), .PRReq th:nth-child(12) {
            display: none;
        }

        .PRReq td:nth-child(13), .PRReq th:nth-child(13) {
            display: none;
        }

        .PRReq td:nth-child(14), .PRReq th:nth-child(14) {
            display: none;
        }

        .PRReq td:nth-child(15), .PRReq th:nth-child(15) {
            display: none;
        }

        .PRReq td:nth-child(16), .PRReq th:nth-child(16) {
            display: none;
        }

        .PRReq td:nth-child(17), .PRReq th:nth-child(17) {
            display: none;
        }

        .PRReq td:nth-child(18), .PRReq th:nth-child(18) {
            display: none;
        }

        .PRReq td:nth-child(19), .PRReq th:nth-child(19) {
            display: none;
        }

        .PRReq td:nth-child(20), .PRReq th:nth-child(20) {
            display: none;
        }

        .PRReq td:nth-child(21), .PRReq th:nth-child(21) {
            display: none;
        }

        .PRReq td:nth-child(22), .PRReq th:nth-child(22) {
            display: none;
        }

        .PRReq td:nth-child(23), .PRReq th:nth-child(23) {
            display: none;
        }

        .PRReq td:nth-child(24), .PRReq th:nth-child(24) {
            display: none;
        }

        .PRReq td:nth-child(25), .PRReq th:nth-child(25) {
            display: none;
        }

        .PRReq td:nth-child(26), .PRReq th:nth-child(26) {
            display: none;
        }

        .PRReq td:nth-child(27), .PRReq th:nth-child(27) {
            display: none;
        }

        .PRReq td:nth-child(28), .PRReq th:nth-child(28) {
            display: none;
        }

        .PRReq td:nth-child(29), .PRReq th:nth-child(29) {
            display: none;
        }

        .PRReq td:nth-child(30), .PRReq th:nth-child(30) {
            display: none;
        }

        .PRReq td:nth-child(31), .PRReq th:nth-child(31) {
            min-width: 100px;
            max-width: 100px;
        }

        .PRReq td:nth-child(32), .PRReq th:nth-child(32) {
            display: none;
        }

        .ui-dialog-titlebar-close {
            visibility: hidden;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchaseReturn');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchaseReturn";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <%------------------------------------------- DateTimePicker -------------------------------------------%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtReturnDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        });
    </script>
    <script type="text/javascript">
        var hdfTaxType;
    </script>
    <%------------------------------------------- Page Load -------------------------------------------%>
    <script type="text/javascript">
        var lastRowNo = 0;
        function pageLoad() {
            //============================  Assign Variable for Simply =========================//
            if ($('#<%=hidBillvalue.ClientID%>').val() == "A") {
                $('#lblPr').removeClass('lowercase');
                $('#lblPr').addClass('uppercase');
            }
            else if ($('#<%=hidBillvalue.ClientID%>').val() == "B") {
                $('#lblPr').removeClass('uppercase');
                $('#lblPr').addClass('lowercase');
            }

            hdfTaxType = $('#<%=hdfTaxType.ClientID %>');
            if (hdfTaxType.val() == "I") {
                $('#ContentPlaceHolder1_trheader').css('background','#d64937');
            }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            $("#<%= txtBasicCost.ClientID %>").prop('readonly', false);
            $("#<%= txtReturnDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            fncDecimal();

            //=======================> Item Code Focus Out
            $('#<%=txtItemCode.ClientID %>').focusout(function () {
                if (($('#<%=txtItemCode.ClientID %>').val() == "")) {
                    return;
                }
              <%--  if ($('#<%=ddlVendorCode.ClientID %>').val() == "") {

                    alert('Please Select Vendor Code');
                    return false;
                }--%>
                //__doPostBack('ctl00$ContentPlaceHolder1$lnkItemCode', '');
            });


            //------------------------------------------- L Qty Lost Focus -------------------------------------------------//

            $('#<%=txtLQty.ClientID %>').focusout(function () {
                try {
                    var LQty = $('#<%=txtLQty.ClientID%>').val();
                    $('#<%=txtNQty.ClientID%>').val(LQty);

                    fncCalcNetAmount(); //Calc Net Amount
                }
                catch (err) {
                    alert(err.Message);
                }
            });

            //------------------------------------------- N Qty Lost Focus -------------------------------------------------//

            $('#<%=txtNQty.ClientID %>').focusout(function () {
                try {
                    var NQty = $('#<%=txtNQty.ClientID%>').val();
                    $('#<%=txtLQty.ClientID%>').val(NQty);

                    fncCalcNetAmount();  //Calc Net Amount
                }
                catch (err) {
                    alert(err.Message);
                }
            });


            function fncCalcNetAmount() {
                var NQty = $('#<%=txtNQty.ClientID%>').val();
                if (NQty != "" && NQty != "0") {
                    //===============> Net Amt Calc

                    if (hdfTaxType.val() == 'S') {
                        var txtSGST = $('#<%=txtSGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtSGST.ClientID%>').val();
                        var txtCGST = $('#<%=txtCGST.ClientID%>').val() == '' ? '0.00' : $('#<%=txtCGST.ClientID%>').val()
                        var TotalGstPerc = parseFloat(txtSGST) + parseFloat(txtCGST);
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var TotalGstAmt = ((GrossCost * TotalGstPerc) / (100));
                        var CessAmt = (GrossCost / 100 )* $('#<%=hidCessPer.ClientID%>').val()
                        NetAmt = parseFloat(GrossCost) + parseFloat(TotalGstAmt) + parseFloat(CessAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(4));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(4) * parseFloat(NetAmt).toFixed(4)
                        $('#<%=txtNetTotal.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(4));
                    }
                    else if (hdfTaxType.val() == 'I') { //
                       <%-- $('#<%=txtSGST.ClientID%>').val($('#<%=txtIGST.ClientID%>').val());--%>
                        $('#<%=txtCGST.ClientID%>').val(0);
                        var txtSGST = $('#<%=txtSGST.ClientID%>').val();
                        var txtIGST = txtSGST;
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(4));
                        }

                        NetAmt = parseFloat(GrossCost) + parseFloat(IGSTAmt);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(4));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotal.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                        $('#<%=txtIGST.ClientID%>').val(txtIGST);
                    }
                    else if (hdfTaxType.val() == 'Import') {
                        var txtIGST = $('#<%=txtIGST.ClientID%>').val();
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        var IGSTAmt = 0;
                        if (txtIGST != "" && txtSGST != "0.00") {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            var IGSTAmt = ((GrossCost * txtIGST) / (100));
                        }
                        else {
                            var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(4));
                        }

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(4));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotal.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
                    else {
                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();

                        var GrossCost = $('#<%=txtGrossCost.ClientID%>').val();
                        $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost).toFixed(4));

                        NetAmt = parseFloat(GrossCost);
                        $('#<%=txtNet.ClientID%>').val(parseFloat(NetAmt).toFixed(4));

                        //==========> Net Total Amt Calc
                        NetAmt = $('#<%=txtNet.ClientID%>').val();
                        var NetTotalAmt = parseFloat(NQty).toFixed(2) * parseFloat(NetAmt).toFixed(2)
                        $('#<%=txtNetTotal.ClientID%>').val(parseFloat(NetTotalAmt).toFixed(2));
                    }
        }
    }

    shortcut.add("Ctrl+A", function () {
        $('#<%=hidBillvalue.ClientID%>').val('A');
        $('#lblPr').removeClass('lowercase');
        $('#lblPr').addClass('uppercase');

    });
    shortcut.add("Ctrl+B", function () {
        $("#<%=hidBillvalue.ClientID %>").val('B');
        $('#lblPr').removeClass('uppercase');
        $('#lblPr').addClass('lowercase');
    });

}
        <%--function fncKeyPress(event,value) {
            try { 
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (value == 'FQty') {
                    if (keycode == '13') {
                        $('#<%=txtBasicCost.ClientID%>').focus().select();
                    }
                }
                
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>
         

    </script>
    <%------------------------------------------- Batch Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenBatch() {
            $("#dialog-Batch").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "blind",
                    duration: 500
                },
                hide: {
                    effect: "blind",
                    duration: 500
                },
                buttons: {
                    Exit: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Clear Textbox After Add Inventory -------------------------------------------%>
    <script type="text/javascript">
        function fncClearAfterAddInv() {
            //$("div[id*=addInv]").find("input[type=text]").val("");
            clearForm();
            fncEnableDisablePRAddItem();
        }
    </script>
    <%------------------------------------------- Add Validation -------------------------------------------%>
    <script type="text/javascript">
        function fncAddValidation() {
            var status = true;
            if (document.getElementById("<%=txtItemCodeAdd.ClientID%>").value == "") {
                fncToastInformation('Please Enter Select Inventory');
                $('#<%=txtItemCode.ClientID %>').select();
                status = false;
                return false;
            }
            else if (parseFloat($('#<%=txtNQty.ClientID %>').val()) == 0) {
                fncToastInformation('Please Enter L Qty');
                $('#<%=txtLQty.ClientID %>').select();
                status = false;
                return false;
            }
            else if (parseFloat($('#<%=txtMrp.ClientID %>').val()) < parseFloat($('#<%=txtNet.ClientID %>').val())) {
                fncToastInformation('NetCost is Must be Lesser than or Equal to MRP .!');
                $('#<%=txtNet.ClientID %>').select();
                status = false;
                return false;
            }
            else {
                $("[id*=gvPurRetn] tr").each(function () {
                    var rowObj = $(this);
                    if (rowObj.find(".cssItemcode").text().trim() == $('#<%=txtItemCodeAdd.ClientID%>').val().trim() && rowObj.find(".cssBatchno").text().trim() == $('#<%=txtBatchNo.ClientID%>').val().trim()) {
                        {
                            if (status == true) {
                                fncInitializeAlreadyExist();
                                status = false;
                                return false;
                            }
                            <%--status = false;
                            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID%>');
                            $('#<%=txtItemCode.ClientID%>').select();
                            fncToastInformation('<%=Resources.LabelCaption.Alert_AddedList%>');
                            clearForm();
                            return false;--%>
                        }
                    }
                }
            );

                if (status == false)
                    return false;
                else
                    $('#<%=txtStock.ClientID%>').val('0.00');

            }

    }
    </script>
    <%------------------------------------------- Save Validation -------------------------------------------%>
    <script type="text/javascript">
        var SaveFlag = false;
        function Validate() {
            $('#<%=lnkSave.ClientID %>').css("display", "none");
            if (SaveFlag == false) {
                if ($("#ContentPlaceHolder1_gvPurRetn tbody").children().length == 1 && $("#<%=hdVItemCode.ClientID %>").val() == "") {
                    if ($("#ContentPlaceHolder1_gvPurRetn_Label3").text() == "No Records Found") {
                        fncToastInformation('No Records Found');
                        $('#<%=lnkSave.ClientID %>').css("display", "block");
                        return false;
                    }
                }

                if (document.getElementById("<%=ddlRemarks.ClientID%>").value == "0" || document.getElementById("<%=ddlRemarks.ClientID%>").value == "") {
                    fncToastInformation('Please Select Remarks');
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                    setTimeout(function () {
                        $("#<%=ddlRemarks.ClientID %>").trigger("liszt:open");
                    }, 50);
                    return false;
                }
                else {
                    SaveFlag = true;
                    return true;
                }
            }

            

            else {
              
                return false;
            }
           
           
        }

        /// Work Start by saravanan 06-12-2017
        function fncGidNoandBillNoChange(value) {
            try {
                if (value == "gid") {
                    $('#<%=ddlVendorBillNo.ClientID %>').val($('#<%=ddlGIDNo.ClientID %>').find('option:selected').text());
                    $("#<%=ddlVendorBillNo.ClientID %>").trigger("liszt:updated");
                }
                else {
                    $('#<%=ddlGIDNo.ClientID %>').val($('#<%=ddlVendorBillNo.ClientID %>').find('option:selected').text());
                    $("#<%=ddlGIDNo.ClientID %>").trigger("liszt:updated");
                }

                $('#<%=btnVendorchange.ClientID %>').click();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        ///Get InventoryDetail
        function fncGetInventoryDetail(source) {
            try {
                var grossCost = 0, netCost = 0, gstAmt = 0, cessAmt = 0;
                var obj = {};
                var rowObj, batch;
                // rowObj = $(source).closest("tr");
                rowObj = source;


                $("#<%=txtLUOM.ClientID %>").val($.trim($("td", rowObj).eq(21).text()));
                $("#<%=txtWUOM.ClientID %>").val($.trim($("td", rowObj).eq(3).text()));
                $("#<%=txtItemCodeAdd.ClientID %>").val($.trim($("td", rowObj).eq(1).text()));
                $("#<%=txtItemNameAdd.ClientID %>").val($.trim($("td", rowObj).eq(2).text()));
                $("#<%=txtMrp.ClientID %>").val($.trim($("td", rowObj).eq(8).text()));
                $("#<%=txtBasicCost.ClientID %>").val($.trim($("td", rowObj).eq(10).text()));
                $("#<%=txtSGST.ClientID %>").val($.trim($("td", rowObj).eq(17).text()));
                $("#<%=txtCGST.ClientID %>").val($.trim($("td", rowObj).eq(18).text()));
                $("#<%=txtIGST.ClientID %>").val($.trim($("td", rowObj).eq(19).text()));
                $("#<%=txtSellingPrice.ClientID %>").val($.trim($("td", rowObj).eq(9).text()));
                $("#<%=txtBatchNo.ClientID %>").val($.trim($("td", rowObj).eq(16).text()));
                $("#<%=txtDisc.ClientID %>").val($.trim($("td", rowObj).eq(23).text()));
                $("#<%=txtDisAmt.ClientID %>").val($.trim($("td", rowObj).eq(15).text()));
                $("#<%=txtSD.ClientID %>").val($.trim($("td", rowObj).eq(24).text()));
                $("#<%=txtSdAmt.ClientID %>").val($.trim($("td", rowObj).eq(25).text()));
                grossCost = parseFloat($("#<%=txtBasicCost.ClientID %>").val()) - parseFloat($("#<%=txtDisAmt.ClientID %>").val()) - parseFloat($("#<%=txtSdAmt.ClientID %>").val());
                $("#<%=txtGrossCost.ClientID %>").val(grossCost);
                gstAmt = (grossCost * parseFloat($("#<%=txtIGST.ClientID %>").val())) / 100;
                cessAmt = (grossCost * parseFloat($("td", rowObj).eq(21).text())) / 100;
                netCost = parseFloat(grossCost) + parseFloat(gstAmt) + parseFloat(cessAmt);
                $("#<%=txtNet.ClientID %>").val(netCost);
                $("#<%=txtNet.ClientID %>").val(0);
                setTimeout(function () {
                    $("#<%=txtLQty.ClientID %>").select();
                }, 10);

                if ($.trim($("td", rowObj).eq(21).text()) == "PCS") {

                    $("#<%=txtWQty.ClientID %>").number(true, 0);
                    $("#<%=txtLQty.ClientID %>").number(true, 0);
                    $("#<%=txtFOC.ClientID %>").number(true, 0);
                    $("#<%=txtNQty.ClientID %>").number(true, 0);
                }
                else {
                    $("#<%=txtWQty.ClientID %>").number(true, 2);
                    $("#<%=txtLQty.ClientID %>").number(true, 2);
                    $("#<%=txtFOC.ClientID %>").number(true, 2);
                    $("#<%=txtNQty.ClientID %>").number(true, 2);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        /// Add Batch Details
        function fncAssignbatchvaluestotextbox(batchNo) {
            var obj = {};
            try {

                if ($('#<%=ddlGIDNo.ClientID %>').find('option:selected').text() == "-- Select --") {
                    obj.gidNo = "";
                    obj.mode = "1";
                }
                else {
                    obj.gidNo = $('#<%=ddlGIDNo.ClientID %>').find('option:selected').text();
                    obj.mode = "6";
                }

                obj.itemcode = $("#<%=hidItemcode.ClientID %>").val();
                obj.batchNo = batchNo;


                $("#<%=hidBatchNo.ClientID %>").val(batchNo);
                $.ajax({
                    type: "POST",
                    url: "frmPurchaseReturn.aspx/fncGetInventoryStatus",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "Batch") {
                            $("#<%=btnItem.ClientID %>").click();
                        }
                        else {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.msg_noGidRecord%>');
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Clear Text Box values
        function clearForm() {
            try {
                $("#<%=txtItemCodeAdd.ClientID %>").val('');
                $("#<%=txtItemNameAdd.ClientID %>").val('');
                $("#<%=txtWQty.ClientID %>").val('0');
                $("#<%=txtNQty.ClientID %>").val('0');
                $("#<%=txtMrp.ClientID %>").val('0');
                $("#<%=txtBasicCost.ClientID %>").val('0');
                $("#<%=txtDisc.ClientID %>").val('0');
                $("#<%=txtDisAmt.ClientID %>").val('0');
                $("#<%=txtSD.ClientID %>").val('0');
                $("#<%=txtSdAmt.ClientID %>").val('0');
                $("#<%=txtGrossCost.ClientID %>").val('0');
                $("#<%=txtNet.ClientID %>").val('0');
                $("#<%=txtNetTotal.ClientID %>").val('0');
                $("#<%=txtSGST.ClientID %>").val('0');
                $("#<%=txtCGST.ClientID %>").val('0');
                $("#<%=txtLQty.ClientID %>").val('0');
                $("#<%=txtFOC.ClientID %>").val('0');
                $("#<%=txtSellingPrice.ClientID %>").val('0');
                $("#<%=txtBatchNo.ClientID %>").val('');
                $("#<%=txtWUOM.ClientID %>").val('');
                $("#<%=txtLUOM.ClientID %>").val('');
                $("#<%=txtItemCodeAdd.ClientID %>").focus();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            fncDropdownKeyAction();
            fncDecimal();
        });

        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123, 66);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 112) {
                        return false;
                    }
                    else if (e.keyCode == 114) {

                        if ($('#<%=hidPRAddItemVis.ClientID %>').val() == '0') {
                            $('#<%=lblMode.ClientID %>').text("Purchase Return Details");
                            $('#divPRAddList').css('display', 'block');
                            $('#divPRVenList').css('display', 'none');
                            $('#<%=hidPRAddItemVis.ClientID %>').val('1');
                            $('#<%=btnAddGrid.ClientID %>').click();
                            e.preventDefault();
                            return false;
                        }
                        else {
                            $('#<%=lblMode.ClientID %>').text("Vendor Details");
                            $('#divPRAddList').css('display', 'none');
                            $('#divPRVenList').css('display', 'block');
                            $('#<%=hidPRAddItemVis.ClientID %>').val('0');
                            e.preventDefault();
                            return false;
                        }

                    }
                    else if (e.keyCode == 113) {

                        <%-- page = '<%=ResolveUrl("~/Inventory/frmCategoryMaster.aspx") %>';
                        $dialog = $('<div id="popupVendor" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                            autoOpen: false,
                            modal: true,
                            height: 645,
                            width: 1300,
                            title: "Vendor",
                            closeOnEscape: true
                        });
                        $dialog.dialog('open');--%>
                        alert('Cehck');

                    }
                    else if (e.keyCode == 115) {
                        if (Validate() == true) {
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        }
                    }
                    else if (e.keyCode == 117) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClearAll', '');
                    }
                    else if (e.keyCode == 119) {
                        window.location = "frmPurchaseReturnView.aspx";
                    }
                    else if (e.keyCode == 121) {
                        $('#<%=chkPurchaseReq.ClientID%>').prop("checked", true);
                        fncOpenPurchaseRequestDet();
                        e.preventDefault();
                        return false;
                    }
                    else if (e.keyCode == 123) {
                        if ($('#<%=cbVendorItem.ClientID%>').is(':checked')) {
                            $("#<%=cbVendorItem.ClientID%>").removeAttr("checked");
                        }
                        else {
                            $("#<%=cbVendorItem.ClientID%>").attr("checked", "checked");
                        }
                        e.preventDefault();
                        e.preventDefault();
                        return false;
                    }

}

}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncEnableDisablePRAddItem() {
    try {

        $('#divPRAddList').css('display', 'block');
        $('#divPRVenList').css('display', 'none');
        fncChangeSetNumberFormat();
        //$('#<%=hidPRAddItemVis.ClientID %>').val('1');  

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncSetFocustoNextRow(evt, source, mode) {
    var rowobj, charCode, verHight;
    rowobj = $(source).parent().parent();
    charCode = (evt.which) ? evt.which : evt.keyCode;
    try {
        if (charCode == 13 || charCode == 40) {
            var NextRowobj = rowobj.next();
            if (NextRowobj.length > 0) {

                if (mode == "venItem" || mode == "WQty") {
                    verHight = $("#gvVendorItem").scrollTop();
                    $("#gvVendorItem").scrollTop(verHight + 10);
                }

                if (mode == "WQty") {
                    fncSetFocustoObject(NextRowobj.find('td input[id*="txtWQty"]'));
                }
                else {
                    fncSetFocustoObject(NextRowobj.find('td input[id*="txtLQty"]'));
                }
            }
            else {
                if (charCode == 13) {
                    if (mode == "venItem") {
                        if ($('#ContentPlaceHolder1_gvVendorItem tbody').children().length == 1) {
                            $('#<%=lnkSave.ClientID %>').focus();
                        }
                        else {
                            fncSetFocustoObject(rowobj.siblings().first().find('td input[id*="txtLQty"]'));
                        }
                    }
                }
            }

            if (charCode == 13) {
                return false;
            }
        }
        else if (charCode == 38) {
            var prevrowobj = rowobj.prev();
            if (prevrowobj.length > 0) {

                if (mode == "venItem" || mode == "WQty") {
                    verHight = $("#gvItemLoad").scrollTop();
                    $("#gvItemLoad").scrollTop(verHight - 10);
                }
                if (mode == "WQty") {
                    fncSetFocustoObject(prevrowobj.find('td input[id*="txtWQty"]'));
                }
                else {
                    fncSetFocustoObject(prevrowobj.find('td input[id*="txtLQty"]'));
                }


            }
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncRepeaterColumnValueChange(value) {
    var obj, venItemcode = "", batchNo = "", GetQty = 0, ItemCode = "";
    var addObj, addItemcode = "", addBatchNo = "", Itemstatus = false;
    var totalItems = 0, netvalue = 0, totalValue = 0;
    try {

        if (value == "NewQty") {
            $("#ContentPlaceHolder1_gvVendorItem tbody").children().each(function () {
                obj = $(this);

                venItemcode = $.trim($("td", obj).eq(1).text());
                batchNo = $.trim($("td", obj).eq(16).text());
                GetQty = obj.find('td input[id*=txtLQty]').val();
                if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {
                    if (ItemCode == "") {
                        ItemCode = $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val() + ";" + batchNo;
                    }
                    else {
                        ItemCode = ItemCode + "|" + $.trim($("td", obj).eq(1).text()) + ";" + GetQty + ";" + obj.find('td input[id*=txtWQty]').val() + ";" + batchNo;
                    }
                }

                //Add Inventory Calculation
                Itemstatus = false;
                $("#ContentPlaceHolder1_gvPurRetn tbody").children().each(function () {
                    addObj = $(this);
                    addItemcode = $.trim($("td", addObj).eq(3).text());
                    addBatchNo = $.trim($("td", addObj).eq(25).text());
                    if (venItemcode == addItemcode && batchNo == addBatchNo) {
                        //addObj.find('td input[id*=txtQty]').val(GetQty);
                        Itemstatus = true;
                    }
                });

                if (Itemstatus == false) {
                    if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {
                        var grossCost = 0, GSTPer = 0;
                        totalItems = parseFloat(totalItems) + 1;
                        grossCost = parseFloat($("td", obj).eq(10).text()) - parseFloat($("td", obj).eq(15).text());
                        GSTPer = parseFloat($("td", obj).eq(19).text()) + parseFloat(parseFloat($("td", obj).eq(20).text()));
                        netvalue = parseFloat(grossCost) + parseFloat((grossCost * GSTPer) / 100);
                        totalValue = parseFloat(totalValue) + parseFloat(GetQty * netvalue);
                    }
                }

            });

            ///CalCulate Added Total Value
            $("#ContentPlaceHolder1_gvPurRetn tbody").children().each(function () {

                obj = $(this);
                GetQty = $.trim($("td", addObj).eq(5).text());
                if (GetQty != '0.00' && GetQty != '0' && GetQty != '') {
                    totalItems = parseFloat(totalItems) + 1;
                    totalValue = parseFloat(totalValue) + parseFloat($("td", addObj).eq(20).text());
                }
            });
            fncTotalValueCal(totalItems, totalValue);
            $('#<%=hdVItemCode.ClientID%>').val(ItemCode); // Get ItemCode  
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncLQtyChange(value) {
    var totalVal = 0;
    try {
        if (value == 'Qty') {
            $('#<%=txtNQty.ClientID%>').val($('#<%=txtLQty.ClientID%>').val());
            totalVal = $('#<%=txtLQty.ClientID%>').val() * $('#<%=txtNet.ClientID%>').val();
            $('#<%=txtNetTotal.ClientID%>').val(totalVal);
        }
        else if (value == 'BasicCost') {
            var GrossCost = parseFloat($('#<%=txtBasicCost.ClientID%>').val()) - (parseFloat($('#<%=txtDisc.ClientID%>').val()) + parseFloat($('#<%=txtSD.ClientID%>').val()))
            $('#<%= txtGrossCost.ClientID%>').val(GrossCost.toFixed(2));
            var GSTAmt = (parseFloat(GrossCost) * ((parseFloat($('#<%= txtSGST.ClientID%>').val()) + (parseFloat($('#<%= txtCGST.ClientID%>').val())))) / 100)
            var CessAmt = ((GrossCost / 100) * $('#<%= hidCessPer.ClientID%>').val());
            $('#<%=txtNet.ClientID%>').val(parseFloat(GrossCost) + parseFloat(GSTAmt) + parseFloat(CessAmt));
            totalVal = $('#<%=txtLQty.ClientID%>').val() * $('#<%=txtNet.ClientID%>').val();
            $('#<%=txtNetTotal.ClientID%>').val(totalVal);
        }
}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncTotalValueCal(totalItems, totalValue) {
    try {
        $('#<%=txtTotalItems.ClientID%>').val(totalItems);
        $('#<%=txtPRTotal.ClientID%>').val(totalValue);
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncWQtyChange(source) {
    var rowObj, qty = 0;
    try {
        rowObj = $(source).closest("tr");
        if ($.trim($("td", rowObj).eq(3).text()) != "") {
            qty = parseFloat(rowObj.find('td input[id*="txtWQty"]').val()) * parseFloat($("td", rowObj).eq(22).text());
            rowObj.find('td input[id*="txtLQty"]').val(qty);
            fncRepeaterColumnValueChange('NewQty');
        }

    }
    catch (err) {
        fncToastError(err.message);
    }
}

/// Grid View Search
function fncItemSearch(itemObj) {
    var rowObj, SearchData;
    try {
        SearchData = $(itemObj).val().trim();
        SearchData = SearchData.toLowerCase();
        if (SearchData == "") {
            $("#ContentPlaceHolder1_gvVendorItem tbody").children().removeClass("display_none");
        }
        else {
            $("#ContentPlaceHolder1_gvVendorItem tbody").children().each(function () {
                rowObj = $(this);
                if ($.trim($("td", rowObj).eq(1).text()).toLowerCase().indexOf(SearchData) < 0) {
                    rowObj.addClass("display_none");
                }
                else {
                    rowObj.removeClass("display_none");
                }

            });
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncDropdownKeyAction() {
    try {

        $("#ContentPlaceHolder1_ddlRemarks_chzn").bind('keyup', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

            if (keyCode == 115) {
                if (Validate() == true) {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                }
            }
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncDecimal() {
    try {
        $('#<%=txtPRTotal.ClientID%>').number(true, 2);
        $('#<%=txtMrp.ClientID%>').number(true, 2);
        $('#<%=txtBasicCost.ClientID%>').number(true, 2);
        $('#<%=txtDisc.ClientID%>').number(true, 2);
        $('#<%=txtDisAmt.ClientID%>').number(true, 2);
        $('#<%=txtSD.ClientID%>').number(true, 2);
        $('#<%=txtSdAmt.ClientID%>').number(true, 2);
        $('#<%=txtGrossCost.ClientID%>').number(true, 2);
        $('#<%=txtNet.ClientID%>').number(true, 4);
        $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
        $('#<%=txtNetTotal.ClientID%>').number(true, 2);
        $('#<%=txtSGST.ClientID%>').number(true, 4);
        $('#<%=txtCGST.ClientID%>').number(true, 4);
        $('#<%=hidCessPer.ClientID %>').number(true, 4);
    }
    catch (err) {
        fncToastError(err.message);
    }
}

/// Change Grid Color
function fncChangeSetNumberFormat() {
    var obj;
    try {
        $("#ContentPlaceHolder1_gvVendorItem tbody").children().each(function () {
            obj = $(this);

            if ($("td", obj).eq(21).text().trim() == "PCS") {
                obj.find('td input[id*="txtWQty"]').number(true, 0);
                obj.find('td input[id*="txtLQty"]').number(true, 0);
            }
            else {
                obj.find('td input[id*="txtWQty"]').number(true, 2);
                obj.find('td input[id*="txtLQty"]').number(true, 2);
            }

        });
        fncDropdownKeyAction();
        $('#<%=txtItemCodeAdd.ClientID %>').removeAttr("disabled");
        if ($('#<%=hidPrrList.ClientID %>').val() != "") {
            fncPRRListShow();
            $("#<%= txtReturnDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        }


    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Inventory Search
function SetAutoComplete() {
    $("[id$=txtItemCodeAdd]").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            valitemcode: item.split('|')[1],
                            valName: item.split('|')[2]
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
        focus: function (e, i) {
            $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(i.item.valitemcode));
            e.preventDefault();
        },
        select: function (e, i) {
            //alert(i.item.val);
            $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(i.item.valitemcode));
            $('#<%=txtItemNameAdd.ClientID %>').val($.trim(i.item.valName));
            //$('#<%=txtItemCodeAdd.ClientID %>').focus();
            fncGetItemDetail();
            return false;
        },
        minLength: 1
    });
}
function fncGetItemDetail() {
    var obj = {};
    try {
        obj.itemcode = $('#<%=txtItemCodeAdd.ClientID %>').val();
            if ($('#<%=cbVendorItem.ClientID %>').is(":checked")) {
                obj.vendorcode = $('#<%=txtVendor.ClientID %>').val();
            }
            else {
                obj.vendorcode = "";
            }
            $.ajax({
                url: '<%=ResolveUrl("~/WebService.asmx/fncGetItemdetailForPR")%>',
                data: JSON.stringify(obj),
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    fncAssignValuesToControl($.parseJSON(data.d));
                },
                error: function (response) {
                    fncToastError(response.message);
                },
                failure: function (response) {
                    fncToastError(response.message);
                }
            });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncAssignValuesToControl(obj) {
        try {

            if (obj[0]["Status"] == "InvalidItems") {
                fncToastInformation("Invalid Items");
                clearForm();
                return false;
            }
            else if (obj[0]["Status"] == "Scan") {
                $('#<%=txtItemCodeAdd.ClientID %>').val(obj[0]["InventoryCode"]);
            $('#<%=txtItemNameAdd.ClientID %>').val(obj[0]["Description"]);
            $('#<%=txtLUOM.ClientID %>').val(obj[0]["UOM"]);
            $('#<%=txtWUOM.ClientID %>').val(obj[0]["Compatible"]);
            $('#<%=txtMrp.ClientID %>').val(obj[0]["tbMrp"]);
            $('#<%=txtBasicCost.ClientID %>').val(obj[0]["tbBasiccost"]);
            $('#<%=txtDisc.ClientID %>').val(obj[0]["DiscountBasicPer"]);
            $('#<%=txtDisAmt.ClientID %>').val(obj[0]["DiscountBasicAmt"]);
            $('#<%=txtGrossCost.ClientID %>').val(obj[0]["GrossCost"]);
            $('#<%=txtNet.ClientID %>').val(obj[0]["tbNetCost"]);
            $('#<%=txtSellingPrice.ClientID %>').val(obj[0]["tbSellingPrice"]);
            $('#<%=txtSGST.ClientID %>').val(obj[0]["ITaxPer1"]);
            $('#<%=txtCGST.ClientID %>').val(obj[0]["ITaxPer2"]);
            $('#<%=txtIGST.ClientID %>').val(obj[0]["ITaxPer3"]);
            $('#<%=hidCessPer.ClientID %>').val(obj[0]["ITaxPer4"]);
            $('#<%=txtBatchNo.ClientID %>').val(obj[0]["tbBatchno"]);
            if ($('#<%=txtWUOM.ClientID %>').val() != "") {
                $('#<%=txtWQty.ClientID %>').select();
                }
                else {
                    $('#<%=txtLQty.ClientID %>').select();
                }
            //alert(obj[0]["ITaxPer4"])
            }
            else if (obj[0]["Status"] == "Batch") {
                fncGetBatchDetail_Master($('#<%=txtItemCodeAdd.ClientID %>').val());

                $('#<%=txtItemCodeAdd.ClientID %>').val(obj[0]["InventoryCode"]);
                $('#<%=txtItemNameAdd.ClientID %>').val(obj[0]["Description"]);
                $('#<%=txtLUOM.ClientID %>').val(obj[0]["UOM"]);
                $('#<%=txtWUOM.ClientID %>').val(obj[0]["Compatible"]);
                $('#<%=txtMrp.ClientID %>').val(obj[0]["MRP"]);
                   <%-- $('#<%=txtBasicCost.ClientID %>').val(obj[0]["BasicCost"]);--%>
                $('#<%=txtDisc.ClientID %>').val(obj[0]["DiscountBasicPer"]);
                $('#<%=txtDisAmt.ClientID %>').val(obj[0]["DiscountBasicAmt"]);
                    <%--$('#<%=txtGrossCost.ClientID %>').val(obj[0]["GrossCost"]);
                    $('#<%=txtNet.ClientID %>').val(obj[0]["NetCost"]);--%>
                $('#<%=txtSellingPrice.ClientID %>').val(obj[0]["NetSellingPrice"]);
                $('#<%=txtSGST.ClientID %>').val(obj[0]["ITaxPer1"]);
                $('#<%=txtCGST.ClientID %>').val(obj[0]["ITaxPer2"]);
                $('#<%=txtIGST.ClientID %>').val(obj[0]["ITaxPer3"]);
                $('#<%=hidCessPer.ClientID %>').val(obj[0]["ITaxPer4"]);
            }
            else {
                if ($('#<%=txtWUOM.ClientID %>').val() != "") {
                    $('#<%=txtWQty.ClientID %>').select();
                    }
                    else {
                        $('#<%=txtLQty.ClientID %>').select();
                    }
                    $('#<%=txtItemCodeAdd.ClientID %>').val(obj[0]["InventoryCode"]);
                $('#<%=txtItemNameAdd.ClientID %>').val(obj[0]["Description"]);
                $('#<%=txtLUOM.ClientID %>').val(obj[0]["UOM"]);
                $('#<%=txtWUOM.ClientID %>').val(obj[0]["Compatible"]);
                $('#<%=txtMrp.ClientID %>').val(obj[0]["MRP"]);
                $('#<%=txtBasicCost.ClientID %>').val(obj[0]["BasicCost"]);
                $('#<%=txtDisc.ClientID %>').val(obj[0]["DiscountBasicPer"]);
                $('#<%=txtDisAmt.ClientID %>').val(obj[0]["DiscountBasicAmt"]);
                $('#<%=txtGrossCost.ClientID %>').val(obj[0]["GrossCost"]);
                $('#<%=txtNet.ClientID %>').val(obj[0]["NetCost"]);
                $('#<%=txtSellingPrice.ClientID %>').val(obj[0]["NetSellingPrice"]);
                $('#<%=txtSGST.ClientID %>').val(obj[0]["ITaxPer1"]);
                $('#<%=txtCGST.ClientID %>').val(obj[0]["ITaxPer2"]);
                $('#<%=txtIGST.ClientID %>').val(obj[0]["ITaxPer3"]);
                $('#<%=hidCessPer.ClientID %>').val(obj[0]["ITaxPer4"]);
            }
}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncAssignbatchvaluestotextbox(batchNo, mrp, sellingprice, expMonth, expYear, basiccost, stock, netCost,gross) {
    try {
        $('#<%=txtBatchNo.ClientID %>').val(batchNo);
        $('#<%=txtMrp.ClientID %>').val(mrp);
        $('#<%=txtSellingPrice.ClientID %>').val(sellingprice);
        $('#<%=txtBasicCost.ClientID %>').val(basiccost);
        $('#<%=txtNet.ClientID %>').val(netCost);
        $('#<%=txtGrossCost.ClientID %>').val(gross);

        if ($('#<%=txtWUOM.ClientID %>').val() != "") {
            $('#<%=txtWQty.ClientID %>').select();
        }
        else {
            $('#<%=txtLQty.ClientID %>').select();
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

///Get Inventory code
//function fncGetInventorycodePR(evt) {
//    var charCode = (evt.which) ? evt.which : evt.keyCode;
//    try {
//        if (charCode == 13) {
//            fncGetItemDetail();
//            return false;
//        }
//    }
//    catch (err) {
//        fncToastError(err.message);
//    }
//}

function fncBindRurchaseReturnRequest(obj) {
    var PurReqBody, row;
    try {
        PurReqBody = $("#PRRquest tbody");
        PurReqBody.children().remove();
        obj = $.parseJSON(obj);
        if (obj.length > 0) {
            for (var i = 0; i < obj.length; i++) {
                row = "<tr>"
                    + "<td id='tdRowNo_" + i + "' > " + obj[i]["RowNo"] + "</td>"
                    + "<td><input id='cbPRReq_" + i + "' type='checkbox' /></td>"
                     + "<td id='tdCode_" + i + "' > " + obj[i]["Inventorycode"] + "</td>"
                     + "<td id='tdDescription_" + i + "' > " + obj[i]["Description"] + "</td>"
                     + "<td id='tdLQty_" + i + "'style = 'text-align:right !important''' > " + obj[i]["PRReqQty"] + "</td>"
                     + "<td id='tdFOC_" + i + "'style = 'text-align:right !important''' > 0 </td>"
                     + "<td id='tdMRP_" + i + "'style = 'text-align:right !important''' > " + parseFloat(obj[i]["MRP"]).toFixed(2) + "</td>"
                     + "<td id='tdSelling_" + i + "'style = 'text-align:right !important''' > " + parseFloat(obj[i]["SellingPrice"]).toFixed(2) + "</td>"
                     + "<td id='tdCost_" + i + "'style = 'text-align:right !important''' > " + obj[i]["CurrPrice"] + "</td>"
                     + "<td id='tdDiscAmt_" + i + "'style = 'text-align:right !important''' >" + obj[i]["DiscountBasicAmt"] + "</td>"
                     + "<td id='tdDiscPrc_" + i + "'style = 'text-align:right !important''' >" + obj[i]["DiscountBasicPer"] + "</td>"
                     + "<td id='tdODiscAmt_" + i + "'style = 'text-align:right !important''' >0</td>"
                     + "<td id='tdDiscountBasicAmt_" + i + "'style = 'text-align:right !important''' >0</td>"
                     + "<td id='tdGross_" + i + "'style = 'text-align:right !important''' >0</td>"
                     + "<td id='tdITaxPer3_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer3"] + "</td>"
                     + "<td id='tdITaxPer1_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer1"] + "</td>"
                     + "<td id='tdITaxPer2_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer2"] + "</td>"
                     + "<td id='tdITaxPer4_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer4"] + "</td>"
                     + "<td id='tdNetCost_" + i + "'style = 'text-align:right !important''' > " + obj[i]["NetCost"] + "</td>"
                     + "<td id='tdNetTotal_" + i + "'style = 'text-align:right !important''' > " + (parseFloat(obj[i]["PRReqQty"]) * parseFloat(obj[i]["NetCost"])).toFixed(2) + "</td>"
                     + "<td id='tdSGSTAmt_" + i + "'style = 'text-align:right !important''' > " + (parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 / 2).toFixed(2) + " </td>"
                     + "<td id='tdCGSTAmt_" + i + "'style = 'text-align:right !important''' >" + (parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 / 2).toFixed(2) + "</td>"
                     + "<td id='tdIGSTAmt_" + i + "'style = 'text-align:right !important''' >" + parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 + "</td>"
                     + "<td id='tdCessAmt_" + i + "'style = 'text-align:right !important''' >" + parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer4"]) + "</td>"
                     + "<td id='tdVendorBill_" + i + "' ></td>"
                     + "<td id='tdGIDNo_" + i + "' ></td>"
                     + "<td id='tdBatchNo_" + i + "' >" + obj[i]["BatchNo"] + "</td>"
                     + "<td id='tdStock_" + i + "' >0</td>"
                     + "<td id='tdSerialNo_" + i + "' >0</td>"
                     + "<td id='tdUOM_" + i + "' >" + obj[i]["UOM"] + "</td>"
                     + "<td id='tdPRReqNo_" + i + "' >" + obj[i]["PRReqNo"] + "</td>"
                     + "</tr>";
                PurReqBody.append(row);
            }

        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncGetPurchaseReturnRequestDet() {
    var obj = {};
    try {
        obj.vendorCode = $('#<%=txtVendor.ClientID%>').val();
        $.ajax({
            url: "frmPurchaseReturn.aspx/fncGetPRReqDet",
            data: JSON.stringify(obj),
            type: "POST",
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (msg) {
                fncBindRurchaseReturnRequest(msg.d);
            },
            failure: function (msg) {
                fncToastError(msg.d);
            }

        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncOpenPRRDailog() {
    try {
        $("#divPPRquest").dialog({
            modal: true,
            width: 1000,

            buttons: {
                Ok: function () {

                    if (fncCreatePRReqXmlForToAddGrid() == true) {
                        $(this).dialog("close");
                        $('#<%=btnPRReq.ClientID%>').click();
                    }
                }
            }
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncOpenPurchaseRequestDet() {
    try {
        if ($('#<%=chkPurchaseReq.ClientID%>').is(':checked')) {
            if ($("#PRRquest tbody").children().length > 0) {
                fncOpenPRRDailog();
            }
            else {
                fncGetPurchaseReturnRequestDet();
                fncOpenPRRDailog();
            }
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncCreatePRReqXmlForToAddGrid() {
    var obj, xml;
    var status = false;
    try {
        xml = '<NewDataSet>';
        $("#PRRquest tr").each(function () {
            if (!this.rowIndex) return;
            obj = $(this);
            if (obj.find('td input[id*="cbPRReq"]').is(':checked')) {
                status = true;
                xml += "<Table>";
                xml += "<RowNo>1</RowNo>";
                xml += "<Inventorycode>" + obj.find('td[id*=tdCode]').text().trim() + "</Inventorycode>";
                xml += "<Description>" + obj.find('td[id*=tdDescription]').text().trim() + "</Description>";
                xml += "<WQty>0</WQty>";
                xml += "<LQty>" + obj.find('td[id*=tdLQty]').text().trim() + "</LQty>";
                xml += "<Foc>" + obj.find('td[id*=tdFOC]').text().trim() + "</Foc>";
                xml += "<MRP>" + obj.find('td[id*=tdMRP]').text().trim() + "</MRP>";
                xml += "<Selling>" + obj.find('td[id*=tdSelling]').text().trim() + "</Selling>";
                xml += "<Cost>" + obj.find('td[id*=tdCost]').text().trim() + "</Cost>";
                xml += "<DiscAmt>" + obj.find('td[id*=tdDiscAmt]').text().trim() + "</DiscAmt>";
                xml += "<DiscPRC>" + obj.find('td[id*=tdDiscPrc]').text().trim() + "</DiscPRC>";
                xml += "<ODiscAmt>0</ODiscAmt>";
                xml += "<DiscountBasicAmt>0</DiscountBasicAmt>";
                xml += "<Gross>" + obj.find('td[id*=tdGross]').text().trim() + "</Gross>";
                xml += "<IGSTPrc>" + obj.find('td[id*=tdITaxPer3]').text().trim() + "</IGSTPrc>";
                xml += "<SGSTPrc>" + obj.find('td[id*=tdITaxPer1]').text().trim() + "</SGSTPrc>";
                xml += "<CGSTPrc>" + obj.find('td[id*=tdITaxPer2]').text().trim() + "</CGSTPrc>";
                xml += "<CessPrc>" + obj.find('td[id*=tdITaxPer4]').text().trim() + "</CessPrc>";
                xml += "<Nett>" + obj.find('td[id*=tdNetCost]').text().trim() + "</Nett>";
                xml += "<Total>" + obj.find('td[id*=tdNetTotal]').text().trim() + "</Total>";
                xml += "<SGSTAmt>" + obj.find('td[id*=tdSGSTAmt]').text().trim() + "</SGSTAmt>";
                xml += "<CGSTAmt>" + obj.find('td[id*=tdCGSTAmt]').text().trim() + "</CGSTAmt>";
                xml += "<IGSTAmt>" + obj.find('td[id*=tdIGSTAmt]').text().trim() + "</IGSTAmt>";
                xml += "<CessAmt>" + obj.find('td[id*=tdCessAmt]').text().trim() + "</CessAmt>";
                xml += "<VendorBill>" + obj.find('td[id*=tdVendorBill]').text().trim() + " </VendorBill>";
                xml += "<GIDNo>" + obj.find('td[id*=tdGIDNo]').text().trim() + "</GIDNo>";
                xml += "<BatchNo>" + obj.find('td[id*=tdBatchNo]').text().trim() + "</BatchNo>";
                xml += "<Stock>" + obj.find('td[id*=tdStock]').text().trim() + "</Stock>";
                xml += "<SerialNo>" + obj.find('td[id*=tdSerialNo]').text().trim() + "</SerialNo>";
                xml += "<UOM>" + obj.find('td[id*=tdUOM]').text().trim() + "</UOM>";
                xml += "<PRReqNo>" + obj.find('td[id*=tdPRReqNo]').text().trim() + "</PRReqNo>";
                xml += "</Table>";
            }
        });

        if (status == false) {
            fncToastInformation("Select any one Record.");
            return status;
        }

        xml = xml + '</NewDataSet>'
        xml = escape(xml);
        $('#<%=hidPRReqXml.ClientID %>').val(xml);
        return status;
    }
    catch (err) {
        fncToastError(err.message);
    }
}
function fncprrlist() {
    var prrList;
    var prrlistarray = [10];
    var prrlistarray2 = [10];
    var obj = {};
    var vendor = [25];
    try {
        prrList = $('#<%=hidPrrList.ClientID %>').val();
                vendor = prrList.split('@');
                prrlistarray = prrList.split('|');
                tblStkBody = $("#PRRquest tbody");
                $('#<%=txtVendor.ClientID %>').val(vendor[0]);
                $('#<%=btnVendorchange.ClientID %>').click();
                if (prrlistarray.length > 0) {
                    for (var i = 0; i < prrlistarray.length; i++) {
                        prrlistarray2 = prrlistarray[i].split('@');
                        obj.vendorCode = prrlistarray2[0];
                        obj.preqno = prrlistarray2[1];
                        $.ajax({
                            url: "frmPurchaseReturn.aspx/fncPRRList",
                            data: JSON.stringify(obj),
                            type: "POST",
                            contentType: "application/json;charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                fncBindPRRListData(msg.d);
                            },
                            failure: function (msg) {
                                fncToastError(msg.d);
                            }

                        });
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncPRRListShow() {
            try {
                $('#<%=chkPurchaseReq.ClientID %>').prop("checked", true);
                fncOpenPRRDailog();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBindPRRListData(obj) {
            try {
                var PurReqBody, row;

                PurReqBody = $("#PRRquest tbody");
                // PurReqBody.children().remove();
                lastRowNo = parseFloat(lastRowNo) + 1;
                obj = $.parseJSON(obj);
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                        row = "<tr>"
                            + "<td id='tdRowNo_" + i + "' > " + lastRowNo + "</td>"
                            + "<td><input id='cbPRReq_" + i + "'style = 'margin-left:41% !important''' type='checkbox'  /></td>"
                             + "<td id='tdCode_" + i + "' > " + obj[i]["Inventorycode"] + "</td>"
                             + "<td id='tdDescription_" + i + "' > " + obj[i]["Description"] + "</td>"
                             + "<td id='tdLQty_" + i + "'style = 'text-align:right !important''' > " + obj[i]["PRReqQty"] + "</td>"
                             + "<td id='tdFOC_" + i + "'style = 'text-align:right !important''' > 0 </td>"
                             + "<td id='tdMRP_" + i + "'style = 'text-align:right !important''' > " + parseFloat(obj[i]["MRP"]).toFixed(2) + "</td>"
                             + "<td id='tdSelling_" + i + "'style = 'text-align:right !important''' > " + parseFloat(obj[i]["SellingPrice"]).toFixed(2) + "</td>"
                             + "<td id='tdCost_" + i + "'style = 'text-align:right !important''' > " + obj[i]["CurrPrice"] + "</td>"
                             + "<td id='tdDiscAmt_" + i + "'style = 'text-align:right !important''' >" + obj[i]["DiscountBasicAmt"] + "</td>"
                             + "<td id='tdDiscPrc_" + i + "'style = 'text-align:right !important''' >" + obj[i]["DiscountBasicPer"] + "</td>"
                             + "<td id='tdODiscAmt_" + i + "'style = 'text-align:right !important''' >0</td>"
                             + "<td id='tdDiscountBasicAmt_" + i + "'style = 'text-align:right !important''' >0</td>"
                             + "<td id='tdGross_" + i + "'style = 'text-align:right !important''' >0</td>"
                             + "<td id='tdITaxPer3_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer3"] + "</td>"
                             + "<td id='tdITaxPer1_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer1"] + "</td>"
                             + "<td id='tdITaxPer2_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer2"] + "</td>"
                             + "<td id='tdITaxPer4_" + i + "'style = 'text-align:right !important''' > " + obj[i]["ITaxPer4"] + "</td>"
                             + "<td id='tdNetCost_" + i + "'style = 'text-align:right !important''' > " + obj[i]["NetCost"] + "</td>"
                             + "<td id='tdNetTotal_" + i + "'style = 'text-align:right !important''' > " + (parseFloat(obj[i]["PRReqQty"]) * parseFloat(obj[i]["NetCost"])).toFixed(2) + "</td>"
                             + "<td id='tdSGSTAmt_" + i + "'style = 'text-align:right !important''' > " + (parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 / 2).toFixed(2) + " </td>"
                             + "<td id='tdCGSTAmt_" + i + "'style = 'text-align:right !important''' >" + (parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 / 2).toFixed(2) + "</td>"
                             + "<td id='tdIGSTAmt_" + i + "'style = 'text-align:right !important''' >" + parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer3"]) / 100 + "</td>"
                             + "<td id='tdCessAmt_" + i + "'style = 'text-align:right !important''' >" + parseFloat(obj[i]["CurrPrice"]) * parseFloat(obj[i]["ITaxPer4"]) + "</td>"
                             + "<td id='tdVendorBill_" + i + "' ></td>"
                             + "<td id='tdGIDNo_" + i + "' ></td>"
                             + "<td id='tdBatchNo_" + i + "' >" + obj[i]["BatchNo"] + "</td>"
                             + "<td id='tdStock_" + i + "' >0</td>"
                             + "<td id='tdSerialNo_" + i + "' >0</td>"
                             + "<td id='tdUOM_" + i + "' >" + obj[i]["UOM"] + "</td>"
                             + "<td id='tdPRReqNo_" + i + "' >" + obj[i]["PRReqNo"] + "</td>"
                             + "</tr>";
                        PurReqBody.append(row);
                    }

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncInitializeAlreadyExist() {
            try {
                $("#divExistingItemadded").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true,
                    title: "Item Exists Alert",
                    appendTo: 'form:first',
                    closeOnEscape: false,
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncAlreadyexixtsno() {
            try {
                $("#divExistingItemadded").dialog("destroy");
                status = false;
                $('#<%=txtItemCodeAdd.ClientID%>').select();
                clearForm();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncAlreadyexixtsyes() {
            try {

                $('#<%=txtStock.ClientID%>').val('0.00');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                $("#divExistingItemadded").dialog("destroy");


            }

            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCodeAdd.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemNameAdd.ClientID %>').val($.trim(Description));
                    fncGetItemDetail();
                    return false;
                }
                $('#<%=btnVendorchange.ClientID %>').click();
                $('#<%=txtItemCodeAdd.ClientID %>').removeAttr("disabled");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(function () {

            $("#<%=txtFOC.ClientID %>").keypress(function () {
                alert("Wow; Its Work!.")

            });

        }); 


        function showAll() {

            $("#dialog-Batch").dialog({
                dialogClass: "no-close",                                                              //musaraf 19112022
                title: "Enterpriser Web",
                width: 400,
                modal: true,
                buttons: {
                    "OK": function () {

                        $("#<%=btnsave.ClientID %>").click();
                    }
                    //Cancel: function () {
                    //    $(this).dialog("close");
                    //}
                }
            }).html("Purchase Return Saved Successfully..!")
        }

  


        function fncShowSearchDialogInventory(event, Inventory,  txtItemCodeAdd, txtWQty) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;  
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemCodeAdd, txtWQty);
            }
            else if (charCode == 13) {
                fncGetItemDetail();
                return false;
            }
        } 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Purchase/frmPurchaseReturnView.aspx">View Purchase Return</a></li>
                <li class="active-page" id="lblPr">PURCHASE RETURN</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <asp:UpdatePanel ID="updtPnlTop" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div class="top-purchase-container">
                        <div class="top-purchase-left-container-pr">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="PR NO"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPRNO" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Vendor Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlVendorCode" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorCode_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res"
                                            onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>

                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Location Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlLocationCode" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" ReadOnly="true" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Item Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemCode" Enabled="false" runat="server" oninput="fncItemSearch(this)" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-middle-container-pr">
                            <div class="control-group-split">
                                <div class="control-group-left" style="width: 100%">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Vendor Bill No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlVendorBillNo" runat="server" CssClass="form-control-res" onchange="fncGidNoandBillNoChange('bill');">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left" style="width: 100%">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text="GID No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlGIDNo" runat="server" CssClass="form-control-res" onchange="fncGidNoandBillNoChange('gid');">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="top-purchase-right-container-pr">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Return Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtReturnDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>

                                    <div class="label-left">
                                        <asp:Label ID="Label16" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right" style="padding-top: 3px;">
                                        <asp:DropDownList ID="ddlRemarks" runat="server" CssClass="form-control-res">
                                            <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                            <%-- <asp:ListItem Value="1">Rate Change</asp:ListItem>
                                            <asp:ListItem Value="2">Expiry</asp:ListItem>
                                            <asp:ListItem Value="3">Damage</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="control-group-right">
                                    <div class="label-right" style="width: 100%">
                                        <asp:TextBox ID="txtVendorAddresscur" Enabled="false" runat="server" CssClass="form-control-res multiline vendor-address"
                                            TextMode="MultiLine" Style="width: 100%; height: 52px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="PR_VendorItems">
                        <div class="PR_header">
                            <asp:Label ID="lblMode" runat="server" Style="font-weight: bold; color: Red" Text="Vendor Details"></asp:Label>
                        </div>
                        <div class="PR_header">
                            <asp:CheckBox ID="cbVendorItem" runat="server" Text="Vendor Items" Checked="true" />
                        </div>
                        <div class="PR_header">
                            <asp:CheckBox ID="chkPurchaseReq" onclick="fncOpenPurchaseRequestDet();" runat="server" Text="Purchase Request(F10)" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="divPRVenList" class="bottom-purchase-container">
                <%--<div style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left1">
                            &nbsp;&nbsp; <span style="font-weight: bold; color: Red">Vendor Details</span>
                        </div>
                        <div class="control-group-middle hiddencol">
                            <div class="label-left" style="width: 80%">
                                <asp:Label ID="Label17" runat="server" Text="Purchase Request(F10)"></asp:Label>
                            </div>
                            <div class="label-right" style="width: 20%">
                                <asp:CheckBox ID="chkPurchaseReq" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>--%>
                <asp:UpdatePanel ID="updtPnlGrid1" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="over_flowhorizontal">
                            <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header purchaseReturn">
                                <tr id ="trheader">
                                    <th>S.No</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>WQty</th>
                                    <th>Qty</th>
                                    <th>Stock</th>
                                    <th>PRR Qty</th>
                                    <th>MRP</th>
                                    <th>Selling</th>
                                    <th>Cost</th>
                                    <th>Pur-Date</th>
                                    <th>Pur Qty</th>
                                    <th>Sold Qty</th>
                                    <th>Pr.Price</th>
                                    <th>DiscAmt</th>
                                    <th>BatchNo</th>
                                    <th>SGST</th>
                                    <th>CGST</th>
                                    <th>IGST</th>
                                    <th>CessPerc</th>
                                    <th>UOM</th>
                                    <th>UomConv</th>
                                    <th>DisPer</th>
                                    <th>SchemeDisPer</th>
                                    <th>SD</th>
                                </tr>
                            </table>
                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 230px; width: 1708px;">
                                <asp:GridView ID="gvVendorItem" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                    CssClass="pshro_GridDgn purchaseReturn"
                                    DataKeyNames="Inventorycode" OnRowDataBound="gvVendorItem_OnRowDataBound">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                        <asp:BoundField DataField="Inventorycode" HeaderText="Code"></asp:BoundField>
                                        <asp:BoundField DataField="Description" HeaderText="Name"></asp:BoundField>
                                        <asp:BoundField DataField="Compatible" HeaderText="Size"></asp:BoundField>
                                        <asp:TemplateField HeaderText="W.Qty">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWQty" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'WQty');"
                                                    CssClass="grid-textbox_right" onkeypress="isNumberKeyWithDecimalNew(event);" onchange="fncWQtyChange(this);" Text="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="L.Qty">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtLQty" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'venItem');"
                                                    CssClass="grid-textbox_right" onkeypress="isNumberKeyWithDecimalNew(event);" onchange="fncRepeaterColumnValueChange('NewQty')" Text="0"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Qtyonhand" HeaderText="Stock" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="PRReqQty" HeaderText="PRR" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="CurrPrice" HeaderText="Cost" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField NullDisplayText="" HeaderText="Pur-Date"></asp:BoundField>
                                        <asp:BoundField NullDisplayText="" HeaderText="Pur Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField NullDisplayText="" HeaderText="Sold Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField NullDisplayText="" HeaderText="Pr.Price"></asp:BoundField>
                                        <asp:BoundField DataField="DiscountBasicAmt" HeaderText="DiscAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                        <asp:BoundField DataField="ITaxPer1" HeaderText="SGSTPerc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="ITaxPer2" HeaderText="SGSTPerc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="ITaxPer3" HeaderText="SGSTPerc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="ITaxPer4" HeaderText="CessPerc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="UOM" HeaderText="UOM"></asp:BoundField>
                                        <asp:BoundField DataField="UomConv" HeaderText="UomConv" ItemStyle-CssClass="text-right"></asp:BoundField>
                                        <asp:BoundField DataField="DiscountBasicPer" HeaderText="DisPer" ItemStyle-CssClass="text-right display_none"></asp:BoundField>
                                         <asp:BoundField DataField="SchemeDiscPerc" HeaderText="SchemeDiscPerc" ItemStyle-CssClass="text-right display_none"></asp:BoundField> 
                                        <asp:BoundField DataField="SD" HeaderText="SD" ItemStyle-CssClass="text-right display_none"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <asp:Button ID="btnItem" runat="server" Text="Button" CssClass="hiddencol" OnClick="btnItem_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:UpdatePanel ID="updtPnlGrid2" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div id="dialog-Batch" title="Batch Item" style="display: none">
                        <div class="grid-popup">
                            <asp:GridView ID="gvBatchItem" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                CssClass="pshro_GridDgn"
                                DataKeyNames="BatchNo">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:CommandField ShowSelectButton="true" ButtonType="Button" SelectText="Select" />
                                    <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="SellingPrice" HeaderText="S.Price"></asp:BoundField>
                                    <asp:BoundField DataField="InwardQty" HeaderText="Stock"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="bottom-purchase-container">
                <div id="divPRAddList" class="display_none">
                    <%--<div style="width: 50%">
                        <div class="control-group-split">
                            <div class="control-group-left1">
                                &nbsp;&nbsp; <span style="font-weight: bold; color: Red">Purchase Return Details</span>
                            </div>
                        </div>

                    </div>--%>
                    <asp:UpdatePanel ID="updtPnlGrid3" UpdateMode="Conditional" runat="Server">
                        <ContentTemplate>
                            <div class="over_flowhorizontal">
                                <table rules="all" border="1" id="tblPurchaseReturnAdd" runat="server" class="fixed_header purchaseReturnAdd">
                                    <tr class="backClr_chocolate">
                                        <th>S.No</th>
                                        <th>Delete</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Size</th>
                                        <th>L.Qty</th>
                                        <th>Foc</th>
                                        <th>MRP</th>
                                        <th>Selling</th>
                                        <th>Cost</th>
                                        <th>Disc Amt</th>
                                        <th>Disc(%)</th>
                                        <th>O.Disc Amt</th>
                                        <th>O.Disc(%)</th>
                                        <th>Gross</th>
                                        <th>SGST</th>
                                        <th>CGST</th>
                                        <th>IGST</th>
                                        <th>CessPerc</th>
                                        <th>Nett</th>
                                        <th>Total</th>
                                        <th>SGSTAmt</th>
                                        <th>CGSTAmt</th>
                                        <th>IGSTAmt</th>
                                        <th>CessAmt</th>
                                        <th>Vendor Bill</th>
                                        <th>GID No</th>
                                        <th>Batch No</th>
                                        <th>Stock</th>
                                        <th>Serial No</th>
                                        <th>UOM</th>
                                        <th>PRReqNo</th>
                                    </tr>
                                </table>
                                <div class="GridDetails " style="overflow-x: hidden; overflow-y: scroll; height: 230px; width: 2487px;">
                                    <asp:GridView ID="gvPurRetn" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                        ShowHeader="false" CssClass="pshro_GridDgn purchaseReturnAdd">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png" OnClick="delete_click"
                                                        ToolTip="Click here to Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Inventorycode" HeaderText="Code" ItemStyle-CssClass="cssItemcode"></asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Name"></asp:BoundField>
                                            <asp:BoundField NullDisplayText="" HeaderText="Size"></asp:BoundField>
                                            <asp:BoundField DataField="LQty" HeaderText="L.Qty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Foc" HeaderText="Foc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Selling" HeaderText="Selling" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Cost" HeaderText="Cost" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="DiscAmt" HeaderText="Disc Amt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="DiscPRC" HeaderText="Disc(%)" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="ODiscAmt" HeaderText="O.Disc Amt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="DiscountBasicAmt" HeaderText="O.Disc(%)" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Gross" HeaderText="Gross" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="SGSTPrc" HeaderText="SGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="CGSTPrc" HeaderText="CGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="IGSTPrc" HeaderText="IGST" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="CessPrc" HeaderText="CessPrc" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Nett" HeaderText="Nett" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="SGSTAmt" HeaderText="SGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="CGSTAmt" HeaderText="CGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="IGSTAmt" HeaderText="IGSTAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="CessAmt" HeaderText="CessAmt" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="VendorBill" HeaderText="Vendor Bill"></asp:BoundField>
                                            <asp:BoundField DataField="GIDNo" HeaderText="GID No"></asp:BoundField>
                                            <asp:BoundField DataField="BatchNo" HeaderText="Batch No" ItemStyle-CssClass="cssBatchno"></asp:BoundField>
                                            <asp:BoundField DataField="Stock" HeaderText="Stock"></asp:BoundField>
                                            <asp:BoundField DataField="SerialNo" HeaderText="Serial No"></asp:BoundField>
                                            <asp:BoundField DataField="WQty" HeaderText="WQty" ItemStyle-CssClass="text-right"></asp:BoundField>
                                            <asp:BoundField DataField="UOM" HeaderText="UOM"></asp:BoundField>
                                            <asp:BoundField DataField="PRReqNo" HeaderText="PRReqNo"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="updtPnlButton1" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="bottom-purchase-container-add" id="addInv">
                            <div class="bottom-purchase-container-add-Text">
                                <div class="control-group-split">
                                    <div class="container7">
                                        <div class="label-left">
                                            <asp:Label ID="Label61" runat="server" Text="Item Code"></asp:Label>
                                        </div>
                                        <div class="float_left">
                                            <asp:TextBox ID="txtItemCodeAdd" runat="server" CssClass="form-control-res" Font-Bold="true" Enabled="false" onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemCodeAdd', 'txtWQty');"></asp:TextBox>
                                            <%--onkeydown="return fncGetInventorycodePR(event);"--%>
                                        </div>
                                    </div>
                                    <div class="PP_container">
                                        <div class="label-left">
                                            <asp:Label ID="Label71" runat="server" Text="ItemDesc"></asp:Label>
                                        </div>
                                        <div class="float_left" style="width: 100%">
                                            <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label141" runat="server" Text="W.Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWQty" onfocus="this.select();" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label216" runat="server" Text="L.Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtLQty" onfocus="this.select();" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true" onchange="fncLQtyChange('Qty');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label127" runat="server" Text="FOC"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFOC" onfocus="this.select();" runat="server" CssClass="form-control-res-right" Text="0"
                                                Font-Bold="true"></asp:TextBox>
                                            <%-- onkeyup ="return fncKeyPress(event,'FQty')"--%>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label151" runat="server" Text="N.Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtNQty" onfocus="this.select();" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label161" runat="server" Text="MRP"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMrp" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label171" runat="server" Text="BasicCost"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBasicCost" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true" onchange="fncLQtyChange('BasicCost');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label181" runat="server" Text="Dis(%)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDisc" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label191" runat="server" Text="Dis Amt"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDisAmt" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label201" runat="server" Text="S.D(%)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSD" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label211" runat="server" Text="SD Amt"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSdAmt" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label221" runat="server" Text="G.Cost"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtGrossCost" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%-- <div class="control-group-left" style="width: 5%">
                                    <div class="label-left" style="width: 100%">
                                        <asp:Label ID="Label111" runat="server" Text="Vat(%)"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                        <asp:TextBox ID="txtVat" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>--%>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label213" runat="server" Text="Nett"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtNet" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label218" runat="server" Text="Sell Price"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="container2">
                                        <div class="label-left">
                                            <asp:Label ID="Label241" runat="server" Text="Net Total"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtNetTotal" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%">
                                <div class="bottom-purchase-container-blns">
                                    <div class="control-group-split">
                                        <asp:Panel ID="pnlCGST" runat="server">
                                            <div class="container2">
                                                <div class="label-left">
                                                    <asp:Label ID="Label5" runat="server" Text="SGST"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtSGST" runat="server" CssClass="form-control-res-right" Text="0" onkeypress="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfSGSTCode" runat="server" />
                                                </div>
                                            </div>
                                            <div class="container2">
                                                <div class="label-left">
                                                    <asp:Label ID="Label36" runat="server" Text="CGST"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtCGST" runat="server" CssClass="form-control-res-right" Text="0" onkeypress="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfCGSTCode" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlIGST" runat="server">
                                            <div class="container2">
                                                <div class="label-left">
                                                    <asp:Label ID="Label9" runat="server" Text="IGST"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtIGST" runat="server" CssClass="form-control-res" onkeypress="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                    <asp:HiddenField ID="hdfIGSTCode" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label131" runat="server" Text="W.Uom"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtWUOM" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                <%-- <asp:DropDownList ID="ddlWUom" runat="server" CssClass="form-control-res" Font-Bold="true">
                                                </asp:DropDownList>--%>
                                            </div>
                                        </div>
                                        <div class="container2">
                                            <div class="label-left">
                                                <asp:Label ID="Label215" runat="server" Text="L.Uom"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtLUOM" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                <%-- <asp:DropDownList ID="ddlLUOM" runat="server" CssClass="form-control-res" Font-Bold="true">
                                                </asp:DropDownList>--%>
                                            </div>
                                        </div>
                                        <%-- <div class="container2" >
                                            <div class="label-left" >
                                                <asp:Label ID="Label216" runat="server" Text="L.Qty"></asp:Label>
                                            </div>
                                            <div class="label-right" >
                                                <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res" Font-Bold="true" onchange="fncLQtyChange();" ></asp:TextBox>
                                            </div>
                                        </div>--%>
                                        <%--      <div class="container2" >
                                            <div class="label-left" >
                                                <asp:Label ID="Label127" runat="server" Text="FOC"></asp:Label>
                                            </div>
                                            <div class="label-right" >
                                                <asp:TextBox ID="txtFOC" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                        <%-- <div class="container1" >
                                            <div class="label-left" >
                                                <asp:Label ID="Label218" runat="server" Text="Selling Price"></asp:Label>
                                            </div>
                                            <div class="label-right" >
                                                <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                        <div class="container1">
                                            <div class="label-left">
                                                <asp:Label ID="Label2" runat="server" Text="Batch No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdfTaxType" runat="server" />
                                <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" OnClick="lnkAdd_Click"
                                            OnClientClick="return fncAddValidation()">Add</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm();return false;">Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-purchase-container">
                            <div style="float: left; width: 50%">
                                <div class="control-group-split">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Total Items"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTotalItems" runat="server" CssClass="form-control-res-right" Text="0" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-middle display_none">
                                        <div class="label-left">
                                            <asp:Label ID="Label93" runat="server" Text="Stock"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtStock" runat="server" CssClass="form-control-res-right" Text="0.00" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label94" runat="server" Text="PR Total"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPRTotal" runat="server" CssClass="form-control-res-right" Text="0.00" Font-Bold="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hdfTotalBasicCost" runat="server" />
                                    <asp:HiddenField ID="hdfTotalDiscount" runat="server" />
                                    <asp:HiddenField ID="hdfTotalGrossCost" runat="server" />
                                    <asp:HiddenField ID="hdfTotalTax" runat="server" />
                                    <asp:HiddenField ID="hidPrrList" runat="server" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="updtPnlButton2" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkItemCode" runat="server" class="button-blue" Text="ItemCode"
                            Visible="false"></asp:LinkButton>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return Validate()"
                                    OnClick="lnkSave_Click">Save(F4)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-red"
                                    OnClick="lnkClearAll_Click">Clear(F6)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkViewPR" runat="server" class="button-red" PostBackUrl="~/Purchase/frmPurchaseReturnView.aspx">PR List(F8)</asp:LinkButton>
                            </div>

                             <asp:Button ID="btnsave" runat="server" style="display:none"  OnClick="btn_save_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidItemcode" runat="server" />
        <asp:HiddenField ID="hidBatchNo" runat="server" />
        <asp:HiddenField ID="hisBatch" runat="server" />
        <asp:HiddenField ID="hidCurLocation" runat="server" Value="" />
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hidCessPer" runat="server" Value="0" />
        <asp:HiddenField ID="hidPRReqXml" runat="server" />
        <asp:Button ID="btnPRReq" runat="server" OnClick="btnPRReq_click" />

        <div id="divExistingItemadded">
            <div>
                <asp:Label ID="lblExistingItemadded" Text='This Item Is Already Exists. Do you want to add?' runat="server"></asp:Label>
            </div>
            <div class="paymentDialog_Center">
                <div class="PaymentDialog_yes">
                    <asp:LinkButton ID="lnkCDYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                        OnClientClick="fncAlreadyexixtsyes();return false;"> </asp:LinkButton>
                </div>
                <div class="PaymentDialog_No">
                    <asp:LinkButton ID="lnkCDNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                        OnClientClick="fncAlreadyexixtsno();return false;"> </asp:LinkButton>
                </div>
            </div>
        </div>

        <asp:UpdatePanel ID="upVendorChange" runat="Server">
            <ContentTemplate>
                <asp:HiddenField ID="hidPRAddItemVis" runat="server" Value="0" />
                <asp:HiddenField ID="hdVItemCode" runat="server" Value="" />
                <asp:Button ID="btnVendorchange" runat="server" OnClick="btnVendorchange_click" />
                <asp:Button ID="btnAddGrid" runat="server" OnClick="btnAddGrid_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="display_none">
        <div title="Purchase Return Request " id="divPPRquest" class="Payment_fixed_headers PRReq">
            <table id="PRRquest" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Select</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>LQty</th>
                        <th>FOC</th>
                        <th>MRP</th>
                        <th>Selling</th>
                        <th>Cost</th>
                        <th>DiscAmt</th>
                        <th>DiscPRC</th>
                        <th>ODiscAmt</th>
                        <th>DiscountBasicAmt</th>
                        <th>Gross</th>
                        <th>IGSTPrc</th>
                        <th>SGSTPrc</th>
                        <th>CGSTPrc</th>
                        <th>CessPrc</th>
                        <th>Net Cost</th>
                        <th>Total</th>
                        <th>SGSTAmt</th>
                        <th>CGSTAmt</th>
                        <th>IGSTAmt</th>
                        <th>CessAmt</th>
                        <th>VendorBill</th>
                        <th>GIDNo</th>
                        <th>BatchNo</th>
                        <th>Stock</th>
                        <th>SerialNo</th>
                        <th>UOM</th>
                        <th>PRReqNo</th>

                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hidBillvalue" runat="server" Value="A" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidVendorStatus" runat="server" />
</asp:Content>
