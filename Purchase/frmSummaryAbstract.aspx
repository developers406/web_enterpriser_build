﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSummaryAbstract.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmSummaryAbstract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6) {
            min-width: 70px;
            max-width: 70px;
            text-align: center;
        }

        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7) {
            min-width: 110px;
            max-width: 110px;
            text-align: center;
        }


        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8) {
            min-width: 200px;
            max-width: 200px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9) {
            min-width: 200px;
            max-width: 200px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10) {
            min-width: 200px;
            max-width: 200px;
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11) {
            min-width: 200px;
            max-width: 200px;
            text-align: left;
        }

        .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12) {
            min-width: 200px;
            max-width: 200px;
            text-align: right;
        }

        .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13) {
            display: none;
        }

        .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14) {
            display: none;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SummaryAbstract');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SummaryAbstract";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        shortcut.add("Ctrl+A", function () {
            $('#breadcrumbs_text').removeClass('lowercase');
            $('#breadcrumbs_text').addClass('uppercase');
            $("#<%=hidBillType.ClientID%>").val('A');
        });
        shortcut.add("Ctrl+B", function () {
            $('#breadcrumbs_text').removeClass('uppercase');
            $('#breadcrumbs_text').addClass('lowercase');
            $("#<%=hidBillType.ClientID%>").val('B');
        });
        var rowDeleteobj;
        function pageLoad() {
            $("#tblga tbody > tr").first().focus();
            fncRowClick($("#tblga tbody > tr").first());
            if ($("#<%= rbnClosed.ClientID %>").is(':checked')) {
                $('.Barcode_fixed_headers td:nth-child(2)').css("display", "none");
                $('.Barcode_fixed_headers th:nth-child(2)').css("display", "none");
                $('.Barcode_fixed_headers td:nth-child(3)').css("display", "none");
                $('.Barcode_fixed_headers th:nth-child(3)').css("display", "none");
                $('.Barcode_fixed_headers td:nth-child(5)').css("display", "none");
                $('.Barcode_fixed_headers th:nth-child(5)').css("display", "none");
                $('.Barcode_fixed_headers td:nth-child(6)').css("display", "none");
                $('.Barcode_fixed_headers th:nth-child(6)').css("display", "none");
                $('.Barcode_fixed_headers td:nth-child(7)').css("display", "none");
                $('.Barcode_fixed_headers th:nth-child(7)').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
        }

        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
            $("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
        });

        function fncReportValidation() {
            try {
                if ($("#<%=txtFromDate.ClientID%>").val() == "") {
                    ShowPopupMessageBox("Please Enter From Date");
                    return false;
                }
                else if ($("#<%=txtToDate.ClientID%>").val() == "") {
                    ShowPopupMessageBox("Please Enter To Date");
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGARowKeydown(evt, source) {
            var rowobj, charCode, scrollheight;
            try {
                rowobj = $(source);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    return false;
                }
                else if (charCode == 113) {
                    fncGADeleteAndEditNew(rowobj, "Edit");
                }
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    fncRowClick(NextRowobj)
                    if (NextRowobj.length > 0) {
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblga tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td span[id*="lblSNo"]').text()) < 3) {
                                $("#tblga tbody").scrollTop(0);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncRowClick(prevrowobj);
                    if (prevrowobj.length > 0) {
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblga tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblga tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get value for GA and Abs Report
        function fncRowClick(source) {
            try {
                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "aliceblue");
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncABDeleteAndEdit(source, btnvalue) {
            try {

                if (btnvalue == "Delete") {
                    if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                        ShowPopupMessageBox("You have no permission to Delete this Abstract");
                        return false;
                    }
                }
                else if (btnvalue == "Edit") {
                    if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                        ShowPopupMessageBox("You have no permission to Edit this Abstract");
                        return false;
                    }
                }
                else if (btnvalue == "Go") {
                    if ($('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                        ShowPopupMessageBox("You have no permission to Pay this Abstract");
                        return false;
                    }
                }
                else if (btnvalue == "RTGS") {
                    if ($('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                        ShowPopupMessageBox("You have no permission to Pay this Abstract");
                        return false;
                    }
                }

    rowDeleteobj = $(source).parent().parent();
    fncAbDeleteAndEditNew(rowDeleteobj, btnvalue);
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncAbDeleteAndEditNew(source, btnvalue) {
    var gidNo;
    var obj = {};
    try {
        rowDeleteobj = source;
        var Code = $.trim(rowDeleteobj.find('td span[id*="lblAbstractNo"]').text());
        var Bankcode =$.trim(rowDeleteobj.find('td:eq(6)').find('option:selected').text());
        if (btnvalue == "Delete") {
            $("#<%=hidGid.ClientID %>").val(Code);
            fncShowSaveConfirmation_master("Do you want Delete this Abstract " + Code + " ?.");
            return false;
        }
        else if (btnvalue == "Edit") {
            var url = "../Purchase/frmCreateAbstract.aspx?AbsNo=" + Code;
            $(location).attr('href', url);
        }
        else if (btnvalue == "Go") {
            var url = "../Purchase/frmAbstractPayment.aspx?AbsNo=" + Code;
            $(location).attr('href', url);
        }
        else if (btnvalue == "Print") {
            $('#<%=hidAbsNo.ClientID %>').val(Code);
            $('#<%=btnPrint.ClientID %>').click();
        }
        else if (btnvalue == "RTGS") {
            if (Bankcode == "Please select" || Bankcode == "") {
                ShowPopupMessageBox("Plese Enter Bank Code");
                return false;
            } else {
                $('#<%=hidBankCode.ClientID %>').val(Bankcode);
                $('#<%=hidAbsNo.ClientID %>').val(Code);
                $('#<%=btnExcelExport.ClientID %>').click();
            }
        }
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
function fncYesClick_master() {
    try {
        $("#saveConfirmation_master").dialog("destroy");
        fncAbsDelete($("#<%=hidGid.ClientID %>").val());
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncAbsDelete(gidNo) {
    try {
        $.ajax({
            type: "POST",
            url: "frmSummaryAbstract.aspx/fncAbsDelete",
            data: "{ 'GidNo': '" + gidNo + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d == "AlreayPaid") {
                    ShowPopupMessageBox("This Abstract already Paid.You cannot Delete this Abstract.");
                }
                else if (msg.d == "Success") {
                    fncConfirmAbsDelete('Yes');
                }
                else {
                    ShowPopupMessageBox(msg.d);
                }

            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
                return false;
            }
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

function fncConfirmAbsDelete(value) {
    try {
        if (value == "Yes") {

            var rowParentObj = rowDeleteobj.parent();
            rowDeleteobj.remove();
            rowParentObj.children().each(function (index) {
                $(this).find('td span[id*=lblSNo]').html(index + 1);
            });
            rowDeleteobj = null;
            ShowPopupMessageBox("Deleted Successfully.");
            return false;
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="main-container">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Abstract</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page uppercase" id="breadcrumbs_text">Summary Abstract
                    </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
            <div class="container-group-full">
                <div class="purchase-order-header barcodepahe_color">
                </div>
                <div class="control-group-split">
                    <asp:UpdatePanel ID="upAbsview" runat="server">
                        <ContentTemplate>
                            <div class="ga_view_dropdown">
                                <div class="ga_view_dropdown_split">
                                    <div class="ga_label">
                                        <asp:Label ID="lblvendor" runat="server" Text="Abstract No"></asp:Label>
                                    </div>
                                    <div class="ga_dropdown">
                                        <asp:TextBox ID="txtAbstractNo" runat="server" CssClass="full_width"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-container" id="divdate">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="control-button">
                                                <asp:Label ID="lblfromdate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-button">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="control-button">
                                                <asp:Label ID="lbltodate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="control-button">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" OnClick="lnkRefresh_Click"
                                                    Text="Refresh" OnClientClick="return fncReportValidation()"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ga_view_radiobutton">
                                <div class="col-md-6">
                                    <div class="control-button inv_margin_border" style="height: 25px !important;">
                                        <asp:RadioButton ID="rbnPending" runat="server" Text="Pending" Visible="true" GroupName="Abspendingclosed" Checked="true" AutoPostBack="True" OnCheckedChanged="lnkRefresh_Click" />
                                        <asp:RadioButton ID="rbnClosed" runat="server" Text="Closed" OnCheckedChanged="lnkRefresh_Click"
                                            Visible="true" GroupName="Abspendingclosed" AutoPostBack="True" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div id="divrepeater" class="Barcode_fixed_headers">
                <asp:UpdatePanel ID="uprptrga" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblga" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col" runat="server" id="thDelete">Delete
                                    </th>
                                    <th scope="col" runat="server" id="thEdit">Edit  
                                    </th>
                                    <th scope="col" runat="server" id="th2">Print  
                                    </th>
                                    <th scope="col" runat="server" id="th1">Pay  
                                    </th>
                                    <th scope="col" runat="server" id="th3">RTGS  
                                    </th>
                                    <th scope="col" runat="server" id="th4">Bank  
                                    </th>
                                    <th scope="col">Abstract No
                                    </th>
                                    <th scope="col">Abstract Date
                                    </th>
                                    <th scope="col">No.of Bills
                                    </th>
                                    <th scope="col">Created User
                                    </th>
                                    <th scope="col">Total Value
                                    </th>
                                    <th scope="col">Location Code
                                    </th>
                                    <th scope="col">Bill Type
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrGA" runat="server" OnItemDataBound="OnItemDataBound">
                                <HeaderTemplate>
                                    <tbody id="BulkBarcodebody">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="gaRow" tabindex='<%#(Container.ItemIndex)%>' runat="server"
                                        onkeydown=" return fncGARowKeydown(event,this);" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                        </td>
                                        <td runat="server" id="tdDelete">
                                            <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/No.png"
                                                OnClientClick="fncABDeleteAndEdit(this,'Delete');return false;" />
                                        </td>
                                        <td runat="server" id="tdEdit">
                                            <asp:ImageButton ID="btnImgEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                OnClientClick="fncABDeleteAndEdit(this,'Edit')" />
                                        </td>
                                        <td runat="server" id="td1">
                                            <asp:ImageButton ID="ImgPrint" runat="server" ImageUrl="~/images/print1.png"
                                                OnClientClick="fncABDeleteAndEdit(this,'Print');return false;" />
                                        </td>
                                        <td runat="server" id="tdGrn">
                                            <asp:Button ID="btnGrn" runat="server" Text="Pay"
                                                OnClientClick="fncABDeleteAndEdit(this,'Go');return false;" />
                                        </td>
                                        <td runat="server" id="td2">
                                            <asp:Button ID="btnrtgs" runat="server" Text="RTGS"
                                                OnClientClick="fncABDeleteAndEdit(this,'RTGS');return false;" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlBank" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAbstractNo" runat="server" Text='<%# Eval("AbstractNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblAbDate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBillCount" runat="server" Text='<%# Eval("BillCount") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUser" runat="server" Text='<%# Eval("CreateUser") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("Total") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblLocationCode" runat="server" Text='<%# Eval("LocationCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBilltype" runat="server" Text='<%# Eval("BillType") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="hiddencol">
            <div id="deleteGA">
                <div>
                    <asp:Label ID="lblGaDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:UpdatePanel ID="updelete" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkbtnYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                                    OnClientClick="return fncConfirmDeleteGA()"> </asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnlbtnNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncCloseDeleteDialog();return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="GAEdit">
                <div>
                    <asp:Label ID="lbleditStatus" runat="server"></asp:Label>
                </div>
                <div class="dialog_center">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                        OnClientClick="fncCloseEditDialog();return false;"> </asp:LinkButton>
                </div>
            </div>
            <div id="divGidHold">
                <div>
                    <asp:Label ID="lblGidHold" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkgidHoldYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkgidHoldNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncConfirmToOpenHoldInvoice('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div id="divAbsDelete">
                <div>
                    <asp:Label ID="lblAbsDelete" runat="server"></asp:Label>
                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkAbsYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>' OnClientClick="fncConfirmAbsDelete('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkAbsNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>' OnClientClick="fncConfirmAbsDelete('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>

            <asp:UpdatePanel ID="upgidno" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btngidno" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:HiddenField ID="hidBillType" runat="server" Value="A" />

            <div id="dialog-DiscontinueEntry">
            </div>

            <asp:Button ID="btnPrint" runat="server" OnClick="lnkPrint_Click" />
            <div class="display_none">
                <asp:Button ID="btnExcelExport" runat="server" OnClick="lnkExport_Click" />
                <asp:HiddenField ID="hidSort" runat="server" />
            </div>
        </div>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="uprptrga">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidAbstractValue" runat="server" />
    <asp:HiddenField ID="hidCount" runat="server" />
    <asp:HiddenField ID="hidAbsNo" runat="server" />
    <asp:HiddenField ID="hidGid" runat="server" />
    <asp:HiddenField ID="hidBankCode" runat="server" />
</asp:Content>
