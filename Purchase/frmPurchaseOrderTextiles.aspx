﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseOrderTextiles.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseOrderTextiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .ReGeneratePO_Itemhistory td:nth-child(1), .ReGeneratePO_Itemhistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            display: none;
        }

        .ReGeneratePO_Itemhistory td:nth-child(2), .ReGeneratePO_Itemhistory th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(3), .ReGeneratePO_Itemhistory th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(3) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(4), .ReGeneratePO_Itemhistory th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(4) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(5), .ReGeneratePO_Itemhistory th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(5) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(6), .ReGeneratePO_Itemhistory th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(6) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(7), .ReGeneratePO_Itemhistory th:nth-child(7) {
            min-width: 60px;
            max-width: 60px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(7) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(8), .ReGeneratePO_Itemhistory th:nth-child(8) {
            min-width: 70px;
            max-width: 70px;
            display: none;
        }

        .ReGeneratePO_Itemhistory td:nth-child(8) {
            text-align: right !important;
        }

        .ReGeneratePO_Itemhistory td:nth-child(9), .ReGeneratePO_Itemhistory th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }

        .ReGeneratePO_Itemhistory td:nth-child(9) {
            text-align: right !important;
        }


        .POItemwisePending td:nth-child(1), .POItemwisePending th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            display: none;
        }

        .POItemwisePending td:nth-child(2), .POItemwisePending th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
        }

        .POItemwisePending td:nth-child(3), .POItemwisePending th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .POItemwisePending td:nth-child(4), .POItemwisePending th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
        }

        .POItemwisePending td:nth-child(5), .POItemwisePending th:nth-child(5) {
            min-width: 50px;
            max-width: 50px;
        }

        .POItemwisePending td:nth-child(5) {
            text-align: right !important;
        }

        .POItemwisePending td:nth-child(6), .POItemwisePending th:nth-child(6) {
            min-width: 65px;
            max-width: 65px;
        }

        .POItemwisePending td:nth-child(6) {
            text-align: right !important;
        }

        .lastFocDet td:nth-child(1), .lastFocDet th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            display: none;
        }

        .lastFocDet td:nth-child(2), .lastFocDet th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
        }

        .lastFocDet td:nth-child(3), .lastFocDet th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
        }

        .lastFocDet td:nth-child(4), .lastFocDet th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
        }

        .lastFocDet td:nth-child(5), .lastFocDet th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
        }

        .lastFocDet td:nth-child(5) {
            text-align: right !important;
        }

        .lastFocDet td:nth-child(6), .lastFocDet th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .lastFocDet td:nth-child(7), .lastFocDet th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .control-button {
            float: left;
            margin: 2px 2px;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'PurchaseOrderTextiles');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "PurchaseOrderTextiles";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        var BindCode = "";
        var BindDesc = "";
        var BindStock = "";
        var BindMRP = "";
        var BindSelling = "";
        var BindCost = "";
    </script>

    <script type="text/javascript">

        function pageLoad() {

            fncDecimal();

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });

            fncDisableSaleshistory();

            //================================> Initialize Dialog
            $("#dialog-Filteration").dialog({
                appendTo: 'form:first',
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "Search": function () {
                        if (BindFilterData()) {
                            $(this).dialog("close");
                        }
                    },
                    Clear: function () {
                        fncClearFilterValue();
                    },
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }

    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#<%= txtPurchaseDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtDeliveryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $(document).on('keydown', disableFunctionKeys);
        });

        //================================> Close Dialog
        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                window.parent.$('#dialog-Filteration').dialog('close');
            }
        });

        //================================> Clear Filter Value
        function fncClearFilterValue() {
            $('#<%=txtVendorFilter.ClientID %>').val('');
            $('#<%=txtDepartment.ClientID %>').val('');
            $('#<%=txtClass.ClientID %>').val('');
            $('#<%=txtCategory.ClientID %>').val('');
            $('#<%=txtSubCategory.ClientID %>').val('');
            $('#<%=txtBrand.ClientID %>').val('');
            $('#<%=txtFloor.ClientID %>').val('');
            $('#<%=txtShelf.ClientID %>').val('');
        }

        function SetAutoComplete() {
            //================================> Inventory Search
            $(".invcode").autocomplete({
                source: function (request, response) {                  
                    var obj = {};
                    obj.mode = 'PlaningInventory';
                    obj.searchData = request.term.trim();
                    obj.SearchOption = 'StartWith';

                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncCommonSearchAutoComplete")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1],
                                    desc: item.split('|')[2],
                                    Stock: item.split('|')[3],
                                    MRP: item.split('|')[4],
                                    Selling: item.split('|')[5],
                                    Cost: item.split('|')[6]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    BindCode.val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {                  
                    BindCode.val(i.item.val);
                    BindDesc.html(i.item.desc);
                    BindStock.html(i.item.Stock);
                    BindMRP.html(i.item.MRP);
                    BindSelling.html(i.item.Selling);
                    BindCost.html(i.item.Cost);
                    return false;
                },
                minLength: 1
            });
        }

        //================================> Decimal Value
        function fncDecimal() {
            try {
                $('#<%=txtMrp.ClientID%>').number(true, 2);
                $('#<%=txtBasicCost.ClientID%>').number(true, 2);
                $('#<%=txtGrossCost.ClientID%>').number(true, 2);
                $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
                $('#<%=txtNetTotalAdd.ClientID%>').number(true, 2);
                $('#<%=txtSGST.ClientID%>').number(true, 2);
                $('#<%=txtCGST.ClientID%>').number(true, 2);
                $('#<%=txtIGST.ClientID%>').number(true, 2);
                $('#<%=txtCESS.ClientID%>').number(true, 2);
                $('#<%=txtTaxableAmt.ClientID%>').number(true, 2);
                $('#<%=txtGST.ClientID%>').number(true, 2);
                $('#<%=txtSubTotal.ClientID%>').number(true, 2);
                $('#<%=txtDiscountPerc.ClientID%>').number(true, 2);
                $('#<%=txtDiscountAmt.ClientID%>').number(true, 2);
                $('#<%=txtNetTotal.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> After Common Search
        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=txtVendorDetails.ClientID %>').val(VendorAddress);
                }
                if (SearchTableName == "Inventory") {
                    fncGetInventoryDetail();
                }
                $('#<%=hdfFilterValue.ClientID %>').val(Code);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetInventoryDetail() {
            try {
                <%--if ($('#<%=txtVendor.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('Please Select Vendor Code');
                    $("[id$=txtItemCodeAdd]").val('');
                    return false;
                }--%>

                var obj = {};
                var jsonObj;

                obj.InvCode = $('#<%=txtItemCodeAdd.ClientID %>').val().trim();
                obj.VendorCode = $('#<%=txtVendor.ClientID %>').val().trim();

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Purchase/frmPurchaseOrder.aspx/GetAddInvDetail") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        jsonObj = JSON.parse(msg.d);

                        if (jsonObj.Table.length > 0) {
                            if (jsonObj.Table[0]["inventoryStatus"] == "0") {
                                fncToastInformation("Invalid Inventorycode");
                                fncClearEntervalue();
                                return false;
                            }
                        }
                        <%--if (jsonObj.Table1.length > 0) {
                            if (jsonObj.Table1[0]["vendorStatus"] == "0") {
                                fncToastInformation('<%=Resources.LabelCaption.alert_DifferenVendor%>');
                                fncClearEntervalue()
                                return false;
                            }
                        }--%>
                        if (jsonObj.Table2.length > 0) {
                            $('#<%=txtItemCodeAdd.ClientID %>').val(jsonObj.Table2[0]["InventoryCode"]);
                            $('#<%=txtItemNameAdd.ClientID %>').val(jsonObj.Table2[0]["Description"]);
                            $('#<%=txtMrp.ClientID %>').val(jsonObj.Table2[0]["MRP"]);
                            $('#<%=txtBasicCost.ClientID %>').val(jsonObj.Table2[0]["BasicCost"]);
                            $('#<%=txtGrossCost.ClientID %>').val(jsonObj.Table2[0]["GrossCost"]);
                            $('#<%=txtSellingPrice.ClientID %>').val(jsonObj.Table2[0]["NetSellingPrice"]);
                            $('#<%=txtSGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer1"]);
                            $('#<%=txtCGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer2"]);
                            $('#<%=txtIGST.ClientID %>').val(jsonObj.Table2[0]["ITaxPer3"]);
                            $('#<%=txtCESS.ClientID %>').val(jsonObj.Table2[0]["ITaxPer4"]);
                            $('#<%=txtNetTotalAdd.ClientID %>').val('0.00');
                        }

                        $('#<%=txtLQty.ClientID %>').focus().select();

                    },
                    error: function (data) {
                        fncToastError(data.message);
                    }
                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Clear Enter Value

        function fncClearEntervalue() {
            try {
                $('#<%=txtItemCodeAdd.ClientID %>').val("");
                $('#<%=txtItemNameAdd.ClientID %>').val("");
                $('#<%=txtLQty.ClientID %>').val("0");
                $('#<%=txtMrp.ClientID %>').val("0");
                $('#<%=txtBasicCost.ClientID %>').val("0");
                $('#<%=txtGrossCost.ClientID %>').val("0");
                $('#<%=txtNetTotalAdd.ClientID %>').val("0");
                setTimeout(function () {
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Load Purchase Plan Data 

        function fncLoadPurchasePlanData() {
            if ($('#<%=txtPlanName.ClientID%>').val() == 0) {
                fncToastError("Please Select Plan Name");
                return false;
            }
            BindData();
            return false;
        }

        function BindData() {
            try {

                $("#table-po tbody").empty();
                var obj = {};
                obj.PlanCode = $('#<%=txtPlanName.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: 'frmPurchaseOrderTextiles.aspx/fncGetLoadData',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {

                        var objdata = $.parseJSON(data.d);

                        if (objdata.Table.length == 0) {
                            fncToastInformation("No Records Found");
                            return;
                        }

                        if (objdata.Table.length > 0) {

                            for (var i = 0; i < objdata.Table.length; i++) {
                                var InvCode = objdata.Table[i]["InventoryCode"];
                                var objTable = '';
                                if (InvCode.indexOf('PP') == 0) {
                                    objTable = "<tr ondblclick='fncPoTabledblClick(this)'>"
                                            + "<td>" + objdata.Table[i]["SNo"] + "</td>"
                                            + "<td><input type='text' class='form-control-res border_nonewith_yellow invcode' value='" + objdata.Table[i]["InventoryCode"] + "' onfocus='return fncFocusPoTable(this);' onkeydown='return fncInvCodeKeyDown(this);'></td>"
                                            + "<td>" + objdata.Table[i]["Description"] + "</td>"
                                            + "<td>" + objdata.Table[i]["OrderQty"] + "</td>"
                                            + "<td><input type='text' class='form-control-res border_nonewith_yellow text-qty' value='0' onfocus='return fncFocusPoTable(this);' onkeydown='return fncEnterToNextRowPoTable(event, this);' onchange='fncPoQtyChange(this);'></td>"
                                            + "<td>" + objdata.Table[i]["BalanceQty"] + "</td>" // Bal Qty
                                            + "<td>" + objdata.Table[i]["QtyOnHand"] + "</td>"
                                            + "<td>" + objdata.Table[i]["MRP"] + "</td>"
                                            + "<td>" + objdata.Table[i]["SellingPrice"] + "</td>"
                                            + "<td>" + objdata.Table[i]["NetCost"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["BalanceQty"] + "</td>" // Bal Qty
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer1"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer2"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer4"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["VendorCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["DepartmentCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["Class"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["CategoryCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["subcategorycode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["BrandCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["FLOOR"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["Shelf"] + "</td>"
                                            + "</tr>";
                                }
                                else {
                                    objTable = "<tr ondblclick='fncPoTabledblClick(this)'>"
                                            + "<td>" + objdata.Table[i]["SNo"] + "</td>"
                                            + "<td>" + objdata.Table[i]["InventoryCode"] + "</td>"
                                            + "<td>" + objdata.Table[i]["Description"] + "</td>"
                                            + "<td>" + objdata.Table[i]["OrderQty"] + "</td>"
                                            + "<td><input type='text' class='form-control-res border_nonewith_yellow text-qty' value='0' onfocus='return fncFocusPoTable(this);' onkeydown='return fncEnterToNextRowPoTable(event, this);' onchange='fncPoQtyChange(this);'></td>"
                                            + "<td>" + objdata.Table[i]["BalanceQty"] + "</td>" // Bal Qty
                                            + "<td>" + objdata.Table[i]["QtyOnHand"] + "</td>"
                                            + "<td>" + objdata.Table[i]["MRP"] + "</td>"
                                            + "<td>" + objdata.Table[i]["SellingPrice"] + "</td>"
                                            + "<td>" + objdata.Table[i]["NetCost"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["BalanceQty"] + "</td>" // Bal Qty
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer1"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer2"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["ITaxPer4"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["VendorCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["DepartmentCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["Class"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["CategoryCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["subcategorycode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["BrandCode"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["FLOOR"] + "</td>"
                                            + "<td style='display:none'>" + objdata.Table[i]["Shelf"] + "</td>"
                                            + "</tr>";
                                }
                                $("#table-po tbody").append(objTable);
                            }

                            $(".text-qty").number(true, 3);
                            SetAutoComplete();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //================================> Filteration
        function BindFilterData() {
            try {
                var Filter = false;
                if ($('#<%=txtVendorFilter.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Vendor : ' + $('#<%=txtVendorFilter.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtDepartment.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Department : ' + $('#<%=txtDepartment.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtClass.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Class : ' + $('#<%=txtClass.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtCategory.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Category : ' + $('#<%=txtCategory.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtSubCategory.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('SubCategory : ' + $('#<%=txtSubCategory.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtBrand.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Brand : ' + $('#<%=txtBrand.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtFloor.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Floor : ' + $('#<%=txtFloor.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }
                if ($('#<%=txtShelf.ClientID %>').val() != "") {
                    if (Filter == false) {
                        $('#lblFilteration').html('Shelf : ' + $('#<%=txtShelf.ClientID %>').val());
                        Filter = true;
                    }
                    else {
                        fncToastError("Select any one filteration");
                        return false;
                    }
                }

                if (Filter == false) {
                    $('#lblFilteration').html('All');
                    $('#<%=hdfFilterValue.ClientID %>').val('');
                }

                var value = $('#<%=hdfFilterValue.ClientID %>').val().toLowerCase().trim();
                $("#table-po tbody tr").each(function (index) {
                    $(this).find("td").each(function () {
                        var id = $(this).text().toLowerCase().trim();
                        var not_found = (id.indexOf(value) == -1);
                        $(this).closest('tr').toggle(!not_found);
                        return not_found;
                    });
                });

                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //================================> Add Inventory After button Click

        function AddbuttonClick() {
            try {
                debugger;
                var bInvExists = false;

                //================================> Inventory If exists

                $("#table-po tbody tr").each(function () {
                    var TableInvCode = $(this).find("td").eq(1).html();
                    if (TableInvCode == $('#<%=txtItemCodeAdd.ClientID%>').val()) {

                        bInvExists = true;
                        //========> Update Qty

                        var Qty = parseFloat($('#<%=txtLQty.ClientID %>').val());

                        $(this).find("td").eq(4).children(":text").val(Qty);

                        fncToastError('Inventory Qty Updated');
                       
                        CalcTotal();
                        fncClearEntervalue();
                        return;
                    }
                });

                if (bInvExists == false) {
                    var InvCode = $('#<%=txtItemCodeAdd.ClientID %>').val();
                    if (InvCode == '') {
                        fncToastError('Select Inventory');
                        return false;;
                    }

                    var obj = {};
                    obj.InvCode = InvCode;
                    $.ajax({
                        type: "POST",
                        url: 'frmPurchaseOrderTextiles.aspx/fncGetAddData',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',

                        success: function (data) {
                            debugger;
                            var objdata = $.parseJSON(data.d);

                            var InvDesc = $('#<%=txtItemNameAdd.ClientID %>').val();
                            var Qty = $('#<%=txtLQty.ClientID %>').val();
                            var MRP = $('#<%=txtMrp.ClientID %>').val();
                            var SellingPrice = $('#<%=txtSellingPrice.ClientID %>').val();
                            var GrossCost = $('#<%=txtGrossCost.ClientID %>').val();
                            var ITaxPer1 = $('#<%=txtSGST.ClientID %>').val();
                            var ITaxPer2 = $('#<%=txtCGST.ClientID %>').val();
                            var ITaxPer4 = $('#<%=txtCESS.ClientID %>').val();

                            var RowCount = $('#table-po tbody tr').length + 1;

                            objTable = "<tr ondblclick='fncPoTabledblClick(this)'>"
                                                    + "<td>" + RowCount + "</td>"
                                                    + "<td>" + InvCode + "</td>"
                                                    + "<td>" + InvDesc + "</td>"
                                                    + "<td>" + 0 + "</td>"
                                                    + "<td><input type='text' class='form-control-res border_nonewith_yellow text-qty' value='" + Qty + "' onfocus='return fncFocusPoTable(this);' onkeydown='return fncEnterToNextRowPoTable(event, this);' onchange='fncPoQtyChange(this);'></td>"
                                                    + "<td>" + 0 + "</td>" // Bal Qty
                                                    + "<td>" + objdata.Table[0]["QtyOnHand"] + "</td>"
                                                    + "<td>" + MRP + "</td>"
                                                    + "<td>" + SellingPrice + "</td>"
                                                    + "<td>" + GrossCost + "</td>"
                                                    + "<td style='display:none'>" + 0 + "</td>" // Bal Qty
                                                    + "<td style='display:none'>" + ITaxPer1 + "</td>"
                                                    + "<td style='display:none'>" + ITaxPer2 + "</td>"
                                                    + "<td style='display:none'>" + ITaxPer4 + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["VendorCode"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["DepartmentCode"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["Class"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["CategoryCode"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["subcategorycode"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["BrandCode"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["FLOOR"] + "</td>"
                                                    + "<td style='display:none'>" + objdata.Table[0]["Shelf"] + "</td>"
                                                    + "</tr>";
                            $("#table-po tbody").append(objTable);

                            $("#table-po tbody").find("tr").last().find("td").eq(4).children(":text").focus().select();
                            CalcTotal();
                            fncClearEntervalue();
                            return false;
                        }
                    });
                }               
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        //================================> Table Row Column Set AutoComplete
        function fncInvCodeKeyDown(source) {
            var rowobj = $(source).parent().parent();
            BindCode = rowobj.find("td").eq(1).children(":text");
            BindDesc = rowobj.find("td").eq(2);
            BindStock = rowobj.find("td").eq(6);
            BindMRP = rowobj.find("td").eq(7);
            BindSelling = rowobj.find("td").eq(8);
            BindCost = rowobj.find("td").eq(9);
        }

        //================================> Focus in Table
        function fncFocusPoTable(source) {
            try {
                var rowobj = $(source).parent().parent();
                rowobj.css("background-color", "lightblue");
                rowobj.siblings().css("background-color", "white");
                rowobj.select().focus();
                $(source).select();

                var InvCode = rowobj.find("td").eq(1).html();
                var Desc = rowobj.find("td").eq(2).html();

                if ($('#divItemhistory').css('display') != 'none') {
                    fncBindSalesHistory(InvCode, Desc, "");
                }
                return false;
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        //================================> Enter Key, Down arrow, Up arrow to Focus
        function fncEnterToNextRowPoTable(evt, source) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var rowobj = $(source).parent().parent();
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    //For Check Display none Or Not
                    for (var i = NextRowobj.index() ; i < $('#table-po tbody tr').length; i++) {
                        if (NextRowobj.css("display") == "none") {
                            NextRowobj = NextRowobj.next();
                        }
                        else {
                            break;
                        }
                    }
                    if (NextRowobj.length > 0) {
                        NextRowobj.find("td").eq(4).children(":text").focus().select();
                        NextRowobj.css("background-color", "lightblue");  //Set Row Background Color
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();
                    }
                    return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    //For Check Display none Or Not
                    for (var i = prevrowobj.index() ; i < $('#table-po tbody tr').length; i--) {
                        if (prevrowobj.css("display") == "none") {
                            prevrowobj = prevrowobj.prev();
                        }
                        else {
                            break;
                        }
                    }
                    if (prevrowobj.length > 0) {
                        prevrowobj.find("td").eq(4).children(":text").focus().select();
                        prevrowobj.css("background-color", "lightblue");  //Set Row Background Color
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();
                    }
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        //================================> Click Double Click
        function fncPoTabledblClick(source) {
            try {
                var InvCode = $(source).find("td").eq(1).children(":text").val();
                if (InvCode != null && InvCode.indexOf('PP') == 0) {
                    fncOpenItemhistory("N");
                }
                else {
                    InvCode = $(source).find("td").eq(1).html();
                    fncOpenItemhistory(InvCode);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> PO Qty Change
        function fncPoQtyChange(source) {
            try {
                //Find Balance Qty
                var Qty = $(source).parent().parent().find("td").eq(4).children(":text").val();
                var PlanningBlnsQty = $(source).parent().parent().find("td").eq(10).html();
                var BalanceQty = parseFloat(PlanningBlnsQty) - parseFloat(Qty)
                $(source).parent().parent().find("td").eq(5).html(BalanceQty.toFixed(2));

                CalcTotal();
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        //================================> Calc Total
        function CalcTotal() {
            try {
                var TotalQty = 0
                var ItemsCount = 0;
                var SubTotal = 0;
                var GSTAmt = 0;

                $("#table-po tbody tr").each(function () {
                    var Qty = $(this).find("td").eq(4).children(":text").val();
                    var NetCost = $(this).find('td').eq(9).html();
                    var ITaxPerc1 = $(this).find('td').eq(10).html();
                    var ITaxPerc2 = $(this).find('td').eq(11).html();
                    var ITaxPerc4 = $(this).find('td').eq(12).html();

                    if (parseFloat(Qty) != 0) {

                        ItemsCount = ItemsCount + 1;

                        TotalQty = parseFloat(TotalQty) + parseFloat(Qty);
                        SubTotal = parseFloat(SubTotal) + +parseFloat(Qty * NetCost)

                        $('#<%=hdfTaxType.ClientID%>').val('S');
                        if ($('#<%=hdfTaxType.ClientID%>').val() == "S") {
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc1) / 100);
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc2) / 100);
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc4) / 100);
                        }
                        else if ($('#<%=hdfTaxType.ClientID%>').val() == "I") {
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc1) / 100);
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc4) / 100);
                        }
                        else if ($('#<%=hdfTaxType.ClientID%>').val() == "Import") {
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc1) / (100 + ITaxPerc1));
                            GSTAmt = GSTAmt + ((Qty * NetCost * ITaxPerc4) / 100);
                        }
                }
                });

                //============> Set Value 
            $('#<%=txtItems.ClientID%>').val(ItemsCount); // Total Item
                $('#<%=txtQty.ClientID%>').val(TotalQty); // Total Qty              
                $('#<%=txtTaxableAmt.ClientID%>').val(SubTotal); // Total Cost                       
                $('#<%=txtGST.ClientID%>').val(GSTAmt); // GST  

                var NetCost = parseFloat(SubTotal) + parseFloat(GSTAmt);
                $('#<%=txtSubTotal.ClientID%>').val(NetCost); // Sub Total Cost                        

                SubTotal = $('#<%=txtSubTotal.ClientID%>').val();
                DiscAmt = $('#<%=txtDiscountAmt.ClientID%>').val();

                $('#<%=txtNetTotal.ClientID%>').val(parseFloat(SubTotal) - parseFloat(DiscAmt));
            }
            catch (err) {
                fncToastError(err.message)
            }
        }

        //================================> Filteration Open Popup
        function fncOpenFliterPopup() {
            $("#dialog-Filteration").dialog("open");
        }

        //================================> Open Item History
        function fncOpenItemhistory(InvCode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            if (InvCode != "N") {
                page = page + "?InvCode=" + InvCode + "&Status=dailog";
            }
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 630,
                width: 1300,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        //================================> Open Image
        function fncOpenImageScreen() {
            var page = '<%=ResolveUrl("~/Purchase/frmPurchaseOrderImage.aspx") %>';
            var page = page + "?PoNo=" + $('#<%=txtPONo.ClientID%>').val()
            var $dialog = $('<div id="PopupPoImage" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                height: 500,
                width: 900,
                title: "Purchase Order Image",
                appendTo: 'form:first'
            });
            $dialog.dialog('open');
        }

        //================================> Calc Discount
        function fncDiscountAmtCal(value) {
            var TaxableAmt = 0, DiscPerc = 0, DiscAmt = 0; var NetAmt = 0;
            try {
                TaxableAmt = $('#<%=txtTaxableAmt.ClientID%>').val();
                DiscPerc = $('#<%=txtDiscountPerc.ClientID%>').val();
                DiscAmt = $('#<%=txtDiscountAmt.ClientID%>').val();
                if (value == "DisPrc") {
                    DiscAmt = (TaxableAmt * DiscPerc) / 100;
                }
                else {
                    DiscPerc = (DiscAmt * 100) / TaxableAmt;
                }
                NetAmt = parseFloat(TaxableAmt) - parseFloat(DiscAmt);
                $('#<%=txtDiscountAmt.ClientID%>').val(DiscAmt);
                $('#<%=txtDiscountPerc.ClientID%>').val(DiscPerc);
                $('#<%=txtNetTotal.ClientID%>').val(NetAmt);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Short cut key function Option
        function fncToEnableShortcutKey(value) {
            try {

                if (value == "Vendor") {
                    shortcutKeyvalue = "Vendor";
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        //================================> Set Function Key
        function disableFunctionKeys(e) {
            try {
                if (e.keyCode == 113) {
                    if (shortcutKeyvalue == "Vendor") {
                        page = '<%=ResolveUrl("~/Masters/frmVendorMaster.aspx") %>';
                        $dialog = $('<div id="popupVendor"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                            autoOpen: false,
                            modal: true,
                            height: 645,
                            width: 1300,
                            title: "Vendor"
                        });
                        $dialog.dialog('open');
                    }
                    e.preventDefault();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        //================================> Save Validate
        function ValidateSave() {
            if ($('#<%=txtVendor.ClientID%>').val() == '') {
                fncToastError("Please Select Vendor");
                return false;
            }
            if ($('#<%=ddlLocation.ClientID%>').val() == '') {
                fncToastError("Please Select Location");
                return false;
            }
            if ($('#<%=txtItems.ClientID%>').val() == 0) {
                fncToastError("No Records");
                return false;
            }
            var iSNo = 1;
            var TableXml = '<NewDataSet>';
            $("#table-po tbody tr").each(function () {
                var cells = $("td", this);
                var itemCode;
                if (cells.length > 0) {
                    var Qty = $(this).find("td").eq(4).children(":text").val();
                    var itemCode = $(this).find("td").eq(1).children(":text").val();
                    if (parseFloat(Qty) != 0) {
                        TableXml += "<Table>";
                        TableXml += "<SNo>" + iSNo + "</SNo>";
                        //if( TableXml += "<Code>" + cells.eq(1).find('input').val()+ "</Code>")
                        //    TableXml += "<Code>" + cells.eq(1).find('input').val().trim() + "</Code>";
                        //else
                        //    TableXml += "<Code>" + cells.eq(1).text().trim() + "</Code>";

                        if ($(this).find("td").eq(1).children(":text").val() === undefined) {
                             itemCode = cells.eq(1).text().trim();
                        } else {
                             itemCode = $(this).find("td").eq(1).children(":text").val();
                        }
                        TableXml += "<Code>" + itemCode + "</Code>";
                        TableXml += "<Description>" + cells.eq(2).text().trim().replace('<', '(').replace('>', ')') + "</Description>";
                        TableXml += "<Qty>" + cells.eq(4).find('input').val().trim() + "</Qty>";
                        TableXml += "<Cost>" + cells.eq(9).text().trim() + "</Cost>";
                        TableXml += "<Itaxper1>" + cells.eq(10).text().trim() + "</Itaxper1>";
                        TableXml += "<Itaxper2>" + cells.eq(11).text().trim() + "</Itaxper2>";
                        TableXml += "<Itaxper4>" + cells.eq(12).text().trim() + "</Itaxper4>";
                        TableXml += "</Table>";
                        iSNo = iSNo + 1;
                    }
                }
            });

            if (iSNo == 1) {
                fncToastError("No Records to Add");
                return false;
            }

            TableXml = TableXml + '</NewDataSet>'
            TableXml = escape(TableXml);
            $("#<%=hdfXmlSave.ClientID %>").val(TableXml);
        }

    </script>

    <script type="text/javascript">

        //================================> Disable Sales History
        function fncDisableSaleshistory() {
            try {
                $('#divItemhistory').css('display', 'none');
                $('#locWiseSaleshis').css('display', 'none');
                $('#divPoPending').css('display', 'none');
                $('#divLastFoc').css('display', 'none');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);

            }
        }

        //================================> Bind Sales history to table
        function fncBindSalesHistory(itemcode, desc, month) {
            var glitemhistorybody, DisContinueEntry = "";
            glitemhistorybody = $("#tblItemhistory tbody");
            var obj = {};
            try {
                $('#<%=lblcode.ClientID %>').text(itemcode);
                $('#<%=txtDesc.ClientID %>').val(desc);

                $('#<%=loc_Code.ClientID %>').text(itemcode);
                $('#<%=txtlocName.ClientID %>').val(desc);

                $('#<%=lblFOcItem.ClientID %>').text(itemcode);
                $('#<%=txtFocItem.ClientID %>').val(desc);

                $('#<%=lblPOpendingItem.ClientID %>').text(itemcode);
                $('#<%=txtPOPenCode.ClientID %>').val(desc);

                obj.itemcode = itemcode;
                obj.location = $('#<%=ddlLocation.ClientID %>').val();
                obj.month = month;
                obj.vendorcode = $('#<%=txtVendor.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: "frmPurchaseOrder.aspx/fncGetSalesHistory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (month == "") {
                            glitemhistorybody.children().remove();
                            var objItemhistory = jQuery.parseJSON(msg.d);
                            if (objItemhistory.Table.length > 0) {
                                for (var i = 0; i < objItemhistory.Table.length; i++) {
                                    glitemhistorybody.append("<tr>"
                                        + "<td>" + objItemhistory.Table[i]["RowNo"] + "</td>"
                                        + "<td id='tdMNName_" + i + "' >" + objItemhistory.Table[i]["MNName"] + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK1"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK2"].toFixed(2) + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK3"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK4"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK5"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK6"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Total"].toFixed(2) + "</td>"
                                        + "</tr>");
                                }
                                glitemhistorybody.children().dblclick(fncGetLocationwiseSaleshistory);

                                if ($('#divItemhistory').css('display') == 'block') {
                                    $('#locWiseSaleshis').css('display', 'none');
                                }
                            }
                        }
                        else {

                            var tblLocSalesHis;
                            tblLocSalesHis = $('#tblLocwiseSaleshistory tbody');
                            tblLocSalesHis.children().remove();
                            var objItemhistory = jQuery.parseJSON(msg.d);

                            if (objItemhistory.Table.length > 0) {
                                for (var i = 0; i < objItemhistory.Table.length; i++) {
                                    tblLocSalesHis.append("<tr>"
                                        + "<td>" + objItemhistory.Table[i]["RowNo"] + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Location"] + "</td>"
                                        + "<td id='tdMNName_" + i + "' >" + objItemhistory.Table[i]["MNName"] + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK1"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK2"].toFixed(2) + "</td>"
                                        + "<td >" + objItemhistory.Table[i]["WK3"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK4"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK5"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["WK6"].toFixed(2) + "</td>"
                                        + "<td>" + objItemhistory.Table[i]["Total"].toFixed(2) + "</td>"
                                        + "</tr>");
                                }
                                $('#divItemhistory').css('display', 'none');
                                $('#locWiseSaleshis').css('display', 'block');
                            }
                        }

                        if (objItemhistory.Table1.length > 0) {
                            fncShowItemwisePOPendings(objItemhistory.Table1);

                        }
                        else {
                            $('#divPoPending').css('display', 'none');
                            fncEnableItemPanel();
                        }

                        if (objItemhistory.Table2.length > 0) {
                            fncShowLastFOCDetail(objItemhistory.Table2);
                        }
                        else {
                            $('#tblLastFOC tbody').children().remove();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Get Location Wise Sales History
        function fncGetLocationwiseSaleshistory() {
            try {
                fncBindSalesHistory($('#<%=lblcode.ClientID %>').text(), $('#<%=txtDesc.ClientID %>').val(), $(this).find('td[id*=tdMNName]').text());
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Enable Item Panel
        function fncEnableItemPanel() {
            try {
                if ($('#divItemhistory').css('display') == 'none' && $('#locWiseSaleshis').css('display') == 'none' &&
                    $('#divLastFoc').css('display') == 'none' && $('#divPoPending').css('display') == 'none') {
                    $('#addInv').css('display', 'block');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Open Last FOC Detail
        function fncOpenLastFOCDet() {
            try {
                if ($('#divLastFoc').css('display') == 'none') {
                    $('#divLastFoc').css('display', 'block');
                    $('#calc-detail').css('display', 'none');
                    $('#<%=lnkLastFocDetail.ClientID%>').css("background-color", "#f44336");
                }
                else {
                    $('#divLastFoc').css('display', 'none');
                    $('#<%=lnkLastFocDetail.ClientID%>').css("background-color", "blueviolet");
                    fncEnableCalcDetail();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Show And Hide Salesh History
        function fncShowandHideSaleshistory() {
            try {
                if ($('#divItemhistory').css('display') == 'none') {
                    $('#divItemhistory').css('display', 'block');
                    $('#calc-detail').css('display', 'none');
                    $('#<%=lnkSaleshistory.ClientID%>').css("background-color", "#f44336");
                }
                else {
                    $('#divItemhistory').css('display', 'none');
                    $('#<%=lnkSaleshistory.ClientID%>').css("background-color", "blueviolet");
                    fncEnableCalcDetail();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //================================> Enable Calc Detail
        function fncEnableCalcDetail() {
            try {
                if ($('#divItemhistory').css('display') == 'none' && $('#divLastFoc').css('display') == 'none' && $('#divPoPending').css('display') == 'none') {
                    $('#calc-detail').css('display', 'block');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>

    <style type="text/css">
        .btn-img {
            background-color: green !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="updtPnlTop" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Purchase Order Textiles </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>

                <div class="container-group-full">

                    <div class="top-purchase-container top-padding" style="padding: 2px">
                        <div class="col-md-3">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Po No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPONo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');" onfocus="fncToEnableShortcutKey('Vendor');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Plan Name"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPlanName" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PurchasePlanData', 'txtPlanName', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="control-group-single-res">
                                <asp:TextBox ID="txtVendorDetails" runat="server" CssClass="form-control-res" TextMode="MultiLine"
                                    Style="height: 48px" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label3" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="control-group-single-res">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkLastFocDetail" runat="server" class="button-link" OnClientClick="fncOpenLastFOCDet();return false;">Last Foc Detail</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkPOPending" runat="server" class="button-link" Text="PO Pending" OnClientClick="return false;"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSaleshistory" runat="server" class="button-link" OnClientClick="fncShowandHideSaleshistory();return false;">Sales History</asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkImgOpen" runat="server" class="button-link btn-img" OnClientClick="fncOpenImageScreen();return false;">Insert Image</asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <span style="color: red; font-weight: bold">Filtered :- </span>
                                <label id="lblFilteration" style="color: green; font-weight: bold"></label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="Purchase Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Delivery Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div style="float: right">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkOpenFliterPopup" runat="server" class="button-red" OnClientClick="fncOpenFliterPopup(); return false;">Open Filter</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkLoad" runat="server" class="button-blue" OnClientClick="fncLoadPurchasePlanData(); return false;">Load</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="clearForm(); return false;">Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="po-table">
                        <table class="table-design" id="table-po" style="display: table">
                            <thead class="table-header table-po-header">
                                <tr>
                                    <th>S No</th>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>PlanningQty</th>
                                    <th>Qty</th>
                                    <th>Bal Qty</th>
                                    <th>Stock</th>
                                    <th>MRP</th>
                                    <th>Selling</th>
                                    <th>Cost</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdfFilterValue" runat="server" />
                        <asp:HiddenField ID="hdfXmlSave" runat="server" />
                    </div>

                    <div class="bottom-purchase-container-add gid_top_border">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="col-md-6" style="padding: 0px">
                                <div class="col-md-2" style="padding: 0px">
                                    <asp:Label ID="Label8" runat="server" Text="Item Code"></asp:Label>
                                    <asp:TextBox ID="txtItemCodeAdd" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCodeAdd', 'txtLQty');"></asp:TextBox>
                                    <asp:HiddenField ID="hfInvCode" runat="server" />
                                </div>
                                <div class="col-md-5" style="padding: 0px">
                                    <asp:Label ID="Label9" runat="server" Text="ItemDesc"></asp:Label>
                                    <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label26" runat="server" Text="L.Qty"></asp:Label>
                                    <asp:TextBox ID="txtLQty" runat="server" CssClass="form-control-res-right" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label16" runat="server" Text="MRP"></asp:Label>
                                    <asp:TextBox ID="txtMrp" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label17" runat="server" Text="B.Cost"></asp:Label>
                                    <asp:TextBox ID="txtBasicCost" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label22" runat="server" Text="G.Cost"></asp:Label>
                                    <asp:TextBox ID="txtGrossCost" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label28" runat="server" Text="Sel.Price"></asp:Label>
                                    <asp:TextBox ID="txtSellingPrice" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6" style="padding: 0px">
                                <div class="col-md-2" style="padding: 0px">
                                    <asp:Label ID="Label24" runat="server" Text="Net Total"></asp:Label>
                                    <asp:TextBox ID="txtNetTotalAdd" runat="server" Text="0.00" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label11" runat="server" Text="SGST(%)"></asp:Label>
                                    <asp:TextBox ID="txtSGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                    <asp:HiddenField ID="hdfSGSTCode" runat="server" />
                                </div>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label36" runat="server" Text="CGST(%)"></asp:Label>
                                    <asp:TextBox ID="txtCGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                    <asp:HiddenField ID="hdfCGSTCode" runat="server" />
                                </div>
                                <asp:Panel ID="pnlIGST" runat="server" Style="display: none;">
                                    <div class="col-md-1" style="padding: 0px">
                                        <asp:Label ID="Label10" runat="server" Text="IGST(%)"></asp:Label>
                                        <asp:TextBox ID="txtIGST" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        <asp:HiddenField ID="hdfIGSTCode" runat="server" />
                                    </div>
                                </asp:Panel>
                                <div class="col-md-1" style="padding: 0px">
                                    <asp:Label ID="Label37" runat="server" Text="CESS(%)"></asp:Label>
                                    <asp:TextBox ID="txtCESS" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-4" style="padding: 0px">
                                    <asp:Label ID="Label12" runat="server" Text="Remarks"></asp:Label>
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                                <div style="margin-top: 14px">
                                    <div class="col-md-1" style="padding: 0px">
                                        <asp:LinkButton ID="lnkAddInv" runat="server" class="button-blue" OnClientClick="AddbuttonClick(); return false;" Text="Add"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1" style="padding: 0px; margin-left: 15px">
                                        <asp:LinkButton ID="lnkClearValue" runat="server" Text="Clear" class="button-blue" OnClientClick="fncClearEntervalue();return false;"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 purchase-bottom-total" id="calc-detail">
                        <div class="col-md-1">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label29" runat="server" Text="Items"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItems" runat="server" Text="0" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label30" runat="server" Text="Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtQty" runat="server" Text="0" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblTaxableAmt" runat="server" Text="T.Amt"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtTaxableAmt" runat="server" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="GST"></asp:Label>
                                    <%-- <asp:LinkButton ID="lnkGST" runat="server" Text="GST(F11)" OnClientClick="fncInitializeVetDetailDialog();return false;"></asp:LinkButton>--%>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtGST" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label33" runat="server" Text="SubTotal"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtSubTotal" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label38" runat="server" Text="Discount(%)"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtDiscountPerc" onchange="fncDiscountAmtCal('DisPrc');" runat="server" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label39" runat="server" Text="Discount(Rs)"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtDiscountAmt" runat="server" onchange="fncDiscountAmtCal('DisAmt');" CssClass="form-control-res-right" Text="0.00"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label35" runat="server" Text="Net Total"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtNetTotal" runat="server" Text="0.00" CssClass="form-control-res-right" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdfTaxType" runat="server" />

                <div class="table-focus-detail">
                    <div class="float_left item-history">
                        <div id="divItemhistory">
                            <div class="display_table">
                                <div class="po_saleshistory">
                                    <asp:Label ID="lblcode" runat="server"></asp:Label>
                                </div>
                                <div class="po_saleshistory">
                                    <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control-res border_none" Style="width: 379px;"></asp:TextBox>
                                </div>
                            </div>
                            <div class="Payment_fixed_headers ReGeneratePO_Itemhistory PO_Itemhistory" style="width: 510px;">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Month
                                            </th>
                                            <th scope="col">1st Week
                                            </th>
                                            <th scope="col">2st Week
                                            </th>
                                            <th scope="col">3st Week
                                            </th>
                                            <th scope="col">4st Week
                                            </th>
                                            <th scope="col">5st Week
                                            </th>
                                            <th scope="col">6st Week
                                            </th>
                                            <th scope="col">Total
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div id="locWiseSaleshis">
                            <div class="display_table">
                                <div class="po_saleshistory">
                                    <asp:Label ID="loc_Code" runat="server"></asp:Label>
                                </div>
                                <div class="po_saleshistory">
                                    <asp:TextBox ID="txtlocName" runat="server" CssClass="form-control-res border_none"></asp:TextBox>
                                </div>
                            </div>
                            <div class="Payment_fixed_headers ReGeneratePO_Itemhistory PO_Itemhistory" style="width: 510px;">
                                <table id="tblLocwiseSaleshistory" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Location
                                            </th>
                                            <th scope="col">Month
                                            </th>
                                            <th scope="col">1st Week
                                            </th>
                                            <th scope="col">2st Week
                                            </th>
                                            <th scope="col">3st Week
                                            </th>
                                            <th scope="col">4st Week
                                            </th>
                                            <th scope="col">5st Week
                                            </th>
                                            <th scope="col">6st Week
                                            </th>
                                            <th scope="col">Total
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="divLastFoc" class="float_left">
                        <div class="display_table">
                            <div class="po_saleshistory">
                                <asp:Label ID="lblFOcItem" runat="server"></asp:Label>
                            </div>
                            <div class="po_saleshistory">
                                <asp:TextBox ID="txtFocItem" runat="server" CssClass="form-control-res border_none" Style="width: 341px;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Payment_fixed_headers lastFocDet PO_Itemhistory" style="width: 425px;">
                            <table id="tblLastFOC" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Itemcode
                                        </th>
                                        <th scope="col">Qty
                                        </th>
                                        <th scope="col">F.Itemcode
                                        </th>
                                        <th scope="col">F.Qty
                                        </th>
                                        <th scope="col">GID No
                                        </th>
                                        <th scope="col">GID Date
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div id="divPoPending" class="float_left">
                        <div class="display_table">
                            <div class="po_saleshistory">
                                <asp:Label ID="lblPOpendingItem" runat="server"></asp:Label>
                            </div>
                            <div class="po_saleshistory">
                                <asp:TextBox ID="txtPOPenCode" runat="server" CssClass="form-control-res border_none" Style="width: 270px;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Payment_fixed_headers POItemwisePending PO_Itemhistory" style="width: 404px;">
                            <table id="tblPOItemwisePending" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">PO No
                                        </th>
                                        <th scope="col">PO Date
                                        </th>
                                        <th scope="col">Item code
                                        </th>
                                        <th scope="col">PO Qty
                                        </th>
                                        <th scope="col">PO R.Qty
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="dialog-Filteration" title="Filteration">

                    <div class="container-filter">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label14" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendorFilter" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendorFilter', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label18" runat="server" Text="Class"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label19" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label20" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label21" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label23" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', '');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="control-container">
                    <div class="control-button" style="width: 500px">
                        <div style="float: left; width: 100px">
                            <asp:Label ID="Label25" runat="server" Text="Remarks"></asp:Label>
                        </div>
                        <div style="float: right; width: 400px">
                            <asp:TextBox ID="txtPORemarks" runat="server" CssClass="form-control-res" Style="float: right"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return ValidateSave()" OnClick="lnkSave_Click" Text="Save"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text="Clear"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkViewPo" runat="server" class="button-blue" Text="PO List" PostBackUrl="~/Purchase/frmPurchaseOrderView.aspx"></asp:LinkButton>
                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
