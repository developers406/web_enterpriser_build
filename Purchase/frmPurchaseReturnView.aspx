﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmPurchaseReturnView.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseReturnView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .PRSummary td:nth-child(1), .PRSummary th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .PRSummary td:nth-child(2), .PRSummary th:nth-child(2) {
            min-width: 40px;
            max-width: 40px;
        }

        .PRSummary td:nth-child(3), .PRSummary th:nth-child(3) {
            min-width: 40px;
            max-width: 40px;
        }

        .PRSummary td:nth-child(4), .PRSummary th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .PRSummary td:nth-child(5), .PRSummary th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRSummary td:nth-child(6), .PRSummary th:nth-child(6) {
            min-width: 597px;
            max-width: 597px;
        }

        .PRSummary td:nth-child(7), .PRSummary th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRSummary td:nth-child(8), .PRSummary th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRSummary td:nth-child(9), .PRSummary th:nth-child(9) {
            min-width: 50px;
            max-width: 50px;
        }

        .PRSummary td:nth-child(10), .PRSummary th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .PRSummary td:nth-child(11), .PRSummary th:nth-child(11) {
            min-width: 150px;
            max-width: 150px;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchaseReturnView');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchaseReturnView";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>



    <script type="text/javascript">

         $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };


        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        });
         
 
    </script>
    <script type="text/javascript">
        function pageLoad() {
             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
                $('#<%=lnkAdd.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkAdd.ClientID %>').css("display", "none");
              }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            shortcut.add("Ctrl+A", function () {
                $('#<%=hidBillType.ClientID%>').val('A');
                 $('#lblView').removeClass('lowercase');
                 $('#lblView').addClass('uppercase');

             });
             shortcut.add("Ctrl+B", function () {
                 $("#<%=hidBillType.ClientID %>").val('B');
                 $('#lblView').removeClass('uppercase');
                 $('#lblView').addClass('lowercase');
             });
        }
    </script>
    <%------------------------------------------- PR Delete Detail Open Popup -------------------------------------------%>
    <script type="text/javascript">

        function fncPRDetail(source) {
            var rowObj;
            try {
                rowObj = $(source).closest("tr");
                $("#<%=hidPRNo.ClientID %>").val($("td", rowObj).eq(3).text().replace(/&nbsp;/g, ''));
                $("#<%=hidLoc.ClientID %>").val($("td", rowObj).eq(8).text().replace(/&nbsp;/g, ''));
                $("#<%=hidPRDate.ClientID %>").val($("td", rowObj).eq(4).text().replace(/&nbsp;/g, ''));
                //fncShowDeleteConfirmation_master( "Do you want delete this Purchase Return - "+$("#<%=hidPRNo.ClientID %>").val()+ "?" );
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncOpenPRDelete() {
            try {
                if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Purchase Return");
                return false;
            }
                $("#dialog-PRDelete").dialog({
                    resizable: false,
                    height: "auto",
                    width: 900,
                    modal: true,
                    title: "Delete Purchase Return",
                    hide: {
                        effect: "fade",
                        duration: 500
                    },
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            $("#<%=btnDelete.ClientID %>").click();
                        },
                        No: function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetValue() {
            try {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkLoad', '');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="frmPurchaseReturnView.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page" id="lblView">VIEW PURCHASE RETURN</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="purchase-order-header">
                        <div class="control-container" style="float: left; clear: both; display: table; width: 30%">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" OnClick ="lnkAdd_Click">New</asp:LinkButton>  <%--PostBackUrl="~/Purchase/frmPurchaseReturn.aspx"--%>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkLoad" OnClick="lnkLoad_click" runat="server" class="button-blue">Refresh</asp:LinkButton>
                            </div>
                            <div class="control-button PO_Summary_border">
                                <asp:RadioButton ID="rbtSummary" runat="server" Text="Internal" GroupName="Print" Checked="true" />
                                <asp:RadioButton ID="rbtDetail" runat="server" Text="External" GroupName="Print" />
                            </div>
                            <%--  <div class="control-button">
                                <asp:LinkButton ID="lnkDelete" runat="server" class="button-blue" OnClick="lnkDelete_Click">Delete</asp:LinkButton>
                                <asp:LinkButton ID="lnkDeleteConfirm" runat="server" class="button-blue" Text="Delete"
                                    OnClick="lnkDeleteConfirm_Click" Visible="false"></asp:LinkButton>
                            </div>--%>
                        </div>
                        <%-- <div class="purchase-option-container" style="width: 30%; float: left">
                            <div class="control-group-split">
                                <div class="control-group-left" style="width: 100%">
                                    <div class="label-left">
                                        <asp:RadioButton ID="rbtSummary" runat="server" Text="Internal"  GroupName="Print" Checked="true" />
                                        <asp:RadioButton ID="rbtDetail" runat="server" Text="External"  GroupName="Print" />
                                    </div>
                                    <div class="label-right">
                                        <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_click" class="button-red" Visible="false">Print</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>--%>
                        <div class="purchase-option-container" style="width: 40%; float: right">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Vendor Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                                        <%-- <asp:DropDownList ID="ddlVendorCode" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlVendorCode_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Location Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <%--<asp:DropDownList ID="ddlLocationCode" runat="server" CssClass="form-control-res"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlLocationCode_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="purchase-option-container" style="width: 40%; float: right">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="From Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="To Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="purchase-order-detail">
                        <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header PRSummary">
                            <tr>
                                <th>S.No</th>
                                <th>Delete</th>
                                <th>Print</th>
                                <th>PR NO</th>
                                <th>PR Date</th>
                                <th>Vendor Name</th>
                                <th>Net Total</th>
                                <th>Created By</th>
                                <th>Loc</th>
                                <th>No Of Items</th>
                                <th>Remarks</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 430px; width: 1355px;">
                            <asp:GridView ID="gvPR" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                CssClass="pshro_GridDgn PRSummary">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <%--  <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />--%>
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                ToolTip="Click here to Delete" OnClick="imgbtnDelete_click" OnClientClick="fncPRDetail(this);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnPrint" runat="server" ImageUrl="~/images/print1.png"
                                                ToolTip="Click here to Delete" OnClick="lnkPrint_click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ReturnNo" HeaderText="PR NO"></asp:BoundField>
                                    <asp:BoundField DataField="ReturnDate" HeaderText="PR Date"></asp:BoundField>
                                    <asp:BoundField DataField="VendorName" HeaderText="Vendor Name"></asp:BoundField>
                                    <asp:BoundField DataField="TotalNetTotal" HeaderText="Net Total"></asp:BoundField>
                                    <asp:BoundField DataField="CreateUser" HeaderText="Created By"></asp:BoundField>
                                    <asp:BoundField DataField="Location" HeaderText="Loc"></asp:BoundField>
                                    <asp:BoundField DataField="NofItems" HeaderText="No Of Items"></asp:BoundField>
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div id="dialog-PRDelete" style="display: none">
                        <div class="grid-popup">
                            <asp:GridView ID="gvPRDelete" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                CssClass="pshro_GridDgn">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="ReturnNo" HeaderText="PR No"></asp:BoundField>
                                    <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="BalanceQty" HeaderText="Current Stock"></asp:BoundField>
                                    <asp:BoundField DataField="NReturnQty" HeaderText="Return Qty"></asp:BoundField>
                                    <asp:BoundField DataField="FOC" HeaderText="Return Foc Qty"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidPRNo" runat="server" />
        <asp:HiddenField ID="hidLoc" runat="server" />
        <asp:HiddenField ID="hidPRDate" runat="server" />
        <asp:HiddenField ID="hidBillType" runat="server" Value="A" />
        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_click" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
