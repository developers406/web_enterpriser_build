﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPurchaseReturnList.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmPurchaseReturnList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .selected_row {
            background-color: Teal;
            color: White;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }

        .transfer td:nth-child(1), .transfer th:nth-child(1) {
            min-width: 55px;
            max-width: 55px;
        }

        .transfer td:nth-child(2), .transfer th:nth-child(2) {
            min-width: 55px;
            max-width: 55px;
        }

        .transfer td:nth-child(3), .transfer th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(4), .transfer th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(5), .transfer th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(6), .transfer th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(6) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(7), .transfer th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(7) {
            text-align: right !important;
            padding-right: 9px;
        }

        .transfer td:nth-child(8), .transfer th:nth-child(8) {
            min-width: 437px;
            max-width: 437px;
        }

        .transfer td:nth-child(9), .transfer th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(10), .transfer th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(11), .transfer th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .transfer td:nth-child(12), .transfer th:nth-child(12) {
            display: none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'PurchaseReturnList');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "PurchaseReturnList";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        $(function () {
            fncFormInitialLoad();
        });

        function fncFormInitialLoad() {
            try {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "-1");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }
        catch (err)
        { }
    }

    function ValidateForm() {
        var Show = '';
        if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

            else {
                return true;
            }
        }

        $(function () {

            $("[id*=grdPurchaseReturnList]").on('click', 'td', function (e) {
                //DisplayChildDetails($(this).closest("tr")); 
                //alert($("td", $(this).closest("tr")).eq(2).html());
                var ReqNo = $("td", $(this).closest("tr")).eq(2).html();
                $("#<%=hidenReqNo.ClientID %>").val(ReqNo);
                e.preventDefault();
            });
        });

        $(function () {
            $("[id*=grdPurchaseReturnList] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdPurchaseReturnList] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                        var ReqNo = $("td", $(this).closest("tr")).eq(2).html();
                        $("#hidenReqNo").val(ReqNo);
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("table[id$='grdPurchaseReturnList']").html("");

            return false;
        }

        function Search_Gridview(strKey) {
            try {

                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdPurchaseReturnList.ClientID %>");
                var rowData;

                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;

                }
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function pageLoad() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Toast(source) {
            var row = source.parentNode.parentNode;
            var PrNO = row.cells[2].innerHTML;
            var Vendorcode = row.cells[8].innerHTML;
            if ($("#<%=hidViewbtn.ClientID %>").val() != "V1") {
                ShowPopupMessageBox("You have no permission to View this Purchase Return Request");
                return false;
            }
            else {
                $("#<%=hidReqNo.ClientID %>").val(PrNO);
                $("#<%=hidVendor.ClientID %>").val(Vendorcode);
                $("#<%=btnRedirect.ClientID %>").click();
            }
        }

        function ToastDelete() {
            ShowPopupMessageBox("You have no permission to Delete this Purchase Return Request");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Purchase Return Requested List</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full-gid">
            <div style="margin-left: 5px;" class="top-purchase-container">
                <div class="top-purchase-left-container" style="width: 45%; float: left;">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblfromdate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_returnType %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="DropDownReturnType" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtReturnType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PRType', 'txtReturnType', '');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbltodate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_ReturnNo %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="DropDownReturnNo" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtReturnNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PRNumber', 'txtReturnNo', '');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-container-purch">
                        <div class="control-button" style="float: left;">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnadd %>'
                                OnClick="lnkAdd_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="return Clearall()"
                                Text='<%$ Resources:LabelCaption,btnclear %>'></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_fetch %>'
                                OnClick="lnkRefresh_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Print %>'
                                OnClick="lnkPrint_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right; border: 1px solid; padding: 5px 5px 0 5px;">
                            <asp:RadioButton ID="rdoPrint" runat="server" GroupName="RegularMenu" Text="Bill Size" />
                        </div>
                        <div class="control-button" style="float: right; border: 1px solid; padding: 5px 5px 0 5px;">
                            <asp:RadioButton ID="rdoA4Print" runat="server" GroupName="RegularMenu" Text="A4 Size"
                                Checked="true" />
                        </div>
                    </div>
                </div>
                <div class="top-purchase-left-container" style="padding-left: 20px; width: 45%; float: left;">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lblLocation %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_vendor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="DropDownVendor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                            </div>
                        </div>
                        <%--   <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return Clearall()"
                                Text='<%$ Resources:LabelCaption,btnclear %>'></asp:LinkButton>
                        </div>
                        <div class="control-button" style="float: right;">
                            <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_fetch %>'
                                OnClick="lnkRefresh_Click"></asp:LinkButton>
                        </div>--%>
                    </div>
                </div>
            </div>
            <%--<div class="bottom-audit-container">--%>
            <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="over_flowhorizontal">
                        <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header transfer">
                            <tr>
                                <th>Delete</th>
                                <th>View</th>
                                <th>PRReq No</th>
                                <th>Req Date</th>
                                <th>Return Type</th>
                                <th>Total Qty</th>
                                <th>Total Amount</th>
                                <th>Vendor Name</th>
                                <th>Vendor code</th>
                                <th>Createuser</th>
                                <th>CreateDate</th>
                                <th>Status</th>
                            </tr>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 350px; width: 1355px; background-color: aliceblue;">
                            <asp:GridView ID="grdPurchaseReturnList" runat="server" AutoGenerateColumns="true" ShowHeader="false"
                                PageSize="14" ShowHeaderWhenEmpty="false" CssClass="transfer" OnRowCommand="grdPurchaseReturnList_RowCommand"
                                OnRowDataBound="grdPurchaseReturnList_RowDataBound" OnRowDeleting="grdPurchaseReturnList_RowDeleting">
                                <PagerStyle CssClass="pshro_text tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <Columns>
                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                    <asp:TemplateField HeaderText="Supplier (Vendor) Name">
                                        <ItemTemplate>
                                            <asp:Button ID="btnVendorChange" runat="server" Style="background-color: #0090cb; border-color: #0090cb; color: #fff" Text="View" OnClientClick="Toast(this);return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:CommandField HeaderText="View"  ShowEditButton="True" EditText="View" ButtonType="Button" />--%>
                                </Columns>
                                <%--  <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>--%>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    PageButtonCount="5" Position="Bottom" />
                                <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                            </asp:GridView>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--</div>--%>
        </div>
        <div class="container-group-full display_none">
            <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
            <asp:HiddenField ID="hidenReqNo" runat="server" Value="" />
            <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidReqNo" runat="server" />
            <asp:HiddenField ID="hidVendor" runat="server" />
            <asp:Button ID="btnRedirect" runat="server" Text="View" OnClick="lnkRedirect_click" />
        </div>
    </div>
</asp:Content>
