﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="frmTextileGA.aspx.cs" Inherits="EnterpriserWebFinal.Purchase.frmTextileGA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />
    <script type="text/javascript">
        var Status = 0;
        function pageLoad() {
            try {
                $(".chzn-container").css('display', 'none');
                $(".form-control-res").css('height', '25px');
                $(".form-control-res").css('display', 'block');
                $("#<%=txtEntryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $("#<%=txtInvDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $("#<%=txtNetAmt.ClientID %>").attr('disabled', 'disabled');
                $("#<%=txtRound.ClientID %>").attr('disabled', 'disabled');
                //if ($.session.get('height') != null && $.session.get('height') != "") {
                //    var height = $.session.get('height');
                //    height = parseFloat(height) - 310;
                //    $("#tblAttribute tbody").css('height', '' + height + "px" + '');
                //}
                fncDecimal();
                fncGetGADetails('N');
              <%--  $("#<%=chkGid.ClientID %>").change(function () {
                    var tblGABody = $("#tblInvoice tbody");
                    if (this.checked) {
                        Status = 1;
                        fncGetGADetails('Y');
                        tblGABody.css('background-color', 'aliceblue');
                    }
                    else {
                        Status = 0;
                        fncGetGADetails('N');
                        tblGABody.css('background-color', 'white');
                    }
                });--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetGADetails(status) {
            try {
                $.ajax({
                    url: "frmTextileGA.aspx/GetGaDetails",
                    data: "{ 'Code': '" + status + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        fncBindData(jQuery.parseJSON(data.d));
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBindData(obj) {
            try {
                var vars = {};
                var divstart = "";
                var tblGABody = $("#tblInvoice tbody");
                tblGABody.children().remove();
                for (var i = 0; i < obj.Table.length; i++) {
                    var row = "<tr id='GRNBodyrow_" + i + "'  tabindex='" + i + "'  >"
                        + "<td id ='tdCompanyCode_" + i + "'  >" + obj.Table[i]["CompanyCode"] + "</td>"
                        + "<td id ='tdSupplierName_" + i + "'  >" + obj.Table[i]["SupplierName"] + "</td>"
                        + "<td id ='tdDoNo_" + i + "'  >" + obj.Table[i]["DoNo"] + "</td>"
                        + "<td id ='tdDoDate_" + i + "'  >" + obj.Table[i]["DoDate"] + "</td>"
                        + "<td id ='tdNOfPcs_" + i + "'  >" + obj.Table[i]["NOfPcs"] + "</td>"
                        + "<td id ='tdBillValue_" + i + "' >" + obj.Table[i]["BillValue"] + "</td>"
                        + "<td id ='tdGstPer_" + i + "' >" + obj.Table[i]["GstPer"] + "</td>"
                        + "<td id ='tdDisPer_" + i + "' >" + obj.Table[i]["DisPer"] + "</td>"
                        + "<td id ='tdNet_" + i + "' >" + obj.Table[i]["NetAmt"] + "</td>"
                        + "<td id ='tdTran_" + i + "' style='display:none !important'>" + obj.Table[i]["TranNo"] + "</td>"
                        + "<td id ='tdVendodCode_" + i + "' style='display:none !important'>" + obj.Table[i]["VendorName"] + "</td>"
                        + "<td  onclick ='fncGo(this);return false;' style='text-align: center !important;' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Del\");' ><span id='tdremove_" + i + "' class ='glyphicon glyphicon-remove-circle' style ='font-size: 20px;color: red;cursor: pointer;' />&nbsp&nbsp&nbsp<span id='tdsign_" + i + "' class ='glyphicon glyphicon-ok-sign' style ='font-size: 20px;color: green;cursor: pointer;' />&nbsp&nbsp&nbsp<span id='tdEdit_" + i + "' class ='glyphicon glyphicon-edit' style ='font-size: 20px;color: dark blue;cursor: pointer;' /></td></tr> ";
                    tblGABody.append(row);
                }

            }
            catch (ex) {
                ShowPopupMessageBox(ex.message);
            }
        }

        function fncDecimal() {
            $('#<%=txtBillValue.ClientID%>').number(true, 2);
            $('#<%=txtExpense.ClientID%>').number(true, 2);
            $('#<%=txtLRExpense.ClientID%>').number(true, 2);
            $('#<%=txtBaseAmt.ClientID%>').number(true, 2);
            $('#<%=txtLRDiscount.ClientID%>').number(true, 2);
            $('#<%=txtLRPcsDisc.ClientID%>').number(true, 2);
            $('#<%=txtGrossCharge.ClientID%>').number(true, 2);
            $('#<%=txtRound.ClientID%>').number(true, 2);
            $('#<%=txtNetAmt.ClientID%>').number(true, 2);
        }

        function fncAdd() {
            try {
                var Show = '', InterState = 0;
                if ($('#<%=ddlCompany.ClientID%>').val() == "") {
                    Show = Show + 'Please Select Comapany.';
                }
                if ($('#<%=txtEntryDate.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Entry Date.';
                }
                if ($('#<%=txtInvDate.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Invoice Date.';
                }
                if ($('#<%=ddlSupplier.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Supplier.';
                }
                if ($('#<%=txtInvNo.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Invoice Number.';
                }
                if ($('#<%=txtBillValue.ClientID%>').val() == "" || parseFloat($('#<%=txtBillValue.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Bill Value.';
                }

                $("#tblGA tbody").children().each(function () {
                    var obj = $(this);
                    if (obj.find('td input[id*=txtInvNo]').val().trim() == $('#<%=txtInvNo.ClientID%>').val().trim()) {
                        Show = Show + '<br /> Invoice Number Already Exists.';
                    }
                });
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                if ($('#<%=chkInterState.ClientID%>').is(":checked")) {
                    InterState = 1;
                }
                var tax = parseFloat($('#<%=txtBillValue.ClientID%>').val() * parseFloat($('#<%=txtExpense.ClientID%>').val()) / 100).toFixed(2);
                var NetAmt = parseFloat($('#<%=txtBillValue.ClientID%>').val()) + parseFloat(tax);

                var tblGABody = $("#tblGA tbody");
                var maxRowNo = tblGABody.children().length;
                var rowno = "rowno";
                var row = "<tr id='GRNBodyrow_" + maxRowNo + "'  tabindex='" + maxRowNo + "'  >"

                    + "<td><input type='text' id='txtInvNo_" + maxRowNo + "' value='" + $('#<%=txtInvNo.ClientID%>').val() + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"InvNo\");' onkeypress='return fnctblColEnableFalse(event);' /></td>"

                    + "<td><input type='text' id='txtBillValue_" + maxRowNo + "' value='" + parseFloat($('#<%=txtBillValue.ClientID%>').val()).toFixed(2) + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Bill\");' onkeypress='return numericOnly(event);'"
                    + "onchange='fncRepeaterColumnValueChange(this,\"Bill\");' /></td>"

                    + "<td><input type='text' id='txtDiscountPer_" + maxRowNo + "' value='" + 0 + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onkeypress='return numericOnly(event);'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DisPer\");' onchange='fncRepeaterColumnValueChange(this,\"DisPer\");' /></td>"

                    + "<td><input type='text' id='txtDisAmt_" + maxRowNo + "' value='" + 0 + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onkeypress='return numericOnly(event);'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"DisAmt\");'  onchange='fncRepeaterColumnValueChange(this,\"DisAmt\");'/></td>"

                    + "<td><input type='text' id='txtGSTPer_" + maxRowNo + "' value='" + parseFloat($('#<%=txtExpense.ClientID%>').val()).toFixed(2) + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' onchange='fncRepeaterColumnValueChange(this,\"TaxPer\");'  onkeypress='return numericOnly(event);'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TaxPer\");' /></td>"

                    + "<td><input type='text' id='txtTaxValue_" + maxRowNo + "' value='" + parseFloat(tax).toFixed(2) + "'  "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right'onkeypress='return fnctblColEnableFalse(event);'"
                    + "onkeydown=' return fncSetFocusNextRowandCol(event,this,\"TaxValue\");'  /></td>"

                    + "<td><input type='text' id='txtNetAmt_" + maxRowNo + "' value='" + parseFloat(NetAmt).toFixed(2) + "' onkeypress='return fnctblColEnableFalse(event);' "
                    + " onfocus='this.select();' Class='border_nonewith_yellow form-control-res-right' /onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Net\");' ></td>"

                    + "<td id ='tdPcs_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtNoPcs.ClientID%>').val() + "</td>"
                    + "<td id ='tdEntryDate_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtEntryDate.ClientID%>').val() + "</td>"
                    + "<td id ='tdInvDate_" + maxRowNo + "' style='display:none !important'>" + $('#<%=txtInvDate.ClientID%>').val() + "</td>"
                    + "<td id ='tdInterState_" + maxRowNo + "' style='display:none !important'>" + InterState + "</td>"
                    + "<td id ='tdCompany_" + maxRowNo + "' style='display:none !important'>" + $('#<%=ddlCompany.ClientID%> option:selected').val() + "</td>"
                    + "<td id ='tdSupplier_" + maxRowNo + "' style='display:none !important'>" + $('#<%=ddlSupplier.ClientID%> option:selected').val() + "</td>"
                    + "<td  onclick ='DeleteRow(this);return false;' style='text-align: center !important;' onkeydown=' return fncSetFocusNextRowandCol(event,this,\"Del\");' ><span id='tdAction_" + maxRowNo + "' class ='glyphicon glyphicon-remove-circle' style ='font-size: 20px;color: red;cursor: pointer;' /></td></tr > ";

                tblGABody.append(row);
                //console.log(row);
                fncTotalCalculation();
                // fncClearAttribute();
                fncClear();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncTotalCalculation() {
            try {
                var TotBill = 0, TotDis = 0, TotTax = 0, TotNet = 0;
                $("#tblGA tbody").children().each(function () {
                    TotBill = parseFloat(TotBill) + parseFloat($(this).find('td input[id*=txtBillValue]').val());
                    TotDis = parseFloat(TotDis) + parseFloat($(this).find('td input[id*=txtDisAmt]').val());
                    TotTax = parseFloat(TotTax) + parseFloat($(this).find('td input[id*=txtTaxValue]').val());
                    TotNet = parseFloat(TotNet) + parseFloat($(this).find('td input[id*=txtNetAmt]').val());
                });

                <%--$('#<%=lblBillValue.ClientID%>').text((TotBill).toFixed(2));
                $('#<%=lblDis.ClientID%>').text(parseFloat(TotDis).toFixed(2));
                $('#<%=lblTax.ClientID%>').text(parseFloat(TotTax).toFixed(2));
                $('#<%=lblNet.ClientID%>').text(parseFloat(TotNet).toFixed(2));--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function numericOnly(elementRef) {

            var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

            if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57) || (keyCodeEntered == 189) || (keyCodeEntered == 109)) {

                return true;

            }

            // '.' decimal point...  

            else if (keyCodeEntered == 46) {

                // Allow only 1 decimal point ('.')...  

                if ((elementRef.target.value) && (elementRef.target.value.indexOf('.') >= 0))

                    return false;

                else

                    return true;

            }

            return false;

        }
        function fncRepeaterColumnValueChange(source, value) {
            try {
                var rowObj = $(source).parent().parent();
                if (value == 'Bill') {
                    if (rowObj.find('td input[id*="txtBillValue"]').val() != '' && rowObj.find('td input[id*="txtBillValue"]').val() >= 0) {
                        rowObj.find('td input[id*="txtBillValue"]').val(0);
                    }
                }
                var BillValue = rowObj.find('td input[id*="txtBillValue"]').val();
                var DisPer = rowObj.find('td input[id*="txtDiscountPer"]').val();
                var DiscountAmt = 0;
                if (value == 'DisPer') {
                    DiscountAmt = BillValue * (DisPer / 100);
                    rowObj.find('td input[id*="txtDisAmt"]').val(parseFloat(DiscountAmt).toFixed(2));
                }
                if (value == 'DisAmt') {
                    DisPer = (parseFloat(rowObj.find('td input[id*="txtDisAmt"]').val()) / parseFloat(BillValue)) * 100;
                    rowObj.find('td input[id*="txtDiscountPer"]').val(parseFloat(DisPer).toFixed(2));
                }
                if (value == 'TaxPer') {
                    BillValue = parseFloat(BillValue) - parseFloat(rowObj.find('td input[id*="txtDisAmt"]').val());
                    var tax = parseFloat(BillValue * parseFloat(rowObj.find('td input[id*="txtGSTPer"]').val()) / 100).toFixed(2);
                    rowObj.find('td input[id*="txtTaxValue"]').val(parseFloat(tax).toFixed(2));
                }
                var Tot = parseFloat(BillValue) - parseFloat(rowObj.find('td input[id*="txtDisAmt"]').val()) + parseFloat(rowObj.find('td input[id*="txtTaxValue"]').val());
                rowObj.find('td input[id*="txtNetAmt"]').val(parseFloat(Tot).toFixed(2));
                fncTotalCalculation();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetFocusNextRowandCol(evt, source, value) {
            var rowobj, rowId = 0, NextRowobj, prevrowobj;
            try {

                rowobj = $(source).parent().parent();
                rowId = rowobj.find('td input[id*="txtSNo"]').val();
                rowId = parseFloat(rowId) - parseFloat(1);
                charCode = (evt.which) ? evt.which : evt.keyCode;
                rowobj.css("background-color", "#ffffff");
                //// Enter and Right Arrow
                if (charCode == 13 || charCode == 39) {
                    rowobj.css("background-color", "#999999");

                    if (value == "InvNo") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtBillValue"]'));
                        return false;
                    }
                    else if (value == "Bill") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscountPer"]'));
                        return false;
                    }
                    else if (value == "DisPer") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDisAmt"]'));
                        return false;
                    }
                    else if (value == "DisAmt") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtGSTPer"]'));
                        return false;
                    }
                    else if (value == "TaxPer") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtTaxValue"]'));
                        return false;
                    }
                    else if (value == "TaxValue") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetAmt"]'));
                        return false;
                    }
                    else if (value == "Net") {
                        fncSetSelectAndFocus(rowobj.find('td span[id*="tdAction"]'));
                        return false;
                    }
                }
                ///// Left Arrow
                else if (charCode == 37) {
                    rowobj.css("background-color", "#999999");
                    if (value == "Bill") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtInvNo"]'));
                        return false;
                    }
                    else if (value == "DisPer") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtBillValue"]'));
                        return false;
                    }
                    else if (value == "DisAmt") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDiscountPer"]'));
                        return false;
                    }
                    else if (value == "TaxPer") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtDisAmt"]'));
                        return false;
                    }
                    else if (value == "TaxValue") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtGSTPer"]'));
                        return false;
                    }
                    else if (value == "Net") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtTaxValue"]'));
                        return false;
                    }
                    else if (value == "Del") {
                        fncSetSelectAndFocus(rowobj.find('td input[id*="txtNetAmt"]'));
                        return false;
                    }

                }
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    NextRowobj.css("background-color", "#999999");
                    if (NextRowobj.length == 0) {
                        rowobj.css("background-color", "#999999");
                        return false;
                    }
                    if (value == "InvNo") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtInvNo"]'));
                        return false;
                    }
                    else if (value == "Bill") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBillValue"]'));
                        return false;
                    }
                    else if (value == "DisPer") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtDiscountPer"]'));
                        return false;
                    }
                    else if (value == "DisAmt") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtDisAmt"]'));
                        return false;
                    }
                    else if (value == "TaxPer") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtGSTPer"]'));
                        return false;
                    }
                    else if (value == "TaxValue") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtTaxValue"]'));
                        return false;
                    }
                    else if (value == "Net") {
                        fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtNetAmt"]'));
                        return false;
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    prevrowobj.css("background-color", "#999999");
                    if (prevrowobj.length == 0) {
                        return false;
                    }
                    if (value == "InvNo") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtInvNo"]'));
                        return false;
                    }
                    else if (value == "Bill") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtBillValue"]'));
                        return false;
                    }
                    else if (value == "DisPer") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtDiscountPer"]'));
                        return false;
                    }
                    else if (value == "DisAmt") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtDisAmt"]'));
                        return false;
                    }
                    else if (value == "TaxPer") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtGSTPer"]'));
                        return false;
                    }
                    else if (value == "TaxValue") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtTaxValue"]'));
                        return false;
                    }
                    else if (value == "Net") {
                        fncSetSelectAndFocus(prevrowobj.find('td input[id*="txtNetAmt"]'));
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetSelectAndFocus(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncClear() {
            try {
                $('#<%=txtBillValue.ClientID%>').val(0);
                $('#<%=txtExpense.ClientID%>').val(0);
                $('#<%=txtNoPcs.ClientID%>').val(0);
                $('#<%=txtInvNo.ClientID%>').val('');
                $('#<%=chkInterState.ClientID%>').removeAttr('checked');
                $('#<%=txtInvNo.ClientID%>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncLrCalculation(mode) {
            try {
                //if (mode == '') {

                //}
                var NetAmt = parseFloat($('#<%=txtLRExpense.ClientID%>').val()) + parseFloat($('#<%=txtBaseAmt.ClientID%>').val())
                    - parseFloat($('#<%=txtLRDiscount.ClientID%>').val()) - parseFloat($('#<%=txtLRPcsDisc.ClientID%>').val());
                var taxAmt = parseFloat(NetAmt * parseFloat($('#<%=txtGrossCharge.ClientID%>').val()) / 100).toFixed(2);

                NetAmt = parseFloat(NetAmt) + parseFloat(taxAmt);

                $('#<%=txtNetAmt.ClientID%>').val(SellingPriceRoundOff(parseFloat(NetAmt).toFixed(2)));

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Selling Price Round Off
        function SellingPriceRoundOff(SellingPrice) {
            var sellingprice = 0, roundOff = 0;
            sellingprice = SellingPrice;
            var NetSellingprice = 0;

            if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '1') {
                NetSellingprice = (Math.round(SellingPrice)).toFixed(2); //1 Paise RoundOff
            }
            else if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '50') {
                NetSellingprice = (Math.round(SellingPrice * 2) / 2).toFixed(2); //50 Paise RoundOff
            }
            else if ($('#<%=hidfSellingPriceRoundOff.ClientID %>').val() == '25') {
                NetSellingprice = (Math.round(SellingPrice * 4) / 4).toFixed(2); //25 Paise RoundOff
            }
            else {
                NetSellingprice = SellingPrice;

            }
            roundOff = (parseFloat(NetSellingprice) - parseFloat(sellingprice)).toFixed(2);
            $('#<%=txtRound.ClientID %>').val(roundOff);

            return NetSellingprice;
        }

        function fncSave() {
            try {
                var xml = '<NewDataSet>', Show = '';
                var rowNo = 0;

                if ($('#<%=chkInterState.ClientID%>').is(":checked")) {
                    InterState = 1;
                }
                else {
                    InterState = 0;
                }
               <%-- if ($('#<%=txtLrNo.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter LR Number.';
                }
                if ($('#<%=txtLRExpense.ClientID%>').val() == "" || parseFloat($('#<%=txtLRExpense.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter LR Expense.';
                }--%>

                //if (Show != '') {
                //    ShowPopupMessageBox(Show);
                //    return false;
                //}

                xml += "<Table>";
                xml += "<InvNo>" + $('#<%=txtInvNo.ClientID %>').val().trim() + "</InvNo>";
                xml += "<BillValue>" + $('#<%=txtBillValue.ClientID %>').val().trim() + "</BillValue>";
                xml += "<DisPer>" + 0 + "</DisPer>";
                xml += "<DisAmt>" + 0 + "</DisAmt>";
                xml += "<GstPer>" + $('#<%=txtExpense.ClientID %>').val().trim() + "</GstPer>";
                xml += "<Tax>" + $('#<%=txtGrossCharge.ClientID %>').val().trim() + "</Tax>";
                xml += "<NetAmt>" + $('#<%=txtNetAmt.ClientID %>').val().trim() + "</NetAmt>";
                xml += "<Pcs>" + $('#<%=txtNoPcs.ClientID %>').val().trim() + "</Pcs>";
                xml += "<EntryDate>" + $('#<%=txtEntryDate.ClientID %>').val().trim() + "</EntryDate>";
                xml += "<InvDate>" + $('#<%=txtInvDate.ClientID %>').val().trim() + "</InvDate>";
                xml += "<ISate>" + InterState + "</ISate>";
                xml += "<Company>" + $('#<%=ddlCompany.ClientID %>').val().trim() + "</Company>";
                xml += "<Remarks>" + "PUR" + "</Remarks>";
                xml += "<Vendor>" + $('#<%=ddlSupplier.ClientID %>').val().trim() + "</Vendor>";
                //LR Details
                xml += "<LrNo>" + $('#<%=txtLrNo.ClientID %>').val() + "</LrNo>";
                xml += "<Expense>" + $('#<%=txtExpense.ClientID %>').val() + "</Expense>";
                xml += "<BaseAmt>" + $('#<%=txtBaseAmt.ClientID %>').val() + "</BaseAmt>";
                xml += "<Discount>" + $('#<%=txtLRDiscount.ClientID %>').val() + "</Discount>";
                xml += "<PcsDiscount>" + $('#<%=txtLRPcsDisc.ClientID %>').val() + "</PcsDiscount>";
                xml += "<LRTax>" + $('#<%=txtGrossCharge.ClientID %>').val() + "</LRTax>";
                xml += "<RoundOff>" + $('#<%=txtRound.ClientID %>').val() + "</RoundOff>";
                xml += "<LRNetAmt>" + $('#<%=txtNetAmt.ClientID %>').val() + "</LRNetAmt>";
                xml += "</Table>";
           
            xml = xml + '</NewDataSet>'
            xml = escape(xml);
            $('#<%=hidSaveSata.ClientID %>').val(xml);
            console.log($('#<%=hidSaveSata.ClientID %>').val());
            <%--    $('#<%=btnclk.ClientID %>').click();--%>
            return true;
        }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGo(source) {
            try {
                $('#<%=hidTranNo.ClientID %>').val($(source).closest('tr').find('td[id*=tdTran]').text());
                $('#<%=hidVendor.ClientID %>').val($(source).closest('tr').find('td[id*=tdVendodCode]').text());
                $('#<%=hidNetAmt.ClientID %>').val($(source).closest('tr').find('td[id*=tdNet]').text());
                $('#<%=hidInvoiceNo.ClientID %>').val($(source).closest('tr').find('td[id*=tdDoNo]').text());
                $('#<%=hidBillValue.ClientID %>').val($(source).closest('tr').find('td[id*=tdBillValue]').text());
                $('#<%=hidStatus.ClientID %>').val(Status);
                $('#<%=btnClick.ClientID %>').click();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function DeleteRow(source) {
            var Itemcode = $(source).closest('tr').find('td input[id*=txtSNo]').val();
            var tblVat = 0;
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                open: function () {
                    var markup = 'Do you want Delete this S.No? -' + Itemcode;
                    $(this).html(markup);
                },
                buttons: {
                    "NO": function () {
                        $(this).dialog("close");
                    },
                    "YES": function () {
                        $(this).dialog("close");
                        $(source).closest('tr').remove();
                        fncTotalCalculation();
                    }
                }
            });
        }

        function fncLearAll() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);

        }
       
    </script>
    <style type="text/css">
        .Invoice td, th {
            width: 122px !important;
        }

       /* #tblInvoice td, th {
            width: 122px !important;
        }*/

        .GA td, th {
            width: 138px !important;
        }

        .saleslabel {
            font-size: 15px;
            font-weight: 900;
            color: blue;
        }

        .salesData {
            font-size: 15px;
            font-weight: 900;
            color: green;
        }

        /* .Invoice td, th {
            width: 109px !important;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a>Purchase</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">GA Entry</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div >
<%--            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-6">
                    <span class="glyphicon glyphicon-arrow-left hedertext"></span><span class="whole-price-header">GA Entry</span>
                </div>
                <div class="col-md-6">
                    <div class="button-contol">
                        <div class="control-button">
             
                            <div class="glyphicon glyphicon-chevron-left gylSeperator"></div>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="glyphicon glyphicon-floppy-remove" OnClientClick="fncClear();return false;">
                             <span class="btnSpan">Clear</span></asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 headerborder">
                </div>
            </div>--%>

            <div class="container-group-full EnableScroll">
                <div style="width: 100%; margin-bottom: 0px; display: table">
                    <div class="panel panel-default" style="width: 100%; margin-bottom: 0px; display: table">
                        <div id="Tabs" role="tabpanel">
                            <ul class="nav nav-tabs custnav" role="tablist">
                                <li class="active ga_tab_header"><a href="#GAForm" aria-controls="GAForm" role="tab"
                                    data-toggle="tab">GOODS ACKNOWLEDGEMENT RECEIPT</a></li>

                            </ul>
                            <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto;">
                                <div class="tab-pane active" role="tabpanel" id="GAForm">
                                    <div class="left-container ga_left-container" style="width: 33%;">
                                        <div class="left-container-detail">
                                            <div class="left-container-header">
                                                <%=Resources.LabelCaption.lblGoodsAcknowledgementReceipt%>
                                            </div>
                                            <div class="control-group-single" style="margin-top: 5px;">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label16" runat="server" Text='Company'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label12" runat="server" Text='EntryDate'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtEntryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label2" runat="server" Text='InvoiceDate'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtInvDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label4" runat="server" Text='Supplier'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label6" runat="server" Text='InterState'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:CheckBox ID="chkInterState" runat="server" Class="radioboxlist" />
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label7" runat="server" Text='Invoice No'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtInvNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label8" runat="server" Text='Bill Value'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtBillValue" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label9" runat="server" Text='GST %'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtExpense" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label10" runat="server" Text='No.of.Pcs'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtNoPcs" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label11" runat="server" Text='LR NO'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtLrNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vehicle', 'txtLrNo', 'txtLRExpense');"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label13" runat="server" Text='LR Expense'
                                                        Font-Bold="True"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtLRExpense" runat="server" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('LrExp');return false;" Text="0"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label14" runat="server" Text='Base Amt'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtBaseAmt" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Base');return false;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label15" runat="server" Text='Discount'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtLRDiscount" runat="server" CssClass="form-control-res" Text="0" onfocus="this.select();" onchange="fncLrCalculation('Dis');return false;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label17" runat="server" Text='PCS Discount'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtLRPcsDisc" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Pcs');return false;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label18" runat="server" Text='Tax Charges'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtGrossCharge" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Tax');return false;"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label19" runat="server" Text='Rounding'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtRound" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="ga_label_left">
                                                    <asp:Label ID="Label20" runat="server" Text='Net Amount'
                                                        Font-Bold="True"></asp:Label><span class="mandatory"></span>
                                                </div>
                                                <div class="ga_label_right">

                                                    <asp:TextBox ID="txtNetAmt" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                    
                                 
               <%-- <div class="col-md-6">
                    <asp:Label ID="lblSalesMArgin" CssClass="saleslabel" runat="server" Text="Tot BillValue : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblBillValue" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label1" CssClass="saleslabel" runat="server" Text="Tot Discount : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblDis" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label3" CssClass="saleslabel" runat="server" Text="Tot Tax : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblTax" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label5" CssClass="saleslabel" runat="server" Text="Tot NetAmt : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblNet" CssClass="salesData" Style="color: red;" runat="server" Text="0.00" />
                </div>--%>
                <div style="margin-top: 25px;float:right">
                    <asp:LinkButton ID="lnkSaveNext" runat="server" class="button-green" PostBackUrl="~/Purchase/frmTextileGADashboard.aspx"><i class="icon-play"></i>View</asp:LinkButton>

                    <asp:LinkButton ID="lnkSaveCur" runat="server" class="button-red" 
                        OnClientClick="return fncSave();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>

                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue"
                        OnClientClick="fncLearAll();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                </div>
            
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9" id="divDynamic" style="display:none">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Company</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:DropDownList ID="ddlCompany1" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">EntryDate</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtEntryDate1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">InvoiceDate</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtInvDate1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Supplier</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:DropDownList ID="ddlSupplier1" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">InterState</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:CheckBox ID="chkInterState1" runat="server" Class="radioboxlist" />
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Invoice No</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtInvNo1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-1 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Bill Value</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtBillValue1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">GST %</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtExpense1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">No.of.Pcs</span>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <asp:TextBox ID="txtNoPcs1" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" Style="float: right;"
                            OnClientClick="fncAdd();return false;"><i class="icon-play"></i>Add</asp:LinkButton>
                    </div>
                </div>
             <%--   <div class="col-md-12 Cat_Margin">
                    <div class="Payment_fixed_headers Cat_Margin">
                        <table id="tblGA" cellspacing="0" rules="all" border="1" style="width: 100%;" class="GA">
                            <thead>
                                <tr>
                                    <th scope="col">InvNo
                                    </th>
                                    <th scope="col">Amount On
                                    </th>
                                    <th scope="col">Dis %
                                    </th>
                                    <th scope="col">Discount  
                                    </th>
                                    <th scope="col">Tax
                                    </th>
                                    <th scope="col">T.Value  
                                    </th>
                                    <th scope="col">NetAmount
                                    </th>
                                    <th scope="col">Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody style="overflow-y: scroll; height: 210px;">
                            </tbody>
                        </table>
                    </div>
                </div>--%>
            </div>
            <div class="col-md-3" style="display:none">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">LR No</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtLrNo1" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vehicle', 'txtLrNo', 'txtLRExpense');"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">LR Expense</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtLRExpense1" runat="server" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('LrExp');return false;" Text="0"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">Base Amt</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtBaseAmt1" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Base');return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">Discount</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtLRDiscount1" runat="server" CssClass="form-control-res" Text="0" onfocus="this.select();" onchange="fncLrCalculation('Dis');return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">PCS Discount</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtLRPcsDisc1" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Pcs');return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">Tax Charges</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtGrossCharge1" runat="server" Text="0" CssClass="form-control-res" onfocus="this.select();" onchange="fncLrCalculation('Tax');return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">Rounding</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtRound1" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-6 Cat_Margin">
                        <span class="lblBlack">Net Amount</span>
                    </div>
                    <div class="col-md-6 Cat_Margin">
                        <asp:TextBox ID="txtNetAmt1" runat="server" Text="0" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
          <%--  <div class="col-md-9 Cat_Margin">
                <div class="col-md-2 Cat_Margin">
                    <asp:CheckBox ID="chkGid" runat="server" Class="radioboxlist" Text="Closed GID" />
                </div>
                <div class="col-md-3 Cat_Margin">
                    <asp:TextBox ID="txtTranSearch" runat="server" CssClass="form-control-res" placeholder="Tran No to Search"></asp:TextBox>
                </div>
                <div class="Payment_fixed_headers Cat_Margin">
                    <table id="tblInvoice" cellspacing="0" rules="all" border="1" style="width: 100%;" class="Invoice">
                        <thead>
                            <tr>
                                <th scope="col">Company
                                </th>
                                <th scope="col">SupplierName
                                </th>
                                <th scope="col">Invoice No
                                </th>
                                <th scope="col">Date
                                </th>
                                <th scope="col">Bundles  
                                </th>
                                <th scope="col">Base
                                </th>
                                <th scope="col">Tax  
                                </th>
                                <th scope="col">Expense
                                </th>
                                <th scope="col">Total
                                </th>
                                <th scope="col">Action
                                </th>
                            </tr>
                        </thead>
                        <tbody style="overflow-y: scroll; height: 203px;">
                        </tbody>
                    </table>
                </div>
            </div>--%>
            <div class="col-md-3" style="display:none">
                <div class="col-md-6">
                    <asp:Label ID="lblSalesMArgin1" CssClass="saleslabel" runat="server" Text="Tot BillValue : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblBillValue1" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label111" CssClass="saleslabel" runat="server" Text="Tot Discount : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblDis1" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label31" CssClass="saleslabel" runat="server" Text="Tot Tax : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblTax1" CssClass="salesData" runat="server" Text="0.00" />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="Label511" CssClass="saleslabel" runat="server" Text="Tot NetAmt : " />
                </div>
                <div class="col-md-6">
                    <asp:Label ID="lblNet1" CssClass="salesData" Style="color: red;" runat="server" Text="0.00" />
                </div>
              <%--  <div style="float: right; margin-top: 50px;">
                    <asp:LinkButton ID="lnkSaveNext1" runat="server" class="button-green" PostBackUrl="~/Purchase/frmTextileGADashboard.aspx"><i class="icon-play"></i>View</asp:LinkButton>

                    <asp:LinkButton ID="lnkSaveCur1" runat="server" class="button-blue" 
                        OnClientClick="return fncSave();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>

                    <asp:LinkButton ID="lnkClearAll1" runat="server" class="button-blue"
                        OnClientClick="fncLearAll();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                </div>--%>
            </div>
        </div>
        <asp:HiddenField ID="hidTaxAmt" runat="server" />
        <asp:HiddenField ID="hidNetAmt" runat="server" />
        <asp:HiddenField ID="hidfSellingPriceRoundOff" runat="server" />
        <asp:HiddenField ID="hidSaveSata" runat="server" />
        <asp:HiddenField ID="hidVendor" runat="server" />
        <asp:HiddenField ID="hidTranNo" runat="server" />
        <asp:HiddenField ID="hidInvoiceNo" runat="server" />
        <asp:HiddenField ID="hidBillValue" runat="server" />
        <asp:HiddenField ID="hidStatus" runat="server" />
        <div class="display_none">
            <asp:Button ID="btnClick" runat="server" OnClick="lnkGo_Click" />
      
        </div>
       
    </div>
</asp:Content>
