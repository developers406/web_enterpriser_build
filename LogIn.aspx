﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogIn.aspx.cs" Inherits="EnterpriserWebFinal.LogIn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="icon" type="image/gif" href="images/favicon.jpg" />
    <%---------------- Use Auto Comoplete, Tabs Css, Datetimepicker ---------------%>
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/stylelogin.css" rel="stylesheet" type="text/css" />
    <script src="js/jsCommon.js" type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jsCommon.js") %>' type="text/javascript"></script>



    <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css" href="css/ValidationEngine.css" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src='<%= ResolveUrl("~/js/jquery-1.8.3.min.js") %>' type="text/javascript"></script>
    <%---------------- Use InventoryMaster, Auto Comoplete, Tabs, Datetimepicker ---------------%>
    <script src='<%= ResolveUrl("~/js/jquery-ui.js") %>' type="text/javascript"></script>
    <script src='<%= ResolveUrl("~/js/jquery.toast.js") %>' type="text/javascript"></script> 
    <link href="css/jquery.toast.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquerysession.js" type="text/javascript"></script>

    <%---------------- Validation ---------------%>
    <%-- <script src="js/Validation/jquery.min.js" type="text/javascript"></script>
    <script src="js/Validation/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="js/Validation/jquery.validationEngine.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        //$(function () {
        //    $("#form1").validationEngine('attach', { promptPosition: "topRight" });
        //});

        ////        //Work by saravanan To Check Java Script
        ////        function fncPassFocusOut() {
        ////            $("#hidjsCheck").val("Y");
        ////        }
        //        function fncEnterFocus(event) {
        //            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        //            if (keyCode == 13)
        //                document.getElementById("<%=btnSignIn.ClientID %>").focus();


        //        }
        //validate for form done by chidambaram - 14/11/2017
        
        $(document).ready(function () {
            ipValue();
            //var width = $(window).width(); 
            var height = $(window).height();
            $.session.set('height', height);
            //alert('' + height + "px" + '');
           // alert(width + " : " + height);
        });

        function UrlExists(url, cb) {
            jQuery.ajax({
                url: url,
                dataType: 'text',
                type: 'GET',
                complete: function (xhr) {
                    if (typeof cb === 'function')
                        cb.apply(this, [xhr.status]);
                }
            });
        }

       
        //function urlExists(url) {
        //    $.ajax({
        //        url: url,
        //        //data: JSON.stringify({ printername: zpl, base64Image: base64result, jobName: jobName }),
        //        type: "post",
        //        async: false,
        //        proccessData: false,
        //        dataType: "json",
        //        contentType: "application/json; charset=utf-8",
        //        cache: false,
        //        success: function (data) {
        //            if (data == true) {
        //                alert(data);
        //            }
        //        },
        //        error: function (jqXHR, exception) {
        //            alert(exception);
        //        }
        //    });
        //}


        var scope = "https://www.googleapis.com/auth/moderator";
        var token = '';  

        function ValidateForm() {
            if (document.getElementById("<%=txtUserName.ClientID%>").value == "") {
                ShowPopupMessageBox('Please Enter Your User Name');
                document.getElementById("<%=txtUserName.ClientID %>").focus();
                document.getElementById("<%=txtUserName.ClientID %>").style.borderColor = "#FF0000";
                return false;
            }
            else if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
                ShowPopupMessageBox('Please Enter Your Password');
                document.getElementById("<%=txtPassword.ClientID %>").focus();
                document.getElementById("<%=txtPassword.ClientID %>").style.borderColor = "#FF0000";

                return false;
            }
             
            else {
                //var url = 'https://developer.tomtom.com/maps-sdk-web-js/functional-examplessdfdsfsdfdsfdfsexamples,code,vector-map.html'; 
                //UrlExists(url, function (status) {
                //    if (status === 200) {
                //        alert(status);
                //    } else if (status === 404) {
                //        alert(status);
                //    } else {
                //        alert(status);
                //    }
                //});
                return true;
            }
    }

        function ipValue() {
        var findIP = new Promise(r=> {
            var w = window, a = new (w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({ iceServers: [] }), b = () => { };
            a.createDataChannel("");
            a.createOffer(c=>a.setLocalDescription(c, b, b), b); a.onicecandidate = c=> { try { c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r) } catch (e) { } }
        })
        findIP.then(ip =>  $("#hdIP").val(ip)).catch(e => console.error(e))
            
       
    }

    function fncClicksign(event) {
        if (event.keyCode === 13) {
            $("#<%= btnSignIn.ClientID %>").click();
                event.preventDefault();
            }
        }

        function focusUserName() {
            if (document.getElementById("<%=txtUserName.ClientID%>").value == "") {
            //ShowPopupMessageBox('Please Enter Your User Name');
            document.getElementById("<%=txtUserName.ClientID %>").style.borderColor = "#FF0000";

        }
        else {
            document.getElementById("<%=txtUserName.ClientID %>").style.borderColor = "white";
        }
    }

    function focusPassword() {
        if (document.getElementById("<%=txtPassword.ClientID%>").value == "") {
            //ShowPopupMessageBox('Please Enter Your Password');
            document.getElementById("<%=txtPassword.ClientID %>").style.borderColor = "#FF0000";

        }
        else {
            document.getElementById("<%=txtPassword.ClientID %>").style.borderColor = "white";
        }
    }
    function fncInitializeDialog(SaveMsg) {
        try {
            var Password = $('#<%=hdnPassword.ClientID %>').val();
                $('#<%=txtPassword.ClientID %>').val(Password);
                $('#<%=lnkbtnOk.ClientID %>').css("display", "block");
                $('#<%=lblremainingdays.ClientID %>').text(SaveMsg);
                $("#Save").dialog({
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncverify() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $("#License").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
               // heading: "do you",
                title: "Enterpriser Web",
                buttons:
                {
                    Yes: function () {
                        
                        $(this).dialog("close");
                        $('#<%=lblreturn.ClientID %>').text("Request Successfully Sent to Unipro.....!");
                        $('#<%=lblreturn1.ClientID %>').text("For more info Contact - 044 421 421 40");
                        $('#<%=lblreturn2.ClientID %>').text("(OR) Email - softwaresupport@uniprotech.co.in");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                    $("#<%= btnclickLicense.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },

                        No: function () {
                          $("#<%= btnclick.ClientID %>").click();
                            $(this).dialog("close");
                    }
                },
             
            });
          
        }
       
        function fncMsgbox(Msgbox)
        {
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $('#<%=lblupdate.ClientID %>').text(Msgbox);
            $("#returnmsginitial").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                title: "Enterpriser Web",
                buttons: {
                    Ok: function () {
                       $(this).dialog('close');
                        fncLicense();
                    }
                },
            });
        }
        function fncLicense() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $("#License").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                // heading: "do you",
                title: "Enterpriser Web",
                buttons:
                {
                    Yes: function () {

                        $(this).dialog("close");
                        $('#<%=lblreturn.ClientID %>').text("License is Updated Successfully.....!");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                   $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },

                      No: function () {
                    
                          $(this).dialog("close");
                          $('#<%=lblreturn.ClientID %>').text("License is Updated Successfully.....!");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                    $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },
                        
                },

            });
        }
        function fncExist(box) {
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $('#<%=lblinitial.ClientID %>').text(box);
            $("#Existmsginitial").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                title: "Enterpriser Web",
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                        fncLic();
                    }
                },
            });
        }
        function fncLic() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
             $("#License").dialog({
                 resizable: false,
                 height: 'auto',
                 width: 'auto',
                 modal: true,
                 // heading: "do you",
                 title: "Enterpriser Web",
                 buttons:
                 {
                     Yes: function () {

                         $(this).dialog("close");
                         $('#<%=lblreturn.ClientID %>').text("Your Previous Request is Pending.....!");
                         $('#<%=lblreturn1.ClientID %>').text("For more info Contact - 044 421 421 40");
                         $('#<%=lblreturn2.ClientID %>').text("(OR) Email - softwaresupport@uniprotech.co.in");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                   $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },
                      No: function () {
                        $("#<%= btnclick.ClientID %>").click();
                        $(this).dialog("close");
                    }
                },

            });
         }
        function fncDialogClose() {
            $("#Save").dialog("close");
            fncverify(); 
        }
        function fncToastInformation(msg) {
            try {
                $.toast({
                    heading: '',
                    text: msg,
                    icon: 'info',
                    loader: true,        // Change it to false to disable loader
                    loaderBg: '#9EC600',  // To change the background
                    //bgColor: '#ffb3ff',
                    bgColor: '#e49013',
                    position: 'top-right',
                    textColor: 'Black'
                })
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncExpire(msg) {
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $('#<%=lblupdate.ClientID %>').text(msg);
           $("#returnmsginitial").dialog({
               resizable: false,
               height: 'auto',
               width: 'auto',
               modal: true,
               title: "Enterpriser Web",
               buttons: {
                   Ok: function () {
                       $(this).dialog('close');
                       fncLicenseExpire();
                   }
               },
           });
        }
        function fncLicenseExpire() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $("#License").dialog({
                 resizable: false,
                height: 'auto',
                width: 'auto',
                 modal: true,
                 // heading: "do you",
                 title: "Enterpriser Web",
                 buttons:
                 {
                     Yes: function () {

                         $(this).dialog("close");
                         $('#<%=lblreturn.ClientID %>').text("Request Successfully Sent to Unipro.....!");
                         $('#<%=lblreturn1.ClientID %>').text("For more info Contact - 044 421 421 40");
                         $('#<%=lblreturn2.ClientID %>').text("(OR) Email - softwaresupport@uniprotech.co.in");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                    $("#<%= btnEx.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },

                     No: function () {
                         $("#<%= btnclick.ClientID %>").click(); 
                         $(this).dialog("close");
                     }
                 },

             });
        }
        function fncExpirepending(msg) {

            $('#<%=lblupdate.ClientID %>').text(msg);
            $("#returnmsginitial").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                title: "Enterpriser Web",
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                        fncLicenseExpirepending();
                    }
                },
            });
        }
        function fncLicenseExpirepending() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $("#License").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                // heading: "do you",
                title: "Enterpriser Web",
                buttons:
                {
                    Yes: function () {

                        $(this).dialog("close");
                        $('#<%=lblreturn.ClientID %>').text("Your Previous Request is Pending.....!");
                         $('#<%=lblreturn1.ClientID %>').text("For more info Contact - 044 421 421 40");
                         $('#<%=lblreturn2.ClientID %>').text("(OR) Email - softwaresupport@uniprotech.co.in");
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () { 
                                    $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },

                    No: function () {
                        $("#<%= btnclick.ClientID %>").click();
                         $(this).dialog("close");
                     }
                 },

             });
        }
        function fncExpireUpdated() {
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
            $('#<%=lblupdate.ClientID %>').text("Your License is Expired");
            $("#returnmsginitial").dialog({
                resizable: false,
                height: 'auto',
                width: 'auto',
                modal: true,
                title: "Enterpriser Web",
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                        fncLicenseUpdated();
                    }
                },
            });
        }
        function fncLicenseUpdated() {
            $('#<%=lblremainingdayLicense.ClientID %>').text("DO YOU WANT TO REQUEST UNIPRO ?");
            var Password = $('#<%=hdnPassword.ClientID %>').val();
            $('#<%=txtPassword.ClientID %>').val(Password);
             $("#License").dialog({
                 resizable: false,
                 height: 'auto',
                 width: 'auto',
                 modal: true,
                 // heading: "do you",
                 title: "Enterpriser Web",
                 buttons:
                 {
                     Yes: function () {

                         $(this).dialog("close");
                         $('#<%=lblreturn.ClientID %>').text("License is Updated Successfully.....!");
                        
                        $("#returnmsg").dialog({
                            resizable: false,
                            height: 'auto',
                            width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                   $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                    },

                      No: function () {
                       
                          $(this).dialog("close");
                          $('#<%=lblreturn.ClientID %>').text("License is Updated Successfully.....!");
                        
                         $("#returnmsg").dialog({
                            resizable: false,
                             height: 'auto',
                             width: 'auto',
                            modal: true,
                            title: "Enterpriser Web",
                            buttons: {
                                Ok: function () {
                                    $("#<%= btnclick.ClientID %>").click();
                                    $(this).dialog('close');

                                }
                            },
                        });

                     },
                    
                },

            });
         }
    </script>
    <script type="text/javascript">
        function ShowPopupMessageBox(message) {
            $(function () {
                $("#dialog-MessageBox").html(message);
                $("#dialog-MessageBox").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Ok: function () {
                            $(this).dialog('close');
                            fncSetFocustoObject($('#<%= txtUserName.ClientID %>'));
                        }
                    },
                    modal: true
                });
            });
        };
    </script>
    <style type="text/css">
        .button-blue {
            position: relative;
            height: 30px;
            line-height: 26px;
            padding: 0 20px;
            text-align: center;
            float: left;
            border-radius: 10px;
            font-family: "Roboto",sans-serif;
            font-weight: 500;
            font-size: 14px;
            color: #FFF;
            text-decoration: none;
            background-color: #1e82c5;
            border-bottom: 5px solid #146ba4;
            text-shadow: 0px -2px #2980B9; /* Animation */
            transition: all 0.1s;
            -webkit-transition: all 0.1s;
            top: 0px;
            left: 0px;
        }

            .button-blue:hover, .button-blue:focus {
                position: relative;
                height: 30px;
                line-height: 26px;
                padding: 0 20px;
                text-align: center;
                float: left;
                border-radius: 10px;
                font-family: "Roboto",sans-serif;
                font-weight: 500;
                font-size: 14px;
                color: #FFF;
                text-decoration: none;
                background-color: #26c538;
                border-bottom: 5px solid #06a518;
                text-shadow: 0px -2px #156748; /* Animation */
                transition: all 0.1s;
                -webkit-transition: all 0.1s;
                top: 0px;
                left: 0px;
            }

            .button-blue:active {
                transform: translate(0px,5px);
                -webkit-transform: translate(0px,5px);
                border-bottom: 1px solid;
            }

        .dialog_center {
            display: table;
            margin: auto;
            padding-top: 10px;
        }
    </style>
</head>
<body id="backgroundImage">
    <div id="dialog-MessageBox" style="display: none">
    </div>

    <div class="login-form">
        <form id="form1" runat="server">
            <div class="hiddencol"  style="display: none">
                <div id="Save">
                    <div>
                        <asp:Label ID="lblremainingdays" runat="server"></asp:Label>
                        <div class="dialog_center">
                            <asp:LinkButton ID="lnkbtnOk" runat="server" Style="display: none" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                                OnClientClick="fncDialogClose();return false;"> </asp:LinkButton>
                        </div>
                    </div>

                </div>
                 <div id="License" style="display: none">
                         <div class="lic">
                        <asp:Label ID="lblremainingdayLicense" runat="server"></asp:Label>
                         <div class="dialog_center">
                         <asp:LinkButton ID="lnkbtnYes" runat="server" Style="display: none" class="button-blue" > </asp:LinkButton>
                            <asp:LinkButton ID="lnkbtnNo" runat="server" Style="display: none" class="button-blue"> </asp:LinkButton>
                        </div>
                    </div>
                      </div>
                   <div id="returnmsg" class="dialog"  style="display: none" >
                          <div class="lic">
                        <asp:Label ID="lblreturn" style ="color:red;" runat="server"></asp:Label></div></br>
                             <div><asp:Label ID="lblreturn1" runat="server"></asp:Label></div></br>
                               <div><asp:Label ID="lblreturn2" runat="server"></asp:Label></div>
                         <div class="dialog_center">
                         <asp:LinkButton ID="lnkbtnOkmsg" runat="server" Style= "display:none" class="button-blue" > </asp:LinkButton>    
                        </div>
                    </div>
                     
                         <div id="returnmsginitial" class="dialog"   style="display: none">
                           <div class="lic">
                        <asp:Label ID="lblupdate" runat="server"></asp:Label>
                         <div class="dialog_center">
                         <asp:LinkButton ID="lnkbtnupdate" runat="server" Style= "display:none" class="button-blue" > </asp:LinkButton>    
                        </div>
                    </div>
                      </div>

                         <div id="Existmsginitial" class="dialog"  style="display: none">
                         <div >
                        <asp:Label ID="lblinitial" runat="server"></asp:Label>
                         <div class="dialog_center">
                         <asp:LinkButton ID="lnkbtnInitial" runat="server" Style= "display:none" class="button-blue" > </asp:LinkButton>    
                        </div>
                    </div>
                      </div>
                
                 
            </div>

            <asp:Button ID="btnclick" runat="server" Style="display:none;" OnClick="btnremainingday_Click" />
            <asp:Button ID="btnclickLicense" runat="server" Style="display:none;" OnClick=" btnremainingdayLicense_Click" />
           <asp:Button ID="btnEx" runat="server" Style="display:none;" OnClick="btnExpire_Click" />

            <div style="text-align: center">
                <img alt="Uniprotech" src="images/CompanyLogo.png" />

            </div>
            <div class="form-group " style="margin-top: 15px">


                <asp:TextBox ID="txtUserName" runat="server" MaxLength="30"
                    placeholder="User Name" class="form-control" autocomplete="off"></asp:TextBox>
                <i class="fa fa-user"></i>
            </div>
            <div class="form-group log-status">
                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" onkeypress=" return fncClicksign(event);" autocomplete="off" TextMode="Password" MaxLength="30"
                    class="form-control"></asp:TextBox>
                <i class="fa fa-lock"></i>
            </div>
            <div>
                <%--validate for form done by chidambaram - 14/11/2017--%>
                <asp:Button ID="btnSignIn" class="log-btn" runat="server" Text="Sign In"
                    OnClick="btnSignIn_Click" OnClientClick="return ValidateForm()" />
            </div>
            
            <asp:HiddenField ID="hdIP" runat="server" />
            <asp:HiddenField ID="hdTitle" Value="" runat="server" />
            <asp:HiddenField ID="hdnUserName" Value="" runat="server" />
            <asp:HiddenField ID="hdnPassword" Value="" runat="server" />
        </form>
        <%--Footer for form done by chidambaram - 14/11/2017--%>
        <footer class="page-footer">
          <div class="footer-copyright">
            <div  style = "text-align : center">
           Copyright © 2020 UniproTech Solutions Pvt Ltd.
               <div style = "clear:both; text-align:center">ALL RIGHTS RESERVED  </div>
            </div>
          </div>
  </footer>
    </div>


    <%--<script type="text/javascript" src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>--%>
    <%--<script type="text/javascript" src="js/index.js"></script>--%>
    <script type="text/javascript">
        //function pageLoad(){

        document.title = document.getElementById("<%=hdTitle.ClientID %>").value;
        //}
    </script>
</body>
</html>
