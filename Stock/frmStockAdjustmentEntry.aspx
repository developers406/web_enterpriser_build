﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmStockAdjustmentEntry.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockAdjustmentEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }


        .stockAdjustmentEntry td:nth-child(1), .stockAdjustmentEntry th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .stockAdjustmentEntry td:nth-child(2), .stockAdjustmentEntry th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }


        .stockAdjustmentEntry td:nth-child(2) {
            text-align: center !important;
        }

        .stockAdjustmentEntry td:nth-child(3), .stockAdjustmentEntry th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(4), .stockAdjustmentEntry th:nth-child(4) {
            min-width: 355px;
            max-width: 355px;
        }

        .stockAdjustmentEntry td:nth-child(5), .stockAdjustmentEntry th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(6), .stockAdjustmentEntry th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(7), .stockAdjustmentEntry th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }


        .stockAdjustmentEntry td:nth-child(7) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(8), .stockAdjustmentEntry th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(8) {
            text-align: right;
        }

        .stockAdjustmentEntry td:nth-child(9), .stockAdjustmentEntry th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(10), .stockAdjustmentEntry th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(10) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(11), .stockAdjustmentEntry th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(11) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(12), .stockAdjustmentEntry th:nth-child(12) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(13), .stockAdjustmentEntry th:nth-child(13) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(14), .stockAdjustmentEntry th:nth-child(14) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(15), .stockAdjustmentEntry th:nth-child(15) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(16), .stockAdjustmentEntry th:nth-child(16) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(17), .stockAdjustmentEntry th:nth-child(17) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(18), .stockAdjustmentEntry th:nth-child(18) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(19), .stockAdjustmentEntry th:nth-child(19) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(20), .stockAdjustmentEntry th:nth-child(20) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(21), .stockAdjustmentEntry th:nth-child(21) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(22), .stockAdjustmentEntry th:nth-child(22) {
            display: none;
        }



        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }

        .WPrice {
            width: 60%;
        }

        .Exp {
            width: 70%;
        }

        .ui-autocomplete {
            max-height: 200px !important;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 20px;
        }

        .ui-dialog-titlebar-close {
            visibility: hidden;
        }
    </style>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'StockAdjustmentEntry');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "StockAdjustmentEntry";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        var lastRowNo = 0, rowTotalValue = 0, tblBatchBody;
        var oldMRP = 0, InventoryCodeDEL, AlExpDate;
        var MRPDEL = "";
        function pageLoad() {
            try {
                $('#<%=txtItemCode.ClientID%>').focus();
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }
                var newOption = {};
                var option = '';
                $("#<%=ddlSort.ClientID %>").empty();

                $('#tblStockAdjustment th').each(function (e) {
                    var index = $(this).index();
                    var table = $(this).closest('table');
                    var val = $.trim(table.find('.click th').eq(index).text());

                    if ($(this).is(":visible")) {
                        if (val != "Qty") {
                            if (val != "Delete")
                                option += '<option value="' + val + '">' + val + '</option>';
                        }
                    }
                });
                $("#<%=ddlSort.ClientID %>").append(option);
                $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");
                $("select").chosen({ width: '100%' });

                $('#lblUp').live('click', function (event) {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "green");
                    var length = $("#tblStockAdjustment tbody").children().length;
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex; //
                    if (length > 0) {
                        var tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                        if (columnIndex == 0) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                            //tdArray.push($('#tblStockAdjustment').find('input'));
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                            });
                        }
                        else if (columnIndex == 4 || columnIndex == 5 || columnIndex == 7 || columnIndex == 8) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                            //tdArray.push($('#tblStockAdjustment').find('input'));
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                            });
                        }
                        else if (columnIndex == 1 || columnIndex == 2) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData < nData ? -1 : 1;
                            });
                        }
                        else if (columnIndex == 6) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData < nData ? -1 : 1;
                            });
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#tblStockAdjustment').append(row);
                        });
                    }
                    else {
                        $('#lblDown').css("color", "");
                        $('#lblUp').css("color", "");
                        ShowPopupMessageBox("Invalid Records");
                        return false;
                    }


                });
                $('#lblDown').live('click', function (event) {
                    $('#lblDown').css("color", "green");
                    $('#lblUp').css("color", "");
                    var length = $("#tblStockAdjustment tbody").children().length;
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
                    var tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                    if (length > 0) {

                        if (columnIndex == 0) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                            //tdArray.push($('#tblStockAdjustment').find('input'));
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                            });
                        }
                        else if (columnIndex == 4 || columnIndex == 5 || columnIndex == 7 || columnIndex == 8) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                            //tdArray.push($('#tblStockAdjustment').find('input'));
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                            });
                        }//if (columnIndex == 5) {
                        //    var table = $('#tblStockAdjustment');
                        //    var rows = table.find('tr td:eq(5) input').toArray().sort(comparer($(this).index()));
                        //    this.asc = !this.asc;
                        //    if (!this.asc) {
                        //        rows = rows.reverse();
                        //    }
                        //    table.append(rows);

                        //}
                        else if (columnIndex == 1 || columnIndex == 2) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData > nData ? -1 : 1;
                            });
                        }
                        else if (columnIndex == 6) {
                            tdArray = $('#tblStockAdjustment').closest("table").find("tr td:nth-child(" + (columnIndex + 3) + ")");
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData > nData ? -1 : 1;
                            });
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#tblStockAdjustment').append(row);
                        });

                    }
                    else {
                        $('#lblDown').css("color", "");
                        $('#lblUp').css("color", "");
                        ShowPopupMessageBox("Invalid Records");
                        return false;
                    }

                });

                $("#thBatchNo").text($('#<%=hidBatchText.ClientID%>').val());
                $('#<%=txtQty.ClientID %>').val('1');
                $('#<%=lblBatchNo.ClientID%>').text($('#<%=hidBatchText.ClientID%>').val());
                $('#thStkBatchNo').text($('#<%=hidBatchText.ClientID%>').val());
                $("#<%= txtExpiredDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" });
                if ($('#<%=hidPrrList.ClientID %>').val() != "") {
                    fncprrlist();
                }
                $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                $("#<%=txtItemCode.ClientID%>").focus();
                $('#<%=txtStockAdjustmentDate.ClientID %>').val(currentDateformat_Master);
                lastRowNo = 0;
                fncDecimal();
                fncNumberFormat();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function comparer(index) {
            return function (a, b) {
                var valA = getCellValue(a, index),
                    valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ?
                    valA - valB : valA.localeCompare(valB);
            };
        }

        function getCellValue(row, index) {
            return $(row).children('td').eq(index).text();
        }

        $(function () {
            SetAutoCompleteForItemSearch();
        });

        function sortTable() {
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("myTable");
            switching = true;
            /*Make a loop that will continue until
            no switching has been done:*/
            while (switching) {
                //start by saying: no switching is done:
                switching = false;
                rows = table.rows;
                /*Loop through all table rows (except the
                first, which contains table headers):*/
                for (i = 1; i < (rows.length - 1); i++) {
                    //start by saying there should be no switching:
                    shouldSwitch = false;
                    /*Get the two elements you want to compare,
                    one from current row and one from the next:*/
                    x = rows[i].getElementsByTagName("TD")[0];
                    y = rows[i + 1].getElementsByTagName("TD")[0];
                    //check if the two rows should switch place:
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                if (shouldSwitch) {
                    /*If a switch has been marked, make the switch
                    and mark that a switch has been done:*/
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }

        ///Load Stock Adjustment
        $(document).ready(function () {
            if ($('#<%=txtStkAdjNo.ClientID %>').val() != "") {

                var objStkAdjustment, tblStkAdjBody;
                objStkAdjustment = jQuery.parseJSON($('#<%=hidJsonStockDetail.ClientID %>').val());
                var tblStkBody = $("#tblStockAdjustment tbody");
                tblStkBody.children().remove();
                if (objStkAdjustment.length > 0) {
                    for (var i = 0; i < objStkAdjustment.length; i++) {
                        row = "<tr><td> " +
                            objStkAdjustment[i]["RowNo"] + "</td><td>" +
                            "<img alt='Delete' src='../images/No.png' /></td><td >" +
                            objStkAdjustment[i]["InventoryCode"] + "</td><td >" +
                            objStkAdjustment[i]["Description"] + "</td><td  >" +
                            objStkAdjustment[i]["PrcCode"] + "</td><td  >" +
                            objStkAdjustment[i]["Qty"] + "</td><td  >" +
                            objStkAdjustment[i]["UnitPrice"] + "</td><td  >" +
                            objStkAdjustment[i]["NetAmount"] + "</td><td  >" +
                            objStkAdjustment[i]["BatchNo"] + "</td><td>" +
                            objStkAdjustment[i]["MRP"] + "</td><td >" +
                            objStkAdjustment[i]["SPrice"] + "</td></tr>";
                        tblStkBody.append(row);
                    }
                }
            }
        });
        //Get Inventory code
        function fncGetInventoryCode() {
            try {
                $('#<%=hidInvLength.ClientID %>').val($('#<%=txtItemCode.ClientID %>').val());
                $('#<%=lnkNewBatch.ClientID %>').show();
                var obj = {};
                obj.code = $('#<%=txtItemCode.ClientID %>').val();
                obj.formName = 'StkAdj';

                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var objStk = jQuery.parseJSON(msg.d); //skip create new medical batch surya20211111
                        if ($.trim(objStk[0]["BatchNo"]) == 'MEDBATCH') { 
                            $('#<%=lnkNewBatch.ClientID %>').hide();
                        }
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });


            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncAssignValuestoTextBox(msg) {
            try {
                var inputNumber = $('#<%=hidInvLength.ClientID %>').val();
                var regexp = /^[0-9]*$/;
                var numberCheck = regexp.exec(inputNumber);
                var objStk, row;
                objStk = jQuery.parseJSON(msg.d);
                $('#<%=txtItemCode.ClientID %>').attr('disabled', 'disabled');
                if (objStk.length > 0) {

                    if (objStk.length == 1 && numberCheck != null && ($('#<%=hidInvLength.ClientID %>').val().length == 15
                        || $('#<%=hidInvLength.ClientID %>').val().length == 12)) {
                        if (objStk[0]["BatchNo"] != "") {
                            $('#<%=txtItemCode.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                            $('#<%=txtBatchNo.ClientID %>').val($.trim(objStk[0]["BatchNo"]));
                            $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                            $('#<%=txtCurrentStock.ClientID %>').val($.trim(objStk[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objStk[0]["Cost"]));
                            $('#<%=txtMRP.ClientID %>').val($.trim(objStk[0]["MRP"]));
                            $('#<%=txtSPrice.ClientID %>').val($.trim(objStk[0]["SellingPrice"]));
                            $('#<%=txtQty.ClientID %>').focus();
                        }
                        else if (objStk[0]["BatchNo"] == "") {
                            $('#<%=txtItemCode.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                            $('#<%=txtBatchNo.ClientID %>').val('');
                            $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                            $('#<%=txtCurrentStock.ClientID %>').val($.trim(objStk[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objStk[0]["Cost"]));
                            $('#<%=txtMRP.ClientID %>').val($.trim(objStk[0]["MRP"]));
                            $('#<%=txtSPrice.ClientID %>').val($.trim(objStk[0]["SellingPrice"]));
                            $('#<%=txtBatchNo.ClientID %>').attr("disabled", true);
                            $('#<%=txtQty.ClientID %>').focus();
                        }
                    }
                    else if (objStk[0]["BatchNo"] == "") {
                        $('#<%=txtItemCode.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                        $('#<%=txtBatchNo.ClientID %>').val('');
                        $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                        $('#<%=txtCurrentStock.ClientID %>').val($.trim(objStk[0]["Qty"]));
                        $('#<%=txtCost.ClientID %>').val($.trim(objStk[0]["Cost"]));
                        $('#<%=txtMRP.ClientID %>').val($.trim(objStk[0]["MRP"]));
                        $('#<%=txtSPrice.ClientID %>').val($.trim(objStk[0]["SellingPrice"]));
                        if (objStk[0]["Lqty"] != undefined)
                            $('#<%=txtQty.ClientID%>').val(parseFloat(parseFloat(objStk[0]["Lqty"].trim()) / 1000).toFixed(3));

                        $('#<%=txtBatchNo.ClientID %>').attr("disabled", true);
                        $('#<%=txtQty.ClientID %>').focus();

                    }
                    else {
                        tblBatchBody = $("#tblBatch tbody");
                        tblBatchBody.children().remove();
                        for (var i = 0; i < objStk.length; i++) {

                            var gst = objStk[i]["ITaxPer3"];
                            $('#<%=hidGstamt.ClientID %>').val(gst);
                            row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                $.trim(objStk[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                $.trim(objStk[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" +
                                objStk[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                                objStk[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                                objStk[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                objStk[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                $.trim(objStk[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                                $.trim(objStk[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["AllowExpireDate"]) + "</td><td id='tdGstamt_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ITaxPer3"]).toFixed(2) + "</td></tr>";

                            tblBatchBody.append(row);

                        }
                        //<td id='tdMRPDEL_ ' style ='display:none'" + i + "' >" +
                        //(objStk[i]["MRPDEL"]) + "</td>

                        fncInitializeBatchDetail();
                        //tblBatchBody.children().first().css("background-color", "Yellow");
                        tblBatchBody.children().dblclick(fncBatchRowClick);

                        tblBatchBody[0].setAttribute("style", "cursor:pointer");

                        tblBatchBody.children().on('mouseover', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "Yellow");
                        });

                        tblBatchBody.children().on('mouseout', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "white");
                        });

                        tblBatchBody.children().on('keydown', function () {
                            fncBatchKeyDown($(this), event);
                        });

                        $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                        $("#tblBatch tbody > tr").first().focus();

                    }
                }
                fncBatchDeleteCheck();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        function fncBatchDeleteCheck() {
            try {
                var obj = {}
                obj.Inventorycode = $('#<%=txtItemCode.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmStockAdjustmentEntry.aspx/fncGetInventoryDetail_BatchDelete")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBatchDeleteInventories(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Initialize Batch Detail
        function fncInitializeBatchDetail() {
            try {
                $("#stkadj_batch").dialog({
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true,
                    title: "Batch Detail",
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                            fncClear();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //function fncBatchKeyDown(rowobj, evt) {
        //    try {
        //        //var rowobj = $(this);
        //        var charCode = (evt.which) ? evt.which : evt.keyCode;

        //        if (charCode == 13) {
        //            rowobj.dblclick();
        //        }
        //        else if (charCode == 40) {
        //            var NextRowobj = rowobj.next();
        //            if (NextRowobj.length > 0) {
        //                NextRowobj.css("background-color", "Yellow");
        //                NextRowobj.siblings().css("background-color", "white");
        //                NextRowobj.select().focus();
        //            }                   
        //        }
        //        else if (charCode == 38) {
        //            var prevrowobj = rowobj.prev();
        //            if (prevrowobj.length > 0) {
        //                prevrowobj.css("background-color", "Yellow");
        //                prevrowobj.siblings().css("background-color", "white");
        //                prevrowobj.select().focus();
        //            }                   
        //        }
        //    }
        //    catch (err) {
        //        ShowPopupMessageBox(err.message);
        //    }
        //}

        ////Batch Inventory Values Assign to Text Box

        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {

            var stock;
            try {

                if (BatchNo == "NewBatch") {
                    $("#<%=txtCost.ClientID%>").attr("disabled", false);
                    $("#<%=txtMRP.ClientID%>").attr("disabled", false);
                    $("#<%=txtSPrice.ClientID%>").attr("disabled", false);
                    $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                    stock = "0.00";
                }
                else {
                    $("#<%=txtCost.ClientID%>").attr("disabled", true);
                    $("#<%=txtMRP.ClientID%>").attr("disabled", true);
                    $("#<%=txtSPrice.ClientID%>").attr("disabled", true);
                    $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                    stock = $.trim($(rowObj).find('td[id*=tdStock]').text());
                }
                oldMRP = $.trim($(rowObj).find('td[id*=tdMRPNew]').text());
                //  MRPDEL = $.trim($(rowObj).find('td[id*=tdMRPDEL]').text());
                AlExpDate = $.trim($(rowObj).find('td[id*=tdAllowExpDate]').text());
                $('#<%=txtItemCode.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));
                $('#<%=txtBatchNo.ClientID %>').val(BatchNo);
                $('#<%=txtDescription.ClientID %>').val($.trim($(rowObj).find('td[id*=tdDesc]').text()));
                $('#<%=txtCurrentStock.ClientID %>').val(stock);
                $('#<%=txtCost.ClientID %>').val($.trim($(rowObj).find('td[id*=tdCost]').text()));
                $('#<%=txtMRP.ClientID %>').val($.trim($(rowObj).find('td[id*=tdMRPNew]').text()));
                $('#<%=txtSPrice.ClientID %>').val($.trim($(rowObj).find('td[id*=tdSellingPrice]').text()));
                $('#<%=txtW1.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice1]').text()));
                $('#<%=txtW2.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice2]').text()));
                $('#<%=txtW3.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice3]').text()));

                $('#<%=hidAllowExpDate.ClientID %>').val($.trim($(rowObj).find('td[id*=tdAllowExpDate]').text()));
                $("#stkadj_batch").dialog('close');

                var d = new Date($.trim($(rowObj).find('td[id*=tdExpDate]').text()));
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "-" + month + "-" + year;

                $('#<%=txtExpiredDate.ClientID %>').val(date);

                if ($('#<%=hidAllowExpDate.ClientID %>').val() == "true") {

                    $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                    if (BatchNo == "NewBatch") {
                        $('#<%=txtBatchNo.ClientID %>').val("");
                        $('#divTextMedBatch').show();
                        $('#divlabelMedBatch').show();
                    }
                    else {
                        $('#divTextMedBatch').hide();
                        $('#divlabelMedBatch').hide();
                    }

                    $('#<%=txtQty.ClientID %>').focus();
                    $('#<%=divExpired.ClientID %>').show();

                    if ($('#<%=hidParamWhole.ClientID%>').val() == "Y") {
                        if (BatchNo == "NewBatch") {
                            $('#<%=txtBatchNo.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtQty.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtQty.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtCost.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtCost.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtMRP.ClientID %>').focus();
                            }
                        });

                        $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtSPrice.ClientID %>').focus();
                            }
                        });

                        $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtSPrice.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtSPrice.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtW1.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtW1.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtW2.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtW2.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtW3.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtW3.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtExpiredDate.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                if ($('#divTextMedBatch').is(":visible"))
                                    $('#<%=txtMedBatch.ClientID %>').focus();
                                else
                                    $('#<%=lnkAdd.ClientID %>').focus();
                            }
                        });
                            $('#<%=txtMedBatch.ClientID %>').keydown(function (event) {
                                if (event.keyCode == 13) {
                                        $('#<%=lnkAdd.ClientID %>').focus();
                                }
                            });

                    }
                    else if (BatchNo != "NewBatch" && BatchNo != "") {
                        $('#<%=txtQty.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=txtExpiredDate.ClientID %>').focus();
                            }
                        });

                        $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                if ($('#divTextMedBatch').is(":visible"))
                                    $('#<%=txtMedBatch.ClientID %>').focus();
                                else
                                    $('#<%=lnkAdd.ClientID %>').focus();
                            }
                        });
                        $('#<%=txtMedBatch.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) {
                                $('#<%=lnkAdd.ClientID %>').focus();
                            }
                        });
                    }
                }
                else if (BatchNo == "NewBatch") {
                    $('#<%=txtBatchNo.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtQty.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtQty.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtCost.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtCost.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtMRP.ClientID %>').focus();
                        }
                    });


                    $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtSPrice.ClientID %>').focus();
                        }
                    });

                    $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtSPrice.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtSPrice.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtExpiredDate.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            if ($('#divTextMedBatch').is(":visible"))
                                $('#<%=txtMedBatch.ClientID %>').focus();
                            else
                                $('#<%=lnkAdd.ClientID %>').focus();
                        }
                    });
                        $('#<%=txtMedBatch.ClientID %>').keydown(function (event) {
                            if (event.keyCode == 13) { 
                                     $('#<%=lnkAdd.ClientID %>').focus();
                             }
                         });
                    $('#<%=txtBatchNo.ClientID %>').focus();
                }
                else if (BatchNo != "NewBatch" && BatchNo != "") {
                    $('#<%=txtQty.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtExpiredDate.ClientID %>').focus();
                        }
                    });

                    $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            if ($('#divTextMedBatch').is(":visible"))
                                $('#<%=txtMedBatch.ClientID %>').focus();
                            else
                                $('#<%=lnkAdd.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtMedBatch.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=lnkAdd.ClientID %>').focus();
                        }
                    });
                    }

                } else if ($('#<%=hidParamWhole.ClientID%>').val() == "Y") {
                    if (BatchNo == "NewBatch") {

                        $('#<%=txtBatchNo.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtQty.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtQty.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtCost.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtCost.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtMRP.ClientID %>').focus();
                        }
                    });

                    $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtSPrice.ClientID %>').focus();
                        }
                    });

                    $('#<%=txtMRP.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtSPrice.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtSPrice.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtW1.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtW1.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtW2.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtW2.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtW3.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtW3.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            $('#<%=txtExpiredDate.ClientID %>').focus();
                        }
                    });
                    $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) {
                            if ($('#divTextMedBatch').is(":visible"))
                                $('#<%=txtMedBatch.ClientID %>').focus();
                            else
                                $('#<%=lnkAdd.ClientID %>').focus();
                        }
                    });
                        $('#<%=txtMedBatch.ClientID %>').keydown(function (event) {
                        if (event.keyCode == 13) { 
                                $('#<%=lnkAdd.ClientID %>').focus();
                             }
                         });
                    $('#<%=txtBatchNo.ClientID %>').focus();
                } else {

                    $('#<%=txtQty.ClientID %>').focus();
                    }
                }


                else {
                    $('#<%=txtQty.ClientID %>').focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Clear All Text Box
        function fncClear() {
            try {

                $('#<%=txtItemCode.ClientID %>').val('');
                $('#<%=txtItemCode.ClientID %>').removeAttr('disabled');
                $('#<%=txtBatchNo.ClientID %>').val('');
                $('#<%=txtDescription.ClientID %>').val('');
                $('#<%=txtCurrentStock.ClientID %>').val('');
                $('#<%=txtCost.ClientID %>').val('0');
                $('#<%=txtMRP.ClientID %>').val('0');
                $('#<%=txtSPrice.ClientID %>').val('0');
                $('#<%=txtTotal.ClientID %>').val('0');
                $('#<%=txtQty.ClientID %>').val('1');
                <%--$('#<%=txtW1.ClientID %>').val('0');
                $('#<%=txtW2.ClientID %>').val('0');
                $('#<%=txtW3.ClientID %>').val('0');--%>
                $('#<%=divWprice.ClientID %>').hide();
                $('#<%=divExpired.ClientID %>').hide();
                $("#<%=txtBatchNo.ClientID%>").attr("disabled", false);

                $('#<%=ddlPrcCode.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlPrcCode.ClientID %>').trigger("liszt:updated");
                $('#<%=txtItemCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Total Value Calculation
        function fncTotalValueCalc() {
            try {
                var totalvalue = 0, prcCode;
                prcCode = $('#<%=ddlPrcCode.ClientID %>').val();
                if (prcCode == "PRC1") {
                    totalvalue = parseFloat($('#<%=txtCost.ClientID %>').val()) * parseFloat($('#<%=txtQty.ClientID %>').val());
                    $('#<%=txtTotal.ClientID %>').val(totalvalue.toFixed(2));
                }
                else {
                    $('#<%=txtTotal.ClientID %>').val('0.00');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Inventory Search For Qualifying Item
        function SetAutoCompleteForItemSearch() {
            $("[id$=txtItemCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                focus: function (event, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtQty.ClientID %>').focus();
                    $('#<%=txtItemCode.ClientID %>').attr('disabled', 'disabled');
                    fncGetInventoryCode();
                    return false;
                },
                minLength: 1
            });
            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtItemCode'
                });
            });
        }
        ///Item Add To 
        function fncAddItemToTable() {

            var tempmrp = 0;
            var tempmrpdel = 0;
            try {
                var tblStkBody, row, batchNo="", infor = "";
                var netTotal = 0;

                tempmrp = $('#<%=txtMRP.ClientID%>').val();
                tempmrpdel = $('#<%=txtMRP.ClientID%>').val();
                if (fncAddValidation() == false) {
                    //fncClear();
                    return false;
                }
                else if (AlExpDate == "true") {
                    if ($('#<%=txtBatchNo.ClientID %>').val() == "NewBatch") {
                        if (oldMRP == Number.parseFloat(tempmrp).toFixed(2)) {
                            ShowPopupMessageBoxandFocustoObject("Please Change Mrp");
                            $('#<%=txtMRP.ClientID%>').focus();
                            return false;
                        }
                    }
                    <%--else
                        batchNo = $('#<%=txtBatchNo.ClientID %>').val();--%>
                }


                else if ($('#<%=txtBatchNo.ClientID %>').val() == "NewBatch") {
                    if (oldMRP == Number.parseFloat(tempmrp).toFixed(2)) {
                        ShowPopupMessageBoxandFocustoObject("Please Change Mrp");
                        //fncClear();
                        return false;
                    }
               
                   
            else if (MRPDEL != "") {
                MRPDEL = MRPDEL.slice(0, -1);
                var MrpDelete = {};
                MrpDelete = MRPDEL.split(',');
                for (var i = 0; i < MrpDelete.length; i++) {
                    if (MrpDelete[i] == Number.parseFloat(tempmrpdel)) {
                        infor = "Please activate this item.<br/> InventoryCode :" + $('#<%=txtItemCode.ClientID%>').val().trim();
                                infor = infor + "  MRP :" + Number.parseFloat(tempmrpdel).toFixed(2);
                                ShowPopupMessageBoxandFocustoObject(infor);
                                fncClear();
                                return false;
                            }
                        }
                    }
                    if (parseFloat($('#<%=txtMRP.ClientID %>').val()) > 9999) {
                        batchNo = Math.trunc(parseFloat($('#<%=txtMRP.ClientID %>').val())) / 1000000;
                batchNo = batchNo + "00000000";
            }
            else {
                batchNo = Math.trunc(parseFloat($('#<%=txtMRP.ClientID %>').val()) * 100) / 1000000;
                        batchNo = batchNo + "00000000";
                    }
                    batchNo = batchNo.substr(2, 6)
                }
                else
                   // batchNo = $('#<%=txtBatchNo.ClientID %>').val();
                if (batchNo == undefined || $('#<%=hidAllowExpDate.ClientID %>').val() == "true" && $('#divTextMedBatch').is(":visible") && batchNo == "") {
                    batchNo = $('#<%=txtMedBatch.ClientID %>').val();
                }
                else {
                    //batchNo = $('#<%=txtBatchNo.ClientID %>').val();
                }
                tblStkBody = $("#tblStockAdjustment tbody");
                if (tblStkBody.children().length > 0) {
                    lastRowNo = $('#tblStockAdjustment tr:last').find("td").html().trim();
                    lastRowNo = parseFloat(lastRowNo) + 1;
                }
                else
                    lastRowNo = parseFloat(lastRowNo) + 1;

                 if ($('#<%=txtBatchNo.ClientID %>').val() == "MEDBATCH") {
                    batchNo = "MEDBATCH";

                }
                if (batchNo == "") {
                    batchNo = $('#<%=txtBatchNo.ClientID %>').val();

                }

                var gstamt = $('#<%=txtCost.ClientID %>').val() * $('#<%=hidGstamt.ClientID %>').val() / 100 * $('#<%=txtQty.ClientID %>').val();
                $('#<%=hidGstamt.ClientID %>').val(gstamt);

                row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                    lastRowNo + "</td><td>" +
                    "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                    $('#<%=txtItemCode.ClientID %>').val() + "</td><td id='tdDesc_" + lastRowNo + "'  dblclick='alert()'>" +
                    $('#<%=txtDescription.ClientID %>').val() + "</td><td id='tdPrcCode_" + lastRowNo + "' >" +
                    $('#<%=ddlPrcCode.ClientID %>').val() + "</td><td id='tdQty_" + lastRowNo + "' >" +
                    "<input id='txtQty_" + lastRowNo + "' type='text' Class='form-control-res-right' onkeypress=' return fncMoveNextRow(event,this);'  onfocus='fncGetRowTotalValue(this);return false;' onblur='fncTotalValueCalcForRowChange(this);return false;'  value='" + $('#<%=txtQty.ClientID %>').val() + "'>" + "</td><td id='tdCost_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtCost.ClientID %>').val()).toFixed(2) + "</td><td id='tdTotalValue_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtTotal.ClientID %>').val()).toFixed(2) + "</td><td id='tdBatchNo_" + lastRowNo + "' >" +
                    batchNo + "</td><td id='tdMRP_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtMRP.ClientID %>').val()).toFixed(2) + "</td><td id='tdSellingPrice_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtSPrice.ClientID %>').val()).toFixed(2) + "</td><td id='tdGstamt_" + lastRowNo + "' >" +
                    parseFloat($('#<%=hidGstamt.ClientID %>').val()).toFixed(2) + "</td><td id='tdPriceCode_" + lastRowNo + "' >" +

                    $('#<%=ddlPrcCode.ClientID %>').val() + "</td><td id='tdActBatchNo_" + lastRowNo + "' >" +
                    batchNo + "</td><td id='tdOldQty_" + lastRowNo + "' >" +
                    $('#<%=txtCurrentStock.ClientID %>').val() + "</td><td id='tdWPrice1_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtW1.ClientID %>').val()).toFixed(2) + "</td><td id='tdWPrice2_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtW2.ClientID %>').val()).toFixed(2) + "</td><td id='tdWPrice3_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtW3.ClientID %>').val()).toFixed(2) + "</td><td id='tdExpiredDate_" + lastRowNo + "' >" +
                    $('#<%=txtExpiredDate.ClientID %>').val() + "</td><td id='tdMedBatch_" + lastRowNo + "' >" +
                    $('#<%=txtMedBatch.ClientID %>').val() + "</td></tr>";


                tblStkBody.append(row);
                //fncTotalValueCalcForRowChange(this);


                netTotal = $('#<%=txtNetTotal.ClientID %>').val();
                netTotal = parseFloat(netTotal) + parseFloat($('#<%=txtTotal.ClientID %>').val());
                $('#<%=txtNetTotal.ClientID %>').val(netTotal.toFixed(2));
                fncSaveStockAdjEntryTemporarly();
                fncClear();
                //tblStkBody.children().dblclick(fncVenItemRowdblClk);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncUpdate(inv, bat, mrp, Qty, QtyNew, tbltotal, txttotal, net) {
            try {
                var inventory = inv;
                var batch = bat;
                var mrp = mrp;
                var qty = Qty;
                var QtyNew = QtyNew;
                var tbltotal = tbltotal;
                var txttotal = txttotal;
                var net = net;
                var total = parseFloat(tbltotal) + parseFloat(txttotal);
                var totalQty = parseFloat(qty) + parseFloat(QtyNew);

                //$('#tblStockAdjustment rowID #txtQty').val('XXX')
                $("#tblStockAdjustment tbody").children().each(function () {
                    var cells = $("td", this);
                    var qtys = $("td:eq(5) input[type='text']", this);
                    if (inventory == cells.eq(2).text().trim() && batch == cells.eq(8).text().trim() &&
                        parseFloat(mrp) == cells.eq(9).text().trim()) {
                        qtys.val(totalQty);
                        cells.eq(7).text(total.toFixed(2));
                        var nettotal = parseFloat(net) + parseFloat(txttotal);
                        $('#<%=txtNetTotal.ClientID %>').val(nettotal.toFixed(2));
                    }
                });
                $('#<%=txtItemCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        <%--function fncMRPCheck() {
            
            try {
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/frmStockAdjustmentEntry.asmx/fncGetCompareMrp")%>',
                    data: "{ 'mrp': '" + $('#<%=txtMRP.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        //  
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>
        //Item to Validation
        function fncAddValidation() {

            try {

                var itemCode, batchNo, mrp, duplicateStatus = true;

                var MRP = parseFloat($('#<%=txtMRP.ClientID %>').val());
                var SP = parseFloat($('#<%=txtSPrice.ClientID %>').val());    
                var BC = parseFloat($('#<%=txtCost.ClientID %>').val());
                


                if (MRP < SP) {
                    fncToastInformation("MRP Should Greater than Sellingprice");
                    return false;
                }
                else if (SP < BC) {
                    fncToastInformation("Selling Should Greater than Basic");
                    return false;
                }
               
                if ($('#<%=txtItemCode.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_EnterItem%>');
                    return false;
                }
                else if ($('#<%=txtQty.ClientID %>').val() == "" || parseFloat($('#<%=txtQty.ClientID %>').val()) == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_EnterQty%>');
                    return false;
                }

                else if ($('#<%=txtTotal.ClientID %>').val() == "" || parseFloat($('#<%=txtTotal.ClientID %>').val()) == 0) {
                    ShowPopupMessageBox("Please Enter Valid Details");
                    return false;
                }
                else if ($('#<%=hidAllowExpDate.ClientID %>').val() == "true") {
                    if ($('#<%=txtExpiredDate.ClientID %>').val() == "") {
                        ShowPopupMessageBox("Please Enter Expired Date.");
                        return false;
                    }
                    else if ($('#<%=txtMedBatch.ClientID %>').val() == "" && $('#divTextMedBatch').is(":visible")) {
                        ShowPopupMessageBox("Please Enter Medical Batch No.");
                        return false;
                    }
                }
                else {
                    $("#tblStockAdjustment tbody").children().each(function () {
                        itemCode = $(this).find('td[id*=tdItemcode]').text();
                        batchNo = $(this).find('td[id*=tdBatchNo]').text();
                        mrp = $(this).find('td[id*=tdMRP]').text();
                        var Qty = $(this).find("td:eq(5) input[type='text']").val();
                        var QtyNew = $('#<%=txtQty.ClientID %>').val();
                        var tblTotal = $(this).find('td[id*=tdTotalValue]').text();
                        var txtTotal = $('#<%=txtTotal.ClientID %>').val();
                        var net = $('#<%=txtNetTotal.ClientID %>').val();
                        if (itemCode == $('#<%=txtItemCode.ClientID %>').val() && batchNo == $('#<%=txtBatchNo.ClientID %>').val() &&
                            parseFloat(mrp) == parseFloat($('#<%=txtMRP.ClientID %>').val())) {
                         <%--   //$(this).find('td[id*=tdQty]').text() = "0";
                            if ($('#<%=hidQtyUpdate.ClientID %>').val() == "N")
                                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_alreadyadded%>');
                            else--%>
                            fncUpdate(itemCode, batchNo, mrp, Qty, QtyNew, tblTotal, txtTotal, net);
                            // fncQtyUpdate(itemCode, batchNo, mrp, Qty, QtyNew, tblTotal, txtTotal, net);  
                            duplicateStatus = false;
                        }
                    });
                    return duplicateStatus;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Get Total Value
        function fncGetRowTotalValue(source) {
            try {
                var rowObj = $(source).parent().parent();
                rowTotalValue = rowObj.find('td[id*=tdTotalValue]').text()
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            $('#<%=txtItemCode.ClientID %>').focus();
        });
        ///Net Total Value Calculation
        function fncTotalValueCalcForRowChange(source) {
            try {
                debugger;
                var rowObj, qty, cost, totalValue, netTotal
                rowObj = $(source).parent().parent();


                if (rowObj.find('td[id*=tdPriceCode]').text() == "PRC1") {
                    qty = rowObj.find('td[id*=tdQty] input').val();
                    cost = rowObj.find('td[id*=tdCost]').text();
                    totalValue = parseFloat(qty) * parseFloat(cost);

                    rowObj.find('td[id*=tdTotalValue]').text(totalValue.toFixed(2));

                    netTotal = parseFloat($('#<%=txtNetTotal.ClientID %>').val()) - parseFloat(rowTotalValue);
                    netTotal = parseFloat(netTotal) + parseFloat(totalValue);
                    $('#<%=txtNetTotal.ClientID %>').val(netTotal.toFixed(2));
                }
                fncSaveStockAdjEntryTemporarly();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Delete Particular Row
        function fncDeleteItem(source) {
            try {
                var rowObj, totalValue, netTotal;
                rowObj = $(source).parent().parent();

                totalValue = rowObj.find('td[id*=tdTotalValue]').text();
                netTotal = parseFloat($('#<%=txtNetTotal.ClientID %>').val()) - parseFloat(totalValue);
                $('#<%=txtNetTotal.ClientID %>').val(netTotal.toFixed(2));
                rowObj.remove();

                lastRowNo = 0;

                $("#tblStockAdjustment tbody").children().each(function () {
                    lastRowNo = parseInt(lastRowNo) + 1;
                    $(this).find('td[id*=tdRowNo]').text(lastRowNo);
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        ///Save Stock Adjustment
        function fncSaveStockAdjustment() {
            var date = "No", desc = "";
            var ExpireDate = "";
            var SplitExp = "";
            var Exp = "", batch = "";
            try {
                $('#<%=hidAddDeduction.ClientID %>').val($('#<%=ddlStkAdjType.ClientID %>').val().split('|')[0]);
                $('#<%=hidStokAdjCode.ClientID %>').val($('#<%=ddlStkAdjType.ClientID %>').val().split('|')[1]);

                $('#<%=lnkSave.ClientID %>').css("display", "none");

                rowStkDet = "<table>";
                $("#tblStockAdjustment tbody").children().each(function () {
                    date = "yes";
                    ExpireDate = $(this).find('td[id*=tdExpiredDate]').text();
                    SplitExp = ExpireDate.split('-');
                    Exp = SplitExp[2] + '-' + SplitExp[1] + '-' + SplitExp[0];
                    desc = escape($(this).find('td[id*=tdDesc]').text());
                    batch = $(this).find('td[id*=tdActBatchNo]').text();
                    if (batch == undefined)
                        batch = '';
                    rowStkDet = rowStkDet + "<stkDet Itemcode='" + $(this).find('td[id*=tdItemcode]').text() + "'";
                    rowStkDet = rowStkDet + " Desc='" + desc + "'";
                    rowStkDet = rowStkDet + " Qty='" + $(this).find('td[id*=tdQty] input').val() + "'";
                    rowStkDet = rowStkDet + " AddDeduct='" + $('#<%=hidAddDeduction.ClientID %>').val() + "' ";
                    rowStkDet = rowStkDet + " PrcCode='" + $(this).find('td[id*=tdPrcCode]').text() + "'";
                    rowStkDet = rowStkDet + " Cost='" + $(this).find('td[id*=tdCost]').text() + "'";
                    rowStkDet = rowStkDet + " SellingPrice='" + $(this).find('td[id*=tdSellingPrice]').text() + "'";
                    rowStkDet = rowStkDet + " TotalValue='" + $(this).find('td[id*=tdTotalValue]').text() + "'";
                    rowStkDet = rowStkDet + " BatchNo='" + batch + "'";
                    rowStkDet = rowStkDet + " MRP='" + $(this).find('td[id*=tdMRP]').text() + "'";
                    rowStkDet = rowStkDet + " WPrice1='" + $(this).find('td[id*=tdWPrice1]').text() + "'";
                    rowStkDet = rowStkDet + " WPrice2='" + $(this).find('td[id*=tdWPrice2]').text() + "'";
                    rowStkDet = rowStkDet + " WPrice3='" + $(this).find('td[id*=tdWPrice3]').text() + "'";
                    rowStkDet = rowStkDet + " OldQty='" + $(this).find('td[id*=tdOldQty]').text() + "'";
                    rowStkDet = rowStkDet + " GST='" + $(this).find('td[id*=tdGstamt]').text() + "'";
                    if (batch == "") {
                        rowStkDet = rowStkDet + " ExpiredDate='" + "0" + "'";
                        rowStkDet = rowStkDet + " Expiredmonth='" + "0" + "'";
                        rowStkDet = rowStkDet + " ExpiredYear='" + "0" + "'";
                    } else {
                        rowStkDet = rowStkDet + " ExpiredDate='" + Exp + "'";
                        rowStkDet = rowStkDet + " Expiredmonth='" + SplitExp[1] + "'";
                        rowStkDet = rowStkDet + " ExpiredYear='" + SplitExp[2] + "'";
                    }
                    rowStkDet = rowStkDet + " MedBatch='" + $(this).find('td[id*=tdMedBatch]').text() + "'";
                    rowStkDet = rowStkDet + "></stkDet>";
                });
                if (date == "No") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                    return false;
                }
                rowStkDet = rowStkDet + "</table>";
                $('#<%=hidStockDetail.ClientID %>').val(escape(rowStkDet));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Initialize Save Dialog
        function fncInitializeSaveDialog(msg) {
            try {
                $('#<%=lblSave.ClientID %>').text(msg);
                $("#StockAdjusSave").dialog({
                    resizable: false,
                    height: 130,
                    width: 380,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //function fncWPrice() {
        //    try{
        //        fncGetLastBatchDetail();
        //    }
        //    catch (err) {
        //        ShowPopupMessageBox(err.message);
        //    }
        //}
        ///Get Last Purchase Batch Detail
        function fncGetLastBatchDetail() {
            try {
                var lastRowObj;
                if ($('#<%=hidParamWhole.ClientID%>').val() == "Y") {

                    lastRowObj = $("#tblBatch tbody").children().last();
                    fncBatchInventoryValuesToTextBox(lastRowObj, 'NewBatch');
                    $('#<%=divWprice.ClientID %>').show();
                    $('#<%=txtSPrice.ClientID %>').focusout(function (e) {

                        $('#<%=txtW1.ClientID %>').val($(this).val());
                        $('#<%=txtW2.ClientID %>').val($(this).val());
                        $('#<%=txtW3.ClientID %>').val($(this).val());

                    });

                }
                else if ($('#<%=hidParamWhole.ClientID%>').val() == "N") {
                    lastRowObj = $("#tblBatch tbody").children().last();
                    fncBatchInventoryValuesToTextBox(lastRowObj, 'NewBatch');
                }
                $('#<%=txtCost.ClientID%>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Move focus to Next Control
        function fncMoveFocustoControl(event, code) {
            try {
                var keycode, batchNo;
                keycode = event.which;
                batchNo = $('#<%=txtBatchNo.ClientID %>').val();
                if (keycode == 13) {
                    if (batchNo == "NewBatch") {
                        if (code == "Qty")
                            $('#<%=txtCost.ClientID %>').focus();
                        else if (code == "Cost")
                            $('#<%=txtMRP.ClientID %>').focus();
                        else if (code == "MRP")
                            $('#<%=txtSPrice.ClientID %>').focus();
                        else
                            $('#<%=lnkAdd.ClientID %>').focus();
                    }
                    else
                        $('#<%=lnkAdd.ClientID %>').focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Set Focus to Next Row
        function fncMoveNextRow(evt, source) {
            try {

                var rowobj, charCode;
                rowobj = $(source).parent().parent();
                charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    if (rowobj.next().length > 0) {
                        rowobj.next().find('td[id*=tdQty] input').select();
                    }
                    else {
                        rowobj.siblings().first().find('td[id*=tdQty] input').select();
                    }
                    return false;

                }
                else if (charCode == 38) {
                    if (rowobj.prev().length > 0) {
                        rowobj.prev().find('td[id*=tdQty] input').select();
                    }
                    else {
                        rowobj.siblings().last().find('td[id*=tdQty] input').select();
                    }
                    return false;
                }
                else if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                    return false;
                else
                    return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtCurrentStock.ClientID %>').number(true, 2);
                $('#<%=txtQty.ClientID %>').number(true, 2);
                $('#<%=txtCost.ClientID %>').number(true, 2);
                $('#<%=txtMRP.ClientID %>').number(true, 2);
                $('#<%=txtSPrice.ClientID %>').number(true, 2);
                $('#<%=txtTotal.ClientID %>').number(true, 2);
                $('#<%=txtNetTotal.ClientID %>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var charCode = (e.which) ? e.which : e.keyCode;
                if (charCode == 27) {
                    fncClear();
                    e.preventDefault();
                }
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 116) {
                        if ($("#stkadj_batch").dialog('isOpen') === true) {
                            fncGetLastBatchDetail();
                        }

                    }

                    e.preventDefault();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncNumberFormat() {
            try {
                $("#<%=txtW1.ClientID %>").number(true, 2);
                $("#<%=txtW2.ClientID %>").number(true, 2);
                $("#<%=txtW3.ClientID %>").number(true, 2);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncprrlist() {

            var prrList;
            var prrlistarray = [10];
            var prrlistarray2 = [10];
            //  var rowStkDet;
            tblStkBody = $("#tblStockAdjustment tbody");
            try {
                var netTotal = 0;
                prrList = $('#<%=hidPrrList.ClientID %>').val();
                prrlistarray = prrList.split('||');

                if (prrlistarray.length > 0) {
                    for (var i = 0; i < prrlistarray.length; i++) {

                        prrlistarray2 = prrlistarray[i].split('@');

                        lastRowNo = parseFloat(lastRowNo) + 1;

                        row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                            lastRowNo + "</td><td>" +
                            "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                            prrlistarray2[0] + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                            prrlistarray2[1] + "</td><td id='tdPrcCode_" + lastRowNo + "' >" +
                            prrlistarray2[5] + "</td><td id='tdQty_" + lastRowNo + "' >" +
                            "<input id='txtQty_" + lastRowNo + "' type='text' readonly Class='form-control-res-right' onkeypress=' return fncMoveNextRow(event,this);'  onfocus='fncGetRowTotalValue(this);return false;' onblur='fncTotalValueCalcForRowChange(this);return false;'  value='" +
                            prrlistarray2[3] + "'>" + "</td><td id='tdCost_" + lastRowNo + "' >" +
                            prrlistarray2[7] + "</td><td id='tdTotalValue_" + lastRowNo + "' >" +
                            (prrlistarray2[7] * prrlistarray2[3]).toFixed(2) + "</td><td id='tdBatchNo_" + lastRowNo + "' >" +
                            prrlistarray2[4] + "</td><td id='tdMRP_" + lastRowNo + "' >" +
                            prrlistarray2[2] + "</td><td id='tdSellingPrice_" + lastRowNo + "' >" +
                            prrlistarray2[6] + "</td><td id='tdPriceCode_" + lastRowNo + "' >" +
                            "PRC1" + "</td><td id='tdActBatchNo_" + lastRowNo + "' >" +
                            prrlistarray2[4] + "</td><td id='tdOldQty_" + lastRowNo + "' >" +
                            prrlistarray2[3] + "</td></tr>";
                        tblStkBody.append(row);
               <%-- if (date == "No") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false;
                }--%>

                        // rowStkDet = rowStkDet + "</table>";

                        netTotal = parseFloat(netTotal) + (prrlistarray2[7] * prrlistarray2[3]);
                        $('#<%=txtNetTotal.ClientID %>').val(netTotal.toFixed(2));
                    }

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSaveStockAdjEntryTemporarly() {
            var obj = {};
            try {
                obj.xmlData = fncGetGridValuesForSave();
                $.ajax({
                    type: "POST",
                    url: "frmStockAdjustmentEntry.aspx/fncSaveStockAdjEntryTemporarly",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        return false;
                    },
                    error: function (data) {
                        fncToastError(data.message);
                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncGetGridValuesForSave() {
            var obj;
            try {
                var xml = '<NewDataSet>';
                $("#tblStockAdjustment tbody").children().each(function () {
                    obj = $(this);

                    if (parseFloat(obj.find('td input[id*=txtQty]').val()) > 0) {

                        xml += "<Table>";
                        xml += "<RowNo>" + obj.find('td[id*=tdRowNo]').text() + "</RowNo>";
                        xml += "<ItemCode>" + obj.find('td[id*=tdItemcode]').text() + "</ItemCode>";
                        xml += "<ItemDescription>" + obj.find('td[id*=tdDesc]').text() + "</ItemDescription>";
                        xml += "<PrcCode>" + obj.find('td[id*=tdPrcCode]').text() + "</PrcCode>";
                        xml += "<TxtQty>" + obj.find('td input[id*=txtQty]').val() + "</TxtQty>";
                        xml += "<Cost>" + obj.find('td[id*=tdCost]').text() + "</Cost>";
                        xml += "<Total>" + obj.find('td[id*=tdTotalValue]').text() + "</Total>";
                        xml += "<BatchNo>" + obj.find('td[id*=tdBatchNo]').text() + "</BatchNo>";
                        xml += "<MRP>" + obj.find('td[id*=tdMRP]').text() + "</MRP>";
                        xml += "<SellinPrice>" + obj.find('td[id*=tdSellingPrice]').text() + "</SellinPrice>";
                        xml += "<PriceCode>" + obj.find('td[id*=tdPriceCode]').text() + "</PriceCode>";
                        xml += "<ActBatchNo>" + obj.find('td[id*=tdActBatchNo]').text() + "</ActBatchNo>";
                        xml += "<OldQty>" + obj.find('td[id*=tdOldQty]').text() + "</OldQty>";
                        xml += "<Wprc1>" + obj.find('td[id*=tdWPrice1]').text() + "</Wprc1>";
                        xml += "<Wprc2>" + obj.find('td[id*=tdWPrice2]').text() + "</Wprc2>";
                        xml += "<Wprc3>" + obj.find('td[id*=tdWPrice3]').text() + "</Wprc3>";
                        xml += "<ExpiredDate>" + obj.find('td[id*=tdExpiredDate]').text() + "</ExpiredDate>";
                        xml += "<GST>" + obj.find('td[id*=tdGstamt]').text() + "</GST>";
                        xml += "</Table>";
                    }
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
            <%--$('#<%=GRNXmldata.ClientID %>').val(xml);--%>
                return xml;
                console.log(xml);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncLoadStockData() {
            debugger;
            var allObj, obj;
            var obj;
            var netTotal = 0;
            try {
                allObj = jQuery.parseJSON($('#<%=hidStocDisEntry.ClientID %>').val());
                obj = allObj.Table;
                for (var i = 0; i < obj.length; i++) {
                    poNo = obj[i]["PONo"];
                    fncAddNewRowsToGrid(i, obj[i]["ItemCode"], obj[i]["ItemDescription"],
                        obj[i]["PrcCode"], obj[i]["TxtQty"], obj[i]["Cost"], obj[i]["Total"], obj[i]["BatchNo"],
                        obj[i]["MRP"], obj[i]["SellinPrice"], obj[i]["PriceCode"], obj[i]["ActBatchNo"], obj[i]["OldQty"], obj[i]["Wprc1"],
                        obj[i]["Wprc2"], obj[i]["Wprc3"], obj[i]["ExpiredDate"]);
                    netTotal = parseFloat(netTotal) + (parseFloat(obj[i]["Cost"]) * parseFloat(obj[i]["TxtQty"]));
                    $('#<%=txtNetTotal.ClientID %>').val(netTotal.toFixed(2));
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncAddNewRowsToGrid(lastRowNo, itemcode, ItemDesc, PrcCode, TxtQty, Cost, Total, BatchNo, MRP,
            SellPrice, PriceCode, ACTBatch, oldQty, W1, W2, W3, Expire) {
            var tblGRNBody, row;
            try {
                ItemDesc = ItemDesc.replace("'", '')
                tblStockAdjustment = $("#tblStockAdjustment tbody");
                var rowno = "rowno";


                lastRowNo = parseFloat(lastRowNo) + 1;

                row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                    lastRowNo + "</td><td>" +
                    "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                    itemcode + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                    ItemDesc + "</td><td id='tdPrcCode_" + lastRowNo + "' >" +
                    PrcCode + "</td><td id='tdQty_" + lastRowNo + "' >" +
                    "<input id='txtQty_" + lastRowNo + "' type='text'  Class='form-control-res-right' onkeypress=' return fncMoveNextRow(event,this);'  onfocus='fncGetRowTotalValue(this);return false;' onblur='fncTotalValueCalcForRowChange(this);return false;'  value='" +
                    TxtQty + "'>" + "</td><td id='tdCost_" + lastRowNo + "' >" +
                    Cost + "</td><td id='tdTotalValue_" + lastRowNo + "' >" +
                    (Cost * TxtQty).toFixed(2) + "</td><td id='tdBatchNo_" + lastRowNo + "' >" +
                    BatchNo + "</td><td id='tdMRP_" + lastRowNo + "' >" +
                    MRP + "</td><td id='tdSellingPrice_" + lastRowNo + "' >" +
                    SellPrice + "</td><td id='tdPriceCode_" + lastRowNo + "' >" +
                    PriceCode + "</td><td id='tdActBatchNo_" + lastRowNo + "' >" +
                    ACTBatch + "</td><td id='tdOldQty_" + lastRowNo + "' >" +
                    oldQty + "</td><td id='tdWPrice1_" + lastRowNo + "' >" +
                    W1 + "</td><td id='tdWPrice2_" + lastRowNo + "' >" +
                    W2 + "</td><td id='tdWPrice3_" + lastRowNo + "' >" +
                    W3 + "</td><td id='tdExpiredDate_" + lastRowNo + "' >" +
                    Expire + "</td></tr>";
                tblStockAdjustment.append(row);

                tblStockAdjustment.children().dblclick(fncVenItemRowdblClk);

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        <%--function fncQtyUpdate(itemCode, batchNo, mrp, Qty, QtyNew, tblTotal, txtTotal, net) {
     $("#dialog-confirm").dialog({
         resizable: false,
         height: "auto",
         width: 400,
         modal: true,
         buttons: {
             "YES": function () {
                 fncUpdate(itemCode, batchNo, mrp, Qty, QtyNew, tblTotal, txtTotal, net);
                 $('#<%=txtItemCode.ClientID %>').select();
                 $(this).dialog("close");
             },
             "NO": function () {
                 $(this).dialog("close");
             }
         }
     });
 }--%>

        function fncBatchDeleteInventories(source) {
            try {
                MRPDEL = "";
                var obj = {};
                obj = jQuery.parseJSON(source.d);
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                        MRPDEL += obj[i]["MRP"] + ',';
                    }
                }
                //console.log(MRPDEL);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
            }
            if (charCode == 13) {
                fncGetInventoryCode();
                return false;
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    fncGetInventoryCode();
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncVenItemRowdblClk() {
            try {
                fncOpenItemhistory($.trim($(this).find('td[id*="tdItemcode"]').text()));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Open Item History
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function fncValidateMedicalBatchorNot() {
            try {
                if ($('#<%=hidAllowExpDate.ClientID %>').val() == "true" && $('#divTextMedBatch').is(":visible")) {
                    if ($('#<%=txtExpiredDate.ClientID %>').val() == "")
                        ShowPopupMessageBox("Please Enter Expired Date.");
                    else if ($('#<%=txtMedBatch.ClientID %>').val() == "")
                        ShowPopupMessageBox("Please Enter Medical Batch.");
                    else
                        fncGetMedBatchDetail();
                }
               <%-- else if ($('#<%=txtMRP.ClientID %>').val() < $('#<%=txtSPrice.ClientID %>').val()) {
                    if ($('#<%=txtSPrice.ClientID %>').val() < $('#<%=txtCost.ClientID %>').val()) {
                        ShowPopupMessageBox("Selling Price must be less than MRP ");
                        return false;
                    }
                }--%>
                else {

                
                    fncAddItemToTable();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetMedBatchDetail() {
            try {
                var obj = {};
                obj.ItemCode = $('#<%=txtItemCode.ClientID %>').val();
                obj.BatchNo = $('#<%=txtMedBatch.ClientID %>').val();
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Stock/frmStockAdjustmentEntry.aspx/fncGetMedBatchDetail")%>',
                     data: JSON.stringify(obj),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                    success: function (msg) { 
                        if (msg != "") {
                            var obj = {};
                            obj = jQuery.parseJSON(msg.d);
                            if (obj.length > 0) {
                                if (obj[0]["Val"] == 'A') {
                                    ShowPopupMessageBox("This Medical BatchNo Already Exists.");
                                    return false;
                                }
                                else {
                                    fncAddItemToTable();
                                }
                            }
                        }
                        else {
                            fncAddItemToTable();
                        }

                     },
                     error: function (data) {
                         ShowPopupMessageBox(data.message);
                     }

                 });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a href="../Stock/frmStockAdjustmentSummery.aspx">StockSummary</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_stkAdjustment%>
                </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full">
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-5">
                        <asp:Label ID="lblStkAdjNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_StkAdjustmentNo %>'></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtStkAdjNo" runat="server" CssClass="form-control-res" onmousedown="return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-md-5">
                        <asp:Label ID="lblstkAdjtype" runat="server" Text='<%$ Resources:LabelCaption,lbl_stkAdjtype %>'></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:DropDownList ID="ddlStkAdjType" runat="server" Width="100%">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-5">
                        <asp:Label ID="lblStkAdjDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_stkAdjDate %>'></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtStockAdjustmentDate" runat="server" CssClass="form-control-res"
                            onmousedown="return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-5">
                        <asp:Label ID="lblRemarks" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-3 sort_by">
                    <div class="col-md-3">
                        <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                    </div>
                    <div class="col-md-7">
                        <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-1">
                        <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                    </div>
                    <div class="col-md-1">
                        <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                    </div>
                </div>
            </div>
            <div class="Payment_fixed_headers stockAdjustmentEntry">
                <table id="tblStockAdjustment" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr class="click">
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Item Description
                            </th>
                            <th scope="col">Prc Code
                            </th>
                            <th scope="col">Qty
                            </th>
                            <th scope="col">Cost
                            </th>
                            <th scope="col">Total
                            </th>
                            <th id="thStkBatchNo" scope="col">BatchNo
                            </th>
                            <th scope="col">MRP
                            </th>
                            <th scope="col">S.Price
                            </th>
                            <th scope="col">PriceCode
                            </th>
                            <th scope="col">WPrice1
                            </th>
                            <th scope="col">WPrice2
                            </th>
                            <th scope="col">WPrice3
                            </th>
                            <th scope="col">Expired Date
                            </th>
                            <th scope="col">Expired Month
                            </th>
                            <th scope="col">Expired Year
                            </th>
                            <th scope="col">GST
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div id="divItemAddSection" runat="server">
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                            onkeydown="return fncShowSearchDialogInventory(event,'Inventory',  'txtItemCode', 'txtQty');"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_Description">
                    <div>
                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblCurrentStk" runat="server" Text='<%$ Resources:LabelCaption,lbl_curStock %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtCurrentStock" runat="server" CssClass="form-control-res-right" Text="0" onmousedown="return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res-right" onblur="fncTotalValueCalc();return false;"
                            onkeydown="fncMoveFocustoControl(event,'Qty');">0</asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblBasicCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicCost %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res-right" Enabled="false" onblur="fncTotalValueCalc();return false;"
                            onkeydown="fncMoveFocustoControl(event,'Cost');" Text="0"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res-right" Enabled="false"
                            onkeydown="fncMoveFocustoControl(event,'MRP');">0</asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblPriceCode" runat="server" Text='<%$ Resources:LabelCaption,lbl_PrcCode %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:DropDownList ID="ddlPrcCode" runat="server" Width="100%">
                            <asp:ListItem Text="PRC1" Value="PRC1"></asp:ListItem>
                            <asp:ListItem Text="FOC1" Value="FOC1"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblSPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellPrice %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res-right" Enabled="false"
                            onkeydown="fncMoveFocustoControl(event,'SellingPrice');">0</asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Total %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control-res-right" Text="0" onmousedown="return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_save_button">
                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='<%$ Resources:LabelCaption,btnAdd %>'
                        OnClientClick="fncValidateMedicalBatchorNot();return false;"></asp:LinkButton>
                </div>
                <div class="stockAdjustment_save_button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='Clear'
                        OnClientClick="fncClear();return false;"></asp:LinkButton>
                </div>
            </div>
            <div class="stockAdjustment_footer">

                <%-- <div class="stockAdjustment_NetValues_lbl">
                        <asp:Label ID="lblSubtotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_Subtotal %>'></asp:Label>
                    </div>
                    <div class="stockAdjustment_NetValues_txt">
                        <asp:TextBox ID="txtSubTotal" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="stockAdjustment_NetValues_lbl stockAdjustment_vat_lbl">
                        <asp:Label ID="lblVat" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vat %>'></asp:Label>
                    </div>
                    <div class="stockAdjustment_NetValues_txt">
                        <asp:TextBox ID="txtVat" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>--%>
                <div class="col-md-3">
                    <div class="col-md-3">
                        <asp:Label ID="lblNetTotal" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetTotal %>'></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtNetTotal" runat="server" CssClass="form-control-res-right" onmousedown="return false;">0</asp:TextBox>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="divWprice" runat="server" class="display_none" style="margin-left: 163px;">
                        <div class="col-md-1">
                            <asp:Label ID="Label1" runat="server" Text="W1"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtW1" runat="server" CssClass="form-control-res-right WPrice" Text="0" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="Label2" runat="server" Text="W2"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtW2" runat="server" CssClass="form-control-res-right WPrice" Text="0" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Label ID="Label3" runat="server" Text="W3"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtW3" runat="server" Text="0" CssClass="form-control-res-right WPrice" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    <div id="divExpired" runat="server" class="display_none">
                        <div class="col-md-2">
                            <asp:Label ID="Label4" runat="server" Text="E.Date"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtExpiredDate" MaxLength="12" Width="100%" runat="server" CssClass="form-control-res Exp"></asp:TextBox>
                        </div>
                        <div class="col-md-2" id="divlabelMedBatch">
                            <asp:Label ID="Label5" runat="server" Text="M.Batch"></asp:Label>
                        </div>
                        <div class="col-md-4" id="divTextMedBatch">
                            <asp:TextBox ID="txtMedBatch" Width="100%" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:UpdatePanel ID="upStk" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lnkSave %>'
                                    OnClientClick="return fncSaveStockAdjustment();" OnClick="lnkSave_Click"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
        <div class="hiddencol">
            <div id="stkadj_batch">
                <div class="Payment_fixed_headers BatchDetail">
                    <table id="tblBatch" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th id="thBatchNo" scope="col">BatchNo
                                </th>
                                <th scope="col">Stock
                                </th>
                                <th scope="col">Cost
                                </th>
                                <th scope="col">MRP
                                </th>
                                <th scope="col">SellingPrice
                                </th>
                                <th scope="col">InventoryCode
                                </th>
                                <th scope="col">Description
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkNewBatch" runat="server" class="button-blue" Text='NewBatch (F5)'
                            OnClientClick="fncGetLastBatchDetail();return false;"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="hiddencol">
            <asp:HiddenField ID="hidAddDeduction" runat="server" />
            <asp:HiddenField ID="hidStokAdjCode" runat="server" />
            <asp:HiddenField ID="hidStockDetail" runat="server" />
            <asp:HiddenField ID="hidJsonStockDetail" runat="server" />
            <asp:HiddenField ID="hidBatchText" runat="server" Value="" />
            <asp:HiddenField ID="hidParamWhole" runat="server" Value="" />
            <asp:HiddenField ID="hidPrrList" runat="server" Value="" />
            <asp:HiddenField ID="hidAllowExpDate" runat="server" Value="" />
            <asp:HiddenField ID="hidTextileLoc" runat="server" Value="" />
            <asp:HiddenField ID="hidStocDisEntry" runat="server" Value="" />
            <asp:HiddenField ID="hidQtyUpdate" runat="server" Value="" />
            <asp:HiddenField ID="hidInvLength" runat="server" Value="" />

            <asp:HiddenField ID="hdIP" runat="server" />
            <div id="StockAdjusSave">
                <div>
                    <asp:Label ID="lblSave" runat="server" Text="" />
                </div>
                <div class="dialog_center">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                        OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>

            </div>
        </div>
        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 12px 12px 20px 0;"></span>This Item Already Exists.Do you want to Update?</p>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidGstamt" runat="server" />
</asp:Content>
