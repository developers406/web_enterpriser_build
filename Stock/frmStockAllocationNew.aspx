﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmStockAllocationNew.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockAllocationNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
        .stk_Allocation_New td:nth-child(1), .stk_Allocation_New th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
        }

        .stk_Allocation_New td:nth-child(2), .stk_Allocation_New th:nth-child(2) {
            min-width: 90px; /*172px*/
            max-width: 90px;
        }

        .stk_Allocation_New td:nth-child(3), .stk_Allocation_New th:nth-child(3) {
            min-width: 350px;
            max-width: 350px;
        }

        .stk_Allocation_New td:nth-child(4), .stk_Allocation_New th:nth-child(4) {
            display: none;
        }

        .stk_Allocation_New td:nth-child(5), .stk_Allocation_New th:nth-child(5) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(5) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(6), .stk_Allocation_New th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(6) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(7), .stk_Allocation_New th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(7) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(8), .stk_Allocation_New th:nth-child(8) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(8) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(9), .stk_Allocation_New th:nth-child(9) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(9) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(10), .stk_Allocation_New th:nth-child(10) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(10) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(11), .stk_Allocation_New th:nth-child(11) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(11) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(12), .stk_Allocation_New th:nth-child(12) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(12) {
            text-align: right;
        }

        .stk_Allocation_New td:nth-child(13), .stk_Allocation_New th:nth-child(13) {
            min-width: 75px;
            max-width: 75px;
        }

        .stk_Allocation_New td:nth-child(13) {
            text-align: right;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockAllocationNew');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockAllocationNew";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript" language="Javascript">
        function pageLoad() {
            try {
                $('#<%=ddlStkAllMethod.ClientID %>').show().removeClass('chzn-done');
                $('#<%=ddlStkAllMethod.ClientID %>').next().remove();

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
                fncAssignColWidth();
                fncAssignColWidthstock();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncAssignColWidth() {
            var colTotCount = 0;
            try {
                colTotCount = $("#<%= hidColCnt.ClientID %>").val();

                for (var i = 14; i <= colTotCount; i++) {
                    $(" .stk_Allocation_New td:nth-child(" + i + "), .stk_Allocation_New th:nth-child(" + i + ")").css("min-width", "70px");
                    $(" .stk_Allocation_New td:nth-child(" + i + "), .stk_Allocation_New th:nth-child(" + i + ")").css("max-width", "70px");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncAssignColWidthstock() {
            var colTotCount = 0;
            try {
                colTotCount = $("#<%= hidColCntstock.ClientID %>").val();

                 for (var i = 10; i <= colTotCount; i++) {
                     $(" .stk_Allocation_New td:nth-child(" + i + "), .stk_Allocation_New th:nth-child(" + i + ")").css("min-width", "70px");
                     $(" .stk_Allocation_New td:nth-child(" + i + "), .stk_Allocation_New th:nth-child(" + i + ")").css("max-width", "70px");
                 }
             }
             catch (err) {
                 fncToastError(err.message);
             }
         }
        function fncShowVendorSearchDailog() {
            try {
                $('#<%=txtGidSearch.ClientID%>').val($('#<%=txtGidNo.ClientID%>').val());
                fncShowGidSearch();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncShowPONOSearchDailog() {
            try {
                $('#<%=txtPONOSearch.ClientID%>').val($('#<%=txtPONO.ClientID%>').val());
                fncShowPONOSearch();
             }
             catch (err) {
                 fncToastError(err.message);
             }
        }
        ///PONO search
        function fncPONOSearch() {
            try {
                $("[id$=txtPONOSearch]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        $.ajax({
                            url: '<%=ResolveUrl("~/Stock/frmStockAllocationNew.aspx/fncGetPONO")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtPONOSearch.ClientID %>').val($.trim(i.item.label));

                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $("#dialog-PONO").dialog("destroy");
                        $('#<%=txtPONO.ClientID %>').val($.trim(i.item.valitemcode));
                        $('#<%=btnPONO.ClientID %>').click();
                        return false;
                    },
                    appendTo: $("#dialog-PONO"),
                    minLength: 2
                });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
        //// AutoComplete Search for Gid
        function fncGidSearch() {
            try {
                $("[id$=txtGidSearch]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        obj.location = $('#<%=hidCurLocation.ClientID%>').val();
                        obj.allocType = $('#<%=ddlStkAllMethod.ClientID%>').val();
                        obj.FromDate = $('#<%=txtFromDate.ClientID%>').val();
                        obj.ToDate = $('#<%=txtToDate.ClientID%>').val();
                        $.ajax({
                            url: '<%=ResolveUrl("~/Stock/frmStockAllocationNew.aspx/fncGetGidNo")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtGidSearch.ClientID %>').val($.trim(i.item.valitemcode));

                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $("#dialog-Gid").dialog("destroy");
                        $('#<%=txtGidNo.ClientID %>').val($.trim(i.item.valitemcode));
                        $('#<%=btnGid.ClientID %>').click();
                        return false;
                    },
                    appendTo: $("#dialog-Gid"),
                    minLength: 2
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Show Item Search Table
        function fncShowGidSearch() {
            try {
                $("#dialog-Gid").dialog({
                    resizable: false,
                    height: 300,
                    width: 500,
                    modal: true,
                    title: "Gid Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowPONOSearch() {
            try {
                $("#dialog-PONO").dialog({
                    resizable: false,
                    height: 300,
                    width: 500,
                    modal: true,
                    title: "PONO Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            fncGidSearch();
            fncPONOSearch();
            fncAssignDecimal();
            fncGridKeyTraversel();
        });

        function fncGetRecordForStockAllocation() {
            try {
                if ($('#<%=txtGidNo.ClientID %>').val() == ""  && $('#<%=txtPONO.ClientID %>').val()=="") {
                    fncToastInformation("Select any one GRN");
                }
                if ($('#<%=txtPONO.ClientID %>').val != "")
                {
                    
                    $('#<%=btnPONO.ClientID %>').click();
                }
                if ($('#<%=txtGidNo.ClientID %>').val != "")
                {
                    
                    $('#<%=btnGid.ClientID %>').click();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncAssignDecimal() {
            try {
                $("#ContentPlaceHolder1_gvStockAllocation tbody").children().each(function () {
                    if ($.trim($("td", $(this)).eq(3).text()) == "PCS") {
                        $(this).find('td input[id*=loc]').number(true, 0);
                    }
                    else {
                        $(this).find('td input[id*=loc]').number(true, 2);
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncGridKeyTraversel() {
            var nextCellID;
            var  horizontalwidth = 0;
            var nextRowID, prvRowID;
            var curRowID,nextCellID;
            
            try {
                $('[id*="ContentPlaceHolder1_gvStockAllocation_loc"]').on('keydown', function (e) {
                    if (e.which == 13 || e.which == 39) {
                        nextCellID = $(this).attr("id").split('_')[2];
                        curRowID = $(this).attr("id").split('_')[3];
                        nextCellID = nextCellID.substring(3, nextCellID.length);
                        nextCellID = parseInt(nextCellID) + 1;

                        horizontalwidth = $("#divStkAlloc").scrollLeft();
                        $("#divStkAlloc").scrollLeft(horizontalwidth + 50);
                        //$('[id*="ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '"]').select();
                        //console.log($('"ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '_' + nextCell + '"]'));
                        //$('#ContentPlaceHolder1_gvStockAllocation_loc' + 'nextCellID' + '_' + nextCell);
                        //fncSetFocustoObject($('[id*="ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '_' + nextCell + '"]'));
                        //alert('#ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '_' + nextCell);
                        fncSetFocustoObject($('#ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '_' + curRowID));
                        
                        return false;
                    }
                    else if (e.which == 37) {
                        nextCellID = $(this).attr("id").split('_')[2];
                        curRowID = $(this).attr("id").split('_')[3];
                        nextCellID = nextCellID.substring(3, nextCellID.length);
                        nextCellID = parseInt(nextCellID) - 1;

                        horizontalwidth = $("#divStkAlloc").scrollLeft();
                        $("#divStkAlloc").scrollLeft(horizontalwidth - 50);
                        //$('[id*="ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '"]').select();
                        //fncSetFocustoObject($('[id*="ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '"]'));
                        fncSetFocustoObject($('#ContentPlaceHolder1_gvStockAllocation_loc' + nextCellID + '_' + curRowID));
                    }
                    else if (e.which == 40) //down
                    {
                        var curRow = {};
                        var nextRow;
                        curRow = $(this).attr("id").split('_');
                        nextRow = parseInt(curRow[3]) + parseInt(1);
                        nextRowID = curRow[0] + "_" + curRow[1] + "_" + curRow[2] + "_" + nextRow;
                        //$("#" + nextRowID + "").select();
                        fncSetFocustoObject($("#" + nextRowID + ""));
                        
                    }
                    else if (e.which == 38)//UP
                    {
                        var curRow = {};
                        var prvRow;
                        curRow = $(this).attr("id").split('_');
                        prvRow = parseInt(curRow[3]) - parseInt(1);
                        prvRowID = curRow[0] + "_" + curRow[1] + "_" + curRow[2] + "_" + prvRow;
                        fncSetFocustoObject($("#" + prvRowID + ""));                        
                        //$("#" + prvRowID + "").select();
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    /*    xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';*/
        function fncToFormXmlForSave() {
            var obj, xml;
            try {
                var rowno = $("#<%=gvStockAllocation.ClientID %> td").length;
                if (rowno > 0) {
                    var xml = '<NewDataSet>';
                    $("#<%=gvStockAllocation.ClientID %> tr").each(function () {

                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {
                            if (j > 12) {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + $("input:text",   cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>'
                            }
                            else {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';
                            }
                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                    $("#<%=hidTransfer.ClientID %>").val(escape(xml));
                }
                var no = $("#<%=gvStock.ClientID %> td").length;
                if (no > 0) {
                    var xml = '<NewDataSet>';
                    $("#<%=gvStock.ClientID %> tr").each(function () {

                        var cells = $("td", this);
                        if (cells.length > 0) {

                            xml += "<Table>";
                            for (var k = 0; k < cells.length; ++k) {
                                if (k >= 9) {
                                    xml += '<' + $(this).parents('table:first').find('th').eq(k).text() + '>' + $("input:text", cells.eq(k)).val() + '</' + $(this).parents('table:first').find('th').eq(k).text() + '>';
                                }
                                else {
                                    xml += '<' + $(this).parents('table:first').find('th').eq(k).text() + '>' + cells.eq(k).text().trim() + '</' + $(this).parents('table:first').find('th').eq(k).text() + '>';
                                }
                            }
                            xml += "</Table>";
                        }
                    });

                    xml = xml + '</NewDataSet>'
                    $("#<%=hidstock.ClientID %>").val(escape(xml));
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="/EnterpriserWebNewFinal/Masters/frmMain.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Stock</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Stock Allocation</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div>
            <div class="stkAllocation_new">
                <div class="col-md-5">
                    <asp:Label ID="Label4" Font-Bold="true" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="col-md-7">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                </div>
                <div class="col-md-5" style="margin-top: 5px;">
                    <asp:Label ID="Label5" Font-Bold="true" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="col-md-7" style="margin-top: 5px;">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                </div>
            </div>
            <div class="stkAllocation_new">
                <div class="col-md-4">
                    <asp:Label ID="Label11" runat="server" Text="GID/Dis"></asp:Label>
                </div>
                <div class="col-md-8">
                    <asp:TextBox ID="txtGidNo" runat="server" CssClass="form-control-res" oninput="fncShowVendorSearchDailog();"></asp:TextBox>
                </div>
                <div class="col-md-4">
                    <asp:Label ID="lblStkAllocation" runat="server" Text="Allocation"></asp:Label>
                </div>
                <div class="col-md-8" style="padding-top: 2px;">
                    <asp:DropDownList ID="ddlStkAllMethod" runat="server" CssClass="form-control-res">
                        <asp:ListItem Value="BasedonStkReq" Text="BasedonStkReq"> </asp:ListItem>
                        <asp:ListItem Value="SalesBase" Text="SalesBase"> </asp:ListItem>                                                
                        <asp:ListItem Value="Manual" Text="Manual"> </asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="stkAllocation_new">
                <div class="col-md-8">
                    <asp:RadioButton ID="rbnSalesBaseGRN" runat="server" Text="GRN Only" GroupName="SalesBase" Checked="true" CssClass="radioboxlist" />
                </div>
                <div class="col-md-4">
                    <asp:RadioButton ID="rbnSalesBaseGRNAll" runat="server" Text="All" GroupName="SalesBase" CssClass="radioboxlist" />
                </div>
            </div>
            <div class="float_right">
                <div class="stkalloc_radiobtn">
                    <asp:LinkButton ID="lnkTransfer" runat="server" class="button-blue"
                        Text="Transfer" OnClick="lnkTransfer_Click" OnClientClick="fncToFormXmlForSave();"></asp:LinkButton>
                </div>
                <div class="stkalloc_radiobtn">
                    <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue"
                        Text="Fetch" OnClientClick="fncGetRecordForStockAllocation();return false;"></asp:LinkButton>
                </div>
            </div>
            <div class="col-md-4">
                    <asp:Label ID="Label2" runat="server" Text="AutoPONO"></asp:Label>
                </div>
            <div class="col-md-8">
                 <asp:TextBox ID="txtPONO" runat="server" CssClass="form-control-res" oninput="fncShowPONOSearchDailog();" style="position: absolute;width: auto;top: -34px;left: 307px;"></asp:TextBox>
            </div>
        </div>
        <div>
            <div id="divStkAlloc" class="GridDetails" style="overflow-x: scroll; overflow-y: scroll; height: 430px; width: 1355px; background-color: aliceblue;">
            <asp:GridView ID="gvStkhdr" runat="server" AutoGenerateColumns="true"
                ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid stk_Allocation_New" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                <PagerStyle CssClass="stk_Allocation_New" />
            </asp:GridView>
            
                <asp:GridView ID="gvStockAllocation" runat="server" AutoGenerateColumns="true"
                    ShowHeaderWhenEmpty="true" CssClass="stk_Allocation_New" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                    ShowHeader="true" OnRowDataBound="gvStockAllocation_DataBound">
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter " />
                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left_new" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left_new" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="stk_Allocation_New" />
                </asp:GridView>
                <asp:GridView ID="gvStock" runat="server" AutoGenerateColumns="true" 
                                                   
                                                   ShowHeaderWhenEmpty="false" ShowHeader="true" CssClass="pshro_GridDgn"  EmptyDataRowStyle-CssClass="Emptyidclassforselector" OnRowDataBound="gvStock_RowDataBound">

                                                    <PagerStyle CssClass="stk_Allocation_New" />
                                                    
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                     <RowStyle CssClass="pshro_GridDgnStyle tbl_left_new" />
                   
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        PageButtonCount="5" Position="Bottom" />
                                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    
                </asp:GridView>
            </div>

        </div>
    </div>
    <div class="display_none">
        <asp:Button ID="btnGid" runat="server" OnClick="btnGid_clik" />
        <asp:Button ID="btnPONO" runat="server" OnClick="btnPONO_clik" />
        <asp:HiddenField ID="hidCurLocation" runat="server" />
        <asp:HiddenField ID="hidColCnt" runat="server" />
        <asp:HiddenField ID="hidTransfer" runat="server" />
        <asp:HiddenField ID="hidstock" runat="server" />
        <asp:HiddenField ID="hidColCntstock" runat="server" />
        <div id="dialog-Gid">
            <div>
                <div class="float_left">
                    <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
                </div>
                <div class="gidItem_Search">
                    <asp:TextBox ID="txtGidSearch" runat="server" class="form-control-res"></asp:TextBox>
                </div>
              
            </div>
        </div>
         <div id="dialog-PONO">
            <div>
                <div class="float_left">
                    <asp:Label ID="Label1" runat="server" Text="Search"></asp:Label>
                </div>
                
                   <div class="gidPONO_Search">
                    <asp:TextBox ID="txtPONOSearch" runat="server" class="form-control-res"></asp:TextBox>
                </div>
            </div>
        </div>
        <asp:HiddenField ID ="hidEditbtn" runat="server" />
          <asp:HiddenField ID ="hidDeletebtn" runat="server" />
          <asp:HiddenField ID ="hidViewbtn" runat="server" />  
         <asp:HiddenField ID ="hidSavebtn" runat="server" />
    </div>
</asp:Content>
