﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmExpiredDateChange.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmExpiredDateChange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        

    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .stockAdjustmentEntry td:nth-child(1), .stockAdjustmentEntry th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .stockAdjustmentEntry td:nth-child(2), .stockAdjustmentEntry th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }


        .stockAdjustmentEntry td:nth-child(2) {
            text-align: center !important;
        }

        .stockAdjustmentEntry td:nth-child(3), .stockAdjustmentEntry th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(4), .stockAdjustmentEntry th:nth-child(4) {
            min-width: 355px;
            max-width: 355px;
        }

        .stockAdjustmentEntry td:nth-child(5), .stockAdjustmentEntry th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(5) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(6), .stockAdjustmentEntry th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(7), .stockAdjustmentEntry th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }


        .stockAdjustmentEntry td:nth-child(7) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(8), .stockAdjustmentEntry th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(8) {
            text-align: right !important;
        }

        .stockAdjustmentEntry td:nth-child(9), .stockAdjustmentEntry th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(10), .stockAdjustmentEntry th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(11), .stockAdjustmentEntry th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustmentEntry td:nth-child(12), .stockAdjustmentEntry th:nth-child(12) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(13), .stockAdjustmentEntry th:nth-child(13) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(14), .stockAdjustmentEntry th:nth-child(14) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(15), .stockAdjustmentEntry th:nth-child(15) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(16), .stockAdjustmentEntry th:nth-child(16) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(17), .stockAdjustmentEntry th:nth-child(17) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(18), .stockAdjustmentEntry th:nth-child(18) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(19), .stockAdjustmentEntry th:nth-child(19) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(20), .stockAdjustmentEntry th:nth-child(20) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(21), .stockAdjustmentEntry th:nth-child(21) {
            display: none;
        }

        .stockAdjustmentEntry td:nth-child(22), .stockAdjustmentEntry th:nth-child(22) {
            display: none;
        }



        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }

        .WPrice {
            width: 60%;
        }

        .Exp {
            width: 70%;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'ExpiredDateChange');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "ExpiredDateChange";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        var lastRowNo = 0, rowTotalValue = 0, tblBatchBody;
        var oldMRP = 0, InventoryCodeDEL, MRPDEL, AlExpDate;
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                 }
                 else {
                     $('#<%=lnkSave.ClientID %>').css("display", "none");
                 }
                <%--$("#thBatchNo").text($('#<%=hidBatchText.ClientID%>').val());
                $('#thStkBatchNo').text($('#<%=hidBatchText.ClientID%>').val());--%>
                $("#<%= txtExpiredDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" });
                $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                $("#<%=txtCurrentStock.ClientID%>").attr("disabled", true);
                $("#<%=txtDescription.ClientID%>").attr("disabled", true);
                $("#<%=txtItemCode.ClientID%>").focus();
                fncDecimal();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(function () {
            SetAutoCompleteForItemSearch();
        });

        ///Load Stock Adjustment

        //Get Inventory code
        function fncGetInventoryCode() {

            try {
                var obj = {};
                obj.code = $('#<%=txtItemCode.ClientID %>').val();
                obj.formName = 'StkAdj';

                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Assign Values to Text Box
        function fncAssignValuestoTextBox(msg) {
            try {

                var objStk, row;
                objStk = jQuery.parseJSON(msg.d);

                if (objStk.length > 0) {
                    tblBatchBody = $("#tblBatch tbody");
                    tblBatchBody.children().remove();

                    for (var i = 0; i < objStk.length; i++) {
                        if (objStk[i]["AllowExpireDate"] == true) {
                            row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                $.trim(objStk[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                $.trim(objStk[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" +
                                objStk[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                                objStk[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                                objStk[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                objStk[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                $.trim(objStk[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                                $.trim(objStk[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                               (objStk[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["AllowExpireDate"]) + "</td><td id='tdMRPDEL_ ' style ='display:none'" + i + "' >" +
                               (objStk[i]["MRPDEL"]) + "</td></tr>";

                            tblBatchBody.append(row);

                        }
                        else {
                            ShowPopupMessageBox("Please Enter Valid Item");
                            fncClear();
                            return false;
                        }

                    }

                    fncInitializeBatchDetail();
                    //tblBatchBody.children().first().css("background-color", "Yellow");
                    tblBatchBody.children().dblclick(fncBatchRowClick);

                    tblBatchBody[0].setAttribute("style", "cursor:pointer");

                    tblBatchBody.children().on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });

                    tblBatchBody.children().on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });

                    tblBatchBody.children().on('keydown', function () {
                        fncBatchKeyDown($(this), event);
                    });

                    $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                    $("#tblBatch tbody > tr").first().focus();

                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Initialize Batch Detail
        function fncInitializeBatchDetail() {
            try {
                $("#stkadj_batch").dialog({
                    resizable: false,
                    height: 250,
                    width: 633,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {

            try {
                oldMRP = $.trim($(rowObj).find('td[id*=tdMRPNew]').text());
                MRPDEL = $.trim($(rowObj).find('td[id*=tdMRPDEL]').text());
                AlExpDate = $.trim($(rowObj).find('td[id*=tdAllowExpDate]').text());
                $('#<%=txtItemCode.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));
                $('#<%=txtBatchNo.ClientID %>').val(BatchNo);
                $('#<%=txtDescription.ClientID %>').val($.trim($(rowObj).find('td[id*=tdDesc]').text()));
                $('#<%=txtCurrentStock.ClientID %>').val($.trim($(rowObj).find('td[id*=tdStock]').text()));
                $('#<%=txtCost.ClientID %>').val($.trim($(rowObj).find('td[id*=tdCost]').text()));
                $('#<%=txtMRP.ClientID %>').val($.trim($(rowObj).find('td[id*=tdMRPNew]').text()));
                $('#<%=txtSPrice.ClientID %>').val($.trim($(rowObj).find('td[id*=tdSellingPrice]').text()));
                $('#<%=txtW1.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice1]').text()));
                $('#<%=txtW2.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice2]').text()));
                $('#<%=txtW3.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice3]').text()));

                $('#<%=hidAllowExpDate.ClientID %>').val($.trim($(rowObj).find('td[id*=tdAllowExpDate]').text()));
                $("#stkadj_batch").dialog('close');

                var d = new Date($.trim($(rowObj).find('td[id*=tdExpDate]').text()));
                var day = d.getDate();
                var month = d.getMonth() + 1;
                var year = d.getFullYear();
                if (day < 10) {
                    day = "0" + day;
                }
                if (month < 10) {
                    month = "0" + month;
                }
                var date = day + "-" + month + "-" + year;

                $('#<%=txtExpiredDate.ClientID %>').val(date);
                $('#<%=txtExpiredDate.ClientID %>').focus();


                $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                    if (event.keyCode == 13) {
                        $('#<%=lnkAdd.ClientID %>').focus();
                                }
                 });
                 <%-- $('#<%=lnkAdd.ClientID %>').keydown(function (event) {
                                if (event.keyCode == 13) {
                                    $('#<%=lnkAdd.ClientID %>').click();
                                }
                 });--%>
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Clear All Text Box
        function fncClear() {
            try {

                $('#<%=txtItemCode.ClientID %>').val('');
                $('#<%=txtBatchNo.ClientID %>').val('');
                $('#<%=txtDescription.ClientID %>').val('');
                $('#<%=txtCurrentStock.ClientID %>').val('');
                $('#<%=txtCost.ClientID %>').val('0');
                $('#<%=txtMRP.ClientID %>').val('0');
                $('#<%=txtSPrice.ClientID %>').val('0');
                $('#<%=divWprice.ClientID %>').hide();
                $('#<%=txtExpiredDate.ClientID %>').val('');
                $('#<%=txtItemCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Total Value Calculation

        function SetAutoCompleteForItemSearch() {
            $("[id$=txtItemCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmExpiredDateChange.aspx/GetInventoryCode",
                        data: "{ 'Code': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtExpiredDate.ClientID %>').focus();
                    
                    <%-- $('#<%=txtExpiredDate.ClientID %>').keydown(function (event) {
                                if (event.keyCode == 13) {
                                    $('#<%=lnkAdd.ClientID %>').focus();
                                }
                 });--%>
                <%--  $('#<%=lnkAdd.ClientID %>').keydown(function (event) {
                                if (event.keyCode == 13) {
                                    fncAddItemToTable();
                                }
                 });--%>
                    return false;
                },
                minLength: 1
            });
        }
        function fncAddValidation() {
            try {
                var itemCode, batchNo, mrp, duplicateStatus = true;
                if ($('#<%=txtItemCode.ClientID %>').val() == "") {
                     ShowPopupMessageBox("Please Enter Item Code");
                     return false;
                 }
                 else {
                     $("#tblStockAdjustment tbody").children().each(function () {
                         itemCode = $(this).find('td[id*=tdItemcode]').text();
                         batchNo = $(this).find('td[id*=tdBatchNo]').text();
                         mrp = $(this).find('td[id*=tdMRP]').text();
                         if (itemCode == $('#<%=txtItemCode.ClientID %>').val() && batchNo == $('#<%=txtBatchNo.ClientID %>').val() &&
                             parseFloat(mrp) == parseFloat($('#<%=txtMRP.ClientID %>').val())) {
                             ShowPopupMessageBox('<%=Resources.LabelCaption.alert_alreadyadded%>');
                             duplicateStatus = false;
                         }
                     });
                     return duplicateStatus;
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         ///Item Add To 
         function fncAddItemToTable() {

             var tempmrp = 0;
             var tempmrpdel = 0;
             var duplicateStatus = true;
             try {
                 var tblStkBody, row, batchNo, infor = "";
                 var netTotal = 0;

                 var tbody = $("#tblStockAdjustment tbody");
                 if (fncAddValidation() == false) {
                     fncClear();
                     return;
                 }
                 tempmrp = $('#<%=txtMRP.ClientID%>').val();
                 tempmrpdel = $('#<%=txtMRP.ClientID%>').val();
                var sDate = {};
                var Date = $('#<%=txtExpiredDate.ClientID %>').val();
                sDate = Date.split('-');
                var Year, Month;
                Month = sDate[1];
                Year = sDate[2];
                lastRowNo = parseFloat(lastRowNo) + 1;

                tblStkBody = $("#tblStockAdjustment tbody");
                row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                               lastRowNo + "</td><td>" +
                                "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                                $('#<%=txtItemCode.ClientID %>').val() + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                                $('#<%=txtDescription.ClientID %>').val() + "</td><td id='tdCost_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtCost.ClientID %>').val()).toFixed(2) + "</td><td id='tdBatchNo_" + lastRowNo + "' >" +
                                $('#<%=txtBatchNo.ClientID %>').val() + "</td><td id='tdMRP_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtMRP.ClientID %>').val()).toFixed(2) + "</td><td id='tdSellingPrice_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtSPrice.ClientID %>').val()).toFixed(2) + "</td><td id='tdExpiredDate_" + lastRowNo + "' >" +
                                $('#<%=txtExpiredDate.ClientID %>').val() + "</td><td id='tdExpiredMonth_" + lastRowNo + "' >" +
                                Month + "</td><td id='tdExpiredYear_" + lastRowNo + "' >" +
                                Year + "</td><td id='tdWPrice1_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtW1.ClientID %>').val()).toFixed(2) + "</td><td id='tdWPrice2_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtW2.ClientID %>').val()).toFixed(2) + "</td><td id='tdWPrice3_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtW3.ClientID %>').val()).toFixed(2) + "</td><td id='tdActBatchNo_" + lastRowNo + "' >" +
                                $('#<%=txtCurrentStock.ClientID %>').val() + "</td></tr>";
                tblStkBody.append(row);
                fncClear();
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Delete Particular Row
        function fncDeleteItem(source) {
            try {
                var rowObj, totalValue, netTotal;
                rowObj = $(source).parent().parent();

                totalValue = rowObj.find('td[id*=tdTotalValue]').text();

                rowObj.remove();

                lastRowNo = 0;

                $("#tblStockAdjustment tbody").children().each(function () {
                    lastRowNo = parseInt(lastRowNo) + 1;
                    $(this).find('td[id*=tdRowNo]').text(lastRowNo);
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Save Stock Adjustment
        function fncSaveStockAdjustment() {
            var date = "No", desc = "";
            var ExpireDate = "";
            var SplitExp = "";
            var Exp = "", batch = "";
            var SNo = 0;
            try {


                var rowStkDet = "<table>";

                $("#tblStockAdjustment tbody").children().each(function () {
                    date = "yes";
                    SNo = parseFloat(SNo) + 1;
                    ExpireDate = $(this).find('td[id*=tdExpiredDate]').text();
                    SplitExp = ExpireDate.split('-');
                    Exp = SplitExp[2] + '-' + SplitExp[1] + '-' + SplitExp[0];
                    batch = $(this).find('td[id*=tdBatchNo]').text();
                    rowStkDet = rowStkDet + "<Expdate SNO='" + SNo + "' ";
                    rowStkDet = rowStkDet + "Itemcode='" + $(this).find('td[id*=tdItemcode]').text() + "' ";
                    rowStkDet = rowStkDet + " BatchNo='" + batch + "' ";
                    rowStkDet = rowStkDet + " ExpiredDate='" + Exp + "' ";
                    rowStkDet = rowStkDet + " Expiredmonth='" + SplitExp[1] + "' ";
                    rowStkDet = rowStkDet + " ExpiredYear='" + SplitExp[2] + "' ";
                    rowStkDet = rowStkDet + "></Expdate>";
                });

                if (date == "No") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                return false;
            }

            rowStkDet = rowStkDet + "</table>";
            $('#<%=hidStockDetail.ClientID %>').val(escape(rowStkDet));
            lastRowNo = 0;
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    function fncInitializeSave() {
        try {
            ShowPopupMessageBox("Expired Date Updated Successfully");
            $("#tblStockAdjustment").find("tr:gt(0)").remove();
            fncClear();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    //Initialize Save Dialog
    function fncInitializeSaveDialog(msg) {
        try {
            $('#<%=lblSave.ClientID %>').text(msg);
            $("#StockAdjusSave").dialog({
                resizable: false,
                height: 130,
                width: 380,
                modal: true,
                title: "Enterpriser Web"
            });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }


    function fncDecimal() {
        try {
            $('#<%=txtCurrentStock.ClientID %>').number(true, 2);
            $('#<%=txtCost.ClientID %>').number(true, 2);
            $('#<%=txtMRP.ClientID %>').number(true, 2);
            $('#<%=txtSPrice.ClientID %>').number(true, 2);
            $("#<%=txtW1.ClientID %>").number(true, 2);
            $("#<%=txtW2.ClientID %>").number(true, 2);
            $("#<%=txtW3.ClientID %>").number(true, 2);

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });
    $(document).ready(function () {
        $('#<%=txtItemCode.ClientID %>').focus();
    });
    function disableFunctionKeys(e) {
        try {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 116) {
                    if ($("#stkadj_batch").dialog('isOpen') === true) {
                        fncGetLastBatchDetail();
                    }

                }
                e.preventDefault();
            }
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    $(document).ready(function () {
        $('#<%=lnkSave.ClientID %>').click(function () {
            $("#tblStockAdjustment").find("tr:gt(0)").remove();

        });
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <a style="text-decoration: none;">Expired Date Change</a> </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full">

            <div class="Payment_fixed_headers stockAdjustmentEntry">
                <table id="tblStockAdjustment" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Item Description
                            </th>
                            <th scope="col">Cost
                            </th>
                            <th scope="col">BatchNo
                            </th>
                            <th scope="col">MRP
                            </th>
                            <th scope="col">S.Price
                            </th>
                            <th scope="col">Expired Date
                            </th>
                            <th scope="col">Expired Month
                            </th>
                            <th scope="col">Expired Year
                            </th>
                            <th scope="col">WPrice1
                            </th>
                            <th scope="col">WPrice2
                            </th>
                            <th scope="col">WPrice3
                            </th>

                            <th scope="col">InventoryCodeDEL
                            </th>
                            <th scope="col">MRPDEL
                            </th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div id="divItemAddSection" runat="server">
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onchange="fncGetInventoryCode()"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_Description">
                    <div>
                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblCurrentStk" runat="server" Text='<%$ Resources:LabelCaption,lbl_curStock %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtCurrentStock" runat="server" CssClass="form-control-res-right" Text="0" onmousedown="return false;"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblBasicCost" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicCost %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res-right" Enabled="false" onchange="fncTotalValueCalc()"
                            onkeydown="fncMoveFocustoControl(event,'Cost');" Text="0"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    </div>
                    <div class="spilt">
                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res-right" Enabled="false"
                            onkeydown="fncMoveFocustoControl(event,'MRP');">0</asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="lblSPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellPrice %>'></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res-right" Enabled="false"
                            onkeydown="fncMoveFocustoControl(event,'SellingPrice');">0</asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_itemcode">
                    <div>
                        <asp:Label ID="Label4" runat="server" Text="Expired Date"></asp:Label>
                    </div>
                    <div>
                        <asp:TextBox ID="txtExpiredDate" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="stockAdjustment_save_button">
                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='<%$ Resources:LabelCaption,btnAdd %>'
                        OnClientClick="fncAddItemToTable();return false;"></asp:LinkButton>
                </div>
                <div class="stockAdjustment_save_button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='Clear'
                        OnClientClick="fncClear();return false;"></asp:LinkButton>
                </div>
                <div class="stockAdjustment_save_button">
                    <div class="control-button" style="margin-top: 0px">
                        <asp:UpdatePanel ID="upStk" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lnkSave %>'
                                    OnClientClick=" return fncSaveStockAdjustment();" OnClick="lnkSave_Click"></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <div class="stockAdjustment_footer">
                    <div class="col-md-5">
                        <div id="divWprice" runat="server" class="display_none" style="margin-left: 163px;">
                            <div class="col-md-1">
                                <asp:Label ID="Label1" runat="server" Text="W1"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtW1" runat="server" CssClass="form-control-res-right WPrice" Text="0" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:Label ID="Label2" runat="server" Text="W2"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtW2" runat="server" CssClass="form-control-res-right WPrice" Text="0" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                            <div class="col-md-1">
                                <asp:Label ID="Label3" runat="server" Text="W3"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtW3" runat="server" Text="0" CssClass="form-control-res-right WPrice" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <div class="hiddencol">
                <div id="stkadj_batch">
                    <div class="Payment_fixed_headers BatchDetail">
                        <table id="tblBatch" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th id="thBatchNo" scope="col">BatchNo
                                    </th>
                                    <th scope="col">Stock
                                    </th>
                                    <th scope="col">Cost
                                    </th>
                                    <th scope="col">MRP
                                    </th>
                                    <th scope="col">SellingPrice
                                    </th>
                                    <th scope="col">InventoryCode
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="hiddencol">
                <asp:HiddenField ID="hidAddDeduction" runat="server" />
                <asp:HiddenField ID="hidStokAdjCode" runat="server" />
                <asp:HiddenField ID="hidStockDetail" runat="server" />
                <asp:HiddenField ID="hidJsonStockDetail" runat="server" />
                <asp:HiddenField ID="hidBatchText" runat="server" Value="" />
                <asp:HiddenField ID="hidParamWhole" runat="server" Value="" />
                <asp:HiddenField ID="hidPrrList" runat="server" Value="" />
                <asp:HiddenField ID="hidAllowExpDate" runat="server" Value="" />
                <asp:HiddenField ID="hidSavebtn" runat="server" />
                <asp:HiddenField ID="hidDeletebtn" runat="server" />
                <asp:HiddenField ID="hidEditbtn" runat="server" />
                <asp:HiddenField ID="hidViewbtn" runat="server" />
                <div id="StockAdjusSave">
                    <div>
                        <asp:Label ID="lblSave" runat="server" Text="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
