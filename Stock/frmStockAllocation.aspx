﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master"   AutoEventWireup="true" CodeBehind="frmStockAllocation.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockAllocation" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockAllocation');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockAllocation";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

   <script type="text/javascript" language="Javascript">
        function pageLoad() {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }

        $(document).ready(function () {
            $('.pshro_GridDgn input[type=text]').keydown(function (e) {
                if (e.keyCode == 40) { // down
                    $(':focus').nextAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 39) { //right
                    $(':focus').nextAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 38) { //up
                    $(':focus').prevAll('input:not([tabIndex=-1]):first').focus()
                } else if (e.keyCode == 37) { //left
                    $(':focus').prevAll('input:not([tabIndex=-1]):first').focus()
                }
            });
        });

        //$('.pshro_GridDgn input[type=text]').keypress(function (e) {
        //    if (e.keyCode == 13) {
        //        //eerst de td vinden ipv $(this).next anders werkt het niet
        //        $(this).closest('td').next('td').find('input[type=text]').focus();
        //    } else {
        //        return;
        //    }
        //});

        function ShowPopupPO(name) {
            $("#dvPo").dialog({
                title: name,
                width: 500,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }


        function XmlGridValue() {
            try {

                var xml = '<NewDataSet>';
                $("#<%=gvRequestedStock.ClientID %> tr").each(function () {

                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {
                            if ($(this).parents('table:first').find('th').eq(j).text().indexOf('@') != -1) {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';
                            }
                            else {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text() + '>';
                            }
                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'

               // alert(xml);


                $("#<%=HiddenXmldata.ClientID %>").val(escape(xml));
                alert($("#<%=HiddenXmldata.ClientID %>").val()); 


            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        //$("#<%=lstRequestNo.ClientID %>,#<%=lstRequestNo.ClientID %>_chzn,#<%=lstRequestNo.ClientID %>_chzn > div,#<%=lstRequestNo.ClientID %>_chzn > div > div > input").css("width", '100%');

        $(function () {

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        function Savevalue(evt) { 
            //console.log("Saving value " + $(evt.target).val());
            //$(evt.target).focus().select();
            $(evt.target).data('val', $(evt.target).val());        
            
        }

        function onFocusOutQty(evt) {

              var currentRow = $(evt.target).closest("tr");
              var Stock = parseFloat(currentRow.find("td:eq(4)").html());
                //var col1 = currentRow.find("td:eq(0)").html(); // get current row 1st table cell TD value 
                //var col3 = currentRow.find(".CRP").val(); // get current row 3rd table cell  TD value
                //var data = col1 + "\n" + col2 + "\n" + col3;
                var dReqQty = 0;
                $(evt.target).closest("tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) { 
                        for (var j = 0; j < cells.length; ++j) {
                            if ($(this).parents('table:first').find('th').eq(j).text().indexOf('_') != -1) {                                
                                dReqQty += parseFloat($("input:text", cells.eq(j)).val());                                
                            } 
                        } 
                    }
                });

                if (dReqQty > Stock)
                {                   
                    var prev = $(evt.target).data('val'); 
                    var current = $(evt.target).val();
                    $(evt.target).val(prev);
                    //console.log("Prev value " + prev);
                    //console.log("New value " + current);
                    $(evt.target).focus().select();
                    //ShowPopupMessageBox('Invalid Quantity..!');
                    alert('Invalid Quantity..!');
                } 

                XmlGridValue();

            <%--    $('#<%=gvRequestedStock.ClientID%>').find('tr').each(function(row) {
                    $(this).find('input,select,textarea').each(function (Pricetxt) {
                        $ctl = $(this);
                        if ($ctl.is('select')) {
                            //dropdown code
                        } else if  (ctl.is('input:checkbox')) {
                            //checkbox code 
                        }  else {
                            // textbox/textarea code
                        } 
                    });
                });--%>

                //$(evt.target).each(function () {
                //    //if ($(this).find('input:checkbox').attr("checked"))
                //    sum += parseInt($(this).find('input:text').attr("value"));
                //});
        }


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                evt.preventDefault();

            if (charCode == 46 && $(evt.target).val().indexOf('.') != -1) {
                evt.preventDefault();
            }
            if (charCode == 13) {
                XmlGridValue(); 
                Pricetxt = $(evt.target).closest("td").next().find("input");
                Pricetxt.focus().select();
            }
        }
        function fncRequestqtyChange(evt) {

            var POtd = null;
            var TOtd = null;
            var Qtytxt = null;
            var Stocktd = null;
            var ActualQtytd = null;

            Qtytxt = $(evt.target);
            Stocktd = $(evt.target).closest("td").next();
            POtd = $(evt.target).closest("td").next().next().next();
            TOtd = $(evt.target).closest("td").next().next().next();
            ActualQtytd = $(evt.target).closest("tr").children('td:last');

            var ReqQty = parseFloat(Qtytxt.val());
            var Stock = parseFloat(Stocktd.text());
            var ActualQty = parseFloat(ActualQtytd.text());

            if (ReqQty > Stock) {
                ShowPopupMessageBox('Req Qty is Greater than Stock..!');
                Qtytxt.val(ActualQty.toFixed(3));
                Qtytxt.focus();
                return;
            }

            if (ReqQty <= Stock) {
                TOtd.text(ReqQty.toFixed(3));
                Qtytxt.val(ReqQty.toFixed(3));
            }

            return true;
        }

        //function isReqQtyText(evt) {

        //    var charCode = (evt.which) ? evt.which : evt.keyCode;
        //    if (charCode != 46 && charCode > 31
        //    && (charCode < 48 || charCode > 57))
        //        return false;

        //    if (charCode == 13) {
        //        //alert('enter');
        //        //                var Pricetxt = $(evt.target).closest("td").next().find("input");
        //        //                Pricetxt.focus().select();
        //        fncRequestqtyChange(evt);

        //    }
        //    return true;
        //}

        function isReqQtyText(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57) && !(charCode > 95 && charCode < 106) && charCode != 110) {
                    return false;
                    evt.preventDefault();
                }

                if (charCode == 13) {

                    fncRequestqtyChange(evt);
                }
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function GetValues() {
            var values = "";
            var listBox = document.getElementById("<%= lstRequestNo.ClientID%>");
            for (var i = 0; i < listBox.options.length; i++) {
                if (listBox.options[i].selected) {
                    values += listBox.options[i].innerHTML + " " + listBox.options[i].value + "\n";
                }
            }
            $("#hidenReqNo").val(values);
            //alert(values);
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="maincontainer" style="padding: 10px 3px 3px;">
        <div class="breadcrumbs">
        <ul>
        <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul></div>
        <div class="col-md-2 form_bootstyle_panel" id="divDateFilter">
            <div class="container-invoice-header">
                DATE FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12 ">
                    <div class="col-md-5">
                        <asp:Label ID="Label4" Font-Bold="true" runat="server" Text="From Date"></asp:Label>
                    </div>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                    <div class="col-md-5" style="margin-top: 5px;">
                        <asp:Label ID="Label5" Font-Bold="true" runat="server" Text="To Date"></asp:Label>
                    </div>
                    <div class="col-md-7" style="margin-top: 5px;">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style="text-align: center;"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 form_bootstyle_panel" id="divLocationFilter">

            <div class="container-invoice-header">
                LOCATION FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <asp:Label ID="Label3" Font-Bold="true" runat="server" Text="Location"></asp:Label>
                    </div>
                    <div class="col-md-8">
                        <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4" style="margin-top: 5px;">
                        <asp:Label ID="Label18" Font-Bold="true" runat="server" Text="Vendor"></asp:Label>
                    </div>
                    <div class="col-md-8" style="margin-top: 5px;">
                        <asp:DropDownList ID="DropdownVendor" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-md-3 form_bootstyle_panel" id="divRequestFilter" style="display:none;">
            <div class="container-invoice-header">
                REQUEST NO FILTER
            </div>
            <div class="small-box bg-aqua" style="margin-top: 5px;">
                <div class="col-md-12">
                    <asp:ListBox ID="lstRequestNo" runat="server" Rows="3" CssClass="form-control-res" SelectionMode="Multiple"></asp:ListBox>
                </div>
            </div>
        </div>

        <div class="col-md-2" style="padding: 20px;">

            <div class="col-md-8">
                <asp:LinkButton ID="lnkTransferOut" runat="server" class="button-blue"
                    Text="TransferOut" OnClientClick="XmlGridValue()" OnClick="lnkTransferOut_Click"></asp:LinkButton>
            </div>
            <div class="col-md-4">
                <asp:LinkButton ID="lnkLoadPendingTO" runat="server" class="button-blue"
                    Text="Fetch" OnClientClick="GetValues()" OnClick="lnkLoadPendingTO_Click"></asp:LinkButton>
            </div>


        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-md-12" style="margin-top: 3px; height: 100%;" id="divIndentRequest">
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                        style="height: 400px">
                        <asp:GridView ID="gvRequestedStock" runat="server" Width="100%" AutoGenerateColumns="false"
                            ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                            CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                            OnRowDataBound="gvRequestedStock_DataBound">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <%-- <asp:BoundField DataField="ReqQty" HeaderText="ActualQty" ItemStyle-CssClass="hiddencol"
                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hidenReqNo" runat="server" Value="" /> 
        <input type="hidden" id="HiddenXmldata" runat="server" /> 

    </div>

    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
  
 
</asp:Content>
