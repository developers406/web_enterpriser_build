﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmStockUpdatePosting.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockUpdatePosting" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }
    </style>
    <script type="text/javascript" language="Javascript">
       <!--

        function pageLoad() {

            if ($('#<%=hidBatchDeativation.ClientID%>').val() != '') {
                $('#navigation').hide();
                $('#ContentPlaceHolder1_HideFilter_GridOverFlow').css('height','300');
                
            }
        if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
           <%-- $('#<%=lnkUpdate.ClientID %>').css("display", "block");--%>
        }
        else {
            $('#<%=lnkUpdate.ClientID %>').css("display", "none");
        }
        if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
            <%--$('#<%=Delete.ClientID %>').css("display", "block");--%>
            }
            else {
                $('#<%=Delete.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
        }
        function Ink_Checkbox_Check() {
            var total = $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").length
              var selected = $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]:checked").length
              var Balance = parseInt(total) - parseInt(selected);
              $("#id_Total").text(total);
              $("#id_Selected").text(selected);
              $("#id_Balance").text(Balance);


              //alert(total);
              //alert(selected);

          }

          function isNumberKey(evt) {
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                  return false;

              return true;
          }
          function fncValidateCheckAll() {

              try {
                  var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
            if (gridtr.length == '0') {
                //AlertNoItems
                fncShowAlertNoItemsMessage();
            }
            else {
                if ($("#<%=Check.ClientID %>").text() == "Select All") {

                    $("#<%=Check.ClientID %>").text("De-Select All");
                    $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").prop('checked', true);
                    Ink_Checkbox_Check();

                }
                else {

                    $("#<%=Check.ClientID %>").text("Select All");
                    $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").prop('checked', false);
                    Ink_Checkbox_Check();

                }
            }

        }
        catch (err) {
            alert(err.message);
            console.log(err);
        }
    }
    function fncShowAlertNoItemsMessage() {
        try {
            InitializeDialogAlertNoItems();
            $("#AlertNoItems").dialog('open');
        }
        catch (err) {
            alert(err.message);
            console.log(err);
        }
    }
    //Close Save Dialog
    function fncCloseAlertNoItemsDialog() {
        try {
            $("#AlertNoItems").dialog('close');
            return false;

        }
        catch (err) {
            alert(err.message);
        }
    }
    //Save Dialog Initialation
    function InitializeDialogAlertNoItems() {
        try {
            $("#AlertNoItems").dialog({
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true
            });
        }
        catch (err) {
            alert(err.message);
        }
    }
    function fncValidateSave() {
        try {
            var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
            if (gridtr.length == '0') {
                //AlertNoItems
                fncShowAlertNoItemsMessage();
            }
            else {
                //alert($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked"));
                if ($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                    fncShowConfirmSaveMessage();
                } else {
                    fncShowMessage();
                }
            }

        }
        catch (err) {
            alert(err.message);
            console.log(err);
        }
    }
    function fncValidateDelete() {
        try {
            var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
            if (gridtr.length == '0') {
                //AlertNoItems
                fncShowAlertNoItemsMessage();
            }
            else {
                //alert($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //alert("message");
                InitializeDialog1();
                $("#StockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncCloseDeleteDialog() {
            try {
                $("#StockUpdatePosting").dialog('close');


            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdateSelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdateSelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdateSelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
                var total = $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").length
                var selected = $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]:checked").length
                var Balance = parseInt(total) - parseInt(selected);
                $("#id_Total").text(total);
                $("#id_Selected").text(selected);
                $("#id_Balance").text(Balance);
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockUpdatePosting');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockUpdatePosting";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                    <li class="active-page">
                        <%=Resources.LabelCaption.lbl_Stock_Update_Posting%></li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
            <div class="container-group-price">
                <div class="container-left-price" id="pnlFilter" runat="server">
                    <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                        EnableVendorDropDown="true"
                        EnableDepartmentDropDown="true"
                        EnableCategoryDropDown="true"
                        EnableBrandDropDown="true"
                        EnableItemCodeTextBox="true"
                        EnableItemNameTextBox="true"
                        EnableFilterButton="true"
                        EnableClearButton="true"
                        OnClearButtonClientClick="clearForm(); return false;"
                        OnFilterButtonClick="lnkLoadFilter_Click" />
                    <table style="width: 100%" border="1">
                        <thead>
                            <tr>
                                <th>Total
                                </th>
                                <th>Selected
                                </th>
                                <th>Balance
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr align="center">
                                <td id="id_Total"></td>
                                <td id="id_Selected"></td>
                                <td id="id_Balance"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                    style="height: 560px">
                    <div class="control-group-split" style="width: 65%; float: left">
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text=" *Remarks"></asp:Label>
                                </div>
                                <div class="label-right" id="ddl">
                                    <asp:DropDownList ID="ddlRemarks" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                        style="height: 490px">
                        <asp:GridView ID="gvStockUpdate" runat="server" Width="100%" AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                            CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector" OnRowDataBound="gvStockUpdate_DataBound">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSingle" runat="server" onclick="Ink_Checkbox_Check();" Width="40px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                <asp:BoundField DataField="BatchNo" HeaderText="Batch No" />
                                <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                <asp:BoundField DataField="BatchPrice" HeaderText="Selling Price"></asp:BoundField>
                                <asp:BoundField DataField="QtyInHand" HeaderText="Current Stock" />
                                <asp:BoundField DataField="NewQty" HeaderText="Scanned Qty" />
                                <asp:BoundField DataField="Cost" HeaderText="Cost"></asp:BoundField>
                                <asp:BoundField DataField="NewQty" HeaderText="New Stock" />
                                <asp:BoundField DataField="TotQty" HeaderText="Variance" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div style="float: right; display: table">

                        <div class="control-button">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                                Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click"
                                OnClientClick="fncValidateSave();return false;" Text='<%$ Resources:LabelCaption,btn_Update %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                                Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click"
                                Text="Print">
                            </asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                                Text="Delete"></asp:LinkButton>
                        </div>
                        <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
                    </div>

                </div>

            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSelectAny">
            <p>
                Total Quantity must be Zero.Do You Want Update ?
            </p>
            <div style="width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteStockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdatePosting">
            <p>
               
                Stock Update Successfully
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidBatchDeativation" runat="server" Value ="" />
</asp:Content>
