﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmOpeningStockReset.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmOpeningStockReset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

       
        .stockReset td:nth-child(1), .stockReset th:nth-child(1)
        {
            min-width: 80px;
            max-width: 80px;
        }
        .stockReset td:nth-child(2), .stockReset th:nth-child(2)
        {
            min-width: 110px;
            max-width: 110px;
        }
        .stockReset td:nth-child(3), .stockReset th:nth-child(3)
        {
            min-width: 400px;
            max-width: 400px;
        }
        .stockReset td:nth-child(4), .stockReset th:nth-child(4)
        {
            min-width: 130px;
            max-width: 130px;
        }
        .stockReset td:nth-child(5), .stockReset th:nth-child(5)
        {
            min-width: 130px;
            max-width: 130px;
        }
        .stockReset td:nth-child(6), .stockReset th:nth-child(6)
        {
            min-width: 130px;
            max-width: 130px;
        }
        .stockReset td:nth-child(7), .stockReset th:nth-child(7)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .stockReset td:nth-child(8), .stockReset th:nth-child(8)
        {
            min-width: 150px;
            max-width: 150px;
        }
        .stockReset td:nth-child(9), .stockReset th:nth-child(9)
        {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
      <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'OpeningStockReset');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "OpeningStockReset";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="barcodepahe_color">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_OpeningStockReset%>
                </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full">
            <div class="Payment_fixed_headers stockReset">
                <asp:UpdatePanel runat="server" ID="upStockAdjustment" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblStockAdjusmentSummmery" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">
                                        S.No
                                    </th>
                                    <th scope="col">
                                        Inventory Code
                                    </th>
                                    <th scope="col">
                                        Description
                                    </th>
                                    <th scope="col">
                                        MRP
                                    </th>
                                    <th scope="col">
                                        Cost
                                    </th>
                                    <th scope="col">
                                        SellingPrice
                                    </th>
                                    <th scope="col">
                                        BatchNo
                                    </th>
                                    <th scope="col">
                                        Qty On Hand
                                    </th>
                                    <th scope="col">
                                        <asp:CheckBox ID="cbHeader" runat="server" />
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrStkAdj" runat="server">
                                <HeaderTemplate>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="AdditionBodyRow" runat="server">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblInventorycode" runat="server" Text='<%# Eval("StkAdjNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("StkAdjDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("StkAdjTypeCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCost" runat="server" Text='<%# Eval("Remarks") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSellingPrice" runat="server" Text='<%# Eval("NetTotal") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("CreateUser") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblQtyonhand" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbRow" runat="server" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                                <tr id="emptyrow" class="Stock_Reset_Footer">
                                    <td colspan="10">
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilter" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Filter %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
