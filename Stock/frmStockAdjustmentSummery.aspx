﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmStockAdjustmentSummery.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockAdjustmentSummery" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .stockAdjustment_View td:nth-child(1), .stockAdjustment_View th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
        }

        .stockAdjustment_View td:nth-child(2), .stockAdjustment_View th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
        }


        .stockAdjustment_View td:nth-child(3), .stockAdjustment_View th:nth-child(3) {
            min-width: 55px;
            max-width: 55px;
        }

        .stockAdjustment_View td:nth-child(4), .stockAdjustment_View th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .stockAdjustment_View td:nth-child(5), .stockAdjustment_View th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustment_View td:nth-child(6), .stockAdjustment_View th:nth-child(6) {
            min-width: 200px;
            max-width: 200px;
        }

        .stockAdjustment_View td:nth-child(7), .stockAdjustment_View th:nth-child(7) {
            min-width: 250px;
            max-width: 250px;
        }

        .stockAdjustment_View td:nth-child(8), .stockAdjustment_View th:nth-child(8) {
            min-width: 110px;
            max-width: 110px;
        }

        .stockAdjustment_View td:nth-child(9), .stockAdjustment_View th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustment_View td:nth-child(10), .stockAdjustment_View th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .stockAdjustment_View td:nth-child(11), .stockAdjustment_View th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
        }

        .todate {
            margin-left: -582px;
            margin-right: 407px;
        }

        .filter {
            margin-top: -33px;
            margin-left: 619px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockAdjustmentSummary');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockAdjustmentSummary";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">
        var rowObj, stockAdjNo;
        function pageLoad() {
            try {
                <%--if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkAdd.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkAdd.ClientID %>').css("display", "none");
                }--%>


                stockAdjNo = "", rowObj = "";

                $("select").chosen({ width: '100%' });
                if ($("#<%= hidFromDete.ClientID %>").val() != ""){ 
                    $("#<%= txtFromDate.ClientID %>").val($("#<%= hidFromDete.ClientID %>").val());
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                    }
                else
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "-1");
                if ($("#<%= hidTodate.ClientID %>").val() != ""){
                    $("#<%= txtToDate.ClientID %>").val($("#<%= hidTodate.ClientID %>").val()); 
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                    }
                else
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Delte Stock Adjustment
        function fncDeleteStockAdj(stockAdjNo) {
            try {
                var rowNo = 0;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmStockAdjustmentSummery.aspx/fncStockAdjustmentDelete")%>',
                    data: "{'stkAdjNo':'" + stockAdjNo + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            $("#divStockDelete").dialog('close');
                            ShowPopupMessageBox(stockAdjNo + '<%=Resources.LabelCaption.save_stockAdjDelete%>');
                            rowObj.remove();

                            $("#tblStockAdjusmentSummmery tbody").children().each(function () {
                                rowNo = rowNo + 1;
                                $(this).find('td span[id*=lblSNo]').text(rowNo);
                            });
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.responseText);
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDeleteStkAdj(soure) {
            try {
                rowObj = $(soure).parent().parent();
                stockAdjNo = $.trim(rowObj.find('td span[id*=lblStkAdjNo]').text());
                fncInitializeStockAdjustment();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Open GidHold Dialog
        function fncInitializeStockAdjustment() {
            try {
                $("#<%=lblStockDel.ClientID%>").text(stockAdjNo + '<%=Resources.LabelCaption.alert_StockDelete%>');
                $("#divStockDelete").dialog({
                    resizable: false,
                    height: 130,
                    width: 240,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Stock Adjustment Delete
        function fncConfirmToDelete(value) {
            try {
                
                if (value == "Yes") {
                    if ($("#<%=hidDeletebtn.ClientID%>").val() != "D1") {
                        $("#divStockDelete").dialog('close');
                        ShowPopupMessageBox("You have no permission to Delete this Stock Adjustment");
                        return false;
                    }
                    fncDeleteStockAdj(stockAdjNo);
                }
                else
                    $("#divStockDelete").dialog('close');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetStockNo(soure) {
            try {
                var currentRow;
                currentRow = $(soure).parent().parent();
                $("#<%=hidStockAdjusNo.ClientID%>").val($.trim(currentRow.find('td span[id*=lblStkAdjNo]').text()));

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get Stock Adjustment Number
        function fncRowClick(source) {
            try {

              <%--  var currentRow;
                currentRow = $(source).parent().parent();
                $("#<%=hidStockAdjusNo.ClientID%>").val($.trim(currentRow.find('td span[id*=lblStkAdjNo]').text()));--%>

                $(source).css("background-color", "#8080ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "white");
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Row Selected Or Not
        function fncCheckRowSelectedOrNot(source) {
            try {
                var currentRow;

                currentRow = $(source).parent().parent();
                $("#<%=hidStockAdjusNo.ClientID%>").val($.trim(currentRow.find('td span[id*=lblStkAdjNo]').text()));
                if ($("#<%=hidStockAdjusNo.ClientID%>").val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_anyOneRow%>');
                    return false;
                }
                else {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkPrint', '');
                }
                fncRowClick(source);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDiscontinue() {
            $(function () {
                $("#dialog-DiscontinueEntry").html('<%=Resources.LabelCaption.lbl_loadDiscontinueEntry%>');
                $("#dialog-DiscontinueEntry").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            $("#<%=lnkDisEntry.ClientID%>").click();
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                            $("#<%=hidNew.ClientID%>").val('New');
                            $("#<%=lnkDisEntry.ClientID%>").click();
                        }
                    },
                    modal: true
                });
            });
        };

        function ValidateEndDate() {
            var startDate = $("#<%=txtFromDate.ClientID%>").val();
            var endDate = $("#<%=txtToDate.ClientID%>").val();
            if (startDate != '' && endDate != '') {
                if (Date.parse(startDate) > Date.parse(endDate)) {
                    $("#<%=txtToDate.ClientID%>").val('');
                    ShowPopupMessageBox("From date should not be greater than To date");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                ShowPopupMessageBox("Please Enter the Date");
                return false;
            }

        }
     function fncPermision() {  //surya27112021
         try {
             if ($('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                 $("#adminauthentication").dialog({
                     height: 120,
                     width: "auto",
                     modal: true
                 });
                   return false;
             }
             else {
                 $("#<%=lnkAddPath.ClientID%>").click();
             }
         }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
     }
        function fncAdminAuthentication(evt) {
            try {

                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {

                    var obj = {};
                    obj.password = $("#<%=txtPassword.ClientID %>").val();


                    if (obj.password == "") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Enterpassword%>');
                        return;
                    }

                    $.ajax({
                        type: "POST",
                        url: "frmStockAdjustmentSummery.aspx/fncPasswordAuthantication",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {

                            if (msg.d == "InvalidPassword") {
                               
                                ShowPopupMessageBox('<%=Resources.LabelCaption.msgInvalidPassword%>');
                                
                                
                            }
                            else if (msg.d == "Success") {
                                fncAuthenticationDialogClose();
                                $("#<%=lnkAddPath.ClientID%>").click();
                            }

                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                            return false;
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAuthenticationDialogClose() {
            try {
                $("#adminauthentication").dialog('close');
                $("#<%=txtPassword.ClientID %>").val('');
      
              

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
            <li><a href="../Stock/frmStockAdjustmentSummery.aspx">StockSummary</a> <i class="fa fa-angle-right"></i></li>
            <li class="active-page">
                <%=Resources.LabelCaption.lbl_stkAdjustment%>
            </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="supplierNote-group-full">
        <div>
            <div class="float_left">
                <div class="float_left">
                    <asp:Label ID="lblstkAdjtype" runat="server" Text='<%$ Resources:LabelCaption,lbl_stkAdjtype %>'></asp:Label>
                </div>
                <div class="freeItem_groupCode">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlStkAdjType" runat="server" Style="width: 100%" OnSelectedIndexChanged="ddlStkAdjType_SelectedIndexChanged"
                                AutoPostBack="True" CssClass="form-control-res">
                            </asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="float_left" style="margin-left: 130px;">
                    <asp:Label ID="Label1" runat="server" Text="FromDate"></asp:Label>
                </div>

                <div class="float_left" style="margin-left: 10px;">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                </div>
                <div class="control-group-single-res">
                    <div class="float_left" style="margin-left: 366px;">
                        <asp:Label ID="Label2" runat="server" Text="ToDate"></asp:Label>
                    </div>

                    <div class="float_right todate">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                    </div>
                </div>
                <div class="float_left filter">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilter" runat="server" class="button-blue" Text="Filter"
                            OnClick="lnkDatefilter_Click" OnClientClick="return ValidateEndDate();"> </asp:LinkButton>
                        <%-- <button id="btFilter" type="button" class="button-blue">Filter</button>--%>
                    </div>
                </div>
            </div>
            <div class="freeItemView_button">
                <div class="control-button">
                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>' 
                  OnClientClick="return fncPermision();" ></asp:LinkButton>
                </div>
                <div class="control-button">
                    <%--<asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" Text=""
                        OnClick="lnkPrint_Click" OnClientClick="return fncCheckRowSelectedOrNot();"></asp:LinkButton>--%>
                </div>
            </div>
        </div>
        <div>
            <asp:UpdatePanel runat="server" ID="upStockAdjustment" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="Payment_fixed_headers stockAdjustment_View">
                        <table id="tblStockAdjusmentSummmery" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Delete
                                    </th>
                                    <th scope="col">View
                                    </th>
                                    <th scope="col">Stk.Adj.No
                                    </th>
                                    <th scope="col">Stk.Adj.Date
                                    </th>
                                    <th scope="col">Stk.Adj.Type
                                    </th>
                                    <th scope="col">Remarks
                                    </th>
                                    <th scope="col">Net Total
                                    </th>
                                    <th scope="col">Created User
                                    </th>
                                    <th scope="col">Created Date
                                    </th>
                                    <th scope="col">Print
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrStkAdj" runat="server">
                                <HeaderTemplate>
                                    <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="AdditionBodyRow" runat="server" onclick="fncRowClick(this);">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                        </td>
                                        <td>
                                            <img src="../images/No.png" alt="Delete" onclick="fncDeleteStkAdj(this);" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnview" runat="server" Text="View" OnClick="btnview_Click" OnClientClick="fncGetStockNo(this);" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStkAdjNo" runat="server" Text='<%# Eval("StkAdjNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStkAdjDate" runat="server" Text='<%# Eval("StkAdjDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStkAdjtype" runat="server" Text='<%# Eval("StkAdjTypeCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNetTotal" runat="server" Text='<%# Eval("NetTotal") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateUser" runat="server" Text='<%# Eval("CreateUser") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnprint" runat="server" Text="Print" OnClick="lnkPrint_Click" OnClientClick="fncCheckRowSelectedOrNot(this);" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                            </tfoot>
                        </table>

                    </div>
                    <ups:PaginationUserControl runat="server" ID="StkadjPaging" OnPaginationButtonClick="StkadjPaging_PaginationButtonClick" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="hiddencol">
            <div id="divStockDelete">
                <div>
                    <asp:Label ID="lblStockDel" runat="server"></asp:Label>

                </div>
                <div class="paymentDialog_Center">
                    <div class="PaymentDialog_yes">
                        <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'
                            OnClientClick="fncConfirmToDelete('Yes');return false;"> </asp:LinkButton>
                    </div>
                    <div class="PaymentDialog_No">
                        <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblNo %>'
                            OnClientClick="fncConfirmToDelete('No');return false;"> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hidStockAdjusNo" runat="server" />
            <asp:HiddenField ID="hidNew" runat="server" />

        </div>
        <div id="dialog-DiscontinueEntry">
        </div>
        <div class="display_none">
            <asp:Button ID="lnkDisEntry" runat="server" OnClick="lnkDisEntry_Click" />
            <asp:Button ID="lnkAddPath" runat="server" OnClick="lnkAdd_Click" />
         </div>
         <div id="adminauthentication" title="Authentication" style="display:none">
                <div>
                    <asp:Label ID="lblCountPrint" runat="server" ></asp:Label>
                </div>
                <div>
                    <asp:Label ID="lblPassword" runat="server" Text="Please Enter Admin Password"></asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control-res"
                        onkeydown="fncAdminAuthentication(event);"></asp:TextBox>
                </div>
            </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidFromDete" runat="server" Value="" />
    <asp:HiddenField ID="hidTodate" runat="server" Value="" />
    <asp:HiddenField ID="hidFilter" runat="server" Value="" />
</asp:Content>
