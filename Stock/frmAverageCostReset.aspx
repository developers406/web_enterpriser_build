﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmAverageCostReset.aspx.cs" Inherits="EnterpriserWebFinal.Management.frmAverageCostReset" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type ="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'AverageCostReset');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "AverageCostReset";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript">

        var fromdatectrl;
        var todatectrl;
        var txtPricectrl;

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkUpdate.ClientID %>').css("display", "none");
             }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc 
        }

        //         try {
        //                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
        //                if (keyCode == 13) {
        //                    return false;
        //                }
        //            }
        //            catch (err) {
        //                alert(err.Message);
        //            }


        function fncHideFilter() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {

                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //onkeypress = "return isNumberKey(event)" 
        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            if (charCode == 13) {
                $(evt.target).closest("tr").next().find('input[id*=txtNewCost]').focus().select();
            }
            return true;
        }

        function clearForm() {
            try {

                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                $(':checkbox, :radio').prop('checked', false);

                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }
        function ValidateUpdate() {
            try {
                var counter = 0;
                $("#<%=grdItemDetails.ClientID%> input[id*='chkSelect']:checkbox").each(function (index) {
                    if ($(this).is(':checked'))
                        counter++;
                });
                //alert(counter);
                if (counter > 0) {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("Do you want to Reset Average Cost?")) {
                        confirm_value.value = "Yes";

                    } else {
                        confirm_value.value = "No";
                    }
                    document.forms[0].appendChild(confirm_value);
                }
                else {

                    alert("No Item is Selected !");
                    return false;
                }

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
        function checkAll(objRef) {
            try {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    //Get the Cell To find out ColumnIndex
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            //If the header checkbox is checked
                            //check all checkboxes
                            //and highlight all rows
                            row.style.backgroundColor = "aqua";
                            inputList[i].checked = true;
                        }
                        else {
                            //If the header checkbox is checked
                            //uncheck all checkboxes
                            //and change rowcolor back to original 
                            if (row.rowIndex % 2 == 0) {
                                //Alternating Row Color
                                row.style.backgroundColor = "#c4ddff";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }

            }
            catch (err) { 
                alert(err.Message);
            }
        }

        function Check_Click(objRef) {

            try {
                //Get the Row based on checkbox
                var row = objRef.parentNode.parentNode;
                if (objRef.checked) {
                    //If checked change color to Aqua
                    row.style.backgroundColor = "aqua";
                }
                else {
                    //If not checked change back to original color
                    if (row.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        row.style.backgroundColor = "#c4ddff";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                }
                //Get the reference of GridView
                var GridView = row.parentNode;

                //Get all input elements in Gridview
                var inputList = GridView.getElementsByTagName("input");

                for (var i = 0; i < inputList.length; i++) {
                    //The First element is the Header Checkbox
                    var headerCheckBox = inputList[0];
                    //Based on all or none checkboxes
                    //are checked check/uncheck Header Checkbox
                    var checked = true;
                    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                        if (!inputList[i].checked) {
                            checked = false;
                            break;
                        }
                    }
                }
                headerCheckBox.checked = checked;
            }
            catch (err) {
                alert(err.Message);
            }
        } 

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li class="active-page">AverageCost Reset </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" 
                    onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl" 
                
                            EnableVendorDropDown = "true"
                            EnableDepartmentDropDown = "true"
                            EnableCategoryDropDown = "true"
                            EnableSubCategoryDropDown= "true"
                            EnableBrandDropDown = "true"
                            EnableMerchandiseDropDown = "true" 
                            EnableManufactureDropDown = "true" 
                            EnableFloorDropDown = "true" 
                            EnableSectionDropDown = "true" 
                            EnableBinDropDown = "true" 
                            EnableShelfDropDown = "true" 
                            EnableWarehouseDropDown = "true" 
                            EnableItemCodeTextBox = "true" 
                            EnableItemNameTextBox = "true"
                            EnableFilterButton = "true"
                            EnableClearButton = "true"

                    OnClearButtonClientClick="clearForm(); return false;"
                    OnFilterButtonClick="lnkLoadFilter_Click" />
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <div class="gridDetails grid-overflow-default" id="HideFilter_GridOverFlow" runat="server">
                    <asp:GridView ID="grdItemDetails" runat="server" Width="100%" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                        CssClass="pshro_GridDgn">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                        <PagerStyle CssClass="pshro_text" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" onclick="Check_Click(this)" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                            <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                            <asp:BoundField DataField="Cost" HeaderText="Net Cost"></asp:BoundField>
                            <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                            <asp:TemplateField HeaderText="Old AverageCost" ItemStyle-Width="45">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOldCost" runat="server" onMouseDown="return false" Text='<%# Eval("OldAverageCost") %>'
                                        CssClass="grid-textbox"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="New AverageCost" ItemStyle-Width="45">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNewCost" Text='<%# Eval("NewAveragecost") %>' runat="server"
                                        onkeypress="return isNumberKey(event)" CssClass="grid-textbox" Style="background-color: White"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DepartmentCode" HeaderText="Department" />
                            <asp:BoundField DataField="CategoryCode" HeaderText="Category" />
                            <asp:BoundField DataField="BrandCode" HeaderText="Brand" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="container-bottom-invchange">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick=" return fncHideFilter()"
                            Text='<%$ Resources:LabelCaption,btn_hide_filter %>' class="button-blue"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick=" return ValidateUpdate()"
                            Text='<%$ Resources:LabelCaption,btn_update %>' OnClick="lnkUpdate_Click"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnclear %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkReport" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_report %>'></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" /> 
</asp:Content>
