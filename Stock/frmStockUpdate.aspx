﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmStockUpdate.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmStockUpdate" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 55px;
            max-width: 55px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 250px;
            max-width: 250px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 95px;
            max-width: 95px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            display: none;
        }
          .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 135px;
            max-width: 135px;
            text-align: right !important;
        }
            .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }
              .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }
               .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            min-width: 110px;
            max-width: 110px;
            text-align: left !important;
        }
                .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }
                 .grdLoad td:nth-child(18), .grdLoad th:nth-child(18) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad td {
            padding: 2px !important;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'StockUpdate');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "StockUpdate";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <script type="text/javascript" language="Javascript">

        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
                    $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                    $('#<%=InkExport.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                    $('#<%=InkExport.ClientID %>').css("display", "block");
                }
                var newOption = {};
                var option = '';
                $("#<%=ddlSort.ClientID %>").empty();

                $('#tblStockUpdate th').each(function (e) {
                    var index = $(this).index();
                    var table = $(this).closest('table');
                    var val = $.trim(table.find('.click th').eq(index).text());

                    if ($(this).is(":visible")) {
                        if (val != "Select") {
                            if (val != "New Qty")
                                option += '<option value="' + val + '">' + val + '</option>';
                        }
                    }
                });
                $("#<%=ddlSort.ClientID %>").append(option);
                $("#<%=ddlSort.ClientID %>").prop('selectedIndex', 2);
                $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");
                $("select").chosen({ width: '100%' });

                $('#lblUp').live('click', function (event) {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "green");
                    var length = $("[id*=gvStockUpdate] td").length;
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex; //
                    if (length > 0) {
                        var tdArray = $("[id*=gvStockUpdate]").closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
                        if (columnIndex == 2 || columnIndex == 7) {
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData < nData ? -1 : 1;
                            });
                        }
                        else {
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                            });
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#<%=gvStockUpdate.ClientID %>').append(row);
                        });
                    }
                    else {
                        $('#lblDown').css("color", "");
                        $('#lblUp').css("color", "");
                        ShowPopupMessageBox("Invalid Records");
                        return false;
                    }


                });
                $('#lblDown').live('click', function (event) {
                    $('#lblDown').css("color", "green");
                    $('#lblUp').css("color", "");
                    var length = $("[id*=gvStockUpdate] td").length;
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
                    if (length > 0) {
                        var tdArray = $("[id*=gvStockUpdate]").closest("table").find("tr td:nth-child(" + (columnIndex + 2) + ")");
                        if (columnIndex == 2 || columnIndex == 7) {
                            tdArray.sort(function (p, n) {
                                var pData = $.trim($(p).text().toUpperCase());
                                var nData = $.trim($(n).text().toUpperCase());
                                return pData > nData ? -1 : 1;
                            });
                        }
                        else {
                            tdArray.sort(function (p, n) {
                                var pData = parseFloat($.trim($(p).text()));
                                var nData = parseFloat($.trim($(n).text()));
                                return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                            });
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#<%=gvStockUpdate.ClientID %>').append(row);
                        });

                        }
                        else {
                            $('#lblDown').css("color", "");
                            $('#lblUp').css("color", "");
                            ShowPopupMessageBox("Invalid Records");
                            return false;
                        }

                });
                }
                catch (err) {
                    fncToastError(err.message);
                }

            }

        function isNumberKey(evt, source) {
            var row = source.parentNode.parentNode;
            var uom = row.cells[18].innerHTML;
          
                var charCode = (evt.which) ? evt.which : evt.keyCode;
            
            if ($.trim(uom) == "PCS") {  // surya25102021 chck PCS and KG
                if (charCode == 46) {
                    return false;
                }
            }
                if (charCode == 13) {
                    
                    //alert($(evt.target).attr('id'));
                    var tr = $(evt.target).closest('tr');
                    tr.find('input[id *= chkSingle]').prop('checked', true);
                    tr.next().find('input[id *= txtNewQty]').focus();
                    return false;
                }

                if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
            function fncValidateCheckAll() {
                try {
                    var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").prop('checked', true);

                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateExportNoItems() {
            try {
                var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems

                    fncShowAlertNoItemsMessage();
                    return false;

                }
                else {
                    return true;

                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateAllZero() {
            try {
                var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    $("#<%=gvStockUpdate.ClientID %> [id*=txtNewQty]").val('0.000');
                    //alert($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked"));
                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvStockUpdate.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvStockUpdate.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdate").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdate").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdate").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        function clearForm() {
            //alert("Clear");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("select").val(0);
            $("select").trigger("liszt:updated");
            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }

        function fncHideFilter() {
            try {

                <%-- if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {

                $("[id*=pnlFilter]").hide();
                $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");

            $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                    'width': '100%'
                });

                $("select").trigger("liszt:updated");
                $('.grdLoad td:nth-child(1)').css("min-width", "55px");
                $('.grdLoad td:nth-child(1)').css("max-width", "55px");
                $('.grdLoad td:nth-child(1)').css("text-align", "center !important");

                $('.grdLoad td:nth-child(2)').css("min-width", "70px");
                $('.grdLoad td:nth-child(2)').css("max-width", "70px");
                $('.grdLoad td:nth-child(2)').css("text-align", "center !important");

                $('.grdLoad td:nth-child(3)').css("min-width", "85px");
                $('.grdLoad td:nth-child(3)').css("max-width", "85px");
                $('.grdLoad td:nth-child(3)').css("text-align", "left !important");

                $('.grdLoad td:nth-child(4)').css("min-width", "375px");
                $('.grdLoad td:nth-child(4)').css("max-width", "375px");
                $('.grdLoad td:nth-child(4)').css("text-align", "left !important");

                $('.grdLoad td:nth-child(5)').css("min-width", "100px");
                $('.grdLoad td:nth-child(5)').css("max-width", "100px");
                $('.grdLoad td:nth-child(5)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(6)').css("min-width", "100px");
                $('.grdLoad td:nth-child(6)').css("max-width", "100px");
                $('.grdLoad td:nth-child(6)').css("text-align", "right !important");


                $('.grdLoad td:nth-child(7)').css("min-width", "100px");
                $('.grdLoad td:nth-child(7)').css("max-width", "100px");
                $('.grdLoad td:nth-child(7)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(8)').css("min-width", "100px");
                $('.grdLoad td:nth-child(8)').css("max-width", "100px");
                $('.grdLoad td:nth-child(8)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(9)').css("min-width", "115px");
                $('.grdLoad td:nth-child(9)').css("max-width", "115px");
                $('.grdLoad td:nth-child(9)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(10)').css("min-width", "130px");
                $('.grdLoad td:nth-child(10)').css("max-width", "130px");
                $('.grdLoad td:nth-child(10)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(11)').css("min-width", "80px");
                $('.grdLoad td:nth-child(11)').css("max-width", "80px");
                $('.grdLoad td:nth-child(11)').css("text-align", "right !important");


                $('.grdLoad th:nth-child(12)').css("display", "none");

                $('.grdLoad th:nth-child(1)').css("min-width", "55px");
                $('.grdLoad th:nth-child(1)').css("max-width", "55px");
                $('.grdLoad th:nth-child(1)').css("text-align", "center !important");

                $('.grdLoad th:nth-child(2)').css("min-width", "70px");
                $('.grdLoad th:nth-child(2)').css("max-width", "70px");
                $('.grdLoad th:nth-child(2)').css("text-align", "center !important");

                $('.grdLoad th:nth-child(3)').css("min-width", "85px");
                $('.grdLoad th:nth-child(3)').css("max-width", "85px");
                $('.grdLoad th:nth-child(3)').css("text-align", "left !important");

                $('.grdLoad th:nth-child(4)').css("min-width", "385px");
                $('.grdLoad th:nth-child(4)').css("max-width", "385px");
                $('.grdLoad th:nth-child(4)').css("text-align", "left !important");

                $('.grdLoad th:nth-child(5)').css("min-width", "100px");
                $('.grdLoad th:nth-child(5)').css("max-width", "100px");
                $('.grdLoad th:nth-child(5)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(6)').css("min-width", "100px");
                $('.grdLoad th:nth-child(6)').css("max-width", "100px");
                $('.grdLoad th:nth-child(6)').css("text-align", "right !important");


                $('.grdLoad th:nth-child(7)').css("min-width", "100px");
                $('.grdLoad th:nth-child(7)').css("max-width", "100px");
                $('.grdLoad th:nth-child(7)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(8)').css("min-width", "100px");
                $('.grdLoad th:nth-child(8)').css("max-width", "100px");
                $('.grdLoad th:nth-child(8)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(9)').css("min-width", "115px");
                $('.grdLoad th:nth-child(9)').css("max-width", "115px");
                $('.grdLoad th:nth-child(9)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(10)').css("min-width", "135px");
                $('.grdLoad th:nth-child(10)').css("max-width", "135px");
                $('.grdLoad th:nth-child(10)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(11)').css("min-width", "80px");
                $('.grdLoad th:nth-child(11)').css("max-width", "80px");
                $('.grdLoad th:nth-child(11)').css("text-align", "right !important");


                $('.grdLoad td:nth-child(12)').css("display", "none");
            }
            else {

                $("[id*=pnlFilter]").show();
                $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
            $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                    'width': '74%'
                });
                $("select").trigger("liszt:updated");
                $('.grdLoad td:nth-child(1)').css("min-width", "55px");
                $('.grdLoad td:nth-child(1)').css("max-width", "55px");
                $('.grdLoad td:nth-child(1)').css("text-align", "center !important");

                $('.grdLoad td:nth-child(2)').css("min-width", "50px");
                $('.grdLoad td:nth-child(2)').css("max-width", "50px");
                $('.grdLoad td:nth-child(2)').css("text-align", "center !important");

                $('.grdLoad td:nth-child(3)').css("min-width", "65px");
                $('.grdLoad td:nth-child(3)').css("max-width", "65px");
                $('.grdLoad td:nth-child(3)').css("text-align", "left !important");

                $('.grdLoad td:nth-child(4)').css("min-width", "250px");
                $('.grdLoad td:nth-child(4)').css("max-width", "250px");
                $('.grdLoad td:nth-child(4)').css("text-align", "left !important");

                $('.grdLoad td:nth-child(5)').css("min-width", "75px");
                $('.grdLoad td:nth-child(5)').css("max-width", "75px");
                $('.grdLoad td:nth-child(5)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(6)').css("min-width", "75px");
                $('.grdLoad td:nth-child(6)').css("max-width", "75px");
                $('.grdLoad td:nth-child(6)').css("text-align", "right !important");


                $('.grdLoad td:nth-child(7)').css("min-width", "75px");
                $('.grdLoad td:nth-child(7)').css("max-width", "75px");
                $('.grdLoad td:nth-child(7)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(8)').css("min-width", "85px");
                $('.grdLoad td:nth-child(8)').css("max-width", "85px");
                $('.grdLoad td:nth-child(8)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(9)').css("min-width", "95px");
                $('.grdLoad td:nth-child(9)').css("max-width", "95px");
                $('.grdLoad td:nth-child(9)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(10)').css("min-width", "75px");
                $('.grdLoad td:nth-child(10)').css("max-width", "75px");
                $('.grdLoad td:nth-child(10)').css("text-align", "right !important");

                $('.grdLoad td:nth-child(11)').css("min-width", "80px");
                $('.grdLoad td:nth-child(11)').css("max-width", "80px");
                $('.grdLoad td:nth-child(11)').css("text-align", "right !important");
                $('.grdLoad td:nth-child(12)').css("display", "none");

                $('.grdLoad th:nth-child(1)').css("min-width", "55px");
                $('.grdLoad th:nth-child(1)').css("max-width", "55px");
                $('.grdLoad th:nth-child(1)').css("text-align", "center !important");

                $('.grdLoad th:nth-child(2)').css("min-width", "50px");
                $('.grdLoad th:nth-child(2)').css("max-width", "50px");
                $('.grdLoad th:nth-child(2)').css("text-align", "center !important");

                $('.grdLoad th:nth-child(3)').css("min-width", "65px");
                $('.grdLoad th:nth-child(3)').css("max-width", "65px");
                $('.grdLoad th:nth-child(3)').css("text-align", "left !important");

                $('.grdLoad th:nth-child(4)').css("min-width", "250px");
                $('.grdLoad th:nth-child(4)').css("max-width", "250px");
                $('.grdLoad th:nth-child(4)').css("text-align", "left !important");

                $('.grdLoad th:nth-child(5)').css("min-width", "75px");
                $('.grdLoad th:nth-child(5)').css("max-width", "75px");
                $('.grdLoad th:nth-child(5)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(6)').css("min-width", "75px");
                $('.grdLoad th:nth-child(6)').css("max-width", "75px");
                $('.grdLoad th:nth-child(6)').css("text-align", "right !important");


                $('.grdLoad th:nth-child(7)').css("min-width", "75px");
                $('.grdLoad th:nth-child(7)').css("max-width", "75px");
                $('.grdLoad th:nth-child(7)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(8)').css("min-width", "85px");
                $('.grdLoad th:nth-child(8)').css("max-width", "85px");
                $('.grdLoad th:nth-child(8)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(9)').css("min-width", "95px");
                $('.grdLoad th:nth-child(9)').css("max-width", "95px");
                $('.grdLoad th:nth-child(9)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(10)').css("min-width", "75px");
                $('.grdLoad th:nth-child(10)').css("max-width", "75px");
                $('.grdLoad th:nth-child(10)').css("text-align", "right !important");

                $('.grdLoad th:nth-child(11)').css("min-width", "80px");
                $('.grdLoad th:nth-child(11)').css("max-width", "80px");
                $('.grdLoad th:nth-child(11)').css("text-align", "right !important");
                $('.grdLoad th:nth-child(12)').css("display", "none");

                $("select").chosen({ width: '100%' });
                $("select").trigger("liszt:updated");
            }

            return false;
        }--%>
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    // $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'height:40%; width:100%');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%");
                    $("#<%=Div1.ClientID%>").width("1300px");
                    
                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("#<%=Div1.ClientID%>").width("1035px");
                    $("select").trigger("liszt:updated");
                }
                return false;
            }
        catch (err) {
            return false;
            alert(err.Message);
        }
    }
    function fncVenItemRowdblClk(rowObj) {
        try {
            rowObj = $(rowObj);
            //alert($.trim($("td", rowObj).eq(2).text()));
            fncOpenItemhistory($.trim($("td", rowObj).eq(2).text()));

        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    /// Open Item History
    function fncOpenItemhistory(itemcode) {
        var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
    page = page + "?InvCode=" + itemcode + "&Status=dailog";
    var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 700,
        width: 1250,
        title: "Inventory History"
    });
    $dialog.dialog('open');
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Always" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                            <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                        <li class="active-page">
                            <%=Resources.LabelCaption.lbl_Stock_Update%></li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                            EnableVendorDropDown="true"
                            EnableDepartmentDropDown="true"
                            EnableCategoryDropDown="true"
                            EnableSubCategoryDropDown="true"
                            EnableBrandDropDown="true"
                            EnableMerchandiseDropDown="true"
                            EnableManufactureDropDown="true"
                            EnableFloorDropDown="true"
                            EnableSectionDropDown="true"
                            EnableBinDropDown="true"
                            EnableShelfDropDown="true"
                            EnableWarehouseDropDown="true"
                            EnableBatchNoTextBox="false"
                            EnableItemCodeTextBox="true"
                            EnableItemNameTextBox="true"
                            EnableFilterButton="true"
                            EnableClearButton="true"
                            OnClearButtonClientClick="clearForm(); return false;"
                            OnFilterButtonClick="lnkLoadFilter_Click" />

                    </div>
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                        style="height: 500px">
                        <div class="control-group-split" style="width: 40%; float: left">
                            <div class="control-groupbarcode">
                                <div style="text-align: center">
                                    <div>
                                        <div style="float: left">
                                            <asp:RadioButton ID="rbnAll" runat="server" Text='<%$ Resources:LabelCaption,lbl_All %>'
                                                GroupName="barcode" Checked="True" />
                                        </div>
                                        <div style="float: left; margin-left: 10%">
                                            <asp:RadioButton ID="rbnBatch" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batch %>'
                                                GroupName="barcode" />
                                        </div>
                                        <div style="float: left; margin-left: 10%">
                                            <asp:RadioButton ID="rbnNonBatch" runat="server" Text='<%$ Resources:LabelCaption,lbl_NonBatch %>'
                                                GroupName="barcode" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split" style="width: 45%; float: right">
                            <div class="control-group-left" style="width: 46%;">
                                <div class="label-right" style="width: 10%">
                                    <asp:CheckBox ID="chkActive" runat="server" Checked="true" AutoPostBack="true" />
                                </div>
                                <div class="label-left" style="width: 55%">
                                    <asp:Label ID="Label32" runat="server" Text='<%$ Resources:LabelCaption,lbl_ActiveItem %>'></asp:Label>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 27%;">
                                <div class="label-right" style="width: 10%">
                                    <fieldset>
                                        <asp:CheckBox ID="chkQtyGreater" runat="server" AutoPostBack="true" />
                                    </fieldset>
                                </div>
                                <div class="label-left" style="width: 40%">
                                    <asp:Label ID="Label31" runat="server" Text='<%$ Resources:LabelCaption,lbl_GreaterZero %>'></asp:Label>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 27%;">
                                <div class="label-right" style="width: 10%">
                                    <fieldset>
                                        <asp:CheckBox ID="chkQtyLess" runat="server" AutoPostBack="true" />
                                    </fieldset>
                                </div>
                                <div class="label-left" style="width: 40%">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_LessZero %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-3 sort_by">
                                <div class="col-md-3">
                                    <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                                </div>
                                <div class="col-md-7">
                                    <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                                </div>
                                <div class="col-md-1">
                                    <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="conditional">
                                    <ContentTemplate>
                                        <div style="float: left">
                                            <div class="control-button">
                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                            </div>
                                        </div>
                                        <div style="float: right">
                                            <div class="control-button">
                                                <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" class="button-blue"
                                                    Text="Import Data" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnImport" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="conditional">
                            <ContentTemplate>
                                <div style="text-align: center">
                                    <div>
                                        <div style="float: left">
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                 
                                <div style="text-align: center">
                                    <div>
                                        <div style="float: left">
                                            <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" class="button-blue"
                                                Text="Import Data" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                         <div  class="grdLoad" style="overflow-x: scroll; overflow-y: hidden; height: 415px; width: 1035px; background-color: aliceblue;"
                                         id="Div1" runat="server">
                                    <div class="row"> 
                                        <div class="grdLoad">
                       <%-- <div class="grdLoad">--%>
                           <%-- <div class="row">--%>
                                <table id="tblStockUpdate" cellspacing="0" rules="all" border="1" class="fixed_headers">
                                    <thead>
                                        <tr class="click">
                                            <th scope="col">Select</th>
                                            <th scope="col">S.No</th>
                                            <th scope="col">ItemCode</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">MRP</th>
                                            <th scope="col">NetCost</th>
                                            <th scope="col">S.Price</th>
                                            <th scope="col">TotalCost</th>
                                            <th scope="col">BatchNo</th>
                                            <th scope="col">OtyonHand</th>
                                            <th scope="col">New Qty</th>
                                            <th scope="col">PRReqQty</th>
                                            <th scope="col">Last Update Date</th>
                                            <th scope="col">Short/Excess Qty</th>
                                            <th scope="col">Short/Excess Amt</th>
                                            <th scope="col">User</th>
                                            <th scope="col">Terminal</th>
                                            <th scope="col">Last Update</th>
                                            <th scope="col" style="display:none">UOM</th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                                    style="height: 375px;width:1585px">
                                    <asp:GridView ID="gvStockUpdate" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                        CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector" OnRowDataBound="gvStockUpdate_DataBound">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="RowNo" HeaderText="SNo"></asp:BoundField>
                                            <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                            <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                            <asp:BoundField DataField="cost" HeaderText="Net Cost"></asp:BoundField>
                                            <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                            <asp:BoundField DataField="TotalCost" HeaderText="Total cost" />
                                            <asp:BoundField DataField="BatchNo" HeaderText="Batch No" />
                                            <asp:BoundField DataField="BatchQty" HeaderText="Qty On Hand" />
                                            <asp:TemplateField HeaderText="New Qty" ItemStyle-Width="65">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtNewQty" runat="server" Text='<%# Eval("NewQty") %>'
                                                        onkeypress="return isNumberKey(event,this)" CssClass="grid-textbox"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PRReqQty" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"
                                                ItemStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtBatchQty" runat="server" onMouseDown="return false" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol" CssClass="grid-textbox" Style="background-color: White"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:BoundField DataField="LastDate" HeaderText="Last Update Date" />
                                            <asp:BoundField DataField="AdjustmentQty" HeaderText="Short/Excess Qty" />
                                            <asp:BoundField DataField="Amount" HeaderText="Short/Excess Amt"/>
                                             <asp:BoundField DataField="UpdatedUser" HeaderText="User" />
                                            <asp:BoundField DataField="TERMINALCODE" HeaderText="Terminal" />
                                            <asp:BoundField DataField="ScannedQty" HeaderText="Last Update"/>
                                            <asp:BoundField DataField="UOM" HeaderText="UOM" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                         </div>

                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="float: right; display: table; ">
        <div class="control-button">
            <%--<asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>--%>
            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="InkExport" runat="server" class="button-blue" OnClick="btnExportToExcel_Click" OnClientClick="return fncValidateExportNoItems();"
                Text='<%$ Resources:LabelCaption,btn_Export %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="InkAllZero" runat="server" class="button-blue" OnClientClick="fncValidateAllZero();return false;"
                Text='<%$ Resources:LabelCaption,btn_ApplyZero %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click"
                Text="Print"></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick="fncValidateSave();return false;"
                Text='<%$ Resources:LabelCaption,btn_Update %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
        </div>
        <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdate">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'>
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
