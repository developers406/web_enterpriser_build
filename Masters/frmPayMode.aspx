﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPayMode.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmPayMode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
     </style>
    <script type="text/javascript">


        function ValidateForm() {
            var Show = '';
            var Position = $.trim($('#<%=txtPosition.ClientID%>').val());    // 23112022 musaraf
            var PayMode = $.trim($('#<%=txtPayMode.ClientID%>').val());
            var Description = $.trim($('#<%=txtDescription.ClientID%>').val());

            if (Position == "") {
                Show = Show + 'Please Enter Position';
                document.getElementById("<%=txtPosition.ClientID %>").focus();
            }
            if (PayMode == "") {
                Show = Show + '<br />Please Enter Pay Mode';
                document.getElementById("<%=txtPayMode.ClientID %>").focus();
            }
            if (Description == "") {
                Show = Show + '<br />Please Enter Description';
                document.getElementById("<%=txtDescription.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function PayMode_Delete(source) {

            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(4).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }
        function imgbtnEdit_ClientClick(source) {

            DisplayDetails($(source).closest("tr"));

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function DisplayDetails(row) {



            $('#<%=txtPayMode.ClientID %>').prop("disabled", true);
            $('#<%=txtPosition.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtPayMode.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtDescription.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
            $('#<%=txtCommission.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            $('#<%=txtServiceCharge.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
            $('#<%=txtGLAccountCode.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
            $('#<%=txtDiscount.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));
            if ($("td", row).eq(9).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkActive.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= chkActive.ClientID %>').removeAttr("checked");
            }
            if ($("td", row).eq(10).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkDenamination.ClientID %>').attr("checked", "checked");
                $('#<%=lnkDemination.ClientID %>').show();
            }
            else {
                $('#<%= chkDenamination.ClientID %>').removeAttr("checked");
                $('#<%=lnkDemination.ClientID %>').hide();
            }




        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            <%-- $('#<%=chkDenamination.ClientID %>').change(function () {
                if (this.checked) {
                    $('#<%=lnkDemination.ClientID %>').show();
                }
                else {
                    $('#<%=lnkDemination.ClientID %>').hide();
                }
            });--%>
            

            $('#<%=lnkDemination.ClientID %>').click(function () {
                var sPayMode = $("#<%=txtPayMode.ClientID%>").val();
                debugger;
                if (sPayMode == "CREDITCARD") {
                    $("#<%=btncreditcard.ClientID%>").click();



                }
                else {
                

                        $("#<%=btnother.ClientID%>").click();
                   
                }
                $('#<%=txtPayMode.ClientID %>').prop("disabled", true);
                $('#<%=lnkDemination.ClientID %>').css("display", "block");
                return false;


            });
            $('#<%=txtPosition.ClientID %>').focus();
            $("select").chosen({ width: '100%' });
            $('#<%=txtPayMode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmPayMode.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtPayMode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtPayMode.ClientID %>').prop("disabled", true);
                            $('#<%=txtPosition.ClientID %>').val(strarray[0]);
                            $('#<%=txtPayMode.ClientID %>').val(strarray[1]);
                            $('#<%=txtDescription.ClientID %>').val(strarray[2]);
                            $('#<%=txtCommission.ClientID %>').val(strarray[3]);
                            $('#<%=txtServiceCharge.ClientID %>').val(strarray[4]);
                            $('#<%=txtGLAccountCode.ClientID %>').val(strarray[5]);
                            $('#<%=txtDiscount.ClientID %>').val(strarray[8]);

                            if ($.trim(strarray[6]) == "1") {
                                $('#<%= chkActive.ClientID %>').attr("checked", "checked");

                            }
                            else {
                                $('#<%= chkActive.ClientID %>').removeAttr("checked");

                            }
                            if ($.trim(strarray[7]) == "1") {
                                $('#<%= chkDenamination.ClientID %>').attr("checked", "checked");
                                $('#<%=lnkDemination.ClientID %>').show();
                            }
                            else {
                                $('#<%= chkDenamination.ClientID %>').removeAttr("checked");
                                $('#<%=lnkDemination.ClientID %>').hide();
                            }








                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });


            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmPayMode.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[2],
                                    val: item.split('-')[0]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                <%--focus: function (event, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmPayMode.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },

                    minLength: 1
                });

                }
                function clearForm() {
                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    $(':checkbox, :radio').prop('checked', false);
                }
          function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            $('#<%= lnkSave.ClientID %>').click();
                             e.preventDefault();	
                         }
                         else if (e.keyCode == 117) {
 $("input").removeAttr('disabled');
                             $('#<%= lnkClear.ClientID %>').click();
                             e.preventDefault();
                         }
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>
        
       
}
};

$(document).ready(function () {
    $(document).on('keydown', disableFunctionKeys);
});
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PayMode');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PayMode";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow:hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Pay Mode</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="EnableScroll">
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <div class="container-group-small col-md-4">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Position"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPosition" runat="server" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNew(event);" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Pay Mode"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPayMode" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Commission"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCommission" runat="server" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNew(event);" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Service Charge"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtServiceCharge" runat="server" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNew(event);" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="GL Account Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtGLAccountCode" runat="server" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Discount"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDiscount" runat="server" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNew(event);" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Active"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="Denamination"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkDenamination" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div style="float: right;">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkDemination" runat="server" style="display:none" class="button-red">Demination</asp:LinkButton>
                        </div>

                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()" ><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                ><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display:none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="col-md-8" style="padding-left: 20px">
                    <div id="divCreditcard" runat="server" style="display: none">
                        <asp:GridView ID="grdCreditcard" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" OnRowDeleting="OnRowDeleting"
                            CssClass="pshro_GridDgn" oncopy="return false">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <Columns>
                               
                                <asp:BoundField DataField="PayMode" HeaderText="PayMode"></asp:BoundField>
                                <asp:BoundField DataField="Denamination" HeaderText="Type"></asp:BoundField>
                                <asp:BoundField DataField="Commission" HeaderText="Bank Commission"></asp:BoundField>
                                <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge To Customer%"></asp:BoundField>
                                <asp:BoundField DataField="AccountCode" HeaderText="Account Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                <asp:BoundField DataField="CommisionAccountCode" HeaderText="Commision Account Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:CommandField ShowDeleteButton="True" ButtonType="Button" />
                            </Columns>
                        </asp:GridView>
                        <div style="padding-top: 10px;">
                            <div class="col-md-3">
                                PayMode
                        <asp:TextBox ID="txtPaymode_pop_Creditcard" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>

                            <div class="col-md-3">
                                Type
                        <asp:TextBox ID="txttype_pop_Creditcard" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                Bank Commision %
                        <asp:TextBox ID="txtBankCommision_pop_Creditcard"  onkeydown="return isNumberKeyWithDecimalNew(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                Service Charge To Customer %
                        <asp:TextBox ID="txtServiceCharge_pop_Creditcard"  onkeydown="return isNumberKeyWithDecimalNew(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>


                        </div>
                        <div style="padding-top: 55px;">
                            <div class="col-md-8">
                            </div>

                            <div class="col-md-2">

                                <asp:LinkButton ID="btnadd_Creditcard" OnClick="btnadd_Creditcard_Click" runat="server" class="button-red" Width="100px"><i class="icon-play"></i>Add</asp:LinkButton>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="btnsave_Creditcard" OnClick="btnsave_Creditcard_Click" runat="server" class="button-red" Width="100px"><i class="icon-play"></i>Save</asp:LinkButton>
                            </div>
                           
                        </div>

                    </div>
                    <div id="divother" runat="server" style="display: none">
                        <asp:GridView ID="grdother"  runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" OnRowDeleting="OnRowDeleting_Other"
                            CssClass="pshro_GridDgn" oncopy="return false">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <Columns>
                                <asp:BoundField DataField="VoucherGroup" HeaderText="Voucher Group"></asp:BoundField>
                                <asp:BoundField DataField="Denamination" HeaderText="Denamination"></asp:BoundField>
                                 <asp:BoundField DataField="DValue" HeaderText="DValue"></asp:BoundField>
                                <asp:BoundField DataField="Commission" HeaderText="Commission"></asp:BoundField>
                                <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge"></asp:BoundField>
                                <asp:BoundField DataField="AccountCode" HeaderText="Account Code"></asp:BoundField>
                                <asp:BoundField DataField="CommisionAccountCode" HeaderText="Commision AccountCode"></asp:BoundField>
                              <asp:CommandField ShowDeleteButton="True"  ButtonType="Button" />
                            </Columns>
                        </asp:GridView>
                        <div style="padding-top: 10px;">
                                <div class="col-md-2">
                            Voucher Group
                         <asp:DropDownList ID="ddlVoucherGroup_pop_other" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                        </div>

                        <div class="col-md-2">
                            Denamination
                        <asp:TextBox ID="txtDenamination_pop_other" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            Value
                        <asp:TextBox ID="txtValue_pop_other" onkeydown="return isNumberKeyWithDecimalNew(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            Commission
                        <asp:TextBox ID="txtCommission_pop_other" onkeydown="return isNumberKeyWithDecimalNew(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            Service Charge
                        <asp:TextBox ID="txtServiceCharge_pop_other" onkeydown="return isNumberKeyWithDecimalNew(event);" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            Gl AC Denomination 
                        <asp:DropDownList ID="ddlGlACDenomination_pop_other" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                        </div>
                       
                        </div>
                    
                        <div style="padding-top: 55px;">
                            <div class="col-md-6">
                            </div>

                            <div class="col-md-2">

                                <asp:LinkButton ID="btnadd_Other" OnClick="btnadd_Other_Click" runat="server" class="button-red" Width="100px"><i class="icon-play"></i>Add</asp:LinkButton>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="btnsave_Other" OnClick="btnsave_Other_Click" runat="server" class="button-red" Width="100px"><i class="icon-play"></i>Save</asp:LinkButton>
                            </div>
                           
                        </div>

                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                             <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    
                    <asp:GridView ID="gvPayMode" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                        CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="PayMode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick="return PayMode_Delete(this);  return false;"
                                        CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                        ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                            <asp:BoundField DataField="Position" HeaderText="Position"></asp:BoundField>
                            <asp:BoundField DataField="PayMode" HeaderText="Pay Mode"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                        
                        
                            <asp:BoundField DataField="ServiceCharge" HeaderText="Service Charge"></asp:BoundField>
                          
                            <asp:BoundField DataField="Active" HeaderText="Active"></asp:BoundField>
                            <asp:BoundField DataField="Denamination" HeaderText="Denamination"></asp:BoundField>
                      
                            <asp:BoundField DataField="ModifyUser" HeaderText="ModifyUser"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate"></asp:BoundField>
                        </Columns>
                    </asp:GridView>

                </div>
                <div class="hiddencol">
                    <asp:Button ID="btncreditcard" runat="server" OnClick="btncreditcard_Click" />
                     <asp:Button ID="btnother" runat="server" OnClick="btnother_Click" />
                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
                </div>


            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
