﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmStyle.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmStyle" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 127px;
            max-width: 127px;  text-align: center !important;
          
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 82px;
            max-width: 82px;  text-align: center !important;
            
        }
        
        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 92px;
            max-width: 92px;  text-align: center !important;
           
        }
        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 211px;
            max-width: 211px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 330px;
            max-width: 330px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            display: none;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            display: none;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            display: none;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            display: none;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            display: none;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display: none;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            display: none;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            display: none;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            display: none;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            display: none;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            display: none;
        }

        .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            display: none;
        }

        .grdLoad td:nth-child(18), .grdLoad th:nth-child(18) {
            display: none;
        }

        .grdLoad td:nth-child(19), .grdLoad th:nth-child(19) {
            display: none;
        }

        .grdLoad td:nth-child(20), .grdLoad th:nth-child(20) {
            display: none;
        }

        .grdLoad td:nth-child(21), .grdLoad th:nth-child(21) {
            display: none;
        }

        .grdLoad td:nth-child(22), .grdLoad th:nth-child(22) {
            display: none;
        }

        .grdLoad td:nth-child(23), .grdLoad th:nth-child(23) {
            display: none;
        }

        .grdLoad td:nth-child(24), .grdLoad th:nth-child(24) {
            display: none;
        }

        .grdLoad td:nth-child(25), .grdLoad th:nth-child(25) {
            display: none;
        }

        .grdLoad td:nth-child(26), .grdLoad th:nth-child(26) {
            display: none;
        }

        .grdLoad td:nth-child(27), .grdLoad th:nth-child(27) {
            display: none;
        }

        .grdLoad td:nth-child(28), .grdLoad th:nth-child(28) {
            display: none;
        }

        .grdLoad td:nth-child(29), .grdLoad th:nth-child(29) {
            display: none;
        }

        .grdLoad td:nth-child(30), .grdLoad th:nth-child(30) {
            display: none;
        }

        .grdLoad td:nth-child(31), .grdLoad th:nth-child(31) {
            display: none;
        }

        .grdLoad td:nth-child(32), .grdLoad th:nth-child(32) {
            display: none;
        }

        .grdLoad td:nth-child(33), .grdLoad th:nth-child(33) {
            display: none;
        }

        .grdLoad td:nth-child(34), .grdLoad th:nth-child(34) {
            display: none;
        }

        .grdLoad td:nth-child(35), .grdLoad th:nth-child(35) {
            display: none;
        }

        .grdLoad td:nth-child(36), .grdLoad th:nth-child(36) {
            display: none;
        }

        .grdLoad td:nth-child(37), .grdLoad th:nth-child(37) {
            display: none;
        }

        .grdLoad td:nth-child(38), .grdLoad th:nth-child(38) {
            display: none;
        }

        .grdLoad td:nth-child(39), .grdLoad th:nth-child(39) {
            display: none;
        }

        .grdLoad td:nth-child(40), .grdLoad th:nth-child(40) {
            display: none;
        }

        .grdLoad td:nth-child(41), .grdLoad th:nth-child(41) {
            display: none;
        }

        .grdLoad td:nth-child(42), .grdLoad th:nth-child(42) {
            display: none;
        }

        .grdLoad td:nth-child(43), .grdLoad th:nth-child(43) {
            display: none;
        }

        .grdLoad td:nth-child(44), .grdLoad th:nth-child(44) {
            display: none;
        }

        .grdLoad td:nth-child(45), .grdLoad th:nth-child(45) {
            display: none;
        }

        .grdLoad td:nth-child(46), .grdLoad th:nth-child(46) {
            display: none;
        }

        .grdLoad td:nth-child(47), .grdLoad th:nth-child(47) {
            min-width: 222px;
            max-width: 222px;
        }

        .grdLoad td:nth-child(48), .grdLoad th:nth-child(48) {
            min-width: 222px;
            max-width: 222px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
             .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        /*.grdLoad td:nth-child(48), .grdLoad th:nth-child(48) {
           display:none;
        }*/
    </style>

    <script type="text/javascript">

        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupStyle').dialog('close');
                window.parent.$('#popupStyle').remove();

            }

        });
        function ValidateForm() {
            if ($('#<%=txtStyleCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Style");
                return false;
            }
            if (!($('#<%=txtStyleCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Style");
                return false;
            }
            var Show = '';
            var StyleCode = $.trim($('#<%=txtStyleCode.ClientID %>').val());            //22112022 musaraf
            var Description = $.trim($('#<%=txtDescription.ClientID %>').val());

            if (StyleCode == "") {
                Show = Show + 'Please Enter StyleCode';
                document.getElementById("<%=txtStyleCode.ClientID %>").focus();
            }

            if (Description == "") {
                Show = Show + '<br />Please Enter Description';
                document.getElementById("<%=txtDescription.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function Style_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Style");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                        $("#<%=btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }
        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Style");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


        function DisplayDetails(row) { 
            clearForm();
            $('#<%=txtStyleCode.ClientID %>').prop("disabled", true);
            $('#<%=txtStyleCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtDescription.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlDepartment.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlCategory.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlBrand.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlBrand.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlOrigin.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlOrigin.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlWareHouse.ClientID %>').val($("td", row).eq(10).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlWareHouse.ClientID %>').trigger("liszt:updated");
            $('#<%=ddluompurchase.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));
            $('#<%=ddluompurchase.ClientID %>').trigger("liszt:updated");
            $('#<%=txtPackage.ClientID %>').val($("td", row).eq(12).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlManufauture.ClientID %>').val($("td", row).eq(13).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlManufauture.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlItemType.ClientID %>').val($("td", row).eq(14).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlItemType.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlVendor.ClientID %>').val($("td", row).eq(15).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlVendor.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlMerchandise.ClientID %>').val($("td", row).eq(16).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlMerchandise.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlFloor.ClientID %>').val($("td", row).eq(17).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlSection.ClientID %>').val($("td", row).eq(18).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlSection.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlBin.ClientID %>').val($("td", row).eq(19).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlBin.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlShelf.ClientID %>').val($("td", row).eq(20).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlSales.ClientID %>').val($("td", row).eq(43).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlSales.ClientID %>').trigger("liszt:updated");                                         // musaraf 18112022
            if ($("td", row).eq(22).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkexpiredate.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkexpiredate.ClientID %>').removeAttr("checked");
            }
            if ($("td", row).eq(23).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkPackage.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkPackage.ClientID %>').removeAttr("checked");
            }
            if ($("td", row).eq(24).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkautopoallowed.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkautopoallowed.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(25).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkserialnorequired.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkserialnorequired.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(26).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkSpecial.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkSpecial.ClientID %>').removeAttr("checked");
            }
            if ($("td", row).eq(27).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkmemberdiscountallowed.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkmemberdiscountallowed.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(28).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkSeasoned1.ClientID %>').attr("checked", "checked");
         }
         else {

             $('#<%= chkSeasoned1.ClientID %>').removeAttr("checked");
         }
         if ($("td", row).eq(29).html().replace(/&nbsp;/g, '') == "1") {
             $('#<%= chkBatch.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkBatch.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(30).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkWarehouseStorage.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkWarehouseStorage.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(31).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkloyalty.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkloyalty.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(32).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkMakingItem.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkMakingItem.ClientID %>').removeAttr("checked");
        }

        if ($("td", row).eq(33).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkPurchaseOrder.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkPurchaseOrder.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(34).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkgrnpurchase.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkgrnpurchase.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(35).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkPurchaseReturn.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkPurchaseReturn.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(36).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkpointofsales.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkpointofsales.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(37).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= ChksalesReturn.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= ChksalesReturn.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(38).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkorderbooking.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkorderbooking.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(39).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkEstimate.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkEstimate.ClientID %>').removeAttr("checked");
        }
        if ($("td", row).eq(40).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkQuotation.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkQuotation.ClientID %>').removeAttr("checked");
        }
        $('#<%=ddlVat.ClientID %>').val($("td", row).eq(41).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlVat.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlccode.ClientID %>').val($("td", row).eq(42).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlccode.ClientID %>').trigger("liszt:updated");
            if ($("td", row).eq(44).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkhidedateonbarcode.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkhidedateonbarcode.ClientID %>').removeAttr("checked");
            }
            $('#<%=ddlstocktype.ClientID %>').val($("td", row).eq(45).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlstocktype.ClientID %>').trigger("liszt:updated");


            if (
                $("td", row).eq(33).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(34).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(35).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(36).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(37).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(38).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(39).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(40).html().replace(/&nbsp;/g, '') == "1"
                ) {
                $('#<%= ChkSelectAll1.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= ChkSelectAll1.ClientID %>').removeAttr("checked");
            }

            if ($("td", row).eq(22).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(23).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(24).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(25).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(26).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(27).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(28).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(29).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(30).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(31).html().replace(/&nbsp;/g, '') == "1" &&
                $("td", row).eq(32).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= ChkSelectAll.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= ChkSelectAll.ClientID %>').removeAttr("checked");
            }


        }


    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
            $('#<%=txtStyleCode.ClientID %>').focus();
            $('#<%=txtStyleCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmStyle.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtStyleCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            clearForm();
                            $('#<%=txtStyleCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtStyleCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtDescription.ClientID %>').val(strarray[1]);

                            $('#<%=ddlDepartment.ClientID %>').val($.trim(strarray[3]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlCategory.ClientID %>').val($.trim(strarray[4]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlBrand.ClientID %>').val($.trim(strarray[5]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlBrand.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlOrigin.ClientID %>').val($.trim(strarray[6]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlOrigin.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlWareHouse.ClientID %>').val($.trim(strarray[7]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlWareHouse.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddluompurchase.ClientID %>').val($.trim(strarray[8]).replace(/&nbsp;/g, ''));
                            $('#<%=ddluompurchase.ClientID %>').trigger("liszt:updated");
                            $('#<%=txtPackage.ClientID %>').val(strarray[9]);
                            $('#<%=ddlManufauture.ClientID %>').val($.trim(strarray[10]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlManufauture.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlItemType.ClientID %>').val($.trim(strarray[11]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlItemType.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlVendor.ClientID %>').val($.trim(strarray[12]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlVendor.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlMerchandise.ClientID %>').val($.trim(strarray[13]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlMerchandise.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlFloor.ClientID %>').val($.trim(strarray[14]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlSection.ClientID %>').val($.trim(strarray[15]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlSection.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlBin.ClientID %>').val($.trim(strarray[16]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlBin.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlShelf.ClientID %>').val($.trim(strarray[17]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
                            if ($.trim(strarray[19]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkexpiredate.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkexpiredate.ClientID %>').removeAttr("checked");
                            }
                            if ($.trim(strarray[20]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkPackage.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkPackage.ClientID %>').removeAttr("checked");
                            }
                            if ($.trim(strarray[21]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkautopoallowed.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkautopoallowed.ClientID %>').removeAttr("checked");
                            }
                            if ($.trim(strarray[22]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkserialnorequired.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkserialnorequired.ClientID %>').removeAttr("checked");
                            }
                            if ($.trim(strarray[23]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkSpecial.ClientID %>').attr("checked", "checked");
                             }
                             else {

                                 $('#<%= chkSpecial.ClientID %>').removeAttr("checked");
                             }
                             if ($.trim(strarray[24]).replace(/&nbsp;/g, '') == "1") {
                                 $('#<%= chkmemberdiscountallowed.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkmemberdiscountallowed.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[25]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkSeasoned1.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkSeasoned1.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[26]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkBatch.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkBatch.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[27]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkWarehouseStorage.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkWarehouseStorage.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[28]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkloyalty.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkloyalty.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[29]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkMakingItem.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkMakingItem.ClientID %>').removeAttr("checked");
                                }

                                if ($.trim(strarray[30]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkPurchaseOrder.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkPurchaseOrder.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[31]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkgrnpurchase.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkgrnpurchase.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[32]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkPurchaseReturn.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkPurchaseReturn.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[33]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkpointofsales.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkpointofsales.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[34]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= ChksalesReturn.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= ChksalesReturn.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[35]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkorderbooking.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkorderbooking.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[36]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkEstimate.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkEstimate.ClientID %>').removeAttr("checked");
                                }
                                if ($.trim(strarray[37]).replace(/&nbsp;/g, '') == "1") {
                                    $('#<%= chkQuotation.ClientID %>').attr("checked", "checked");
                                }
                                else {

                                    $('#<%= chkQuotation.ClientID %>').removeAttr("checked");
                                }
                                $('#<%=ddlVat.ClientID %>').val($.trim(strarray[38]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlVat.ClientID %>').trigger("liszt:updated");
                            $('#<%=ddlccode.ClientID %>').val($.trim(strarray[39]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlccode.ClientID %>').trigger("liszt:updated");
                            if ($.trim(strarray[41]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkhidedateonbarcode.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkhidedateonbarcode.ClientID %>').removeAttr("checked");
                            }
                            $('#<%=ddlstocktype.ClientID %>').val($.trim(strarray[46]).replace(/&nbsp;/g, ''));
                            $('#<%=ddlstocktype.ClientID %>').trigger("liszt:updated");


                            if (
                                $.trim(strarray[32]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[33]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[34]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[35]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[36]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[37]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[38]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[39]).replace(/&nbsp;/g, '') == "1"
                                ) {
                                $('#<%= ChkSelectAll1.ClientID %>').attr("checked", "checked");
                            }
                            else {
                                $('#<%= ChkSelectAll1.ClientID %>').removeAttr("checked");
                            }

                            if ($.trim(strarray[21]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[22]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[23]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[24]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[25]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[26]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[27]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[28]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[29]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[30]).replace(/&nbsp;/g, '') == "1" &&
                                $.trim(strarray[31]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= ChkSelectAll.ClientID %>').attr("checked", "checked");
                            }
                            else {
                                $('#<%= ChkSelectAll.ClientID %>').removeAttr("checked");
                            }

                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmStyle.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });
                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmStyle.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                         event.preventDefault();
                     },
                     select: function (e, i) {
                         $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                            return false;
                        },

                        minLength: 1
                 });
                        $(document).ready(function () {
                            $('#<%= ChkSelectAll1.ClientID %>').change(function () {
                                if (this.checked) {
                                    $('#<%= chkorderbooking.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkgrnpurchase.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkEstimate.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkPurchaseOrder.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkQuotation.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkPurchaseReturn.ClientID %>').attr("checked", "checked");
                                    $('#<%= ChksalesReturn.ClientID %>').attr("checked", "checked");
                                    $('#<%= chkpointofsales.ClientID %>').attr("checked", "checked");


                                }
                                else {
                                    $('#<%= chkorderbooking.ClientID %>').removeAttr("checked");
                                    $('#<%= chkgrnpurchase.ClientID %>').removeAttr("checked");
                                    $('#<%= chkEstimate.ClientID %>').removeAttr("checked");
                                    $('#<%= chkPurchaseOrder.ClientID %>').removeAttr("checked");
                                    $('#<%= chkQuotation.ClientID %>').removeAttr("checked");
                                    $('#<%= chkPurchaseReturn.ClientID %>').removeAttr("checked");
                                    $('#<%= ChksalesReturn.ClientID %>').removeAttr("checked");
                                    $('#<%= chkpointofsales.ClientID %>').removeAttr("checked");


                                }
                            });
                        });

                        $(document).ready(function () {
                            $('#<%= ChkSelectAll.ClientID %>').change(function () {
                            if (this.checked) {
                                $('#<%= chkexpiredate.ClientID %>').attr("checked", "checked");
                                $('#<%= chkhidedateonbarcode.ClientID %>').attr("checked", "checked");
                                $('#<%= chkserialnorequired.ClientID %>').attr("checked", "checked");
                                $('#<%= chkautopoallowed.ClientID %>').attr("checked", "checked");
                                $('#<%= chkBatch.ClientID %>').attr("checked", "checked");
                                $('#<%= chkPackage.ClientID %>').attr("checked", "checked");
                                $('#<%= chkWarehouseStorage.ClientID %>').attr("checked", "checked");
                                $('#<%= chkloyalty.ClientID %>').attr("checked", "checked");
                                $('#<%= chkSpecial.ClientID %>').attr("checked", "checked");
                                $('#<%= chkmemberdiscountallowed.ClientID %>').attr("checked", "checked");
                                $('#<%= chkSeasoned1.ClientID %>').attr("checked", "checked");
                                $('#<%= chkMakingItem.ClientID %>').attr("checked", "checked");

                            }
                            else {
                                $('#<%= chkexpiredate.ClientID %>').removeAttr("checked");
                                $('#<%= chkhidedateonbarcode.ClientID %>').removeAttr("checked");
                                $('#<%= chkserialnorequired.ClientID %>').removeAttr("checked");
                                $('#<%= chkautopoallowed.ClientID %>').removeAttr("checked");
                                $('#<%= chkBatch.ClientID %>').removeAttr("checked");
                                $('#<%= chkPackage.ClientID %>').removeAttr("checked");
                                $('#<%= chkWarehouseStorage.ClientID %>').removeAttr("checked");
                                $('#<%= chkloyalty.ClientID %>').removeAttr("checked");
                                $('#<%= chkSpecial.ClientID %>').removeAttr("checked");
                                $('#<%= chkmemberdiscountallowed.ClientID %>').removeAttr("checked");
                                $('#<%= chkSeasoned1.ClientID %>').removeAttr("checked");
                                $('#<%= chkMakingItem.ClientID %>').removeAttr("checked");

                            }
                        });
                    });

                    //$('input[type=text]').bind('change', function () {
                    //    if (this.value.match(SpecialChar)) {

                    //        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    //        this.value = this.value.replace(SpecialChar, '');
                    //    }

                    //});
                }
                function clearForm() {
                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    $(':checkbox, :radio').prop('checked', false);
                    $('#<%=ddlDepartment.ClientID %>').val("");
                    $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlCategory.ClientID %>').val("");
                    $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlBrand.ClientID %>').val("");
                    $('#<%=ddlBrand.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlVendor.ClientID %>').val("");
                    $('#<%=ddlVendor.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlItemType.ClientID %>').val("");
                    $('#<%=ddlItemType.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlWareHouse.ClientID %>').val("");
                    $('#<%=ddlWareHouse.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlManufauture.ClientID %>').val("");
                    $('#<%=ddlManufauture.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlMerchandise.ClientID %>').val("");
                    $('#<%=ddlMerchandise.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlOrigin.ClientID %>').val("");
                    $('#<%=ddlOrigin.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlccode.ClientID %>').val("");
                    $('#<%=ddlccode.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlVat.ClientID %>').val("");
                    $('#<%=ddlVat.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlFloor.ClientID %>').val("");
                    $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlSection.ClientID %>').val("");
                    $('#<%=ddlSection.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlBin.ClientID %>').val("");
                    $('#<%=ddlBin.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlShelf.ClientID %>').val("");
                    $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddluompurchase.ClientID %>').val("");
                    $('#<%=ddluompurchase.ClientID %>').trigger("liszt:updated");
                }

                function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                 $('#<%= lnkSave.ClientID %>').click();
                             e.preventDefault();
                         }
                         else if (e.keyCode == 117) {
                             $("input").removeAttr('disabled');
                             $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                            <%--    else if (e.keyCode == 119) {
                                    $('#<%= lnkClose.ClientID %>').click();
                                 e.preventDefault();
                             }--%>


                     }
                 };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Style');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Style";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlTax" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
            <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
            </div>
            <asp:HiddenField ID="hdnValue" Value="" runat="server" />
            <div class="main-container" style="overflow: hidden">
                <div id="breadcrumbs" class="breadcrumbs" runat="Server">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Style</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="container-group-full">
                            <div class="top-container" style="border: 1px solid #d8d9d8">
                                <div class="top-container-detail">
                                    <div class="left-top-container">
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label2" runat="server" Text="Style Code"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtStyleCode" MaxLength="10" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label1" runat="server" Text="Department"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label3" runat="server" Text="Category"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label8" runat="server" Text="Brand"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label9" runat="server" Text="Vendor"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label10" runat="server" Text="Item Type"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label11" runat="server" Text="WareHouse"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label13" runat="server" Text="Manufauture"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlManufauture" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label14" runat="server" Text="Merchandise"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label5" runat="server" Text="Origin"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlOrigin" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label6" runat="server" Text="C.Code"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlccode" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-top-container">
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label16" runat="server" Text="Tax"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlVat" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label17" runat="server" Text="Package"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtPackage" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label36" runat="server" Text="Floor"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label37" runat="server" Text="Section"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label20" runat="server" Text="Bin"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label21" runat="server" Text="Shelf"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label7" runat="server" Text="UOM Purchase"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddluompurchase" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="control-group-right">
                                                <div class="label-left">
                                                    <asp:Label ID="Label12" runat="server" Text="Sales"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlSales" runat="server" CssClass="form-control-res">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left">
                                                    <asp:Label ID="Label15" runat="server" Text="Stock Types"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlstocktype" runat="server" CssClass="form-control-res">
                                                        <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                                        <asp:ListItem Value="Stock">Stock</asp:ListItem>
                                                        <asp:ListItem Value="NonStock">NonStock</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="top-container" style="border: 1px solid #d8d9d8">
                                <div class="top-container-detail">
                                    <div class="top-container-detail">
                                        <div class="left-top-container">
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label24" AssociatedControlID="chkexpiredate" runat="server" Text="Expire Date"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkexpiredate" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label25" AssociatedControlID="chkhidedateonbarcode" runat="server" Text="Hide Date On Barcode"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkhidedateonbarcode" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label26" AssociatedControlID="chkserialnorequired" runat="server" Text="Serial No Required"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkserialnorequired" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label27" AssociatedControlID="chkMakingItem" runat="server" Text="Making Item"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkMakingItem" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label28" AssociatedControlID="chkautopoallowed" runat="server" Text="Auto Po Allowed"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkautopoallowed" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label29" AssociatedControlID="chkBatch" runat="server" Text="Batch"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkBatch" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label30" AssociatedControlID="chkPackage" runat="server" Text="Package"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkPackage" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label31" AssociatedControlID="chkWarehouseStorage" runat="server" Text="Warehouse Storage"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkWarehouseStorage" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label32" AssociatedControlID="chkloyalty" runat="server" Text="Loyalty"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkloyalty" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label33" AssociatedControlID="chkSpecial" runat="server" Text="Special"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkSpecial" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label18" AssociatedControlID="chkmemberdiscountallowed" runat="server" Text="Member discount allowed"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkmemberdiscountallowed" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label19" AssociatedControlID="chkSeasoned1" runat="server" Text="Seasoned Item"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkSeasoned1" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label23" AssociatedControlID="ChkSelectAll" Font-Bold="true" ForeColor="blue" runat="server" Text="Select All"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="ChkSelectAll" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="right-top-container">
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label22" AssociatedControlID="chkorderbooking" runat="server" Text="Order Booking"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkorderbooking" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label38" AssociatedControlID="chkgrnpurchase" runat="server" Text="GRN (Purchase)"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkgrnpurchase" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label40" AssociatedControlID="chkEstimate" runat="server" Text="Estimate"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkEstimate" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label34" AssociatedControlID="chkPurchaseOrder" runat="server" Text="Purchase Order"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkPurchaseOrder" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label41" AssociatedControlID="chkQuotation" runat="server" Text="Quotation"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkQuotation" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label35" AssociatedControlID="chkPurchaseReturn" runat="server" Text="Purchase Return"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkPurchaseReturn" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="control-group-split">
                                                <div class="control-group-left">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label42" AssociatedControlID="ChksalesReturn" runat="server" Text="Sales Return"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="ChksalesReturn" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label39" AssociatedControlID="chkpointofsales" runat="server" Text="Point Of Sales"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="chkpointofsales" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-split">
                                                <div class="control-group-right">
                                                    <div class="label-left" style="width: 60%">
                                                        <asp:Label ID="Label43" AssociatedControlID="ChkSelectAll1" Font-Bold="true" ForeColor="blue" runat="server" Text="Select All"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 20%">
                                                        <asp:CheckBox ID="ChkSelectAll1" runat="server"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="button-contol" style="margin-left: 175px">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                        OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                                </div>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="GridDetails">
                            <div class="grid-search">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                                <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                                </asp:Panel>
                            </div>

                            <div class="GridDetails">

                                <div class="right-container-top-detail">
                                    <div class="GridDetails">
                                        <div class="row">
                                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                <div class="grdLoad">
                                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Delete</th>
                                                                <th scope="col">Edit</th>
                                                                <th scope="col">Serial No</th>
                                                                <th scope="col">Style code</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Inventory Type</th>
                                                                <th scope="col">Department Code</th>
                                                                <th scope="col">Category code</th>
                                                                <th scope="col">Brand Code</th>
                                                                <th scope="col">Origin</th>
                                                                <th scope="col">Warehouse</th>
                                                                <th scope="col">Uom</th>
                                                                <th scope="col">Compatible</th>
                                                                <th scope="col">Manufacturer</th>
                                                                <th scope="col">Item Type</th>
                                                                <th scope="col">Vendor Code</th>
                                                                <th scope="col">Merchandise Code</th>
                                                                <th scope="col">Floor</th>
                                                                <th scope="col">FSection</th>
                                                                <th scope="col">BinNo</th>
                                                                <th scope="col">Shelf</th>
                                                                <th scope="col">AutoPoReqBy</th>
                                                                <th scope="col">AllowExpireDate</th>
                                                                <th scope="col">PackageItem</th>
                                                                <th scope="col">AutoPoAllowed</th>
                                                                <th scope="col">SerialNoRequired</th>
                                                                <th scope="col">SpecialPriceAllowed</th>
                                                                <th scope="col">AllowMemberDiscount</th>
                                                                <th scope="col">SeasonalItem</th>
                                                                <th scope="col">Batch</th>
                                                                <th scope="col">Warehousestorage</th>
                                                                <th scope="col">AllowReward</th>
                                                                <th scope="col">MarkingItem</th>
                                                                <th scope="col">PO</th>
                                                                <th scope="col">GID</th>
                                                                <th scope="col">PR</th>
                                                                <th scope="col">POS</th>
                                                                <th scope="col">SR</th>
                                                                <th scope="col">OrderBooking</th>
                                                                <th scope="col">Estimate</th>
                                                                <th scope="col">Quotation</th>
                                                                <th scope="col">Tax</th>
                                                                <th scope="col">companyCode</th>
                                                                <th scope="col">sizes</th>
                                                                <th scope="col">Hide Barcode Date</th>
                                                                <th scope="col">StockType</th>
                                                                <th scope="col">Modify User</th>
                                                                <th scope="col">Modify Date
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 53px; width: 1304px; background-color: aliceblue;">
                                                        <asp:GridView ID="gvStyle" runat="server" AllowSorting="true" AutoGenerateColumns="False" ShowHeader="false"
                                                            ShowHeaderWhenEmpty="true" oncopy="return false" DataKeyNames="Stylecode"
                                                            CssClass="pshro_GridDgn grdLoad">
                                                            <EmptyDataTemplate>
                                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                            </EmptyDataTemplate>
                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                            <RowStyle CssClass="tbl_left" />
                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png" CommandName="Select"
                                                                            ToolTip="Click here to Delete" OnClientClick=" return Style_Delete(this); return false;" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                            ToolTip="Click here to edit" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:BoundField DataField="RowNumber" HeaderText="Serial No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField DataField="Stylecode" HeaderText="Style code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField DataField="InventoryType" HeaderText="Inventory Type" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="DepartmentCode" HeaderText="Department Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Categorycode" HeaderText="Category code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="BrandCode" HeaderText="Brand Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Origin" HeaderText="Origin" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Uom" HeaderText="Uom" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Compatible" HeaderText="Compatible" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="VendorCode" HeaderText="Vendor Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="MerchandiseCode" HeaderText="Merchandise Code" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Floor" HeaderText="Floor" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="FSection" HeaderText="FSection" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="BinNo" HeaderText="BinNo" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Shelf" HeaderText="Shelf" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="AutoPoReqBy" HeaderText="AutoPoReqBy" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="AllowExpireDate" HeaderText="AllowExpireDate" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="PackageItem" HeaderText="PackageItem" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="AutoPoAllowed" HeaderText="AutoPoAllowed" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="SerialNoRequired" HeaderText="SerialNoRequired" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="SpecialPriceAllowed" HeaderText="SpecialPriceAllowed" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="AllowMemberDiscount" HeaderText="AllowMemberDiscount" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="SeasonalItem" HeaderText="SeasonalItem" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Batch" HeaderText="Batch" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Warehousestorage" HeaderText="Warehousestorage" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="AllowReward" HeaderText="AllowReward" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="MarkingItem" HeaderText="MarkingItem" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="PO" HeaderText="PO" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="GID" HeaderText="GID" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="PR" HeaderText="PR" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="POS" HeaderText="POS" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="SR" HeaderText="SR" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>

                                                                <asp:BoundField DataField="OrderBooking" HeaderText="OrderBooking" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Estimate" HeaderText="Estimate" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Quotation" HeaderText="Quotation" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="Tax" HeaderText="Tax" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="companyCode" HeaderText="companyCode" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="sizes" HeaderText="sizes" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="HideBarcodeDate" HeaderText="Hide Barcode Date" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="StockType" HeaderText="StockType" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                <asp:BoundField DataField="ModifyUser" HeaderText="Modify User" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                                <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ups:PaginationUserControl runat="server" ID="StylePaging" OnPaginationButtonClick="Style_PaginationButtonClick" />
                            </div>
                            <div class="hiddencol">

                                <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
