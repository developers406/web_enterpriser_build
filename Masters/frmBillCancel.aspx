﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBillCancel.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmBillCancel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        </style>
    <script type="text/javascript">

        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupFloor').dialog('close');
                window.parent.$('#popupFloor').remove();

            }
        });
        function ValidateForm() {
            var Show = '';
            if (document.getElementById("<%=txtLocation.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Location Code';
                document.getElementById("<%=txtLocation.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtBillNo.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Bill Number';
                document.getElementById("<%=txtBillNo.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                return true;
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkBillCancel.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkBillCancel', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 112) {
                    e.preventDefault();
                }
        }
    };

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkBillCancel.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkBillCancel.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });

            $("[id$=txtBillNo]").autocomplete({

                source: function (request, response) {

                    $.ajax({
                        url: "frmBillCancel.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "','sLocationCode': '" + $("#<%=txtLocation.ClientID%>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("[id*=txtBillNo]").val(i.item.label);
                    return false;
                },
                minLength: 1
            });
        }
        function clearForm() {
           <%-- $('#<%=ddlLocation.ClientID %>').val('');
            $('#<%=ddlLocation.ClientID %>').trigger("liszt:updated");--%>
            $('#<%= txtBillNo.ClientID %>').val('');
            //$('#ContentPlaceHolder1_ddlLocation').val('');
            //$('#ContentPlaceHolder1_ddlLocation').trigger("liszt:updated");
            $('#<%=txtLocation.ClientID %>').val('');
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BillCancel');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BillCancel";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Bill Cancel</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>

                <div class="container-group-small" style="margin-bottom: 200px;">
                    <div class="container-control">

                        <div class="control-group-single-res" style="padding-top: 20px">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Location Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtBillNo');"></asp:TextBox>

                            </div>
                        </div>

                        <div class="control-group-single-res" style="padding-top: 20px">
                            <div class="label-left">
                                <asp:Label ID="lblBillNo" runat="server" Text="Bill Number"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBillNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Bill',  'txtBillNo', '');" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                    <div class="button-contol" style="margin-left: 80px; padding-bottom: 20px; padding-top: 10px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkBillCancel" runat="server" class="button-red" OnClick="lnkBillCancel_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Bill Cancel(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
