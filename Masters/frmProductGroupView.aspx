﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmProductGroupView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmProductGroupView" %>
<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 105px;
            max-width: 105px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 105px;
            max-width: 105px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 105px;
            max-width: 105px;
            text-align: center;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 200px;
            max-width: 200px;
        }
        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 200px;
            max-width: 200px;
        }
         
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkNew.ClientID %>').css("display", "initial");
                }
                else {
                    $('#<%=lnkNew.ClientID %>').css("display", "none");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'ProductGroup');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CategoryAttrribute";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }
        function fncDelete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                 ShowPopupMessageBox("You have no permission to Delete this Department");
                 return false;
             }
             $("#dialog-confirm").dialog({
                 resizable: false,
                 height: "auto",
                 width: "auto",
                 modal: true,
                 buttons: {
                     "YES": function () {
                         $(this).dialog("close");
                         console.log($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                         $('#<%=hidValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, '')); 
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false();
                    }
                }
            });
         }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Attribute</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="updateInvGrid" runat="Server">
            <ContentTemplate>
                   <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <div class="gridDetails">
                    <div class="grid-search">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"
                                        onFocus="this.select()"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'></asp:LinkButton>
                                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue display_none" PostBackUrl="~/Masters/frmProductGroup.aspx" Text="New"></asp:LinkButton>
                                </asp:Panel>
                            </div>
                           
                        </div>
                    </div>

                    <div class="container-group-pc1">
                        <div class="container-horiz-top-header1">
                            <div class="right-container-top-header">
                                Product Group
                            </div>
                            <div class="right-container-top-detail">
                                <div class="GridDetails">
                                    <div class="row">
                                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                            <div class="grdLoad">
                                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Delete
                                                            </th>
                                                            <th scope="col">Edit
                                                            </th>
                                                            <th scope="col">S.No
                                                            </th>
                                                            <th scope="col">GroupCode
                                                            </th>
                                                            <th scope="col">Group Name
                                                            </th>
                                                            <th scope="col">Department
                                                            </th>
                                                            <th scope="col">Category
                                                            </th>
                                                            <th scope="col">BarcodePrefix
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 376px; width: 1325px; background-color: aliceblue;">
                                                    <asp:GridView ID="grdProductGroup" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                        DataKeyNames="GroupNo" CssClass="pshro_GridDgn grdLoad">
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                               <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                                        ToolTip="Click here to delete" OnClientClick="fncDelete(this);return false;"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                        ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="RowNumber" HeaderText="S.No"></asp:BoundField>
                                                            <asp:BoundField DataField="GroupNo" HeaderText="GroupCode"></asp:BoundField>
                                                            <asp:BoundField DataField="GroupName" HeaderText="GroupName"></asp:BoundField> 
                                                            <asp:BoundField DataField="Department" HeaderText="Department" />
                                                            <asp:BoundField DataField="Category" HeaderText="Category"></asp:BoundField>
                                                            <asp:BoundField DataField="BarcodePrefix" HeaderText="BarcodePrefix"></asp:BoundField> 
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                                <ups:PaginationUserControl runat="server" ID="InvPaging" OnPaginationButtonClick="InvPaging_PaginationButtonClick" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updateInvGrid">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidValue" runat="server" />
    <div class="display_none">
        <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
    </div>
</asp:Content>
