﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmReasonMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmReasonMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 187px;
            max-width: 187px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 245px;
            max-width: 245px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 250px;
            max-width: 250px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 165px;
            max-width: 165px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 140px;
            max-width: 140px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }   
    </style>

    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';
            var ReasonCode = $.trim($('#<%=txtReasonCode.ClientID %>').val());
             if ($('#<%=txtReasonCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Reason");
                return false;
            }
            if (!($('#<%=txtReasonCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Reason");
                return false;
            }
            if (ReasonCode =='') {
                Show = Show + 'Please Enter Reason Code';
                document.getElementById("<%=txtReasonCode.ClientID %>").focus();

            }

            if (document.getElementById("<%=txtReasonDescription.ClientID%>").value == 0) {
                Show = Show + '<br />Please Enter Reason Description';
                document.getElementById("<%=txtReasonDescription.ClientID %>").focus();
            }

            if (document.getElementById("<%=ddlTranType.ClientID%>").value == 0) {
                Show = Show + '<br />Please Enter Transection Type';
                document.getElementById("<%=ddlTranType.ClientID %>").focus();

            }

            if (document.getElementById("<%=ddladditiondeduction.ClientID%>").value == "") {
                if (document.getElementById("<%=ddlTranType.ClientID%>").value == "BILL CANCEL" || document.getElementById("<%=ddlTranType.ClientID%>").value == "REPRINT") {

                }
                else {
                    Show = Show + '<br />Please Enter Addition/Deduction';
                    document.getElementById("<%=ddladditiondeduction.ClientID %>").focus();

                }
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function Reason_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
            ShowPopupMessageBox("You have no permission to Delete this Reason");
            return false;
        }
        $("#dialog-confirm").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "YES": function () {
                    $(this).dialog("close");
                    $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));

                    $("#<%= btnDelete.ClientID %>").click();
                },
                "NO": function () {
                    $(this).dialog("close");
                    returnfalse();
                }
            }
        });

    }




    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
            <%--$('#<%= txtReasonCode.ClientID %>').focus();--%>
            //            jQuery.extend(jQuery.expr[':'], {
            //                focusable: function (el, index, selector) {
            //                    return $(el).is('a, button, :input,  [tabindex]');
            //                    ShowPopupMessageBox(el);
            //                }
            //            });



            //            $(document).on('keypress', 'input,select', function (e) {
            //                if (e.which == 13) {
            //                    e.preventDefault();
            //                    // Get all focusable elements on the page
            //                    var $canfocus = $(':focusable');
            //                    var index = $canfocus.index(document.activeElement) + 1;
            ////                    ShowPopupMessageBox($canfocus.length);
            //                    if (index >= $canfocus.length) index = 0;
            ////                    ShowPopupMessageBox(index);
            //                    $canfocus.eq(index).focus();
            //                }
            //            });
            //$(document).ready(function () {
            //    $("input").first().focus();

            //    $('input').bind("keydown", function (e) {
            //        var n = $("input").length;
            //        if (e.which == 13) { //Enter key
            //            e.preventDefault(); //to skip default behavior of the enter key
            //            var nextIndex = $('input').index(this) + 1;
            //            if (nextIndex < n)
            //                $('input')[nextIndex].focus();
            //            else {
            //                $('input')[nextIndex - 1].blur();
            //                // $('#btnSubmit').click();
            //            }
            //        }
            //    });

            //$('#btnSubmit').click(function () {
            // ShowPopupMessageBox('Form Submitted');
            //  });
            //});
            $('#<%=txtReasonCode.ClientID %>').focus();
            $('#<%=txtReasonCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmReasonMaster.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtReasonCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtReasonCode.ClientID %>').prop("disabled", true);
                             $('#<%=txtReasonCode.ClientID %>').val(strarray[0]);
                             $('#<%=txtReasonDescription.ClientID %>').val(strarray[1]);
                             $('#<%= ddlTranType.ClientID %>').prop("disabled", true);
                             $('#<%=ddlTranType.ClientID %>').val($.trim(strarray[2]));
                             $('#<%=ddlTranType.ClientID %>').trigger("liszt:updated");
                             if ($.trim(strarray[2]).replace(/&nbsp;/g, '') == "BILL CANCEL" || $.trim(strarray[2]).replace(/&nbsp;/g, '') == "REPRINT") {

                                 $('#<%= ddladditiondeduction.ClientID %>').prop("disabled", true);
                                    $('#<%= ddladditiondeduction.ClientID %>').val("");
                                    $('#<%= ddladditiondeduction.ClientID %>').trigger("liszt:updated");
                                }
                                else {
                                    $('#<%=ddladditiondeduction.ClientID %>').prop("disabled", false);
                                    $('#<%=ddladditiondeduction.ClientID %>').val($.trim(strarray[3]).replace(/&nbsp;/g, ''));
                                    $('#<%=ddladditiondeduction.ClientID %>').trigger("liszt:updated");
                                }

                            }

                     },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

                $("[id$=txtReasonDescription]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmReasonMaster.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }

                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
               <%-- focus: function (event, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                    select: function (e, i) {

                        $('#<%=txtReasonDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
                });

                $(function () {
                    $('input:text:first').focus();
                    var $inp = $('.cls');
                    $inp.bind('keydown', function (e) {
                        //var key = (e.keyCode ? e.keyCode : e.charCode);
                        var key = e.which;
                        if (key == 13) {
                            e.preventDefault();
                            var nxtIdx = $inp.index(this) + 1;
                            $(".cls:eq(" + nxtIdx + ")").focus();
                        }
                    });
                });
                $(function () {
                    $('#<%=ddlTranType.ClientID %>').change(function () {
                        var selectedText = $(this).find("option:selected").text();
                        var selectedValue = $(this).val();
                        if (selectedValue == "BILL CANCEL" || selectedValue == "REPRINT") {

                            $('#<%= ddladditiondeduction.ClientID %>').prop("disabled", true);
                        $('#<%= ddladditiondeduction.ClientID %>').val('');
                        $('#<%= ddladditiondeduction.ClientID %>').trigger("liszt:updated");


                    }
                    else {
                        $('#<%=ddladditiondeduction.ClientID %>').prop("disabled", false);

                        $('#<%=ddladditiondeduction.ClientID %>').trigger("liszt:updated");
                    }
                    });
                });


            $('input[type=text]').bind('change', function () {
                if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {

                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
                }
            });

        }

        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Reason");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function DisplayDetails(row) {
            $('#<%= txtReasonCode.ClientID %>').prop("disabled", true);
            $('#<%= ddlTranType.ClientID %>').prop("disabled", true);
            $('#<%=txtReasonCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
            $('#<%=txtReasonDescription.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlTranType.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=ddlTranType.ClientID %>').trigger("liszt:updated");
            if ($("td", row).eq(4).html().replace(/&nbsp;/g, '') == "BILL CANCEL" || $("td", row).eq(4).html().replace(/&nbsp;/g, '') == "REPRINT") {

                $('#<%= ddladditiondeduction.ClientID %>').prop("disabled", true);
                $('#<%= ddladditiondeduction.ClientID %>').val("");
                $('#<%= ddladditiondeduction.ClientID %>').trigger("liszt:updated");
            }
            else {
                $('#<%=ddladditiondeduction.ClientID %>').prop("disabled", false);
                $('#<%=ddladditiondeduction.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
                $('#<%=ddladditiondeduction.ClientID %>').trigger("liszt:updated");
            }

        }


        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        ValidateForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                        else if (e.keyCode == 119) {
                            $('#<%= lnkClose.ClientID %>').click();
                                 e.preventDefault();
                             }


                 }
             };

             $(document).ready(function () {
                 $(document).on('keydown', disableFunctionKeys);
             });
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            $('#<%= ddlTranType.ClientID %>').val('');
            $('#<%=ddlTranType.ClientID %>').trigger("liszt:updated");
            $('#<%= ddladditiondeduction.ClientID %>').val('');
            $('#<%=ddladditiondeduction.ClientID %>').trigger("liszt:updated");
            return false;
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'ReasonMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "ReasonMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Reason Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group">
            <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                    </div>
                    <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                    <div class="container-control">
                        <div class="container-left">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="lblReasoncode" runat="server" Text='<%$ Resources:LabelCaption,lblReasoncode %>'></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtReasonCode" TabIndex="1" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="lblReasonDescription" runat="server" Text='<%$ Resources:LabelCaption,lblReasonDescription %>'></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtReasonDescription" TabIndex="2" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="lblTransactionType" runat="server" Text="TransactionType"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:DropDownList ID="ddlTranType" TabIndex="3" runat="server" CssClass="form-control" Width="230px">
                                        <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="BILL CANCEL" Value="BILL CANCEL"></asp:ListItem>
                                        <asp:ListItem Text="REPRINT" Value="REPRINT"></asp:ListItem>
                                        <asp:ListItem Text="PURCHASE RETURN" Value="PURCHASE RETURN"></asp:ListItem>
                                        <asp:ListItem Text="SALES RETURN" Value="SALES RETURN"></asp:ListItem>
                                        <asp:ListItem Text="CREDIT/DEBIT NOTE" Value="CREDIT/DEBIT NOTE"></asp:ListItem>
                                        <asp:ListItem Text="TRANSFER" Value="TRANSFER"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>

                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="lbladditiondeduction" runat="server" Text='<%$ Resources:LabelCaption,lbladditiondeduction %>'></asp:Label>
                                </div>
                                <div style="float: right">

                                    <asp:DropDownList ID="ddladditiondeduction" TabIndex="4" runat="server" CssClass="form-control" Width="230px">
                                        <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                        <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="GridDetails">
            <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">

                            <%-- <div class="right-container-top-detail">--%>
                            <div class="grdLoad">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">Edit
                                            </th>
                                            <th scope="col">Reason Code
                                            </th>
                                            <th scope="col">Reason Description
                                            </th>
                                            <th scope="col">Transaction Type
                                            </th>
                                            <th scope="col">Addition/Deduction
                                            </th>
                                            <th scope="col">Modify User
                                            </th>
                                            <th scope="col">Modify Date
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 250px; width: 1350px; background-color: aliceblue;">
                                    <asp:GridView ID="gvReason" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                        ShowHeaderWhenEmpty="true"
                                        CssClass="pshro_GridDgnStyle grdLoad" oncopy="return false" AllowPaging="True" PageSize="8"
                                        OnPageIndexChanging="gvReason_PageIndexChanging" DataKeyNames="ReasonCode" ShowHeader="false">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                        ToolTip="Click here to Delete" OnClientClick=" return Reason_Delete(this); return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                        ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ReasonCode" HeaderText="Reason Code" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            <asp:BoundField DataField="ReasonDescription" HeaderText="Reason Description" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            <asp:BoundField DataField="Transectiontype" HeaderText="Transaction Type" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            <asp:BoundField DataField="AddDedu" HeaderText="Addition/Deduction" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hiddencol">

                        <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
