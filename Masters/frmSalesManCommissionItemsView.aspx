﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesManCommissionItemsView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmSalesManCommissionItemsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 85px;
            max-width: 85px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 83px;
            max-width: 83px;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 65px;
            max-width: 65px;
        }


        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 185px;
            max-width: 185px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 170px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 107px;
            max-width: 107px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 96px;
            max-width: 96px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 130px;
            max-width: 130px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript" language="Javascript">

        function fncValidateCheckAll() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvItemWiseSales.ClientID %> [id*=chkSingle]").prop('checked', true);

                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvItemWiseSales.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    //ShowPopupMessageBox($("#<%=gvItemWiseSales.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvItemWiseSales.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKeyTextBox(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //ShowPopupMessageBox($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.next().find('input[id *= txtComm]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }



        function fncValidateViewCommission() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {

                    return true;
                }
                else {
                    fncShowDataLossMessage();

                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        function fncShowDataLossMessage() {
            try {
                InitializeDialogDataLoss();
                $("#ConfirmDataLoss").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDataLoss() {
            try {
                $("#ConfirmDataLoss").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseDataLoss() {
            try {
                $("#ConfirmDataLoss").dialog('close');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowShowPopupMessageBoxNoItemsMessage() {
            try {
                InitializeDialogShowPopupMessageBoxNoItems();
                $("#ShowPopupMessageBoxNoItems").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseShowPopupMessageBoxNoItemsDialog() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogShowPopupMessageBoxNoItems() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncValidateSave() {
            try {
                //
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        function fncShowSuccessDeleteMessage() {
            try {
                //ShowPopupMessageBox("message");
                InitializeDialogdelete();
                $("#StockUpdatePosting").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogdelete() {
            try {
                $("#StockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCloseDeleteDialog() {
            try {
                $("#StockUpdatePosting").dialog('close');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("#<%=txtDescription.ClientID %>").val('');


        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SalesManCommissionItemView');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SalesManCommissionItemView";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Man</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Masters/frmSalesManCommissionItemWise.aspx">
                    <%=Resources.LabelCaption.lbl_ItemWiseSaleManCom%></a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_SalesManCommissionItemList%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="top-container-im">
                <div class="left-container-detail">
                    <div class="control-group-split">
                        <div class="control-group-split" style="width: 70%;">
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 30%;">
                            <div style="float: right; display: table">
                                <div class="control-button">
                                    <asp:LinkButton ID="LinkButton11" runat="server" class="button-blue" OnClick="lnkfetch_Click"
                                        Text='<%$ Resources:LabelCaption,btn_Fetch %>' Width="100px"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split" style="width: 70%;">
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                        style="height: 350px">

                        <div class="GridDetails">
                            <div class="row">
                                 <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="right-container-top-detail">
                                        <div class="grdLoad">
                                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Select
                                                        </th>
                                                        <th scope="col">Item Code
                                                        </th>
                                                        <th scope="col">Serial No
                                                        </th>
                                                        <th scope="col">Description
                                                        </th>
                                                        <th scope="col">Department
                                                        </th>
                                                        <th scope="col">Category
                                                        </th>
                                                        <th scope="col">Brand
                                                        </th>
                                                        <th scope="col">Selling Price
                                                        </th>
                                                        <th scope="col">Valid From
                                                        </th>
                                                        <th scope="col">Valid To
                                                        </th>
                                                        <th scope="col">Comm.(%)
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 201px; width: 1335px; background-color: aliceblue;">
                                                <asp:GridView ID="gvItemWiseSales" runat="server" AutoGenerateColumns="False"
                                                    ShowHeaderWhenEmpty="true" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                        <asp:BoundField DataField="Inventorycode" HeaderText="Item Code"></asp:BoundField>
                                                        <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                        <asp:BoundField DataField="DepartmentCode" HeaderText="Department"></asp:BoundField>
                                                        <asp:BoundField DataField="CategoryCode" HeaderText="Category"></asp:BoundField>
                                                        <asp:BoundField DataField="BrandCode" HeaderText="Brand"></asp:BoundField>
                                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="PFDate" HeaderText="Valid From"></asp:BoundField>
                                                        <asp:BoundField DataField="PTDate" HeaderText="Valid To"></asp:BoundField>
                                                        <asp:BoundField DataField="SCom" HeaderText="Comm.(%)" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div style="float: right; display: table">
                <div class="control-button">
                    <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                        Text='<%$ Resources:LabelCaption,btn_SelectAll %>' Width="150px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Delete %>'
                        OnClientClick="fncValidateDelete();return false;" Width="150px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                        OnClientClick="clearForm();return true;" OnClick="lnkbtnClear_Click" Width="150px"><i class="icon-play"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="SelectAny">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteStockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmDataLoss">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_DataLoss%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return CloseDataLoss()"
                        Text='<%$ Resources:LabelCaption,btn_OK %>' OnClick="lnkViewCommission_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton8" runat="server" class="button-blue" OnClientClick="return CloseDataLoss() "
                        Text='<%$ Resources:LabelCaption,btn_Cancel %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_Delete%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ShowPopupMessageBoxNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseShowPopupMessageBoxNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full">
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
