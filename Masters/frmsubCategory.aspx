﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmsubCategory.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmsubCategory" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 64px;
            max-width: 64px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 64px;
            max-width: 64px;
            text-align: center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 64px;
            max-width: 64px;
            text-align: center !important;
        }
        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 261px;
            max-width: 261px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 250px;
            max-width: 264px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 167px;
            max-width: 167px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 110px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 165px;
            max-width: 165px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 167px;
            max-width: 167px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            display: none;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display: none;
        }


        .Discount {
            margin-left: 438px;
            padding: 10px 10px 0px 10px;
            margin-top: -199px;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
             .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        var divDis = '0';
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupSubCategory').dialog('close');
                window.parent.$('#popupSubCategory').remove();

            }
        });
        function ValidateForm() {
            if ($('#<%=txtSubCategoryCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Subcategory");
                return false;
            }
            if (!($('#<%=txtSubCategoryCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Subcategory");
                return false;
            }
            var Show = '';
            var SubCategoryCode = $.trim($('#<%=txtSubCategoryCode.ClientID %>').val());         //22112022 musaraf
            var SubCategoryName = $.trim($('#<%=txtSubCategoryName.ClientID %>').val());

            if (SubCategoryCode == "") {
                Show = Show + 'Please Enter SubCategoryCode';
                document.getElementById("<%=txtSubCategoryCode.ClientID %>").focus();
            }
            if (SubCategoryName == "") {
                Show = Show + '<br />Please Enter Sub Category Name';
                document.getElementById("<%=txtSubCategoryName.ClientID %>").focus();
            }

            if (document.getElementById("<%=ddlCategory.ClientID%>").value == 0) {
                Show = Show + '<br />Please Select Category';
                document.getElementById("<%=ddlCategory.ClientID %>").focus();
            }

            if (divDis == "1") {
                if ($("[id*=cblLocation] input:checked").length == 0) {
                    Show = Show + '<br />Please Enter any one Location';
                }
            }


            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }



        function Toast() {

            $('#<%=divDiscount.ClientID%>').show();
        divDis = '1';

    }
    function SubCategory_Delete(source) {

        if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                 ShowPopupMessageBox("You have no permission to Delete this Subcategory");
                 return false;
             }
             $("#dialog-confirm").dialog({
                 resizable: false,
                 height: "auto",
                 width: 400,
                 modal: true,
                 buttons: {
                     "YES": function () {
                         $(this).dialog("close");
                         $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                         $("#<%= btnDelete.ClientID %>").click();
                     },
                     "NO": function () {
                         $(this).dialog("close");
                         returnfalse();
                     }
                 }
             });

         }
         function imgbtnEdit_ClientClick(source) {
             if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Subcategory");
                return false;
            }
            clearForm();
            $('#<%=rdDiscount.ClientID %>').find("input[value='SellingPrice']").prop("checked", true);
        DisplayDetails($(source).closest("tr"));

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        return true;
    }

    function DisplayDetails(row) {

        $('#<%=txtSubCategoryCode.ClientID %>').prop("disabled", true);
        $('#<%=ddlCategory.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
        $('#<%=txtSubCategoryCode.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        $('#<%=txtSubCategoryName.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        $('#<%=txtDiscountPercent.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        if ($("td", row).eq(9).html() == 'SellingPrice') {
            $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
        }
        else if ($("td", row).eq(9).html() == 'Mrp') {
            $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
        }
        GetLocation();

    }
    function GetLocation() {
        //alert('test');
        var subCode = $('#<%=txtSubCategoryCode.ClientID %>').val();
            var dis = $('#<%=txtDiscountPercent.ClientID %>').val();
            //alert('test');
            try {
                $.ajax({
                    url: "frmsubCategory.aspx/fncGetLoc",
                    data: "{ 'subCode': '" + subCode + "','dis': '" + dis + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        objData = jQuery.parseJSON(msg.d);

                        // debugger;                                                    
                        if (objData.length > 0) {
                            var objPrice = objData[0]["DiscountBasedOn"];
                            if (objPrice == 'SellingPrice') {
                                $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
                            }
                            else if (objPrice == 'Mrp') {
                                $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
                            }
                            var valNew = objData[0]["LocationCode"].split(',');

                            var data = valNew.slice(1, 10000);
                            //console.log($("[id*=cblLocation] input"));
                            $("[id*=cblLocation] input:checkbox").removeAttr("checked");
                            var checkboxList = $("[id*=cblLocation]");
                            var chk = checkboxList.slice(1, 10000);
                            for (var i = 0; i <= data.length; i++) {


                                checkboxList.each(function () {
                                    if ($(this).val() == data[i]) {
                                        $(this).prop("checked", true);
                                    }
                                });
                                // console.log(checkboxList.length);

                            }
                            if (chk.length == data.length) {
                                $("[id*=cbSelectAll]").attr("checked", "checked");
                            }
                        }


                    },
                    error: function (msg) {
                        console.log(msg.message);
                    },
                    failure: function (msg) {
                        console.log(msg.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });
    </script>
    <script type="text/javascript">
        function pageLoad() {

            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
             $("select").chosen({ width: '100%' }); // width in px, %, em, etc

             $('#<%=txtSubCategoryCode.ClientID %>').focus();

            $('#<%=txtSubCategoryCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmsubCategory.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtSubCategoryCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {

                            $('#<%=txtSubCategoryCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtSubCategoryCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtSubCategoryName.ClientID %>').val(strarray[1]);
                            $('#<%=txtDiscountPercent.ClientID %>').val(strarray[3]);
                            $('#<%=ddlCategory.ClientID %>').val($.trim(strarray[2]));
                            $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");



                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });


            $("[id$=txtSubCategoryName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmsubCategory.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                select: function (e, i) {

                    $('#<%=txtSubCategoryName.ClientID %>').val($.trim(i.item.label));

                     return false;
                 },
                 minLength: 1
             });

                 $("[id$=txtSearch]").autocomplete({
                     source: function (request, response) {
                         $.ajax({
                             url: "frmsubCategory.aspx/GetFilterValue",
                             data: "{ 'prefix': '" + request.term + "'}",
                             dataType: "json",
                             type: "POST",
                             contentType: "application/json; charset=utf-8",
                             success: function (data) {
                                 response($.map(data.d, function (item) {
                                     return {
                                         label: item.split('-')[1],
                                         val: item.split('-')[1]
                                     }
                                 }))
                             },
                             error: function (response) {
                                 ShowPopupMessageBox(response.responseText);
                             },
                             failure: function (response) {
                                 ShowPopupMessageBox(response.responseText);
                             }
                         });
                     },

                     focus: function (event, i) {
                         $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                            event.preventDefault();
                        },
                        select: function (e, i) {
                            $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },

                    minLength: 1
                    });

                    //$('input[type=text]').bind('change', function () {
                    //    if (this.value.match(SpecialChar)) {

                    //        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    //        this.value = this.value.replace(SpecialChar, '');
                    //    }
                    //});

                }
                function clearForm() {
                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    $(':checkbox, :radio').prop('checked', false);
                    $('#<%=txtSubCategoryCode.ClientID %>').focus();
                    $('#<%=ddlCategory.ClientID %>').val('');
                    $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                }
                function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            $('#<%= lnkSave.ClientID %>').click();
                             e.preventDefault();
                         }
                         else if (e.keyCode == 117) {
                             $("input").removeAttr('disabled');
                             $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                        <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                     }
                 };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'SubCategory');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "SubCategory";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Sub Category</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Sub Category Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategoryCode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Sub Category Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategoryName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Category"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">

                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="DiscountPercent"></asp:Label>
                            </div>
                            <div class="label-right">

                                <asp:TextBox ID="txtDiscountPercent" MaxLength="18" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 Discount" runat="server" id="divDiscount">
                    <div class="container-group-small" style="width: 100%;">
                        <div class="container-control">
                            <div class="control-group-single-res">

                                <div class="terminal-detail-header">
                                    Discount Based On
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="col-md-8">
                                    <asp:RadioButtonList ID="rdDiscount" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                        <asp:ListItem Value="SellingPrice" Selected="True">Selling Price</asp:ListItem>

                                        <asp:ListItem style="margin-left: 71px" Value="Mrp">MRP</asp:ListItem>

                                    </asp:RadioButtonList>
                                    <%--<asp:RadioButton ID="rdSellingPrice" runat="server" Text="Selling Price" checked="true" />  
                                    <asp:RadioButton ID="rdMRP" runat="server" Text="MRP" Style="margin-left:71px" />  --%>
                                </div>

                            </div>

                            <div class="control-group-single-res">
                                <%--  <div class="label-left">
                                    <asp:Label ID="Label20" runat="server" Text="Location"></asp:Label>
                                </div>--%>
                                <div class="label-right">

                                    <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                </div>
                                <div class="control-group-single">
                                    <div style="overflow-y: auto; width: 75%; height: 90px">
                                        <asp:CheckBoxList ID="cblLocation" CssClass="cbLocation" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <hr />
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                             <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    <br />


                    <div class="right-container-top-detail">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Edit
                                                </th>
                                                <th scope="col">Serial No
                                                </th>
                                                <th scope="col">SubCategoryCode
                                                </th>
                                                <th scope="col">SubCategoryName
                                                </th>
                                                <th scope="col">RefCategoryCode
                                                </th>
                                                <th scope="col">DiscountPercent
                                                </th>
                                                <th scope="col">Modify User
                                                </th>
                                                <th scope="col">Modify Date
                                                </th>
                                                <th scope="col">DiscountBasedon
                                                </th>
                                                <th scope="col">Location
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 126px; width: 1330px; background-color: aliceblue;">
                                        <asp:GridView ID="gvSubCategory" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn grdLoad" oncopy="return false" DataKeyNames="SubCategoryCode">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return SubCategory_Delete(this); return false;"
                                                            ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                            ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNumber" HeaderText="Serial No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="SubCategoryCode" HeaderText="SubCategoryCode" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="SubCategoryName" HeaderText="SubCategoryName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="RefCategoryCode" HeaderText="RefCategoryCode" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="DiscountPercent" HeaderText="DiscountPercent" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="ModifyUser" HeaderText="Modify User" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date" ItemStyle-HorizontalAlign="Left"></asp:BoundField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ups:PaginationUserControl runat="server" ID="SubCategoryPaging" OnPaginationButtonClick="SubCategory_PaginationButtonClick" />


                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
