﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmCustomerMasterView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmCustomerMasterView" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        } 
        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }


        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 103px;
            max-width: 103px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 214px;
            max-width: 214px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 442px;
            max-width: 442px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 123px;
            max-width: 123px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
        .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function fncClickYearEnd() {
            $('#<%=txtYear.ClientID %>').val('');

            $("#dialog-confirm").dialog("open");
            return false;
        }

        function fncClickChangeMember() {
            $("#dialog-CustomerChange").find("input[type=text]").val('');

            $("#dialog-CustomerChange").dialog("open");
            return false;
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
           <%--  if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }--%>
            $(function () {
                $("#dialog-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            $("#dialog-YearEnd").dialog("open");
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });


            $(function () {
                $("#dialog-YearEnd").dialog({
                    appendTo: 'form:first',
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 350,
                    modal: true,
                    buttons: {
                        "Ok": function () {

                            if ($('#<%=txtYear.ClientID %>').val() == "") {
                                ShowPopupMessageBox('Enter Year')
                                return false;
                            }

                            __doPostBack('ctl00$ContentPlaceHolder1$lnkOk', '');

                            $(this).dialog("close");
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });

            //=============> For Default dialog plugin moves the dialog DIV to the end of the page Move below line moves the DIV back into the form

            //            $("#dialog-YearEnd").parent().appendTo(jQuery("form:first"));

            $(function () {
                $("#dialog-CustomerChange").dialog({
                    appendTo: 'form:first',
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 650,
                    modal: true,
                    buttons: {
                        "Save": function () {

                            if ($('#<%=txtInvoiceNo.ClientID %>').val() == "") {
                                ShowPopupMessageBox('Enter Invoice No')
                                return false;
                            }
                            if ($('#<%=txtInvoiceAmt.ClientID %>').val() == "") {
                                ShowPopupMessageBox('Enter Invoice No')
                                return false;
                            }
                            if ($('#<%=txtNewCustName.ClientID %>').val() == "") {
                                ShowPopupMessageBox('Select Customer Name')
                                return false;
                            }

                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSaveChangeMem', '');

                            $(this).dialog("close");

                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });


            $('#<%=txtInvoiceNo.ClientID %>').focusout(function () {
                if ($('#<%=txtInvoiceNo.ClientID %>').val() == "") {
                    //ShowPopupMessageBox('Enter Invoice No');
                    return false;
                }
                var sInvoiceNo = $('#<%=txtInvoiceNo.ClientID%>').val()

                $.ajax({
                    type: "POST",
                    url: "frmCustomerMasterView.aspx/loadInvoiceNo",
                    data: "{'InvoiceNo':'" + sInvoiceNo + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null) {
                            $('#<%=txtInvoiceDate.ClientID %>').val(msg.d[0]);
                            $('#<%=txtInvoiceAmt.ClientID %>').val(msg.d[1]);
                            $('#<%=txtPrevCustomerCode.ClientID %>').val(msg.d[2]);
                            $('#<%=txtPrevCustomerName.ClientID %>').val(msg.d[3]);
                        }
                        else {
                            ShowPopupMessageBox("Invalid Invoice No");
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });
            });


            $("[id$=txtNewCustCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmCustomerMasterView.aspx/loadCustomer",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0] + '-' + item.split('-')[1],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                }, 
                //==========================> After Select Value
                select: function (e, i) {
                    $.ajax({
                        type: "POST",
                        url: "frmCustomerMasterView.aspx/GetCustomerDetail",
                        data: "{'CustomerCode':'" + i.item.val + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            $('#<%=txtNewCustCode.ClientID %>').val(i.item.val);
                            $('#<%=txtNewCustName.ClientID %>').val(msg.d[0]);
                            $('#<%=txtAddress1.ClientID %>').val(msg.d[1]);
                            $('#<%=txtAddress2.ClientID %>').val(msg.d[2]);
                            $('#<%=txtAddress3.ClientID %>').val(msg.d[3]);
                            $('#<%=txtZipCode.ClientID %>').val(msg.d[4]);
                            $('#<%=txtMobileNo.ClientID %>').val(msg.d[5]);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.d);
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.d);
                        }
                    });

                    return false;
                },

                minLength: 1
            });
        }
    </script>
    <script type="text/javascript">
        $(document).keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        })
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'CustomerMasterView');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "CustomerMasterView";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer Master View</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Customer</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="gridDetails">
                    <div class="grid-search">
                        <div class="grid-search-left">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" OnClick="lnkSearchGrid_Click"
                                    OnClientClick="return Validate()">Search</asp:LinkButton>
                                <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px" OnClick="lnkNew_Click"><i class="icon-play"></i>New</asp:LinkButton>
                            </asp:Panel>
                        </div>
                        <div class="grid-search-right">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkYearEnd" runat="server" class="button-blue" Text="Year End Process"
                                    OnClientClick="return fncClickYearEnd()"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkChangeMember" runat="server" class="button-blue" Text="Change Member By Invoice"
                                    OnClientClick="return fncClickChangeMember()"></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                         <asp:label Id="lblcount" runat="server" Text="Total CustomerMasterView : " style="font-weight: 900; float:left;"></asp:label>
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                            <div class="right-container-top-detail">
                                   <div class="GridDetails">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">Customer Code
                                                    </th>
                                                    <th scope="col">Customer Name
                                                    </th>
                                                    <th scope="col">Address
                                                    </th>
                                                    <th scope="col">Mobile No
                                                    </th>
                                                    <th scope="col">Loyalty Card No
                                                    </th>
                                                    <th scope="col">Status
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 349px; width: 1330px; background-color: aliceblue;">
                                            <asp:GridView ID="gvCustomer" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" 
                                                oncopy="return false" AllowPaging="True" PageSize="18" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                                DataKeyNames="CUST_CODE">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <%--<PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                                <PagerStyle CssClass="pshro_text" />--%>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                    <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code"></asp:BoundField>
                                                    <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name"></asp:BoundField>
                                                    <asp:BoundField DataField="Address" HeaderText="Address"></asp:BoundField>
                                                    <asp:BoundField DataField="CUST_MOBILE" HeaderText="Mobile No"></asp:BoundField>
                                                    <asp:BoundField DataField="CUST_LOYALTY_CARD_NO" HeaderText="Loyalty Card No"></asp:BoundField>
                                                    <asp:BoundField DataField="CUST_STATUS" HeaderText="Status"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
           <ups:PaginationUserControl runat="server" ID="cashierPaging" OnPaginationButtonClick="cashierPagingPaging_PaginationButtonClick" />
                </div>
                <div id="dialog-confirm" title="Enterpriser">
                    <p>
                        <span class="ui-icon ui-icon-ShowPopupMessageBox" style="float: left; margin: 1px 12px 20px 0;"></span>Do You want Proceed Year End Process ?
                    </p>
                </div>
                <div id="dialog-YearEnd" title="Enterpriser">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text="Enter Year"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control-res" MaxLength="4"
                                onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>
                        <asp:LinkButton ID="lnkOk" runat="server" class="button-blue" Text="Ok" OnClick="lnkOk_Click"
                            Visible="false"></asp:LinkButton>
                    </div>
                </div>
                <div id="dialog-CustomerChange" title="Change Member By Invoice">
                    <div class="control-group-single">
                        <div class="label-left">
                            <asp:Label ID="Label19" runat="server" Text="Invoice No"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label22" runat="server" Text="Invoice Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label23" runat="server" Text="Invoice Amt"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtInvoiceAmt" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Customer Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPrevCustomerCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-right" style="width: 100%">
                                <asp:TextBox ID="txtPrevCustomerName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="New Cust Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtNewCustCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-right" style="width: 100%">
                                <asp:TextBox ID="txtNewCustName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-single">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text="Address1"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Address2"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Address3"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <asp:LinkButton ID="lnkSaveChangeMem" runat="server" class="button-blue" Text="Ok"
                        Visible="false" OnClick="lnkSaveChangeMem_Click"></asp:LinkButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
