﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAgent.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmAgent" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: Black;
            background-color: Silver;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }

        input:checked + label {
            color: white;
            background: red;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    </style>
    <script type="text/javascript">
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupVendor').dialog('close');
                window.parent.$('#popupVendor').remove();

            }
        });
        function ValidateForm() {

            if (document.getElementById("<%=txtAgentCode.ClientID %>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAgentCode.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Agent Code');
                return false;
            }
            else if (document.getElementById("<%=txtAgentName.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAgentName.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Agent Name');
                return false;
            }
            else if (document.getElementById("<%=txtAddress1.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAddress1.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Address');
                return false;
            }
            else {
                return true;
            }


}

function fncSetText() {

    //var GSttxt = $("[id$=txtTIN]").val();
    //var checked_radio = $("[id*=rdoGstType] input:checked");
    //if (GSttxt == '') {
    //    checked_radio.next().text(checked_radio.next().text().replace(" - RCM", "") + ' - RCM');
    //}
    //else {
    //    var value = checked_radio.val();
    //    if (value == '1')
    //        checked_radio.next().text('IntraState');
    //    else if (value == '2')
    //        checked_radio.next().text('InterState');
    //}
}

function fncCountryValidate() {

    var Country = $("[id$=txtCountry]").val();
    var Orgin = $("[id*=HiddenCountry]").val();
    //ShowPopupMessageBox(Country.toUpperCase()); 
    if (Country.trim() != '') {
        if (Country.toUpperCase() != Orgin.toUpperCase()) {
            var radio = $("[id*=rdoGstType] label:contains('Import')").closest("td").find("input");
            radio.attr("checked", "checked");
        }
        else {
            var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
            radio.attr("checked", "checked");
        }
    }

}
    </script>
    <script type="text/javascript">
        function pageLoad() {

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
                $('#<%=chkActive.ClientID %>').prop("checked", true);
            jQuery("[id$=txtAgentName]").keyup(function () {
                var raw_text = jQuery(this).val();
                var return_text = raw_text.replace(/[^a-zA-Z0-9 _]/g, 'null');
                if (return_text == "null") {
                    jQuery(this).val('');
                    popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAgentName.ClientID%>");
                    ShowPopupMessageBox('Please Enter The Vaild Charaters');
                    return false;
                }
                else {
                    jQuery(this).val(return_text);
                }


            });
            $("[id$=txtAgentName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Masters/frmAgent.aspx/GetFilterValue") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtAgentCode.ClientID %>').val($.trim(i.item.val));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtAgentCode.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtAgentName.ClientID %>').val($.trim(i.item.label));
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                    return false;

                },
                minLength: 1
            });

            //$('#<%=txtTIN.ClientID %>').prop("disabled", true);
            //$('#<%=txtCST.ClientID %>').prop("disabled", true);


            $(document).ready(function () {
                $('#<%=chkTIN.ClientID %>').change(function () {
                    if ($(this).is(":checked")) {
                        $('#<%=txtTIN.ClientID %>').prop('disabled', false);
                    }
                    else {
                        $('#<%=txtTIN.ClientID %>').prop('disabled', true);
                    }
                });

                $('#<%=chkCST.ClientID %>').change(function () {
                    if ($(this).is(":checked")) {
                        $('#<%=txtCST.ClientID %>').prop('disabled', false);
                        $('#<%=txtCSTPRC.ClientID %>').prop('disabled', true);
                    }
                    else {
                        $('#<%=txtCST.ClientID %>').prop('disabled', true);
                        $('#<%=txtCSTPRC.ClientID %>').prop('disabled', true);
                    }
                });
            });
            $('#<%=ddlState.ClientID %>').change(function () {
                $('#<%=txtPhoneNo.ClientID %>').focus();
            });

        }


        $(document).ready(function () {
            $('#<%=ddlState.ClientID %>').change(function () {
                $('#<%=txtPhoneNo.ClientID %>').focus();
  });
        });
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
        }
    </script>
    <script language="Javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Agent');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Agent";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Agent Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="top-container">
                        <div class="col-md-9">
                            <div class="left-top-container-header-vm">
                                Agent Infomation
                            </div>
                            <div class="left-top-container-detail-vm">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label1" runat="server" Text="Agent Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAgentCode" runat="server" MaxLength="20" ReadOnly="false" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text="Agent Name"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAgentName" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="Address1"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label18" runat="server" Text="Address2"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" MaxLength="50" Text="City"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text="State"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Zip Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtZipCode" MaxLength="20" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Phone No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPhoneNo" MaxLength="30" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text="Mobile No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10" onkeydown="return isNumberKeyWithDecimalNew(event)" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">

                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="Country"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCountry" runat="server" MaxLength="50" CssClass="form-control-res" onfocusout="return fncCountryValidate();"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none;">
                                        <div class="label-left">
                                            <asp:Label ID="Label7" runat="server" Text="Fax"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFax" MaxLength="30" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="E-Mail"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtEmail" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="URL"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtURL" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text="Purchase Limit"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPurchaseLimit" MaxLength="9" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Attn"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAttn" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label50" runat="server" Text="PO Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPORemarks" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label51" runat="server" Text="Cheque Print Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtChequePrintName" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label52" runat="server" Text="Bank Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBankCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label53" runat="server" Text="Bank Branch"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBankBranch" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label54" runat="server" Text="Bank AC No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBankAcNo" runat="server" MaxLength="40" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:CheckBox ID="chkTIN" runat="server" Visible="false" />
                                            GSTTIN
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTIN" runat="server" CssClass="form-control-res" onfocusout="return fncSetText();"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkActive" runat="server" Text="Active" Class="radioboxlistgreen" />
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none;">
                                        <div class="label-left">
                                            <asp:CheckBox ID="chkCST" runat="server" />
                                            CST
                                        </div>
                                        <div class="label-right" style="width: 32%">
                                            <asp:TextBox ID="txtCST" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                        <div class="label-right" style="width: 5%; margin-left: 1%">
                                            %
                                        </div>
                                        <div class="label-right" style="width: 20%; margin-left: 1%">
                                            <asp:TextBox ID="txtCSTPRC" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-container">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                        OnClientClick="return ValidateForm()">Save</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()">Clear</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkView" runat="server" class="button-red" PostBackUrl="~/Masters/frmAgentView.aspx">View</asp:LinkButton>
                                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Style="visibility: hidden"
                                        OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="HiddenCountry" runat="server" ClientIDMode="Static" Value="" />
    </div>
</asp:Content>

