﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPaymodecategory.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmPaymodecategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#<%= txtfromdate.ClientID %>").val() == "") {
                $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
                $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
                $("#ContentPlaceHolder1_HideFilter_GridOverFlow").remove();
            }
            else {
                $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" });
                $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" });
                
            }
        });
        function fncClr() {
            $("#ContentPlaceHolder1_HideFilter_GridOverFlow").remove();
            $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
            $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">PaymodeCategory</li>
            </ul>
        </div>
    </div>
     <div class="container-group-small">
        <div class="container-control">


            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblfrm" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtfromdate" runat="server" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblto" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txttodate" runat="server" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            </div>
         
     <div class="button-contol" style="margin-left: 160px">
                <div class="control-button">
                    <asp:LinkButton ID="lnkLoadDet" runat="server" class="button-red"   OnClick="lnkload_Click"><i class="icon-play"></i>Fetch</asp:LinkButton>
                </div>
         <div class="control-button">
         <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" onClick="lnkExport_Click">Export</asp:LinkButton>
             </div>
           <div class="control-button">
         <asp:LinkButton ID="lnkclear" runat="server" class="button-blue" onClientClick="return fncClr();">Clear</asp:LinkButton>
             </div>
</div>
   </div>
    <asp:UpdatePanel ID="updatePnl" UpdateMode="Conditional" runat="Server">
                            <ContentTemplate>
    <div class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: scroll; height: 300px; background-color: aliceblue;"
                                        id="HideFilter_GridOverFlow" runat="server">
                                        <div class="grdLoad">
                                            <table rules="all" id="tblHead" class="Payment_fixed_headers">
                                             
                                            </table>
                                          <div class="grdbodyAp" id="divGrd2" runat="server">
                                                <asp:GridView ID="grdItemDetails" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="FixedHeader" OnRowDataBound="grdItemDetails_RowDataBound"
                                                  
                                                    PageSize="14" ShowHeaderWhenEmpty="True" ShowHeader="true" CssClass="pshro_GridDgn">

                                                    <PagerStyle CssClass="pshro_text" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                   

                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        PageButtonCount="5" Position="Bottom" />
                                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                </asp:GridView>
                                            </div>

                                    </div>
                                    </div>
                                </ContentTemplate>
                              <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnload" />
                            </Triggers>
        </asp:UpdatePanel>
                    <div class="display_none">
                   <asp:LinkButton ID="btnLoad" runat="server" OnClick="lnkload_Click"></asp:LinkButton>
                  </div>
                                
</asp:Content>
