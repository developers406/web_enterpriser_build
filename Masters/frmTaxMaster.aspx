﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" enableEventValidation="false"
CodeBehind="frmTaxMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmTaxMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    hr { 
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
} 
      .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }

</style>
<script type="text/javascript">
       $(document).ready(function () {
           $("#<%= txtActDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
           $("#<%= txtEndDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
    });

    function DisableCopyPaste(e) {
        var kCode = event.keyCode || e.charCode;
        if (kCode == 17 || kCode == 2) {
            
            return false;
        }
    }

    function pageLoad() {
        $("#<%= txtActDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
        $("#<%= txtEndDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });         
    }  
    function ValidateForm() {
        var Show = '';

        if (document.getElementById("<%=txtTaxCode.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter Tax Code');
            document.getElementById("<%=txtTaxCode.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter Description Code');
            document.getElementById("<%=txtDescription.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=TxtPerc.ClientID%>").value == "") {
           ShowPopupMessageBox('Please Enter Percentage');
            document.getElementById("<%=TxtPerc.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=TxtSymbol.ClientID%>").value == "") {
            ShowPopupMessageBox(' Please Enter Symbol');
            document.getElementById("<%=TxtSymbol.ClientID %>").focus();
            return false;
        }
       
        else if (document.getElementById("<%=txtActDate.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter ActDate');
            document.getElementById("<%=txtActDate.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=txtEndDate.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter EndDate');
            document.getElementById("<%=txtEndDate.ClientID %>").focus();
            return false;
        }
        else {
            return true;
        }
    }
    
    function Confirm_Tax_Delete() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to delete Tax data?")) {
            return;
        } else {
            return false;
        }
        document.forms[0].appendChild(confirm_value);
    }
    function Confirm_Commodity_Delete() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Do you want to delete Commodity data?")) {
            return;
        } else {
            return false;
        }
        document.forms[0].appendChild(confirm_value);
    }
    function Validate_Commodity_Form() {
        var Show = '';

        if (document.getElementById("<%=txtCommodityCode.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter Commodity Code');
            document.getElementById("<%=txtCommodityCode.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=txtCommodityName.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter Commodity Name');
            document.getElementById("<%=txtCommodityName.ClientID %>").focus();
            return false;
        }

        else if (document.getElementById("<%=txtCommoditycategory.ClientID%>").value == "") {
            ShowPopupMessageBox('Please Enter Commodity category');
            document.getElementById("<%=txtCommoditycategory.ClientID %>").focus();
            return false;
        }

        else {
            return true;
        }
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            return false;
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'TaxMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "TaxMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         <asp:UpdatePanel ID="updtPnlTax" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Tax Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="col-md-4">
                <div class="container-control">
                    <div class="container-left">
                            <div class="control-group">
                            <div style="float: left">
                                    <asp:Label ID="Label1" runat="server" Text="Tax Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtTaxCode" runat="server" MaxLength="6" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            <div class="control-group">
                               <div style="float: left">
                                    <asp:Label ID="Label4" runat="server" Text="(%)"></asp:Label><span class="mandatory">*</span>
                                </div>
                                
                                <div style="float: right">
                                    <asp:TextBox ID="TxtPerc" MaxLength="5" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label5" runat="server" Text="Symbol"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="TxtSymbol" MaxLength="3" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upactdate" UpdateMode="Conditional" runat="Server">
                            <ContentTemplate>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label7" runat="server" Text="Act Date"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtActDate" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label8" runat="server" Text="End Date"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtEndDate" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                           </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label6" runat="server" Text="Sales Allowed"></asp:Label>
                                </div>
                                <div style="float: left; margin-left: 95px">
                                    <asp:CheckBox ID="chkSalesAllowed" runat="server" />
                                </div>
                               </div>
                                </div>
                             </div>
                       
                        <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" 
                                OnClientClick="return ValidateForm()" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"
                                ><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                    </div>
                    </div>
                    
                </div>
            <div class="row">
                        <div class="col-md-8">
                        <div class="GridDetails">
                    
                            <asp:GridView ID="gvTax" runat="server"  Width="100%" AutoGenerateColumns="False" PageSize="10"
                                ShowHeaderWhenEmpty="true" 
                                CssClass="pshro_GridDgn" oncopy="return false" 
                                OnPageIndexChanging="gvTax_PageIndexChanging"
                                 DataKeyNames="TaxCode">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                
                                <Columns>
                                <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                ToolTip="Click here to Delete"  OnClientClick = " return Confirm_Tax_Delete()" OnClick="imgbtnDelete_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TaxCode" HeaderText="Tax Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="Percentage" HeaderText="Percentage"></asp:BoundField>
                                    <asp:BoundField DataField="Symbol" HeaderText="Symbol"></asp:BoundField>
                                    <asp:BoundField DataField="ActiveDate" HeaderText="Active Date"></asp:BoundField>
                                    <asp:BoundField DataField="EndDate" HeaderText="End Date"></asp:BoundField>
                                    <asp:BoundField DataField="SalesAllowed" HeaderText="SalesAllowed"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <hr />
                 <div class="main-container">
                <div class="breadcrumbs">
                    <b>Commodity Code</b>
                </div>
                <div class="col-md-4">
                <div class="container-control">
                    <div class="container-left">
                            <div class="control-group">
                            <div style="float: left">
                                    <asp:Label ID="Label9" runat="server" Text="Commodity Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtCommodityCode" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label10" runat="server" Text="Commodity Name"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtCommodityName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            <div class="control-group">
                               <div style="float: left">
                                    <asp:Label ID="Label11" runat="server" Text="Commodity category"></asp:Label><span class="mandatory">*</span>
                                </div>
                                
                                <div style="float: right">
                                    <asp:TextBox ID="txtCommoditycategory" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                </div>
                            
                                </div>
                             </div>
                       
                        <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="LinkButton1" runat="server" class="button-red" 
                                OnClientClick="return Validate_Commodity_Form()" OnClick="lnkSave_Commodity_Click"><i class="icon-play"></i>Save</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="LinkButton2" runat="server" class="button-red" OnClientClick="return clearForm()"
                                ><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                    </div>
                    </div>
                    
                </div>
            <div class="row">
                        <div class="col-md-8">
                        <div class="GridDetails">
                    
                            <asp:GridView ID="gvCommodity" runat="server" AllowSorting="true" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" 
                                CssClass="pshro_GridDgn" oncopy="return false" AllowPaging="True" PageSize="10"
                                OnPageIndexChanging="gvCommodity_PageIndexChanging"
                                 DataKeyNames="VatGroupCode">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                ToolTip="Click here to edit" OnClick="imgbtnDelete_Commodity_Click" OnClientClick = "return Confirm_Commodity_Delete()" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClick="imgbtnEdit_Commodity_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="VatGroupCode" HeaderText="Commodity Code"></asp:BoundField>
                                    <asp:BoundField DataField="VatGroupName" HeaderText="Commodity Name"></asp:BoundField>
                                    <asp:BoundField DataField="Category" HeaderText="Commodity Category"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                
            
        </ContentTemplate>
    </asp:UpdatePanel>
     <script type="text/javascript">
        require(['dist/jquery-table-fixed-header'], function ($) {
            $(function ($) {
                $('#ContentPlaceHolder1_gvTax').tableFixedHeader();
            });
        });
</script>
</asp:Content>
