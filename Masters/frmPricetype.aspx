﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPricetype.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmPricetype" %>


<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 90px;
            max-width: 90px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 90px;
            max-width: 90px;
            text-align: center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 237px;
            max-width: 237px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 280px;
            max-width: 280px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 190px;
            max-width: 190px;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function ValidateForm() {
            if ($('#<%=txtPricetypeCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Pricetype");
                return false;
            }
            if (!($('#<%=txtPricetypeCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Pricetype");
                return false;
            }
            var Show = '';

            if (document.getElementById("<%=txtPricetypeCode.ClientID%>").value == "") {
                Show = Show + 'Please Enter Pricetype Code';
                document.getElementById("<%=txtPricetypeCode.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Description';
                document.getElementById("<%=txtDescription.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

    </script>
    <script type="text/javascript">

        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Pricetype");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function DisplayDetails(row) {
            $('#<%=txtPricetypeCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtPricetypeCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtDescription.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));


            if ($("td", row).eq(5).html().replace(/&nbsp;/g, '') == "Yes") {
                $('#<%= chkAllowed.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkAllowed.ClientID %>').removeAttr("checked");
            }

        }
        function Pricetype_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Pricetype");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(4).html().replace(/&nbsp;/g, ''));
                        $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkSave.ClientID %>').css("display", "none");
              }
              $('#<%=txtPricetypeCode.ClientID %>').focus();

            $('#<%=txtPricetypeCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmPricetype.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtPricetypeCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtPricetypeCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtPricetypeCode.ClientID %>').val(strarray[0]);

                            $('#<%=txtDescription.ClientID %>').val(strarray[1]);
                            if ($.trim(strarray[2]) == "Yes") {
                                $('#<%= chkAllowed.ClientID %>').attr("checked", "checked");
                            }
                            else {
                                $('#<%= chkAllowed.ClientID %>').removeAttr("checked");
                            }
                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmPricetype.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmPricetype.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },

                    minLength: 1
                });

                    $('input[type=text]').bind('change', function () {
                        if (this.value.match(/[^a-zA-Z0-9 ]/g)) {

                            ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                            this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
                        }
                    });
                }
                function isNumberKey(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                        return false;
                    return true;
                }
                function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                    $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                                e.preventDefault();
                            }
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                        }
                    };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            $('#<%=txtPricetypeCode.ClientID %>').focus();
            return false;
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PriceType');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PriceType";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Price Type </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>

                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Price Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPricetypeCode" MaxLength="20" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Active"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 95px">
                                <asp:CheckBox ID="chkAllowed" runat="server" />
                            </div>
                        </div>





                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()">Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>



            </ContentTemplate>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>

                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                        Text='<%$ Resources:LabelCaption,btn_Search %>' OnClick="lnkSearchGrid_Click"><i class="icon-play"></i></asp:LinkButton>
                         <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>

                    <div class="gridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="right-container-top-detail">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                     <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">PriceCode
                                                    </th>
                                                    <th scope="col">Description
                                                    </th>
                                                    <th scope="col">Active
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 151px; width: 1331px; background-color: aliceblue;">
                                            <asp:GridView ID="gvPricetype" runat="server" AutoGenerateColumns="False"
                                                ShowHeaderWhenEmpty="true" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                                oncopy="return false"
                                                DataKeyNames="PriceCode">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />

                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                                ToolTip="Click here to Delete" OnClientClick=" return Pricetype_Delete(this);return false;" CommandName="Select" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                    <asp:BoundField DataField="PriceCode" HeaderText="PriceCode"></asp:BoundField>
                                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                    <asp:BoundField DataField="Active" HeaderText="Active"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ups:PaginationUserControl runat="server" ID="PricetypePaging" OnPaginationButtonClick="PricetypePaging_PaginationButtonClick" />

                        <div class="hiddencol">

                            <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                        </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>


</asp:Content>
