﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmMemberPointAdjustment.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMemberPointAdjustment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .divPopUp {
            height: auto;
            width: 589px !important;
            top: 163px;
            left: 308px;
            display: block;
            z-index: 101;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function setDisabled(value) {            
            $("#<%= lnkSave.ClientID %>").css("display", "block");        
            return false;
        }
      <%--  function enableButton() {
            $("#<%= lnkSave.ClientID %>").prop("disabled", "");
            $("#<%= lnkSave.ClientID %>").attr("value", "Click");
        }


        $("#<%= lnkSave.ClientID %>").click(function () { 
            $(this).attr("disabled", "disabled");

            var minutes = 1;
            var time = minutes * (60 * 1000);
            $("#<%= lnkSave.ClientID %>").attr("value", minutes);
            setTimeout(function () {
                enableButton();
            }, time);

        });--%>

        function ValidateForm() {
             $('#<%=lnkSave.ClientID %>').css("display", "none");
            var MinAdjust = parseFloat($('#<%=lblMinAdjustmentPoint.ClientID%>').text())
            var AdjustPoint = parseFloat($('#<%=txtAdjustmentPoint.ClientID%>').val())
            var Totalreward = parseFloat($('#<%=txtTotalReward.ClientID%>').val())
            if (MinAdjust > AdjustPoint) {
                ShowPopupMessageBox('Total Reward point Should be greater than Eligible point');
             $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            if (AdjustPoint > Totalreward) {
                ShowPopupMessageBox('Adjustment Point Should not greater than Total Reward point');
                $('#<%=lnkSave.ClientID %>').css("display", "block");
                 return false;
             }

            var Show = '';
            if (document.getElementById("<%=txtRemarks.ClientID%>").value == "") {
                Show = Show + 'Please Enter Remarks'
                document.getElementById("<%=txtRemarks.ClientID %>").focus();
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                $('#<%=lnkSave.ClientID %>').css("display", "block");
                return false;
            }
            if($('#<%=hidOtpParam.ClientID%>').val() =="Y") {
                fncInitializeOTPDialog();
                $("#divPopUp").dialog('open');
                return true;
            }
        }
        function btnPrint_ClientClick(source) {
            DisplayDetails($(source).closest("tr"));

        }
        function DisplayDetails(row) {
            $('#<%=hidvoNo.ClientID %>').val(($("td", row).eq(0).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        }
        function fncOpenPopup() {
            fncInitializeOTPDialog();
            $("#divPopUp").dialog('open');
        }
        function fncInitializeOTPDialog() {
            try {
                $("#divPopUp").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 140,
                    width: 604,
                    modal: true,
                    title: "OTP",
                    appendTo: 'form:first'
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'MemberPointAdjustment');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "MemberPointAdjustment";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Member Point Adjustment</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-mpa">
                    <div class="container-control" style="padding: 0px">
                        <div class="top-container-mpa">
                            <div class="top-container-mpa-left">
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="NRIC No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtNRICNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label3" runat="server" Text="Card No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCardNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="top-container-mpa-right">
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Total Reward"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTotalReward" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Gift Amount"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtGiftAmount" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-container-mpa">
                            <div class="gridDetails" style="overflow: auto; height: 250px">
                                <asp:GridView ID="gvAdjustPoint" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                    CssClass="pshro_GridDgn">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:BoundField DataField="AdjustmentNo" HeaderText="Adjustment No"></asp:BoundField>
                                        <asp:BoundField DataField="AdjustmentDate" HeaderText="Adjustment Date"></asp:BoundField>
                                        <asp:BoundField DataField="AdjustmentPoint" HeaderText="Adjustment Point"></asp:BoundField>
                                        <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                        <asp:BoundField DataField="CreateUser" HeaderText="Adjustment User"></asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnPrint" Text="Print" runat="server" OnClick="lnkPrint_Click" OnClientClick="btnPrint_ClientClick(this);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="bottom-container-mpa">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Adjustment User"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAdjustmentUser" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Adjustment Date"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAdjustmentDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="Adjustment Point"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 20%">
                                        <asp:TextBox ID="txtAdjustmentPoint" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                    <div class="label-right" style="width: 39%">
                                        <span style="margin-left: 5px; color: blue">Min AdjustmentPoint
                                            <asp:Label ID="lblMinAdjustmentPoint" runat="server" Text="0"></asp:Label></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkBack" runat="server" class="button-blue" Text="Back" OnClick="lnkBack_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button display_none">
                            <asp:LinkButton ID="lnkVoucherPrint" runat="server" class="button-red" Text="Voucher Print"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text="Save" OnClientClick="return ValidateForm()"
                                OnClick="lnkSave_Click"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text="Clear" OnClientClick="clearForm()"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hidvoNo" runat="server" />
        <asp:HiddenField ID="hidMobile" runat="server" />
        <asp:HiddenField ID="hidOtpParam" runat="server" />
    </div>


    <div id="divPopUp" class="display_none" style="height:169px !important">
        <div class="col-md-12" style ="margin-top:10px">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <asp:TextBox ID="txtOtp" runat="server"  onkeypress="return isNumberKey(event)" MaxLength="4" placeholder="Enter 4 digit No"></asp:TextBox>
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="button-contol control-button" style="float: right">
                        <asp:LinkButton ID="lnkReSend" runat="server" class="button-blue" Text="Resend" OnClick ="lnkResend_Click"></asp:LinkButton>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="button-contol  control-button">
                        <asp:LinkButton ID="lnkSubmit" runat="server" class="button-red" Text="Submit" OnClick ="lnkSubmit_Click"></asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
