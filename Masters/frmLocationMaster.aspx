﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmLocationMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmLocationMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 134px;
            max-width: 134px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 181px;
            max-width: 181px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            display: none;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            display: none;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            display: none;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 115px;
            max-width: 115px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            display: none;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            display: none;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            min-width: 170px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            min-width: 134px;
            max-width: 134px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          .no-close .ui-dialog-titlebar-close{
            display:none;
        }
            
    </style>
    <script type="text/javascript">
        function ValidateForm() {

            var location = $.trim($('#<%=txtLocCode.ClientID %>').val());         //21112022 musaraf
            var LocationName = $.trim($('#<%=txtLocName.ClientID %>').val());
          
             if ($('#<%=txtLocCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                 ShowPopupMessageBox("You have no permission to save existing Location");
                return false;
            }
            if (!($('#<%=txtLocCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Location");
                return false;
            }
           
            if (location == '') {
                ShowPopupMessageBox('Please Enter Location Code');
                document.getElementById("<%=txtLocCode.ClientID %>").focus();
                return false;
            }
           
            else if (LocationName == '') {
                ShowPopupMessageBox('<br />Please Enter Location Name');
                document.getElementById("<%=txtLocName.ClientID %>").focus();
                return false;
            }
            else if (document.getElementById("<%=txtSortCode.ClientID%>").value == "") {
                ShowPopupMessageBox('<br />Please Enter Sort Code');
                document.getElementById("<%=txtSortCode.ClientID %>").focus();
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
}

    </script>
    <script type="text/javascript">
        function pageLoad() {

             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                   $('#<%=lnkSave.ClientID %>').css("display", "none");
             }

            $('#<%=lblWareHouse.ClientID %>').css("display", "none");
            $('#<%=ddlAnotherDiv.ClientID %>').css("display", "none");
            <%--$('#<%=txtLocCode.ClientID %>').focus();--%>
           <%-- $('#<%=lnkSave.ClientID %>').focus();--%>
            // register jQuery extension
            //jQuery.extend(jQuery.expr[':'], {
            //    focusable: function (el, index, selector) {
            //        return $(el).is('a, button, :input, [tabindex]');
            //    }
            //});

            //$(document).on('keypress', 'input,select', function (e) {
            //    if (e.which == 13) {
            //        e.preventDefault();
            //        // Get all focusable elements on the page
            //        var $canfocus = $(':focusable');
            //        var index = $canfocus.index(document.activeElement) + 1;
            //        if (index >= $canfocus.length) index = 0;
            //        $canfocus.eq(index).focus();
            //    }
            //});
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $(document).ready(function () {
                $("input").first().focus();


            });
            $('#<%=txtLocCode.ClientID %>').focus();
            $('#<%=txtLocCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmLocationMaster.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtLocCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtLocCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtLocCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtLocName.ClientID %>').val(strarray[1]);
                            $('#<%=txtAddress1.ClientID %>').val(strarray[2]);
                            $('#<%=txtAddress2.ClientID %>').val(strarray[3]);
                            $('#<%=txtAddress3.ClientID %>').val(strarray[4]);
                            $('#<%=txtIncharge.ClientID %>').val(strarray[5]);
                            $('#<%=txtPhone.ClientID %>').val(strarray[6]);
                            $('#<%=txtFax.ClientID %>').val(strarray[7]);

                            if ($.trim(strarray[8]).replace(/&nbsp;/g, '') == "1") {

                                $('#<%= chkWareHouse.ClientID %>').attr("checked", "checked");

                                if ($.trim(strarray[8]).replace(/&nbsp;/g, '') != null) {
                                    $('#<%=lblWareHouse.ClientID %>').css("display", "block");
                                    $('#<%= ddlWareHouse.ClientID %>').val($.trim(strarray[11]).replace(/&nbsp;/g, ''));
                                    $('#<%=ddlAnotherDiv.ClientID %>').css("display", "block");
                                    $('#<%= ddlWareHouse.ClientID %>').trigger("liszt:updated");
                                }
                            }
                            else {
                                $('#<%= ddlWareHouse.ClientID %>')[0].selectedIndex = 0;
                                $('#<%= ddlWareHouse.ClientID %>').trigger("liszt:updated");
                                $('#<%= chkWareHouse.ClientID %>').removeAttr("checked");
                                $('#<%=ddlAnotherDiv.ClientID %>').css("display", "none");
                                $('#<%=ddlWareHouse.ClientID %>').css("display", "none");
                                $('#<%=lblWareHouse.ClientID %>').css("display", "none");

                            }
                            $('#<%=txtSortCode.ClientID %>').val($.trim(strarray[9]).replace(/&nbsp;/g, ''));

                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtLocName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmLocationMaster.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtLocName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $('#<%=chkWareHouse.ClientID %>').change(function () {

                if ($('#<%=chkWareHouse.ClientID %>').is(':checked')) {

                        $('#<%=lblWareHouse.ClientID %>').css("display", "block");
                    $('#<%=ddlAnotherDiv.ClientID %>').css("display", "block");
                } else {
                    $('#<%=lblWareHouse.ClientID %>').css("display", "none");
                    $('#<%=ddlAnotherDiv.ClientID %>').css("display", "none");

                }
                });

            $("[id$=txtSearch]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmLocationMaster.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                focus: function (event, i) {
                    $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                    return false;
                },

                minLength: 1
            });

           
                $(document).on('keydown', disableFunctionKeys);
                $('input[type=text]').bind('change', function () {
                    if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {

                        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                        this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
                    }
                });         

            }

            function Location_Delete(source) {
                 if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                        ShowPopupMessageBox("You have no permission to Delete this Location");
                        return false;
                    }
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "YES": function () {
                            $(this).dialog("close");
                            $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                            $("#<%= btnDelete.ClientID %>").click();
                        },
                        "NO": function () {
                            $(this).dialog("close");
                            returnfalse();
                        }
                    }
                });

            }
            function imgbtnEdit_ClientClick(source) {
                  if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                        ShowPopupMessageBox("You have no permission to Edit this Location");
                        return false;
                  }
                else
                DisplayDetails($(source).closest("tr"));

            }

            function DisplayDetails(row) {
                $('#<%= txtLocCode.ClientID %>').prop("disabled", true);
                $('#<%=txtLocCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
                $('#<%=txtLocName.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
                $('#<%=txtAddress1.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
                $('#<%=txtAddress2.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
                $('#<%=txtAddress3.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
                $('#<%=txtIncharge.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
                $('#<%=txtPhone.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
                $('#<%=txtFax.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));

                if ($("td", row).eq(10).html().replace(/&nbsp;/g, '') == "1") {

                    $('#<%= chkWareHouse.ClientID %>').attr("checked", "checked");

                    if ($("td", row).eq(10).html().replace(/&nbsp;/g, '') != null) {
                        $('#<%=lblWareHouse.ClientID %>').css("display", "block");
                            $('#<%= ddlWareHouse.ClientID %>').val($("td", row).eq(13).html().replace(/&nbsp;/g, ''));
                            $('#<%=ddlAnotherDiv.ClientID %>').css("display", "block");
                            $('#<%= ddlWareHouse.ClientID %>').trigger("liszt:updated");
                        }
                    }
                    else {
                        $('#<%= ddlWareHouse.ClientID %>')[0].selectedIndex = 0;
                    $('#<%= ddlWareHouse.ClientID %>').trigger("liszt:updated");
                    $('#<%= chkWareHouse.ClientID %>').removeAttr("checked");
                    $('#<%=ddlAnotherDiv.ClientID %>').css("display", "none");
                    $('#<%=ddlWareHouse.ClientID %>').css("display", "none");
                    $('#<%=lblWareHouse.ClientID %>').css("display", "none");

                }
                $('#<%=txtSortCode.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));


            }
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            function disableFunctionKeys(e) {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) { 
                    if($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                      <%--   else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                }
            };

       
      
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            $('#<%= ddlWareHouse.ClientID %>').val('');
            $('#<%=ddlWareHouse.ClientID %>').trigger("liszt:updated");
            //            $("#lblLastUpdateUser span").text('AA');
            //            $("#lblLastUpdateDate span").text('01/01/2008');
            return false;
        }
    </script>

        <script type="text/javascript">
            var d1;
            var d2;
            var Opendate;
            var dur;
            function fncGetUrl() {
                fncSaveHelpVideoDetail('', '', 'LocationMaster');
            }
            function fncOpenvideo() {

                document.getElementById("ifHelpVideo").src = HelpVideoUrl;

                var Mode = "LocationMaster";
                var d = new Date($.now());
                Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                d1 = new Date($.now()).getTime();



                $("#dialog-Open").dialog({
                    autoOpen: true,
                    resizable: false,
                    height: "auto",
                    width: 1093,
                    modal: true,
                    dialogClass: "no-close",
                    buttons: {
                        Close: function () {
                            $(this).dialog("destroy");
                            var d = new Date($.now());
                            var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                            d2 = new Date($.now()).getTime();
                            var Diff = Math.floor((d2 - d1) / 1000);
                            //alert(Diff);
                            if (Diff >= 60) {
                                fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                            }

                        }
                    }
                });
            }


        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden;">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Location Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group">
            <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
                <ContentTemplate>

                    <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                    </div>
                    <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                    <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                    <div class="container-control">
                        <div class="container-left">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Location Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLocCode" MaxLength="5" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Location Name"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtLocName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Address"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAddress1" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-right" style="margin-left: 40%">
                                    <asp:TextBox ID="txtAddress2" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-right" style="margin-left: 40%">
                                    <asp:TextBox ID="txtAddress3" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <%--<br />--%>
                            <%--<div class="bottom-right-container-header">
                                Last Updated
                            </div>
                            <div class="control-group-single-res">
                                    
                                        <div class="label-left" >
                                            <asp:Label ID="Label48" runat="server" Text="Date : "></asp:Label>
                                        </div>
                                        <div class="label-right" id="lblLastUpdateDate">
                                            <span  runat="server" style="color: Blue">01/01/2008</span>
                                        </div>
                                    </div>
                            <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label49" runat="server" Text="User : "></asp:Label>
                                        </div>
                                        <div class="label-right" id="lblLastUpdateUser">
                                            <span  runat="server" style="color: Blue">AA</span>
                                        </div>
                                    </div>--%>
                        </div>
                        <div class="container-right">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="Incharge"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtIncharge" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text="Phone"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPhone" onkeypress="return isNumberKey(event)" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="Fax"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtFax" onkeypress="return isNumberKey(event)" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label9" runat="server" Text="SortCode"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtSortCode" onkeypress="return isNumberKey(event)" MaxLength="2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Warehouse"></asp:Label>
                                </div>
                                <div class="label-left">
                                    <asp:CheckBox ID="chkWareHouse" runat="server" />
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblWareHouse" runat="server" Text="Warehouse"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <div id="ddlAnotherDiv" runat="server">
                                        <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control-res" Width="120px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="gridDetails">
            <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>

                        </asp:Panel>
                    </div>
                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Location Code
                                                    </th>
                                                    <th scope="col">Location Name
                                                    </th>
                                                    <th scope="col">Address1
                                                    </th>
                                                    <th scope="col">Address2
                                                    </th>
                                                    <th scope="col">Address3
                                                    </th>
                                                    <th scope="col">PersonIncharge
                                                    </th>
                                                    <th scope="col">TelePhone
                                                    </th>
                                                    <th scope="col">FaxNo
                                                    </th>
                                                    <th scope="col">Ware House
                                                    </th>
                                                    <th scope="col">Sort Code
                                                    </th>
                                                    <th scope="col">Last EOD Date
                                                    </th>
                                                    <th scope="col">Ware House Code
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 231px; width: 1317px; background-color: aliceblue;">
                                            <asp:GridView ID="gvLocation" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                oncopy="return false" AllowPaging="True" PageSize="8" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                                OnPageIndexChanging="gvLocation_PageIndexChanging" DataKeyNames="LocationCode">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Location_Delete(this);  return false;"
                                                                CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                    <asp:BoundField DataField="LocationName" HeaderText="Location Name"></asp:BoundField>
                                                    <asp:BoundField DataField="Address1" HeaderText="Address 1" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="Address2" HeaderText="Address 2" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="Address3" HeaderText="Address 3" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="PersonIncharge" HeaderText="Person In Charge"></asp:BoundField>
                                                    <asp:BoundField DataField="TelePhone" HeaderText="Tele Phone" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="FaxNo" HeaderText="FaxNo" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="WareHouse" HeaderText="Ware House"></asp:BoundField>
                                                    <asp:BoundField DataField="SortCode" HeaderText="Sort Code"></asp:BoundField>
                                                    <asp:BoundField DataField="EODDate" HeaderText="Last EOD Date"></asp:BoundField>
                                                    <asp:BoundField DataField="WarehouseCode" HeaderText="Ware House Code"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>

                    <div class="hiddencol">

                        <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
           <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />

         <asp:UpdateProgress ID="upPaymentProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
