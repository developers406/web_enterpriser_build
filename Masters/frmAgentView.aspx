﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAgentView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmAgentView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          
           .no-close .ui-dialog-titlebar-close{
            display:none;

           }
        

     </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AgentView');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AgentView";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Master</a><i class="fa fa-angle-right"></i></li>
                 <li><a style="text-decoration:none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                 <li><a style="text-decoration:none;">Agent Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Agent</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="gridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px"
                                OnClick="lnkSearchGrid_Click" OnClientClick="return Validate()">Search</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" class="button-blue" Width="100px" PostBackUrl="~/Masters/frmAgent.aspx">New</asp:LinkButton>
                        </asp:Panel>
                    </div>
                    <asp:GridView ID="grdVendorMaster" runat="server" Width="100%" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" AllowPaging="True"
                        PageSize="18" OnPageIndexChanging="grdVendorMaster_PageIndexChanging">   <%--DataKeyNames="VendorCode--%>
                        <EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                        <PagerStyle CssClass="pshro_text" />
                        <Columns>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Code" HeaderText="Agent Code"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Agent Name"></asp:BoundField>
                            <asp:BoundField DataField="Address" HeaderText="Address" />
                            <asp:BoundField DataField="Phone" HeaderText="Phone"></asp:BoundField>
                            <asp:BoundField DataField="MobileNo" HeaderText="Mobile No"></asp:BoundField>
                            <asp:BoundField DataField="GSTNo" HeaderText="GST No"></asp:BoundField>
                            <asp:BoundField DataField="Status" HeaderText="Active"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>

