﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLuckyDrawStockUpdate.aspx.cs" Inherits="EnterpriserWebFinal.Stock.frmLuckyDrawStockUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .gid_Itemsearch td:nth-child(1), .gid_Itemsearch th:nth-child(1) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(2), .gid_Itemsearch th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(3), .gid_Itemsearch th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .gid_Itemsearch td:nth-child(4), .gid_Itemsearch th:nth-child(4) {
            min-width: 460px;
            max-width: 460px;
        }

        .gid_Itemsearch td:nth-child(5), .gid_Itemsearch th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(6), .gid_Itemsearch th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(7), .gid_Itemsearch th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }
    </style>

    <script type="text/javascript">
        function fncScanInventoryCode(evt) {
            try {
                //
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                InventoryCode = $('#<%=txtInventory.ClientID %>').val();




                if (charCode == 13 || charCode == 9) {

                    fncGetInventoryCode();
                }

                return true;
            }
            catch (err) {

                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtInventory.ClientID %>').val($.trim(Code));
                    $('#<%=txtDesc.ClientID %>').val($.trim(Description));
                    fncFetch();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncFetch() {
            $('#fullgrid').dialog({
                //autoOpen: true,

                height: "auto",
                width: "auto",

                buttons: {
                    Close: function () {

                        $(this).dialog("close");

                    }

                }
            });
            fncget();
        }
        function fncget() {

            try {

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmLuckyDrawStockUpdate.aspx/fncItemSearchForGrid")%>',
                    data: "{ 'Code': '" + $("#<%=txtDesc.ClientID%>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncItemBindtoTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }

        }
        function fncItemBindtoTable(msg) {
            try {
                var objSearchData, tblSearchData, Rowno = 0;
                objSearchData = jQuery.parseJSON(msg.d);

                tblSearchData = $("#tblItemSearch tbody");
                tblSearchData.children().remove();

                if (objSearchData.length > 0) {
                    for (var i = 0; i < objSearchData.length; i++) {

                        row = "<tr tabindex = '" + i + "' onfocus = 'return fncMouseSearchesFocus(this);' onkeydown = 'return fncSearchTableKeyDowns(event, this);'>"
                            + "<td id='tdRowNo'>  " + $.trim(objSearchData[i]["SNo"]) + "</td>" +
                            "<td id='tdItemCode' > " + $.trim(objSearchData[i]["InventoryCOde"]) + "</td>" +
                            "<td id='tdBatchNo' > " + $.trim(objSearchData[i]["BatchNo"]) + "</td>" +
                            "<td id='tdDes'>" + $.trim(objSearchData[i]["Description"]) + "</td>" +
                            "<td id='tdBatchQty' style ='text-align:right !important'>" + $.trim(objSearchData[i]["BatchQty"]) + "</td>" +
                            "<td id='tdPrice' style ='text-align:right !important'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                            "<td id='tdRemarks' style ='display:none'>" + $.trim(objSearchData[i]["Remarks"]) + "</td>" +
                            "<td id='tdVendorCode' style ='display:none'>" + $.trim(objSearchData[i]["VendorCode"]) + "</td>" +
                            "<td id='tdCashDiscount'style ='display:none'>" + $.trim(objSearchData[i]["CashDiscount"]) + "</td>" +
                            "<td id='tdVendorName'style ='display:none'>" + $.trim(objSearchData[i]["VendorName"]) + "</td>" +
                            //"<td id='tdPrice'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                            "<td id='tdMrp' style ='width:9%;text-align:right !important;'>" + $.trim(objSearchData[i]["MRP"]) + "</td></tr>";
                        tblSearchData.append(row);
                    }




                    //var scroll = tblSearchData[0].scrollHeight;
                    //tblSearchData.scrollTop(scroll);

                    tblSearchData.children().dblclick(fncSearchRowDoubleClick);
                    tblSearchData[0].setAttribute("style", "cursor:pointer");

                    tblSearchData.children().on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });

                    tblSearchData.children().on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });


                    $("tr[tabindex=0]").focus();

                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function fncMouseSearchesFocus(source) {
            try {
                var rowobj = $(source);
                rowobj.css("background-color", "#ADD8E6");
                rowobj.siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err);
            }
        }

        function fncSearchTableKeyDowns(evt, source) {
            try {
                var rowobj = $(source);
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //Enter Key
                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }

                //Down Key
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "#ADD8E6");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();
                        $('#<%=txtInventory.ClientID %>').val($(NextRowobj).find("td").eq(3).html().trim());
                    }
                    return false;
                }
                //Up Key
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "#ADD8E6");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();
                        $('#<%=txtInventory.ClientID %>').val($(prevrowobj).find("td").eq(3).html().trim());
                    }
                    else {
                        prevrowobj.css("background-color", "");
                        prevrowobj.siblings().css("background-color", "");
                        $("#search").focus();
                    }
                    return false;
                }
                return false;
            }
            catch (err) {
                fncToastError(err);
            }
        }

        function fncBatchKeyDown(rowobj, evt) {
            //  alert('test');
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblItemSearch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblItemSearch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblItemSearch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblItemSearch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblItemSearch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        var val = "";
        function fncSearchRowDoubleClick() {
            try {
                Itemcode = $.trim($(this).find("td").eq(1).text());



                $('#<%=txtBatch.ClientID %>').val($.trim($(this).find("td").eq(2).text()));


                var tblSearchData = $("#tblItemSearch tbody");
                tblSearchData.children().remove();
                $('#fullgrid').dialog("close");



            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncupdate() {
            try {
                var obj = {};
                obj.inv = $('#<%=txtInvoice.ClientID %>').val();
                obj.code = $('#<%=txtInventory.ClientID %>').val();
                obj.btc = $('#<%=txtBatch.ClientID %>').val();
                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: "frmLuckyDrawStockUpdate.aspx/GetInventorycode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        //fncAssignValuestoTextBox(msg);
                        ShowPopupMessageBox(msg);
                        fncclear();
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {

                ShowPopupMessageBox(err.message);
            }
        }
        function fncclear() {
            $('#<%=txtInvoice.ClientID %>').val('');
            $('#<%=txtInventory.ClientID %>').val('');
            $('#<%=txtBatch.ClientID %>').val('');
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Stock</a><i class="fa fa-angle-right"></i></li>

                <li class="active-page">LuckyDrawStockUpdate </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-small">
            <div class="container-control">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblInvoice" runat="server" Text="InvoiceNo  " CssClass="label-left"></asp:Label>
                            <asp:TextBox ID="txtInvoice" runat="server" CssClass="label-right"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblcode" runat="server" Text="ItemCode  " CssClass="label-left"></asp:Label>
                            <asp:TextBox ID="txtInventory" runat="server" CssClass="label-right" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');"></asp:TextBox>
                            <asp:TextBox ID="txtDesc" runat="server" Style="display: none"></asp:TextBox>
                        </td>

                    </tr>

                </table>
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblBatch" runat="server" Text="BatchNo  " CssClass="label-left"></asp:Label>
                            <asp:TextBox ID="txtBatch" runat="server" CssClass="label-right"></asp:TextBox>
                        </td>
                    </tr>
                </table>

                <div class="button-contol" style="margin-left: 175px">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkfetch" runat="server" class="button-red"
                            OnClientClick="fncupdate();return false;"><i class="icon-play"></i>Update</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div id="fullgrid" style="display: none" title="Batch Detail">
            <div id="divGrid">
                <div id="divItemSearch " class="tbl_left" style="margin-left: 65px;">

                    <div class="Payment_fixed_headers gid_Itemsearch">
                        <table id="tblItemSearch" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Batch No 
                                    </th>
                                    <th scope="col">Item Description
                                    </th>
                                    <th scope="col">Qty
                                    </th>
                                    <th scope="col">Selling Price
                                    </th>
                                    <th scope="col">MRP
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="dialog-Transferout">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
