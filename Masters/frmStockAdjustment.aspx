﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmStockAdjustment.aspx.cs" EnableEventValidation="false"
    Inherits="EnterpriserWebFinal.Masters.frmStockAdjustment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }
        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 181px;
            max-width: 181px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 300px;
            max-width: 300px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 250px;
            max-width: 250px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 222px;
            max-width: 222px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function ValidateForm() {
            if ($('#<%=txtStockAdjustTypeCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing StockAdjustment");
                return false;
            }
            if (!($('#<%=txtStockAdjustTypeCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New StockAdjustment");
                return false;
            }
            var Show = '';
            var StockAdjustTypeCode = $.trim($('#<%=txtStockAdjustTypeCode.ClientID %>').val());         //22112022 musaraf
            var StockAdjustTypeName = $.trim($('#<%=txtStockAdjustTypeName.ClientID %>').val());

            if (StockAdjustTypeCode == "") {
                Show = Show + 'Please Enter Stock Adjust Type Code ';
                document.getElementById("<%=txtStockAdjustTypeCode.ClientID %>").focus();
            }

            if (StockAdjustTypeName == "") {
                Show = Show + '<br />Please Enter Stock Adjust Type Name';
                document.getElementById("<%=txtStockAdjustTypeName.ClientID %>").focus();
            }

            if (document.getElementById("<%=RadAdd.ClientID%>").value == document.getElementById("<%=RadDeduct.ClientID%>").value) {
                Show = Show + '<br />Please Select Any One Stock Type';
                document.getElementById("<%=RadAdd.ClientID %>").focus();
                document.getElementById("<%=RadDeduct.ClientID %>").focus();
            }



            if (document.getElementById("<%=txtSort.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Sort';
                document.getElementById("<%=txtSort.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function Confirm_StockAdjustTypex_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this StockAdjustment");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(4).html().replace(/&nbsp;/g, ''));
                        $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false();
                    }
                }
            });

        }


    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $('#<%=RadDeduct.ClientID %>').prop("checked", true);
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $(document).ready(function () {
                $("input").first().focus();

                $('input').bind("keydown", function (e) {
                    var n = $("input").length;
                    if (e.which == 13) { //Enter key
                        e.preventDefault(); //to skip default behavior of the enter key
                        var nextIndex = $('input').index(this) + 1;
                        if (nextIndex < n)
                            $('input')[nextIndex].focus();
                        else {
                            $('input')[nextIndex - 1].blur();
                            // $('#btnSubmit').click();
                        }
                    }
                });

                //$('#btnSubmit').click(function () {
                // ShowPopupMessageBox('Form Submitted');
                //  });
            });
            $('#<%=txtStockAdjustTypeCode.ClientID %>').focus();
             $('#<%=txtStockAdjustTypeCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmStockAdjustment.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtStockAdjustTypeCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtStockAdjustTypeCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtStockAdjustTypeCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtStockAdjustTypeName.ClientID %>').val(strarray[1]);
                            if ($.trim(strarray[2]) == "1") {
                                $('#<%=RadAdd.ClientID %>').prop("checked", true);
                            }
                            else {
                                $('#<%=RadDeduct.ClientID %>').prop("checked", true);
                            }
                            $('#<%=txtSort.ClientID %>').val(strarray[3]);
                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtStockAdjustTypeName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmStockAdjustment.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtStockAdjustTypeName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $('input[type=text]').bind('change', function () {
                    if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {

                        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                        this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
                    }
                });

            }
            function imgbtnEdit_ClientClick(source) {
                if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                    ShowPopupMessageBox("You have no permission to Edit this StockAdjustment");
                    return false;
                }
                DisplayDetails($(source).closest("tr"));

            }

            function DisplayDetails(row) {
                $('#<%=txtStockAdjustTypeCode.ClientID %>').prop("disabled", true);
                $('#<%=txtStockAdjustTypeCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
                $('#<%=txtStockAdjustTypeName.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
                if ($("td", row).eq(5).html().replace(/&nbsp;/g, '') == "1") {

                    $('#<%=RadAdd.ClientID %>').prop("checked", true);

                 }
                 else {
                     $('#<%=RadDeduct.ClientID %>').prop("checked", true);
                 }

                 $('#<%=txtSort.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            }
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
            function disableFunctionKeys(e) {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                             $('#<%= lnkSave.ClientID %>').click();
                         e.preventDefault();
                     }
                     else if (e.keyCode == 117) {
                         $("input").removeAttr('disabled');
                         $('#<%= lnkClear.ClientID %>').click();
                              e.preventDefault();
                          }
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                 }
             };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'StockAdjustment');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "StockAdjustment";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Stock Adjustment</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group">
            <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                    </div>
                    <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                    <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                    <div class="container-control">
                        <div class="container-left">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label1" runat="server" Text="Adj Type Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtStockAdjustTypeCode" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label2" runat="server" Text="Adj Type Name"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtStockAdjustTypeName" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label3" runat="server" Text="Stock"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:RadioButton ID="RadAdd" runat="server" Text="Add" GroupName="AddDeduct" />
                                    <asp:RadioButton ID="RadDeduct" runat="server" Checked="true" Text="Deduct" GroupName="AddDeduct" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label4" runat="server" Text="Sort"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtSort" runat="server" MaxLength="3" onkeypress="return isNumberKey(event)" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click" OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>

            </asp:UpdatePanel>

        </div>

        <div class="GridDetails">
            <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <asp:label Id="lblcount" runat="server" Text="Total StockAdjustment :" style="font-weight: 900; float:left;"></asp:label>
                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">Adj.Code
                                                    </th>
                                                    <th scope="col">Adj.Name
                                                    </th>
                                                    <th scope="col">Add/Deduct
                                                    </th>
                                                    <th scope="col">Sort Code
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 281px; width: 1314px; background-color: aliceblue;">
                                            <asp:GridView ID="gvStockAdjustment" runat="server" AllowSorting="true" AutoGenerateColumns="False" ShowHeader="false"
                                                ShowHeaderWhenEmpty="true" AllowPaging="True" PageSize="10" CssClass="pshro_GridDgn grdLoad"
                                                OnPageIndexChanging="gvStockAdjustment_PageIndexChanging" DataKeyNames="stockAdjustmenttypeCode">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSave" runat="server" OnClientClick=" return Confirm_StockAdjustTypex_Delete(this); return false;">
                                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/No.png" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                     <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                    <asp:BoundField DataField="stockAdjustmenttypeCode" HeaderText="Adj.Code"></asp:BoundField>
                                                    <asp:BoundField DataField="stockAdjustmenttypeName" HeaderText="Adj.Name"></asp:BoundField>
                                                    <asp:BoundField DataField="AddDeduct" HeaderText="Add/Deduct"></asp:BoundField>
                                                    <asp:BoundField DataField="sortcode" HeaderText="Sort Code"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hiddencol">
                        <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
                        <asp:HiddenField ID="hidSavebtn" runat="server" />
                        <asp:HiddenField ID="hidDeletebtn" runat="server" />
                        <asp:HiddenField ID="hidEditbtn" runat="server" />
                        <asp:HiddenField ID="hidViewbtn" runat="server" />

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
