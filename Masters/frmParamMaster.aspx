﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmParamMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmParamMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            width: 10%;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            width: 8%;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            width: 20%;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            width: 32%;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            width: 10%;
            text-align: center !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            width: 10%;
            text-align: left !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            width: 10%;
            text-align: left !important;
        } 

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <script type="text/javascript"> 

        function ValidateForm() {  
            $('#<%=lnkSave.ClientID %>').css('display', 'none');
            if ($('#<%=txtParamCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                $('#<%=lnkSave.ClientID %>').css('display', 'block');
                ShowPopupMessageBox("You have no permission to save existing ParamCode");
                return false;
            }
            if (!($('#<%=txtParamCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                $('#<%=lnkSave.ClientID %>').css('display', 'block');
                ShowPopupMessageBox("You have no permission to save New ParamCode");
                return false;
            }
            var Show = '';

            if (document.getElementById("<%=txtParamCode.ClientID%>").value == 0) {
                Show = Show + 'Please Enter Param Code';
            }

            if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Description';
            }
            if (document.getElementById("<%=txtValue.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Value';
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                $('#<%=lnkSave.ClientID %>').css('display', 'block');
                return false;
            } 
            else {
                //$("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function imgbtnEdit_ClientClick(source) {

            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Floor");
                return false;
            }
            DisplayDetails($(source).closest("tr"));
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function DisplayDetails(row) {
            $('#<%=txtParamCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
            $('#<%=txtDescription.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtValue.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtCreatedBy.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));

            if ($("td", row).eq(4).html().replace(/&nbsp;/g, '') == "Y" || $("td", row).eq(4).html().replace(/&nbsp;/g, '') == "N") {
                $('#<%=chkValue.ClientID %>').css('display', 'block');
                $('#<%=txtValue.ClientID %>').css('display', 'none');
                if ($("td", row).eq(4).html().replace(/&nbsp;/g, '') == "Y")
                    $('#<%=chkValue.ClientID %>').attr('checked', true);
                else
                    $('#<%=chkValue.ClientID %>').attr('checked', false);
            }
            else {
                $('#<%=chkValue.ClientID %>').css('display', 'none');
                $('#<%=txtValue.ClientID %>').css('display', 'block');
            }
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
        }

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

    </script>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'ParamCode');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "ParamCode";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();

            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);

                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }
                    }
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">ParamCode Master </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upParam" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:HiddenField ID="hdfdstatus" runat="server" />
                                <asp:Label ID="Label1" runat="server" Text="Param Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtParamCode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Value"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkValue" runat="server" />
                                <asp:TextBox ID="txtValue" MaxLength="50" Style="display: none;" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Created By"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCreatedBy" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updGrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Param Code to Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="150px" Style="visibility: hidden"
                        OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>
                            <asp:Label ID="lblcount" runat="server" Style="font-weight: 900;"></asp:Label>
                        </asp:Panel>
                    </div>
                    <div class="row">
                        <div class="grdLoad">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header" style="width:1338px">
                                <thead>
                                    <tr>
                                        <th scope="col">Edit
                                        </th>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">ParamCode
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                        <th scope="col">Value
                                        </th>
                                        <th scope="col">Created By
                                        </th>
                                        <th scope="col">Modified By
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 250px; width: 100%; background-color: aliceblue;">
                                <asp:GridView ID="grdParam" runat="server" AllowSorting="true"
                                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" oncopy="return false"
                                     PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad" >
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label5" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                    ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                        <asp:BoundField DataField="ParamCode" HeaderText="ParamCode"></asp:BoundField>
                                        <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                        <asp:BoundField DataField="Value" HeaderText="value"></asp:BoundField>
                                        <asp:BoundField DataField="CreatedBy" HeaderText="Created By"></asp:BoundField>
                                        <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ups:PaginationUserControl runat="server" ID="ParamMaster" OnPaginationButtonClick="Param_PaginationButtonClick" />
        <div class="hiddencol">

            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />

        </div>
    </div>
</asp:Content>
