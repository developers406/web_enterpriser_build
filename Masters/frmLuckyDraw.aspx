﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLuckyDraw.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmLuckyDraw" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          .no-close .ui-dialog-titlebar-close{
            display:none;
        }
     </style>

<script type="text/javascript">


    $(document).ready(function () {
        $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
        $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
    });

    function ValidateForm() {
        var Show = '';



        if (document.getElementById("<%=txtBillAmount.ClientID%>").value == "") {
            Show = Show + 'Please Enter Bill Amount';
            document.getElementById("<%=txtBillAmount.ClientID %>").focus();
        }

        


        if (Show != '') {
            ShowPopupMessageBox(Show);
            return false;
        }

        else {
            $("input").removeAttr('disabled');
            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
            return true;
        }
    }
    
    

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
    </script>
    <script type="text/javascript">
        function pageLoad() {
             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkSave.ClientID %>').css("display", "none");
              }
            $("select").chosen({ width: '100%' });
            $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });       
            clearForm();
        }
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");


        }
           function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            $('#<%= lnkSave.ClientID %>').click();
                             e.preventDefault();	
                         }
                        
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>
        
       
}
};

$(document).ready(function () {
    $(document).on('keydown', disableFunctionKeys);
});
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'LuckyDraw');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "LuckyDraw";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="main-container" style="overflow:hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Lucky Drawer Settings</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
       
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                
                    <div class="container-group-small">
                        <div class="container-control">
                            
                            
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server"  Text="From Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                 <asp:TextBox ID="txtfromdate" runat="server" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txttodate" runat="server" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" CssClass="form-control-res"></asp:TextBox>
                                  </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Bill Amount"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                 <asp:TextBox ID="txtBillAmount" onkeypress="return isNumberKey(event)" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label3" runat="server" Text="POS"></asp:Label>
                                </div>
                                <div class="label-right" >
                                    <asp:CheckBox ID="chkPOSAllowed" runat="server" />
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Msg 1"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtMgs1" runat="server" MaxLength="40" CssClass="form-control-res"></asp:TextBox>
                                  </div>
                            </div>

                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Msg 2"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="TxtMgs2"  runat="server" MaxLength="40" CssClass="form-control-res"></asp:TextBox>
                                  </div>
                            </div>
                            
                        </div>
                        <div class="button-contol" style="margin-left: 175px">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                    OnClientClick="return ValidateForm()" ><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server"  class="button-red" OnClientClick="clearForm(); return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                           
                        </div>
                    </div>
               
            </ContentTemplate>
        </asp:UpdatePanel> 
    </div> 
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
