﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransport.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmTransport1" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type ="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupTransport').dialog('close');
                window.parent.$('#popupTransport').remove();

            }
        });

        function ValidateForm() {
            var Show = '';
            var TransportName = $.trim($('#<%=txtTransportName.ClientID %>').val());   //23112022 musaraf

            if (document.getElementById("<%=txtTransportCode.ClientID%>").value == "") {
                fncToastError('Please Enter Transport Code');
                document.getElementById("<%=txtTransportCode.ClientID %>").focus();
                return false;
            }

            else if (!validateEmail(document.getElementById("<%=txtEmail.ClientID%>").value)) {
                ShowPopupMessageBoxandOpentoObject('Please Enter Valid Email');     
                popUpObjectForSetFocusandOpen = $('#<%=txtEmail.ClientID %>');                    //musaraf 17112022
                return false;
            }  


            else if (TransportName == "") {
                fncToastError('Please Enter Transport Name');
                document.getElementById("<%=txtTransportName.ClientID %>").focus();
                return false;
            }
           
                
            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }


            

        }

        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test($email);                            //musaraf 17112022
        }

    function Transport_Delete(source) {

        $("#dialog-confirm").dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "YES": function () {
                    $(this).dialog("close");
                    $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                    $("#<%= btnDelete.ClientID %>").click();
                },
                "NO": function () {
                    $(this).dialog("close");
                    return false();
                }
            }
        });

    }
    function imgbtnEdit_ClientClick(source) {

        DisplayDetails($(source).closest("tr"));

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function DisplayDetails(row) {  
        $('#<%=txtTransportCode.ClientID %>').prop("disabled", true);
        $('#<%=hdnValue2.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
        $('#<%=txtTransportCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
        $('#<%=txtTransportName.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, '')); 
        $('#<%=txtContPerson.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
        $('#<%=txtAddress1.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
        $('#<%=txtAddress2.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
        $('#<%=txtPhoneNo.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
        $('#<%=txtMobNo.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));
        $('#<%=txtFax.ClientID %>').val($("td", row).eq(10).html().replace(/&nbsp;/g, ''));
        $('#<%=txtEmail.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));
        $('#<%=txtUrl.ClientID %>').val($("td", row).eq(12).html().replace(/&nbsp;/g, ''));



    }
    </script>
    <script type="text/javascript">
        function pageLoad() {

            $('#<%=txtTransportCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmTransport.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtTransportCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');
                        console.log(strarray);
                        if (temp != "Null") {
                            $('#<%=txtTransportCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtTransportCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtTransportName.ClientID %>').val(strarray[1]);
                            $('#<%=txtContPerson.ClientID %>').val(strarray[2]);
                            $('#<%=txtAddress1.ClientID %>').val(strarray[3]);
                            $('#<%=txtAddress2.ClientID %>').val(strarray[4]);
                            $('#<%=txtPhoneNo.ClientID %>').val(strarray[5]);
                            $('#<%=txtMobNo.ClientID %>').val(strarray[6]);
                            $('#<%=txtFax.ClientID %>').val(strarray[7]);
                            $('#<%=txtEmail.ClientID %>').val(strarray[8]);
                            $('#<%=txtUrl.ClientID %>').val(strarray[9]);
                            console.log(strarray[10]);
                            if ($.trim(strarray[10]).substring(0, 10).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkActive.ClientID %>').attr("checked", "checked");
                            }
                            else {
                                $('#<%= chkActive.ClientID %>').removeAttr("checked");
                            }
                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });


            $("[id$=txtTransportName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmTransport.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[0]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                <%--focus: function (event, i) {

                    $('#<%=txtTransportName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtTransportName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $('#<%=txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmTransport.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                focus: function (event, i) {
                    $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                         event.preventDefault();
                     },
                     select: function (e, i) {
                         $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                            return false;
                        },

                        minLength: 1
                 });

            fncMbLength();

        }
                    function clearForm() {
                        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                        $(':checkbox, :radio').prop('checked', false);
                        fncToastInformation("Cleared Successfully");
                        $('#<%=txtTransportCode.ClientID %>').focus();

                    }
                    function disableFunctionKeys(e) {
                        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                            if (e.keyCode == 115) {
                                $('#<%= lnkSave.ClientID %>').click();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        $("input").removeAttr('disabled');
                        $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };

        $(document).ready(function () {
            $('#<%=txtTransportName.ClientID %>').focus();
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'Transport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "Transport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }



          function fncMbLength() {
              try {
                  $("#<%= txtMobNo.ClientID %>").focusout(function () {
                      if ($("#<%= txtMobNo.ClientID %>").val().length <= 9) {          //musaraf 17112022
                        ShowPopupMessageBox("Please Enter Atleast 10 Number");
                        $("#<%= txtMobNo.ClientID %>").val('');
                 
                    }
                });
              }
              catch (err) {
                  ShowPopupMessageBox(err.message);
              }
          }

          $(document).ready(function () {
              fncMbLength();
          });


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transport</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
           <div class="main-container EnableScroll">
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue2" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Transport Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtTransportCode" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Transport Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtTransportName" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Cont Person"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtContPerson" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="Address 1"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAddress1" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Address 2"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAddress2" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Phone No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPhoneNo" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Mob No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMobNo" runat="server" MaxLength="10" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Fax"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFax" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Email"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Url"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtUrl" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="Active Status"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Update(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter TransportName To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                       <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                            </asp:Panel>
                    </div>

                    <asp:GridView ID="gvTransport" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                        CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="TransportCode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick="return Transport_Delete(this);  return false;"
                                        CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                        ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                            <asp:BoundField DataField="TransportCode" HeaderText="Transport Code"></asp:BoundField>
                            <asp:BoundField DataField="TransportName" HeaderText="Transport Name"></asp:BoundField>
                            <asp:BoundField DataField="ContactPerson" HeaderText="Transport Name" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Address1" HeaderText="Address1" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Address2" HeaderText="Address1" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Phone" HeaderText="Phone" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Mobile" HeaderText="Mobile" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Fax" HeaderText="Fax" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="EMail" HeaderText="EMail" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Url" HeaderText="Url" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Active" HeaderText="Status"></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <ups:PaginationUserControl runat="server" ID="TransportPaging" OnPaginationButtonClick="Transport_PaginationButtonClick" />
                </div>
                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        </div>
</asp:Content>
