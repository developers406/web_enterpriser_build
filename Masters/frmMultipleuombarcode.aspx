﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMultipleuombarcode.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMultipleuombarcode" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <script type="text/javascript">


        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtInvertorycode.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Invertory Code';
                document.getElementById("<%=txtInvertorycode.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlSize.ClientID%>").value == 0) {
                Show = Show + 'Please select Any One UOM Code';
                document.getElementById("<%=ddlSize.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtBarcode.ClientID%>").value == "") {
                Show = Show + 'Please Enter Bar Code';
                document.getElementById("<%=txtBarcode.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function Multipleuombarcode_Delete(source) {


            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                        $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });


        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' });
            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmMultipleuombarcode.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1],
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                <%--focus: function (event, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $('#<%=txtInvertorycode.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmMultipleuombarcode.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    val: item.split('/')[1],
                                    label: item.split('/')[2],
                                    valName: item.split('/')[0]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                focus: function (event, i) {
                    $('#<%=txtInvertorycode.ClientID %>').val($.trim(i.item.valName));

                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtInvertorycode.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtInvertorycode.ClientID %>').prop("disabled", true);
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtDescription.ClientID %>').prop("disabled", true);
                    GetExistingValue();

                    return false;
                },

                    minLength: 1
                });

        }

        function GetExistingValue() {
            $.ajax({
                url: "frmMultipleuombarcode.aspx/GetExistingCode",
                data: "{ 'Code': '" + $("#<%=txtInvertorycode.ClientID%>").val() + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var temp = data.d;
                    var strarray = temp.split(',');

                    if (temp != "Null") {


                        $('#<%=ddlSize.ClientID %>').val($.trim(strarray[0]));
                            $('#<%=ddlSize.ClientID %>').trigger("liszt:updated");
                            $('#<%=txtBarcode.ClientID %>').val($.trim(strarray[1]));
                        }

                    },
                error: function (response) {
                    ShowPopupMessageBox(response.responseText);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.responseText);
                }
            });
            }
            function clearForm() {
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                $(':checkbox, :radio').prop('checked', false);

                $('#<%=ddlSize.ClientID %>').val('');
                 $('#<%=ddlSize.ClientID %>').trigger("liszt:updated");
             }

             function disableFunctionKeys(e) {
                 var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                 if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                     if (e.keyCode == 115) {
                         $('#<%= lnkSave.ClientID %>').click();
                            e.preventDefault();
                        }
                        else if (e.keyCode == 117) {
                            $("input").removeAttr('disabled');
                            $('#<%= lnkClear.ClientID %>').click();
 e.preventDefault();
}
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                    }
    };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Multipleuombarcode');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Multipleuombarcode";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Multiple Uom Barcode</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>

                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />

                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Inventory Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtInvertorycode" runat="server" MaxLength="15" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Size"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlSize" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Barcode"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBarcode" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                    <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">

                   
                    <asp:GridView ID="gvMultipleuombarcode" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                        CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="Barcode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Multipleuombarcode_Delete(this);  return false;"
                                        CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Inventorycode" HeaderText="Inventory Code"></asp:BoundField>
                            <asp:BoundField DataField="Size" HeaderText="Size"></asp:BoundField>
                            <asp:BoundField DataField="Barcode" HeaderText="Barcode"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="ModifyUser"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <ups:PaginationUserControl runat="server" ID="MultipleuombarcodePaging" OnPaginationButtonClick="Multipleuombarcode_PaginationButtonClick" />
                </div>

                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
