﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmInventoryUpload.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmInventoryUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        /*********** Inventory Master  ***********/
        .inventoryMaster td:nth-child(1), .inventoryMaster th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .inventoryMaster td:nth-child(2), .inventoryMaster th:nth-child(2) {
            min-width: 200px;
            max-width: 200px;
        }

        .inventoryMaster td:nth-child(3), .inventoryMaster th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
        }

        .inventoryMaster td:nth-child(4), .inventoryMaster th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .inventoryMaster td:nth-child(5), .inventoryMaster th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .inventoryMaster td:nth-child(6), .inventoryMaster th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(7), .inventoryMaster th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }

        .inventoryMaster td:nth-child(8), .inventoryMaster th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(9), .inventoryMaster th:nth-child(9) {
            min-width: 130px;
            max-width: 130px;
        }

        .inventoryMaster td:nth-child(10), .inventoryMaster th:nth-child(10) {
            min-width: 120px;
            max-width: 120px;
        }

        .inventoryMaster td:nth-child(11), .inventoryMaster th:nth-child(11) {
            min-width: 120px;
            max-width: 120px;
        }

        .inventoryMaster td:nth-child(12), .inventoryMaster th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(13), .inventoryMaster th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(14), .inventoryMaster th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(15), .inventoryMaster th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(16), .inventoryMaster th:nth-child(16) {
            min-width: 60px;
            max-width: 60px;
        }

        .inventoryMaster td:nth-child(17), .inventoryMaster th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(18), .inventoryMaster th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(19), .inventoryMaster th:nth-child(19) {
            min-width: 70px;
            max-width: 70px;
        }

        .inventoryMaster td:nth-child(19) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(20), .inventoryMaster th:nth-child(20) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(20) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(21), .inventoryMaster th:nth-child(21) {
            min-width: 110px;
            max-width: 110px;
        }

        .inventoryMaster td:nth-child(21) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(22), .inventoryMaster th:nth-child(22) {
            min-width: 110px;
            max-width: 110px;
        }

        .inventoryMaster td:nth-child(22) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(23), .inventoryMaster th:nth-child(23) {
            min-width: 110px;
            max-width: 110px;
        }

        .inventoryMaster td:nth-child(23) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(24), .inventoryMaster th:nth-child(24) {
            min-width: 70px;
            max-width: 70px;
        }

        .inventoryMaster td:nth-child(24) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(25), .inventoryMaster th:nth-child(25) {
            min-width: 70px;
            max-width: 70px;
        }

        .inventoryMaster td:nth-child(25) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(26), .inventoryMaster th:nth-child(26) {
            min-width: 100px;
            max-width: 100px;
        }

        .inventoryMaster td:nth-child(26) {
            text-align: right;
        }

        .inventoryMaster td:nth-child(27), .inventoryMaster th:nth-child(27) {
            min-width: 100px;
            max-width: 100px;
        }
         .inventoryMaster td:nth-child(28), .inventoryMaster th:nth-child(28) {
            min-width: 100px;
            max-width: 100px;
        }
          .inventoryMaster td:nth-child(29), .inventoryMaster th:nth-child(29) {
            min-width: 100px;
            max-width: 100px;
        }
           .inventoryMaster td:nth-child(30), .inventoryMaster th:nth-child(30) {
            min-width: 100px;
            max-width: 100px;
        }

        /*********** Vendor Master ***********/
        .vendorMasterNew td:nth-child(1), .vendorMasterNew th:nth-child(1) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(2), .vendorMasterNew th:nth-child(2) {
            min-width: 300px;
            max-width: 300px;
        }

        .vendorMasterNew td:nth-child(3), .vendorMasterNew th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(4), .vendorMasterNew th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(5), .vendorMasterNew th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(6), .vendorMasterNew th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(7), .vendorMasterNew th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(8), .vendorMasterNew th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(9), .vendorMasterNew th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .vendorMasterNew td:nth-child(10), .vendorMasterNew th:nth-child(10) {
            min-width: 130px;
            max-width: 130px;
        }

        .vendorMasterNew td:nth-child(11), .vendorMasterNew th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        /*********** Customer Master ***********/
        .customerMaster td:nth-child(1), .customerMaster th:nth-child(1) {
            min-width: 250px;
            max-width: 250px;
        }

        .customerMaster td:nth-child(2), .customerMaster th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .customerMaster td:nth-child(3), .customerMaster th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(4), .customerMaster th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(5), .customerMaster th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(6), .customerMaster th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(7), .customerMaster th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(8), .customerMaster th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(9), .customerMaster th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(10), .customerMaster th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(11), .customerMaster th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(12), .customerMaster th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(13), .customerMaster th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(14), .customerMaster th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(15), .customerMaster th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(16), .customerMaster th:nth-child(16) {
            min-width: 120px;
            max-width: 120px;
        }

        .customerMaster td:nth-child(17), .customerMaster th:nth-child(17) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(18), .customerMaster th:nth-child(18) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(19), .customerMaster th:nth-child(19) {
            min-width: 100px;
            max-width: 100px;
        }

        .customerMaster td:nth-child(20), .customerMaster th:nth-child(20) {
            min-width: 120px;
            max-width: 120px;
        }

        .customerMaster td:nth-child(21), .customerMaster th:nth-child(21) {
            min-width: 120px;
            max-width: 120px;
        }

        .nav-tabs > li > a {
            color: White;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/jscript">

        function pageLoad() {

        }
        ///Browse Click
        function fncBrowseClick() {
            try {
                $("#<%=hidInvmaster.ClientID%>").val('1');
                $("#<%=excelupload.ClientID%>").click();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///PRN File Upload to Text box
        function fncAssignExcelFiletoTextbox() {
            try {
                <%--$("#<%=excelupload.ClientID%>").click();--%>
                var prnfile = document.getElementById("<%=excelupload.ClientID %>");
                var prnfileName = document.getElementById("<%=excelupload.ClientID %>").files[0].name;

                if (prnfileName != "Unipro_Master_Excel_web.xlsx") {
                    fncClearGrid();
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_choosecorrectexcelfile%>');
                    $('#<%=txtExcelFilePath.ClientID %>').val('');
                    return false;
                }


                if (prnfile.files.length == 0) {
                    $('#<%=txtExcelFilePath.ClientID %>').val('');
                }
                else {
                    $('#<%=txtExcelFilePath.ClientID %>').val(prnfileName);
                    $("#<%=btnloadExcel.ClientID%>").click();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Inventory Master validation
        function fncInvMastervalidation() {
            try {

                if ($('#<%=txtExcelFilePath.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_choosecorrectexcelfile%>');
                    return false;
                }
               <%-- else if ($("#<%=hidInvmaster.ClientID%>").val() == "0") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_invmaster%>');
                    return false;
                }--%>
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Clear static  Variable
    function fncClearGrid() {
        try {
            var inventorytStatus = "";

            $("#tblinventory [id*=InvMasterbody]").each(function () {
                inventorytStatus = "deleted";
                $(this).remove();
            });

            $("#tblVendor [id*=VendorMasterbody]").each(function () {
                $(this).remove();
            });

            $("#tblCustomer [id*=CustomerMasterbody]").each(function () {
                $(this).remove();
            });

            if (inventorytStatus == "deleted") {
                $('#tblinventory tfoot').append("<tr>");
                $('#tblinventory tfoot tr').css("height", "450px");

                $('#tblVendor tfoot').append("<tr>");
                $('#tblVendor tfoot tr').css("height", "450px");

                $('#tblCustomer tfoot').append("<tr>");
                $('#tblCustomer tfoot tr').css("height", "450px");

            }

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("frmInventoryUpload.aspx/fncClearStaticVariable")%>',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                },
                error: function (data) {
                    return false;
                }
            });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    function Alert(alert) {
        try {
            var popup = alert.slice(0, -20);
            ShowPopupMessageBox(popup);
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'InventoryUpload');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "InventoryUpload";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page " id="breadcrumbs_text_GAR">
                <%=Resources.LabelCaption.lbl_InventoryUpload%>
            </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="inventory_download">
        <a href="../Templates/Inventory_Excel_Template.zip">
            <%=Resources.LabelCaption.lblDownloadInvTemplate%>
        </a>
    </div>
    <div class="prnt_upload">
        <%--<div class="prnlbl">
            <asp:Label ID="lblDownloadInvTemplate" runat="server" Text='<%$ Resources:LabelCaption,lblDownloadInvTemplate %>'></asp:Label>
        </div>--%>
        <div class="inventory_browse">
            <div class="prnlbl">
                <asp:Label ID="lblInventorycode" runat="server" Text='<%$ Resources:LabelCaption,lbl_InventoryTemplate %>'></asp:Label>
            </div>
            <div class="prnlbl">
                <asp:TextBox ID="txtExcelFilePath" runat="server" CssClass="form-control-res" Width="400px"></asp:TextBox>
            </div>
            <div class="prnlbl">
                <asp:Button ID="btnBrowse" runat="server" CssClass="button_menu_New" Text="Browse"
                    OnClientClick="fncBrowseClick();return false;" />
                <asp:FileUpload ID="excelupload" runat="server" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                    onchange="return fncAssignExcelFiletoTextbox();" Style="display: none"></asp:FileUpload>
            </div>
            <div class="prnlbl">
                <asp:Button ID="btnUpload" runat="server" CssClass="button_menu_New" Text="Upload"
                    OnClick="btnUpload_Click" OnClientClick="return fncInvMastervalidation()" />
            </div>
        </div>
    </div>
    <div class="container-group-full inventoryExcel_Repeater">
        <div class="panel panel-default">
            <div id="Tabs" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs custnav-invoice custnav-cm-trans" role="tablist">
                    <li class="active"><a href="#Inventory" aria-controls="Inventory" role="tab" data-toggle="tab">Inventory Master</a></li>
                    <li><a href="#Vendor" aria-controls="Vendor" role="tab" data-toggle="tab">Vendor Master</a></li>
                    <li><a href="#Customer" aria-controls="Customer" role="tab" data-toggle="tab">Customer
                        Master</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="Inventory">
                        <div id="divInvMaster" class="Barcode_fixed_headers masterMigration inventoryMaster">
                            <table id="tblinventory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">SNO
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                        <th scope="col" runat="server" id="thDelete">Short Description
                                        </th>
                                        <th scope="col" runat="server" id="th9">HSNCode
                                        </th>
                                        <th scope="col" runat="server" id="thGrn">Batch
                                        </th>
                                        <th scope="col" runat="server" id="thGrnView">RefItemCode
                                        </th>
                                        <th scope="col">IAN Code/Barcode
                                        </th>
                                        <th scope="col">Department
                                        </th>
                                        <th scope="col">CategoryCode
                                        </th>
                                        <th scope="col">BrandCode
                                        </th>
                                        <th scope="col">SubCategory
                                        </th>
                                        <th scope="col">Class
                                        </th>
                                        <th scope="col">SubClass
                                        </th>
                                        <th scope="col">VendorCode
                                        </th>
                                        <th scope="col">Warehouse
                                        </th>
                                        <th scope="col">UOM
                                        </th>
                                        <th scope="col">ItemType
                                        </th>
                                        <th scope="col" runat="server">PriceType
                                        </th>
                                        <th scope="col" runat="server" id="thEdit">MRP
                                        </th>
                                        <th scope="col">BasicCost
                                        </th>
                                        <th scope="col">Disc.BasicPer1
                                        </th>
                                        <th scope="col">Disc.BasicPer2
                                        </th>
                                        <th scope="col">Disc.BasicPer3
                                        </th>
                                        <th scope="col">GSTPer
                                        </th>
                                        <th scope="col">Cess
                                        </th>
                                        <th scope="col">NetSellingPrice
                                        </th> 
                                       <th scope="col">WPrice1
                                        </th> 
                                        <th scope="col">WPrice2
                                        </th>
                                        <th scope="col">WPrice3
                                        </th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptrInventoryMaster" runat="server"> <%--OnItemDataBound="rptrInventoryMaster_ItemDataBound"--%>
                                    <HeaderTemplate>
                                        <tbody id="InvMasterbody">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="invRow" runat="server" onclick="fncRowClick(this);">
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("SNO") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                                            </td>
                                            <td runat="server" id="tbShortDescription">
                                                <asp:Label ID="lblShortDescription" runat="server" Text='<%# Eval("Short Description") %>' />
                                            </td>
                                            <td runat="server">
                                                <asp:Label ID="lblHSNCode" runat="server" Text='<%# Eval("HSNCode") %>' />
                                            </td>
                                            <td runat="server" id="tdBatch">
                                                <asp:Label ID="lblBatch" runat="server" Text='<%# Eval("Batch") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblRefItemCode" runat="server" Text='<%# Eval("RefItemCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblbarcode" runat="server" Text='<%# Eval("IAN Code/Barcode") %>' />
                                            </td>
                                            <td runat="server" id="tdDepartmentcode">
                                                <asp:Label ID="lblDepartmentCode" runat="server" Text='<%# Eval("DepartmentCode") %>' />
                                            </td>
                                            <td runat="server" id="tdCategoryCode">
                                                <asp:Label ID="lblCategoryCode" runat="server" Text='<%# Eval("CategoryCode") %>' />
                                            </td>
                                            <td runat="server" id="tdBarancode">
                                                <asp:Label ID="lblBrandCode" runat="server" Text='<%# Eval("BrandCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubCategoryCode" runat="server" Text='<%# Eval("SubCategoryCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblClass" runat="server" Text='<%# Eval("Class") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSubClass" runat="server" Text='<%# Eval("SubClass") %>' />
                                            </td>
                                            <td runat="server" id="tdVendorCode">
                                                <asp:Label ID="lblVendorCode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblWarehouse" runat="server" Text='<%# Eval("Warehouse") %>' />
                                            </td>
                                            <td runat="server" id="tdUOM">
                                                <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM") %>' />
                                            </td>
                                            <td runat="server" id="tdItemType">
                                                <asp:Label ID="lblItemType" runat="server" Text='<%# Eval("ItemType") %>' />
                                            </td>
                                            <td runat="server" id="tdPriceType">
                                                <asp:Label ID="lblPriceType" runat="server" Text='<%# Eval("PriceType") %>' />
                                            </td>
                                            <td runat="server" id="tdMRP">
                                                <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                            </td>
                                            <td runat="server" id="tdBasicCost">
                                                <asp:Label ID="lblBasicCost" runat="server" Text='<%# Eval("BasicCost") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDiscountBasicPer" runat="server" Text='<%# Eval("DiscountPer1") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDiscountBasicPer2" runat="server" Text='<%# Eval("DiscountPer2") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDiscountBasicPer3" runat="server" Text='<%# Eval("DiscountPer3") %>' />
                                            </td>
                                            <td runat="server" id="tdGSTPer">
                                                <asp:Label ID="lblGSTPer" runat="server" Text='<%# Eval("GSTPer") %>' />
                                            </td>
                                            <td runat="server" id="tdCess">
                                                <asp:Label ID="lblCess" runat="server" Text='<%# Eval("Cess") %>' />
                                            </td>
                                            <td runat="server" id="tdNetSellingPrice">
                                                <asp:Label ID="lblNetSellingPrice" runat="server" Text='<%# Eval("NetSellingPrice") %>' />
                                            </td>
                                           <td runat="server" id="tdW1">
                                                <asp:Label ID="lblW1Price" runat="server" Text='<%# Eval("W1Price") %>' />
                                            </td>
                                            <td runat="server" id="tdW2">
                                                <asp:Label ID="lblW2Price" runat="server" Text='<%# Eval("W2Price") %>' />
                                            </td>
                                            <td runat="server" id="tdW3">
                                                <asp:Label ID="lblW3PRice" runat="server" Text='<%# Eval("W3Price") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr  runat="server" id="emptyrowInventory">
                                        <td colspan="25" class="repeater_td_align_center">
                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                Text="No items to display" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="Vendor">
                        <div id="divVendor" class="Barcode_fixed_headers masterMigration vendorMasterNew">
                            <table id="tblVendor" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">VendorCode
                                        </th>
                                        <th scope="col">VendorName
                                        </th>
                                        <th scope="col">Address1
                                        </th>
                                        <th scope="col" runat="server" id="th2">Address2
                                        </th>
                                        <th scope="col" runat="server" id="th3">Country
                                        </th>
                                        <th scope="col" runat="server" id="th4">PindCode
                                        </th>
                                        <th scope="col">Phone
                                        </th>
                                        <th scope="col">Email
                                        </th>
                                        <th scope="col">Url
                                        </th>
                                        <th scope="col">ChequePrintName
                                        </th>
                                        <th scope="col">GSTINNo
                                        </th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptrVendorMaster" runat="server" OnItemDataBound="rptrVendorMaster_ItemDataBound">
                                    <HeaderTemplate>
                                        <tbody id="VendorMasterbody">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="gaRow" runat="server" onclick="fncRowClick(this);">
                                            <td>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("VendorCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("VendorName") %>' />
                                            </td>
                                            <td id="Td1" runat="server">
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address1") %>' />
                                            </td>
                                            <td runat="server" id="tdEdit">
                                                <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address2") %>' />
                                            </td>
                                            <td runat="server" id="tdGrn">
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>' />
                                            </td>
                                            <td runat="server" id="tdGrnView">
                                                <asp:Label ID="lblPindCode" runat="server" Text='<%# Eval("PindCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblUrl" runat="server" Text='<%# Eval("Url") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblChequePrintName" runat="server" Text='<%# Eval("ChequePrintName") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblGSTINNo" runat="server" Text='<%# Eval("GSTINNo") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr style="height: 450px" runat="server" id="emptyrowVendor">
                                        <td colspan="11" class="repeater_td_align_center">
                                            <asp:Label ID="lblEmptyVendor" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                Text="No items to display" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="Customer">
                        <div id="divCustomer" class="Barcode_fixed_headers masterMigration customerMaster">
                            <table id="tblCustomer" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col" runat="server" id="th5">CustomerName
                                        </th>
                                        <th scope="col">OldCustomerCode
                                        </th>
                                        <th scope="col" runat="server" id="th6">Address1
                                        </th>
                                        <th scope="col" runat="server" id="th7">Address2
                                        </th>
                                        <th scope="col" runat="server" id="th8">Address3
                                        </th>
                                        <th scope="col">Country
                                        </th>
                                        <th scope="col">PinCode
                                        </th>
                                        <th scope="col">Email
                                        </th>
                                        <th scope="col">ResPhone
                                        </th>
                                        <th scope="col">OffPhone
                                        </th>
                                        <th scope="col">Mobile
                                        </th>
                                        <th scope="col">JoinDate
                                        </th>
                                        <th scope="col">ExpiryDate
                                        </th>
                                        <th scope="col">RenewalDate
                                        </th>
                                        <th scope="col">TotalSales
                                        </th>
                                        <th scope="col">TotalRewardPoint
                                        </th>
                                        <th scope="col">DateOfBirth
                                        </th>
                                        <th scope="col">MaritalStatus
                                        </th>
                                        <th scope="col">Sex
                                        </th>
                                        <th scope="col">CreditOutStanding
                                        </th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptrCustomerMaster" runat="server" OnItemDataBound="rptrCustomerMaster_ItemDataBound">
                                    <HeaderTemplate>
                                        <tbody id="CustomerMasterbody">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="gaRow" runat="server" onclick="fncRowClick(this);">
                                            <td id="Td2" runat="server">
                                                <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("CustomerName") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOldCustomerCode" runat="server" Text='<%# Eval("OldCustomerCode") %>' />
                                            </td>
                                            <td runat="server" id="tdEdit">
                                                <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("Address1") %>' />
                                            </td>
                                            <td runat="server" id="tdGrn">
                                                <asp:Label ID="lblAddress2" runat="server" Text='<%# Eval("Address2") %>' />
                                            </td>
                                            <td runat="server" id="tdGrnView">
                                                <asp:Label ID="lblAddress3" runat="server" Text='<%# Eval("Address3") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPinCode" runat="server" Text='<%# Eval("PinCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lbleMail" runat="server" Text='<%# Eval("Email") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBrandCode" runat="server" Text='<%# Eval("ResPhone") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblOffPhone" runat="server" Text='<%# Eval("OffPhone") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblJoinDate" runat="server" Text='<%# Eval("JoinDate") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Eval("ExpiryDate") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblRenewalDate" runat="server" Text='<%# Eval("RenewalDate") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalSales" runat="server" Text='<%# Eval("TotalSales") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalRewardPoint" runat="server" Text='<%# Eval("TotalRewardPoint") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDateOfBirth" runat="server" Text='<%# Eval("DateOfBirth") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMaritalStatus" runat="server" Text='<%# Eval("MaritalStatus") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSex" runat="server" Text='<%# Eval("Sex") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCreditOutStanding" runat="server" Text='<%# Eval("CreditOutStanding") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr style="height: 450px" runat="server" id="emptyrowCustomer">
                                        <td colspan="20" class="repeater_td_align_center">
                                            <asp:Label ID="lblEmptyCustomer" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                Text="No items to display" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <asp:Button ID="btnloadExcel" runat="server" OnClick="btnloadExcel_Click" />
        <asp:HiddenField ID="hidInvmaster" runat="server" Value="1" />
    </div>
     <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
