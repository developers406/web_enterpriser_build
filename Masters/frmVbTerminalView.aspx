﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frmVbTerminalView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmVbTerminalView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
                  $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmVbTerminalView.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                         event.preventDefault();
                     },
                     select: function (e, i) {
                         $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                       __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                       return false;
                   },

                   minLength: 1
                  });

            
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'VBTerminalView');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "VBTerminalView";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration:none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">VB Terminal view</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <br />
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px"
                                 OnClick="lnkNEW_Click"><i class="icon-play"></i>NEW</asp:LinkButton>
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                 OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                        </asp:Panel>
                    </div>
                    <br />
                    <asp:GridView ID="gvTerminal" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                        CssClass="pshro_GridDgn" oncopy="return false" OnRowDataBound="gvTerminal_RowDataBound"
                                            OnRowDeleting="gvTerminal_RowDeleting" OnRowEditing="gvTerminal_RowEditing">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:CommandField HeaderText="Delete" DeleteImageUrl="~/images/No.png" ShowDeleteButton="True" ButtonType="Image" />
                            <asp:CommandField HeaderText="Edit" EditImageUrl="~/images/edit-icon.png" ShowEditButton="True" ButtonType="Image" />
                            <asp:BoundField DataField="TerminalCode" HeaderText="Terminal Code" ></asp:BoundField>
                            <asp:BoundField DataField="TerminalName" HeaderText="Terminal Name"></asp:BoundField>
                            <asp:BoundField DataField="TerminalType" HeaderText="Terminal Type"></asp:BoundField>
                            <asp:BoundField DataField="ComputerName" HeaderText="Computer Name"></asp:BoundField>
                            <asp:BoundField DataField="TillAmount" HeaderText="Till Amount"></asp:BoundField>
                            <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                            <asp:BoundField DataField="FloorCode" HeaderText="Floor Code"></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="ModifyUser"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
