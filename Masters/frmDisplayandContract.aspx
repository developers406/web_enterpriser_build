﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmDisplayandContract.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmDisplayandContract" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 43px;
            max-width: 43px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 48px;
            max-width: 48px;
            text-align: center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 64px;
            max-width: 64px;
            text-align: center !important;
        }


        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 381px;
            max-width: 381px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            display: none;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 69px;
            max-width: 69px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 167px;
            max-width: 167px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            display: none;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            display: none;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            display: none;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            display: none;
        }

        .Discount {
            margin-left: 438px;
            padding: 10px 10px 0px 10px;
            margin-top: -157px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
            //$('input[type=text]').bind('change', function () {
            //    if (this.value.match(SpecialChar)) {
            //        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
            //        this.value = this.value.replace(SpecialChar, '');
            //    }
            //});
            fncDecimal();
            fncSetAutocomplete();
            $('#<%=txtDisplaycode.ClientID %>').focus();
            //  Keycode();           


        }
        function fncDecimal() {
            try {
                $('#<%=txtPerDay.ClientID %>').number(true, 2);
                $('#<%=txtPerMonth.ClientID %>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //function clearForm() {
        //    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        //    $(':checkbox, :radio').prop('checked', false);

        //}
        function ValidateForm() {
            try {
                if ($('#<%=txtDisplaycode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                    ShowPopupMessageBox("You have no permission to save existing Display");
                    return false;
                }
                if (!($('#<%=txtDisplaycode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                    ShowPopupMessageBox("You have no permission to save New Display");
                    return false;
                }
                var Show = '';
                var DisplayCode = $.trim($('#<%=txtDisplaycode.ClientID %>').val());   //23112022 musaraf
                if (DisplayCode == "") {
                    Show = Show + 'Please Enter Display Code';
                    document.getElementById("<%=txtDisplaycode.ClientID %>").focus();
                    //return false;
                }
                if (document.getElementById("<%=ddlLocation.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter Location';
                    document.getElementById("<%=ddlLocation.ClientID %>").focus();
                    //return false;
                }
                if (document.getElementById("<%=ddlDepartment.ClientID%>").value == "") {
                    Show = Show + '<br />Please Enter Department';
                    document.getElementById("<%=ddlDepartment.ClientID %>").focus();
                    //return false;
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    return true;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetAutocomplete() {
            // alert('test');
            //debugger;
            <%--var obj = {};
            obj.Type = 'DepCode';
            obj.Code = $('#<%=txtDisplaycode.ClientID %>').val()--%>;
            try {
                $('#<%=txtDisplaycode.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmDisplayandContract.aspx/fncGetDdl",
                        data: "{ 'Code': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            //console.log(jQuery.parseJSON(data.d)[0]["DisplayCode"]);
                            //debugger;
                            response($.map(data.d, function (item) {
                                return {
                                    label: item,
                                    val: item.split('-')[1]
                                }
                            }))


                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtDisplaycode.ClientID %>').val(i.item.label.split('-')[0]);
                        $('#<%=txtDisplayName.ClientID %>').val(i.item.val);


                        event.preventDefault();
                    },
                select: function (e, i) {

                    $('#<%=txtDisplaycode.ClientID %>').val(i.item.label.split('-')[0]);
                        $('#<%=txtDisplayName.ClientID %>').val(i.item.val);
                    $('#<%=txtDisplaycode.ClientID %>').prop("disabled", true);
                        //fncGetAccountDetails(i.item.label.split('-')[0]);
                        //  __doPostBack('ctl00$ContentPlaceHolder1$lnkDpartment', '');
                        return false;

                    },
                minLength: 1
            });
            // alert('test');
            // var location = 'Location';


                $('#<%=txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmDisplayandContract.aspx/fncGetDdl",
                        data: "{ 'Code': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item,
                                    // val: item.split('-')[1]
                                }
                            }))


                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtSearch.ClientID %>').val(i.item.label.split('-')[0]);
                      <%--  //$('#<%=txtDisplayName.ClientID %>').val(i.item.val);--%>


                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $('#<%=txtSearch.ClientID %>').val(i.item.label.split('-')[0]);
                        <%--$('#<%=txtDisplayName.ClientID %>').val(i.item.val);--%>

                        //fncGetAccountDetails(i.item.label.split('-')[0]);
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');
                        return false;

                    },
                    minLength: 1
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Display_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Display");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });
        }
        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Display");
                return false;
            }//alert('test');
            clearForm();
            DisplayDetails($(source).closest("tr"));
        }
        function DisplayDetails(row) {
            //clearForm();
                    <%--$('#<%=hdfdstatus.ClientID %>').val("EDIT");--%>
            $('#<%=txtDisplaycode.ClientID %>').prop("disabled", true);
            $('#<%=txtDisplaycode.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtDisplayName.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtPerMonth.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtPersonIncharge.ClientID %>').val(($("td", row).eq(7).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));

            if (($("td", row).eq(9).html().replace(/&nbsp;/g, '')) == "1") {
                $('#<%=chActive.ClientID %>').prop('checked', true);
            }
            else {
                $('#<%=chActive.ClientID %>').prop('checked', false);
            }
            $('#<%=ddlLocation.ClientID %>').val($.trim($("td", row).eq(10).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlLocation.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlDepartment.ClientID %>').val(($("td", row).eq(11).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
            $('#<%=txtPerDay.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtRemarks.ClientID %>').val(($("td", row).eq(12).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlFloor.ClientID %>').val(($("td", row).eq(13).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlShelf.ClientID %>').val($.trim($("td", row).eq(14).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                            e.preventDefault();
                        }
                        else if (e.keyCode == 117) {
                            $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 220) {
                    return false;
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function clearForm() {
            $('#<%=txtDisplaycode.ClientID %>').prop("disabled", false);
            $('#<%= txtDisplaycode.ClientID %>').val("");
            $('#<%= txtDisplayName.ClientID %>').val("");
            $('#<%= ddlShelf.ClientID %>').val("0");
            $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
            $('#<%= ddlFloor.ClientID %>').val("0");
            $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
            $('#<%= txtRemarks.ClientID %>').val("");
            $('#<%= txtPerDay.ClientID %>').val("0");
            $('#<%= txtPerMonth.ClientID %>').val("0");
            $('#<%= txtPersonIncharge.ClientID %>').val("");
            $('#<%= ddlLocation.ClientID %>').val("0");
            $('#<%=ddlLocation.ClientID %>').trigger("liszt:updated");
            $('#<%= ddlDepartment.ClientID %>').val("");
            $('#<%= chActive.ClientID %>').prop('checked', false);
            $('#<%=txtDisplaycode.ClientID %>').focus();
        }
        $(document).ready(function () {
            $("input:text").focus(function () { $(this).select(); });
        });



    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Display&Contract');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Display&Contract";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a class="active-page">Display and Contract</a></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>

            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Display Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDisplaycode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Display Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDisplayName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Location"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlLocation" runat="server" class="chzn-select" CssClass="chzn-select">
                                    <asp:ListItem Value="0" Text="---SELECT---"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Department"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <asp:DropDownList ID="ddlDepartment" runat="server" class="chzn-select" Style="width: 238px;" CssClass="chzn-select">
                                <asp:ListItem Value="0" Text="---SELECT---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <asp:DropDownList ID="ddlFloor" runat="server" class="chzn-select" CssClass="chzn-select" Style="width: 238px;">
                                <asp:ListItem Value="0" Text="---SELECT---"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 Discount" runat="server" id="divDiscount">
                    <div class="container-group-small" style="width: 100%;">
                        <div class="container-control">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="Shelf"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlShelf" runat="server" class="chzn-select" CssClass="chzn-select">
                                        <asp:ListItem Value="0" Text="---SELECT---"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label10" runat="server" Text="Display Charges"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label11" runat="server" Text="PerDay"></asp:Label>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 10px">
                                            <asp:TextBox ID="txtPerDay" MaxLength="12" runat="server" CssClass="form-control-res">0</asp:TextBox>
                                        </div>
                                        <div class="col-md-2" style="margin-left: 5px">
                                            <asp:Label ID="Label12" runat="server" Text="PerMonth"></asp:Label>
                                        </div>
                                        <div class="col-md-3" style="margin-left: 24px">
                                            <asp:TextBox ID="txtPerMonth" MaxLength="12" runat="server" CssClass="form-control-res">0</asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Person In-Charge"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPersonIncharge" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label13" runat="server" Text="Active"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:CheckBox ID="chActive" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="button-contol" style="margin-left: 175px">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                        OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails" style="margin-top: 10px">
                    <div class="grid-search" style="margin-left: 10px">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                              <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    <div class="right-container-top-detail">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Edit
                                                </th>
                                                 <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Display Code
                                                </th>
                                                <th scope="col">Display Name
                                                </th>
                                                <th scope="col">DisplayCharges(PD)
                                                </th>
                                                <th scope="col">Display Charges(PM)
                                                </th>
                                                <th scope="col">Person InCharge
                                                </th>
                                                <th scope="col">Occupied
                                                </th>
                                                <th scope="col">Active
                                                </th>
                                                <th scope="col">Location
                                                </th>
                                                <th scope="col">Departmentcode
                                                </th>
                                                <th scope="col">Remarks
                                                </th>
                                                <th scope="col">Floor
                                                </th>
                                                <th scope="col">Shelf
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1330px; background-color: aliceblue;">
                                        <asp:GridView ID="gvDisplay" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn grdLoad" oncopy="return false">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Display_Delete(this); return false;"
                                                            ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                            ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="RowNumber" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="DisplayCode" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="DisplayName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="DisplayChargesPerDay" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="DisplayChargesPerMonth" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="PersonIncharge" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Occupied" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Active" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Location" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Departmentcode" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Remarks" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="Floor" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Shelf" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ups:PaginationUserControl runat="server" ID="Display" OnPaginationButtonClick="Display_PaginationButtonClick" />
                <%--<ups:PaginationUserControl runat="server" ID="SubCategoryPaging" OnPaginationButtonClick="SubCategory_PaginationButtonClick" />--%>


                <div class="hiddencol">

                    <%--<asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="hiddencol">

            <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>
    </div>
</asp:Content>
