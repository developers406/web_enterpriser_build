﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBarcodeCreationRequest.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmBarcodeCreationRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <style type="text/css">

        .rdo-colors input[type="radio"] {
   
    cursor:pointer;
}

.rdo-colors label {
    display:inline-block;
    padding:4px 11px;
}

.rdo-colors input[type="radio"]:checked + label {
    background-color:yellow;
    padding:6px 15px;
}
 .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function ValidateForm() {
            if ($("#ContentPlaceHolder1_gvBarcodeCreationRequest [id*=ContentPlaceHolder1_gvBarcodeCreationRequest_chkStatus]:checked").length == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                return true;
            }
        }
</script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'BarcodeCreationRequest');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "BarcodeCreationRequest";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Barcode Creation Request</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>

            </ul>
        </div>
<asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails" style = "padding-top:21px">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server">
                        <center>
                       <div class="rdo-colors">
                       <asp:RadioButton ID="rbnGrnActivateDeletedRequest" runat="server" Text="Activate Deleted Request" 
                             Visible="true"  GroupName="grnBarCodeCreationRequest" AutoPostBack="True" OnCheckedChanged="rbnGrnActivateDeletedRequest_CheckedChanged" />
                   
                       
                            <asp:RadioButton ID="rbnGrnPendingBarCodeCreation" runat="server" Text="Pending BarCode Creation"
                             Visible="true" GroupName="grnBarCodeCreationRequest" Checked="true" AutoPostBack="True" OnCheckedChanged="rbnGrnPendingBarCodeCreation_CheckedChanged" />
                         
                            <asp:RadioButton ID="rbnGrnBarCodeCreated" runat="server" Text="BarCode Created"
                             Visible="true" GroupName="grnBarCodeCreationRequest" AutoPostBack="True" OnCheckedChanged="rbnGrnBarCodeCreated_CheckedChanged" />
                       </div>
                        
                        </center>
                        </asp:Panel>
                    </div>
                    <br />
                    <div style="height: 180px; overflow: auto; border-style: solid;" >
                    <asp:GridView ID="gvBarcodeCreationRequest" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false"  >
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:BoundField DataField="InventoryCode" HeaderText="Inventory Code"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                            <asp:BoundField DataField="BarCode" HeaderText="Bar Code"></asp:BoundField>
                            <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                            <asp:BoundField DataField="RequestedDate" HeaderText="Requested Date"></asp:BoundField>
                            <asp:BoundField DataField="CreateUser" HeaderText="Requested By"></asp:BoundField>
                            <asp:BoundField DataField="TerminalCode" HeaderText="Terminal"></asp:BoundField>
                            <asp:TemplateField HeaderText = "Select">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkStatus" runat="server" />
                                </ItemTemplate>                    
                            </asp:TemplateField>
                         </Columns>
                    </asp:GridView>
                    </div>
                    <div id="ex" style="float: right; padding-top:21px; padding-right:30px;">
                    <asp:Button ID="bnActivate" OnClientClick="return ValidateForm()" runat="server" class="button-blue" Width="150px"
                    Text="Activate" onclick="bnActivate_Click"></asp:Button>
                    <asp:Button ID="bnDelete" OnClientClick="return ValidateForm()" runat="server" class="button-blue" Width="150px"
                    Text="Delete" onclick="lnkDelete_Click"></asp:Button>
                    <asp:Button ID="bnCreateBarcode"  runat="server" class="button-blue" Width="150px"
                    Text="Create Barcode" OnClientClick="return ValidateForm()" onclick="bnCreateBarcode_Click"></asp:Button>
                    </div>
                                

                    
                    
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
