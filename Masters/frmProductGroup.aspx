﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmProductGroup.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmProductGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/CategoryAttribute.css" rel="stylesheet" />
    <style type="text/css">
        td, th {
            width: 222px !important;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            $(".chzn-container").css('display', 'none');
            $(".form-control-res").css('height', '25px');
            $(".form-control-res").css('display', 'block');
            fncSetAutoComplete();
            $("[id$=txtQty]").number(true, 2);
            $("[id$=txtCost]").number(true, 2);
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "category") {
                    fncGetDepartmentForSelecetedCategory(Description);
                }
                if (SearchTableName == "Inventory") {
                    $("[id$=txtItemCode]").val(Code);
                    $("[id$=txtDescription]").val(Description);
                    fncDisable();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetDepartmentForSelecetedCategory(CategoryCode) {

            try {
                var obj = {};
                obj.CategoryCode = CategoryCode.trim().replace("'", "''");
                //$data = { prefix: CategoryCode.replace("'", "''") };
                $.ajax({
                    type: "POST",
                    url: "../Inventory/frmInventoryMaster.aspx/GetDeptCode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $('#<%=txtDepartment.ClientID %>').val(msg.d);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message)
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetAutoComplete() {
            try {
                $("[id$=txtItemCode]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        obj.prefix = escape(request.term);
                        $.ajax({
                            url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetFilterValue") %>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0].split('}')[0],
                                        val: item.split('/')[1],
                                        desc: item.split('/')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $("[id$=txtItemCode]").val(i.item.desc);
                        event.preventDefault();
                    },
                    //==========================> After Select Value
                    select: function (e, i) { 
                        $("[id$=txtItemCode]").val(i.item.val);
                        $("[id$=txtDescription]").val(i.item.desc);
                        $("[id$=txtCost]").val(i.item.label.split('}')[1]);
                        $("[id$=txtQty]").select();
                        fncDisable();
                        return false;
                    },

                    minLength: 1
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
                return false;
            }
        }

        function fncDisable() {
            try {
                $("[id$=txtItemCode]").attr('disabled', 'disabled');
                $("[id$=txtDescription]").attr('disabled', 'disabled');
                $("[id$=txtCost]").attr('disabled', 'disabled');
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        function fncClearAll() {
            fncClear();
            $("[id$=txtInvcategory]").val('');
            $("[id$=txtDepartment]").val('');
            $("[id$=txtSubCategory]").val('');
            $("[id$=txtClass]").val('');
            $("[id$=txtProfuctGroup]").val('');
            $("[id$=txtBarcodePrefix]").val('');
            $("#tblProductGroup tbody").children().remove();
            fncClear();
        }
        function fncClear() {
            $("[id$=txtItemCode]").removeAttr('disabled');
            $("[id$=txtDescription]").removeAttr('disabled');
            $("[id$=txtCost]").removeAttr('disabled');
            $("[id$=txtDescription]").val('');
            $("[id$=txtQty]").val('');
            $("[id$=txtCost]").val('');
            $("[id$=txtItemCode]").val('');
            $("[id$=txtItemCode]").select();
        }

        function fncAdd() {
            try {
                var Show = '', InterState = 0;
                if ($('#<%=txtItemCode.ClientID%>').val() == "") {
                    Show = Show + 'Please Enter Item Code.';
                }
                if ($('#<%=txtDescription.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Description.';
                }
                if ($('#<%=txtQty.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Invoice Date.';
                }
                if ($('#<%=txtQty.ClientID%>').val() == "" || parseFloat($('#<%=txtQty.ClientID%>').val()) <= 0) {
                    Show = Show + '<br /> Please Enter Valid Qty.';
                }

                $("#tblProductGroup tbody").children().each(function () {
                    var obj = $(this);
                    if (obj.find('td[id*=tdItemCode]').val().trim() == $('#<%=txtItemCode.ClientID%>').val().trim()) {
                        Show = Show + '<br /> ItemCode Number Already Exists.';
                    }
                });


                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                fncAddTable(0, $('#<%=txtItemCode.ClientID%>').val(), $('#<%=txtDescription.ClientID%>').val(), $('#<%=txtCost.ClientID%>').val(), $('#<%=txtQty.ClientID%>').val(), $('#<%=txtQty.ClientID%>').val() * $('#<%=txtCost.ClientID%>').val()); 

                fncClear();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAddItem() {
            try {
                var objAttributeData = jQuery.parseJSON($('#<%=hidEditvalue.ClientID%>').val());

                for (var i = 0; i < objAttributeData.length; i++) {
                    fncAddTable(i, objAttributeData[i]["ItemCode"], objAttributeData[i]["Description"], objAttributeData[i]["Cost"], objAttributeData[i]["Qty"], objAttributeData[i]["TotalCost"],);
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAddTable(Sno, ItemCode, Desc, Cost, Qty, TotalCost) {
            try {
                var tblProductGroup = $("#tblProductGroup tbody");
                var maxRowNo = tblProductGroup.children().length;
                maxRowNo = parseFloat(maxRowNo) + 1;
                var rowno = "rowno";
                var row = "<tr id='GRNBodyrow_" + maxRowNo + "'  tabindex='" + maxRowNo + "'  >"
                    + "<td  onclick ='DeleteRow(this);return false;' style='text-align: center !important;'  ><span id='tdAction_" + maxRowNo + "' class ='glyphicon glyphicon-remove-circle' style ='font-size: 20px;color: red;cursor: pointer;' /></td>"
                    + "<td id ='tdSNo_" + maxRowNo + "' >" + maxRowNo + "</td>"
                    + "<td id ='tdItemCode_" + maxRowNo + "' >" + ItemCode + "</td>"
                    + "<td id ='tdDesc_" + maxRowNo + "' >" + Desc + "</td>"
                    + "<td id ='tdQty_" + maxRowNo + "'  >" + parseFloat(Qty).toFixed(2) + "</td>"
                    + "<td id ='tdCost_" + maxRowNo + "'  >" + parseFloat(Cost).toFixed(2) + "</td>"
                    + "<td id ='tdTotal_" + maxRowNo + "' style='display:none !important'>" + parseFloat(TotalCost).toFixed(2) + "</td></tr > ";
                tblProductGroup.append(row);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function DeleteRow(source) {
            var Itemcode = $(source).closest('tr').find('td[id*=tdDesc]').text();
            var tblVat = 0;
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 'auto',
                modal: true,
                open: function () {
                    var markup = 'Do you want Delete this S.No? -' + Itemcode;
                    $(this).html(markup);
                },
                buttons: {
                    "NO": function () {
                        $(this).dialog("close");
                    },
                    "YES": function () {
                        $(this).dialog("close");
                        $(source).closest('tr').remove();
                    }
                }
            });
        }

        function fncSave() {
            try {
                var Show = '', InterState = 0;
                if ($('#<%=txtInvcategory.ClientID%>').val() == "") {
                    Show = Show + 'Please Enter Category.';
                }
                if ($('#<%=txtDepartment.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Department.';
                }
                if ($('#<%=txtSubCategory.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter SubCategory.';
                }
                if ($('#<%=txtProfuctGroup.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter Product Group.';
                }
                if ($('#<%=txtBarcodePrefix.ClientID%>').val() == "") {
                    Show = Show + '<br /> Please Enter BarCode Prefix.';
                }
                if ($("#tblProductGroup tbody").children().length <= 0) {
                    Show = Show + '<br /> Invalid Raw Material Details.';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    var xml = '<NewDataSet>';
                    $("#tblProductGroup tbody").children().each(function () {
                        var obj = $(this);
                        xml += "<Table>";
                        xml += "<ItemCode>" + obj.find('td[id*=tdItemCode]').text().trim() + "</ItemCode>";
                        xml += "<Description>" + obj.find('td[id*=tdDesc]').text().trim() + "</Description>";
                        xml += "<Qty>" + obj.find('td[id*=tdQty]').text().trim() + "</Qty>";
                        xml += "<Cost>" + obj.find('td[id*=tdCost]').text().trim() + "</Cost>";
                        xml += "<TotalCost>" + obj.find('td[id*=tdTotal]').text().trim() + "</TotalCost>";
                        xml += "<Remarks>" + ''+ "</Remarks>"; 
                        xml += "</Table>";
                    });
                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    $('#<%=hidSaveValue.ClientID %>').val(xml);
                    return true;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSaveAlert(GroupNo) {
            try {
                fncClearAll();
                ShowPopupMessageBox("Product Group Saved Successfully -" + GroupNo);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Product Group View</li>
                <li class="active-page">Product Group</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div id="dialog-confirm" style="display: none;"></div>
        <div class="EnableScroll">
            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-12">
                    <span class="glyphicon glyphicon-arrow-left hedertext"></span><span class="whole-price-header">Product Group</span>
                </div>
            </div>
            <div class="col-md-6" >
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Category</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtInvcategory" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category',  'txtInvcategory', 'txtDepartment');"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Department</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtDepartment" MaxLength="50" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department',  'txtDepartment', 'txtSubCategory');"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">SubCategory</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtSubCategory" MaxLength="50" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Subcategory',  'txtSubCategory', 'txtClass');"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Class</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtClass" MaxLength="50" runat="server" CssClass="form-control-res"  onkeydown="return fncShowSearchDialogCommon(event, 'Class',  'txtClass', 'txtProfuctGroup');"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Product Group</span>
                    </div>
                    <div class="col-md-10 Cat_Margin">
                        <asp:TextBox ID="txtProfuctGroup" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2 Cat_Margin">
                        <span class="mandatory">*</span><span class="lblBlack">Barcode Prefix</span>
                    </div>
                    <div class="col-md-4 Cat_Margin">
                        <asp:TextBox ID="txtBarcodePrefix" MaxLength="3" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="background-color: aliceblue">
                <div class="col-md-12">
                    <span class="whole-price-header">Raw Materials</span>
                </div>
            </div>
            <div class="Payment_fixed_headers Cat_Margin">
                <table id="tblProductGroup" cellspacing="0" rules="all" border="1" style="width: 100%;" class="Invoice">
                    <thead>
                        <tr>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">ItemCode
                            </th>
                            <th scope="col">Description  
                            </th>
                            <th scope="col">Qty  
                            </th>
                            <th scope="col">Cost
                            </th>
                        </tr>
                    </thead>
                    <tbody style="overflow-y: scroll; height: 200px;">
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">ItemCode</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:TextBox ID="txtItemCode" runat="server" MaxLength="50" CssClass="form-control-res"
                            onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemCode', 'txtQty');"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">Description</span>
                    </div>
                    <div class="col-md-3 Cat_Margin">
                        <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">Qty</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:TextBox ID="txtQty" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <span class="lblBlack">Cost</span>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:TextBox ID="txtCost" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" Style="float: right;"
                            OnClientClick="fncAdd();return false;"><i class="icon-play"></i>Add</asp:LinkButton>
                    </div>
                    <div class="col-md-1 Cat_Margin">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Style="float: left;"
                            OnClientClick="fncClear();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>
                <div class="col-md-12 Cat_Margin">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Style="float: right; margin-left: 10px;"
                            OnClientClick="return fncSave();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save</asp:LinkButton>

                        <asp:LinkButton ID="lnkAClearAll" runat="server" class="button-blue" Style="float: right; margin-left: 10px;"
                            OnClientClick="fncClearAll();return false;"><i class="icon-play"></i>Clear</asp:LinkButton>

                        <asp:LinkButton ID="lnkClose" runat="server" class="button-green" Style="float: right; margin-left: 10px;" PostBackUrl="~/Masters/frmProductGroupView.aspx"><i class="icon-play"></i>Close</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
        <asp:HiddenField ID="hidSaveValue" runat="server" />
        <asp:HiddenField ID="hidGroupNo" runat="server" />
        <asp:HiddenField ID="hidEditvalue" runat="server" />
    </div>
</asp:Content>
