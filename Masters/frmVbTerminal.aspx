﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmVbTerminal.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmVbTerminal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .header {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
        }

        .padding {
            padding-top: 15px;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtSetting.ClientID%>").value != "A" && document.getElementById("<%=txtSetting.ClientID%>").value != "B") {
                Show = Show + 'Please Enter Only A OR B IN Setting';
                document.getElementById("<%=txtSetting.ClientID %>").focus();
            }


            if (document.getElementById("<%=txtTerminalCode.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Terminal Code';
                document.getElementById("<%=txtTerminalCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtTerminalName.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Terminal Name';
                document.getElementById("<%=txtTerminalName.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlTerminalType.ClientID%>").value == 0) {
                Show = Show + '<br />Please Enter Terminal Type';
                document.getElementById("<%=ddlTerminalType.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlFloorName.ClientID%>").value == 0) {
                Show = Show + '<br />Please Enter Floor Name';
                document.getElementById("<%=ddlFloorName.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtComputername.ClientID%>").value == 0) {
                Show = Show + '<br />Please Enter Computer Name';
                document.getElementById("<%=txtComputername.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                return true;
            }
        }

        function pageLoad() {
            $('#<%=ddlCommPortNo.ClientID %>').trigger("liszt:updated");
            $("select").chosen({ width: '100%' });
            <%--Attachedweightmachinesettingfalse();
            $('#<%=lblBarcodeAttended.ClientID %>').hide();
            $('#<%=txtBarcodeAttendedPortNo.ClientID %>').hide();
            $('#<%=lblCustDisplayPortNo.ClientID %>').hide();
            $('#<%=txtCustDisplayPortNo.ClientID %>').hide();
            MeCodeHide();
            $('#<%= rb40ColDriver.ClientID %>').attr('checked', 'checked');
            $('#<%= rbPrinter.ClientID %>').attr('checked', 'checked');
            $('#<%= rbPosiflex.ClientID %>').attr('checked', 'checked');--%>
            $(function () {
                $('#<%=ddlCommPortNo.ClientID %>').change(function () {
                    $('#<%=txtPortNo.ClientID %>').val($('#<%=ddlCommPortNo.ClientID %>').find('option:selected').val());
                })
            });
            $('#<%=txtTerminalCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmVBTerminal.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtTerminalCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {


                        if (data.d != "Null") {
                            $('#<%=hdnValue.ClientID %>').val($("#<%=txtTerminalCode.ClientID%>").val());
                             $("#<%= btnEdit.ClientID %>").click();
                         }
                     },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });
             $(document).ready(function () {
                 $('#<%= cbAttachedweightmachinesetting.ClientID %>').change(function () {
                        if (this.checked) {
                            Attachedweightmachinesettingtrue();
                        }
                        else {
                            Attachedweightmachinesettingfalse();
                        }
                    });
                });
                $(function () {
                    $('#<%= lnkpos.ClientID %>').on('click', function () {
                    if (document.getElementById("<%=txtTerminalCode.ClientID%>").value == "") {
                        ShowPopupMessageBox('Please Enter Terminal Code');
                        document.getElementById("<%=txtTerminalCode.ClientID %>").focus();
                        $('#<%=txtTerminalCode.ClientID %>').prop("disabled", true);
                    }
                    else {

                        $("#divPosSetting").show();
                        $("#divMainContainer").hide();
                        return false;
                    }

                });
            });
            $(document).ready(function () {
                $('#<%= chkBarcodeAttended.ClientID %>').change(function () {
                    if (this.checked) {
                        $('#<%=lblBarcodeAttended.ClientID %>').show();
                        $('#<%=txtBarcodeAttendedPortNo.ClientID %>').show();
                    }
                    else {
                        $('#<%=lblBarcodeAttended.ClientID %>').hide();
                        $('#<%=txtBarcodeAttendedPortNo.ClientID %>').hide();
                    }
                });
            });
            $(document).ready(function () {
                $('#<%= ChkCustDisplayAttended.ClientID %>').change(function () {
                    if (this.checked) {
                        $('#<%=lblCustDisplayPortNo.ClientID %>').show();
                        $('#<%=txtCustDisplayPortNo.ClientID %>').show();
                    }
                    else {
                        $('#<%=lblCustDisplayPortNo.ClientID %>').hide();
                        $('#<%=txtCustDisplayPortNo.ClientID %>').hide();
                    }
                });
            });
            jQuery(document).ready(function () {

                jQuery('#<%=txtSetting.ClientID%>').keyup(function () {
                    $(this).val($(this).val().toUpperCase());
                    if (document.getElementById("<%=txtSetting.ClientID%>").value == "A" || document.getElementById("<%=txtSetting.ClientID%>").value == "B") {

                    }
                    else {
                        ShowPopupMessageBox("Please Enter Only A OR B IN Setting");
                        $(this).val('');
                    }

                });
            });
            $(document).ready(function () {
                $('#<%=txtSetting.ClientID%>').keypress(function (e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
            });
            $(document).ready(function () {
                $('#<%= ChkMeCode.ClientID %>').change(function () {
                    if (this.checked) {
                        $('#<%=txtMeCode.ClientID %>').show();
                    }
                    else {
                        $('#<%=txtMeCode.ClientID %>').hide();
                    }
                });
            });
            $('#<%= rbSerial.ClientID %>').change(function () {
                var checked = $(this).attr('checked', true);
                if (checked) {
                    $('#<%=txtSerial.ClientID %>').show();
                }
                else {
                    $('#<%=txtSerial.ClientID %>').hide();
                }
            });
            $('#<%= rbPrinter.ClientID %>').change(function () {
                var checked = $(this).attr('checked', true);
                if (checked) {
                    $('#<%=txtSerial.ClientID %>').hide();
                }
                else {

                    $('#<%=txtSerial.ClientID %>').show();
                }
            });
        }
        function Attachedweightmachinesettingtrue() {

            $('#<%=ddlCommPortNo.ClientID %>').prop("disabled", false);
            $('#<%=ddlCommPortNo.ClientID %>').trigger("liszt:updated");
            $('#<%=txtPortNo.ClientID %>').prop("disabled", false);
            $('#<%=ddlBaud.ClientID %>').prop("disabled", false);
            $('#<%=ddlBaud.ClientID %>').trigger("liszt:updated");
            $('#<%=txtDataBites.ClientID %>').prop("disabled", false);
            $('#<%=txtParity.ClientID %>').prop("disabled", false);
            $('#<%=txtStopBits.ClientID %>').prop("disabled", false);
        }
        function Attachedweightmachinesettingfalse() {
            $('#<%=ddlCommPortNo.ClientID %>').prop("disabled", true);
            $('#<%=ddlCommPortNo.ClientID %>').trigger("liszt:updated");
            $('#<%=txtPortNo.ClientID %>').prop("disabled", true);
            $('#<%=ddlBaud.ClientID %>').prop("disabled", true);
            $('#<%=ddlBaud.ClientID %>').trigger("liszt:updated");
            $('#<%=txtDataBites.ClientID %>').prop("disabled", true);
            $('#<%=txtParity.ClientID %>').prop("disabled", true);
            $('#<%=txtStopBits.ClientID %>').prop("disabled", true);
        }
        function ShowPopupPosSetting(name) {
            $("#divPosSetting").dialog({
                title: name,
                width: 700,
                buttons: {
                    New: function () {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkNew', '');
                    },
                    Update: function () {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkUpdate', '');
                    }

                },
                modal: true
            });
        }
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        if (row.rowIndex % 2 == 0) {
                            row.style.backgroundColor = "#C2D69B";
                        }
                        else {
                            row.style.backgroundColor = "white";
                        }
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function fncInitializeSaveDialog(SaveMsg) {
            try {

                $('#<%=lblSave.ClientID %>').text(SaveMsg);
                    $("#Save").dialog({
                        resizable: false,
                        height: 130,
                        width: 325,
                        modal: true,
                        title: "Enterpriser Web"
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            function fncSaveDialogClose() {
                $("#Save").dialog('close');
                
            }
         function fnclocationchange() {
            try {
              
                setTimeout(function () {
                    $("#<%=ddlFloorName.ClientID %>").trigger("liszt:open");
                }, 50);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
         }
        function fncfloorchange() {
            try {
              
                setTimeout(function () {
                    $("#<%=ddlWareHouseName.ClientID %>").trigger("liszt:open");
                }, 50);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
         }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'VBTerminal');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "VBTerminal";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container">
       <div class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a href="frmVbTerminalView.aspx">VB Terminal view</a></li>
                <li class="active-page">VB Terminal</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <div id="divMainContainer">
                    <asp:HiddenField ID="hdnNewUser" runat="server" />
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-5">
                        <div class="container-group-small">
                            <div class="container-control">

                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Terminal Code"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTerminalCode" runat="server" MaxLength="2" CssClass="form-control-res"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text="Terminal Name"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTerminalName" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="Terminal Type"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlTerminalType" runat="server" CssClass="form-control-res">

                                            <asp:ListItem Text="Cashier" Value="Cashier" />
                                            <asp:ListItem Text="POS" Value="POS Terminal" />
                                            <asp:ListItem Text="ESTIMATION" Value="ESTIMATION" />
                                            <asp:ListItem Text="BACKOFFICE" Value="BACKOFFICE" />
                                            <asp:ListItem Text="EXCHANGE" Value="EXCHANGE" />
                                            <asp:ListItem Text="ADMIN" Value="ADMIN" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Computer Name"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtComputername" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                         
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Location Code"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlLocationCode" runat="server" onchange="fnclocationchange();return false;" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label8" runat="server" Text="Floor Name"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlFloorName" runat="server" onchange="fncfloorchange();return false;" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label9" runat="server" Text="WareHouse Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:DropDownList ID="ddlWareHouseName" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label3" runat="server" Text="BarCode Attended"></asp:Label>
                                    </div>
                                    <div class="col-md-1">
                                        <asp:CheckBox ID="chkBarcodeAttended" runat="server" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label ID="lblBarcodeAttended" runat="server" Text="BarCode Port No"></asp:Label>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtBarcodeAttendedPortNo" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="control-group-single-res">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label4" runat="server" Text="Cust.Display Attached"></asp:Label>
                                    </div>
                                    <div class="col-md-1">
                                        <asp:CheckBox ID="ChkCustDisplayAttended" runat="server" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Label ID="lblCustDisplayPortNo" runat="server" Text="Cust.Display Port No"></asp:Label>
                                    </div>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtCustDisplayPortNo" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label11" runat="server" Text="Barcode File Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBarcodeFileName" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="No Of Labels"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtNoOfLabels" runat="server" onkeypress="return isNumberKey(event)" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label13" runat="server" Text="Printer Attached"></asp:Label>
                                    </div>
                                    <div class="col-md-1">
                                        <asp:CheckBox ID="ChkPrinterAttached" runat="server" />
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtPrinterattached" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Till Amount"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtTillAmount" runat="server" Text="0" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label15" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="col-md-4">
                                        <asp:Label ID="Label16" runat="server" Text="ME Code"></asp:Label>
                                    </div>
                                    <div class="col-md-1">
                                        <asp:CheckBox ID="ChkMeCode" runat="server" />
                                    </div>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtMeCode" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label17" runat="server" Text="Setting"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSetting" runat="server" MaxLength="1" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label18" runat="server" Text="Bill Print Count"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBillPrintCount" runat="server" onkeypress="return isNumberKey(event)" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>


                            </div>
                            <div class="control-group-single-res">
                                <div class="col-md-5">
                                    <asp:Label ID="Label27" runat="server" Text="Show Zero & Negative Stock" Font-Bold="true"></asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:CheckBox ID="chkshowZeroNegativeStock" runat="server" />
                                </div>
                                <div class="col-md-3">
                                    <asp:LinkButton ID="lnkpos" runat="server" class="button-blue" Width="100px"><i class="icon-play"></i>POS</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="container-group-small">
                            <div class="container-control" style="border-style: solid; border-width: 2px; padding: 5px;">

                                <div class="header">
                                    Printer Type
                                </div>
                                <div class="control-group-single-res">
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rb40ColDriver" runat="server" Text="40 Col Driver" GroupName="Printertype"></asp:RadioButton>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rb40ColReceipt" runat="server" Text="40 Col Receipt" GroupName="Printertype"></asp:RadioButton>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rb80ColDotMatrix" runat="server" Text="80 Col Dot Matrix" GroupName="Printertype"></asp:RadioButton>
                                    </div>



                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rb60ColDriver" runat="server" Text="60 Col Driver" GroupName="Printertype"></asp:RadioButton>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rb40ColThermal" runat="server" Text="40 Col Thermal" GroupName="Printertype"></asp:RadioButton>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:RadioButton ID="rbCrystalReport" runat="server" Text="Crystal Report" GroupName="Printertype"></asp:RadioButton>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="padding">
                            <div class="container-group-small">
                                <div class="container-control" style="border-style: solid; border-width: 2px; padding: 5px;">

                                    <div class="header">
                                        Cash Drawer Interface
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rbPrinter" runat="server" Text="Printer" GroupName="CashDrawerInterface"></asp:RadioButton>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:RadioButton ID="rbSerial" runat="server" Text="Serial" GroupName="CashDrawerInterface"></asp:RadioButton>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtSerial" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="padding">
                            <div class="container-group-small">
                                <div class="container-control" style="border-style: solid; border-width: 2px; padding: 5px;">

                                    <div class="header">
                                        Customer Display Type
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rbPosiflex" runat="server" Text="Posiflex" GroupName="CustomerDisplayType"></asp:RadioButton>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rbPartnerTex" runat="server" Text="Partner Tex" GroupName="CustomerDisplayType"></asp:RadioButton>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rbDsp800" runat="server" Text="Dsp 800" GroupName="CustomerDisplayType"></asp:RadioButton>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:RadioButton ID="rbIBM" runat="server" Text="IBM" GroupName="CustomerDisplayType"></asp:RadioButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="padding">
                            <div class="container-group-small">
                                <div class="container-control" style="border-style: solid; border-width: 2px; padding: 5px;">

                                    <div class="header">
                                        Weight Machine Setting
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-1">
                                            <asp:CheckBox ID="cbAttachedweightmachinesetting" runat="server" />
                                        </div>
                                        <div class="col-md-9">
                                            <b>Attached (Yes/No)</b>
                                        </div>
                                        <div class="container-control" style="border-style: solid; border-width: 2px; padding: 5px;">
                                            <div class="col-md-6">
                                                <div class="label-left">
                                                    <asp:Label ID="Label23" runat="server" Text="Comm Port"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlCommPortNo" runat="server" CssClass="form-control-res">

                                                        <asp:ListItem Text="COM1" Value="1" />
                                                        <asp:ListItem Text="COM2" Value="2" />
                                                        <asp:ListItem Text="COM3" Value="3" />
                                                        <asp:ListItem Text="COM4" Value="4" />

                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="label-left">
                                                    <asp:Label ID="Label19" runat="server" Text="Port No"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtPortNo" runat="server" Text="1" onMouseDown="return false" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="label-left">
                                                    <asp:Label ID="Label24" runat="server" Text="Baud"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddlBaud" runat="server" CssClass="form-control-res">
                                                        <asp:ListItem Text="9600" Value="9600" />
                                                        <asp:ListItem Text="19200" Value="19200" />
                                                        <asp:ListItem Text="38400" Value="38400" />
                                                        <asp:ListItem Text="57600" Value="57600" />
                                                        <asp:ListItem Text="115200" Value="115200" />
                                                        <asp:ListItem Text="320400" Value="320400" />
                                                        <asp:ListItem Text="460800" Value="460800" />
                                                        <asp:ListItem Text="921600" Value="921600" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="label-left">
                                                    <asp:Label ID="Label20" runat="server" Text="Data Bites"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDataBites" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="label-left">
                                                    <asp:Label ID="Label21" runat="server" Text="Partity"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtParity" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                </div>

                                            </div>
                                            <div class="col-md-6">

                                                <div class="label-left">
                                                    <asp:Label ID="Label22" runat="server" Text="Stop Bits"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtStopBits" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <b>Example:-9600,n,8,1</b>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <asp:Label ID="Label25" runat="server" Text="Start PT"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtStartPT" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:Label ID="Label26" runat="server" Text="End PT"></asp:Label>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtEndPT" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return ValidateForm()" OnClick="lnkSave_Click" Style="float: right; margin-right: 23px" Width="100px"><i class="icon-play"></i>Save</asp:LinkButton>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>

                <div id="divPosSetting" class="container-group-full" style="display: none">
                    <asp:GridView ID="gvPosSetting" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="FunctionCode">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:BoundField DataField="FunctionCode" HeaderText="Function Code"></asp:BoundField>
                            <asp:BoundField DataField="FunctionName" HeaderText="Function Name"></asp:BoundField>
                            <asp:TemplateField HeaderText="Active">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPossetting" runat="server" Checked='<%# Eval("Allow") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click" Style="display: block" Width="100px"><i class="icon-play"></i>Update</asp:LinkButton>
                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" OnClick="lnkNew_Click" Style="display: block" Width="100px"><i class="icon-play"></i>New</asp:LinkButton>
                </div>
                <asp:Button ID="btnEdit" runat="server" CssClass="hiddencol" OnClick="LoadData_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="hiddencol">
            <div id="Save">
                <div>
                    <asp:Label ID="lblSave" runat="server"></asp:Label>
                    <div class="dialog_center">
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                            OnClientClick="fncSaveDialogClose()" OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
