﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCompanyMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmCompanyMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }
          
        </style>
    <script type="text/javascript">
        function pageLoad() {
             if ($('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                   $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            $('#divState').hide();
            $("#<%=ddlBillPrint.ClientID%>").attr('disabled', true);
            $('#<%=ddlBillPrint.ClientID %>').trigger("liszt:updated");

            $("#<%=ddlTaxTaken.ClientID%>").attr('disabled', true);
            $('#<%=ddlTaxTaken.ClientID %>').trigger("liszt:updated");
            $('#divRoundoff').show();
            $('#Sales').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('#Purchase').find('input, textarea, button, select').attr('disabled', 'disabled');
            $('#Credit').find('input, textarea, button, select').attr('disabled', 'disabled');
            $("#<%=chk1.ClientID %>").prop("checked", false);
            $("#<%=chk25.ClientID %>").prop("checked", false);
            $("#<%=chk50.ClientID %>").prop("checked", false);
            $("#<%=chk75.ClientID %>").prop("checked", false);

            $("#<%=chk1.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                }
                else {
                    $("#<%=chk25.ClientID %>").removeAttr("disabled");
                    $("#<%=chk50.ClientID %>").removeAttr("disabled");
                    $("#<%=chk75.ClientID %>").removeAttr("disabled");
                }
            });
            $("#<%=chk25.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                }
                else {
                    $("#<%=chk1.ClientID %>").removeAttr("disabled");
                    $("#<%=chk50.ClientID %>").removeAttr("disabled");
                    $("#<%=chk75.ClientID %>").removeAttr("disabled");
                }
            });
            $("#<%=chk50.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                }
                else {
                    $("#<%=chk1.ClientID %>").removeAttr("disabled");
                    $("#<%=chk25.ClientID %>").removeAttr("disabled");
                    $("#<%=chk75.ClientID %>").removeAttr("disabled");
                }
            });
            $("#<%=chk75.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                    $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                }
                else {
                    $("#<%=chk1.ClientID %>").removeAttr("disabled");
                    $("#<%=chk25.ClientID %>").removeAttr("disabled");
                    $("#<%=chk50.ClientID %>").removeAttr("disabled");
                }
            });
            $("#<%=chkRoundOff.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $('#divRoundoff').show();
                }
                else {
                    $('#divRoundoff').hide();
                    $("#<%=chk1.ClientID %>").prop("checked", false);
                    $("#<%=chk25.ClientID %>").prop("checked", false);
                    $("#<%=chk50.ClientID %>").prop("checked", false);
                    $("#<%=chk75.ClientID %>").prop("checked", false);
                    $("#<%=chk25.ClientID %>").removeAttr("disabled");
                    $("#<%=chk50.ClientID %>").removeAttr("disabled");
                    $("#<%=chk75.ClientID %>").removeAttr("disabled");
                    $("#<%=chk1.ClientID %>").removeAttr("disabled");
                }
            });
            //surya 15112021 not to change branchdropdown
            $('#<%=ddlBranchCode.ClientID%>').prop("disabled", true);
            $('#<%=ddlBranchCode.ClientID%>').trigger("liszt:updated");


            $('#<%=ddlHqCode.ClientID%>').prop("disabled", true);
            $('#<%=ddlHqCode.ClientID%>').trigger("liszt:updated");
           

        }
    </script>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtCompanyCode.ClientID%>").value == "") {
                Show = Show + '  Please Enter Company Code';
                document.getElementById("<%=txtCompanyCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlHqCode.ClientID%>").value == "") {
                Show = Show + 'Please Select HQ Code';
                document.getElementById("<%=ddlHqCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlBranchCode.ClientID%>").value == "") {
                Show = Show + '<br />Please Select Branch Code';
                document.getElementById("<%=ddlBranchCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=ddlState.ClientID%>").value == "") {
                Show = Show + '<br />Please Select State Code';
                document.getElementById("<%=ddlState.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtName.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Company Name';
                document.getElementById("<%=txtName.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtAddress1.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter The Address';
                document.getElementById("<%=txtAddress1.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtCountry.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter The Country';
                document.getElementById("<%=txtAddress1.ClientID %>").focus();
            }
           <%-- if (document.getElementById("<%=txtEmail.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter The E-Mail Address';
                document.getElementById("<%=txtEmail.ClientID %>").focus();
            }--%>  //Vijay 20190319
            else {
                var emailPat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                var emailid = document.getElementById("<%=txtEmail.ClientID %>").value;
                var matchArray = emailid.match(emailPat);
                if (matchArray == null) {

                    document.getElementById("<%=txtEmail.ClientID %>").focus();
                    Show = Show + '<br />  Enter Valid E-Mail Address';
                }
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;              
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        ValidateForm();
                        <%--//$('#<%= lnkSave.ClientID %>').click();--%> 
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 119) {
                    $('#<%= lnkClose.ClientID %>').click();
                        e.preventDefault();
                    }


        }
    };

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
        $("#<%=chkRoundOff.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $('#divRoundoff').show();
                    }
                    else {
                        $('#divRoundoff').hide();
                        $("#<%=chk1.ClientID %>").prop("checked", false);
                         $("#<%=chk25.ClientID %>").prop("checked", false);
                         $("#<%=chk50.ClientID %>").prop("checked", false);
                         $("#<%=chk75.ClientID %>").prop("checked", false);
                         $("#<%=chk25.ClientID %>").removeAttr("disabled");
                         $("#<%=chk50.ClientID %>").removeAttr("disabled");
                         $("#<%=chk75.ClientID %>").removeAttr("disabled");
                         $("#<%=chk1.ClientID %>").removeAttr("disabled");
                     }
                });
                $("#<%=chk1.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                     }
                     else {
                         $("#<%=chk25.ClientID %>").removeAttr("disabled");
                         $("#<%=chk50.ClientID %>").removeAttr("disabled");
                         $("#<%=chk75.ClientID %>").removeAttr("disabled");
                     }
                 });
                $("#<%=chk25.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                     }
                     else {
                         $("#<%=chk1.ClientID %>").removeAttr("disabled");
                         $("#<%=chk50.ClientID %>").removeAttr("disabled");
                         $("#<%=chk75.ClientID %>").removeAttr("disabled");
                     }
                 });
                $("#<%=chk50.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk75.ClientID %>").attr("disabled", "disabled");
                     }
                     else {
                         $("#<%=chk1.ClientID %>").removeAttr("disabled");
                         $("#<%=chk25.ClientID %>").removeAttr("disabled");
                         $("#<%=chk75.ClientID %>").removeAttr("disabled");
                     }
                 });
                $("#<%=chk75.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chk1.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk25.ClientID %>").attr("disabled", "disabled");
                         $("#<%=chk50.ClientID %>").attr("disabled", "disabled");
                     }
                     else {
                         $("#<%=chk1.ClientID %>").removeAttr("disabled");
                         $("#<%=chk25.ClientID %>").removeAttr("disabled");
                         $("#<%=chk50.ClientID %>").removeAttr("disabled");
                     }
                });
        $('#<%=txtCountry.ClientID %>').change(function () {
            if ($('#<%=txtCountry.ClientID %>').val() != "INDIA" ) {
                $('#divState').show();
                $('#divStateddl').hide();
            }
            else { 
                $('#divState').hide();
                $('#divStateddl').show();
            }
        });
            });
             function clearForm() {
                 $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                 $(':checkbox, :radio').prop('checked', false);
                 $("input").prop('checked', false);
                 $('#<%=txtCompanyCode.ClientID %>').focus();
                 return false;
             }
        
    </script>
          <script type="text/javascript">
              var d1;
              var d2;
              var Opendate;
              var dur;
              function fncGetUrl() {
                  fncSaveHelpVideoDetail('', '', 'CompanyMaster');
              }
              function fncOpenvideo() {

                  document.getElementById("ifHelpVideo").src = HelpVideoUrl;

                  var Mode = "CompanyMaster";
                  var d = new Date($.now());
                  Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                  d1 = new Date($.now()).getTime();



                  $("#dialog-Open").dialog({
                      autoOpen: true,
                      resizable: false,
                      height: "auto",
                      width: 1093,
                      modal: true,
                      dialogClass: "no-close",
                      buttons: {
                          Close: function () {
                              $(this).dialog("destroy");
                              var d = new Date($.now());
                              var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                              d2 = new Date($.now()).getTime();
                              var Diff = Math.floor((d2 - d1) / 1000);
                              //alert(Diff);
                              if (Diff >= 60) {
                                  fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                              }

                          }
                      }
                  });
              }


          </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Company Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" class="EnableScroll">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="top-container" style="border: 1px solid #d8d9d8">
                        <div class="top-container-header">
                            General
                        </div>
                        <div class="top-container-detail">
                            <div class="left-top-container">
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="Company Code"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCompanyCode" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label1" runat="server" Text="HQ Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlHqCode" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text="Branch Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlBranchCode" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text="Name"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtName" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text="Address"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-right" style="float: right">
                                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="Country"></asp:Label><span class ="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" Text="City"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="State/Province"></asp:Label>
                                        </div>
                                        <div class="label-right" id="divStateddl">
                                            <%-- <asp:TextBox ID="txtState" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                          <div class="label-right" id="divState">
                                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Postal Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPostal" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="E-Mail"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="WebPage"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtWebpage" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text="Phone"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label14" runat="server" Text="Fax"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="right-top-container">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label16" runat="server" Text="Date Format"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDateFormat" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text="Time"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTime" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label36" runat="server" Text="Default Currency"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDefaultCurrency" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label37" runat="server" Text="Currency Sign"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCurrencySign" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label18" runat="server" Text="Business Reg No"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBussinessRegNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label15" runat="server" Text="Tax type & Perc"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 30%">
                                            <asp:TextBox ID="txtTaxType" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                        <div class="label-right" style="width: 28%; margin-left: 1%">
                                            <asp:TextBox ID="txtTaxPrc" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label35" runat="server" Text="GSTIN"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTinNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label19" runat="server" Text="Bill Message 1"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBillMessage1" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label38" runat="server" Text="Bill Message 2"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtBillMessage2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label39" runat="server" Text="Display msg 1"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtDisplayMessege1" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label40" runat="server" Text="Display msg 2 "></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtDisplayMessage2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label20" runat="server" Text="Barcode Comport"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBarcodeComport" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label21" runat="server" Text="NextMailID"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtNextMailID" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label22" runat="server" Text="NextBankTransNo"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtNextBankTransNo" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label23" runat="server" Text="DL No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDLNO" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-container display_none">
                        <div class="bottom-left-container" id="Sales">
                            <div class="bottom-left-container-header">
                                Sales
                            </div>
                            <div class="bottom-left-container-detail">
                                <div class="left-bottom-container">
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label24" runat="server" Text="Next Invoice #"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextInv" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label25" runat="server" Text="Next Receipt No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextReceiptNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label26" runat="server" Text="Max Discount %"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMaxDis" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label27" runat="server" Text="Price Change %"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtPriceChange" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label28" runat="server" Text="Next PO No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextPONO" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label29" runat="server" Text="Tax IN"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtTaxIn" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label30" runat="server" Text="Max Cost %"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMaxCost" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label31" runat="server" Text="Expired Time"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtExpiredTime" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:Label ID="Label32" runat="server" Text="BillPrint(Y/N)"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:DropDownList ID="ddlBillPrint" runat="server" CssClass="form-control-res">
                                                    <asp:ListItem Text="Y" Value="Y" />
                                                    <asp:ListItem Text="N" Value="N" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left">
                                                <asp:Label ID="Label33" runat="server" Text="Tax Incl/Excl"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:DropDownList ID="ddlTaxTaken" runat="server" CssClass="form-control-res">
                                                    <asp:ListItem Text="I" Value="I" />
                                                    <asp:ListItem Text="E" Value="E" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-right-container">
                            <div class="bottom-right-container-left" id="Purchase">
                                <div class="bottom-right-container-header">
                                    Purchase
                                </div>
                                <div class="bottom-right-container-detail">
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 100%">
                                            <div class="label-left">
                                                <asp:Label ID="Label34" runat="server" Text="Next Return #"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextReturn" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 100%">
                                            <div class="label-left">
                                                <asp:Label ID="Label41" runat="server" Text="Next Payment #"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextPayment" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-right-container-right" id="Credit">
                                <div class="bottom-right-container-header">
                                    Credit Bill
                                </div>
                                <div class="bottom-right-container-detail">
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 100%">
                                            <div class="label-left">
                                                <asp:Label ID="Label42" runat="server" Text="Next Invoice #"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtNextInvoice" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-right-container" style="margin-top: 7px">
                            <div class="bottom-right-container-left">
                                <div class="bottom-right-container-header">
                                    RoundOff/Paymode Disc
                                </div>
                                <div class="bottom-right-container-detail">
                                    <div class="control-group-split">
                                        <div class="control-group-left">
                                            <div class="label-left" style="width: 80%">
                                                <asp:Label ID="Label43" runat="server" Text="Round Off"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 20%">
                                                <asp:CheckBox ID="chkRoundOff" runat="server" Checked="true" />
                                            </div>
                                        </div>
                                        <div class="control-group-right">
                                            <div class="label-left" style="width: 80%">
                                                <asp:Label ID="Label46" runat="server" Text="Paymode Disc"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 20%">
                                                <asp:CheckBox ID="chkPaymodeDisc" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="control-group-split" id="divRoundoff">
                                        <div class="control-group-left">
                                            <div class="label-left">
                                                <asp:CheckBox ID="chk1" runat="server" Text="1Re"></asp:CheckBox>
                                            </div>
                                            <div class="label-right">
                                                <asp:CheckBox ID="chk25" runat="server" Text="25P" />
                                            </div>
                                            <div class="label-left">
                                                <asp:CheckBox ID="chk50" runat="server" Text="50P" />
                                            </div>
                                            <div class="label-right">
                                                <asp:CheckBox ID="chk75" runat="server" Text="75P" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-right-container-right">
                                <div class="bottom-right-container-header">
                                    EOD
                                </div>
                                <div class="bottom-right-container-detail">
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 100%">
                                            <div class="label-left">
                                                <asp:Label ID="Label45" runat="server" Text="Next EOD Date"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtEODDate" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-container">
                        <div class="bottom-left-container display_none">
                            <div class="bottom-left-container-header">
                                Member
                            </div>
                            <div class="bottom-left-container-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left" style="width: 80%">
                                            <asp:Label ID="Label44" runat="server" Text="Member Module"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 20%">
                                            <asp:CheckBox ID="chkMemberModule" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left" style="width: 80%">
                                            <asp:Label ID="Label47" runat="server" Text="Allow Multiple Discount"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 20%">
                                            <asp:CheckBox ID="chkAllowMultipleDis" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bottom-right-container">
                            <div class="bottom-right-container-header">
                                Last Updated
                            </div>
                            <div class="bottom-right-container-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left" style="width: 25%">
                                            <asp:Label ID="Label48" runat="server" Text="Date : "></asp:Label>
                                        </div>
                                        <div class="label-right" style="float: left; width: 74%">
                                            <span id="lblLastUpdateDate" runat="server" style="color: Blue">30/05/2016</span>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left" style="width: 25%">
                                            <asp:Label ID="Label49" runat="server" Text="User : "></asp:Label>
                                        </div>
                                        <div class="label-right" style="float: left; width: 74%">
                                            <span id="lblLastUpdateUser" runat="server" style="color: Blue">aa</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkRefresh" runat="server" class="button-red" OnClick="lnkRefresh_Click" Visible="false">Refresh</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                            OnClientClick="return ValidateForm();">Save(F4)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()" Visible="false">Clear(F6)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
