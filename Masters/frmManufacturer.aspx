﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmManufacturer.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmManufacturer" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 60px;
            max-width: 60px;
            text-align: center !important;
        }
        
        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 108px;
            max-width: 108px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 140px;
            max-width: 140px;            /*22112022 musaraf*/
            word-break: break-all;  
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 120px;
            max-width: 120px;           /*22112022 musaraf*/
            word-break: break-all;  

        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 114px;
            max-width: 114px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">


        function ValidateForm() {
            if ($('#<%=txtManufacturerCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Manufacture");
                return false;
            }
            if (!($('#<%=txtManufacturerCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Manufacture");
                return false;
            }
            var Show = '';
            var ManufacturerCode = $.trim($('#<%=txtManufacturerCode.ClientID %>').val());         //22112022 musaraf
            var ManufacturerName = $.trim($('#<%=txtManufacturerName.ClientID %>').val());

            if (ManufacturerCode == "") {
                Show = Show + 'Please Enter Manufacturer Code';
                document.getElementById("<%=txtManufacturerCode.ClientID %>").focus();
            }

            if (ManufacturerName == "") {
                Show = Show + '<br />Please Enter Manufacturer Name';
                document.getElementById("<%=txtManufacturerName.ClientID %>").focus();
            }
            if (document.getElementById("<%=TxtEmail.ClientID%>").value != '') {

                if (!validateEmail(document.getElementById("<%=TxtEmail.ClientID%>").value)) {
                    Show = Show + '<br />Please Enter Valid EmailId';
                    document.getElementById("<%=TxtEmail.ClientID %>").focus();
                }
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test($email);
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function Manufacturer_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Manufacture");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });


        }
        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Manufacture");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function DisplayDetails(row) {

            $('#<%=txtManufacturerCode.ClientID %>').prop("disabled", true);
            $('#<%=txtManufacturerCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtManufacturerName.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtaddress1.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
            $('#<%=txtaddress2.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            $('#<%=txtaddress3.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
            $('#<%=txtpincode.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
            $('#<%=TxtEmail.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));
            $('#<%=txtphone.ClientID %>').val($("td", row).eq(10).html().replace(/&nbsp;/g, ''));
            $('#<%=txtFax.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));


        }




    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
             $("select").chosen({ width: '100%' });
             $(document).keyup(function (e) {

                 if (e.keyCode == 27) {
                     window.parent.jQuery('#popupManufacture').dialog('close');
                     window.parent.$('#popupManufacture').remove();

                 }
             });

             $('#<%=txtManufacturerCode.ClientID %>').focus();
            $('#<%=txtManufacturerCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmManufacturer.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtManufacturerCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtManufacturerCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtManufacturerCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtManufacturerName.ClientID %>').val(strarray[1]);
                            $('#<%=txtaddress1.ClientID %>').val(strarray[2]);
                            $('#<%=txtaddress2.ClientID %>').val(strarray[3]);
                            $('#<%=txtaddress3.ClientID %>').val(strarray[4]);
                            $('#<%=txtpincode.ClientID %>').val(strarray[5]);
                            $('#<%=TxtEmail.ClientID %>').val(strarray[6]);
                            $('#<%=txtphone.ClientID %>').val(strarray[7]);
                            $('#<%=txtFax.ClientID %>').val(strarray[8]);


                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtManufacturerName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmManufacturer.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });


                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmManufacturer.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                         __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                         return false;
                     },

                     minLength: 1
                });

            ////$('input[type=text]').bind('change', function () {
            ////    if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {
            ////        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
            ////        this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
            ////    }
            ////});

                 }
                 function clearForm() {
                     $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                     $(':checkbox, :radio').prop('checked', false);
                     $('#<%=txtManufacturerCode.ClientID %>').focus();
                        return false;
                    }
                    function disableFunctionKeys(e) {
                        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                            if (e.keyCode == 115) {
                                if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                    $('#<%= lnkSave.ClientID %>').click();
                                 e.preventDefault();
                             }
                             else if (e.keyCode == 117) {
                                 $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                   <%--      else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                        }
                    };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Manufacturer');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Manufacturer";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow:hidden">
      
  <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Manufacturer</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
       
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />

                <div class="container-group">
                    <div class="container-control">
                        <div class="container-left">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label1" runat="server" Text="Manufacturer Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtManufacturerCode" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label2" runat="server" Text="Manufacturer Name"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtManufacturerName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label4" runat="server" Text="Address"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtaddress1" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtaddress2" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtaddress3" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <div class="container-right">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label7" runat="server" Text="Pin Code"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtpincode" MaxLength="20" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label6" runat="server" Text="Email"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label8" runat="server" Text="Phone"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtphone" MaxLength="20" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:Label ID="Label5" runat="server" Text="Fax"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtFax" MaxLength="20" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="button-contol" style="margin-left: 175px">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                    OnClientClick="return ValidateForm()" ><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                  ><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display:none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Manufacturer Name To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" OnClick="lnkSearchGrid_Click" Style="visibility: hidden"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                             <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    
                     <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad" style="overflow-x: scroll; overflow-y: hidden;">         <%--22112022 musaraf--%>
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">Manufacturer Code
                                                    </th>
                                                    <th scope="col">ManufacturerName
                                                    </th>
                                                    <th scope="col">Address 1
                                                    </th>
                                                    <th scope="col">Address 2
                                                    </th>
                                                    <th scope="col">Address 3
                                                    </th>
                                                    <th scope="col">PinCode
                                                    </th>
                                                    <th scope="col">EMail
                                                    </th>
                                                    <th scope="col">Phone
                                                    </th>
                                                    <th scope="col">Fax
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 210px; width: 1470px; background-color: aliceblue;">
                    <asp:GridView ID="gvManufacturer" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                        ShowHeaderWhenEmpty="true"  oncopy="return false" DataKeyNames="ManufacturerCode" CssClass="pshro_GridDgn grdLoad">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Manufacturer Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Manufacturer_Delete(this);  return false;"
                                        CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                        ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField DataField="RowNumber" HeaderText="Serial No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                            <asp:BoundField DataField="ManufacturerCode" HeaderText="Manufacturer Code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                            <asp:BoundField DataField="ManufacturerName" HeaderText="ManufacturerName" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                            <asp:BoundField DataField="Address1" HeaderText="Address 1" ></asp:BoundField>
                            <asp:BoundField DataField="Address2" HeaderText="Address 2" ></asp:BoundField>
                            <asp:BoundField DataField="Address3" HeaderText="Address 3" ></asp:BoundField>
                            <asp:BoundField DataField="PinCode" HeaderText="PinCode" ></asp:BoundField>
                            <asp:BoundField DataField="EMail" HeaderText="EMail" ></asp:BoundField>
                            <asp:BoundField DataField="Phone" HeaderText="Phone" ></asp:BoundField>
                            <asp:BoundField DataField="Fax" HeaderText="Fax" ></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User" ></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date" ></asp:BoundField>


                        </Columns>
                    </asp:GridView>
                                            </div>
                                    <//div>
                                </div>
                            </div>
                        </div>
                     </div>
                    <ups:PaginationUserControl runat="server" ID="ManufacturerPaging" OnPaginationButtonClick="Manufacturer_PaginationButtonClick" />
                </div>
                <div class="hiddencol"> 
                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" /> 
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div> 
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
