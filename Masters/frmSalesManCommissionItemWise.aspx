﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmSalesManCommissionItemWise.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmSalesManCommissionItemWise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 58px;
            max-width: 58px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 97px;
            max-width: 97px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 180px;
            max-width: 180px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 165px;
            max-width: 165px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 123px;
            max-width: 123px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript" language="Javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkUpdate.ClientID %>').css("display", "none");
             }
                $("#<%= txtvValidFrom.ClientID %>, #<%= txtvValidTo.ClientID %>, #<%= txtAFromDate.ClientID %>, #<%= txtAToDate.ClientID %>, input[id*=txtValidFrom],input[id*=txtValidTo]").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");

                <%--$("#<%= txtvValidFrom.ClientID %>, #<%= txtvValidTo.ClientID %>, #<%= txtAFromDate.ClientID %>, #<%= txtAToDate.ClientID %>, input[id*=txtValidFrom],input[id*=txtValidTo]").on('keydown', function () {
                    return false;

                });--%>

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncApplyAllZero() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    var comm = $('#<%=txtApplyCommissionAll.ClientID %>').val();
                    if (comm == '') {
                        ShowPopupMessageBox("Enter Commission (%)");
                    } else {

                        $("#<%=gvItemWiseSales.ClientID %> [id*=txtComm]").val(comm);
                    }

                }


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncApplyAllDate() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {

                    $("#<%=gvItemWiseSales.ClientID %> [id*=txtValidFrom]").val($('#<%=txtAFromDate.ClientID %>').val());
                    $("#<%=gvItemWiseSales.ClientID %> [id*=txtValidTo]").val($('#<%=txtAToDate.ClientID %>').val());

                }


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncCommFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=lnkAdd.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKeyTextBox(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //ShowPopupMessageBox($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.next().find('input[id *= txtComm]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtInventoryCode.ClientID %>').val(itemcode);
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }
        //Get BatchStatus
        function fncInventoryCodeEnterkey_Master() {
            try {
                fncGetBatchStatus_Master($('#<%=txtInventoryCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }

        }

        //Get Inventory Detail
        function fncGetBatchStatus() {
            try {
                fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }

        //Assign values to text box
        function fncAssignInventoryDetailToControls_Master(inventorydata) {
            try {
                //ShowPopupMessageBox(inventorydata[0]);
                $('#<%=txtItemNameAdd.ClientID%>').val(inventorydata[0]);
                $('#<%=hidvendorcode.ClientID %>').val(inventorydata[9]);



                //ShowPopupMessageBox(inventorydata[1]);
                $('#<%=txtvDepartment.ClientID %>').val(inventorydata[1]);
                $('#<%=txtvCategory.ClientID%>').val(inventorydata[2]);
                $('#<%=txtvBrand.ClientID%>').val(inventorydata[3]);
                $('#<%=txtvPrice.ClientID%>').val(inventorydata[5]);
                $('#<%=txtvCost.ClientID%>').val(inventorydata[6]);
                $('#<%=txtvValidFrom.ClientID%>').focus();



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncValidateClearSure() {
            try {
                debugger;
                fncShowConfirmClearMessage();
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //ShowPopupMessageBox("message");
                InitializeDialog1();
                $("#StockUpdatePosting").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncShowConfirmClearMessage() {
            try {
                InitializeDialogClear();
                $("#ClearSure").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogClear() {
            try {
                $("#ClearSure").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmClearSure() {
            try {
                $("#ClearSure").dialog('close');
                clearFormAll();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function ClearSure() {
            try {
                $("#ClearSure").dialog('close');
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncValidateViewCommission() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {

                    return true;
                }
                else {

                    fncShowDataLossMessage();
                    return false;

                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        function fncShowDataLossMessage() {
            try {
                InitializeDialogDataLoss();
                $("#ConfirmDataLoss").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDataLoss() {
            try {
                $("#ConfirmDataLoss").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseDataLoss() {
            try {
                $("#ConfirmDataLoss").dialog('close');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowShowPopupMessageBoxNoItemsMessage() {
            try {
                InitializeDialogShowPopupMessageBoxNoItems();
                $("#ShowPopupMessageBoxNoItems").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseShowPopupMessageBoxNoItemsDialog() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogShowPopupMessageBoxNoItems() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvItemWiseSales.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    fncShowConfirmSaveMessage();

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncValidateAdd() {
            try { 
                var itemcode = $('#<%=txtInventoryCode.ClientID %>').val();
                var itemdesc = $('#<%=txtItemNameAdd.ClientID %>').val();

                if (itemcode = "") {
                    ShowPopupMessageBox("Please Enter Item Code.");
                    return false;
                }
                else if (itemdesc == "") {
                    ShowPopupMessageBox("Please Enter Description.")
                    return false;
                }
                else if ($('#<%=txtvValidFrom.ClientID %>').val() == "") {
                    ShowPopupMessageBox("Please Enter From Date.")
                    return false;
                } 
                else if ($('#<%=txtvValidTo.ClientID %>').val() == "") {
                    ShowPopupMessageBox("Please Enter ToDate.")
                    return false;
                }
                else if ($('#<%=txtvCommission.ClientID %>').val() == "" && parseFloat($('#<%=txtvCommission.ClientID %>').val()) > 0) {
                    ShowPopupMessageBox("Please Enter Commission.")
                    return false;
                }
                else {
                    return true;
                } 
            }
            catch (err) {
                ShowPopupMessageBox(err.message); 
            }
        }

        function InitializeDialog1() {
            try {
                $("#StockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCloseDeleteDialog() {
            try {
                $("#StockUpdatePosting").dialog('close');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");


        }
        function clearFormAll() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $('#<%=txtInventoryCode.ClientID %>').val('');
            $('#<%=txtItemNameAdd.ClientID %>').val('');
            $('#<%=txtvDepartment.ClientID %>').val('');
            $('#<%=txtvCategory.ClientID%>').val('');
            $('#<%=txtvBrand.ClientID%>').val('');
            $('#<%=txtvPrice.ClientID%>').val('');
            $('#<%=txtvCost.ClientID%>').val('');
            $('#<%=txtvCommission.ClientID%>').val('');

            $('#<%=txtInventoryCode.ClientID %>').focus();


        }
        function fncClear(event) {
            try {

                $('#<%=txtInventoryCode.ClientID %>').val('');
                $('#<%=txtItemNameAdd.ClientID %>').val('');
                $('#<%=txtvDepartment.ClientID %>').val('');
                $('#<%=txtvCategory.ClientID%>').val('');
                $('#<%=txtvBrand.ClientID%>').val('');
                $('#<%=txtvPrice.ClientID%>').val('');
                $('#<%=txtvCost.ClientID%>').val('');
                $('#<%=txtvCommission.ClientID%>').val('');

                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SalesManCommissionItemWise');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SalesManCommissionItemWise";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Man</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_ItemWiseSaleManCom%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="top-container-im">
                <div class="left-container-detail">
                    <div class="control-group-split">
                        <div class="control-group-split" style="width: 70%;">
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 30%;">
                            <div style="float: right; display: table">
                                <div class="control-button">
                                    <asp:LinkButton ID="LinkButton6" runat="server" class="button-blue" OnClick="lnkViewCommission_Click"
                                        OnClientClick="return fncValidateViewCommission();" Text='<%$ Resources:LabelCaption,lbl_ViewCommission %>'></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="LinkButton11" runat="server" class="button-blue" OnClick="lnkfetch_Click"
                                        Text='<%$ Resources:LabelCaption,btn_Fetch %>' Width="100px"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split" style="width: 70%;">
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 45%;">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%;">
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server" style="height: 224px;">

                        <div class="GridDetails">
                            <div class="row">
                                  <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="right-container-top-detail">
                                        <div class="grdLoad">
                                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Serial No</th>
                                                        <th scope="col">Item Code
                                                        </th>
                                                        <th scope="col">Description
                                                        </th>
                                                        <th scope="col">Department 
                                                        </th>
                                                        <th scope="col">Category
                                                        </th>
                                                        <th scope="col">Brand
                                                        </th>
                                                        <th scope="col">Cost
                                                        </th>
                                                        <th scope="col">Selling Price
                                                        </th>
                                                        <th scope="col">Valid From
                                                        </th>
                                                        <th scope="col">Valid To
                                                        </th>
                                                        <th scope="col">Comm.(%)
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 189px; width: 1335px; background-color: aliceblue;">
                                                <asp:GridView ID="gvItemWiseSales" runat="server" Width="100%" AutoGenerateColumns="False" ShowHeader="false"
                                                    ShowHeaderWhenEmpty="true" OnRowDeleting="gv_OnRowDeleting"
                                                    OnRowDataBound="gv_OnRowDataBound"
                                                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Delete" />
                                                        <asp:TemplateField Visible="false">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label18" runat="server" Visible="false" Text="Select"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Inventorycode" HeaderText="Item Code"></asp:BoundField>
                                                        <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                        <asp:BoundField DataField="DepartmentCode" HeaderText="Department"></asp:BoundField>
                                                        <asp:BoundField DataField="CategoryCode" HeaderText="Category"></asp:BoundField>
                                                        <asp:BoundField DataField="BrandCode" HeaderText="Brand"></asp:BoundField>
                                                        <asp:BoundField DataField="AverageCost" HeaderText="Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="CurrPrice" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right" ></asp:BoundField>
                                                        <asp:TemplateField HeaderText="Valid From" ItemStyle-Width="80">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtValidFrom" runat="server" Text='<%# Eval("ValidFrom") %>' CssClass="grid-textbox"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Valid To" ItemStyle-Width="80">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtValidTo" runat="server" Text='<%# Eval("ValidTo") %>' CssClass="grid-textbox"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Comm.(%)" ItemStyle-Width="65" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtComm" runat="server" Text='<%# Eval("Comm") %>' onkeypress="return isNumberKeyTextBox(event)"
                                                                    CssClass="grid-textbox"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="bottom-purchase-container-add" style="margin-top: 10px;">
                <div class="bottom-purchase-container-add-Text">
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 9%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 3px">
                                <asp:TextBox ID="txtInventoryCode" runat="server" onkeydown="return fncGetInventoryCode(event)"
                                    CssClass="form-control" Style="width: 100px"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 23%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res" onMouseDown="return false"
                                    Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 17%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtvDepartment" runat="server" onkeypress="return isNumberKey(event)"
                                    onMouseDown="return false" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 17%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label161" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtvCategory" runat="server" onkeypress="return isNumberKey(event)"
                                    onMouseDown="return false" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 17%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtvBrand" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                    runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 8%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="lblcost" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtvCost" runat="server" onMouseDown="return false" CssClass="form-control-res"
                                    Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 8%">
                            <div class="label-left" style="width: 100%">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
                            </div>
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <asp:TextBox ID="txtvPrice" runat="server" onMouseDown="return false" CssClass="form-control-res"
                                    Font-Bold="true"></asp:TextBox>
                            </div>
                        </div>
                        <div style="width: 100%">
                            <div class="button-contol" style="float: left; padding-top: 0px; margin-top: 2px; clear: none;">
                                <div class="control-group-left" style="width: 15%">
                                    <div class="label-left" style="width: 100%">
                                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_ValidFrom %>'></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                        <asp:TextBox ID="txtvValidFrom" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 15%">
                                    <div class="label-left" style="width: 100%">
                                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_ValidTo %>'></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                        <asp:TextBox ID="txtvValidTo" runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" style="width: 15%">
                                    <div class="label-left" style="width: 100%">
                                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_CommissionPer %>'></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 99%; margin-left: 1%">
                                        <asp:TextBox ID="txtvCommission" runat="server" onkeypress="return isNumberKey(event)"
                                            onkeydown="fncCommFocus(event)" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                <div class="control-button">
                                    <div class="label-left" style="width: 100%">
                                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                            OnClick="lnkAdd_Click" OnClientClick="return fncValidateAdd();"><i class="icon-play"></i></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="control-button">
                                    <div class="label-right" style="width: 100%">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                            OnClientClick="fncClear();"><i class="icon-play"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height: 100px; margin-top: 10px;">
                <div class="selling-right">
                    <div class="selling-header">
                        Apply Commission
                    </div>
                    <div class="selling-detail">
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 100%">
                                <div class="label-left">
                                    <asp:Label ID="Label97" runat="server" Text='<%$ Resources:LabelCaption,lbl_CommissionPer %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtApplyCommissionAll" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                <div class="control-button">
                                    <asp:LinkButton ID="LinkButton9" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_ApplyAllItems %>'
                                        OnClientClick="fncApplyAllZero();return false;"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="selling-middle" style="width: 30%;">
                    <div class="selling-header">
                        Apply Valid Date
                    </div>
                    <div class="selling-detail">
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 50%">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left" style="width: 50%">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                <div class="control-button">
                                    <asp:LinkButton ID="LinkButton10" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_ApplyAllItems %>'
                                        OnClientClick="fncApplyAllDate();return false;"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                <div style="float: right; display: table">
                    <div class="control-button">
                        <asp:LinkButton ID="LinkButton12" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                            OnClientClick="return fncValidateClearSure();" Width="150px"><i class="icon-play"></i></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Update %>'
                            OnClientClick="fncValidateSave();return false;" Width="150px"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="SelectAny">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteStockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ClearSure">
            <p>
                <%=Resources.LabelCaption.Alert_ClearSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton13" runat="server" class="button-blue" OnClientClick="return CloseConfirmClearSure()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkbtnClear_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton14" runat="server" class="button-blue" OnClientClick="return ClearSure();"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="CloseConfirmSave(); return false;"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmDataLoss">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_DataLoss%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return CloseDataLoss();"
                        Text='<%$ Resources:LabelCaption,btn_OK %>' OnClick="lnkViewCommission_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton8" runat="server" class="button-blue" OnClientClick="CloseDataLoss(); return false;"
                        Text='<%$ Resources:LabelCaption,btn_Cancel %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_Delete%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ShowPopupMessageBoxNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseShowPopupMessageBoxNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full">
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
