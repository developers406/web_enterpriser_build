﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTray.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmTray" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <script type="text/javascript">


    function ValidateForm() {
        var Show = '';

        if (document.getElementById("<%=txtTrayCode.ClientID%>").value == "") {
            Show = Show + 'Please Enter TrayCode';
            document.getElementById("<%=txtTrayCode.ClientID %>").focus();
        }

        if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
            Show = Show + '<br />Please Enter Description';
            document.getElementById("<%=txtDescription.ClientID %>").focus();
        }

        if (Show != '') {
            ShowPopupMessageBox(Show);
            return false;
        }

        else {
            $("input").removeAttr('disabled');
            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
            return true;
        }
    }
        function Tray_Delete(source) {

            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                        
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

    }
    function imgbtnEdit_ClientClick(source) {

        DisplayDetails($(source).closest("tr"));

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function DisplayDetails(row) {

        $('#<%=txtTrayCode.ClientID %>').prop("disabled", true);
        $('#<%=txtTrayCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
        $('#<%=txtDescription.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));



    }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' });

            $(document).ready(function () {
                $("input").first().focus();

                $('input').bind("keydown", function (e) {
                    var n = $("input").length;
                    if (e.which == 13) { //Enter key
                        e.preventDefault(); //to skip default behavior of the enter key
                        var nextIndex = $('input').index(this) + 1;
                        if (nextIndex < n)
                            $('input')[nextIndex].focus();
                        else {
                            $('input')[nextIndex - 1].blur();
                            // $('#btnSubmit').click();
                        }
                    }
                });

                //$('#btnSubmit').click(function () {
                // ShowPopupMessageBox('Form Submitted');
                //  });
            });

            $('#<%=txtTrayCode.ClientID %>').focus();

            $('#<%=txtTrayCode.ClientID %>').focusout(function (){
                 $.ajax({
                     url: "frmTray.aspx/GetExistingCode",
                        data: "{ 'Code': '" + $("#<%=txtTrayCode.ClientID%>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                           var temp = data.d;
                            var strarray = temp.split(',');
                            
                            if (temp != "Null")

                            {
                                $('#<%=txtTrayCode.ClientID %>').prop("disabled", true);
                                $('#<%=txtTrayCode.ClientID %>').val(strarray[0]);
                              
                                $('#<%=txtDescription.ClientID %>').val(strarray[1]);
                            }
                              },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                 });
             });

            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmTray.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

        }
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);

        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Tray');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Tray";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Tray </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
       
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
               <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p ><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                
                    <div class="container-group-small">
                        <div class="container-control">
                        
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Tray Code"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                 <asp:TextBox ID="txtTrayCode" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    
                                      <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="button-contol" style="margin-left: 175px">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                    OnClientClick="return ValidateForm()" Text='<%$ Resources:LabelCaption,lnkSave %>'><i class="icon-play"></i></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                    Text='<%$ Resources:LabelCaption,lnkClear %>'><i class="icon-play" ></i></asp:LinkButton>
                            </div>
                        </div>
                    </div>
               
            </ContentTemplate>
        </asp:UpdatePanel>
       
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                        </asp:Panel>
                    </div>
                   
                    <asp:GridView ID="gvTray" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="TrayCode" >
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Tray_Delete(this); return false;"
                                         CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                        ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TrayCode" HeaderText="Tray Code"></asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                    <ups:PaginationUserControl runat="server" ID="TrayPaging" OnPaginationButtonClick="Tray_PaginationButtonClick" />
                </div>
                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
