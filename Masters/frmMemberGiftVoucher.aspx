﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmMemberGiftVoucher.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMemberGiftVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 111px;
            max-width: 111px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 85px;
            max-width: 85px;
            text-align: center !important;
        }
          .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 342px;
            max-width: 342px; text-align: center !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 340px;
            max-width: 340px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 249px;
            max-width: 249px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
     .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function ValidateForm() {
            if ($('#<%=txtVoucherCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Giftvoucher");
                return false;
            }
            if (!($('#<%=txtVoucherCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Giftvoucher");
                return false;
            }
            var Show = '';
            if (document.getElementById("<%=txtVoucherCode.ClientID%>").value == "") {
                Show = Show + 'Please Enter Voucher Code'
                document.getElementById("<%=txtVoucherCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtVoucherAmt.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Voucher Amount';
                document.getElementById("<%=txtVoucherAmt.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
             $('#<%=txtVoucherCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmMemberGiftVoucher.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtVoucherCode.ClientID%>").val() + "'}",
                   dataType: "json",
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   success: function (data) {
                       var temp = data.d;
                       var strarray = temp.split(',');

                       if (temp != "Null") {
                           $('#<%=txtVoucherCode.ClientID %>').prop("disabled", true);
                           $('#<%=txtVoucherCode.ClientID %>').val(strarray[0]);
                           $('#<%=txtVoucherAmt.ClientID %>').val(strarray[1]);
                           $('#<%=txtRewardUsagePoints.ClientID %>').val(strarray[2]);





                       }

                   },
                   error: function (response) {
                       ShowPopupMessageBox(response.responseText);
                   },
                   failure: function (response) {
                       ShowPopupMessageBox(response.responseText);
                   }
               });
           });

           //$('input[type=text]').bind('change', function () {
           //    if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {
           //        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
           //        this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
           //    }
           //});

       }

       function isNumberKey(evt) {
           var charCode = (evt.which) ? evt.which : evt.keyCode;
           //            ShowPopupMessageBox(charCode); 
           if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 37 || charCode > 40))
               return false;
           return true;
       }

       function clearFormIn() {
           $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
           $(':checkbox, :radio').prop('checked', false);
           $('#<%=txtVoucherCode.ClientID %>').focus();
           $('#<%=txtVoucherCode.ClientID %>').prop("disabled", false);

           return false;
       }



    </script>

    <script type="text/javascript">
        function Confirm_Delete() {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Membertype");
                return false;
            }

            if (confirm("Do you want to delete Gift Voucher?")) {
                return true;
            } else {
                return false;
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                     e.preventDefault();
                 }
                 else if (e.keyCode == 117) {
                     $("input").removeAttr('disabled');
                     $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'MemberGiftVoucher');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "MemberGiftVoucher";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Member Gift Voucher</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Voucher Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVoucherCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Voucher Amount"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVoucherAmt" MaxLength="9" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Reward Usage Points"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRewardUsagePoints" MaxLength="9" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()">Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearFormIn()">Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="gridDetails">
                    <div class="row">
                          <asp:label Id="lblcount" runat="server" Text="Total MemberGiftVoucher : " style="font-weight: 900; float:left;"></asp:label>
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                            <div class="right-container-top-detail">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Edit
                                                </th>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Voucher Code
                                                </th>
                                                <th scope="col">Voucher Amount
                                                </th>
                                                <th scope="col">Rewards Points Usage
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 284px; width: 1330px; background-color: aliceblue;">
                                        <asp:GridView ID="gvMemberGiftVoucher" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            ShowHeaderWhenEmpty="true" AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                            OnPageIndexChanging="gvMemberGiftVoucher_PageIndexChanging" DataKeyNames="VoucherCode">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                            ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/delete.png"
                                                            ToolTip="Click here to Delete" OnClientClick=" return Confirm_Delete()" OnClick="imgbtnDelete_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                <asp:BoundField DataField="VoucherCode" HeaderText="Voucher Code"></asp:BoundField>
                                                <asp:BoundField DataField="VAmount" HeaderText="Voucher Amount" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="RPUsage" HeaderText="Rewards Points Usage" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
