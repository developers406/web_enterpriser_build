﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" Async="true"
    CodeBehind="frmCustomerMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmCustomerMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }

        .radioboxlist label {
            color: Black;
            background-color: Silver;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }

        input:checked + label {
            color: white;
            background: red;
        }

        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            fncMbLength();
            $("#<%= txtDOB.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtJoinDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker();
            $("#<%= txtExpiryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" })
            $("#<%= txtRenewalDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker();
            $("#<%= txtAnnivDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtMonFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker();
            $("#<%= txtMonToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker();

            fncSetDate();

            //============================> For Maintain Current Tab while postback

            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "PersonalDetails";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });
            });

            //===========================> Set Width for Chosen Dropdown

            $("#<%=ddlPriceType.ClientID %>, #<%=ddlPriceType.ClientID %>_chzn, #<%=ddlPriceType.ClientID %>_chzn > div, #<%=ddlPriceType.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlCreditType.ClientID %>, #<%=ddlCreditType.ClientID %>_chzn, #<%=ddlCreditType.ClientID %>_chzn > div, #<%=ddlCreditType.ClientID %>_chzn > div > div > input").css("width", '100%');

        }
        function fncMbLength() {
            try {
                $("#<%= txtMobileNo.ClientID %>").focusout(function () {
                    if ($("#<%= txtMobileNo.ClientID %>").val().length <= 9) {
                        ShowPopupMessageBox("Please Enter Atleast 10 Number");
                        $("#<%= txtMobileNo.ClientID %>").val('');
                   <%-- if ($("#<%= txtMobileNo.ClientID %>").val().length <= 9) {
                    ShowPopupMessageBox("Please Enter Atleast 10 Number");
                    $("#<%= txtMobileNo.ClientID %>").val('');
                    //return false;--%>
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            fncMbLength();
        });
        function fncSetDate() {
            try {
                if ($("#<%= hidMode.ClientID %>").val() == "New") {
                    $(function () {
                        $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0", new Date());
                        $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0", new Date());
                        $("#<%= txtMonFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", new Date());
                        $("#<%= txtMonToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", new Date());
                        $("#<%= txtJoinDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", new Date());
                        $("#<%= txtExpiryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", new Date());
                        $("#<%= txtRenewalDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", new Date());
                        var sSplit = $("#<%= txtFromDate.ClientID %>").val().split('/');
                        $("#<%= txtFromDate.ClientID %>").val('01' + '/' + sSplit[1] + '/' + sSplit[2]);
                    });
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    </script>

    <script type="text/javascript">

        function fncCountryValidate() {

            var Country = $("[id$=ddlCountry] option:selected").text();
            var Orgin = $("[id*=HiddenCountry]").val();
            var state = $("[id$=ddlState] option:selected").val();
            var OrginState = $("[id*=HiddenState]").val();
            //ShowPopupMessageBox(Country.toUpperCase());
            //alert(Country);
            //alert(Orgin);
            //alert(state);
            //alert(OrginState);
            if (Country.trim() != '') {
                if (Country.toUpperCase() != Orgin.toUpperCase()) {
                    var radio = $("[id*=rdoGstType] label:contains('Import')").closest("td").find("input");
                    radio.attr("checked", "checked");
                    //alert("Import");
                }
                else if (Country.toUpperCase() == Orgin.toUpperCase() && state.toUpperCase() == OrginState.toUpperCase()) {
                    var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                    radio.attr("checked", "checked");
                    //alert("IntraState");
                }

                else if (Country.toUpperCase() == Orgin.toUpperCase() && state.toUpperCase() != OrginState.toUpperCase()) {
                    var radio = $("[id*=rdoGstType] label:contains('InterState')").closest("td").find("input");
                    radio.attr("checked", "checked");
                    //alert("InterState");
                }
            }

        }

    </script>
    <script type="text/javascript">

</script>
    <script type="text/javascript">
        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">

        function Validate() {
            var Show = '';
           <%-- if (document.getElementById("<%=txtCustCode.ClientID%>").value == "") {
                //popUpObjectForSetFocusandOpen = document.getElementById("<%=txtCustCode.ClientID %>");
                //ShowPopupMessageBoxandFocustoObject('Please Enter Customer Code');

                return false;
            }--%>
            if (document.getElementById("<%=txtCustName.ClientID%>").value == "") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Customer Name');
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtCustName.ClientID %>");
                return false;
            }
            else if (document.getElementById("<%=txtAddress1.ClientID%>").value == "") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Address');
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAddress1.ClientID %>");
                return false;
            }
            else if (document.getElementById("<%=txtMobileNo.ClientID%>").value == "") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Mobile No');
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtMobileNo.ClientID %>");
                return false;
            }
         <%--  else if (document.getElementById("<%=txtDistance.ClientID%>").value == "") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Distance');
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtDistance.ClientID %>");
                return false;
            }--%>
            <%--else if (document.getElementById("<%=ddlGender.ClientID%>").value == "") {
                ShowPopupMessageBoxandOpentoObject('Please Select Gender');
                popUpObjectForSetFocusandOpen = $('#<%=ddlGender.ClientID %>');
                return false;
            }--%>
          <%--  else if (document.getElementById("<%=ddlState.ClientID%>").value == "") {
                ShowPopupMessageBoxandOpentoObject('Please Select State');
                popUpObjectForSetFocusandOpen = $('#<%=ddlState.ClientID %>');
                return false;
            }--%>
            else if (!validatemobile(document.getElementById("<%=txtMobileNo.ClientID%>").value)) {
                ShowPopupMessageBoxandOpentoObject('Please Enter Valid MobileNo');
                popUpObjectForSetFocusandOpen = $('#<%=txtMobileNo.ClientID %>');
                return false;
            }
            else if (!validateEmail(document.getElementById("<%=txtEmail.ClientID%>").value)) {  //jayanthi 05082022
                ShowPopupMessageBoxandOpentoObject('Please Enter Valid Email');
                popUpObjectForSetFocusandOpen = $('#<%=txtEmail.ClientID %>');
                return false;
            }
            else {

                $('input').removeAttr('disabled');
                return true; 
            }

            
           
        }
        function validatemobile(num) {
            var mobNum = num;
            var filter = /^\d*(?:\.\d{1,2})?$/;

            if (filter.test(mobNum)) {
                if (mobNum.length == 10) {
                    return true;
                } else {
                    alert('Please put 10  digit mobile number');

                    return false;
                }
            }
            else {
                alert('Not a valid number');

                return false;
            }
        }
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test($email);
        }

    </script>
    <%------------------------------------------- Clear Form -------------------------------------------%>
    <script type="text/javascript">
        function clearForm() {
            //$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $('#<%=txtCustName.ClientID %>').val('');
            $('#<%=txtDOB.ClientID %>').val('');
            $('#<%=txtOccupation.ClientID %>').val('');
            $('#<%=txtDesignation.ClientID %>').val('');
            $('#<%=txtEmail.ClientID %>').val('');
            $('#<%=txtEmployer.ClientID %>').val('');
            $('#<%=txtGSTNo.ClientID %>').val('');
            $('#<%=txtIDNo.ClientID %>').val('');
            $('#<%=txtEducation.ClientID %>').val('');
            $('#<%=txtRepresentative.ClientID %>').val('');
            $('#<%=txtAnnivDate.ClientID %>').val('');
            $('#<%=txtAddress1.ClientID %>').val('');
            $('#<%=txtAddress2.ClientID %>').val('');
            $('#<%=txtAddress3.ClientID %>').val('');
            $('#<%=txtDiscountPRC.ClientID %>').val('');
            $('#<%=txtDistance.ClientID %>').val('');
            $('#<%=txtPin.ClientID %>').val('');
            $('#<%=txtPhoneO.ClientID %>').val('');
            $('#<%=txtPhoneR.ClientID %>').val('');
            $('#<%=txtNationality.ClientID %>').val('');
            $('#<%=txtZone.ClientID %>').val('');
            $('#<%=txtMobileNo.ClientID %>').val('');
            $('#<%=txtLoyalityCardNo.ClientID %>').val('');
            $('#<%=txtJoinDate.ClientID %>').val('');
            $('#<%=txtRenewalDate.ClientID %>').val('');
            $('#<%=txtExpiryDate.ClientID %>').val('');
            $('#<%=txtCreditLimit.ClientID %>').val('');
            $('#<%=txtCreditDays.ClientID %>').val('');
            $('#<%=txtCreditBalance.ClientID %>').val('');
            $('#<%=txtFromDate.ClientID %>').val('');
            $('#<%=txtToDate.ClientID %>').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $('#<%=txtDOB.ClientID %>').focus();
            return false;
            // fncMbLength();
        }
        function clearImage() {
            $('#<%=imgpreview.ClientID %>').val('');
            return false;
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function fncVerify(val) { //surya Verify 20191226
            try {
                var splitVal = val.split(':');
                $('#<%=txtVerifyGstIn.ClientID %>').val($('#<%=txtGSTNo.ClientID %>').val());
                $('#<%=txtRegName.ClientID %>').val(splitVal[0]);
                $('#<%=txtBustype.ClientID %>').val(splitVal[1]);
                $('#<%=txtBusName.ClientID %>').val(splitVal[2]);
                $('#<%=txtGstinAddress.ClientID %>').val(splitVal[3]);
                if ($('#<%=txtBustype.ClientID %>').val() != "Active") {
                    $('#<%=txtBustype.ClientID %>').css('background', 'pink');
                }
                $("#divGstinVerify").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "GSTIN Verify",
                    appendTo: 'form:first',
                    buttons: {
                        "Confirm": function () {
                            if ($('#<%=txtBustype.ClientID %>').val() == "Active") {
                                $('#<%=hidGSTStatus.ClientID %>').val('1');
                                $('#<%=lnkGstVerify.ClientID %>').removeClass('button-green');
                                $('#<%=lnkGstVerify.ClientID %>').addClass('button-blue');
                                $('#<%=lnkGstVerify.ClientID %>').text('Clear');
                                $('#<%=txtGSTNo.ClientID %>').attr('disabled', 'disabled');

                                //surya 03082022
                                $('#<%=txtAddress1.ClientID %>').val($('#<%=txtGstinAddress.ClientID %>').val());
                                $('#<%=txtCustName.ClientID %>').val($('#<%=txtRegName.ClientID %>').val());
                                var splitadd = splitVal[3].split(',');
                                var len = splitadd.length;
                                $('#<%=txtAddress1.ClientID %>').val(splitadd[len - len]);
                                $('#<%=txtAddress2.ClientID %>').val(splitadd[len - 5] + ',' + splitadd[len - 4]);
                                
                                <%--$('#<%=ddlCity.ClientID %>').val(splitadd[len-3]);--%>
                                var splitpin = splitadd[len-1].split('-');
                                $('#<%=txtPin.ClientID %>').val(splitpin[1]);
                                $('#<%=ddlState.ClientID %>').val((splitadd[len - 2].toUpperCase()));
                                $('#<%=ddlState.ClientID %>').trigger("liszt:updated");
                                $('#<%=ddlCity.ClientID %>').val(splitadd[len-3]);
                                $('#<%=ddlCity.ClientID %>').trigger("liszt:updated");
                                $('#<%=ddlCountry.ClientID %>').val('IND');
                                $('#<%=ddlCountry.ClientID %>').trigger("liszt:updated");

                            }
                            else {
                                ShowPopupMessageBox("This Gst Number in Cancelled Status.You cannot Confirm this GST- " + $('#<%=txtGSTNo.ClientID %>').val())
                            }
                            $(this).dialog("close");
                        },
                        "Close": function () {
                            $(this).dialog("close");
                            $('#<%=hidGSTStatus.ClientID %>').val('0');
                        }
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }
        function fncClearClick() {
            if ($('#<%=txtGSTNo.ClientID %>').is(':disabled')) {
                $('#<%=hidGSTStatus.ClientID %>').val('0');
                $('#<%=lnkGstVerify.ClientID %>').removeClass('button-blue');
                $('#<%=lnkGstVerify.ClientID %>').addClass('button-green');
                $('#<%=lnkGstVerify.ClientID %>').text('Verify');
                $('#<%=txtGSTNo.ClientID %>').removeAttr("disabled");
                return false;
            }
        }

        function fncStateChange() {
            var Country = $("[id$=ddlCountry] option:selected").text().trim();
            var Orgin = $("[id*=HiddenCountry]").val().trim();
            var state = $("[id$=ddlState] option:selected").val().trim();
            var OrginState = $("[id*=HiddenState]").val().trim();


            if (Country == Orgin && state == OrginState) {
                $('#<%=rdoGstType.ClientID %>').find("input[value='0']").prop("checked", true);
            }
            else if (Country == Orgin && state != OrginState) {
                $('#<%=rdoGstType.ClientID %>').find("input[value='1']").prop("checked", true);
            }
            else if (Country == "" && state != OrginState) {
                $('#<%=rdoGstType.ClientID %>').find("input[value='0']").prop("checked", true);
            }
            else {
                $('#<%=rdoGstType.ClientID %>').find("input[value='2']").prop("checked", true);
            }
        }
        function fncToastInvalidResponse() {
            ShowPopupMessageBox("GST Portal is Not Connected.Please try After.");
            return false;
        }
        function fncToast() {
            ShowPopupMessageBox("Invalid GST Number.This Number Not in GST Portal.");
            return false;
        }

       

    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'CustomerMaster');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CustomerMaster";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }
        function showAll() {

            $("#divsave").dialog({           //musaraf 21112022
                dialogClass: "no-close",
                title: "Enterpriser Web",
                height: 160,
                width: 400,
                modal: true,
                buttons: {
                    "OK": function () {
                        __doPostBack('ctl00$ContentPlaceHolder1$btnsave', '');
                    }
                    
                }
            }).html("Customer Saved Successfully..!")
         }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Master </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" class="EnableScroll">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="container-pers-top-header">
                        <div class="container-pers-header-left">
                            <div class="control-group-split">
                                <div class="control-group-left" style="width: 62%">
                                    <div class="label-left">
                                        <asp:Label ID="Label29" runat="server" Font-Bold="true" Font-Size="15px" Text='<%$ Resources:LabelCaption,lbl_CustomerCode %>'></asp:Label><span
                                            class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtCustCode" runat="server" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right" style="width: 35%">
                                    <div class="label-left">
                                        <asp:Label ID="Label30" runat="server" Text='<%$ Resources:LabelCaption,lbl_Title %>'></asp:Label>
                                    </div>
                                    <div class="label-right" style="color: Black">
                                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-pers-header-right">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left" style="width: 30%">
                                        <asp:Label ID="Label31" runat="server" Font-Bold="true" Font-Size="14px" Text='<%$ Resources:LabelCaption,lbl_CustomerName %>'></asp:Label><span
                                            class="mandatory">*</span>
                                    </div>
                                    <div class="label-right" style="width: 70%">
                                        <asp:TextBox ID="txtCustName" runat="server" MaxLength="200" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right">
                                    <%-- <div class="label-left" style="width: 58%">
                                        <asp:Label ID="Label32" runat="server" Text="Create Location as Customer"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 42%">
                                        <asp:CheckBox ID="chkCreateLocation" runat="server" />
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="Tabs" role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs custnav custnav-cm" role="tablist">
                                <li><a href="#PersonalDetails" aria-controls="Inventory Pricing" role="tab" data-toggle="tab">Personal Details </a></li>
                                <li><a href="#OtherDetails" aria-controls="OtherDetails" role="tab" data-toggle="tab">Other Details</a></li>
                                <li><a href="#DiscountDetails" aria-controls="DiscountDetails" role="tab" data-toggle="tab">Discount Details</a></li>
                                <li><a href="#RewardPointUsage" aria-controls="RewardPointUsage" role="tab" data-toggle="tab">Reward Point Usage</a></li>
                                <li><a href="#TotalMonthlySales" aria-controls="TotalMonthlySales" role="tab" data-toggle="tab">Total Monthly Sales</a></li>
                                <%--  <li><a href="#MemberWiseDisc" aria-controls="MemberWise Disc" role="tab" data-toggle="tab">
                                    MemberWise Disc</a></li>--%>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style="overflow-y: hidden;">
                                <div class="tab-pane active" role="tabpanel" id="PersonalDetails">
                                    <div class="container-pers-top">
                                        <div class="container-pers-header">
                                            Identification Details
                                        </div>
                                        <div class="container-pers-detail">
                                            <div class="container-pers-left">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_IDType %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:DropDownList ID="ddlIDType" runat="server" CssClass="form-control-res">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_DOB %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_Occupation %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtOccupation" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label4" runat="server" Text="Designation"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDesignation" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_Employer %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtEmployer" runat="server" MaxLength="75" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label61" runat="server" Text="GSTIN No"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtGSTNo" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="col-md-6">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:LinkButton ID="lnkGstVerify" Text="Verify" OnClientClick="return fncClearClick();" Style="float: right;" OnClick="lnkVerify_Click" runat="server" CssClass="button-green"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-pers-middle">
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_IDNo %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtIDNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_AgeGroup %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlAgeGroup" runat="server" CssClass="form-control-res">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_MaritalStatus %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="form-control-res">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Education %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtEducation" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlClass" runat="server" class="chosen-select" CssClass="chzn-select">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_Income %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlIncome" runat="server" CssClass="form-control-res">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label12" runat="server" Text="Member Type"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control-res">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Representative %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtRepresentative" runat="server" MaxLength="30" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label60" runat="server" Text="GST Type"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:RadioButtonList ID="rdoGstType" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="IntraState" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="InterState" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Import - RCM" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="Zero-GST" Value="4"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-pers-right">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label15" runat="server" Font-Bold="true" Font-Size="15px" Text='<%$ Resources:LabelCaption,lbl_Gender %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:DropDownList ID="ddlGender" runat="server" CssClass="form-control-res">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_AnniversaryDate %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtAnnivDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_Active %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <asp:Image ID="imgpreview" runat="server" Height="100px" Width="165px" />
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="lnkSave" />
                                                        </Triggers>
                                                        <ContentTemplate>
                                                            <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClientClick="clearImage()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-pers-bottom">
                                        <div class="container-pers-header">
                                            Communication Details
                                        </div>
                                        <div class="container-pers-detail">
                                            <div class="container-pers-left">
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label18" runat="server" Font-Bold="true" Font-Size="13px" Text='<%$ Resources:LabelCaption,lbl_Address1 %>'></asp:Label><span
                                                            class="mandatory">*</span>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="500" CssClass="form-control-res" Height="50px"
                                                            TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_Address2 %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="500" CssClass="form-control-res" Height="50px"
                                                            TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lbl_Address3 %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtAddress3" runat="server" MaxLength="500" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label62" runat="server" Font-Bold="true" Font-Size="14px" Text='Distance'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDistance"  onkeydown="return isNumberKeyWithDecimalNew(event)" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-pers-middle">
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lbl_Country %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control-res" onchange="return fncCountryValidate();">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_Zone %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtZone" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label25" runat="server" Font-Bold="true" Font-Size="15px" Text='<%$ Resources:LabelCaption,lbl_State %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlState" runat="server" class="chosen-select" CssClass="chzn-select" onchange="fncStateChange();return false;">
                                                            </asp:DropDownList>
                                                            <%--AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"--%>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label26" runat="server" Text='<%$ Resources:LabelCaption,lbl_City %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control-res">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_Pin %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtPin" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label28" runat="server" Font-Bold="true" Font-Size="15px" Text='<%$ Resources:LabelCaption,lbl_MobileNo %>'></asp:Label><span
                                                                class="mandatory">*</span>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control-res" MaxLength="10" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_PhoneR %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtPhoneR" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label27" runat="server" Text='<%$ Resources:LabelCaption,lbl_PhoneO %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtPhoneO" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label52" runat="server" Text='<%$ Resources:LabelCaption,lbl_Nationality %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtNationality" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label53" runat="server" Text='<%$ Resources:LabelCaption,lbl_Email %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label63" runat="server" Text="Area"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlArea" runat="server" class="chosen-select" CssClass="chzn-select" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="OtherDetails">
                                    <div class="container-other-top">
                                        <div class="container-other-top-left">
                                            <div class="container-other-common-header">
                                                Discount & Loyalty
                                            </div>
                                            <div clss="container-other-common-detail">
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label33" runat="server" Text="LoyaltyCardNo"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtLoyalityCardNo" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label34" runat="server" Text='<%$ Resources:LabelCaption,lbl_PriceType %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlPriceType" runat="server" CssClass="form-control-res">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label35" runat="server" Text='<%$ Resources:LabelCaption,lbl_JoinDate %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtJoinDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label36" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDate %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label37" runat="server" Text='<%$ Resources:LabelCaption,lbl_RenewalDate %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtRenewalDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label39" runat="server" Text='<%$ Resources:LabelCaption,lbl_AllowPoint %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:CheckBox ID="chkAllowPoint" runat="server" Checked="true" />
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container-other-top-middle">
                                            <div class="container-other-common-header">
                                                Credit Limit
                                            </div>
                                            <div clss="container-other-common-detail">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label41" runat="server" Text='<%$ Resources:LabelCaption,lbl_CreditType %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:DropDownList ID="ddlCreditType" runat="server" CssClass="form-control-res">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label42" runat="server" Text='<%$ Resources:LabelCaption,lbl_CreditLimit %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCreditLimit" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label43" runat="server" Text='<%$ Resources:LabelCaption,lbl_CreditDays %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCreditDays" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label44" runat="server" Text='<%$ Resources:LabelCaption,lbl_CreditBalance %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                 <asp:TextBox ID="txtCreditBalance" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container-other-top-right">
                                            <div class="container-other-common-header">
                                                Transaction Details
                                            </div>
                                            <div clss="container-other-common-detail">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label45" runat="server" Text="Last Sales Date : "></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:Label ID="lblLastSalesDate" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label46" runat="server" Text="Last Paid Date : "></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:Label ID="lblLastPaidDate" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label47" runat="server" Text="Total Sales Amount : "></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:Label ID="lblTotalSalesAmount" runat="server" Text="Total Sales Amount"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label54" runat="server" Text="Allow Discount"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:CheckBox ID="chkAllowDiscount" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label55" runat="server" Text="Disc %"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDiscountPRC" runat="server" MaxLength="8" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label48" runat="server" Text="Total Discount : "></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:Label ID="lblTotalDiscount" runat="server" Text="Total Discount"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-other-bottom">
                                        <div class="container-other-common-header">
                                            Invoice List
                                        </div>
                                        <div class="container-other-common-detail">
                                            <div class="container-other-bottom-left">
                                                <div class="control-group-split">
                                                    <div class="control-group-left1">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label38" runat="server" Text="From Date"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-middle">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label40" runat="server" Text="To Date"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right1">
                                                        <div class="label-left">
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:Button ID="btnLoad" runat="server" class="button-blue" Text="Load" OnClick="btnLoad_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="container-other-bottom-right">
                                                <div class="control-group-split">
                                                    <div class="control-group-left1">
                                                        <div class="label-left" style="width: 60%">
                                                            <asp:Label ID="Label49" runat="server" Text="Points Gained :"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 40%">
                                                            <asp:Label ID="lblPointsGained" runat="server" Text="0.00"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-middle">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label50" runat="server" Text="Redeemed :"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:Label ID="lblRedeemed" runat="server" Text="0.00"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right1">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label51" runat="server" Text="Balance :"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:Label ID="lblBalance" runat="server" Text="0.00"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gridDetails" style="overflow: auto; height: 210px">
                                        <asp:GridView ID="gvCustInvoice" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                            RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No"></asp:BoundField>
                                                <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date"></asp:BoundField>
                                                <asp:BoundField DataField="InvoiceAmount" HeaderText="Invoice Amount" />
                                                <asp:BoundField DataField="VoucherAmount" HeaderText="Voucher Amount"></asp:BoundField>
                                                <asp:BoundField DataField="UsagePoint" HeaderText="Usage Points"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="DiscountDetails">
                                    <div class="container-pers-top">
                                        <div class="control-group-single-res" style="width: 300px; margin: 0 auto; padding: 10px">
                                            <div class="label-left">
                                                <asp:Label ID="Label56" runat="server" Text="Total Sales"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtTotalSales" runat="server" MaxLength="8" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="gridDetails" style="overflow: auto; height: 395px">
                                            <asp:GridView ID="gvDiscountDetail" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No"></asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice Date"></asp:BoundField>
                                                    <asp:BoundField DataField="InvoiceAmount" HeaderText="Sales Amount" />
                                                    <asp:BoundField DataField="MemberDiscount" HeaderText="Discount"></asp:BoundField>
                                                    <asp:BoundField DataField="RewardPoint" HeaderText="Reward Points"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="RewardPointUsage">
                                    <div class="container-pers-top">
                                        <div class="control-group-single-res" style="width: 400px; margin: 0 auto; padding: 10px">
                                            <div class="label-left">
                                                <asp:Label ID="Label57" runat="server" Text="Total Reward Points"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtTotalRewardPoints" runat="server" MaxLength="9" CssClass="form-control-res"
                                                    ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="gridDetails" style="overflow: auto; height: 395px">
                                            <asp:GridView ID="gvRewardPoint" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField DataField="AdjustmentNo" HeaderText="Adjustment No"></asp:BoundField>
                                                    <asp:BoundField DataField="AdjustmentDate" HeaderText="Adjustment Date"></asp:BoundField>
                                                    <asp:BoundField DataField="PointBefore" HeaderText="Total Reward Point" />
                                                    <asp:BoundField DataField="AdjustmentPoint" HeaderText="Redumption Point"></asp:BoundField>
                                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" role="tabpanel" id="TotalMonthlySales">
                                    <div class="control-group-split" style="width: 700px; padding: 10px 0 0 0">
                                        <div class="control-group-left" style="width: 38%">
                                            <div class="label-left">
                                                <asp:Label ID="Label58" runat="server" Text="From Date"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMonFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right" style="width: 38%; float: left; margin-left: 1%">
                                            <div class="label-left">
                                                <asp:Label ID="Label59" runat="server" Text="To Date"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtMonToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-right" style="width: 7%; float: left; margin-left: 1%">
                                            <div class="control-button">
                                                <asp:CheckBox ID="chkAllMonthlySales" runat="server" />
                                                All
                                            </div>
                                        </div>
                                        <div class="control-group-right" style="width: 15%">
                                            <div class="control-button">
                                                <asp:Button ID="btnMonthlySalesFetch" runat="server" Text="Fetch" class="button-blue"
                                                    OnClick="btnMonthlySalesFetch_Click" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-pers-top">
                                        <div class="gridDetails" style="overflow: auto; height: 390px">
                                            <asp:GridView ID="gvTotalMonthlySales" runat="server" AutoGenerateColumns="False"
                                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField DataField="dYear" HeaderText="Year"></asp:BoundField>
                                                    <asp:BoundField DataField="dMonth" HeaderText="Month"></asp:BoundField>
                                                    <asp:BoundField DataField="dNetTotal" HeaderText="Total Sales" />
                                                    <asp:BoundField DataField="dNetTotal" HeaderText="Running Total"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <%--<div class="tab-pane" role="tabpanel" id="MemberWiseDisc">
                                    <div class="container-pers-top">
                                        <asp:GridView ID="gvMemWiseDisc" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                            RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="InvoiceNo" HeaderText="Filter Head"></asp:BoundField>
                                                <asp:BoundField DataField="InvoiceDate" HeaderText="Value"></asp:BoundField>
                                                <asp:BoundField DataField="UsagePoint" HeaderText="Disc %"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                        <asp:HiddenField ID="TabName" runat="server" />
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkMemberAdjust" runat="server" class="button-red" OnClick="lnkMemberAdjust_Click"
                            Text="Adjust Point"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return Validate();"
                            OnClick="lnkSave_Click">Save(F4)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-red" PostBackUrl="~/Masters/frmCustomerMasterView.aspx"
                            Text='<%$ Resources:LabelCaption,btn_View %>'></asp:LinkButton>
                    </div>

                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                            Text="Clear(F6)"></asp:LinkButton>
                    </div>
                </div>
                 <div id="divsave" >
                            <asp:Button ID="btnsave" runat="server" style="display:none" OnClick="btnsave_Click" />
                        </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="HiddenCountry" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="HiddenState" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hidMode" runat="server" Value="" />
    <div id="divGstinVerify" style="display: none" title="Enterpriser Web">
        <div class="col-md-12">
            <div class="col-md-12" style="">
                <div class="col-md-4">
                    <asp:Label ID="Label32" runat="server" Text="GSTIN No"></asp:Label><span class="mandatory">*</span>
                </div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtVerifyGstIn" runat="server" MaxLength="15" onkeydown="fncGstNoVerify(event);return false;" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="col-md-12 GSTINPopup">
                <div class="col-md-4">
                    <asp:Label ID="Label64" runat="server" Text="Register Name"></asp:Label>
                </div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtRegName" runat="server" MaxLength="30" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="col-md-12 GSTINPopup">
                <div class="col-md-4">
                    <asp:Label ID="Label65" runat="server" Text="Business Name"></asp:Label>
                </div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtBusName" runat="server" ReadOnly="true" MaxLength="30" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="col-md-12 GSTINPopup">
                <div class="col-md-4">
                    <asp:Label ID="Label66" runat="server" Text="Address"></asp:Label>
                </div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtGstinAddress" runat="server" ReadOnly="true" TextMode="MultiLine" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <div class="col-md-12 GSTINPopup">
                <div class="col-md-4">
                    <asp:Label ID="Label67" runat="server" Text="Status"></asp:Label>
                </div>
                <div class="col-md-6">
                    <asp:TextBox ID="txtBustype" runat="server" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidGSTStatus" runat="server" Value="0" />
</asp:Content>
