﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmGiftVoucherCreation.aspx.cs"
    EnableEventValidation="false" Inherits="EnterpriserWebFinal.Masters.frmGiftVoucherCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 260px;
            max-width: 260px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 260px;
            max-width: 260px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 260px;
            max-width: 260px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 260px;
            max-width: 260px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 259px;
            max-width: 259px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            SetAutoComplete();
            GetFromNo();
            GetToNo();

        });

        function Padder(len, pad) {
            if (len === undefined) {
                len = 1;
            } else if (pad === undefined) {
                pad = '0';
            }

            var pads = '';
            while (pads.length < len) {
                pads += pad;
            }

            this.pad = function (what) {
                var s = what.toString();
                return pads.substring(0, pads.length - s.length) + s;
            };
        }



        function DeleteConfirmbox() {
            var Show = '';
            if (document.getElementById('<%=txtBookNo.ClientID %>').value == "") {

                Show = Show + 'Please Enter Book No';
                document.getElementById('<%=txtBookNo.ClientID %>').focus();
                //return false;

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                else {
                    return true;
                }
            }
            var sBookno = $('#<%=txtBookNo.ClientID %>').val();
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm(sBookno + ' - ' + 'Are you sure delete Gift Voucher Book? ')) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }

        // function OnSuccess  
        function OnSuccess(response) {
            var Show = '';
            return false;
            Show = Show + (response.d);
            if (response.d == 'true') {
                Show = Show + ("Deleted Successfully!");
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                return true;
            }
        }


        function pageLoad() {
            $("select").chosen({ width: '100%' });
            $('#<%=txtStartNo.ClientID %>').focusout(function () {
                if (StartNo == '') {
                    alert('Please Enter StartNo');
                    $('#<%=txtStartNo.ClientID %>').focus();
                    return false;
                }
            });


            $('#<%=txtQuantity.ClientID %>').focusout(function () {

                if ($('#<%=txtStartNo.ClientID%>').val() == '') {
                    //alert('Please Enter StartNo');
                    return false;
                }
                else if ($('#<%=txtQuantity.ClientID%>').val() == '') {
                    alert('Please Enter Qunatity');
                    return false;
                }

                if ($('#<%=txtQuantity.ClientID%>').val() != '') {
                    fncGenVoucherNo();
                }
            });


        }

        function SetAutoComplete() {

            $("[id$=txtBookNo]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Masters/frmGiftVoucherCreation.aspx/GetGiftVoucherDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            Show = Show + (response.responseText);
                        },
                        failure: function (response) {
                            Show = Show + (response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //Show = Show +(i.item.val);  
                    $('#<%=txtBookNo.ClientID %>').val(i.item.val);
                    $('#<%=txtValue.ClientID %>').focus();
                    //$('#<%=txtValue.ClientID %>').focus();
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkLoadBookList', ''); 

                    return false;

                },
                minLength: 2
            });
        }


        function GetFromNo() {
            try {
                //Show = Show +('test');//

                $('#<%=txtFromtransfer.ClientID %>').autocomplete({
                    source: function (request, response) {
                        var obj = {};

                        obj.bookno = $('#<%=DropDownBookNo.ClientID %>').find('option:selected').text();
                        obj.denaminator = $('#<% =txtDenaminationtransfer.ClientID %>').val();
                        obj.fromno = request.term;

                        $.ajax({
                            url: '<%=ResolveUrl("~/Masters/frmGiftVoucherCreation.aspx/GetFromNo") %>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0]
                                    }
                                }));
                            },

                            error: function (response) {
                                console.log(response.responseText);
                            },
                            failure: function (response) {
                                console.log(response.responseText);
                            }
                        });

                    },

                    select: function (e, i) {
                        //Show = Show +(i.item.val);  

                        $('#<%=txtFromtransfer.ClientID %>').val(i.item.label);


                        return false;

                    },
                    minLength: 1
                });


                }

                catch (err) {
                    fncToastError(err.message);
                }
            }


            function GetToNo() {
                try {
                    //Show = Show +('test');//

                    $('#<%=txtTotransfer.ClientID %>').autocomplete({
                        source: function (request, response) {
                            var obj = {};

                            obj.bookno = $('#<%=DropDownBookNo.ClientID %>').find('option:selected').text();
                            obj.denaminator = $('#<% =txtDenaminationtransfer.ClientID %>').val();
                            obj.tono = request.term;

                            $.ajax({
                                url: '<%=ResolveUrl("~/Masters/frmGiftVoucherCreation.aspx/GetToNo") %>',
                                data: JSON.stringify(obj),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0]
                                        }
                                    }));
                                },

                                error: function (response) {
                                    console.log(response.responseText);
                                },
                                failure: function (response) {
                                    console.log(response.responseText);
                                }
                            });

                        },

                        select: function (e, i) {
                            //Show = Show +(i.item.val);  

                            $('#<%=txtTotransfer.ClientID %>').val(i.item.label);
                            GetQuantity();

                            return false;

                        },
                        minLength: 1
                    });


                    }

                    catch (err) {
                        fncToastError(err.message);
                    }
                }


                function ValidateForm() {
                    var Show = '';
                    try {

                        if (document.getElementById('<%=txtBookNo.ClientID %>').value == "") {
                            Show = Show + 'Please Enter BookNo';
                            document.getElementById('<%=txtBookNo.ClientID %>').focus();
                            // return false;
                        }
                        if (document.getElementById('<%=DropdownDenamination.ClientID %>').value == '') {

                            Show = Show + '<br />Please Enter Denomination';
                            document.getElementById('<%=DropdownDenamination.ClientID %>').focus();
                            // return false;
                        }
                        if (document.getElementById('<%=txtValue.ClientID %>').value == '' || $('<%=txtValue.ClientID %>').val() == 0) {

                            Show = Show + '<br />Please Enter Value';
                            document.getElementById('<%=txtValue.ClientID %>').focus();
                            //return false;
                        }
                       
                        if (document.getElementById('<%=txtStartNo.ClientID %>').value == '') {

                            Show = Show + '<br />Please Enter StartNo';
                            document.getElementById('<%=txtStartNo.ClientID %>').focus();
                            //return false;
                        }
                        if (document.getElementById('<%=txtQuantity.ClientID %>').value == '') {

                            Show = Show + '<br />Please Enter Quantity';
                            document.getElementById('<%=txtQuantity.ClientID %>').focus();
                            //return false;
                        }
                        if (document.getElementById('<%=txtValidity.ClientID %>').value == '') {

                            Show = Show + '<br />Please Enter Validity';
                            document.getElementById('<%=txtValidity.ClientID %>').focus();
                            // return false;
                        }
                        if (Show != '') {
                            ShowPopupMessageBox(Show);
                            return false;
                        }

                        else {
                            return true;
                        }

                        return true;
                    }
                    catch (err) {
                        fncToastError(err.message);
                        return false;
                    }

                }

                function fncGetVoucherDetail() {
                    try {

                        $('#<%=txtFromtransfer.ClientID %>').val('');
                        $('#<%=txtTotransfer.ClientID %>').val('');
                        $('#<%=txtQuantitytransfer.ClientID %>').val('');
                    }
                    catch (err) {
                        fncToastError(err.message);
                    }
                };

                function ValidateTransfer() {
                    var Show = '';
                    try {

                        if (document.getElementById('<%=LocationDropDown.ClientID %>').value == '') {
                            Show = Show + 'Please Enter Location';
                            //document.getElementById('<%=LocationDropDown.ClientID %>').trigger("liszt:updated");
                            //document.getElementById('<%=LocationDropDown.ClientID %>').trigger("liszt:open");
                            //return false;
                        }
                        if (document.getElementById('<%=DropDownBookNo.ClientID %>').value == '') {
                            Show = Show + '<br />Please Enter BookNo';
                            //document.getElementById('<%=DropDownBookNo.ClientID %>').trigger("liszt:updated");
                        //document.getElementById('<%=DropDownBookNo.ClientID %>').trigger("liszt:open");
                        //return false;
                    }
                        if (document.getElementById('<%=txtDenaminationtransfer.ClientID %>').value == '') {
                            Show = Show + '<br />Please Enter Denomination';
                            document.getElementById('<%=txtDenaminationtransfer.ClientID %>').focus();
                        //return false;
                    }
                    if (document.getElementById('<%=txtFromtransfer.ClientID %>').value == '') {
                            Show = Show + '<br />Please Enter From No';
                            document.getElementById('<%=txtStartNo.ClientID %>').focus();
                    //return false;
                    }
                    if (document.getElementById('<%=txtTotransfer.ClientID %>').value == '') {
                            Show = Show + '<br />Please Enter To No';
                            document.getElementById('<%=txtEndNo.ClientID %>').focus();
                    //return false;
                    }
                    if (document.getElementById('<%=txtQuantitytransfer.ClientID %>').value == '') {
                            Show = Show + '<br />Please Enter Quantity';
                            document.getElementById('<%=txtQuantitytransfer.ClientID %>').focus();
                    //return false;
                    }
                    if (Show != '') {
                        ShowPopupMessageBox(Show);
                        return false;
                    }

                    else {
                        return true;
                    }

                    return true;
                }
                catch (err) {
                    Show = Show + (err.message);
                    return false;
                }

            }

            function fncGenVoucherNo(Qty) {
                var Qty = $('#<%=txtQuantity.ClientID%>').val()
            if (Qty == 0) {
                alert('Invalid Quantity!')
                $('#<%=txtQuantity.ClientID %>').focus();
                return false;
            }
            var StartNo = $('#<%=txtStartNo.ClientID%>').val();
            var Prefix = $('#<%=txtPrefix.ClientID%>').val().toUpperCase();
            if (!StartNo.search('GV')) {
                StartNo = StartNo.substr(StartNo.length - 8)
                //alert(StartNo);
            }
            var EndNo = parseInt(StartNo) + parseInt(Qty);
            //alert(EndNo);
            var zero4 = new Padder(8);
            var Stcode = zero4.pad(StartNo);
            var Endcode = zero4.pad(EndNo - 1);
            //alert(Endcode);
            //alert(code);
            StartNo = "GV" + Prefix + Stcode;
            Endcode = "GV" + Prefix + Endcode;
            $('#<%=txtStartNo.ClientID%>').val(StartNo);
            $('#<%=txtEndNo.ClientID%>').val(Endcode);

            fncGenVoucherNoList();
        }

        function fncGenVoucherNoList() {

            var Qty = $('#<%=txtQuantity.ClientID%>').val()
            var StartNo = $('#<%=txtStartNo.ClientID%>').val();
            var Prefix = $('#<%=txtPrefix.ClientID%>').val().toUpperCase();
            if (!StartNo.search('GV')) {
                StartNo = StartNo.substr(StartNo.length - 8)
            }

            var arr = [];
            var sGVNo;
            for (var i = 1; i <= Qty; i++) {
                //alert(i);
                var zero4 = new Padder(8);
                var Stcode = zero4.pad(StartNo);
                sGVNo = "GV" + Prefix + Stcode;
                arr.push(sGVNo);
                StartNo = parseInt(StartNo) + 1;
            }
            alert(arr);
            $("#HidenGVNumbers").val(arr);

        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function isAlfa(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;
            }
            $('#<%=txtStartNo.ClientID%>').val('');
            $('#<%=txtEndNo.ClientID%>').val('');
            $('#<%=txtQuantity.ClientID%>').val('');
            return true;
        }

        $(function () {

            $("#<%= DropdownDenamination.ClientID %>").change(function () {
                var status = this.value;
                var sDvalue = $('#<%=DropdownDenamination.ClientID%>').val();
                $('#<%=txtValue.ClientID%>').val(sDvalue);
                setTimeout(function () {
                    $('#<%=txtPrefix.ClientID %>').focus().select();
                }, 100);
            });

        });

        $(function () {

            $("#<%= DropDownBookNo.ClientID %>").change(function () {
                var status = this.value;
                var sDvalue = $('#<%=DropDownBookNo.ClientID%>').val();
                $('#<%=txtDenaminationtransfer.ClientID%>').val(sDvalue);


                return false;
            });


        });

        function GetQuantity() {
            //var myLength = $("#myTextbox").val().length;
            if (($('#<%=txtFromtransfer.ClientID %>').val().length) && ($('#<%=txtFromtransfer.ClientID %>').val().length) == '11') {
                var from = parseFloat($('#<%=txtFromtransfer.ClientID %>').val().substring(5, 11));
                var to = parseFloat($('#<%=txtTotransfer.ClientID %>').val().substring(5, 11));
                var quantity = parseFloat(to - from) + 1;
                $('#<%=txtQuantitytransfer.ClientID %>').val(quantity);
            }
            else if (($('#<%=txtFromtransfer.ClientID %>').val().length) && ($('#<%=txtFromtransfer.ClientID %>').val().length) == '12') {
                var from = parseFloat($('#<%=txtFromtransfer.ClientID %>').val().substring(5, 12));
                var to = parseFloat($('#<%=txtTotransfer.ClientID %>').val().substring(5, 12));
                var quantity = parseFloat(to - from) + 1;
                $('#<%=txtQuantitytransfer.ClientID %>').val(quantity);

            }
            else if (($('#<%=txtFromtransfer.ClientID %>').val().length) == '11' && ($('#<%=txtFromtransfer.ClientID %>').val().length) == '12') {
                var from = parseFloat($('#<%=txtFromtransfer.ClientID %>').val().substring(5, 11));
            var to = parseFloat($('#<%=txtTotransfer.ClientID %>').val().substring(5, 12));
            var quantity = parseFloat(to - from) + 1;
            $('#<%=txtQuantitytransfer.ClientID %>').val(quantity);
        }
        else if (($('#<%=txtFromtransfer.ClientID %>').val().length) == '12' && ($('#<%=txtFromtransfer.ClientID %>').val().length) == '11') {
            var from = parseFloat($('#<%=txtFromtransfer.ClientID %>').val().substring(5, 12));
                 var to = parseFloat($('#<%=txtTotransfer.ClientID %>').val().substring(6, 11));
                 var quantity = parseFloat(to - from) + 1;
                 $('#<%=txtQuantitytransfer.ClientID %>').val(quantity);
        }
        else if (($('#<%=txtFromtransfer.ClientID %>').val().length) == '10' && ($('#<%=txtFromtransfer.ClientID %>').val().length) == '10') {
            var from = parseFloat($('#<%=txtFromtransfer.ClientID %>').val().substring(3, 10));
            var to = parseFloat($('#<%=txtTotransfer.ClientID %>').val().substring(3, 10));
            var quantity = parseFloat(to - from) + 1;
            $('#<%=txtQuantitytransfer.ClientID %>').val(quantity);
        }
    {

    }

}

function transferclear() {
    try {
        $('#<%=LocationDropDown.ClientID %>').val('');
        $('#<%=DropDownBookNo.ClientID %>').val('');
        $('#<%=txtDenaminationtransfer.ClientID %>').val('');
        $('#<%=txtFromtransfer.ClientID %>').val('');
        $('#<%=txtTotransfer.ClientID %>').val('');
        $('#<%=txtQuantitytransfer.ClientID %>').val('');
    }
    catch (err) {
        fncToastError(err);
    }
}


//$(function () {
//    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "VoucherBook";
//    $('#Tabs a[href="#' + tabName + '"]').tab('show');
//    $("#Tabs ul li a").click(function () {
//        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
//    });
//});


function Clearall() {

    $("select").val(0);
    $("select").trigger("liszt:updated");
    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
    $("table[id$='grdGiftVoucher']").html("");
    // $('#<%=lnkSave.ClientID %>').show();

            return false;
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'GiftVoucherCreation');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "GiftVoucherCreation";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container" style="overflow: hidden;">

        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Gift Voucher Creation</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="Tabs" role="tabpanel">
                    <ul class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liVoucherBook" class="active attribute_tabwidth"><a href="#VoucherBook" aria-controls="VoucherBook" role="tab" data-toggle="tab">Voucher Book</a></li>
                        <li id="liTransfer" class="attribute_tabwidth"><a href="#Transfer" aria-controls="Transfer" role="tab" data-toggle="tab">Transfer</a></li>
                    </ul>

                    <div class="tab-content" style="padding-top: 5px; overflow: hidden; height: auto">
                        <div class="tab-pane active" role="tabpanel" id="VoucherBook">
                            <div id="divVoucherCreation" class="container-control" style="width: 70%">
                                <%-- <div  id="divVoucherCreation" runat="server">--%>
                                <div class="container-left">
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Book No"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBookNo" MaxLength="10" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text="Denamination"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="DropdownDenamination" runat="server" CssClass="chzn-select">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label13" runat="server" Text="Value"></asp:Label><span class="mandatory">*</span>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtValue" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">

                                                <asp:Label ID="Label14" runat="server" Text="Start No"></asp:Label><span class="mandatory">*</span>
                                                <span style="float: right; color: royalblue">Prefix</span>
                                            </div>
                                            <div class="label-right">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtPrefix" MaxLength="2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                                <div class="col-md-8">

                                                    <asp:TextBox ID="txtStartNo" MaxLength="12" runat="server" onkeypress="return isNumber(event)" CssClass="form-control-res"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label15" runat="server" Text="Quantity"></asp:Label><span class="mandatory">*</span>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtQuantity" MaxLength="50" runat="server" onkeypress="return isNumber(event)" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label16" runat="server" Text="End No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtEndNo" MaxLength="50" runat="server" onkeypress="return isNumber(event)" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="Label17" runat="server" Text="Validity"></asp:Label><span class="mandatory">*</span>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtValidity" MaxLength="50" runat="server" Placeholder="Days" CssClass="form-control-res"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="padding: 10px 10px 0px 10px;">
                                    <div class="container-group-small" style="width: 100%;">
                                        <div class="container-control">
                                            <div class="control-group-single-res">

                                                <div class="terminal-detail-header">
                                                    BarCode Setting
                                                </div>
                                            </div>
                                            <div class="control-group-single-res">
                                                <div class="col-md-8">
                                                    <asp:Label ID="Label18" runat="server" Text="Hide Company Name" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:CheckBox ID="chHideCompName" runat="server" />
                                                </div>
                                            </div>
                                            <div class="control-group-single-res">
                                                <div class="label-left">
                                                    <asp:Label ID="Label19" runat="server" Text="Template"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtTemplate" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res">
                                                <div class="label-left">
                                                    <asp:Label ID="Label20" runat="server" Text="Size"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="ddSize" class="chzn-select" CssClass="chzn-select" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="button-contol">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click"
                                            OnClientClick="return ValidateForm()" class="button-red"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return Clearall()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkDelete" runat="server" class="button-red" OnClientClick="return DeleteConfirmbox()" OnClick="lnkDelete_Click"><i class="icon-play"></i>Delete(F3)</asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkBarcode" runat="server" class="button-red"><i class="icon-play"></i>Barcode</asp:LinkButton>
                                    </div>

                                </div>
                            </div>

                            <div class="right-container-top-detail">
                                <div class="GridDetails">
                                    <div class="row">
                                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                            <div class="grdLoad">
                                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Book No
                                                            </th>
                                                            <th scope="col">Denamination
                                                            </th>
                                                            <th scope="col">Value
                                                            </th>
                                                            <th scope="col">GVNo
                                                            </th>
                                                            <th scope="col">Validity
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 197px; width: 1317px; background-color: aliceblue;">
                                                    <asp:GridView ID="gdVoucherbook" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false" CssClass="pshro_GridDgn grdLoad">
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                            NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                            <asp:BoundField DataField="BookNo" HeaderText="Book No"></asp:BoundField>
                                                            <asp:BoundField DataField="Denamination" HeaderText="Denamination"></asp:BoundField>
                                                            <asp:BoundField DataField="DValue" HeaderText="Value"></asp:BoundField>
                                                            <asp:BoundField DataField="GvNo" HeaderText="GVNo"></asp:BoundField>
                                                            <asp:BoundField DataField="Validity" HeaderText="Validity"></asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="tab-pane" role="tabpanel" id="Transfer">
                            <div class="col-md-4" style="padding: 10px 30px 0px 400px;">
                                <div class="container-group-small">
                                    <%--<div class="container-left-price" id="divVoucherTransfer" runat="server">--%>
                                    <div class="container-control">
                                        <div class="control-group-single-res">
                                            <div class="label-left">
                                                <asp:Label ID="blss" runat="server" Text="Location"></asp:Label><span class="mandatory">*</span>
                                            </div>
                                            <div class="label-right">
                                                <asp:DropDownList ID="LocationDropDown" Width="233px" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="control-group-single-res">
                                                <div class="label-left">
                                                    <asp:Label ID="Label3" runat="server" Text="Book No"></asp:Label><span class="mandatory">*</span>
                                                </div>
                                                <div class="label-right">
                                                    <asp:DropDownList ID="DropDownBookNo" Width="233px" runat="server" CssClass="form-control-res"
                                                        onchange="fncGetVoucherDetail();">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label4" runat="server" Text="Denamination"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDenaminationtransfer" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label5" runat="server" maxlength ="12" Text="From"></asp:Label><span class="mandatory">*</span>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtFromtransfer" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label1" runat="server" Text="To"></asp:Label><span class="mandatory">*</span>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtTotransfer" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label6" runat="server" Text="Quantity"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtQuantitytransfer" onchange="GetQuantity();" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="button-contol">
                                                    <div class="control-button">
                                                        <asp:LinkButton ID="lnkTransfer" OnClientClick="return ValidateTransfer()" OnClick="lnkTransfer_Click" runat="server" class="button-red"><i class="icon-play"></i>Transfer</asp:LinkButton>
                                                    </div>
                                                    <div class="control-button">
                                                        <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClientClick="javascript:window.close();"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkLoadBookList" Visible="false" runat="server" class="button-red"
                                                    OnClick="lnkLoadBookList_Click"><i class="icon-play"></i>Book List</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="HidenGVNumbers" runat="server" ClientIDMode="Static" Value="" />
    </div>
</asp:Content>
