﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPickerMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmPickerMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />
    <!-- Include Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Style -->
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 170px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 235px;
            max-width: 235px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 280px;
            max-width: 280px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 330px;
            max-width: 330px;
        }

        .switch-field {
            display: flex;
            margin-bottom: 36px;
            overflow: hidden;
        }

            .switch-field input {
                position: absolute !important;
                clip: rect(0, 0, 0, 0);
                height: 1px;
                width: 1px;
                border: 0;
                overflow: hidden;
            }

            .switch-field label {
                background-color: #e4e4e4;
                color: rgba(0, 0, 0, 0.6);
                font-size: 14px;
                line-height: 1;
                text-align: center;
                padding: 8px 16px;
                margin-right: -1px;
                border: 1px solid rgba(0, 0, 0, 0.2);
                box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);
                transition: all 0.1s ease-in-out;
            }

                .switch-field label:hover {
                    cursor: pointer;
                }

            .switch-field input:checked + label {
                background-color: #a5dc86;
                box-shadow: none;
            }

            .switch-field label:first-of-type {
                border-radius: 4px 0 0 4px;
            }

            .switch-field label:last-of-type {
                border-radius: 0 4px 4px 0;
            }

        /* This is just for CodePen. */

        .form {
            max-width: 600px;
            font-family: "Lucida Grande", Tahoma, Verdana, sans-serif;
            font-weight: normal;
            line-height: 1.625;
            margin: 8px auto;
            padding: 16px;
        }

        h2 {
            font-size: 18px;
            margin-bottom: 8px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=txtPickerName.ClientID%>').keypress(function (e) {
                var regex = new RegExp("^[a-zA-Z0-9]+$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });
        });

        function OnClearEvent() {
            $("#<%=txtPickerName.ClientID%>").val("");
            $("#<%=txtPickerCode.ClientID%>").val("");
            $('input:radio[name=radioName][value=true]').attr('checked', true);
        }

        function OnClieckEvent() {                      
            var pickername = $("#<%=txtPickerName.ClientID%>").val();
            var pickerid = $("#<%=txtPickerCode.ClientID%>").val();
            if (pickername != "" && pickerid != "") {
                if ($("#<%=lnkSave.ClientID%>").val() == "") {
                    $.ajax({
                        url: "frmPickerMaster.aspx/GetPickerName",
                        data: "{ 'Code': '" + pickername + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {                            
                            if (data.d != null) {
                                ShowPopupMessageBox("This " + data.d + " Name already yours..!");
                                $("#<%=txtPickerName.ClientID%>").val("");
                                $("#<%=txtPickerCode.ClientID%>").val("")
                            }
                            else {
                                $.ajax({
                                    url: "frmPickerMaster.aspx/SavePickerName",
                                    data: "{ 'PickerCode': '" + $("#<%=txtPickerCode.ClientID%>").val() + "','PickerName': '" + $("#<%=txtPickerName.ClientID%>").val() + "','PickerStatus': '" + $('input[name="radioName"]:checked').val() + "','Type': '" + $("#<%=lnkSave.ClientID%>").val() + "','CreateName': '" + $('#<%=hidUserName.ClientID%>').val() + "'}",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        if (data.d = "true") {
                                            if (Type != "Update") {
                                                $('#<%=lnkSave.ClientID %>').val("Save");
                                                ShowPopupMessageBox("Data saved successfully...");
                                                $("#<%=txtPickerName.ClientID%>").val("");     
                                                location.reload();
                                            }
                                            else {
                                                $('#<%=lnkSave.ClientID %>').val("Save");
                                                ShowPopupMessageBox("Data Update successfully...");
                                                $("#<%=txtPickerName.ClientID%>").val("");
                                                location.reload();
                                            }
                                        }
                                        else {
                                            ShowPopupMessageBox("Data not saved something went wrong.");
                                            $("#<%=txtPickerName.ClientID%>").val("");
                                        }
                                    },
                                    error: function (response) {
                                        ShowPopupMessageBox(response.responseText);
                                    },
                                    failure: function (response) {
                                        ShowPopupMessageBox(response.responseText);
                                    }
                                });
                            }
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                }
                else {
                    $.ajax({
                        url: "frmPickerMaster.aspx/SavePickerName",
                        data: "{ 'PickerCode': '" + $("#<%=txtPickerCode.ClientID%>").val() + "','PickerName': '" + $("#<%=txtPickerName.ClientID%>").val() + "','PickerStatus': '" + $('input[name="radioName"]:checked').val() + "','Type': '" + $("#<%=lnkSave.ClientID%>").val() + "','CreateName': '" + $('#<%=hidUserName.ClientID%>').val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d = "true") {
                                if (Type != "Update") {
                                    $('#<%=lnkSave.ClientID %>').val("Save");
                                    ShowPopupMessageBox("Data saved successfully...");
                                    $("#<%=txtPickerName.ClientID%>").val("");
                                    location.reload();
                                }
                                else {
                                    $('#<%=lnkSave.ClientID %>').val("Save");
                                    ShowPopupMessageBox("Data Update successfully...");
                                    $("#<%=txtPickerName.ClientID%>").val("");
                                    location.reload();
                                }
                            }
                            else {
                                ShowPopupMessageBox("Data not saved something went wrong.");
                                $("#<%=txtPickerName.ClientID%>").val("");
                            }
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                }
            }
            else {
                ShowPopupMessageBox("Picker ID or Name field empty..!");
            }
        }
        function ValidationName(PickerName) {
            var returnvalue;
            $.ajax({
                url: "frmPickerMaster.aspx/GetPickerName",
                data: "{ 'Code': '" + PickerName + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {                    
                    if (data.d != null) {
                        returnvalue = data.d;
                        return returnvalue;
                    }
                },
                error: function (response) {
                    ShowPopupMessageBox(response.responseText);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.responseText);
                }
            });

        }
        function Picker_Delete(source) {
            
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Bin");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                        $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }

                }
            });
        }
        function imgbtnEdit_ClientClick(source) {
            
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Bin");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function DisplayDetails(row) {
            $('#<%=lnkSave.ClientID %>').val("Update");
            $('#<%=txtPickerCode.ClientID %>').prop("disabled", true);
            $('#<%=txtPickerCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
            $('#<%=txtPickerName.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            if ($("td", row).eq(4).html().replace(/&nbsp;/g, '') == 'True') {
                $('input:radio[name=radioName][value=true]').attr('checked', true);
            }
            else {
                $('input:radio[name=radioName][value=false]').attr('checked', true);
            }
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Picker Master</li>
            </ul>
        </div>

        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">

                                <asp:Label ID="Label1" runat="server" Text="Picker ID"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPickerCode" onpaste="return false;" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">

                                <asp:Label ID="Label2" runat="server" Text="Picker Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPickerName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">

                                <asp:Label ID="Label4" runat="server" Text="Picker Status"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <div class="switch-field">
                                    <input type="radio" id="radio-one" name="radioName" value="true" checked />
                                    <label id="lbl1" for="radio-one">Yes</label>
                                    <input type="radio" id="radio-two" name="radioName" value="false" />
                                    <label id="lbl2" for="radio-two">No</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" OnClientClick="OnClieckEvent()" runat="server" class="button-red"><i class="icon-play"></i>Save</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" OnClientClick="OnClearEvent()"  runat="server" class="button-red"><i class="icon-play" ></i>Clear</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Name To Search"></asp:TextBox>&nbsp;&nbsp;
                           
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                            <asp:Label ID="lblcount" runat="server" Style="font-weight: 900;"></asp:Label>
                        </asp:Panel>
                    </div>

                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Picker Code
                                                    </th>
                                                    <th scope="col">Picker Status
                                                    </th>
                                                    <th scope="col">Picker Name
                                                    </th>
                                                    <th scope="col">Create By
                                                    </th>
                                                    <th scope="col">Create Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 152px; width: 1348px; background-color: aliceblue;">
                                            <asp:GridView ID="gvPicker" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                oncopy="return false" DataKeyNames="PickerID" CssClass="pshro_GridDgn grdLoad">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick="return Picker_Delete(this);  return false;"
                                                                CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="PickerID" HeaderText="Picker Code"></asp:BoundField>
                                                    <asp:BoundField DataField="PickerName" HeaderText="Picker Name"></asp:BoundField>
                                                    <asp:BoundField DataField="PickerStatus" HeaderText="Picker Status"></asp:BoundField>
                                                    <asp:BoundField DataField="CreateBy" HeaderText="Create By"></asp:BoundField>
                                                    <asp:BoundField DataField="CreateDate" HeaderText="Create Date" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ups:PaginationUserControl runat="server" ID="PickerPaging" OnPaginationButtonClick="PickerPaging_PaginationButtonClick" />
                </div>
                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidUserName" runat="server" />
</asp:Content>

