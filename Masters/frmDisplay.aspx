﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="    .aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmDisplay" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 138px;
            max-width: 138px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 124px;
            max-width: 124px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 40px;
            max-width: 40px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 40px;
            max-width: 40px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 123px;
            max-width: 123px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 122px;
            max-width: 122px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 122px;
            max-width: 122px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 122px;
            max-width: 122px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <script type="text/javascript">


        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtdisplaycode.ClientID%>").value == "") {
            Show = Show + 'Please Enter Display Code';
            document.getElementById("<%=txtdisplaycode.ClientID %>").focus();
        }


        if (document.getElementById("<%=txtdisplayname.ClientID%>").value == "") {
            Show = Show + '<br />Please Enter Display Name';
            document.getElementById("<%=txtdisplayname.ClientID %>").focus();
        }

        if (document.getElementById("<%=ddlDepartment.ClientID%>").value == 0) {
            Show = Show + '<br />Please Enter Department';
            document.getElementById("<%=ddlDepartment.ClientID %>").focus();
        }

        if (Show != '') {
            ShowPopupMessageBox(Show);
            return false;
        }

        else {
            $("input").removeAttr('disabled');
            return true;
        }
    }
    function Display_Delete() {

        if (confirm("Do you want to delete Display Details data?")) {
            return;
        } else {
            return false;
        }

    }
    function imgbtnEdit_ClientClick(source) {

        DisplayDetails($(source).closest("tr"));

    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function DisplayDetails(row) {
        $('#<%=txtdisplaycode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
        $('#<%=txtdisplayname.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
        $('#<%=ddlLocation.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
        $('#<%=ddlLocation.ClientID %>').trigger("liszt:updated");
        $('#<%=ddlDepartment.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
        $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
        $('#<%=ddlFloor.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
        $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
        $('#<%=ddlShelf.ClientID %>').val($("td", row).eq(7).html().replace(/&nbsp;/g, ''));
        $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
        $('#<%=txtperday.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
        $('#<%=txtpermonth.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));
        $('#<%=txtpersoninchange.ClientID %>').val($("td", row).eq(10).html().replace(/&nbsp;/g, ''));
        $('#<%=txtremarks.ClientID %>').val($("td", row).eq(11).html().replace(/&nbsp;/g, ''));
        if ($("td", row).eq(12).html().replace(/&nbsp;/g, '') == "1") {
            $('#<%= chkActive.ClientID %>').attr("checked", "checked");
        }
        else {

            $('#<%= chkActive.ClientID %>').removeAttr("checked");
        }

    }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' });


        }
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);

            $('#<%=ddlLocation.ClientID %>').val('');
            $('#<%=ddlLocation.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlDepartment.ClientID %>').val('');
            $('#<%=ddlDepartment.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlShelf.ClientID %>').val('');
            $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
            $('#<%=ddlFloor.ClientID %>').val('');
            $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
        }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'Display');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "Display";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Display Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>

                <div class="container-group-small">
                    <div class="container-control">

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Display Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtdisplaycode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Display Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtdisplayname" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Location"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text="Department"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="Floor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text="Shelf"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>


                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label9" runat="server" Text="Per Day"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtperday" runat="server" onkeypress="return isNumberKey(event)" MaxLength="3" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label10" runat="server" Text="Per Month"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtpermonth" runat="server" onkeypress="return isNumberKey(event)" MaxLength="3" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Person In-Change"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtpersoninchange" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="Remarks"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtremarks" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Active"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkActive" runat="server" />
                            </div>
                        </div>

                    </div>
                    <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red"
                                OnClientClick="return ValidateForm()" Text='<%$ Resources:LabelCaption,lnkSave %>' OnClick="lnkSave_Click"><i class="icon-play"></i></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                Text='<%$ Resources:LabelCaption,lnkClear %>'><i class="icon-play" ></i></asp:LinkButton>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <hr />
        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                        </asp:Panel>
                    </div>
                    <div class="gridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="right-container-top-detail">

                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Display Code
                                                    </th>
                                                    <th scope="col">Display Name
                                                    </th>
                                                    <th scope="col">Location Code
                                                    </th>
                                                    <th scope="col">Department code
                                                    </th>
                                                    <th scope="col">Floor
                                                    </th>
                                                    <th scope="col">Shelf
                                                    </th>
                                                    <th scope="col">DisplayCharges PerDay
                                                    </th>
                                                    <th scope="col">DisplayCharges PerMonth
                                                    </th>
                                                    <th scope="col">Person In charge
                                                    </th>
                                                    <th scope="col">remarks
                                                    </th>
                                                    <th scope="col">Active
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 51px; width: 1330px; background-color: aliceblue;">
                                            <asp:GridView ID="gvDisplay" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                                ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="DisplayCode" OnSelectedIndexChanged="imgbtnDelete_Click">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Display_Delete()"
                                                                CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DisplayCode" HeaderText="Display Code"></asp:BoundField>
                                                    <asp:BoundField DataField="DisplayName" HeaderText="Display Name"></asp:BoundField>
                                                    <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                    <asp:BoundField DataField="Departmentcode" HeaderText="Department code"></asp:BoundField>
                                                    <asp:BoundField DataField="Floor" HeaderText="Floor"></asp:BoundField>
                                                    <asp:BoundField DataField="Shelf" HeaderText="Shelf"></asp:BoundField>
                                                    <asp:BoundField DataField="DisplayChargesPerDay" HeaderText="DisplayChargesPerDay" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="DisplayChargesPerMonth" HeaderText="DisplayChargesPerMonth" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="PersonIncharge" HeaderText="Person In charge"></asp:BoundField>
                                                    <asp:BoundField DataField="remarks" HeaderText="remarks"></asp:BoundField>
                                                    <asp:BoundField DataField="Active" HeaderText="Active"></asp:BoundField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ups:PaginationUserControl runat="server" ID="DisplayPaging" OnPaginationButtonClick="Display_PaginationButtonClick" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
