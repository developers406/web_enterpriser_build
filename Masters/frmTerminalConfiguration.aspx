﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmTerminalConfiguration.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmTerminalConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 170px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 182px;
            max-width: 182px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {

            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        } 
        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            display: none;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 182px;
            max-width: 182px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 120px;
            max-width: 120px;
            text-align: center;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
            text-align: center;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 290px;
            max-width: 290px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
            text-align: center;
        }


        .grdLoad2 td:nth-child(1), .grdLoad2 th:nth-child(1) {
            min-width: 284px;
            max-width: 284px;
        }

        .grdLoad2 td:nth-child(2), .grdLoad2 th:nth-child(2) {
            min-width: 133px;
            max-width: 133px;
            text-align: center;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <script type="text/javascript">
        function pageLoad() {
            
             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                   $('#<%=lnkSave.ClientID %>').css("display", "none");
             }

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $(document).ready(function () {
                $('#<%=chkIsPrinterAttached.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=chkIsPrinterAttached.ClientID %>').is(':checked')) {
                            $('#<%=rbtOPOS.ClientID %>').prop("disabled", false);
                            $('#<%=rbtBatch.ClientID %>').prop("disabled", false);
                            $('#<%=rbtDirect.ClientID %>').prop("disabled", false);
                            $('#<%=rbtPrintTem.ClientID %>').prop("disabled", false);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", false);
                            if ($('#<%=rbtBatch.ClientID %>').is(':checked')) {
                                $('#<%=txtPrintPort.ClientID %>').prop("disabled", false);
                                $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                            }
                            if ($('#<%=rbtDirect.ClientID %>').is(':checked')) {
                                $('#<%=txtPrintPort.ClientID %>').prop("disabled", true);
                                $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                                $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", true);
                                $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                            }
                            if ($('#<%=rbtPrintTem.ClientID %>').is(':checked')) {
                                $('#<%=txtPrintPort.ClientID %>').prop("disabled", true);
                                $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                                $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", true);
                                $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                            }
                        }
                        else {
                            $('#<%=rbtOPOS.ClientID %>').prop("disabled", true);
                            $('#<%=rbtBatch.ClientID %>').prop("disabled", true);
                            $('#<%=rbtDirect.ClientID %>').prop("disabled", true);
                            $('#<%=rbtPrintTem.ClientID %>').prop("disabled", true);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop('checked', false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtOPOS.ClientID %>').change(function () {
                    debugger;
                    try {
                        if ($('#<%=rbtOPOS.ClientID %>').is(':checked')) {
                            $('#<%=txtPrintPort.ClientID %>').prop("disabled", true);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", false);
                            $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", true);
                            $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop('checked', false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtBatch.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=rbtBatch.ClientID %>').is(':checked')) {
                            $('#<%=txtPrintPort.ClientID %>').prop("disabled", false);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtDirect.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=rbtDirect.ClientID %>').is(':checked')) {
                            $('#<%=txtPrintPort.ClientID %>').prop("disabled", true);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", true);
                            $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop('checked', false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtPrintTem.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=rbtPrintTem.ClientID %>').is(':checked')) {
                            $('#<%=txtPrintPort.ClientID %>').prop("disabled", true);
                            $('#<%=txtLogicalDeviceName.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop("disabled", true);
                            $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                            $('#<%=chkNetUseCommand.ClientID %>').prop('checked', false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=chkNetUseCommand.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=chkNetUseCommand.ClientID %>').is(':checked')) {
                            $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", false);
                        }
                        else {
                            $('#<%=txtNormalPrinter.ClientID %>').prop("disabled", true);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=chkPoleDisplay.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=chkPoleDisplay.ClientID %>').is(':checked')) {
                            $('#<%=txtPoleDisplayName.ClientID %>').prop("disabled", false);
                        }
                        else {
                            $('#<%=txtPoleDisplayName.ClientID %>').prop("disabled", true);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=chkCashDrawer.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=chkPoleDisplay.ClientID %>').is(':checked')) {
                            $('#<%=txtCashDrawerName.ClientID %>').prop("disabled", false);
                        }
                        else {
                            $('#<%=txtCashDrawerName.ClientID %>').prop("disabled", true);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=chkWeighingMachineAttached.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=chkWeighingMachineAttached.ClientID %>').is(':checked')) {
                            $('#<%=txtSystemPort.ClientID%>').prop("disabled", false);
                            $('#<%=ddlParity.ClientID%>').prop("disabled", false);
                            $('#<%=ddlBits.ClientID%>').prop("disabled", false);
                            $('#<%=ddlStopBits.ClientID%>').prop("disabled", false);
                            $('#<%=ddlDataBits.ClientID%>').prop("disabled", false);
                            $('#<%=rbtSerialPort.ClientID%>').prop("disabled", false);
                            $('#<%=rbtWeighingOPOS.ClientID%>').prop("disabled", false);
                            $('#<%=btnRestore.ClientID%>').prop("disabled", false);
                        }
                        else {
                            $('#<%=txtSystemPort.ClientID%>').prop("disabled", true);
                            $('#<%=ddlParity.ClientID%>').prop("disabled", true);
                            $('#<%=ddlBits.ClientID%>').prop("disabled", true);
                            $('#<%=ddlStopBits.ClientID%>').prop("disabled", true);
                            $('#<%=ddlDataBits.ClientID%>').prop("disabled", true);
                            $('#<%=rbtSerialPort.ClientID%>').prop("disabled", true);
                            $('#<%=rbtWeighingOPOS.ClientID%>').prop("disabled", true);
                            $('#<%=btnRestore.ClientID%>').prop("disabled", true);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtWeighingOPOS.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=rbtWeighingOPOS.ClientID %>').is(':checked')) {
                            $('#<%=txtSystemPort.ClientID%>').prop("disabled", true);
                            $('#<%=ddlParity.ClientID%>').prop("disabled", true);
                            $('#<%=ddlBits.ClientID%>').prop("disabled", true);
                            $('#<%=ddlStopBits.ClientID%>').prop("disabled", true);
                            $('#<%=ddlDataBits.ClientID%>').prop("disabled", true);
                            $('#<%=btnRestore.ClientID%>').prop("disabled", true);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

                $('#<%=rbtSerialPort.ClientID %>').change(function () {
                    try {
                        debugger;
                        if ($('#<%=rbtSerialPort.ClientID %>').is(':checked')) {
                            $('#<%=txtSystemPort.ClientID%>').prop("disabled", false);
                            $('#<%=ddlParity.ClientID%>').prop("disabled", false);
                            $('#<%=ddlBits.ClientID%>').prop("disabled", false);
                            $('#<%=ddlStopBits.ClientID%>').prop("disabled", false);
                            $('#<%=ddlDataBits.ClientID%>').prop("disabled", false);
                            $('#<%=btnRestore.ClientID%>').prop("disabled", false);
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });
            });

            $(function () {
                $("[id*=btnRestore]").click(function () {
                    try {
                        debugger;
                        $('#<%=ddlParity.ClientID%>').val('3');
                        $('#<%=ddlBits.ClientID%>').val('6');
                        $('#<%=ddlStopBits.ClientID%>').val('1');
                        $('#<%=ddlDataBits.ClientID%>').val('4');
                        return false;
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });

            });
        }

         function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                    e.preventDefault();
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });


    </script>
    <script type="text/javascript">
        function Check_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            var iChk = 0;
            //if (objRef.checked) {
            //    //If checked change color to Aqua
            //    row.style.backgroundColor = "aqua";
            //}
            //else {
            //    //If not checked change back to original color
            //    if (row.rowIndex % 2 == 0) {
            //        //Alternating Row Color
            //        row.style.backgroundColor = "#c4ddff";
            //    }
            //    else {
            //        row.style.backgroundColor = "white";
            //    }
            //}
            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                //The First element is the Header Checkbox
                var headerCheckBox = inputList[0];
                //Based on all or none checkboxes
                //are checked check/uncheck Header Checkbox
                var checked = true;   
                if (inputList[i].type == "checkbox" && inputList[i] == headerCheckBox ) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }
    </script>
    <script type="text/javascript">

        function checkAllDept(source) {
            try {
                if ($(source).is(":checked")) {
                    $("#ContentPlaceHolder1_grdDepartment tbody").children().each(function () {
                        fncCalculateReOrderLevel(this, 'chkDeptAllow', '1');
                    });
                }
                else {
                    $("#ContentPlaceHolder1_grdDepartment tbody tr").find('td input[id*="chkDeptAllow"]').prop("checked", false);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncCalculateReOrderLevel(source, obj, hdrclick) {
            var minQty = 0, maxQty = 0, reOrderQty = 0, rowobj;
            try {

                if (hdrclick == "1") {
                    rowobj = $(source);
                }
                else {
                    rowobj = $(source).parent().parent();
                }
                if (reOrderQty < 0) {
                    rowobj.find('td input[id*="chkDeptAllow"]').prop("checked", false);
                    return false;
                }
                else {
                    rowobj.find('td input[id*="chkDeptAllow"]').prop("checked", true);
                    console.log(rowobj);
                    return true;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }



        function checkAllTran(source) {
            try {
                if ($(source).is(":checked")) {
                    $("#ContentPlaceHolder1_grdTransaction tbody").children().each(function () {
                        fncCalculateReOrderLevelTran(this, 'chkTranAllow', '1');
                    });
                }
                else {
                    $("#ContentPlaceHolder1_grdTransaction tbody tr").find('td input[id*="chkTranAllow"]').prop("checked", false);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncCalculateReOrderLevelTran(source, obj, hdrclick) {
            var minQty = 0, maxQty = 0, reOrderQty = 0, rowobj;
            try {

                if (hdrclick == "1") {
                    rowobj = $(source);
                }
                else {
                    rowobj = $(source).parent().parent();
                }
                if (reOrderQty < 0) {
                    rowobj.find('td input[id*="chkTranAllow"]').prop("checked", false);
                    return false;
                }
                else {
                    rowobj.find('td input[id*="chkTranAllow"]').prop("checked", true);
                    console.log(rowobj);
                    return true;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //function checkAll(objRef) {
        //    debugger;
        //    var GridView = objRef.parentNode.parentNode.parentNode;
        //    var inputList = GridView.getElementsByTagName("input");
        //    var iChk = 0;
        //    for (var i = 0; i < inputList.length; i++) {
        //        //Get the Cell To find out ColumnIndex
        //        var row = inputList[i].parentNode.parentNode;
        //        if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
        //            if (objRef.checked) {
        //                //If the header checkbox is checked
        //                //check all checkboxes
        //                //and highlight all rows
        //                //row.style.backgroundColor = "aqua";
        //                if (iChk % 2 === 0) {
        //                    inputList[i].checked = true;
        //                }
        //            }
        //            else {
        //                //If the header checkbox is checked
        //                //uncheck all checkboxes
        //                //and change rowcolor back to original 
        //                //if (row.rowIndex % 2 == 0) {
        //                //    //Alternating Row Color
        //                //    row.style.backgroundColor = "#c4ddff";
        //                //}
        //                //else {
        //                //    row.style.backgroundColor = "white";
        //                //}
        //                if (iChk % 2 === 0) {
        //                    inputList[i].checked = false;
        //                }
        //            }
        //            iChk = iChk + 1;
        //        }
        //    }
        //}

        //function checkAllDept(objRef) {
        //    debugger;
        //    var GridView = objRef.parentNode.parentNode.parentNode;
        //    var inputList = GridView.getElementsByTagName("input");
        //    var iChk = 0;
        //    for (var i = 0; i < inputList.length; i++) {
        //        //Get the Cell To find out ColumnIndex
        //        var row = inputList[i].parentNode.parentNode;
        //        if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
        //            if (objRef.checked) {
        //                inputList[i].checked = true;
        //            }
        //            else {
        //                inputList[i].checked = false;
        //            }
        //        }
        //    }
        //}
    </script>

    <script type="text/javascript">

        function Validate() {
            var Show = '';
            debugger;

            if (document.getElementById("<%=ddlLocation.ClientID%>").value == "") {
                Show = Show + '<br />  Please Select Location';
                document.getElementById("<%=ddlLocation.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtTillCode.ClientID%>").value == "") {
                Show = Show + '<br />  Please Enter Terminal Code';
                document.getElementById("<%=txtTillCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtTillDesc.ClientID%>").value == "") {
                Show = Show + '<br />  Please Enter Terminal Desc';
                document.getElementById("<%=txtTillDesc.ClientID %>").focus();
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else { 
                showAll();
                return true;
            }
            
        }

    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'TerminalConfiguration');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "TerminalConfiguration";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }



         function showAll() {

             $("#divsave").dialog({           //musaraf 21112022
                 dialogClass: "no-close",
                 title: "Enterpriser Web",
                 height: 160,
                 width: 400,
                 modal: true,
                 buttons: {
                     "OK": function () {
                         __doPostBack('ctl00$ContentPlaceHolder1$btnsave', '');
                     }

                 }
             }).html("Terminal Saved Successfully..!")
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Terminal View</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Terminal Configuration </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-full EnableScroll" style="display: table">
                    <div class="terminal-left">
                        <div class="terminal-detail">
                            <asp:Label ID="lblUserCount" runat="server" Font-Bold ="true"  Text="You have Reached Licensed level Terminals" Visible ="false" Style ="margin-left: 215px; background: darkturquoise;font-size: large;"></asp:Label>
                            <div class="terminal-detail-header">
                                Terminal Detail
                            </div>
                            <div class="terminal-detail-detail">
                                <div class="control-group-split-terminal">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_Location %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lblTillCode %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTillCode" runat="server" CssClass="form-control-res" MaxLength="2"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lblTillDesc %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTillDesc" runat="server" CssClass="form-control-res" MaxLength="25"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split-terminal">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblSystemName %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSysName" runat="server" CssClass="form-control-res" MaxLength="30"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblIsPhramacyTer %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkPhramacy" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblAutoLoginPos %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkAutoLogInPos" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split-terminal">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lblItemDelAllowed %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkItemDeleteAllowed" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblShowZero %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkNegStock" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split-terminal">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lblPosScreenType %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlPosScreen" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblSpecialCounter %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkSpecialCounter" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lblPriceChangeAllowed %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkPriceChangeAllow" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split-terminal">
                                    <div class="control-group-left1">
                                        <div class="label-left">
                                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblTillType %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:DropDownList ID="ddlTillType" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-middle">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblOpticalTerminal %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkOpticalTerminal" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right1">
                                        <div class="label-left">
                                            <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lblAllowDisCoupon %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:CheckBox ID="chkAllowDisCoupon" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tender-mapping">
                            <div class="tender-mapping-header">
                                Tender Mapping
                            </div>
                            <div class="tender-mapping-detail">
                                <div class="row">

                                    <div class="right-container-top-detail">
                                        <div class="GridDetails">
                                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                <div class="grdLoad">
                                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">TenderCode
                                                                </th>
                                                                <th scope="col">TenderType
                                                                </th>
                                                                <th scope="col">MinimumValue
                                                                </th>
                                                                <th scope="col">MaximumValue
                                                                </th>
                                                                <th scope="col">TenderAllowed
                                                                </th>
                                                                <th scope="col">RefundAllowed
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 101px; width: 850px; background-color: aliceblue;">
                                                        <asp:GridView ID="grdTender" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                            oncopy="return false" PageSize="5">
                                                            <EmptyDataTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                            </EmptyDataTemplate>
                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                            <PagerStyle CssClass="pshro_text" />
                                                            <Columns>
                                                                <asp:BoundField DataField="TenderCode" HeaderText="TenderCode"></asp:BoundField>
                                                                <asp:BoundField DataField="TenderType" HeaderText="TenderType"></asp:BoundField>
                                                                <asp:TemplateField HeaderText="MinimumValue" ItemStyle-Width="120">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtMinimumValue" runat="server" CssClass="grid-form-control" Text='<%# Eval("MinimumValue") %>'
                                                                            onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="MaximumValue" ItemStyle-Width="120">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtMaximumValue" runat="server" CssClass="grid-form-control" Text='<%# Eval("MaximumValue") %>'
                                                                            onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="TenderAllowed" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkTenderAllowed" runat="server" Checked='<%# Eval("TenderAllowed") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RefundAllowed" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkRefundAllowed" runat="server" Checked='<%# Eval("RefundAllowed") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="tran-setting">
                                <div class="tran-setting-header">
                                    Transaction Settings
                                </div>
                                <div class="tran-setting-detail">
                                    <div class="row">
                                        <div class="tender-mapping-detail">
                                            <div class="right-container-top-detail">
                                                <div class="GridDetails">
                                                    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                        <div class="grdLoad1">
                                                            <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">TenderCode
                                                                        </th>
                                                                        <th scope="col">TenderType
                                                                        </th>
                                                                        <th scope="col">
                                                                            <asp:CheckBox ID="checkAll" runat="server" onclick="checkAllTran(this);" />Allow         
                                                                        </th>
                                                                        <th scope="col">IsPrePrinted
                                                                        </th>
                                                                        <th scope="col">PrinterTemplate
                                                                        </th>
                                                                        <th scope="col">PrintCount
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 126px; width: 850px; background-color: aliceblue;">
                                                                <asp:GridView ID="grdTransaction" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                                    oncopy="return false" PageSize="5" OnRowDataBound="OnRowDataBound" ShowHeader="false">
                                                                    <EmptyDataTemplate>
                                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                                    </EmptyDataTemplate>
                                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                                    <PagerStyle CssClass="pshro_text" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Code" HeaderText="TransactionCode" ItemStyle-CssClass="hiddencol"
                                                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                                        <asp:BoundField DataField="TranType" HeaderText="TransactionType"></asp:BoundField>
                                                                        <asp:TemplateField ItemStyle-Width="100">
                                                                            <%--  <HeaderTemplate>
                                                                                        <asp:CheckBox ID="checkAll" runat="server" onclick="checkAll(this);" />
                                                                                        Allow
                                                                                    </HeaderTemplate>--%>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkTranAllow" runat="server" Checked='<%# Eval("Allow") %>' onclick="Check_Click(this)" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="IsPrePrinted" ItemStyle-Width="100">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkIsPreprinted" runat="server" Checked='<%# Eval("IsPrePrinted") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="PrinterTemplate">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTemplate" runat="server" Text='<%# Eval("TemplateCode") %>' Visible="false" />
                                                                                <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="form-control-res">
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="PrintCount" ItemStyle-Width="75">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtPrintCount" runat="server" CssClass="grid-form-control" Text='<%# Eval("PrintCount") %>'></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="terminal-right">
                        <div class="weighing">
                            <div class="panel panel-default" style="width: 100%; margin-bottom: 0px; display: table">
                                <div id="Tabs" role="tabpanel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs custnav" role="tablist">
                                        <li class="active" style="width: 15%"><a href="#printer" aria-controls="printer"
                                            role="tab" data-toggle="tab">Printer</a></li>
                                        <li style="width: 50%"><a href="#cashdrawer" aria-controls="cashdrawer" role="tab"
                                            data-toggle="tab">Pole Display/Cash Drawer</a></li>
                                        <li style="width: 35%"><a href="#weighingMachine" aria-controls="weighingMachine"
                                            role="tab" data-toggle="tab">Weighing Machine</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content" style="padding-top: 5px; overflow: auto; height: 150px">
                                        <div class="tab-pane active" role="tabpanel" id="printer">
                                            <div class="printer">
                                                <div class="printer-attached" style="float: left">
                                                    <asp:CheckBox ID="chkIsPrinterAttached" runat="server" />
                                                    Printer Attached &nbsp;
                                                </div>
                                                <div class="radio-printer" style="float: left; border: 1px solid #000; padding: 0 3px">
                                                    <asp:RadioButton ID="rbtOPOS" runat="server" CssClass="radio-text" GroupName="rbtPrinter"
                                                        Text=" OPOS" />
                                                    <asp:RadioButton ID="rbtBatch" runat="server" CssClass="radio-text" GroupName="rbtPrinter"
                                                        Text=" Batch" />
                                                    <asp:RadioButton ID="rbtDirect" runat="server" CssClass="radio-text" GroupName="rbtPrinter"
                                                        Text=" Direct" />
                                                    <asp:RadioButton ID="rbtPrintTem" runat="server" CssClass="radio-text" GroupName="rbtPrinter"
                                                        Text=" Print Temp" />
                                                </div>
                                                <div class="printer-port">
                                                    <div class="control-group-split">
                                                        <div class="control-group-left" style="width: 30%">
                                                            <div class="label-left" style="width: 60%">
                                                                <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lblPrintPort %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right" style="width: 40%">
                                                                <asp:TextBox ID="txtPrintPort" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right" style="width: 67%">
                                                            <div class="label-left" style="width: 55%">
                                                                <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblLogicalDevice %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right" style="width: 45%">
                                                                <asp:TextBox ID="txtLogicalDeviceName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:CheckBox ID="chkNetUseCommand" runat="server" />
                                                Net Use Command &nbsp;
                                                <div class="control-group-single" style="padding-left: 10%">
                                                    <div class="label-left" style="width: 40%">
                                                        <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lblNormalPrinter %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 60%">
                                                        <asp:TextBox ID="txtNormalPrinter" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single" style="padding-left: 10%">
                                                    <div class="label-left" style="width: 40%">
                                                        <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lblBarcodePrinter %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="width: 60%">
                                                        <asp:TextBox ID="txtBarcodePrinter" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="cashdrawer">
                                            <div class="cash">
                                                <div class="cash-line">
                                                    <div class="control-group-split">
                                                        <div class="control-group-left">
                                                            <div class="label-left" style="width: 100%">
                                                                <asp:CheckBox ID="chkPoleDisplay" runat="server" />
                                                                Pole Display Attached
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right">
                                                            <div class="label-left" style="width: 45%">
                                                                <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lblPort %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right" style="width: 55%">
                                                                <asp:TextBox ID="txtPort" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cash-line">
                                                    <div class="control-group-single">
                                                        <div class="label-left" style="width: 60%">
                                                            <asp:Label ID="Label20" runat="server" Text="Pole Display Logical Display Name"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 40%">
                                                            <asp:TextBox ID="txtPoleDisplayName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cash-line">
                                                    <div class="control-group-single">
                                                        <div class="label-left" style="width: 60%">
                                                            <asp:CheckBox ID="chkCashDrawer" runat="server" />
                                                            Cash Drawer Attached
                                                        </div>
                                                        <div class="label-right">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cash-line">
                                                    <div class="control-group-single">
                                                        <div class="label-left" style="width: 60%">
                                                            <asp:Label ID="Label21" runat="server" Text="Cash Drawer Logical Display Name"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 40%">
                                                            <asp:TextBox ID="txtCashDrawerName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="weighingMachine">
                                            <div class="weighing-tab">
                                                <div class="serial-header">
                                                    <div class="control-group-split">
                                                        <div class="control-group-left">
                                                            <div class="label-left" style="width: 100%">
                                                                <asp:CheckBox ID="chkWeighingMachineAttached" runat="server" />
                                                                Weighing Machine Attached
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right" style="border: 1px solid #000; padding: 0px 5px">
                                                            <div class="label-left" style="width: 60%">
                                                                <asp:RadioButton ID="rbtSerialPort" runat="server" GroupName="rbtWeighing" />
                                                                Serial Port
                                                            </div>
                                                            <div class="label-right" style="width: 40%">
                                                                <asp:RadioButton ID="rbtWeighingOPOS" runat="server" GroupName="rbtWeighing" />
                                                                OPOS
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="weighing-detail">
                                                    <div class="control-group-split">
                                                        <div class="control-group-left">
                                                            <div class="label-left">
                                                                <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lblPort %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right">
                                                                <asp:TextBox ID="txtSystemPort" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right">
                                                            <div class="label-left">
                                                                <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lblParity %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right">
                                                                <asp:DropDownList ID="ddlParity" runat="server" CssClass="form-control-res">
                                                                    <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                                                    <asp:ListItem Value="1">Even</asp:ListItem>
                                                                    <asp:ListItem Value="2">Odd</asp:ListItem>
                                                                    <asp:ListItem Value="3">None</asp:ListItem>
                                                                    <asp:ListItem Value="4">Mark</asp:ListItem>
                                                                    <asp:ListItem Value="5">Space</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-split">
                                                        <div class="control-group-left">
                                                            <div class="label-left">
                                                                <asp:Label ID="Label25" runat="server" Text="Bits/Sec"></asp:Label>
                                                            </div>
                                                            <div class="label-right">
                                                                <asp:DropDownList ID="ddlBits" runat="server" CssClass="form-control-res">
                                                                    <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                                                    <asp:ListItem Value="1">110</asp:ListItem>
                                                                    <asp:ListItem Value="2">300</asp:ListItem>
                                                                    <asp:ListItem Value="3">1200</asp:ListItem>
                                                                    <asp:ListItem Value="4">2400</asp:ListItem>
                                                                    <asp:ListItem Value="5">4800</asp:ListItem>
                                                                    <asp:ListItem Value="6">9600</asp:ListItem>
                                                                    <asp:ListItem Value="7">19200</asp:ListItem>
                                                                    <asp:ListItem Value="8">38400</asp:ListItem>
                                                                    <asp:ListItem Value="9">57600</asp:ListItem>
                                                                    <asp:ListItem Value="10">115200</asp:ListItem>
                                                                    <asp:ListItem Value="11">230400</asp:ListItem>
                                                                    <asp:ListItem Value="12">460800</asp:ListItem>
                                                                    <asp:ListItem Value="13">921600</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right">
                                                            <div class="label-left">
                                                                <asp:Label ID="Label26" runat="server" Text='<%$ Resources:LabelCaption,lblStopBits %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right">
                                                                <asp:DropDownList ID="ddlStopBits" runat="server" CssClass="form-control-res">
                                                                    <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                                                    <asp:ListItem Value="1">1</asp:ListItem>
                                                                    <asp:ListItem Value="2">1.5</asp:ListItem>
                                                                    <asp:ListItem Value="3">2</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-split">
                                                        <div class="control-group-left">
                                                            <div class="label-left">
                                                                <asp:Label ID="Label27" runat="server" Text='<%$ Resources:LabelCaption,lblDataBits %>'></asp:Label>
                                                            </div>
                                                            <div class="label-right">
                                                                <asp:DropDownList ID="ddlDataBits" runat="server" CssClass="form-control-res">
                                                                    <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                                                    <asp:ListItem Value="1">5</asp:ListItem>
                                                                    <asp:ListItem Value="2">6</asp:ListItem>
                                                                    <asp:ListItem Value="3">7</asp:ListItem>
                                                                    <asp:ListItem Value="4">8</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-right">
                                                            <asp:Button ID="btnRestore" runat="server" Text="Restore Defaults" Font-Size="12px" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="department">
                            <div class="department-header">
                                Department Mapping
                            </div>
                            <div class="department-detail">
                                <div class="row">
                                    <div class="tran-setting-detail">
                                        <div class="tender-mapping-detail">
                                            <div class="right-container-top-detail">
                                                <div class="GridDetails">
                                                    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                        <div class="grdLoad2">
                                                            <table id="tblItemhistory2" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="col">Department
                                                                        </th>
                                                                        <th scope="col">
                                                                            <asp:CheckBox ID="checkAll1" runat="server" onclick="checkAllDept(this);" />Allow         
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 185px; width: 435px; background-color: aliceblue;">
                                                                <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30" style="overflow: auto; height: 332px">--%>
                                                                <asp:GridView ID="grdDepartment" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                                    oncopy="return false" PageSize="5">
                                                                    <EmptyDataTemplate>
                                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                                    </EmptyDataTemplate>
                                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                                    <PagerStyle CssClass="pshro_text" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Department" HeaderText="Department"></asp:BoundField>
                                                                        <asp:TemplateField ItemStyle-Width="150">
                                                                            <%--<HeaderTemplate>
                                                                                            <asp:CheckBox ID="checkAll" runat="server" onclick="checkAllDept(this);" />
                                                                                            Allow
                                                                                        </HeaderTemplate>--%>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkDeptAllow" runat="server" Checked='<%# Eval("Allow") %>' onclick="Check_Click(this)" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-red" PostBackUrl="~/Masters/frmTerminalView.aspx"
                            Text='<%$ Resources:LabelCaption,btn_View %>'></asp:LinkButton>
                    </div>
                    <div class="control-button" id ="divSave" runat="server">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                            OnClientClick="return Validate()" Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClick="lnkClear_Click"
                            Text='Clear(F6)'></asp:LinkButton>
                    </div>
                </div>
            </div>
              <div id="divsave" >
                            <asp:Button ID="btnsave" runat="server" style="display:none"  OnClick="btnsave_Click" />
                        </div>
        </ContentTemplate>
    </asp:UpdatePanel> 
            <asp:HiddenField ID="hidSavebtn" runat="server" /> 
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
</asp:Content>
