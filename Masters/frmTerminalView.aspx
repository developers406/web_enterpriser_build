﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmTerminalView.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmTerminalView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 130px;
            max-width: 170px;
            text-align:center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 130px;
            max-width: 170px;
            text-align:center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 230px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 190px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 245px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 240px;
            max-width: 165px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 167px;
            max-width: 167px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Deactive this Terminal?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'TerminalView');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "TerminalView";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Terminal</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="gridDetails">
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="grid-search" style="height: 30px">
                        <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px" PostBackUrl="~/Masters/frmTerminalConfiguration.aspx">New</asp:LinkButton>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">

                            <div class="grdLoad">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Edit
                                            </th>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">Location
                                            </th>
                                            <th scope="col">TerminalCode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">System Name
                                            </th>
                                            <th scope="col">Active
                                            </th>
                                        </tr>
                                    </thead>
                                </table>


                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 250px; width: 1350px; background-color: aliceblue;">
                                    <asp:GridView ID="grdTerminal" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdTerminal_RowDataBound" ShowHeaderWhenEmpty="true"
                                         oncopy="return false" AllowPaging="True" PageSize="18" ShowHeader="false"
                                        OnPageIndexChanging="grdTerminal_PageIndexChanging" DataKeyNames="TerminalCode, Location" CssClass="pshro_GridDgn grdLoad">
                                       <%-- <RowStyle Height="20px" />
                                        <AlternatingRowStyle Height="20px" />--%>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Edit">           <%--HeaderStyle-Width="100px"--%>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png" ImageAlign="Middle"
                                                        CommandName="Select" ToolTip="Click here to edit"  OnClick="imgbtnEdit_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete" >
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/delete.png" ImageAlign="Middle"
                                                        ToolTip="Click here to edit" OnClick="imgbtnDelete_Click" OnClientClick="Confirm()" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                                            <asp:BoundField DataField="TerminalCode" HeaderText="Terminal Code"></asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Description" />
                                            <asp:BoundField DataField="SystemName" HeaderText="System Name" />
                                            <asp:BoundField DataField="Status" HeaderText="Active" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>  
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidSavebtn" runat="server" /> 
            <asp:HiddenField ID="hidEditbtn" runat="server" /> 
    </div>
</asp:Content>
