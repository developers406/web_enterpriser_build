﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLorrymanagement.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmLorrymanagement" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 100px;
            max-width: 100px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
             min-width: 150px;
            max-width: 150px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
             min-width: 106px;
            max-width: 106px;
            text-align: left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
             min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
            text-align: left;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 130px;
            max-width: 130px;
            text-align: left;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
   
             min-width: 100px;
            max-width: 100px;
            text-align: left;
        }
         .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
   
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }
         .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
   
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }
          .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
   
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }
        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            display: none;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            display: none;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            display: none;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            display: none;
        }

        .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            display: none;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            setAutocomplete();
        }
        function ValidateForm() {
            var Show = '';
            if ($('#<%=hidTransfercode.ClientID%>').val() != "" && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Transport");
                return false;
            }
            if ($('#<%=hidTransfercode.ClientID%>').val() == "" && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Transport");
                return false;
            }
            if (document.getElementById("<%=txtTransportName.ClientID%>").value == "") {
                Show = Show + 'Please Enter Transport Name';
                document.getElementById("<%=txtTransportName.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtCity.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter City';
                document.getElementById("<%=txtCity.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtMobileNo.ClientID%>").value == "" || document.getElementById("<%=txtMobileNo.ClientID%>").value.length !=10 ) {
                Show = Show + '<br />Please Enter Valid 10 Digit Number';
                document.getElementById("<%=txtMobileNo.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtFromLocation.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter From Location';
                document.getElementById("<%=txtFromLocation.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtToLocation.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter To Location';
                document.getElementById("<%=txtToLocation.ClientID %>").focus();
            }
           <%-- if (document.getElementById("<%=ddlBankCode.ClientID%>").value == "") {
                
                Show = Show + '<br />Please Enter BankCode';
                 document.getElementById("<%=ddlBankCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtIFSCcode.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter IFSCcode';
                 document.getElementById("<%=txtIFSCcode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtBranchName.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter BranchName';
                 document.getElementById("<%=txtBranchName.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtAccountno.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Accountno';
                document.getElementById("<%=txtAccountno.ClientID %>").focus();
            }--%>
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                //__doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        if (ValidateForm() == true)
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").prop('checked', false);
            $('#<%=txtTransportName.ClientID %>').focus();
             $('#<%=chActive.ClientID%>').attr('checked', 'checked');
            return false;
        }

        function setAutocomplete() {
            try {
                $("[id$=txtTransportName]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmLorrymanagement.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) { 
                                    return {
                                        label: item.split('|')[1],
                                        val: item.split('|')[1],
                                        val1: item.split('|')[2],
                                        val2: item.split('|')[3],
                                        val3: item.split('|')[4],
                                        val4: item.split('|')[5],
                                        val5: item.split('|')[6],
                                        val6: item.split('|')[7],
                                        val7: item.split('|')[8],
                                        val8: item.split('|')[9],
                                        val9: item.split('|')[10],
                                        val9: item.split('|')[11]
                                     
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=hidTransfercode.ClientID %>').val($.trim(i.item.label));
                        $('#<%=txtTransportName.ClientID %>').val($.trim(i.item.val));
                        $('#<%=txtAdress.ClientID %>').val($.trim(i.item.val1));
                        $('#<%=txtCity.ClientID %>').val($.trim(i.item.val2));
                        $('#<%=txtMobileNo.ClientID %>').val($.trim(i.item.val3));
                        $('#<%=txtLandlineNo.ClientID %>').val($.trim(i.item.val4));
                        $('#<%=txtFromLocation.ClientID %>').val($.trim(i.item.val5));
                        $('#<%=txtToLocation.ClientID %>').val($.trim(i.item.val6));
                        $('#<%=ddlBankCode.ClientID %>').val($.trim(i.item.val7)); 
                        $('#<%=txtIFSCcode.ClientID %>').val($.trim(i.item.val8));
                        $('#<%=txtBranchName.ClientID %>').val($.trim(i.item.val9));
                        $('#<%=txtAccountno.ClientID %>').val($.trim(i.item.val10));
                        if ($.trim(i.item.val11) == "1")
                            $('#<%=chActive.ClientID%>').attr('checked', 'checked');
                        else
                            $('#<%=chActive.ClientID%>').removeAttr('checked');
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=hidTransfercode.ClientID %>').val($.trim(i.item.label));
                        $('#<%=txtTransportName.ClientID %>').val($.trim(i.item.val));
                        $('#<%=txtAdress.ClientID %>').val($.trim(i.item.val1));
                        $('#<%=txtCity.ClientID %>').val($.trim(i.item.val2));
                        $('#<%=txtMobileNo.ClientID %>').val($.trim(i.item.val3));
                        $('#<%=txtLandlineNo.ClientID %>').val($.trim(i.item.val4));
                        $('#<%=txtFromLocation.ClientID %>').val($.trim(i.item.val5));
                        $('#<%=txtToLocation.ClientID %>').val($.trim(i.item.val6));
                        $('#<%=ddlBankCode.ClientID %>').val($.trim(i.item.val7)); 
                        $('#<%=txtIFSCcode.ClientID %>').val($.trim(i.item.val8));
                        $('#<%=txtBranchName.ClientID %>').val($.trim(i.item.val9));
                        $('#<%=txtAccountno.ClientID %>').val($.trim(i.item.val10));
                        if ($.trim(i.item.val11) == "1")
                            $('#<%=chActive.ClientID%>').attr('checked', 'checked');
                        else
                            $('#<%=chActive.ClientID%>').removeAttr('checked');
                        return false;
                    },
                    minLength: 1
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Department");
                return false;
            }
            clearForm();
            DisplayDetails($(source).closest("tr"));
        }

        function DisplayDetails(row) {
            $('#<%=hidTransfercode.ClientID %>').val(($("td", row).eq(13).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtTransportName.ClientID %>').val(($("td", row).eq(2).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtAdress.ClientID %>').val(($("td", row).eq(10).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtCity.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtLandlineNo.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtMobileNo.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtFromLocation.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtToLocation.ClientID %>').val(($("td", row).eq(7).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlBankCode.ClientID %>').val(($("td", row).eq(8).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlBankCode.ClientID %>').trigger("liszt:updated");
            $('#<%=txtIFSCcode.ClientID %>').val(($("td", row).eq(9).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&')); 
            $('#<%=txtBranchName.ClientID %>').val(($("td", row).eq(10).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtAccountno.ClientID %>').val(($("td", row).eq(11).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            if (($("td", row).eq(14).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&') == "1")
                $('#<%=chActive.ClientID%>').attr('checked', 'checked');
            else
                $('#<%=chActive.ClientID%>').removeAttr('checked');
        }

        function dept_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Department");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(13).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        return false;
                    }
                }
            });
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'LorryManagement');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "LorryManagement";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transport Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="updtPnlgrdgrdDepartmentList" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Transport Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtTransportName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Address"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAdress" TextMode="MultiLine" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="City"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCity" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Mobile No"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMobileNo" MaxLength="12" TextMode="Number" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Landline No"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtLandlineNo" MaxLength="12" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="From Location"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromLocation" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="To Location"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToLocation" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="BankCode"></asp:Label> 
                                  </div>
                                <div class="label-right">
                                  
                              <asp:DropDownList ID="ddlBankCode" runat="server" CssClass="form-control-res" Width="100%">
                              </asp:DropDownList>
                                    
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="IFSC code"></asp:Label> 
                                  </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtIFSCcode" runat="server" CssClass="form-control-res"></asp:TextBox>
                              
                            </div>
                        </div>
                         <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="BranchName"></asp:Label> 
                                  </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBranchName" runat="server" CssClass="form-control-res"></asp:TextBox>
                              
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text="Accountno"></asp:Label> 
                                  </div>
                                <div class="label-right">
                                  
                          
                           <asp:TextBox ID="txtAccountno" runat="server" CssClass="form-control-res"></asp:TextBox>
                              
                                    
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Active"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chActive" runat="server" Checked="true"></asp:CheckBox>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm();return false;"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" Style="display: none" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updtPnltopgrdDepartmentgrd" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Transport Name to Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" OnClick="lnkSearchGrid_Click" class="button-blue" Width="150px" Style="visibility: hidden"><i class="icon-play"></i>Search</asp:LinkButton>
                        </asp:Panel>
                    </div>
                    <div class="row">
                        <div class="grdLoad">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                        <th scope="col">Delete
                                        </th>
                                        <th scope="col">Edit
                                        </th>
                                        <th scope="col">Transport Name
                                        </th>
                                        <th scope="col">City
                                        </th>
                                        <th scope="col">Mobile No
                                        </th>
                                        <th scope="col">Landline No
                                        </th>
                                        <th scope="col">From Location
                                        </th>
                                        <th scope="col">To Location
                                        </th>
                                        <th scope="col">BankCode
                                        </th>
                                        <th scope="col">IFSCcode
                                        </th>
                                        <th scope="col">BranchName
                                        </th>
                                         <th scope="col">Accountno
                                        </th>
                                        <th scope="col">Created User
                                        </th>
                                        <th scope="col">TransportCode
                                        </th>
                                        <th scope="col">Address
                                        </th>
                                        <th scope="col">Active
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1335px; background-color: aliceblue;">
                                <asp:GridView ID="grdTransport" runat="server" AllowSorting="true"
                                    AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" oncopy="return false"
                                    AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                    DataKeyNames="TranCode">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label5" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                    ToolTip="Click here to Delete" OnClientClick="return dept_Delete(this);  return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                    ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="TranName" HeaderText="TranName"></asp:BoundField>
                                        <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="MobileNumber" HeaderText="MobileNumber"></asp:BoundField>
                                        <asp:BoundField DataField="LandlineNo" HeaderText="LandlineNo"></asp:BoundField>
                                        <asp:BoundField DataField="FromLocation" HeaderText="FromLocation"></asp:BoundField>
                                        <asp:BoundField DataField="ToLocation" HeaderText="ToLocation"></asp:BoundField>
                                         <asp:BoundField DataField="BankCode" HeaderText="BankCode"></asp:BoundField>
                                               <asp:BoundField DataField="IFSCcode" HeaderText="IFSCcode"></asp:BoundField>
                                        <asp:BoundField DataField="BranchName" HeaderText="BranchName"></asp:BoundField>
                                         <asp:BoundField DataField="Accountno" HeaderText="Accountno"></asp:BoundField>
                                        <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" ItemStyle-CssClass="hiddencol"
                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                        <asp:BoundField DataField="TranCode" HeaderText="TranCode" ItemStyle-CssClass="hiddencol"
                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                        <asp:BoundField DataField="Address" HeaderText="Address" ItemStyle-CssClass="hiddencol"
                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                        <asp:BoundField DataField="Active" HeaderText="Active" ItemStyle-CssClass="hiddencol"
                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ups:PaginationUserControl runat="server" ID="TransportMaster" OnPaginationButtonClick="lnkTransporMaster_PageClick" />
        <div class="hiddencol">
            <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidTransfercode" Value="" runat="server" />
        </div>
    </div>
</asp:Content>
