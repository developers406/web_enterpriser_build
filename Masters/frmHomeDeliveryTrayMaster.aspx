﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmHomeDeliveryTrayMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmHomeDeliveryTrayMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }
          .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 150px;
            max-width: 150px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 350px;
            max-width: 350px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 612px;
            max-width: 612px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
          .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <script type="text/javascript">

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
             SetAutoComplete();
             $('input[type=text]').bind('change', function () {
                 if (this.value.match(/[^a-zA-Z0-9]/g)) {

                     ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                     this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
                 }
             });
         }
         function disableFunctionKeys(e) {
             var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
             if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                 if (e.keyCode == 115) {
                     if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                            e.preventDefault();
                        }
                        else if (e.keyCode == 117) {
                            $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };
        function HomeTray_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this HomeDeliery");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });
        }
        function ValidateForm() {
            try {
                if ($('#<%=txtBagCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                    ShowPopupMessageBox("You have no permission to save existing HomeDeliery");
                    return false;
                }
                if (!($('#<%=txtBagCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                    ShowPopupMessageBox("You have no permission to save New HomeDeliery");
                    return false;
                }
                var Show = '';
                var BagCode = $.trim($('#<%=txtBagCode.ClientID%>').val());

                if (BagCode == "") {
                    Show = Show + 'Please Enter Bag Code';
                    document.getElementById("<%=txtBagCode.ClientID %>").focus();
                //return false;
            }



            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    function imgbtnEdit_ClientClick(source) {
        //alert('test');
        if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this HomeDeliery");
                return false;
            }
            clearForm();
            DisplayDetails($(source).closest("tr"));
        }
        function DisplayDetails(row) {
            // debugger;
            //clearForm();
            <%--$('#<%=hdfdstatus.ClientID %>').val("EDIT");--%>
            $('#<%=txtBagCode.ClientID %>').prop("disabled", true);
            $('#<%=txtBagCode.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtDescription.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
        }

        function SetAutoComplete() {
            try {

                $('#<%=txtSearch.ClientID %>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmHomeDeliveryTrayMaster.aspx/fncGetSearch",
                            data: "{ 'Code': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                //console.log(jQuery.parseJSON(data.d)[0]["DisplayCode"]);
                                //debugger;
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item,
                                        // val: item.split('-')[1]
                                    }
                                }))


                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val(i.item.label.split('-')[0]);
                        <%--  //$('#<%=txtDisplayName.ClientID %>').val(i.item.val);--%>


                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $('#<%=txtSearch.ClientID %>').val(i.item.label.split('-')[0]);
                        <%--$('#<%=txtDisplayName.ClientID %>').val(i.item.val);--%>

                        //fncGetAccountDetails(i.item.label.split('-')[0]);
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');
                        return false;

                    },
                    minLength: 1
                });

                $('#<%=txtBagCode.ClientID %>').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmHomeDeliveryTrayMaster.aspx/fncGetSearch",
                            data: "{ 'Code': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                //console.log(jQuery.parseJSON(data.d)[0]["DisplayCode"]);
                                //debugger;
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item,
                                        // val: item.split('-')[1]
                                    }
                                }))


                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtBagCode.ClientID %>').val(i.item.label.split('-')[0]);
                        $('#<%=txtDescription.ClientID %>').val(i.item.label.split('-')[1]);
                        $('#<%=txtBagCode.ClientID %>').prop("disabled", true);
                        <%--  //$('#<%=txtDisplayName.ClientID %>').val(i.item.val);--%>


                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtBagCode.ClientID %>').val(i.item.label.split('-')[0]);
                        $('#<%=txtDescription.ClientID %>').val(i.item.label.split('-')[1]);
                        $('#<%=txtBagCode.ClientID %>').prop("disabled", true);
                        return false;

                    },
                    minLength: 1
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                            e.preventDefault();
                        }
                        else if (e.keyCode == 117) {
                            $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });


        function clearForm() {
            $('#<%=txtBagCode.ClientID %>').prop("disabled", false);
            $('#<%=txtBagCode.ClientID %>').val("");
            $('#<%=txtDescription.ClientID %>').val("");
            $('#<%=txtBagCode.ClientID %>').focus();

            return false;
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'HomeDeliveryTrayMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "HomeDeliveryTrayMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a class="active-page">Home Delivery Tray</a></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />

        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Bag Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBagCode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="button-contol" style="margin-left: 175px">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                    OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails" style="margin-top: 10px">
                    <div class="grid-search" style="margin-left: 10px">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Description To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                              <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    <div class="right-container-top-detail">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Edit
                                                </th>
                                                <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Tray Code
                                                </th>
                                                <th scope="col">Description
                                                </th>

                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 221px; width: 1330px; background-color: aliceblue;">
                                        <asp:GridView ID="gvHomTray" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn grdLoad" oncopy="return false">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return HomeTray_Delete(this); return false;"
                                                            ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                            ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNumber" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="BagCode" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ups:PaginationUserControl runat="server" ID="Hometray" OnPaginationButtonClick="Hometray_PaginationButtonClick" />
                <%--<ups:PaginationUserControl runat="server" ID="SubCategoryPaging" OnPaginationButtonClick="SubCategory_PaginationButtonClick" />--%>


                <div class="hiddencol">
                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
