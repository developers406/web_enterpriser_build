﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" CodeBehind="frmTaxMasterGST.aspx.cs"
    Inherits="EnterpriserWebFinal.Masters.frmTaxMasterGST" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .button-contol_test {
            clear: both;
            display: table;
            float: right;
            margin-right: 10px;
            padding: 0;
        }

        .container-group_tes {
            width: 520px;
            border: 1px solid #d8d9d8;
            background-color: #f3f3f3;
            padding: 0px;
            margin-bottom: 5px;
        }

        .form-control-res_test {
            font-size: 14px;
            display: block;
            width: 100%;
            height: 24px;
            font-family: caption;
            padding: 0 2px 0px 2px;
            line-height: 1.42857143;
            color: #000000;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #d8d9d8;
            border-radius: 5px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -webkit-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            -moz-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            -o-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
        }

        .groove {
            border-size: 2px;
            border-style: solid;
            border-color: black;
        }

        .form-control-res_test:focus {
            border-color: #80deea;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(128, 222, 234, 0.6);
            -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(128, 222, 234, 0.6);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(128, 222, 234, 0.6);
        }

        .form-control-res_test::-moz-placeholder {
            color: #999999;
            opacity: 1;
        }

        .form-control-res_test:-ms-input-placeholder {
            color: #999999;
        }

        .form-control-res_test::-webkit-input-placeholder {
            color: #999999;
        }

        .form-control-res_test[disabled], .form-control-res_test[readonly], fieldset[disabled] {
            background-color: lightyellow;
            opacity: 1;
        }

        .form-control-res_test[disabled], fieldset[disabled] {
            cursor: not-allowed;
        }

        .container-control_TEST {
            display: table;
            padding: 5px 10px;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 75px;
            max-width: 75px; text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 75px;
            max-width: 75px; text-align: center !important;
        }
          .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
              text-align: center !important;
        }
        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 205px;
            max-width: 205px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 245px;
            max-width: 245px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            display: none;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            display: none;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 169px;
            max-width: 169px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 110px;
            max-width: 110px;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
           .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            var date = new Date();
            var currentMonth = date.getMonth();
            var currentDate = date.getDate();
            var currentYear = date.getFullYear();
            $("#<%= txtActDate.ClientID %>").datepicker({
                minDate: new Date(currentYear, currentMonth, currentDate)
            });
            $("#<%= txtEndDate.ClientID %>").datepicker({
                minDate: new Date(currentYear, currentMonth, currentDate)
            });

            $("#<%= txtActDate.ClientID %>").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#<%= txtEndDate.ClientID %>").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#<%= txtActDate.ClientID %>").datepicker().datepicker("setDate", new Date());
            $("#<%= txtEndDate.ClientID %>").datepicker().datepicker("setDate", "365");

        });
        $('#<%=txtdirectigstper.ClientID %>').hide();
        function DisableCopyPaste(e) {
            var kCode = event.keyCode || e.charCode;
            if (kCode == 17 || kCode == 2) {

                return false;
            }
        }

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $(document).ready(function () {
                $("input").first().focus();

                $('input').bind("keydown", function (e) {
                    var n = $("input").length;
                    if (e.which == 13) {
                        if ($('#<%=txtnormaligstper.ClientID %>').is(":focus")) {
                            e.preventDefault();
                            $('#<%=TxtSgstSymbol.ClientID %>').focus();
                        }
                        else {
                            e.preventDefault(); //to skip default behavior of the enter key
                            var nextIndex = $('input').index(this) + 1;
                            if (nextIndex < n)
                                $('input')[nextIndex].focus();
                            else {
                                $('input')[nextIndex - 1].blur();

                            }
                        }//Enter key

                    }
                });


            });


            $('#<%= chkSgstSalesAllowed.ClientID %>').attr("checked", "checked");
            $('#<%= chkCgstSalesAllowed.ClientID %>').attr("checked", "checked");

            $('#<%=txtTaxSgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxCgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxIgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtSgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtCgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtIgstDescription.ClientID %>').attr("disabled", "disabled");

            $('#<%=txtdirectigstper.ClientID %>').hide();
            $('#<%=lbldirectigstper.ClientID %>').hide();

            $('#<%=txtnormaligstper.ClientID %>').focusout(function () {
                var taxperc = $('#<%=txtnormaligstper.ClientID %>').val();
                var re1 = parseFloat($('#<%=txtnormaligstper.ClientID %>').val());
                var re = re1 / 2;

                if (re1 <= 49.00) {

                    var num = re.toFixed(2);

                    var str = num.toString();

                    var numarray = str.split('.');
                    var a = new Array();
                    a = numarray;
                    var Taxvalue1 = a[1];
                    Taxvalue = FormatMe(a[0]);


                    $('#<%=txtTaxSgstCode.ClientID %>').val('S' + Taxvalue + '' + Taxvalue1);
                    $('#<%=txtSgstDescription.ClientID %>').val('SGST ' + taxperc / 2 + '' + ' %');

                    $('#<%=txtTaxCgstCode.ClientID %>').val('C' + Taxvalue + '' + Taxvalue1);
                    $('#<%=txtCgstDescription.ClientID %>').val('CGST ' + taxperc / 2 + '' + ' %');

                    var igst = Taxvalue + '' + Taxvalue1;
                    Taxvalue = igst * 2;

                    if (Taxvalue.toString().length > 3) {
                        $('#<%=txtTaxIgstCode.ClientID %>').val('I' + Taxvalue);
                        $('#<%=txtIgstDescription.ClientID %>').val('IGST ' + taxperc + ' %');
                    }
                    else if (Taxvalue.toString().length == 1) {
                        $('#<%=txtTaxIgstCode.ClientID %>').val('I000' + Taxvalue);
                        $('#<%=txtIgstDescription.ClientID %>').val('IGST ' + taxperc + ' %');
                    }
                    else {
                        $('#<%=txtTaxIgstCode.ClientID %>').val('I0' + Taxvalue);
                        $('#<%=txtIgstDescription.ClientID %>').val('IGST ' + taxperc + ' %');
                    }
            }
            else {

                ShowPopupMessageBox('Invaild tax code');

            }



            });






        function FormatMe(n) {
            return n > 9 ? "" + n : "0" + n;
        }



    }

        function ValidateForm() {

              if ($('#<%=txtnormaligstper.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Tax");
                return false;
            }
            if (!($('#<%=txtnormaligstper.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Tax");
                return false;
            }

        if (document.getElementById("<%=txtnormaligstper.ClientID%>").value == "") {

            ShowPopupMessageBox('Please Enter Tax Code');
            document.getElementById("<%=txtnormaligstper.ClientID %>").focus();
            return false;
        }
        else {
            $("input").removeAttr('disabled');
            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
            return true;
        }
    }



    function Confirm_Tax_Delete(source) {
        if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Tax");
                return false;
            }
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                    $("#<%=btnDelete.ClientID %>").click();
                },
                "NO": function () {
                    $(this).dialog("close");
                    returnfalse();
                }
            }
        });
        document.forms[0].appendChild(confirm_value);
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        //ShowPopupMessageBox(charCode);
        if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 37 || charCode > 40) && charCode != 13)
            return false;
        return true;
    }

    function imgbtnEdit_ClientClick(source) {
        if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Tax");
                return false;
            }
            DisplayDetails($(source).closest("tr"));


        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                     e.preventDefault();
                 }
<%--else if (e.keyCode == 119) {
    $('#<%= lnkClose.ClientID %>').click();
                                 e.preventDefault();
                             }--%>


            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function DisplayDetails(row) {
            clearForm();
            if ($("td", row).eq(3).html().replace(/&nbsp;/g, '').indexOf('S') != -1) {
                $('#<%=txtnormaligstper.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtTaxCgstCode.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtCgstDescription.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtTaxIgstCode.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtIgstDescription.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtTaxSgstCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
                $('#<%=txtTaxSgstCode.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtnormaligstper.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, '') * 2);
                $('#<%=txtSgstDescription.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
                $('#<%=txtSgstDescription.ClientID %>').attr("disabled", "disabled");
                $('#<%=TxtSgstSymbol.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
                var ValidFrom = $("td", row).eq(7).html().substring(0, 10).replace(/&nbsp;/g, '');
                //            ShowPopupMessageBox(ValidFrom);
                $('#<%=txtActDate.ClientID %>').datepicker("setDate", ValidFrom).datepicker({ dateFormat: "dd/mm/yy" });
                     var dateAr = $("td", row).eq(8).html().substring(0, 10).replace(/&nbsp;/g, '');
                     $('#<%=txtEndDate.ClientID %>').datepicker("setDate", dateAr).datepicker({ dateFormat: "dd/mm/yy" });
                if ($("td", row).eq(9).html().replace(/&nbsp;/g, '') == "1") {
                    $('#<%= chkSgstSalesAllowed.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= chkSgstSalesAllowed.ClientID %>').removeAttr("checked");
            }
        }
        else if ($("td", row).eq(3).html().replace(/&nbsp;/g, '').indexOf('C') != -1) {
            $('#<%=txtnormaligstper.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxCgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxSgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtSgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxIgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtIgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxCgstCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtnormaligstper.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, '') * 2);
            $('#<%=txtCgstDescription.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtCgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=TxtCgstSymbol.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            var ValidFrom = $("td", row).eq(7).html().substring(0, 10).replace(/&nbsp;/g, '');
            $('#<%=txtActDate.ClientID %>').datepicker("setDate", ValidFrom).datepicker({ dateFormat: "dd/mm/yy" });
            var dateAr = $("td", row).eq(8).html().substring(0, 10).replace(/&nbsp;/g, '');
            $('#<%=txtEndDate.ClientID %>').datepicker("setDate", dateAr).datepicker({ dateFormat: "dd/mm/yy" });
            if ($("td", row).eq(9).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkCgstSalesAllowed.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= chkCgstSalesAllowed.ClientID %>').removeAttr("checked");
            }
        }
        else if ($("td", row).eq(3).html().replace(/&nbsp;/g, '').indexOf('N') != -1) {
            ShowPopupMessageBox("Editing is not allowed in no Tax")
        }
        else {
            //                $("#<%=txtdirectigstper.ClientID %>").show();
            //                $("#<%=lbldirectigstper.ClientID %>").show();
            $('#<%=txtnormaligstper.ClientID %>').attr("disabled", "disabled");
            //                $('#<%=txtdirectigstper.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxIgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxSgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtSgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtTaxCgstCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtCgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtnormaligstper.ClientID %>').val(parseInt($("td", row).eq(5).html().replace(/&nbsp;/g, '')));
            $('#<%=txtTaxIgstCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            //                $('#<%=txtdirectigstper.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtIgstDescription.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txtIgstDescription.ClientID %>').attr("disabled", "disabled");
            $('#<%=TxtIgstSymbol.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            var ValidFrom = $("td", row).eq(7).html().substring(0, 10).replace(/&nbsp;/g, '');
            $('#<%=txtActDate.ClientID %>').datepicker("setDate", ValidFrom).datepicker({ dateFormat: "dd/mm/yy" });
            var dateAr = $("td", row).eq(8).html().substring(0, 10).replace(/&nbsp;/g, '');
            $('#<%=txtEndDate.ClientID %>').datepicker("setDate", dateAr).datepicker({ dateFormat: "dd/mm/yy" });
            if ($("td", row).eq(9).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkIgstSalesAllowed.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%= chkIgstSalesAllowed.ClientID %>').removeAttr("checked");
            }
        }

}
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            $("#<%= txtActDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtEndDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "365");
            $('#<%=txtnormaligstper.ClientID %>').val("");
            //            $('#<%=txtdirectigstper.ClientID %>').val("");
            //            $("#<%=txtdirectigstper.ClientID %>").hide();
            //            $("#<%=lbldirectigstper.ClientID %>").hide();

            return false;
        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TaxMasterGST');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TaxMasterGST";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlTax" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
            <div class="main-container" style="overflow: hidden">

                <div id="breadcrumbs" class="breadcrumbs" runat="Server">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Tax Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                        </div>
                        <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                        <div class="container-group_tes">
                            <div class="container-control_TEST">
                                <div class="col-md-2">
                                    <asp:Label ID="Label4" runat="server" Text="Tax %"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtnormaligstper" MaxLength="6" CssClass="form-control-res_test"
                                        runat="server" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-4">
                                    <%--<asp:CheckBox ID="chkIgstPercentagecalculation" Style="margin-left: 20" runat="server"
                                        Text="IGST = CGST + SGST" />--%>
                                </div>
                                <div class="col-md-1">
                                    <asp:Label ID="lbldirectigstper" runat="server" Text="IGST%"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:TextBox ID="txtdirectigstper" MaxLength="5" CssClass="form-control-res_test"
                                        runat="server" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                                <div class="col-md-12">
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label10" runat="server" Style="text-align: center" Font-Bold="True"
                                                Text="SGST"></asp:Label>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label11" runat="server" Style="text-align: center" Font-Bold="True"
                                                Text="CGST"></asp:Label>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:Label ID="Label12" runat="server" Style="text-align: center" Font-Bold="True"
                                                Text="IGST"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label1" runat="server" Text="Tax Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtTaxSgstCode" CssClass="form-control-res_test"
                                                runat="server" MaxLength="6"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtTaxCgstCode" CssClass="form-control-res_test"
                                                runat="server" MaxLength="6"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtTaxIgstCode" CssClass="form-control-res_test"
                                                runat="server" MaxLength="6"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtSgstDescription" MaxLength="50" CssClass="form-control-res_test" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtCgstDescription" MaxLength="50" CssClass="form-control-res_test" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtIgstDescription" MaxLength="50" CssClass="form-control-res_test" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label5" runat="server" Text="Symbol"></asp:Label>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="TxtSgstSymbol" CssClass="form-control-res_test" MaxLength="3" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="TxtCgstSymbol" CssClass="form-control-res_test" MaxLength="3" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="TxtIgstSymbol" CssClass="form-control-res_test" MaxLength="3" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label6" runat="server" Text="Sales Allowed"></asp:Label>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:CheckBox ID="chkSgstSalesAllowed" runat="server" />
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:CheckBox ID="chkCgstSalesAllowed" runat="server" />
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:CheckBox ID="chkIgstSalesAllowed" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label7" runat="server" Text="Act Date"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtActDate" CssClass="form-control-res_test" onkeypress="return false"
                                                nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false"
                                                oncut="return false" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-single-res">
                                        <div class="col-md-2">
                                            <asp:Label ID="Label8" runat="server" Text="End Date"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-2">
                                            <asp:TextBox ID="txtEndDate" CssClass="form-control-res_test" onkeypress="return false"
                                                nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false"
                                                oncut="return false" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="button-contol">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return ValidateForm()" OnClick="lnkSave_Click">Save(F4)</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <hr />
                <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="GridDetails">
                            <div class="grid-search">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px"
                                        OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>
                                  <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                                </asp:Panel>
                            </div>


                            <div class="GridDetails">
                                <div class="row">
                                    <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                     <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">Tax Code
                                                    </th>
                                                    <th scope="col">Description
                                                    </th>
                                                    <th scope="col">Percentage 
                                                    </th>
                                                    <th scope="col">Symbol
                                                    </th>
                                                    <th scope="col">Active Date
                                                    </th>
                                                    <th scope="col">End Date
                                                    </th>
                                                    <th scope="col">SalesAllowed
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 101px; width: 1342px; background-color: aliceblue;">
                                            <asp:GridView ID="gvTaxGST" runat="server" AllowSorting="true" AutoGenerateColumns="False" ShowHeader="false"
                                                ShowHeaderWhenEmpty="true" oncopy="return false" DataKeyNames="TaxCode" CssClass="pshro_GridDgn grdLoad">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png" CommandName="Select"
                                                                ToolTip="Click here to Delete" OnClientClick=" return Confirm_Tax_Delete(this); return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                ToolTip="Click here to edit" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                     <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                    <asp:BoundField DataField="TaxCode" HeaderText="Tax Code"></asp:BoundField>
                                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                    <asp:BoundField DataField="Percentage" HeaderText="Percentage" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="Symbol" HeaderText="Symbol"></asp:BoundField>
                                                    <asp:BoundField DataField="ActiveDate" HeaderText="Active Date" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="EndDate" HeaderText="End Date" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                    <asp:BoundField DataField="SalesAllowed" HeaderText="SalesAllowed" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ups:PaginationUserControl runat="server" ID="TaxPaging" OnPaginationButtonClick="TaxPagingPaging_PaginationButtonClick" />
                        </div>
                        </div>
                        <div class="hiddencol">

                            <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
