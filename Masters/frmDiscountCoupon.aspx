﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmDiscountCoupon.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmDiscountCoupon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 75px;
            max-width: 75px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 160px;
            max-width: 160px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 160px;
            max-width: 160px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 180px;
            max-width: 180px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 180px;
            max-width: 180px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 175px;
            max-width: 175px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 195px;
            max-width: 195px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 195px;
            max-width: 195px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
        
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript" language="Javascript">
        function pageLoad() {
              if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkUpdate.ClientID %>').css("display", "none");
              }
             if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                $('#<%=Delete.ClientID %>').css("display", "block");
             }
             else {
                  $('#<%=Delete.ClientID %>').css("display", "none");
             }
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtCouponFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtCouponToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

        }

        //        $(document).ready(function () {
        //           
        //        });

        function fncValidateActiveCheckAll() {

            try {
                var gridtr = $("#<%= gvDiscount.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    if ($("#<%=CheckActiveAll.ClientID %>").text() == "Active All") {

                        $("#<%=CheckActiveAll.ClientID %>").text("De-Active All");
                        $("#<%=gvDiscount.ClientID %> [id*=chkIsACTIF]").prop('checked', true);


                    }
                    else {

                        $("#<%=CheckActiveAll.ClientID %>").text("Active All");
                        $("#<%=gvDiscount.ClientID %> [id*=chkIsACTIF]").prop('checked', false);


                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncValidateCheckAll() {

            try {
                var gridtr = $("#<%= gvDiscount.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvDiscount.ClientID %> [id*=chkSingle]").prop('checked', true);


                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvDiscount.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvDiscount.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    //ShowPopupMessageBox($("#<%=gvDiscount.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvDiscount.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage1();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete1() {
            try {
                // ShowPopupMessageBox("sdfg");
                $("#DisplayDelete").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //ShowPopupMessageBox("message");
                InitializeDialogDelete1();
                $("#DisplayDelete").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        function fncCloseDeleteDialog() {
            try {
                $("#DisplayDelete").dialog('close');


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowConfirmDeleteMessage1() {
            try {
                InitializeDialogDeletes();
                $("#DeleteConfirm").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDeletes() {
            try {
                $("#DeleteConfirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirm").dialog('close');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirm").dialog('close'); return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBillValFrmFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=txtBillValTo.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBillValToFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=txtCouponVal.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCouponFrmDateFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=txtCouponFromDate.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCouponFrmToFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=txtCouponToDate.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAddFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=lnkAdd.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function EnableOrDisableDropdown(element, isEnable) {
            //ShowPopupMessageBox(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function fncShowShowPopupMessageBoxNoItemsMessage() {
            try {
                InitializeDialogShowPopupMessageBoxNoItems();
                $("#ShowPopupMessageBoxNoItems").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseShowPopupMessageBoxNoItemsDialog() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogShowPopupMessageBoxNoItems() {
            try {
                $("#ShowPopupMessageBoxNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvDiscount.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //ShowPopupMessageBoxNoItems
                    fncShowShowPopupMessageBoxNoItemsMessage();
                }
                else {
                    //ShowPopupMessageBox($("#<%=gvDiscount.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvDiscount.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }


        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");


        }
        function fncClear() {

            $("#<%=txtBillValFrm.ClientID %>").val('');
            $("#<%=txtBillValTo.ClientID %>").val('');
            $("#<%=txtCouponVal.ClientID %>").val('');

        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'DiscountCoupon');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "DiscountCoupon";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Voucher</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_Discount_Coupon%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <asp:UpdatePanel ID="update" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 25%">
                            <div class="label-left" style="width: 40%">
                                <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left" style="width: 25%">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float: right; display: table">
                        </div>
                        <div class="control-group-split" style="width: 20%; float: left">
                            <div class="control-groupbarcode">
                                <div style="text-align: center">
                                    <div>
                                        <div style="float: left">
                                            <asp:RadioButton ID="rbnActive" runat="server" Text='<%$ Resources:LabelCaption,lbl_Active %>'
                                                GroupName="barcode" Checked="True" />
                                        </div>
                                        <div style="float: left; margin-left: 10%">
                                            <asp:RadioButton ID="rbnInActive" runat="server" Text='<%$ Resources:LabelCaption,lbl_In_Active %>'
                                                GroupName="barcode" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="LinkButton6" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Fetch %>'
                                OnClick="lnkLoadFilter_Click" Width="130px"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="Check" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_SelectAll %>'
                                OnClientClick="fncValidateCheckAll();return false;" Width="130px"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="CheckActiveAll" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_ActiveAll %>'
                                OnClientClick="fncValidateActiveCheckAll();return false;" Width="130px"></asp:LinkButton>
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
               <%-- <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                    style="height: 400px">--%>

                    <div class="gridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="right-container-top-detail">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col"></th>
                                                    <th scope="col">Select
                                                    </th>
                                                    <th scope="col">Active
                                                    </th>
                                                    <th scope="col">Range From
                                                    </th>
                                                    <th scope="col">Range To
                                                    </th>
                                                    <th scope="col">Coupon Value
                                                    </th>
                                                    <th scope="col">FromDate
                                                    </th>
                                                    <th scope="col">ToDate
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 184px; width: 1321px; background-color: aliceblue;">
                                            <asp:GridView ID="gvDiscount" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                                ShowHeaderWhenEmpty="true" OnRowDeleting="gv_OnRowDeleting" CssClass="pshro_GridDgn grdLoad"
                                                OnRowDataBound="gv_OnRowDataBound" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                 <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Remove" />
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Active">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkIsACTIF" runat="server" Checked='<%# (Eval("Active").ToString() == "1" ? true : false) %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkACTIF" runat="server" Checked='<%# (Eval("Active").ToString() == "1" ? true : false) %>'
                                                                Enabled="true" />
                                                        </EditItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkNewACTIF" runat="server" Checked="true" />
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField >
                                    <HeaderTemplate>
                                        <asp:Label ID="Label18" runat="server" Text="Active"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkActive" Checked='<%# bool.Parse(Eval("Active")) %>'  runat="server" Width="40px" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                                    <asp:BoundField DataField="FromRange" HeaderText="Range From" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="ToRange" HeaderText="Range To" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="CouponValue" HeaderText="Coupon Value" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField DataField="FromDate" HeaderText="FromDate"></asp:BoundField>
                                                    <asp:BoundField DataField="ToDate" HeaderText="ToDate"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottom-purchase-container-add">
            <div class="bottom-purchase-container-add-Text">
                <div class="control-group-split">
                    <div class="control-group-left" style="width: 10%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lbl_BillValFrm %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtBillValFrm" runat="server" onkeypress="return isNumberKey(event)"
                                onkeydown="fncBillValFrmFocus(event)" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 10%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_BillValTo %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtBillValTo" runat="server" onkeypress="return isNumberKey(event)"
                                onkeydown="fncBillValToFocus(event)" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 10%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label161" runat="server" Text='<%$ Resources:LabelCaption,lbl_CouponVal %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtCouponVal" runat="server" onkeypress="return isNumberKey(event)"
                                onkeydown="fncCouponFrmDateFocus(event)" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 10%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtCouponFromDate" runat="server" CssClass="form-control-res" onkeydown="fncCouponFrmToFocus(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 10%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="lblBatchno" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtCouponToDate" runat="server" CssClass="form-control-res" onkeydown="fncAddFocus(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%">
                        <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                            <div class="control-button">
                                <div class="label-left" style="width: 100%">
                                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                        OnClick="lnkAdd_Click" OnClientClick="return fncValidateAdd();"><i class="icon-play"></i></asp:LinkButton>
                                </div>
                            </div>
                            <%--<div class="control-button">
                            <asp:LinkButton ID="lnkDelete" runat="server" class="button-red-small"><i class="icon-play"></i>Delete</asp:LinkButton>
                        </div>--%>
                            <div class="control-button">
                                <div class="label-right" style="width: 100%">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                        OnClientClick="fncClear();"><i class="icon-play"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="float: right; display: table">
                <div class="control-button">
                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'
                        OnClientClick="fncValidateSave();return false;" OnClick="lnkConfirmSavebtnOk_Click"
                        Width="150px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                        Text='<%$ Resources:LabelCaption,btn_Delete %>'></asp:LinkButton>
                </div>
                <%--<div class="control-button">
                        <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                            Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                            Text="Delete"></asp:LinkButton>
                    </div>--%>
                <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
            </div>
        </div>
    </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="SelectAny">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DisplayDelete">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteConfirm">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdatePosting">
            <p>
                <%=Resources.LabelCaption.Alert_Delete%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ShowPopupMessageBoxNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full">
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
