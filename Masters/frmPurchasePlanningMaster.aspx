﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" 
    CodeBehind="frmPurchasePlanningMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmPurchasePlanningMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 120px;
            max-width: 120px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 225px;
            max-width: 225px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 325px;
            max-width: 325px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
             min-width: 225px;
            max-width: 225px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 225px;
            max-width: 225px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
          
              min-width: 87px;
             max-width: 225px;
        }
               .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        var divDis = '0';
        function ValidateForm() {
            var Show = '';
            if (document.getElementById("<%=txtPlanCode.ClientID%>").value == "") {
                Show = Show + 'Please Enter Plan Code';
                document.getElementById("<%=txtPlanCode.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtPlanName.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Plan Name';
                document.getElementById("<%=txtPlanName.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

    </script>

    <script type="text/javascript">

        function pageLoad() {

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc   

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");

            $('input[type=text]').bind('change', function () {
                if (this.value.match(/[^a-zA-Z0-9/]/g)) {

                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(/[^a-zA-Z0-9/]/g, '');
                }
            });
        }

        function imgbtnEdit_ClientClick(source) {
            clearForm();
            DisplayDetails($(source).closest("tr"));
        }

        function DisplayDetails(row) {
            $('#<%=hdfdstatus.ClientID %>').val("EDIT");
            $('#<%=txtPlanCode.ClientID %>').prop("disabled", true);
            $('#<%=txtPlanCode.ClientID %>').val(($("td", row).eq(2).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtPlanName.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtFromDate.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtToDate.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));           
            if ($("td", row).eq(6).html().replace(/&nbsp;/g, '') == "True") {
                $('#<%= chkIsActive.ClientID %>').attr("checked", "checked");
                }
                else {
                    $('#<%= chkIsActive.ClientID %>').removeAttr("checked");
                }
        }
         function dept_Delete(source) {
             debugger;
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });
         }

        $('input[type=text]').bind('change', function () {
            if (this.value.match(SpecialChar)) {
                ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                this.value = this.value.replace(SpecialChar, '');
            }
        });       

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        ValidateForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").prop('checked', false);
            return false;
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchasePlanningMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchasePlanningMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnl" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>

            <div class="main-container">
                <div id="breadcrumbs" class="breadcrumbs" runat="Server">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Purchase Plan Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>

                 <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <div class="container-group-small">                    
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:HiddenField ID="hdfdstatus" runat="server" />
                                <asp:Label ID="Label1" runat="server" Text="Plan Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPlanCode" MaxLength="20" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Plan Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPlanName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display:none">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display:none">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Is Active"></asp:Label>
                            </div>
                            <div class="label-left">
                                <asp:CheckBox ID="chkIsActive" runat="server" />
                            </div>
                        </div>
                    </div>

                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" Style="display: none" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Plan Name to Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="150px" Style="visibility: hidden"
                        OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>
                        </asp:Panel>
                    </div>

                    <div class="col-md-12">
                        <div class="grdLoad">
                         <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                        <th scope="col">Delete
                                        </th>
                                        <th scope="col">Edit
                                        </th>
                                        <th scope="col">Plane Code
                                        </th>
                                        <th scope="col">Plane Name
                                        </th>
                                        <th scope="col">From Date
                                        </th>
                                        <th scope="col">To date
                                        </th>
                                        <th scope="col">Is Active
                                        </th>
                                                                            
                                    </tr>
                                </thead>
                            </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1338px; background-color: aliceblue;">
                        <asp:GridView ID="gvPurchasePlanList" runat="server" AllowSorting="true" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                            AllowPaging="True" PageSize="10" ShowHeader="false" ssClass="pshro_GridDgn" DataKeyNames="PlanCode">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                    ToolTip="Click here to Delete" OnClientClick="return dept_Delete(this);  return false;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                            ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PlanCode" HeaderText="Plan Code"></asp:BoundField>
                                <asp:BoundField DataField="PlanDesc" HeaderText="Plan Desc"></asp:BoundField>
                                <asp:BoundField DataField="FromDate" HeaderText="From Date"></asp:BoundField>
                                <asp:BoundField DataField="ToDate" HeaderText="To Date"></asp:BoundField>
                                <asp:BoundField DataField="IsActive" HeaderText="IsActive"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                   </div>
                </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
       <ups:PaginationUserControl runat="server" ID="PlanningMaster" OnPaginationButtonClick="Planning_PaginationButtonClick" />
      <asp:HiddenField ID="hdnValue" Value="" runat="server" />
     <div class="hiddencol">
     <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
       </div>
</asp:Content>
