﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCustomerLoyaltyMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmCustomerLoyaltyMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textright {
            text-align: right;
            padding-right: 5px;
        }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input,select").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                             e.preventDefault();
                         }

                 }
             };
             $(document).ready(function () {
                 $('#<%=ddlType.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlType.ClientID %>').next().remove();
                 $('#<%=txtAmountFrom.ClientID %>').number(true, 2);
                $('#<%=txtAmountTo.ClientID %>').number(true, 2);
            clearForm();
        });
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=ddlType.ClientID%>").value == 0) {
                Show = Show + 'Please select Any Type';
                document.getElementById("<%=ddlType.ClientID %>").focus();
            }


            if (document.getElementById("<%=txtMonth.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Month';
                document.getElementById("<%=txtMonth.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtAmountFrom.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter From Amount';
                document.getElementById("<%=txtAmountFrom.ClientID %>").focus();
            }
            if (parseFloat(document.getElementById("<%=txtAmountFrom.ClientID%>").value) > parseFloat(document.getElementById("<%=txtAmountTo.ClientID%>").value)) {
                Show = Show + '<br />Please Enter Amount Between range';
                     document.getElementById("<%=txtAmountTo.ClientID %>").focus();
            }
               

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                $("input").removeAttr('disabled');
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }


        }
        function imgbtnEdit_ClientClick(source) {

            DisplayDetails($(source).closest("tr"));

        }

        function DisplayDetails(row) {

            $('#<%=ddlType.ClientID %>').val($("td", row).eq(1).html().replace(/&nbsp;/g, ''));
              
             $('#<%=ddlType.ClientID %>').prop("disabled", true);
             $('#<%=txtMonth.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
             $('#<%=txtAmountFrom.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
             $('#<%=txtAmountTo.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
         }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'CustomerLoyaltyMaster');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CustomerLoyaltyMaster";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Loyalty Master</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>

                <div class="container-group-small" style="width: 100%; margin-top: 20px; margin-bottom: 20px">
                    <div class="container-control">

                        <div class="control-group-single-res">
                            <div class="col-md-2" style="text-align: center;">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Type"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="col-md-2" style="text-align: center;">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Month"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtMonth" onkeypress="return isNumberKey(event)" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                </div>

                            </div>
                            <div class="col-md-2" style="text-align: center;">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="From"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAmountFrom" Style="text-align: right;" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                </div>

                            </div>
                            <div class="col-md-2" style="text-align: center;">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="To"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtAmountTo" Style="text-align: right;" runat="server" MaxLength="10" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-1">


                                <asp:LinkButton ID="lnkSave" runat="server" OnClientClick="return ValidateForm()" OnClick="lnkSave_Click" class="button-red"><i class="icon-play"></i>Save(F4)</asp:LinkButton>





                            </div>

                            <div class="col-md-1">


                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear(F6)</asp:LinkButton>





                            </div>
                        </div>


                    </div>

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">


                    <asp:GridView ID="gvCustomerLoyaltyMaster" runat="server" AutoGenerateColumns="False"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false"
                        DataKeyNames="Type">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                        <Columns>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server"
                                        OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                        ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Type" HeaderText="Type"></asp:BoundField>
                            <asp:BoundField DataField="Month" HeaderText="Month"></asp:BoundField>
                            <asp:BoundField DataField="Amount_From" ItemStyle-CssClass="textright" HeaderText="From"></asp:BoundField>
                            <asp:BoundField DataField="Amount_To" ItemStyle-CssClass="textright" HeaderText="To"></asp:BoundField>

                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>

                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
