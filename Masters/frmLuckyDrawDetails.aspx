﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLuckyDrawDetails.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmLuckyDrawDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #grdBatch {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdBatch td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
                font-size: 11px;
            }

            #grdBatch tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #grdBatch tr:hover {
                background-color: lightpink;
                /*font-weight: bolder;*/
            }

            #grdBatch th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: center;
                background-color: crimson;
                font-size: 10px;
                font-weight: bolder;
                color: white;
            }

        #grdBatch2 {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdBatch2 td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
                font-size: 11px;
            }

            #grdBatch2 tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #grdBatch2 tr:hover {
                background-color: lightpink;
                /*font-weight: bolder;*/
            }

            #grdBatch2 th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: center;
                background-color: crimson;
                font-size: 10px;
                font-weight: bolder;
                color: white;
            }
             .gid_Itemsearch td:nth-child(1), .gid_Itemsearch th:nth-child(1) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(2), .gid_Itemsearch th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(3), .gid_Itemsearch th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .gid_Itemsearch td:nth-child(4), .gid_Itemsearch th:nth-child(4) {
            min-width: 460px;
            max-width: 460px;
        }

        .gid_Itemsearch td:nth-child(5), .gid_Itemsearch th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(6), .gid_Itemsearch th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(7), .gid_Itemsearch th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }
    </style>
    <script type="text/javascript">
         $(document).ready(function () {
             $("#<%= txtfrm.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
             $("#<%= txtto.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
             $("[id$=txtcode]").autocomplete({
                 source: function (request, response) {
                     var obj = {};
                     //obj.prefix = request.term.replace("'", "''");
                     obj.prefix = escape(request.term);
                     $.ajax({
                         url: '<%=ResolveUrl("~/Masters/frmLuckyDrawVoucher.aspx/GetFilterValue") %>',
                         //data: "{ 'prefix': '" + request.term + "'}",
                         data: JSON.stringify(obj),
                         dataType: "json",
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         success: function (data) {
                             response($.map(data.d, function (item) {
                                 return {
                                     label: item.split('/')[0],
                                     val: item.split('/')[1],
                                     desc: item.split('/')[2]
                                 }
                             }))
                         },
                         error: function (response) {
                             ShowPopupMessageBox(response.responseText);
                         },
                         failure: function (response) {
                             ShowPopupMessageBox(response.responseText);
                         }
                     });
                 },
                 focus: function (event, i) {
                     $("[id$=txtDesc]").val(i.item.desc);
                     $("[id$=txtcode]").val(i.item.val);
                     event.preventDefault();
                 },
                 //==========================> After Select Value
                 select: function (e, i) {

                     $("[id$=txtDesc]").val(i.item.desc);
                     $("[id$=txtcode]").val(i.item.val);
                     $("[id$=txtQty]").focus().select();

                     fncGetInventoryCode();


                     return false;
                 },

                 minLength: 2
             });
         });
         function clearForm() {
             $("#<%= txtfrm.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
             $("#<%= txtto.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
         }
         function fncOpendiv() {
             $("#grdBatch2 tbody").children().remove();
             clr();
             $("#dialog-confirm").dialog({
                 //autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: false,
                 buttons: {
                     Close: function () {
                         fncClear();
                         $(this).dialog("destroy");
                     }
                 }
             });
         }
         function fncScanInventoryCode(evt) {
             try {
                 //
                 var charCode = (evt.which) ? evt.which : evt.keyCode;
                 InventoryCode = $('#<%=txtcode.ClientID %>').val();




                 if (charCode == 13 || charCode == 9) {

                     fncGetInventoryCode();
                 }

                 return true;
             }
             catch (err) {

                 fncToastError(err.message);
             }
         }
         function fncSetValue() {
             try {
                 if (SearchTableName == "Inventory") {
                     $('#<%=txtcode.ClientID %>').val($.trim(Code));
                     $('#<%=txtDesc.ClientID %>').val($.trim(Description));
                     fncGetInventoryCode();
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncGetInventoryCode() {

             $('#fullgrid').dialog({
                 //autoOpen: true,

                 height: "auto",
                 width: "auto",

                 buttons: {
                     Close: function () {

                         $(this).dialog("close");

                     }

                 }
             });

             fncItemSearchForGrid();
            <%--if ($('#<%=txtBatch.ClientID %>').val() == "") {


                try {
                    var obj = {};
                    obj.code = $('#<%=txtcode.ClientID %>').val();


                    if (obj.code == "")
                        return;

                    $.ajax({
                        type: "POST",

                        url: "frmLuckyDrawVoucher.aspx/GetInventorycode",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            fncAssignValuestoTextBox(msg);
                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });
                }

                catch (err) {

                    ShowPopupMessageBox(err.message);
                }
            }--%>

         }
         function fncAssignValuestoTextBox(msg) {
             try {
                 var objStk, row;
                 var val = [];
                 objStk = msg.d;
                 val = objStk[0].split('|')
                 if (objStk.length > 0) {


                     $('#<%=txtBatch.ClientID %>').val($.trim(val[0]));
                    //$('#<%=txtQty.ClientID %>').val($.trim(val[1]));

                 }

                 else {
                     fncToastInformation('Invalied Item !');

                     return false;
                 }
             }


             catch (err) {
                 alert("a");
                 fncToastError(err.message);
             }
         }
         function Validate() {
             var Show = '';
           <%-- if (document.getElementById("<%=txtCustCode.ClientID%>").value == "") {
                //popUpObjectForSetFocusandOpen = document.getElementById("<%=txtCustCode.ClientID %>");
                //ShowPopupMessageBoxandFocustoObject('Please Enter Customer Code');

                return false;
            }--%>
             var userval = $('#<%=txtQty.ClientID %>').val();
             var stock = $('#<%=hidBatchNo.ClientID %>').val();
             if (document.getElementById("<%=txtfrmRange.ClientID%>").value == "") {
                 ShowPopupMessageBoxandFocustoObject('Please Enter From Range');
                 popUpObjectForSetFocusandOpen = document.getElementById("<%=txtfrmRange.ClientID %>");
                 return false;
             }
             else if (document.getElementById("<%=txtToRange.ClientID%>").value == "") {
                 ShowPopupMessageBoxandFocustoObject('Please Enter ToRange');
                 popUpObjectForSetFocusandOpen = document.getElementById("<%=txtToRange.ClientID %>");
                 return false;
             }
             else if (document.getElementById("<%=txtcode.ClientID%>").value == "") {
                 ShowPopupMessageBoxandFocustoObject('Please Enter ItemCode');
                 popUpObjectForSetFocusandOpen = document.getElementById("<%=txtcode.ClientID %>");
                 return false;
             }
             else if (document.getElementById("<%=txtDesc.ClientID%>").value == "") {
                 ShowPopupMessageBoxandFocustoObject('Please Enter ItemName');
                 popUpObjectForSetFocusandOpen = document.getElementById("<%=txtDesc.ClientID %>");
                 return false;
             }
             else if (document.getElementById("<%=txtBatch.ClientID%>").value == "") {
                 ShowPopupMessageBoxandOpentoObject('Please Select Batch');
                 popUpObjectForSetFocusandOpen = $('#<%=txtBatch.ClientID %>');
                 return false;
             }
             else if (document.getElementById("<%=txtQty.ClientID%>").value == "") {
                 ShowPopupMessageBoxandOpentoObject('Please Select Qty');
                 popUpObjectForSetFocusandOpen = $('#<%=txtQty.ClientID %>');
                 return false;
             }
             else if (parseInt(stock) < parseInt(userval)) {
                 ShowPopupMessageBox("GivenQty greater than QtyOnHand");
                 return false;
             }
             else {
                 $('input').removeAttr('disabled');


             }
             fncAdd();
             // fncClear();
             // return true;
         }
         var img = "";
         function showpreview(input) {
             if (input.files && input.files[0]) {
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $('#<%=Image1.ClientID %>').attr('src', e.target.result);
                     imgPic = e.target.result;
                 }
                 reader.readAsDataURL(input.files[0]);
             }
             //  __doPostBack('ctl00$ContentPlaceHolder1$btnclick1', '');
         }
         function binEncode(img1) {
             var binArray = []
             var datEncode = "";
             for (var i = 0; i < img1.length; i++) {
                 binArray.push(img1[i].charCodeAt(0).toString(2));
             }
             for (var j = 0; j < binArray.length; j++) {
                 var pad = padding_left(binArray[j], '0', 8);
                 datEncode += pad + ' ';
             }
             function padding_left(s, c, n) {
                 if (!s || !c || s.length >= n) {
                     return s;
                 }
                 var max = (n - s.length) / c.length;
                 for (var k = 0; k < max; k++) {
                     s = c + s;
                 }
                 return s;
             }
             console.log(binArray);
         }
        <%--function clearImage() {
            $('#<%=imgpreview.ClientID %>').val('');
            $('#<%=imgpreview.ClientID %>').attr('src','');
             return false;
        }--%>
         function disableFunctionKeys(e) {
             var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
             if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                 if (e.keyCode == 115) {
                   <%-- $("#<%= btnclick.ClientID %>").click();--%>
                     e.preventDefault();
                 }

             }
         };
         function fncItemSearchForGrid() {

             try {

                 $.ajax({
                     type: "POST",
                     url: '<%=ResolveUrl("frmLuckyDrawVoucher.aspx/fncItemSearchForGrid")%>',
                     data: "{ 'Code': '" + $("#<%=txtDesc.ClientID%>").val() + "'}",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (msg) {
                         fncItemBindtoTable(msg);
                     },
                     error: function (data) {
                         ShowPopupMessageBox(data.message);
                     }
                 });
                 return false;
             }
             catch (err) {
                 fncToastError(err.message);
             }

         }
         function fncItemBindtoTable(msg) {
             try {
                 var objSearchData, tblSearchData, Rowno = 0;
                 objSearchData = jQuery.parseJSON(msg.d);

                 tblSearchData = $("#tblItemSearch tbody");
                 tblSearchData.children().remove();

                 if (objSearchData.length > 0) {
                     for (var i = 0; i < objSearchData.length; i++) {

                         row = "<tr tabindex = '" + i + "' onfocus = 'return fncMouseSearchesFocus(this);' onkeydown = 'return fncSearchTableKeyDowns(event, this);'>"
                             + "<td id='tdRowNo'>  " + $.trim(objSearchData[i]["SNo"]) + "</td>" +
                             "<td id='tdItemCode' > " + $.trim(objSearchData[i]["InventoryCOde"]) + "</td>" +
                             "<td id='tdBatchNo' > " + $.trim(objSearchData[i]["BatchNo"]) + "</td>" +
                             "<td id='tdDes'>" + $.trim(objSearchData[i]["Description"]) + "</td>" +
                             "<td id='tdBatchQty' style ='text-align:right !important'>" + $.trim(objSearchData[i]["BatchQty"]) + "</td>" +
                             "<td id='tdPrice' style ='text-align:right !important'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                             "<td id='tdRemarks' style ='display:none'>" + $.trim(objSearchData[i]["Remarks"]) + "</td>" +
                             "<td id='tdVendorCode' style ='display:none'>" + $.trim(objSearchData[i]["VendorCode"]) + "</td>" +
                             "<td id='tdCashDiscount'style ='display:none'>" + $.trim(objSearchData[i]["CashDiscount"]) + "</td>" +
                             "<td id='tdVendorName'style ='display:none'>" + $.trim(objSearchData[i]["VendorName"]) + "</td>" +
                             //"<td id='tdPrice'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                             "<td id='tdMrp' style ='width:9%;text-align:right !important;'>" + $.trim(objSearchData[i]["MRP"]) + "</td></tr>";
                         tblSearchData.append(row);
                     }




                     //var scroll = tblSearchData[0].scrollHeight;
                     //tblSearchData.scrollTop(scroll);

                     tblSearchData.children().dblclick(fncSearchRowDoubleClick);
                     tblSearchData[0].setAttribute("style", "cursor:pointer");

                     tblSearchData.children().on('mouseover', function (evt) {
                         var rowobj = $(this);
                         rowobj.css("background-color", "Yellow");
                     });

                     tblSearchData.children().on('mouseout', function (evt) {
                         var rowobj = $(this);
                         rowobj.css("background-color", "white");
                     });


                     $("tr[tabindex=0]").focus();

                     return false;
                 }
             }
             catch (err) {
                 fncToastError(err.message);
             }

         }

         function fncMouseSearchesFocus(source) {
             try {
                 var rowobj = $(source);
                 rowobj.css("background-color", "#ADD8E6");
                 rowobj.siblings().css("background-color", "white");
             }
             catch (err) {
                 fncToastError(err);
             }
         }

         function fncSearchTableKeyDowns(evt, source) {
             try {
                 var rowobj = $(source);
                 var charCode = (evt.which) ? evt.which : evt.keyCode;
                 //Enter Key
                 if (charCode == 13) {
                     rowobj.dblclick();
                     return false;
                 }

                 //Down Key
                 else if (charCode == 40) {
                     var NextRowobj = rowobj.next();
                     if (NextRowobj.length > 0) {
                         NextRowobj.css("background-color", "#ADD8E6");
                         NextRowobj.siblings().css("background-color", "white");
                         NextRowobj.select().focus();
                         $('#<%=txtcode.ClientID %>').val($(NextRowobj).find("td").eq(3).html().trim());
                     }
                     return false;
                 }
                 //Up Key
                 else if (charCode == 38) {
                     var prevrowobj = rowobj.prev();
                     if (prevrowobj.length > 0) {
                         prevrowobj.css("background-color", "#ADD8E6");
                         prevrowobj.siblings().css("background-color", "white");
                         prevrowobj.select().focus();
                         $('#<%=txtcode.ClientID %>').val($(prevrowobj).find("td").eq(3).html().trim());
                     }
                     else {
                         prevrowobj.css("background-color", "");
                         prevrowobj.siblings().css("background-color", "");
                         $("#search").focus();
                     }
                     return false;
                 }
                 return false;
             }
             catch (err) {
                 fncToastError(err);
             }
         }

         function fncBatchKeyDown(rowobj, evt) {
             //  alert('test');
             var rowobj, charCode;
             var scrollheight = 0;
             try {
                 //var rowobj = $(this);
                 var charCode = (evt.which) ? evt.which : evt.keyCode;

                 if (charCode == 13) {
                     rowobj.dblclick();
                     return false;
                 }
                 else if (charCode == 40) {

                     var NextRowobj = rowobj.next();
                     if (NextRowobj.length > 0) {
                         NextRowobj.css("background-color", "Yellow");
                         NextRowobj.siblings().css("background-color", "white");
                         NextRowobj.select().focus();

                         setTimeout(function () {
                             scrollheight = $("#tblItemSearch tbody").scrollTop();
                             if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                 $("#tblItemSearch tbody").scrollTop(0);
                             }
                             else if (parseFloat(scrollheight) > 10) {
                                 scrollheight = parseFloat(scrollheight) - 10;
                                 $("#tblItemSearch tbody").scrollTop(scrollheight);
                             }
                         }, 50);
                     }
                 }
                 else if (charCode == 38) {
                     var prevrowobj = rowobj.prev();
                     if (prevrowobj.length > 0) {
                         prevrowobj.css("background-color", "Yellow");
                         prevrowobj.siblings().css("background-color", "white");
                         prevrowobj.select().focus();

                         setTimeout(function () {
                             scrollheight = $("#tblItemSearch tbody").scrollTop();
                             if (parseFloat(scrollheight) > 3) {
                                 scrollheight = parseFloat(scrollheight) + 1;
                                 $("#tblItemSearch tbody").scrollTop(scrollheight);
                             }
                         }, 50);

                     }
                 }
             }
             catch (err) {
                 fncToastError(err.message);
             }
         }
         var val = "";
         function fncSearchRowDoubleClick() {
             try {
                 Itemcode = $.trim($(this).find("td").eq(1).text());



                 $('#<%=txtBatch.ClientID %>').val($.trim($(this).find("td").eq(2).text()));
                 $('#<%=hidBatchNo.ClientID %>').val($.trim($(this).find("td").eq(4).text()));
                 val = $('#<%=hidBatchNo.ClientID %>').val();

                 var tblSearchData = $("#tblItemSearch tbody");
                 tblSearchData.children().remove();
                 $('#fullgrid').dialog("close");

                 $('#<%=txtQty.ClientID%>').focus();

             }
             catch (err) {
                 fncToastError(err.message);
             }
         }
         function fncDeleteItem(source) {
             try {
                 var rowObj, totalValue, netTotal;
                 rowObj = $(source).parent().parent();
                 Reason_Delete(source);

             }
             catch (err) {
                 alert("a");
                 fncToastError(err.message);
             }
         }
         function Reason_Delete(source) {

             $("#dialog-confirm1").dialog({

                 height: "auto",
                 width: "auto", 
                 buttons: {
                     "YES": function () {
                         $(this).dialog("close");
                         var rowObj, totalValue, netTotal;
                         rowObj = $(source).parent().parent();
                         var ItemCode = rowObj.find('td[id*=tdItemcode]').text();
                         var BatchNo = rowObj.find('td[id*=tdBatch]').text();
                         $.ajax({
                             type: "POST",
                             url: '<%=ResolveUrl("~/Masters/frmLuckyDrawDetails.aspx/fncDelete") %>',
                             data: "{'ItemCode':'" + ItemCode + "', 'Batch':'" + BatchNo + "', 'GroupNo':'" + $("#<%= txtLdno.ClientID %>").val() + "'}",
                             contentType: "application/json; charset=utf-8",
                             dataType: "json",
                             success: function (msg) {
                                 if (msg.d =="Deleted Successfully")
                                     rowObj.remove();

                                 ShowPopupMessageBox(msg.d);
                             },
                             error: function (data) {
                                 ShowPopupMessageBox('Something Went Wrong')
                             }
                         });
                         
                         lastRowNo = 0;

                         $("#grdBatch2 tbody").children().each(function () {
                             lastRowNo = parseInt(lastRowNo) + 1;
                             $(this).find('td[id*=tdRowNo]').text(lastRowNo);
                         });
                     },
                     "NO": function () {
                         $(this).dialog("close");
                         return false();
                     }
                 }
             });

         }
         var imgPic = '';
         function fncAdd() {

           //  imgPic = $('#<%=Image1.ClientID %>').val();
             //$.session.set('Key', imgPic);
             //fncAddGrid();
             __doPostBack('ctl00$ContentPlaceHolder1$btnclick1', '');
         <%--   $("#<%= btnclick.ClientID %>").click();--%>


           <%--// var img = $('#<%=imgpreview.ClientID %>').val();
            grdBatch2 = $("#grdBatch2 tbody");
            row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                lastRowNo + "</td><td>" +
                "<img style='text-align:center;' alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                $('#<%=txtcode.ClientID %>').val() + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                $('#<%=txtDesc.ClientID %>').val() + "</td><td  id='tdBatch_" + lastRowNo + "' >" +
                $('#<%=txtBatch.ClientID %>').val() + "</td><td  >" + "<img  style='text-align:center;height:40px;width:40px' id='tdimg_'  src='" + img + "'/></td><td  id='tdQty_" + lastRowNo + "' >"
                + $('#<%=txtQty.ClientID %>').val()
                + "</td></tr>";



            grdBatch2.append(row);
            fncClear();--%>
             //return false;

         }
         function fncsave() {
             rowStkDet = "<table>";
             $("#grdBatch2 tbody").children().each(function () {
                 desc = escape($.trim($(this).find('td[id*=tdDesc_]').text()));
                 rowStkDet = rowStkDet + "<stkDet Itemcode='" + $(this).find('td[id*=tdItemcode_]').text() + "'";
                 rowStkDet = rowStkDet + " Descr='" + desc + "'";
                 rowStkDet = rowStkDet + " Batch='" + $(this).find('td[id*=tdBatch_] ').text() + "'";
                 //rowStkDet = rowStkDet + " Img='" + $(this).find('td img[id*=tdimg_] ').attr('src') + "'";
                 rowStkDet = rowStkDet + " Byte='" + $(this).find('td[id*=tdByte_] ').text() + "'";
                 rowStkDet = rowStkDet + " Qty='" + $(this).find('td[id*=tdQty_]').text() + "'";
                 rowStkDet = rowStkDet + "></stkDet>";
             });
             rowStkDet = rowStkDet + "</table>";

             $('#<%=hidsaveVoucher.ClientID %>').val(escape(rowStkDet));
             var frmdate = $("#<%= txtfrm.ClientID %>").val();
             var todate = $("#<%= txtto.ClientID %>").val();
             var mark = $('#<%=txtRemarks.ClientID %>').val();
             var frmRange = $('#<%=txtfrmRange.ClientID %>').val();
             var toRange = $('#<%=txtToRange.ClientID %>').val();
             $('#<%=hidfrmdate.ClientID %>').val(frmdate);
             $('#<%=hidtodate.ClientID %>').val(todate);
             $('#<%=hidfrmRange.ClientID %>').val(frmRange);
             $('#<%=hidtoRange.ClientID %>').val(toRange);
             $('#<%=hidRemarks.ClientID %>').val(mark);

         }
         function clr() {
             $("#<%= txtfrm.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
             $("#<%= txtto.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
             $('#<%=txtRemarks.ClientID %>').val('');
             $('#<%=txtfrmRange.ClientID %>').val('');
             $('#<%=txtToRange.ClientID %>').val('');
         }
         function fncClear() {
           <%-- $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtfrm.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtto.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");--%>
             $('#<%=txtBatch.ClientID %>').val('');
             $('#<%=txtQty.ClientID %>').val('');
             $('#<%=txtcode.ClientID %>').val('');
             $('#<%=txtDesc.ClientID %>').val('');
           <%-- $('#<%=imgpreview.ClientID %>').attr('src', '');--%>
            <%--$('#<%=txtRemarks.ClientID %>').val(''); --%>
             $('#<%=hidBatchNo.ClientID %>').val('');
           <%-- $('#<%=txtfrmRange.ClientID %>').val(''); 
            $('#<%=txtToRange.ClientID %>').val('');--%>

         }
         function fncBindHeader(obj) {
             var grdBatch = $("#grdBatch tbody");



             var len = obj.length;
             if (len > 0) {
                 for (var i = 0; i < obj.length; i++) {
                     var split = obj[i].split('|');
                     val = "<tr class='text-center' style='text-align:right'>";
                     val += "<td>" + split[0] + "</td>";
                     val += "<td>" + split[1] + "</td>";
                     val += "<td>" + split[2] + "</td>";
                     val += "<td>" + split[3] + "</td>";
                     val += "<td>" + split[4] + "</td>";
                     val += "<td>" + split[5] + "</td>";
                     val += "</tr>";

                     grdBatch.append(val);
                 }
             }
         }

         $(document).ready(function () {
             $(document).on('keydown', disableFunctionKeys);
         });

         function fncAddGrid() {
             var grdBatch2, row, netTotal, batchNo, id, lastRowNo = "0";
             lastRowNo = 0;
             $.ajax({
                 type: "POST",
                 url: '<%=ResolveUrl("~/Masters/frmLuckyDrawDetails.aspx/fncGetDetails") %>',
                 data: "{'GroupNo':'" + $("#<%= txtLdno.ClientID %>").val() + "'}",
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (msg) {
                     var obj = jQuery.parseJSON(msg.d); 
                     var grdBatch2 = $("#grdBatch2 tbody");
                     grdBatch2.children().remove();
                     for (var i = 0; i < obj.length; i++) {
                         lastRowNo = i + 1;
                         var row = "<tr><td id='tdRowNo_" + lastRowNo + "' > " +
                             lastRowNo + "</td><td>" +
                             "<img style='text-align:center;' alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                             $.trim(obj[i]["ItemCode"]) + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                             $.trim(obj[i]["ItemName"]) + "</td><td  id='tdBatch_" + lastRowNo + "' >" +
                             $.trim(obj[i]["BatchNo"]) + "</td><td  >" + "<img  style='text-align:center;height:40px;width:40px' id='tdimg_'  src='" + $.trim(obj[i]["Image"])  + "'/></td><td  id='tdQty_" + lastRowNo + "' >"
                             + $.trim(obj[i]["StockQty"])
                               + "</td><td style='display:none;' id='tdByte_" + lastRowNo + "' >"
                             + $.trim('')
                               + "</td></tr>";
                            
                         grdBatch2.append(row);
                     }
                     fncClear();
                 },
                 error: function (data) {
                     ShowPopupMessageBox('Something Went Wrong')
                 }
             });

        }

        function fncsaveDia() {
            var grdBatch2 = $("#grdBatch2 tbody");
            if (grdBatch2.children().length > 0) {
                $("#dialog-save").dialog({
                    height: "auto",
                    width: "auto",
                    buttons: {
                        "OK": function () {
                            $(this).dialog("close");

                            //location.reload();
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSA', ''); 
                            
                        }
                    }
                });
            }
            else {
                ShowPopupMessageBox("Invalid Details");
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Lucky Drawer Voucher Details</li>
            </ul>
        </div>
        <div class="dialog-inv">
            <div class="dialog-inv-header">
                <div class="control-group-split">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="lblstatus" runat="server" Text="Active"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:CheckBox runat="server" ID="chkbox" Checked="true" />
                        </div>

                    </div>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblldno" runat="server" Text="LDNumber  " CssClass="label-left"></asp:Label>

                                <asp:TextBox ID="txtLdno" runat="server" CssClass="label-right" Style="background-color: yellow; cursor: not-allowed" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td style="padding: 0px 0px 0px 25px">

                                <asp:Label ID="lblfrmdate" runat="server" Text="FromDate  " CssClass="label-left"></asp:Label>

                                <asp:TextBox ID="txtfrm" runat="server" CssClass="label-right"></asp:TextBox>
                            </td>
                            <td style="padding: 0px 0px 0px 25px">

                                <asp:Label ID="lbltodate" runat="server" Text="ToDate  " CssClass="label-left"></asp:Label>


                                <asp:TextBox ID="txtto" runat="server" CssClass="label-right"></asp:TextBox>

                            </td>
                        </tr>
                    </table>
                    <br />
                    <table>
                        <tr>
                            <td style="padding: 0px 0px 0px 0px">
                                <asp:Label ID="lblfrmRange" runat="server" Text="FromRange  " CssClass="label-left"></asp:Label>

                                <asp:TextBox ID="txtfrmRange" runat="server" CssClass="label-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                            </td>
                            <td style="padding: 0px 0px 0px 25px">
                                <asp:Label ID="lbltoRange" runat="server" Text="ToRange  " CssClass="label-left"></asp:Label>

                                <asp:TextBox ID="txtToRange" runat="server" CssClass="label-right" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                            </td>
                            <td style="padding: 0px 0px 0px 25px">
                                <asp:Label ID="lblremark" runat="server" Text="Description  " CssClass="label-left"></asp:Label>

                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="label-right"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="dialog-inv-tax">
                <div class="dialog-inv-tax-header" style="background: #004bc6; color: white; padding: 0px 0px 0px 10px">
                    Voucher Item
                </div>
                <div class="dialog-Inv-tax-detail">
                    <div class="control-group-split">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblcode" runat="server" Text="ItemCode  " CssClass="label-left"></asp:Label>
                                    <asp:TextBox ID="txtcode" runat="server" CssClass="label-right" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtcode', 'txtQty');"></asp:TextBox>
                                </td>
                                <td style="padding: 0px 0px 0px 25px">
                                    <asp:Label ID="lbldesc" runat="server" Text="ItemDesc  " CssClass="label-left"></asp:Label>
                                    <asp:TextBox ID="txtDesc" runat="server" CssClass="label-right"></asp:TextBox>
                                </td>
                                <td style="padding: 0px 0px 0px 25px">
                                    <asp:Label ID="lblbatch" runat="server" Text="BatchNo  " CssClass="label-left"></asp:Label>
                                    <asp:TextBox ID="txtBatch" runat="server" CssClass="label-right"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table>
                            <tr>
                                <td style="padding: 0px 0px 0px 10px">
                                    <asp:Label ID="lblStock" runat="server" Text="AvlQty" CssClass="label-left"></asp:Label>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="label-right"></asp:TextBox>
                                    <asp:TextBox ID="mulitxt" runat="server" TextMode="MultiLine" Style="display: none"></asp:TextBox>
                                </td>
                                <td style="padding: 0px 0px 0px 10px">
                                    <asp:Image ID="Image1" runat="server" Height="100px" Width="165px" />
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnclick1" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <asp:FileUpload ID="FileUpload1" runat="server" onchange="showpreview(this);" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td style="padding: 0px 0px 0px 10px">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkadd" runat="server" class="button-blue" OnClientClick="Validate(); return false;"><i class="icon-play">Add</i></asp:LinkButton>
                                    </div>
                                </td>
                                <td style="padding: 0px 0px 0px 10px">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="fncsaveDia();return false;" ><i class="icon-play">Save</i></asp:LinkButton>  <%--OnClick="lnkSave_Click"--%>
                                    </div> 
                                        </td> 
                                    </tr>
                                </table>

                        <div class="tab-pane" role="tabpanel" id="Batch2" style="height: 250px; overflow: auto">
                            <div id="divBatchList2" style="height: 250px; border: 1px solid #d8d9d8; background-color: #f3f3f3; width: 1007px;">
                                <table id="grdBatch2" cellspacing="0" rules="all" border="1" style="border: 1px solid #d8d9d8; background-color: #f3f3f3; width: 1005px">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">ItemCode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">BatchNo
                                            </th>
                                            <th scope="col">Image
                                            </th>
                                            <th scope="col">AvlQty
                                            </th> 
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div id="fullgrid" style="display: none" title="Batch Detail">
                            <div id="divGrid">
                                <div id="divItemSearch " class="tbl_left" style="margin-left: 65px;">

                                    <div class="Payment_fixed_headers gid_Itemsearch">
                                        <table id="tblItemSearch" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr>
                                                    <th scope="col">S.No
                                                    </th>
                                                    <th scope="col">Item Code
                                                    </th>
                                                    <th scope="col">Batch No 
                                                    </th>
                                                    <th scope="col">Item Description
                                                    </th>
                                                    <th scope="col">Qty
                                                    </th>
                                                    <th scope="col">Selling Price
                                                    </th>
                                                    <th scope="col">MRP
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="dialog-Transferout">
                                </div>
                            </div>
                        </div>
                        <div id="dialog-confirm1" style="display: none;" title="Enterpriser">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                        </div>
                         <div id="dialog-save" style="display: none;" title="Enterpriser">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Saved Success fully</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="display_none">
                <asp:LinkButton ID="btnclick1" runat="server" OnClick="btnimg_click" />
                <asp:LinkButton ID="lnkSA" runat="server" OnClick="btnSave_Click" />
            </div>
        </div>

        <asp:HiddenField ID="hidBatchNo" runat="server" Value="" />
        <asp:HiddenField ID="hidsaveVoucher" runat="server" Value="" />
        <asp:HiddenField ID="hidfrmdate" runat="server" Value="" />
        <asp:HiddenField ID="hidtodate" runat="server" Value="" />
        <asp:HiddenField ID="hidfrmRange" runat="server" Value="" />
        <asp:HiddenField ID="hidtoRange" runat="server" Value="" />
        <asp:HiddenField ID="hidRemarks" runat="server" Value="" />
        <asp:HiddenField ID="hidbyte" runat="server" Value="" />

    </div>
</asp:Content>
