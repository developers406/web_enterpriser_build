﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmMemberType.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMemberType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 90px;
            max-width: 90px;
            text-align:center !important;
        }
        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 90px;
            max-width: 90px;
            text-align:center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
            text-align:center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 147px;
            max-width: 147px;

        }
        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 207px;
            max-width: 207px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 166px;
            max-width: 166px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 150px;
            max-width: 202px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 110px;
            max-width: 152px;
        }
        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 125px;
            max-width: 122px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
         
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function ValidateForm() {
            if ($('#<%=txtCardType.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Membertype");
                return false;
            }
            if (!($('#<%=txtCardType.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Membertype");
                return false;
            }
            var Show = '';

            if (document.getElementById("<%=txtCardType.ClientID%>").value == "") {
                Show = Show + 'Please Enter Card Type'
                document.getElementById("<%=txtCardType.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Card Description';
                document.getElementById("<%=txtDescription.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtValidityPeriod.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Validity Period';
                document.getElementById("<%=txtValidityPeriod.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtCardPrice.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Card Price';
                document.getElementById("<%=txtCardPrice.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

        function Cashier_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Membertype");
                return false;
            }

            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                         $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }
        function clearFormIn() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $('#<%=txtCardType.ClientID %>').prop("disabled", false);
            $('#<%=txtCardType.ClientID %>').focus();
            return false;
        }
        function pageLoad() {

             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
            //$(document).ready(function () {
            //    $('input[type=text]').bind('change', function () {
            //        if (this.value.match(/[^a-zA-Z0-9-, ]/g)) {
            //            ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
            //            this.value = this.value.replace(/[^a-zA-Z0-9-, ]/g, '');
            //        }
            //    });
            //});
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                     if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                    $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
                       <%-- else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'MemberType');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "MemberType";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Member Type</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
                <div class="container-group">
                    <div class="container-control">
                        <div class="container-left">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Card Type"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtCardType" runat="server" CssClass="form-control-res" MaxLength="20"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text="Validity Period(Months)"></asp:Label><span
                                        class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtValidityPeriod" onkeydown="return isNumberKeyWithDecimalNew(event)" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label10" runat="server" Text="Card Price"></asp:Label><span class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtCardPrice" onkeydown="return isNumberKeyWithDecimalNew(event)" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="container-right">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label7" runat="server" Text="Reward Points"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtRewardPoints" onkeydown="return isNumberKeyWithDecimalNew(event)" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text="Promotion Allowed"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:CheckBox ID="chkPromotionAllowed" runat="server" />
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="Label5" runat="server" Text="Active"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:CheckBox ID="chkActive" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()">Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearFormIn()">Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="gridDetails">
                    <div class="row">
                        <asp:label Id="lblcount" runat="server" Text="Total Membertype : " style="font-weight: 900; float:left;"></asp:label>
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                            <div class="right-container-top-detail">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Edit
                                                </th>
                                                <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Member Type
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Validity(Months)
                                                </th>
                                                <th scope="col">Card Price
                                                </th>
                                                <th scope="col">Reward Points
                                                </th>
                                                <th scope="col">Active
                                                </th>
                                                <th scope="col">Promotion Allowed
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 284px; width: 1330px; background-color: aliceblue;">
                                        <asp:GridView ID="gvMemberType" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                                            ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn grdLoad" AllowPaging="True" PageSize="10" ShowHeader="false"
                                            OnPageIndexChanging="gvMemberType_PageIndexChanging" DataKeyNames="MemberType">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                            ToolTip="Click here to Delete" OnClientClick=" return Cashier_Delete(this);  return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                            ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                <asp:BoundField DataField="MemberType" HeaderText="Member Type"></asp:BoundField>
                                                <asp:BoundField DataField="MemberTypeDescription" HeaderText="Description"></asp:BoundField>
                                                <asp:BoundField DataField="Validity" HeaderText="Validity(Months)" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="CardPrice" HeaderText="Card Price" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="RPPerDollar" HeaderText="Reward Points" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="Active" HeaderText="Active"></asp:BoundField>
                                                <asp:BoundField DataField="PromotionAllowed" HeaderText="Promotion Allowed"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hiddencol">
                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div> 
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
