﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="Subsync.aspx.cs" Inherits="EnterpriserWebFinal.Masters.Subsync" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="../js/jquery-1.8.3.min.js"></script>--%>
   <%-- <script src="../js/jquery.min.js"></script>--%>
   <script src="../js/jquery.dataTables.js"></script>
   <%-- <script src="../js/jquery.dataTables.min.js"></script>--%>
    
    <script type ="text/javascript">
        $(document).ready(function () {
          
            
            fncDownload();
          
            $("#tblItemhistory_wrapper").hide();
            //$("#tbldownload_wrapper").hide();
        
            //$("#tblUpload_wrapper").hide();
        });
        var Value = "";
        function fncGetSubDetails() {
            $("#drop").show();
                $("#tblItemhistory_wrapper").show();
                $("#tbldownload_wrapper").hide();
            $("#tblUpload_wrapper").hide();
            $("#down").hide();
            $("#up").hide();
            $("#title").hide();
            $("#title1").hide();
                try {
                    var obj = {};

                    obj.Mode = $("#txtLicense").val();<%--$('#<%=ddltype.ClientID %>').val();--%>
                    
                 
                    $.ajax({
                        type: "POST",
                        url: "Subsync.aspx/Getsync",
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                        console.log(response);
                        fncBindData(response.d);
                        },
                        failure: function (response) {
                            ShowPopupError(response);
                        }
                    });
                }
                catch (err) {
                    ShowPopupError(err.message);
                }
            }
        function fncBindData(obj) {
            if (parseFloat(timeoutDownload) > 1) {
                clearTimeout(timeoutDownload);
                // timeout = 0;
            }
            if (parseFloat(timeoutUpload) > 0) {
                clearTimeout(timeoutUpload);
                //  timeout = 0;
            }
                var tblItemhistory = $("#tblItemhistory tbody");
                tblItemhistory.children().remove();
                var sno = 0;
                var row = "";
                if (obj.length == 0) {

      /*              $("#tblItemhistory").dataTable();*/

                }
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {

                        var split = obj[i].split('|');
                        row = "<tr class='text-center'style='background-color:white;height:30px;'>";

                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[0] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[1]+ "</td>";
                        row += "<td class='row'  style='color:Black;font-size:17px'>" + split[2] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[3] + "</td>";
                        row += "<td  class='row' style='color:Black;font-size:17px'>" + split[4] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[5] + "</td>";
                        row += "<td  class='row' style='color:Black;font-size:17px'>" + split[6] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[7] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[8] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[9] + "</td>";
                        row += "<td class='row' style='color:Black;font-size:17px'>" + split[10] + "</td>";
                       
                        row += "</tr>";

                        tblItemhistory.append(row);

                    }

                }
            /*$("#tblItemhistory").dataTable();*/
            return;
        }
        var timeoutDownload = 0;
        var timeoutUpload = 0;
        function fncDownload() {
            $("#drop").hide();
            //$("#down").show();
            //$("#up").hide();
            $("#tbldownload_wrapper").show();
            $("#tblItemhistory_wrapper").hide();
            $("#tblUpload_wrapper").show();
            $("#title").show();
            $("#title1").show();

            var obj = {};
            obj.Mode = "Download";
            $.ajax({
                type: "POST",
                url: "Subsync.aspx/Getdownload",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    fncBinddownload(response.d);
                },
                failure: function (response) {
                    // ShowPopupError(response);
                }
            });

            var obj = {};
            obj.Mode = "Upload";
            $.ajax({
                type: "POST",
                url: "Subsync.aspx/Getdownload",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    fncBindupload(response.d);
                },
                failure: function (response) {
                    // ShowPopupError(response);
                }
            });


            function fncBinddownload(obj) {
                if (parseFloat(timeoutUpload) > 0) {

                    clearTimeout(timeoutUpload);
                    //timeoutUpload = 0;
                }

                var tbldownload = $("#tbldownload tbody");
                tbldownload.children().remove();
                var sno = 0;
                var row = "";
                if (obj.length == 0) {

                   /* $("#tbldownload").dataTable();*/

                }
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                   <%-- var val = $('#<%=DropDownList1.ClientID %>').val();--%>
                        var split = obj[i].split('|');
                        var d1 = split[2].split('/');
                        var newDate = d1[1] + '-' + d1[0].slice(-2) + '-' + d1[2];
                        var start_actual_time = new Date(newDate);

                        var d = new Date($.now());
                        //var CurrDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + (d.getHours() + 0) + ":" + d.getMinutes() + ":" + d.getSeconds();
                        //end_date = new Date(CurrDate);
                        var Diff = d - start_actual_time;
                        var diffSeconds = Diff / 1000;
                        var HH = Math.round(diffSeconds / 3600);
                        //if (val != "All") {
                        //    if (HH > 0 && HH <= 1) {

                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tbldownload.append(row);
                        //    }
                        //    if (HH >1 && HH<=2) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tbldownload.append(row);
                        //    }
                        //    if (HH >2 && HH<=3) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tbldownload.append(row);
                        //    }
                        //    if (HH>3 && HH<=4 ) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tbldownload.append(row);
                        //    }
                        //    if (HH >4 && HH<= 5) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tbldownload.append(row);
                        //    }
                        //}
                        // if (val == "All") {
                        if (HH > 1) {


                            row = "<tr class='text-center'style='background-color:red;height:40px;'>";

                            row += "<td style='color:Black;font-size:17px'>" + split[0] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[1] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[2] + "</td>";

                            row += "</tr>";

                            tbldownload.append(row);
                        }
                        if (HH <= 1) {

                            row = "<tr class='text-center'style='background-color:lightgreen;height:40px;'>";

                            row += "<td style='color:Black;font-size:17px'>" + split[0] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[1] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[2] + "</td>";

                            row += "</tr>";

                            tbldownload.append(row);
                        }
                        // }
                    }

                }
                /*$("#tbldownload").dataTable();*/
                clearTimeout(timeoutDownload);
                timeoutDownload = setTimeout(function () {

                    //fncUpload();
                    //alert("download");
                }, 10000);
                return false;
            }
            //function fncUpload() {
            //    $("#drop").hide();
            //    //$("#down").hide();
            //    //$("#up").show();
            //    $("#tbldownload_wrapper").show();
            //    $("#tblItemhistory_wrapper").hide();
            //    $("#tblUpload_wrapper").show();
            //    try {

            //        var obj = {};
            //        obj.Mode = "Upload";
            //        $.ajax({
            //            type: "POST",
            //            url: "Subsync.aspx/Getdownload",
            //            data: JSON.stringify(obj),
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            success: function (response) {
            //                console.log(response);
            //                fncBindupload(response.d);
            //            },
            //            failure: function (response) {
            //                // ShowPopupError(response);
            //            }
            //        });
            //    }
            //    catch (err) {
            //        //ShowPopupError(err.message);
            //    }
            //}
            function fncBindupload(obj) {
                if (parseFloat(timeoutDownload) > 1) {

                    clearTimeout(timeoutDownload);

                    // timeoutDownload = 0;
                }

                var tblUpload = $("#tblUpload tbody");
                tblUpload.children().remove();
                var sno = 0;
                var row = "";
                if (obj.length == 0) {

                    $("#tblUpload").dataTable();

                }
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                   <%--// var ddl = $('#<%=DropDownList2.ClientID %>').val();--%>
                        var split = obj[i].split('|');
                        var d1 = split[2].split('/');
                        var newDate = d1[1] + '-' + d1[0].slice(-2) + '-' + d1[2];
                        var start_actual_time = new Date(newDate);

                        var d = new Date($.now());
                        //var CurrDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + (d.getHours() + 0) + ":" + d.getMinutes() + ":" + d.getSeconds();
                        //end_date = new Date(CurrDate);
                        var Diff = d - start_actual_time;
                        var diffSeconds = Diff / 1000;
                        var HH = Math.round(diffSeconds / 3600);
                        //if (ddl != "All") {
                        //    if (HH > 0 && HH <= 1) {

                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tblUpload.append(row);
                        //    }
                        //    if (HH > 1 && HH <= 2) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tblUpload.append(row);
                        //    }
                        //    if (HH > 2 && HH <= 3) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tblUpload.append(row);
                        //    }
                        //    if (HH > 3 && HH <= 4) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tblUpload.append(row);
                        //    }
                        //    if (HH > 4 && HH <= 5) {
                        //        row = "<tr class='text-center'style='background-color:white;'>";

                        //        row += "<td style='color:Black'>" + split[0] + "</td>";
                        //        row += "<td style='color:Black'>" + split[1] + "</td>";
                        //        row += "<td style='color:Black'>" + split[2] + "</td>";

                        //        row += "</tr>";

                        //        tblUpload.append(row);
                        //    }
                        //}
                        //if (ddl == "All") {
                        if (HH > 1) {


                            row = "<tr class='text-center'style='background-color:red;height:40px;'>";

                            row += "<td style='color:Black;font-size:17px'>" + split[0] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[1] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[2] + "</td>";

                            row += "</tr>";

                            tblUpload.append(row);
                        }
                        if (HH <= 1) {

                            row = "<tr class='text-center'style='background-color:lightgreen;height:40px;'>";

                            row += "<td style='color:Black;font-size:17px'>" + split[0] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[1] + "</td>";
                            row += "<td style='color:Black;font-size:17px'>" + split[2] + "</td>";

                            row += "</tr>";

                            tblUpload.append(row);
                        }
                        //}
                    }

                }
                /*$("#tblUpload").dataTable();*/
                clearTimeout(timeoutUpload);
                timeoutUpload = setTimeout(function () {
                    fncDownload();

                    //alert("upload");
                }, 60000);
            }
        }
        $(document).ready(function () {
            $("#txtLicense").change(function () {
                
                fncGetSubDetails();
                
            });
        });
       
      
        $(document).ready(function () {
            $(".click").mouseenter(function () {
                $(".click").css('background', 'orangered');
                $(".click").mouseleave(function () {
                    $(".click").css('background', 'midnightblue');
                });

            });
        });
        $(document).ready(function () {
            $(".clicked").mouseenter(function () {
                $(".clicked").css('background', 'orangered');
                $(".clicked").mouseleave(function () {
                    $(".clicked").css('background', 'midnightblue');
                });

            });
        });
        $(document).ready(function () {
            $(".clicks").mouseenter(function () {
                $(".clicks").css('background', 'orangered');
                $(".clicks").mouseleave(function () {
                    $(".clicks").css('background', 'midnightblue');
                });

            });
        });
      
        function fncboth() {
            fncDownload();
            //fncUpload();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Subsync</li>
            </ul>
        </div>
           <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
               <div>
                </br>
                      <div class="col-md-12" >
                          <div class="col-md-8"></div>
                          <div class="col-md-1" style="margin-right:-49px">
                   <asp:Button ID="btndetail" Text="Detail" runat="server" class="click" style="border-radius: 10px;background: midnightblue;color: white;" OnClientClick="fncGetSubDetails();return false;"/>
                              </div>
                           <div class="col-md-2" style="margin-right:-81px">
                   <asp:Button ID="btnsync" Text="Download & Upload" runat="server" class="clicked" style="border-radius: 10px;background-color: midnightblue;color:white;" OnClientClick="fncboth();return false;"/></div>
                                <div class="col-md-1">
                   <asp:Button ID="btnback" Text="Back To Dashboard" runat="server" class="clicks" style="border-radius: 10px;background-color: midnightblue;color:white;" OnClick="btnback_Click"/></div></div>
                  <%--  <asp:Button ID="btnDownload" Text="Download" runat="server" class="clicks" style="border-radius: 10px;background-color: orangered;color:white" OnClientClick="fncDownload();return false;"/>
                   <asp:Button ID="btnUpload" Text="Upload" runat="server" class="clicked" style="border-radius: 10px;background-color: orangered;color:white" OnClientClick="fncUpload();return false;"/>--%></br></br>
                 </div>
                   
             

                    <div class="right-container-top-detail">
                          
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                      
                                        <div id="tblItemhistory_wrapper">
                                         
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                              
                           <div class="float-right" style="margin-left:1017px">
                              
                            <%--    <div id="txtLicense_chzn" class="chzn-container chzn-container-single" style="width: 166px;position: absolute;top: 13px;left: 1066px;">--%>
                            <select id="txtLicense" style="position: absolute;top: 8px;left: 92px;height: 29px;width: 166px; ">
                               
                                <option>All</option>
                                <option>Master</option>
                                <option>PurchaseUpload</option>
                                <option>SalesUpload</option>
                                <option>SalesUploadAD</option>
                                <option>TransactionDownload</option>
                            </select>
                        </div></br>
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="width:130px">MasterData
                                                    </th>
                                                    <th scope="col" style="width:130px">SubData
                                                    </th>
                                                    <th scope="col" style="width:130px">Value
                                                    </th>
                                                    <th scope="col" style="width:130px">Createuser
                                                    </th>
                                                     <th scope="col" style="width:130px">Createdate
                                                    </th>
                                                    <th scope="col" style="width:130px">ModifyUser
                                                    </th>
                                                    <th scope="col" style="width:130px">ModifyDate
                                                    </th>
                                                     <th scope="col" style="width:130px">RecPerInst
                                                    </th>
                                                     <th scope="col" style="width:130px">TranType
                                                    </th>
                                                     <th scope="col" style="width:130px">LastSyncDateTime
                                                    </th>
                                                      <th scope="col" style="width:130px">ProcessOrder
                                                    </th>
                                                </tr>
                                            </thead>
                                          <tbody></tbody>
                                        </table>
                                               
                                       
                                                
                                         </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div></div></br>
               <div id="title" class="col-md-12">
                   <div class="col-md-2"></div>
                   <div class="col-md-5"><span style="font-size: x-large;text-decoration:underline">Download Status</span></div>
                   <div class="col-md-4"><span style="font-size: x-large;text-decoration:underline">Upload Status</span></div>
               </div>
                                       <div class="float_left" style="padding: 42px 37px 2px 143px;">
                                        <div id="tbldownload_wrapper"><%--  style="padding: 37px 529px 55px 448px;">--%>
                                        <table id="tbldownload" cellspacing="0" rules="all" border="1" class="fixed_header">
                                           
                                            <thead>
                                                 <center>
                                                <tr>
                                                    <th scope="col" style="width:130px">MasterData</th>
                                                    <th scope="col" style="width:130px">SubData</th>
                                                     <th scope="col" style="width:148px">LastSyncDateTime
                                                    </th>
                                                </tr>
                                                      </center>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                               
                                        </table>
                                           
                                         </div>
                                           </div></br>
                 <div id="title1"></div></br></br>
                                        <div class="float_right" style="padding: 22px 213px 43px 34px;">
                                         <div id="tblUpload_wrapper" >
                                        <table id="tblUpload" cellspacing="0" rules="all" border="1" class="fixed_header">
                                           
                                            <thead>
                                                 <center>
                                                <tr>
                                                    <th scope="col" style="width:130px">MasterData</th>
                                                    <th scope="col" style="width:130px">SubData</th>
                                                     <th scope="col" style="width:148px">LastSyncDateTime
                                                    </th>
                                                </tr>
                                                      </center>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                               
                                        </table>
                                            </div>
                                            </div>
                                            
                                        
                                    
                    </div>
                   
        </div>
                     </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
     <asp:HiddenField ID="Drop" runat="server" Value="" />
</asp:Content>
