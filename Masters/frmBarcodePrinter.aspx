﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBarcodePrinter.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmBarcodePrinter" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 170px;
            max-width: 170px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 235px;
            max-width: 235px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 280px;
            max-width: 280px;
        }

         </style>
     <script type="text/javascript">
         $(document).keyup(function (e) {

             if (e.keyCode == 27) {
                 window.parent.jQuery('#popupBin').dialog('close');
                 window.parent.$('#popupBin').remove();

             }
         });
    
         function ValidateForm() {
             if ($('#<%=txtPrintCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                 ShowPopupMessageBox("You have no permission to save existing Bin");
                 return false;
             }

             if (!($('#<%=txtPrintCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                 ShowPopupMessageBox("You have no permission to save New Bin");
                 return false;
             }
             var Show = '';
             var PrintCode = $.trim($('#<%=txtPrintCode.ClientID%>').val());     // 23112022 musaraf
             var PrintName = $.trim($('#<%=txtPrintName.ClientID%>').val()); 

             if (PrintCode == "") {
                 Show = Show + '<br />Please Enter PrintCode';
                 document.getElementById("<%=txtPrintCode.ClientID %>").focus();
             }
             if (PrintName == "") {
                 Show = Show + '<br />Please Enter PrintName';
                 document.getElementById("<%=txtPrintName.ClientID %>").focus();
             }

             if (Show != '') {
                 ShowPopupMessageBox(Show);
                 return false;
             }

             else {
                 $("input").removeAttr('disabled');
                 __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                 return true;
             }
         }     
              function Barcode_Delete(source) {
                     if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                         ShowPopupMessageBox("You have no permission to Delete this Bin");
                         return false;
                     }
                     $("#dialog-confirm").dialog({
                         resizable: false,
                         height: "auto",
                         width: 400,
                         modal: true,
                         buttons: {
                             "YES": function () {
                                 $(this).dialog("close");
                                 $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                                 $('#<%=hdnValue1.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));
                                 $("#<%= btnDelete.ClientID %>").click();
                             },
                             "NO": function () {
                                 $(this).dialog("close");
                                 returnfalse();
                             }

                         }
                     });
                 }
                 function imgbtnEdit_ClientClick(source) {
                     if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                         ShowPopupMessageBox("You have no permission to Edit this Bin");
                         return false;
                     }
                     DisplayDetails($(source).closest("tr"));

                 }

                 function isNumberKey(evt) {
                     var charCode = (evt.which) ? evt.which : evt.keyCode;
                     if (charCode > 31 && (charCode < 48 || charCode > 57))
                         return false;
                     return true;
                 }

                 function DisplayDetails(row) {

                     $('#<%=txtPrintCode.ClientID %>').prop("disabled", true);
                     $('#<%=txtPrintCode.ClientID %>').val($("td", row).eq(2).html().replace(/&nbsp;/g, ''));
                     $('#<%=txtPrintName.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));


             }
     </script>

                 <script type="text/javascript">
                     function pageLoad() {
                       if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                          $('#<%=lnkSave.ClientID %>').css("display", "block");
                             }
                          else {
                      $('#<%=lnkSave.ClientID %>').css("display", "none");
                    }
              
                     $('#<%=txtPrintCode.ClientID %>').focusout(function () {
                      $.ajax({
                          url: "frmBin.aspx/GetExistingCode",
                          data: "{ 'Code': '" + $("#<%=txtPrintCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtPrintCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtPrintCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtPrintName.ClientID %>').val(strarray[1]);


                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });


            $("[id$=txtPrintName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmBin.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[0]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               
                select: function (e, i) {

                    $('#<%=txtPrintName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmBin.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },

                    minLength: 1
                });

                    $('input[type=text]').bind('change', function () {
                        if (this.value.match(/[^a-zA-Z0-9-. ]/g)) {

                            ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                            this.value = this.value.replace(/[^a-zA-Z0-9-. ]/g, '');
                        }
                    });

                }
                function clearForm() {
                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    $(':checkbox, :radio').prop('checked', false);

                  
                }

                function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                 $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClear.ClientID %>').click();
                                e.preventDefault();
                            }


                     }
                 };

        $(document).ready(function () {
                      $(document).on('keydown', disableFunctionKeys);
        });
             
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Barcode Printer</li>
            </ul>
        </div>
         
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                    <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                     <asp:HiddenField ID="hdnValue1" Value="" runat="server" />
          <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                            
                                <asp:Label ID="Label1" runat="server" Text="Printer Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPrintCode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            </div>
                              <div class="control-group-single-res">
                              <div class="label-left">
                            
                                <asp:Label ID="Label2" runat="server" Text="Printer Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPrintName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        </div>
                   <div class="button-contol" style="margin-left: 175px">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save</asp:LinkButton>
                        </div>
                             <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play" ></i>Clear</asp:LinkButton>
                        </div>
                    </div>
              </div>
                   </ContentTemplate>
        </asp:UpdatePanel>
           <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="GridDetails">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Name To Search"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'><i class="icon-play"></i></asp:LinkButton>
                                 <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>

                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                    <th scope="col">Printer Code
                                                    </th>
                                                    <th scope="col">Printer Name
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                         <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 152px; width: 1018px; background-color: aliceblue;">
                                            <asp:GridView ID="gvBarcode" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                oncopy="return false" DataKeyNames="PrintCode" CssClass="pshro_GridDgn grdLoad">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick="return Barcode_Delete(this);  return false;"
                                                                CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="PrintCode" HeaderText="Print Code"></asp:BoundField>
                                                     <asp:BoundField DataField="PrintName" HeaderText="Print Name"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyUser" HeaderText="ModifyUser"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate"></asp:BoundField>
                                                   
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <ups:PaginationUserControl runat="server" ID="BarcodePaging" OnPaginationButtonClick="Barcode_PaginationButtonClick" />
                </div>
                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
           <asp:HiddenField ID="hidSavebtn" runat="server" />
          <asp:HiddenField ID="hidDeletebtn" runat="server" />
          <asp:HiddenField ID="hidEditbtn" runat="server" />
          <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
