﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMISReportDashBoard.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMISReportDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/MIS/Chart.bundle.min.js" type="text/javascript"></script>
    <script src="../js/MIS/script.js" type="text/javascript"></script>
    <link href="../css/MIS/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <nav class="navbar navbar-default" style="margin-bottom: 0px">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header animation-element slide-left">

                <a class="navbar-brand " id="header">MIS Report</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right animation-element slide-right">



                    <select class="form-control" style="margin-top: 8px" id="ddlLocation"></select>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="col-md-12">
        <div class="col-md-3">
            <div id="TodaySales" class="maincontentborder" >

                <canvas id="ctxTodaySales" height="205px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="TodaySalesSum">Total - XXXXXX</p>
            </div>

        </div>
        <div class="col-md-3">
            <div id="CurrentMonthSales" class="maincontentborder" >
                <canvas id="ctxCurrentMonthSales" height="205px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="CurrentMonthSalesSum">Total - XXXXXX</p>
            </div>

        </div>
        <div class="col-md-3">
            <div id="TodayPurchase" class="maincontentborder" >
                <canvas id="ctxTodayPurchase" height="205px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="TodayPurchaseSum">Total - XXXXXX</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="maincontentborder" >
                <canvas id="ctxCurrentMonthPurchase" height="205px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="CurrentMonthPurchaseSum">Total - XXXXXX</p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-3">
            <div class="maincontentborder" >
                <canvas id="ctxVendorWisePurchaseToday" height="210px"></canvas>
                 <hr class="maincontenthr" />
                <p class="maincontentP" id="VendorWisePurchaseTodaySum">Total - XXXXXX</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="maincontentborder" >
                <canvas id="ctxVendorWisePurchaseOrderToday" height="210px"></canvas>
                 <hr class="maincontenthr" />
                <p class="maincontentP" id="VendorWisePurchaseOrderTodaySum">Total - XXXXXX</p>
            </div>
        </div>
        <div class="col-md-3">
            <div id="MovingStockAsOn" class="maincontentborder">
                <canvas id="ctxMovingStockAsOn" height="210px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="MovingStockAsOnSum">Total - XXXXXX</p>
            </div>
        </div>
        <div class="col-md-3">
            <div id="StockAsOn" class="maincontentborder" >
                <canvas id="ctxStockAsOn" height="210px"></canvas>
                <hr class="maincontenthr" />
                <p class="maincontentP" id="StockAsOnSum">Total - XXXXXX</p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
    <div class="col-md-6">
        <div class="col-md-12">
            <div class="maincontentborder " style="height: auto">
                <div id="BillValueRecgord" class="maincontentborder" >
                 
                    <table width="100%">
                        <tr>
                            <th></th>
                            <th>Today</th>
                            <th>Month</th>
                        </tr>

                        <tr id="trNoOfBills">
                            <td style="cursor: pointer;">No Of Bills</td>
                            <td id="NoOfBillsToday" style="text-align: right; cursor: pointer;">290</td>
                            <td id="NoOfBillsMonth" style="text-align: right; cursor: pointer;">9351</td>

                        </tr>
                        <tr id="trTotalSalesValue">
                            <td style="cursor: pointer;">Total Sales Value</td>
                            <td id="TotalSalesValueToday" style="text-align: right; cursor: pointer;">157984.00</td>
                            <td id="TotalSalesValueMonth" style="text-align: right; cursor: pointer;">5209966.00</td>
                        </tr>
                        <tr id="trAvgBillValue">
                            <td style="cursor: pointer;">Avg Bill Value</td>
                            <td id="AvgBillValueToday" style="text-align: right; cursor: pointer;">544.77</td>
                            <td id="AvgBillValueMonth" style="text-align: right; cursor: pointer;">557.00</td>
                        </tr>
                    </table>
                </div>
                <div class="maincontentborder">
                    <h4 class="text-center">Item Wise Bills</h4>
                     <table width='100%'><tbody><tr><th style='width: 28.5%'>Item</th><th style='width: 16.9%'>Today</th><th style='width: 16.9%'>Percentage</th><th style='width: 17.5%'>Month</th><th style='width: 19.5%'>PercentageMonth</th></tr></tbody></table>
                    <div id="BillValueRecord" style=" height: 175px; overflow: auto">
                    </div>
                </div>
                <div class="maincontentborder">
                     <h4 class="text-center">Cost To Cost Sales</h4>
                    <table width='100%'><tbody><tr><th style='width: 50%'>Item</th><th style='width: 27%'>Today Qty</th><th>Today Value</th></tr></tbody></table>
                    <div id="CostToCostSales"  style=" height: 175px; overflow: auto">
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    <div class="col-md-6">
        <div id="CurrentMonthPurchase" class="maincontentborder animation-element slide-top" >
            <canvas id="ctxHourlySales" height="258px"></canvas>
        </div>
    </div>
        </div>
    <div class="col-md-6">
            <div class="maincontentborder animation-element slide-top" >
                <canvas id="ctxBillingRangeValue" height="135px"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="maincontentborder animation-element slide-top" >
                <canvas id="ctxMarginReport" height="135px"></canvas>
            </div>
        </div>
    




    <div class="col-md-6">

        <div class="col-md-6">
            <div class="maincontentborder" >
                <canvas id="ctxSalesDateCompare" height="236px"></canvas>
            </div>
        </div>
        <div class="col-md-6">
            <div class="maincontentborder" >
                <canvas id="ctxSalesMonthCompare" height="236px"></canvas>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order By Record</h4>
                </div>
                <div class="modal-body">




                    <center>
                        <div style="width: 50%">
                            <select class="form-control" style="margin-top: 8px" id="ddlFilter">
                                <option value="Department">Department</option>
                                <option value="Category">Category</option>
                                <option value="Brand">Brand</option>
                            </select>
                            <button type="button" id="btLoadReport" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                        </div>
                    </center>


                    </nav>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalStock" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Stock As On</h4>
                </div>
                <div class="modal-body">




                    <center>
                        <div style="width: 50%">
                            <select class="form-control" style="margin-top: 8px" id="ddlFilterStock">
                                <option value="Department">Department</option>
                                <option value="Category">Category</option>
                                <option value="Brand">Brand</option>
                            </select>
                            <button type="button" id="btLoadReportStock" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                        </div>
                    </center>


                    </nav>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalMargin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Margin Report</h4>
                </div>
                <div class="modal-body">
                    <center>
                        <div style="width: 50%">
                            <select class="form-control" style="margin-top: 8px" id="ddlFilterMargin">
                                <option value="Department">Department</option>
                                <option value="Category">Category</option>
                                <option value="Brand">Brand</option>
                            </select>
                            <button type="button" id="btLoadReportMargin" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                        </div>
                    </center>


                    </nav>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalItemValue" role="dialog" width="100%">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ModalItemValueText">Select Filter</h4>
                </div>
                <div class="modal-body">

                    <div id="myModalItemValueValueRecord" class="maincontentborder animation-element slide-bottom" style="background-color: white; overflow: auto">
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalStockValue" role="dialog" width="100%">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ModalStockValueText">Select Filter</h4>
                </div>
                <div class="modal-body">

                    <div id="myModalStockRecord" class="maincontentborder animation-element slide-bottom" style="background-color: white; overflow: auto">
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="myModalMarginValue" role="dialog" width="100%">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="ModalMarginText">Select Filter</h4>
                </div>
                <div class="modal-body">

                    <div id="myModalMarginRecord" class="maincontentborder" style="background-color: white; overflow: auto">
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
