﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmLoyaltyMasterSetup.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmLoyaltyMasterSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 28px;
            max-width: 28px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 43px;
            max-width: 43px;
            text-align: center !important;
        }
         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 39px;
            max-width: 39px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 64px;
            max-width: 64px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 64px;
            max-width: 64px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 67px;
            max-width: 67px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 68px;
            max-width: 68px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 58px;
            max-width: 58px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
    
 .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
          <%--  if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }--%>
         }
         function Validate() {
           <%-- if ($('#<%=txtVoucherCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing LoyaltyMastersetup");
                return false;
            }
            if (!($('#<%=txtVoucherCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New LoyaltyMastersetup");
                return false;
            }--%>
            var Show = '';


            if (document.getElementById("<%=txtBaseAmount.ClientID%>").value == "") {
                Show = Show + 'Please Enter Base Amount';
                document.getElementById("<%=txtBaseAmount.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtBasePoints.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Base Points';
                document.getElementById("<%=txtBasePoints.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            //ShowPopupMessageBox(charCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 37 || charCode > 40) && charCode != 13)
                return false;
            return true;
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                $('#<%= lnkSave.ClientID %>').click();
                     e.preventDefault();
                 }
                 else if (e.keyCode == 117) {
                     $('#<%= lnkClear.ClientID %>').click();
                             e.preventDefault();
                         }
                         else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
                                 e.preventDefault();
                             }


                 }
             };

             $(document).ready(function () {
                 $(document).on('keydown', disableFunctionKeys);
             });
             function clearForm() {
                 $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                 $(':checkbox, :radio').prop('checked', false);
                 $("input").prop('checked', false); 
                 $('#<%=txtBaseAmount.ClientID %>').focus();
            return false;
        }
    </script>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'LoyaltyMasterSetup');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "LoyaltyMasterSetup";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Customer</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Loyalty Gift Setup</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Base Amount"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBaseAmount" runat="server" MaxLength="9" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Base Points"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBasePoints" runat="server" MaxLength="9" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Voucher Value"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVoucherValue" runat="server" MaxLength="9" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Voucher Points"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVoucherPoints" runat="server" MaxLength="9" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Periods"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtPeriods" runat="server" MaxLength="4" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return Validate()">Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()">Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                         <asp:label Id="lblcount" runat="server" Text="Total LoyaltyMastersetup :" style="font-weight: 900;"></asp:label>
                        </div>
                    </div>
                </div>
                <div class="gridDetails" style="width: 420px">

                    <div class="row">
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                            <div class="right-container-top-detail">

                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Edit
                                                </th>
                                                <th scope="col">Delete
                                                </th>
                                                 <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Base Amount
                                                </th>
                                                <th scope="col">Base Points
                                                </th>
                                                <th scope="col">Voucher Value
                                                </th>
                                                <th scope="col">Voucher Points
                                                </th>
                                                <th scope="col">Periods
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; background-color: aliceblue;">
                                        <asp:GridView ID="gvLoyaltyMasterSetup" runat="server" AllowSorting="true" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" DataKeyNames="BaseAmount">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                            ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/delete.png"
                                                            ToolTip="Click here to Delete" OnClientClick=" return Confirm_Delete()" OnClick="imgbtnDelete_Click" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="RowNumber" HeaderText="Base Amount" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="BaseAmount" HeaderText="Base Amount" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="BasePoints" HeaderText="Base Points" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="VoucherValue" HeaderText="Voucher Value" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="VoucherPoints" HeaderText="Voucher Points" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="Periods" HeaderText="Periods" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
