﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" Async="true"
    CodeBehind="frmVendorMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmVendorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .radioboxlist radioboxlistStyle {
            font-size: x-large;
            padding-right: 10px;
        }



        .radioboxlist label {
            color: Black;
            background-color: Silver;
            padding-left: 6px;
            padding-right: 5px;
            padding-top: 2px;
            padding-bottom: 2px;
            border: 1px solid #AAAAAA;
            white-space: nowrap;
            clear: left;
            margin-right: 5px;
            margin-left: 5px;
        }

        input:checked + label {
            color: white;
            background: red;
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <script type="text/javascript">
        $(document).keyup(function (e) {
            if (e.keyCode == 27) {
                window.parent.jQuery('#popupVendor').dialog('close');
                window.parent.$('#popupVendor').remove();
            }
        });
        function ValidateForm() {
            document.getElementById('<%=lnkSave.ClientID %>').style.display = "none";
            <%--if (document.getElementById("<%=txtVendorCode.ClientID %>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtVendorCode.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Vendor Code');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
            }
            else--%>
            if (document.getElementById("<%=txtVendorName.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtVendorName.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Your Name');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
            }
            else if (document.getElementById("<%=txtAddress1.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtAddress1.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Address');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
            }
                <%-- else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtHoldBills.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter Hold Bills');
                return false;
            }

            else if (document.getElementById("<%=txtCRLimit.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtCRLimit.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter CR Limit');
                return false;
            }
            else if (document.getElementById("<%=txtCRDays.ClientID%>").value == "") {

                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtCRDays.ClientID%>");
                ShowPopupMessageBoxandFocustoObject('Please Enter CR Days');
                return false;
            }--%>
            <%--else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                //ShowPopupMessageBoxandFocustoObject('10');
                return false;
            }
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                return false;
            }
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                return false;
            }
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value != "0") {
                return false;
            }--%>
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "0"
                && parseFloat(document.getElementById("<%=txtCRLimit.ClientID%>").value) != 0
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                ShowPopupMessageBoxandFocustoObject('Please Enter The Value of CR Days');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
            }
           <%-- else if (document.getElementById("<%=txtHoldBills.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                ShowPopupMessageBoxandFocustoObject('Please Enter The Value of CR Days');
                return false;
            }--%>
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value == "0") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Any One Value Hold Bills Or CR Limit');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
            }
         <%--   else if (document.getElementById("<%=txtHoldBills.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value != "0" &&
                document.getElementById("<%=txtCRDays.ClientID%>").value != "0") {
                ShowPopupMessageBoxandFocustoObject('Please Enter Hold Bills');
                return false;
            }
            else if (document.getElementById("<%=txtHoldBills.ClientID%>").value != "0"
                && document.getElementById("<%=txtCRLimit.ClientID%>").value == "0"
                && document.getElementById("<%=txtCRDays.ClientID%>").value != "0") {
                ShowPopupMessageBoxandFocustoObject('Please Enter CR Limit');
                return false;
            }--%>


              <%-- else if (!validateURL(document.getElementById("<%=txtURL.ClientID%>").value))
                {
                popUpObjectForSetFocusandOpen = document.getElementById("<%=txtURL.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject('Please Enter URL');
                document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                return false;
                }--%>

           



             else if (!validateEmail(document.getElementById("<%=txtEmail.ClientID%>").value))
                {
                    popUpObjectForSetFocusandOpen = document.getElementById("<%=txtEmail.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject('Please Enter Valid Email ID');
                    document.getElementById('<%=lnkSave.ClientID %>').style.display = "block";
                    return false;
                }

            else {

                showAll();
                $('input').removeAttr('disabled');
                return true;

            }           
        }
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test($email);
        }
        function validateURL(textval) {
            var urlregex = new RegExp(
                "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
            console.log(urlregex.test(textval));
            return urlregex.test(textval);
           
        }
        function fncSetText() {

            var GSttxt = $("[id$=txtTIN]").val();
            var checked_radio = $("[id*=rdoGstType] input:checked");
            if (GSttxt == '') {
                checked_radio.next().text(checked_radio.next().text().replace(" - RCM", "") + ' - RCM');
            }
            else {
                var value = checked_radio.val();
                if (value == '1')
                    checked_radio.next().text('IntraState');
                else if (value == '2')
                    checked_radio.next().text('InterState');
            }
        }

        function fncCountryValidate() {

            var Country = $("[id$=txtCountry]").val();
            var Orgin = $("[id*=HiddenCountry]").val();
            //ShowPopupMessageBox(Country.toUpperCase()); 
            if (Country.trim() != '') {
                if (Country.toUpperCase() != Orgin.toUpperCase()) {
                    var radio = $("[id*=rdoGstType] label:contains('Import')").closest("td").find("input");
                    radio.attr("checked", "checked");
                }
                else {
                    var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                    radio.attr("checked", "checked");
                }
            }

        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $('#<%=txtAgentCode.ClientID %>').removeAttr('disabled');
            $('#<%=txtAgentName.ClientID %>').removeAttr('disabled');
            var User = $("[id*=HiddenUser]").val();
            $("select").chosen({ width: '100%' });
            $('#divState').hide();
            $("*[name$='rdoGstType']").attr("disabled", "disabled");
            $('#<%=txtCountry.ClientID %>').val("INDIA");
            $("#<%=txtCRLimit.ClientID %>").number(true, 2);
            $("#<%=txtCRDays.ClientID %>").attr("Enabled", "Enabled");
            $("#<%=txtCRLimit.ClientID %>").attr("disabled", "disabled");
            $("#<%=txtHoldBills.ClientID %>").attr("disabled", "disabled");
            if (!$("#<%=txtVendorName.ClientID %>").is(':disabled')) {
                setAutocomplete();
            }
            if ($("#<%=chkStopPuchase.ClientID%>").attr('checked') == "checked") {
                $('#divStopReason').show();
            }
            else {
                $('#divStopReason').hide();
            }

            $("#<%=chkStopPuchase.ClientID%>").change(function () {
                if ($(this).attr('checked') == "checked") {
                    $('#divStopReason').show();
                    $("#<%=txtStopReason.ClientID%>").attr('placeholder', 'Reason for Stop Purchase');
                    $("#<%=txtStopReason.ClientID%>").focus();
                }
                else {
                    $('#divStopReason').hide();
                }
                return false;
            });

         
          


                    if ($("#<%=chkStopPayment.ClientID%>").attr('checked') == "checked") {
                        $('#divStopReasones').show();
                    }
                    else {
                        $('#divStopReasones').hide();
                     }

            $("#<%=chkStopPayment.ClientID%>").change(function () {
                if ($(this).attr('checked') == "checked") {
                    $('#divStopReasones').show();
                    $("#<%=txtStopReasones.ClientID%>").attr('placeholder', 'Reason for Stop Payment');
                      $("#<%=txtStopReasones.ClientID%>").focus();
                  }
                  else {
                      $('#divStopReasones').hide();
                  }

                  return false;


               });
         
          <%--  jQuery("[id$=txtVendorName]").keyup(function () {
                var raw_text = jQuery(this).val();
                var return_text = raw_text.replace(/[^a-zA-Z0-9 _.&()]/g, ' null');
                var data = return_text.split(" ").splice(-1);
                if (data == "null") {
                    jQuery(this).val(return_text.replace(" null", ""));
                    popUpObjectForSetFocusandOpen = document.getElementById("<%=txtVendorName.ClientID%>");
                    ShowPopupMessageBoxandFocustoObject('Please Enter The Vaild Charaters');
                    return false;
                }
                else {
                    jQuery(this).val(return_text);
                }

            });--%>

            $('#<%=txtTIN.ClientID %>').focusout(function () {
                if ($('#<%=txtTIN.ClientID %>').val().length != 15) {
                    $("*[name$='rdoGstType']").removeAttr("disabled");
                }
                else if ($('#<%=txtTIN.ClientID %>').val().length != 15) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtTIN.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject("Invalid GSTIN No");
                    $("*[name$='rdoGstType']").attr("disabled", "disabled");
                }
                else if ($('#<%=txtTIN.ClientID %>').val().length == 15) {
                    var res = $('#<%=txtTIN.ClientID %>').val().substr(0, 2);
                    var split = $('#<%=hidState.ClientID %>').val().split('-');
                    if (res == split[0]) {
                        $('#<%=ddlState.ClientID %>').val(res);
                        $('#<%=ddlState.ClientID %>').trigger("liszt:updated");
                        var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                    }
                    else {
                        var radio = $("[id*=rdoGstType] label:contains('InterState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                    }
                    $("*[name$='rdoGstType']").attr("disabled", "disabled");
                    $('#<%=txtIFSCcode.ClientID %>').focus();
                }

            });

            $('#<%=ddlState.ClientID %>').change(function () {
                var State = $(this).children("option:selected").val().trim();
                if ($('#<%=txtTIN.ClientID %>').val() == "") {
                    if ($('#<%=hidStateCodeCompany.ClientID %>').val() == State) {
                        var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                        var checked_radio = $("[id*=rdoGstType] input:checked");
                        checked_radio.next().text(checked_radio.next().text().replace(" - RCM", "") + ' - RCM');
                    }
                    else {
                        var radio = $("[id*=rdoGstType] label:contains('InterState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                        var checked_radio = $("[id*=rdoGstType] input:checked");
                        checked_radio.next().text(checked_radio.next().text().replace(" - RCM", "") + ' - RCM');
                    }
                }
                else {
                    var sGstState = $('#<%=txtTIN.ClientID %>').val().substr(0, 2);
                    if (sGstState == State) {
                        var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                    }
                    else {
                        var radio = $("[id*=rdoGstType] label:contains('InterState')").closest("td").find("input");
                        radio.attr("checked", "checked");
                    }

                }
                $("*[name$='rdoGstType']").attr("disabled", "disabled");
            });


            $(document).ready(function () {
                $("#<%=chkHoldBills.ClientID %>").click(function () { //Vijay Bills
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chkCRLimit.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtCRLimit.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtHoldBills.ClientID %>").removeAttr("disabled");
                        $("#<%=txtCRDays.ClientID %>").removeAttr("disabled");
                    }
                    else {
                        $("#<%=chkCRLimit.ClientID %>").removeAttr("disabled");
                        //$("#<%=txtCRLimit.ClientID %>").removeAttr("disabled");
                        $("#<%=txtHoldBills.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtCRDays.ClientID %>").attr("disabled", "disabled");
                    }
                });
                $("#<%=chkCRLimit.ClientID %>").click(function () { //Vijay Check
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#<%=chkHoldBills.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtHoldBills.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtCRLimit.ClientID %>").removeAttr("disabled");
                        $("#<%=txtCRDays.ClientID %>").removeAttr("disabled");
                    }
                    else {
                        $("#<%=chkHoldBills.ClientID %>").removeAttr("disabled");
                        //$("#<%=txtHoldBills.ClientID %>").removeAttr("disabled");
                        $("#<%=txtCRLimit.ClientID %>").attr("disabled", "disabled");
                        $("#<%=txtCRDays.ClientID %>").attr("disabled", "disabled");
                    }
                });
                $('#<%=txtTIN.ClientID %>').focusout(function () {
                    if ($('#<%=txtTIN.ClientID %>').val().length != 15) {
                        $("*[name$='rdoGstType']").removeAttr("disabled");
                    }
                    else if ($('#<%=txtTIN.ClientID %>').val().length != 15) {
                        <%--//$('#<%=txtTIN.ClientID %>').val("UNREGISTERED"); 
                        $("*[name$='rdoGstType']").removeAttr("disabled");
                        fncToastError("Please Enter 15 Digit GST Number");
                        if ($('#<%=txtCountry.ClientID %>').val() == "INDIA") {
                            var radio = $("[id*=rdoGstType] label:contains('Zero')").closest("td").find("input");
                            radio.attr("checked", "checked");
                        }
                        $('#<%=txtIFSCcode.ClientID %>').focus();--%>
                        popUpObjectForSetFocusandOpen = $('#<%=txtTIN.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject("Invalid GSTIN");
                        $("*[name$='rdoGstType']").attr("disabled", "disabled");
                    }
                    else if ($('#<%=txtTIN.ClientID %>').val().length == 15) {
                        var res = $('#<%=txtTIN.ClientID %>').val().substr(0, 2);
                        var split = $('#<%=hidState.ClientID %>').val().split('-');
                        if (res == split[0]) {
                            $('#<%=ddlState.ClientID %>').val(res);
                            $('#<%=ddlState.ClientID %>').trigger("liszt:updated");
                            var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
                            radio.attr("checked", "checked");
                        }
                        else {
                            var radio = $("[id*=rdoGstType] label:contains('InterState')").closest("td").find("input");
                            radio.attr("checked", "checked");
                        }
                        $("*[name$='rdoGstType']").attr("disabled", "disabled");
                        $('#<%=lnkG.ClientID %>').focus();
                    }
                });
                $('#<%=chkTIN.ClientID %>').change(function () {
                    if ($(this).is(":checked")) {
                        $('#<%=txtTIN.ClientID %>').prop('disabled', false);
                    }
                    else {
                        $('#<%=txtTIN.ClientID %>').prop('disabled', true);
                    }
                });

                $('#<%=chkCST.ClientID %>').change(function () {
                    if ($(this).is(":checked")) {
                        $('#<%=txtCST.ClientID %>').prop('disabled', false);
                        $('#<%=txtCSTPRC.ClientID %>').prop('disabled', true);
                    }
                    else {
                        $('#<%=txtCST.ClientID %>').prop('disabled', true);
                        $('#<%=txtCSTPRC.ClientID %>').prop('disabled', true);
                    }
                });
                $('#<%=txtCountry.ClientID %>').change(function () {
                    if ($('#<%=txtCountry.ClientID %>').val().toUpperCase() != "INDIA") {
                        $('#divState').show();
                        $('#divStateddl').hide();
                    }
                    else {
                        $('#divState').hide();
                        $('#divStateddl').show();
                    }
                });
            });

           <%-- if (User.toUpperCase() == "ADMIN") { //sankar
               // $('input[type=text]').bind('change', function () {
                $('input[type=text]').attr('disabled', true)
                 $('#<%=lnkSave.ClientID %>').prop('disabled', true);
                
            }--%>
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            $('#<%=txtVendorName.ClientID %>').focusout(function () {
                $('#<%=txtChequePrintName.ClientID %>').val($('#<%=txtVendorName.ClientID %>').val());
            });
        });
    </script>
    <script type="text/javascript">
        function clearForm() {
            window.location.href = "frmVendorMaster.aspx?";
            <%--var vendorcode = $('#<%=txtVendorCode.ClientID %>').val();
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $('#<%=lstLocsunday.ClientID %>').empty();
            $('#<%=lstLocMon.ClientID %>').empty();
            $('#<%=lstLocTue.ClientID %>').empty();
            $('#<%=lstLocWed.ClientID %>').empty();
            $('#<%=lstLocThu.ClientID %>').empty();
            $('#<%=lstLocFri.ClientID %>').empty();
            $('#<%=lstLocSat.ClientID %>').empty();
            $('#<%=txtVendorCode.ClientID %>').val(vendorcode);
            var radio = $("[id*=rdoGstType] label:contains('IntraState')").closest("td").find("input");
            radio.attr("checked", "checked");
            $('#<%=txtVendorName.ClientID %>').removeAttr("disabled");
            $('#<%=txtCountry.ClientID %>').val("INDIA");
            $("#<%=txtCRDays.ClientID %>").attr("disabled", "disabled");
            $("#<%=txtCRLimit.ClientID %>").attr("disabled", "disabled");
            $("#<%=txtHoldBills.ClientID %>").attr("disabled", "disabled");
            $('#<%=txtVendorName.ClientID %>').focus();--%>
        }
    </script>
    <script language="Javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Agent") {
                    $('#<%=txtAgentCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtAgentName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function getJSONP(url, success) {

            var ud = '_' + +new Date,
                script = document.createElement('script'),
                head = document.getElementsByTagName('head')[0]
                    || document.documentElement;

            window[ud] = function (data) {
                head.removeChild(script);
                success && success(data);
            };

            script.src = url.replace('callback=?', 'callback=' + ud);
            head.appendChild(script);

        }

        function fncToast() {
            ShowPopupMessageBox("Invalid GST Number.This Number Not in GST Portal.");
            return false;
        }

        function fncVerify(val) { //Vijay Verify 20191226
            try {
                var splitVal = val.split(':');
                $('#<%=txtVerifyGstIn.ClientID %>').val($('#<%=txtTIN.ClientID %>').val());
                $('#<%=txtRegName.ClientID %>').val(splitVal[0]);
                $('#<%=txtBustype.ClientID %>').val(splitVal[1]);
                $('#<%=txtBusName.ClientID %>').val(splitVal[2]);
                $('#<%=txtGstinAddress.ClientID %>').val(splitVal[3]);
                var splitadd = splitVal[3].split(',');
                if ($('#<%=txtBustype.ClientID %>').val() != "Active") {
                    $('#<%=txtBustype.ClientID %>').css('background', 'pink');
                }
                $("#divGstinVerify").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "GSTIN Verify",
                    appendTo: 'form:first',
                    buttons: {
                        "Confirm": function () {
                            if ($('#<%=txtBustype.ClientID %>').val() == "Active") {
                                $('#<%=hidGSTStatus.ClientID %>').val('1');
                                $('#<%=lnkG.ClientID %>').removeClass('button-green');
                                $('#<%=lnkG.ClientID %>').addClass('button-blue');
                                $('#<%=lnkG.ClientID %>').val('Clear');
                                $('#<%=txtTIN.ClientID %>').attr('disabled', 'disabled');

                                $('#<%=txtAddress1.ClientID %>').val($('#<%=txtGstinAddress.ClientID %>').val());
                                $('#<%=txtVendorName.ClientID %>').val($('#<%=txtRegName.ClientID %>').val());
                                var splitadd = splitVal[3].split(',');
                                var len = splitadd.length;
                                $('#<%=txtAddress1.ClientID %>').val(splitadd[len - len]);
                                $('#<%=txtAddress2.ClientID %>').val(splitadd[len-5] + ','+splitadd[len-4]);
                                $('#<%=txtCity.ClientID %>').val(splitadd[len-3]);
                                var splitpin = splitadd[len-1].split('-');
                                $('#<%=txtZipCode.ClientID %>').val(splitpin[1]);
                            }
                            else {
                                ShowPopupMessageBox("This Gst Number in Cancelled Status.You cannot Confirm this GST - " + $('#<%=txtTIN.ClientID %>').val())
                            }
                            $(this).dialog("close");
                        },
                        "Close": function () {
                            $(this).dialog("close");
                            $('#<%=hidGSTStatus.ClientID %>').val('0');
                        }
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        function fncGstNoVerify(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function setAutocomplete() {
            try {
                $("[id$=txtVendorName]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/Masters/frmVendorMaster.aspx/GetFilterValue") %>',
                            data: "{ 'prefix': '" + request.term.replace("'", "%27") + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtVendorName.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtVendorCode.ClientID %>').val($.trim(i.item.val));
                        $('#<%=txtVendorName.ClientID %>').val($.trim(i.item.label));
                        $('#<%=hidVendorname.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },
                    minLength: 1
                });

                $('#<%=txtVendorName.ClientID %>').focusout(function () {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Masters/frmVendorMaster.aspx/GetvendorDetail") %>',
                        data: "{ 'Code': '" + $("#<%=txtVendorName.ClientID%>").val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var temp = data.d;
                            var strarray = temp.split(',');

                            if (temp != "Null") {
                                $('#<%=txtVendorCode.ClientID %>').prop("disabled", true);
                                $('#<%=txtVendorCode.ClientID %>').val(strarray[0]);
                                $('#<%=txtVendorName.ClientID %>').val(strarray[2]);
                                __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');
                                <%--$('#<%=ddlsectionCode.ClientID %>').prop("disabled", true);--%>
                                return false;
                            }

                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClear() {
            $('#<%=txtVendorName.ClientID %>').removeAttr("disabled");
           // $('#<%=txtVendorName.ClientID %>').val('');
            $('#<%=txtVendorName.ClientID %>').focus();
        }

        function fncClearClick() {
            if ($('#<%=txtTIN.ClientID %>').is(':disabled')) {
                $('#<%=hidGSTStatus.ClientID %>').val('0');
                $('#<%=lnkG.ClientID %>').removeClass('button-blue');
                $('#<%=lnkG.ClientID %>').addClass('button-green');
                $('#<%=lnkG.ClientID %>').val('Verify');
                $('#<%=txtTIN.ClientID %>').removeAttr("disabled");
                $('#<%=txtAddress1.ClientID %>').val('');
                return false;
            }
        }

        function fncToastInvalidResponse() {
            ShowPopupMessageBox("GST Portal is Not Connected.Please try After.");
            return false;
        }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'VendorMaster');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "VendorMaster";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

        function fncKeyEnter(evt, value) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    if (value == 'Vendor') {
                        $('#<%=txtAddress1.ClientID %>').focus();
                         return false;
                     }
                     else if (value == 'Hold') {
                         $('#<%=txtCRDays.ClientID %>').select();
                         return false;
                     }
                     else if (value == 'CRL') {
                         $('#<%=txtCRDays.ClientID %>').select();
                         return false;
                     }
                     else if (value == 'CRD') {
                         $('#<%=lnkSave.ClientID %>').focus();
                        return false;
                    }
                }
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function showAll() {

            $("#divsave").dialog({           //musaraf 21112022
                dialogClass: "no-close",
                title: "Enterpriser Web",
                height: 160,
                width: 400,
                modal: true,
                buttons: {
                    "OK": function () {

                        $("#<%=btnsave.ClientID %>").click();

                    }
                    //Cancel: function () {
                    //    $(this).dialog("close");
                    //}
                }
            }).html("Vendor Saved Successfully..!")
         }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Purchase</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Vendor Master </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" class ="EnableScroll">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="top-container">
                        <div class="left-top-container-vm">
                            <div class="left-top-container-header-vm">
                                Vendor Infomation
                            </div>
                            <div class="left-top-container-detail-vm">
                                <div class="col-md-12" style="margin-left: -2%">
                                    <div class="col-md-5">
                                        <div class="col-md-6">
                                            <asp:Label ID="Label1" runat="server" Text="Vendor Code"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtVendorCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="col-md-4">
                                            <asp:Label ID="Label3" runat="server" Text="Vendor Name"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtVendorName" Style="width: 100%" onkeydown="return fncKeyEnter(event,'Vendor');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                        <div class="col-md-2" runat="server" id="divEdit">
                                            <asp:Button ID="btnEdit" runat="server" CssClass="button-blue" OnClientClick="fncClear();return false;" Text="Edit"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label12" runat="server" Text="Address1"></asp:Label><span class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label18" runat="server" Text="Address2"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAddress2" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" MaxLength="50" Text="City"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text="State"></asp:Label>
                                        </div>
                                        <div class="label-right" id="divStateddl">
                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>
                                            <%--OnSelectedIndexChanged="ddlState_SelectedIndexChanged"--%>
                                        </div>
                                        <div class="label-right" id="divState">
                                            <asp:TextBox ID="txtState" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text="Zip Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtZipCode" MaxLength="20" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text="Phone No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPhoneNo" MaxLength="30" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label2" runat="server" Text="Mobile No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="10" onkeydown="return isNumberKeyWithDecimalNew(event)" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">

                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="Country"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCountry" runat="server" MaxLength="50" CssClass="form-control-res" onfocusout="return fncCountryValidate();"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none;">
                                        <div class="label-left">
                                            <asp:Label ID="Label7" runat="server" Text="Fax"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFax" MaxLength="30" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label4" runat="server" Text="E-Mail"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtEmail" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="URL"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtURL" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text="Purchase Limit"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPurchaseLimit" MaxLength="9" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text="Attn"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtAttn" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label36" runat="server" Text="Purchase Profit"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtpurchaseProfit1" runat="server" CssClass="form-control-res" Style="background-color: #95f595;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="display: list-item; visibility: hidden;">
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPruchaseProfit2" runat="server" CssClass="form-control-res" Style="background-color: #95f595;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="display: list-item; visibility: hidden;">
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPurchaseProfit3" runat="server" CssClass="form-control-res" Style="background-color: #95f595;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label37" runat="server" Text="Selling Margin"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSmargin1" runat="server" CssClass="form-control-res" Style="background-color: #e08d8d;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="display: list-item; visibility: hidden;"></div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSMargin2" runat="server" CssClass="form-control-res" Style="background-color: #e08d8d;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="display: list-item; visibility: hidden;"></div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtSmargin3" runat="server" CssClass="form-control-res" Style="background-color: #e08d8d;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label6" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label50" runat="server" Text="PO Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtPORemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label51" runat="server" Text="Cheque Print Name"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtChequePrintName" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label52" runat="server" Text="Bank Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:TextBox ID="txtBankCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlBankCode" runat="server" Width="100%"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none">
                                        <div class="label-left">
                                            <asp:Label ID="Label53" runat="server" Text="Bank Branch"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBankBranch" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label54" runat="server" Text="Bank AC No"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBankAcNo" runat="server" MaxLength="40" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label21" runat="server" Text="IFSC Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtIFSCcode" runat="server" MaxLength="11" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none">
                                        <div class="label-left">
                                            <asp:Label ID="Label20" runat="server" Text="Days"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:ListBox ID="lstdays" runat="server" CssClass="form-control-res" SelectionMode="Multiple">
                                                <asp:ListItem Text="Sunday" Value="1" />
                                                <asp:ListItem Text="Monday" Value="2" />
                                                <asp:ListItem Text="Tuesday" Value="3" />
                                                <asp:ListItem Text="Wednesday" Value="4" />
                                                <asp:ListItem Text="Thursday" Value="5" />
                                                <asp:ListItem Text="Friday" Value="6" />
                                                <asp:ListItem Text="Saturday" Value="7" />
                                            </asp:ListBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:CheckBox ID="chkTIN" runat="server" Visible="false" />
                                            GSTTIN
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTIN" runat="server" Style="text-transform: uppercase" MaxLength="15" CssClass="form-control-res" onfocusout="return fncSetText();"></asp:TextBox>
                                            <asp:Button ID="lnkG" runat="server" Style="margin-left: 260px; margin-top: -25px; width: 35%;" OnClientClick="return fncClearClick();"
                                                Text="Verify" class="button-green" OnClick="lnkVerify_Click"></asp:Button><%-- OnClientClick="fncVerify();return false;"--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none;">
                                        <div class="label-left">
                                            <asp:CheckBox ID="chkCST" runat="server" />
                                            CST
                                        </div>
                                        <div class="label-right" style="width: 32%">
                                            <asp:TextBox ID="txtCST" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                        <div class="label-right" style="width: 5%; margin-left: 1%">
                                            %
                                        </div>
                                        <div class="label-right" style="width: 20%; margin-left: 1%">
                                            <asp:TextBox ID="txtCSTPRC" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split" id="divAgent" runat="server">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label22" runat="server" Text="Agent Code"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAgentCode" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Agent',  'txtAgentCode', '');" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label23" runat="server" Text="Agent Name"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtAgentName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:CheckBox ID="chkAllowPayment" runat="server" />
                                            Allow Payment in Pos
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div style="float: right;">
                                            <asp:CheckBox ID="chkActive" runat="server" Checked="true" />
                                            Active
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="right-top-container-vm">
                            <div class="right-top-container-header-vm">
                                GST Option
                            </div>
                            <div class="right-top-container-detail-vm">
                                <div class="control-group-split">
                                    <div class="label-left">
                                        <asp:RadioButtonList ID="rdoGstType" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="IntraState" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="InterState" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Import - RCM" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Zero-GST" Value="4"></asp:ListItem>
                                        </asp:RadioButtonList>

                                    </div>
                                    <div class="control-group-left">
                                        <div class="label-left" style="display: none;">
                                            <asp:RadioButton ID="rbtFaxImmediate" runat="server" GroupName="Fax" />
                                            Immediate
                                        </div>
                                        <div class="label-right" style="display: none;">
                                            <asp:RadioButton ID="rbtFaxSchedule" runat="server" GroupName="Fax" />
                                            Schedule
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="display: none;">
                                        <div class="label-left">
                                            <asp:RadioButton ID="rbtFaxInterState" runat="server" GroupName="State" />
                                            InterState
                                        </div>
                                        <div class="label-right" style="display: none;">
                                            <asp:RadioButton ID="rbtFaxOtherState" runat="server" GroupName="State" />
                                            Other State
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-top-container-vm" style="display: none;">
                            <div class="right-top-container-header-vm">
                                Email Option
                            </div>
                            <div class="right-top-container-detail-vm">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:RadioButton ID="rbtEmailImmediate" runat="server" GroupName="EmailOption" />
                                            Immediate
                                        </div>
                                        <div class="label-right">
                                            <asp:RadioButton ID="rbtEmailSchedule" runat="server" GroupName="EmailOption" />
                                            Schedule
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-top-container-vm">
                            <div class="right-top-container-header-vm">
                                Payment Option
                            </div>
                            <div class="right-top-container-detail-vm">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:RadioButton ID="rbtCheque" runat="server" GroupName="Payment" />
                                            Cheque
                                        </div>
                                        <div class="label-right">
                                            <asp:RadioButton ID="rbtLocalCheque" runat="server" GroupName="Payment" />
                                            Local Cheque
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:RadioButton ID="rbtCash" runat="server" GroupName="Payment" />
                                            Cash
                                        </div>
                                        <div class="label-right">
                                            <asp:RadioButton ID="rbtRTGS" runat="server" GroupName="Payment" />
                                            DD / RTGS
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-top-container-vm" style="padding: 10px">
                            <div class="control-group-single">
                                <div class="label-left" style="width: 5%">
                                    <asp:CheckBox ID="chkHoldBills" runat="server" />
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <asp:Label ID="Label15" runat="server" Text="Hold Bills"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 15%; margin-left: 3%">
                                    <asp:TextBox ID="txtHoldBills" runat="server" Text="0" MaxLength="20" Style="text-align: right"
                                        CssClass="form-control-res" onkeydown="return fncKeyEnter(event,'Hold');" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single">
                                <div class="label-left" style="width: 5%">
                                    <asp:CheckBox ID="chkCRLimit" runat="server" />
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <asp:Label ID="Label16" runat="server" Text="Cr.Limit (Rs.)"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 15%; margin-left: 3%">
                                    <asp:TextBox ID="txtCRLimit" runat="server" Text="0" MaxLength="9" Style="text-align: right"
                                        CssClass="form-control-res" onkeydown="return fncKeyEnter(event,'CRL');" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single">
                                <div class="label-left" style="width: 5%">
                                    <asp:CheckBox ID="chkCRDays" runat="server" Style="visibility: hidden;" />
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <asp:Label ID="Label17" runat="server" Text="Cr.Days"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 15%; margin-left: 3%;">
                                    <asp:TextBox ID="txtCRDays" runat="server" Text="0" MaxLength="9" Style="text-align: right"
                                        CssClass="form-control-res" onkeydown="return fncKeyEnter(event,'CRD');" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single">
                                <div class="label-left" style="width: 5%">
                                    <asp:CheckBox ID="chkStopPayment" runat="server" />
                                </div>
                                <div class="label-right" style="width: 20%;">
                                    <asp:Label ID="Label38" runat="server" Text="Stop Payment"></asp:Label>
                                 </div>
                                     <div class="control-group-single display_none" id="divStopReasones" style="width:100%;">
                                <asp:TextBox ID="txtStopReasones" runat="server" TextMode="MultiLine"
                                    CssClass="form-control-res"></asp:TextBox>
                            

                                </div>
                                <div class="label-left" style="width: 5%">
                                    <asp:CheckBox ID="chkStopPuchase" runat="server" />
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <asp:Label ID="Label39" runat="server" Text="Stop Purchase"></asp:Label>
                                </div>
                            </div>
                            <div class="control-group-single display_none" id="divStopReason">
                                <asp:TextBox ID="txtStopReason" runat="server" TextMode="MultiLine"
                                    CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="right-top-container-vm" style="padding: 3px; display: none;">
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblSunday" runat="server" Text="Sunday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocsunday" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblMonday" runat="server" Text="Monday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocMon" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblTuesday" runat="server" Text="Tuesday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocTue" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblWednesday" runat="server" Text="Wednesday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocWed" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblThursday" runat="server" Text="Thursday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocThu" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblFriday" runat="server" Text="Friday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocFri" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-split">
                                <div class="control-group-left">
                                    <div class="label-left">
                                        <asp:Label ID="lblSaturday" runat="server" Text="Saturday"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:ListBox ID="lstLocSat" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="right-top-container-vm" style="padding: 3px;">
                            <div class="col-md-4">
                                <div class="col-md-10">
                                    <asp:Label ID="Label29" runat="server" Text="Sunday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkSunday" runat="server"></asp:CheckBox>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label30" runat="server" Text="Monday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <div class="label-right">
                                        <asp:CheckBox ID="chkMonday" runat="server"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label31" runat="server" Text="Tuesday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <div class="label-right">
                                        <asp:CheckBox ID="chkTus" runat="server"></asp:CheckBox>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label32" runat="server" Text="Wednesday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkWed" runat="server"></asp:CheckBox>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label33" runat="server" Text="Thursday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkThurs" runat="server"></asp:CheckBox>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label34" runat="server" Text="Friday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkFriday" runat="server"></asp:CheckBox>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="Label35" runat="server" Text="Saturday"></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:CheckBox ID="chkSat" runat="server"></asp:CheckBox>
                                </div>
                            </div>
                            <div class="col-md-8" style="display: none;">
                                <div class="col-md-4">
                                    <asp:Label ID="lblGrnremarks" runat="server" Text="GRN Remarks"></asp:Label>
                                </div>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtGRNRemarks" runat="server" TextMode="MultiLine" Width="100%" Height="100px"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-red"
                            PostBackUrl="~/Masters/frmVendorView.aspx">View</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                            OnClientClick="return ValidateForm()">Save(F4)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red"
                            OnClientClick="clearForm();return false;">Clear(F6)</asp:LinkButton>
                        <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Style="visibility: hidden"
                            OnClick="lnkSearchGrid_Click" Text='<%$ Resources:LabelCaption,btn_Search %>'>
                            <i class="icon-play"></i></asp:LinkButton>
                        <div id="divsave" style="display:none">
                            <asp:Button ID="btnsave" runat="server" style="display:none"  OnClick="btnsave_Click" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="HiddenCountry" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenUser" runat="server" Value="" />
        <asp:HiddenField ID="hidState" runat="server" />
        <asp:HiddenField ID="hidValue" runat="server" Value="" />
        <asp:HiddenField ID="hidStateCodeCompany" runat="server" Value="" />
        <asp:HiddenField ID="hidVendorname" runat="server" />
        <div id="divGstinVerify" style="display: none" title="Enterpriser Web">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <asp:Label ID="Label24" runat="server" Text="GSTIN No"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtVerifyGstIn" runat="server" MaxLength="15" onkeydown="fncGstNoVerify(event);return false;" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="col-md-12 GSTINPopup">
                    <div class="col-md-4">
                        <asp:Label ID="Label25" runat="server" Text="Register Name"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtRegName" runat="server" MaxLength="30" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="col-md-12 GSTINPopup">
                    <div class="col-md-4">
                        <asp:Label ID="Label26" runat="server" Text="Business Name"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtBusName" runat="server" ReadOnly="true" MaxLength="30" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="col-md-12 GSTINPopup">
                    <div class="col-md-4">
                        <asp:Label ID="Label27" runat="server" Text="Address"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtGstinAddress" runat="server" ReadOnly="true" TextMode="MultiLine" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="col-md-12 GSTINPopup">
                    <div class="col-md-4">
                        <asp:Label ID="Label28" runat="server" Text="Status"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <asp:TextBox ID="txtBustype" runat="server" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
            </div>
        </div>
        <div class="display_none">
            <asp:HiddenField ID="hidGSTStatus" runat="server" Value="0" />
            <asp:HiddenField ID="hidEditEnable" runat="server" Value="0" />
        </div>
    </div>
</asp:Content>
