﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmMain.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        
        .loc_MixDet td:nth-child(1), .loc_MixDet th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .loc_MixDet td:nth-child(2), .loc_MixDet th:nth-child(2) {
            min-width: 200px;
            max-width: 200px;
            text-align: left !important;
        }

        .loc_MixDet td:nth-child(3), .loc_MixDet th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }
         .loc_MixDet{
             padding:2px;
         }
        .loc_Det td:nth-child(1), .loc_Det th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .loc_Det td:nth-child(2), .loc_Det th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .loc_Det td:nth-child(3), .loc_Det th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .loc_Det td:nth-child(3) {
            text-align: right !important;
        }

        .loc_Det1 td:nth-child(2), .loc_Det1 th:nth-child(2) {
            min-width: 49px;
            max-width: 49px;
        }

        .loc_Det1 td:nth-child(3), .loc_Det1 th:nth-child(3) {
            min-width: 332px;
            max-width: 332px;
        }

        .loc_Det1 td:nth-child(4), .loc_Det1 th:nth-child(4) {
            min-width: 94px;
            max-width: 94px;
        }

        .loc_Det1 td:nth-child(5), .loc_Det1 th:nth-child(5) {
            min-width: 33px;
            max-width: 33px;
        }

        .loc_Det1 td:nth-child(6), .loc_Det1 th:nth-child(6) {
            min-width: 33px;
            max-width: 33px;
        }

        .loc_Det1 td:nth-child(7), .loc_Det1 th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }
    </style>
    <link href="../css/dashboard/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/jsCommon.js" type="text/javascript"></script>
    <link href="../css/dashboard/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/dashboard/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <script src="../js/app.min.js" type="text/javascript"></script>

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        #display {
            background: transparent;
            height: 50px;
            width: 500px;
            overflow: hidden;
            position: relative;
        }

        #text {
            background: transparent;
            cursor: pointer;
            overflow: hidden;
            position: absolute;
            width: auto;
            left: 10px;
            margin-right: 10px;
        }

        .dropdown-menu > li > a {
            color: black;
            font-family: Roboto,sans-serif;
        }

        .main-nav > li > a {
            font-family: Roboto,sans-serif;
        }

        .container-fluid {
            padding-right: 5px;
            padding-left: 5px;
        }

        body {
            overflow-x: auto;
            overflow-y: auto;
        }

        .div-left {
            float: left;
            padding-left: 10px;
        }

        .div-right {
            float: right;
            padding-right: 10px;
        }

        body {
            overflow-x: hidden;
        }

        #tree-table {
            /*font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;*/
            border-collapse: collapse;
            width: 100%;
        }

            #tree-table td, #customers th {
                border: 1px solid BLACK;
                padding: 5px;
            }

            #tree-table tr:nth-child(even) {
                background-color: #f2f2f2;
                cursor: pointer;
            }

            #tree-table tr:hover {
                background-color: #ddd;
                cursor: pointer;
            }

            #tree-table th {
                /*padding-top: 12px;
                padding-bottom: 12px;*/
                text-align: center;
                background-color: #00a65a;
                color: white;
            }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Main');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Main";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">

        var locwiseDet, showDialog = "";

        function pageLoad() {


            try {
                if ($('#<%=hidLocationcode.ClientID %>').val() != "HQ") {
                    $("#salesmore_Infor").css("display", "none");
                    $("#purchasemore_Infor").css("display", "none");
                    $("#Stockmore_Infor").css("display", "none");
                }
                if ($('#<%=hidparamMinMax.ClientID %>').val() != "Y") {
                    $("#divMinMaxQty").css("display", "none");
                }
                else {
                    $("#divMinMaxQty").css("display", "block");
                }
                if ($('#<%=hidNonMovingItemsDisplay.ClientID %>').val() != "Y") {
                    $("#divMovingItems").css("display", "none");
                }
                else {
                    $("#divMovingItems").css("display", "block");
                    fncGetdispalynonbatchdetail();
                    $('#tblTrnsInghList').find('tr').click(function () {
                        $("#locwisenonMovingitems").dialog({
                            resizable: false,
                            height: 500,
                            width: "50%",
                            modal: true,
                            title: "Non Moving items",
                            appendTo: 'form:first',
                            autoOpen: false
                        });
                        $("#locwisenonMovingitems").dialog('open');


                    });
                }
            }
            catch (err) {
                err.Message;
            }
        }
        $.date = function (dateObject) {



            return date;
        };

        function fncGetdispalynonbatchdetail() {
            var Content;
            $.ajax({
                type: "POST",
                url: "frmMain.aspx/Getdispalynonbatchdetail",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var objData = jQuery.parseJSON(response.d);
                    $('#NonMovingitemsCount').html(objData.length);
                    for (var i = 0; i < objData.length; i++) {
                         
                        Content += "<tr>"
                        //background-color: #00c0ef !important;
                       + "<td style='width: 5px;'></td>"
                       + "<td data-column='name'>" + objData[i]["Code"] + "</td>"
                       + "<td>" + objData[i]["Description"] + "</td>"
                      // + "<td>" + date + "</td>"
                      // + "<td >" + objData[i]["Qty"] + "</td>"
                      // + "<td >" + objData[i]["Expired"] + "</td>"
                       // + "<td >" + objData[i]["Batch"] + "</td>"
                       + "<td >" + objData[i]["PurDate"] + "</td>"
                       + "</tr>";
                        // }
                    }
                    $('#mainPagePlaceholder').html(Content);

                    $('[id*=MovingItems_Refresh]').hide();
                },
                error: function (response) {
                    console.log(response);
                },
                failure: function (response) {
                    console.log(response);
                }
            });
        }

        function fncUpdatePanel() {
            <%=ClientScript.GetPostBackEventReference(upDashBoard, "")%>;
        }
        function Update_UpdatePaanel() {
            document.getElementById('<%= BtnUpdate.ClientID %>').click();
        }

        function Hide() {
            if ($('#<%=HiddenChartDashbord.ClientID %>').val() != "Y") {
                $("#footerview").hide();
                $("#ContentPlaceHolder1_dashboard1").hide();
                $("#ContentPlaceHolder1_dashboard2").hide();
            }
        }

        function Show() {
            if ($('#<%=HiddenChartDashbord.ClientID %>').val() != "Y") {
                $("#footerview").show();
                $("#ContentPlaceHolder1_dashboard1").show();
                $("#ContentPlaceHolder1_dashboard2").show();
            }
        }
        function ShowMulti() {
            if ($('#<%=HiddenChartDashbord.ClientID %>').val() != "Y") {
                $("#footerview").show();
                $("#ContentPlaceHolder1_dashboard1").show();
            }
        }

        var TrnsOutListtbody;
        //tbody Initialize
        $(document).ready(function () {

            TrnsOutListtbody = $("#tblTrnsOutList tbody");
            TrnsInListtbody = $("#tblTrnsInList tbody");
            fncShowLocwiseDet();
            fncGetSummaryRecord();

        });

        function fncGetSummaryRecord() {
            try {

                Update_UpdatePaanel();
                $('[id*=Sales_Refresh]').show();
                $('[id*=Purchase_Refresh]').show();
                $('[id*=Stock_Refresh]').show();
                $('[id*=SalesRet_Refresh]').show();
                $('[id*=TransferIn_Refresh]').show();
                $('[id*=TransferOut_Refresh]').show();
                $('[id*=MovingItems_Refresh]').show();
                $.ajax({
                    type: "POST",
                    url: "frmMain.aspx/GetMainRecord",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        var objvendor = jQuery.parseJSON(msg.d);
                        locwiseDet = objvendor;
                        if (objvendor.Table.length > 0) {


                            $('#<%=lbl_SalesAmt.ClientID %>').text(Number(objvendor.Table[0]["TotalSales"]).toFixed(2));
                            if ($('#<%=lbl_SalesAmt.ClientID %>').text().length > 15) {
                                //$('#<%=lbl_SalesAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table[0]["TotalSales"]).toFixed(2) + "</span></marquee>");
                                $('#<%=lbl_SalesAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + fncCommaSeperatorIndianFormatFloat((objvendor.Table[0]["TotalSales"]).toFixed(2)) + "</span></marquee>");
                            } else {
                                //$('#<%=lbl_SalesAmt.ClientID %>').text(Number(objvendor.Table[0]["TotalSales"]).toFixed(2));
                                $('#<%=lbl_SalesAmt.ClientID %>').text(fncCommaSeperatorIndianFormatFloat((objvendor.Table[0]["TotalSales"]).toFixed(2)));
                            }

                        }
                        if (objvendor.Table1.length > 0) {

                            $('#<%=lbl_PurchaseAmt.ClientID %>').text(Number(objvendor.Table1[0]["PurTotal"]).toFixed(2));

                            if ($('#<%=lbl_PurchaseAmt.ClientID %>').text().length > 15) {
                                //$('#<%=lbl_PurchaseAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table1[0]["PurTotal"]).toFixed(2) + "</span></marquee>");
                                $('#<%=lbl_PurchaseAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + fncCommaSeperatorIndianFormatFloat((objvendor.Table1[0]["PurTotal"]).toFixed(2)) + "</span></marquee>");
                            } else {
                                //$('#<%=lbl_PurchaseAmt.ClientID %>').text(Number(objvendor.Table1[0]["PurTotal"]).toFixed(2));
                                $('#<%=lbl_PurchaseAmt.ClientID %>').text(fncCommaSeperatorIndianFormatFloat((objvendor.Table1[0]["PurTotal"]).toFixed(2)));
                            }

                        }
                        if (objvendor.Table2.length > 0) {

                            $('#<%=lbl_StockAmt.ClientID %>').text(Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2));

                            //console.log($('#<%=lbl_StockAmt.ClientID %>').text().length);
                            if ($('#<%=lbl_StockAmt.ClientID %>').text().length > 15) {
                                //$('#<%=lbl_StockAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2) + "</span></marquee>");
                                $('#<%=lbl_StockAmt.ClientID %>').html("<marquee onmouseover='this.stop();' onmouseout='this.start();'><span>" + fncCommaSeperatorIndianFormatFloat((objvendor.Table2[0]["totalinvamt"]).toFixed(2)) + "</span></marquee>");
                            } else {

                                //$('#<%=lbl_StockAmt.ClientID %>').text(Number(objvendor.Table2[0]["totalinvamt"]).toFixed(2));
                                $('#<%=lbl_StockAmt.ClientID %>').text(fncCommaSeperatorIndianFormatFloat((objvendor.Table2[0]["totalinvamt"]).toFixed(2)));
                            }


                        }

                        $('[id*=Sales_Refresh]').hide();
                        $('[id*=Purchase_Refresh]').hide();
                        $('[id*=Stock_Refresh]').hide();
                        $('[id*=MovingItems_Refresh]').hide();
                       <%-- // $('[id*=SalesRet_Refresh]').hide();--%>



                        if (objvendor.Table4.length > 0) {
                            TrnsOutListtbody.children().remove();
                            for (var i = 0; i < objvendor.Table4.length; i++) {
                                TrnsOutListtbody.append(
                                "<tr></td><td id='tdTransferNo_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["TransferNo"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["Date"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["FromLocation"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["ToLocation"]
                                + "</td><td  id='tdToLocationStatus_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table4[i]["FromLocationStatus"]
                                + "</td></tr>");
                            }

                        }
                        if (objvendor.Table5.length > 0) {
                            TrnsInListtbody.children().remove();
                            for (var i = 0; i < objvendor.Table5.length; i++) {
                                TrnsInListtbody.append(
                                "<tr></td><td id='tdTransferNo_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["TransferNo"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["Date"]
                                + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["FromLocation"]
                                 + "</td><td style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["ToLocation"]
                                + "</td><td  id='tdToLocationStatus_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table5[i]["ToLocationStatus"]
                                + "</td></tr>");
                            }

                        }
                        if ($('#<%=hidparamMinMax.ClientID %>').val() == "Y") {
                            if (objvendor.Table9.length > 0) {
                                var tblMinQty = $("#tblMinQty tbody");
                                for (var i = 0; i < objvendor.Table9.length; i++) {
                                    tblMinQty.append(
                                    "<tr title='Double Click'><td id='tdLocationCode_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table9[i]["LocationCode"]
                                    + "</td><td  id='tdCount_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table9[i]["Count"]
                                    + "</td></tr>");
                                }

                                tblMinQty.children().dblclick(fncMinQty);
                            }
                            if (objvendor.Table10.length > 0) {
                                var tblMaxQty = $("#tblMaxQty tbody");
                                for (var i = 0; i < objvendor.Table10.length; i++) {
                                    tblMaxQty.append(
                                    "<tr title='Double Click'><td id='tdLocationCodeMax_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table10[i]["LocationCode"]
                                    + "</td><td  id='tdCountMax_" + i + "' style=padding:5px;border-color:initial;>" + objvendor.Table10[i]["Count"]
                                    + "</td></tr>");
                                }

                                tblMaxQty.children().dblclick(fncMaxQty);
                            }
                            $('[id*=MinMaxQty_Refresh]').hide();
                        }
                        else {
                            if (objvendor.Table9.length > 0) {
                                if (parseFloat(objvendor.Table9[0]["Count"]) > 0) {
                                    $('#<%=lbl_StockAmt.ClientID %>').css('text-decoration', 'line-through');
                                    $('#pStock').text("Stock MisMatch -Please Contact Unipro");
                                }
                            }
                        }
                        if (objvendor.Table11.length > 0) {
                            if (parseFloat(objvendor.Table11[0]["Count"]) > 0) {
                                $('#<%=lbl_StockAmt.ClientID %>').css('text-decoration', 'line-through');
                                $('#pStock').text("Stock MisMatch -Please Contact Unipro");

                            }
                        }


                        $('[id*=TransferIn_Refresh]').hide();
                        $('[id*=TransferOut_Refresh]').hide();

                        if ($("#locationwiseDet").dialog('isOpen') === true) {
                            $("#locationwiseDet").dialog('close');
                            fncBindLocationDet(showDialog);
                        }
                        if ($('#<%=hidNonMovingItemsDisplay.ClientID %>').val() == "Y") {
                            fncGetdispalynonbatchdetail();
                        }

                        setTimeout('fncGetSummaryRecord()', 60000);
                    },
                    error: function (data) {
                        // alert('Something Went Wrong')
                        setTimeout('fncGetSummaryRecord()', 60000);
                    }
                });

            }
            catch (err) {
                //alert(err.Message);
            }
        }

        ///New Vendor Creation
        function fncShowLocwiseDet() {
            try {
                $("#locationwiseDet").dialog({
                    resizable: false,
                    height: 320,
                    width: "auto",
                    modal: true,
                    title: "Location Detail",
                    appendTo: 'form:first',
                    autoOpen: false
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        function fncBindLocationDet(value) {
            var tblLocDet, row;
            var curObj;
            try {
                showDialog = value;
                tblLocDet = $("#tblLocationwiseDetail tbody");


                tblLocDet.children().remove();
                if (value == "Sales") {
                    curObj = locwiseDet.Table6;
                }
                else if (value == "Purchase") {
                    curObj = locwiseDet.Table7;
                }
                else {
                    if ($('#pStock').text() == "Stock MisMatch -Please Contact Unipro") {
                        ShowPopupMessageBox("Stock Value is Ivalid");
                        return false;
                    } else {
                        curObj = locwiseDet.Table8;
                    }
                }

                if (curObj.length > 0) {
                    var totalSum = 0;
                    for (var i = 0; i < curObj.length; i++) {
                        totalSum = parseFloat(totalSum) + parseFloat(curObj[i]["value"].toFixed(2));
                        row = "<tr>"
                           + "<td >" + curObj[i]["RowNo"] + "</td>"
                           + "<td  >" + curObj[i]["locationcode"] + "</td>"
                           + "<td >" + fncCommaSeperatorIndianFormatFloat(curObj[i]["value"].toFixed(2))
                           + "</tr>";
                        tblLocDet.append(row);
                    }
                    $("#tblLocationwiseDetail [id*=lblTotalsum]").text(fncCommaSeperatorIndianFormatFloat(totalSum.toFixed(2)));
                    //fncShowLocwiseDet();
                    $("#locationwiseDet").dialog('open');
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncMinQty() {
            try {
                var Location = $(this).find('td').eq(0).html();
                var Count = $(this).find('td').eq(1).html();
                fncBindMinMaxDetail(Location, 'Min');
                $('#<%=h3Location.ClientID %>').text('Min Qty -' + $.trim(Location));
                $('#<%=hidMinMax.ClientID %>').val('Min Qty');
                $('#<%=hidMixLoc.ClientID %>').val(Location);
                fncMixMaxAlert();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncMaxQty() {
            try {
                var Location = $(this).find('td').eq(0).html();
                var Count = $(this).find('td').eq(1).html();
                fncBindMinMaxDetail(Location, 'Max');
                $('#<%=h3Location.ClientID %>').text('Max Qty -' + $.trim(Location));
                $('#<%=hidMinMax.ClientID %>').val('Max Qty');
                $('#<%=hidMixLoc.ClientID %>').val(Location);
                fncMixMaxAlert();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncMixMaxAlert() {
            $("#MinMax").dialog({
                resizable: false,
                height: 400,
                width: "auto",
                modal: true,
                title: "Min-Max Detail",
                appendTo: 'form:first',
                autoOpen: false
            });
            $("#MinMax").dialog('open');
        }
        function fncBindMinMaxDetail(Loc, msg) {
            try {
                $.ajax({
                    url: "frmMain.aspx/GetMinMaxDetail",
                    data: "{ 'Loc': '" + Loc + "','Mode': '" + msg + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        var objItem = jQuery.parseJSON(msg.d);
                        var tblMinmaxDetail = $("#tblMinmaxDetail tbody");
                        tblMinmaxDetail.children().remove();
                        var total = 0;
                        var row;
                        if (objItem.length > 0) {
                            for (var j = 0; j < objItem.length; j++) {
                                total = parseFloat(total) + parseFloat($.trim(objItem[j]["Count"]));
                                row = "<tr><td id='tdRowNo_" + j + "'>" +
                                 $.trim(objItem[j]["RowNo"]) + "</td><td  id='tdCategory_" + j + "'>" +
                                 $.trim(objItem[j]["Category"]) + "</td><td  id='tdCount_" + j + "'>" +
                                 $.trim(objItem[j]["Count"]) + "</td></tr>";
                                tblMinmaxDetail.append(row);
                            }
                            $("#tblMinmaxDetail [id*=lblSumCount]").text(fncCommaSeperatorIndianFormatFloat(total.toFixed(2)));
                        }
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
       
        <div class="breadcrumbs">
            <ul> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= fncGetUrl(); return false;"></i></li>
                </ul></div>
        <div class="main-image">
            <div class="col-md-4">
                <h4 class="page-header">Today's Status
                </h4>
            </div>
            <div class="col-md-4">
                <center>
                    <h4 class="page-header" id="Location" runat="server"></h4>
                </center>
            </div>
            <div class="col-md-4">
                <h4 class="page-header  div-right" id="version" runat="server"></h4>
            </div>

            <%--<br />
            <div>
                <asp:Label ID="lbl_Today" runat="server" Font-Bold="true">Today's Status</asp:Label>
            </div>
            <br />--%>
            <!-- Small boxes (Stat box) -->
            <div id="dashboard3" runat="server" style="display: none">
                <iframe src="http://localhost:250/Index.html" sandbox="allow-same-origin allow-scripts allow-popups allow-forms" onload="this.width=screen.width;this.height=screen.height;" frameborder="0">Sorry your browser does not support inline frames.
                </iframe>
            </div>

            <div id="dashboard1" runat="server" style="display: none">
                <div class="row">
                    <div class="col-lg-4 col-xs-4">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>
                                    <asp:Label ID="lbl_SalesAmt" runat="server"><span id="bSales" style="display:block;">0.00</span></asp:Label></h3>
                                <p>
                                    Sales
                                </p>
                            </div>
                            <div class="icon" style="padding-top: 10px;">
                                <i class="ion ion-bag"></i>
                            </div>
                            <div id="Sales_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div id="salesmore_Infor" class="dash_board">
                                <a href="#" class="small-box-footer dash_board" onclick="fncBindLocationDet('Sales');">More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-4">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>
                                    <asp:Label ID="lbl_PurchaseAmt" runat="server"><span id="bPur" style="display:block;">0.00</span></asp:Label></h3>
                                <p>
                                    Purchase
                                </p>
                            </div>
                            <div class="icon" style="padding-top: 10px;">
                                <i class="ion ion-ios-cart-outline"></i>
                            </div>
                            <div id="Purchase_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div id="purchasemore_Infor" class="dash_board">
                                <a href="#" class="small-box-footer dash_board" onclick="fncBindLocationDet('Purchase');">More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-4">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <div id="display" style="width: auto">
                                    <h3 id="text">
                                        <asp:Label ID="lbl_StockAmt" runat="server"><span id="bStock" style="display:block">0.00</span></asp:Label></h3>
                                </div>
                                <p id="pStock">
                                    Stock
                                </p>
                            </div>
                            <div class="icon" style="padding-top: 10px;">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <div id="Stock_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                                <i class="fa fa-refresh fa-spin"></i>
                            </div>
                            <div id="Stockmore_Infor" class="dash_board">
                                <a href="#" class="small-box-footer dash_board" onclick="fncBindLocationDet('stock');">More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- ./col -->
                    <%-- <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>
                                <asp:Label ID="lbl_SalesRetAmt" runat="server"><span id="bSalRet" style="display:block;">0.00</span></asp:Label></h3>
                            <p>
                                Sales Return</p>
                        </div>
                        <div class="icon" style="padding-top: 10px;">
                            <i class="ion-arrow-return-left"></i>
                        </div>
                        <div id="SalesRet_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>--%>
                    <!-- ./col -->
                </div>
                <asp:UpdatePanel ID="upDashBoard" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="row">
                            <asp:Repeater runat="server" ID="rptDashBoard">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="col-lg-2 col-xs-2">
                                        <div class="box box-solid <%# Eval("Color")%>">
                                            <div class="box-header">
                                                <h3 class="box-title">
                                                    <%# Eval("Title")%>
                                                </h3>
                                            </div>
                                            <div class="box-body">
                                                <h3>
                                                    <span id="<%# Eval("idtext")%>" style="display: block">
                                                        <%# Eval("Value")%>
                                                    </span>
                                                </h3>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div style="display: none">
                            <asp:Button ID="BtnUpdate" runat="server" OnClick="lnkUpdate_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <!-- /.row -->
        <div id="dashboard2" runat="server" style="display: none">
            <div class="row">
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-blue">
                        <div id="TransferOut_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px; z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">Transfer Out</h3>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 100px; width: auto">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblTrnsOutList" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 34%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Transfer No
                                                </th>
                                                <th scope="col" style="width: 30%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Transfer Date
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">From
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">To
                                                </th>
                                                <th scope="col" style="width: 35%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Status
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-purple">
                        <div id="TransferIn_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px; z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">Transfer In</h3>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 100px; width: auto">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblTrnsInList" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 34%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Transfer No
                                                </th>
                                                <th scope="col" style="width: 30%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Transfer Date
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">From
                                                </th>
                                                <th scope="col" style="border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">To
                                                </th>
                                                <th scope="col" style="width: 35%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Status
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-aqua" id="divMovingItems">
                        <div id="MovingItems_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px; z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <h3 class="box-title">Non Moving Items</h3>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 100px; width: auto">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblTrnsInghList" cellspacing="0" style="cursor: pointer;" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 2%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Detail
                                                </th>
                                                <th scope="col" style="width: 1%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Count
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style='padding: 5px; border-color: initial; text-align: center;'>No Of Non Moving items</td>
                                                <td style='padding: 5px; border-color: initial; text-align: center;' id="NonMovingitemsCount">0</td>
                                            </tr>

                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-4">
                    <!-- Success box -->
                    <div class="box box-solid bg-aqua" id="divMinMaxQty">
                        <div id="MinMaxQty_Refresh" class="overlay" style="padding-left: 5px; padding-bottom: 2px; z-index: 0;">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <div class="box-header">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3 class="box-title">Min Qty</h3>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="box-title">Max Qty</h3>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row" style="overflow: auto; height: 100px; width: auto">
                                <div class="col-xs-6 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblMinQty" cellspacing="0" style="cursor: pointer;" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 2%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Location
                                                </th>
                                                <th scope="col" style="width: 1%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Count
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="col-xs-6 fw_light m_bottom_45 m_xs_bottom_30">
                                    <table id="tblMaxQty" cellspacing="0" style="cursor: pointer;" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 2%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Location
                                                </th>
                                                <th scope="col" style="width: 1%; border-color: initial; border-width: 2px; text-align: center; font-weight: bold;">Count
                                                </th>
                                            </tr>

                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hidLocationcode" runat="server" Value="" />
        <asp:HiddenField ID="hidNonMovingItemsDisplay" runat="server" Value="" />
        <asp:HiddenField ID="HiddenChartDashbord" runat="server" Value="" />
        <asp:HiddenField ID="HiddenUserMaster" runat="server" Value="" />
        <div id="locationwiseDet" class="Payment_fixed_headers loc_Det">
            <table id="tblLocationwiseDetail" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">Location
                        </th>
                        <th scope="col">Value
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr class="Paymentfooter_Color">
                        <td class="paymentfooter_total">Total:
                        </td>
                        <td class="paymentfooter_total"></td>
                        <td class="paymentfooter_total">
                            <asp:Label ID="lblTotalsum" runat="server" />
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div id="locwisenonMovingitems" style="overflow-y: hidden;" class="Payment_fixed_headers loc_Det1">

            <table cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th></th>
                        <th scope="col">Code
                        </th>
                        <th scope="col">Description
                        </th>
                        <th scope="col">PurDate
                        </th>
                        <%-- <th scope="col">Qty
                        </th>
                        <th scope="col">Age
                        </th>
                        <th scope="col">Batch
                        </th>--%>
                    </tr>
                </thead>


            </table>
            <div class="Payment_fixed_headers loc_Det1">
                <table cellspacing="0" rules="all" border="1">
                    <tbody id="mainPagePlaceholder" style="height: 375px">
                    </tbody>
                </table>
            </div>


        </div>
        <div id="MinMax" class="Payment_fixed_headers loc_MixDet">
            <div>
                <asp:Label ID="h3Location" runat="server" Style="font-weight: bold; font-size: 15px" />
            </div>
            <table id="tblMinmaxDetail" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">Category
                        </th>
                        <th scope="col">Item Count
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr class="Paymentfooter_Color">
                        <td class="paymentfooter_total">Total:
                        </td>
                        <td class="paymentfooter_total"></td>
                        <td class="paymentfooter_total">
                            <asp:Label ID="lblSumCount" runat="server" />
                        </td>
                    </tr>
                </tfoot>
            </table>
            <div style="margin-top: 5px;">
                <asp:LinkButton ID="btnDetail" Text="Detail" runat="server" class="button-blue" OnClick="lnkDetail_Click" Style="float: right;" />
            </div>
            <asp:HiddenField ID="hidMinMax" runat="server" />
            <asp:HiddenField ID="hidMixLoc" runat="server" />
            <asp:HiddenField ID="hidparamMinMax" runat="server" />
        </div>
    </div>
</asp:Content>
