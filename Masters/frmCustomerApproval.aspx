﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCustomerApproval.aspx.cs" Inherits="EnterpriserWebFinal.Masters.frmCustomerApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 30px;
            max-width: 30px;
            text-align: center;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 30px;
            max-width: 30px;
            text-align: center;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 60px;
            max-width: 60px;
            text-align: left;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 60px;
            max-width: 60px;
            text-align: left;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
            padding-left: 5px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 50px;
            max-width: 50px;
            text-align: left;
            padding-left: 5px;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


       .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));

            $("[id*=txtCustomerCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmCustomerApproval.aspx/GetCustomerDetails") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtCustomerCode]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtCustomerCode]").val(i.item.val);
                    return false;
                },
            });

            if ($("#<%=chkDate.ClientID%>").is(':checked')) { // dinesh
                $("[id*=txtFromDate]").removeAttr("disabled");
                $("[id*=txtToDate]").removeAttr("disabled");
            }
            else {
                $("[id*=txtFromDate]").attr("disabled", "disabled");
                $("[id*=txtToDate]").attr("disabled", "disabled");
            }

            $('#<%=chkDate.ClientID%>').click(function () {
                if (this.checked) {
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });


            $(document).ready(function () {
                $(document).on('keydown', disableFunctionKeys);
            });
            function disableFunctionKeys(e) {
                try {
                    if (e.keyCode == 116) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkLoad', '');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 119) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 117) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                        e.preventDefault();
                        return;
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.message)
                }
            }
        }
        function SetDefaultDate(FromDate, Todate) {
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $('#<%=ddMode.ClientID %>').val('');
            $('#<%=ddMode.ClientID %>').trigger("liszt:updated");
        }
        function fncLoadValidation() {
            var Bydatevalue = $("#<%=chkDate.ClientID%>").is(':checked');
            if (Bydatevalue) {
                var day, month, year;
                var frmDate = $('#<%=txtFromDate.ClientID %>').val();
                var strarray = frmDate.split('-');
                day = strarray[0];
                month = strarray[1];
                year = strarray[2];
                frmDate = year + "-" + month + "-" + day;
                var toDate = $('#<%=txtToDate.ClientID %>').val();
                var strarray = toDate.split('-');
                day = strarray[0];
                month = strarray[1];
                year = strarray[2];
                toDate = year + "-" + month + "-" + day;
                if ($('#<%= txtFromDate.ClientID %>').val() == "") {
                    $('#<%= txtFromDate.ClientID %>').select();
                    fncToastInformation('<%=Resources.LabelCaption.Alert_FromDate%>');
                    return false;
                }
                else if ($('#<%= txtToDate.ClientID %>').val() == "") {
                    $('#<%= txtToDate.ClientID %>').select();
                    fncToastInformation('<%=Resources.LabelCaption.Alert_ToDate%>');
                    return false;
                }
                else if (new Date(frmDate) > new Date(toDate)) {
                    fncToastInformation('<%=Resources.LabelCaption.alert_fromdategreaterthencurDate%>');
                        return false;
                    }
        }
    }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'CustomerApproval');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "CustomerApproval";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Approval</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price" style="padding-top: 15px; padding-left: 15px">
            <div class="col-md-12">
                <div class="col-md-2">
                    <div class="col-md-3 label-left">
                        <asp:Label ID="lblCustomerCode" runat="server" Text="Customer"></asp:Label>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-8 label-right">
                        <asp:TextBox ID="txtCustomerCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-1"></div>
                    <div class="col-md-3 label-left">
                        <asp:Label ID="lblFromDate" runat="server" Text="FromDate"></asp:Label>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6 label-right">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-1"></div>
                    <div class="col-md-3 label-left">
                        <asp:Label ID="lblToDate" runat="server" Text="ToDate"></asp:Label>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6 label-right">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-1" style="padding-left: 15px">
                    <asp:CheckBox ID="chkDate" runat="server" Text="By Date" Font-Bold="True" />
                </div>
                <div class="col-md-2">
                    <div class="col-md-2 label-left">
                        <asp:Label ID="lblMode" runat="server" Font-Bold="True" Text="Mode"></asp:Label>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-8 label-right">
                        <asp:DropDownList ID="ddMode" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-2">
                    <asp:LinkButton ID="lnkLoad" runat="server" class="button-blue"
                        OnClientClick="return fncLoadValidation();" OnClick="lnkLoad_Click"
                        Text="Load (F5)"></asp:LinkButton>
                </div>
            </div>
            <div id="tbl" style="padding-top: 45px">
                <table rules="all" border="1" id="tblHead" runat="server" class="grdLoad fixed_header pshro_GridDgn">
                    <tr>
                        <td style="display: none">Select</td>
                        <td>Active</td>
                        <td>Customer Code</td>
                        <td>Customer Name</td>
                        <td>Customer Mobile</td>
                        <td>Customer Address</td>
                        <td>PinCode</td>
                    </tr>
                </table>
            </div>
            <div class="grid-overflow-Invchange" id="divGrid" runat="server">
                <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <div id="grd">
                            <asp:GridView ID="grdCustomerDetails" runat="server" AutoGenerateColumns="False" PageSize="14"
                                ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn grdLoad" ShowHeader="false">
                                <%--OnRowDataBound="grdCustomerDetails_RowDataBound"--%>
                                <PagerStyle CssClass="pshro_text" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hidden">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSelect" DataField="Select" Text="Select" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" DataField="Select" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblActive" DataField="Active" Text="Active" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActive" DataField="Active" runat="server" Checked='<%# Eval("Active") %>'
                                                AutoPostBack="true" OnCheckedChanged="chkActive_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="SS" HeaderText="Test" ItemStyle-CssClass="text-left"></asp:BoundField>--%>
                                    <asp:BoundField DataField="CustomerCode" HeaderText="Customer Code" ItemStyle-CssClass="text-left"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ItemStyle-CssClass="text-left"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerMobile" HeaderText="Customer Mobile" ItemStyle-CssClass="text-left"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerAddress" HeaderText="Customer Address" ItemStyle-CssClass="text-left"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerPinCode" HeaderText="PinCode" ItemStyle-CssClass="text-left"></asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="lblNoRecord" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    PageButtonCount="5" Position="Bottom" />
                                <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <%--<div class="col-md-12"></div>--%>
            <div class="col-md-12" style="padding-top: 20px">
                <asp:UpdatePanel ID="UpdatePanelSave" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="col-md-6"></div>
                        <div class="col-md-1 label-right">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_Click" Text="Save (F8)">
                            </asp:LinkButton>
                        </div>
                        <div class="col-md-1 label-right">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text="Clear (F6)"
                                OnClientClick="clearForm()" OnClick="lnkClear_Click"></asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
