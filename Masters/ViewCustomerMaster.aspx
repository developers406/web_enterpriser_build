﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="ViewCustomerMaster.aspx.cs" Inherits="EnterpriserWebFinal.Masters.ViewCustomerMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
        .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function fncClickYearEnd() {
            $("#dialog-confirm").dialog("open");
            return false;
        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            $(function () {
                $("#dialog-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            $("#dialog-YearEnd").dialog("open");
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });


            $(function () {
                $("#dialog-YearEnd").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 350,
                    modal: true,
                    buttons: {
                        "Ok": function () {
                            __doPostBack('<%= lnkOk.ClientID %>', '')
                            //                            $('#<%= lnkOk.ClientID %>').click();
                        },
                        Cancel: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'ViewCustomerMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "ViewCustomerMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Customer</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="gridDetails">
                    <div class="grid-search">
                        <div class="grid-search-left">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" OnClick="lnkSearchGrid_Click"
                                    OnClientClick="return Validate()">Search</asp:LinkButton>
                                <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Width="100px" OnClick="lnkNew_Click"><i class="icon-play"></i>New</asp:LinkButton>
                            </asp:Panel>
                        </div>
                        <div class="grid-search-right">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkYearEnd" runat="server" class="button-blue" Text="Year End Process"
                                    OnClientClick="return fncClickYearEnd()"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkChangeMember" runat="server" class="button-blue" Text="Change Member By Invoice"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                            <asp:GridView ID="gvCustomer" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                CssClass="pshro_GridDgn" oncopy="return false" AllowPaging="True" PageSize="18"
                                OnPageIndexChanging="gvCustomer_PageIndexChanging" DataKeyNames="CUST_CODE">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CUST_CODE" HeaderText="Customer Code"></asp:BoundField>
                                    <asp:BoundField DataField="CUST_NAME" HeaderText="Customer Name"></asp:BoundField>
                                    <asp:BoundField DataField="Address" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="CUST_MOBILE" HeaderText="Mobile No"></asp:BoundField>
                                    <asp:BoundField DataField="CUST_LOYALTY_CARD_NO" HeaderText="Loyality Card No"></asp:BoundField>
                                    <asp:BoundField DataField="CUST_STATUS" HeaderText="Status"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div id="dialog-confirm" title="Enterpriser">
                    <p>
                        <span class="ui-icon ui-icon-ShowPopupMessageBox" style="float: left; margin: 1px 12px 20px 0;">
                        </span>Do You want Proceed Year End Process ?</p>
                </div>
                <div id="dialog-YearEnd" title="Enterpriser">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text="Enter Year"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtYear" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <asp:LinkButton ID="lnkOk" runat="server" class="button-blue" Text="Ok" OnClick="lnkOk_Click"></asp:LinkButton>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
</asp:Content>
