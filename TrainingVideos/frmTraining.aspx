﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTraining.aspx.cs" Inherits="EnterpriserWebFinal.TrainingVideos.frmTraining" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl(val) {
            fncSaveHelpVideoDetailmenu('', '', val);
        }
        function fncOpenvideo(get) {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = get;
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();

            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

        function ValidateForm() {
            var val;
            var vSales = $("[id*=DivSalesReport]").is(':visible');
            if (vSales) {
                var checkedvalue = $("#<%=rdoMaster.ClientID%>").find(":checked").val();
                if (checkedvalue == "CompanyMaster") {
                    val = "CompanyMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "LocationMaster") {
                    val = "LocationMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "TerminalView") {
                    val = "TerminalView";
                    fncGetUrl(val);
                }
                if (checkedvalue == "ReasonMaster") {
                    val = "ReasonMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "DepartmentMaster") {
                    val = "DepartmentMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "categoryMaster") {
                    val = "categoryMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SubCategory") {
                    val = "SubCategory";
                    fncGetUrl(val);
                }
                if (checkedvalue == "BrandMaster") {
                    val = "BrandMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Floor") {
                    val = "Floor";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Class") {
                    val = "Class";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SubClass") {
                    val = "SubClass";
                    fncGetUrl(val);
                }
                if (checkedvalue == "ItemType") {
                    val = "ItemType";
                    fncGetUrl(val);
                }
                if (checkedvalue == "TaxMasterGST") {
                    val = "TaxMasterGST";
                    fncGetUrl(val);
                }
                if (checkedvalue == "UOM") {
                    val = "UOM";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Section") {
                    val = "Section";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Bin") {
                    val = "Bin";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Shelf") {
                    val = "Shelf";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Warehouse") {
                    val = "Warehouse";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Size") {
                    val = "Size";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Style") {
                    val = "Style";
                    fncGetUrl(val);
                }
                if (checkedvalue == "BaseUOM") {
                    val = "BaseUOM";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Addition_Deduction") {
                    val = "Addition_Deduction";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Zone") {
                    val = "Zone";
                    fncGetUrl(val);
                }
                if (checkedvalue == "CompanySetting") {
                    val = "CompanySetting";
                    fncGetUrl(val);
                }
                if (checkedvalue == "StockAdjustment") {
                    val = "StockAdjustment";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Merchandise") {
                    val = "Merchandise";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Manufacturer") {
                    val = "Manufacturer";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Origin") {
                    val = "Origin";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Transport") {
                    val = "Transport";
                    fncGetUrl(val);
                }
                if (checkedvalue == "MemberType") {
                    val = "MemberType";
                    fncGetUrl(val);
                }
                if (checkedvalue == "CustomerMaster") {
                    val = "CustomerMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "LoyaltyMasterSetup") {
                    val = "LoyaltyMasterSetup";
                    fncGetUrl(val);
                }
                if (checkedvalue == "LoyaltyTerm") {
                    val = "LoyaltyTerm";
                    fncGetUrl(val);
                }
                if (checkedvalue == "VendorView") {
                    val = "VendorView";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Agent") {
                    val = "Agent";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Term") {
                    val = "Term";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Display&Contract") {
                    val = "Display&Contract";
                    fncGetUrl(val);
                }
                if (checkedvalue == "BankMaster") {
                    val = "BankMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SalesManMaster") {
                    val = "SalesManMaster";
                    fncGetUrl(val);
                }
                if (checkedvalue == "DeliveryMan") {
                    val = "DeliveryMan";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SalesManCommissionBrand") {
                    val = "SalesManCommissionBrand";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SalesManCommissionItemWise") {
                    val = "SalesManCommissionItemWise";
                    fncGetUrl(val);
                }
                if (checkedvalue == "SalesManCommissionItemView") {
                    val = "SalesManCommissionItemView";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Voucher") {
                    val = "Voucher";
                    fncGetUrl(val);
                }
                if (checkedvalue == "VoucherRedemption") {
                    val = "VoucherRedemption";
                    fncGetUrl(val);
                }
                if (checkedvalue == "DiscountCoupon") {
                    val = "DiscountCoupon";
                    fncGetUrl(val);
                }
                if (checkedvalue == "PosPayment") {
                    val = "PosPayment";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Cashier") {
                    val = "Cashier";
                    fncGetUrl(val);
                }
                if (checkedvalue == "PayMode") {
                    val = "PayMode";
                    fncGetUrl(val);
                }
                if (checkedvalue == "BarcodePrinter") {
                    val = "BarcodePrinter";
                    fncGetUrl(val);
                }
                if (checkedvalue == "Picker") {
                    val = "Picker";
                    fncGetUrl(val);
                }
               


            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li id="ReportTitle" runat="server" class="active-page">Training Videos </li>
                
            </ul>
        </div>
         <div id="Tabs" role="tabpanel">
                 <ul id="tablist" class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liInventory"><a href="#Inventory" aria-controls="Inventory" role="tab" data-toggle="tab" onclick="return fncInv()">Master</a></li>
                        <li id="liCategory"><a href="#Category" aria-controls="Category" role="tab" data-toggle="tab" onclick="return fncCat()">Inventory</a></li>
                           <li id="lishop"><a href="#Shop" aria-controls="Shop" role="tab" data-toggle="tab" onclick="return fncShop()">Merchandising</a></li>
                     <li id="liCompany"><a href="#Company" aria-controls="Company" role="tab" data-toggle="tab" onclick="return fncCompany()">Purchase</a></li>
                    </ul>
                  <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                        <div class="tab-pane active" role="tabpanel" id="Inventory">
                            <div id="divMainContainer_Inventory">
         <div class="ziehharmonika EnableScroll">
            <div style="overflow: visible; display: block;" runat="server" id="DivSalesReport">
                
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoMaster" runat="server" Class="radioboxlist">
                                <asp:ListItem Value="CompanyMaster">Company</asp:ListItem>
                                <asp:ListItem Value="LocationMaster">Location</asp:ListItem>
                                <asp:ListItem Value="ReasonMaster">Reason Master</asp:ListItem>
                                <asp:ListItem Value="TerminalView">Terminal Master</asp:ListItem>
                                <asp:ListItem Value="DepartmentMaster">Department</asp:ListItem>
                                <asp:ListItem Value="categoryMaster">Category</asp:ListItem>
                                <asp:ListItem Value="SubCategory">Sub Category</asp:ListItem>
                                <asp:ListItem Value="BrandMaster">Brand</asp:ListItem>
                                <asp:ListItem Value="Floor">Floor</asp:ListItem>
                                <asp:ListItem Value="Class">Class</asp:ListItem>
                                <asp:ListItem Value="SubClass">Sub Class</asp:ListItem>
                                <asp:ListItem Value="ItemType">Item type</asp:ListItem>
                                <asp:ListItem Value="TaxMasterGST">Tax</asp:ListItem>
                                <asp:ListItem Value="UOM">UOM</asp:ListItem>
                                <asp:ListItem Value="Section">Section</asp:ListItem>
                                <asp:ListItem Value="Bin">Bin</asp:ListItem>
                                <asp:ListItem Value="Shelf">Shelf</asp:ListItem>
                                <asp:ListItem Value="Warehouse">Warehouse</asp:ListItem>
                                <asp:ListItem Value="Size">Size</asp:ListItem>
                                <asp:ListItem Value="Style">Style</asp:ListItem>
                                <asp:ListItem Value="BaseUOM">UOM Conversion</asp:ListItem>
                                <asp:ListItem Value="Addition_Deduction">Deduction/Addition</asp:ListItem>
                                <asp:ListItem Value="Zone">Zone</asp:ListItem>
                                <asp:ListItem Value="CompanySetting">Company Settings</asp:ListItem>
                                <asp:ListItem Value="StockAdjustment">StockAdjustment Type</asp:ListItem>
                                <asp:ListItem Value="Merchandise">Merchandise</asp:ListItem>
                                <asp:ListItem Value="Manufacturer">Manufacture</asp:ListItem>
                                <asp:ListItem Value="Origin">Origin</asp:ListItem>
                                <asp:ListItem Value="Transport">Transport Master</asp:ListItem>
                                <asp:ListItem Value="MemberType">Customer Type</asp:ListItem>
                                <asp:ListItem Value="CustomerMaster">CustomerLoyality Master</asp:ListItem>
                                <asp:ListItem Value="LoyaltyMasterSetup">LoyalitySetup Master</asp:ListItem>
                                <asp:ListItem Value="VendorView">Vendor Master</asp:ListItem>
                                <asp:ListItem Value="Agent">Agent</asp:ListItem>
                                <asp:ListItem Value="Term">Term</asp:ListItem>
                                <asp:ListItem Value="Display&Contract">Display</asp:ListItem>
                                <asp:ListItem Value="BankMaster">Bank</asp:ListItem>
                                <asp:ListItem Value="SalesManMaster">SalesMan Master</asp:ListItem>
                                <asp:ListItem Value="DeliveryMan">Delivery Man</asp:ListItem>
                                <asp:ListItem Value="SalesManCommissionBrand">BrandWise SalesCommission</asp:ListItem>
                                <asp:ListItem Value="SalesManCommissionItemWise">ItemWise Commission</asp:ListItem>
                                <asp:ListItem Value="SalesManCommissionItemView">SalesMan Commission</asp:ListItem>
                                <asp:ListItem Value="Voucher">Voucher</asp:ListItem>
                                <asp:ListItem Value="VoucherRedemption">Voucher redemption</asp:ListItem>
                                <asp:ListItem Value="DiscountCoupon">Discount Coupon</asp:ListItem>
                                <asp:ListItem Value="PosPayment">POS Payment</asp:ListItem>
                                <asp:ListItem Value="Cashier">Cashier</asp:ListItem>
                                <asp:ListItem Value="PayMode">PayMode</asp:ListItem>
                                <asp:ListItem Value="BarcodePrinter">Barcode Printer</asp:ListItem>
                                <asp:ListItem Value="Picker">Picker Master</asp:ListItem>
                                
                            </asp:RadioButtonList>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                        <ContentTemplate>
                            <div style="float: right; margin-top: 20px">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="btn btn-danger" Width="100px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" OnClientClick="return showFilter()">Filter</asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" 
                                    OnClientClick="return ValidateForm()">Load Report</asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" OnClientClick=" return Clear()">Clear</asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                </div>
   </div>
             </div></div></div></div></div>
         
</asp:Content>
