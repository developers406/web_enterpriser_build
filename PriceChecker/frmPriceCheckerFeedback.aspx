﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPriceCheckerFeedback.aspx.cs" Inherits="EnterpriserWebFinal.PriceChecker.frmPriceCheckerFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

    </style>
    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "-1");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
        
           <li class="active-page">FeedbackReport </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
     <div class="container-group-small">
        <div class="container-control">                
            
            
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
            </div>
           
        </div>
          <div class="button-contol">
               <div class="control-button">
                            <asp:LinkButton ID="lnkLoad" runat="server" class="button-red" OnClick="lnkLoad_Click" ><i class="icon-play"></i>Load</asp:LinkButton>
                        </div>
                       
              </div>

         </div>

     <div class="container-group-top">
                    <div class="col-md-12" style="overflow: auto; height: 550px;">
                        <div id="divgvPickedItemDetails" >
                            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn" ShowHeaderWhenEmpty="true"   AllowPaging="false" >
                               
                                <EmptyDataTemplate>
                                    <asp:Label ID="Lbl2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="SNo" runat="server" Text="SNo"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="SNo" runat="server" Text='<%# Eval("SNo") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="heDescription" runat="server" Text="Description"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Description"   Width ="100"  runat="server" Text='<%# Eval("description") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Customer" runat="server" Text="Customer"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Customer" runat="server" Text='<%# Eval("customer") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Mobile" runat="server" Text="MobileNo"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Mobile" runat="server" Text='<%# Eval("phone") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="rating" runat="server" Text="Rating"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="rating" runat="server" Text='<%# Eval("rating") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Date" runat="server" Text="Date"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Date" runat="server" Text='<%# Eval("Date") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                  
                                      
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
</asp:Content>
