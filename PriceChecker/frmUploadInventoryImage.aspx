﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmUploadInventoryImage.aspx.cs" Inherits="EnterpriserWebFinal.PriceChecker.frmUploadInventoryImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
     <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="http://dev.jquery.com/view/trunk/plugins/autocomplete/jquery.autocomplete.js"></script>
    <script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--%>
     <script type="text/javascript">
         function pageLoad() {
             if ($("#<%=hidInvfield.ClientID %>").val() == "I") {
                 $('#liCategory').removeClass('active');
                 $('#Category').removeClass('active');
                 $('#liInventory').addClass('active');
                 $('#Inventory').addClass('active');
                 $('#lishop').removeClass('active');
                 $('#Shop').removeClass('active');
                 $('#liCompany').removeClass('active');
                 $('#Company').removeClass('active');
                 $("#divMainContainer_Category").hide();
                 $("#divMainContainer_Shop").hide();
                 $("#divMainContainer_Company").hide();
                 $("#divMainContainer_Inventory").show();
             }
             if ($("#<%=hidcatfield.ClientID %>").val() == "C") {
                 $('#liCategory').addClass('active');
                 $('#Category').addClass('active');
                 $('#liInventory').removeClass('active');
                 $('#Inventory').removeClass('active');
                 $('#lishop').removeClass('active');
                 $('#Shop').removeClass('active');
                 $('#liCompany').removeClass('active');
                 $('#Company').removeClass('active');
                 $("#divMainContainer_Category").show();
                 $("#divMainContainer_Shop").hide();
                 $("#divMainContainer_Company").hide();
                 $("#divMainContainer_Inventory").hide();
             }
         }
         function fncSetValue() {
             try {
                 if (SearchTableName == "Inventory") {

                     $("#<%=txtInventory.ClientID %>").val(Code);



                 }
                 else if (SearchTableName == "Department") {

                     $('#<%=txtdepartment.ClientID %>').val(Code);

                 }
                 else if (SearchTableName == "Brand") {
                     $('#<%=txtbrand.ClientID %>').val(Code);
                 }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
         }
         function clearForm() {
         
             $("#<%=txtInventory.ClientID %>").val("");
             $('#<%=txtbrand.ClientID %>').val("");
             $('#<%=txtdepartment.ClientID %>').val("");
             return false;
         }
         function fncInv() {
             $('#liCategory').removeClass('active');
             $('#Category').removeClass('active');
             $('#liInventory').addClass('active');
             $('#Inventory').addClass('active');
             $('#lishop').removeClass('active');
             $('#Shop').removeClass('active');
             $('#liCompany').removeClass('active');
             $('#Company').removeClass('active');
             $("#divMainContainer_Category").hide();
             $("#divMainContainer_Shop").hide();
             $("#divMainContainer_Company").hide();
             $("#divMainContainer_Inventory").show();
         }
         function fncCat() {
             $('#liCategory').addClass('active');
             $('#Category').addClass('active');
             $('#liInventory').removeClass('active');
             $('#Inventory').removeClass('active');
             $('#lishop').removeClass('active');
             $('#Shop').removeClass('active');
             $('#liCompany').removeClass('active');
             $('#Company').removeClass('active');
             $("#divMainContainer_Inventory").hide();
             $("#divMainContainer_Shop").hide();
             $("#divMainContainer_Company").hide();
             $("#divMainContainer_Category").show();
             
         }
         function fncShop() {
             $('#liInventory').removeClass('active');
             $('#Inventory').removeClass('active');
             $('#liCategory').removeClass('active');
             $('#Category').removeClass('active');
             $('#lishop').addClass('active');
             $('#Shop').addClass('active');
             $('#liCompany').removeClass('active');
             $('#Company').removeClass('active');
             $("#divMainContainer_Category").hide();
             $("#divMainContainer_Inventory").hide();
             $("#divMainContainer_Company").hide();
             $("#divMainContainer_Shop").show();
         }
         function fncCompany() {
             $('#liInventory').removeClass('active');
             $('#Inventory').removeClass('active');
             $('#liCategory').removeClass('active');
             $('#Category').removeClass('active');
             $('#lishop').removeClass('active');
             $('#Shop').removeClass('active');
             $('#liCompany').addClass('active');
             $('#Company').addClass('active');
             $("#divMainContainer_Category").hide();
             $("#divMainContainer_Inventory").hide();
             $("#divMainContainer_Company").show();
             $("#divMainContainer_Shop").hide();
         }
         function fncSearchvalidate() {
             if ($("#<%=txtInventory.ClientID %>").val() == "" && $("#<%=txtdepartment.ClientID %>").val() == "" && $("#<%=txtbrand.ClientID %>").val() == "")
             {
                 alert("Choose any one Filteration");
                 return false;
             }
         }
         function fncToastInformation(msg) {
             try {

                 ShowPopupMessageBox(msg);
                 return false;
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
        
           <li class="active-page">InventoryImage </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>

             <div id="Tabs" role="tabpanel">
                 <ul id="tablist" class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liInventory"><a href="#Inventory" aria-controls="Inventory" role="tab" data-toggle="tab" onclick="return fncInv()">Inventory</a></li>
                        <li id="liCategory"><a href="#Category" aria-controls="Category" role="tab" data-toggle="tab" onclick="return fncCat()">Category</a></li>
                           <li id="lishop"><a href="#Shop" aria-controls="Shop" role="tab" data-toggle="tab" onclick="return fncShop()">ShopLogo</a></li>
                     <li id="liCompany"><a href="#Company" aria-controls="Company" role="tab" data-toggle="tab" onclick="return fncCompany()">Company</a></li>
                    </ul>
                  <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                        <div class="tab-pane active" role="tabpanel" id="Inventory">
                            <div id="divMainContainer_Inventory">
      <div class="container-group-small">
                    <div class="container-control">
     <div class="control-group-single-res">
                  <div class="label-left">
                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="ItemCode/Desc"></asp:Label>
                                                    </div>
                      <div class="label-right">
                        <asp:TextBox ID="txtInventory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory', 'txtInventory', 'txtBarcode');"></asp:TextBox>
                             
                                                    </div>
                                                </div>
                        <div class="control-group-single-res">
                  <div class="label-left">
                        <asp:Label ID="lblBrand" runat="server" Font-Bold="True" Text="Brand"></asp:Label>
                                                    </div>
                      <div class="label-right">
                        <asp:TextBox ID="txtbrand" runat="server" CssClass="form-control-res" onKeydown="return fncShowSearchDialogCommon(event,'Brand','txtbrand','');" ></asp:TextBox>
                             
                                                    </div>
                                                </div>
    <div class="control-group-single-res" >
        <div class="label-left">
            <asp:label ID="lbldept" runat="server" Font-Bold="true" Text="Department"></asp:label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtdepartment" runat="server" CssClass="form-control-res" onKeydown="return fncShowSearchDialogCommon(event,'Department','txtdepartment','');"></asp:TextBox>
        </div>
            
    </div>
                        <div class="control-group-single-res">
            
                                              <div style="display:none" >
                                        <asp:CheckBox ID="chkNoImage" runat="server" Text="with Image" Checked="true" />
                                    </div>
<%--                                            <div>
                                                <asp:FileUpload ID="fu1" runat="server" />
                                            </div>--%>

                        </div>
         <div class="button-contol">
             <div class="control-button">
                 <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-warning waves-effect" Text="Search" OnClientClick="return fncSearchvalidate();" OnClick="btnSearch_Click"  />
                 </div>
                      <div class="control-button">
                            <asp:Button ID="btnUploadCloud"  Visible ="true"  runat="server" Text="Upload" OnClick="btnUploadCloud_Click" CssClass="btn btn-success waves-effect" /></div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                       
                    </div>
                </div>
          </div>
                            
                                

                <div class="container-group-top">
                    <div class="col-md-12" style="overflow: auto; height: 550px;">
                        <div id="divgvPickedItemDetails" >
                            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn" ShowHeaderWhenEmpty="true"   AllowPaging="false" OnRowDataBound="gv1_RowDataBound" OnRowUpdating="gv1_RowUpdating">
                               
                                <EmptyDataTemplate>
                                    <asp:Label ID="Lbl2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="InventoryCode" runat="server" Text="InventoryCode"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="InventoryCode" runat="server" Text='<%# Eval("InventoryCode") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="heDescription" runat="server" Text="Description"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Description"   Width ="100"  runat="server" Text='<%# Eval("Description") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="heDepartmentCode" runat="server" Text="Department"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="DepartmentCode" runat="server" Text='<%# Eval("Department") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                     <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="heBrand" runat="server" Text="Brand"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Brand" runat="server" Text='<%# Eval("BrandCode") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="hlblimg" runat="server" Text="Image"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div>
                                                <asp:Image ID="img1" runat="server" ImageUrl='<%# Eval("ImgPath") %>'
                                                    Height="100px" Width="100px" />
                                            </div>
                                            <div>
                                                <asp:FileUpload ID="fu11" runat="server" />
                                            </div>

                                            </div> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <ItemTemplate>
                                            <asp:Button ID="btnupdate" runat="server" CssClass="btn btn-primary waves-effect" Text="Update" CommandName="Update" />

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                                </div></div></div>
                        <div class="tab-pane" role="tabpanel" id="Category">
                            <div id="divMainContainer_Category">
                                <div class="container-group-small">
                    <div class="container-control">
                         <div class="control-group-single-res">
        <div class="label-left">
            <asp:label ID="Label2" runat="server" Font-Bold="true" Text="Category"></asp:label>
        </div>
        <div class="label-right">
            <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onKeydown="return fncShowSearchDialogCommon(event,'Category','txtCategory','');"></asp:TextBox>
        </div>
            
    </div>
                         <div class="control-group-single-res">
            
                                              <div style="display:none" >
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="with Image" Checked="true"/>
                                    </div>
<%--                                            <div>
                                                <asp:FileUpload ID="fu1" runat="server" />
                                            </div>--%>

                        </div>

                        <div class="button-contol">
                                         <div class="control-button">
                 <asp:Button ID="btnSearch_cat" runat="server" CssClass="btn btn-warning waves-effect" Text="Search" onclick="btnSearchCat_Click" />
                 </div>
                      <div class="control-button">
                            <asp:Button ID="btnupload_cat"  Visible ="true"  runat="server" Text="Upload" OnClick="btnUpload_category_Click" CssClass="btn btn-success waves-effect" /></div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkclear_Cat" runat="server" class="button-red" OnClientClick="return clearForm()"><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                       
                    </div>
                    </div></div>

                                
                <div class="container-group-top">
                    <div class="col-md-12" style="overflow: auto; height: 550px;">
                        <div id="divgvPickedItemDetailss" >
                            <asp:GridView ID="gv2" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn" ShowHeaderWhenEmpty="true"   AllowPaging="false" OnRowDataBound="gv2_RowDataBound" OnRowUpdating="gv2_RowUpdating">
                               
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Category" runat="server" Text="Category"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Category" runat="server" Text='<%# Eval("Category") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                  
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Label12" runat="server" Text="Image"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div>
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("ImgPath") %>'
                                                    Height="100px" Width="100px" />
                                            </div>
                                            <div>
                                                <asp:FileUpload ID="fu12" runat="server" />
                                            </div>

                                            </div> 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                      
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="hdrow" />
                                        <ItemTemplate>
                                            <asp:Button ID="Button3" runat="server" CssClass="btn btn-primary waves-effect" Text="Update" CommandName="Update" />

                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                                </div></div>
                  <div class="tab-pane" role="tabpanel" id="Shop">
                            <div id="divMainContainer_Shop">
                                 <div class="control-group-single-res">
                                         <div class="container-group-small">
                                            <div class="container-control">
                                             
                                            <div>
                                                <asp:FileUpload ID="fu4" runat="server" />
                                            </div>
                                                <div>
                                       <asp:Image ID="ImageShop" runat="server" 
                                                    Height="100px" Width="100px" />
                                                </div>
                                            </div>
                                                <div class="button-contol">
                      <div class="control-button">
                            <asp:Button ID="Button1"  Visible ="true"  runat="server" Text="Upload" OnClick="btnUpload_shop_Click" CssClass="btn btn-success waves-effect" /></div>
                        
                       
                    </div>
                                         </div></div>
                                </div></div>

                    <div class="tab-pane" role="tabpanel" id="Company">
                            <div id="divMainContainer_Company">
                                 <div class="control-group-single-res">
                                    <div class="container-group-small">
                                            <div class="container-control">
                                             
                                            <div>
                                                <asp:FileUpload ID="fu3" runat="server" />
                                            </div>
                                                 <div>
                                       <asp:Image ID="ImageCom" runat="server" 
                                                    Height="100px" Width="100px" />
                                                </div>
                                                   <div class="button-contol">
                      <div class="control-button">
                            <asp:Button ID="Button2"  Visible ="true"  runat="server" Text="Upload" OnClick="btnUpload_company_Click" CssClass="btn btn-success waves-effect" /></div>
                        
                       
                    </div>
                                            </div></div></div>
                                </div></div>
                            <div class="hiddencol">
                                <asp:HiddenField ID="hidInvfield" runat="server" Value="" />
                                <asp:HiddenField ID="hidcatfield" runat="server" Value="" />
                            </div>


</asp:Content>
