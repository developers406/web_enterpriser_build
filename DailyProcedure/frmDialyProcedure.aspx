﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmDialyProcedure.aspx.cs" Inherits="EnterpriserWebFinal.DailyProcedure.frmDialyProcedure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .body {
            background: #F5F5F5;
        }
    </style>
    <script type="text/javascript">

        /////Page Load
        //function pageLoad() {
        //    try
        //    {
        //        //fncHeaderCheckandUnCheck();
        //    }
        //    catch (err)
        //    {
        //        ShowPopupMessageBox(err.message);
        //    }
        //}


        $(function () {
            ///Master Check and UnCheck
            $("[id*=cbLocation]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbCategory]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbbrand]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbVendor]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbClass]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbBank]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbVoucherGroup]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbMember]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbWarehouse]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbStyle]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbPaymode]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbMemberdet]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbOthers]").bind("click", function () {
                fncSelectMaster();
            });
            $("[id*=cbInventory]").bind("click", function () {
                fncSelectMaster();
            });

            //TransactionDownload Check and UnCheck
            $("[id*=cbPurchaseOrder]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbSO]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbDistribution]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbFV]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbBreakQty]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbOutletPurchase]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbFI]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbProEvents]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbSPecialPrice]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            $("[id*=cbBonusPoint]").bind("click", function () {
                fncTransactionDownloadSelect();
            });
            ///Purchase Upload
            $("[id*=cbuPurchaseOrder]").bind("click", function () {
                fncPurchaseUploadSelect();
            });
            $("[id*=cbGA]").bind("click", function () {
                fncPurchaseUploadSelect();
            });
            $("[id*=cbGID]").bind("click", function () {
                fncPurchaseUploadSelect();
            });
            $("[id*=cbPurchaseReturn]").bind("click", function () {
                fncPurchaseUploadSelect();
            });
            $("[id*=cbAPTransaction]").bind("click", function () {
                fncPurchaseUploadSelect();
            });
            ///Sales Upload
            $("[id*=cbSalesHistory]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbCreditSales]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbSalesReFund]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbSalesReturn]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbSOSatus]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbOutletSales]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbUVDen]").bind("click", function () {
                fncSalesUploadSelect();
            });
            $("[id*=cbVouSalesUpdate]").bind("click", function () {
                fncSalesUploadSelect();
            });
            ///Transaction Upload
            $("[id*=cbStock]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbPosPayments]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbTranlog]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbMemnerTran]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbAudit]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbUItemTransfer]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbUDistribution]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbNewBatchCreation]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbNewBarcodeCreation]").bind("click", function () {
                fncTransactionUploadSelect();
            });
            $("[id*=cbFVU]").bind("click", function () {
                fncTransactionUploadSelect();
            });
        });


        function fncSelectMaster() {
            if ($('#<%=cbLocation.ClientID %>').is(":checked") && $('#<%=cbDepartment.ClientID %>').is(":checked") &&
                $('#<%=cbCategory.ClientID %>').is(":checked") && $('#<%=cbSubCategory.ClientID %>').is(":checked") &&
                $('#<%=cbbrand.ClientID %>').is(":checked") && $('#<%=cbUom.ClientID %>').is(":checked") &&
                $('#<%=cbVendor.ClientID %>').is(":checked") && $('#<%=cbsize.ClientID %>').is(":checked") &&
                $('#<%=cbClass.ClientID %>').is(":checked") && $('#<%=cbSubClass.ClientID %>').is(":checked") &&
                $('#<%=cbBank.ClientID %>').is(":checked") && $('#<%=cbZone.ClientID %>').is(":checked") &&
                $('#<%=cbVoucherGroup.ClientID %>').is(":checked") && $('#<%=cbLocWpricesett.ClientID %>').is(":checked") &&
                $('#<%=cbMember.ClientID %>').is(":checked") && $('#<%=cbMember.ClientID %>').is(":checked") &&
                $('#<%=cbWarehouse.ClientID %>').is(":checked") && $('#<%=cbGV.ClientID %>').is(":checked") &&
                $('#<%=cbStyle.ClientID %>').is(":checked") && $('#<%=cbVDen.ClientID %>').is(":checked") &&
                $('#<%=cbPaymode.ClientID %>').is(":checked") && $('#<%=cbVehile.ClientID %>').is(":checked") &&
               $('#<%=cbMemberdet.ClientID %>').is(":checked") && $('#<%=cbAuditlog.ClientID %>').is(":checked") &&
                $('#<%=cbOthers.ClientID %>').is(":checked") && $('#<%=cbInventory.ClientID %>').is(":checked")) {
                $('#<%=cbMaster.ClientID %>').attr("checked", "checked");
            }
            else {
                $('#<%=cbMaster.ClientID %>').removeAttr("checked");
            }
        }

        ///Transaction Download Select
        function fncTransactionDownloadSelect() {
            try {
                if ($('#<%=cbPurchaseOrder.ClientID %>').is(":checked") && $('#<%=cbSO.ClientID %>').is(":checked") &&
                    $('#<%=cbDistribution.ClientID %>').is(":checked") && $('#<%=cbFV.ClientID %>').is(":checked") &&
                    $('#<%=cbBreakQty.ClientID %>').is(":checked") && $('#<%=cbOutletPurchase.ClientID %>').is(":checked") &&
                    $('#<%=cbFI.ClientID %>').is(":checked") && $('#<%=cbProEvents.ClientID %>').is(":checked") &&
                    $('#<%=cbSPecialPrice.ClientID %>').is(":checked") && $('#<%=cbBonusPoint.ClientID %>').is(":checked")) {
                    $('#<%=cbTransaction.ClientID %>').attr("checked", "checked");
                }
                else {
                    $('#<%=cbTransaction.ClientID %>').removeAttr("checked");
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
                ShowPopupMessageBox(err.message);
            }
        }

        ///Purchase Upload Select
        function fncPurchaseUploadSelect() {
            try {
                if ($('#<%=cbuPurchaseOrder.ClientID %>').is(":checked") && $('#<%=cbGA.ClientID %>').is(":checked") &&
                    $('#<%=cbGID.ClientID %>').is(":checked") && $('#<%=cbPurchaseReturn.ClientID %>').is(":checked") &&
                    $('#<%=cbAPTransaction.ClientID %>').is(":checked")) {
                    $('#<%=cbPurchase.ClientID %>').attr("checked", "checked");
                }
                else {
                    $('#<%=cbPurchase.ClientID %>').removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Sales Upload Select
        function fncSalesUploadSelect() {
            try {
                if ($('#<%=cbSalesHistory.ClientID %>').is(":checked") && $('#<%=cbCreditSales.ClientID %>').is(":checked") &&
                    $('#<%=cbCreditSales.ClientID %>').is(":checked") && $('#<%=cbSOSatus.ClientID %>').is(":checked") &&
                    $('#<%=cbOutletSales.ClientID %>').is(":checked") && $('#<%=cbUVDen.ClientID %>').is(":checked") &&
                    $('#<%=cbUVDen.ClientID %>').is(":checked") && $('#<%=cbVouSalesUpdate.ClientID %>').is(":checked")) {
                    $('#<%=cbSales.ClientID %>').attr("checked", "checked");
                }
                else {
                    $('#<%=cbSales.ClientID %>').removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Transaction Upload Select
        function fncTransactionUploadSelect() {
            try {
                if ($('#<%=cbStock.ClientID %>').is(":checked") && $('#<%=cbPosPayments.ClientID %>').is(":checked") &&
                    $('#<%=cbTranlog.ClientID %>').is(":checked") && $('#<%=cbMemnerTran.ClientID %>').is(":checked") &&
                    $('#<%=cbAudit.ClientID %>').is(":checked") && $('#<%=cbUItemTransfer.ClientID %>').is(":checked") &&
                    $('#<%=cbUDistribution.ClientID %>').is(":checked") && $('#<%=cbNewBatchCreation.ClientID %>').is(":checked") &&
                    $('#<%=cbNewBarcodeCreation.ClientID %>').is(":checked") && $('#<%=cbFVU.ClientID %>').is(":checked")) {
                    $('#<%=cbUTransaction.ClientID %>').attr("checked", "checked");
                }
                else {
                    $('#<%=cbUTransaction.ClientID %>').removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //Select All Check Box in CheckBox List
        $(function () {
            try {

                ///All Masters Select and UnSelect
                $("[id*=cbMaster]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $('#<%=cbLocation.ClientID %>').attr("checked", "checked");
                        $('#<%=cbDepartment.ClientID %>').attr("checked", "checked");
                        $('#<%=cbCategory.ClientID %>').attr("checked", "checked");
                        $('#<%=cbSubCategory.ClientID %>').attr("checked", "checked");
                        $('#<%=cbbrand.ClientID %>').attr("checked", "checked");
                        $('#<%=cbUom.ClientID %>').attr("checked", "checked");
                        $('#<%=cbVendor.ClientID %>').attr("checked", "checked");
                        $('#<%=cbsize.ClientID %>').attr("checked", "checked");
                        $('#<%=cbClass.ClientID %>').attr("checked", "checked");
                        $('#<%=cbSubClass.ClientID %>').attr("checked", "checked");
                        $('#<%=cbBank.ClientID %>').attr("checked", "checked");
                        $('#<%=cbZone.ClientID %>').attr("checked", "checked");
                        $('#<%=cbVoucherGroup.ClientID %>').attr("checked", "checked");
                        $('#<%=cbLocWpricesett.ClientID %>').attr("checked", "checked");
                        $('#<%=cbMember.ClientID %>').attr("checked", "checked");
                        $('#<%=cbWarehouse.ClientID %>').attr("checked", "checked");
                        $('#<%=cbGV.ClientID %>').attr("checked", "checked");
                        $('#<%=cbStyle.ClientID %>').attr("checked", "checked");
                        $('#<%=cbVDen.ClientID %>').attr("checked", "checked");
                        $('#<%=cbPaymode.ClientID %>').attr("checked", "checked");
                        $('#<%=cbVehile.ClientID %>').attr("checked", "checked");
                        $('#<%=cbMemberdet.ClientID %>').attr("checked", "checked");
                        $('#<%=cbAuditlog.ClientID %>').attr("checked", "checked");
                        $('#<%=cbOthers.ClientID %>').attr("checked", "checked");
                        $('#<%=cbInventory.ClientID %>').attr("checked", "checked");
                    } else {
                        $('#<%=cbLocation.ClientID %>').removeAttr("checked");
                        $('#<%=cbDepartment.ClientID %>').removeAttr("checked");
                        $('#<%=cbCategory.ClientID %>').removeAttr("checked");
                        $('#<%=cbSubCategory.ClientID %>').removeAttr("checked");
                        $('#<%=cbbrand.ClientID %>').removeAttr("checked");
                        $('#<%=cbUom.ClientID %>').removeAttr("checked");
                        $('#<%=cbVendor.ClientID %>').removeAttr("checked");
                        $('#<%=cbsize.ClientID %>').removeAttr("checked");
                        $('#<%=cbClass.ClientID %>').removeAttr("checked");
                        $('#<%=cbSubClass.ClientID %>').removeAttr("checked");
                        $('#<%=cbBank.ClientID %>').removeAttr("checked");
                        $('#<%=cbZone.ClientID %>').removeAttr("checked");
                        $('#<%=cbVoucherGroup.ClientID %>').removeAttr("checked");
                        $('#<%=cbLocWpricesett.ClientID %>').removeAttr("checked");
                        $('#<%=cbMember.ClientID %>').removeAttr("checked");
                        $('#<%=cbWarehouse.ClientID %>').removeAttr("checked");
                        $('#<%=cbGV.ClientID %>').removeAttr("checked");
                        $('#<%=cbStyle.ClientID %>').removeAttr("checked");
                        $('#<%=cbVDen.ClientID %>').removeAttr("checked");
                        $('#<%=cbPaymode.ClientID %>').removeAttr("checked");
                        $('#<%=cbVehile.ClientID %>').removeAttr("checked");
                        $('#<%=cbMemberdet.ClientID %>').removeAttr("checked");
                        $('#<%=cbAuditlog.ClientID %>').removeAttr("checked");
                        $('#<%=cbOthers.ClientID %>').removeAttr("checked");
                        $('#<%=cbInventory.ClientID %>').removeAttr("checked");
                    }
                });
                ///Transaction Download Select and UnSelect
                $("[id*=cbTransaction]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $('#<%=cbPurchaseOrder.ClientID %>').attr("checked", "checked");
                        $('#<%=cbSO.ClientID %>').attr("checked", "checked");
                        <%--$('#<%=cbStockIndent.ClientID %>').attr("checked", "checked");--%>
                        $('#<%=cbDistribution.ClientID %>').attr("checked", "checked");
                        <%--$('#<%=cbItemtransfer.ClientID %>').attr("checked", "checked");--%>
                        $('#<%=cbFV.ClientID %>').attr("checked", "checked");
                        $('#<%=cbBreakQty.ClientID %>').attr("checked", "checked");
                        <%--$('#<%=cbWarehousesales.ClientID %>').attr("checked", "checked");--%>
                        $('#<%=cbOutletPurchase.ClientID %>').attr("checked", "checked");
                        $('#<%=cbFI.ClientID %>').attr("checked", "checked");
                        $('#<%=cbProEvents.ClientID %>').attr("checked", "checked");
                        $('#<%=cbSPecialPrice.ClientID %>').attr("checked", "checked");
                        $('#<%=cbBonusPoint.ClientID %>').attr("checked", "checked");

                    } else {
                        $('#<%=cbPurchaseOrder.ClientID %>').removeAttr("checked");
                        $('#<%=cbSO.ClientID %>').removeAttr("checked");
                        <%--$('#<%=cbStockIndent.ClientID %>').removeAttr("checked");--%>
                        $('#<%=cbDistribution.ClientID %>').removeAttr("checked");
                        <%--$('#<%=cbItemtransfer.ClientID %>').removeAttr("checked");--%>
                        $('#<%=cbFV.ClientID %>').removeAttr("checked");
                        $('#<%=cbBreakQty.ClientID %>').removeAttr("checked");
                        <%--$('#<%=cbWarehousesales.ClientID %>').removeAttr("checked");--%>
                        $('#<%=cbOutletPurchase.ClientID %>').removeAttr("checked");
                        $('#<%=cbFI.ClientID %>').removeAttr("checked");
                        $('#<%=cbProEvents.ClientID %>').removeAttr("checked");
                        $('#<%=cbSPecialPrice.ClientID %>').removeAttr("checked");
                        $('#<%=cbBonusPoint.ClientID %>').removeAttr("checked");
                    }
                });
                ///Purchase Upload Select and UnSelect
                $("[id$=cbPurchase]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $('#<%=cbuPurchaseOrder.ClientID %>').attr("checked", "checked");
                        $('#<%=cbGA.ClientID %>').attr("checked", "checked");
                        $('#<%=cbGID.ClientID %>').attr("checked", "checked");
                        $('#<%=cbPurchaseReturn.ClientID %>').attr("checked", "checked");
                        $('#<%=cbAPTransaction.ClientID %>').attr("checked", "checked");

                    } else {
                        $('#<%=cbuPurchaseOrder.ClientID %>').removeAttr("checked");
                        $('#<%=cbGA.ClientID %>').removeAttr("checked");
                        $('#<%=cbGID.ClientID %>').removeAttr("checked");
                        $('#<%=cbPurchaseReturn.ClientID %>').removeAttr("checked");
                        $('#<%=cbAPTransaction.ClientID %>').removeAttr("checked");
                    }
                });

                ///Sales Upload Select and UnSelect
                $("[id$=cbSales]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $('#<%=cbSalesHistory.ClientID %>').attr("checked", "checked");
                        $('#<%=cbCreditSales.ClientID %>').attr("checked", "checked");                       
                        $('#<%=cbSOSatus.ClientID %>').attr("checked", "checked");
                        $('#<%=cbOutletSales.ClientID %>').attr("checked", "checked");
                        $('#<%=cbUVDen.ClientID %>').attr("checked", "checked");
                        $('#<%=cbVouSalesUpdate.ClientID %>').attr("checked", "checked");
                        $('#<%=cbSalesLog.ClientID %>').attr("checked", "checked");

                    } else {
                        $('#<%=cbSalesHistory.ClientID %>').removeAttr("checked");
                        $('#<%=cbCreditSales.ClientID %>').removeAttr("checked");                        
                        $('#<%=cbSOSatus.ClientID %>').removeAttr("checked");
                        $('#<%=cbOutletSales.ClientID %>').removeAttr("checked");
                        $('#<%=cbUVDen.ClientID %>').removeAttr("checked");
                        $('#<%=cbVouSalesUpdate.ClientID %>').removeAttr("checked");
                        $('#<%=cbSalesLog.ClientID %>').removeAttr("checked");
                    }
                });
                ///Transaction Upload Select and UnSelect
                $("[id*=cbUTransaction]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $('#<%=cbStock.ClientID %>').attr("checked", "checked");
                        $('#<%=cbPosPayments.ClientID %>').attr("checked", "checked");
                        $('#<%=cbTranlog.ClientID %>').attr("checked", "checked");
                        $('#<%=cbMemnerTran.ClientID %>').attr("checked", "checked");
                        $('#<%=cbAudit.ClientID %>').attr("checked", "checked");
                        $('#<%=cbUItemTransfer.ClientID %>').attr("checked", "checked");
                        $('#<%=cbUDistribution.ClientID %>').attr("checked", "checked");
                        $('#<%=cbNewBatchCreation.ClientID %>').attr("checked", "checked");
                        $('#<%=cbNewBarcodeCreation.ClientID %>').attr("checked", "checked");
                        $('#<%=cbFVU.ClientID %>').attr("checked", "checked");

                    } else {
                        $('#<%=cbStock.ClientID %>').removeAttr("checked");
                        $('#<%=cbPosPayments.ClientID %>').removeAttr("checked");
                        $('#<%=cbTranlog.ClientID %>').removeAttr("checked");
                        $('#<%=cbMemnerTran.ClientID %>').removeAttr("checked");
                        $('#<%=cbAudit.ClientID %>').removeAttr("checked");
                        $('#<%=cbUItemTransfer.ClientID %>').removeAttr("checked");
                        $('#<%=cbUDistribution.ClientID %>').removeAttr("checked");
                        $('#<%=cbNewBatchCreation.ClientID %>').removeAttr("checked");
                        $('#<%=cbNewBarcodeCreation.ClientID %>').removeAttr("checked");
                        $('#<%=cbFVU.ClientID %>').removeAttr("checked");
                    }
                });


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

        //Save Dialog Initialation
        function fncSaveSettings() {
            try {
                $("#save").dialog({
                    title: "Daily Procedure",
                    resizable: false,
                    height: 130,
                    width: 300,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
            <li class="active-page">
                <%=Resources.LabelCaption.lblDailyProcedure%>
            </li>
        </ul>
    </div>
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="col-md-8 Daily_Procedure_syncinterval">
                <div class="col-md-4">
                    <asp:Label ID="lblStaus" runat="server" Text='<%$ Resources:LabelCaption,lblsyncinterval %>'></asp:Label>
                </div>
                <div class="col-md-1">
                    <asp:TextBox ID="txtSynctime" runat="server" CssClass="form-control-res" onKeyDown="return isNumberKey(event)"
                        MaxLength="3"></asp:TextBox>
                </div>
                <div class="col-md-2 ">
                    <asp:CheckBox ID="cbService" runat="server" CssClass="float_right"  Text='<%$ Resources:LabelCaption,lbl_Service %>' />
                
            </div>
            </div>
              
            <div class="col-md-4">
                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue Daily_Procedure_save"
                    Text='<%$ Resources:LabelCaption,lnkSave %>' OnClick="lnkSave_Click"></asp:LinkButton>
            </div>
        </div>
        <div class="col-md-12 Dailyprocedure_border">
            <div class="col-md-12 DailyProcedure_hdr_color">
                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblDownloadfromHQ %>'></asp:Label>
            </div>
            <div class="col-md-12 Dailyprocedure_border">
                <div class="col-md-12 Daily_Proceure_subhdr">
                    <asp:CheckBox ID="cbMaster" runat="server" Text='<%$ Resources:LabelCaption,lblMasters %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbInventory" runat="server" Text='<%$ Resources:LabelCaption,lbl_Inventory %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbLocation" runat="server" Text='<%$ Resources:LabelCaption,lblLocation %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbDepartment" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbCategory" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSubCategory" runat="server" Text='<%$ Resources:LabelCaption,lbl_SubCategory %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbbrand" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbUom" runat="server" Text='<%$ Resources:LabelCaption,lbl_UOM %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbVendor" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbsize" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbClass" runat="server" Text='<%$ Resources:LabelCaption,lblClass %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSubClass" runat="server" Text='<%$ Resources:LabelCaption,lblSubClass %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbBank" runat="server" Text='<%$ Resources:LabelCaption,lblBank %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbZone" runat="server" Text='<%$ Resources:LabelCaption,lbl_Zone %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbVoucherGroup" runat="server" Text='<%$ Resources:LabelCaption,lblVouchergroup %>' />
                </div>
                <div class="col-md-2 hiddencol ">
                    <asp:CheckBox ID="cbLocWpricesett" runat="server" Text='<%$ Resources:LabelCaption,lblLocwPricesettings %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbMember" runat="server" Text='<%$ Resources:LabelCaption,lblMember %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbWarehouse" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbGV" runat="server" Text='<%$ Resources:LabelCaption,lblGiftVoucher %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbStyle" runat="server" Text='<%$ Resources:LabelCaption,lbl_Style %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbVDen" runat="server" Text='<%$ Resources:LabelCaption,lblVoucherDenamination %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbPaymode" runat="server" Text='<%$ Resources:LabelCaption,lblPayMode %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbVehile" runat="server" Text='<%$ Resources:LabelCaption,lblVehilemaster %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbMemberdet" runat="server" Text='<%$ Resources:LabelCaption,lblMemerdetail %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbAuditlog" runat="server" Text='<%$ Resources:LabelCaption,lblAuditlog %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbOthers" runat="server" Text='<%$ Resources:LabelCaption,lblOthers %>' />
                </div>
                <div class="col-md-12 Daily_Proceure_subhdr">
                    <asp:CheckBox ID="cbTransaction" runat="server" Text='<%$ Resources:LabelCaption,lblTransaction %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbPurchaseOrder" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSO" runat="server" Text='<%$ Resources:LabelCaption,lblSalesOrder %>' />
                </div>
                <%-- <div class="col-md-1">
                    <asp:CheckBox ID="cbStockIndent" runat="server"  Text='<%$ Resources:LabelCaption,lblStockIndent %>' />
                </div>--%>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbDistribution" runat="server" Text='<%$ Resources:LabelCaption,lblDistribution %>' />
                </div>
                <%--<div class="col-md-1">
                    <asp:CheckBox ID="cbItemtransfer" runat="server"  Text='<%$ Resources:LabelCaption,lblItemtransfer %>' />
                </div>--%>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbFV" runat="server" Text='<%$ Resources:LabelCaption,lblFestivalVoucher %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbBreakQty" runat="server" Text='<%$ Resources:LabelCaption,lblBreakQty %>' />
                </div>
                <%-- <div class="col-md-2">
                    <asp:CheckBox ID="cbWarehousesales" runat="server"  Text='<%$ Resources:LabelCaption,lblWarehousesales %>' />
                </div>--%>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbOutletPurchase" runat="server" Text='<%$ Resources:LabelCaption,lblOutletPurchase %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbFI" runat="server" Text='<%$ Resources:LabelCaption,lbl_FreeItemHdr %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbProEvents" runat="server" Text='<%$ Resources:LabelCaption,lblPromotionevents %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSPecialPrice" runat="server" Text='<%$ Resources:LabelCaption,lblSpecialPrice %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbBonusPoint" runat="server" Text='<%$ Resources:LabelCaption,lblBonusPoint %>' />
                </div>
            </div>
            <div class="col-md-12 DailyProcedure_hdr_color">
                <asp:Label ID="lblUploadtoHQ" runat="server" Text='<%$ Resources:LabelCaption,lblUploadtoHQ %>'></asp:Label>
            </div>
            <div class="col-md-12 Dailyprocedure_border">
                <div class="col-md-12 Daily_Proceure_subhdr">
                    <asp:CheckBox ID="cbPurchase" runat="server" Text='<%$ Resources:LabelCaption,lblPurchase %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbuPurchaseOrder" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbGA" runat="server" Text='<%$ Resources:LabelCaption,lblGA %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbGID" runat="server" Text='<%$ Resources:LabelCaption,lblGID %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbPurchaseReturn" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseReturn %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbAPTransaction" runat="server" Text='<%$ Resources:LabelCaption,lblAPTransaction %>' />
                </div>
                <div class="col-md-12 Daily_Proceure_subhdr">
                    <asp:CheckBox ID="cbSales" runat="server" Text='<%$ Resources:LabelCaption,lblSales %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSalesHistory" runat="server" Text='<%$ Resources:LabelCaption,lblSalesHistory %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbCreditSales" runat="server" Text='<%$ Resources:LabelCaption,lblCreditSales %>' />
                </div>
                <%-- <div class="col-md-1">
                    <asp:CheckBox ID="cbSalesReFund" runat="server" Text='<%$ Resources:LabelCaption,lbl_salesrefund %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSalesReturn" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesReturn %>' />
                </div>--%>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbSOSatus" runat="server" Text='<%$ Resources:LabelCaption,lblSOStatus %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbOutletSales" runat="server" Text='<%$ Resources:LabelCaption,lblOutletSales %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbUVDen" runat="server" Text='<%$ Resources:LabelCaption,lblVoucherDenamination %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbVouSalesUpdate" runat="server" Text='<%$ Resources:LabelCaption,lblVoucherSalesUpdate %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbSalesLog" runat="server" Text='<%$ Resources:LabelCaption,lbl_Saleslog %>' />
                </div>
                <div class="col-md-12 Daily_Proceure_subhdr">
                    <asp:CheckBox ID="cbUTransaction" runat="server" Text='<%$ Resources:LabelCaption,lblTransaction %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbStock" runat="server" Text='<%$ Resources:LabelCaption,lbl_Stock %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbPosPayments" runat="server" Text='<%$ Resources:LabelCaption,lbl_PosPayments %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbMemnerTran" runat="server" Text='<%$ Resources:LabelCaption,lblMemnerTransaction %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbAudit" runat="server" Text='<%$ Resources:LabelCaption,lblAudit %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbUItemTransfer" runat="server" Text='<%$ Resources:LabelCaption,lblItemtransfer %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbUDistribution" runat="server" Text='<%$ Resources:LabelCaption,lblDistribution %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbNewBatchCreation" runat="server" Text='<%$ Resources:LabelCaption,lblNewBatchCreation %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbNewBarcodeCreation" runat="server" Text='<%$ Resources:LabelCaption,lblNewBarcodeCreation %>' />
                </div>
                <div class="col-md-2">
                    <asp:CheckBox ID="cbFVU" runat="server" Text='<%$ Resources:LabelCaption,lblFestivalVoucherUsage %>' />
                </div>
                <div class="col-md-1">
                    <asp:CheckBox ID="cbTranlog" runat="server" Text='<%$ Resources:LabelCaption,lbl_TransactionLog %>' />
                </div>
            </div>
          <%--  <div class="col-md-12 ">
                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue Daily_Procedure_save"
                    Text='<%$ Resources:LabelCaption,lnkSave %>' OnClick="lnkSave_Click"></asp:LinkButton>
            </div>--%>
        </div>
        <div class="hiddencol">
            <div id="save" class="barcodesavedialog">
                <p>
                    <%=Resources.LabelCaption.Alert_Save%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblOk %>'
                        OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
