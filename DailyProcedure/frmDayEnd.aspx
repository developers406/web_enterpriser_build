﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MainMaster.master" CodeBehind="frmDayEnd.aspx.cs" Inherits="EnterpriserWebFinal.DailyProcedure.frmDayEnd" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
              <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Daily Procedure</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Day End</li>
            </ul>
        </div>
        <br />
            <center style="padding-top: 10px;">
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="System Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSystemDate" runat="server" ReadOnly="true" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Day End Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDayenddate" ReadOnly="true" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <asp:LinkButton ID="lnkProceed" OnClick="lnkSave_Click"  runat="server" class="button-blue" Style="float: right; margin-right: 23px"><i class="icon-play"></i>Proceed(ENT)</asp:LinkButton>
                    </div>
                </div>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel> 
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
