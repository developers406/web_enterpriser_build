﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMisDashBoard.aspx.cs" Inherits="EnterpriserWebFinal.MISDashboard.frmMisDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }

        #display {
            background: transparent;
            height: 50px;
            width: 500px;
            overflow: hidden;
            position: relative;
        }

        #text {
            background: transparent;
            cursor: pointer;
            overflow: hidden;
            position: absolute;
            width: auto;
            left: 10px;
            margin-right: 10px;
        }

        .dropdown-menu > li > a {
            color: black;
            font-family: Roboto,sans-serif;
        }

        .main-nav > li > a {
            font-family: Roboto,sans-serif;
        }

        .container-fluid {
            padding-right: 5px;
            padding-left: 5px;
        }

        body {
            overflow-x: auto;
            overflow-y: auto;
        }

        .div-left {
            float: left;
            padding-left: 10px;
        }

        .div-right {
            float: right;
            padding-right: 10px;
        }

        body {
            overflow-x: hidden;
            overflow-y: hidden;     /*22112022 musaraf*/
              
        }

        #tree-table {
            /*font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;*/
            border-collapse: collapse;
            width: 100%;
        }

            #tree-table td, #customers th {
                border: 1px solid BLACK;
                padding: 5px;
            }

            #tree-table tr:nth-child(even) {
                background-color: #f2f2f2;
                cursor: pointer;
            }

            #tree-table tr:hover {
                background-color: #ddd;
                cursor: pointer;
            }

            #tree-table th {
                /*padding-top: 12px;
                padding-bottom: 12px;*/
                text-align: center;
                background-color: #00a65a;
                color: white;
            }
    </style>
    <link type="text/css" href="../css/misdashboard.css" rel="stylesheet" />
    <script src="../js/DashBoard_JS/Chart.bundle.min.js" type="text/javascript"></script>
    <script src="../js/DashBoard_JS/jquerysession.js" type="text/javascript"></script>

    <script type="text/javascript">
        var ip = '';
        var vLocation;
        var LocationParameter;
        var myDoughnutTodaySales, myDoughnutCurrentMonthSales, myDoughnutTodayPurchase, myDoughnutCurrentMonthPurchase,
            myDoughnutVendorWisePurchaseToday, myDoughnutVendorWisePurchaseOrderToday, myDoughnutMovingStockAsOn,
            myDoughnutStockAsOn, myLineHourlySales, myBarBillingRangeValue, myBarSalesDateCompare, myBarSalesMonthCompare, myBarMarginReport,
            checkedvalue = "Actual";
        $(document).ready(function () {
            $('#lblversion').html('Version: ' + $('#<%= hidVersion.ClientID %>').val());
            $('#lblLicense').html('License: ' + $('#<%= hidLicense.ClientID %>').val());
            if ($('#<%= hidViewbtn.ClientID %>').val() == "V1") {
                $('input[type=radio][name=Diagrm]').change(function () {
                    if (this.value == 'Actual') {
                        checkedvalue = "Actual";
                        fncgetGraphdata($('#ddlLocation').val());
                    }
                    else if (this.value == 'Diagram') {
                        checkedvalue = "Diagram";
                        fncgetGraphdata($('#ddlLocation').val());
                    }
                });
                $("select").show().removeClass('chzn-done');
                $("select").next().remove();
                $("#trNoOfBills").click(function () {
                    $('#myModal').modal('show');
                });
                $("#trTotalSalesValue").click(function () {
                    $('#myModal').modal('show');
                });
                $("#trAvgBillValue").click(function () {
                    $('#myModal').modal('show');
                });
                $('#ddlLocation').on('change', function () {
                    vLocation = this.value;
                    fncgetGraphdata(vLocation);
                });
                $("#btLoadReport").click(function () {
                    var Content;
                    $('#myModal').modal('hide');
                    var obj = {};
                    obj.Location = vLocation;
                    obj.Filter = $('#ddlFilter').val();
                    obj.CompanyID = '';
                    $.ajax({
                        //url: "http://" + ip + "/MobileRpos.svc/fncGetReportDataItemValue?Location=" + vLocation + "&Filter=" + $('#ddlFilter').val(),+
                        url: "frmMisDashBoard.aspx/fncGetReportDataItemValue",
                        data: JSON.stringify(obj),
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var objData = response;
                            Content = "<table class='list-group' width='100%''><tbody>";
                            Content += "<th>Name</th><th>Today Value</th><th>Today Percentage</th><th>Month Value</th><th>Month Percentage</th><th>Previous Month Value</th><th>Previous Month Percentage</th>"
                            for (var i = 0; i < objData.Table.length; i++) {
                                Content += "<tr><td>" +
                                    objData.Table[i]["Name"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["TodayValue"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["TodayPercentage"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["MonthValue"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["MonthPercentage"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["PreviousMonthValue"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["PreviousMonthPercentage"] + "</td style='text-align: right;'>"
                            }
                            Content += "</tr></tbody></table>"

                            $('#myModalItemValueValueRecord').html(Content);
                            $('#ModalItemValueText').text("Selected Row " + objData.Table.length);
                            $('#myModalItemValue').modal('show');
                        },
                        error: function (response) {
                            console.log(response.message);
                        },
                        failure: function (response) {
                            console.log(response.message);
                        }
                    });
                });

                $("#btLoadReportStock").click(function () {
                    var Content;
                    $('#myModalStock').modal('hide');
                    var obj = {};
                    obj.Location = vLocation;
                    obj.Filter = $('#ddlFilterStock').val();
                    obj.CompanyID = '';
                    /*alert(obj.Location);*/
                    $.ajax({
                        // url: "http://" + ip + "/MobileRpos.svc/fncGetReportDataStockValue?Location=" + vLocation + "&Filter=" + $('#ddlFilterStock').val(),
                        url: '<%=ResolveUrl("frmMisDashBoard.aspx/fncGetReportDataStockValue") %>',
                        /*url: "frmMisDashBoard.aspx/fncGetReportDataStockValue",*/
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        type: "GET",
                        success: function (response) {
                            var objData = response;
                           /* alert(objData["Name"]);*/
                            Content = "<table class='list-group' width='100%''><tbody>";
                            Content += "<th>Name</th><th>Today Value</th><th>Today Percentage</th>"
                            for (var i = 0; i < objData.Table.length; i++) {
                                Content += "<tr><td>" +
                                    objData.Table[i]["Name"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["TodayValue"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["TodayPercentage"] + "</td><td style='text-align: right;'>"
                                
                            }
                            Content += "</tr></tbody></table>"
                            $('#myModalStockRecord').html(Content);
                            $('#ModalStockValueText').text("Selected Row " + objData.Table.length);
                            $('#myModalStockValue').modal('show');
                        },

                       error: function (response) {
                            console.log(response.message);
                        },

                        failure: function (response) {
                            console.log(response.message);
                          
                        }
                        
                    });
                   
                   
                });
              
                $("#btLoadReportMargin").click(function () {
                    var Content;
                    $('#myModalMargin').modal('hide');
                    var obj = {};
                    obj.Location = vLocation;
                    obj.Filter = $('#ddlFilterMargin').val();
                    obj.CompanyID = '';
                    $.ajax({

                        // url: "http://" + ip + "/MobileRpos.svc/fncGetReportDataMarginValue?Location=" + vLocation + "&Filter=" + $('#ddlFilterMargin').val(),
                        url: "frmMisDashBoard.aspx/fncGetReportDataMarginValue",
                        data: obj,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var objData = $.parseJSON(response);
                            Content = "<table class='list-group' width='100%''><tbody>";
                            Content += "<th>Name</th><th>Salese</th><th>Purchase</th><th>Profit</th><th>ProfitPercentage</th>"
                            for (var i = 0; i < objData.Table.length; i++) {
                                Content += "<tr><td>" +
                                    objData.Table[i]["Name"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["Sales"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["Purchase"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["Profit"] + "</td><td style='text-align: right;'>" +
                                    objData.Table[i]["ProfitPercentage"] + "</td><td style='text-align: right;'>"
                            }
                            Content += "</tr></tbody></table>"

                            $('#myModalMarginRecord').html(Content);
                            $('#ModalMarginText').text("Selected Row " + objData.Table.length);
                            $('#myModalMarginValue').modal('show');
                        },
                        error: function (response) {
                            console.log(response.message);
                        },
                        failure: function (response) {
                            console.log(response.message);
                        }
                    });
                });
                $.sum = function (arr) {
                    var r = 0;
                    $.each(arr, function (i, v) {
                        r += +v;
                    });
                    return r;
                }
                $.ajax({
                    //url: "http://" + ip + "/MobileRpos.svc/GetCompanyDetails",
                    type: "POST",
                    url: "frmMisDashBoard.aspx/fncGetCompanyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var objData = $.parseJSON(response.d);
                        $('#header').html(objData[0]["Description"] + " - " + objData[0]["Code"]);
                    },
                    error: function (response) {
                        console.log(response.message);
                    },
                    failure: function (response) {
                        console.log(response.message);
                    }
                });

                LocationParameter = "Y";
                if (LocationParameter == "Y") {
                    vLocation = "All";
                }
                else {
                    vLocation = "HQ";
                }
                $.ajax({
                    // url: "http://" + ip + "/MobileRpos.svc/GetLocation",
                    url: "frmMisDashBoard.aspx/fncGetLocation",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var objData = $.parseJSON(response.d);
                        if (objData.length > 0) {

                            if (LocationParameter == "Y") {
                                $("#ddlLocation").append($("<option></option>").val("ALL").text("ALL"));
                            }
                            for (var i = 0; i < objData.length; i++) {

                                $("#ddlLocation").append($("<option></option>").val(objData[i]["LocationName"]).text(objData[i]["LocationName"].trim()));
                            }

                        }
                        fncgetGraphdata(vLocation);
                    },
                    error: function (response) {
                        console.log(response.message);
                    },
                    failure: function (response) {
                        console.log(response.message);
                    }
                });
            }
        });

        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };
        window.chartColors = {

            Red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            pink: 'rgb(255,192,203)',
            brown: 'rgb(165,42,42)',
            LightBlue: 'rgb(135, 206, 235)'
        };
        var colorNames = Object.keys(window.chartColors);
        var color = Chart.helpers.color;

        var TodaySalesData;
        var TodaySaleslabels;
        var configTodaySales;
        var backgroundColor = [
            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,

            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,

            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,

            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,

            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,

            window.chartColors.Red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
            window.chartColors.pink,
            window.chartColors.brown,
            window.chartColors.LightBlue,];


        var CurrentMonthSalesData;
        var CurrentMonthSaleslabels;
        var configCurrentMonthSales;



        var TodayPurchaseData;
        var TodayPurchaselabels;
        var configTodayPurchase;



        var CurrentMonthPurchaselabels;
        var CurrentMonthPurchaseData;
        var configCurrentMonthPurchase;



        var VendorWisePurchaseTodayData;
        var VendorWisePurchaseTodaylabels;
        var configVendorWisePurchaseToday;



        var VendorWisePurchaseOrderTodaydata;
        var VendorWisePurchaseOrderTodaylabels;
        var configVendorWisePurchaseOrderToday;


        var MovingStockAsOndata;
        var MovingStockAsOnlabels;
        var configMovingStockAsOn;


        var StockAsOndata;
        var StockAsOnlabels;
        var configStockAsOn;


        var BillingRangeValuelabels;
        var BillingRangeValueRangedata;
        var BillingRangeValueValuedata;
        var BillingRangeValuePercentagedata;
        var configBillingRangeValue;

        var SalesDatelabels;
        var SalesDatetoday;
        var SalesDatePervioustoday;
        var SalesDatetodaydata;
        var SalesDatePervioustodaydata;
        var configSalesDateCompare;

        var SalesMonthlabels;
        var SalesMonthtoday;
        var SalesMonthPervioustoday;
        var SalesMonthtodaydata;
        var SalesMonthtPervioustodaydata;
        var configSalesMonthCompare;

        var MarginReportPurchaselabels;
        var MarginReportPurchaseValuedata;
        var MarginReportSalesValuedata;
        var configMarginReport;

        var HourlySalesdata;
        var HourlySaleslabels;
        var configHourlySales;
        // var currency = '&#x20b9;';
        var currency_symbols = {
            'USD': '$', // US Dollar
            'EUR': '€', // Euro
            'CRC': '₡', // Costa Rican Colón
            'GBP': '£', // British Pound Sterling
            'ILS': '₪', // Israeli New Sheqel
            'INR': '₹', // Indian Rupee
            'JPY': '¥', // Japanese Yen
            'KRW': '₩', // South Korean Won
            'NGN': '₦', // Nigerian Naira
            'PHP': '₱', // Philippine Peso
            'PLN': 'zł', // Polish Zloty
            'PYG': '₲', // Paraguayan Guarani
            'THB': '฿', // Thai Baht
            'UAH': '₴', // Ukrainian Hryvnia
            'VND': '₫', // Vietnamese Dong
        };

        var currency_name = 'INR';
        var CurrencyName = currency_symbols[currency_name];
        var currency = CurrencyName;
        //console.log(currency);
        function fncgetGraphdata(Location) {
            var obj = {};
            obj.Location = Location;
            $.ajax({
                // url: "http://" + ip + "/MobileRpos.svc/fncGetSalesReport?&Location=" + Location,
                url: "frmMisDashBoard.aspx/fncGetSelectedReport",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                success: function (response) {

                    var objData = $.parseJSON(response.d);
                    if (checkedvalue == "Active") {
                        fncBuildArray(objData.Table, objData.Table1, objData.Table2, objData.Table3,
                            objData.Table4, objData.Table5, objData.Table6, objData.Table7, objData.Table8,
                            objData.Table9, objData.Table10, objData.Table11, objData.Table12, objData.Table13,
                            objData.Table14, objData.Table15, objData.Table16, objData.Table17, objData.Table18, objData.Table19);
                    }
                    else {
                        fncDiagramChange(objData.Table, objData.Table1, objData.Table2, objData.Table3,
                            objData.Table4, objData.Table5, objData.Table6, objData.Table7, objData.Table8,
                            objData.Table9, objData.Table10, objData.Table11, objData.Table12, objData.Table13,
                            objData.Table14, objData.Table15, objData.Table16, objData.Table17, objData.Table18, objData.Table19);
                    }
                    setTimeout('fncgetGraphdata(vLocation);', $('#<%=hidPageRefresh.ClientID%>').val());
                },
                error: function (response) {
                    console.log(response.message);
                },
                failure: function (response) {
                    console.log(response.message);
                }
            });
        }
        function fncBuildArray(D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13, D14, D15, D16, D17, D18, D19, D20) {
            var Content;
            var d = new Date();

            var month = d.getMonth() + 1;
            var day = d.getDate();

            var CurrentDate = (('' + day).length < 2 ? '0' : '') + day + '-' +
                (('' + month).length < 2 ? '0' : '') + month + '-' +
                d.getFullYear();

            var sum = {};
            sum = D1.map(function (e) { return parseFloat(e.Data) });
            if (sum.length > 0) {
                for (var i = 0; i < sum.length; i++) {
                    if (sum[i] < 0) {
                        sum[i] = parseFloat(sum[i]) * -1;
                    }

                }
            }

            TodaySalesData = sum;
            for (var i = 0; i < sum.length; i++) {

                sum[i] = parseFloat(sum[i]);
            }

            $('p#TodaySalesSum').html('Total -  ' + currency + '' + $.sum(D1.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            TodaySaleslabels = D1.map(function (e) { return e.Labels });

            var Month = {};
            Month = D2.map(function (e) { return parseFloat(e.Data) });
            if (Month.length > 0) {
                for (var i = 0; i < Month.length; i++) {
                    if (Month[i] < 0) {
                        Month[i] = parseFloat(Month[i]) * -1;
                    }

                }
            }
            CurrentMonthSalesData = Month;

            CurrentMonthSaleslabels = D2.map(function (e) { return e.Labels });
            if (CurrentMonthSaleslabels.length > 8) {
                $('#divTodaySalesSum').css('width', '33.33333333%');
                $('#divCurrentMonth').css('width', '33.33333333%');
                $('#divTodayPurchase').css('width', '33.33333333%');
                $('#divMonthPurchaseSum').css('width', '33.33333333%');
                $('#divVendorPurchaseSum').css('width', '33.33333333%');
                $('#divvendorpurchaseOrderSum').css('width', '33.33333333%');
                $('#divMovingStockason').css('width', '33.33333333%');
                $('#divStockasOn').css('width', '33.33333333%');
            }
            //if (D3[0]["InvDate"] ==CurrentDate) {
            var PurchaseData = {};
            PurchaseData = D3.map(function (e) { return parseFloat(e.Data) });
            if (PurchaseData.length > 0) {
                for (var i = 0; i < Month.length; i++) {
                    if (PurchaseData[i] < 0) {
                        PurchaseData[i] = parseFloat(PurchaseData[i]) * -1;
                    }
                }
            }
            TodayPurchaseData = PurchaseData;

            TodayPurchaselabels = D3.map(function (e) { return e.Labels });
            $('p#TodayPurchaseSum').html('Total -  ' + currency + '' + $.sum(PurchaseData).toFixed(2));
            //}
            //else{
            //   $('p#TodayPurchaseSum').html('Total -  ' + currency + '' + '0.00');
            //}

            CurrentMonthPurchaseData = D4.map(function (e) { return parseFloat(e.Data) });
            CurrentMonthPurchaselabels = D4.map(function (e) { return e.Labels });
            // if (D5[0]["InvDate"] ==CurrentDate) {
            VendorWisePurchaseTodaylabels = D5.map(function (e) { return e.Labels });
            VendorWisePurchaseTodayData = D5.map(function (e) { return parseFloat(e.Data) });
            $('p#VendorWisePurchaseTodaySum').html('Total -  ' + currency + '' + $.sum(D5.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            //}else{
            //  $('p#VendorWisePurchaseTodaySum').html('Total -  ' + currency + '' + '0.00');
            //}


            VendorWisePurchaseOrderTodaydata = D6.map(function (e) { return parseFloat(e.Data) });
            VendorWisePurchaseOrderTodaylabels = D6.map(function (e) { return e.Labels });
            $('p#VendorWisePurchaseOrderTodaySum').html('Total -  ' + currency + '' + $.sum(D6.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            MovingStockAsOndata = D7.map(function (e) { return parseFloat(e.Data) });
            MovingStockAsOnlabels = D7.map(function (e) { return e.Labels });


            var StockAsOn = {};
            StockAsOn = D8.map(function (e) { return parseFloat(e.Data) });  // 26112022 musaraf
            if (StockAsOn.length > 0) {
                for (var i = 0; i < StockAsOn.length; i++) {
                    if (StockAsOn[i] < 0) {
                        StockAsOn[i] = parseFloat(StockAsOn[i]) * -1;
                    }
                }
            }
            StockAsOndata = StockAsOn;


            StockAsOndata = StockAsOn;/*D8.map(function (e) { return parseInt(e.Data) });*/   // 26112022 musaraf
            StockAsOnlabels = D8.map(function (e) { return e.Labels });

            HourlySalesdata = D9.map(function (e) { return parseFloat(e.Data) });
            HourlySaleslabels = D9.map(function (e) { return e.Labels });
            //console.log(D10);
            BillingRangeValuelabels = D10.map(function (e) { return e.Labels });
            //console.log(BillingRangeValuelabels);
            BillingRangeValueRangedata = D10.map(function (e) { return parseFloat(e.Range) });
            //console.log(BillingRangeValueRangedata);
            BillingRangeValueValuedata = D10.map(function (e) { return parseFloat(e.Value) });
            //console.log(BillingRangeValueValuedata);
            BillingRangeValuePercentagedata = D10.map(function (e) { return parseFloat(e.Percentage) });

            $('p#CurrentMonthSalesSum').html('Total -  ' + currency + '' + $.sum(D2.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            $('p#CurrentMonthPurchaseSum').html('Total - ' + currency + '' + $.sum(D4.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            $('p#MovingStockAsOnSum').html('Total -  ' + currency + '' + $.sum(D7.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            $('p#StockAsOnSum').html('Total -  ' + currency + '' + $.sum(D8.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            //if (D11[0]["InvDate"] == CurrentDate) { 
            $('#NoOfBillsToday').text($.sum(D11.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            // $('#NoOfBillsToday').text('0.00');
            //}
            $('#NoOfBillsMonth').text($.sum(D11.map(function (e) { return parseFloat(e.Month) })));
            //if (D12[0]["InvDate"] == CurrentDate) { 
            $('#TotalSalesValueToday').text($.sum(D12.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            //  $('#TotalSalesValueToday').text('0.00');
            //}
            $('#TotalSalesValueMonth').text($.sum(D12.map(function (e) { return parseFloat(e.Month) })).toFixed(2));
            //if (D13[0]["InvDate"] == CurrentDate) { 
            $('#AvgBillValueToday').text($.sum(D13.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            //$('#AvgBillValueToday').text('0.00');
            //}
            $('#AvgBillValueMonth').text($.sum(D13.map(function (e) { return parseFloat(e.Month) })).toFixed(2));


            SalesDatetoday = D15.map(function (e) { return e.Date });
            SalesDatetodaydata = D15.map(function (e) { return parseFloat(e.Value) });
            SalesDatePervioustoday = D16.map(function (e) { return e.Date });
            SalesDatePervioustodaydata = D16.map(function (e) { return parseFloat(e.Value) });

            SalesMonthtoday = D17.map(function (e) { return e.Month });
            SalesMonthtodaydata = D17.map(function (e) { return parseFloat(e.Value) });
            SalesMonthPervioustoday = D18.map(function (e) { return e.Month });
            SalesMonthtPervioustodaydata = D18.map(function (e) { return parseFloat(e.Value) });
            MarginReportPurchaselabels = D19.map(function (e) { return e.Labels });
            MarginReportSalesValuedata = D19.map(function (e) { return parseFloat(e.Sales) });
            MarginReportPurchaseValuedata = D19.map(function (e) { return parseFloat(e.Purchase) });

            ContentD14 = "<table width='100%'><tbody>";
            //console.log(D14);
            for (var i = 0; i < D14.length; i++) {
                ContentD14 += "<tr>" + "<td style='width: 29%'>" + D14[i]["Name"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'> " + D14[i]["Value"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'>" + D14[i]["Percentage"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'> " + D14[i]["ValueMonth"] +
                    "<td style='text-align: right;width: 17.5%'>" + D14[i]["PercentageMonth"] +
                    "</td>"
            }
            ContentD14 += "</tr></tbody></table>";
            // console.log();

            ContentD20 = "<table width='100%'><tbody>"; for (var i = 0; i < D20.length; i++) {
                //  if(D20[i]["InvDate"] == CurrentDate){
                ContentD20 += "<tr>" + "<td style='width: 51.4%' >" + D20[i]["Labels"] + "</td>" +
                    "<td style='text-align: right;width: 27.8%'>" + D20[i]["TodayQty"] + "</td>" +
                    "<td style='text-align: right;'> " + currency + D20[i]["TodayValue"] + "</td>"
                //  }

            }
            ContentD20 += "</tr></tbody></table>";
            $('#CostToCostSales').html(ContentD20);
            $('#BillValueRecord').html(ContentD14);
            fncconfig();
        }

        function fncLoadGraph() {

            var ctxTodaySales = document.getElementById('ctxTodaySales').getContext('2d');
            if (myDoughnutTodaySales != null) {

                myDoughnutTodaySales.destroy();
            }
            myDoughnutTodaySales = new Chart(ctxTodaySales, configTodaySales);
            // {
            //type: 'pie',
            //data: { 
            // labels: TodaySaleslabels,
            //datasets: [{
            //             data: TodaySalesData,
            //             backgroundColor: backgroundColor,
            //         }],

            //     },
            // options: {
            //   legend: {
            //   display: true
            //  }
            // ,
            // legendCallback: function(chart3) {
            // var text = [];
            //  text.push('<ul class="0-legend">');
            //  var ds = chart3.data.datasets[0];
            //  var sum = ds.data.reduce(function add(a, b) { return a + b; }, 0);
            //  for (var i=0; i<ds.data.length; i++) {
            //    text.push('<li>');
            //    var perc = Math.round(100*ds.data[i]/sum,0);
            //  text.push('<span style="background-color:' + ds.backgroundColor[i] + '">' + '</span>' 
            //    + chart3.data.labels[i] + ' ('+ ds.data[i] +') ('+perc+'%)');
            //   text.push('</li>');
            // }
            // text.push('</ul>');
            //  return text.join("");
            // }
            // }
            //});

            var ctxCurrentMonthSales = document.getElementById('ctxCurrentMonthSales').getContext('2d');
            if (myDoughnutCurrentMonthSales != null) {

                myDoughnutCurrentMonthSales.destroy();
            }
            myDoughnutCurrentMonthSales = new Chart(ctxCurrentMonthSales, configCurrentMonthSales);

            var ctxTodayPurchase = document.getElementById('ctxTodayPurchase').getContext('2d');

            if (myDoughnutTodayPurchase != null) {

                myDoughnutTodayPurchase.destroy();
            }

            myDoughnutTodayPurchase = new Chart(ctxTodayPurchase, configTodayPurchase);

            //ctxTodayPurchase.remove(); // this is my <canvas> element
            // $('#TodayPurchase').append('<canvas id="ctxTodayPurchase"><canvas>');
            //canvas = document.querySelector(ctxTodayPurchase);

            /*new Chart(ctxTodayPurchase, configTodayPurchase);*/

            var ctxCurrentMonthPurchase = document.getElementById('ctxCurrentMonthPurchase').getContext('2d');
            if (myDoughnutCurrentMonthPurchase != null) {

                myDoughnutCurrentMonthPurchase.destroy();
            }
            myDoughnutCurrentMonthPurchase = new Chart(ctxCurrentMonthPurchase, configCurrentMonthPurchase);
            var ctxVendorWisePurchaseToday = document.getElementById('ctxVendorWisePurchaseToday').getContext('2d');
            if (myDoughnutVendorWisePurchaseToday != null) {

                myDoughnutVendorWisePurchaseToday.destroy();
            }
            myDoughnutVendorWisePurchaseToday = new Chart(ctxVendorWisePurchaseToday, configVendorWisePurchaseToday);
            var ctxVendorWisePurchaseOrderToday = document.getElementById('ctxVendorWisePurchaseOrderToday').getContext('2d');
            if (myDoughnutVendorWisePurchaseOrderToday != null) {

                myDoughnutVendorWisePurchaseOrderToday.destroy();
            }
            myDoughnutVendorWisePurchaseOrderToday = new Chart(ctxVendorWisePurchaseOrderToday, configVendorWisePurchaseOrderToday);
            var ctxMovingStockAsOn = document.getElementById('ctxMovingStockAsOn').getContext('2d');
            if (myDoughnutMovingStockAsOn != null) {

                myDoughnutMovingStockAsOn.destroy();
            }
            myDoughnutMovingStockAsOn = new Chart(ctxMovingStockAsOn, configMovingStockAsOn);

            var ctxStockAsOn = document.getElementById('ctxStockAsOn').getContext('2d');
            if (myDoughnutStockAsOn != null) {

                myDoughnutStockAsOn.destroy();
            }
            myDoughnutStockAsOn = new Chart(ctxStockAsOn, configStockAsOn);

            var ctxHourlySales = document.getElementById('ctxHourlySales').getContext('2d');
            if (myLineHourlySales != null) {

                myLineHourlySales.destroy();
            }
            myLineHourlySales = new Chart(ctxHourlySales, configHourlySales);

            var ctxBillingRangeValue = document.getElementById('ctxBillingRangeValue').getContext('2d');
            if (myBarBillingRangeValue != null) {

                myBarBillingRangeValue.destroy();
            }
            myBarBillingRangeValue = new Chart(ctxBillingRangeValue, configBillingRangeValue);

            var ctxSalesDateCompare = document.getElementById('ctxSalesDateCompare').getContext('2d');
            if (myBarSalesDateCompare != null) {

                myBarSalesDateCompare.destroy();
            }
            myBarSalesDateCompare = new Chart(ctxSalesDateCompare, configSalesDateCompare);
            myBarSalesMonthCompare
            var ctxSalesMonthComparee = document.getElementById('ctxSalesMonthCompare').getContext('2d');
            if (myBarSalesMonthCompare != null) {

                myBarSalesMonthCompare.destroy();
            }
            myBarSalesMonthCompare = new Chart(ctxSalesMonthComparee, configSalesMonthCompare);
            var ctxMarginReportValue = document.getElementById('ctxMarginReport').getContext('2d');
            if (myBarMarginReport != null) {

                myBarMarginReport.destroy();
            }
            myBarMarginReport = new Chart(ctxMarginReportValue, configMarginReport);
            //var ctx = document.getElementById('canvas2').getContext('2d');
            // window.myLine = new Chart(ctx, config);
            document.getElementById('ctxStockAsOn').onclick = function (evt) {
                var activePoints = myDoughnutStockAsOn.getElementsAtEvent(evt);
                if (activePoints[0]) {
                    //var chartData = activePoints[0]['_chart'].config.data;
                    //var idx = activePoints[0]['_index'];
                    //var label = chartData.labels[idx];
                    //var value = chartData.datasets[0].data[idx];
                    //var url = "http://example.com/?label=" + label + "&value=" + value;
                    //console.log(url);
                    $('#myModalStock').modal('show');
                }
            };
            document.getElementById('ctxMarginReport').onclick = function (evt) {
                var activePoints = myBarMarginReport.getElementsAtEvent(evt);
                if (activePoints[0]) {
                    //    var chartData = activePoints[0]['_chart'].config.data;
                    //var idx = activePoints[0]['_index'];

                    //var label = chartData.labels[idx];
                    //var value = chartData.datasets[0].data[idx];

                    //var url = "http://example.com/?label=" + label + "&value=" + value;
                    //console.log(url);
                    $('#myModalMargin').modal('show');
                }
            };
        }

        var MALE_BAR_COLOUR = 'rgba(0, 51, 78, 0.3)';
        var MALE_BAR_ACTIVE_COLOUR = 'rgba(0, 51, 78, 1)';
        var FEMALE_BAR_COLOUR = 'rgba(248, 142, 40, 0.3)';
        var FEMALE_BAR_ACTIVE_COLOUR = 'rgba(248, 142, 40, 1)';

        function fncconfig() {

            configTodaySales = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: TodaySalesData,
                        backgroundColor: backgroundColor,
                    }],
                    labels: TodaySaleslabels
                },
                options: {

                    legend: {
                        position: 'left',
                        display: true,
                        labels: {
                            generateLabels: function (chart) {
                                var data = chart.data;
                                if (data.labels.length && data.datasets.length) {
                                    return data.labels.map(function (label, i) {
                                        var meta = chart.getDatasetMeta(0);
                                        var ds = data.datasets[0];
                                        var arc = meta.data[i];
                                        var custom = arc && arc.custom || {};
                                        var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                                        var arcOpts = chart.options.elements.arc;
                                        var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                        var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                        var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

                                        // We get the value of the current label
                                        var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

                                        return {
                                            // Instead of `text: label,`
                                            // We add the value to the string
                                            text: label + " : " + value,
                                            fillStyle: fill,
                                            strokeStyle: stroke,
                                            lineWidth: bw,
                                            hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                            index: i
                                        };
                                    });
                                } else {
                                    return [];
                                }
                            }
                        }
                    },

                    title: {
                        display: true,
                        text: 'Today Sales',
                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }

            };

            configCurrentMonthSales = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: CurrentMonthSalesData,
                        backgroundColor: backgroundColor,
                    }],

                    labels: CurrentMonthSaleslabels,
                },
                options: {

                    legend: {
                        position: 'left',
                        display: true,
                        labels: {
                            generateLabels: function (chart) {
                                var data = chart.data;
                                if (data.labels.length && data.datasets.length) {
                                    return data.labels.map(function (label, i) {
                                        var meta = chart.getDatasetMeta(0);
                                        var ds = data.datasets[0];
                                        var arc = meta.data[i];
                                        var custom = arc && arc.custom || {};
                                        var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                                        var arcOpts = chart.options.elements.arc;
                                        var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                        var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                        var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

                                        // We get the value of the current label
                                        var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

                                        return {
                                            // Instead of `text: label,`
                                            // We add the value to the string
                                            text: label + " : " + value,
                                            fillStyle: fill,
                                            strokeStyle: stroke,
                                            lineWidth: bw,
                                            hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                            index: i
                                        };
                                    });
                                } else {
                                    return [];
                                }
                            }
                        }
                    },
                    title: {
                        display: true,
                        text: 'Current Month Sales',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configTodayPurchase = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: TodayPurchaseData,
                        backgroundColor: backgroundColor,
                    }],


                    labels: TodayPurchaselabels
                },
                options: {

                    legend: {
                        position: 'left',
                        display: true,
                        labels: {
                            generateLabels: function (chart) {
                                var data = chart.data;
                                if (data.labels.length && data.datasets.length) {
                                    return data.labels.map(function (label, i) {
                                        var meta = chart.getDatasetMeta(0);
                                        var ds = data.datasets[0];
                                        var arc = meta.data[i];
                                        var custom = arc && arc.custom || {};
                                        var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                                        var arcOpts = chart.options.elements.arc;
                                        var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                        var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                        var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

                                        // We get the value of the current label
                                        var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

                                        return {
                                            // Instead of `text: label,`
                                            // We add the value to the string
                                            text: label + " : " + value,
                                            fillStyle: fill,
                                            strokeStyle: stroke,
                                            lineWidth: bw,
                                            hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                            index: i
                                        };
                                    });
                                } else {
                                    return [];
                                }
                            }
                        }
                    },
                    title: {
                        display: true,
                        text: 'Today Purchase',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configCurrentMonthPurchase = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: CurrentMonthPurchaseData,
                        backgroundColor: backgroundColor,
                    }],


                    labels: CurrentMonthPurchaselabels
                },
                options: {

                    legend: {
                        position: 'left',
                        display: true,
                        labels: {
                            generateLabels: function (chart) {
                                var data = chart.data;
                                if (data.labels.length && data.datasets.length) {
                                    return data.labels.map(function (label, i) {
                                        var meta = chart.getDatasetMeta(0);
                                        var ds = data.datasets[0];
                                        var arc = meta.data[i];
                                        var custom = arc && arc.custom || {};
                                        var getValueAtIndexOrDefault = Chart.helpers.getValueAtIndexOrDefault;
                                        var arcOpts = chart.options.elements.arc;
                                        var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                                        var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                                        var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);

                                        // We get the value of the current label
                                        var value = chart.config.data.datasets[arc._datasetIndex].data[arc._index];

                                        return {
                                            // Instead of `text: label,`
                                            // We add the value to the string
                                            text: label + " : " + value,
                                            fillStyle: fill,
                                            strokeStyle: stroke,
                                            lineWidth: bw,
                                            hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
                                            index: i
                                        };
                                    });
                                } else {
                                    return [];
                                }
                            }
                        }
                    },
                    title: {
                        display: true,
                        text: 'Current Month Purchase',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configVendorWisePurchaseToday = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: VendorWisePurchaseTodayData,
                        backgroundColor: backgroundColor,
                    }],

                    labels: VendorWisePurchaseTodaylabels
                },
                options: {

                    legend: {
                        position: 'none',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Vendor Wise Purchase Today',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configVendorWisePurchaseOrderToday = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: VendorWisePurchaseOrderTodaydata,
                        backgroundColor: backgroundColor
                    }],


                    labels: VendorWisePurchaseOrderTodaylabels
                },
                options: {

                    legend: {
                        position: 'none',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Vendor Wise Purchase Order Today',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configMovingStockAsOn = {
                type: 'pie',
                data: {
                    datasets: [{
                        data: MovingStockAsOndata,
                        backgroundColor: backgroundColor
                    }],


                    labels: MovingStockAsOnlabels
                },
                options: {

                    legend: {
                        position: 'none',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Non Moving Stock As On',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            


            configStockAsOn = {
                type: 'pie',
                data: {
                    datasets: [{
                        
                        data: StockAsOndata,
                        backgroundColor: backgroundColor,
                        

                    }],

                    labels: StockAsOnlabels
                },
                options: {

                    legend: {
                        position: 'none',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Stock As On',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configHourlySales = {
                type: 'line',
                data: {
                    labels: HourlySalesdata,
                    datasets: [{
                        label: 'Amount',
                        fontColor: '#004966',
                        backgroundColor: window.chartColors.Red,
                        borderColor: window.chartColors.Red,
                        data: HourlySaleslabels,
                        fill: false,
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    showAllTooltips: true,
                    animation: {
                        duration: 250
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Hourly Sales',
                        fontColor: '#004966'
                    },
                    tooltips: {
                        intersect: false,
                        fontColor: '#004966',

                        callbacks: {
                            title: function (t, d) {
                                const o = d.datasets.map((ds) => "Amount:" + ds.data[t[0].index])
                                return o.join(', ');
                            },
                            label: function (t, d) {
                                return "Time:" + d.labels[t.index];
                            }
                        }
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Time',
                                fontColor: '#004966'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Amount',
                                stacked: true, fontColor: '#004966'
                            }
                        }]
                    }
                }
            };
            configBillingRangeValue = {
                type: 'bar',
                data: {
                    labels: BillingRangeValuelabels,
                    datasets: [{
                        label: 'Bill Count',
                        backgroundColor: color(window.chartColors.Red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.Red,
                        borderWidth: 1,
                        data: BillingRangeValueRangedata,
                        fontColor: '#004966'
                    }, {
                        label: 'Value',
                        backgroundColor: color(window.chartColors.yellow).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.yellow,
                        borderWidth: 1,
                        data: BillingRangeValueValuedata,
                        fontColor: '#004966'
                    }, {
                        label: 'Percentage',
                        backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.green,
                        borderWidth: 1,
                        data: BillingRangeValuePercentagedata,
                        fontColor: '#004966'
                    }]

                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                        labels: {
                            fontColor: '#004966'
                        }
                    },

                    title: {
                        display: true,
                        text: 'Billing Range Value',

                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };

            configSalesDateCompare = {
                type: 'bar',
                data: {
                    //labels: SalesDatetoday,
                    datasets: [{
                        label: SalesDatetoday,
                        backgroundColor: color(window.chartColors.Red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.Red,
                        borderWidth: 1,
                        data: SalesDatetodaydata
                    }, {
                        label: SalesDatePervioustoday,
                        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data: SalesDatePervioustodaydata
                    }]

                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'none',
                    },
                    title: {
                        display: true,
                        text: 'Sales In ' + SalesDatetoday + ' - ' + SalesDatetodaydata + ' Sales In ' + SalesDatePervioustoday + ' - ' + SalesDatePervioustodaydata,
                        fontColor: '#004966',

                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            //console.log(SalesMonthtodaydata + " " + SalesMonthtPervioustodaydata);
            configSalesMonthCompare = {
                type: 'bar',
                data: {
                    //labels: SalesMonthlabels,
                    datasets: [{
                        label: SalesMonthtoday,
                        backgroundColor: color(window.chartColors.Red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.Red,
                        borderWidth: 1,
                        data: SalesMonthtodaydata
                    }, {
                        label: SalesMonthPervioustoday,
                        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data: SalesMonthtPervioustodaydata
                    }]

                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'none',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Sales As On ' + SalesMonthtoday + ' - ' + SalesMonthtodaydata + ' Sales As On ' + SalesMonthPervioustoday + ' - ' + SalesMonthtPervioustodaydata,
                        fontColor: '#004966',

                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            configMarginReport = {
                type: 'bar',
                data: {
                    labels: MarginReportPurchaselabels,
                    datasets: [{
                        label: 'Sales',
                        backgroundColor: color(window.chartColors.Red).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.Red,
                        borderWidth: 1,
                        data: MarginReportSalesValuedata
                    }, {
                        label: 'Purchase',
                        backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                        borderColor: window.chartColors.blue,
                        borderWidth: 1,
                        data: MarginReportPurchaseValuedata
                    }]

                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    responsive: true,
                    legend: {
                        position: 'top',
                        labels: {
                            fontColor: '#004966'
                        }
                    },
                    title: {
                        display: true,
                        text: 'Stock As On',

                        fontColor: '#004966'
                    },
                    title: {
                        display: true,
                        text: 'Margin Report',
                        fontColor: '#004966'
                    },
                    tooltips: {
                        mode: 'index',
                        axis: 'y'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            fncLoadGraph();
        }
        /*Interactivity to determine when an animated element in in view. In view elements trigger our animation*/
        //$(document).ready(function () {

        //    //window and animation items
        //    var animation_elements = $.find('.animation-element');
        //    var web_window = $(window);

        //    //check to see if any animation containers are currently in view
        //    function check_if_in_view() {
        //        //get current window information
        //        var window_height = web_window.height();
        //        var window_top_position = web_window.scrollTop();
        //        var window_bottom_position = (window_top_position + window_height);

        //        //iterate through elements to see if its in view
        //        $.each(animation_elements, function () {

        //            //get the element sinformation
        //            var element = $(this);
        //            var element_height = $(element).outerHeight();
        //            var element_top_position = $(element).offset().top;
        //            var element_bottom_position = (element_top_position + element_height);

        //            //check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
        //            if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
        //                element.addClass('in-view');
        //            } else {
        //                element.removeClass('in-view');
        //            }
        //        });

        //    }

        //    //on or scroll, detect elements in view
        //    $(window).on('scroll resize', function () {
        //        check_if_in_view()
        //    })
        //    //trigger our scroll event on initial load
        //    $(window).trigger('scroll');


        //    //caches a jQuery object containing the X element
        //    var footer = $(".footer-sticky");
        //    $(window).scroll(function () {
        //        var scroll = $(window).scrollTop();

        //        if (scroll >= 300) {
        //            footer.addClass("scrollActive");
        //        } else {
        //            footer.removeClass("scrollActive");
        //        }
        //    });
        //});



        //////LEGEND\\\\
        //var myLegendContainer = document.getElementById("legend");
        // generate HTML legend
        //myLegendContainer.innerHTML = myDoughnutTodaySales.generateLegend();
        // bind onClick event to all LI-tags of the legend
        //var legendItems = myLegendContainer.getElementsByTagName('li');
        //for (var i = 0; i < legendItems.length; i += 1) {
        //legendItems[i].addEventListener("click", legendClickCallback, false);
        //}

        //function legendClickCallback(event) {
        // event = event || window.event;

        // var target = event.target || event.srcElement;
        // while (target.nodeName !== 'LI') {
        //   target = target.parentElement;
        //  }
        //  var parent = target.parentElement;
        // var chartId = parseInt(parent.classList[0].split("-")[0], 10);
        // var chart = Chart.instances[chartId];
        //  var index = Array.prototype.slice.call(parent.children).indexOf(target);
        //  var meta = chart.getDatasetMeta(0);
        //  console.log(index);
        //   var item = meta.data[index];

        // if (item.hidden === null || item.hidden === false) {
        // item.hidden = true;
        // target.classList.add('hidden');
        // } else {
        //  target.classList.remove('hidden');
        //  item.hidden = null;
        // }
        // chart.update();
        //}

        function fncDiagramChange(D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13, D14, D15, D16, D17, D18, D19, D20) {
            var Content;
            var d = new Date();

            var month = d.getMonth() + 1;
            var day = d.getDate();

            var CurrentDate = (('' + day).length < 2 ? '0' : '') + day + '-' +
                (('' + month).length < 2 ? '0' : '') + month + '-' +
                d.getFullYear();

            //if (D1[0]["InvDate"] ==CurrentDate) {
            var sum = {};
            sum = D1.map(function (e) { return parseFloat(e.Data) });

            if (sum.length > 0) {
                for (var i = 0; i < sum.length; i++) {
                    if (sum[i] < 0) {
                        sum[i] = parseFloat(sum[i]) * -1;
                    }

                }
            }


            TodaySalesData = sum;


            $('p#TodaySalesSum').html('Total -  ' + currency + '' + $.sum(D1.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            //}else{
            // $('p#TodaySalesSum').html('Total -  ' + currency + '' + '0.00');
            //}
            TodaySaleslabels = D1.map(function (e) { return e.Labels });

            var Month = {};
            Month = D2.map(function (e) { return parseFloat(e.Data) });
            if (Month.length > 0) {
                for (var i = 0; i < Month.length; i++) {
                    if (Month[i] < 0) {
                        Month[i] = parseFloat(Month[i]) * -1;
                    }

                }
            }
            CurrentMonthSalesData = Month;

            CurrentMonthSaleslabels = D2.map(function (e) { return e.Labels });
            if (CurrentMonthSaleslabels.length > 8) {
                $('#divTodaySalesSum').css('width', '33.33333333%');
                $('#divCurrentMonth').css('width', '33.33333333%');
                $('#divTodayPurchase').css('width', '33.33333333%');
                $('#divMonthPurchaseSum').css('width', '33.33333333%');
                $('#divVendorPurchaseSum').css('width', '33.33333333%');
                $('#divvendorpurchaseOrderSum').css('width', '33.33333333%');
                $('#divMovingStockason').css('width', '33.33333333%');
                $('#divStockasOn').css('width', '33.33333333%');
            }
            //if (D3[0]["InvDate"] ==CurrentDate) {
            var PurchaseData = {};
            PurchaseData = D3.map(function (e) { return parseFloat(e.Data) });
            if (PurchaseData.length > 0) {
                for (var i = 0; i < Month.length; i++) {
                    if (PurchaseData[i] < 0) {
                        PurchaseData[i] = parseFloat(PurchaseData[i]) * -1;
                    }
                }
            }
            TodayPurchaseData = PurchaseData;

            TodayPurchaselabels = D3.map(function (e) { return e.Labels });
            $('p#TodayPurchaseSum').html('Total -  ' + currency + '' + $.sum(PurchaseData).toFixed(2));
            //}
            //else{
            //   $('p#TodayPurchaseSum').html('Total -  ' + currency + '' + '0.00');
            //}

            CurrentMonthPurchaseData = D4.map(function (e) { return parseFloat(e.Data) });
            CurrentMonthPurchaselabels = D4.map(function (e) { return e.Labels });
            // if (D5[0]["InvDate"] ==CurrentDate) {
            VendorWisePurchaseTodaylabels = D5.map(function (e) { return e.Labels });
            VendorWisePurchaseTodayData = D5.map(function (e) { return parseFloat(e.Data) });
            $('p#VendorWisePurchaseTodaySum').html('Total -  ' + currency + '' + $.sum(D5.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            //}else{
            //  $('p#VendorWisePurchaseTodaySum').html('Total -  ' + currency + '' + '0.00');
            //}


            VendorWisePurchaseOrderTodaydata = D6.map(function (e) { return parseFloat(e.Data) });
            VendorWisePurchaseOrderTodaylabels = D6.map(function (e) { return e.Labels });
            $('p#VendorWisePurchaseOrderTodaySum').html('Total -  ' + currency + '' + $.sum(D6.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            MovingStockAsOndata = D7.map(function (e) { return parseFloat(e.Data) });
            MovingStockAsOnlabels = D7.map(function (e) { return e.Labels });

           

            var StockData1 = {};
            StockData1 = D8.map(function (e) { return parseFloat(e.Data) });    //musaraf 26112022
            if (StockData1.length > 0) {
                for (var i = 0; i < StockData1.length; i++) {
                    if (StockData1[i] < 0) {
                        StockData1[i] = parseFloat(StockData1[i]) * -1;
                    }

                }
            }


            StockAsOndata = StockData1; /*D8.map(function (e) { return parseInt(e.Data) });*/   //musaraf 26112022
            StockAsOnlabels = D8.map(function (e) { return e.Labels });

            HourlySalesdata = D9.map(function (e) { return parseFloat(e.Data) });
            HourlySaleslabels = D9.map(function (e) { return e.Labels });
            //console.log(D10);
            BillingRangeValuelabels = D10.map(function (e) { return e.Labels });
            //console.log(BillingRangeValuelabels);
            BillingRangeValueRangedata = D10.map(function (e) { return parseFloat(e.Range) });
            //console.log(BillingRangeValueRangedata);
            BillingRangeValueValuedata = D10.map(function (e) { return parseFloat(e.Value) });
            //console.log(BillingRangeValueValuedata);
            BillingRangeValuePercentagedata = D10.map(function (e) { return parseFloat(e.Percentage) });

            $('p#CurrentMonthSalesSum').html('Total -  ' + currency + '' + $.sum(D2.map(function (e) { return parseFloat(e.Data) })).toFixed(2));

            $('p#CurrentMonthPurchaseSum').html('Total - ' + currency + '' + $.sum(D4.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            $('p#MovingStockAsOnSum').html('Total -  ' + currency + '' + $.sum(D7.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            $('p#StockAsOnSum').html('Total -  ' + currency + '' + $.sum(D8.map(function (e) { return parseFloat(e.Data) })).toFixed(2));
            //if (D11[0]["InvDate"] == CurrentDate) { 
            $('#NoOfBillsToday').text($.sum(D11.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            // $('#NoOfBillsToday').text('0.00');
            //}
            $('#NoOfBillsMonth').text($.sum(D11.map(function (e) { return parseFloat(e.Month) })));
            //if (D12[0]["InvDate"] == CurrentDate) { 
            $('#TotalSalesValueToday').text($.sum(D12.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            //  $('#TotalSalesValueToday').text('0.00');
            //}
            $('#TotalSalesValueMonth').text($.sum(D12.map(function (e) { return parseFloat(e.Month) })).toFixed(2));
            //if (D13[0]["InvDate"] == CurrentDate) { 
            $('#AvgBillValueToday').text($.sum(D13.map(function (e) { return parseFloat(e.Today) })));
            //}
            //else{
            //$('#AvgBillValueToday').text('0.00');
            //}
            $('#AvgBillValueMonth').text($.sum(D13.map(function (e) { return parseFloat(e.Month) })).toFixed(2));


            SalesDatetoday = D15.map(function (e) { return e.Date });
            SalesDatetodaydata = D15.map(function (e) { return parseFloat(e.Value) });
            SalesDatePervioustoday = D16.map(function (e) { return e.Date });
            SalesDatePervioustodaydata = D16.map(function (e) { return parseFloat(e.Value) });

            SalesMonthtoday = D17.map(function (e) { return e.Month });
            SalesMonthtodaydata = D17.map(function (e) { return parseFloat(e.Value) });
            SalesMonthPervioustoday = D18.map(function (e) { return e.Month });
            SalesMonthtPervioustodaydata = D18.map(function (e) { return parseFloat(e.Value) });
            MarginReportPurchaselabels = D19.map(function (e) { return e.Labels });
            MarginReportSalesValuedata = D19.map(function (e) { return parseFloat(e.Sales) });
            MarginReportPurchaseValuedata = D19.map(function (e) { return parseFloat(e.Purchase) });

            ContentD14 = "<table width='100%'><tbody>";
            //console.log(D14);
            for (var i = 0; i < D14.length; i++) {
                ContentD14 += "<tr>" + "<td style='width: 29%'>" + D14[i]["Name"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'> " + D14[i]["Value"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'>" + D14[i]["Percentage"] + "</td>" +
                    "<td style='text-align: right;width: 17.5%'> " + D14[i]["ValueMonth"] +
                    "<td style='text-align: right;width: 17.5%'>" + D14[i]["PercentageMonth"] +
                    "</td>"
            }
            ContentD14 += "</tr></tbody></table>";
            // console.log();

            ContentD20 = "<table width='100%'><tbody>"; for (var i = 0; i < D20.length; i++) {
                //if(D20[i]["InvDate"] == CurrentDate){
                ContentD20 += "<tr>" + "<td style='width: 51.4%' >" + D20[i]["Labels"] + "</td>" +
                    "<td style='text-align: right;width: 27.8%'>" + D20[i]["TodayQty"] + "</td>" +
                    "<td style='text-align: right;'> " + currency + D20[i]["TodayValue"] + "</td>"
                // }

            }
            ContentD20 += "</tr></tbody></table>";
            $('#CostToCostSales').html(ContentD20);
            $('#BillValueRecord').html(ContentD14);
            fncconfig();
        }

        function fncLoadGraph() {

            var ctxTodaySales = document.getElementById('ctxTodaySales').getContext('2d');
            if (myDoughnutTodaySales != null) {

                myDoughnutTodaySales.destroy();
            }
            myDoughnutTodaySales = new Chart(ctxTodaySales, configTodaySales);

            var ctxCurrentMonthSales = document.getElementById('ctxCurrentMonthSales').getContext('2d');
            if (myDoughnutCurrentMonthSales != null) {

                myDoughnutCurrentMonthSales.destroy();
            }
            myDoughnutCurrentMonthSales = new Chart(ctxCurrentMonthSales, configCurrentMonthSales);

            var ctxTodayPurchase = document.getElementById('ctxTodayPurchase').getContext('2d');

            if (myDoughnutTodayPurchase != null) {

                myDoughnutTodayPurchase.destroy();
            }

            myDoughnutTodayPurchase = new Chart(ctxTodayPurchase, configTodayPurchase);

            var ctxCurrentMonthPurchase = document.getElementById('ctxCurrentMonthPurchase').getContext('2d');
            if (myDoughnutCurrentMonthPurchase != null) {

                myDoughnutCurrentMonthPurchase.destroy();
            }
            myDoughnutCurrentMonthPurchase = new Chart(ctxCurrentMonthPurchase, configCurrentMonthPurchase);
            var ctxVendorWisePurchaseToday = document.getElementById('ctxVendorWisePurchaseToday').getContext('2d');
            if (myDoughnutVendorWisePurchaseToday != null) {

                myDoughnutVendorWisePurchaseToday.destroy();
            }
            myDoughnutVendorWisePurchaseToday = new Chart(ctxVendorWisePurchaseToday, configVendorWisePurchaseToday);
            var ctxVendorWisePurchaseOrderToday = document.getElementById('ctxVendorWisePurchaseOrderToday').getContext('2d');
            if (myDoughnutVendorWisePurchaseOrderToday != null) {

                myDoughnutVendorWisePurchaseOrderToday.destroy();
            }
            myDoughnutVendorWisePurchaseOrderToday = new Chart(ctxVendorWisePurchaseOrderToday, configVendorWisePurchaseOrderToday);
            var ctxMovingStockAsOn = document.getElementById('ctxMovingStockAsOn').getContext('2d');
            if (myDoughnutMovingStockAsOn != null) {

                myDoughnutMovingStockAsOn.destroy();
            }
            myDoughnutMovingStockAsOn = new Chart(ctxMovingStockAsOn, configMovingStockAsOn);

            var ctxStockAsOn = document.getElementById('ctxStockAsOn').getContext('2d');
            if (myDoughnutStockAsOn != null) {

                myDoughnutStockAsOn.destroy();
            }
            myDoughnutStockAsOn = new Chart(ctxStockAsOn, configStockAsOn);

            var ctxHourlySales = document.getElementById('ctxHourlySales').getContext('2d');
            if (myLineHourlySales != null) {

                myLineHourlySales.destroy();
            }
            myLineHourlySales = new Chart(ctxHourlySales, configHourlySales);

            var ctxBillingRangeValue = document.getElementById('ctxBillingRangeValue').getContext('2d');
            if (myBarBillingRangeValue != null) {

                myBarBillingRangeValue.destroy();
            }
            myBarBillingRangeValue = new Chart(ctxBillingRangeValue, configBillingRangeValue);

            var ctxSalesDateCompare = document.getElementById('ctxSalesDateCompare').getContext('2d');
            if (myBarSalesDateCompare != null) {

                myBarSalesDateCompare.destroy();
            }
            myBarSalesDateCompare = new Chart(ctxSalesDateCompare, configSalesDateCompare);
            myBarSalesMonthCompare
            var ctxSalesMonthComparee = document.getElementById('ctxSalesMonthCompare').getContext('2d');
            if (myBarSalesMonthCompare != null) {

                myBarSalesMonthCompare.destroy();
            }
            myBarSalesMonthCompare = new Chart(ctxSalesMonthComparee, configSalesMonthCompare);
            var ctxMarginReportValue = document.getElementById('ctxMarginReport').getContext('2d');
            if (myBarMarginReport != null) {

                myBarMarginReport.destroy();
            }
            myBarMarginReport = new Chart(ctxMarginReportValue, configMarginReport);
            //var ctx = document.getElementById('canvas2').getContext('2d');
            // window.myLine = new Chart(ctx, config);
            document.getElementById('ctxStockAsOn').onclick = function (evt) {
                var activePoints = myDoughnutStockAsOn.getElementsAtEvent(evt);
                if (activePoints[0]) {
                    //var chartData = activePoints[0]['_chart'].config.data;
                    //var idx = activePoints[0]['_index'];
                    //var label = chartData.labels[idx];
                    //var value = chartData.datasets[0].data[idx];
                    //var url = "http://example.com/?label=" + label + "&value=" + value;
                    //console.log(url);
                    $('#myModalStock').modal('show');
                }
            };
            document.getElementById('ctxMarginReport').onclick = function (evt) {
                var activePoints = myBarMarginReport.getElementsAtEvent(evt);
                if (activePoints[0]) {
                    //    var chartData = activePoints[0]['_chart'].config.data;
                    //var idx = activePoints[0]['_index'];

                    //var label = chartData.labels[idx];
                    //var value = chartData.datasets[0].data[idx];

                    //var url = "http://example.com/?label=" + label + "&value=" + value;
                    //console.log(url);
                    $('#myModalMargin').modal('show');
                }
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divBody" runat="server"> 
        <%-- <div class="col-md-4" style ="margin-top:5px;text-align:center;">
            <label id="lblLicense" style="font-weight: 800;color: #ff6384;"></label>
        </div>--%>  
        <div class="col-md-12" style="margin-top: 5px;">
            <div class="col-md-4">
                <label id="lblversion" style="font-weight: 800; color: #4c4d9d;"></label>
            </div>
            <div class="col-md-4" style="text-align: center!important;">
                 
            </div>
            <div class="col-md-4"> 
                <asp:Button ID="btnstatus" Text="Sync Status"   Style="background-color: darkslateblue; color: white; width: 90px;margin-left:25%" runat="server" OnClick="btnstatus_Click" />
                <select class="form-control" id="ddlLocation" style="float: right;height:auto"></select>
            </div>
        </div>
        <div class="col-md-12 EnableScroll" style="overflow-y:scroll">           <%--22112022 musaraf--%>
            <div class="col-md-4" id="divTodaySalesSum">
                <div id="TodaySales" class="maincontentborder">
                    <canvas id="ctxTodaySales" height="205px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="TodaySalesSum">Total - ******</p>
                </div>
                <div id="legend"></div>
            </div>
            <div class="col-md-4" id="divCurrentMonth">
                <div id="CurrentMonthSales" class="maincontentborder">
                    <canvas id="ctxCurrentMonthSales" height="205px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="CurrentMonthSalesSum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-4" id="divTodayPurchase">
                <div id="TodayPurchase" class="maincontentborder">
                    <canvas id="ctxTodayPurchase" height="205px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="TodayPurchaseSum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-4" id="divMonthPurchaseSum">
                <div class="maincontentborder">
                    <canvas id="ctxCurrentMonthPurchase" height="205px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="CurrentMonthPurchaseSum">Total - ******</p>
                </div>
            </div>

            <div class="col-md-4" id="divVendorPurchaseSum">
                <div class="maincontentborder">
                    <canvas id="ctxVendorWisePurchaseToday" height="210px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="VendorWisePurchaseTodaySum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-4" id="divvendorpurchaseOrderSum">
                <div class="maincontentborder">
                    <canvas id="ctxVendorWisePurchaseOrderToday" height="210px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="VendorWisePurchaseOrderTodaySum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-4" id="divMovingStockason">
                <div id="MovingStockAsOn" class="maincontentborder">
                    <canvas id="ctxMovingStockAsOn" height="210px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="MovingStockAsOnSum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-4" id="divStockasOn">
                <div id="StockAsOn" class="maincontentborder">
                    <canvas id="ctxStockAsOn" height="210px"></canvas>
                    <hr class="maincontenthr" />
                    <p class="maincontentP" id="StockAsOnSum">Total - ******</p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="maincontentborder " style="height: auto">
                            <div id="BillValueRecgord" class="maincontentborder">

                                <table width="100%">
                                    <tr>
                                        <th></th>
                                        <th>Today</th>
                                        <th>Month</th>
                                    </tr>

                                    <tr id="trNoOfBills">
                                        <td style="cursor: pointer;">No Of Bills</td>
                                        <td id="NoOfBillsToday" style="text-align: right; cursor: pointer;">0</td>
                                        <td id="NoOfBillsMonth" style="text-align: right; cursor: pointer;">0</td>

                                    </tr>
                                    <tr id="trTotalSalesValue">
                                        <td style="cursor: pointer;">Total Sales Value</td>
                                        <td id="TotalSalesValueToday" style="text-align: right; cursor: pointer;">0.00</td>
                                        <td id="TotalSalesValueMonth" style="text-align: right; cursor: pointer;">0.00</td>
                                    </tr>
                                    <tr id="trAvgBillValue">
                                        <td style="cursor: pointer;">Avg Bill Value</td>
                                        <td id="AvgBillValueToday" style="text-align: right; cursor: pointer;">0.00</td>
                                        <td id="AvgBillValueMonth" style="text-align: right; cursor: pointer;">0.00</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="maincontentborder">
                                <h4 class="text-center">Item Wise Bills</h4>
                                <table width='100%'>
                                    <tbody>
                                        <tr>
                                            <th style='width: 28.5%'>Item</th>
                                            <th style='width: 16.9%'>Today</th>
                                            <th style='width: 16.9%'>Percentage</th>
                                            <th style='width: 17.5%'>Month</th>
                                            <th style='width: 19.5%'>PercentageMonth</th>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="BillValueRecord" style="height: 175px; overflow: auto">
                                </div>
                            </div>
                            <div class="maincontentborder">
                                <h4 class="text-center">Cost To Cost Sales</h4>
                                <table width='100%'>
                                    <tbody>
                                        <tr>
                                            <th style='width: 50%'>Item</th>
                                            <th style='width: 27%'>Today Qty</th>
                                            <th>Today Value</th>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="CostToCostSales" style="height: 175px; overflow: auto">
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-6">
                    <div id="CurrentMonthPurchase" class="maincontentborder animation-element slide-top">
                        <canvas id="ctxHourlySales" height="258px"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="maincontentborder animation-element slide-top">
                    <canvas id="ctxBillingRangeValue" height="135px"></canvas>
                </div>
            </div>
            <div class="col-md-6">
                <div class="maincontentborder animation-element slide-top">
                    <canvas id="ctxMarginReport" height="135px"></canvas>
                </div>
            </div>

            <div class="col-md-6">

                <div class="col-md-6">
                    <div class="maincontentborder">
                        <canvas id="ctxSalesDateCompare" height="236px"></canvas>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="maincontentborder">
                        <canvas id="ctxSalesMonthCompare" height="236px"></canvas>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Order By Record</h4>
                        </div>
                        <div class="modal-body">




                            <center>
                                <div style="width: 50%">
                                    <select class="form-control" style="margin-top: 8px" id="ddlFilter">
                                        <option value="Department">Department</option>
                                        <option value="Category">Category</option>
                                        <option value="Brand">Brand</option>
                                    </select>
                                    <button type="button" id="btLoadReport" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                                </div>
                            </center>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="myModalStock" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Stock As On</h4>
                        </div>
                        <div class="modal-body">




                            <center>
                                <div style="width: 50%">
                                    <select class="form-control" style="margin-top: 8px" id="ddlFilterStock">
                                        <option value="Department">Department</option>
                                        <option value="Category">Category</option>
                                        <option value="Brand">Brand</option>
                                    </select>
                                    <button type="button" id="btLoadReportStock" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                                </div>
                            </center>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="myModalMargin" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Margin Report</h4>
                        </div>
                        <div class="modal-body">
                            <center>
                                <div style="width: 50%">
                                    <select class="form-control" style="margin-top: 8px" id="ddlFilterMargin">
                                        <option value="Department">Department</option>
                                        <option value="Category">Category</option>
                                        <option value="Brand">Brand</option>
                                    </select>
                                    <button type="button" id="btLoadReportMargin" style="margin-top: 8px" class="form-control btn btn-primary">Load</button>
                                </div>
                            </center>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="myModalItemValue" role="dialog" width="100%">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ModalItemValueText">Select Filter</h4>
                        </div>
                        <div class="modal-body">

                            <div id="myModalItemValueValueRecord" class="maincontentborder animation-element slide-bottom" style="background-color: white; overflow: auto">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="myModalStockValue" role="dialog" width="100%">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ModalStockValueText">Select Filter</h4>
                        </div>
                        <div class="modal-body">

                            <div id="myModalStockRecord" class="maincontentborder animation-element slide-bottom" style="background-color: white; overflow: auto">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal fade" id="myModalMarginValue" role="dialog" width="100%">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id="ModalMarginText">Select Filter</h4>
                        </div>
                        <div class="modal-body">

                            <div id="myModalMarginRecord" class="maincontentborder" style="background-color: white; overflow: auto">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" Value="0" />
    <asp:HiddenField ID="hidVersion" runat="server" />
    <asp:HiddenField ID="hidLicense" runat="server" />
    <asp:HiddenField ID="hidPageRefresh" runat="server" />
</asp:Content>
