﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInventorySearchTextiles.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventorySearchTextiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'InventorySearchTextiles');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "InventorySearchTextiles";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="updtPnlTax" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
            <div class="main-container" style="overflow: hidden">

                <div id="breadcrumbs" class="breadcrumbs" runat="Server">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Inventory Search Textile</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                        <div class="container well" style="margin-top: 20px;">
                            <div class="form-group ">
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Vendor</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtVendor" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Department</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtDepartment" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Category</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtCategory" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Class</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtClass" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Brand</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtBrand" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Item Code</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtitemCode" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtDescription" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="form-group row col-md-3">
                                    <label for="staticEmail" class="col-sm-3 col-form-label">Remarks</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtRemarks" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                    </div>

                                </div>
                                <asp:Button ID="btClear" Text="Clear" runat="server" Style="float: right" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="container well" style="padding-top: 10px;">
                            <div class="barcode-header">
                                Attributes
                            </div>
                            <div style="height:50px; border:solid">

                            </div>
                            <br />
                            <div class="col-md-1">
                                <asp:CheckBox ID="chkonlytextile" Text="Only Textile" runat="server" />  
                            </div>
                            <div class="col-md-7">
                               
                                <asp:Button ID="btFetch" Text="Fetch" runat="server" Style="float: left" CssClass="btn btn-primary" />
                            </div>
                            <div class="col-md-4">
                                <asp:Button ID="btclear2" Text="Clear" runat="server" Style="float: right" CssClass="btn btn-primary" />
                            </div>
                        </div>
                       
                       <%-- <div class="container well" style="padding-top: 10px;">
                            
                            <div style="height:50px; border:solid">

                            </div>
                            <br />
                            
                            <div class="col-md-12">
                                 <label for="staticEmail" Width="5%" class="col-form-label">Ven SI.No</label>
                                <asp:TextBox ID="TextBox1" Width="5%" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Style Code</label>
                                <asp:TextBox ID="TextBox2"  Width="5%" MaxLength="50" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Qty</label>
                                <asp:TextBox ID="TextBox3" Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Cost Price</label>
                                <asp:TextBox ID="TextBox4"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Disc %</label>
                                <asp:TextBox ID="TextBox5"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Disc Amt</label>
                                <asp:TextBox ID="TextBox6"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">SD %</label>
                                <asp:TextBox ID="TextBox7"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">SD Amt</label>
                                <asp:TextBox ID="TextBox8"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">G.Cost</label>
                                <asp:TextBox ID="TextBox9"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">O.Tax %</label>
                                <asp:TextBox ID="TextBox15"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">OT Amt</label>
                                <asp:TextBox ID="TextBox14"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">GST %</label>
                                <asp:TextBox ID="TextBox12"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Vendor</label>
                                <asp:TextBox ID="TextBox13"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">GST Amt</label>
                                <asp:TextBox ID="TextBox16"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Net Cost</label>
                                <asp:TextBox ID="TextBox17"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Margin</label>
                                <asp:TextBox ID="TextBox18"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">MRP</label>
                                <asp:TextBox ID="TextBox11"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                 <label for="staticEmail" Width="5%" class="col-form-label">Margin SP</label>
                                <asp:TextBox ID="TextBox10"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>
                                <label for="TextBox19" Width="5%" class="col-form-label">S.Price</label>
                                <asp:TextBox ID="TextBox19"  Width="5%" runat="server" CssClass="form-control-plaintext"></asp:TextBox>

                            </div>
                           
                        </div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <hr />
                <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
                    <ContentTemplate>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
