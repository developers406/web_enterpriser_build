﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmInventoryImage.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
           .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtInvCode.ClientID%>").value == "") {
                Show = Show + ' <%=Resources.LabelCaption.Alert_InvCode%>'
                document.getElementById("<%=txtInvCode.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtInvName.ClientID%>").value == "") {
                Show = Show + '\n <%=Resources.LabelCaption.Alert_InvName%>';
                document.getElementById("<%=txtInvName.ClientID %>").focus();
            }

            if (document.getElementById("<%=fuimage.ClientID%>").value == "") {
                Show = Show + '\n <%=Resources.LabelCaption.Alert_InvImage%>';
                document.getElementById("<%=fuimage.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

            else {
                return true;
            }
        }

    </script>
    <script type="text/javascript">
        function pageLoad() {
            //------------------------------------------- Get Item Name ----------------------------------------//
            $("[id$=txtInvCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryImage.aspx/GetFilterValue") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },

                //==========================> After Select Value
                select: function (e, i) {
                    $("[id$=hfInvCode]").val(i.item.val);
                    $("[id$=txtInvCode]").val(i.item.val);
                    $("[id$=txtInvName]").val(i.item.desc);

                    //===========================> Get Batch Details

                    $.ajax({

                        type: "POST",
                        url: "frmInventoryImage.aspx/GetInventory",
                        data: "{'InvCode':'" + i.item.val + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {

                            $('#<%=txtInvName.ClientID %>').val(msg.d[0]);
                            $('#<%=txtBatchNo.ClientID %>').val(msg.d[1]);
                            $('#<%=txtMRP.ClientID %>').val(msg.d[2]);

                            document.getElementById("<%=imgpreview.ClientID%>").src = "data:image/png;base64," + msg.d[3];

                        },
                        error: function (data) {
                            alert('Something Went Wrong')
                        }
                    });

                    return false;
                },

                minLength: 1
            });


        }
    
    </script>
    <script type="text/javascript">
        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'InventoryImage');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "InventoryImage";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Image</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group">
            <div class="container-control">
                <div class="container-left">
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label1" runat="server" Text="Inventory"></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtInvCode" runat="server" CssClass="form-control" placeholder="Search Inventory"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblInventoryName %>'></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtInvName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="container-right">
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Image ID="imgpreview" runat="server" Height="100px" Width="165px" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="lnkSave" />
                                </Triggers>
                                <ContentTemplate>
                                    <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div style="float: right">
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-contol">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                        OnClientClick="return ValidateForm();">Save</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()">Clear</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
