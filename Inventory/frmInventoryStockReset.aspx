﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmInventoryStockReset.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryStockReset" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 35px;
            max-width: 35px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
            text-align: left;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 235px;
            max-width: 235px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {             /*05122022 musaraf*/
            min-width: 108px;
            max-width: 108px;
            text-align: left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 70px;
            max-width: 70px;
            text-align: right;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
            text-align: right;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
            text-align: right;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 85px;
            max-width: 85px;
            text-align: right;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
            text-align: right;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
            text-align: right;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 75px;
            max-width: 75px;
        }
        .grdLoad{
            padding:2px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            if($("#<%=chkShowBatch.ClientID %>").is(':checked')){
                $("#<%=lnkUpdate.ClientID %>").css("display","none");
            }
            if ($("#<%=hiQuickReset.ClientID %>").val().trim() == "Y") {
                $("#divQuickReset").show(); 
                $("#divQuick").show();
            }
            else
            {
                $("#divQuickReset").hide();
                $("#divQuick").hide();
            }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc 
            $("label[for='ContentPlaceHolder1_chkQuickReset']").css("font-weight", "900");
            $("label[for='ContentPlaceHolder1_chkQuickReset']").css("margin-left", "10px");
            $("#<%=chkQuickReset.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#divQuickReset").show();
                    $("#divHidebtn").hide(); 
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    fncHideFilter();
                    $('#<%=gvInvStockReset.ClientID%>').remove();                     
                }
                else {
                    $("#divQuickReset").hide();
                    $("#divHidebtn").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    fncHideFilter();
                }
            });

            $(function () {
                $("#dialog-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Yes": function () { 
                           // __doPostBack('ctl00$ContentPlaceHolder1$btnUpdate', ''); 
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkUpdate', '');
                            $(this).dialog("close");
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        }
    </script>
    <script type="text/javascript">
        function fncClickResetInv() {
            $("#dialog-confirm").dialog("open");
            return false;
        }
    </script>
    <script type="text/javascript">

        function HideClick() {
            $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
            fncHideFilter();
        }
        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") { 
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '100%',
                        'margin-left': '0'
                    });
                    $("select").trigger("liszt:updated");

                    $('.grdLoad td:nth-child(3)').css("min-width", "590px");
                    $('.grdLoad td:nth-child(3)').css("max-width", "590px");
                    $('.grdLoad td:nth-child(3)').css("text-align", "left");
                    $('.grdLoad th:nth-child(3)').css("min-width", "590px");
                    $('.grdLoad th:nth-child(3)').css("max-width", "590px");
                    $('.grdLoad th:nth-child(3)').css("text-align", "left");
                   
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '74%',
                        'margin-left': '1%'
                    });
                    $("select").trigger("liszt:updated");

                    $('.grdLoad td:nth-child(3)').css("min-width", "235px");
                    $('.grdLoad td:nth-child(3)').css("max-width", "235px");
                    $('.grdLoad td:nth-child(3)').css("text-align", "left");
                    $('.grdLoad th:nth-child(3)').css("min-width", "235px");
                    $('.grdLoad th:nth-child(3)').css("max-width", "235px");
                    $('.grdLoad th:nth-child(3)').css("text-align", "left");
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
    </script>
    <script type="text/javascript">
        function chkSingle_Click(objRef) {
            //Get the Row based on checkbox
            var row = objRef.parentNode.parentNode;
            if (objRef.checked) {
                //If checked change color to Aqua
                row.style.backgroundColor = "aqua";
            }
            else {
                //If not checked change back to original color
                if (row.rowIndex % 2 == 0) {
                    //Alternating Row Color
                    row.style.backgroundColor = "#c4ddff";
                }
                else {
                    row.style.backgroundColor = "#edf1fe";
                }
            }
            //Get the reference of GridView
            var GridView = row.parentNode;

            //Get all input elements in Gridview
            var inputList = GridView.getElementsByTagName("input");

            //for (var i = 0; i < inputList.length; i++) {
            //    //The First element is the Header Checkbox
            //    var headerCheckBox = inputList[0];
            //    //Based on all or none checkboxes
            //    //are checked check/uncheck Header Checkbox
            //    var checked = true;
            //    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
            //        if (!inputList[i].checked) {
            //            checked = false;
            //            break;
            //        }
            //    }
            //}
              
        }
    </script>
    <script type="text/javascript">
        function chkAll_Click(objRef) {  
            try {
                if (($(objRef).is(":checked"))) {
                    $("#<%=gvInvStockReset.ClientID %> tbody tr").each(function () {
                        $(this).find('td input[id*="ChkSingle"]').attr("checked", "checked");
                    }); 
                }
                else { 
                    $("#<%=gvInvStockReset.ClientID%> input[id*='ChkSingle']:checkbox").removeAttr("checked"); 
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
         
            //var GridView = objRef.parentNode.parentNode.parentNode;
            //var inputList = GridView.getElementsByTagName("input");
            //for (var i = 0; i < inputList.length; i++) {
            //    //Get the Cell To find out ColumnIndex
            //    var row = inputList[i].parentNode.parentNode;
            //    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
            //        if (objRef.checked) {
            //            //If the header checkbox is checked
            //            //check all checkboxes
            //            //and highlight all rows
            //            row.style.backgroundColor = "aqua";
            //            inputList[i].checked = true;
            //        }
            //        else {
            //            //If the header checkbox is checked
            //            //uncheck all checkboxes
            //            //and change rowcolor back to original 
            //            if (row.rowIndex % 2 == 0) {
            //                //Alternating Row Color
            //                row.style.backgroundColor = "#c4ddff";
            //            }
            //            else {
            //                row.style.backgroundColor = "#edf1fe";
            //            }
            //            inputList[i].checked = false;
            //        }
            //    }
            //}
        }
    </script>
    <script type="text/javascript">
        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "orange";
            }
            else {
                if (checkbox.checked) {
                    objRef.style.backgroundColor = "aqua";
                }
                else if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        objRef.style.backgroundColor = "#C4DDFF";
                    }
                    else {
                        objRef.style.backgroundColor = "#edf1fe";
                    }
                }
            }
        }

        $(document).ready(function () {
            $("#<%=chkQuickReset.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    $("#divQuickReset").show();
                    $("#divHidebtn").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    fncHideFilter();
                    $('#<%=gvInvStockReset.ClientID%>').remove(); 
                }
                else {
                    $("#divQuickReset").hide();
                    $("#divHidebtn").show();
                    fncHideFilter();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                }
            });
        });
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'InventoryStockReset');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "InventoryStockReset";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a>Inventory</a> <i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Inventory Stock Reset</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                            EnableVendorDropDown="true"
                            EnableDepartmentDropDown="true"
                            EnableCategoryDropDown="true"
                            EnableSubCategoryDropDown="true"
                            EnableBrandDropDown="true"
                            EnableMerchandiseDropDown="true"
                            EnableManufactureDropDown="true"
                            EnableFloorDropDown="true"
                            EnableSectionDropDown="true"
                            EnableShelfDropDown="true"
                            EnableWarehouseDropDown="true"
                            EnableItemTypeDropDown="true"
                            EnableItemCodeTextBox="true"
                            EnableItemNameTextBox="true"
                            EnableFilterButton="true"
                            EnableClearButton="true"
                            OnClearButtonClientClick="clearForm(); return false;"
                            OnFilterButtonClick="lnkLoadFilter_Click" />
                    </div>
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                        style="height: 500px">
                        <div class="control-group-split" style="width: 40%; float: right">
                            <div class="control-group-left">
                                <div class="label-left" style="width: 80%">
                                    <asp:Label ID="Label31" runat="server" Text="Show Qty in Package"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <fieldset>
                                        <asp:CheckBox ID="chkShowQty" runat="server" AutoPostBack="true" OnCheckedChanged="chkShowQty_CheckedChanged" />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left" style="width: 80%">
                                    <asp:Label ID="Label32" runat="server" Text="Show Batch Wise"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 20%">
                                    <asp:CheckBox ID="chkShowBatch" runat="server" AutoPostBack="true" OnCheckedChanged="chkShowBatch_CheckedChanged" />
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="grdLoad">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th scope="col">Item Code
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Batch No
                                            </th>
                                            <th scope="col">MRP 
                                            </th>
                                            <th scope="col">Selling Price
                                            </th>
                                            <th scope="col">Net Cost
                                            </th>
                                            <th scope="col">Average Cost
                                            </th>
                                            <th scope="col">Qty on Hand     
                                            </th>
                                            <th scope="col">Stock Value
                                            </th>
                                            <th scope="col">
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="chkAll_Click(this);" />
                                            </th>
                                        </tr>
                                    </thead>
                                </table> 

                                <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                                    style="height: 400px;">
                                    <asp:GridView ID="gvInvStockReset" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                        CssClass="pshro_GridDgn" OnRowDataBound="RowDataBound" ShowHeader ="false">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:BoundField DataField="RowNumber" HeaderText="S.No"></asp:BoundField>
                                            <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                            <asp:BoundField DataField="ShortDescription" HeaderText="Description"></asp:BoundField>
                                            <asp:BoundField DataField="BatchNo" HeaderText="Batch No" />
                                            <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                            <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                            <asp:BoundField DataField="NetCost" HeaderText="Net cost"></asp:BoundField>
                                            <asp:BoundField DataField="AverageCost" HeaderText="Average Cost"></asp:BoundField>
                                            <asp:BoundField DataField="QtyOnHand" HeaderText="Qty On Hand"></asp:BoundField>
                                            <asp:BoundField DataField="StockValue" HeaderText="Stock Value"></asp:BoundField>
                                            <asp:TemplateField>
                                                 <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server"  />
                                        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkSingle" runat="server" onclick="chkSingle_Click(this)" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            
                                </div>
                             </div>
                        <div class="control-button" id="divHidebtn">
                            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue"
                                OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                        </div>
                        <div class="control-button" id="divQuick" style="margin-left: 45%;">
                            <asp:CheckBox ID="chkQuickReset" runat="server"  Text="Quick Reset" />
                        </div>
                        <div id="divQuickReset" style="display: none;">
                            <div class="control-button" style="margin-left: 2%;">
                                <asp:RadioButton ID="rdoBatch" runat="server" GroupName="Inventory" Text="All Batch Reset"
                                    Class="radioboxlist" Checked="true" />
                            </div>
                            <div class="control-button" style="margin-left: 2%;">
                                <asp:RadioButton ID="rdoNonBatch" runat="server" GroupName="Inventory" Text="All Non Batch Reset"
                                    Class="radioboxlist" />
                            </div>
                        </div>
                       
                    </div>
                    <div id="dialog-confirm" title="Enterpriser">
                        <p>
                            <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 4px 20px 0;"></span>Do You want to reset stock for the selected items ?
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     <div style="float: right; display: table">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkUpdate" runat="server" Text="Reset Stock" class="button-blue"
                                   OnClick="btnUpdate_Click" /> <%--OnClientClick="return fncClickResetInv();" --%>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                    OnClientClick="clearForm();" OnClick="lnkClearAll_Click"></asp:LinkButton>
                            </div>
                        </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdate">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'>
                </asp:LinkButton>
            </div>
        </div>
    </div>
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hiQuickReset" runat="server" />
            <asp:HiddenField ID="hidButtonClick" runat="server" />
</asp:Content>
