﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCategoryAttributeMapping.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmCategoryAttributeMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .attr_fixed_headers {
            width: 420px;
            overflow: auto;
        }

        .gid_LastPurchase td:nth-child(1), .gid_LastPurchase th:nth-child(1) {
            min-width: 30px;
            max-width: 30px;
        }

        .gid_LastPurchase td:nth-child(2), .gid_LastPurchase th:nth-child(2) {
            min-width: 300px;
            max-width: 300px;
        }

        .gid_LastPurchase td:nth-child(3), .gid_LastPurchase th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(4), .gid_LastPurchase th:nth-child(4) {
            min-width: 30px;
            max-width: 30px;
        }

        #grdAttributeValue {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeValue td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeValue tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

            #grdAttributeValue tr:hover {
                background-color: lightpink;
                font-weight: bolder;
            }

            #grdAttributeValue th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: red;
                font-weight: bolder;
                color: white;
            }
              .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            fncClear();
            <%--  setTimeout(function () {
                $('#<%=ddlCategory.ClientID %>').trigger("liszt:open");
            }, 50);--%>

            $("[id$=ddlCategory]").change(function () {
                var selectedVal = $("[id$=ddlCategory]").val();
                //alert(selectedVal);
                fncGetAttributes('Mapping', selectedVal);
            });
       
            $('input[type=text]').bind('change', function () {
                if (this.value.match(/[^a-zA-Z0-9-. ]/g)) {

                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(/[^a-zA-Z0-9-. ]/g, '');
                }
            });
        
        }
        function XmlGridValue(tableid, XmlID) {
            try {

                var xml = '<NewDataSet>';

                // $('#grdAttribute tr').each(function () {
                tableid.each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().indexOf('Select') != -1) {
                                //alert($("input:checkbox", cells.eq(j)).val());
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:checkbox", cells.eq(j)).is(':checked') + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }
                            else if ($(this).parents('table:first').find('th').eq(j).text().indexOf('Sort') != -1) {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:text", cells.eq(j)).val() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }
                            else {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }

                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                console.log(xml);
                //alert(xml);

                XmlID.val(escape(xml));
                //alert(XmlID.val());
                //$("#hiddenXml").val($("[id*=txtPoNumber]").val());
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function ValidateForm() {

            var bCheckItemCode = false;
             
            $('#grdAttributeValue tr').each(function () {
                var cells = $("td", this);
                if (cells.length > 0) {
                    
                    for (var j = 0; j < cells.length; ++j) {

                        if ($(this).parents('table:first').find('th').eq(j).text().indexOf('Select') != -1) {

                            var code = cells.eq(2).text().trim();
                            debugger;
                            if(code == 'SLEEVE')
                            {
                                if ($("input:checkbox", cells.eq(j)).is(':checked') == true)
                                    bCheckItemCode = true;
                            }
                            if (code == 'SUB CATEGORY')
                            {
                                if ($("input:checkbox", cells.eq(j)).is(':checked') == true)
                                    bCheckItemCode = true;
                            }
                        }

                    } 
                }
            });

            //alert(bCheckItemCode);

            if (bCheckItemCode == false) { 
                ShowPopupMessageBoxandOpentoObject('Please select SubCategory / Sleeve !');
                return false;
            }

            if ($('#<%=ddlCategory.ClientID %>').val() == '') {
                popUpObjectForSetFocusandOpen = $('#<%=ddlCategory.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please select Product Name!');
                return false;
            }
            else if ($('#<%=txtProductCost.ClientID %>').val() <= 0) {
                popUpObjectForSetFocusandOpen = $('#<%=txtProductCost.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please enter Product Cost!');
                return false;
            }
            else if ($('#<%=txtMrp.ClientID %>').val() <= 0) {
                popUpObjectForSetFocusandOpen = $('#<%=txtMrp.ClientID %>');
                ShowPopupMessageBoxandOpentoObject('Please enter Mrp!');
                return false;
            }


            else {

                XmlGridValue($('#grdAttributeValue tr'), $('#<%=hiddenXml.ClientID %>'));
                return true;
            }
}

// Get Attributes
function fncGetAttributes(mode, attributecode) {
    var obj = {};
    try {
        obj.mode = mode;
        obj.attributecode = attributecode;

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("~/Inventory/frmCategoryAttributeMapping.aspx/fncGetAttributes") %>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                funBindAttributes(jQuery.parseJSON(msg.d));
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

/// Bind Attributes
function funBindAttributes(jsonObj) {

    try {

        if (jsonObj == null)
            return;

        ////Creating table rows 
        var table1 = document.getElementById("grdAttributeValue");
        $('#grdAttributeValue').find("tr:gt(0)").remove();
        console.log(jsonObj);
        var obj;
        for (var i = 0; i < jsonObj.length; i++) {
            if (jsonObj[i].ProductCost > 0) {
                obj = jsonObj[i];
                break;
            }
        }
        //console.log(obj);
        $(obj).each(function (index, element) {
            //console.log(element.ProductCost);
            $('#<%=txtProductCost.ClientID%>').val(element.ProductCost);
            $('#<%=txtMrp.ClientID%>').val(element.MRP);
            $('#<%=txtMiscelenious.ClientID%>').val(element.Miscelenious);
            $('#<%=txtMargin.ClientID%>').val(element.Margin);
            $('#<%=txtGSTPerc.ClientID%>').val(element.GSTPerc);
            $('#<%=txtGSTAmt.ClientID%>').val(element.GSTAmt);
            $('#<%=txtProductCost.ClientID%>').focus().select();
        });

        $(jsonObj).each(function (index, element) {
            //alert('id: ' + element.ValueCode + ', name: ' + element.ValueName);
            var newRow = table1.insertRow(table1.length),
                    cell1 = newRow.insertCell(0),
                    cell2 = newRow.insertCell(1),
                    cell3 = newRow.insertCell(2),
                    cell4 = newRow.insertCell(3);

            cell2.className = "hiddencol";
            cell1.style = "text-align:center";
            cell4.style = "text-align:center";
            // add values to the cells  
            cell1.innerHTML = "<input type='text' style='width:100%; text-align:center;' value='" + element.Sortcode + "' name='text-tab1'>";
            cell2.innerHTML = element.AttributeCode;
            cell3.innerHTML = element.AttributeName;

            var AttributeName = element.AttributeName.replace(" ", "");

            if (element.Active)
                cell4.innerHTML = "<input type='checkbox' onchange='doalert(this);' id='" + AttributeName + "' name='" + element.AttributeName + "' checked>";
            else
                cell4.innerHTML = "<input type='checkbox' onchange='doalert(this);' id='" + AttributeName + "'  name='" + element.AttributeName + "' >";

            //console.log(element.Active);
        });


    }
    catch (err) {
        ShowPopupMessageBox(err.message)
    }
}


function doalert(checkboxElem) {
    var rowobj = $(checkboxElem);
    //alert(rowobj.attr('name'))
    var check = false;
    var Name = rowobj.attr('name')
    debugger;

    if (Name == 'SUB CATEGORY') {         
        if ($("#SLEEVE").is(":checked")) { 
            rowobj.attr('checked', false);
            ShowPopupMessageBox("Sleeve already Selected !");
        } 
    }
    if (Name == 'SLEEVE') { 
        if ($("#SUBCATEGORY").is(":checked")) {         
            rowobj.attr('checked', false);
            ShowPopupMessageBox("Sub Category already Selected !");
        }
    }
}

function fncClear() {
    $('#<%=txtProductCost.ClientID%>').val('0');
    $('#<%=txtMrp.ClientID%>').val('0');
    $('#<%=txtMiscelenious.ClientID%>').val('0');
    $('#<%=txtMargin.ClientID%>').val('0');
    $('#<%=txtGSTPerc.ClientID%>').val('0');
    $('#<%=txtGSTAmt.ClientID%>').val('0');
    $('#<%=txtProductCost.ClientID%>').focus().select();
}

function fncCalculateMrp() {

    var productcost = 0;
    var miscelenious = 0;
    var margin = 0;
    var marginAmt = 0;
    var gstperc = 0;
    var gstamt = 0;
    var mrp = 0;

    var objProductcost = $('#<%=txtProductCost.ClientID%>');
    var objMrp = $('#<%=txtMrp.ClientID%>');
    var objMiscelenious = $('#<%=txtMiscelenious.ClientID%>');
    var objMargin = $('#<%=txtMargin.ClientID%>');
    var objGSTperc = $('#<%=txtGSTPerc.ClientID%>');
    var objGSTamount = $('#<%=txtGSTAmt.ClientID%>');

    productcost = objProductcost.val();
    miscelenious = objMiscelenious.val();
    margin = objMargin.val();

    miscelenious = parseFloat(productcost) + parseFloat(miscelenious)
    marginAmt = (miscelenious * parseFloat(margin)) / 100;
    marginAmt = marginAmt + miscelenious;

    if (marginAmt > 1000) {
        objGSTperc.val('12.00');
        gstamt = (marginAmt * 12) / 100;
        objGSTamount.val(gstamt.toFixed(2));
        mrp = gstamt + marginAmt;
        objMrp.val(mrp.toFixed(2));
    }
    else {
        objGSTperc.val('5.00');
        gstamt = (marginAmt * 5) / 100;
        objGSTamount.val(gstamt.toFixed(2));
        mrp = gstamt + marginAmt;
        objMrp.val(mrp.toFixed(2));
    }

    console.log(marginAmt);

}

    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'CategoryAttributeMapping');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CategoryAttributeMapping";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">

        <div id="breadcrumbs" class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Category Attribute Mapping </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="col-md-4" style="padding: 10px 20px 20px 10px;">
            <div class="container-group-small">
                <div class="container-control">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label8" runat="server" Text="Product Name"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="DropDownList1" class="chzn-select" CssClass="chzn-select" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">--%>
                            <asp:DropDownList ID="ddlCategory" class="chzn-select" CssClass="chzn-select" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="attr_fixed_headers gid_LastPurchase">
                    <table id="grdAttributeValue" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">Sort
                                </th>
                                <th scope="col" class="hiddencol">AttributeCode
                                </th>
                                <th scope="col">AttributeName
                                </th>
                                <th scope="col">Select
                                </th>
                            </tr>
                        </thead>
                    </table>
                    <%-- <asp:GridView ID="grdGroupCreation" runat="server" Width="100%" AutoGenerateColumns="false"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe"
                                CssClass="pshro_GridDgn">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="AttributeCode" HeaderText="AttributeCode" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="hiddencol" />
                                    <asp:BoundField DataField="AttributeName" HeaderText="Attribute Name" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label39" runat="server" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" Checked='<%# Eval("Active") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>--%>
                </div>
            </div>
        </div>

        <div class="col-md-4" style="padding: 10px 10px 0px 10px;">
            <div class="container-group-small" style="width: 70%;">
                <div class="container-control">
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="blss" runat="server" Text="Product Cost"></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtProductCost" runat="server" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text="Miscelenious"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMiscelenious" runat="server" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text="Margin %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMargin" runat="server" oninput="fncCalculateMrp()" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label5" runat="server" Text="GST %"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTPerc" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label1" runat="server" Text="GST Amount"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGSTAmt" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label6" runat="server" Text="MRP"></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtMrp" runat="server" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>

                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClientClick="return ValidateForm();" OnClick="lnkSave_Click"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" OnClientClick="fncClear(); return false;" class="button-red"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" Style="display: none" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="hiddenXml" runat="server" Value="" />

    </div>
</asp:Content>
