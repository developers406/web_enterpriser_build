﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCompanySettings.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmCompanySettings" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <style type="text/css">
         .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
             min-width: 70px;
             max-width: 70px;
             text-align: center !important;
             padding-right: 10px !important;
         }

         .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
             min-width: 92px;
             max-width: 92px;
             text-align: center !important;
             padding-right: 10px !important;
         }
          .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
             min-width: 104px;
             max-width: 104px;
             text-align: center !important;
             padding-right: 10px !important;
         }

         .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
             min-width: 112px;
             max-width: 112px;
         }

         .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
             min-width: 300px;
             max-width: 300px;
         }

         .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
             min-width: 141px;
             max-width: 141px;
         }

         .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
             display: none;
         }

         .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
             display: none;
         }

         .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
             display: none;
         }

         .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
             display: none;
         }

         .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
             min-width: 155px;
             max-width: 155px;
         }

         .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
             min-width: 183px;
             max-width: 183px;
         }

         .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
             min-width: 155px;
             max-width: 155px;
         }
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
           .no-close .ui-dialog-titlebar-close{
            display:none;
        }

     </style>
    <script type="text/javascript">
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupCCode').dialog('close');
                window.parent.$('#popupCCode').remove();

            }
        });
        function ValidateForm() {
             if ($('#<%=txtCompanyCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Company");
                return false;
            }
            if (!($('#<%=txtCompanyCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Company");
                return false;
            }
            var Show = '';
            var CompanyCode = $.trim($('#<%=txtCompanyCode.ClientID %>').val());         //22112022 musaraf
            var CashierName = $.trim($('#<%=txtCompanyName.ClientID %>').val());

            if (CompanyCode == "") {
                Show = Show + 'Please Enter Company Code';
                document.getElementById("<%=txtCompanyCode.ClientID %>").focus();
            }

            if (CashierName == "") {
                Show = Show + '<br />Please Enter Cashier Name';
                document.getElementById("<%=txtCompanyName.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtinvoiceno.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Invoice No';
                document.getElementById("<%=txtinvoiceno.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

       

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");


        });

        function DisableCopyPaste(e) {
            var kCode = event.keyCode || e.charCode;
            if (kCode == 17 || kCode == 2) {

                return false;
            }
        }
        function imgbtnEdit_ClientClick(source) {
              if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Company");
                return false;
            }
            DisplayDetails($(source).closest("tr"));

        }

        function DisplayDetails(row) {
            $('#<%=txtCompanyCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtCompanyCode.ClientID %>').val($("td", row).eq(3).html().replace(/&nbsp;/g, ''));
            $('#<%=txtCompanyName.ClientID %>').val($("td", row).eq(4).html().replace(/&nbsp;/g, ''));
            $('#<%=txttinno.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
            $('#<%=txtCstno.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
            var dateAr = $("td", row).eq(7).html().substring(0, 10).replace(/&nbsp;/g, '');
            $('#<%=txtDate.ClientID %>').datepicker("setDate", dateAr).datepicker({ dateFormat: "dd/mm/yy" });
            $('#<%=txtinvoiceno.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));
            $('#<%=txtremarks.ClientID %>').val($("td", row).eq(9).html().replace(/&nbsp;/g, ''));

            if ($("td", row).eq(10).html().replace(/&nbsp;/g, '') == "1") {
                $('#<%= chkStatus.ClientID %>').attr("checked", "checked");
            }
            else {

                $('#<%= chkStatus.ClientID %>').removeAttr("checked");
            }

        }
        function Company_Delete(source) {
             if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Company");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, '')); 
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
               }
               else {
                   $('#<%=lnkSave.ClientID %>').css("display", "none");
               }
            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });


            $('#<%=txtCompanyCode.ClientID %>').focus();
            $('#<%=txtCompanyCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmCompanySettings.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtCompanyCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {
                            $('#<%=txtCompanyCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtCompanyCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtCompanyName.ClientID %>').val(strarray[1]);
                            $('#<%=txttinno.ClientID %>').val(strarray[2]);
                            $('#<%=txtCstno.ClientID %>').val(strarray[3]);
                            var dateAr = $.trim(strarray[4]).substring(0, 10).replace(/&nbsp;/g, '');

                            $('#<%=txtDate.ClientID %>').datepicker("setDate", dateAr).datepicker({ dateFormat: "dd/mm/yy" });
                            $('#<%=txtinvoiceno.ClientID %>').val(strarray[5]);
                            if ($.trim(strarray[6]).replace(/&nbsp;/g, '') == "1") {
                                $('#<%= chkStatus.ClientID %>').attr("checked", "checked");
                            }
                            else {

                                $('#<%= chkStatus.ClientID %>').removeAttr("checked");
                            }
                            $('#<%=txtremarks.ClientID %>').val(strarray[7]);



                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtCompanyName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmCompanySettings.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {

                    $('#<%=txtManufacturerName.ClientID %>').val($.trim(i.item.label));

                    event.preventDefault();
                },--%>
                select: function (e, i) {

                    $('#<%=txtCompanyName.ClientID %>').val($.trim(i.item.label));

                    return false;
                },
                minLength: 1
            });

                $("[id$=txtSearch]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmCompanySettings.aspx/GetFilterValue",
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },

                    focus: function (event, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                         __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                         return false;
                     },

                     minLength: 1
                });
           
                $('input[type=text]').bind('change', function () {
                    if (this.value.match(/[^a-zA-Z0-9-,/ ]/g)) {

                        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                        this.value = this.value.replace(/[^a-zA-Z0-9-,/ ]/g, '');
                    }
                });
           
                 }
                 function isNumberKey(evt) {
                     var charCode = (evt.which) ? evt.which : evt.keyCode;
                     if (charCode > 31 && (charCode < 48 || charCode > 57))
                         return false;
                     return true;
                 }
                 function disableFunctionKeys(e) {
                     var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                     if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                         if (e.keyCode == 115) {
                              if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                             $('#<%= lnkSave.ClientID %>').click();
                                e.preventDefault();
                            }
                            else if (e.keyCode == 117) {
                                $("input").removeAttr('disabled');
                                $('#<%= lnkClear.ClientID %>').click();
                            e.preventDefault();
                        }
                     <%--    else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                        }
                    };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');

            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");

        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'CompanySetting');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "CompanySetting";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="main-container" style="overflow:hidden">
          <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Company Settings</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
        <br />
          <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p ><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                
          <div class="container-group">
            <div class="container-control">
                <div class="container-left">
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblCompanyCode %>'></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtCompanyCode"  MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label2" runat="server"  Text='<%$ Resources:LabelCaption,lblCompanyName %>'></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div style="float: right">
                            <asp:TextBox ID="txtCompanyName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblTinNo %>'></asp:Label>
                        </div>
                        <div style="float: right"">
                            <asp:TextBox ID="txttinno" MaxLength="25" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lblCstNo %>'></asp:Label>
                        </div>
                        <div style="float: right"">
                            <asp:TextBox ID="txtCstno" MaxLength="25" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    </div>
                    <div class="container-Right">
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'></asp:Label>
                        </div>
                        <div style="float: right"">
                            <asp:TextBox ID="txtDate" onkeypress="return false" nMouseDown="return DisableCopyPaste (event)" oncopy="return false" onpaste="return false" oncut="return false" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblInvoiceNo %>'></asp:Label><span class="mandatory">*</span>
                        </div>
                        <div style="float: right"">
                            <asp:TextBox ID="txtinvoiceno" MaxLength="18" onkeypress="return isNumberKey(event)" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                        </div>
                        <div style="float: right"">
                            <asp:TextBox ID="txtremarks" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblStatus %>'></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 93px">
                            <asp:CheckBox ID="chkStatus" runat="server" />
                            &nbsp;&nbsp; Active
                        </div>
                    </div>
                    </div>
               
            </div>
            <div class="button-contol">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click" 
                        OnClientClick="return ValidateForm()">Save(F4)</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()" 
                       ><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                </div>
                <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display:none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
            </div>
        </div>
          
        
         
        </ContentTemplate>
    </asp:UpdatePanel>
            <hr />

      <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
        <ContentTemplate>
         
        <div class="GridDetails">
            <div class="grid-search">
                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Company Name To Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                        Text='<%$ Resources:LabelCaption,btn_Search %>' OnClick="lnkSearchGrid_Click"><i class="icon-play"></i></asp:LinkButton>
                <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                </asp:Panel>
            </div>
            <br />
                    
             <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit
                                                    </th>
                                                     <th scope="col">Serial No
                                                    </th>
                                                    <th scope="col">Company Code
                                                    </th>
                                                    <th scope="col">Company Name
                                                    </th>
                                                    <th scope="col">GSTIN No
                                                    </th>
                                                    <th scope="col">CstNo
                                                    </th>
                                                    <th scope="col">CstDate
                                                    </th>
                                                    <th scope="col">InvoiceNo
                                                    </th>
                                                    <th scope="col">Remarks
                                                    </th>
                                                    <th scope="col">Status 
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1330px; background-color: aliceblue;">
                    <asp:GridView ID="gvCompanySettings" runat="server"  AutoGenerateColumns="False" ShowHeader="false"
                        ShowHeaderWhenEmpty="true" oncopy="return false" DataKeyNames="CompanyCode" CssClass="pshro_GridDgn grdLoad">
                         <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                         </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        
                        <Columns>
                        <asp:TemplateField HeaderText="Delete" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                        ToolTip="Click here to Delete"  OnClientClick = " return Company_Delete(this);  return false;" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                        ToolTip="Click here to edit"  OnClientClick="imgbtnEdit_ClientClick(this);return false;"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                            <asp:BoundField DataField="CompanyCode" HeaderText='<%$ Resources:LabelCaption,lblCompanyCode %>'></asp:BoundField>
                            <asp:BoundField DataField="CompanyName" HeaderText='<%$ Resources:LabelCaption,lblCompanyName %>'></asp:BoundField>
                            <asp:BoundField DataField="TinNo" HeaderText='<%$ Resources:LabelCaption,lblTinNo %>' ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                            <asp:BoundField DataField="CstNo" HeaderText='<%$ Resources:LabelCaption,lblCstNo %>' ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="CstDate" HeaderText='<%$ Resources:LabelCaption,lbl_Data %>' ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="InvoiceNo" HeaderText='<%$ Resources:LabelCaption,lblInvoiceNo %>' ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Remarks" HeaderText='<%$ Resources:LabelCaption,lbl_Remarks %>' ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                            <asp:BoundField DataField="Active" HeaderText='<%$ Resources:LabelCaption,lblStatus %>' ></asp:BoundField>
                            <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                            <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ups:PaginationUserControl runat="server" ID="CompanyPaging" OnPaginationButtonClick="CompanyPaging_PaginationButtonClick"/>
                    </div>
                
        </div>
   <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
         </ContentTemplate>
    </asp:UpdatePanel> 
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>

</asp:Content>

