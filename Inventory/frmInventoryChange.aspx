﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmInventoryChange.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryChange" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link type="text/css" href="../css/invchange.css" rel="stylesheet" />
    <script src="../js/gridviewScroll.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var fromdatectrl;
        var todatectrl;
        var txtPricectrl;

        $(document).ready(function () {

            $("[id*=grdItemDetails] tr").live('click', function (event) {
                //alert('test');
                if ($(event.target).is(':checkbox')) {
                    return;
                }

                var chkboxSelection = $(this).find('input:checkbox')[0].checked;
                if (chkboxSelection == true) {
                    $(this).closest('tr').css("background-color", "white");
                    $(this).find('input:checkbox')[0].checked = false;

                }
                else if (chkboxSelection == false) {
                    $(this).closest('tr').css("background-color", "#acb3a4");
                    $(this).find('input:checkbox')[0].checked = true;

                }
            });

        });
        //$(function () {
        //    $('tr.dataRow').on('click', function () {
        //        alert('test');
        //        var checked = $(this).find('input[id*=checkBox]').prop('checked');
        //        $(this).find('input[id*=checkBox]').prop('checked', !checked);

        //    });
        //});
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                $('#<%=lnkReOrderUpdate.ClientID %>').css("display", "none");
            }
            else {
                $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                $('#<%=lnkReOrderUpdate.ClientID %>').css("display", "none");
            }
            if ($('#<%=hidWholesale.ClientID%>').val() != "Y") {
                //$('.grdLoad td:nth-child(19)').css("display", "none");
                $('.grdLoad th:nth-child(19)').css("display", "none");
                // $('.grdLoad td:nth-child(20)').css("display", "none");
                $('.grdLoad th:nth-child(20)').css("display", "none");
                // $('.grdLoad td:nth-child(21)').css("display", "none");
                $('.grdLoad th:nth-child(21)').css("display", "none");
                // $('.grdLoad td:nth-child(22)').css("display", "none");
                $('.grdLoad th:nth-child(22)').css("display", "none");
                // $('.grdLoad td:nth-child(23)').css("display", "none");
                $('.grdLoad th:nth-child(23)').css("display", "none");
                // $('.grdLoad td:nth-child(24)').css("display", "none");
                $('.grdLoad th:nth-child(24)').css("display", "none");
                $('#<%=HideFilter_GridOverFlow.ClientID %>').css("width", "2470px");
            }
           <%-- else {
                 $('.grdLoad td:nth-child(19)').css("display", "block");
                $('.grdLoad th:nth-child(19)').css("display", "block");
                $('.grdLoad td:nth-child(20)').css("display", "block");
                $('.grdLoad th:nth-child(20)').css("display", "block");
                $('.grdLoad td:nth-child(21)').css("display", "block");
                $('.grdLoad th:nth-child(21)').css("display", "block");
                $('#<%=HideFilter_GridOverFlow.ClientID %>').css("width", "2770px"); 
            }--%>
            var newOption = {};
            var option = '';

            $("#<%=ddlSort.ClientID %>").empty();

            $('#tblItemhistory th').each(function (e) {
                var index = $(this).index();
                var table = $(this).closest('table');
                var val = table.find('.click th').eq(index).text();

                //if ($(this).is(":visible")) {
                option += '<option value="' + val + '">' + val + '</option>';
                //}
            });
            $("#<%=ddlSort.ClientID %>").append(option);
            $("#<%=ddlSort.ClientID %>").prop('selectedIndex', 3);
            $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");
            fncDecimal();


            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            //SetQualifyingItemAutoComplete();
            <%--$('#<%=rdoNewItem.ClientID%>').change(function () {
                if (this.checked) {
                    $('#<%=rdoSalesPer.ClientID%>').removeAttr('checked');
                }
            });

            $('#<%=rdoSalesPer.ClientID%>').change(function () {
                if (this.checked) {
                    $('#<%=rdoNewItem.ClientID%>').removeAttr('checked');
                }
            });--%>
            if ($("#<%=chkFixed.ClientID%>").is(':checked')) { // dinesh
                $('#DivMargin').show();
            }
            else {
                $('#DivMargin').hide();
            }

            if ($("#<%=rdoSalesPer.ClientID%>").is(':checked')) {
                $('#ContentPlaceHolder1_DivSalesPer').show();
            }
            else {
                $('#ContentPlaceHolder1_DivSalesPer').hide();
                $('#ContentPlaceHolder1_DivNew').css("margin-top", "0px");
            }

            if ($("#<%=rdoNewItem.ClientID%>").is(':checked')) {
                $('#ContentPlaceHolder1_DivNew').show();
            }
            else {
                $('#ContentPlaceHolder1_DivNew').hide();
            }

            if ($("#<%=rdoInventory.ClientID%>").is(':checked')) {
                $('#ContentPlaceHolder1_divInventory').show();
                $('#ContentPlaceHolder1_divReorder').hide();
                $('#ContentPlaceHolder1_divActivation').hide();
            }
            else if ($("#<%=rdoReorder.ClientID%>").is(':checked')) {
                $('#ContentPlaceHolder1_divInventory').hide();
                $('#ContentPlaceHolder1_divReorder').show();
                $('#ContentPlaceHolder1_divActivation').hide();
            }
            else if ($("#<%=rdoActivation.ClientID%>").is(':checked')) {
                $('#ContentPlaceHolder1_divInventory').hide();
                $('#ContentPlaceHolder1_divReorder').hide();
                $('#ContentPlaceHolder1_divActivation').show();
            }
            else {
                $('#ContentPlaceHolder1_divInventory').hide();
                $('#ContentPlaceHolder1_divReorder').hide();
                $('#ContentPlaceHolder1_divActivation').hide();
            } //

        $("#tblActivation tbody tr").hover(function () {
            $(this).addClass("selected");
        }, function () {
            $(this).removeClass("selected");
        });

        $('#lblUp').live('click', function (event) {
            $('#lblDown').css("color", "");
            $('#lblUp').css("color", "green");
            var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
            if (columnIndex > 0) {
                var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                if (columnIndex == 1 || columnIndex == 11 || columnIndex == 12 || columnIndex == 13
                     || columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17) {
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text());
                        var nData = $.trim($(n).text());
                        if (pData == "")
                            pData = 0;
                        if (nData == "")
                            nData = 0;
                        return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                    });
                }
                else {
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text().toUpperCase());
                        var nData = $.trim($(n).text().toUpperCase());
                        return pData < nData ? -1 : 1;
                    });
                }
                tdArray.each(function () {
                    var row = $(this).parent();
                    $('#<%=grdItemDetails.ClientID%>').append(row);
                });
            }
            else {
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "");
                ShowPopupMessageBox("Please select Altlest one Filter");
                return false;
            }


        });
        $('#lblDown').live('click', function (event) {
            $('#lblDown').css("color", "green");
            $('#lblUp').css("color", "");
            var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
            var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
            if (columnIndex > 0) {

                if (columnIndex == 1 || columnIndex == 11 || columnIndex == 12 || columnIndex == 13
                     || columnIndex == 14 || columnIndex == 15 || columnIndex == 16 || columnIndex == 17) {
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text());
                        var nData = $.trim($(n).text());
                        if (pData == "")
                            pData = 0;
                        if (nData == "")
                            nData = 0;
                        return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                    });
                }
                else {
                    tdArray.sort(function (p, n) {
                        var pData = $.trim($(p).text().toUpperCase());
                        var nData = $.trim($(n).text().toUpperCase());
                        return pData > nData ? -1 : 1;
                    });
                }
                tdArray.each(function () {
                    var row = $(this).parent();
                    $('#<%=grdItemDetails.ClientID%>').append(row);
                });
            }
            else {
                $('#lblDown').css("color", "");
                $('#lblUp').css("color", "");
                ShowPopupMessageBox("Please select Altlest one Filter");
                return false;
            }

        });

        $('#<%=chkSelectAllRight.ClientID %>').click(function () {
                try {
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkGRN.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkEstimate.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkTransfer.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisGrn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisPo.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=ChkPriceWList.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkAllowNeg.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkloyality.ClientID %>').prop('checked', $(this).prop("checked"));
                    }
                    else {
                        $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkGRN.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkEstimate.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkTransfer.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisGrn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisPo.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=ChkPriceWList.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkAllowNeg.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkloyality.ClientID %>').prop('checked', $(this).prop("checked"));
                    }
                }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        });
            $('#<%=chkActivation.ClientID %>').click(function () {
                var checked = $(this).is(':checked');
                if (checked == true) {
                    fncActivationClick();
                }
                else {
                    $("#divActSet").dialog("close");
                }
            });

            $('.ui-dialog-titlebar-close');/*.css('visibility', 'hidden')*/    //musaraf 17112022
        }


        function fncHideFilter() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {

                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                $('#<%=lnkHide.ClientID%>').html("Show Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidth');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
                    $("select").trigger("liszt:updated");
                }
                else {
                    //alert("check")
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                $('#<%=lnkHide.ClientID%>').html("Hide Filter");
                    //$("#<%=HideFilter_ContainerRight.ClientID%>").addClass('divwidthShow');
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //        $(function () {

   <%-- //            fromdatectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.FromDateTextBox).ClientID %>");
        //            todatectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ToDateTextBox).ClientID %>");
        //            txtPricectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.PriceTextBox).ClientID %>");--%>

        //            //console.log(fromdatectrl.parent());
        //            //console.log(todatectrl.parent());
        //            //alert(fromdatectrl);

        //            txtPricectrl.on('keypress', isNumberKey);
        //            fromdatectrl.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });
        //            todatectrl.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });

        //            if (fromdatectrl.val() === '') {
        //                fromdatectrl.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        //            }

        //            if (todatectrl.val() === '') {
        //                todatectrl.datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
        //            }
        //        });

        $(function () {
            // Main Report Date
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            // Sales Performance - Dinesh
            $("#<%= txtSalesFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtSalesToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtSalesFromDate.ClientID %>").val() === '') {
                $("#<%= txtSalesFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtSalesToDate.ClientID %>").val() === '') {
                $("#<%= txtSalesToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            // Margin
            $("#<%= txtNewFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtNewToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtNewFromDate.ClientID %>").val() === '') {
                $("#<%= txtNewFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtNewToDate.ClientID %>").val() === '') {
                $("#<%= txtNewToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            //
        });

        function ValidateForm() {
            <%--//            fromdatectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.FromDateTextBox).ClientID %>");
            //            todatectrl = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ToDateTextBox).ClientID %>");--%>
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    return true;
                }

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtMargin.ClientID%>').number(true, 2);
                $('#<%=txtMRP.ClientID%>').number(true, 2);
                $('#<%=txtBasicCost.ClientID%>').number(true, 2);
                $('#<%=txtBelowCost.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //onkeypress = "return isNumberKey(event)" 
        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function clearForm() {
            try {
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                $(':checkbox, :radio').prop('checked', false);
                $("select").val(0);
                $("select").trigger("liszt:updated");
                $("#ContentPlaceHolder1_rdoInventory").attr('checked', 'checked');
                $("#ContentPlaceHolder1_rdoReplace").attr('checked', 'checked');
                $('#<%=grdItemDetails.ClientID%>').remove();
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
        }
        function clearFormFilter() {
            try {
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                $(':checkbox, :radio').prop('checked', false);
                $("select").val(0);
                $("select").trigger("liszt:updated");
                $("#ContentPlaceHolder1_rdoInventory").attr('checked', 'checked');
                $("#ContentPlaceHolder1_rdoReplace").attr('checked', 'checked');
                $('#<%=grdItemDetails.ClientID%>').remove();
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
        }
        <%--function SetQualifyingItemAutoComplete() {
            $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {

                    $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {

                    $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));

                    return false;
                },
                minLength: 1
            });
            }--%>
        function checkAll(objRef) {
            try {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    //Get the Cell To find out ColumnIndex
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            //If the header checkbox is checked
                            //check all checkboxes
                            //and highlight all rows
                            row.style.backgroundColor = "#959382";
                            inputList[i].checked = true;
                        }
                        else {
                            //If the header checkbox is checked
                            //uncheck all checkboxes
                            //and change rowcolor back to original 
                            if (row.rowIndex % 2 == 0) {
                                //Alternating Row Color
                                row.style.backgroundColor = "#c4ddff";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function Check_Click(objRef, val) {

            try {
                //Get the Row based on checkbox
                var row = objRef.parentNode.parentNode;
                if (val == 'row') {
                    objRef.checked = true;
                }
                if (objRef.checked) {
                    //If checked change color to Aqua
                    row.style.backgroundColor = "#acb3a4";
                }
                else {
                    //If not checked change back to original color
                    if (row.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        row.style.backgroundColor = "#c4ddff";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                }
                //Get the reference of GridView
                // var GridView = row.parentNode;

                //Get all input elements in Gridview
                //var inputList = GridView.getElementsByTagName("input");

                //for (var i = 0; i < inputList.length; i++) {
                //    //The First element is the Header Checkbox
                //    var headerCheckBox = inputList[1];
                //    //Based on all or none checkboxes
                //    //are checked check/uncheck Header Checkbox
                //    var checked = true;
                //    if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                //        if (!inputList[i].checked) {
                //            checked = false;
                //            break;
                //        }
                //    }
                //}
                //headerCheckBox.checked = checked;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }



        function ShowPopup(name) {
            $("#dialog").dialog({
                title: name,
                width: 700,
                buttons: {
                    OK: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        //Header Row Click
        function fncrptrHeaderclick(source) {
            try {
                if (($(source).is(":checked"))) {
                    $("#tblInventoryDetail [id*=cbrptrrow]").attr("checked", "checked");
                }
                else {
                    $("#tblInventoryDetail [id*=cbrptrrow]").removeAttr("checked");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Repeater Row Click
        function fncrptrRowClick(source) {
            try {
                if ($("#tblInventoryDetail [id*=cbrptrrow]").length == $("#tblInventoryDetail [id*=cbrptrrow]:checked").length) {
                    $("#tblInventoryDetail [id*=cbheader]").attr("checked", "checked");

                }
                else {
                    $("#tblInventoryDetail [id*=cbheader]").removeAttr("checked");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        $(document).ready(function () {

            $('input[type="radio"]').change(function () {
                if ($("#<%=rdoInventory.ClientID%>").is(':checked')) {
                    $('#ContentPlaceHolder1_divInventory').show();
                    $('#ContentPlaceHolder1_divReorder').hide();
                    $('#ContentPlaceHolder1_divActivation').hide();
                }
                else if ($("#<%=rdoReorder.ClientID%>").is(':checked')) {
                    $('#ContentPlaceHolder1_divInventory').hide();
                    $('#ContentPlaceHolder1_divReorder').show();
                    $('#ContentPlaceHolder1_divActivation').hide();
                }
                else if ($("#<%=rdoActivation.ClientID%>").is(':checked')) {
                    $('#ContentPlaceHolder1_divInventory').hide();
                    $('#ContentPlaceHolder1_divReorder').hide();
                    $('#ContentPlaceHolder1_divActivation').show();
                }
                else {
                    $('#ContentPlaceHolder1_divInventory').hide();
                    $('#ContentPlaceHolder1_divReorder').hide();
                    $('#ContentPlaceHolder1_divActivation').hide();
                }
            });
            $('#ContentPlaceHolder1_chkFixed').change(function () {
                if ($(this).is(":checked")) {
                    $('#DivMargin').show();
                }
                else {
                    $('#DivMargin').hide();
                }
            });

            $('#ContentPlaceHolder1_rdoSalesPer').change(function () {
                if ($(this).is(":checked")) {
                    $('#ContentPlaceHolder1_DivSalesPer').show();
                }
                else {
                    $('#ContentPlaceHolder1_DivSalesPer').hide();
                }
            });

            $('#ContentPlaceHolder1_rdoNewItem').change(function () {
                if ($(this).is(":checked")) {
                    $('#ContentPlaceHolder1_DivNew').show();
                }
                else {
                    $('#ContentPlaceHolder1_DivNew').hide();
                }
            });

            $("#<%=checkAll.ClientID %>").click(function () {
                var checked = $(this).is(':checked');
                var row = $("[id*=grdItemDetails]").closest("tr");
                if (checked == true) {
                    $("[id*=grdItemDetails]").find("[id*=CheckBox1]").prop("checked", true);
                    row.css("background-color", "#acb3a4");
                }
                else {
                    $("[id*=grdItemDetails]").find("[id*=CheckBox1]").prop("checked", false);
                    row.css("background-color", "aliceblue");

                }
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblActivation tr').click(function () {
                var rowobj = $(this);
                rowobj.css("background-color", "Yellow");
            });
        });
    </script>
    <script type="text/javascript">
        function fncReOrderSettings() { // dinesh
            //alert('dinesh');
            var page = '<%=ResolveUrl("~/Inventory/frmReorderSettings.aspx") %>';
            //var page = page + "?ItemCode=" + inv + "&BatchNo=" + sBatchNo
            var $dialog = $('<div id="popupBatchInfo" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 670,
                width: 1230,
                title: "ReOrder Settings",
                closeOnEscape: true
            });
            $dialog.dialog('open');
        }
    </script>
    <script type="text/javascript">
        function fncValidateForm() {
            try {
                var Show = '';
                Show = Show + '\n  Choose ReOrder Process';
                if ($("#<%= txtReorder.ClientID %>").val() == "") {
                    $("#<%= txtReorder.ClientID %>").focus();
                }
                else if ($("#<%= txtAutoPo.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose AutoPo ReqBy';
                    $("#<%= txtAutoPo.ClientID %>").focus();
                }
                else if ($("#<%= txtMinimum.ClientID %>").val() == "") {
                    Show = Show + '\n  Enter Qty';
                    $("#<%= txtMinimum.ClientID %>").focus();
                }
                //else {
                //    var sStatus = fncGetReOrderSettingsValue();
                //    alert(sStatus);
                //    if (sStatus == "") {
                //        alert("Please Select Atleast One Item");
                //        return false;
                //    }
                //}

        if (Show != '') {
            alert(Show);
            return false;
        }
        else {
            return true;
        }
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

function fncGetReOrderSettingsValue() {
    var obj;
    var xml = "";
    var Empty = "";
    var RowNo = 0;
    try {
        if ($("#tblReorderDetails tbody").children().length > 0) {
            xml = '<NewDataSet>';
            $("#tblReorderDetails tbody").children().each(function () {
                obj = $(this);
                if (obj.find('td input[id*="chkSelect"]').is(":checked")) {
                    RowNo = RowNo + 1;
                    Empty = "Value";
                    xml += "<Table>";
                    xml += "<RowNo>" + RowNo + "</RowNo>";
                    xml += "<InventoryCode>" + obj.find('td[id*="tdInventoryCode"]').text().trim() + "</InventoryCode>";
                    xml += "<LocationCode>" + obj.find('td[id*="tdLocationCode"]').text().trim() + "</LocationCode>";
                    xml += "<MaxQty>" + obj.find('td input[id*=MaxQty]').val().trim() + "</MaxQty>";
                    xml += "<MinQty>" + obj.find('td input[id*=MinQty]').val().trim() + "</MinQty>";
                    xml += "<ReOrderLevel>" + obj.find('td input[id*=ReOrderLevel]').val().trim() + "</ReOrderLevel>";
                    xml += "</Table>";
                }
            });

            xml = xml + '</NewDataSet>'
            xml = escape(xml);
            $('#<%=ReOrderValue.ClientID %>').val(xml);
        }

        if (Empty == "") {
            xml = "";
            $('#<%=ReOrderValue.ClientID %>').val(xml);
        }
        return Empty;
    }
    catch (err) {
        xml = "";
        $('#<%=ReOrderValue.ClientID %>').val(xml);
        fncToastError(err.message);
        return "";
    }
}
function fncVenItemRowdblClk(rowObj) {
    try {
        rowObj = $(rowObj);
        fncOpenItemhistory($.trim($("td", rowObj).eq(2).text()));
    }
    catch (err) {
        fncToastError(err.message);
    }
}
/// Open Item History
function fncOpenItemhistory(itemcode) {
    var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
    page = page + "?InvCode=" + itemcode + "&Status=dailog";
    var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
        autoOpen: false,
        modal: true,
        height: 700,
        width: 1250,
        title: "Inventory History"
    });
    $dialog.dialog('open');
}
//-------------------Search Grid Functions-------------------------------
function fncSetValue() {
    try {
        if (SearchTableName == "InventoryType") {
            $('#ContentPlaceHolder1_txtInventoryType').val($.trim(Description));
        }
        if (SearchTableName == "OutputVat") {
            $('#<%=txtOutputVat.ClientID %>').val($.trim(Description));
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
function fncGetInventoryType(event) {
    try {
        var data = $('#<%=txtInventoryType.ClientID %>').val();
        if (data == "GST")
            data = "TaxMasterQuck"
        fncShowSearchDialogCommon(event, data, 'txtSelectedInventoryType', 'txtOutputVat');
    }
    catch (err) {
        fncToastError(err.message);
    }
}

        <%--$(document).ready(function () {
         $('#<%=grdItemDetails.ClientID%>').gridviewScroll({
                width: 1000,
                //height: 300,
                freezesize: 4, // Freeze Number of Columns.
               // headerrowcount: 1, //Freeze Number of Rows with Header.
                //startHorizontal: 0,
                //wheelstep: 10,
                barhovercolor: "#3399FF",
                barcolor: "#3399FF"
                //arrowsize: 30//,
                //varrowtopimg: "Images/arrowvt.png",
                //varrowbottomimg: "Images/arrowvb.png",
                //harrowleftimg: "Images/arrowhl.png",
                //harrowrightimg: "Images/arrowhr.png"
            });
});--%>

        //Vijay -- Activation Fiteration 20200317----

        function fncActivationClick() {
            try {
                $("#divActSet").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Activation Settings",
                    appendTo: 'form:first',
                    closeOnEscape: false,
                    buttons: {
                        "Confirm": function () {
                            $(this).dialog("close");
                        },
                        "Close": function () {
                            $(this).dialog("close");
                            $('#<%=chkActivation.ClientID %>').removeAttr('checked');
                        }
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        function fncCheckBoxKeyDown(evt, value) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {

                if (value == "chkGRN") {
                    if (charCode == 40) {
                        $('#<%=chkPurchaseReturn.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkPurchaseOrder.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
            }
            else if (value == "chkPurchaseReturn") {
                if (charCode == 40) {
                    $('#<%=chkPointOfSale.ClientID %>').focus();
                }
                else if (charCode == 38) {
                    $('#<%=chkGRN.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkPointOfSale") {
    if (charCode == 40) {
        $('#<%=chkAllowNeg.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPurchaseReturn.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSalesReturn") {
    if (charCode == 40) {
        $('#<%=chkOrderBooking.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPointOfSale.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkOrderBooking") {
    if (charCode == 40) {
        $('#<%=chkEstimate.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSalesReturn.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkEstimate") {
    if (charCode == 40) {
        $('#<%=chkQuatation.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkOrderBooking.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkQuatation") {
    if (charCode == 40) {
        $('#<%=chkSelectAllRight.ClientID %>').focus();
    }
    if (charCode == 38) {
        $('#<%=chkEstimate.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSelectAllRight") {
    if (charCode == 38) {
        $('#<%=chkQuatation.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkAllowNegative") {
    if (charCode == 40) {
        $('#<%=chkSalesReturn.ClientID %>').focus();
    }
    if (charCode == 38) {
        $('#<%=chkPointOfSale.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
                }
                else if (value == "chkloyality") {
                    if (charCode == 40) {
                        $('#<%=chkSelectAllRight.ClientID %>').focus();
                    }
                    if (charCode == 38) {
                        $('#<%=chkPointOfSale.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }

}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

    </script>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'InventoryChange');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "InventoryChange";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container" style="overflow: hidden">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Change </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price EnableScroll">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                    EnableVendorDropDown="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableClassDropDown="true"
                    EnableSubClassDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemTypeDropDown="false"
                    EnablePriceTextBox="false"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    EnableGstDropDown="true"
                    OnClearButtonClientClick="clearFormFilter(); return false;"
                    OnFilterButtonClick="lnkLoadFilter_Click" />
                <div class="control-group-single-res">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <asp:CheckBox ID="chkFixed" runat="server" Text="Fixed Margin" Font-Bold="True" />
                        </div>
                        <div class="col-md-8">
                            <asp:CheckBox ID="rdoSalesPer" runat="server" Text="Item Transaction between" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <asp:CheckBox ID="rdoNewItem" runat="server" Text="New Items Created" /><%--'<%$ Resources:LabelCaption,lbl_Newitems %>'--%>
                        </div>
                        <div class="col-md-6">
                            <asp:CheckBox ID="chkActivation" runat="server" Text="Activation Settings" />
                            <%--onclick="fncActivationClick();return false;"--%>
                        </div>
                    </div>
                </div>
                <div class="control-group-single-res" id="DivMargin">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label id="lblFixedMargin">E.Margin</label>
                        </div>
                        <div class="col-md-3">
                            <label id="lblMRP">MRP</label>
                        </div>
                        <div class="col-md-3">
                            <label id="lblCost">N.Cost</label>
                        </div>
                        <div class="col-md-3" style="display: none;">
                            <label id="lblbCost">B.Cost</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <asp:TextBox ID="txtMargin" runat="server" Text="0" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtMRP" runat="server" Text="0" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtBasicCost" runat="server" Text="0" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-3" style="display: none;">
                            <asp:TextBox ID="txtBelowCost" runat="server" Text="0" Style="text-align: right;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 5px;">
                        <div class="col-md-3">
                            <asp:RadioButton ID="rdoEqual" runat="server" Checked="true" GroupName="Con" Text="="
                                Class="radioboxlist" />
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButton ID="rdoGreater" runat="server" GroupName="Con" Text=">"
                                Class="radioboxlist" />
                        </div>
                        <div class="col-md-3">
                            <asp:RadioButton ID="rdoLesser" runat="server" GroupName="Con" Text="<"
                                Class="radioboxlist" />
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                </div>

                <div class="label-right">
                    <%-- <div>
                        <%--'<%$ Resources:LabelCaption,lbl_Salesperformance %>'--%>
                    <%--</div>--%>
                    <div class="col-md-12" id="DivSalesPer" runat="server">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <asp:Label ID="lblSalesFromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSalesFromDate" runat="server" CssClass="form-control-res Datefilter"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblSalesToDate" runat="server" Style="margin-left: 15px;" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="txtSalesToDate" runat="server" CssClass="form-control-res Datefilter"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" id="DivNew" runat="server" style="margin-top: 25px">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <asp:Label ID="lblNewFromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtNewFromDate" runat="server" CssClass="form-control-res Datefilter"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:Label ID="lblToFromDate" runat="server" Style="margin-left: 15px;" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtNewToDate" runat="server" CssClass="form-control-res Datefilter"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <div class="col-md-12">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-3 sort_by">
                        <div class="col-md-3">
                            <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                        </div>
                        <div class="col-md-7">
                            <asp:DropDownList runat="server" Width="100%" ID="ddlSort">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-1">
                            <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                        </div>
                        <div class="col-md-1">
                            <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="grdLoad">
                        <div class="row" style="overflow: auto;">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr class="click" style="background-color: #4163e1; color: white;">
                                        <th class="fixed-side" scope="col">
                                            <asp:CheckBox ID="checkAll" runat="server" />
                                        </th>
                                        <th class="fixed-side" scope="col">S.No</th>
                                        <th class="fixed-side" scope="col">ItemCode
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                        <th scope="col">Department
                                        </th>
                                        <th scope="col">Category 
                                        </th>
                                        <th scope="col">Brand
                                        </th>
                                        <th scope="col">Vendor
                                        </th>
                                        <th scope="col">Merchandise 
                                        </th>
                                        <th scope="col">SubCategory 
                                        </th>
                                        <th scope="col">GST
                                        </th>
                                        <th scope="col">QtyonHand
                                        </th>
                                        <th scope="col">UnitCost
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th scope="col">MarkDownPerc
                                        </th>
                                        <th scope="col">NETCOST
                                        </th>
                                        <th scope="col">EarnedMargin
                                        </th>
                                        <th scope="col">Sellingprice
                                        </th>
                                        <th scope="col">Wprice1
                                        </th>
                                        <th scope="col">Wprice2
                                        </th>
                                        <th scope="col">Wprice3
                                        </th>
                                        <th scope="col">MPWprice1
                                        </th>
                                        <th scope="col">MPWprice2
                                        </th>
                                        <th scope="col">MPWprice3
                                        </th>
                                        <th scope="col">ITEMType
                                        </th>
                                        <th scope="col">Manufacturer
                                        </th>
                                        <th scope="col">WareHouse
                                        </th>
                                        <th scope="col">Origin
                                        </th>
                                        <th scope="col">Floor
                                        </th>
                                        <th scope="col">FSection
                                        </th>
                                        <th scope="col">BinNo
                                        </th>
                                        <th scope="col">Shelf
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div id="HideFilter_GridOverFlow" runat="server" style="overflow-x: hidden; overflow-y: scroll; height: 275px; width: 3070px;">
                                <asp:GridView ID="grdItemDetails" runat="server" AutoGenerateColumns="true" ShowHeader="false"
                                    ShowHeaderWhenEmpty="True" OnRowDataBound="grdItemDetails_RowDataBound">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <PagerStyle CssClass="pshro_text" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="FrozenCell">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this,'click')" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        PageButtonCount="5" Position="Bottom" />
                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-group-price" id="divUpdation">

                    <div class="control-group-single-res" id="divUpdationHead" runat="server">
                        <div>
                            <div class="barcode-header col-md-2">
                                Updation Mode
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-top: 5px;" id="divUpdationBody">
                            <div class="col-md-2" style="width: 100px;">
                                <asp:RadioButton ID="rdoInventory" runat="server" GroupName="Inventory" Text="Inventory"
                                    Class="radioboxlist" Checked="true" />
                            </div>
                            <div class="col-md-2" style="width: 100px; display:none" >
                                <asp:RadioButton ID="rdoReorder" Visible="false" runat="server" GroupName="Inventory" Text="Reorder"
                                    Class="radioboxlist" />
                            </div>
                            <div class="col-md-2" style="width: 125px;">
                                <asp:HyperLink ID="hypReorder" Style="font-weight: bold; text-decoration-line: underline"
                                    Text="Reorder Setting" onclick="fncReOrderSettings();" runat="server" /><%-- NavigateUrl="#"--%>
                            </div>
                            <div class="col-md-2" style="visibility: hidden">
                                <asp:RadioButton ID="rdoActivation" runat="server" GroupName="Inventory"
                                    Text="Activation Settings" Class="radioboxlist" />
                            </div>
                            <div class="col-md-2" style="width: 215px; visibility: hidden">
                                <asp:HyperLink ID="hypMinMax" Style="font-weight: bold; text-decoration-line: underline"
                                    Text="Max and Min Sold Qty Settings" runat="server" /><%--NavigateUrl="http://localhost:10211/Purchase/frmAutoPO.aspx"--%>
                            </div>
                            <div class="col-md-2" style="visibility: hidden">
                                <asp:HyperLink ID="hypBonus" Style="font-weight: bold; text-decoration-line: underline"
                                    Text="Set Bonus Points" runat="server" />
                            </div>
                        </div>

                    </div>
                </div>

                <div id="divInventory" runat="server">
                    <div class="barcode-header col-md-2" id="divInventoryHead">
                        Inventory Type
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <div id="divInventoryBody" runat="server">
                                <div class="col-md-12">
                                    <div class="col-md-6 InvChange_InvType_left" id="DivDateFilter" runat="server">
                                        <div class="col-md-12">
                                            <div class="col-md-8">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Lblfrod" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-8">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="lblto" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="control-button">
                                                    <asp:LinkButton ID="lnkReport" runat="server" class="button-blue" OnClick="lnkReport_Click"
                                                        Text='<%$ Resources:LabelCaption,btn_Report %>'></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                        <%--<div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:CheckBox ID="rdoNewItem" runat="server" Text='<%$ Resources:LabelCaption,lbl_Newitems %>'
                                        Font-Bold="True" />
                                </div>
                                <div class="label-right">
                                    <asp:CheckBox ID="rdoSalesPer" runat="server" Text='<%$ Resources:LabelCaption,lbl_Salesperformance %>'
                                        Font-Bold="True" />
                                </div>
                            </div>--%>
                                    </div>
                                    <div class="col-md-6 InvChange_InvType_right" id="DivChanges" runat="server">
                                        <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="server">
                                            <ContentTemplate>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Select Field for Change"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtInventoryType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'InventoryType', 'txtInventoryType', 'txtSelectedInventoryType');" onchange="return fncOnchange()"></asp:TextBox>
                                                        <%--<asp:DropDownList ID="UpdateDropdown" runat="server" AutoPostBack="true" CssClass="form-control-res"
                                                OnSelectedIndexChanged="UpdateDropdown_SelectedIndexChanged">
                                            </asp:DropDownList>--%>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="lblSelect" runat="server" Font-Bold="True" Text="New Value for the Field"></asp:Label><%--'<%$ Resources:LabelCaption,lbl_selected %>'--%>
                                                    </div>
                                                    <div class="label-right">
                                                        <%--<asp:DropDownList ID="SelectedDropDown" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                                        <asp:TextBox ID="txtSelectedInventoryType" runat="server" CssClass="form-control-res" onkeydown="return fncGetInventoryType(event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res" runat="server" id="divOutputvat" style="display: none">
                                                    <div class="label-left">
                                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text='OUTPUT GST'></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <%--<asp:DropDownList ID="OutputVatDropDown" runat="server" AutoPostBack="true" CssClass="form-control-res" OnSelectedIndexChanged="OutputVatDropDown_SelectedIndexChanged">
                                            </asp:DropDownList>--%>
                                                        <asp:TextBox ID="txtOutputVat" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'OutputVat', 'txtOutputVat', '');"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res" runat="server" id="divVendorType" style="display: none">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <asp:RadioButton runat="server" ID="rdoReplace" GroupName="Vendor" Checked="true"
                                                                Text="Replace Vendor to Inventory" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:RadioButton runat="server" ID="rdoAdd" GroupName="Vendor"
                                                                Text="Add Vendor to Inventory" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 marginPer3">
                            <div class="container-bottom-invchange">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick=" return fncHideFilter()"
                                        class="button-blue" Text='<%$ Resources:LabelCaption,btn_Hide_filter %>'> </asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click"
                                        Text='<%$ Resources:LabelCaption,btn_Update %>'></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="clearForm();return false;"
                                        Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="divReorder" runat="server">
                    <div class="col-md-12" id="divReorderHead">
                        <div class="barcode-header col-md-2">
                            Reorder
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-8 InvChange_InvType_left_Reorder">
                            <div id="divReorderBody" runat="server">
                                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Always" runat="server">
                                    <ContentTemplate>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <asp:Label ID="lblReorder" runat="server" Text="Reorder Process Type"></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="lblMinimum" runat="server" Text="Minimum Qty"></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="lblAuto" runat="server" Text="Auto Po.Req.By"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <%--<asp:DropDownList ID="ddReorder" runat="server" AutoPostBack="true" CssClass="form-control-res"
                                            OnSelectedIndexChanged="ReorderDropdown_SelectedIndexChanged">
                                        </asp:DropDownList>--%>
                                                <asp:TextBox ID="txtReorder" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Reorder', 'txtReorder', '');"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtMinimum" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtAutoPo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'AutoPo', 'AutoPo', '');"></asp:TextBox>
                                                <%--<asp:DropDownList ID="ddAutoPo" runat="server" CssClass="form-control-res">
                                        </asp:DropDownList>--%>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="col-md-4 marginPer3">
                            <div class="container-bottom-invchange">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkHide" runat="server" OnClientClick=" return fncHideFilter()"
                                        class="button-blue" Text='<%$ Resources:LabelCaption,btn_Hide_filter %>'> </asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkReOrderUpdate" runat="server" class="button-blue" OnClientClick=" return fncValidateForm()"
                                        OnClick="lnkReOrderUpdate_Click" Text='<%$ Resources:LabelCaption,btn_Update %>'></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearUpdate" runat="server" class="button-blue" OnClientClick="clearForm();return false;"
                                        Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div id="divActivation" runat="server">

                    <div class="col-md-12" id="divActivationHead">
                        <div class="barcode-header col-md-2">
                            Activation Settings
                        </div>
                    </div>

                    <div id="divActivationBody" runat="server" style="padding-top: 20px">

                        <div class="col-md-3 gid_Itemsearch gidItem_Searchtbl" style="width: 200px">
                            <table id="tblActivation" width="100%" rules="all" border="2">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 20px;"></th>
                                        <th scope="col">Activation</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td scope="col" style="text-align: center">1</td>
                                        <td scope="col" style="padding-left: 5px">Purchase Order</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">2</td>
                                        <td scope="col" style="padding-left: 5px">GRN</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">3</td>
                                        <td scope="col" style="padding-left: 5px">Purchase Return</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">4</td>
                                        <td scope="col" style="padding-left: 5px">Point Of Sales</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">5</td>
                                        <td scope="col" style="padding-left: 5px">Sales Return</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">6</td>
                                        <td scope="col" style="padding-left: 5px">Order Booking</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">7</td>
                                        <td scope="col" style="padding-left: 5px">Estimate</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">8</td>
                                        <td scope="col" style="padding-left: 5px">Quatation</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">9</td>
                                        <td scope="col" style="padding-left: 5px">Expire Date</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">10</td>
                                        <td scope="col" style="padding-left: 5px">Serial No Required</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">11</td>
                                        <td scope="col" style="padding-left: 5px">Auto PO Allowed</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">12</td>
                                        <td scope="col" style="padding-left: 5px">Loyalty</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">13</td>
                                        <td scope="col" style="padding-left: 5px">Package Item</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">14</td>
                                        <td scope="col" style="padding-left: 5px">Marking Item</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">15</td>
                                        <td scope="col" style="padding-left: 5px">Allow Negative</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">16</td>
                                        <td scope="col" style="padding-left: 5px">Member Discount Allowed</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">17</td>
                                        <td scope="col" style="padding-left: 5px">Seasonal Item</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">18</td>
                                        <td scope="col" style="padding-left: 5px">Special</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">19</td>
                                        <td scope="col" style="padding-left: 5px">Warehouse Storage</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">20</td>
                                        <td scope="col" style="padding-left: 5px">Hide Barcode Date</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">21</td>
                                        <td scope="col" style="padding-left: 5px">Cost Edit in GRN</td>
                                    </tr>
                                    <tr>
                                        <td scope="col" style="text-align: center">22</td>
                                        <td scope="col" style="padding-left: 5px">Need Barcode Print</td>
                                    </tr>

                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>

                        <div class="col-md-1" style="width: 80px; padding-left: 10px; padding-top: 200px;">
                            <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue"
                                Text=">>"></asp:LinkButton>
                        </div>

                        <div class="col-md-3 gid_Itemsearch gidItem_Searchtbl">
                            <table id="tblActive" width="100%" rules="all" border="2">
                                <thead>
                                    <tr>
                                        <th scope="col">Activation</th>
                                        <th scope="col" style="width: 70px">Active</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                    <div class="container-bottom-invchange">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkActivationHide" runat="server" OnClientClick=" return fncHideFilter()"
                                class="button-blue" Text='<%$ Resources:LabelCaption,btn_Hide_filter %>'> </asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkActivationUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click"
                                Text='<%$ Resources:LabelCaption,btn_Update %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkActivationClear" runat="server" class="button-blue" OnClientClick="clearForm();return false;"
                                Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>
            <div class="hiddencol">
                <asp:HiddenField ID="ReOrderValue" runat="server" />
                <asp:HiddenField ID="hidSavebtn" runat="server" />
                <asp:HiddenField ID="hidDeletebtn" runat="server" />
                <asp:HiddenField ID="hidEditbtn" runat="server" />
                <asp:HiddenField ID="hidViewbtn" runat="server" />
                <asp:HiddenField ID="hidWholesale" runat="server" />

                <div id="divActSet">
                    <div class="activation-right" style="width: 100%;">
                        <div class="inv_settinghdr">
                            <span>Transaction Activation Settings </span>
                        </div>
                        <div class="activation-check-left">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPurchaseOrder" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseOrder');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkGRN" onkeydown="return fncCheckBoxKeyDown(event,'chkGRN');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label62" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNPurchase %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPurchaseReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseReturn');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label63" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseReturn %>'></asp:Label>
                                </div>
                            </div>
                           
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPointOfSale" onkeydown="return fncCheckBoxKeyDown(event,'chkPointOfSale');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label64" runat="server" Text='<%$ Resources:LabelCaption,lbl_POS %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkAllowNeg" onkeydown="return fncCheckBoxKeyDown(event,'chkAllowNegative');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label117" runat="server" Text="Allow Negative"></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkTransfer" onkeydown="return fncCheckBoxKeyDown(event,'chkTransfer');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label118" runat="server" Text='Transfer(DC)'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="activation-check-right">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSalesReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkSalesReturn');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label68" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesReturn %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkOrderBooking" onkeydown="return fncCheckBoxKeyDown(event,'chkOrderBooking');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label69" runat="server" Text='<%$ Resources:LabelCaption,lbl_OrderBooking %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkEstimate" onkeydown="return fncCheckBoxKeyDown(event,'chkEstimate');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label70" runat="server" Text='<%$ Resources:LabelCaption,lbl_Estimate %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkQuatation" onkeydown="return fncCheckBoxKeyDown(event,'chkQuatation');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lbl_Quatation %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control" style="display: none">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkDisGrn" onkeydown="return fncCheckBoxKeyDown(event,'chkDisGrn');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label119" runat="server" Text='AllowDiscount(GRN)'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control" style="display: none">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkDisPo" onkeydown="return fncCheckBoxKeyDown(event,'chkDisPo');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label120" runat="server" Text='AllowDiscount(PO)'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control" id="divPriceWatch">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="ChkPriceWList" onkeydown="return fncCheckBoxKeyDown(event,'ChkPriceWList');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label121" runat="server" Text='Price Watch List'></asp:Label>
                                </div>
                            </div>
                              <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkloyality" onkeydown="return fncCheckBoxKeyDown(event,'chkloyality');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label3" runat="server" Text='Loyalty'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSelectAllRight" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAllRight');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <span style="color: Blue">Select All</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
