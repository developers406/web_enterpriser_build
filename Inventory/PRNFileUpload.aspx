﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="PRNFileUpload.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.PRNFileUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>

    <script type="text/javascript">
        function pageLoad() {
            try {
                 
                 $("select").chosen({ width: '100%' }); // width in px, %, em, etc

                 if ($('#<%=hidMode.ClientID %>').val() == "Init") {
                     fncInitLoad();
                 }
                 else if ($('#<%=hidMode.ClientID %>').val() == "New") {
                     $('#intidiv').hide();
                     $('#editPrn').hide();
                 }



             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }

         ///Initial Load 
         function fncInitLoad() {
             try {
                 $('#addPrn').hide();
                 $('#editPrn').hide();
                 $('#multitext').hide();
                 $('#<%=intidiv.ClientID %>').css('display', 'block'); // Hide element
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Show Upload PRN Template
        function fncShowUploadPRN() {
            try {
                fncClear();
                $('#addPrn').show();
                $('#multitext').show();
                $('#<%=intidiv.ClientID %>').css('display', 'none'); // Hide element
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Show Edit PRN Template
        function fncShowEditPRN() {
            try {
                fncClear();
                $('#editPrn').show();
                $('#multitext').show();
                $('#<%=intidiv.ClientID %>').css('display', 'none'); // Hide element

                //                (selector).css('visibility', 'hidden'); // Hide element
                //    $(selector).css('visibility', 'visible'); // Show element

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///PRN File Upload to Text box
        function fncAssignPRNFiletoTextbox() {
            try {
                var prnfile = document.getElementById("<%=prnupload.ClientID %>");

                if (prnfile.files.length == 0) {
                    $('#<%=txtPrnFilePath.ClientID %>').val('');
                }
                else {
                    $('#<%=txtPrnFilePath.ClientID %>').val(document.getElementById("<%=prnupload.ClientID %>").files[0].name);
                    $("#<%=btnLoadprnfile.ClientID%>").click();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Browse Click
        function fncBrowseClick() {
            try {
                $("#<%=prnupload.ClientID%>").click();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Close Success Dialog
        function fncCloseSaveDialog() {
            try {
                $("#prnsave").dialog('close');
                fncClear();
                fncInitLoad();
                $('#<%=txtTemplate.ClientID%>').val(parseInt($('#<%=txtTemplate.ClientID%>').val()) + 1);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }

        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#prnsave").dialog({
                    title: "PRN File",
                    resizable: false,
                    height: 130,
                    width: 300,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Check Template available or not
        function fncSaveValidation() {
            try {

                debugger;

                if ($('#<%=txtPrnFilePath.ClientID%>').val() == "") {
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_chooseprnfile%>');
                    popUpObjectForSetFocusandOpen = $('#<%=btnBrowse.ClientID%>');
                    return false;
                }
                else if ($('#<%=ddlCross.ClientID%>')[0].selectedIndex == 0) {
                    ShowPopupMessageBoxandOpentoObject('<%=Resources.LabelCaption.msg_Cross%>');
                    popUpObjectForSetFocusandOpen = $('#<%=ddlCross.ClientID%>');
                    return false;
                }
            return true;
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Update PRN File
    function fncUpdateValidation() {
        try {
            if ($('#<%=ddlTemplate.ClientID%>')[0].selectedIndex == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.msg_Choosetemplate%>');
                return false;
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Clear All Inputs---
    function fncClear() {
        try {
            $('#<%=txtPrntxtfile.ClientID%>').val('');
            $('#<%=txtPrnFilePath.ClientID%>').val('');
            //$('#<%=txtTemplate.ClientID%>').val($('#<%=txtTemplate.ClientID%>').val());
            $('#<%= ddlCross.ClientID %>')[0].selectedIndex = 0;
            $('#<%= ddlTemplate.ClientID %>')[0].selectedIndex = 0;
            $('#<%= ddlCross.ClientID %>').trigger("liszt:updated");

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ////GetPRNFile from Data Base
    function fncGetPRNFile() {
        try {

            if ($('#<%=ddlTemplate.ClientID%>')[0].selectedIndex == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.msg_Choosetemplate%>');
                return;
            }

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("PRNFileUpload.aspx/fncGetPRNFile")%>',
                data: "{ 'Itemcode': '" + $('#<%=ddlTemplate.ClientID%>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $('#<%=txtPrntxtfile.ClientID%>').val(msg.d);
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                    return false;
                }
            });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>

    <style type="text/css">
        .body {
            background: #F5F5F5;
        }

        textarea {
            white-space: pre;
            overflow-wrap: normal;
            overflow: scroll;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'PRNFileUpload');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "PRNFileUpload";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
            <li class="active-page " id="breadcrumbs_text_GAR">
                <%=Resources.LabelCaption.lbl_PRNFile%>
            </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="inventory_download">
        <a href="../Templates/BarcodeTemplate.zip">
            <%=Resources.LabelCaption.lblDownloadBarcodeTemplate%>
        </a>
    </div>
    <div class="prnt_upload">
        <%--<asp:UpdatePanel ID="upadd" runat="server">
            <ContentTemplate>--%>
        <div class="prn_file prn_file_inithight" id="intidiv" runat="server">
            <div class="prn_file_Init ">
                <div class="float_left prn_add_padding">
                    <asp:Button ID="btnAddprn" runat="server" CssClass="button_menu_New" Text='<%$ Resources:LabelCaption,lblUploadPRNTemplate %>'
                        OnClientClick="fncShowUploadPRN();return false;" />
                </div>
                <div class="float_left prn_add_padding">
                    <asp:Button ID="btmEditprn" runat="server" CssClass="button_menu_New" Text='<%$ Resources:LabelCaption,lblEditPRNTemplate %>'
                        OnClientClick="fncShowEditPRN();return false;" />
                </div>
            </div>
        </div>
        <div class="prn_file" id="addPrn">
            <div class="prn_Add_Edit">
                <div class="float_left">
                    <div class="prnlbl">
                        <asp:Label ID="lblPRNFile" runat="server" Text='<%$ Resources:LabelCaption,lblPRNFile %>'></asp:Label>
                    </div>
                    <div class="prnlbl">
                        <asp:TextBox ID="txtPrnFilePath" runat="server" CssClass="form-control-res" Width="400px"
                            onkeypress="return false;" onpaste="return false" oncut="return false" onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div class="float_left">
                    <div class="prnlbl">
                        <asp:Button ID="btnBrowse" runat="server" CssClass="button_menu_New" Text='<%$ Resources:LabelCaption,lbl_Browse %>'
                            OnClientClick="fncBrowseClick();return false;" />
                        <asp:FileUpload ID="prnupload" runat="server" onchange="fncAssignPRNFiletoTextbox()"
                            Style="display: none"></asp:FileUpload>
                    </div>
                </div>
            </div>
            <div>
                <div class="prnlbl">
                    <asp:Label ID="lblTemplate" runat="server" Text='<%$ Resources:LabelCaption,lblTemplate %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:TextBox ID="txtTemplate" runat="server" CssClass="form-control-res" onkeypress="return false;"
                        onpaste="return false" oncut="return false" onMouseDown="return false"></asp:TextBox>
                </div>
                <div class="prnlbl">
                    <asp:Label ID="lblCross" runat="server" Text='<%$ Resources:LabelCaption,lblCross %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:DropDownList ID="ddlCross" runat="server" Style="width: 172px">
                    </asp:DropDownList>
                </div>
                <div class="prnlbl">
                    <asp:UpdatePanel ID="upUpload" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Button ID="btnUpload" runat="server" CssClass="button_menu_New" Text="Upload"
                                OnClientClick=" return fncSaveValidation()" OnClick="btnUpload_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="prn_file" id="editPrn">
            <div>
                <div class="prnlbl">
                    <asp:Label ID="lblEditTemplate" runat="server" Text='<%$ Resources:LabelCaption,lblTemplate %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:DropDownList ID="ddlTemplate" runat="server" Style="width: 172px">
                    </asp:DropDownList>
                </div>
                <div class="prnlbl">
                    <asp:Button ID="btnLoad" runat="server" CssClass="button_menu_New" Text='<%$ Resources:LabelCaption,lblLoadPRNTemplate %>'
                        OnClientClick="fncGetPRNFile();return false;" />
                </div>
                <div class="prnlbl">
                    <asp:UpdatePanel ID="upUpdatebtn" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button_menu_New" Text='<%$ Resources:LabelCaption,lblUpdatePRNTemplate %>'
                                OnClick="btnUpdate_Click" OnClientClick=" return fncUpdateValidation()" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="prn_multitext" id="multitext">
            <asp:TextBox ID="txtPrntxtfile" runat="server" TextMode="MultiLine" Height="400px"
                CssClass="form-control-res"></asp:TextBox>
        </div>
        <%--<div class="float_left">
            <asp:Button ID="btnUpdate" runat="server" CssClass="button_menu_New" Text="Update"
                OnClick="btnUpdate_Click" OnClientClick=" return fncSaveValidation()" />
        </div>--%>
        <%--
        <div class="prn_file">
            <div class="prn_browse">
                <div class="prnlbl">
                    <asp:Label ID="lblInventorycode" runat="server" Text='<%$ Resources:LabelCaption,lblPRNFile %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:TextBox ID="txtPrnFilePath" runat="server" CssClass="form-control-res" Width="400px"></asp:TextBox>
                </div>
                <div class="prnlbl">
                    <asp:Button ID="btnBrowse" runat="server" CssClass="button_menu_New" Text="Browse" OnClientClick="fncBrowseClick();return false;" />
                    <asp:FileUpload ID="prnupload" runat="server" onchange="fncAssignPRNFiletoTextbox()"
                        Style="display: none"></asp:FileUpload>
                </div>
            </div>
            <div class="prn_update">
                <div class="prnlbl">
                    <asp:Label ID="lblTemplate" runat="server" Text='<%$ Resources:LabelCaption,lblTemplate %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:TextBox ID="txtTemplate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="prnlbl">
                    <asp:Label ID="lblCross" runat="server" Text='<%$ Resources:LabelCaption,lblCross %>'></asp:Label>
                </div>
                <div class="prnlbl">
                    <asp:DropDownList ID="ddlCross" runat="server" Style="width: 172px">
                    </asp:DropDownList>
                </div>
                <div class="prnlbl">
                    <asp:Button ID="btnUpdate" runat="server" CssClass="button_menu_New" Text="Update" OnClick="btnUpdate_Click"
                        OnClientClick=" return fncSaveValidation()" />
                </div>
            </div>
        </div>--%>
        <div class="hiddencol">
            <asp:Button ID="btnLoadprnfile" runat="server" OnClick="btnLoadprnfile_Click" />
            <asp:HiddenField ID="hidTemplate" runat="server" Value="" />
            <asp:HiddenField ID="hidMode" runat="server" Value="Init" />
            <div id="prnsave" class="barcodesavedialog">
                <p>
                    <%=Resources.LabelCaption.savePrnfile%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
