﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmInventoryMaster.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryMaster" %>

<%@ Register TagPrefix="ups" TagName="ItemHistory" Src="~/UserControls/ItemHistory.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <%--------------------------------- js file  ------------------------------------%>
    
    <script type="text/javascript" src="../js/utf.js"></script>
    <script type="text/javascript" src="../js/tamil.js"></script>
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold;
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        #tblParam {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #tblParamHead {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 97%;
        }

            #tblParamHead th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: red;
                font-weight: bolder;
                color: white;
            }

        #tblParam td {
            border: 1px solid #ddd;
            padding: 4px;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 400px;
            max-width: 400px;
            text-align: left;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            display: none;
        }

        iframe {
            overflow: hidden;
        }

        .chzn-disabled {
            cursor: default;
            opacity: 0.9 !important;
        }

        .ui-autocomplete {
            z-index: 9999 !important;
        }

        #dialog-ViewItemHistory {
            width: 100% !important;
        }

        .container-inv-history {
            width: 100% !important;
        }

        .cust-pages {
            display: none !important;
        }

        .breakPrice td:nth-child(1), .breakPrice th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .breakPrice td:nth-child(2), .breakPrice th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .breakPrice td:nth-child(3), .breakPrice th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(4), .breakPrice th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(5), .breakPrice th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(6), .breakPrice th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
        }

        .breakPrice td:nth-child(7), .breakPrice th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(8), .breakPrice th:nth-child(8) {
            display: none;
        }

        .tbl_barcode td:nth-child(1), .tbl_barcode th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2) {
            min-width: 260px;
            max-width: 260px;
        }

        .newvendor td:nth-child(1), .newvendor th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .newvendor td:nth-child(1) {
            text-align: center !important;
        }

        .newvendor td:nth-child(2), .newvendor th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .newvendor td:nth-child(3), .newvendor th:nth-child(3) {
            min-width: 250px;
            max-width: 250px;
        }

        .newvendor td:nth-child(4), .newvendor th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .newvendor td:nth-child(4) {
            text-align: center !important;
        }

        .poList td:nth-child(1), .poList th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .poList td:nth-child(2), .poList th:nth-child(2) {
            min-width: 350px;
            max-width: 350px;
        }

        .poList td:nth-child(3), .poList th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .poList td:nth-child(4), .poList th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .poList td:nth-child(5), .poList th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(5) {
            text-align: right !important;
        }

        .poList td:nth-child(6), .poList th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(6) {
            text-align: right !important;
        }

        .poList td:nth-child(7), .poList th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(7) {
            text-align: right !important;
        }

        .poList td:nth-child(8), .poList th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(8) {
            text-align: right !important;
        }

        .poList td:nth-child(9), .poList th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(9) {
            text-align: right !important;
        }

        .poList td:nth-child(10), .poList th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(10) {
            text-align: right !important;
        }
        .StockandPurhistory td:nth-child(1), .StockandPurhistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }
        .StockandPurhistory td:nth-child(1) {
            text-align: left !important;
        }

        .StockandPurhistory td:nth-child(2), .StockandPurhistory th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .StockandPurhistory td:nth-child(2) {
            text-align: left !important;
        }

        .StockandPurhistory td:nth-child(3), .StockandPurhistory th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(4), .StockandPurhistory th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(5), .StockandPurhistory th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(6), .StockandPurhistory th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(7), .StockandPurhistory th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(8), .StockandPurhistory th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(9), .StockandPurhistory th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(10), .StockandPurhistory th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(11), .StockandPurhistory th:nth-child(11) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(12), .StockandPurhistory th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(13), .StockandPurhistory th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(14), .StockandPurhistory th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(15), .StockandPurhistory th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(16), .StockandPurhistory th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(17), .StockandPurhistory th:nth-child(17) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(18), .StockandPurhistory th:nth-child(18) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(19), .StockandPurhistory th:nth-child(19) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(20), .StockandPurhistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(21), .StockandPurhistory th:nth-child(21) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(22), .StockandPurhistory th:nth-child(22) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(23), .StockandPurhistory th:nth-child(23) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(24), .StockandPurhistory th:nth-child(24) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(25), .StockandPurhistory th:nth-child(25) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(26), .StockandPurhistory th:nth-child(26) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(27), .StockandPurhistory th:nth-child(27) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(28), .StockandPurhistory th:nth-child(28) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(29), .StockandPurhistory th:nth-child(29) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(30), .StockandPurhistory th:nth-child(30) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(31), .StockandPurhistory th:nth-child(31) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(20), .StockandPurhistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(32), .StockandPurhistory th:nth-child(32) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(33), .StockandPurhistory th:nth-child(33) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(34), .StockandPurhistory th:nth-child(34) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(35), .StockandPurhistory th:nth-child(35) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(36), .StockandPurhistory th:nth-child(36) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(37), .StockandPurhistory th:nth-child(37) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(38), .StockandPurhistory th:nth-child(38) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(39), .StockandPurhistory th:nth-child(39) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(40), .StockandPurhistory th:nth-child(40) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(41), .StockandPurhistory th:nth-child(41) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(42), .StockandPurhistory th:nth-child(42) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(43), .StockandPurhistory th:nth-child(43) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(20), .StockandPurhistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(44), .StockandPurhistory th:nth-child(44) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(45), .StockandPurhistory th:nth-child(45) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(46), .StockandPurhistory th:nth-child(46) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(47), .StockandPurhistory th:nth-child(47) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(48), .StockandPurhistory th:nth-child(48) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(49), .StockandPurhistory th:nth-child(49) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(50), .StockandPurhistory th:nth-child(50) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(51), .StockandPurhistory th:nth-child(51) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(52), .StockandPurhistory th:nth-child(52) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(53), .StockandPurhistory th:nth-child(53) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(54), .StockandPurhistory th:nth-child(54) {
            min-width: 80px;
            max-width: 80px;
        }

        .StockandPurhistory td:nth-child(55), .StockandPurhistory th:nth-child(55) {
            min-width: 80px;
            max-width: 80px;
        }  



        .saleshistory td:nth-child(1), .saleshistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .saleshistory td:nth-child(1) {
            text-align: left !important;
        }

        .saleshistory td:nth-child(2), .saleshistory th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .saleshistory td:nth-child(2) {
            text-align: left !important;
        }

        .saleshistory td:nth-child(3), .saleshistory th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(4), .saleshistory th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(5), .saleshistory th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(6), .saleshistory th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(7), .saleshistory th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(8), .saleshistory th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(9), .saleshistory th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(10), .saleshistory th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(11), .saleshistory th:nth-child(11) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(12), .saleshistory th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(13), .saleshistory th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(14), .saleshistory th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(15), .saleshistory th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(16), .saleshistory th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(17), .saleshistory th:nth-child(17) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(18), .saleshistory th:nth-child(18) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(19), .saleshistory th:nth-child(19) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(21), .saleshistory th:nth-child(21) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(22), .saleshistory th:nth-child(22) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(23), .saleshistory th:nth-child(23) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(24), .saleshistory th:nth-child(24) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(25), .saleshistory th:nth-child(25) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(26), .saleshistory th:nth-child(26) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(27), .saleshistory th:nth-child(27) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(28), .saleshistory th:nth-child(28) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(29), .saleshistory th:nth-child(29) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(30), .saleshistory th:nth-child(30) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(31), .saleshistory th:nth-child(31) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(32), .saleshistory th:nth-child(32) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(33), .saleshistory th:nth-child(33) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(34), .saleshistory th:nth-child(34) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(35), .saleshistory th:nth-child(35) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(36), .saleshistory th:nth-child(36) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(37), .saleshistory th:nth-child(37) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(38), .saleshistory th:nth-child(38) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(39), .saleshistory th:nth-child(39) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(40), .saleshistory th:nth-child(40) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(41), .saleshistory th:nth-child(41) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(42), .saleshistory th:nth-child(42) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(43), .saleshistory th:nth-child(43) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(44), .saleshistory th:nth-child(44) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(45), .saleshistory th:nth-child(45) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(46), .saleshistory th:nth-child(46) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(47), .saleshistory th:nth-child(47) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(48), .saleshistory th:nth-child(48) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(49), .saleshistory th:nth-child(49) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(50), .saleshistory th:nth-child(50) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(51), .saleshistory th:nth-child(51) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(52), .saleshistory th:nth-child(52) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(53), .saleshistory th:nth-child(53) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(54), .saleshistory th:nth-child(54) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(55), .saleshistory th:nth-child(55) {
            min-width: 80px;
            max-width: 80px;
        }

        .childitems td:nth-child(1), .childitems th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .childitems td:nth-child(2), .childitems th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(3), .childitems th:nth-child(3) {
            min-width: 300px;
            max-width: 300px;
        }

        .childitems td:nth-child(4), .childitems th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .childitems td:nth-child(5), .childitems th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(5) {
            text-align: right;
        }

        .childitems td:nth-child(6), .childitems th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(6) {
            text-align: right;
        }

        .multipleUOM td:nth-child(1), .multipleUOM th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .multipleUOM td:nth-child(2), .multipleUOM th:nth-child(2) {
            min-width: 265px;
            max-width: 265px;
        }

        .multipleUOM td:nth-child(3), .multipleUOM th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .multipleUOM td:nth-child(3) {
            text-align: right !important;
        }

        .multipleUOM td:nth-child(4), .multipleUOM th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .multipleUOM td:nth-child(5), .multipleUOM th:nth-child(5) {
            min-width: 50px;
            max-width: 50px;
        }


        .TransferDet td:nth-child(1), .TransferDet th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .TransferDet td:nth-child(2), .TransferDet th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .TransferDet td:nth-child(3), .TransferDet th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
        }

        .TransferDet td:nth-child(4), .TransferDet th:nth-child(4) {
            min-width: 90px;
            max-width: 90px;
        }

        .TransferDet td:nth-child(5), .TransferDet th:nth-child(5) {
            min-width: 90px;
            max-width: 90px;
        }

        .TransferDet td:nth-child(6), .TransferDet th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .TransferDet td:nth-child(6) {
            text-align: right !important;
        }

        .TransferDet td:nth-child(7), .TransferDet th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }        

        </style>
    <%-------------------------------------------- Common Declartion -----------------------------------------%>
    <script type="text/javascript">
        var ddlInputSGST;
        var ddlInputCGST;
        var ddlInputIGST;
        var ddlOutputSGST;
        var ddlOutputCGST;
        var ddlOutputIGST;
        var txtCESSPrc;
        var txtHSNCode;
        var txtMRP;

        var txtMRPMarkDown;
        var txtBasicCost;
        var txtBasicMargin;
        var txtDiscountPrc1;
        var txtDiscountPrc2;
        var txtDiscountPrc3;
        var txtDiscountAmt;
        var txtGrossCost;
        var txtInputSGSTPrc;
        var txtInputSGSTAmt;
        var txtInputCGSTPrc;
        var txtInputCGSTAmt;
        var txtInputCESSPrc;
        var txtInputCESSAmt;
        var txtNetCost;
        var txtMarginMRP;
        var txtMarginSP;
        var txtFixedMargin;
        var txtMRPAmt;
        var txtSPAmt;
        var txtProfitMRPPrc;
        var txtProfitSPPrc;
        var txtBasicSelling;
        var txtOutputSGSTPrc;
        var txtOutputSGSTAmt;
        var txtOutputCGSTPrc;
        var txtOutputCGSTAmt;
        var txtOutputCESSPrc;
        var txtOutputCESSAmt;
        var txtSellingPrice;
        var txtVatLiabilitySGSTPrc;
        var txtVatLiabilitySGSTAmt;
        var txtVatLiabilityCGSTPrc;
        var txtVatLiabilityCGSTAMt;
        var txtWholePrice1Popup;
        var txtWholePrice2Popup;
        var txtWholePrice3Popup;
        var txtMarginWPrice1;
        var txtMarginWPrice2;
        var txtMarginWPrice3;
        var txtMinimumQty;
        var txtMaxQty;
        var txtReOrderQty;
        var breakRowNo = 1;
        var breakRowPrice;
        var breakPriceBody;
        var txtAdDiscount;
        var txtAddCessAmt;
        var txtGSTLiabilityPer;
        var txtGSTLiability;

        var NetSellingprice = 0;
        var HiddeLocation;
        var NonbatchCalculation = "true";

        var WPriceReverse = 'N'; 

    </script>
    <%-------------------------------------------- Page Load -----------------------------------------%>
    <script type="text/javascript">
       
        function pageLoad() {
            $("#hideChckboxMemDis").hide();
            $("#hideChckboxSeas").hide();
            if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "N") {
                $('#<%=divNewCalculation.ClientID%>').hide();
                $('#<%=divBasicMargin.ClientID%>').hide();
                $('#divAffectNetCost').hide(); 
            }
            else { 
                $('#<%=divBasicMargin.ClientID%>').show();
                $('#<%=divNewCalculation.ClientID%>').show();
                $('#divAffectNetCost').show(); 
            }
            if ($(<%=hidPricewatch.ClientID%>).val().trim() == "N") {
                $('#divPriceWatch').hide();
            }
            else {
                $('#divPriceWatch').show();
            }
            if ($('#<%=hidSavebtn.ClientID%>').val("n1") == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            if ($(<%=hidRbdPartialMrp.ClientID%>).val() == "CS") {
                $('#<%=divAddDis.ClientID %>').css("display", " none");
                $('#<%=divDis.ClientID %>').css("display", "none");
            }
            //  $('#divGenerateMrp').hide();
            $(txtDiscountAmt).attr("disabled", "disabled");
            $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
            $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");

            $(<%=txtMRPMargrin.ClientID%>).attr("readonly", "readonly");
            $(<%=txtsellMargin.ClientID%>).attr("readonly", "readonly");
            $(<%=txtMRPMarkDownNew.ClientID%>).attr("readonly", "readonly");
            $(<%=txtMRPMarkUp.ClientID%>).attr("readonly", "readonly");
            $(<%=txtSellMarkDown.ClientID%>).attr("readonly", "readonly");
            $(<%=txtSellMarkUp.ClientID%>).attr("readonly", "readonly");

            var franObj, locationName;
            if ($(<%=hidGRNSession.ClientID%>).val() != "") {
                var ItemCode = $(<%=hidGRNSession.ClientID%>).val();
                $.session.set('Key', ItemCode);
            }
            $("#<%=chkAllowNeg.ClientID %>").click(function () { //Vijay Check                 
                var checked = $(this).is(':checked');
                if (checked == true) {
                    if ($('#<%=hidAllowNegative.ClientID %>').val() == "NA")
                        $('#<%=chkAllowNeg.ClientID %>').prop('checked', true);
                    else {
                        $('#<%=chkAllowNeg.ClientID %>').prop('checked', false);
                        ShowPopupMessageBox("Already Common rule allow Negative for all items has been set"
                            + " to " + $('#<%=hidAllowNegative.ClientID %>').val() + " . Hence,this rule will not work.");
                    }
                }
                else {
                    $('#<%=chkAllowNeg.ClientID %>').prop('checked', false);
                }
            });
            //$("input:text").focus(function () { $(this).select(); });
            if ($(<%=hidinventoryStatus.ClientID%>).val() != "Edit") {

                InventoryHistory = "T";
                if ($(<%=hidRbdPartialMrp.ClientID%>).val() == "M1") {
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#<%=rbtMRP.ClientID %>').prop("checked", true);
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                }
                else if ($(<%=hidRbdPartialMrp.ClientID%>).val() == "M2") {
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#<%=rbnMRP2.ClientID %>').prop("checked", true);
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                }
                else {
                    $('#<%=rbtCost.ClientID %>').prop("checked", true);

                }
            }
            else {
                if ($(<%=hidMrpFocus.ClientID%>).val().trim() == "B1") {
                    $('#<%=ddlPurchase.ClientID %>').val('Cost')
                    $('#<%=ddlSales.ClientID %>').val('Cost')
              <%--      $("#<%=ddlSales.ClientID %> option[value='Mrp']").prop('selected', true);--%>
                 }
                 else if ($(<%=hidMrpFocus.ClientID%>).val().trim() == "B2") {
                    $('#<%=ddlPurchase.ClientID %>').val('Cost');
                    $('#<%=ddlSales.ClientID %>').val('Mrp');
                  <%--  $("#<%=ddlSales.ClientID %> option[value='Mrp']").prop('selected', true);--%>
                }
                else if ($(<%=hidMrpFocus.ClientID%>).val().trim() == "B3") {
                    $('#<%=ddlPurchase.ClientID %>').val('Mrp')
                    $('#<%=ddlSales.ClientID %>').val('Cost')
                }
                else if ($(<%=hidMrpFocus.ClientID%>).val().trim() == "B4") {
                    $('#<%=ddlPurchase.ClientID %>').val('Mrp')
                     $('#<%=ddlSales.ClientID %>').val('Mrp')
                }
                $('#<%=ddlPurchase.ClientID %>').attr('disabled', 'disabled');
                $('#<%=ddlSales.ClientID %>').attr('disabled', 'disabled');
            }
              <%--  if ($(<%=hidRbdPartialMrp.ClientID%>).val() != "") {
                    $('#<%=rbnMRP2.ClientID %>').prop("checked", true);
                }
                else {
                    $('#<%=rbtCost.ClientID %>').prop("checked", true);
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP');
                    $('#<%=txtAdDiscount.ClientID %>').hide();
                }--%>
            //$("input:text").focus(function () { $(this).select(); });
            if ($(<%=txtUOMPurchase.ClientID%>).val() == "")
                $(<%=txtUOMPurchase.ClientID%>).val('PCS');
            if ($(<%=txtSalesUOM.ClientID%>).val() == "")
                $(<%=txtSalesUOM.ClientID%>).val('PCS');
            if ($(<%=txtStockType.ClientID%>).val() == "")
                $(<%=txtStockType.ClientID%>).val('Non Food');
            if ($(<%=txtFloor.ClientID%>).val() == "")
                $(<%=txtFloor.ClientID%>).val('GF');
            <%--if ($(<%=hidRbdPartialMrp.ClientID%>).val() == "MrpEnter") {
                $('#<%=rbnMRP2.ClientID %>').prop("checked", true);
             }
             else {
                 $('#<%=rbtCost.ClientID %>').prop("checked", true);
                 $('#<%=txtAdDiscount.ClientID %>').hide();
                 $('#ContentPlaceHolder1_lblAdDiscount').text('MRP');
             }--%>
            if ($(<%=hidFranchiseMaster.ClientID%>).val() == "Y") {
                $("#divWholeSales").css("width", "50%");
                $("#divLocationList").css("display", "block");
                fncSelectAndUnSelectLocation();
                if ($('#<%=hidinventoryStatus.ClientID%>').val() == "New") {
                    $("[id*=cblLocation] input").attr("checked", "checked");
                    $("[id*=cbSelectAll]").attr("checked", "checked");
                }
                else {
                    franObj = $.parseJSON($('#<%=hidFranchiserValue.ClientID%>').val());

                    for (var i = 0; i < franObj.length; i++) {
                        locationName = franObj[i]["Locationcode"];
                        $("[id*=cblLocation] input").each(function () {

                            if (locationName == $(this).val()) {
                                $(this).prop("checked", true)
                            }
                        });
                    }
                }

            }

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc      

            $('#<%=ddlOrderBy.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlOrderBy.ClientID %>').next().remove();

            $('#<%=ddlMiniBy.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlMiniBy.ClientID %>').next().remove();

            $('#<%=ddlByReOrder.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlByReOrder.ClientID %>').next().remove();

            $('#<%=ddlBreakPriceType.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlBreakPriceType.ClientID %>').next().remove();

            //-------------------------------------------Inventory Details-----------------------------------------//
            $("[id$=txtItemName]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetFilterValue") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                //focus: function (event, i) {
                //    $("[id$=txtItemName]").val(i.item.desc);
                //    event.preventDefault();
                //},
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtItemName]").val(i.item.desc);

                    if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                        $("[id$=txtItemCode]").val(i.item.val);
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkEditItemCode', '')
                    }


                    return false;
                },

                minLength: 1
            });

           
            //--------------------------------------------Select All--------------------------------------------//

            $('#<%=chkSelectAll.ClientID %>').change(function () {
                try {
                    $('#<%=chkExpiryDate.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSerialNo.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkAutoPO.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkPackage.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkLoyality.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSpecial.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkMemDis.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSeasonal.ClientID %>').prop('checked', $(this).prop("checked"));
                    //$('#<%=chkBatch.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkShelf.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkHideData.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkMarking.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkBarcodePrint.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkAllowCost.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=cbWeightbased.ClientID %>').prop('checked', $(this).prop("checked"));         //jayanthi 02072022
                    $('#<%=chkWholeSale.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=cbAdditionalCess.ClientID %>').prop('checked', $(this).prop("checked"));
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            });


            $('#<%=chkSelectAllRight.ClientID %>').click(function () {
                try {
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkGRN.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkEstimate.ClientID %>').prop('checked', $(this).prop("checked"));

                        $('#<%=chkTransfer.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisGrn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisPo.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=ChkPriceWList.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOnlineOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        if ($('#<%=hidAllowNegative.ClientID %>').val() == "NA")
                            $('#<%=chkAllowNeg.ClientID %>').prop('checked', $(this).prop("checked"));
                        else {
                            $('input[name="chkSelectAllRight"]:checked');
                            $('#<%=chkAllowNeg.ClientID %>').prop('checked', false);
                            ShowPopupMessageBox("Already Common rule for allow Negative for all items has been set"
                                + " to YES/NO. Hence,this rule will not work");
                        }
                        if ($("#<%=hidWeightCanversion.ClientID %>").val() == "Y") {
                            if ($("#<%=cbWeightbased.ClientID %>").is(':checked')) {
                                ShowPopupMessageBox("Please Uncheck Weight Based");
                                $("#<%=chkQuatation.ClientID %>").prop("checked", false);
                            } else {
                                $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                            }
                        }
                        else {
                            $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                        }
                    }
                    else {
                        $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkGRN.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkEstimate.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkTransfer.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisGrn.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkDisPo.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=ChkPriceWList.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkOnlineOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                        $('#<%=chkAllowNeg.ClientID %>').prop('checked', false);
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            });


            //============================  Assign Variable for Simply =========================//

            ddlInputSGST = $('#<%=ddlInputSGST.ClientID %>');
            ddlInputCGST = $('#<%=ddlInputCGST.ClientID %>');
            ddlInputIGST = $('#<%=ddlInputIGST.ClientID %>');
            ddlOutputSGST = $('#<%=ddlOutputSGST.ClientID %>');
            ddlOutputCGST = $('#<%=ddlOutputCGST.ClientID %>');
            ddlOutputIGST = $('#<%=ddlOutputIGST.ClientID %>');
            txtCESSPrc = $('#<%=txtCESSPrc.ClientID %>');
            txtHSNCode = $('#<%=txtHSNCode.ClientID %>');
            txtMRP = $('#<%=txtMRP.ClientID %>');
            txtMRPMarkDown = $('#<%=txtMRPMarkDown.ClientID %>');
            txtBasicCost = $('#<%=txtBasicCost.ClientID %>');
            txtBasicMargin = $('#<%=txtBasicMargin.ClientID %>');
            txtDiscountPrc1 = $('#<%=txtDiscountPrc1.ClientID %>');
            txtDiscountPrc2 = $('#<%=txtDiscountPrc2.ClientID %>');
            txtDiscountPrc3 = $('#<%=txtDiscountPrc3.ClientID %>');
            txtDiscountAmt = $('#<%=txtDiscountAmt.ClientID %>');
            txtGrossCost = $('#<%=txtGrossCost.ClientID %>');
            txtInputSGSTPrc = $('#<%=txtInputSGSTPrc.ClientID %>');
            txtInputSGSTAmt = $('#<%=txtInputSGSTAmt.ClientID %>');
            txtInputCGSTPrc = $('#<%=txtInputCGSTPrc.ClientID %>');
            txtInputCGSTAmt = $('#<%=txtInputCGSTAmt.ClientID %>');
            txtInputCESSPrc = $('#<%=txtInputCESSPrc.ClientID %>');
            txtInputCESSAmt = $('#<%=txtInputCESSAmt.ClientID %>');
            txtNetCost = $('#<%=txtNetCost.ClientID %>');
            txtMarginMRP = $('#<%=txtMarginMRP.ClientID %>');
            txtMarginSP = $('#<%=txtMarginSP.ClientID %>');
            txtFixedMargin = $('#<%=txtFixedMargin.ClientID %>');
            txtMRPAmt = $('#<%=txtMRPAmt.ClientID %>');
            txtSPAmt = $('#<%=txtSPAmt.ClientID %>');
            txtProfitMRPPrc = $('#<%=txtProfitMRPPrc.ClientID %>');
            txtProfitSPPrc = $('#<%=txtProfitSPPrc.ClientID %>');
            txtBasicSelling = $('#<%=txtBasicSelling.ClientID %>');
            txtOutputSGSTPrc = $('#<%=txtOutputSGSTPrc.ClientID %>');
            txtOutputSGSTAmt = $('#<%=txtOutputSGSTAmt.ClientID %>');
            txtOutputCGSTPrc = $('#<%=txtOutputCGSTPrc.ClientID %>');
            txtOutputCGSTAmt = $('#<%=txtOutputCGSTAmt.ClientID %>');
            txtOutputCESSPrc = $('#<%=txtOutputCESSPrc.ClientID %>');
            txtOutputCESSAmt = $('#<%=txtOutputCESSAmt.ClientID %>');
            txtSellingPrice = $('#<%=txtSellingPrice.ClientID %>');
            txtVatLiabilitySGSTPrc = $('#<%=txtVatLiabilitySGSTPrc.ClientID %>');
            txtVatLiabilitySGSTAmt = $('#<%=txtVatLiabilitySGSTAmt.ClientID %>');
            txtVatLiabilityCGSTPrc = $('#<%=txtVatLiabilityCGSTPrc.ClientID %>');
            txtVatLiabilityCGSTAMt = $('#<%=txtVatLiabilityCGSTAMt.ClientID %>');
            txtWholePrice1Popup = $('#<%=txtWholePrice1Popup.ClientID %>');
            txtWholePrice2Popup = $('#<%=txtWholePrice2Popup.ClientID %>');
            txtWholePrice3Popup = $('#<%=txtWholePrice3Popup.ClientID %>');
            txtMarginWPrice1 = $('#<%=txtMarginWPrice1.ClientID %>');
            txtMarginWPrice2 = $('#<%=txtMarginWPrice2.ClientID %>');
            txtMarginWPrice3 = $('#<%=txtMarginWPrice3.ClientID %>');
            txtMinimumQty = $('#<%=txtMinimumQty.ClientID %>');
            txtMaxQty = $('#<%=txtMaxQty.ClientID %>');
            txtReOrderQty = $('#<%=txtReOrderQty.ClientID %>');
            txtAdDiscount = $('#<%=txtAdDiscount.ClientID %>');
            txtAddCessAmt = $('#<%=txtAddCessAmt.ClientID %>');
            txtGSTLiability = $('#<%=txtGSTLiability.ClientID %>');
            txtGSTLiabilityPer = $('#<%=txtGSTLiabilityPer.ClientID %>');
            HiddeLocation = $('#<%=HiddeLocation.ClientID %>');
            txtUOMPurchase = $('#<%=txtUOMPurchase.ClientID %>');
            txtSalesUOM = $('#<%=txtSalesUOM.ClientID %>');
            //============================  Dialog Open For Inventory Pricing =========================//

            $(function () {
                $("#dialog-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 350,
                    modal: true,
                    buttons: {
                        "Yes": function () {

                            $(this).dialog("close");

                            //==========================> Redirect Page to Price Change Batch

                            var InvCode = $('#<%=txtItemCode.ClientID %>').val();
                            var page = '<%=ResolveUrl("~/Merchandising/frmPriceChangeBatch.aspx") %>';
                            var page = page + "?InvCode=" + InvCode
                            var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                                autoOpen: false,
                                modal: true,
                                height: 645,
                                width: 1300,
                                title: "Price Change Batch",
                                buttons: [
                                    {
                                        text: "Close",
                                        click: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                ]
                            });
                            $dialog.dialog('open');


                        },
                        No: function () {
                            $(this).dialog("close");
                            $('#<%=txtMRP.ClientID %>').attr("disabled", "disabled");
                            $("#dialog-InvPrice").dialog("open");
                            fncOpenDialogWithDetail();
                            fncMDownOnMRP();
                        }
                    }
                });
            });

            $(function () {
                $("#dialog-clear-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 250,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            $("*[name$='grpPurchased']").removeAttr("disabled");
                            fncClearPriceChange();
                            $('#<%=hidNetcostnew.ClientID %>').val("0");
                            $('#<%=hidGrossCostNew.ClientID %>').val("0");
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });
            <%--$(document).ready(function () {
                //$("input:text").focus(function () { $(this).select(); });
                if ($(<%=hidRbdPartialMrp.ClientID%>).val() == "MrpEnter") {
                    $('#<%=rbnMRP2.ClientID %>').prop("checked", true);
                }
                else {
                    $('#<%=rbtCost.ClientID %>').prop("checked", true);
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP');
                    $('#<%=txtAdDiscount.ClientID %>').hide();
                }
            });--%>
            $(document).ready(function () {
                $('#<%=rbtCost.ClientID %>').click(function () {
                    if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        fncClearPriceChange();
                    }
                    $(txtBasicCost).removeAttr("disabled");
                    $('#<%=txtMRPMarkDown.ClientID %>').hide(); 
                    $(txtDiscountAmt).attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP');
                    $('#<%=txtAdDiscount.ClientID %>').hide();
                    $('#<%=divMrp.ClientID %>').width(125);
                    $('#divAddDis').css("display", "none");
                    $('#divDis').css("display", "none");
                    if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                        $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                        $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                        txtCESSPrc.val('0');
                        txtAddCessAmt.val('0');
                    }
                    else {
                        $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                        $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    }

                    $("#<%=chkAllowNeg.ClientID %>").click(function () { //Vijay Check 
                        var checked = $(this).is(':checked');
                        if (checked == true) {
                            if ($('#<%=hidAllowNegative.ClientID %>').val() == "NA")
                                $('#<%=chkAllowNeg.ClientID %>').prop('checked', true);
                            else {
                                $('#<%=chkAllowNeg.ClientID %>').prop('checked', false);
                                ShowPopupMessageBox("Already Common rule for allow Negative for all items has been set"
                                    + "to YES/NO. Hence,this rule will not work");
                            }
                        }
                        else {
                            $('#<%=chkAllowNeg.ClientID %>').prop('checked', $(this).prop("checked"));
                        }
                    });
                    if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "Y") {
                        $('#<%=divNewCalculation.ClientID%>').show();
                        $('#<%=divBasicMargin.ClientID%>').show();
                        $('#divAffectNetCost').show();
                    }
                });
                $('#<%=rbtMRP.ClientID %>').click(function () {
                    if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        fncClearPriceChange();
                    }
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#<%=txtMRPMarkDown.ClientID %>').show();
                    $('#<%=txtMRPMarkDown.ClientID %>').val('0.00');
                    $('#<%=txtAdDiscount.ClientID %>').show();
                    $('#<%=txtAdDiscount.ClientID %>').val('0.00');
                    $('#<%=divMrp.ClientID %>').width(81);
                    $('#<%=divAddDis.ClientID %>').css("display", "block");
                    $('#<%=divDis.ClientID %>').css("display", "block");
                    $('#<%=divNewCalculation.ClientID%>').hide();
                    $('#<%=divBasicMargin.ClientID%>').hide(); 
                    if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                        $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                        $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                        txtCESSPrc.val('0');
                        txtAddCessAmt.val('0');
                    }
                    else {
                        $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                        $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    }
                });
                $('#<%=rbnMRP2.ClientID %>').click(function () {
                    if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        fncClearPriceChange();
                    }
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                    $('#<%=txtMRPMarkDown.ClientID %>').show();
                    $('#<%=txtMRPMarkDown.ClientID %>').val('0.00');
                    $('#<%=txtAdDiscount.ClientID %>').show();
                    $('#<%=txtAdDiscount.ClientID %>').val('0.00');
                    $('#<%=divMrp.ClientID %>').width(81);
                    $('#<%=divAddDis.ClientID %>').css("display", "block");
                    $('#<%=divDis.ClientID %>').css("display", "block");
                    if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                        $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                        $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                        txtCESSPrc.val('0');
                        txtAddCessAmt.val('0');
                    }
                    else {
                        $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                        $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    }
                    $('#<%=divNewCalculation.ClientID%>').hide();
                    $('#<%=divBasicMargin.ClientID%>').hide(); 
                });
                $('#<%=rbnMRP3.ClientID %>').click(function () {
                    if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        fncClearPriceChange();
                    }
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                    $('#<%=txtMRPMarkDown.ClientID %>').show();
                    $('#<%=txtMRPMarkDown.ClientID %>').val('0.00');
                    $('#<%=txtAdDiscount.ClientID %>').show();
                    $('#<%=txtAdDiscount.ClientID %>').val('0.00');
                    $('#<%=divMrp.ClientID %>').width(81);
                    $('#<%=divAddDis.ClientID %>').css("display", "block");
                    $('#<%=divDis.ClientID %>').css("display", "block");
                    if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                        $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                        $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                        txtCESSPrc.val('0');
                        txtAddCessAmt.val('0');
                    }
                    else {
                        $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                        $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    }
                    $('#<%=divNewCalculation.ClientID%>').hide();
                    $('#<%=divBasicMargin.ClientID%>').hide(); 
                });
                $('#<%=rbnMRP4.ClientID %>').click(function () {
                    if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        fncClearPriceChange();
                    }
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                    $('#<%=txtMRPMarkDown.ClientID %>').show();
                    $('#<%=txtMRPMarkDown.ClientID %>').val('0.00');
                    $('#<%=txtAdDiscount.ClientID %>').show();
                    $('#<%=txtAdDiscount.ClientID %>').val('0.00');
                    $('#<%=divMrp.ClientID %>').width(81);
                    $('#<%=divAddDis.ClientID %>').css("display", "block");
                    $('#<%=divDis.ClientID %>').css("display", "block");
                    if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                        $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                        $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                        txtCESSPrc.val('0');
                        txtAddCessAmt.val('0');
                    }
                    else {
                        $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                        $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    }
                    $('#<%=divNewCalculation.ClientID%>').hide();
                    $('#<%=divBasicMargin.ClientID%>').hide(); 
                });
            });
            /// work modified by saravanan 29-11-2017
            // $("#dialog-InvPrice").parent().appendTo($("form:first")); // For After Postback 
            $(document).ready(function () {
                fncinitializeinventoryCal();
                $("#dialog-InvPrice").parent().appendTo($("form:first"));
            });

            /// work modified by saravanan 29-11-2017
            ////Initialize Inventory Calculation Pricing
            function fncinitializeinventoryCal() {
                try {
                    $("#dialog-InvPrice").dialog({
                        appendTo: 'form:first',
                        autoOpen: false,
                        resizable: false,
                        height: "auto",
                        width: 1050,
                        modal: true,
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        },
                        buttons: {
                            "Back": function () {
                                fncSaveInvPriceChange();
                                $('#<%=hidPriceEntered.ClientID %>').val("1");
                                $('#<%=txtBarcode.ClientID %>').focus();
                            },
                            Clear: function () {
                                $("#dialog-clear-confirm").dialog("open");
                                //$('#<%=hidPriceEntered.ClientID %>').val("0");
                              
                            },
                            Close: function () {
                                $(this).dialog("close");
                                $('#<%=txtBarcode.ClientID %>').focus();
                            }
                        }
                    });
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }

            //=============================================> Set Default Value in Price Change Dialog
            <%--txtMRPMarkDown.val('0.00')--%>
            if ($('#<%=hidPriceEntered.ClientID %>').val() == "") {
                txtMRPMarkDown.val('0.00')
                txtAdDiscount.val('0.00')
            }//vijay

            //===================================== Price Change Calculation =====================================//

            //=============================>  Input IGST Change
            ddlInputIGST.change(function () {


                if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                    //txtCESSPrc.prop("disabled", "disabled");
                    //txtAddCessAmt.prop("disabled", "disabled");
                    $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                    $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                    txtCESSPrc.val('0');
                    txtAddCessAmt.val('0');
                }
                else {
                    //txtCESSPrc.removeAttr("disabled");
                    //txtAddCessAmt.removeAttr("disabled");
                    $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                    $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                }
                fncInputGSTValueChange();

            });


            /// Input GST Value Changed
            function fncInputGSTValueChange() {
                try {
                    var InputIGSTCode = ddlInputIGST.val();
                    var GSTObj, GSTType;
                    //alert(InputIGSTCode);

                    //=============================>  Output IGST Change
                    ddlOutputIGST.val(InputIGSTCode);
                    ddlOutputIGST.trigger("liszt:updated");

                    if (InputIGSTCode == "EIGST")
                        GSTType = "EIGST";
                    else
                        GSTType = "IGST";

                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                        data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + GSTType + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            //=============================>  Input SGST Change
                            GSTObj = jQuery.parseJSON(msg.d);

                            if (GSTObj.length > 0) {
                                for (var i = 0; i < GSTObj.length; i++) {
                                    if (GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                        //=============================>  Input SGST Change
                                        ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputSGST.trigger("liszt:updated");

                                        var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                        var InputSGSTPrc = array[1];
                                        if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                            txtInputSGSTPrc.val(InputSGSTPrc);
                                        }

                                        //=============================>  Output SGST Change
                                        ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputSGST.trigger("liszt:updated");

                                        var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                        var OutputSGSTPrc = array[1];
                                        if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                            txtOutputSGSTPrc.val(OutputSGSTPrc);
                                            //fncCalcSelingPice();
                                        }

                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                        //=============================>  Input CGST Change
                                        ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputCGST.trigger("liszt:updated");

                                        var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                        var InputCGSTPrc = array[1];
                                        if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                            txtInputCGSTPrc.val(InputCGSTPrc);
                                        }

                                        //=============================>  Output CGST Change
                                        ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputCGST.trigger("liszt:updated");

                                        var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                        var OutputCGSTPrc = array[1];
                                        if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                            txtOutputCGSTPrc.val(OutputCGSTPrc);
                                        }
                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("S") != -1 || GSTObj[i]["TaxCode"].indexOf("ES") != -1) {

                                        //=============================>  Input SGST Change
                                        ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputSGST.trigger("liszt:updated");

                                        var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                        var InputSGSTPrc = array[1];
                                        if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                            txtInputSGSTPrc.val(InputSGSTPrc);
                                        }

                                        //=============================>  Output SGST Change
                                        ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputSGST.trigger("liszt:updated");

                                        var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                        var OutputSGSTPrc = array[1];
                                        if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                            txtOutputSGSTPrc.val(OutputSGSTPrc);
                                            //fncCalcSelingPice();
                                        }

                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 || GSTObj[i]["TaxCode"].indexOf("EC") != -1) {

                                        //=============================>  Input CGST Change
                                        ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputCGST.trigger("liszt:updated");

                                        var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                        var InputCGSTPrc = array[1];
                                        if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                            txtInputCGSTPrc.val(InputCGSTPrc);
                                        }

                                        //=============================>  Output CGST Change
                                        ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputCGST.trigger("liszt:updated");

                                        var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                        var OutputCGSTPrc = array[1];
                                        if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                            txtOutputCGSTPrc.val(OutputCGSTPrc);
                                        }
                                    }//


                                }
                                <%--if (parseFloat($('#<%=txtMRP.ClientID %>').val()) != 0 && parseFloat($('#<%=txtMRPMarkDown.ClientID %>').val()) != 0) {
                                    fnctxtMRPMarkDownFocusout();
                                    fnctxtFixedMarginfocusout();
                                }
                                else {
                                    fncCalcSelingPice();
                                }--%>//cmd by Velu 28-02-2019
                            }//End

                            if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "-- Select --") {
                                $('#<%=txtHSNCode.ClientID %>').select().focus();
                            } else {
                                $('#<%=txtCESSPrc.ClientID %>').select();

                            }
                            fncCalculationNew();
                            fncNetMarginCalculation();
                            TaxLiabilityCalc();
                            fncMDownOnMRP();
                        },
                        error: function (data) {
                            ShowPopupMessageBox('Something Went Wrong')
                        }
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err);
                }
            }
            //Department FocusOut
            $('#<%=txtDepartment.ClientID %>').focusout(function () {
                if ($('#<%=txtItemCode.ClientID %>').val() == "") {
                    fncDepatrtmentWiseitemcode();
                }
            });
            //=============================> CESS Focus Out

            txtCESSPrc.focusout(function () {

                var CessPrc = txtCESSPrc.val();

                if (CessPrc != '') {

                    txtCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                    txtInputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                    txtOutputCESSPrc.val(parseFloat(CessPrc).toFixed(2));

                    if (CessPrc == "0") {

                        $('#<%=txtHSNCode.ClientID %>').select();
                        $('#<%=txtAddCessAmt.ClientID %>').attr("readonly", "readonly");
                        $('#<%=txtAddCessAmt.ClientID %>').val('0');

                    }
                    else if (parseFloat(CessPrc).toFixed(2) <= 100) {
                        $('#<%=txtAddCessAmt.ClientID %>').removeAttr("readonly");
                        $('#<%=txtAddCessAmt.ClientID %>').select();
                        return false;
                    }
                    else {
                        $('#<%=txtCESSPrc.ClientID %>').select();
                        fncInitializeHSNvalidation('% should be less than or equal to 100');
                        FocusCtrl = 'txtCESSPrc.ClientID';
                        return false;
                    }
                    if (parseFloat(CessPrc) > 0) {
                        fncCalculationNew();
                        fncNetMarginCalculation();
                        TaxLiabilityCalc();
                    }
                }
            });

            //=============================> MRP Focus Out
            txtBasicMargin.change(function () { //surya20112021
                if (txtBasicMargin.val() != '') {
                    fncCalculationNew();
                    fncNetMarginCalculation();
                    TaxLiabilityCalc();
                }
            });

            $('#<%=chkAffectNetCost.ClientID %>').change(function () {
                if (txtMRP.val() != '' && parseFloat(txtMRP.val()) > 0) {
                   
                    fncCalculationNew();
                    fncNetMarginCalculation();
                    TaxLiabilityCalc();
                    if ($('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                        fncAfterWSaleMarginSellingPrice('1', txtMarginWPrice1.val());
                        fncAfterWSaleMarginSellingPrice('2', txtMarginWPrice2.val());
                        fncAfterWSaleMarginSellingPrice('3', txtMarginWPrice3.val());
                    }
                }
            });
             
            txtMRP.change(function () {
                generatemrp = txtMRP.val();
                //add by Mowzoon 09032019
                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();

            <%--if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                    fncMRP1_NetCost();
                    fncMRP2_NetSellingPrice_Calculation();
                }
                else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                    fncMRP1_NetCost();
                    fncMRP1_NetSelling();
                }--%>//cmd by velu 09032019


                var MRP = txtMRP.val();
                if (MRP != '') {
                    txtMRP.val(parseFloat(MRP).toFixed(2));
                }

                if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                    var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
                    var SellingPrice = parseFloat(MRP) - parseFloat((MRP * FixedMargin) / 100)
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                if ($('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                    fncAfterWSaleMarginSellingPrice('1', txtMarginWPrice1.val());
                    fncAfterWSaleMarginSellingPrice('2', txtMarginWPrice2.val());
                    fncAfterWSaleMarginSellingPrice('3', txtMarginWPrice3.val());
                }
               
            });

            //=============================> Basic Cost Focus Out
            txtBasicCost.change(function () {

                var BasicCost = txtBasicCost.val();
                if (BasicCost != '') {
                    txtBasicCost.val(parseFloat(BasicCost).toFixed(2));
                    //fncCalcSelingPice();
                    fncCalculationNew();
                    fncNetMarginCalculation();
                    TaxLiabilityCalc();
                }
            });

            //=============================>  Discount PRC 1 Focus Out
            txtDiscountPrc1.change(function () {
                
                    var DiscountPrc1 = txtDiscountPrc1.val();
                    if (DiscountPrc1 != '') {
                        txtDiscountPrc1.val(parseFloat(DiscountPrc1).toFixed(2));
                        fncCalculationNew();
                        fncNetMarginCalculation();
                        TaxLiabilityCalc();
                    }
                
              
            });

            //=============================>  Discount PRC 2 Focus Out 
            txtDiscountPrc2.change(function () {
              
                    var DiscountPrc2 = txtDiscountPrc2.val();
                    if (DiscountPrc2 != '') {
                        txtDiscountPrc2.val(parseFloat(DiscountPrc2).toFixed(2));
                        //fncCalcSelingPice();
                        fncCalculationNew();

                        fncNetMarginCalculation();
                        TaxLiabilityCalc();
                    }
                
            
            });

            //=============================>  Discount PRC 3 Focus Out 
        txtDiscountPrc3.change(function () {
            
                var DiscountPrc3 = txtDiscountPrc3.val();
                if (DiscountPrc3 != '') {
                    txtDiscountPrc3.val(parseFloat(DiscountPrc3).toFixed(2));
                    //fncCalcSelingPice();
                    fncCalculationNew();
                    fncNetMarginCalculation();
                    TaxLiabilityCalc();
                }
            
         });

            //=============================>  Fixed Margin Focus Out
            txtFixedMargin.change(function () {

                fncCalculationNew();
                //add by Mowzoon 09032019
                fncNetMarginCalculation();
                TaxLiabilityCalc();
                if ($('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                    fncAfterWSaleMarginSellingPrice('1', txtMarginWPrice1.val());
                    fncAfterWSaleMarginSellingPrice('2', txtMarginWPrice2.val());
                    fncAfterWSaleMarginSellingPrice('3', txtMarginWPrice3.val());
                }

                <%--var FixedMargin = txtFixedMargin.val();
                if (FixedMargin != '') {
                    //Always Mark Down
                    if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var SellingPrice = (MRP * FixedMargin) / 100
                        SellingPrice = parseFloat(MRP) - parseFloat(SellingPrice)
                        txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                        fncSellingPriceFocusOutCal();
                        //txtSellingPrice.focus(); /// work modified by saravanan 01-12-2017
                        //txtMRPMarkDown.focus();
                    }
                    else {
                        txtFixedMargin.val(parseFloat(FixedMargin).toFixed(2));
                        fncAfterMarginMRPSellingPrice();
                    }

                    // Assign Margin WholeSale Price
                    txtMarginWPrice1.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice2.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice3.val(parseFloat(FixedMargin).toFixed(2))
                }--%> //cmd by Velu 28-02-2019
            });
           $(document).ready(function () {
                var MdownMrp = "0.00";
                var MRP = $('#<%=txtMRP.ClientID %>').val();
                 var SPrice = $('#<%=txtSellingPrice.ClientID %>').val();
                var MrpMargin = parseFloat(MRP) - parseFloat(SPrice);
                /*(MrPMargin / Sellingprice) * 100*/
                 MdownMrp = parseFloat(((MrpMargin) / SPrice) * 100).toFixed(2);
                 $('#<%=txtMDownMrp.ClientID %>').val(MdownMrp);
             }); //surya16102021
            function fnctxtFixedMarginfocusout() {
                var FixedMargin = txtFixedMargin.val();
                if (FixedMargin != '') {
                    //Always Mark Down
                    if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var SellingPrice = (MRP * FixedMargin) / 100
                        SellingPrice = parseFloat(MRP) - parseFloat(SellingPrice)
                        txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                        fncSellingPriceFocusOutCal();
                        //txtSellingPrice.focus(); /// work modified by saravanan 01-12-2017
                        //txtMRPMarkDown.focus();
                    }
                    else {
                        txtFixedMargin.val(parseFloat(FixedMargin).toFixed(2));
                        fncAfterMarginMRPSellingPrice();
                    }

                    // Assign Margin WholeSale Price
                    txtMarginWPrice1.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice2.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice3.val(parseFloat(FixedMargin).toFixed(2))

                    $('#<%=txtEarnedMar1.ClientID %>').val(parseFloat(FixedMargin).toFixed(2))
                    $('#<%=txtEarnedMar2.ClientID %>').val(parseFloat(FixedMargin).toFixed(2))
                    $('#<%=txtEarnedMar3.ClientID %>').val(parseFloat(FixedMargin).toFixed(2))
                }
            }

            //=============================>  Margin WholeSale 1 focus Out
            txtMarginWPrice1.change(function () {
           
                var MarginWPrice1 = txtMarginWPrice1.val();
                if (MarginWPrice1 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice1) == 0) {
                        $('#<%=txtWholePrice1Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var WholeSalePrice1 = (MRP * MarginWPrice1) / 100
                        WholeSalePrice1 = parseFloat(MRP) - parseFloat(WholeSalePrice1)
                        txtWholePrice1Popup.val(parseFloat(WholeSalePrice1).toFixed(2));
                    }
                    else {
                        txtMarginWPrice1.val(parseFloat(MarginWPrice1).toFixed(2));
                        $('#<%= txtEarnedMar1.ClientID %>').val(parseFloat(MarginWPrice1).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('1', MarginWPrice1);
                    }
                }
            });

            //=============================>  Margin WholeSale 2 focus Out
            txtMarginWPrice2.change(function () {
                var MarginWPrice2 = txtMarginWPrice2.val();
                if (MarginWPrice2 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice2) == 0) {
                        $('#<%=txtWholePrice2Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var MarginWPrice2 = (MRP * MarginWPrice2) / 100
                        MarginWPrice2 = parseFloat(MRP) - parseFloat(MarginWPrice2)
                        txtWholePrice2Popup.val(parseFloat(MarginWPrice2).toFixed(2));
                    }
                    else {
                        txtMarginWPrice2.val(parseFloat(MarginWPrice2).toFixed(2));
                        $('#<%= txtEarnedMar2.ClientID %>').val(parseFloat(MarginWPrice2).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('2', MarginWPrice2);
                    }
                }
            });

            //=============================>  Margin WholeSale 3 focus Out
            txtMarginWPrice3.change(function () {
                var MarginWPrice3 = txtMarginWPrice3.val();
                if (MarginWPrice3 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice3) == 0) {
                        $('#<%=txtWholePrice3Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var WholeSalePrice3 = (MRP * MarginWPrice3) / 100
                        WholeSalePrice3 = parseFloat(MRP) - parseFloat(WholeSalePrice3)
                        txtWholePrice3Popup.val(parseFloat(WholeSalePrice3).toFixed(2));
                    }
                    else {
                        txtMarginWPrice3.val(parseFloat(MarginWPrice3).toFixed(2));
                        $('#<%= txtEarnedMar3.ClientID %>').val(parseFloat(MarginWPrice3).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('3', MarginWPrice3);
                    }
                }
            });


            //=============================>  Selling Price Focus Out
            // Re Written by Mowzoon 09-03-2019
            txtSellingPrice.change(function () {
                if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                    Reverse_Calculation_IF_SellingPriceModified_C_M2();
                }
                else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                    Reverse_Calculation_IF_SellingPriceModified_M1();
                }
                else {
                    Reverse_Calculation_IF_SellingPriceModified_C_M2();
                }

                //add by Mowzoon 09032019
                fncNetMarginCalculation();
                TaxLiabilityCalc();
                if (parseFloat(txtWholePrice1Popup.val()) != 0 && parseFloat(txtWholePrice2Popup.val()) != 0
                    && parseFloat(txtWholePrice3Popup.val()) != 0) {
                    fncArriveWpricePercentage("wprice1");
                    fncArriveWpricePercentage("wprice2");
                    fncArriveWpricePercentage("wprice3");
                }
                //fncSellingPriceFocusOutCal();    // Commented by Mowzoon
                MarginCalculationNew();

            });
            // var generatemrp = "";
           <%-- $("#<%=chkGenerateMrp.ClientID %>").click(function () { //Vijay Check
                
                var checked = $(this).is(':checked');
                if (checked == true) {
                    txtMRP.val(parseFloat(txtSellingPrice.val()) + parseFloat($('#<%=hidMarkupMrp.ClientID %>').val())); 
                }
                else {
                    txtMRP.val(generatemrp);
                }
            });--%>
            txtSellingPrice.blur(function () {
                if ($('#<%=hidMarkupMrp.ClientID %>').val().indexOf('T') == 0 && $('#<%=rbtCostPopup.ClientID %>').prop("checked")
                    && $('#<%=txtSubClass.ClientID %>').val() != "" && $('#<%=txtSubClass.ClientID %>').val().trim() == "WithoutMrp") {
                    var MarkupMrpVal = $('#<%=hidMarkupMrp.ClientID %>').val().replace('T', '');
                    txtMRP.val(parseFloat(txtSellingPrice.val()) + parseFloat(MarkupMrpVal));
                    <%--$('#<%=chkBatch.ClientID %>').attr("disabled", "disabled");--%>
                    <%-- }
                else {
                    $('#<%=chkBatch.ClientID %>').removeAttr("disabled");
                }--%>
                }
            });

            // By Mowzoon 09-03-2019
            function Reverse_Calculation_IF_SellingPriceModified_C_M2() {
                var NetSell = txtSellingPrice.val();
                var NetSellwoAdCess = (parseFloat(NetSell) - parseFloat(txtAddCessAmt.val()).toFixed(2)).toFixed(2);
                var TotGSTPer = (parseFloat(txtOutputCGSTPrc.val()) + parseFloat(txtOutputSGSTPrc.val()) + parseFloat(txtCESSPrc.val())).toFixed(2);
                var TotGSTAmt = ((NetSellwoAdCess * TotGSTPer).toFixed(2) / (parseFloat(100) + parseFloat(TotGSTPer)).toFixed(2)).toFixed(2);
                var SGSTAmt = (TotGSTAmt / TotGSTPer * txtOutputSGSTPrc.val()).toFixed(2);
                var CGSTAmt = SGSTAmt;
                var CessAmt = (parseFloat(TotGSTAmt) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt)).toFixed(2);
                var BasicSelling = parseFloat(NetSellwoAdCess) - parseFloat(TotGSTAmt);
                var SPFixing = ((parseFloat(BasicSelling) - parseFloat(txtGrossCost.val())) * 100 / txtGrossCost.val()).toFixed(2);

                if (parseFloat(CessAmt).toFixed(2) == '-0.01' || parseFloat(CessAmt).toFixed(2) == '0.01') {
                    BasicSelling = parseFloat(BasicSelling) + parseFloat(CessAmt);
                    CessAmt = 0.00;
                }

                txtSellingPrice.val(NetSell);
                txtOutputCGSTAmt.val(CGSTAmt);
                txtOutputSGSTAmt.val(SGSTAmt);
                txtOutputCESSAmt.val(CessAmt);
                txtBasicSelling.val(BasicSelling);
                txtFixedMargin.val(SPFixing);
                $('#<%=txtRoundOff.ClientID %>').val(0);

                if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                   
                    txtWholePrice1Popup.val(NetSell);
                    txtWholePrice2Popup.val(NetSell);
                    txtWholePrice3Popup.val(NetSell);
                } else {
                    if (txtWholePrice1Popup.val() == 0)
                        txtWholePrice1Popup.val(NetSell);
                    if (txtWholePrice2Popup.val() == 0)
                        txtWholePrice2Popup.val(NetSell);
                    if (txtWholePrice3Popup.val() == 0)
                        txtWholePrice3Popup.val(NetSell);
                }
            }

            // By Mowzoon 09-03-2019
            function Reverse_Calculation_IF_SellingPriceModified_M1() {
                var NetSell = txtSellingPrice.val();
                var MRP = txtMRP.val();
                var SPFixing = ((parseFloat(MRP) - parseFloat(NetSell)) / MRP * 100).toFixed(2);

                var NetSellwoAdCess = (parseFloat(NetSell) - parseFloat(txtAddCessAmt.val())).toFixed(2);
                var TotGSTPer = (parseFloat(txtOutputCGSTPrc.val()) + parseFloat(txtOutputSGSTPrc.val()) + parseFloat(txtCESSPrc.val())).toFixed(2);
                var TotGSTAmt = ((NetSellwoAdCess * TotGSTPer).toFixed(2) / (parseFloat(100) + parseFloat(TotGSTPer)).toFixed(2)).toFixed(2);
                var SGSTAmt = (TotGSTAmt / TotGSTPer * txtOutputSGSTPrc.val()).toFixed(2);
                var CGSTAmt = SGSTAmt;
                var CessAmt = (parseFloat(TotGSTAmt) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt)).toFixed(2);
                var BasicSelling = parseFloat(NetSell) - parseFloat(TotGSTAmt);

                if (parseFloat(CessAmt).toFixed(2) == '-0.01' || parseFloat(CessAmt).toFixed(2) == '0.01') {
                    BasicSelling = parseFloat(BasicSelling) + parseFloat(CessAmt);
                    CessAmt = 0.00;
                }

                txtSellingPrice.val(NetSell);
                txtOutputCGSTAmt.val(CGSTAmt);
                txtOutputSGSTAmt.val(SGSTAmt);
                txtOutputCESSAmt.val(CessAmt);
                txtBasicSelling.val(BasicSelling);
                txtFixedMargin.val(SPFixing);

                if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                    txtWholePrice1Popup.val(NetSell);
                    txtWholePrice2Popup.val(NetSell);
                    txtWholePrice3Popup.val(NetSell);
                } else {
                    if (txtWholePrice1Popup.val() == 0)
                        txtWholePrice1Popup.val(NetSell);
                    if (txtWholePrice2Popup.val() == 0)
                        txtWholePrice2Popup.val(NetSell);
                    if (txtWholePrice3Popup.val() == 0)
                        txtWholePrice3Popup.val(NetSell);
                }
            }


            txtWholePrice1Popup.change(function () {
                if ($('#<%=hidWPriceReverse.ClientID%>').val() == 'Y')
                    WPriceReverse = 'Y';
                fncArriveWpricePercentage("wprice1");
                WPriceReverse = 'N';
            });
            txtWholePrice2Popup.change(function () {
                if ($('#<%=hidWPriceReverse.ClientID%>').val() == 'Y')
                    WPriceReverse = 'Y';
                fncArriveWpricePercentage("wprice2");
                WPriceReverse = 'N';
            });
            txtWholePrice3Popup.change(function () {
                if ($('#<%=hidWPriceReverse.ClientID%>').val() == 'Y')
                    WPriceReverse = 'Y';
                fncArriveWpricePercentage("wprice3");
                WPriceReverse = 'N';
            });

            /// work modified by saravanan 16-02-2018
            function fncSellingPriceFocusOutCal() {
                try {

                    var SPMargin = 0;
                    var addCessAmt = 0;
                    /// Work done by saravanan 15-02-2018
                    if (parseFloat(NetSellingprice) == parseFloat(txtSellingPrice.val()) && parseFloat(txtFixedMargin.val()) != 0)
                        return;



                    var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
                    var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();
                    addCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
                    /// work modified by saravanan 2018-01-01
                    //var OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
                    //var OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();

                    var OutputSGSTLiabilityAmt, OutputCGSTLiabilityAmt;

                    var BasicSelling = SellingPrice;
                    var OutputSGSTAmt = '0';
                    var OutputCGSTAmt = '0';
                    var OutputCESSAmt = '0';

                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                    var OutputSGSTPrc = txtOutputSGSTPrc.val();
                    var OutputCSGSTPrc = txtOutputCGSTPrc.val();
                    var OutputCESSPrc = txtCESSPrc.val();
                    var TotalOutputGSTPrc = 0;

                    if (OutputSGSTPrc != '') {
                        TotalOutputGSTPrc = OutputSGSTPrc;
                    }
                    if (OutputCSGSTPrc != '') {
                        TotalOutputGSTPrc = parseFloat(TotalOutputGSTPrc) + parseFloat(OutputCSGSTPrc);
                    }
                    if (OutputCESSPrc != '') {
                        TotalOutputGSTPrc = parseFloat(TotalOutputGSTPrc) + parseFloat(OutputCESSPrc);
                    }

                    var TotalOutputGSTAmt = 0;
                    if (TotalOutputGSTPrc != '') {
                        SellingPrice = parseFloat(SellingPrice) - parseFloat(addCessAmt);
                        TotalOutputGSTAmt = ((SellingPrice * TotalOutputGSTPrc) / (parseFloat(TotalOutputGSTPrc) + parseFloat(100)));
                        if (TotalOutputGSTAmt != '') {
                            BasicSelling = parseFloat(SellingPrice) - parseFloat(TotalOutputGSTAmt);
                            txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2));
                            //Split Two Precentage
                            if (OutputSGSTPrc != '') {
                                OutputSGSTAmt = TotalOutputGSTAmt * OutputSGSTPrc / TotalOutputGSTPrc;
                                txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));
                            }
                            if (OutputCGSTAmt != '') {
                                OutputCGSTAmt = TotalOutputGSTAmt * OutputCSGSTPrc / TotalOutputGSTPrc;
                                txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));
                            }
                            if (OutputCESSAmt != '') {
                                OutputCESSAmt = TotalOutputGSTAmt * OutputCESSPrc / TotalOutputGSTPrc;
                                txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtBasicSelling.val(parseFloat(txtSellingPrice.val()).toFixed(2));
                    }

                    //==================> Tax Liability Calc
                    TaxLiabilityCalc();

                    OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
                    OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();

                   <%-- $('#<%=txtGSTLiability.ClientID %>').val(parseFloat(OutputSGSTLiabilityAmt) + parseFloat(OutputSGSTLiabilityAmt));
                    $('#<%=txtGSTLiabilityPer.ClientID %>').val(parseFloat($('#<%=txtVatLiabilitySGSTPrc.ClientID %>').val()) + parseFloat($('#<%=txtVatLiabilityCGSTPrc.ClientID %>').val()));--%>

                    //SellingPrice RoundOff based On ParamMaster
                    txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));


                    //===================> Margin MRP Amt  
                    var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

                    MRP = txtMRP.val();
                    GrossCost = txtGrossCost.val();
                    array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                    if (array == ',, Select ,,') {
                        GSTPer = '0.00';
                    }
                    else
                        GSTPer = array[1];
                    GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
                    MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
                    MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);
                    //MRPGst = (parseFloat(MRP) * parseFloat(GSTPer) /  100);


                    var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost)) - (parseFloat(MRPGst)) / (parseFloat(GSTPer) + 100);
                    MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
                    txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

                    /// work done by saravanan 15-02-2015

                    MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
                    txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));

                    //===================> Margin Selling Price Prc   
                    SellingPrice = txtSellingPrice.val();
                    var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    var SPPrc1 = SPPrc / SellingPrice * 100
                    SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
                    txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

                    //===================> Margin Selling Price Amt 
                    var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    txtSPAmt.val(parseFloat(SPAmt).toFixed(2));


                    // Assign WholeSale Price
                    txtWholePrice1Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice2Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice3Popup.val(parseFloat(SellingPrice).toFixed(2))



                    /// To Arrive SellingPrice Margin
                    if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        if (parseFloat($('#<%=txtGrossCost.ClientID %>').val()) != 0) {
                            SPMargin = (parseFloat($('#<%=txtBasicSelling.ClientID %>').val()) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtFixedMargin.ClientID %>').val(SPMargin.toFixed(2));

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));

                            $('#<%=txtEarnedMar1.ClientID %>').val(parseFloat(SPMargin).toFixed(2))
                            $('#<%=txtEarnedMar2.ClientID %>').val(parseFloat(SPMargin).toFixed(2))
                            $('#<%=txtEarnedMar3.ClientID %>').val(parseFloat(SPMargin).toFixed(2))
                        }
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }


            /// work done by saravanan 16-02-2018
            // To Arrive Whole sale Margin  Viajy WPrices
            function fncArriveWpricePercentage(mode) {
                try {

                    var SPMargin = 0, GSTAmt = 0, WPBasicAmt = 0;
                    var array, GSTPer = 0;

                    <%--if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                        if (array == ',, Select ,,') {
                            GSTPer = '0.00';
                        }
                        else
                            GSTPer = array[1]; 
                        if (mode == "wprice1") {
                          <%--  GSTAmt = (parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W1 = ((parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()))/parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice1.ClientID %>').val(W1); 

                        }
                        else if (mode == "wprice2") {--%>
                    <%-- GSTAmt = (parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W2 = ((parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice2.ClientID %>').val(W2);
                        }
                        else {--%>
                    <%-- GSTAmt = (parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W3 = ((parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()))/parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice3.ClientID %>').val(W3);
                        }
                }--%>

                    array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                    if (array == ',, Select ,,') {
                        GSTPer = '0.00';
                    }
                    else
                        GSTPer = array[1];

                    if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked')) { //surya 22112021 MarkDown Akshaya
                        if (mode == "wprice1") {
                           <%-- var W1 = (parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);--%>
                            //W1 = Math.abs(W1);
                            var W1 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                $('#<%=txtMarginWPrice1.ClientID %>').val(parseFloat(W1).toFixed(2));
                            $('#<%=txtEarnedMar1.ClientID %>').val(parseFloat(W1).toFixed(2))
                        }
                        else if (mode == "wprice2") {
                           <%-- var W2 = (parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);--%>
                            //W2 = Math.abs(W2);
                            var W2 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                $('#<%=txtMarginWPrice2.ClientID %>').val(parseFloat(W2).toFixed(2));
                            $('#<%=txtEarnedMar2.ClientID %>').val(parseFloat(W2).toFixed(2))
                        }
                        else {
                            <%--var W3 = (parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);--%>
                            //W3 = Math.abs(W3);
                            var W3 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                $('#<%=txtMarginWPrice3.ClientID %>').val(parseFloat(W3).toFixed(2));
                            $('#<%=txtEarnedMar3.ClientID %>').val(parseFloat(W3).toFixed(2))
                        }
                    }

                    else {
                       

                        if (mode == "wprice1") {
                          
                           <%-- var W1 = (parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) / parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) * 100).toFixed(2);
                              
                            W1 = Math.abs(W1);--%>
                            var W1 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                if (W1 == "NaN") {
                                    W1 = "0.00";
                                }
                            $('#<%=txtMarginWPrice1.ClientID %>').val(parseFloat(W1).toFixed(2));
                        $('#<%=txtEarnedMar1.ClientID %>').val(parseFloat(W1).toFixed(2));
                    }
                    else if (mode == "wprice2") {
                         <%--   var W2 = (parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) / parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) * 100).toFixed(2);
                           
                            W2 = Math.abs(W2);--%>
                            var W2 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                if (W2 == "NaN") {
                                    W2 = "0.00";
                                }
                            $('#<%=txtMarginWPrice2.ClientID %>').val(parseFloat(W2).toFixed(2));
                        $('#<%=txtEarnedMar2.ClientID %>').val(parseFloat(W2).toFixed(2));
                    }
                    else {
                            <%--var W3 = (parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) / parseFloat($('#<%=hidNetcostnew.ClientID %>').val()) * 100).toFixed(2);
                            W3 = Math.abs(W3);--%>
                            var W3 = $('#<%=txtFixedMargin.ClientID %>').val();
                            if (WPriceReverse == 'N')
                                if (W3 == "NaN") {
                                    W3 = "0.00";
                                }
                            $('#<%=txtMarginWPrice3.ClientID %>').val(parseFloat(W3).toFixed(2));
                            $('#<%=txtEarnedMar3.ClientID %>').val(parseFloat(W3).toFixed(2));
                        }
                    }


                }

                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }
            function fncNetCostcalc() {
                try {
                    txtMRPMarkDown.val(parseFloat(txtMRPMarkDown.val()).toFixed(2));
                    var MRPMarkDown = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                    var MRP = txtMRP.val();
                    var AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
                    if (MRPMarkDown != '' && MRP != '') {
                        var NetCost = parseFloat(MRP) * parseFloat(MRPMarkDown) / 100;
                        NetCost = parseFloat(MRP).toFixed(2) - parseFloat(NetCost).toFixed(2);
                        //NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                        txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        return NetCost;
                    }
                }
                catch (err) {

                    //ShowPopupMessageBox(err.message);
                }
            }

            // Mowzoon - Full Mark Down Net Cost and GST. CESS, Adl Cess Calculation  -  28-02-2019
            function fncMRP1_NetCost() {
                try {

                    var MRP1 = parseFloat(txtMRP.val()).toFixed(2);
                    if (parseFloat(MRP1) > 0) {
                        var MRP1_MDP = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                        var MRP1_MDAP = parseFloat(txtAdDiscount.val()).toFixed(2);
                        //var NetCost = MRP1 - MRP1 * MRP1_MDP / 100;
                        var NetCost = parseFloat(MRP1 - MRP1 * MRP1_MDP / 100).toFixed(2);
                        NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2);

                        var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                        var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                        var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);

                        var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat(txtAddCessAmt.val()).toFixed(2);
                        var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)))).toFixed(2);

                        var SGSTAmt;
                        var CGSTAmt;
                        var CESSAmt;
                        if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                            SGSTAmt = 0;
                            CGSTAmt = 0;
                            CESSAmt = 0;
                        }
                        else {
                            SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) * parseFloat(SGST)).toFixed(2);
                            CGSTAmt = SGSTAmt;
                            CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                        }

                        var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                        if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                            GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                            CESSAmt = 0.00;
                        }

                        txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                        txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                        txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                        txtBasicCost.val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
                        txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                        txtNetCost.val(parseFloat(NetCost).toFixed(2));
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }

            // Mowzoon - Full Mark Down Net Selling and GST. CESS, Adl Cess Calculation  -  28-02-2019
            function fncMRP1_NetSelling() {
                try {
                    //txtFixedMargin
                    //Basic Selling : txtBasicSelling
                    //Output SGST txtOutputSGSTPrc txtOutputSGSTAmt
                    //Output CGST txtOutputCGSTPrc  txtOutputCGSTAmt
                    //Output CESS txtOutputCESSPrc  txtOutputCESSAmt

                    //RoundOFF : txtRoundOff
                    //NetSelling txtSellingPrice
                    //Liab % :  txtGSTLiabilityPer   Amt: txtGSTLiability
                    if (parseFloat(txtMRP.val()) > 0) {
                        var NetSelling = parseFloat(txtMRP.val()).toFixed(2) - (parseFloat(txtMRP.val()).toFixed(2) * parseFloat(txtFixedMargin.val()).toFixed(2) / 100);

                        var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                        var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                        var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                        var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                        var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer) + parseFloat(OutputCESSPer);

                        var TotalTaxAmt = ((parseFloat(NetSelling) - parseFloat(AdlCessAmt)) * TotalPer / (parseFloat(100) + parseFloat(TotalPer))).toFixed(2);

                        var OutputCGSTAmt;
                        var OutputSGSTAmt;
                        var OutputCESSAmt;
                        if (TotalPer == 0) {
                            OutputCGSTAmt = 0;
                            OutputSGSTAmt = 0;
                            OutputCESSAmt = 0;
                        }
                        else {
                            OutputCGSTAmt = (TotalTaxAmt / TotalPer * OutputCGSTPer).toFixed(2);
                            OutputSGSTAmt = OutputCGSTAmt;
                            OutputCESSAmt = parseFloat(TotalTaxAmt) - parseFloat(OutputCGSTAmt) - parseFloat(OutputSGSTAmt);
                        }


                        var BasicSelling = parseFloat(NetSelling) - parseFloat(AdlCessAmt) - parseFloat(TotalTaxAmt);

                        if (parseFloat(OutputCESSAmt).toFixed(2) == '-0.01' || parseFloat(OutputCESSAmt).toFixed(2) == '0.01') {
                            BasicSelling = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt);
                            OutputCESSAmt = 0.00;
                        }
                        txtBasicSelling.val(BasicSelling);
                        txtOutputCGSTAmt.val(OutputCGSTAmt);
                        txtOutputSGSTAmt.val(OutputSGSTAmt);
                        txtOutputCESSAmt.val(OutputCESSAmt);
                        txtSellingPrice.val(NetSelling);

                        txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));

                        if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                            txtWholePrice1Popup.val(NetSelling);
                            txtWholePrice2Popup.val(NetSelling);
                            txtWholePrice3Popup.val(NetSelling);
                        } else {
                            if (txtWholePrice1Popup.val() == 0)
                                txtWholePrice1Popup.val(NetSelling);
                            if (txtWholePrice2Popup.val() == 0)
                                txtWholePrice2Popup.val(NetSelling);
                            if (txtWholePrice3Popup.val() == 0)
                                txtWholePrice3Popup.val(NetSelling);
                        }
                        // Call 
                        //TaxLiabilityCalc();
                        //fncNetMarginCalculation();
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }

            }

            //----------Mark Down -- Arrive Gross Cost from MRP and Net Cost from Gross Cost
            function fncMRP34_GrossCost() {
                try {

                    var MRP34 = parseFloat(txtMRP.val()).toFixed(2);
                    var MRP34_MDP = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                    var MRP34_MDAP = parseFloat(txtAdDiscount.val()).toFixed(2);

                    var GrossCost = parseFloat(MRP34 - MRP34 * MRP34_MDP / 100).toFixed(2);
                    GrossCost = GrossCost - parseFloat(GrossCost * MRP34_MDAP / 100).toFixed(2);

                    var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                    var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                    var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);
                    var AdCess = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var SGSTAmt;
                    var CGSTAmt;
                    var CESSAmt;

                    if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                        SGSTAmt = 0;
                        CGSTAmt = 0;
                        CESSAmt = 0;
                    }
                    else {
                        SGSTAmt = (parseFloat(GrossCost) * parseFloat(SGST) / 100).toFixed(2);
                        CGSTAmt = SGSTAmt;
                        CESSAmt = (parseFloat(GrossCost) * parseFloat(CESS) / 100).toFixed(2);
                    }

                    var NetCost = (parseFloat(parseFloat(GrossCost) + parseFloat(SGSTAmt) + parseFloat(CGSTAmt) + parseFloat(CESSAmt) + parseFloat(AdCess))).toFixed(2);

                    if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                        GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                        CESSAmt = 0.00;
                    }

                    txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                    txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                    txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                    txtNetCost.val(parseFloat(NetCost).toFixed(2));
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }
            //--=============================================================================

            <%--//Calculation of Net Margin Selling Price and MRP
        function fncNetMarginCalculation()//by velu 28-02-2019
        {
            try {
                OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
                OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();
                //SellingPrice RoundOff based On ParamMaster
                txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));

                //===================> Margin MRP Amt  

                var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

                MRP = txtMRP.val();
                GrossCost = txtGrossCost.val();
                array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                if (array == ',, Select ,,') {
                    GSTPer = '0.00';
                }
                else
                    GSTPer = array[1];
                GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
                MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
                MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);



                var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
                MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
                txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

                /// work done by saravanan 15-02-2015

                MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
                txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));
                //===================> Margin Selling Price Prc  
                var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
                var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();

                var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    var SPPrc1 = SPPrc / SellingPrice * 100
                    SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
                    txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

                //===================> Margin Selling Price Amt 
                    var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    txtSPAmt.val(parseFloat(SPAmt).toFixed(2));

                // Assign WholeSale Price
                    txtWholePrice1Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice2Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice3Popup.val(parseFloat(SellingPrice).toFixed(2))

                /// To Arrive SellingPrice Margin
                    if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        if (parseFloat($('#<%=txtGrossCost.ClientID %>').val()) != 0) {
                            SPMargin = (parseFloat($('#<%=txtBasicSelling.ClientID %>').val()) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtFixedMargin.ClientID %>').val(SPMargin.toFixed(2));

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));
                        }
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }--%>

            function fncMRP2_NetSellingPrice_Calculation() {
                try {

                    var BasicSelling = parseFloat(parseFloat(txtGrossCost.val()) + parseFloat(txtGrossCost.val() * txtFixedMargin.val() / 100)).toFixed(2);

                    var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                    var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                    var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                    var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer);

                    var TotalTaxAmt = parseFloat(BasicSelling * TotalPer / 100).toFixed(2);

                    var OutputCGSTAmt = parseFloat(TotalTaxAmt / 2).toFixed(2);
                    var OutputSGSTAmt = OutputCGSTAmt;
                    var OutputCESSAmt = parseFloat(BasicSelling * OutputCESSPer / 100).toFixed(2);

                    var NetSellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCESSAmt) + parseFloat(AdlCessAmt);

                    txtBasicSelling.val(BasicSelling);
                    txtOutputCGSTAmt.val(OutputCGSTAmt);
                    txtOutputSGSTAmt.val(OutputSGSTAmt);
                    txtOutputCESSAmt.val(OutputCESSAmt);
                    txtSellingPrice.val(NetSellingPrice);

                    txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));


                    if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                        txtWholePrice1Popup.val(NetSellingPrice);
                        txtWholePrice2Popup.val(NetSellingPrice);
                        txtWholePrice3Popup.val(NetSellingPrice);
                    } else {
                        if (txtWholePrice1Popup.val() == 0)
                            txtWholePrice1Popup.val(NetSellingPrice);
                        if (txtWholePrice2Popup.val() == 0)
                            txtWholePrice2Popup.val(NetSellingPrice);
                        if (txtWholePrice3Popup.val() == 0)
                            txtWholePrice3Popup.val(NetSellingPrice);
                    }
                    // Call 
                    //TaxLiabilityCalc();
                    //fncNetMarginCalculation();
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }


            //=============================>  MRP PRC Focus Out
            function fnctxtMRPMarkDownFocusout() {

                fncMRP1_NetCost();
<%--            txtMRPMarkDown.val(parseFloat(txtMRPMarkDown.val()).toFixed(2));
            var MRPMarkDown = parseFloat(txtMRPMarkDown.val()).toFixed(2);
            var MRP = txtMRP.val();
            var AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
            if (MRPMarkDown != '' && MRP != '') {
                var NetCost = parseFloat(MRP) * parseFloat(MRPMarkDown) / 100;
                NetCost = parseFloat(MRP).toFixed(2) - parseFloat(NetCost).toFixed(2);
                //NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                txtNetCost.val(parseFloat(NetCost).toFixed(2));
                var InputSGSTAmt = '0';
                var InputCGSTAmt = '0';
                var InputCESSAmt = '0';
                if (NetCost != '') {
                    var InputSGSTPrc = txtInputSGSTPrc.val();
                    var InputCGSTPrc = txtInputCGSTPrc.val();
                    var InputCESSPrc = txtInputCESSPrc.val();
                    var TotalInputGSTPrc = 0;
                    if (InputSGSTPrc != '') {
                        TotalInputGSTPrc = InputSGSTPrc;
                    }
                    if (InputCGSTPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCGSTPrc);
                    }
                    if (InputCESSPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCESSPrc);
                    }

                    var TotalInputGSTAmt = 0;
                    var GrossCost = 0;

                    //NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                    //TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                    ////bugger;
                    if (TotalInputGSTPrc != '') {
                        NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                        TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                        if (TotalInputGSTAmt != '') {
                            //GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(TotalInputGSTAmt).toFixed(2);
                            //txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                            //Split Precentage
                            if (InputSGSTPrc != '') {
                                InputSGSTAmt = TotalInputGSTAmt * InputSGSTPrc / TotalInputGSTPrc;
                                txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                            }
                            if (InputCGSTPrc != '') {
                                InputCGSTAmt = TotalInputGSTAmt * InputCGSTPrc / TotalInputGSTPrc;
                                txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                            }
                            if (InputCESSAmt != '') {
                                InputCESSAmt = TotalInputGSTAmt * InputCESSPrc / TotalInputGSTPrc;
                                txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtGrossCost.val(parseFloat(NetCost).toFixed(2));
                    }

                    GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(InputSGSTAmt).toFixed(2) - parseFloat(InputCGSTAmt).toFixed(2) - parseFloat(InputCESSAmt);
                    //GrossCost = parseFloat(GrossCost) - parseFloat(AddCessAmt);
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2)); //GrossCost
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2)); //BasicCost                                               

                    //==================> Vat Liability Calc
                    TaxLiabilityCalc();

                }
            }

            fncCalcSelingPice();
            if(parseFloat($('#<%=txtFixedMargin.ClientID %>').val()) != 0){
            fnctxtFixedMarginfocusout();
            }--%>
            }
            txtMRPMarkDown.change(function () {


                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();
            <%--txtMRPMarkDown.val(parseFloat(txtMRPMarkDown.val()).toFixed(2));
            var MRPMarkDown = parseFloat(txtMRPMarkDown.val()).toFixed(2);
            var MRP = txtMRP.val();
            var AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
            if (MRPMarkDown != '' && MRP != '') {
                var NetCost = parseFloat(MRP) * parseFloat(MRPMarkDown) / 100;
                NetCost = parseFloat(MRP).toFixed(2) - parseFloat(NetCost).toFixed(2);
                //NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                txtNetCost.val(parseFloat(NetCost).toFixed(2));
                var InputSGSTAmt = '0';
                var InputCGSTAmt = '0';
                var InputCESSAmt = '0';
                if (NetCost != '') {
                    var InputSGSTPrc = txtInputSGSTPrc.val();
                    var InputCGSTPrc = txtInputCGSTPrc.val();
                    var InputCESSPrc = txtInputCESSPrc.val();
                    var TotalInputGSTPrc = 0;
                    if (InputSGSTPrc != '') {
                        TotalInputGSTPrc = InputSGSTPrc;
                    }
                    if (InputCGSTPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCGSTPrc);
                    }
                    if (InputCESSPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCESSPrc);
                    }

                    var TotalInputGSTAmt = 0;
                    var GrossCost = 0;

                    //NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                    //TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                    ////bugger;
                    if (TotalInputGSTPrc != '') {
                        NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                        TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                        if (TotalInputGSTAmt != '') {
                            //GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(TotalInputGSTAmt).toFixed(2);
                            //txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                            //Split Precentage
                            if (InputSGSTPrc != '') {
                                InputSGSTAmt = TotalInputGSTAmt * InputSGSTPrc / TotalInputGSTPrc;
                                txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                            }
                            if (InputCGSTPrc != '') {
                                InputCGSTAmt = TotalInputGSTAmt * InputCGSTPrc / TotalInputGSTPrc;
                                txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                            }
                            if (InputCESSAmt != '') {
                                InputCESSAmt = TotalInputGSTAmt * InputCESSPrc / TotalInputGSTPrc;
                                txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtGrossCost.val(parseFloat(NetCost).toFixed(2));
                    }

                    GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(InputSGSTAmt).toFixed(2) - parseFloat(InputCGSTAmt).toFixed(2) - parseFloat(InputCESSAmt);
                    //GrossCost = parseFloat(GrossCost) - parseFloat(AddCessAmt);
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2)); //GrossCost
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2)); //BasicCost                                               

                    //==================> Vat Liability Calc
                    TaxLiabilityCalc();

                }
            }

            fncCalcSelingPice();
            if(parseFloat($('#<%=txtFixedMargin.ClientID %>').val()) != 0){
            fnctxtFixedMarginfocusout();
            }--%>

            });

            txtAddCessAmt.change(function () {//Add by Velu 28-02-2019
                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();
            });
            ///vijay
            function MarginCalculationNew() {
                try {
                    var MrpMargin = 0, MrpMarkup = 0, MrpmarkDown = 0;
                    var Sellmargin = 0, SellMarkUp = 0, SellMarkDown = 0;
                    if (parseFloat(txtNetCost.val()) > 0 && parseFloat(txtMRP.val()) > 0) {
                        MrpMargin = parseFloat(txtMRP.val()) - parseFloat(txtNetCost.val());
                        MrpMarkup = parseFloat((MrpMargin / parseFloat(txtNetCost.val())) * 100).toFixed(2);
                        MrpmarkDown = parseFloat((MrpMargin / parseFloat(txtMRP.val())) * 100).toFixed(2);
                    }
                    if (parseFloat(txtSellingPrice.val()) > 0 && parseFloat(txtNetCost.val()) > 0) {
                        Sellmargin = parseFloat(txtSellingPrice.val()) - parseFloat(txtNetCost.val());
                        SellMarkUp = parseFloat((Sellmargin / parseFloat(txtNetCost.val())) * 100).toFixed(2);
                        SellMarkDown = parseFloat((Sellmargin / parseFloat(txtSellingPrice.val())) * 100).toFixed(2);
                    }
                    $(<%=txtMRPMargrin.ClientID%>).val(parseFloat(MrpMargin).toFixed(2));
                    $(<%=txtsellMargin.ClientID%>).val(parseFloat(Sellmargin).toFixed(2));
                    $(<%=txtMRPMarkDownNew.ClientID%>).val(MrpmarkDown);
                    $(<%=txtMRPMarkUp.ClientID%>).val(MrpMarkup);
                    $(<%=txtSellMarkDown.ClientID%>).val(SellMarkDown);
                    $(<%=txtSellMarkUp.ClientID%>).val(SellMarkUp);
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }

            function fncCalculationNew() {
                if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                    fncMRP1_NetCost();
                    fncMRP2_NetSellingPrice_Calculation();
                }
                else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                    fncMRP1_NetCost();
                    fncMRP1_NetSelling();
                }
                else if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                    if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "N" || parseFloat(txtBasicMargin.val()) == 0)  
                        fncMarkup_Netcost_Calculation();
                    else if ($(<%=ddlPurchase.ClientID%>).val() == "Cost" && parseFloat(txtBasicMargin.val()) > 0)
                        fncMarkup_Netcost_Calculation_New();
                    else  
                        fncMRP1_NetCost_New();
                    if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "N")  
                        fncMRP2_NetSellingPrice_Calculation();
                     else if ($(<%=ddlSales.ClientID%>).val() == "Cost") 
                        fncMRP2_NetSellingPrice_Calculation_New();
                     else
                        fncMRP1_NetSelling_New(); 
                }
                else if ($('#<%=rbnMRP3.ClientID %>').is(':checked')) {
                    fncMRP34_GrossCost();
                    fncMRP1_NetSelling();
                }
                else if ($('#<%=rbnMRP4.ClientID %>').is(':checked')) {
                    fncMRP34_GrossCost();
                    fncMRP2_NetSellingPrice_Calculation();
                }

                MarginCalculationNew();
                if (parseFloat(txtWholePrice1Popup.val()) != 0 && parseFloat(txtWholePrice2Popup.val()) != 0
                    && parseFloat(txtWholePrice3Popup.val()) != 0) {
                    fncArriveWpricePercentage("wprice1");
                    fncArriveWpricePercentage("wprice2");
                    fncArriveWpricePercentage("wprice3");
                }
            }
            $(<%=ddlPurchase.ClientID%>).change(function () { 
                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();
               <%-- if ($('#<%=ddlPurchase.ClientID %>').val() == "Cost") {--%>
                    $(txtBasicCost).removeAttr("disabled");
                    $('#<%=txtMRPMarkDown.ClientID %>').hide();
                    $(txtDiscountAmt).attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP');
                    $('#<%=txtAdDiscount.ClientID %>').hide();
                    $('#<%=divMrp.ClientID %>').width(125);
                    $('#divAddDis').css("display", "none");
                    $('#divDis').css("display", "none");
                    $('#<%=divAddDis.ClientID %>').css("display", "none");
                    $('#<%=divDis.ClientID %>').css("display", "none");
                    $(txtDiscountPrc1).removeAttr("disabled");
                    $(txtDiscountPrc2).removeAttr("disabled");
                    $(txtDiscountPrc3).removeAttr("disabled");
                    
              //  }
                <%--else {
                    $('#ContentPlaceHolder1_lblAdDiscount').text('MRP/Dis%/AdDis%');
                    $(txtBasicCost).attr("disabled", "disabled");
                    $('#<%=txtMRPMarkDown.ClientID %>').show();
                    $('#<%=txtMRPMarkDown.ClientID %>').val('0.00');
                    $('#<%=txtAdDiscount.ClientID %>').show();
                    $('#<%=txtAdDiscount.ClientID %>').val('0.00');
                    $('#<%=divMrp.ClientID %>').width(80);
                    $('#<%=divAddDis.ClientID %>').css("display", "block");
                    $('#<%=divDis.ClientID %>').css("display", "block"); 
                    $(txtMRPMarkDown).removeAttr("disabled");
                    $(txtDiscountPrc1).attr("disabled", "disabled");
                    $(txtDiscountPrc2).attr("disabled", "disabled");
                    $(txtDiscountPrc3).attr("disabled", "disabled");
                }--%>
            });
            $(<%=ddlSales.ClientID%>).change(function () {
                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();
            });
            txtAdDiscount.change(function () {
                fncCalculationNew();
                fncNetMarginCalculation();
                TaxLiabilityCalc();

            <%--txtAdDiscount.val(parseFloat(txtAdDiscount.val()).toFixed(2));
            var MRPMarkDown = parseFloat(txtAdDiscount.val()).toFixed(2);
            var MRP = fncNetCostcalc();
            var AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
            if (MRPMarkDown != '' && MRP != '') {
                var NetCost = parseFloat(MRP) * parseFloat(MRPMarkDown) / 100;
                NetCost = parseFloat(MRP).toFixed(2) - parseFloat(NetCost).toFixed(2);
                //NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                txtNetCost.val(parseFloat(NetCost).toFixed(2));
                var InputSGSTAmt = '0';
                var InputCGSTAmt = '0';
                var InputCESSAmt = '0';
                if (NetCost != '') {
                    var InputSGSTPrc = txtInputSGSTPrc.val();
                    var InputCGSTPrc = txtInputCGSTPrc.val();
                    var InputCESSPrc = txtInputCESSPrc.val();
                    var TotalInputGSTPrc = 0;
                    if (InputSGSTPrc != '') {
                        TotalInputGSTPrc = InputSGSTPrc;
                    }
                    if (InputCGSTPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCGSTPrc);
                    }
                    if (InputCESSPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCESSPrc);
                    }

                    var TotalInputGSTAmt = 0;
                    var GrossCost = 0;

                    //NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                    //TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                    ////bugger;
                    if (TotalInputGSTPrc != '') {
                        NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                        TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                        if (TotalInputGSTAmt != '') {
                            //GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(TotalInputGSTAmt).toFixed(2);
                            //txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                            //Split Precentage
                            if (InputSGSTPrc != '') {
                                InputSGSTAmt = TotalInputGSTAmt * InputSGSTPrc / TotalInputGSTPrc;
                                txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                            }
                            if (InputCGSTPrc != '') {
                                InputCGSTAmt = TotalInputGSTAmt * InputCGSTPrc / TotalInputGSTPrc;
                                txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                            }
                            if (InputCESSAmt != '') {
                                InputCESSAmt = TotalInputGSTAmt * InputCESSPrc / TotalInputGSTPrc;
                                txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtGrossCost.val(parseFloat(NetCost).toFixed(2));
                    }

                    GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(InputSGSTAmt).toFixed(2) - parseFloat(InputCGSTAmt).toFixed(2) - parseFloat(InputCESSAmt);
                    //GrossCost = parseFloat(GrossCost) - parseFloat(AddCessAmt);
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2)); //GrossCost
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2)); //BasicCost                                               

                    //==================> Vat Liability Calc
                    TaxLiabilityCalc();

                }
            }

            fncCalcSelingPice();
                if(parseFloat($('#<%=txtFixedMargin.ClientID %>').val()) != 0){
            fnctxtFixedMarginfocusout();
            }--%>

            });
            //=============================> ReOrder Process
            //=============================>  Max Qty Focus Out
            txtMaxQty.change(function () {
                if (parseInt(txtMinimumQty.val()) > parseInt(txtMaxQty.val())) {
                    // work done by saravanan 2017-11-28
                    ShowPopupMessageBoxandFocustoObject('Max Qty should greater than Min qty');
                    popUpObjectForSetFocusandOpen = txtMaxQty;
                    //txtMaxQty.focus().select();
                    return;
                }
                if (txtMinimumQty.val() != '' && txtMaxQty.val() != '') {
                    txtReOrderQty.val(parseInt(txtMaxQty.val()) - parseInt(txtMinimumQty.val()))
                }
            });
            txtMinimumQty.change(function () { //surya16112021
                if (parseInt(txtMinimumQty.val()) > parseInt(txtMaxQty.val())) {
                    
                    ShowPopupMessageBoxandFocustoObject('Max Qty should greater than Min qty');
                    popUpObjectForSetFocusandOpen = txtMaxQty;
                    //txtMaxQty.focus().select();
                    return;
                }
                if (txtMinimumQty.val() != '' && txtMaxQty.val() != '') {
                    txtReOrderQty.val(parseInt(txtMaxQty.val()) - parseInt(txtMinimumQty.val()))
                }
            });
            //============================> For Maintain Current Tab while postback
            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "InventoryPricing";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs ul li a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });

                // For Scrol down in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $("html, body").scrollTop($(document).height());
                })
            });

            //=============================> Set Width for Chosen Dropdown

            $("#<%=ddlInputSGST.ClientID %>, #<%=ddlInputSGST.ClientID %>_chzn, #<%=ddlInputSGST.ClientID %>_chzn > div, #<%=ddlInputSGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlInputCGST.ClientID %>, #<%=ddlInputCGST.ClientID %>_chzn, #<%=ddlInputCGST.ClientID %>_chzn > div, #<%=ddlInputCGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlInputIGST.ClientID %>, #<%=ddlInputIGST.ClientID %>_chzn, #<%=ddlInputIGST.ClientID %>_chzn > div, #<%=ddlInputIGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputSGST.ClientID %>, #<%=ddlOutputSGST.ClientID %>_chzn, #<%=ddlOutputSGST.ClientID %>_chzn > div, #<%=ddlOutputSGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputCGST.ClientID %>, #<%=ddlOutputCGST.ClientID %>_chzn, #<%=ddlOutputCGST.ClientID %>_chzn > div, #<%=ddlOutputCGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputIGST.ClientID %>, #<%=ddlOutputIGST.ClientID %>_chzn, #<%=ddlOutputIGST.ClientID %>_chzn > div, #<%=ddlOutputIGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlMiniBy.ClientID %>, #<%=ddlMiniBy.ClientID %>_chzn, #<%=ddlMiniBy.ClientID %>_chzn > div, #<%=ddlMiniBy.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlByReOrder.ClientID %>, #<%=ddlByReOrder.ClientID %>_chzn, #<%=ddlByReOrder.ClientID %>_chzn > div, #<%=ddlByReOrder.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOrderBy.ClientID %>, #<%=ddlOrderBy.ClientID %>_chzn, #<%=ddlOrderBy.ClientID %>_chzn > div, #<%=ddlOrderBy.ClientID %>_chzn > div > div > input").css("width", '100%');

            $("#<%=ddlPurchase.ClientID %>, #<%=ddlPurchase.ClientID %>_chzn, #<%=ddlPurchase.ClientID %>_chzn > div, #<%=ddlPurchase.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlSales.ClientID %>, #<%=ddlSales.ClientID %>_chzn, #<%=ddlSales.ClientID %>_chzn > div, #<%=ddlSales.ClientID %>_chzn > div > div > input").css("width", '100%');

            //// AutoComplete Search for Package
            $("[id$=txtMultipleUOM]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term.replace("'", "''");
                    obj.mode = "Package";

                    $.ajax({
                        url: '<%=ResolveUrl("frmInventoryMaster.aspx/fncCommonSearch")%>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valConvQty: item.split('|')[2]

                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtMultipleUOM.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtMultipleUOM.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtConvQty.ClientID %>').val($.trim(i.item.valConvQty));
                    $('#<%=cbUOMBin.ClientID %>').select();

                                           <%-- if ($.trim(i.item.valitemcode).toUpperCase() == "KG") {
                                                $('#<%=txtConvQty.ClientID %>').attr("disabled", false);
                                                $('#<%=txtConvQty.ClientID %>').select();
                                            }
                                            else {
                                                $('#<%=txtConvQty.ClientID %>').attr("disabled", true);
                                                $('#<%=cbUOMBin.ClientID %>').select();
                                            }--%>
                    return false;
                },
                minLength: 1
            });
            fncStockandPurhistorycheckchanged();
        }  //
        //========================================================End of Page Load=========================================================================================
        /// Add New Vendor
        function fncAddNewVendor(vendorcode, vendorname) {
            try {
                var tblNewVendorCreation, rowNo = 1, row;
                var status = true;
                tblNewVendorCreation = $("#tblNewVendorCreation tbody");


                /// save Multiple Vendor
                var obj = {};
                obj.mode = "Save";
                obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
                obj.vendorcode = vendorcode;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncSaveAndDeleteMultipleVendor") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "Success") {

                            if (tblNewVendorCreation.children().length == 0) {
                                row = "<tr>"
                                    + "<td id='tdRowNo' >" + rowNo + "</td>"
                                    + "<td id='tdVendorcode' >" + vendorcode + "</td>"
                                    + "<td id='tdVendorname' >" + vendorname + "</td>"
                                    + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                                    + "</tr>";
                            }
                            else {
                                tblNewVendorCreation.children().each(function () {

                                    if ($(this).find('td[id*=tdVendorcode]').text().trim() == vendorcode) {
                                        status = false;
                                        popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                                                                    ShowPopupMessageBoxandFocustoObject("Already vendor Added");
                                                                    return false;
                                                                }

                                                                rowNo = $(this).find('td[id*=tdRowNo]').text();
                                                                rowNo = parseInt(rowNo) + 1;
                                                                row = "<tr>"
                                                                    + "<td id='tdRowNo' >" + rowNo + "</td>"
                                                                    + "<td id='tdVendorcode' >" + vendorcode + "</td>"
                                                                    + "<td id='tdVendorname' >" + vendorname + "</td>"
                                                                    + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                                                                    + "</tr>";


                                                            });
                                                        }

                                                        if (status == true) {
                                                            tblNewVendorCreation.append(row);
                                                            tblNewVendorCreation.children().click(fncMultipleVendorRowClick);

                                                            popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('Vendor added successfully');
                            }



                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });

                $('#<%=txtNewVendorcode.ClientID %>').val("");
                //$('#<%=txtNewVendorcode.ClientID %>').select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// to show back ground color on click
        function fncMultipleVendorRowClick() {
            try {
                $(this).css("background-color", "#80b3ff");
                $(this).siblings().css("background-color", "white");

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncDeleteMultipleVendor(source) {
            var rowNo = 1, rowObj, obj = {};;
            try {
                /// save Multiple Vendor
                rowObj = $(source).parent().parent();
                obj.mode = "Delete";
                obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
                obj.vendorcode = rowObj.find('td[id*=tdVendorcode]').text();
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncSaveAndDeleteMultipleVendor") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "Success") {
                            rowObj.remove();
                            $("#tblNewVendorCreation tbody").children().each(function () {
                                $(this).find('td[id*=tdRowNo]').text(rowNo)
                                rowNo = rowNo + 1;
                            });
                            popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                            ShowPopupMessageBoxandFocustoObject('Vendor Deleted successfully');
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });

                $('#<%=txtNewVendorcode.ClientID %>').val("");


            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <%--------------------------------------- End Page Load Script --------------------------------------%>
    <%--------------------------------------- Selling Price Calculation --------------------------------------%>
    <script type="text/javascript">


        function fncCalcSelingPice() {
            try {

                var BasicCost = txtBasicCost.val();
                var AddCessAmt = 0;
                if (BasicCost == '') {
                    return false;
                }
                txtGrossCost.val(parseFloat(BasicCost).toFixed(2));  // Assign to Gross Cost from Basic Cost Value
                AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
                var GrossCost = txtGrossCost.val();
                if (GrossCost == '') {
                    return false;
                }

                //================> Discount 1
                txtDiscountAmt.val('0'); // Initial State //Surya18112021 for Skip discount adding
                
                    var DisPrc1 = txtDiscountPrc1.val();
                    if (DisPrc1 != '') {
                        var DisAmt = (GrossCost * DisPrc1) / 100;
                        DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                        txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                    }
                
                //================> Discount 2
                
                    var DisPrc2 = txtDiscountPrc2.val();
                    if (DisPrc2 != '') {
                        var DisAmt = (GrossCost * DisPrc2) / 100;

                        if (txtDiscountAmt.val() == '') {
                            txtDiscountAmt.val('0');
                        }
                        DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                        txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                    }
                

                //================> Discount 3
            
                    var DisPrc3 = txtDiscountPrc3.val();
                    if (DisPrc3 != '') {
                        var DisAmt = (GrossCost * DisPrc3) / 100;

                        if (txtDiscountAmt.val() == '') {
                            txtDiscountAmt.val('0');
                        }
                        DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                        txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                    }
                

                txtGrossCost.val(parseFloat(GrossCost).toFixed(2));  // Set After Discount Calculation

                //==================> Input SGST
                var InputSGSTPrc = txtInputSGSTPrc.val();
                if (InputSGSTPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputSGSTAmt = (GrossCost * InputSGSTPrc) / 100;
                        if (InputSGSTAmt == '') {
                            InputSGSTAmt = '0';
                        }
                        txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputCGSTAmt = txtInputCGSTAmt.val();
                        if (InputCGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }

                //==================> Input CGST
                var InputCGSTPrc = txtInputCGSTPrc.val();
                if (InputCGSTPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputCGSTAmt = (GrossCost * InputCGSTPrc) / 100;
                        if (InputCGSTAmt == '') {
                            InputCGSTAmt = '0';
                        }
                        txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputSGSTAmt = txtInputSGSTAmt.val();
                        if (InputSGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCGSTAmt) + parseFloat(InputSGSTAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCGSTAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }

                //==================> Input CESS
                var InputCESSPrc = txtInputCESSPrc.val();
                if (InputCESSPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputCESSAmt = (GrossCost * InputCESSPrc) / 100;
                        if (InputCESSAmt == '') {
                            InputCESSAmt = '0';
                        }
                        txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputSGSTAmt = txtInputSGSTAmt.val();
                        var InputCGSTAmt = txtInputCGSTAmt.val();
                        if (InputSGSTAmt != '' && InputCGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt) + parseFloat(InputCESSAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCESSAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }


                //==================> Based on Fixed Margin                
                var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
                txtFixedMargin.val(parseFloat(FixedMargin).toFixed(2))
                fncAfterMarginMRPSellingPrice();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncMDownOnMRP() {
            var MdownMrp = "0.00"
            var MRPnew = $('#<%=txtMRP.ClientID %>').val();
            var SPricenew = $('#<%=txtSellingPrice.ClientID %>').val();
                        var MrpMargin = parseFloat(MRPnew) - parseFloat(SPricenew);
                        
                        if (MrpMargin != "0" && SPricenew != "0") {
                            MdownMrp = parseFloat(((MrpMargin) / SPricenew) * 100).toFixed(2);
                        }
                       
                        
            $('#<%=txtMDownMrp.ClientID %>').val(MdownMrp);
        }

        function fncAfterMarginMRPSellingPrice() {

            //==================> Selling Price CGST
            var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
            var MRP = txtMRP.val() == '' ? '0' : txtMRP.val();
            var BasicCost = txtBasicCost.val() == '' ? '0' : txtBasicCost.val();
            var GrossCost = txtGrossCost.val() == '' ? '0' : txtGrossCost.val();
            var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();
            var InputSGSTPrc = txtInputSGSTPrc.val() == '' ? '0' : txtInputSGSTPrc.val();
            var InputCGSTPrc = txtInputCGSTPrc.val() == '' ? '0' : txtInputCGSTPrc.val();
            var InputCESSPrc = txtInputCESSPrc.val() == '' ? '0' : txtInputCESSPrc.val();
            var InputSGSTAmt = txtInputSGSTAmt.val() == '' ? '0' : txtInputSGSTAmt.val();
            var InputCGSTAmt = txtInputCGSTAmt.val() == '' ? '0' : txtInputCGSTAmt.val();
            var InputCESSAmt = txtInputCESSAmt.val() == '' ? '0' : txtInputCESSAmt.val();
            var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
            var OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
            var OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();
            var addCessAmt = $('#<%=txtAddCessAmt.ClientID%>').val();



            /// work done by saravanan 15-02-2015

            var array, GSTPer, MRPGst, MarginMRPAmt;
            array = ddlOutputIGST.find("option:selected").text().trim().split("-");
            GSTPer = array[1];
            GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
            MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
            //MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);
            MRPGst = (parseFloat(MRP) * parseFloat(GSTPer) / 100);


            //var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost)) - (parseFloat(MRPGst)) / (parseFloat(GSTPer) + 100);
            var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
            MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
            txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

            //===================> Margin MRP Amt     
            //var MarginMRPAmt = parseFloat(MarginMRPPrc) - parseFloat(MRPVatLiablity1);
            //txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2))
            /// work done by saravanan 15-02-2015

            MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
            txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));

            //===================> Margin Based Set Basic Selling 
            var FixedMarginAmt = (GrossCost * FixedMargin) / 100
            BasicSelling = parseFloat(GrossCost) + parseFloat(FixedMarginAmt)
            txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2))

            //===================> Output SGST
            var OutputSGSTPrc = txtOutputSGSTPrc.val();
            if (OutputSGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputSGSTAmt = (BasicSelling * OutputSGSTPrc) / 100;
                if (OutputSGSTAmt == '') {
                    OutputSGSTAmt = '0';
                }
                txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));

                var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputCGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }

            //==================> Output CGST
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputCGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                if (OutputCGSTAmt == '') {
                    OutputCGSTAmt = '0';
                }
                txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));

                var OutputSGSTAmt = txtOutputSGSTAmt.val();
                if (OutputSGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }

            //==================> Output CESS
            var OutputCESSPrc = txtOutputCESSPrc.val();
            if (OutputCESSPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCESSAmt = (BasicSelling * OutputCESSPrc) / 100;
                if (OutputCESSAmt == '') {
                    OutputCESSAmt = '0';
                }
                txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));

                var OutputSGSTAmt = txtOutputSGSTAmt.val();
                var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputSGSTAmt != '' && OutputCGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt) + parseFloat(OutputCESSAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }


            TaxLiabilityCalc();
            //SellingPrice RoundOff based On ParamMaster
            txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));


            //===================> Margin Selling Price Prc          
            var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2);  //- parseFloat(OutputCGSTLiabilityAmt))
            var SPPrc1 = SPPrc / SellingPrice * 100
            SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
            txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

            //===================> Margin Selling Price Amt 
            var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
            txtSPAmt.val(parseFloat(SPAmt).toFixed(2));


            // Assign WholeSale Price
            txtWholePrice1Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
            txtWholePrice2Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
            txtWholePrice3Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))

            //==================> Tax Liability Calc

        }

        function fncAfterWSaleMarginSellingPrice(Mode, WholeSalePrice) { //Doubt



            //==================> Selling Price CGST
            var FixedMargin = WholeSalePrice;
      
           <%-- if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked')) { //surya--%>
                var GrossCost = txtGrossCost.val();
                if (GrossCost == '') {
                    return false;
                }
                var NetCost = txtNetCost.val();
                if (NetCost == '') {
                    return false;
                }
           <%-- if ($('#<%=chkAffectNetCost.ClientID %>').is(':notchecked')) {
                var GrossCost = $('#<%=hidGrossCostNew.ClientID %>').val();
                if (GrossCost == '') {
                    return false;
                }
                var NetCost = $('#<%=hidNetcostnew.ClientID %>').val();
                if (NetCost == '') {
                    return false;
                }
            }--%>
            //} 
           <%-- else {
                var GrossCost = $('#<%=hidGrossCostNew.ClientID %>').val();
                if (GrossCost == '') {
                    return false;
                }
                var NetCost = $('#<%=hidNetcostnew.ClientID %>').val();
                if (NetCost == '') {
                    return false;
                }--%>

           // }

            //Margin Based Set Basic Selling 
            if ($('#<%=hidNewCalculation.ClientID %>').val() == 'Y') {
                if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false) {
                    var GrossCost = $('#<%=hidGrossCostNew.ClientID %>').val();
                    if (GrossCost == '') {
                        return false;
                    }
                    var NetCost = $('#<%=hidNetcostnew.ClientID %>').val();
                    if (NetCost == '') {
                        return false;
                    }
                }
            }
            var BasicSelling = (GrossCost * FixedMargin) / 100

            BasicSelling = parseFloat(GrossCost) + parseFloat(BasicSelling)
            //txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2))
            var WholeSalePriceResult;

            //==================> Output SGST
            var OutputSGSTPrc = txtOutputSGSTPrc.val();
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputSGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputSGSTAmt = (BasicSelling * OutputSGSTPrc) / 100;
                if (OutputSGSTAmt == '') {
                    OutputSGSTAmt = '0';
                }
                //txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));

                //var OutputCGSTAmt = txtOutputCGSTAmt.val();
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                if (OutputCGSTAmt == '') {
                    OutputCGSTAmt = '0';
                }

                if (OutputCGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt);
                }
                else {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt)
                }
            }

            //==================> Output CGST
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputCGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                //if (OutputCGSTAmt == '') {
                //    OutputCGSTAmt = '0';
                //}
                //txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));

                //var OutputSGSTAmt = txtOutputSGSTAmt.val();
                if (OutputSGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt);
                }
                else {
                    WholeSalePrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt)
                }
            }

            //==================> Output CESS
            var OutputCESSPrc = txtOutputCESSPrc.val();
            if (OutputCESSPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCESSAmt = (BasicSelling * OutputCESSPrc) / 100;
                if (OutputCESSAmt == '') {
                    OutputCESSAmt = '0';
                }
                //txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));

                //var OutputSGSTAmt = txtOutputSGSTAmt.val();
                //var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputSGSTAmt != '' && OutputCGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt) + parseFloat(OutputCESSAmt);
                }
                else {
                    WholeSalePrice = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt)
                }
            }
            // surya WPrice calculation 02022022
           
            // Assign WholeSale Price
            if (Mode == '1') {
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B2' || $('#<%=hidPurchasedBy.ClientID %>').val() == "B4" || $('#<%=ddlSales.ClientID %>').val() == "Mrp") {
                    WholeSalePriceResult = parseFloat(txtMRP.val()) - (parseFloat(txtMRP.val() * parseFloat(txtMarginWPrice1.val())) / 100);
                }
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B3' || $('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                    if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == true) {
                        WholeSalePriceResult = parseFloat(txtNetCost.val()) + (parseFloat(txtNetCost.val() * parseFloat(txtMarginWPrice1.val()))) / 100;
                    }
                    else {
                        WholeSalePriceResult = parseFloat(txtBasicCost.val()) + parseFloat(txtOutputSGSTAmt.val()) + parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputCESSAmt.val()) + (parseFloat(txtBasicCost.val() * parseFloat(txtMarginWPrice1.val()))) / 100;
                    }
                }
                txtWholePrice1Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
            else if (Mode == '2') {
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B2' || $('#<%=hidPurchasedBy.ClientID %>').val() == "B4" || $('#<%=ddlSales.ClientID %>').val() == "Mrp") {
                    WholeSalePriceResult = parseFloat(txtMRP.val()) - (parseFloat(txtMRP.val() * parseFloat(txtMarginWPrice2.val())) / 100);
                }
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B3' || $('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                     if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == true) {
                         WholeSalePriceResult = parseFloat(txtNetCost.val()) + (parseFloat(txtNetCost.val() * parseFloat(txtMarginWPrice2.val()))) / 100;
                     }
                     else {
                         WholeSalePriceResult = parseFloat(txtBasicCost.val()) + parseFloat(txtOutputSGSTAmt.val()) + parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputCESSAmt.val()) + (parseFloat(txtBasicCost.val() * parseFloat(txtMarginWPrice2.val()))) / 100;
                     }
                 }
                txtWholePrice2Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
            else if (Mode == '3') {
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B2' || $('#<%=hidPurchasedBy.ClientID %>').val() == "B4" || $('#<%=ddlSales.ClientID %>').val()=="Mrp") {
                    WholeSalePriceResult = parseFloat(txtMRP.val()) - (parseFloat(txtMRP.val() * parseFloat(txtMarginWPrice3.val())) / 100);
                }
                if ($('#<%=hidPurchasedBy.ClientID %>').val() == 'B3' || $('#<%=ddlPurchase.ClientID %>').val() == "Mrp" && $('#<%=ddlSales.ClientID %>').val() == "Cost") {
                     if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == true) {
                         WholeSalePriceResult = parseFloat(txtNetCost.val()) + (parseFloat(txtNetCost.val() * parseFloat(txtMarginWPrice3.val()))) / 100;
                     }
                     else {
                         WholeSalePriceResult = parseFloat(txtBasicCost.val()) + parseFloat(txtOutputSGSTAmt.val()) + parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputCESSAmt.val()) + (parseFloat(txtBasicCost.val() * parseFloat(txtMarginWPrice3.val()))) / 100;
                     }
                 }
                txtWholePrice3Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
        }

        // This function commented by Mowzoon 09-03-2019
<%--        function TaxLiabilityCalc() {
            //==================> Vat Liability SGST
            var OutputSGST = txtOutputSGSTAmt.val();
            var InputSGST = txtInputSGSTAmt.val();
            if (OutputSGST != '' && InputSGST != '') {
                var VatLiabilitySGST = parseFloat(OutputSGST) - parseFloat(InputSGST);
                txtVatLiabilitySGSTAmt.val(parseFloat(VatLiabilitySGST).toFixed(2))
            }
            else {
                txtVatLiabilitySGSTAmt.val('0.00')
            }

            //==================> Vat Liability CGST
            var OutputCGST = txtOutputCGSTAmt.val();
            var InputCGST = txtInputCGSTAmt.val();
            if (OutputCGST != '' && InputCGST != '') {
                var VatLiabilityCGST = parseFloat(OutputCGST) - parseFloat(InputCGST);
                txtVatLiabilityCGSTAMt.val(parseFloat(VatLiabilityCGST).toFixed(2))
            }
            else {
                txtVatLiabilityCGSTAMt.val('0.00')
            }

            var SellingPrice = txtSellingPrice.val();
            if (SellingPrice != "") {

                var inputCessAmt = 0, outputCessAmt = 0, cessLiability = 0, liabilityPerc = 0, liabilityAmt = 0;
                //var VatLiabilitySGSTPrc = (txtVatLiabilitySGSTAmt.val() / SellingPrice) * 100
                //txtVatLiabilitySGSTPrc.val(parseFloat(VatLiabilitySGSTPrc).toFixed(2))
                //var VatLiabilityCGSTPrc = (txtVatLiabilityCGSTAMt.val() / SellingPrice) * 100
                //txtVatLiabilityCGSTPrc.val(parseFloat(VatLiabilityCGSTPrc).toFixed(2))

                inputCessAmt = $('#<%=txtInputCESSAmt.ClientID%>').val();
                outputCessAmt = $('#<%=txtOutputCESSAmt.ClientID%>').val();
                cessLiability = parseFloat(outputCessAmt) - parseFloat(inputCessAmt);

                /// work done by saravanan 16-02-2018
                liabilityAmt = parseFloat(txtVatLiabilitySGSTAmt.val()) + parseFloat(txtVatLiabilityCGSTAMt.val()) + parseFloat(cessLiability);
                $('#<%=txtGSTLiability.ClientID %>').val(liabilityAmt.toFixed(2));
                liabilityPerc = (parseFloat($('#<%=txtGSTLiability.ClientID %>').val()) / parseFloat(SellingPrice)) * 100
                $('#<%=txtGSTLiabilityPer.ClientID %>').val(liabilityPerc.toFixed(2));
            }
        }--%>

        //work done by saravanan 2017-11-27

        ///Selling Price Round Off
        function SellingPriceRoundOff(SellingPrice) {

            var sellingprice = 0, roundOff = 0;
            sellingprice = SellingPrice;

            try {
                if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '1') //1 Ruppees RoundOff
                {
                    NetSellingprice = (Math.round(SellingPrice)).toFixed(2); //1 Ruppees RoundOff
                }
                else if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '50') {
                    NetSellingprice = (Math.round(SellingPrice * 2) / 2).toFixed(2); //50 Paise RoundOff
                }
                else if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '25') {
                    NetSellingprice = (Math.round(SellingPrice * 4) / 4).toFixed(2); //25 Paise RoundOff
                }
                else {
                    NetSellingprice = SellingPrice;
                }

                roundOff = (parseFloat(NetSellingprice) - parseFloat(sellingprice)).toFixed(2);
                //if (parseFloat(roundOff) > 0) {
                //    roundOff = "+" + roundOff;
                //}
                $('#<%=txtRoundOff.ClientID %>').val(roundOff);
                return NetSellingprice;
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }


        }

    </script>
    <%--//surya--%>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'InventoryMaster');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "InventoryMaster";
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit")
                Mode = "InventoryMasterView";

            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>




    <%------------------------------------------- Selling Price Calc Dialog Open -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenInvPriceChange() {
            //Vijayv
            //fncdecimal(); 
            <%-- if (!($('#<%=chkBatch.ClientID %>').prop("checked")) && $('#<%=rbtCost.ClientID %>').is(':checked')) {
                $('#divGenerateMrp').show();
            }--%>
            if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "") {
                $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                txtCESSPrc.val('0');
                txtAddCessAmt.val('0');
            }
            else {
                if (HiddeLocation.val() != "HQ") {
                    $(<%=txtAddCessAmt.ClientID%>).attr("readonly", "readonly");
                    $(<%=txtCESSPrc.ClientID%>).attr("readonly", "readonly");
                    ddlInputIGST.prop("disabled", true);
                }
                if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit" && $('#<%=hidCostEdit.ClientID%>').val() == "CostEdit") {
                    $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                    $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                    ddlInputIGST.prop("disabled", false);
                }
                if (HiddeLocation.val() == "HQ") {
                    $(<%=txtAddCessAmt.ClientID%>).removeAttr("readonly");
                    $(<%=txtCESSPrc.ClientID%>).removeAttr("readonly");
                }
            }
            fncFocusOutForNumberTextControl();
            <%--if ($('#<%=txtItemCode.ClientID %>').val() == '') {
                ShowPopupMessageBox('Invalid Inventory Code');
                return false;
            }--%>

            if ($('#<%=hfBatch.ClientID %>').val() == 'True') {
                $("#dialog-confirm").dialog("open");  // Open Popup
                return;
            }

            if ($("#dialog-InvPrice").dialog('isOpen') === true) {
                return;
            }
            //fncinitializeinventoryCal();

            fncRemoveDisableAttForInputGST();
            //fncInvPriceDialogOpen();
            $("#dialog-InvPrice").dialog("open");  // Open Popup            
            fncOpenDialogWithDetail();



        }

        // Append Dailog 
        function fncAppendToFirstPriceDailog() {
            try {
                //fncinitializeinventoryCal();
                $("#dialog-InvPrice").parent().appendTo($("form:first")); // For After Postback 

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncRemoveDisableAttForInputGST() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() != "Liquor") {
                    setTimeout(function () {
                        $('#<%=ddlInputIGST.ClientID%>').removeAttr("disabled");
                        $('#<%=ddlInputIGST.ClientID%>').trigger("liszt:updated");
                        //  $('#<%=ddlInputIGST.ClientID%>').trigger("liszt:open");
                    }, 50);
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <script type="text/javascript">

        /// Load Initial Data for Item History  
        function fncInitialLoad() {
            try {
                fncOpenDialogWithDetail();
                fncBindBreakPriceQty();
                fncBindBarcode();
                fncDropdownEnterKeyVal();
                fncBindMultipleUOM();
                fncOrderByChanged();
                fncBindTransferDetail();
                fncBindPriceChangeLog();
                
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }


        function fncOpenDialogWithDetail() {
            //alert('fncOpenDialogWithDetail');
            
            //surya20102021 for dropdownSelection
            
            if ($('#<%=hidMrpFocus.ClientID %>').val() == "B1") {

                $("#<%=ddlPurchase.ClientID %> option[value='Cost']").prop('selected', true);
                $('#<%=ddlPurchase.ClientID %>').trigger("liszt:updated");
                $("#<%=ddlSales.ClientID %> option[value='Cost']").prop('selected', true);
                $('#<%=ddlSales.ClientID %>').trigger("liszt:updated");
            }
            else if ($('#<%=hidMrpFocus.ClientID %>').val() == "B2") {
                $("#<%=ddlPurchase.ClientID %> option[value='Cost']").prop('selected', true);
                $('#<%=ddlPurchase.ClientID %>').trigger("liszt:updated");
                $("#<%=ddlSales.ClientID %> option[value='Mrp']").prop('selected', true);
                $('#<%=ddlSales.ClientID %>').trigger("liszt:updated");
            }
            else if ($('#<%=hidMrpFocus.ClientID %>').val() == "B3") {
                $("#<%=ddlPurchase.ClientID %> option[value='Mrp']").prop('selected', true);
                $('#<%=ddlPurchase.ClientID %>').trigger("liszt:updated");
                $("#<%=ddlSales.ClientID %> option[value='Cost']").prop('selected', true);
                $('#<%=ddlSales.ClientID %>').trigger("liszt:updated");
            }
            else if ($('#<%=hidMrpFocus.ClientID %>').val() == "B4") {
                $("#<%=ddlPurchase.ClientID %> option[value='Mrp']").prop('selected', true);
                $('#<%=ddlPurchase.ClientID %>').trigger("liszt:updated");
                $("#<%=ddlSales.ClientID %> option[value='Mrp']").prop('selected', true);
                $('#<%=ddlSales.ClientID %>').trigger("liszt:updated");
            }
            var obj = {};
            

            if ($('#<%=rbtCost.ClientID %>').is(':checked')) {

                $('#<%=rbtCostPopup.ClientID %>').prop('checked', true);  // unchecked     
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', false);
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked
                $('#<%=rbtPMD4opup.ClientID %>').prop('checked', false);
                $('#<%=rbtMD3Popup.ClientID %>').prop('checked', false);
                $(txtMRPMarkDown).attr("disabled", "disabled");
                $(txtMRPMarkDown).val("0.00"); // work modified by saravanan 01-12-2017
                $(txtDiscountPrc1).removeAttr("disabled");
                $(txtDiscountPrc2).removeAttr("disabled");
                $(txtDiscountPrc3).removeAttr("disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");

                $("#markup").css("background", "#4169e1");
                $("#markup").css("color", "white");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

                $("#PMD").css("background", "");
                $("#PMD").css("color", "");

            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtBasicCost).attr("disabled", "disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked     
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', false);
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', true);  // checked
                $('#<%=rbtPMD4opup.ClientID %>').prop('checked', false);
                $('#<%=rbtMD3Popup.ClientID %>').prop('checked', false);

                $("#markdown").css("background", "#4169e1");
                $("#markdown").css("color", "white");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#PMD").css("background", "");
                $("#PMD").css("color", "");
                $('#<%=divAddDis.ClientID %>').css("display", "block");
                $('#<%=divDis.ClientID %>').css("display", "block");
                $('#<%=hdfMarkDown.ClientID %>').val('Y');

            }
            else if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {

                $('#<%=hdfMarkDown.ClientID %>').val('N');

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");
                $('#<%=divAddDis.ClientID %>').css("display", "block");
                $('#<%=divDis.ClientID %>').css("display", "block");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked                      
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', true);
                $('#<%=rbtPMD4opup.ClientID %>').prop('checked', false);
                $('#<%=rbtMD3Popup.ClientID %>').prop('checked', false);


                $("#PMD").css("background", "#4169e1");
                $("#PMD").css("color", "white");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

            }
            else if ($('#<%=rbnMRP3.ClientID %>').is(':checked')) {

                $('#<%=hdfMarkDown.ClientID %>').val('N');

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");
                $('#<%=divAddDis.ClientID %>').css("display", "block");
                $('#<%=divDis.ClientID %>').css("display", "block");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked                      
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked
                $('#<%=rbtMD3Popup.ClientID %>').prop('checked', true);
                $('#<%=rbtPMD4opup.ClientID %>').prop('checked', false);


                $("#MD3").css("background", "#4169e1");
                $("#MD3").css("color", "white");


                $("#PMD").css("background", "");
                $("#PMD").css("color", "");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

            }
            else {

                $('#<%=hdfMarkDown.ClientID %>').val('N');

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");
                $('#<%=divAddDis.ClientID %>').css("display", "block");
                $('#<%=divDis.ClientID %>').css("display", "block");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked                      
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked
                $('#<%=rbtPMD4opup.ClientID %>').prop('checked', true);
                $('#<%=rbtMD3Popup.ClientID %>').prop('checked', false);


                $("#MD4").css("background", "#4169e1");
                $("#MD4").css("color", "white");

                $("#MD3").css("background", "");
                $("#MD3").css("color", "");

                $("#PMD").css("background", "");
                $("#PMD").css("color", "");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

            }

            if ($('#<%=txtGSTGroup.ClientID %>').val() != '') {
                txtHSNCode = $('#<%=txtHSNCode.ClientID %>');
                txtHSNCode.val($('#<%=txtGSTGroup.ClientID %>').val());
            }

            obj.InvCode = $('#<%=txtItemCode.ClientID %>').val();
            obj.invStatus = $('#<%=hidinventoryStatus.ClientID %>').val();
            obj.priceEnt = $('#<%=hidPriceEntered.ClientID %>').val();
            if (clearpricechange == "0") {
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetInvPriceDetail") %>',
                    //data: "{'InvCode':'" + $('#<%=txtItemCode.ClientID %>').val() + "'}",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if ($('#<%=hidPriceEntered.ClientID %>').val() == "1")
                            return;
                        $('#<%=txtItemCodePopup.ClientID %>').val($('#<%=txtItemCode.ClientID %>').val());
                        $('#<%=txtItemNamePopup.ClientID %>').val($('#<%=txtItemName.ClientID %>').val());

                        //setTimeout(function(){
                        $('#<%=ddlInputSGST.ClientID %>').val(msg.d[0]);
                        $('#<%=ddlInputSGST.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlInputCGST.ClientID %>').val(msg.d[1]);
                        $('#<%=ddlInputCGST.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlInputIGST.ClientID %>').val(msg.d[2]);
                        $('#<%=ddlInputIGST.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlOutputSGST.ClientID %>').val(msg.d[3]);
                        $('#<%=ddlOutputSGST.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlOutputCGST.ClientID %>').val(msg.d[4]);
                        $('#<%=ddlOutputCGST.ClientID %>').trigger("liszt:updated");
                        $('#<%=ddlOutputIGST.ClientID %>').val(msg.d[5]);
                        $('#<%=ddlOutputIGST.ClientID %>').trigger("liszt:updated");
                        // }, 50);

                        if (msg.d[6] == 'C') {
                            $('#<%=rbtCostPopup.ClientID %>').prop('checked', $(this).prop("checked"));
                        }
                        else {
                            $('#<%=rbtMrpPopup.ClientID %>').prop('checked', $(this).prop("checked"));
                        }
                        $('#<%=txtMRP.ClientID %>').val(msg.d[7]);
                        $('#<%=txtBasicCost.ClientID %>').val(msg.d[8]);
                        $('#<%=txtDiscountPrc1.ClientID %>').val(msg.d[9]);
                        $('#<%=txtDiscountPrc2.ClientID %>').val(msg.d[10]);
                        $('#<%=txtDiscountPrc3.ClientID %>').val(msg.d[11]);
                        $('#<%=txtDiscountAmt.ClientID %>').val(msg.d[12]);
                        $('#<%=txtGrossCost.ClientID %>').val(msg.d[13]);
                        $('#<%=txtInputSGSTPrc.ClientID %>').val(msg.d[14]);
                        $('#<%=txtInputSGSTAmt.ClientID %>').val(msg.d[15]);
                        $('#<%=txtInputCGSTPrc.ClientID %>').val(msg.d[16]);
                        $('#<%=txtInputCGSTAmt.ClientID %>').val(msg.d[17]);
                        $('#<%=txtNetCost.ClientID %>').val(msg.d[18]);
                        $('#<%=txtMarginSP.ClientID %>').val(msg.d[19]);
                        $('#<%=txtSPAmt.ClientID %>').val(msg.d[20]);
                        $('#<%=txtFixedMargin.ClientID %>').val(msg.d[21]);
                        $('#<%=txtBasicSelling.ClientID %>').val(msg.d[22]);
                        $('#<%=txtOutputSGSTPrc.ClientID %>').val(msg.d[23]);
                        $('#<%=txtOutputSGSTAmt.ClientID %>').val(msg.d[24]);
                        $('#<%=txtOutputCGSTPrc.ClientID %>').val(msg.d[25]);
                        $('#<%=txtOutputCGSTAmt.ClientID %>').val(msg.d[26]);
                        $('#<%=txtSellingPrice.ClientID %>').val(msg.d[27]);
                        $('#<%=txtVatLiabilitySGSTPrc.ClientID %>').val(msg.d[28]);
                        $('#<%=txtVatLiabilitySGSTAmt.ClientID %>').val(msg.d[29]);
                        $('#<%=txtVatLiabilityCGSTPrc.ClientID %>').val(msg.d[30]);
                        $('#<%=txtVatLiabilityCGSTAMt.ClientID %>').val(msg.d[31]);
                        $('#<%=txtWholePrice1Popup.ClientID %>').val(msg.d[32]);
                        $('#<%=txtWholePrice2Popup.ClientID %>').val(msg.d[33]);
                        $('#<%=txtWholePrice3Popup.ClientID %>').val(msg.d[34]);
                        $('#<%=txtMarginWPrice1.ClientID %>').val(msg.d[35]);
                        $('#<%=txtMarginWPrice2.ClientID %>').val(msg.d[36]);
                        $('#<%=txtMarginWPrice3.ClientID %>').val(msg.d[37]);
                        $('#<%=txtCESSPrc.ClientID %>').val(msg.d[40]);
                        $('#<%=txtInputCESSPrc.ClientID %>').val(msg.d[40]);
                        $('#<%=txtInputCESSAmt.ClientID %>').val(msg.d[41]);
                        $('#<%=txtOutputCESSPrc.ClientID %>').val(msg.d[44]);
                        $('#<%=txtOutputCESSAmt.ClientID %>').val(msg.d[45]);
                        $('#<%=txtAddCessAmt.ClientID %>').val(msg.d[47]);
                        if (msg.d[48] == '1') {
                            $('#<%=chkAffectNetCost.ClientID %>').prop('checked', true);
                        }
                        else {
                            $('#<%=chkAffectNetCost.ClientID %>').prop('checked', false);
                          } //surya 20211021

                        /// work done by saravanan 15-02-2018
                        $('#<%=txtGSTLiability.ClientID %>').val(parseFloat(msg.d[29]) + parseFloat(msg.d[31]));
                        $('#<%=txtGSTLiabilityPer.ClientID %>').val(parseFloat(msg.d[28]) + parseFloat(msg.d[30]));

                        fncDisableEXGST();

                        //===================> Margin MRP Amt  
                        var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

                        MRP = $('#<%=txtMRP.ClientID %>').val();
                        GrossCost = $('#<%=txtGrossCost.ClientID %>').val();
                        array = $('#<%=ddlOutputIGST.ClientID %>').find("option:selected").text().trim().split("-");
                        GSTPer = array[1];
                        GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
                        MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID %>').val());
                        MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);

                        var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(MRP) * 100;
                        MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
                        $('#<%=txtMarginMRP.ClientID %>').val(parseFloat(MarginMRPPrc).toFixed(2))

                        /// work done by saravanan 15-02-2015

                        MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
                        $('#<%=txtMRPAmt.ClientID %>').val(parseFloat(MarginMRPAmt).toFixed(2));

                        MarginCalculationNew();


                        fncMDownOnMRP();
                          //surya16102021

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            else {
                fncClearPriceChange();
            }
            $('#<%=txtSellingPrice.ClientID %>').change(function () {
                fncMDownOnMRP(); //surya16102021
            });
            $('#<%=txtBasicCost.ClientID %>').change(function () {
                fncMDownOnMRP();  //surya16102021
            });
            $('#<%=txtCESSPrc.ClientID %>').change(function () {
                fncMDownOnMRP();  //surya16102021
            });
            
            $('#<%=txtAddCessAmt.ClientID %>').change(function () {
                fncMDownOnMRP();  //surya16102021
            });
            $('#<%=txtAddCessAmt.ClientID %>').change(function () {
                fncMDownOnMRP();  //surya16102021
            });
            $('#<%=txtFixedMargin.ClientID %>').change(function () {
                fncMDownOnMRP();
            });
            
            ///Disable SellingPrice 
            if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                $('#<%=txtSellingPrice.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice1Popup.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice2Popup.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice3Popup.ClientID %>').attr("disabled", "disabled");

            }
            else {
                $('#<%=txtSellingPrice.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice1Popup.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice2Popup.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice3Popup.ClientID %>').removeAttr("disabled");
            }

            return false;
        }

        function fncDisableEXGST() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() == "Liquor") {

                    ddlInputIGST.append($("<option></option>").val('EIGST').html('EIGST 0 % - 0.00'));
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");

                    $('#<%=ddlInputIGST.ClientID%>').val("EIGST");
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                    fncInputGSTValueChange();

                }
                else {
                    $("select[id$=ddlInputIGST] option[value='EIGST']").remove();
                    $('#<%=ddlInputIGST.ClientID%>').removeAttr("disabled");
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //function fncInputGSTOpen

        /// Input GST Value Changed
        function fncInputGSTValueChange() {
            try {

                var InputIGSTCode = ddlInputIGST.val();
                var GSTObj, GSTType;


                ddlInputIGST.val(InputIGSTCode);
                <%--$('#<%=ddlInputIGST.ClientID%>').attr("disabled", "disabled");
                ddlInputIGST.trigger("liszt:updated");--%>
                //=============================>  Output IGST Change
                ddlOutputIGST.val(InputIGSTCode);
                ddlOutputIGST.trigger("liszt:updated");

                if (InputIGSTCode == "EIGST")
                    GSTType = "EIGST";
                else
                    GSTType = "IGST";

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                    data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + GSTType + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        //=============================>  Input SGST Change
                        GSTObj = jQuery.parseJSON(msg.d);

                        if (GSTObj.length > 0) {
                            for (var i = 0; i < GSTObj.length; i++) {
                                if (GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                    //=============================>  Input SGST Change
                                    ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputSGST.trigger("liszt:updated");

                                    var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                    var InputSGSTPrc = array[1];
                                    if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                        txtInputSGSTPrc.val(InputSGSTPrc);
                                    }

                                    //=============================>  Output SGST Change
                                    ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputSGST.trigger("liszt:updated");

                                    var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                    var OutputSGSTPrc = array[1];
                                    if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                        txtOutputSGSTPrc.val(OutputSGSTPrc);
                                        //fncCalcSelingPice();
                                    }

                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                    //=============================>  Input CGST Change
                                    ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputCGST.trigger("liszt:updated");

                                    var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtInputCGSTPrc.val(InputCGSTPrc);
                                    }

                                    //=============================>  Output CGST Change
                                    ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputCGST.trigger("liszt:updated");

                                    var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                    var OutputCGSTPrc = array[1];
                                    if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                        txtOutputCGSTPrc.val(OutputCGSTPrc);
                                    }
                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("S") != -1 || GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                    //=============================>  Input SGST Change
                                    ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputSGST.trigger("liszt:updated");

                                    var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                    var InputSGSTPrc = array[1];
                                    if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                        txtInputSGSTPrc.val(InputSGSTPrc);
                                    }

                                    //=============================>  Output SGST Change
                                    ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputSGST.trigger("liszt:updated");

                                    var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                    var OutputSGSTPrc = array[1];
                                    if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                        txtOutputSGSTPrc.val(OutputSGSTPrc);
                                        //fncCalcSelingPice();
                                    }

                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 || GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                    //=============================>  Input CGST Change
                                    ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputCGST.trigger("liszt:updated");

                                    var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtInputCGSTPrc.val(InputCGSTPrc);
                                    }

                                    //=============================>  Output CGST Change
                                    ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputCGST.trigger("liszt:updated");

                                    var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                    var OutputCGSTPrc = array[1];
                                    if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                        txtOutputCGSTPrc.val(OutputCGSTPrc);
                                    }
                                }//


                            }

                            //fncCalcSelingPice();
                        }//End

                        /// work done by saravanan 01-12-2017
                        if (ddlInputIGST.val() == "I0000" || ddlInputIGST.val() == "-- Select --") {
                            $('#<%=txtHSNCode.ClientID %>').select().focus();
                        } else {
                            $('#<%=txtCESSPrc.ClientID %>').select();

                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err);
            }
        }

    </script>
    <%--------------------------------------- Selling Price Save --------------------------------------%>
    <script type="text/javascript">
        function fncSaveInvPriceChange() {
            //============================> Validations
            var textile = "0";
            if ($('#<%=hidTextile.ClientID %>').val() == "Y")
                textile = "1";
            if ($('#<%=hidZero.ClientID %>').val() == "N") {
                if (textile == "0") {
                    if (txtMRP.val() == '0') {
                        popUpObjectForSetFocusandOpen = txtMRP;
                        ShowPopupMessageBoxandFocustoObject('MRP Should Not be Empty!!!');
                        return false;
                    }
                    if (HiddeLocation.val() != "HQ" && $('#<%=hidCostEdit.ClientID %>').val() == "CostEdit") {
                        if (ddlInputSGST.val() == '') {
                            popUpObjectForSetFocusandOpen = ddlInputSGST;
                            ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input SGST TaxCode');
                            return false;
                        }
                        if (ddlInputCGST.val() == '') {
                            popUpObjectForSetFocusandOpen = ddlInputCGST;
                            ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input CGST TaxCode');
                            return false;
                        }
                    }
                    if ((ddlInputSGST.val() == '') && (HiddeLocation.val() == "HQ")) {
                        popUpObjectForSetFocusandOpen = ddlInputSGST;
                        ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input SGST TaxCode');
                        return false;
                    }
                    if ((ddlInputCGST.val() == '') && (HiddeLocation.val() == "HQ")) {
                        popUpObjectForSetFocusandOpen = ddlInputCGST;
                        ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input CGST TaxCode');
                        return false;
                    }
                    if (txtSellingPrice.val() == '0') {
                        popUpObjectForSetFocusandOpen = txtSellingPrice;
                        ShowPopupMessageBoxandFocustoObject('Invalid Selling Price');
                        return false;
                    }
                    if (parseFloat(txtBasicCost.val()) == 0) {
                        popUpObjectForSetFocusandOpen = txtBasicCost;
                        ShowPopupMessageBoxandFocustoObject('Invalid Basic Cost');
                        return false;
                    }
                    if (parseFloat(txtMRP.val()) < parseFloat(txtSellingPrice.val())) {
                        popUpObjectForSetFocusandOpen = txtSellingPrice;
                        ShowPopupMessageBoxandFocustoObject('Selling Price should be less than  MRP !!!');
                        return false;
                    }

                    if (parseFloat(txtSellingPrice.val()) < parseFloat(txtNetCost.val())) {
                        popUpObjectForSetFocusandOpen = txtSellingPrice;
                        ShowPopupMessageBoxandFocustoObject('Selling Price Lower than NetCost!!!');
                        return false;
                    }
                    if (parseFloat(txtWholePrice1Popup.val()) < parseFloat(txtNetCost.val())) {
                        popUpObjectForSetFocusandOpen = txtWholePrice1Popup;
                        ShowPopupMessageBoxandFocustoObject('Wprice1 greater than or equal to to Netcost and Less than or equal to MRP');
                        return false;
                    }
                    if (parseFloat(txtWholePrice2Popup.val()) < parseFloat(txtNetCost.val())) {
                        popUpObjectForSetFocusandOpen = txtWholePrice2Popup;
                        ShowPopupMessageBoxandFocustoObject('Wprice2 greater than or equal to to Netcost and Less than or equal to MRP');
                        return false;
                    }
                    if (parseFloat(txtWholePrice3Popup.val()) < parseFloat(txtNetCost.val())) {
                        popUpObjectForSetFocusandOpen = txtWholePrice3Popup;
                        ShowPopupMessageBoxandFocustoObject('Wprice3 greater than or equal to to Netcost and Less than or equal to MRP');
                        return false;
                    }

                    if ($('#<%=hidWpriceGreaterSP.ClientID %>').val() == "N") {
                        if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice1Popup.val())) {
                            popUpObjectForSetFocusandOpen = txtWholePrice1Popup;
                            ShowPopupMessageBoxandFocustoObject('Wprice1 greater than or equal to to Netcost and Less than or equal to MRP');
                            return false;
                        }
                        if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice2Popup.val())) {
                            popUpObjectForSetFocusandOpen = txtWholePrice2Popup;
                            ShowPopupMessageBoxandFocustoObject('Wprice2 greater than or equal to to Netcost and Less than or equal to MRP');
                            return false;
                        }
                        if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice3Popup.val())) {
                            popUpObjectForSetFocusandOpen = txtWholePrice3Popup;
                            ShowPopupMessageBoxandFocustoObject('Wprice3 greater than or equal to to Netcost and Less than or equal to MRP');
                            return false;
                        }
                    }
                }
            }

            var obj = {};
            obj.InventoryCode = $('#<%=txtItemCodePopup.ClientID %>').val();
            if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                obj.PricingType = 'SELLINGPRICE';
                obj.PurchaseBy = 'CS';
                obj.MarginFixType = 'C';
            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                obj.PricingType = 'MRP';
                obj.PurchaseBy = 'M1';
                obj.MarginFixType = 'M';
            }
            else if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                obj.PricingType = 'SELLINGPRICE';
                obj.PurchaseBy = 'M2';
                obj.MarginFixType = 'C';
            }
            else if ($('#<%=rbnMRP3.ClientID %>').is(':checked')) {
                obj.PricingType = 'MRP';
                obj.PurchaseBy = 'M3';
                obj.MarginFixType = 'M';
            }
            else if ($('#<%=rbnMRP4.ClientID %>').is(':checked')) {
                obj.PricingType = 'SELLINGPRICE';
                obj.PurchaseBy = 'M4';
                obj.MarginFixType = 'C';
            }
            if ($('#<%=hidNewCalculation.ClientID %>').val() == 'Y' && $('#<%=rbtCost.ClientID %>').is(':checked')) {
                if ($('#<%=ddlPurchase.ClientID %>').val() == 'Cost' && $('#<%=ddlSales.ClientID %>').val() == 'Cost') {
                    obj.PricingType = 'SELLINGPRICE';
                    obj.PurchaseBy = 'B1';
                    obj.MarginFixType = 'C';
                }
               else if ($('#<%=ddlPurchase.ClientID %>').val() == 'Cost' && $('#<%=ddlSales.ClientID %>').val() == 'Mrp') {
                    obj.PricingType = 'SELLINGPRICE';
                    obj.PurchaseBy = 'B2';
                    obj.MarginFixType = 'C';
                }
                else if ($('#<%=ddlPurchase.ClientID %>').val() == 'Mrp' && $('#<%=ddlSales.ClientID %>').val() == 'Cost') {
                    obj.PricingType = 'SELLINGPRICE';
                    obj.PurchaseBy = 'B3';
                    obj.MarginFixType = 'C';
                }
                else if ($('#<%=ddlPurchase.ClientID %>').val() == 'Mrp' && $('#<%=ddlSales.ClientID %>').val() == 'Mrp') {
                    obj.PricingType = 'SELLINGPRICE';
                    obj.PurchaseBy = 'B4';
                    obj.MarginFixType = 'C';
                }
            }

            obj.HSNCode = txtHSNCode.val();

            obj.MRP = txtMRP.val();
            obj.BasicCost = txtBasicCost.val();
            obj.DiscPer1 = txtDiscountPrc1.val();
            obj.DiscPrc2 = txtDiscountPrc2.val();
            obj.DiscPrc3 = txtDiscountPrc3.val();
            obj.DiscAmt = txtDiscountAmt.val();
            obj.GrossCost = txtGrossCost.val();
            obj.NetCost = txtNetCost.val();
            obj.MarginPer = '0';
            //obj.MarginAmt = txtFixedMargin.val();
            obj.MarginAmt = '0';
            obj.BasicSelling = txtBasicSelling.val();
            obj.NetSellingPrice = txtSellingPrice.val();
            obj.MFPer = txtMarginSP.val();
            obj.MFAmt = txtSPAmt.val();
            obj.EarnedMargin = txtFixedMargin.val();
            obj.WPrice1 = txtWholePrice1Popup.val() == null ? "0.00" : txtWholePrice1Popup.val();
            obj.WPRice2 = txtWholePrice2Popup.val() == null ? "0.00" : txtWholePrice2Popup.val();
            obj.WPrice3 = txtWholePrice3Popup.val() == null ? "0.00" : txtWholePrice3Popup.val();
            obj.MPWPrice1 = txtMarginWPrice1.val() == null ? "0.00" : txtMarginWPrice1.val();
            obj.MPWPrice2 = txtMarginWPrice2.val() == null ? "0.00" : txtMarginWPrice2.val();
            obj.MPWPrice3 = txtMarginWPrice3.val() == null ? "0.00" : txtMarginWPrice3.val();
            if ($('#<%=hidNewCalculation.ClientID %>').val() =="N")
                obj.MRPPurchase = txtMRPMarkDown.val() == "" ? "0.00" : txtMRPMarkDown.val();
            else
                obj.MRPPurchase = txtBasicMargin.val() == "" ? "0.00" : txtBasicMargin.val();

            obj.ITaxCode1 = ddlInputSGST.val();
            obj.ITaxCode2 = ddlInputCGST.val();
            obj.ITaxCode3 = ddlInputIGST.val();
            obj.ITaxCode4 = '';
            obj.ITaxPer1 = txtInputSGSTPrc.val();
            obj.ITaxPer2 = txtInputCGSTPrc.val();
            obj.ITaxPer3 = parseFloat(txtInputSGSTPrc.val()) + parseFloat(txtInputCGSTPrc.val());
            obj.ITaxPer4 = txtInputCESSPrc.val();
            obj.ITaxAmt1 = txtInputSGSTAmt.val();
            obj.ITaxAmt2 = txtInputSGSTAmt.val();
            obj.ITaxAmt3 = parseFloat(txtInputSGSTAmt.val()) + parseFloat(txtInputSGSTAmt.val());
            obj.ITaxAmt4 = txtInputCESSAmt.val();
            obj.OTaxCode1 = ddlOutputSGST.val();
            obj.OTaxCode2 = ddlOutputCGST.val();
            obj.OTaxCode3 = ddlOutputIGST.val();
            obj.OTaxCode4 = '';
            obj.OTaxPer1 = txtOutputSGSTPrc.val();
            obj.OTaxPer2 = txtOutputCGSTPrc.val();
            obj.OTaxPer3 = parseFloat(txtOutputSGSTPrc.val()) + parseFloat(txtOutputCGSTPrc.val());
            obj.OTaxPer4 = txtOutputCESSPrc.val();
            obj.OTaxAmt1 = txtOutputCGSTAmt.val();
            obj.OTaxAmt2 = txtOutputSGSTAmt.val();
            obj.OTaxAmt3 = parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputSGSTAmt.val());
            obj.OTaxAmt4 = txtOutputCESSAmt.val();
            obj.VatliabilityPer1 = txtVatLiabilitySGSTPrc.val();
            obj.VatliabilityPer2 = txtVatLiabilityCGSTPrc.val();
            obj.VatliabilityPer3 = '0';
            obj.VatliabilityAmt1 = txtVatLiabilitySGSTAmt.val();
            obj.VatliabilityAmt2 = txtVatLiabilityCGSTAMt.val();
            obj.VatliabilityAmt3 = '0';
            obj.ITaxAmt5 = $('#<%=txtAddCessAmt.ClientID %>').val();
            obj.OTaxAmt5 = $('#<%=txtAddCessAmt.ClientID %>').val();
            if (obj.WPrice1 == "0.00")
                obj.WPrice1 = parseFloat(txtSellingPrice.val()).toFixed(2);
            if (obj.WPRice2 == "0.00")
                obj.WPRice2 = parseFloat(txtSellingPrice.val()).toFixed(2);
            if (obj.WPrice3 == "0.00")
                obj.WPrice3 = parseFloat(txtSellingPrice.val()).toFixed(2);
            //obj.WPrice1 = txtWholePrice1Popup.val() == "0.00" ? "0.00" : txtSellingPrice.val();
            //obj.WPRice2 = txtWholePrice2Popup.val() == "0.00" ? "0.00" : txtSellingPrice.val();
            //obj.WPrice3 = txtWholePrice3Popup.val() == "0.00" ? "0.00" : txtSellingPrice.val();

            $('#<%=hidInvPriceDetail.ClientID %>').val(JSON.stringify(obj));
            //$("#dialog-InvPrice").dialog("destroy");
            $("#dialog-InvPrice").dialog("close");

            // Set Value                        
            $('#<%=txtFetchSellingPrice.ClientID %>').val(parseFloat(txtSellingPrice.val()).toFixed(2));
            //Prabu start
            if ($('#<%=rbtCostPopup.ClientID %>').is(':checked')) {
                $('#<%=rbtMRP.ClientID %>').attr("disabled", "disabled");
                $('#<%=rbnMRP2.ClientID %>').attr("disabled", "disabled");
            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                $('#<%=rbtCost.ClientID %>').attr("disabled", "disabled");
                $('#<%=rbnMRP2.ClientID %>').attr("disabled", "disabled");
            }
            else if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                $('#<%=rbtMRP.ClientID %>').attr("disabled", "disabled");
                $('#<%=rbtCost.ClientID %>').attr("disabled", "disabled");
            }
            //Prabu End  Pricing type Disable

            $('#<%=lblMRP.ClientID %>').text(parseFloat(txtMRP.val()).toFixed(2));
            $('#<%=hdfMRP.ClientID %>').val(parseFloat(txtMRP.val()).toFixed(2));
            $('#<%=lblNetCost.ClientID %>').text(parseFloat(txtNetCost.val()).toFixed(2));
            $('#<%=hdfNetCost.ClientID %>').val(parseFloat(txtNetCost.val()).toFixed(2));
            <%-- $('#<%=txtWPrice1.ClientID %>').val(parseFloat(txtWholePrice1Popup.val() == null ? '0.00' : txtWholePrice1Popup.val()).toFixed(2));
            $('#<%=txtWPrice2.ClientID %>').val(parseFloat(txtWholePrice2Popup.val() == null ? '0.00' : txtWholePrice2Popup.val()).toFixed(2));
            $('#<%=txtWPrice3.ClientID %>').val(parseFloat(txtWholePrice3Popup.val() == null ? '0.00' : txtWholePrice3Popup.val()).toFixed(2));--%>


            $('#<%=txtWPrice1.ClientID %>').val(obj.WPrice1);
            $('#<%=txtWPrice2.ClientID %>').val(obj.WPRice2);
            $('#<%=txtWPrice3.ClientID %>').val(obj.WPrice3);


            $('#<%=txtGSTGroup.ClientID %>').val(txtHSNCode.val());
            $('#<%=hdfBasicCost.ClientID %>').val($('#<%=txtBasicCost.ClientID %>').val());
            console.log($('#<%=txtWPrice1.ClientID %>').val());
            console.log($('#<%=txtWPrice2.ClientID %>').val());
            console.log($('#<%=txtWPrice3.ClientID %>').val());
            $('#<%=lnkSave.ClientID %>').focus();
        }

        //=====================> Clear Price Change Field
        var clearpricechange = 0;
        function fncClearPriceChange() {
            clearpricechange = 0;
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                clearpricechange = 1;
            }
            //if (parseFloat($('#<%=txtMRP.ClientID %>').val()) > 0)
            $('#<%=txtMRP.ClientID %>').removeAttr("disabled");
            //if (parseFloat($('#<%=txtBasicCost.ClientID %>').val()) > 0)
            if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                $('#<%=txtBasicCost.ClientID %>').removeAttr("disabled");
            }

            $("#dialog-InvPrice").find("input[type=text]").val("0.00");
            $("#dialog-InvPrice").find("select").val(0);
            $("#dialog-InvPrice").find("select").trigger("liszt:updated");
            $('#<%=txtItemCodePopup.ClientID %>').val($('#<%=txtItemCode.ClientID %>').val());
            $('#<%=txtItemNamePopup.ClientID %>').val($('#<%=txtItemName.ClientID %>').val());
            $('#<%=hidPriceEntered.ClientID %>').val("0"); // work done by saravanan 01-12-2017
            $('#<%=txtHSNCode.ClientID %>').val('');
            $('#<%=txtBasicMargin.ClientID %>').val('0');
        }

    </script>
    <%------------------------------------------- Batch Grid Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenBatchDetail() {
            //sfncDisableEXGST();
            //fncAppendToFirstPriceDailog();
            $("#dialog-batch").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 550,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- PR Req Grid Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenPRRequest() {
            //sfncDisableEXGST();
            //fncAppendToFirstPriceDailog();
            $("#dialog-PRReq").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Image Preview -------------------------------------------%>
    <script type="text/javascript">
        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <%------------------------------------------- Validation -------------------------------------------%>
    <script type="text/javascript">
        function Validate() {

            var obj = {}, status = false;
            try {

                if (($('#<%=txtUOMPurchase.ClientID %>').val() == "KG" || $('#<%=txtSalesUOM.ClientID %>').val() == "KG") && $('#<%=chkBatch.ClientID %>').prop("checked") && $("#<%=hidBulkBatchDistribution.ClientID %>").val().toUpperCase() == "N") {
                    ShowPopupMessageBox('Item has not saved,KG items only belongs to Non Batch items');
                    return false;
                }

                obj.Categorycode = $('#<%=txtInvcategory.ClientID %>').val();
                obj.Departmentcode = $('#<%=txtDepartment.ClientID %>').val();
                obj.Brandcode = $('#<%=txtBrand.ClientID %>').val();
                obj.Floor = $('#<%=txtFloor.ClientID %>').val();
                obj.Vendorcode = $('#<%=hidVendorcode.ClientID %>').val();
                obj.warehouse = $('#<%=txtWareHouse.ClientID %>').val();
                obj.manufacture = $('#<%=txtManafacture.ClientID %>').val();
                obj.Merchandise = $('#<%=txtMerchandise.ClientID %>').val();
                obj.Itemtype = $('#<%=txtItemType.ClientID %>').val();
                obj.style = $('#<%=txtStyle.ClientID %>').val();
                obj.size = $('#<%=txtSize.ClientID %>').val();
                obj.Origin = $('#<%=txtOrigin.ClientID %>').val();
                obj.Subcategory = $('#<%=txtSubCategory.ClientID %>').val();
                obj.Bin = $('#<%=txtBin.ClientID %>').val();
                obj.package = $('#<%=txtPackage.ClientID %>').val();
                obj.companycode = $('#<%=txtCCode.ClientID %>').val();
                obj.Class = $('#<%=txtClass.ClientID %>').val();
                obj.Subclass = $('#<%=txtSubClass.ClientID %>').val();
                obj.Section = $('#<%=txtSection.ClientID %>').val();
                obj.Shelf = $('#<%=txtShelf.ClientID %>').val();

                if (fncCheckmandatoryEmpty() == false)
                    return false;


                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncMandatoryStatus") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d != "") {
                            fncShowSaveErrorInfor(msg.d);
                        }
                        else {
                            //$('#<%=hidItemname.ClientID%>').val(escape($('#<%=txtItemName.ClientID%>').val()));
                            $('#<%=hidItemname.ClientID%>').val($('#<%=txtItemName.ClientID%>').val());
                            fncConverthtmlTabletoJSON();
                            fncConverttblbarcodetoJson();
                            fncConverttblMultiplevendortoJson();
                            fncConvertMultipleUOMtoXML();
                            fncSaveInvPriceChange();
                            if ($("#<%=hidSaveStatus.ClientID %>").val() == "0") {
                                
                               // $("#<%=hidSaveStatus.ClientID %>").val('1');
                               <%-- $("#<%=btnSave.ClientID%>").click(); --%>
                                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                            }


                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);

            }
            //return status;
        }

        function fncCheckmandatoryEmpty() {
            try {
                var status = true;
                if ($('#<%=hidInventoryRange.ClientID %>').val() == "Y") {
                    $('#<%=txtItemCode.ClientID %>').removeAttr("disabled");
                }

                if ($('#<%=txtShortName.ClientID %>').val() == "") {
                    $('#<%=txtShortName.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtShortName.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtItemName.ClientID %>').val() == "") {
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtVendor.ClientID %>').val() == "") {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    $('#<%=txtVendor.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtFloor.ClientID %>').val() == "") {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtFloor.ClientID %>');
                    $('#<%=txtFloor.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtItemType.ClientID %>').val() == "") {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtItemType.ClientID %>');
                    $('#<%=txtItemType.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtBrand.ClientID %>').val() == "") {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtBrand.ClientID %>');
                    $('#<%=txtBrand.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtDepartment.ClientID %>').val() == "") {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID %>');
                    $('#<%=txtDepartment.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtInvcategory.ClientID %>').val() == "") {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid #383838");
                }
                //Viajy
                if ($('#<%=txtUOMPurchase.ClientID %>').val() == "") {
                    $('#<%=txtUOMPurchase.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtUOMPurchase.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtUOMPurchase.ClientID %>').css("border", "1px solid #383838");
                }

                if ($('#<%=txtSalesUOM.ClientID %>').val() == "") {
                    $('#<%=txtSalesUOM.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtSalesUOM.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtSalesUOM.ClientID %>').css("border", "1px solid #383838");
                }                
                
                if ($('#<%=txtStockType.ClientID %>').val() == "") {
                    $('#<%=txtStockType.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtStockType.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtStockType.ClientID %>').css("border", "1px solid #383838");
                }
                if (status == false) {
                    fncToastInformation("Please enter valid data for red color field");
                }
                else if ($('#<%=txtFetchSellingPrice.ClientID %>').val() == '0' || $('#<%=txtFetchSellingPrice.ClientID %>').val() == '') // Sivaraj 18122018
                {
                    fncToastInformation("Please Enter Selling Price and enter all mandatory fields");
                    $("#dialog-InvPrice").dialog("open");
                    fncOpenDialogWithDetail();
                    status = false;
                }
                else if ($('#<%=hidPriceEntered.ClientID %>').val() == "0") {
                    fncRemoveDisableAttForInputGST();
                }
                if (fncSaveInvPriceChange() == false) //For Get Inventory Price Detail
                {
                    //  alert('set');
                    $("*[id='divpriceSetting']").attr("disabled", "disabled");
                    //Prabu
                    status = false;
                }
                return status;

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// To show save error message
        function fncShowSaveErrorInfor(status) {
            try {


               
                if (status.indexOf("Vendor") != -1) {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid red");
                //popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    $('#<%=txtVendor.ClientID %>').select();
                }
                else {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Floor") != -1) {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid red");
                //popUpObjectForSetFocusandOpen = $('#<%=txtFloor.ClientID %>');
                    $('#<%=txtFloor.ClientID %>').select();
                }
                else {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("ItemType") != -1) {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid red");
                //popUpObjectForSetFocusandOpen = $('#<%=txtItemType.ClientID %>');
                    $('#<%=txtItemType.ClientID %>').select();
                }
                else {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Brand") != -1) {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid red");
                //popUpObjectForSetFocusandOpen = $('#<%=txtBrand.ClientID %>');
                    $('#<%=txtBrand.ClientID %>').select();
                }
                else {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Department") != -1) {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid red");
                //popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID %>');
                    $('#<%=txtDepartment.ClientID %>').select();
                }
                else {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Category") != -1) {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                }
                else {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid #383838");
                }

                fncToastInformation("Please enter valid data for red color field");

            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Set Focus to controller based on condition /// work done by saravanan 01-12-2017
        function fncInventoryKeyPress(evt, value) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 9) {
                    /// MRP Key Press
                    if (value == "MRP") {
                        if (($('#<%=rbtCostPopup.ClientID %>').is(':checked'))) {
                            if ($('#<%=hidNewCalculation.ClientID %>').val() == "N")
                                $('#<%=txtBasicCost.ClientID %>').select();
                            else {
                               <%-- if ($('#<%=ddlPurchase.ClientID %>').val() == "Cost")--%>
                                    $('#<%=txtBasicMargin.ClientID %>').select();
                               <%-- else
                                    $('#<%=txtMRPMarkDown.ClientID %>').select();--%>
                            }
                        }
                        else {
                            $('#<%=txtMRPMarkDown.ClientID %>').select();
                        }
                    }
                    else if (value == "SellingPrice") {
                        if ($('#<%=pnlWholeSalePrice.ClientID %>').is(":visible")) {
                            $('#<%=txtMarginWPrice1.ClientID %>').select();
                        }
                        else {
                            $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                        }
                    }
                    else if (value == "FixedMargin") {
                        if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {

                            if ($('#<%=pnlWholeSalePrice.ClientID %>').is(":visible")) {
                                $('#<%=txtMarginWPrice1.ClientID %>').select();
                            }
                            else {
                                //$("#ss").parent().find("button:eq(1)").focus(); vijay
                                $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                            }


                            // Assign WholeSale Price
                            txtWholePrice1Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                            txtWholePrice2Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                            txtWholePrice3Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                        }
                        else {
                            $('#<%=txtSellingPrice.ClientID %>').select();
                        }



                    }
                    else if (value == "MRPMarkDown") {
                        <%--if ($('#<%=rbtMRP.ClientID %>').prop('checked')) {
                            $('#<%=txtFixedMargin.ClientID %>').select();
                        }
                        else if($('#<%=rbnMRP2.ClientID %>').prop('checked'))--%>
                        $('#<%=txtAdDiscount.ClientID %>').select();
                    }
                    else if (value == "WPrice3") {
                        $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                    }
                    else if (value == "WPrice3Per") {
                        if ($('#<%=rbtMrpPopup.ClientID %>').prop('checked')) {
                            $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                        }
                        else {
                            $('#<%=txtWholePrice1Popup.ClientID %>').select();
                        }
                    }
                    else if (value == "AdDiscount") {
                        if ($('#<%=hidNewCalculation.ClientID %>').val() == "N")
                            $('#<%=txtFixedMargin.ClientID %>').select();
                        else
                            $('#<%=txtBasicMargin.ClientID %>').select();
                    }
                    else if (value == "BasicMargin") {
                        <%--if ($('#<%=ddlPurchase.ClientID %>').val() == "Cost")--%>
                            $('#<%=txtBasicCost.ClientID %>').select();
                        <%--else
                            $('#<%=txtFixedMargin.ClientID %>').select();--%>
                    }
                    if (value == "txtDiscountPrc3") {
                        $('#<%=txtFixedMargin.ClientID %>').select();
                    }
                    // txtDiscountPrc3
                    return false;
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }


        /// Work done by saravanan 02-12-2017
        function fncShowDuplicateInventory() {
            $(function () {
        <%--  setTimeout(function () {
            $("#<%=hidSaveStatus.ClientID %>").val('0');
            $('#<%=hidDailogName.ClientID %>').val("Deplicatename");
            $('#<%=lnkDupNameNo.ClientID %>').focus();
        }, 50);

        $("#dialog-DuplicateInventoryName").dialog({
            title: "Enterpriser Web",
            modal: true
        });--%>
                window.location.href = "frmInventoryMaster.aspx?";

            });
        };

        /// Work done by saravanan 02-12-2017
        function fncShowBreakPriceQty() {

            $(function () {

                if ($('#<%=chkBatch.ClientID %>').prop('checked')) {
                    fncGetBatchDetail_Master($('#<%=txtBreakPriceItem.ClientID%>').val());
                }
                else {
                    $('#<%=txtBreakMRP.ClientID%>').val($('#<%=txtMRP.ClientID%>').val());
                    $('#<%=hidbrkNetCost.ClientID%>').val($('#<%=txtNetCost.ClientID%>').val());
                    setTimeout(function () {
                        $('#<%=txtBreakQty.ClientID%>').select();
                    }, 50);

                }

                $("#div-BreakPriceQty").dialog({
                    title: "Break Price Qty",
                    width: "auto",
                    modal: true
                });
            });
        };

    </script>
    <%------------------------------------------- Clear Form -------------------------------------------%>
    <script type="text/javascript">

        var shortcutKeyvalue = "";

        function clearFormInv() {

            try {
                var sItemCode = $('#<%=txtItemCode.ClientID %>').val();
                clearForm();
                //$("#<%=btnBarcodeclear.ClientID %>").click();
                $('#<%=txtItemCode.ClientID %>').val(sItemCode);
                //WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$ContentPlaceHolder1$lnkItemHistory", "", false, "", "frmInventoryMaster.aspx", false, true));
                $('#<%=txtItemCode.ClientID %>').select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }

        }

        /// Bind Old Break Price to Table
        function fncBindBreakPriceQty() {

            var objBreakPrice;
            breakPriceBody = $("#tblBreakPriceQty tbody");
            try {
                objBreakPrice = jQuery.parseJSON($("#<%=hidBreakPriceQty.ClientID %>").val());

                if (objBreakPrice.length > 0) {
                    for (var i = 0; i < objBreakPrice.length; i++) {
                        breakRowPrice = "<tr>"
                            + "<td><img alt='Delete' src='../images/No.png' onclick='fncShowDeleteDialog(this);return false;' /></td>"
                            + "<td id='tdbrkRowNo_" + breakRowNo + "' >" + breakRowNo + "</td>"
                            + "<td id='tdbrkItemcode_" + breakRowNo + "' >" + objBreakPrice[i]["InventoryCode"] + "</td>"
                            + " <td id='tdbrkBatchNo_" + breakRowNo + "' >" + objBreakPrice[i]["BatchNo"] + "</td>"
                            + " <td id='tdbrkQty_" + breakRowNo + "' >" + objBreakPrice[i]["BreakQty"] + "</td>"
                            + "<td id='tdbrkPrice_" + breakRowNo + "' >" + objBreakPrice[i]["BreakPrice"] + "</td>"
                            + " <td>" + objBreakPrice[i]["BreakPriceType"] + "</td>"
                            + " <td>" + objBreakPrice[i]["NetCostValidation"] + "</td>"
                            + "</tr>";
                        breakPriceBody.append(breakRowPrice);
                        breakRowNo = parseInt(breakRowNo) + 1;
                    }
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }

        }

        /// Bind Old  Barcode 
        function fncBindBarcode() {

            var tblBarcode, objBarcode, row;
            tblBarcode = $("#tblBarcode tbody");
            try {
                objBarcode = jQuery.parseJSON($("#<%=hidbarcode.ClientID %>").val());
                //console.log(objBarcode);
                tblBarcode.children().remove();
                if (objBarcode.length > 0) {
                    for (var i = 0; i < objBarcode.length; i++) {
                        row = "<tr><td> " + "<img alt='Delete' src='../images/No.png' onclick='fncDeleteBarcode(this);return false;' /></td>" +
                            "<td id='tdBarcode' >" + objBarcode[i]["BarCode"] + "</td></tr>";
                        tblBarcode.append(row);
                    }
                }
                //console.log(tblBarcode);
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Add barcode work done by saravanan 19-02-2018
        function fncAddBarcode() {
            var objBarcode, status = true;
            try {
                tblBarcode = $("#tblBarcode tbody");

                if ($("#tblBarcode tbody").children().length > 0) {
                    $("#tblBarcode tbody").children().each(function () {
                        if ($(this).find('td[id*=tdBarcode]').text() == $('#<%=txtBarcode.ClientID %>').val()) {
                            status = false;
                            return;
                        }
                    });
                }

                if (status == false) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_alreadyadded%>');
                    $('#<%=txtBarcode.ClientID %>').val('');
                    return;
                }
                <%--else if ($('#<%=txtBarcode.ClientID %>').val().trim() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');                    
                    /*ShowPopupMessageBoxandFocustoObject('Please enter barcode');*/
                    return;
                }--%>
                else {
                    var obj = {};
                    obj.mode = "Barcode";
                    obj.barcode = $('#<%=txtBarcode.ClientID %>').val().trim();
                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncBarcodeValidation") %>',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != "") {
                                popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject("Duplicate barcode...Please check the inventorycode: " + msg.d.split(',')[0] + "(" + msg.d.split(',')[1] + ")  ");
                                $('#<%=txtBarcode.ClientID %>').val("");
                            }
                            else {
                                row = "<tr><td> " + "<img alt='Delete' src='../images/No.png' onclick='fncDeleteBarcode(this);return false;' /></td>" +
                                    "<td id='tdBarcode' >" + $('#<%=txtBarcode.ClientID %>').val() + "</td></tr>";
                                if ($('#<%=txtBarcode.ClientID %>').val() == "") {
                                    $('#<%=lnkSave.ClientID %>').click();
                                }
                                else {
                                    tblBarcode.append(row);
                                    $('#<%=txtBarcode.ClientID %>').val('');                /*05122022 musaraf*/
                                }                               
                                                             
                            }                           
                         
                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// delete barcode /// 
        function fncDeleteBarcode(source) {
            try {

                var rowObj, barcode;
                var barcodeObj = {};
                rowObj = $(source).parent().parent();

                if ($('#<%=hidDeletebarcode.ClientID %>').val() != "") {

                    //barcodeObj.barcode = rowObj.find('td[id*=tdBarcode]').text().trim();

                    barcode = $('#<%=hidDeletebarcode.ClientID %>').val();
                    barcode = barcode + "{\"Barcode\":" + "\"" + rowObj.find('td[id*=tdBarcode]').text().trim() + "\"},";
                }
                else {
                    barcode = "{\"Barcode\":" + "\"" + rowObj.find('td[id*=tdBarcode]').text().trim() + "\"},";
                }
                rowObj.remove();
                $('#<%=hidDeletebarcode.ClientID %>').val(barcode);


            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost) {
            try {
                $('#<%=txtBreakPriceBatchNo.ClientID%>').val(batchno);
                $('#<%=txtBreakMRP.ClientID%>').val(mrp);
                $('#<%=hidbrkNetCost.ClientID%>').val(Basiccost);
                $('#<%=txtBreakQty.ClientID%>').select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        // Clear Break Price Detail
        function fncClearBreakPriceQty() {
            try {
                $('#<%=txtBreakPriceBatchNo.ClientID%>').val('');
                $('#<%=txtBreakQty.ClientID%>').val('');
                $('#<%=txtBreakPrice.ClientID%>').val('');
                $('#<%= ddlBreakPriceType.ClientID %>')[0].selectedIndex = 0;
                $('#<%= ddlBreakPriceType.ClientID %>').trigger("liszt:updated");

                if ($('#<%=chkBatch.ClientID %>').prop("checked")) {
                    $('#<%=txtBreakMRP.ClientID%>').val('');
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Add Break Price Detail
        function fncAddBreakPriceQty() {
            try {
                var netCostVal = 1;
                if ($('#<%=chbrkNetCostVal.ClientID%>').prop("checked")) {
                    netCostVal = 1;
                }
                else {
                    netCostVal = 0;
                }



                if (fncBreakPriceValidation() == true) {

                    breakPriceBody = $("#tblBreakPriceQty tbody");
                    breakRowPrice = "<tr>"
                        + "<td><img alt='Delete' src='../images/No.png' onclick='fncShowDeleteDialog(this);return false;' /></td>"
                        + "<td id='tdbrkRowNo_" + breakRowNo + "' >" + breakRowNo + "</td>"
                        + "<td id='tdbrkItemcode_" + breakRowNo + "' >" + $('#<%=txtBreakPriceItem.ClientID%>').val() + "</td>"
                        + " <td id='tdbrkBatchNo_" + breakRowNo + "' >" + $('#<%=txtBreakPriceBatchNo.ClientID%>').val() + "</td>"
                        + " <td id='tdbrkQty_" + breakRowNo + "' >" + $('#<%=txtBreakQty.ClientID%>').val() + "</td>"
                        + "<td id='tdbrkPrice_" + breakRowNo + "' >" + $('#<%=txtBreakPrice.ClientID%>').val() + "</td>"
                        + " <td>" + $('#<%=ddlBreakPriceType.ClientID%>').val() + "</td> "
                        + " <td>" + netCostVal + "</td>"
                        + "</tr>";
                    breakPriceBody.append(breakRowPrice);
                    breakRowNo = parseInt(breakRowNo) + 1;
                    fncClearBreakPriceQty();
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Break Price Validation
        function fncBreakPriceValidation() {
            try {

                var status = true;

                if ($('#<%=txtBreakQty.ClientID%>').val().trim() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBreakQty.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_enterbreakQty%>');
                    status = false;
                }
                else if ($('#<%=txtBreakPrice.ClientID%>').val().trim() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_enterbreakprice%>');
                    status = false;
                }
                else if ($('#<%=txtBreakMRP.ClientID%>').val().trim() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBreakMRP.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_anyonebatch%>');
                    status = false;
                }
                else if (parseFloat($('#<%=txtBreakPrice.ClientID%>').val()) > parseFloat($('#<%=txtBreakMRP.ClientID%>').val())) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_BreakPricelessMRP%>');
                    status = false;
                }
                else if ($('#<%=chbrkNetCostVal.ClientID%>').prop("checked")) {
                    if (parseFloat($('#<%=txtBreakPrice.ClientID%>').val()) < parseFloat($('#<%=hidbrkNetCost.ClientID%>').val())) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_BreakPricegreateNetCost%>');
                        status = false;
                    }
                }

                $("#tblBreakPriceQty tbody").children().each(function () {

                    if ($(this).find('td[id*=tdbrkItemcode]').text().trim() == $('#<%=txtBreakPriceItem.ClientID %>').val().trim() &&
                        $(this).find('td[id*=tdbrkBatchNo]').text().trim() == $('#<%=txtBreakPriceBatchNo.ClientID %>').val().trim() &&
                        parseFloat($(this).find('td[id*=tdbrkQty]').text()) == parseFloat($('#<%=txtBreakQty.ClientID %>').val())) {
                        popUpObjectForSetFocusandOpen = $('#<%=txtBreakQty.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_alreadyadded%>');
                        status = false;
                    }
                });


                return status;
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// delete Break Price Item
        function fncDeleteBrkItem(source) {
            try {

                $(source).parent().parent().remove();

                if ($("#tblBreakPriceQty tbody").children().length > 0) {
                    breakRowNo = 1;
                    $("#tblBreakPriceQty tbody").children().each(function () {
                        $(this).find('td[id*=tdbrkRowNo]').text(breakRowNo);
                        breakRowNo = breakRowNo + 1;
                    });


                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }

        }

        /// show Break Price Qty Delete Dialog 
        function fncShowDeleteDialog(source) {

            var obj = {}, rowObj;
            $(function () {
                $("#delete-BreakPriceQty").html('<%=Resources.LabelCaption.msg_delete_com%>');
                $("#delete-BreakPriceQty").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Yes: function () {
                            $(this).dialog("destroy");

                            rowObj = $(source).parent().parent();

                            obj.inventorycode = rowObj.find('td[id*=tdbrkItemcode]').text().trim();
                            obj.breakQty = rowObj.find('td[id*=tdbrkQty]').text().trim();
                            obj.breakPrice = rowObj.find('td[id*=tdbrkPrice]').text().trim();
                            obj.batchNo = rowObj.find('td[id*=tdbrkBatchNo]').text().trim();
                            $.ajax({
                                type: "POST",
                                url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncDeleteBreakPrice") %>',
                                data: JSON.stringify(obj),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (msg) {
                                    if (msg.d == "1") {
                                        fncDeleteBrkItem(source);
                                        fncShowBreakPriceDeleteSuccess();
                                    }

                                },
                                error: function (data) {
                                    ShowPopupMessageBox(data.message);
                                }
                            });


                        },
                        No: function () {
                            $(this).dialog("destroy");
                        }
                    },
                    modal: true
                });
            });
        };

        /// convert Html table to JSon Format
        function fncConverthtmlTabletoJSON() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {

                $.each($("#tblBreakPriceQty thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblBreakPriceQty tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidBreakPriceQty.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// convert html table to json(barcode)
        function fncConverttblbarcodetoJson() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {


                $.each($("#tblBarcode thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblBarcode tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidbarcode.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        /// convert html table to json(barcode)
        function fncConverttblMultiplevendortoJson() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {


                $.each($("#tblNewVendorCreation thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblNewVendorCreation tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidMultipleVendor.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncConvertMultipleUOMtoXML() {
            var obj, status = "0";
            var bin = "0", active = "0";
            try {
                var xml = '<NewDataSet>';
                $("#tblMultipleUOM tbody").children().each(function () {
                    obj = $(this);

                    if (obj.find('td input[id*="cbBin"]').is(":checked")) {
                        bin = "1";
                    }
                    else {
                        bin = "0";
                    }

                    if (obj.find('td input[id*="cbActive"]').is(":checked")) {
                        active = "1";
                    }
                    else {
                        active = "0";
                    }

                    status = "1";
                    xml += "<Table>";
                    xml += "<Inventorycode>" + $("#<%=txtItemCode.ClientID %>").val() + "</Inventorycode>";
                    xml += "<UOMCode>" + obj.find('td[id*="tdUOMCode"]').text().trim() + "</UOMCode>";
                    xml += "<UOMQty>" + obj.find('td[id*="tdConvQty"]').text().trim() + "</UOMQty>";
                    xml += "<Bin>" + bin + "</Bin>";
                    xml += "<Active>" + active + "</Active>";
                    xml += "</Table>";

                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);

                if (status == "0") {
                    xml = "";
                }

                $("#<%=hidMultipleUOMSave.ClientID %>").val(xml);

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncShowBreakPriceDeleteSuccess() {
            $(function () {
                $("#delete-BreakPriceQtySuccess").html('<%=Resources.LabelCaption.msg_success_com%>');
                $("#delete-BreakPriceQtySuccess").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        Ok: function () {
                            $(this).dialog("destroy");
                        }
                    },
                    modal: true
                });
            });
        };

        ///Function key set
        function disableFunctionKeys(e) {
            try {
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
                if (e.keyCode == 116) {
                    e.preventDefault();
                }
                if (e.keyCode == 27) {
                    window.parent.jQuery('#popupInvMaster').dialog('close');
                    window.parent.$('#popupInvMaster').remove();
                    return;

                }

                if ($("#<%=hidDailogName.ClientID %>").val() == "Deplicatename") {
                    if (e.keyCode == 37) {
                        $("#<%=lnkDupNameYes.ClientID %>").focus();
                    }
                    else if (e.keyCode == 39) {
                        $("#<%=lnkDupNameNo.ClientID %>").focus();
                    }
                }
                else if ($("#<%=hidDailogName.ClientID %>").val() == "NewItemSave") {
                    if (e.keyCode == 37) {
                        $("#<%=lnkNewItemYes.ClientID %>").focus();
                    }
                    else if (e.keyCode == 39) {
                        $("#<%=lnkNewItemNo.ClientID %>").focus();
                    }
                    //else if (e.keyCode == 27 || e.keyCode == 27 || e.keyCode == 27 || e.keyCode == 27)
                    //    e.preventDefault();
                }


                //hidDailogName

                var functionKeys = new Array(113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            Validate();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        // clearFormInv();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 119) {
                        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$ContentPlaceHolder1$lnkItemHistory", "", false, "", "frmInventoryMasterView.aspx", false, true));
                        e.preventDefault();
                    }
                    else if (e.keyCode == 118) {
                        if (shortcutKeyvalue == "Category") {
                            page = '<%=ResolveUrl("~/Inventory/frmCategoryMaster.aspx") %>';
                    $dialog = $('<div id="popupcategory" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Category",
                        closeOnEscape: true
                    });
                    $dialog.dialog('open');


                }
                else if (shortcutKeyvalue == "Department") {
                    page = '<%=ResolveUrl("~/Inventory/frmDepartmentMaster.aspx") %>';
                    $dialog = $('<div id="popupDepartment"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Department"

                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Brand") {
                    page = '<%=ResolveUrl("~/Inventory/frmBrandMaster.aspx") %>';
                    $dialog = $('<div id="popupBrand"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Brand"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "WareHouse") {
                    page = '<%=ResolveUrl("~/Masters/frmWarehouse.aspx") %>';
                    $dialog = $('<div id="popupWareHouse"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "WareHouse"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Manufacture") {
                    page = '<%=ResolveUrl("~/Masters/frmManufacturer.aspx") %>';
                    $dialog = $('<div id="popupManufacture"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Manufacture"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Merchandise") {
                    page = '<%=ResolveUrl("~/Masters/frmMerchandise.aspx") %>';
                    $dialog = $('<div id="popupMerchandise"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Merchandise"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "ItemType") {
                    page = '<%=ResolveUrl("~/Masters/frmItemType.aspx") %>';
                    $dialog = $('<div id="popupItemType"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "ItemType"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Style") {
                    page = '<%=ResolveUrl("~/Masters/frmstyle.aspx") %>';
                    $dialog = $('<div id="popupStyle"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Style"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Size") {
                    page = '<%=ResolveUrl("~/Masters/frmSize.aspx") %>';
                    $dialog = $('<div id="popupSize"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Size"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Origin") {
                    page = '<%=ResolveUrl("~/Masters/frmOrigin.aspx") %>';
                    $dialog = $('<div id="popupOrigin"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Origin"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "SubCategory") {
                    page = '<%=ResolveUrl("~/Masters/frmsubCategory.aspx") %>';
                    $dialog = $('<div id="popupSubCategory"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "SubCategory"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Floor") {
                    page = '<%=ResolveUrl("~/Masters/frmFloor.aspx") %>';
                    $dialog = $('<div id="popupFloor"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Floor"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Bin") {
                    page = '<%=ResolveUrl("~/Masters/frmBin.aspx") %>';
                    $dialog = $('<div id="popupBin"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Bin"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "CCode") {
                    page = '<%=ResolveUrl("~/Inventory/frmCompanySettings.aspx") %>';
                    $dialog = $('<div id="popupCCode"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Company Settings"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Vendor") {
                    page = '<%=ResolveUrl("~/Masters/frmVendorMaster.aspx") %>';
                    $dialog = $('<div id="popupVendor"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Vendor"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Class") {
                    page = '<%=ResolveUrl("~/Masters/frmClass.aspx") %>';
                    $dialog = $('<div id="popupClass"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Class"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "SubClass") {
                    page = '<%=ResolveUrl("~/Masters/frmSubClass.aspx") %>';
                    $dialog = $('<div id="popupSubClass"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "SubClass"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Section") {
                    page = '<%=ResolveUrl("~/Masters/frmSection.aspx") %>';
                    $dialog = $('<div id="popupSection"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 645,
                        width: 1300,
                        title: "Section"
                    });
                    $dialog.dialog('open');
                }
                else if (shortcutKeyvalue == "Shelf") {
                    page = '<%=ResolveUrl("~//Masters/frmshelf.aspx") %>';
                            $dialog = $('<div id="popupShelf"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                                autoOpen: false,
                                modal: true,
                                height: 645,
                                width: 1300,
                                title: "Shelf"
                            });
                            $dialog.dialog('open');
                        }


                        e.preventDefault();
                    }
                    else if (e.keyCode == 114) {

                        if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                            if (shortcutKeyvalue == "Vendor") {

                                /// save Multiple Vendor
                                var obj = {};
                                obj.mode = "GetVendorList";
                                obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
                        obj.vendorcode = "";
                        $.ajax({
                            type: "POST",
                            url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncSaveAndDeleteMultipleVendor") %>',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            fncBindMultpleVendor(msg.d);
                            fncShowMultipleVendorCreation();
                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });

                            }
                        }

                        shortcutKeyvalue = "";

                        e.preventDefault();
                        return false;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }


        }

        //// Bind Multiple Vendors
        function fncBindMultpleVendor(vendorlist) {
            var objMultipleVendor, row, tblNewVendorCreation;
            try {
                tblNewVendorCreation = $("#tblNewVendorCreation tbody");
                objMultipleVendor = jQuery.parseJSON(vendorlist);
                tblNewVendorCreation.children().remove();
                if (objMultipleVendor.length > 0) {
                    for (var i = 0; i < objMultipleVendor.length; i++) {
                        row = "<tr>"
                            + "<td id='tdRowNo' >" + objMultipleVendor[i]["RowNo"] + "</td>"
                            + "<td id='tdVendorcode' >" + objMultipleVendor[i]["VendorCode"] + "</td>"
                            + "<td id='tdVendorname' >" + objMultipleVendor[i]["VendorName"] + "</td>"
                            + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                            + "</tr>";
                        tblNewVendorCreation.append(row);
                        tblNewVendorCreation.children().click(fncMultipleVendorRowClick);
                    }
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }


        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            fncdecimal();
            fncInventoryScreenChangeForDailog();


        });

        function fncInventoryScreenChangeForDailog() {
            try {
                if ($('#<%=hidInvOpenStatus.ClientID %>').val() == "dailog") {
                    fncHideMasterMenu();
                    $(".tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2)").css("min-width", "229px");
                    $(".tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2)").css("max-width", "229px");
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncdecimal() {
            $('#ContentPlaceHolder1_txtCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtMRP').number(true, 2);
            $('#txtNumericalDigits').number(true, 2);
            $('#ContentPlaceHolder1_txtMRPMarkDown').number(true, 2);
            $('#ContentPlaceHolder1_txtBasicMargin').number(true, 2);
            $('#ContentPlaceHolder1_txtBasicCost').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc1').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc2').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc3').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtGrossCost').number(true, 2);
            $('#ContentPlaceHolder1_txtInputSGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtInputSGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCESSAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtNetCost').number(true, 2);

            $('#ContentPlaceHolder1_txtFixedMargin').number(true, 2);
            $('#ContentPlaceHolder1_txtBasicSelling').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputSGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputSGSTAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtOutputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtOutputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtRoundOff').number(true, 2);
            $('#ContentPlaceHolder1_txtSellingPrice').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiabilityPer').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiability').number(true, 2);

            $('#ContentPlaceHolder1_txtGSTLiabilityPer').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiability').number(true, 2);

            $('#<%=txtBreakQty.ClientID%>').number(true, 0);
            $('#<%=txtBreakPrice.ClientID%>').number(true, 2);
            $('#<%=txtBreakMRP.ClientID%>').number(true, 2);
            $('#<%=txtAddCessAmt.ClientID%>').number(true, 2);
            $('#<%=txtAdDiscount.ClientID%>').number(true, 2);

            $(<%=txtMRPMargrin.ClientID%>).number(true, 2);
            $(<%=txtsellMargin.ClientID%>).number(true, 2);
            $(<%=txtMRPMarkDownNew.ClientID%>).number(true, 2);
            $(<%=txtMRPMarkUp.ClientID%>).number(true, 2);
            $(<%=txtSellMarkDown.ClientID%>).number(true, 2);
            $(<%=txtSellMarkUp.ClientID%>).number(true, 2);                       

            $('#ContentPlaceHolder1_txtWeight').number(true, 3);
            $('#ContentPlaceHolder1_txtWidth').number(true, 2);
            $('#ContentPlaceHolder1_txtHeight').number(true, 2);
            $('#ContentPlaceHolder1_txtLength').number(true, 2);

            $('#ContentPlaceHolder1_txtWholePrice1Popup').number(true, 2);
            $('#ContentPlaceHolder1_txtWholePrice2Popup').number(true, 2);
            $('#ContentPlaceHolder1_txtWholePrice3Popup').number(true, 2);

            $('#ContentPlaceHolder1_txtEarnedMar1').number(true, 2);
            $('#ContentPlaceHolder1_txtEarnedMar2').number(true, 2);
            $('#ContentPlaceHolder1_txtEarnedMar3').number(true, 2); 
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            //alert(keyCode);
            //if (charCode == 188 || charCode == 190 || charCode == 191)
            //    return false;
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == 'Edit') {
                if (charCode == 112) {
                    fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
                }

            }
        }
        function fncShowVendorSearchDialogMultiVendor(event, value) {
            try {
                setTimeout(function () {
                    shortcutKeyvalue = "MultiVendor";
                }, 50);
                fncShowSearchDialogCommon(event, value, 'txtNewVendorcode', '');
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        //// Short cut key function Option
        function fncToEnableShortcutKey(value) {
            try {

                if (value == "Category") {
                    shortcutKeyvalue = "Category";
                    $('#lblMasterOpen').text("Press F7 to Create New Category");
            //$('#<%=hidShortcutKey.ClientID%>').val("Category");
                }
                else if (value == "Department") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Department");
                    shortcutKeyvalue = "Department";
                    $('#lblMasterOpen').text("Press F7 to Create New Department");
                }
                else if (value == "Brand") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Brand");
                    shortcutKeyvalue = "Brand";
                    $('#lblMasterOpen').text("Press F7 to Create New Brand");
                }
                else if (value == "WareHouse") {
            //$('#<%=hidShortcutKey.ClientID%>').val("WareHouse");
                    shortcutKeyvalue = "WareHouse";
                    $('#lblMasterOpen').text("Press F7 to Create New WareHouse");
                }
                else if (value == "Manufacture") {
                   // $('#<%=hidShortcutKey.ClientID%>').val("Manufacture");
                    shortcutKeyvalue = "Manufacture";
                    $('#lblMasterOpen').text("Press F7 to Create New Manufacture");
                    //shortcutKeyvalue = "Manufacture";
                }
                else if (value == "Merchandise") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Merchandise");
                    shortcutKeyvalue = "Merchandise";
                    $('#lblMasterOpen').text("Press F7 to Create New Merchandise");
                }
                else if (value == "ItemType") {
            //$('#<%=hidShortcutKey.ClientID%>').val("ItemType");
                    shortcutKeyvalue = "ItemType";
                    $('#lblMasterOpen').text("Press F7 to Create New Item Type");
                }
                else if (value == "Style") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Style");
                    shortcutKeyvalue = "Style";
                    $('#lblMasterOpen').text("Press F7 to Create New Style");
                }
                else if (value == "Size") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Size");
                    shortcutKeyvalue = "Size";
                    $('#lblMasterOpen').text("Press F7 to Create New Size");
                }
                else if (value == "Origin") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Origin");
                    shortcutKeyvalue = "Origin";
                    $('#lblMasterOpen').text("Press F7 to Create New Origin");
                }
                else if (value == "SubCategory") {
            //$('#<%=hidShortcutKey.ClientID%>').val("SubCategory");
                    shortcutKeyvalue = "SubCategory";
                    $('#lblMasterOpen').text("Press F7 to Create New SubCategory");
                }
                else if (value == "Floor") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Floor");
                    shortcutKeyvalue = "Floor";
                    $('#lblMasterOpen').text("Press F7 to Create New Floor");
                }
                else if (value == "Bin") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Bin");
                    shortcutKeyvalue = "Bin";
                    $('#lblMasterOpen').text("Press F7 to Create New Bin");
                }
                else if (value == "CCode") {
            //$('#<%=hidShortcutKey.ClientID%>').val("CCode");
                    shortcutKeyvalue = "CCode";
                    $('#lblMasterOpen').text("Press F7 to Create New CCode");
                }
                else if (value == "Vendor") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Vendor");
                    shortcutKeyvalue = "Vendor";
                    $('#lblMasterOpen').text("Press F7 to Create New Vendor");
                }
                else if (value == "Class") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Class");
                    shortcutKeyvalue = "Class";
                    $('#lblMasterOpen').text("Press F7 to Create New Class");
                }
                else if (value == "SubClass") {
                    shortcutKeyvalue = "SubClass";
                    $('#lblMasterOpen').text("Press F7 to Create New SubClass");
                }
                else if (value == "Section") {
                    shortcutKeyvalue = "Section";
                    $('#lblMasterOpen').text("Press F7 to Create New Section");
                }
                else if (value == "Shelf") {
                    shortcutKeyvalue = "Shelf";
                    $('#lblMasterOpen').text("Press F7 to Create New Shelf");
                }
                else if (value == "Itemname") {
                    shortcutKeyvalue = "";
                    $('#<%=txtItemName.ClientID%>').select();
                    $('#lblMasterOpen').text("Press Alt+L to Change New Language");
                }
                
                else {
                    shortcutKeyvalue = "";
                    $('#lblMasterOpen').text("");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncTextboxKeyDown(event, value) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    if (value == "brand") {
                        $('#<%=txtItemName.ClientID%>').val($('#<%=txtBrand.ClientID%>').val() + ' ' + $('#<%=txtInvcategory.ClientID%>').val());
                        $('#<%=txtItemName.ClientID%>').select();
                        return false;
                    }
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncShowNewItemSavedDailog() {
            $(function () {
                $("#<%=hidSaveStatus.ClientID %>").val('0');
                $("#<%=txtBasicMargin.ClientID %>").val('0');
                $("#<%=hidDeletebarcode.ClientID %>").val('');
                $('#<%=hidDailogName.ClientID%>').val("NewItemSave");
                //$("#NewItemSave").html("Inventory Successfully Updated. \n Do You want to continue with existing values ?");
                $("#NewItemSave").dialog({
                    title: "Enterpriser Web",
                    modal: true
                });
            });
        };
        function fncShowItemEditDailog() {
            $(function () {
                $("#<%=txtBasicMargin.ClientID %>").val('0');
                $("#<%=hidSaveStatus.ClientID %>").val('0');
                $("#<%=hidDeletebarcode.ClientID %>").val('');
                $("#ItemEdit").html("Inventory Updated Successfully");    /*Saved*/
                $("#ItemEdit").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Ok": function () {
                            $(this).dialog("destroy");
                            window.location.href = "frmInventoryMasterView.aspx";
                            //__doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                        }
                    },
                    modal: true
                });
            });
        };

        function fncShowMultipleVendorDeleteDailog(source) {
            $(function () {
                $("#DeleteMultiplevendor").html("Do you want delete slected vendor?");
                $("#DeleteMultiplevendor").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            fncDeleteMultipleVendor(source);
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                        },
                    },
                    modal: true
                });
            });
        };

        $(document).ready(function () {
            $(" #vendorNametooltip ").mouseenter(function () {
                var word = $('#<%=hidVendorName.ClientID%>').val();
                $(" #vendorNametooltip ").attr('title', word);
            });
        });

        /// Inventory code Key Function
        function fncInventorykeydown(keyCode) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    if ($('#<%=txtItemCode.ClientID%>').val().trim() == "") {
                        return false;
                    }
                    else if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                        $('#<%=txtItemCode.ClientID%>').attr("readonly", "readonly");
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkEditItemCode', '');
                    }
                    return false;
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                $("#invhdr").text("Inventory History");
            }
            else {
                $("#userdetail").css("display", "none");
            }

        });

        /// Go GRN Page
        function fncGoGRNPage(source) {
            var rowObj, grnNo = "", gaNo = "", billNo = "";
            var vendorcode = "", billType = "", locationCode = "";
            try {
                rowObj = $(source).closest("tr");
                grnNo = $(source).text().trim();
                billNo = $("td", rowObj).eq(1).html().replace(/&nbsp;/g, '');
                vendorcode = $("td", rowObj).eq(12).html().replace(/&nbsp;/g, '');
                billType = $("td", rowObj).eq(13).html().replace(/&nbsp;/g, '');
                locationCode = $("td", rowObj).eq(14).html().replace(/&nbsp;/g, '');

                var page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                var page = page + "?GRNNo=" + grnNo + "&GANo=" + gaNo;
                var page = page + "&BillNo=" + billNo + "&VendorCode=" + vendorcode;
                var page = page + "&Status=View&BillType=" + billType;
                var page = page + "&LoadPoQty=N" + "&locationcode=" + locationCode;
                var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "VIEW GRN MAINTENANCE"
                });

                //alert(page);
                $dialog.dialog('open');
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        ///New Vendor Creation
        function fncShowMultipleVendorCreation() {
            try {
                $("#dialog-MultipleVendorCreation").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Multiple Vendor",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Mandatory Field validation
        function fncMandatoryval(event, value) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    $('#<%=txtBrand.ClientID%>').select();
                    return false;
                }
                else if (keyCode == 118) {
                    if (value == "Department") {
                        page = '<%=ResolveUrl("~/Inventory/frmDepartmentMaster.aspx") %>';
                        $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                            autoOpen: false,
                            modal: true,
                            height: 645,
                            width: 1300,
                            title: "Department"
                        });

                    }
                    return false;
                }


            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Check Mandatory feilds
        function fncToCheckMandatoryFeilds(value) {
            try {
                if (value == "Department") {
                    if ($('#<%=txtDepartment.ClientID%>').val() == "") {
                        popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject("Please enter departmentcode");
                    }
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        //tblChild
        $(document).ready(function () {
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                funBindSaleshistory(jQuery.parseJSON($('#<%=hidSaleshistory.ClientID%>').val()));
                $("*[name$='grpPurchased']").attr("disabled", "disabled");
            }

        });

        /// Bind Sales history
        function funBindSaleshistory(jsonObj) {
            var tableid = "Saleshistory";
            try {

                if (jsonObj == null)
                    return;

                var tblSalesHistory = document.getElementById(tableid);
                var SalestableContainer = document.getElementById('Saleshistory_id')

                if (tblSalesHistory) {
                    SalestableContainer.removeChild(tblSalesHistory);
                }

                tblSalesHistory = CreateTableFromJSON_Common(jsonObj, tableid);
                SalestableContainer.appendChild(tblSalesHistory);
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Bind Child Sales history
        function funBindChildSaleshistory(jsonObj) {
            var tableid = "childSaleshistory";
            try {

                if (jsonObj == null)
                    return;

                var tblChildSalesHistory = document.getElementById(tableid);
                var childSalestableContainer = document.getElementById('childSaleshistory_id')

                if (tblChildSalesHistory) {
                    childSalestableContainer.removeChild(tblChildSalesHistory);
                }

                tblChildSalesHistory = CreateTableFromJSON_Common(jsonObj, tableid);
                childSalestableContainer.appendChild(tblChildSalesHistory);
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

          /// Bind Stock and Purchase history
        function funBindStockandPurhistory(jsonObj) {  
            var tableid = "StockandPurhistory";
            try {

                if (jsonObj == null)
                    return;

                var tblStockandPurhistory = document.getElementById(tableid);
                var StocktableContainer = document.getElementById('StockandPurhistory_id')

                if (tblStockandPurhistory) {
                    StocktableContainer.removeChild(tblStockandPurhistory);
                }

                tblStockandPurhistory = CreateTableFromJSON_Common(jsonObj, tableid);
                StocktableContainer.appendChild(tblStockandPurhistory);
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }


        /// Get Item Sales history
        function fncGetSalesItemSaleshistory(invmode, mode, inventorycode) {
            var obj = {};
            try {
                obj.mode = mode;
                obj.inventorycode = inventorycode;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncGetItemSaleshistory") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (invmode == "normal") {
                            funBindSaleshistory(jQuery.parseJSON(msg.d));
                        }
                        else {
                            funBindChildSaleshistory(jQuery.parseJSON(msg.d));
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncChildSaleshistorycheckchanged() {
            try {

        //$('#<%=hidchildItem.ClientID%>').val($('#<%=txtItemCode.ClientID%>').val());

                if ($("#<%=rbnMonth.ClientID%>").is(':checked')) {
                    fncGetSalesItemSaleshistory("child", "Month", $('#<%=hidchildItem.ClientID%>').val());
                }
                else if ($("#<%=rbnWeek.ClientID%>").is(':checked')) {
                    fncGetSalesItemSaleshistory("child", "Week", $('#<%=hidchildItem.ClientID%>').val());
                }
                else {
                    fncGetSalesItemSaleshistory("child", "Day", $('#<%=hidchildItem.ClientID%>').val());
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Item sales History check changed
        function fncSaleshistorycheckchanged() {
            try {

                if ($("#<%=rbnSalesMonth.ClientID%>").is(':checked')) {
                    fncGetSalesItemSaleshistory("normal", "Month", $('#<%=txtItemCode.ClientID%>').val());
                }
                else if ($("#<%=rbnSalesWeek.ClientID%>").is(':checked')) {
                    fncGetSalesItemSaleshistory("normal", "Week", $('#<%=txtItemCode.ClientID%>').val());
                }
                else {
                    fncGetSalesItemSaleshistory("normal", "Day", $('#<%=txtItemCode.ClientID%>').val());
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncGetStockandpurhistory(invmode, mode, inventorycode) {  
            var obj = {};
            try {
                obj.mode = mode;
                obj.inventorycode = inventorycode;

                $.ajax({
                    type: "POST",                                 
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncGetItemStockandPurHistory") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (invmode == "normal") {
                            funBindStockandPurhistory(jQuery.parseJSON(msg.d));
                        }                        
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
             }
             catch (err) {
                 //ShowPopupMessageBox(err.message);
             }
         }

        function fncStockandPurhistorycheckchanged() {  
            try {

                if ($("#<%=rbnStockandPurMonth.ClientID%>").is(':checked')) {
                    fncGetStockandpurhistory("normal", "Month", $('#<%=txtItemCode.ClientID%>').val());
                }
                else if ($("#<%=rbnStockandPurWeek.ClientID%>").is(':checked')) {
                    fncGetStockandpurhistory("normal", "Week", $('#<%=txtItemCode.ClientID%>').val());
                }
                else {
                    fncGetStockandpurhistory("normal", "Day", $('#<%=txtItemCode.ClientID%>').val());
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncShowView() {  
            $("#divView").dialog({
                title: "View",
                modal: true,
                width: "auto"
            });
        };


        $(document).ready(function () {
            fncCombinationkeypress();
            fncDisableAndEnableTab();
            fncTamilLanguage();


        });

        function fncTamilLanguage() {
            try {
                $("#<%=txtOtherLanguageName.ClientID%>").on('keydown', function (event) {
                    var charCode = (event.which) ? event.which : event.keyCode;
                    if (charCode == 13) {
                        $("#<%=txtOtherLanguageSName.ClientID%>").val($("#<%=txtOtherLanguageName.ClientID%>").val());
                        $("#<%=txtOtherLanguageSName.ClientID%>").select();
                        return false;
                    }
                    else if (event.which == 121) {
                        $(this).toggleClass('tamil');
                        return false;
                    }
                    if ($(this).hasClass('tamil')) {
                        toggleKBMode(event);
                    } else {
                        return true;
                    }
                });
                $("#<%=txtOtherLanguageName.ClientID%>").on('keypress', function (event) {
                    if ($(this).hasClass('tamil')) {
                        convertThis(event);
                    }
                });

                $("#<%=txtOtherLanguageSName.ClientID%>").on('keydown', function (event) {
                    var charCode = (event.which) ? event.which : event.keyCode;
                    if (charCode == 13) {
                        $("#<%=txtRemarks.ClientID%>").select();
                        return false;
                    }

                    if (event.which == 121) {
                        $(this).toggleClass('tamil');
                        return false;
                    }
                    if ($(this).hasClass('tamil')) {
                        toggleKBMode(event);
                    } else {
                        return true;
                    }
                });
                $("#<%=txtOtherLanguageSName.ClientID%>").on('keypress', function (event) {
                    if ($(this).hasClass('tamil')) {
                        convertThis(event);
                    }
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// Disable and Enable Tab
        function fncDisableAndEnableTab() {
            try {
                if ($('#<%=hidinventoryStatus.ClientID%>').val() == "New") {

                    $('#liStockPurchase').addClass('disabled');
                    $('#liStockPurchase').not('.active').find('a').removeAttr("data-toggle");

                    $('#liSales').addClass('disabled');
                    $('#liSales').not('.active').find('a').removeAttr("data-toggle");

                    $('#liChildInventory').addClass('disabled');
                    $('#liChildInventory').not('.active').find('a').removeAttr("data-toggle");

                    $('#liPromotion').addClass('disabled');
                    $('#liPromotion').not('.active').find('a').removeAttr("data-toggle");
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Combination key press
        function fncCombinationkeypress() {
            try {

                $.alt('1', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', true);
                    $('#<%=rbtMRP.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP2.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP3.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP4.ClientID %>').prop('checked', false);
                });
                $.alt('2', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', false);
                    $('#<%=rbtMRP.ClientID %>').prop('checked', true);
                    $('#<%=rbnMRP2.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP3.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP4.ClientID %>').prop('checked', false);
                });
                $.alt('3', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', false);
                    $('#<%=rbtMRP.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP2.ClientID %>').prop('checked', true);
                    $('#<%=rbnMRP3.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP4.ClientID %>').prop('checked', false);
                });
                $.alt('4', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', false);
                    $('#<%=rbtMRP.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP2.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP3.ClientID %>').prop('checked', true);
                    $('#<%=rbnMRP4.ClientID %>').prop('checked', false);
                });
                $.alt('5', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', false);
                    $('#<%=rbtMRP.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP2.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP3.ClientID %>').prop('checked', false);
                    $('#<%=rbnMRP4.ClientID %>').prop('checked', true);
                });

                $.alt('I', function () {


                    $('#liInventoryPricing').addClass('active');
                    $('#InventoryPricing').addClass('active');


                    $('#liBreakPrice').removeClass('active');
                    $('#BreakPrice').removeClass('active');

                    $('#liActivationSettings').removeClass('active');
                    $('#ActivationSettings').removeClass('active');

                    $('#liStockPurchase').removeClass('active');
                    $('#StockPurchase').removeClass('active');

                    $('#liSales').removeClass('active');
                    $('#Sales').removeClass('active');

                    $('#liChildInventory').removeClass('active');
                    $('#ChildInventory').removeClass('active');

                    $('#liImage').removeClass('active');
                    $('#Image').removeClass('active');

                    $('#liPromotion').removeClass('active');
                    $('#Promotion').removeClass('active');

                    $('#<%=imgPriceChange.ClientID %>').focus();
                });
                $.alt('O', function () {

           <%-- setTimeout(function () {
                $('#<%=txtMinimumQty.ClientID %>').select();
            }, 50);--%>

                    $('#liBreakPrice').addClass('active');
                    $('#BreakPrice').addClass('active');

                    $('#liInventoryPricing').removeClass('active');
                    $('#InventoryPricing').removeClass('active');

                    $('#liActivationSettings').removeClass('active');
                    $('#ActivationSettings').removeClass('active');

                    $('#liStockPurchase').removeClass('active');
                    $('#StockPurchase').removeClass('active');

                    $('#liSales').removeClass('active');
                    $('#Sales').removeClass('active');

                    $('#liChildInventory').removeClass('active');
                    $('#ChildInventory').removeClass('active');

                    $('#liImage').removeClass('active');
                    $('#Image').removeClass('active');

                    $('#liPromotion').removeClass('active');
                    $('#Promotion').removeClass('active');

                    $('#<%=txtMinimumQty.ClientID %>').select();

                });
                $.alt('A', function () {



                    $('#liActivationSettings').addClass('active');
                    $('#ActivationSettings').addClass('active');

                    $('#liInventoryPricing').removeClass('active');
                    $('#InventoryPricing').removeClass('active');

                    $('#liBreakPrice').removeClass('active');
                    $('#BreakPrice').removeClass('active');

                    $('#liStockPurchase').removeClass('active');
                    $('#StockPurchase').removeClass('active');

                    $('#liSales').removeClass('active');
                    $('#Sales').removeClass('active');

                    $('#liChildInventory').removeClass('active');
                    $('#ChildInventory').removeClass('active');

                    $('#liImage').removeClass('active');
                    $('#Image').removeClass('active');

                    $('#liPromotion').removeClass('active');
                    $('#Promotion').removeClass('active');

                    $('#<%=chkExpiryDate.ClientID %>').focus();
                });
                $.alt('T', function () {

                    if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {

                //$('#<%=chkExpiryDate.ClientID %>').focus();

                        $('#liStockPurchase').addClass('active');
                        $('#StockPurchase').addClass('active');

                        $('#liInventoryPricing').removeClass('active');
                        $('#InventoryPricing').removeClass('active');

                        $('#liBreakPrice').removeClass('active');
                        $('#BreakPrice').removeClass('active');

                        $('#liActivationSettings').removeClass('active');
                        $('#ActivationSettings').removeClass('active');

                        $('#liSales').removeClass('active');
                        $('#Sales').removeClass('active');

                        $('#liChildInventory').removeClass('active');
                        $('#ChildInventory').removeClass('active');

                        $('#liImage').removeClass('active');
                        $('#Image').removeClass('active');

                        $('#liPromotion').removeClass('active');
                        $('#Promotion').removeClass('active');
                    }
                });
                $.alt('S', function () {
                    if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {



                        $('#liSales').addClass('active');
                        $('#Sales').addClass('active');

                        $('#liInventoryPricing').removeClass('active');
                        $('#InventoryPricing').removeClass('active');

                        $('#liBreakPrice').removeClass('active');
                        $('#BreakPrice').removeClass('active');

                        $('#liActivationSettings').removeClass('active');
                        $('#ActivationSettings').removeClass('active');

                        $('#liStockPurchase').removeClass('active');
                        $('#StockPurchase').removeClass('active');

                        $('#liChildInventory').removeClass('active');
                        $('#ChildInventory').removeClass('active');

                        $('#liImage').removeClass('active');
                        $('#Image').removeClass('active');

                        $('#liPromotion').removeClass('active');
                        $('#Promotion').removeClass('active');

                        $('#<%=rbnSalesMonth.ClientID %>').focus();                        
                    }
                });
                $.alt('C', function () {
                    if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {

                //$('#<%=rbnSalesMonth.ClientID %>').focus();                  

                        $('#liChildInventory').addClass('active');
                        $('#ChildInventory').addClass('active');

                        $('#liInventoryPricing').removeClass('active');
                        $('#InventoryPricing').removeClass('active');

                        $('#liBreakPrice').removeClass('active');
                        $('#BreakPrice').removeClass('active');

                        $('#liActivationSettings').removeClass('active');
                        $('#ActivationSettings').removeClass('active');

                        $('#liStockPurchase').removeClass('active');
                        $('#StockPurchase').removeClass('active');

                        $('#liSales').removeClass('active');
                        $('#Sales').removeClass('active');

                        $('#liImage').removeClass('active');
                        $('#Image').removeClass('active');

                        $('#liPromotion').removeClass('active');
                        $('#Promotion').removeClass('active');
                    }
                });
                $.alt('M', function () {


                    $('#liImage').addClass('active');
                    $('#Image').addClass('active');

                    $('#liInventoryPricing').removeClass('active');
                    $('#InventoryPricing').removeClass('active');

                    $('#liBreakPrice').removeClass('active');
                    $('#BreakPrice').removeClass('active');

                    $('#liActivationSettings').removeClass('active');
                    $('#ActivationSettings').removeClass('active');

                    $('#liStockPurchase').removeClass('active');
                    $('#StockPurchase').removeClass('active');

                    $('#liSales').removeClass('active');
                    $('#Sales').removeClass('active');

                    $('#liChildInventory').removeClass('active');
                    $('#ChildInventory').removeClass('active');

                    $('#liPromotion').removeClass('active');
                    $('#Promotion').removeClass('active');

                    $('#<%=fuimage.ClientID %>').focus();
                });
                $.alt('P', function () {
                    if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {
                        $('#liPromotion').addClass('active');
                        $('#Promotion').addClass('active');

                        $('#liInventoryPricing').removeClass('active');
                        $('#InventoryPricing').removeClass('active');

                        $('#liBreakPrice').removeClass('active');
                        $('#BreakPrice').removeClass('active');

                        $('#liActivationSettings').removeClass('active');
                        $('#ActivationSettings').removeClass('active');

                        $('#liStockPurchase').removeClass('active');
                        $('#StockPurchase').removeClass('active');

                        $('#liSales').removeClass('active');
                        $('#Sales').removeClass('active');

                        $('#liChildInventory').removeClass('active');
                        $('#ChildInventory').removeClass('active');

                        $('#liImage').removeClass('active');
                        $('#Image').removeClass('active');
                    }
                });

                shortcut.add("Alt+L", function () {

                    if ($("#<%=hidLanguageChange.ClientID%>").val() == "N") {
                        $("#<%=hidLanguageChange.ClientID%>").val("Y");
                        $("#<%=txtOtherLanguageName.ClientID%>").css("display", "block");
                        $("#<%=txtOtherLanguageSName.ClientID%>").css("display", "block");
                        $("#<%=txtItemName.ClientID%>").css("display", "none");
                        $("#<%=txtShortName.ClientID%>").css("display", "none");
                        $("#<%=txtOtherLanguageName.ClientID%>").select();
                    }
                    else {
                        $("#<%=hidLanguageChange.ClientID%>").val("N");
                        $("#<%=txtOtherLanguageName.ClientID%>").css("display", "none");
                        $("#<%=txtOtherLanguageSName.ClientID%>").css("display", "none");
                        $("#<%=txtItemName.ClientID%>").css("display", "block");
                        $("#<%=txtShortName.ClientID%>").css("display", "block");
                        $("#<%=txtItemName.ClientID%>").select();
                    }





                });



            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        ///Get value for GA and GRN Report
        function fncChildRowClick(source) {
            try {
                $("#divchildSales").css("display", "block");
                $("#<%=hidchildItem.ClientID%>").val($.trim($(source).find('td span[id*="lblChildInventory"]').text()));

                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "white");
                });

                fncChildSaleshistorycheckchanged();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        /// Break price Key down function
        function isNumberKeyWithDecimalNewforBreakprice(evt) {
            try {

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 123 && $('#<%=chkBatch.ClientID%>').prop("checked")) {
                    fncGetBatchDetail_Master($('#<%=txtBreakPriceItem.ClientID%>').val());
                    return false;
                }
                else if ((charCode > 95 && charCode < 106) || charCode == 37 || charCode == 39 || charCode == 190) {
                    return true;
                    evt.preventDefault();
                }
                else if ((charCode != 46 && charCode > 31)
                    && (charCode < 48 || charCode > 57) && charCode != 110) {
                    return false;
                    evt.preventDefault();
                }

                return true;
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncDupNameYesClick() {
            try {
                $("#dialog-DuplicateInventoryName").dialog("destroy");
                $("#<%=btnInventorySave.ClientID %>").click();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncDupNameNoClick() {
            try {
                $("#dialog-DuplicateInventoryName").dialog("destroy");
                $('#<%=txtItemName.ClientID %>').select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncNewItemYesClick() {
            try {
                fncInitialLoad();
                $("#NewItemSave").dialog("destroy");
                $('#<%=txtItemName.ClientID %>').select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncNewItemNoClick() {
            try {
                $("#NewItemSave").dialog("destroy");
                __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                //window.location.href = "frmInventoryMasterView.aspx";
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncInventoryStatus(inventoryStatus) {
            try {
                if (inventoryStatus == "InvalidInventory") {
                    $('#<%=txtItemCode.ClientID %>').removeAttr("readonly");
            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                    //ShowPopupMessageBoxandFocustoObject("Invalid inventorycode");
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                    setTimeout(function () {
                        $('#<%=txtItemCode.ClientID %>').select();
                    }, 50);
                }
                else {
                    fncInitialLoad();
                    $('#<%=txtItemName.ClientID %>').select();
                }



            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// block Special Charactor
        function fncBlockSpecialChar(evt) {
            //alert('test');
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 219 || charCode == 221)
                    return false;
                else if (charCode == 188 || charCode == 190)
                    return false;

            }

            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        // barcode key down event
        function fncBarcodeKeyDown(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    fncAddBarcode();
                    return false;
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncImageButtonkeydown(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    fncOpenInvPriceChange();
                    fncDisableEXGST();
                    fncMDownOnMRP();

                    return false;
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncCheckBoxKeyDown(evt, value) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (value == "chkExpiryDate") {
                    if (charCode == 40) {
                        $('#<%=chkSerialNo.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkSerialNo") {
                    if (charCode == 40) {
                        $('#<%=chkAutoPO.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkExpiryDate.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkAutoPO") {
                    if (charCode == 40) {
                        $('#<%=chkPackage.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkSerialNo.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkPackage") {
                    if (charCode == 40) {
                        $('#<%=chkLoyality.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkAutoPO.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkLoyality") {
                    if (charCode == 40) {
                        $('#<%=chkSpecial.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkPackage.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkSpecial") {
                    if (charCode == 40) {
                        $('#<%=chkMemDis.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkLoyality.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkMemDis") {
                    if (charCode == 40) {
                        $('#<%=chkSeasonal.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkSpecial.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkSeasonal") {
                    if (charCode == 40) {
                        if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                            $('#<%=chkShelf.ClientID %>').focus();
                        }
                        else {
                   // $('#<%=chkBatch.ClientID %>').focus();
                        }
                    }
                    else if (charCode == 38) {
                        $('#<%=chkMemDis.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkBatch") {
                    if (charCode == 40 || charCode == 13) {
                        $('#<%=txtStyle.ClientID %>').focus();
                        return false;
                    }
                    else if (charCode == 38) {
                        $('#<%=chkSeasonal.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkShelf") {
                    if (charCode == 40) {
                        $('#<%=chkHideData.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                            $('#<%=chkSeasonal.ClientID %>').focus();
                }
                else {
            //$('#<%=chkBatch.ClientID %>').focus();
                        }
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkHideData") {
                    if (charCode == 40) {
                        $('#<%=chkMarking.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkShelf.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkMarking") {
                    if (charCode == 40) {
                        $('#<%=chkBarcodePrint.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkHideData.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkBarcodePrint") {
                    if (charCode == 40) {
                        $('#<%=chkAllowCost.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkMarking.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkAllowCost") {
                    if (charCode == 40) {
                        if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                            $('#<%=txtExpiryDays.ClientID %>').select();
                        }
                        else {
                            $('#<%=cbWeightbased.ClientID %>').focus();
                        }

                    }
                    else if (charCode == 38) {
                        $('#<%=chkBarcodePrint.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "cbWeightbased") {
                    if (charCode == 40) {
                        $('#<%=txtExpiryDays.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkAllowCost.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }              

                else if (value == "txtExpiryDays") {
                    if (charCode == 13) {
                        $('#<%=txtRating.ClientID %>').select();
                        return false;
                    }

                }
                else if (value == "txtRating") {
                    if (charCode == 13) {
                        $('#<%=txtShelfQty.ClientID %>').select();
                        return false;
                    }
                }
                else if (value == "txtShelfQty") {
                    if (charCode == 13) {
                        $('#<%=chkSelectAll.ClientID %>').focus();
                        return false;
                    }
                }
                else if (value == "chkSelectAll") {
                    if (charCode == 40) {
                        $('#<%=chkPurchaseOrder.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=txtShelfQty.ClientID %>').select();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkPurchaseOrder") {
                    if (charCode == 40) {
                        $('#<%=chkGRN.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkSelectAll.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkGRN") {
                    if (charCode == 40) {
                        $('#<%=chkPurchaseReturn.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkPurchaseOrder.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkPurchaseReturn") {
                    if (charCode == 40) {
                        $('#<%=chkPointOfSale.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkGRN.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkPointOfSale") {
                    if (charCode == 40) {
                        $('#<%=chkAllowNeg.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkPurchaseReturn.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkSalesReturn") {
                    if (charCode == 40) {
                        $('#<%=chkOrderBooking.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkPointOfSale.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkOrderBooking") {
                    if (charCode == 40) {
                        $('#<%=chkEstimate.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkSalesReturn.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkEstimate") {
                    if (charCode == 40) {
                        $('#<%=chkQuatation.ClientID %>').focus();
                    }
                    else if (charCode == 38) {
                        $('#<%=chkOrderBooking.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkQuatation") {
                    if (charCode == 40) {
                        $('#<%=chkSelectAllRight.ClientID %>').focus();
                    }
                    if (charCode == 38) {
                        $('#<%=chkEstimate.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkSelectAllRight") {
                    if (charCode == 38) {
                        $('#<%=chkQuatation.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
                else if (value == "chkAllowNegative") {
                    if (charCode == 40) {
                        $('#<%=chkSalesReturn.ClientID %>').focus();
                    }
                    if (charCode == 38) {
                        $('#<%=chkPointOfSale.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncOrderByChanged() {
            try {
                if ($('#<%=ddlOrderBy.ClientID %>').val() == "2") {
                    $('#divcycledays').css("display", "block");
                }
                else {
                    $('#divcycledays').css("display", "none");
                }

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Set Focus to Object on Choosen Enter key press
        $(document).ready(function () {
            fncDropdownEnterKeyVal();

            if ($('#<%=hidInvStatus.ClientID %>').val() == "InvalidInventory") {
                fncToastInformation("Invalid Inventorycode");
            }

        });
        $(document).ready(function () {
            $("#ContentPlaceHolder1_txtStyle").bind('keydown', function (e) {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode === 13) {
                    if ($('#<%=hdMultipleUOM.ClientID %>').val() == 'N') {
                        $('#<%=txtPackage.ClientID %>').select();
                    }
                    else {
                        $('#<%=txtSize.ClientID %>').select();
                    }

                }
            });

        });
        function fncDropdownEnterKeyVal() {
            try {
        <%--$("#ContentPlaceHolder1_txtStockType").bind('keydown', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13 || keyCode === 9) {
                alert('ff');
                $('#<%=txtStyle.ClientID %>').select();
            }
        });--%>
        <%--$("#ContentPlaceHolder1_txtSalesUOM").bind('keydown', function (e) {
            
               
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode === 13 || keyCode === 9) {
                    if ($('#<%=txtSalesUOM.ClientID %>').val() == "") {
                        //ControlID = 'ddlStockType_chzn';$('#<%=txtSalesUOM.ClientID %>').val() == "" && $('#ContentPlaceHolder1_txtSearch').val().length() > 2
                        setTimeout(function () {
                            //$('#ContentPlaceHolder1_ddlStockType_chzn').focus();
                            $('#<%=ddlStockType.ClientID %>').trigger("liszt:open");
                            $('#<%=ddlStockType.ClientID %>').trigger("chosen:updated");
                        }, 150);
                    }
                    //else {
                    //    ControlID = 'txtSearch';
                    //}
                        
            }
        });--%>


            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        /// Stock Type Cha
        function fncStockTypeChange() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() == "Liquor" || $('#<%=txtStockType.ClientID %>').val() == "Non Liquor") {


                    if ($('#<%=hidPriceEntered.ClientID %>').val() == "1" || $('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                        $('#<%=hidPriceEntered.ClientID %>').val('0');
                        fncRemoveDisableAttForInputGST();
                        $("#dialog-InvPrice").dialog("open");
                        fncOpenDialogWithDetail();
                        fncToastInformation("You changed Stock type.Please Re-Enter Price Detail");
                    }
                    else {

                        setTimeout(function () {
                            $('#<%=txtStyle.ClientID %>').select();
                }, 150);
                    }

                }
                //$('#ContentPlaceHolder1_txtStyle').focus().select();
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncNetCostCalonAddCessAmtChange() {
            //var netCost=0, AddCessAmt = 0;
            try {
                fncCalcSelingPice();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncShowMultipUOM() {
            try {

                $("#divMultipleUOM").dialog({
                    resizable: false,
                    height: "auto",
                    width: 552,
                    modal: true,
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }



        function fncBindMultipleUOM() {
            var uomObj, row, rowNo = 0;
            var mulUOMBody;
            try {
                uomObj = jQuery.parseJSON($('#<%=hidMultipleUOM.ClientID %>').val());
                mulUOMBody = $("#tblMultipleUOM tbody");
                mulUOMBody.children().remove();

                for (var i = 0; i < uomObj.length; i++) {
                    rowNo = parseInt(rowNo) + 1;
                    row = "<tr>"
                        + "<td id='tdRowNo' >" + rowNo + "</td>"
                        + "<td id='tdUOMCode' >" + uomObj[i]["Code"] + "</td>"
                        + "<td id='tdConvQty' >" + parseFloat(uomObj[i]["UomConv"]).toFixed(2) + "</td>"
                        + "<td id='tdBin' ><input id='cbBin_" + rowNo + "' type='checkbox'  /></td>"
                        + "<td id='tdBin' ><input id='cbActive_" + rowNo + "' type='checkbox'  /></td>"
                        + "</tr>";
                    mulUOMBody.append(row);

                    if (uomObj[i]["Bin"] == "1") {
                        $("#cbBin_" + rowNo + "").prop("checked", true);
                    }
                    if (uomObj[i]["Active"] == "1") {
                        $("#cbActive_" + rowNo + "").prop("checked", true);
                    }
                }



            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncAddMultipleUOM() {
            var uomObj, row, rowNo = 0;
            var mulUOMBody, bin = "N";
            var status = true;
            try {

                if ($('#<%=txtMultipleUOM.ClientID %>').val() == "") {
                    ShowPopupMessageBox("Please Enter UOM Code");
                    return false;
                }
                else if ($('#<%=txtConvQty.ClientID %>').val() == "" || parseFloat($('#<%=txtConvQty.ClientID %>').val()) == 0) {
                    ShowPopupMessageBox("Please Enter valid UOM code");
                    return false;
                }
                mulUOMBody = $("#tblMultipleUOM tbody");


                mulUOMBody.children().each(function () {
                    if ($(this).find('td[id*="tdUOMCode"]').text().trim() == $('#<%=txtMultipleUOM.ClientID %>').val().trim() &&
                        parseFloat($(this).find('td[id*="tdConvQty"]').text()) == parseFloat($('#<%=txtConvQty.ClientID %>').val())) {
                        fncToastInformation("This UOM Aready Add");
                        status = false;
                        return false;
                    }
                });

                if (status == false)
                    return false;

                rowNo = mulUOMBody.children().length;
                rowNo = parseInt(rowNo) + 1;

                row = "<tr tabindex='" + rowNo + "' onkeydown='return fncDeleteMultipleUOM(this,event);' >"
                    + "<td id='tdRowNo_" + rowNo + " ' >" + rowNo + "</td>"
                    + "<td id='tdUOMCode_" + rowNo + "' >" + $('#<%=txtMultipleUOM.ClientID %>').val() + "</td>"
                    + "<td id='tdConvQty_" + rowNo + "' >" + parseFloat($('#<%=txtConvQty.ClientID %>').val()).toFixed(2) + "</td>"
                    + "<td id='tdBin' ><input id='cbBin_" + rowNo + "' type='checkbox'  /></td>"
                    + "<td id='tdActive' ><input id='cbActive_" + rowNo + "' type='checkbox' checked='checked'  /></td>"
                    + "</tr>";
                mulUOMBody.append(row);
                if ($('#<%=cbUOMBin.ClientID %>').is(":checked")) {
                    $("#cbBin_" + rowNo + "").prop("checked", true);
                }
                $("#cbActive_" + rowNo + "").attr("disabled", "disabled");

                fncUOMClear();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUOMClear() {
            try {
                $('#<%=txtMultipleUOM.ClientID %>').val('');
                $('#<%=txtConvQty.ClientID %>').val('');
                $('#<%=cbUOMBin.ClientID %>').prop("checked", false);
                $('#<%=txtMultipleUOM.ClientID %>').select();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDeleteMultipleUOM(source, event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 46) {
                    $(source).remove();
                    return false;
                }
                else {
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSelectAndUnSelectLocation() {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        
        // To Bind Transfer Inventory Detail 
        function fncBindTransferDetail() {
            var tranObj, tranBody, row;
            try {
                tranObj = jQuery.parseJSON($('#<%=hidTransferDetail.ClientID %>').val());
                tranBody = $("#tblTransferDet tbody");
                tranBody.children().remove();

                if (tranObj != null) {
                    for (var i = 0; i < tranObj.length; i++) {
                        row = "<tr>"
                            + "<td id='tdRowNo' >" + tranObj[i]["RowNo"] + "</td>"
                            + "<td id='tdTranNo' ><a onclick='fncGoTransferPage(this);return false;' id='lnkTranDet' class='pointer'  >" + tranObj[i]["TransferNo"] + "</a></td>"
                            + "<td id='tdTransferDate' >" + tranObj[i]["TransferDate"] + "</td>"
                            + "<td id='tdToLocation' >" + tranObj[i]["ToLocation"] + "</td>"
                            + "<td id='tdFromLocation' >" + tranObj[i]["FromLocation"] + "</td>"
                            + "<td id='tdQty' >" + parseFloat(tranObj[i]["Qty"]).toFixed(2) + "</td>"
                            + "<td id='tdStatus' >" + tranObj[i]["Status"] + "</td>"
                            + "</tr>";
                        tranBody.append(row);
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncShowTransferDetail() {
            $("#divTransferDet").dialog({
                title: "Transfer Detail",
                modal: true,
                width: "auto"
            });
        };               
        
        function fncGoTransferPage(obj) {
            var rowObj;
            try {
                rowObj = $(obj).parent().parent();
                var page = '<%=ResolveUrl("~/Transfer/frmTransferOutAdd.aspx") %>';
                page = page + "?mode=InvView&TransferNo=" + $(obj).text() + "&transdate=" + rowObj.find('td[id*=tdTransferDate]').text();
                $dialog = $('<div id="popupTransfer"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "View Transfer Out"
                });
                $dialog.dialog('open');
                return false;

                //alert($(obj).text());
                //obj = $(obj).parent().parent();
                //alert(obj.find())
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /*jayanthi*/
        function fncBindPriceChangeLog() {
            var priceObj, priceBody, row;
            try {
                priceObj = jQuery.parseJSON($('#<%=hidPriceChangeLog.ClientID %>').val());
                priceBody = $("#tblPriceChgLog tbody");
                priceBody.children().remove();

                if (priceObj != null) {
                    for (var i = 0; i < priceObj.length; i++) {
                        row = "<tr>"
                            + "<td id='tdBatchNo' style='width:100px;'>" + priceObj[i]["BatchNo"] + "</td>"
                            + "<td id='tdModifyDate' style='width:100px; text-align:center !important;' >" + priceObj[i]["ModifyDate"] + "</td>"
                            + "<td id='tdOldPrice' style='width:100px; text-align:right !important;' >" + priceObj[i]["OldPrice"].toFixed(2) + "</td>"
                            + "<td id='tdNewPrice' style='width:100px; text-align:right !important;' >" + priceObj[i]["NewPrice"].toFixed(2) + "</td>"
                            + "<td id='tdModifyUser' style='width:125px; text-align:right !important;' >" + priceObj[i]["ModifyUser"] + "</td>"
                            + "</tr>";
                        priceBody.append(row);
                    }
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncShowPriceChangeLog() {
            $("#divPriceChgLog").dialog({
                title: "Price Change Log",
                modal: true,
                width: "auto"
            });
        };
        
        function fncShowInventoryMovement() {
            try {
                var page = '<%=ResolveUrl("~/Managements/frmInventoryMovement.aspx") %>';
                page = page + "?Itemcode=" + $('#<%=txtItemCode.ClientID%>').val();
                $dialog = $('<div id="popupInvMovement"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Inventory Movement"
                });
                $dialog.dialog('open');
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //function fncOpenRRReq(xmlDoc) {   
        //    var row;
        //    try {
        //        var xml = $(xmlDoc);
        //        var DishSearch = xml.find("Table1");
        //        if (row == null) {
        //            row = $("[id*=gvPRReq] tr:last-child").clone(true);
        //        }
        //        $("[id*=gvPRReq] tr").not($("[id*=gvPRReq] tr:first-child")).remove();
        //        if (DishSearch.length > 0) {
        //            $.each(DishSearch, function () {                   

        //                $("td", row).eq(0).html($(this).find("PRReqNo").text());
        //                $("td", row).eq(1).html($(this).find("ReqDate").text());
        //                $("td", row).eq(2).html($(this).find("LocationCode").text());
        //                $("td", row).eq(3).html($(this).find("BatchNo").text());
        //                $("td", row).eq(4).html($(this).find("PRReqQty").text());
        //                $("[id*=gvPRReq]").append(row);
        //                row = $("[id*=gvPRReq] tr:last-child").clone(true);
        //            });
        //        }

        //        fncOpenPRRequest();

        //    }
        //    catch (err) {
        //        fncToastError(err.message);
        //    }
        //}

       function fncGetBatchDetAndPRReqDetail(mode) {  //jayanthi 28072022
            
            try {
                var obj = {};
                obj.inventorycode = $('#<%=txtItemCode.ClientID%>').val();
                obj.mode = mode;
                $.ajax({
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncGetPRReqDet") %>',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",                   
                    
                    success: function (msg) {
                        if (mode == 7) {
                            fncAssignValuestoTable(msg);
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });                

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
                   
        function fncAssignValuestoTable(msg) {
            try {
                objStk = msg.d;                
                var array = {};
                $("#tblOpenPRReq tbody").children().remove();                                                      
                for (var i = 0; i < objStk.length; i++) {
                    array = objStk[i].split('|');
                    row = "<tr>"
                        + "<td id='tdPRReqNo' style='width:100px; text-align:right !important;'>" + array[0] + "</td>"
                        + "<td id='tdReqDate' style='width:115px; text-align:right !important;' >" + array[1] + "</td>"
                        + "<td id='tdLocationCode' style='width:100px; text-align:right !important;' >" + array[2] + "</td>"
                        + "<td id='tdBatchNo' style='width:100px; text-align:right !important;' >" + array[3] + "</td>"
                        + "<td id='tdPRReqQty' style='width:125px; text-align:right !important;' >" + array[4] + "</td>"
                        + "</tr>";
                    $("#tblOpenPRReq tbody").append(row);                                     
                }
               fncShowOpenPRReq();                
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }        

        function fncShowOpenPRReq() {
            $("#divOpenPRReq").dialog({
                title: "Purchase Return Request",
                modal: true,
                width: "auto"
            });
        };


        function fncOpenBatchDet(xmlDoc) {
            var row;
            try {
                var xml = $(xmlDoc);
                var DishSearch = xml.find("Table1");
                if (row == null) {
                    row = $("[id*=gvBatchPopup] tr:last-child").clone(true);
                }
                $("[id*=gvBatchPopup] tr").not($("[id*=gvBatchPopup] tr:first-child")).remove();
                if (DishSearch.length > 0) {
                    $.each(DishSearch, function () {
                        $("td", row).eq(0).html($(this).find("BatchNo").text());
                        $("td", row).eq(1).html($(this).find("BalanceQty").text());
                        $("td", row).eq(2).html($(this).find("InwardQty").text());
                        $("td", row).eq(3).html($(this).find("SoldQty").text());
                        $("td", row).eq(4).html($(this).find("MRP").text());
                        $("td", row).eq(5).html($(this).find("SellingPrice").text());
                        $("td", row).eq(6).html($(this).find("CreateDate").text());
                        $("[id*=gvBatchPopup]").append(row);
                        row = $("[id*=gvBatchPopup] tr:last-child").clone(true);
                    });
                }
                fncOpenBatchDetail();

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

       function fncGetBatchDetAndPRReqDet(mode) {   
            var obj = {};
            try {
                obj.inventorycode = $('#<%=txtItemCode.ClientID%>').val();
                obj.mode = mode;
                $.ajax({
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncGetBatchAndPRReqDet") %>',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (mode == 6) {
                            fncOpenBatchDet($.parseXML(data.d));
                        }
                        //else if (mode == 7) {
                        //    fncOpenRRReq($.parseXML(data.d));
                        //}
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "NewVendor") {
                    fncAddNewVendor($.trim(Code), $.trim(Description));

                }
                if (SearchTableName == "Vendor") {
                    $('#<%=txtVendor.ClientID %>').val(Description);
                    $('#<%=hidVendorcode.ClientID %>').val(Code);
                    $('#<%=hidVendorName.ClientID %>').val(Description);
                }
                if (SearchTableName == "Brand" && $(<%=hidinventoryStatus.ClientID%>).val() != "Edit") {
                    $('#<%=txtItemName.ClientID%>').val($('#<%=txtBrand.ClientID%>').val() + ' ' + $('#<%=txtInvcategory.ClientID%>').val());
                }
                if (SearchTableName == "category") {
                    fncGetDepartmentForSelecetedCategory(Description);
                }
                if (SearchTableName == "Package") {
                    $('#<%=txtPackage.ClientID %>').val(Code);
                }
                if (SearchTableName == "WineShop") {
                    $('#<%=hdStoctTypeVal.ClientID %>').val(Code);
                    $('#<%=txtStockType.ClientID %>').val(Description);
                    fncStockTypeChange();
                    //$('#ContentPlaceHolder1_txtStyle').focus().select();
                }
                if (SearchTableName == "Store") {
                    $('#<%=hdStoctTypeVal.ClientID %>').val(Code);
                    $('#<%=txtStockType.ClientID %>').val(Description);
                    //fncStockTypeChange();
                    //$('#ContentPlaceHolder1_txtStyle').focus().select();
                }
        <%--if (SearchTableName == "UOM") {
          if ($('#ContentPlaceHolder1_txtSalesUOM').val()!="") {
               setTimeout(function () {hdStoctTypeVal
             $('#<%=txtStockType.ClientID %>').trigger("liszt:open");
                    $('#<%=txtStockType.ClientID %>').val("chosen:updated");
                }, 10);
                //SetFocusID = 'ddlStockType_chzn';
            }
           
        }--%>
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemName.ClientID %>').val(Description);

           <%-- 
           var ItemName = $('#<%=txtItemName.ClientID%>').val();
            if ($('#<%=hidinventoryStatus.ClientID %>').val() == "New") {
                $('#<%=txtShortName.ClientID%>').val(ItemName.substring(0, 20));
        
            }--%>
                    if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                        $("[id$=txtItemCode]").val(Code);
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkEditItemCode', '')
                    }
                }
                $('#<%=txtItemName.ClientID %>').focusout(function () {
                    var ItemName = $('#<%=txtItemName.ClientID%>').val();
                    if ($('#<%=hidinventoryStatus.ClientID %>').val() == "New") {
                        $('#<%=txtShortName.ClientID%>').val(ItemName.substring(0, 20));
                    }
                });

                if (SearchTableName == "Department") {
                    if ($('#<%=hidInventoryRange.ClientID %>').val() == "Y") {
                        fncDepatrtmentWiseitemcode();
                    }
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncGetDepartmentForSelecetedCategory(CategoryCode) {

            try {
                var obj = {};
                obj.CategoryCode = CategoryCode.trim().replace("'", "''");
                //$data = { prefix: CategoryCode.replace("'", "''") };
                $.ajax({
                    type: "POST",
                    url: "frmInventoryMaster.aspx/GetDeptCode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $('#<%=txtDepartment.ClientID %>').val(msg.d);
                        //console.log($('#<%=txtDepartment.ClientID %>').val());
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message)
                    }
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function checkPhoneKey(event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            var StockType = $('#<%=hdStoctType.ClientID %>').val();
            if (keyCode == 9) {
                $('#ContentPlaceHolder1_txtStyle').focus().select();
            }
            if (StockType == "WineShop") {
                fncShowSearchDialogCommon(event, 'WineShop', 'txtStockType', 'txtStyle');
            }
            else {
                var Focus = 'chkBatch';
                if ($('#<%=chkBatch.ClientID %>').is(':disabled'))
                    Focus = 'txtStyle'
                fncShowSearchDialogCommon(event, 'Store', 'txtStockType', Focus);
                //$("#dialog-InvPrice").dialog("close");
                //$('#ContentPlaceHolder1_txtStyle').focus().select();
            }
            //$('#ContentPlaceHolder1_txtStyle').focus().select();
            return false;
        }
        //--------------------------------------------------Item Name Validation-----------------------------------
        //$(document).ready(function () {
        //    $("input[type=text]").keyup(function () {
        //        $(this).val($(this).val().toUpperCase());
        //    });
        //});

        function fncValidateItemName() {
            try {
                if ($('#<%=hidinventoryStatus.ClientID%>').val() == 'Edit' && $('#<%=txtItemName.ClientID%>').val().toUpperCase().trim() == $('#<%=txtBreakPriceDesc.ClientID%>').val().toUpperCase().trim())
                    return false;

                $('#<%=txtItemName.ClientID %>').val($('#<%=txtItemName.ClientID %>').val().toUpperCase());
                var obj = {}, objdata;
                obj.ItemName = $('#<%=txtItemName.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmInventoryMaster.aspx/fncGetInventoryDetailForValidation")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var Itemname1 = $('#<%=txtItemName.ClientID %>').val().trim();
                        objdata = jQuery.parseJSON(msg.d);
                        if (objdata.Table.length > 0) {
                            var Itemname2;
                            Itemname2 = objdata.Table[0]["Description"];
                            if (Itemname1 == Itemname2) {
                                $('#<%=txtItemName.ClientID %>').select().focus();
                        fncToastError('This Inventory Name Already Exists !!');
                        $('#<%=txtItemName.ClientID %>').css("border", "1px solid red");
                        $('#<%=txtItemName.ClientID %>').select().focus();

                    }
                    else
                        $('#<%=txtShortName.ClientID%>').val(Itemname1.substring(0, 20));
                }
                else {

                    $('#<%=txtShortName.ClientID%>').val(Itemname1.substring(0, 20));
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid black");
                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });


            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        //----------------------------------------------------HSN Code Validation------------------------------------------------------
        $("#divSellingPriceChangeValidation").dialog({
            open: function () {
                $(this).keydown(function (event) {
                    if (event.keyCode == 13) {
                        $("#lnkbtnPriceChangeOk").trigger("click");
                        return false;
                    }
                });
            },
        });
        $('#divSellingPriceChangeValidation').dialog
            ({
                open: function () {
                    $(this).find('select, input, textarea').first().blur();
                }
            });
        function fncInitializeHSNvalidation(Msg) {
            try { 
                $('#<%=lnkbtnPriceChangeOk.ClientID %>').select();
                $('#<%=lblSellingPriceChangeValidation.ClientID %>').text(Msg);
                $("#divSellingPriceChangeValidation").dialog({
                    resizable: false,
                    height: 130,
                    width: 325,
                    modal: true,
                    title: "Enterpriser Web"
                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function HSNValidationClose() {
            $("#divSellingPriceChangeValidation").dialog('close');
            $('#' + FocusCtrl).focus().select();
        }
        var FocusCtrl;
        function fncHSNcodevalidation() {

            try {

                var HSNCode_len = $('#<%=txtHSNCode.ClientID%>').val().length;
                HSNCode_len = parseFloat(HSNCode_len);
                if (HSNCode_len != 0 && HSNCode_len != 2 && HSNCode_len != 4 && HSNCode_len != 6 && HSNCode_len != 8) {
                    $('#<%=txtHSNCode.ClientID %>').select().focus();

                    FocusCtrl = 'txtHSNCode.ClientID';
                    fncInitializeHSNvalidation('Enter Valid HSN Code(2 or 4 or 8 Digits Length or Empty)');
                <%--ShowValidateMessageBox('Enter Valid HSN Code(2 or 4 or 8 Digits Length or Empty)');
                        $('#<%=txtHSNCode.ClientID %>').css("border", "1px solid red");
                        $('#<%=txtHSNCode.ClientID %>').select().focus();--%>
                }
                else {
                    $('#<%=txtHSNCode.ClientID %>').css("border", "1px solid black");

                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncMarkup_Netcost_Calculation() {
            var BasicCost = parseFloat(txtBasicCost.val()).toFixed(2);
            if (parseFloat(BasicCost) > 0) {
                var TotDiscAmt;
                var GrsossCostNew;
                var DiscountedBasicCost;
            

                    var DiscAmt1 = (BasicCost * parseFloat(txtDiscountPrc1.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = DiscAmt1;
                    DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(DiscAmt1)).toFixed(2)
                    var DiscAmt2 = (DiscountedBasicCost * parseFloat(txtDiscountPrc2.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                    DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                    var DiscAmt3 = (DiscountedBasicCost * parseFloat(txtDiscountPrc3.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);
                
               
                var GrossCost = (parseFloat(txtBasicCost.val()).toFixed(2) - TotDiscAmt).toFixed(2);
                 GrsossCostNew = parseFloat(txtBasicCost.val()).toFixed(2);

                var CGSTAmt = (GrossCost * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                var CGSTAmtnew = (GrsossCostNew * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                var SGSTAmt = CGSTAmt;
                var SGSTAmtnew = CGSTAmtnew;
                var CESSAmt = (GrossCost * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);
                var CESSAmtnew = (GrsossCostNew * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);

                var NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
                    parseFloat(CESSAmt) + parseFloat(txtAddCessAmt.val())).toFixed(2);

                
                    txtDiscountAmt.val(parseFloat(TotDiscAmt).toFixed(2));
                
              
                txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));

                var AffetctNetCost = 0;
                if ($('#<%=hidNewCalculation.ClientID %>').val() == 'Y' && $('#<%=rbtCost.ClientID %>').is(':checked')) {
                   <%-- if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked'))
                        AffetctNetCost = GrossCost;
                    else
                        AffetctNetCost = parseFloat(TotDiscAmt) + parseFloat(GrossCost);--%>
               
                        NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
                            parseFloat(CESSAmt) + parseFloat(txtAddCessAmt.val())).toFixed(2);
                   
                  
                    var NetCostnew = parseFloat(parseFloat(GrsossCostNew) + parseFloat(CGSTAmtnew) + parseFloat(SGSTAmtnew) +
                        parseFloat(CESSAmtnew) + parseFloat(txtAddCessAmt.val())).toFixed(2);
           
                    $('#<%=hidNetcostnew.ClientID %>').val(NetCostnew); //surya19112021 new changes Akshaya Discount
                    $('#<%=hidGrossCostNew.ClientID %>').val(GrsossCostNew);
                } 
                txtNetCost.val(parseFloat(NetCost).toFixed(2)); 
            }
        }

        function fncMarkup_Netcost_Calculation_New() {
            
            var GrsossCostNew;
            var Mrp = parseFloat(txtMRP.val()).toFixed(2);
            var BasicMargin = parseFloat(txtBasicMargin.val()).toFixed(2);
            if (parseFloat(Mrp) > 0) {
                var NetCost = parseFloat((parseFloat(Mrp)) / (1 + (parseFloat(BasicMargin) / 100))).toFixed(2);


                var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);

                var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat(txtAddCessAmt.val()).toFixed(2);
                var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)))).toFixed(2);

                var SGSTAmt;
                var CGSTAmt;
                var CESSAmt;
                if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                    SGSTAmt = 0;
                    CGSTAmt = 0;
                    CESSAmt = 0;
                }
                else {
                    SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) * parseFloat(SGST)).toFixed(2);
                    CGSTAmt = SGSTAmt;
                    CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                }

                var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                    GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                    CESSAmt = 0.00;
                }
                var BasicCost = parseFloat(GrossCost).toFixed(2);
                txtBasicCost.val(parseFloat(BasicCost).toFixed(2));
                
                $('#<%=hidOldNetCost.ClientID%>').val(parseFloat(NetCost).toFixed(2));

                if (parseFloat(txtDiscountPrc1.val()) > 0 || parseFloat(txtDiscountPrc2.val()) > 0 || parseFloat(txtDiscountPrc3.val()) > 0) {
                    var DiscAmt1 = (GrossCost * parseFloat(txtDiscountPrc1.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = DiscAmt1;
                    DiscountedBasicCost = (parseFloat(GrossCost) - parseFloat(DiscAmt1)).toFixed(2)
                    var DiscAmt2 = (DiscountedBasicCost * parseFloat(txtDiscountPrc2.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                    DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                    var DiscAmt3 = (DiscountedBasicCost * parseFloat(txtDiscountPrc3.val()).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                    GrossCost = (parseFloat(txtBasicCost.val()).toFixed(2) - TotDiscAmt).toFixed(2);
                    GrsossCostNew = parseFloat(txtBasicCost.val()).toFixed(2);

                    CGSTAmt = (GrossCost * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                    var CGSTAmtnew = (GrsossCostNew * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                    SGSTAmt = CGSTAmt;
                    var SGSTAmtnew = CGSTAmtnew;
                    CESSAmt = (GrossCost * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);
                    var CESSAmtnew = (GrsossCostNew * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);
                    var AffetctNetCost = 0;
                   <%-- if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked'))
                        AffetctNetCost = GrossCost;
                    else
                        AffetctNetCost = parseFloat(TotDiscAmt) + parseFloat(GrossCost);--%>

                    NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
                        parseFloat(CESSAmt) + parseFloat(txtAddCessAmt.val())).toFixed(2);
                    txtDiscountAmt.val(parseFloat(TotDiscAmt).toFixed(2));

                    var NetCostnew = parseFloat(parseFloat(GrsossCostNew) + parseFloat(CGSTAmtnew) + parseFloat(SGSTAmtnew) +
                        parseFloat(CESSAmtnew) + parseFloat(txtAddCessAmt.val())).toFixed(2);

                    $('#<%=hidNetcostnew.ClientID %>').val(NetCostnew); //surya19112021 new changes Akshaya Discount
                    $('#<%=hidGrossCostNew.ClientID %>').val(GrsossCostNew);
                }
                else {
                    txtDiscountAmt.val(parseFloat(0).toFixed(2));
                    $('#<%=hidNetcostnew.ClientID %>').val(NetCost); //surya19112021 new changes Akshaya Discount
                    $('#<%=hidGrossCostNew.ClientID %>').val(GrossCost);
                }

                txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                txtBasicCost.val(parseFloat(BasicCost).toFixed(2));//BasicCost==GrossCost
                txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                txtNetCost.val(parseFloat(NetCost).toFixed(2));

            }
        }

        function fncMRP2_NetSellingPrice_Calculation_New() {
            try { 
                if (parseFloat(txtMRP.val()) > 0  ) {
                    var NetSelling = 0;
                    var NeCost = 0;
                    if (($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false || parseFloat($('#<%=txtDiscountAmt.ClientID %>').val()) == 0) &&
                        parseFloat($('#<%=txtBasicMargin.ClientID %>').val()) > 0 && $(<%=ddlPurchase.ClientID%>).val() != "Mrp") {
                        AffetctNetCost = parseFloat((parseFloat(txtMRP.val())) / (1 + (parseFloat(txtBasicMargin.val()) / 100))).toFixed(2);
                        //parseFloat(AffetctNetCost) * parseFloat(txtFixedMargin.val()) / 100;
                    }
                    else if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false && $(<%=ddlPurchase.ClientID%>).val() != "Mrp"
                        && parseFloat($('#<%=txtDiscountAmt.ClientID %>').val()) > 0) {
                        AffetctNetCost = parseFloat($('#<%=hidNetcostnew.ClientID %>').val()); //+ parseFloat(txtDiscountAmt.val());
                        // NetSelling = parseFloat(parseFloat(txtGrossCost.val()) + parseFloat(txtGrossCost.val() * txtFixedMargin.val() / 100)).toFixed(2);
                    }
                    else if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == true && $(<%=ddlPurchase.ClientID%>).val() == "Mrp") {
                        AffetctNetCost = parseFloat(txtNetCost.val());
                    }
                    else if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false && $(<%=ddlPurchase.ClientID%>).val() == "Mrp" && parseFloat($('#<%=txtBasicMargin.ClientID %>').val()) > 0) {
                        AffetctNetCost = parseFloat(txtMRP.val() * (1 - (txtBasicMargin.val() / 100))).toFixed(2); 
                    }
                    else if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false && $(<%=ddlPurchase.ClientID%>).val() == "Mrp" && parseFloat($('#<%=txtBasicMargin.ClientID %>').val()) == 0) {
                        AffetctNetCost = parseFloat(txtBasicCost.val() * (1 - (txtBasicMargin.val() / 100))).toFixed(2);
                    }
                    else {
                        fncMRP2_NetSellingPrice_Calculation();
                        return false; 
                    }

                    NetSelling = parseFloat(parseFloat(AffetctNetCost) + parseFloat(AffetctNetCost * txtFixedMargin.val() / 100)).toFixed(2);

                    /*NeCost = AffetctNetCost;*/ <%--//$('#<%=hidOldsNetCost.ClientID%>').val();--%>

                    //

                   
                    //parseFloat(txtMRP.val()) / ((parseFloat(txtFixedMargin.val()) + parseFloat(100)) / 100);
                    NetSelling = parseFloat(parseFloat(NeCost) + parseFloat(NetSelling)).toFixed(2);

                    var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                    var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                    var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                    var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer) + parseFloat(OutputCESSPer);

                    var TotalTaxAmt = ((parseFloat(NetSelling) - parseFloat(AdlCessAmt)) * TotalPer / (parseFloat(100) + parseFloat(TotalPer))).toFixed(2);

                    var OutputCGSTAmt;
                    var OutputSGSTAmt;
                    var OutputCESSAmt;
                    if (TotalPer == 0) {
                        OutputCGSTAmt = 0;
                        OutputSGSTAmt = 0;
                        OutputCESSAmt = 0;
                    }
                    else {
                        OutputCGSTAmt = (TotalTaxAmt / TotalPer * OutputCGSTPer).toFixed(2);
                        OutputSGSTAmt = OutputCGSTAmt;
                        OutputCESSAmt = parseFloat(TotalTaxAmt) - parseFloat(OutputCGSTAmt) - parseFloat(OutputSGSTAmt);
                    } 
                    var BasicSelling = parseFloat(NetSelling) - parseFloat(AdlCessAmt) - parseFloat(TotalTaxAmt);

                    if (parseFloat(OutputCESSAmt).toFixed(2) == '-0.01' || parseFloat(OutputCESSAmt).toFixed(2) == '0.01') {
                        BasicSelling = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt);
                        OutputCESSAmt = 0.00;
                    }
                   

                        txtBasicSelling.val(BasicSelling);
                        txtOutputCGSTAmt.val(OutputCGSTAmt);
                        txtOutputSGSTAmt.val(OutputSGSTAmt);
                        txtOutputCESSAmt.val(OutputCESSAmt);
                        txtSellingPrice.val(NetSelling);
                    

                    if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                         txtWholePrice1Popup.val(NetSelling);
                         txtWholePrice2Popup.val(NetSelling);
                         txtWholePrice3Popup.val(NetSelling);
                     } else {
                         if (txtWholePrice1Popup.val() == 0)
                             txtWholePrice1Popup.val(NetSelling);
                         if (txtWholePrice2Popup.val() == 0)
                             txtWholePrice2Popup.val(NetSelling);
                         if (txtWholePrice3Popup.val() == 0)
                             txtWholePrice3Popup.val(NetSelling);
                     }
                     
                 }
             }
             catch (err) {
                 //ShowPopupMessageBox(err.message);
             }
        }

        function fncCessChange(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var CessPrc = txtCESSPrc.val();
            if (charCode == 13) {

                if ($(<%=hidinventoryStatus.ClientID%>).val() == "Edit") {
                    setTimeout(function () {
                        if (CessPrc != '') {

                            if (CessPrc == "0") {
                                $('#<%=txtHSNCode.ClientID %>').select();
                                $('#<%=txtAddCessAmt.ClientID %>').attr("readonly", "readonly");
                                $('#<%=txtAddCessAmt.ClientID %>').val('0');
                            }
                            else if (parseFloat(CessPrc).toFixed(2) <= 100) {
                                txtCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                txtInputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                txtOutputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                fncCalculationNew();
                                $('#<%=txtAddCessAmt.ClientID %>').removeAttr("readonly");
                                $('#<%=txtAddCessAmt.ClientID %>').select();
                            }
                            else {
                                $('#<%=txtCESSPrc.ClientID %>').select();
                                fncInitializeHSNvalidation('% should be less than or equal to 100');
                                FocusCtrl = 'txtCESSPrc.ClientID';
                            }
                        }
                    });
                }
                else {
                    setTimeout(function () {
                        if (CessPrc != '') {
                            if (CessPrc == "0") {
                                $('#<%=txtHSNCode.ClientID %>').select();
                                $('#<%=txtAddCessAmt.ClientID %>').attr("readonly", "readonly");
                                $('#<%=txtAddCessAmt.ClientID %>').val('0');
                            }
                            else if (parseFloat(CessPrc).toFixed(2) <= 100) {
                                txtCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                txtInputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                txtOutputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                                fncCalculationNew();
                                $('#<%=txtAddCessAmt.ClientID %>').removeAttr("readonly");
                                $('#<%=txtAddCessAmt.ClientID %>').select();
                            }
                            else {
                                $('#<%=txtCESSPrc.ClientID %>').select();
                                fncInitializeHSNvalidation('% should be less than or equal to 100');
                                FocusCtrl = 'txtCESSPrc.ClientID';
                            }
                        }
                    }, 50);


                }
                evt.preventDefault();
                return false;
            }
        }

        function MarginCalculationNew() {
            try {
                var MrpMargin = 0, MrpMarkup = 0, MrpmarkDown = 0;
                var Sellmargin = 0, SellMarkUp = 0, SellMarkDown = 0;
                if (parseFloat(txtNetCost.val()) > 0 && parseFloat(txtMRP.val()) > 0) {
                    MrpMargin = parseFloat(txtMRP.val()) - parseFloat(txtNetCost.val());
                    MrpMarkup = parseFloat((MrpMargin / parseFloat(txtNetCost.val())) * 100).toFixed(2);
                    MrpmarkDown = parseFloat((MrpMargin / parseFloat(txtMRP.val())) * 100).toFixed(2);
                }
                if (parseFloat(txtSellingPrice.val()) > 0 && parseFloat(txtNetCost.val()) > 0) {
                    Sellmargin = parseFloat(txtSellingPrice.val()) - parseFloat(txtNetCost.val());
                    SellMarkUp = parseFloat((Sellmargin / parseFloat(txtNetCost.val())) * 100).toFixed(2);
                    SellMarkDown = parseFloat((Sellmargin / parseFloat(txtSellingPrice.val())) * 100).toFixed(2);
                }
                $(<%=txtMRPMargrin.ClientID%>).val(parseFloat(MrpMargin).toFixed(2));
                $(<%=txtsellMargin.ClientID%>).val(parseFloat(Sellmargin).toFixed(2));
                $(<%=txtMRPMarkDownNew.ClientID%>).val(MrpmarkDown);
                $(<%=txtMRPMarkUp.ClientID%>).val(MrpMarkup);
                $(<%=txtSellMarkDown.ClientID%>).val(SellMarkDown);
                $(<%=txtSellMarkUp.ClientID%>).val(SellMarkUp);
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncCalculationNew() {

            if ($('#<%=rbnMRP2.ClientID %>').is(':checked')) {
                fncMRP1_NetCost();
                fncMRP2_NetSellingPrice_Calculation();
            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                fncMRP1_NetCost();
                fncMRP1_NetSelling();
            }
            else if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "N" || parseFloat(txtBasicMargin.val()) == 0)  
                        fncMarkup_Netcost_Calculation();
                else if ($(<%=ddlPurchase.ClientID%>).val() == "Cost" && parseFloat(txtBasicMargin.val()) > 0)
                    fncMarkup_Netcost_Calculation_New();
                else
                    fncMRP1_NetCost_New();
                    if ($(<%=hidNewCalculation.ClientID%>).val().trim() == "N")  
                        fncMRP2_NetSellingPrice_Calculation();
                else if ($(<%=ddlSales.ClientID%>).val() == "Cost")
                    fncMRP2_NetSellingPrice_Calculation_New();
                else
                        fncMRP1_NetSelling_New(); 
            }
            else if ($('#<%=rbnMRP3.ClientID %>').is(':checked')) {
                fncMRP34_GrossCost();
                fncMRP1_NetSelling();
            }
            else if ($('#<%=rbnMRP4.ClientID %>').is(':checked')) {
                fncMRP34_GrossCost();
                fncMRP2_NetSellingPrice_Calculation();
            }
            MarginCalculationNew();
            if (parseFloat(txtWholePrice1Popup.val()) != 0 && parseFloat(txtWholePrice2Popup.val()) != 0
                && parseFloat(txtWholePrice3Popup.val()) != 0) {
                fncArriveWpricePercentage("wprice1");
                fncArriveWpricePercentage("wprice2");
                fncArriveWpricePercentage("wprice3");
            }
        }
        function fncArriveWpricePercentage(mode) {
            try {

                var SPMargin = 0, GSTAmt = 0, WPBasicAmt = 0;
                var array, GSTPer = 0;

                    <%--if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                        if (array == ',, Select ,,') {
                            GSTPer = '0.00';
                        }
                        else
                            GSTPer = array[1]; 
                        if (mode == "wprice1") {
                          <%--  GSTAmt = (parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W1 = ((parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()))/parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice1.ClientID %>').val(W1); 

                        }
                        else if (mode == "wprice2") {--%>
                    <%-- GSTAmt = (parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W2 = ((parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice2.ClientID %>').val(W2);
                        }
                        else {--%>
                    <%-- GSTAmt = (parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));--%>

                    <%-- var W3 = ((parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val()))/parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                            $('#<%=txtMarginWPrice3.ClientID %>').val(W3);
                        }
                }--%>

                array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                if (array == ',, Select ,,') {
                    GSTPer = '0.00';
                }
                else
                    GSTPer = array[1];

                
                    if (mode == "wprice1") {
                        var W1 = ((parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                        if (WPriceReverse == 'N')
                            $('#<%=txtMarginWPrice1.ClientID %>').val(W1);
                        $('#<%=txtEarnedMar1.ClientID %>').val(parseFloat(W1).toFixed(2));
                    }
                    else if (mode == "wprice2") {
                        var W2 = ((parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                        if (WPriceReverse == 'N')
                            $('#<%=txtMarginWPrice2.ClientID %>').val(W2);
                        $('#<%=txtEarnedMar2.ClientID %>').val(parseFloat(W2).toFixed(2));
                    }
                    else {
                        var W3 = ((parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat($('#<%=txtNetCost.ClientID %>').val())) / parseFloat($('#<%=txtNetCost.ClientID %>').val()) * 100).toFixed(2);
                        if (WPriceReverse == 'N')
                            $('#<%=txtMarginWPrice3.ClientID %>').val(W3);
                        $('#<%=txtEarnedMar3.ClientID %>').val(parseFloat(W3).toFixed(2));
                    }
                
              
            }

            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncMRP34_GrossCost() {
            try {

                var MRP34 = parseFloat(txtMRP.val()).toFixed(2);
                var MRP34_MDP = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                var MRP34_MDAP = parseFloat(txtAdDiscount.val()).toFixed(2);

                var GrossCost = parseFloat(MRP34 - MRP34 * MRP34_MDP / 100).toFixed(2);
                GrossCost = GrossCost - parseFloat(GrossCost * MRP34_MDAP / 100).toFixed(2);

                var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);
                var AdCess = parseFloat(txtAddCessAmt.val()).toFixed(2);

                var SGSTAmt;
                var CGSTAmt;
                var CESSAmt;
                if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                    SGSTAmt = 0;
                    CGSTAmt = 0;
                    CESSAmt = 0;
                }
                else {
                    SGSTAmt = (parseFloat(GrossCost) * parseFloat(SGST) / 100).toFixed(2);
                    CGSTAmt = SGSTAmt;
                    CESSAmt = (parseFloat(GrossCost) * parseFloat(CESS) / 100).toFixed(2);
                }

                var NetCost = (parseFloat(parseFloat(GrossCost) + parseFloat(SGSTAmt) + parseFloat(CGSTAmt) + parseFloat(CESSAmt) + parseFloat(AdCess))).toFixed(2);

                if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                    GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                    CESSAmt = 0.00;
                }

                txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                txtBasicCost.val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
                txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                txtNetCost.val(parseFloat(NetCost).toFixed(2));
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncMRP1_NetCost_New() {
            try { 
                //$(txtDiscountPrc1).val('0');
                //$(txtDiscountPrc2).val('0');
                //$(txtDiscountPrc3).val('0');

                var MRP1 = parseFloat(txtMRP.val()).toFixed(2);
                var BasicMargin = parseFloat(txtBasicMargin.val()).toFixed(2);
                if (parseFloat(MRP1) > 0) { 
                    var NetCost = parseFloat(MRP1 * (1 - (BasicMargin / 100))).toFixed(2); 
                   
                   <%-- $('#<%=txtBasicCost.ClientID%>').val(parseFloat(NetCost).toFixed(2));--%>
                    $('#<%=hidOldNetCost.ClientID%>').val(parseFloat(NetCost).toFixed(2));
                    var MRP1_MDP = parseFloat(txtMRPMarkDown.val()).toFixed(2);

                    //if (parseFloat(MRP1_MDP) > 0) {
                    //    var MRP1_MDAP = parseFloat(txtAdDiscount.val()).toFixed(2); 
                    //    NetCost = parseFloat(MRP1 - MRP1 * MRP1_MDP / 100).toFixed(2);
                    //    NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2); 
                    //}
                 

                    var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                    var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                    var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);

                    var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat(txtAddCessAmt.val()).toFixed(2);
                    var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)))).toFixed(2);

                    var SGSTAmt;
                    var CGSTAmt;
                    var CESSAmt;
                    if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                        SGSTAmt = 0;
                        CGSTAmt = 0;
                        CESSAmt = 0;
                    }
                    else {
                        SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) * parseFloat(SGST)).toFixed(2);
                        CGSTAmt = SGSTAmt;
                        CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                    }
                   
                   
                    var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);
                    var BasicCost = (parseFloat(parseFloat(NetCost) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                    if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                        GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                        CESSAmt = 0.00;
                    }
                    var BasicCostChange = '0';
                    if (parseFloat(txtDiscountPrc1.val()) > 0 || parseFloat(txtDiscountPrc2.val()) > 0 || parseFloat(txtDiscountPrc3.val()) > 0) {
                        var DiscAmt1 = (GrossCost * parseFloat(txtDiscountPrc1.val()).toFixed(2) / 100).toFixed(2);
                        TotDiscAmt = DiscAmt1;
                        DiscountedBasicCost = (parseFloat(GrossCost) - parseFloat(DiscAmt1)).toFixed(2)
                        var DiscAmt2 = (DiscountedBasicCost * parseFloat(txtDiscountPrc2.val()).toFixed(2) / 100).toFixed(2);
                        TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                        DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                        var DiscAmt3 = (DiscountedBasicCost * parseFloat(txtDiscountPrc3.val()).toFixed(2) / 100).toFixed(2);
                        TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                        //GrossCost = (parseFloat(txtBasicCost.val()).toFixed(2) - TotDiscAmt).toFixed(2);
                        //var GrossCostnew = (parseFloat(txtBasicCost.val()).toFixed(2));
                        GrossCost = (parseFloat(BasicCost) - TotDiscAmt).toFixed(2);
                        var GrossCostnew = (parseFloat(BasicCost).toFixed(2));
                        BasicCostChange = '0';
                        CGSTAmt = (GrossCost * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                        var CGSTAmtnew = (GrossCostnew * parseFloat(txtInputCGSTPrc.val()) / 100).toFixed(2);
                        SGSTAmt = CGSTAmt;
                        var SGSTAmtnew = CGSTAmtnew;
                        CESSAmt = (GrossCost * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);
                        var CESSAmtnew = (GrossCostnew * parseFloat(txtInputCESSPrc.val()).toFixed(2) / 100).toFixed(2);

                        txtDiscountAmt.val(parseFloat(TotDiscAmt).toFixed(2));
                        var BasicCost1 = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);
                        

                      <%--  if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked'))
                            AffetctNetCost = GrossCost;
                        else
                            AffetctNetCost = parseFloat(TotDiscAmt) + parseFloat(GrossCost);--%>
                        NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) -
                            parseFloat(CESSAmt) + parseFloat(txtAddCessAmt.val())).toFixed(2);

                        NetCostnew = parseFloat(parseFloat(GrossCostnew) + parseFloat(CGSTAmtnew) + parseFloat(SGSTAmtnew) -
                            parseFloat(CESSAmtnew) + parseFloat(txtAddCessAmt.val())).toFixed(2);

                        $('#<%=hidNetcostnew.ClientID %>').val(NetCostnew); //surya19112021 new changes Akshaya Discount
                        $('#<%=hidGrossCostNew.ClientID %>').val(GrossCostnew);
                    }
                    else {
                        txtDiscountAmt.val(parseFloat(0).toFixed(2));
                        $('#<%=hidNetcostnew.ClientID %>').val(NetCost); //surya19112021 new changes Akshaya Discount
                        $('#<%=hidGrossCostNew.ClientID %>').val(GrossCost);
                    }
                   

                    txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                    txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                    txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                    if (BasicCostChange == "0")
                        txtBasicCost.val(parseFloat(BasicCost).toFixed(2)); 
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                    txtNetCost.val(parseFloat(NetCost).toFixed(2)); 
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncMRP1_NetCost() {
            try {

                var MRP1 = parseFloat(txtMRP.val()).toFixed(2);
                if (parseFloat(MRP1) > 0) {
                    var MRP1_MDP = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                    var MRP1_MDAP = parseFloat(txtAdDiscount.val()).toFixed(2);
                    //var NetCost = MRP1 - MRP1 * MRP1_MDP / 100;
                    var NetCost = parseFloat(MRP1 - MRP1 * MRP1_MDP / 100).toFixed(2);
                    NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2);

                    var CGST = parseFloat(txtInputCGSTPrc.val()).toFixed(2);
                    var SGST = parseFloat(txtInputSGSTPrc.val()).toFixed(2);
                    var CESS = parseFloat(txtInputCESSPrc.val()).toFixed(2);

                    var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat(txtAddCessAmt.val()).toFixed(2);
                    var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)))).toFixed(2);

                    var SGSTAmt;
                    var CGSTAmt;
                    var CESSAmt;
                    if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) == 0) {
                        SGSTAmt = 0;
                        CGSTAmt = 0;
                        CESSAmt = 0;
                    }
                    else {
                        SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(CESS)) * parseFloat(SGST)).toFixed(2);
                        CGSTAmt = SGSTAmt;
                        CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                    }

                    var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                    if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                        GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                        CESSAmt = 0.00;
                    }

                    txtInputCGSTAmt.val(parseFloat(CGSTAmt).toFixed(2));
                    txtInputSGSTAmt.val(parseFloat(SGSTAmt).toFixed(2));
                    txtInputCESSAmt.val(parseFloat(CESSAmt).toFixed(2));
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                    txtNetCost.val(parseFloat(NetCost).toFixed(2));

                    //TaxLiabilityCalc();
                    //fncNetMarginCalculation();
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncMRP1_NetSelling_New() {
            try { 
                if (parseFloat(txtMRP.val()) > 0) {
                   <%-- if (($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false || parseFloat($('#<%=txtDiscountAmt.ClientID %>').val()) == 0) &&
                        parseFloat($('#<%=txtBasicMargin.ClientID %>').val()) > 0 && $(<%=ddlPurchase.ClientID%>).val() != "Mrp") {
                        AffetctNetCost = parseFloat((parseFloat(txtMRP.val())) / (1 + (parseFloat(txtBasicMargin.val()) / 100))).toFixed(2); 
                    }
                    else if ($('#<%=chkAffectNetCost.ClientID %>').is(':checked') == false && $(<%=ddlPurchase.ClientID%>).val() != "Mrp") {
                        AffetctNetCost = parseFloat(txtBasicCost.val()); 
                    }
                    else {
                        fncMRP2_NetSellingPrice_Calculation();
                        return false;

                    }--%>
                    var NetSelling = (parseFloat(txtMRP.val()).toFixed(2) * (parseFloat(100) - parseFloat(txtFixedMargin.val()).toFixed(2)) / 100);

                    var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                    var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                    var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                    var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer) + parseFloat(OutputCESSPer);

                    var TotalTaxAmt = ((parseFloat(NetSelling) - parseFloat(AdlCessAmt)) * TotalPer / (parseFloat(100) + parseFloat(TotalPer))).toFixed(2);

                    var OutputCGSTAmt;
                    var OutputSGSTAmt;
                    var OutputCESSAmt;
                    if (TotalPer == 0) {
                        OutputCGSTAmt = 0;
                        OutputSGSTAmt = 0;
                        OutputCESSAmt = 0;
                    }
                    else {
                        OutputCGSTAmt = (TotalTaxAmt / TotalPer * OutputCGSTPer).toFixed(2);
                        OutputSGSTAmt = OutputCGSTAmt;
                        OutputCESSAmt = parseFloat(TotalTaxAmt) - parseFloat(OutputCGSTAmt) - parseFloat(OutputSGSTAmt);
                    }


                    var BasicSelling = parseFloat(NetSelling) - parseFloat(AdlCessAmt) - parseFloat(TotalTaxAmt);

                    if (parseFloat(OutputCESSAmt).toFixed(2) == '-0.01' || parseFloat(OutputCESSAmt).toFixed(2) == '0.01') {
                        BasicSelling = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt);
                        OutputCESSAmt = 0.00;
                    }
                    txtBasicSelling.val(BasicSelling);
                    txtOutputCGSTAmt.val(OutputCGSTAmt);
                    txtOutputSGSTAmt.val(OutputSGSTAmt);
                    txtOutputCESSAmt.val(OutputCESSAmt);
                    txtSellingPrice.val(NetSelling);

                    if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                         txtWholePrice1Popup.val(NetSelling);
                         txtWholePrice2Popup.val(NetSelling);
                        txtWholePrice3Popup.val(NetSelling);
                       
                     } else {
                         if (txtWholePrice1Popup.val() == 0)
                             txtWholePrice1Popup.val(NetSelling);
                         if (txtWholePrice2Popup.val() == 0)
                             txtWholePrice2Popup.val(NetSelling);
                         if (txtWholePrice3Popup.val() == 0)
                             txtWholePrice3Popup.val(NetSelling);
                     }

                     // Call 
                     //TaxLiabilityCalc();
                     //fncNetMarginCalculation();
                 }
             }
             catch (err) {
                 //ShowPopupMessageBox(err.message);
             }

         }
        // Mowzoon - Full Mark Down Net Selling and GST. CESS, Adl Cess Calculation  -  28-02-2019
        function fncMRP1_NetSelling() {
            try {
                //txtFixedMargin
                //Basic Selling : txtBasicSelling
                //Output SGST txtOutputSGSTPrc txtOutputSGSTAmt
                //Output CGST txtOutputCGSTPrc  txtOutputCGSTAmt
                //Output CESS txtOutputCESSPrc  txtOutputCESSAmt

                //RoundOFF : txtRoundOff
                //NetSelling txtSellingPrice
                //Liab % :  txtGSTLiabilityPer   Amt: txtGSTLiability
                if (parseFloat(txtMRP.val()) > 0) {
                    var NetSelling = parseFloat(txtMRP.val()).toFixed(2) - (parseFloat(txtMRP.val()).toFixed(2) * parseFloat(txtFixedMargin.val()).toFixed(2) / 100);

                    var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                    var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                    var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                    var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer) + parseFloat(OutputCESSPer);

                    var TotalTaxAmt = ((parseFloat(NetSelling) - parseFloat(AdlCessAmt)) * TotalPer / (parseFloat(100) + parseFloat(TotalPer))).toFixed(2);

                    var OutputCGSTAmt;
                    var OutputSGSTAmt;
                    var OutputCESSAmt;
                    if (TotalPer == 0) {
                        OutputCGSTAmt = 0;
                        OutputSGSTAmt = 0;
                        OutputCESSAmt = 0;
                    }
                    else {
                        OutputCGSTAmt = (TotalTaxAmt / TotalPer * OutputCGSTPer).toFixed(2);
                        OutputSGSTAmt = OutputCGSTAmt;
                        OutputCESSAmt = parseFloat(TotalTaxAmt) - parseFloat(OutputCGSTAmt) - parseFloat(OutputSGSTAmt);
                    }


                    var BasicSelling = parseFloat(NetSelling) - parseFloat(AdlCessAmt) - parseFloat(TotalTaxAmt);

                    if (parseFloat(OutputCESSAmt).toFixed(2) == '-0.01' || parseFloat(OutputCESSAmt).toFixed(2) == '0.01') {
                        BasicSelling = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt);
                        OutputCESSAmt = 0.00;
                    }
                    txtBasicSelling.val(BasicSelling);
                    txtOutputCGSTAmt.val(OutputCGSTAmt);
                    txtOutputSGSTAmt.val(OutputSGSTAmt);
                    txtOutputCESSAmt.val(OutputCESSAmt);
                    txtSellingPrice.val(NetSelling);

                    if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                        txtWholePrice1Popup.val(NetSelling);
                        txtWholePrice2Popup.val(NetSelling);
                        txtWholePrice3Popup.val(NetSelling);
                    } else {
                        if (txtWholePrice1Popup.val() == 0)
                            txtWholePrice1Popup.val(NetSelling);
                        if (txtWholePrice2Popup.val() == 0)
                            txtWholePrice2Popup.val(NetSelling);
                        if (txtWholePrice3Popup.val() == 0)
                            txtWholePrice3Popup.val(NetSelling);
                    }

                    // Call 
                    //TaxLiabilityCalc();
                    //fncNetMarginCalculation();
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }

        }
//Calculation of Net Margin Selling Price and MRP
<%--function fncNetMarginCalculation()//by velu 28-02-2019 Vijay
{
    try {
        OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
        OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();
        //SellingPrice RoundOff based On ParamMaster
        txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));

        //===================> Margin MRP Amt  
        var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

        MRP = txtMRP.val();
        GrossCost = txtGrossCost.val();
        array = ddlOutputIGST.find("option:selected").text().trim().split("-");
        if (array == ',, Select ,,') {
            GSTPer = '0.00';
        }
        else
            GSTPer = array[1];
        GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
        MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
        MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);



        var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
        MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
        txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

        /// work done by saravanan 15-02-2015

        MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
        txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));
        //===================> Margin Selling Price Prc  
        var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
        var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();

        var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    var SPPrc1 = SPPrc / SellingPrice * 100
                    SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
                    txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

        //===================> Margin Selling Price Amt 
                    var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                txtSPAmt.val(parseFloat(SPAmt).toFixed(2));

        // Assign WholeSale Price
                txtWholePrice1Popup.val(parseFloat(SellingPrice).toFixed(2))
                txtWholePrice2Popup.val(parseFloat(SellingPrice).toFixed(2))
                txtWholePrice3Popup.val(parseFloat(SellingPrice).toFixed(2))

        /// To Arrive SellingPrice Margin
                if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        if (parseFloat($('#<%=txtGrossCost.ClientID %>').val()) != 0) {
                            SPMargin = (parseFloat($('#<%=txtBasicSelling.ClientID %>').val()) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtFixedMargin.ClientID %>').val(SPMargin.toFixed(2));

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));
                        }
                    }
                }
                catch (err) {
                    //ShowPopupMessageBox(err.message);
                }
            }--%>

        function fncMRP2_NetSellingPrice_Calculation() {
            try {
                if (parseFloat(txtGrossCost.val()) > 0) {
                    var BasicSelling = parseFloat(parseFloat(txtGrossCost.val()) + parseFloat(txtGrossCost.val() * txtFixedMargin.val() / 100)).toFixed(2);

                    var OutputCGSTPer = parseFloat(txtOutputCGSTPrc.val()).toFixed(2);
                    var OutputSGSTPer = parseFloat(txtOutputSGSTPrc.val()).toFixed(2);
                    var OutputCESSPer = parseFloat(txtOutputCESSPrc.val()).toFixed(2);

                    var AdlCessAmt = parseFloat(txtAddCessAmt.val()).toFixed(2);

                    var TotalPer = parseFloat(OutputCGSTPer) + parseFloat(OutputSGSTPer);

                    var TotalTaxAmt = parseFloat(BasicSelling * TotalPer / 100).toFixed(2);

                    var OutputCGSTAmt = parseFloat(TotalTaxAmt / 2).toFixed(2);
                    var OutputSGSTAmt = OutputCGSTAmt;
                    var OutputCESSAmt = parseFloat(BasicSelling * OutputCESSPer / 100).toFixed(2);

                    var NetSellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCESSAmt) + parseFloat(AdlCessAmt);

                    txtBasicSelling.val(BasicSelling);
                    txtOutputCGSTAmt.val(OutputCGSTAmt);
                    txtOutputSGSTAmt.val(OutputSGSTAmt);
                    txtOutputCESSAmt.val(OutputCESSAmt);
                    txtSellingPrice.val(NetSellingPrice);

                    if ($('#<%=hidinventoryStatus.ClientID%>').val() != "Edit") {
                        txtWholePrice1Popup.val(NetSellingPrice);
                        txtWholePrice2Popup.val(NetSellingPrice);
                        txtWholePrice3Popup.val(NetSellingPrice);
                    } else {
                        if (txtWholePrice1Popup.val() == 0)
                            txtWholePrice1Popup.val(NetSellingPrice);
                        if (txtWholePrice2Popup.val() == 0)
                            txtWholePrice2Popup.val(NetSellingPrice);
                        if (txtWholePrice3Popup.val() == 0)
                            txtWholePrice3Popup.val(NetSellingPrice);
                    }
                    // Call 
                    //TaxLiabilityCalc();
                    //fncNetMarginCalculation();
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        // Margin Calculation Re Written  --  Mowzoon -- 09-03-2019
        function fncNetMarginCalculation() {
            try {

                var MRP = txtMRP.val();
                var GrossCost = txtGrossCost.val();
                var BasicSelling = txtBasicSelling.val();
                var NetSelling = txtSellingPrice.val();
                var NetCost = txtNetCost.val();
                //txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));
                // Margin Calculation by MRP --  Mowzoon -- 09-03-2019
                if (MRP != 0) {
                    var MRPwoAdCess = (parseFloat(MRP) - parseFloat(txtAddCessAmt.val())).toFixed(2);
                    var GSTPer = (parseFloat(txtOutputCGSTPrc.val()) + parseFloat(txtOutputSGSTPrc.val()) + parseFloat(txtCESSPrc.val())).toFixed(2);
                    var MRPwoGST = (parseFloat(MRPwoAdCess) - parseFloat(MRPwoAdCess) * parseFloat(GSTPer) / (parseFloat(100) + parseFloat(GSTPer))).toFixed(2);
                    //var MarginAmt = (parseFloat(MRPwoGST) - parseFloat(GrossCost)).toFixed(2);
                    //var MrpGst = (parseFloat(MRP) * GSTPer) / 100;
                    //var MrpGst2 = (parseFloat(GSTPer) + 100)/100;
                    //var MarginAmt = (parseFloat(MRP) - parseFloat(GrossCost)) - ((MrpGst / MrpGst2));
                    var MarginAmt = (parseFloat(MRPwoGST) - parseFloat(GrossCost)).toFixed(2);
                    var MarginPer = (parseFloat(MarginAmt) / parseFloat(MRP) * parseFloat(100)).toFixed(2);

                    txtMarginMRP.val(MarginPer);
                    txtMRPAmt.val(MarginAmt);
                }

                // Margin Calculation by Selling Price --  Mowzoon -- 09-03-2019
                if (NetSelling != 0) {
                    //var MarginAmt = (parseFloat(BasicSelling) - parseFloat(GrossCost)).toFixed(2);
                    //var MarginAmt = ((parseFloat(BasicSelling) - parseFloat(NetCost)) - (GSTPer)).toFixed(2);
                    var MarginAmt = (parseFloat(BasicSelling) - parseFloat(GrossCost)).toFixed(2);
                    var MarginPer = (parseFloat(MarginAmt) / parseFloat(NetSelling) * parseFloat(100)).toFixed(2);
                    txtMarginSP.val(MarginPer);
                    txtSPAmt.val(MarginAmt);
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function TaxLiabilityCalc() {
            try {
                var OutputGSTAmt = (parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputSGSTAmt.val())).toFixed(2);
                var InputGSTAmt = (parseFloat(txtInputCGSTAmt.val()) + parseFloat(txtInputSGSTAmt.val())).toFixed(2);
                var GSTLaibAmt = (parseFloat(OutputGSTAmt) - parseFloat(InputGSTAmt)).toFixed(2);
                var SellingPrice = txtSellingPrice.val();
                if (InputGSTAmt != OutputGSTAmt) {
                    var GSTLaibPer = (parseFloat(GSTLaibAmt) / parseFloat(txtSellingPrice.val()) * 100).toFixed(2);
                    txtGSTLiabilityPer.val(GSTLaibPer);
                    txtGSTLiability.val(GSTLaibAmt);

                    var GSTLiabilityAmtPrc = ((parseFloat(GSTLaibAmt) / 2) / parseFloat(txtSellingPrice.val()) * 100).toFixed(2);
                    txtVatLiabilitySGSTPrc.val(parseFloat(GSTLiabilityAmtPrc).toFixed(2));
                    txtVatLiabilityCGSTPrc.val(parseFloat(GSTLiabilityAmtPrc).toFixed(2));

                    var GSTLiabilitySGSTAmt = parseFloat(GSTLaibAmt) / 2;
                    txtVatLiabilitySGSTAmt.val(GSTLiabilitySGSTAmt.toFixed(2));
                    txtVatLiabilityCGSTAMt.val(GSTLiabilitySGSTAmt.toFixed(2));

                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }


        function fncRules() { //Vijay Rules
            try {
                fnctblBind();
                $("#divRules").dialog({
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true,
                    title: "Param Codes",
                    appendTo: 'form:first',
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                        }
                    }

                });
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fnctblBind() {
            try {
                tbody = $("#tblParam tbody");
                var objSearchData = jQuery.parseJSON($('#<%=hidSearch.ClientID %>').val());

                if (objSearchData.length > 0) {
                    tbody.children().remove();
                    for (var i = 0; i < objSearchData.length; i++) {
                        var rows;
                        rows = "<tr tabindex = '" + i + "' onfocus = 'return fncMouseSearchesFocus(this);' onkeydown = 'return fncSearchTableKeyDowns(event, this);' ondblclick='fncSearchTabledblClicks(this);'>" +
                            "<td>" + objSearchData[i]["SNo"] + "</td>" +
                            "<td>" + objSearchData[i]["Paramcode"] + "</td>" +
                            "<td>" + objSearchData[i]["Value"] + "</td>" +
                            "<td>" + objSearchData[i]["Description"] + "</td>"
                            + "</tr>";
                        tbody.append(rows);
                    }
                }
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncSearchTableKeyDowns(evt, source) {
            try {
                var rowobj = $(source);
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //Enter Key
                if (charCode == 13) {
                    fncSearchTabledblClicks(source);
                    return false;
                }

                //Down Key
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "#ADD8E6");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();
                        $('#<%=txtParamcodeDes.ClientID %>').val($(NextRowobj).find("td").eq(3).html().trim());
                    }
                    return false;
                }
                //Up Key
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "#ADD8E6");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();
                        $('#<%=txtParamcodeDes.ClientID %>').val($(prevrowobj).find("td").eq(3).html().trim());
                    }
                    else {
                        $("#search").focus();
                        $('#<%=txtParamcodeDes.ClientID %>').val('');
                        prevrowobj.css("background-color", "");
                    }
                    return false;
                }
                return false;
            }
            catch (err) {
                fncToastError(err);
            }
        }
        function fncSearchTabledblClicks(source) {
            try {
                $('#<%=txtParamcodeDes.ClientID %>').val($(source).find("td").eq(3).html().trim());
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }
        function fncMouseSearchesFocus(source) {
            try {
                var rowobj = $(source);
                rowobj.css("background-color", "#ADD8E6");
                rowobj.siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err);
            }
        }
        $(document).ready(function () {
            $("#search").on("keyup", function () {
                var value = $(this).val().toUpperCase();
                $("#tblParam tr").each(function (index) {
                    if (index >= 0) {
                        $row = $(this);
                        var id = $row.find("td:eq(1)").text();
                        if ($(this).text().toUpperCase().search(value) > -1) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    }
                });
            });

        });

        function fncKeyPressSearch(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 40) {
                //$("#tblParam").find('input:first').focus();
                $("#tblParam tbody > tr").first().focus();
            }
        }

        $(document).ready(function () {


            $("#<%=cbWeightbased.ClientID %>").click(function () { //Vijay Check
                var checked = $(this).is(':checked');
                if (checked == true) {
                    if ($("#<%=hidWeightCanversion.ClientID %>").val() == "Y") {
                        if ($("#<%=chkQuatation.ClientID %>").is(':checked')) {
                            ShowPopupMessageBox("Please Uncheck Quotation");
                            $("#<%=cbWeightbased.ClientID %>").prop("checked", false);
                        }
                    }
                }
            });

            $("#<%=chkQuatation.ClientID %>").click(function () {
                var checked = $(this).is(':checked');
                if (checked == true) {
                    if ($("#<%=hidWeightCanversion.ClientID %>").val() == "Y") {
                        if ($("#<%=cbWeightbased.ClientID %>").is(':checked')) {
                            ShowPopupMessageBox("Please Uncheck Weight Based");
                            $("#<%=chkQuatation.ClientID %>").prop("checked", false);
                        }
                    }
                }
            });
        });

        function fncDepatrtmentWiseitemcode() {
            try {
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncDeptItemCode") %>',
                    data: "{'sMode':'" + "1" + "', 'sDeptcode':'" + $("#<%=txtDepartment.ClientID %>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $("#<%=txtItemCode.ClientID %>").val($.trim(msg.d));
                        $("#<%=hdfItemCode.ClientID %>").val($.trim(msg.d));
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
                return false;
            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncItemType() {
            try {
                if ($("#<%=txtItemType.ClientID %>").val().toUpperCase() == "BULK"
                    && $("#<%=hidBulkBatchDistribution.ClientID %>").val().toUpperCase() == "N") {

                    $('#<%=chkBatch.ClientID %>').prop("checked", false);
                    $('#<%=chkBatch.ClientID %>').attr("disabled", "disabled");
                }
                <%--else {
                    $('#<%=chkBatch.ClientID %>').prop("checked", true);
                    $('#<%=chkBatch.ClientID %>').removeAttr("disabled");
                }--%>

            }
            catch (err) {
                //ShowPopupMessageBox(err.message);
            }
        }

        function fncCheckUom() {
            
            if ($('#<%=txtUOMPurchase.ClientID %>').val() == "KG" || $('#<%=txtSalesUOM.ClientID %>').val() == "KG") {
                $('#<%=chkBatch.ClientID %>').prop("checked", false);
                $('#<%=chkBatch.ClientID %>').attr("disabled", "disabled");
            }
            else {
                $('#<%=chkBatch.ClientID %>').prop("checked", true);
                $('#<%=chkBatch.ClientID %>').removeAttr("disabled", "disabled");
            }

       }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="EnableScroll">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="../Inventory/frmInventoryMaster.aspx">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li id="invhdr" class="active-page">Inventory Master</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;margin-left: -15px" onclick="fncGetUrl(); return false;"></i></li> 
            </ul>
        </div>         
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" UpdateMode="Conditional" >
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="top-container-im">
                        <div class="left-container">
                            <div class="left-container-header">
                                Product Detail
                            </div>
                            <div class="left-container-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtInvcategory" runat="server" MaxLength="50" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Category');" onkeydown="return fncShowSearchDialogCommon(event, 'category',  'txtInvcategory', 'txtDepartment');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDepartment" MaxLength="50" runat="server" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Department');" onkeydown="return fncShowSearchDialogCommon(event, 'Department',  'txtDepartment', 'txtBrand');"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemCode" MaxLength="20" Enabled="false" runat="server" onkeydown=" return fncInventorykeydown(event);" CssClass="form-control-res"></asp:TextBox>
                                            <asp:HiddenField ID="hdfItemCode" runat="server" />
                                            <asp:HiddenField ID="hfBatch" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBrand" runat="server" MaxLength="50" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Brand');" onkeydown="return fncShowSearchDialogCommon(event, 'Brand',  'txtBrand', 'txtItemName');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label><span
                                            class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res" MaxLength="200" onblur="fncValidateItemName();" onfocus="fncToEnableShortcutKey('Itemname');" onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemName', 'txtShortName');"></asp:TextBox>
                                        <asp:TextBox ID="txtOtherLanguageName" runat="server" CssClass="tamil form-control-res display_none" MaxLength="50"></asp:TextBox><%--onkeydown="return fncBlockSpecialChar(event);"--%>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShortName %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtShortName" runat="server" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');" MaxLength="20"></asp:TextBox>
                                        <asp:TextBox ID="txtOtherLanguageSName" runat="server" CssClass="tamil form-control-res display_none" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="100" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWareHouse" runat="server" MaxLength="50" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('WareHouse');" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse',  'txtWareHouse', 'txtMerchandise');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMerchandise" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise',  'txtMerchandise', 'txtManafacture');" onfocus="fncToEnableShortcutKey('Merchandise');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtManafacture" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture',  'txtManafacture', 'txtItemType');"
                                                runat="server" MaxLength="50" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Manufacture');"></asp:TextBox>
                                            <%--  <asp:DropDownList ID="ddlManafacture" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemType %>'></asp:Label>
                                            <span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemType" runat="server" MaxLength="50" CssClass="form-control-res" onfocusout="fncItemType();"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'ItemType',  'txtItemType', 'txtUOMPurchase');" onfocus="fncToEnableShortcutKey('ItemType');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="left-container-detail" style="margin-top: 4px">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="UOM Purchase"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtUOMPurchase" onkeydown="return fncShowSearchDialogCommon(event, 'UOM',  'txtUOMPurchase', 'txtSalesUOM');"
                                                runat="server" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');"
                                                onchange="return fncCheckUom();" ></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlUOMPurchase" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label88" runat="server" Text="Sales UOM"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSalesUOM" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'UOM',  'txtSalesUOM', 'txtStockType');"
                                                CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');" onchange="return fncCheckUom();"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlSalesUOM" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_StockType %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:DropDownList ID="ddlStockType" runat="server" CssClass="form-control-res" onchange="fncStockTypeChange();">
                                            </asp:DropDownList>--%>
                                            <asp:TextBox ID="txtStockType" runat="server" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');" onkeydown="checkPhoneKey(event); return false;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <%--Batch VijayV--%>
                                        <div class="label-left">
                                            <asp:Label ID="Label122" runat="server" Text="Batch"></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:DropDownList ID="ddlStockType" runat="server" CssClass="form-control-res" onchange="fncStockTypeChange();">
                                            </asp:DropDownList>--%>
                                            <asp:CheckBox ID="chkBatch" onkeydown="return fncCheckBoxKeyDown(event,'chkBatch');" CssClass="checkbox1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="right-container-top">
                            <div class="right-container-top-header">
                                More Detail
                            </div>
                            <div class="right-container-top-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_Style %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtStyle" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Style',  'txtStyle', 'txtPackage');" onfocus="fncToEnableShortcutKey('Style');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlStyle" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_Package %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPackage" runat="server" MaxLength="50" onkeydown="return fncShowSearchDialogCommon(event, 'Package',  'txtPackage', 'txtSize');" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');"></asp:TextBox>
                                            <asp:LinkButton ID="lnkMultipeUOM" runat="server" class="button-blue" OnClientClick="fncShowMultipUOM();return false;"
                                                Text='MultipleUOM'></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSize" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Size',  'txtSize', 'txtCCode');" onfocus="fncToEnableShortcutKey('Size');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlSize" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_CCode %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCCode" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'CCode',  'txtCCode', 'txtModelNo');" onfocus="fncToEnableShortcutKey('CCode');"></asp:TextBox>
                                            <%--   <asp:DropDownList ID="ddlCCode" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_ModelNo %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtModelNo" runat="server" MaxLength="50" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div id="vendorNametooltip" class="label-right" title="">
                                            <asp:TextBox ID="txtVendor" runat="server" MaxLength="50" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Vendor');" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtOrigin');"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_Origin %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtOrigin" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Origin', 'txtOrigin', 'txtClass');" onfocus="fncToEnableShortcutKey('Origin');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtClass" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtSubCategory');" onfocus="fncToEnableShortcutKey('Class');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lbl_SubCategory %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSubCategory" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtSubClass');" onfocus="fncToEnableShortcutKey('SubCategory');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label25" runat="server" Text='<%$ Resources:LabelCaption,lbl_SubClass %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSubClass" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'txtSubClass', 'txtFloor');" onfocus="fncToEnableShortcutKey('SubClass');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSubClass" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-container-bottom">
                            <div class="right-container-bottom-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_Floor %>'></asp:Label>
                                            <span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFloor" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');" onfocus="fncToEnableShortcutKey('Floor');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label29" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSection" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');" onfocus="fncToEnableShortcutKey('Section');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label26" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBin" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');" onfocus="fncToEnableShortcutKey('Bin');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label32" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtShelf" runat="server" MaxLength="50" CssClass="form-control-res"
                                                onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtWeight');" onfocus="fncToEnableShortcutKey('Shelf');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-container-bottom">
                            <div class="right-container-bottom-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label28" runat="server" Text="Weight (Kg)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWeight" Text="0.00" runat="server" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label31" runat="server" Text="Width (cm)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWidth" runat="server" Text="0.0" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label27" runat="server" Text="Height (cm)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtHeight" runat="server" Text="0.0" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label30" runat="server" Text="Length (cm)"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtLength" runat="server" Text="0.0" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-container display_block">
                        <div class="panel panel-default">
                            <div id="Tabs" role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs custnav custnav-im" role="tablist">
                                    <li id="liInventoryPricing"><a href="#InventoryPricing" aria-controls="InventoryPricing" role="tab" data-toggle="tab">Inventory Pricing </a></li>
                                    <li id="liBreakPrice"><a href="#BreakPrice" aria-controls="BreakPrice" role="tab" data-toggle="tab">Order
                                        & BreakPrice</a></li>
                                    <li id="liActivationSettings"><a href="#ActivationSettings" aria-controls="ActivationSettings" role="tab" data-toggle="tab">Activation Settings</a></li>
                                    <li id="liStockPurchase"><a href="#StockPurchase" aria-controls="StockPurchase" role="tab" data-toggle="tab">Stock & Purchase</a></li>
                                    <li id="liSales"><a href="#Sales" aria-controls="Sales" role="tab" data-toggle="tab">Sales</a></li>
                                    <li id="liChildInventory"><a href="#ChildInventory" aria-controls="ChildInventory" role="tab" data-toggle="tab">Child Inventory</a></li>
                                    <li id="liImage"><a href="#Image" aria-controls="Image" role="tab" data-toggle="tab">Image</a></li>
                                    <li id="liPromotion"><a href="#Promotion" aria-controls="Promotion" role="tab" data-toggle="tab">Promotion</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                                    <div class="tab-pane active" role="tabpanel" id="InventoryPricing">
                                        <div class="inv-price">
                                            <div class="selling">
                                                <div class="selling-left">
                                                    <div class="selling-header">
                                                        Selling Details
                                                    </div>
                                                    <div class="selling-detail">
                                                        <div class="radio-top" id="divpriceSetting">
                                                            <div class="purchased">
                                                                <asp:Label ID="Label36" runat="server" Text="Purchased By"></asp:Label>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbtCost" runat="server" GroupName="grpPurchased" />
                                                                Cost
                                                                <br />
                                                                <span class="mark">MarkUp</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbtMRP" TabIndex="1" runat="server" GroupName="grpPurchased" />
                                                                MRP1
                                                                <br />
                                                                <span class="mark">F.MarkDown(N.C)</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbnMRP2" runat="server" GroupName="grpPurchased" />
                                                                MRP2
                                                                <br />
                                                                <span class="mark">P.MarkDown(N.C)</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbnMRP3" runat="server" GroupName="grpPurchased" />
                                                                MRP3
                                                                <br />
                                                                <span class="mark">F.MarkDown(G.C)</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbnMRP4" runat="server" GroupName="grpPurchased" />
                                                                MRP4
                                                                <br />
                                                                <span class="mark">P.MarkDown(G.C)</span>
                                                            </div>
                                                            <%--<div class="radio-price">
                                                        <asp:RadioButton ID="rbtMAmt" runat="server" GroupName="grpPurchased" />
                                                        M Amt
                                                    </div>--%>
                                                            <div class="avg-price">
                                                                <div class="label-left" style="width: 45%; float: left">
                                                                    <asp:Label ID="Label76" runat="server" Text="HSN Code"></asp:Label>
                                                                </div>
                                                                <div class="label-right" style="width: 55%; float: right">
                                                                    <asp:TextBox ID="txtGSTGroup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-split">
                                                            <div class="control-group-left1">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label97" runat="server" Text="Selling Price"></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtFetchSellingPrice" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-middle" style="width: 60%">
                                                                <div class="label-left" style="width: 15%; margin-top: -8px">
                                                                    <asp:ImageButton ID="imgPriceChange" runat="server" ImageUrl="~/images/go_arrow.jpg"
                                                                        OnClientClick="fncOpenInvPriceChange(); return false;" onkeydown="return fncImageButtonkeydown(event);"
                                                                        ToolTip="Click to open selling detail" />
                                                                </div>
                                                                <%--OnClientClick="fncOpenInvPriceChange(); return false;"--%><%--onkeydown="return fncImageButtonkeydown(event);"--%>
                                                                <div class="inv_tooltip">
                                                                    <span>Click arrow  to open selling detail </span>
                                                                </div>

                                                                <div class="label-right" style="width: 85%">
                                                                    <span style="color: Blue">MRP : </span>
                                                                    <asp:Label ID="lblMRP" runat="server" Text="0.00" Style="color: Blue"></asp:Label>
                                                                    <asp:HiddenField ID="hdfMRP" runat="server" />
                                                                    &nbsp; <span style="color: Blue">Net Cost : </span>
                                                                    <asp:Label ID="lblNetCost" runat="server" Text="0.00" Style="color: Blue"></asp:Label>
                                                                    <asp:HiddenField ID="hdfNetCost" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="selling-middle" style="width: 24.8%">
                                                    <div id="divWholeSales" class="whole-price float_left">
                                                        <div class="whole-price-header">
                                                            Wholesale Price
                                                        </div>
                                                        <div class="whole-price-detail">
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label37" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice1 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice1" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label38" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice2 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice2" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label39" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice3 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice3" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div id="divLocationList" class="whole-price inv_LocationList">
                                                        <div class="whole-price-header">
                                                            Location
                                                        </div>
                                                        <div class="whole-price-detail">
                                                            <div class="freeItem_cbList" style="height: 122px;">
                                                                <div class="free_selectAll">
                                                                    <asp:CheckBox ID="cbSelectAll" runat="server" Text="Select All" />
                                                                </div>
                                                                <div>
                                                                    <asp:CheckBoxList ID="cblLocation" runat="server">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divBarcode" class="selling-right" runat="server">
                                                    <div class="barcode">
                                                        <div class="barcode-header">
                                                            Barcode & Shortcode
                                                        </div>
                                                        <div class="barcode-detail">

                                                            <div class="Payment_fixed_headers tbl_barcode">
                                                                <table id="tblBarcode" cellspacing="0" rules="all" border="1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Delete
                                                                            </th>
                                                                            <th scope="col">Barcode
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>

                                                            </div>


                                                            <div class="control-group-split" style="padding: 4px 0">
                                                                <%--<asp:Panel ID="Panel1" runat="server" DefaultButton="lnkInsert">--%>
                                                                <div class="control-group-left" style="width: 64%">
                                                                    <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res" MaxLength="20" onkeydown=" return fncBarcodeKeyDown(event);"></asp:TextBox>
                                                                </div>
                                                                <div class="control-group-right" style="width: 34%">
                                                                    <%--<asp:Button ID="btnBarcodeInsert" Text="Insert" runat="server" OnClick="lnkInsert_Click" />--%>
                                                                    <asp:Button ID="lnkInsert" runat="server" class="button-blue" OnClientClick="fncAddBarcode();return false;" Text="Insert" Width="100%"></asp:Button>
                                                                </div>
                                                                <%--</asp:Panel>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <center>
                                                        <asp:LinkButton ID="lblRules" OnClientClick="fncRules();return false;" runat="server" Style="cursor: pointer; color: blue; display: none;">Active Rules</asp:LinkButton>
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label id="lblMasterOpen" style="color: white; background: green; margin-left: 175px;"></label>
                                            </div>
                                            <div id="dialog-confirm" title="Enterpriser">
                                                <p>
                                                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>This is batch Item. Want to go BatchInfo ?
                                                </p>
                                            </div>
                                            <div id="dialog-clear-confirm" title="Enterpriser">
                                                <p>
                                                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>Do you want to Clear ?
                                                </p>
                                            </div>
                                            <div class="hiddencol">
                                                <div id="dialog-InvPrice" title="Selling Price Calculation">
                                                    <div class="dialog-inv EnableScroll">
                                                        <div class="dialog-inv-header">
                                                            <div class="control-group-split">
                                                                <div class="control-group-left" style="width: 30%">
                                                                    <div class="label-left" style="width: 40%">
                                                                        <asp:Label ID="Label87" runat="server" Text="Item Code"></asp:Label>
                                                                    </div>
                                                                    <div class="label-right" style="width: 60%">
                                                                        <asp:TextBox ID="txtItemCodePopup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group-right" style="width: 68%">
                                                                    <div class="label-left" style="width: 20%">
                                                                        <asp:Label ID="Label89" runat="server" Text="Item Name"></asp:Label>
                                                                    </div>
                                                                    <div class="label-right" style="width: 80%">
                                                                        <asp:TextBox ID="txtItemNamePopup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-header">
                                                                Input Tax
                                                            </div>
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label92" runat="server" Text="IGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputIGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label90" runat="server" Text="SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputSGST" runat="server" CssClass="form-control-res" Width="100%">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-right1 inv_purchasebyPartial_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label91" runat="server" Text="CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputCGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-header">
                                                                Output Tax
                                                            </div>
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label95" runat="server" Text="IGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputIGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label93" runat="server" Text="SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputSGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-right1 inv_purchasebyPartial_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label94" runat="server" Text="CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputCGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="grn_net_lbl3" style="width: 38%;">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label96" runat="server" Text="Cess %"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtCESSPrc" runat="server"
                                                                                    CssClass="form-control-res text-right" onkeydown="fncCessChange(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grn_net_lbl3 inv_paddingleft" style="width: 62%;">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label106" runat="server" Text="Adl.Cs.Amt"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtAddCessAmt" runat="server" CssClass="form-control-res text-right" Text="0.00"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        

                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label112" runat="server" Text="HSN Code"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtHSNCode" MaxLength="8" runat="server" onfocusout="fncHSNcodevalidation();" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                                    <div class="control-group-right1 inv_purchasebyPartial_width inv_rbn_purchaseborder">
                                                                                <div class="label-left" style="width: 100%">
                                                                                    <asp:RadioButton ID="rbtCostPopup" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                                    <span id="markup" class="inv_rbn_purchaseby">Cost (MarkUp)  </span>&nbsp;
                                                                                <asp:RadioButton ID="rbtMrpPopup" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                                    <span id="markdown" class="inv_rbn_purchaseby">MRP (MarkDown)  </span>&nbsp;
                                                                                 <asp:RadioButton ID="rbtPartialMD" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                                    <span id="PMD" class="inv_rbn_purchaseby">MRP(PMD)  </span>
                                                                                    <asp:RadioButton ID="rbtMD3Popup" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                                    <span id="MD3" class="inv_rbn_purchaseby">MRP3(MD)  </span>
                                                                                    <asp:RadioButton ID="rbtPMD4opup" runat="server" GroupName="PurchaseBy" Enabled="false" Style="margin-left: 30px;" />
                                                                                    <span id="MD4" class="inv_rbn_purchaseby">MRP4(PMD)  </span>
                                                                                </div>
                                                                                <div class="label-right" style="width: 0%">
                                                                                </div>
                                                                            </div>
                                                                    <div id ="divNewCalculation" class="control-group-split" runat="server"> 
                                                                        <div class ="control-group-left1 inv_purchaseby_width" style="margin-top:-15px;">  
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label102" runat="server" Text="Purchase"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:DropDownList ID="ddlPurchase" runat="server" CssClass="form-control-res" Width="100%">                                         <asp:ListItem Value="Cost">MarkUp</asp:ListItem>
                                                                                    <asp:ListItem Value="Mrp">MarkDown</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            </div>  
                                                                        </div>
                                                                         <div class ="control-group-middle inv_purchaseby_width" style="margin-top:-15px;">  
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label103" runat="server" Text="Sales"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:DropDownList ID="ddlSales" runat="server" CssClass="form-control-res">                                         <asp:ListItem Value="Cost">MarkUp</asp:ListItem>
                                                                                    <asp:ListItem Value="Mrp">MarkDown</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            </div> 
                                                                             </div>

                                                                        </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="dialog-inv-set-price">
                                                            <div class="dialog-inv-container-set-price-header">
                                                                <%--<div class ="col-md-12 dialog-inv-container-set-price-header" > 
                                                                      <div class ="col-md-2" style ="margin-top: 3px;">--%> 
                                                                Selling Price
                                                              <%--  </div>--%>
                                                                <%--  <div class ="col-md-3" id="divGenerateMrp" style ="margin-top: 3px;"> 
                                                                <asp:CheckBox ID="chkGenerateMrp" CssClass="checkbox1" runat="server" Text="Generate MRP"/>
                                                                </div>
                                                                    </div>--%>
                                                            </div>
                                                            <div class="dialog-inv-container-set-price-detail">
                                                                <div class="set-price-left">
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="lblAdDiscount" runat="server" Text="MRP"></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right" style="width: 26%" runat="server" id="divMrp">
                                                                            <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res text-right" onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown=" return fncInventoryKeyPress(event,'MRP'); "></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 17%" runat="server" id="divDis">
                                                                            <asp:TextBox ID="txtMRPMarkDown" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown=" return fncInventoryKeyPress(event,'MRPMarkDown');"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 17%" runat="server" id="divAddDis">
                                                                            <asp:TextBox ID="txtAdDiscount" runat="server" Text="0" CssClass="form-control-res text-right"
                                                                                onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown=" return fncInventoryKeyPress(event,'AdDiscount');"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res" id ="divBasicMargin" runat="server">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label101" runat="server" Text="BasicMargin"></asp:Label> 
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtBasicMargin" runat="server" CssClass="form-control-res text-right" onfocus="this.select();" Text="0"
                                                                                onkeydown=" return fncInventoryKeyPress(event,'BasicMargin'); "></asp:TextBox> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label66" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicCost %>'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtBasicCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdfBasicCost" runat="server" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label67" runat="server" Text="Discount(%)"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc1" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc2" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc3" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown=" return fncInventoryKeyPress(event,'txtDiscountPrc3');" onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 21%">
                                                                            <asp:TextBox ID="txtDiscountAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                     <div class="control-group-single-res" id ="divAffectNetCost">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label123" runat="server" Text="Affect S.Price"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                             <asp:CheckBox ID="chkAffectNetCost" CssClass="checkbox1" runat="server" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label72" runat="server" Text='<%$ Resources:LabelCaption,lbl_GrossCost %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtGrossCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label73" runat="server" Text="Input SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputSGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputSGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label12" runat="server" Text="Input CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputCGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label113" runat="server" Text="CESS"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputCESSPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputCESSAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label74" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetCost %>'></asp:Label><span
                                                                                class="mandatory">#</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtNetCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res inv_margin_border">
                                                                        <div class="col-md-12">
                                                                            <div class="col-md-6">
                                                                                <label id="lblMrp" style="font-weight: 700; color: blue;">Purchase</label>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <label id="lblSP" style="font-weight: 700; color: blue;">Selling Price</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 control-group-single-res">
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label65" runat="server" Text="Margin"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtMRPMargrin" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label107" runat="server" Text="Margin"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtsellMargin" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 control-group-single-res">
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label108" runat="server" Text="MarkUp %"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtMRPMarkUp" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label109" runat="server" Text="MarkUp %"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtSellMarkUp" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 control-group-single-res">
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label110" runat="server" Text="MarkDown%"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtMRPMarkDownNew" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:Label ID="Label111" runat="server" Text="MarkDown%"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtSellMarkDown" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 control-group-single-res">
                                                                             <div class="col-md-6">
                                                                                <asp:Label ID="Label124" runat="server" Text="M.DownOnMRP(NSP)%"></asp:Label>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <asp:TextBox ID="txtMDownMrp" Text="0" runat="server" CssClass="form-control-res text-right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)" ReadOnly="true"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="set-price-right">

                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label82" runat="server" Text='<%$ Resources:LabelCaption,lbl_SPMarginper %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtFixedMargin" runat="server" CssClass="form-control-res text-right"
                                                                                onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown="return fncInventoryKeyPress(event,'FixedMargin');"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label75" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicSelling %>'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtBasicSelling" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label33" runat="server" Text="Output SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputSGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputSGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label34" runat="server" Text="Output CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputCGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label114" runat="server" Text="CESS"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputCESSPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputCESSAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label80" runat="server" Text='<%$ Resources:LabelCaption,lbl_RoundOff %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtRoundOff" runat="server" Enabled="false" CssClass="form-control-res text-right"
                                                                                Text="0.00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label78" runat="server" Text='NetSellingPrice'></asp:Label><span
                                                                                class="mandatory">#</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res text-right"
                                                                                onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown="return fncInventoryKeyPress(event,'SellingPrice');"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res ">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label81" runat="server" Text="Lia(GST+Cess)"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtGSTLiabilityPer" runat="server" Enabled="false" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtGSTLiability" runat="server" Enabled="false" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res  display_none">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label77" runat="server" Text="SGST Liability"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtVatLiabilitySGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtVatLiabilitySGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res display_none">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label35" runat="server" Text="CGST Liability"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtVatLiabilityCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtVatLiabilityCGSTAMt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res" id="divInfo" runat="server">
                                                                        <div class="col-md-12">
                                                                            <label style="color: blue; margin-left: -5px;">Includes Additional Cess Amount </label>
                                                                            <span
                                                                                class="mandatory">#</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="set-price-middle">
                                                                    <div class="inv_margin_border">
                                                                        <span class="inv_margin_hdr">Net Margin from MRP</span>
                                                                        <div class="display_table">
                                                                            <div class="float_left inv_margin_lbl">
                                                                                <asp:Label ID="Label40" runat="server" Text="Percentage(%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMarginMRP" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_margin_amt">
                                                                                <asp:Label ID="Label83" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMRPAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="inv_margin_border inv_netmarginearned">
                                                                        <span class="inv_margin_hdr">Net Margin from Sellingprice</span>
                                                                        <div class="display_table">
                                                                            <div class="float_left inv_margin_lbl">
                                                                                <asp:Label ID="Label79" runat="server" Text="Percentage(%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMarginSP" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_margin_amt">
                                                                                <asp:Label ID="Label84" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtSPAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-split" style="display: none">
                                                                        <div class="control-group-left1" style="width: 38%">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label85" runat="server" Text="Profit%"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtProfitMRPPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-middle" style="width: 39%">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label86" runat="server" Text="Profit%"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtProfitSPPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <asp:Panel ID="pnlWholeSalePrice" CssClass="inv_whole_border" runat="server">
                                                                        <span class="inv_margin_hdr">Whole Sale Price</span>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <span style="font-size: 15px; font-weight: 700; color: green;">FixedMargin</span>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <span style="font-size: 15px; font-weight: 700; color: green;">EarnedMargin</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:TextBox ID="txtMarginWPrice1" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" Style="width: 80%;"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtEarnedMar1" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" Text="0"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label98" runat="server" Text="WPrice1"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice1Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:TextBox ID="txtMarginWPrice2" Style="width: 80%;" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtEarnedMar2" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" Text="0"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label99" runat="server" Text="WPrice2"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice2Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:TextBox ID="txtMarginWPrice3" Style="width: 80%;" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" onkeypress=" return fncInventoryKeyPress(event,'WPrice3Per');"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtEarnedMar3" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" Text="0" onkeypress=" return fncInventoryKeyPress(event,'WPrice3Per');"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label100" runat="server" Text="WPrice3"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice3Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeypress="return isNumberKeyWithDecimalNew(event);" onkeydown="return fncInventoryKeyPress(event,'WPrice3');"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField ID="hdfSellingPriceRoundOff" runat="server" />
                                                    <asp:HiddenField ID="hdfMarkDown" runat="server" />
                                                    <asp:LinkButton ID="lnkSaveChangeMem" runat="server" class="button-blue" Text="Ok"
                                                        Visible="false"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="BreakPrice">
                                        <div class="breakprice">
                                            <div class="order-process order-process-width">
                                                <div style="margin: 0 10px 0px 40%">
                                                    <span style="color: Blue; font-weight: bold">Re-Order Process </span>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left order_txtwidth">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label41" runat="server" Text="Minimum Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtMinimumQty" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right" style="width: 38%">
                                                        <div class="label-left" style="width: 20%">
                                                            <asp:Label ID="Label42" runat="server" Text="By"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 80%">
                                                            <asp:DropDownList ID="ddlMiniBy" runat="server" CssClass="form-control-res">
                                                                <asp:ListItem Value="2">Manual</asp:ListItem>
                                                                <asp:ListItem Value="1">Demand Forecast</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split order_txtwidth1">
                                                    <div class="control-group-left" style="width: 60%">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label43" runat="server" Text="Max Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtMaxQty" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left order_txtwidth">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label45" runat="server" Text="ReOrder Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtReOrderQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right" style="width: 38%">
                                                        <div class="label-left" style="width: 20%">
                                                            <asp:Label ID="Label46" runat="server" Text="By"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 80%">
                                                            <asp:DropDownList ID="ddlByReOrder" runat="server" CssClass="form-control-res">
                                                                <asp:ListItem Value="3">Manual</asp:ListItem>
                                                                <asp:ListItem Value="1">Lead Time</asp:ListItem>
                                                                <asp:ListItem Value="2">Seasonal</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split order_txtwidth1">
                                                    <div class="control-group-left" style="width: 60%">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label47" runat="server" Text="Order By"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlOrderBy" runat="server" onchange="fncOrderByChanged();" CssClass="form-control-res">
                                                                <asp:ListItem Value="1">Order Qty</asp:ListItem>
                                                                <asp:ListItem Value="2">Cyc.Days</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="control-group-split">
                                                    <div id="divcycledays" class="control-group-left order_txtwidth display_none">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label105" runat="server" Text="Cycle Days"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtCycleDays" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event);"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right breakprice_btn">
                                                        <asp:LinkButton ID="lnkbreakpriceQty" class="button-blue breakprice_btn1" Text='<%$ Resources:LabelCaption,lblBreakPriceQty %>'
                                                            OnClientClick="fncShowBreakPriceQty();return false;" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div id="divrepeater" class="poList">
                                                    <table id="tblga" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">S.No
                                                                </th>
                                                                <th scope="col">Supplier
                                                                </th>
                                                                <th scope="col">PO No
                                                                </th>
                                                                <th scope="col">PO Date
                                                                </th>
                                                                <th scope="col">W.Qty
                                                                </th>
                                                                <th scope="col">L.Qty
                                                                </th>
                                                                <th scope="col">F.Qty
                                                                </th>
                                                                <th scope="col">Net Cost
                                                                </th>
                                                                <th scope="col">MRP
                                                                </th>
                                                                <th scope="col">Sellingprice
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptrPoList" runat="server">
                                                            <HeaderTemplate>
                                                                <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblVendorname" runat="server" Text='<%# Eval("VendorName") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPono" runat="server" Text='<%# Eval("PoNo") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPodate" runat="server" Text='<%# Eval("PODate") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblWQty" runat="server" Text='<%# Eval("WQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLQty" runat="server" Text='<%# Eval("LQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblFocQty" runat="server" Text='<%# Eval("FocQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblNetcost" runat="server" Text='<%# Eval("NetCost") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSellingprice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="ActivationSettings">
                                        <div class="activation">
                                            <div class="activation-left">
                                                <div class="inv_settinghdr">
                                                    <span>General Activation Settings </span>
                                                </div>
                                                <div class="activation-check-left activation-check-width">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkExpiryDate" onkeydown=" return fncCheckBoxKeyDown(event,'chkExpiryDate');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label104" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDate %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSerialNo" onkeydown="return fncCheckBoxKeyDown(event,'chkSerialNo');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label48" runat="server" Text='<%$ Resources:LabelCaption,lbl_SerialNoRequired %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkAutoPO" onkeydown="return fncCheckBoxKeyDown(event,'chkAutoPO');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label49" runat="server" Text='<%$ Resources:LabelCaption,lbl_AutoPOAllowed %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPackage" onkeydown="return fncCheckBoxKeyDown(event,'chkPackage');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label50" runat="server" Text='<%$ Resources:LabelCaption,lbl_PackageItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkLoyality" onkeydown="return fncCheckBoxKeyDown(event,'chkLoyality');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label51" runat="server" Text='<%$ Resources:LabelCaption,lbl_Loyality %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSpecial" onkeydown="return fncCheckBoxKeyDown(event,'chkSpecial');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label52" runat="server" Text='<%$ Resources:LabelCaption,lbl_Special %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" id="hideChckboxMemDis">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkMemDis" onkeydown="return fncCheckBoxKeyDown(event,'chkMemDis');" CssClass="checkbox1" runat="server" Checked="false" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label53" runat="server" Text='<%$ Resources:LabelCaption,lbl_MemDisAllowed %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" id="hideChckboxSeas">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSeasonal" onkeydown="return fncCheckBoxKeyDown(event,'chkSeasonal');" CssClass="checkbox1" runat="server" Checked="false" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label54" runat="server" Text='<%$ Resources:LabelCaption,lbl_SeasonalItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right activation-check-width">
                                                    <%--<div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSeasonal" onkeydown="return fncCheckBoxKeyDown(event,'chkSeasonal');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label54" runat="server" Text='<%$ Resources:LabelCaption,lbl_SeasonalItem %>'></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                    <%-- <div class="checkbox-control">
                                                       <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkBatch" onkeydown="return fncCheckBoxKeyDown(event,'chkBatch');" CssClass="checkbox1" runat="server" />
                                                        </div> 
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label55" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batch %>'></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkShelf" onkeydown="return fncCheckBoxKeyDown(event,'chkShelf');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label56" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfStorage %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkHideData" onkeydown="return fncCheckBoxKeyDown(event,'chkHideData');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label57" runat="server" Text='<%$ Resources:LabelCaption,lbl_HideDataOnBarcode %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkMarking" onkeydown="return fncCheckBoxKeyDown(event,'chkMarking');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label58" runat="server" Text='<%$ Resources:LabelCaption,lbl_MarkingItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkBarcodePrint" onkeydown="return fncCheckBoxKeyDown(event,'chkBarcodePrint');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label59" runat="server" Text='<%$ Resources:LabelCaption,lbl_NeedBarcodePrint %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkAllowCost" onkeydown="return fncCheckBoxKeyDown(event,'chkAllowCost');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label60" runat="server" Text='<%$ Resources:LabelCaption,lbl_AllowCostEdit %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbWeightbased" onkeydown="return fncCheckBoxKeyDown(event,'cbWeightbased');" CssClass="checkbox1" Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblWeightbased" runat="server" Text='Weight Based'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control display_none">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbAdditionalCess" onkeydown="return fncCheckBoxKeyDown(event,'cbAdditinalCess');" CssClass="checkbox1 " Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblAdditionalCess" runat="server" Text='Additional Cess'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkWholeSale" Checked ="true" onkeydown="return fncCheckBoxKeyDown(event,'cbWholeSale');" CssClass="checkbox1 "   runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label55" runat="server" Text='Whole Sale'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right activation-check-width">
                                                    <%-- <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbAdditionalCess" onkeydown="return fncCheckBoxKeyDown(event,'cbAdditinalCess');" CssClass="checkbox1" Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblAdditionalCess" runat="server" Text='Additional Cess'></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label44" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDays %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtExpiryDays" onkeydown="return fncCheckBoxKeyDown(event,'txtExpiryDays');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label116" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfQty %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtShelfQty" onkeydown="return fncCheckBoxKeyDown(event,'txtShelfQty');" runat="server" MaxLength="5" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label115" runat="server" Text="Rating"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtRating" onkeydown="return fncCheckBoxKeyDown(event,'txtRating');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="float_left">
                                                        <div class="inv_selectall">
                                                            <asp:CheckBox ID="chkSelectAll" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAll');" runat="server" />
                                                        </div>
                                                        <div class="float_left">
                                                            <span style="color: Blue">Select All</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="activation-right">
                                                <div class="inv_settinghdr">
                                                    <span>Transaction Activation Settings </span>
                                                </div>
                                                <div class="activation-check-left">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPurchaseOrder" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseOrder');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkGRN" onkeydown="return fncCheckBoxKeyDown(event,'chkGRN');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label62" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNPurchase %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPurchaseReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseReturn');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label63" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseReturn %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPointOfSale" onkeydown="return fncCheckBoxKeyDown(event,'chkPointOfSale');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label64" runat="server" Text='<%$ Resources:LabelCaption,lbl_POS %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkAllowNeg" onkeydown="return fncCheckBoxKeyDown(event,'chkAllowNegative');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label117" runat="server" Text="Allow Negative"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkTransfer" onkeydown="return fncCheckBoxKeyDown(event,'chkTransfer');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label118" runat="server" Text='Transfer(DC)'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSalesReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkSalesReturn');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label68" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesReturn %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkOrderBooking" onkeydown="return fncCheckBoxKeyDown(event,'chkOrderBooking');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label69" runat="server" Text='<%$ Resources:LabelCaption,lbl_OrderBooking %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkEstimate" onkeydown="return fncCheckBoxKeyDown(event,'chkEstimate');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label70" runat="server" Text='<%$ Resources:LabelCaption,lbl_Estimate %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkQuatation" onkeydown="return fncCheckBoxKeyDown(event,'chkQuatation');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lbl_Quatation %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" style="display: none">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkDisGrn" onkeydown="return fncCheckBoxKeyDown(event,'chkDisGrn');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label119" runat="server" Text='AllowDiscount(GRN)'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" style="display: none">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkDisPo" onkeydown="return fncCheckBoxKeyDown(event,'chkDisPo');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label120" runat="server" Text='AllowDiscount(PO)'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" id="divPriceWatch">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="ChkPriceWList" onkeydown="return fncCheckBoxKeyDown(event,'ChkPriceWList');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label121" runat="server" Text='Price Watch List'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control" id="divOnline">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkOnlineOrder" onkeydown="return fncCheckBoxKeyDown(event,'OnlineOrder');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblOnlineOrder" runat="server" Text='Online Order'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSelectAllRight" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAllRight');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <span style="color: Blue">Select All</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="StockPurchase">
                                        <div style="height: 35px;" class="stockgrid-top">
                                            <table rules="all" border="1" id="tblgvStockTopdummy" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <%--style="font-family: Arial; font-size: 10pt; width: 600px; color: black; border-collapse: collapse; height: 100%;"--%>
                                                <tr>
                                                    <td style="text-align: center; width: 115px">Location Code</td>     <%--musaraf24112922 table alignment--%>
                                                    <td style="text-align: center; width: 75px">L.Q.O.H</td>
                                                    <td style="text-align: center; width: 75px">W.Q.O.H</td>
                                                    <td style="text-align: center; width: 116px">Sold</td>
                                                    <td style="text-align: center; width: 115px">Min</td>
                                                    <td style="text-align: center; width: 115px">Max</td>
                                                    <td style="text-align: center; width: 115px">ReOrder</td>
                                                    <td style="text-align: center; width: 115px">Price</td>
                                                    <td style="text-align: center; width: 114px">Avg Cost</td>
                                                    <td style="text-align: center; width: 115px">MRP</td>
                                                    <td style="text-align: center; width: 115px">GST</td>
                                                    <td style="text-align: center; width: 140px">Cyc Days</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="stockgrid-top GridDetails">
                                            <asp:GridView ID="gvStockTop" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="75px" ItemStyle-CssClass="text-right" DataField="QtyOnHand" HeaderText="L.Q.O.H"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="75px" NullDisplayText="" HeaderText="W.Q.O.H"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="QtySold" HeaderText="Sold"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MINQTY" HeaderText="Min"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MAXQTY" HeaderText="Max"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="REORDERLEVEL" HeaderText="ReOrder"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="Price" HeaderText="Price"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="AvgCost" HeaderText="Avg Cost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="ITaxPer3" HeaderText="GST"></asp:BoundField>
                                                    <asp:BoundField DataField="REORDERDAYS" ItemStyle-CssClass="text-left" HeaderText="Cyc Days"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div style="height: 35px;" class="stockgrid-top">
                                            <table rules="all" border="1" id="Table1" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <%--style="font-family: Arial; font-size: 10pt; width: 600px; color: black; border-collapse: collapse; height: 100%;"--%>
                                                <tr>
                                                    <td style="text-align: center; width: 160px">Supplier</td>            <%--musaraf24112922 table alignment--%>
                                                    <td style="text-align: center; width: 114px">DO/INV</td>
                                                    <td style="text-align: center; width: 113px">DO/Inv Date</td>
                                                    <td style="text-align: center; width: 97px">WQty</td>
                                                    <td style="text-align: center; width: 99px">LQty</td>
                                                    <td style="text-align: center; width: 114px">FocQty</td>
                                                    <td style="text-align: center; width: 123px">Basic Cost</td>
                                                    <td style="text-align: center; width: 114px">Net Cost</td>
                                                    <td style="text-align: center; width: 112px">MRP</td>
                                                    <td style="text-align: center; width: 114px">Selling Price</td>
                                                    <td style="text-align: center; width: 163px">Gid No</td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div class="stockgrid-bottom GridDetails">
                                            <asp:GridView ID="gvStockBottom" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="160px" ItemStyle-CssClass="text-left" DataField="VendorName" HeaderText="Supplier"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="DONo" HeaderText="DO/INV"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="DODate" HeaderText="DO/Inv Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="100px" ItemStyle-CssClass="text-right" DataField="WQty" HeaderText="WQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="100px" ItemStyle-CssClass="text-right" DataField="LQty" HeaderText="LQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="FocQty" HeaderText="FocQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="125px" ItemStyle-CssClass="text-right" DataField="UnitCost" HeaderText="Basic Cost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="NetCost" HeaderText="NetCost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="GidNo" ItemStyle-CssClass="text-left" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkGidNo" runat="server" Text='<%# Eval("GidNo") %>' OnClientClick="fncGoGRNPage(this);return false;"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="POReceiptNo" HeaderText="GA No" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="vendorcode" HeaderText="Vendorcode" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="BillType" HeaderText="Billtype" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="LocationCode" HeaderText="Location" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="control-container">
                                            <div class="control-button">
                                                <asp:Button ID="btnView" runat="server" class="button-blue" Text="Stock" OnClientClick="fncShowView();return false;" />                                           
                                            </div>

                                            <div class="control-button">
                                                <asp:Button ID="btnPriceChgLog" runat="server" class="button-blue" Text="Price Change Log" OnClientClick="fncShowPriceChangeLog();return false;" />
                                            </div>
                                            <div class="control-button">
                                                <asp:Button ID="btnTransferDet" runat="server" class="button-blue" Text="Transfer Detail" OnClientClick="fncShowTransferDetail();return false;" />
                                            </div>
                                            <div class="control-button">
                                                <asp:Button ID="btnInvMovement" runat="server" class="button-blue" Text="Movement" OnClientClick="fncShowInventoryMovement();return false;" />
                                            </div>
                                            <div class="control-button">
                                                <asp:Button ID="btnPRReq" runat="server" OnClientClick="fncGetBatchDetAndPRReqDetail('7');return false;" class="button-blue" Text="PR Request" />
                                            </div>
                                            <div class="control-button">
                                                <asp:Button ID="btnBatch" runat="server" class="button-blue" Text="Batch" OnClientClick="fncGetBatchDetAndPRReqDet('6');return false;" />
                                            </div>
                                        </div>
                                        <div id="dialog-batch" title="Batch Detail" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="gvBatchPopup" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                                        <asp:BoundField DataField="BalanceQty" HeaderText="Q.O.H"></asp:BoundField>
                                                        <asp:BoundField DataField="InwardQty" HeaderText="Inward Qty"></asp:BoundField>
                                                        <asp:BoundField DataField="SoldQty" HeaderText="SoldQty"></asp:BoundField>
                                                        <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                                           <asp:BoundField DataField="CreateDate" HeaderText="CreateDate"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>                                        
                                            
                                       <div id="dialog-PRReq" title="Purchase Return Request" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="gvPRReq" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="PRReqNo" HeaderText="PR Req No"></asp:BoundField>
                                                        <asp:BoundField DataField="ReqDate" HeaderText="Req Date"></asp:BoundField>
                                                        <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                        <asp:BoundField DataField="PRReqQty" HeaderText="PR Req Qty"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>                                                
                                        </div>                                       

                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Sales">
                                        <div class="Barcode_fixed_headers saleshistory" id="Saleshistory_id">
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesMonth" runat="server" Checked="true" Text="Month" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesWeek" runat="server" Text="Week" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesDay" runat="server" Text="Day" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <%-- <div id="dialog-ViewItemHistory" class="container-center" title="Item History">
                                            <ups:ItemHistory runat="server" ID="itemHistory" />
                                        </div>--%>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="ChildInventory">
                                        <div class="Barcode_fixed_headers childitems">
                                            <table id="tblChildInventory" cellspacing="0" rules="all" border="1">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">S.No
                                                        </th>
                                                        <th scope="col">Inventorycode
                                                        </th>
                                                        <th scope="col">Description
                                                        </th>
                                                        <th scope="col">UOM
                                                        </th>
                                                        <th scope="col">QTY.ON.HAND
                                                        </th>
                                                        <th scope="col">SOLD.QTY
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptrchild" runat="server">
                                                    <HeaderTemplate>
                                                        <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr id="childRow" runat="server" onclick="fncChildRowClick(this);">
                                                            <td>
                                                                <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblChildInventory" runat="server" Text='<%# Eval("INVENTORYCODE") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblChildDesc" runat="server" Text='<%# Eval("Description") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblQtyonhand" runat="server" Text='<%# Eval("QtyOnHand") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblQtySold" runat="server" Text='<%# Eval("QtySold") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="Barcode_fixed_headers saleshistory child_invsalestop" id="childSaleshistory_id">
                                        </div>
                                        <div id="divchildSales" class="display_none">
                                            <div class="saleshistory_inv">
                                                <asp:RadioButton ID="rbnMonth" runat="server" Checked="true" Text="Month" GroupName="childsales" onchange="fncChildSaleshistorycheckchanged();" />
                                            </div>
                                            <div class="saleshistory_inv">
                                                <asp:RadioButton ID="rbnWeek" runat="server" Text="Week" GroupName="childsales" onchange="fncChildSaleshistorycheckchanged();" />
                                            </div>
                                            <div class="saleshistory_inv">
                                                <asp:RadioButton ID="rbnDay" runat="server" Text="Day" GroupName="childsales" onchange="fncChildSaleshistorycheckchanged();" />
                                            </div>
                                        </div>
                                        <div>
                                        </div>
                                        <div id="Div1" title="Batch Detail" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                                        <asp:BoundField DataField="BalanceQty" HeaderText="Q.O.H"></asp:BoundField>
                                                        <asp:BoundField DataField="InwardQty" HeaderText="Inward Qty"></asp:BoundField>
                                                        <asp:BoundField DataField="SoldQty" HeaderText="SoldQty"></asp:BoundField>
                                                        <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div id="Div2" title="Purchase Return Request" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="PRReqNo" HeaderText="PR Req No"></asp:BoundField>
                                                        <asp:BoundField DataField="ReqDate" HeaderText="Req Date"></asp:BoundField>
                                                        <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                        <asp:BoundField DataField="PRReqQty" HeaderText="PR Req Qty"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Image">
                                        <div class="control-group" style="margin: 0 auto">
                                            <asp:Image ID="imgpreview" runat="server" Height="130px" Width="250px" />
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSave" />
                                                </Triggers>
                                                <ContentTemplate>
                                                    <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Promotion">
                                        <div class="promotiongrid-header">
                                            Promotion Details
                                        </div>
                                        <div style="height: 35px">
                                            <table rules="all" border="1" id="Table2" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <%--style="font-family: Arial; font-size: 10pt; width: 600px; color: black; border-collapse: collapse; height: 100%;"--%>
                                                <tr>
                                                    <td style="text-align: center; width: 115px">Promotion Code</td>
                                                    <td style="text-align: center; width: 128px">Promotion Desc</td>
                                                    <td style="text-align: center; width: 80px">Batch No</td>
                                                    <td style="text-align: center; width: 115px">Location</td>
                                                    <td style="text-align: center; width: 115px">Qty</td>
                                                    <td style="text-align: center; width: 115px">Sold Qty</td>
                                                    <td style="text-align: center; width: 115px">Initial Qty</td>
                                                    <td style="text-align: center; width: 115px">Promotion From Date</td>
                                                    <td style="text-align: center; width: 115px">Promotion To Date</td>
                                                    <td style="text-align: center; width: 115px">Discount Perc</td>
                                                    <td style="text-align: center; width: 115px">Create User</td>
                                                    <td style="text-align: center; width: 115px">Create Date</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="promotiongrid">
                                            <asp:GridView ID="gvPromotionGrid" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionCode" HeaderText="Promotion Code"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="130px" ItemStyle-CssClass="text-left" DataField="PromotionDescription" HeaderText="Promotion Desc"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="80px" ItemStyle-CssClass="text-left" DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="Location" HeaderText="Location"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="Qty" HeaderText="Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="SoldQty" HeaderText="Sold Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="InitialQty" HeaderText="Initial Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionFromDate" HeaderText="Promotion From Date"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionToDate" HeaderText="Promotion To Date"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="DiscountPerc" HeaderText="Discount Perc"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="CreateUser" HeaderText="Create User"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="CreateDate" HeaderText="Create Date"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="control-container">
                                            <div class="control-button">
                                                <asp:Button ID="btnLiveDetail" runat="server" class="button-blue" Text="Live Detail"
                                                    OnClick="btnLiveDetail_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField ID="TabName" runat="server" />
                            <asp:HiddenField ID="HiddenNewitem" runat="server" ClientIDMode="Static" Value="NO" />
                            <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="" />
                        </div>
                    </div>
                </div>
				 </ContentTemplate>
        </asp:UpdatePanel>               
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_Click"
                            OnClientClick="Validate();return false;" Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClick="lnkClear_clik"
                            Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkItemHistory" runat="server" class="button-blue" PostBackUrl="~/Inventory/frmInventoryMasterView.aspx"
                            Text="Item List(F8)"></asp:LinkButton>
                    </div>
                    <div class="hiddencol">
                        <asp:Button ID="btnBarcodeclear" runat="server" OnClick="btnBarcodeclear_Click" />
                    </div>
                </div>
            </div>
            </div>
        
       
    
        <asp:UpdatePanel ID="updtInvSave" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton ID="lnkEditItemCode" runat="server" class="button-blue display_none" Text="ItemCode"
                    OnClick="lnkEditItemCode_Click"></asp:LinkButton>
                <div>
                    <div id="userdetail" class="float_left">
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Created Date: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblCreateddate"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Created User: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblCreateuser"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Modify Date: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblModifydate"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Modify User: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblModifyuser"></asp:Label>
                            </div>
                        </div>
                    </div>

                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
     
    <div class="hiddencol">
        <div id="dialog-DuplicateInventoryName">
            <div>
                <asp:Label ID="lblGidSave" runat="server" Text="This Ivnentory name already exists.Do you want save duplicate name?"></asp:Label>
            </div>
            <div class="float_right">
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkDupNameYes" runat="server" Text="Yes" class="button-blue" OnClientClick="fncDupNameYesClick();return false;"></asp:LinkButton>
                </div>
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkDupNameNo" runat="server" Text="No" class="button-blue" OnClientClick="fncDupNameNoClick();return false;"></asp:LinkButton>
                </div>
            </div>

        </div>
        <div id="div-BreakPriceQty">
            <div class="breakpricehdr">
                <div class="breakpriceQty_lbl">
                    <asp:Label ID="lblBreakPriceItem" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                </div>
                <div class="breakpriceitem_lbl">
                    <asp:TextBox ID="txtBreakPriceItem" ReadOnly="true" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="breakpriceQty_lbl">
                    <asp:Label ID="lblBreakPriceDesc" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                </div>
                <div class="float_left">
                    <asp:TextBox ID="txtBreakPriceDesc" ReadOnly="true" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="breakpriceQty_lbl">
                    <asp:CheckBox ID="chbrkNetCostVal" runat="server" Checked="true" Text='<%$ Resources:LabelCaption,msg_NetCostVal %>' />
                </div>
            </div>
            <div class="Payment_fixed_headers breakPrice">
                <table id="tblBreakPriceQty" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Batch No
                            </th>
                            <th scope="col">Break Qty
                            </th>
                            <th scope="col">Break(Price/Perc) 
                            </th>
                            <th scope="col">Price Type
                            </th>
                            <th scope="col">NetCostValidation
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div class="breakPrice_buttom">
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                    <asp:TextBox ID="txtBreakPriceBatchNo" ReadOnly="true" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_BreakQty %>'></asp:Label>
                    <asp:TextBox ID="txtBreakQty" MaxLength="4" onkeydown="return isNumberKeyWithDecimalNewforBreakprice(event);" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_BreakPrice %>'></asp:Label>
                    <asp:TextBox ID="txtBreakPrice" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNewforBreakprice(event);" CssClass="form-control-res-right" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    <asp:TextBox ID="txtBreakMRP" ReadOnly="true" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_ddl">
                    <asp:Label ID="lblPriceType" runat="server" Text='<%$ Resources:LabelCaption,lbl_PriceType %>'></asp:Label>
                    <asp:DropDownList ID="ddlBreakPriceType" runat="server" Style="width: 100%">
                        <asp:ListItem Value="UBP" Text="BreakPrice"> </asp:ListItem>
                        <asp:ListItem Value="UDA" Text="DiscountAmount"> </asp:ListItem>
                        <asp:ListItem Value="UDP" Text="DiscountPercentage"> </asp:ListItem>
                        <asp:ListItem Value="UW1" Text="WPRICE1"> </asp:ListItem>
                        <asp:ListItem Value="UW2" Text="WPRICE2"> </asp:ListItem>
                        <asp:ListItem Value="UW3" Text="WPRICE3"> </asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="breakPrice_Button">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkBreakPriceAdd" runat="server" class="button-blue"
                            Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick="fncAddBreakPriceQty();return false;"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkBreakPriceClear" runat="server" class="button-blue"
                            OnClientClick="fncClearBreakPriceQty();return false;" Text='<%$ Resources:LabelCaption,lnkClear %>'></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnInventorySave" runat="server" OnClick="btnInventorySave_Click" />
        <asp:HiddenField ID="hidinventoryStatus" runat="server" Value="New" />
        <asp:HiddenField ID="hidPriceEntered" runat="server" Value="0" />
        <asp:HiddenField ID="hidBreakPriceQty" runat="server" />
        <asp:HiddenField ID="hidbrkNetCost" runat="server" />
        <asp:HiddenField ID="hidMultipleVendor" runat="server" />
        <asp:HiddenField ID="hidShortcutKey" runat="server" />
        <asp:HiddenField ID="hidVendorName" runat="server" />
        <asp:HiddenField ID="hidVendorcode" runat="server" />
        <asp:HiddenField ID="hidSaleshistory" runat="server" />
        <asp:HiddenField ID="hidchildItem" runat="server" />
        <asp:HiddenField ID="hidInvPriceDetail" runat="server" />
        <asp:HiddenField ID="hidDailogName" runat="server" />
        <asp:HiddenField ID="hidInvStatus" runat="server" />
        <asp:HiddenField ID="hidDeletebarcode" runat="server" />
        <asp:HiddenField ID="hidInvOpenStatus" runat="server" />
        <asp:HiddenField ID="hidItemname" runat="server" />
        <asp:HiddenField ID="hidSaveStatus" runat="server" Value="0" />
        <asp:HiddenField ID="hidWpriceGreaterSP" runat="server" Value="N" />
        <asp:HiddenField ID="hidLanguageChange" runat="server" Value="N" />
        <asp:HiddenField ID="hidMultipleUOM" runat="server" Value="" />
        <asp:HiddenField ID="hidMultipleUOMSave" runat="server" Value="N" />
        <asp:HiddenField ID="hidFranchiseMaster" runat="server" Value="N" />
        <asp:HiddenField ID="hidFranchiserValue" runat="server" Value="" />
        <asp:HiddenField ID="hidTransferDetail" runat="server" Value="" />
        <asp:HiddenField ID="hidPriceChangeLog" runat="server" Value="" />        
        <asp:HiddenField ID="hidView" runat="server" Value="" />        
        <asp:HiddenField ID="hdMultipleUOM" runat="server" />
        <asp:HiddenField ID="hdStoctType" runat="server" />
        <asp:HiddenField ID="hdStoctTypeVal" runat="server" />
        <asp:HiddenField ID="hidMrpEnter" runat="server" Value="" />
        <asp:HiddenField ID="hidRbdPartialMrp" runat="server" Value="" />
        <asp:HiddenField ID="hidQueryString" runat="server" Value="" />
        <asp:HiddenField ID="hidGRNSession" runat="server" Value="" />
        <asp:HiddenField ID="hidRoundOffType" runat="server" Value="" />
        <asp:HiddenField ID="hidSearch" runat="server" Value="" />
        <asp:HiddenField ID="hidMrpFocus" runat="server" Value="" />
        <asp:HiddenField ID="hidAllowNegative" runat="server" Value="" />
        <asp:HiddenField ID="hidPriceType" runat="server" Value="" />
        <asp:HiddenField ID="HiddeLocation" runat="server" Value="" />
        <asp:HiddenField ID="hidCostEdit" runat="server" Value="" />
        <asp:HiddenField ID="hidMarkupMrp" runat="server" Value="" />
        <asp:HiddenField ID="hidWeightCanversion" runat="server" Value="" />
                <asp:HiddenField ID="hidbarcode" runat="server" />

        <%--Prabu--%>
        <asp:UpdatePanel ID="upSave" runat="Server">
            <ContentTemplate>
                <asp:Button ID="btnSave" runat="server" Text="Button" OnClick="lnkSave_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="delete-BreakPriceQty" style="display: none">
    </div>
    <div id="delete-BreakPriceQtySuccess" style="display: none">
    </div>
    <div id="NewItemSave" class="display_none">
        <div>
            <asp:Label ID="lblNewItem" runat="server" Text="Inventory Saved Successfully . Do You want to continue with existing values ?"></asp:Label>
        </div>
        <div class="float_right">
            <div class="inv_dailog_btn">
                <asp:LinkButton ID="lnkNewItemYes" runat="server" Text="Yes" class="button-blue" OnClientClick="fncNewItemYesClick();return false;"></asp:LinkButton>
            </div>
            <div class="inv_dailog_btn">
                <asp:LinkButton ID="lnkNewItemNo" runat="server" Text="No" class="button-blue" OnClientClick="fncNewItemNoClick();return false;"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div id="ItemEdit" class="display_none">
    </div>
    <div id="DeleteMultiplevendor" class="display_none">
    </div>

    <div id="dialog-MultipleVendorCreation" style="display: none">
        <div class="Payment_fixed_headers newvendor">
            <table id="tblNewVendorCreation" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">Vendor Code
                        </th>
                        <th scope="col">Vendor Name
                        </th>
                        <th scope="col">Delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="multiple_vendor">
            <div class="multiple_vendorlbl">
                <asp:Label ID="lblNewvendorcode" runat="server" Text="Vendor Code"></asp:Label>
            </div>
            <div class="multiple_vendorlbl">
                <asp:TextBox ID="txtNewVendorcode" runat="server" onfocus="fncToEnableShortcutKey('NewVendor');" onkeydown="return fncShowVendorSearchDialogMultiVendor(event,'NewVendor');" class="form-control-res"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="display_none">
        <div id="divMultipleUOM" title="Multiple UOM">
            <div class="Payment_fixed_headers multipleUOM">
                <table id="tblMultipleUOM" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">UOM
                            </th>
                            <th scope="col">Conv.Qty
                            </th>
                            <th scope="col">Bin
                            </th>
                            <th scope="col">Active
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div>
                <div class="MultipleUOMtxt">
                    <asp:Label ID="lblMultipleUOM" runat="server" Text="UOM Code"></asp:Label>
                </div>
                <div class="MultipleUOMtxt">
                    <asp:TextBox ID="txtMultipleUOM" class="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="MultipleUOMtxt">
                    <asp:Label ID="lblUOMConv" runat="server" Text="Conv.Qty"></asp:Label>
                </div>
                <div class="MultipleUOMtxt">
                    <asp:TextBox ID="txtConvQty" class="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="MultipleUOMtxt" style="width: 9%">
                    <asp:CheckBox ID="cbUOMBin" runat="server" Text="Bin" />
                </div>
                <div class="MultipleUOMtxt">
                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue"
                        Text='Add' OnClientClick="fncAddMultipleUOM();return false;"></asp:LinkButton>
                </div>
                <div class="MultipleUOMtxt">
                    <asp:LinkButton ID="lnkUOMClear" runat="server" class="button-blue"
                        Text='Clear' OnClientClick="fncUOMClear();return false;"></asp:LinkButton>
                </div>
            </div>
        </div>
        <div id="divTransferDet">
            <div class="Payment_fixed_headers TransferDet">
                <table id="tblTransferDet" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Transfer No</th>
                            <th scope="col">Transfer Date</th>
                            <th scope="col">From Loc</th>
                            <th scope="col">To Loc</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
      <%---2022-06-14 jayanthi ----%>
        <div id="divPriceChgLog">
            <div class="Payment_fixed_headers PriceChgLog" >
                <table id="tblPriceChgLog" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th style="width:100px"  scope="col">Batch No</th>
                            <th style="width:100px"  scope="col">Date</th>
                            <th style="width:100px"  scope="col">Old Price</th>
                            <th style="width:100px"  scope="col">New Price</th>
                            <th style="width:142px" scope="col">Changed By</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>                
               </div>
        </div>  

        <div id="divOpenPRReq">    
            <div class="Payment_fixed_headers OpenPRReq" >
                <table id="tblOpenPRReq" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th style="width:100px"  scope="col">PR Req No</th>
                            <th style="width:115px"  scope="col">Req Date</th>
                            <th style="width:100px"  scope="col">Location Code</th>
                            <th style="width:100px"  scope="col">Batch No</th>
                            <th style="width:125px" scope="col">PR Req Qty</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>                
               </div>
        </div>

        

        <div id="divView">                             
            <div class="Payment_fixed_headers View">                                
               </div>            
             <div id="StockandPurhistory_id" class="Barcode_fixed_headers StockandPurhistory" style="width:1000px; height:300px">
                       </div>
                            <div class="saleshistory_inv" style="display:none">
                           <asp:RadioButton ID="rbnStockandPurMonth" runat="server" Checked="true" Text="Month" GroupName="stock & purchase" onchange="fncStockandPurhistorycheckchanged();" />
                            </div>
                        <div class="saleshistory_inv" style="display:none">
                         <asp:RadioButton ID="rbnStockandPurWeek" runat="server" Text="Week"  GroupName="stock & purchase" onchange="fncStockandPurhistorycheckchanged();" />
                      </div>
                      <div class="saleshistory_inv">
                     <asp:RadioButton ID="rbnStockandPurDay" runat="server" Text="Day"  GroupName="stock & purchase" onchange="fncStockandPurhistorycheckchanged();" />
                </div>
        </div>     
    </div>
    <div class="hiddencol">
        <div id="divSellingPriceChangeValidation">
            <div>
                <asp:Label ID="lblSellingPriceChangeValidation" runat="server"></asp:Label>
            </div>
            <div class="dialog_center">
                <asp:UpdatePanel ID="upValidation" runat="server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkbtnPriceChangeOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblok %>'
                            OnClientClick="HSNValidationClose();return false;"> </asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divRules" style="display: none;" title="Enterpriser Web">
            <div class="GridDetails">
                <div class="grid-search">
                    <input type="text" id="search" placeholder=" Search" onkeydown="return fncKeyPressSearch(event);"></input>
                </div>
                <div class="row">
                    <div class="grdLoad">
                        <table id="tblParamHead" cellspacing="0" rules="all" border="1">
                            <thead class="fixed_header">
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Param Code
                                    </th>
                                    <th scope="col">Value
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div style="overflow-y: visible; overflow-x: hidden; height: 150px">
                            <table id="tblParam" cellspacing="0" rules="all" border="1">
                                <thead class="fixed_header display_none">
                                    <tr>
                                        <th scope="col">S.No
                                        </th>
                                        <th scope="col">Param Code
                                        </th>
                                        <th scope="col">Value
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 5px">
                        <asp:TextBox ID="txtParamcodeDes" ReadOnly="true" Style="background-color: white;" runat="server" Width="500px" TextMode="MultiLine"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidPricewatch" runat="server" Value="" />
    <asp:HiddenField ID="hidInventoryRange" runat="server" Value="" />
    <asp:HiddenField ID="hidTextile" runat="server" Value="" />
    <asp:HiddenField ID="hidBulkBatchDistribution" runat="server" Value="N" />
    <asp:HiddenField ID="hidWPriceReverse" runat="server" Value="N" />
    <asp:HiddenField ID="hidNewCalculation" runat="server" Value="N" />
    <asp:HiddenField ID="hidOldNetCost" runat="server" /> 
    <asp:HiddenField ID="hidPurchasedBy" runat="server" /> 
    <asp:HiddenField ID="hidNetcostnew" runat="server" />
     <asp:HiddenField ID="hidGrossCostNew" runat="server" />
     <asp:HiddenField ID="hidZero" runat="server" />
    
</asp:Content>
