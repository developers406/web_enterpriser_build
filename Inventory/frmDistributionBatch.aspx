﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmDistributionBatch.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmDistributionBatch" %>

<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 90px;
            max-width: 90px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 185px;
            max-width: 185px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            display: none;
        }

        .Description_parent {
            max-width: 300px !important;
            min-width: 300px !important;
        }

        .parent_align {
            max-width: 130px !important;
            min-width: 130px !important;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 435px;
            max-width: 435px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(7), .grdLoad1 th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(8), .grdLoad1 th:nth-child(8) {
            min-width: 116px;
            max-width: 116px;
        }

        .grdLoad1 td:nth-child(9), .grdLoad1 th:nth-child(9) {
            display: none;
        }

        .grdLoad1 td:nth-child(10), .grdLoad1 th:nth-child(10) {
            display: none;
        }


        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .selected_row {
            background-color: #A1DCF2;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }

        .Parent {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        .Child {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        #tags {
            bottom: 40px;
            position: fixed;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            display: none;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>

    <script type="text/javascript">


        //var Templetedropdownctrl;
        //var PackedDatectrl;
        //var SizeTextBoxctrl;
        //var BestBeforeTextBoxctrl;
        //var DateRadioButton;
        //var MonthRadioButton;
        //var DateLable;


        function pageLoad() {
            debugger;
            try {
                pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                SetAutoComplete();
                $("#<%= txtCdescription.ClientID %>").focus(); 
                <%--$("#<%= txtMrp.ClientID %>").attr('readonly', true);--%>
                if ($("#<%= txtMrp.ClientID %>").val() != "Retain") {
                    $("#<%= txtExpiredDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                    return false;
                } 
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        $(document).ready(function () {
            $("#<%= txtCost.ClientID %>").on("keypress", function (e) {
                if (e.keyCode == 13) {
                    if (parseFloat($("#<%= txtCost.ClientID %>").val()) > parseFloat($("#<%= txtMrp.ClientID %>").val())) {
                                ShowPopupMessageBox("Please Enter Cost is Lessar than Mrp");
                                return false;
                            }
                            else {
                                $("#<%= txtSellPrice.ClientID %>").focus().select();
                                return false;
                            }

                        }
             });
            $("#<%= txtSellPrice.ClientID %>").on("keypress", function (e) {
                if (e.keyCode == 13) {
                    if (parseFloat($("#<%= txtSellPrice.ClientID %>").val()) > parseFloat($("#<%= txtMrp.ClientID %>").val())) {
                                ShowPopupMessageBox("Please Enter SellingPrice is Lessar than Mrp");
                                return false;
                            }
                            else if (parseFloat($("#<%= txtSellPrice.ClientID %>").val()) < parseFloat($("#<%= txtCost.ClientID %>").val())) {
                                ShowPopupMessageBox("Please Enter SellingPrice is Greater than or Equal to Cost ");
                                return false;
                            }
                            else {
                                $("#<%= txtBatchNo.ClientID %>").focus().select();
                                return false;
                            }
                    }
                    });
        });
            function disableFunctionKeys(e) {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        e.preventDefault();
                    }
                }
            };

            $(document).ready(function () {
                $(document).on('keydown', disableFunctionKeys);
            });

            function Clearall() {

                $("select").val(0);
                $("select").trigger("liszt:updated");
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');

                return false;
            }

            //Show Popup After Save
            function fncShowBarcodeSuccessMessage() {
                try {

                    fncToastInformation('<%=Resources.LabelCaption.Save_Barcodeprinting%>');

            }
            catch (err) {
                fncToastError(err.message);
                //console.log(err);
            }
        }


        ///Template
        function fncTemplateChange() {
            try {
                debugger;
                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Open Barcode Popup
        function ShowPopupBarcode() {

            dateFormat[0].selectedIndex = 1;
            dateFormat.trigger("liszt:updated");

            $("#divBarcode").dialog({
                appendTo: 'form:first',
                title: "Barcode Print",
                width: 500,
                modal: true

            });
            setTimeout(function () {
                expiredDate.select().focus();
            }, 50);
        }


        var pkdDate;
        var expiredDate;
        var dateFormat;
        var hideMRP;
        var hidePrice;
        var hidepkdDate;
        var hideexpDate;
        var hidecompany;
        var template;
        var size;
        var print;
        //Get Barcode User Comtrol
        $(function () {

            try {

                pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
                expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
                dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
                hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
                hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
                hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
                hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
                hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
                template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
                size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
                print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");


            }
            catch (err) {
                fncToastError(err.message);
            }

        });

        ///Change days and months
        function fncDateMonth() {
            try {

                if ($(DateRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Days)');
                }
                else if ($(MonthRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Months)');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Template
        function fncTemplateChange() {
            try {
                debugger;

                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        $(function () {
            $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        if ($("#<%= txtBillDate.ClientID %>").val() === '') {
            $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
    });
        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        $(function () {
            $('#<%=txtPCSQty.ClientID %>').focusout(function () {
                fncCalcTotalweight();
            });
        });
            $(function () {
                $("[id*=grdDistribution]").on('click', 'td > img', function (e) {
                    var r = ShowPopupMessageBox("Do you want to Delete?");
                    if (r === false) {
                        return false;
                    }

                    var objIssuedQty = null;
                    var objBalanceQty = null;

                    $("[id*=grdBulkItem] tr").each(function () {

                        objIssuedQty = $(this).find(".LIssuedQty");
                        objBalanceQty = $(this).find(".LBalanceQty");
                        objIssuedQty.text("0.00");
                    });

                    $(this).closest("tr").remove();
                    $('#<%=txtCdescription.ClientID %>').focus().select();
                    return false;
                });

            });


            ///Barcode Validation
            function fncBarcodeValidation() {
                debugger;
                try {
                    var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                    //alert(totalRows);
                    if (totalRows == 0) {
                        ShowPopupMessageBox("Add item to Print !");
                    }
                    else {
                        ShowPopupBarcode();
                    }

                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }




            ///Barcode Validation
            function fncBarcodeQtyValidation() {
                try {
                    if (size.val() == "") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                        return false;
                    }
                    else {
                        //fncGetGridValuesForSave();
                        $("#<%=lnkbtnPrint.ClientID %>").click();
                        return false;
                    }

                }
                catch (err) {
                    fncToastError(err.message);
                }
            }

            function UpdateValidation() {
                //alert('test');
                //Show = Show + 'Already Exists';

                //if (Show != '') {
                //    ShowPopupMessageBox(Show);
                //    return false;
                //}
                try {
                    debugger;
                    $("#dialog-confirm").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            "YES": function () {
                                $(this).dialog("close");
                                $("#<%=lnkUpdate.ClientID %>").click();
                                return false;
                            },
                            "NO": function () {
                                $(this).dialog("close");
                                return false();
                            }
                        }
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }

            }
            function AddbtnValidation() {

                try {


                    if ($('#<%=txtChildCode.ClientID %>').val() == '') {
                        ShowPopupMessageBox('Item Code should not be Empty !');
                        $('#<%=txtChildCode.ClientID %>').focus().select();
                        return false;
                    }

                    if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox('Invalid Weight Qty !');
                        $('#<%=txtPCSQty.ClientID %>').focus().select();

                        return false;
                    }

                    if ($('#<%=txtWeight.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox('Invalid Weight Qty !');
                        $('#<%=txtWeight.ClientID %>').focus().select();
                        return false;
                    }
                    if ($('#<%=txtMrp.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox("Invalid MRP!");
                        return false;
                    }
                    if ($('#<%=txtCost.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox("Invalid Cost!");
                        return false;
                    }
                    if ($('#<%=txtSellPrice.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox("Invalid SellingPrice!");
                        return false;
                    }
                  if (parseFloat($("#<%= txtMrp.ClientID %>").val()) < parseFloat($("#<%= txtCost.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter Cost should be Lessar than Mrp");
                        return false;
                    }
                    if (parseFloat($("#<%= txtMrp.ClientID %>").val()) < parseFloat($("#<%= txtSellPrice.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter SellingPrice should be Lessar than Mrp");
                        return false;
                    }
                     if (parseFloat($("#<%= txtCost.ClientID %>").val()) > parseFloat($("#<%= txtSellPrice.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter SellingPrice should be Greater than or Equal to Cost ");
                        return false;
                    }
                    if ($("#HiddenAllowNegDist").val() == 'Y') {

                        //alert('test');
                        <%-- var CheckStock = '';

                    $("[id*=grdBulkItem] tr").each(function () {                       

                        var dBalanceQty = $(this).find(".LBalanceQty").text();
                                            
                                            if (dBalanceQty != "")
                                            {
                                                //alert(dBalanceQty);
                                                //alert($('#<%=txtTotalWeight.ClientID %>').val());
                                                //var Balance = parseFloat(dBalanceQty);
                                                
                                                if (parseFloat($('#<%=txtTotalWeight.ClientID %>').val()) > parseFloat(dBalanceQty)) {

                                                    //alert(dBalanceQty);
                                                    CheckStock = 'less';

                                                  }
                                            }                      
                    });

                    if (CheckStock == 'less')
                    {
                        //ShowPopupMessageBox('Stock is Less !');
                        popUpObjectForSetFocusandOpen = $('#<%=txtPCSQty.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('Stock is Less !');
                        // $('#<%=txtPCSQty.ClientID %>').focus().select();
                        return false;
                    }--%>
                    }
                    <%--       
                    var totalRows = 0;
                    totalRows = $("#<%=grdDistribution.ClientID %> tr").length;--%>
                    <%--if ($("#<%=grdDistribution.ClientID%> tr:has(td)").each(function () {
                         var gitem = $(this).find("td.Itemcode").html();
                        var gDes = $(this).find("td.Description").html();
                        if (($.trim(gitem) == $.trim($('#<%=txtChildCode.ClientID %>').val())) && ($.trim(gDes) == $.trim($('#<%=txtCdescription.ClientID %>').val()))) {
                            $(this).find("td.PCS").html() = $('#<%=txtPCSQty.ClientID %>').val();
                    }

                    });--%>
                    debugger;
                    var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                    var confirm_value = "";
                    //alert(totalRows);
                    $("#<%=grdDistribution.ClientID%> tr").each(function () {

                        if (!this.rowIndex) return;
                        var gitem = $(this).find("td.Itemcode").html();
                        if ($.trim(gitem) == $.trim($('#<%=txtChildCode.ClientID %>').val())) {
                            confirm_value = document.createElement("INPUT");
                            confirm_value.type = "hidden";
                            confirm_value.name = "confirm_value";
                            if (confirm("Item Already exists!. Do you want to update Packed Qty?")) {
                                confirm_value.value = "Yes";

                            } else {
                                confirm_value.value = "No";
                            }

                            document.forms[0].appendChild(confirm_value);
                        }
                    });

                    Confirm();


                }
                catch (err) {
                    return false;
                    ShowPopupMessageBox(err.Message);
                }
            }


            function Confirm() {
                //debugger;
                //var gitem = $(this).find("td:eq(1)");
            }
            function Toast() {
                try {
                    ShowPopupMessageBox("Saved Succesfully");
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            //Get Calc Child Qty
            function fnctxtChildQty(event) {
                try {
                    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                    if (keyCode == 13) {

                        if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {

                            ShowPopupMessageBox('Invalid Weight Qty !');
                            $('#<%=txtPCSQty.ClientID %>').focus().select();

                            return false;
                        }
                        fncCalcTotalweight();
                        $('#<%=txtMrp.ClientID %>').focus().select();
                        return false;
                    }

                }
                catch (err) {
                    return false;
                    ShowPopupMessageBox(err.Message);
                }
            }

            function fncCalcTotalweight() {
                try {
                    var ChildQty = $('#<%=txtPCSQty.ClientID%>').val();
                    var ChildWeight = $('#<%=txtWeight.ClientID%>').val();
                    var TotalQty;
                    TotalQty = ChildWeight * ChildQty;
                    $('#<%=txtTotalWeight.ClientID%>').val(TotalQty.toFixed(3));

                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            }


            //        $(function () {
            //            $("[id*=grdDistribution]").on('click', 'td > img', function (e) {

            //                var r = confirm("Do you want to Delete?");
            //                if (r === false) {
            //                    return false;
            //                }
            //                var rowCount = $("[id*=grdBulkItem] tr:last").index() + 1;
            //                if (rowCount == 2) {
            //                    $("[id*=grdBulkItem] tr:last").after('<tr class="pshro_GridDgnStyle"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img src="../images/delete.png" /></td></tr>');
            //                }
            //                $(this).closest("tr").remove();
            //                $('#<%=txtCdescription.ClientID %>').focus().select();
        //            });
        //            return false;
        //        });


        $(function () {
            $("[id*=grdDistribution] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdDistribution] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });


        function SetAutoComplete() {
            //debugger;

            $("[id$=txtCdescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmDistributionBatch.aspx/GetBulkDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",

                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valInvDet: item.split('|')[0],
                                    valCost: item.split('|')[1],
                                    valSellPr: item.split('|')[2],
                                    valWeight: item.split('|')[3],
                                    valMrp: item.split('|')[4],
                                    valParentCode: item.split('|')[5]
                                }
                            }))
                        },

                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                select: function (e, i) {
                    //alert(i.item.val);
                    $('#<%=txtChildCode.ClientID %>').val($.trim(i.item.valInvDet.split('--')[0]));
                    $('#<%=txtCdescription.ClientID %>').val($.trim(i.item.valInvDet.split('--')[1]));
                    $('#<%=txtCost.ClientID %>').val($.trim(i.item.valCost));
                    $('#<%=txtSellPrice.ClientID %>').val($.trim(i.item.valSellPr));
                    $('#<%=txtWeight.ClientID %>').val($.trim(i.item.valWeight));
                    $('#<%=txtMrp.ClientID %>').val($.trim(i.item.valMrp));
                    if ($('#<%=hidRetain.ClientID %>').val() == "Retain") {
                        fncGetBatchno();
                    }
                    else {
                        $('#<%=txtPCSQty.ClientID %>').focus().select();
                    }

                    $("#hidenChildCode").val($.trim(i.item.valInvDet.split('--')[0]));
                    $("#HiddenMrp").val($.trim(i.item.valMrp));
                    $("#hidenParentcode").val($.trim(i.item.valParentCode));
                    //alert($("#hidenParentcode").val());
                    return false;
                },
                minLength: 1
            });

            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtCdescription'
                });
            });
        }



        function fncBarcodeDetail(sItemCode) {
            //debugger;
            var isBatch, Code = '', Description, GST, Cost;
            $.ajax({
                url: '<%=ResolveUrl("~/Inventory/frmDistributionBatch.aspx/GetItemDetails")%>',
                data: "{ 'ItemCode': '" + sItemCode + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //alert(data.d);
                    $.map(data.d, function (item) {
                        Code = item.split('|')[0],
                        valInvDet = item.split('|')[0],
                        valCost = item.split('|')[1],
                        valSellPr = item.split('|')[2],
                        valWeight = item.split('|')[3],
                        valMrp = item.split('|')[4],
                        valParentCode = item.split('|')[5]
                    })

                    if (Code != '') {
                        $('#<%=txtChildCode.ClientID %>').val($.trim(valInvDet.split('--')[0]));
                        $('#<%=txtCdescription.ClientID %>').val($.trim(valInvDet.split('--')[1]));
                        $('#<%=txtCost.ClientID %>').val($.trim(valCost));
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(valSellPr));
                        $('#<%=txtWeight.ClientID %>').val($.trim(valWeight));
                        $('#<%=txtPCSQty.ClientID %>').focus().select();
                        $("#hidenChildCode").val($.trim(valInvDet.split('--')[0]));
                        $("#HiddenMrp").val($.trim(valMrp));
                        $("#hidenParentcode").val($.trim(valParentCode));
                    }
                    else {
                        ClearTextBox();
                        popUpObjectForSetFocusandOpen = $('#<%=txtCdescription.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('Invalid Code !');
                    }
                },
                error: function (response) {
                    ShowPopupMessageBox(response.responseText);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.responseText);
                }
            });
        }
        function fncInventorySearch(event) {
            try {

                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    if ($("#<%=txtCdescription.ClientID %>").val() != '') {
                        //alert($("#<%=txtCdescription.ClientID %>").val());
                        if (!isNaN($("#<%=txtCdescription.ClientID %>").val())) {
                            fncBarcodeDetail($("#<%=txtCdescription.ClientID %>").val());
                        }

                    }
                    //event.preventDefault();
                    //event.stopPropagation();
                    return false;
                }
                else if (keyCode == 220) {
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtChildCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }

        function ClearTextBox() {
            //alert('test');
            try {
                $('#<%=txtChildCode.ClientID %>').val('');
                $('#<%=txtCdescription.ClientID %>').val('');
                $('#<%=txtCost.ClientID %>').val(0);
                $('#<%=txtSellPrice.ClientID %>').val(0);
                $('#<%=txtWeight.ClientID %>').val(0);
                $('#<%=txtPCSQty.ClientID %>').val(0);
                $('#<%=txtTotalWeight.ClientID %>').val(0);
                $('#<%=txtMrp.ClientID %>').val(0);
                $('#<%=txtBatchNo.ClientID %>').val('');
                if ($('#<%=hidRetain.ClientID %>').val() != "Retain")
                    $('#<%=txtExpiredDate.ClientID %>').datepicker("setDate", "0");
                else
                    $('#<%=txtExpiredDate.ClientID %>').val('');
                $("#hidenChildCode").val('');
                $("#HiddenMrp").val(0);
                $('#<%=txtCdescription.ClientID %>').focus().select();
                return false;
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }

        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {
                $(function () {
                    $("#barcodeprinting").html('<%=Resources.LabelCaption.Save_Barcodeprinting%>');
                    $("#barcodeprinting").dialog({
                        title: "Enterpriser Web",
                        buttons: {
                            Ok: function () {
                                $(this).dialog("destroy");
                                fncClear();
                            }
                        },
                        modal: true
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitializeBatchDetail() {
            try {
                $("#Distribution_batch").dialog({
                    resizable: false,
                    height: 250,
                    width: 633,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetBatchno() {
            debugger;
            try {
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmDistributionBatch.aspx/fncGetRetain_Batches")%>',
                    data: "{ 'Code': '" + $('#<%=txtChildCode.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try {
                var objStk, row;
                objStk = jQuery.parseJSON(msg.d);

                if (objStk.length > 0) {
                    tblBatchBody = $("#tblBatch tbody");
                    tblBatchBody.children().remove();
                    for (var i = 0; i < objStk.length; i++) {
                        row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                            $.trim(objStk[i]["RowNo"]) + "</td><td id='tdDistributionNo_" + i + "' >" +
                            $.trim(objStk[i]["DistributionNo"]) + "</td><td id='tdInventoryCode_" + i + "' >" +
                            objStk[i]["InventoryCode"] + "</td><td id='tdBatchNo_ " + i + "' >" +
                            objStk[i]["BatchNo"] + "</td><td id='tdExpiredDate_" + i + "' >" +
                            objStk[i]["ExpiredDate"] + "</td><td id='tdMrp_" + i + "' >" +
                            objStk[i]["Mrp"].toFixed(2) + "</td><td id='tdQty_" + i + "'>" +
                            $.trim(objStk[i]["Qty"]) + "</td></tr>";
                        tblBatchBody.append(row);
                    }


                    fncInitializeBatchDetail();
                    tblBatchBody.children().dblclick(fncBatchRowClick);

                    tblBatchBody[0].setAttribute("style", "cursor:pointer");

                    tblBatchBody.children().on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });

                    tblBatchBody.children().on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });

                    tblBatchBody.children().on('keydown', function () {
                        fncBatchKeyDown($(this), event);
                    });

                    $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                    $("#tblBatch tbody > tr").first().focus();

                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {
            debugger;
            try {
                setTimeout(function () {
                    $('#<%=hidExp.ClientID %>').val('');
                    $("#<%=txtBatchNo.ClientID%>").attr("disabled", true);
                    $("#<%=txtExpiredDate.ClientID%>").attr("disabled", true);
                    $("#<%=txtMrp.ClientID%>").attr("disabled", true);
                    $('#<%=txtBatchNo.ClientID %>').val($.trim($(rowObj).find('td[id*=tdBatchNo]').text()));
                    $('#<%=txtExpiredDate.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));
                    $('#<%=txtMrp.ClientID %>').val($.trim($(rowObj).find('td[id*=tdMrp]').text()));
                    $("#HiddenMrp").val($.trim($(rowObj).find('td[id*=tdMrp]').text()));
                    $("#Distribution_batch").dialog('close');
                    $('#<%=txtPCSQty.ClientID %>').focus().select();
                    $('#<%=hidExp.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));
                }, 50);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'DistributionBatch');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "DistributionBatch";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:UpdatePanel ID="updtPnlgrdgrdDepartmentList" runat="Server">
        <ContentTemplate>

            <div class="main-container" id="form1">

                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="../Inventory/frmDistributionList.aspx">Distribution List</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page" id="DtCreation" runat="server">Distribution Batch Creation</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        <%-- <li class="active-page display_none" id ="DtReturn" runat >Distribution Return</li>--%>
                    </ul>
                </div>
                <div class="container-group-pc">
                    <asp:HiddenField ID="hidDtReturn" runat="server" Value="" />
                    <div class="right-container-top-distribution">

                        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Are you sure to Update this Item?</p>
                        </div>
                        <%--<div class="right-container-top-header">
                            Header Detail
                        </div>--%>
                        <div class="right-container-bottom-detail">
                            <div class="control-group-split">
                                <div class="control-group-left-pc" style="margin-left: 20px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_partyCode %>'></asp:Label>
                                        <asp:DropDownList ID="PartyCodeDropdown" runat="server" CssClass="form-control-res" Width="100px"
                                            AutoPostBack="true" OnSelectedIndexChanged="PartyCodeDropdown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-left-Distribut-txt" style="margin-left: -57px;">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_name %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc" style="width: 180px">
                                        <asp:TextBox ID="txtPartyName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: -138px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblbillno %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: -21px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Trays %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtTrays" runat="server" Text="0" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: 2px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bags %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBags" Text="0" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: 2px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_Emptybags %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtEmptyBag" Text="0" runat="server" CssClass="form-control-res"
                                            onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right-Distb" style="margin-right: 24px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblBilldate %>'></asp:Label>
                                        <asp:TextBox ID="txtBillDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-pc" style="margin-left: 5px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefrenceNo %>'></asp:Label>
                                        <asp:DropDownList ID="RefNoDropDown" runat="server" CssClass="form-control-res" AutoPostBack="true" Width="100px"
                                            OnSelectedIndexChanged="RefNoDropDown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-left-pc" style="margin-left: -63px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_LotNumber %>'></asp:Label>
                                        <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <%-- <div class="control-group-left-pc" style="margin-left: 20px">
                            <div class="label-right-pc">
                                <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefrenceNo %>'></asp:Label>
                                <asp:DropDownList ID="RefNoDropDown" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                    OnSelectedIndexChanged="RefNoDropDown_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-left-pc" style="margin-left: 10px">
                            <div class="label-right-pc">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_LotNumber %>'></asp:Label>
                                <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-container-top-detail">
                    <div class="col-md-12  Parent">
                        Parent Item
                    </div>
                    <div class="GridDetails">
                        <div class="row">
                            <%--    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                            <div class="grdLoad1">
                                <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Inventorycode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">QtyOnHand
                                            </th>
                                            <th scope="col">TakenQty
                                            </th>
                                            <th scope="col">IssuedQty
                                            </th>
                                            <th scope="col">BalanceQty
                                            </th>
                                            <th scope="col">BatchNo
                                            </th>
                                            <th scope="col">Expired Date
                                            </th>
                                        </tr>
                                    </thead>
                                </table>

                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; background-color: aliceblue; height: 135px;">
                                    <asp:GridView ID="grdBulkItem" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="false"
                                        ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left">
                                        <PagerStyle CssClass="pshro_text" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Cost" DataField="Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="QtyOnHand" DataField="QtyOnHand" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Taken Qty" ItemStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTakenQty" runat="server" Text='<%# Eval("TakenQty") %>'
                                                        onkeypress="return isNumberKey(event)" CssClass="grid-textbox1" Style="background-color: yellow"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="IssuedQty" DataField="IssuedQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LIssuedQty"></asp:BoundField>
                                            <asp:BoundField HeaderText="BalanceQty" DataField="BalanceQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LBalanceQty"></asp:BoundField>
                                            <%-- <asp:BoundField HeaderText="Batch" DataField="Batch" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Expired Date" DataField="ExpiredDate" ItemStyle-HorizontalAlign="Left"></asp:BoundField>--%>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            PageButtonCount="5" Position="Bottom" />
                                        <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-container-top-header">
                    Child Item
                </div>
                <div class="right-container-top-detail">
                    <div class="GridDetails">
                        <div class="row">
                            <div class="grdLoad">
                                <%--   <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">Inventorycode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Weight
                                            </th>
                                            <th scope="col">PCS
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">Total Qty PKD
                                            </th>
                                            <th scope="col">Mrp
                                            </th>
                                            <th scope="col">Barcode
                                            </th>
                                            <th scope="col">ParentCode
                                            </th>
                                        </tr>
                                    </thead>
                                </table>--%>
                                <div class="GridDetails" id="divscroll" style="overflow-x: hidden; overflow-y: scroll; height: 150px; width: 1333px; background-color: aliceblue;">
                                    <asp:GridView ID="grdDistribution" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="true"
                                        ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left" OnRowDataBound="grdDistribution_RowDataBound" OnRowDeleting="grdDistribution_RowDeleting"
                                        OnRowCommand="grdDistribution_RowCommand">
                                        <PagerStyle CssClass="pshro_text" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <Columns>
                                            <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />--%>
                                            <%--<asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Delete" ID="Delete" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <img src="../images/delete.png" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                            <asp:BoundField ItemStyle-CssClass="Itemcode" HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Weight" DataField="Weight" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="PCS" DataField="PCS" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="Cost" DataField="Cost" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Total Qty PKD" DataField="PackedQty" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="Mrp" DataField="Mrp" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Batch" DataField="BatchNo" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Expired Date" DataField="ExpiredDate" ItemStyle-HorizontalAlign="Left"></asp:BoundField>

                                            <asp:TemplateField HeaderText="Barcode" ItemStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtbarcode" runat="server" Text='<%# Eval("Barcode") %>'
                                                        onkeypress="return isNumberKey(event)" CssClass="grid-textbox2" Style="background-color: whitesmoke"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="ParentCode" DataField="Parentcode" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="hiddencol1"></asp:BoundField>
                                            <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox Text="Select All" ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this)" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <%--<EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>--%>
                                        <HeaderStyle CssClass="pshro_GridNewHeaderCellCenter" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            PageButtonCount="5" Position="Bottom" />
                                        <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                    </asp:GridView>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="right-container-bottom-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtChildCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Description" style="width: 30% !important">
                            <div class="label-left-pc">
                                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <div class="ui-widget">
                                    <asp:TextBox ID="txtCdescription" runat="server" CssClass="form-control-res" onkeydown="return fncInventorySearch(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_weight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPCSQty" runat="server" MaxLength="12" CssClass="form-control-res" onkeydown="return fnctxtChildQty(event)"
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label19" runat="server" Text="Mrp"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtMrp" runat="server"   CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCost" runat="server"  CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_sellingPrice %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtSellPrice" runat="server"  CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalWeight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtTotalWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                                <asp:TextBox ID="txtSort" runat="server" Text="1" CssClass="display_none"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label17" runat="server" Text="Batch No"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtBatchNo" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label18" runat="server" Text="Expired Date"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtExpiredDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClick="lnkAdd_Click"
                            Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick=" return AddbtnValidation();"> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                            Text='<%$ Resources:LabelCaption,btnClear %>'> </asp:LinkButton>
                    </div>
                </div>

                <div class="control-button">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClick="lnkSave_Click"
                                    Text='<%$ Resources:LabelCaption,btnSave %>'> </asp:LinkButton>
                            </div>
                            <div class="control-button hiddencol ">
                                <asp:LinkButton ID="lnkClearform" runat="server" CssClass="button-red"
                                    Text='<%$ Resources:LabelCaption,btnClear %>' OnClick="lnkClearform_Click"></asp:LinkButton>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="control-button">
                <asp:LinkButton ID="lnkBarcode" runat="server" CssClass="button-red" OnClientClick="fncBarcodeValidation();"
                    Text='<%$ Resources:LabelCaption,Home_barcode %>'></asp:LinkButton>
            </div>

            <asp:HiddenField ID="hidenChildCode" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="hidenParentcode" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenMrp" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenAllowNegDist" runat="server" ClientIDMode="Static" Value="N" />
            <asp:Button ID="lnkbtnPrint" runat="server" OnClick="lnkbtnPrint_Click" class="hiddencol" />

            <div class="hiddencol">
                <div id="barcodeprinting" class="barcodesavedialog">
                </div>

            </div>
            <div id="divHiddenButton" style="display: none">
                <asp:LinkButton ID="lnkUpdate" runat="server" CssClass="button-red" OnClick="lnkUpdate_Click"> </asp:LinkButton>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divBarcode" style="display: none;">
        <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl" OnPrintButtonClientClick="return fncBarcodeQtyValidation();" />
    </div>
    <div class="hiddencol">
        <div id="Distribution_batch">
            <div class="Payment_fixed_headers BatchDetail">
                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">Row
                            </th>
                            <th scope="col">DistributionNo
                            </th>
                            <th scope="col">InventoryCode
                            </th>
                            <th scope="col">BatchNo
                            </th>
                            <th scope="col">ExpiredDate
                            </th>
                            <th scope="col">MRP
                            </th>
                            <th scope="col">Mrp
                            </th>
                            <th scope="col">Qty
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div class="control-container" style="display: none">
                <div class="control-button">
                    <asp:LinkButton ID="lnkNewBatch" runat="server" class="button-blue" Text='NewBatch (F5)'
                        OnClientClick="fncGetLastBatchDetail();return false;"></asp:LinkButton>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidRetain" runat="server" />
        <asp:HiddenField ID="hidExp" runat="server" />
    </div>
</asp:Content>
