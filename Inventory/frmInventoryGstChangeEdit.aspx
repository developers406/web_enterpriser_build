﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInventoryGstChangeEdit.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryGstChangeEdit" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
            text-align: left;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 395px;
            max-width: 395px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 110px;
            max-width: 110px;
            text-align: left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 110px;
            max-width: 110px;
            text-align: left;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
            text-align: right;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
            text-align: left;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 110px;
            max-width: 110px;
            text-align: right;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 110px;
            max-width: 110px;
            text-align: right;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                   <%-- $('#<%=lnkNew.ClientID %>').css("display", "block");--%>
                }
                else {
                   <%-- $('#<%=lnkNew.ClientID %>').css("display", "none");--%>
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Inv_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Inventory");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(1).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });
        }

         function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter"); 
                    $("#<%=HideFilter_ContainerRight.ClientID%>").width("100%");
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("1330px");  
                    $("select").trigger("liszt:updated");
                }
                else { 
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter"); 
                    $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("90%");
                    $("select").trigger("liszt:updated"); 
                }
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
         }

        
function clearForm() {
    try {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("select").val(0);
        $("select").trigger("liszt:updated");
        $('#<%=grdInventoryList.ClientID%>').remove();
    }
    catch (err) {
        alert(err.Message);
        return false;
    }
}
function clearFormFilter() {
    try {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("select").val(0);
        $("select").trigger("liszt:updated");
    }
    catch (err) {
        ShowPopupMessageBox(err.Message);
        return false;
    }
}

    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'InventoryGSTChangeEdit');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "InventoryGSTChangeEdit";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Inventory</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                    EnableVendorDropDown="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableClassDropDown="true"
                    EnableSubClassDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemTypeDropDown="false"
                    EnablePriceTextBox="true"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    EnableGstDropDown="true"
                    OnClearButtonClientClick="clearFormFilter(); return false;"
                    OnFilterButtonClick="lnkLoadFilter_Click" />

            </div>

            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server" style="margin-left:0% !important;">
                <asp:UpdatePanel ID="updateInvGrid" runat="server">
                    <ContentTemplate>
                        <div class="right-container-top-header">
                            GST Change History
                        </div>
                      <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <div class="right-container-top-detail">
                    <div class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: hidden; height: 415px; background-color: aliceblue;"
                                        id="HideFilter_GridOverFlow" runat="server">
                    <div class="GridDetails">
                        <div class="row"> 
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Item Code
                                                </th>
                                                <th scope="col">Item Name
                                                </th>
                                                <th scope="col">New GST
                                                </th>
                                                <th scope="col">Old GST
                                                </th>
                                                <th scope="col">MRP
                                                </th>
                                                <th scope="col">Efective Date
                                                </th>
                                                <th scope="col">Selling Price
                                                </th>
                                                <th scope="col">NetCost
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 376px; width: 1325px; background-color: aliceblue;">
                                        <asp:GridView ID="grdInventoryList" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                            DataKeyNames="InventoryCode" CssClass="pshro_GridDgn grdLoad">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/No.png"
                                                            ToolTip="Click here to Delete" OnClientClick="return Inv_Delete(this);  return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                                <asp:BoundField DataField="NewGst" HeaderText="NewGst" />
                                                <asp:BoundField DataField="oldGST" HeaderText="oldGST"></asp:BoundField>
                                                <asp:BoundField DataField="Mrp" HeaderText="Mrp"></asp:BoundField>
                                                <asp:BoundField DataField="ExpectedDate" HeaderText="ExpectedDate"></asp:BoundField>
                                                <asp:BoundField DataField="SellingPrice" HeaderText="SellingPrice"></asp:BoundField>
                                                <asp:BoundField DataField="NetCost" HeaderText="NetCost" />
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                    <ups:PaginationUserControl runat="server" ID="InvPaging" OnPaginationButtonClick="InvPaging_PaginationButtonClick" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        </ContentTemplate>
                </asp:UpdatePanel>
                <div class="container-bottom-invchange">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick=" return fncHideFilter()"
                            class="button-blue" Text="Hide Filter"> </asp:LinkButton>
                    </div>  
                    <div class="control-button">
                        <asp:LinkButton ID="lnkNew" runat="server" class="button-red" PostBackUrl="~/Inventory/frmInventoryGstChange.aspx"
                            Text="New"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick ="clearForm();return false;"
                            Text="Clear"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="hiddencol">
                <asp:HiddenField ID="HiddenField1" runat="server" />
                <asp:HiddenField ID="HiddenField2" runat="server" />
                <asp:HiddenField ID="HiddenField3" runat="server" />
                <asp:HiddenField ID="HiddenField4" runat="server" />
                <asp:HiddenField ID="hidWholesale" runat="server" />
                <asp:HiddenField ID="GSTXmldata" runat="server" />
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updateInvGrid">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <div class="display_none">
        <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hdnValue" Value="" runat="server" />
</asp:Content>
