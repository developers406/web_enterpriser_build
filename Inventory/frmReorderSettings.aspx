﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmReorderSettings.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmReorderSettings" %>

<%--dinesh 11082018--%>
<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .ReOrderFixed {
            width: 100%;
            overflow: auto;
        }

            .ReOrderFixed table {
                border: 1px solid black;
            }

            .ReOrderFixed th, .ReOrderFixed td {
                text-align: center;
                padding: 2px 2px;
                word-wrap: break-word;
                top: -31px;
                left: 228px;
                border-color: black;
            }

            .ReOrderFixed td {
                color: black;
                font-weight: bold;
                text-align: left !important;
            }

                .ReOrderFixed td right {
                    text-align: right !important;
                }

            .ReOrderFixed thead {
                color: #fff;
                background-color: #4163e1;
            }

                .ReOrderFixed thead tr {
                    display: block;
                    position: relative;
                }

            .ReOrderFixed tbody {
                display: block;
                overflow-y: visible;
                overflow-x: inherit;
                width: 100%;
                height: 100%;
                background-color: aliceblue;
            }

            .ReOrderFixed tfoot {
                display: block;
                width: 100%;
                background-color: White;
            }
    </style>
    <style type="text/css">
        .textboxOrder {
            background-color: transparent;
            font-size: 13px;
            width: 100%;
            height: 100%;
            font-weight: bold;
            text-align: right;
            padding-right: 5px;
        }

        .left_align {
            padding-left: 5px;
        }

        .right_align {
            text-align: right;
            padding-right: 5px;
        }

        .gridAlign td:nth-child(2) {
            text-align: left !important;
        }

        .gridAlign td:nth-child(3) {
            text-align: left !important;
        }

        .gridAlign td:nth-child(4) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(5) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(6) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(7) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(8) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(9) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(10) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(19) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(20) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(21) {
            text-align: right !important;
        }

        .gridAlign td:nth-child(22) {
            text-align: right !important;
        }
    </style>
    <style type="text/css">
        .Itemhistory td:nth-child(1), .Itemhistory th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
            text-align: center !important;
        }

        .Itemhistory td:nth-child(2), .Itemhistory th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
            padding-left: 5px;
        }

        .Itemhistory td:nth-child(3), .Itemhistory th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
            padding-left: 5px;
        }

        .Itemhistory td:nth-child(4), .Itemhistory th:nth-child(4) {
            min-width: 160px;
            max-width: 160px;
            text-align: left !important;
            padding-left: 5px;
        }

        .Itemhistory td:nth-child(5), .Itemhistory th:nth-child(5) {
            min-width: 85px;
            max-width: 85px;
            text-align: left !important;
            padding-left: 5px;
        }

        .Itemhistory td:nth-child(6), .Itemhistory th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
            padding-right: 5px;
        }

        .Itemhistory td:nth-child(7), .Itemhistory th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
            padding-right: 5px;
        }

        .Itemhistory td:nth-child(8), .Itemhistory th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
            padding-right: 5px;
        }

        .Itemhistory td:nth-child(9), .Itemhistory th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
            padding-right: 5px;
        }

        .Itemhistory td:nth-child(10), .Itemhistory th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
            padding-right: 5px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            fncDecimal();
            SetQualifyingItemAutoComplete();
            if ($("#<%=chkSelectAll.ClientID%>").is(':checked')) { // dinesh
                $("[id*=chkSelect]").attr("checked", true);
            }

            $('#ContentPlaceHolder1_chkSelectAll').click(function () {
                if ($(this).is(':checked')) {
                    $("[id*=chkSelect]").attr("checked", true);
                } else {
                    $("[id*=chkSelect]").attr("checked", false);
                }
            });
        }
        /// Bind Inventory to Data
        function fncGetInventory() {
            var obj = {};
            try {
                obj.sLocationCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtLocation').val();
                obj.sVendorCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtVendor').val();
                obj.sDeptCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtDepartment').val();
                obj.sCategoryCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtCategory').val();
                obj.sBrandCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtBrand').val();
                obj.sSubCategory = $('#ContentPlaceHolder1_searchFilterUserControl_txtSubCategory').val();
                obj.sMerchandise = $('#ContentPlaceHolder1_searchFilterUserControl_txtMerchandise').val();
                obj.sManufacturer = $('#ContentPlaceHolder1_searchFilterUserControl_txtManufacture').val();
                obj.sClass = $('#ContentPlaceHolder1_searchFilterUserControl_txtClass').val();
                obj.sSubClass = $('#ContentPlaceHolder1_searchFilterUserControl_txtSubClass').val();
                obj.sSection = $('#ContentPlaceHolder1_searchFilterUserControl_txtSection').val();
                obj.sBin = $('#ContentPlaceHolder1_searchFilterUserControl_txtBin').val();
                obj.sFloor = $('#ContentPlaceHolder1_searchFilterUserControl_txtFloor').val();
                obj.sShelf = $('#ContentPlaceHolder1_searchFilterUserControl_txtShelf').val();
                obj.sWarehouse = $('#ContentPlaceHolder1_searchFilterUserControl_txtWarehouse').val();
                obj.sItemCode = $('#ContentPlaceHolder1_searchFilterUserControl_txtItemCode').val();
                obj.sItemName = $('#ContentPlaceHolder1_searchFilterUserControl_txtItemName').val();
                obj.sItemtype = $('#ContentPlaceHolder1_searchFilterUserControl_txtItemType').val();
                if (obj.sLocationCode == "" && obj.sVendorCode == "" && obj.sDeptCode == "" && obj.sCategoryCode == "" &&
                    obj.sBrandCode == "" && obj.sSubCategory == "" && obj.sMerchandise == "" && obj.sManufacturer == "" &&
                    obj.sClass == "" && obj.sSubClass == "" && obj.sSection == "" && obj.sBin == "" && obj.sFloor == "" &&
                    obj.sShelf == "" && obj.sWarehouse == "" && obj.sItemCode == "" && obj.sItemName == "" && obj.sItemtype == "") {
                    ShowPopupMessageBox("Please choose any one filtration");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "frmReorderSettings.aspx/fncGetInventory",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        var tblReorderDetails;
                        tblReorderDetails = $('#tblReorderDetails tbody');
                        tblReorderDetails.children().remove();
                        var objItem = jQuery.parseJSON(msg.d);
                        if (objItem.length > 0) {
                            for (var i = 0; i < objItem.length; i++) {
                                tblReorderDetails.append("<tr>"
                                    + "<td><input type='checkbox' id='chkSelect' value=" + objItem[i]["Select"] + "></td>"
                                    + "<td id='tdLocationCode'>" + objItem[i]["LocationCode"] + "</td>"
                                    + "<td id='tdInventoryCode'>" + objItem[i]["InventoryCode"] + "</td>"
                                    + "<td id='tdDescription'>" + objItem[i]["Description"] + "</td>"
                                    + "<td id='tdVendorCode'>" + objItem[i]["VendorCode"] + "</td>"
                                    + "<td id='tdSellingprice'>" + objItem[i]["Sellingprice"] + "</td>"
                                    + "<td id='tdMRP'>" + objItem[i]["MRP"] + "</td>"
                                    + "<td><input type='text' id='MaxQty' onkeydown='return isNumberKeyWithDecimalNew(event)' onchange='return fncValidation(this)' onkeyup='return fncSetFocustoMaxQty(event,this,MaxQty)' style='width:75px;text-align:right;padding-right:5px;' value=" + objItem[i]["MAXQTY"] + "></td>"
                                    + "<td><input type='text' id='MinQty' onkeydown='return isNumberKeyWithDecimalNew(event)' onchange='return fncValidation(this)' onkeyup='return fncSetFocustoMinQty(event,this,MinQty)' style='width:75px;text-align:right;padding-right:5px;' value=" + objItem[i]["MINQTY"] + "></td>"
                                    + "<td><input type='text' id='ReOrderLevel' onkeydown='return isNumberKeyWithDecimalNew(event)' onchange='return fncValidation(this)' onkeyup='return fncSetFocustoReOrderLevel(event,this,ReOrderLevel)' style='width:75px;text-align:right;padding-right:5px;' value=" + objItem[i]["REORDERLEVEL"] + "></td>"
                                    + "</tr>");
                            }
                        }
                        //+ "<td><input type='text' id='MinQty' onkeydown='return isNumberKeyWithDecimalNew(event)' onchange='return fncValidation(this)' onkeyup='return fncSetFocustoMinQty(event,this,MinQty)' style='width:80px;text-align:right;padding-right:5px;' value=" + objItem[i]["MINQTY"] + "></td>"
                    },
                    error: function (data) {
                        console.log(data.message);

                        return false;
                    }
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        // Validaion
        function fncValidateForm() {
            try {
                var Show = '';
                if ($("#<%= ddReOrderBy.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose ReOrderBy';
                    $("#<%= ddReOrderBy.ClientID %>").focus();
                }
                else if ($("#<%= ddMinQty.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose MinQty CallBy';
                    $("#<%= ddMinQty.ClientID %>").focus();
                }
                else if ($("#<%= ddAutoPo.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose AutoPo ReqBy';
                    $("#<%= ddAutoPo.ClientID %>").focus();
                }
                else {
                    var sStatus = fncGetReOrderSettingsValue();
                    if (sStatus == "") {
                        alert("Please Select Atleast One Item");
                        return false;
                    }
                }

        if (Show != '') {
            alert(Show);
            return false;
        }
        else {
            return true;
        }
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}

function fncValidation(lnk) {
    try {
        //var rowobj = $(source).parent().parent();
        var rowobj = $(lnk).parent().parent();
        if (rowobj.find('td input[id*="MaxQty"]').val() == "")
            rowobj.find('td input[id*="MaxQty"]').val(0);
        if (rowobj.find('td input[id*="MinQty"]').val() == "")
            rowobj.find('td input[id*="MinQty"]').val(0);
        if (parseFloat(rowobj.find('td input[id*="MaxQty"]').val()) < parseFloat(rowobj.find('td input[id*="MinQty"]').val())) {
            alert('Minimumqty must be less than Maximumqty');
            rowobj.find('td input[id*="MinQty"]').val(0);
            rowobj.find('td input[id*="ReOrderLevel"]').val(0);
            rowobj.find('td input[id*="chkSelect"]').attr("checked", false);
            return false;
        }
        if (parseFloat(rowobj.find('td input[id*="MaxQty"]').val()) > 0)
            rowobj.find('td input[id*="chkSelect"]').attr("checked", true);
        else
            rowobj.find('td input[id*="chkSelect"]').attr("checked", false);
        //console.log(rowobj.find('td input[id*="chkSelect"]').checked());
        //rowobj.cells[0].getElementsByTagName("input")[0].checked = false;
        rowobj.find('td input[id*="ReOrderLevel"]').val(parseFloat(rowobj.find('td input[id*="MaxQty"]').val()) - parseFloat(rowobj.find('td input[id*="MinQty"]').val()));
        return true;
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
function SetQualifyingItemAutoComplete() {
    $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            valitemcode: item.split('|')[1],
                            valName: item.split('|')[2]
                        }
                    }))
                },
                error: function (response) {
                    ShowPopupMessageBox(response.message);
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.message);
                }
            });
        },
        focus: function (event, i) {

            $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
            event.preventDefault();
        },
        select: function (e, i) {

            $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));

            return false;
        },
        minLength: 1
    });
    }
    //Focus Set to Next Row
    function fncSetFocustoMaxQty(evt, source, curcell) {
        var verHight;
        try {
            //if (fncValidation(source)) {
            var rowobj = $(source).parent().parent();
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13 || charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MaxQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MaxQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="MaxQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MaxQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 39) {
                var NextRowobj = rowobj;
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MinQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MinQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 37) {
                var NextRowobj = rowobj.prev();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="ReOrderLevel"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="ReOrderLevel"]').select();
                }
                event.preventDefault();
            }
            //}
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncSetFocustoMinQty(evt, source, curcell) {
        var verHight;
        try {
            //if (fncValidation(source)) {
            var rowobj = $(source).parent().parent();
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13 || charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MinQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MinQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="MinQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MinQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 39) {
                var NextRowobj = rowobj;
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="ReOrderLevel"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="ReOrderLevel"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 37) {
                var NextRowobj = rowobj;
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MaxQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MaxQty"]').select();
                }
                event.preventDefault();
            }
            //}
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncSetFocustoReOrderLevel(evt, source, curcell) {
        var verHight;
        try {
            //if (fncValidation(source)) {
            var rowobj = $(source).parent().parent();
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode == 13 || charCode == 40) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="ReOrderLevel"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="ReOrderLevel"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="ReOrderLevel"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="ReOrderLevel"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 37) {
                var NextRowobj = rowobj;
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MinQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MinQty"]').select();
                }
                event.preventDefault();
            }
            else if (charCode == 39) {
                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="MaxQty"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="MaxQty"]').select();
                }
                event.preventDefault();
            }
            //}
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    // Clear form
    function clearForm() {
        try {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");
        }
        catch (err) {
            alert(err.Message);
            return false;
        }
        return false;
    }
    // decimal
    function fncDecimal() {
        try {
            $('#<%=txtReOrderDays.ClientID%>').number(true, 0);
            $('#ContentPlaceHolder1_MaxQty').number(true, 0);
            $('#ContentPlaceHolder1_MinQty').number(true, 0);
            $('#ContentPlaceHolder1_ReOrderLevel').number(true, 0);
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    // Clear table
    function cleartable() {
        try {
            $('#tblReorderDetails tbody').empty();
            $('#ContentPlaceHolder1_txtReOrderDays').val('0');
            $('#ContentPlaceHolder1_ddAutoPo').val('');
            $('#ContentPlaceHolder1_ddAutoPo').trigger("liszt:updated");
            $('#ContentPlaceHolder1_ddMinQty').val('');
            $('#ContentPlaceHolder1_ddMinQty').trigger("liszt:updated");
            $('#ContentPlaceHolder1_ddReOrderBy').val('');
            $('#ContentPlaceHolder1_ddReOrderBy').trigger("liszt:updated");
        }
        catch (err) {
            alert(err.Message);
            return false;
        }
        return false;
    }
    // Hide Filter
    function fncHideFilter() {
        try {
            if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                $("[id*=pnlFilter]").hide();
                $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                        $("#<%=divReOrderDetail.ClientID%>").attr('style', 'width:100%');
                $("select").trigger("liszt:updated");
            }
            else {
                $("[id*=pnlFilter]").show();
                $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                        $("#<%=divReOrderDetail.ClientID%>").attr('style', 'width:74%');
                $("select").trigger("liszt:updated");
            }
            return false;
        }
        catch (err) {
            return false;
            alert(err.Message);
        }
    }
    </script>
    <script type="text/javascript">
        function fncGetReOrderSettingsValue() {
            var obj;
            var xml = "";
            var Empty = "";
            var RowNo = 0;
            try {
                if ($("#tblReorderDetails tbody").children().length > 0) {
                    xml = '<NewDataSet>';
                    $("#tblReorderDetails tbody").children().each(function () {
                        obj = $(this);
                        if (obj.find('td input[id*="chkSelect"]').is(":checked")) {
                            RowNo = RowNo + 1;
                            Empty = "Value";
                            xml += "<Table>";
                            xml += "<RowNo>" + RowNo + "</RowNo>";
                            xml += "<InventoryCode>" + obj.find('td[id*="tdInventoryCode"]').text().trim() + "</InventoryCode>";
                            xml += "<LocationCode>" + obj.find('td[id*="tdLocationCode"]').text().trim() + "</LocationCode>";
                            xml += "<MaxQty>" + obj.find('td input[id*=MaxQty]').val().trim() + "</MaxQty>";
                            xml += "<MinQty>" + obj.find('td input[id*=MinQty]').val().trim() + "</MinQty>";
                            xml += "<ReOrderLevel>" + obj.find('td input[id*=ReOrderLevel]').val().trim() + "</ReOrderLevel>";
                            xml += "</Table>";
                        }
                    });

                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    $('#<%=ReOrderSettings.ClientID %>').val(xml);
                }

                if (Empty == "") {
                    xml = "";
                    $('#<%=ReOrderSettings.ClientID %>').val(xml);
                }
                return Empty;
            }
            catch (err) {
                xml = "";
                $('#<%=ReOrderSettings.ClientID %>').val(xml);
                fncToastError(err.message);
                return "";
            }
        }

        function fncClosePopup() {
            window.parent.jQuery('#popupBatchInfo').dialog('close');
            return false;
        }
    </script>
    <%--    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblReorderDetails tbody').click(function () {
                alert('');
                $('#chkSelect').click(function () {
                    if ($(this).is(':checked')) {
                        alert('a');
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    }
                });
            });
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                    EnablelocationDropDown="true"
                    EnableVendorDropDown="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableClassDropDown="true"
                    EnableSubClassDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemTypeDropDown="true"
                    EnablePriceTextBox="false"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    EnableGstDropDown="false"
                    OnClearButtonClientClick="clearForm(); return false;"
                    OnFilterButtonClientClick="fncGetInventory(); return false;" />
            </div>
            <div class="container-right-price" id="divReOrderDetail" runat="server">
                <div style="padding-top: 10px"></div>
                <div class="col-md-11" id="divDropDown">
                    <div class="col-md-3">
                        <div class="col-md-12">
                            <div class="label-left col-md-4">
                                <asp:Label ID="lblReOrderBy" runat="server" Font-Bold="True" Text="ReOrderBy"></asp:Label>
                            </div>
                            <div class="label-right col-md-7" style="padding-left: 12px">
                                <asp:DropDownList ID="ddReOrderBy" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">
                            <div class="label-left col-md-4">
                                <asp:Label ID="lblMinQty" runat="server" Font-Bold="True" Text="Min.QtyCallBy"></asp:Label>
                            </div>
                            <div class="label-right col-md-7" style="padding-left: 12px">
                                <asp:DropDownList ID="ddMinQty" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="col-md-12">
                            <div class="label-left col-md-6">
                                <asp:Label ID="lblReOrderDays" runat="server" Font-Bold="True" Text="ReOrderDays"></asp:Label>
                            </div>
                            <div class="label-right col-md-6" style="padding-left: 20px">
                                <asp:TextBox ID="txtReOrderDays" runat="server" Text="0" Style="text-align: right; padding-right: 5px" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="padding-left: 5px">
                        <div class="col-md-12">
                            <div class="label-left col-md-6">
                                <asp:Label ID="lblAutoPo" runat="server" Font-Bold="True" Text="AutoPOReq.By"></asp:Label>
                            </div>
                            <div class="label-right col-md-6" style="padding-left: 10px">
                                <asp:DropDownList ID="ddAutoPo" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ReOrderFixed Itemhistory PO_Itemhistory" style="padding-top: 10px">
                    <table id="tblReorderDetails" cellspacing="0" rules="all" border="1" style="height: 460px;">
                        <thead>
                            <tr>
                                <th scope="col">Select
                                    <asp:CheckBox ID="chkSelectAll" runat="server" Font-Bold="True" />
                                </th>
                                <th scope="col">Loc.Code
                                </th>
                                <th scope="col">ItemCode
                                </th>
                                <th scope="col">Description
                                </th>
                                <th scope="col">VendorCode
                                </th>
                                <th scope="col">SellingPrice
                                </th>
                                <th scope="col">MRP
                                </th>
                                <th scope="col">MaxQty                                    
                                </th>
                                <th scope="col">MinQty                                    
                                </th>
                                <th scope="col">ReOrderQty                                    
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div class="container-bottom-invchange">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick=" return fncHideFilter()"
                            class="button-blue" Text='<%$ Resources:LabelCaption,btn_Hide_filter %>'> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue"
                            Text='<%$ Resources:LabelCaption,btn_Update %>' OnClientClick="return fncValidateForm();"
                            OnClick="lnkUpdate_Click"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue"
                            Text='<%$ Resources:LabelCaption,btnClear %>' OnClientClick="cleartable(); return false;">
                        </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClose" runat="server" class="button-blue"
                            Text="Close(F8)" OnClientClick ="return fncClosePopup();"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="hiddencol">
            <asp:HiddenField ID="ReOrderSettings" runat="server" />
        </div>
    </div>
</asp:Content>
