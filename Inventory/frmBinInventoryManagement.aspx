﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBinInventoryManagement.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmBinInventoryManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    <script type="text/javascript">

        //After Common Search

        function pageLoad() {
            $('#<%=txtBin.ClientID %>').focus();
        }
        function fncSetValue() {
            
            try {
                if (SearchTableName == "Bin") {
                    fncGetData();
                    $('#<%=txtItemCodeAdd.ClientID %>').focus();
                }
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemNameAdd.ClientID %>').val(Description);
                    $('#<%=txtQty.ClientID %>').focus().select();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetData() {
            try {

                var obj = {};

                obj.Bin = $("#<%= txtBin.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: "frmBinInventoryManagement.aspx/fncGetBinData",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) {
                        var objdata = $.parseJSON(data.d);                   

                        $("#<%= txtTotalQty.ClientID %>").val(parseFloat(objdata.Table[0]["Qty"]).toFixed(0));

                        $("#table-bin tbody").empty();

                        if (objdata.Table2.length > 0) {
                            var objTable = "";
                            for (var i = 0; i < objdata.Table2.length; i++) {

                                objTable = "<tr>"
                                         + "<td style ='text-align:center'><img src='../images/delete.png' onclick = 'fncDeleteTableRow(this);'></td>"
                                         + "<td>" + objdata.Table2[i]["InventoryCode"] + "</td>"
                                         + "<td>" + objdata.Table2[i]["InventoryName"] + "</td>"
                                         + "<td><input type='text' class='form-control-res' value='" + objdata.Table2[i]["Qty"] + "'  onfocus='return fncFocusTable(this);' onkeydown='return fncEnterToNextRowTable(event, this);' onkeypress='return isNumber(event);' onchange='fncQtyChange();' style = 'text-align:right'></td>"
                                         + "</tr>";

                                $("#table-bin tbody").append(objTable);
                            }

                            objTable = "<tr style='font-weight:bold; background-color:yellow'>"
                                     + "<td colspan='3' style='text-align: right'>" + "Total" + "</td>"
                                     + "<td style='text-align: right'>" + parseFloat(objdata.Table1[0]["Qty"]).toFixed(2) + "</td>"
                                     + "</tr>";

                            $("#table-bin tbody").append(objTable);
                        }
                    }
                });

                return false;
            }
            catch (err) {
                ShowToastError(err);
            }
        }

        //=============================> Add Button Click
        var status = true;

        function isNumber(evt) { 
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function AddbuttonClick() {             
            try {
                   
                if ($("#<%= txtItemCodeAdd.ClientID %>").val() == "") {
                    ShowPopupMessageBox("Please Enter ItemCode");
                    return false;
                }
                var obj = {};

                obj.ItemCode = $("#<%= txtItemCodeAdd.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: "frmBinInventoryManagement.aspx/fncCheckItemCode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (data) { 
                        var objdata = $.parseJSON(data.d); 
                        fncAlertIteminBin(objdata);
                    }
                    
                });  
                return false; 
                } 
            catch (err) {
                ShowToastError(err);
            }
        }

        //================================> Focus in Table
        function fncFocusTable(source) {
            try {
                var rowobj = $(source).parent().parent();
                rowobj.select().focus();
                return false;
            }
            catch (err) {
                ShowToastError(err.message)
            }
        }

        //================================> Enter Key, Down arrow, Up arrow to Focus
        function fncEnterToNextRowTable(evt, source) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var rowobj = $(source).parent().parent();
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find("td").eq(3).children(":text").focus().select();
                        NextRowobj.select().focus();
                    }
                    return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find("td").eq(3).children(":text").focus().select();
                        prevrowobj.select().focus();
                    }
                    return false;
                }
            }
            catch (err) {
                ShowToastError(err.message)
            }
        }

        //================================> Qty Change
        function fncQtyChange() {
            try {
                var Total = 0;
                $("#table-bin tbody tr").each(function () {
                    console.log($(this).find("td").eq(3).children(":text").val());
                    if ($(this).closest('tr').next().length) {
                        Total = parseFloat(Total) + parseFloat($(this).find("td").eq(3).children(":text").val());
                    }
                });
                $('#table-bin tbody tr:last').find("td").eq(1).html(Total);
            }
            catch (err) {
                ShowToastError(err.message)
            }
        }

        //================================> Delete Table row
        function fncDeleteTableRow(source) {
            try {
                $(source).closest("tr").remove();
                fncQtyChange();
            }
            catch (err) {
                ShowToastError(err.message)
            }
        }

        //================================> Clear Text Values
        function fncClearEntervalue() {
            $("#<%= txtItemCodeAdd.ClientID %>").val('');
            $("#<%= txtItemNameAdd.ClientID %>").val('');
            $("#<%= txtQty.ClientID %>").val('');
            $("#<%= txtItemCodeAdd.ClientID %>").focus();
        }

        //================================> Save Validate
        function ValidateSave() {
            if ($('#<%=txtBin.ClientID%>').val() == '') {
                fncToastError("Please Bin Vendor");
                return false;
            }

            if (('#table-bin tbody tr').length == 0) {
                fncToastError("Please Add Inventory");
                return false;
            }

            var TotalAmount = $('#table-bin tbody tr:last').find("td").eq(1).text().trim();

            if (parseFloat(TotalAmount) > parseFloat($('#<%= txtTotalQty.ClientID%>').val())) {
                fncToastError("Please Check total Capacity");
                return false;
            }

            $("#<%=hdfTotalAmount.ClientID %>").val(TotalAmount);

            var TableXml = '<NewDataSet>';
            $("#table-bin tbody tr").each(function () {
                var cells = $("td", this);
                if ($(this).closest('tr').next().length) {
                    if (cells.length > 0) {
                        TableXml += "<Table>";
                        TableXml += "<Code>" + cells.eq(1).text().trim() + "</Code>";
                        TableXml += "<Description>" + cells.eq(2).text().trim().replace('<', '(').replace('>', ')') + "</Description>";
                        TableXml += "<Qty>" + cells.eq(3).find('input').val().trim() + "</Qty>";
                        TableXml += "</Table>";
                    }
                }
            });

            TableXml = TableXml + '</NewDataSet>'
            TableXml = escape(TableXml);
            $("#<%=hdfXmlSave.ClientID %>").val(TableXml);
        }


        function fncAlertIteminBin(object) { 
            if (object.Table.length > 0) {
                ShowPopupMessageBox("This Item Already Exists in " + object.Table[0]["BinCode"]);
                return false;
            }
            else {
                if ($('#table-bin tbody tr:last').css("background-color") == 'rgb(255, 255, 0)') {
                    $('#table-bin tbody tr:last').remove();
                }

                var bInvExists = false;

                // Inventory If exists

                $("#table-bin tbody tr").each(function () {
                    var Code = $(this).find("td").eq(1).html();
                    if (Code == $("#<%= txtItemCodeAdd.ClientID %>").val()) {
                        bInvExists = true;
                        fncToastInformation('This Inventory Already Exists');
                        return false;
                    }

                });
                if (bInvExists == false) {

                        var objTable = "<tr>"
                                     + "<td style ='text-align:center'><img src='../images/delete.png' onclick = 'fncDeleteTableRow(this);'></td>"
                                     + "<td>" + $("#<%= txtItemCodeAdd.ClientID %>").val() + "</td>"
                                     + "<td>" + $("#<%= txtItemNameAdd.ClientID %>").val() + "</td>"
                                     + "<td><input type='text' class='form-control-res' value='" + $("#<%= txtQty.ClientID %>").val() + "' onfocus='return fncFocusTable(this);' onkeydown='return fncEnterToNextRowTable(event, this);' onkeypress='return isNumber(event);' onchange='fncQtyChange();' style = 'text-align:right'></td>"
                                     + "</tr>";

                        $("#table-bin tbody").append(objTable);
                    }

              
                    var Total = 0;
                    $("#table-bin tbody tr").each(function () {
                        Total = parseFloat(Total) + parseFloat($(this).find("td").eq(3).children(":text").val());
                        
                    });

                    objTable = "<tr style='font-weight:bold; background-color:yellow'>"
                                 + "<td colspan='3' style='text-align: right'>" + "Total" + "</td>"
                                 + "<td style='text-align: right'>" + Total + "</td>"
                                 + "</tr>";

                    $("#table-bin tbody").append(objTable);


                    var TotalAmount = $('#table-bin tbody tr:last').find("td").eq(1).text().trim();
                    if (parseFloat(TotalAmount) > parseFloat($("#<%= txtTotalQty.ClientID %>").val())) {
                        fncToastInformation('Total Qty Must be Less Bin Qty');
                        //$('#table-bin tbody tr:last').find("td").eq(1).text() = 0;
                        fncClearEntervalue();
                        $('#table-bin tbody tr:last').prev().find("td").eq(3).children(":text").focus().select();
                        //console.log($('#table-bin tr:last').find("td").eq(3).children(":text").focus().select());
                        return false;
                            
                        }
                    fncClearEntervalue();

                    return false;
            }
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BinInventoryManagement');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BinInventoryManagement";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="main-container" style="overflow: hidden">
                <div id="breadcrumbs" class="breadcrumbs" runat="Server">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page">Bin Management</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>

                    </ul>
                </div>

                <div class="container-group">
                    <div class="container-control">
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label1" runat="server" Text="Bin Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label2" runat="server" Text="Qty"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtTotalQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="bin-table" style="overflow: auto; height: 380px; width: 100%">
                        <table class="table-design" id="table-bin" style="display: table">
                            <thead class="table-header table-bin-header">
                                <tr>
                                    <th>Delete</th>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>Qty</th>
                                </tr>
                            </thead>
                            <tbody class="table-body">
                            </tbody>
                        </table>
                        <asp:HiddenField ID="hdfXmlSave" runat="server" />
                        <asp:HiddenField ID="hdfTotalAmount" runat="server" />
                    </div>

                    <div class="bottom-purchase-container-add gid_top_border">
                        <div class="col-md-12" style="padding: 0px">
                            <div class="col-md-2" style="padding: 0px">
                                <asp:Label ID="Label8" runat="server" Text="Item Code"></asp:Label>
                                <asp:TextBox ID="txtItemCodeAdd" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCodeAdd', 'txtLQty');"></asp:TextBox>
                            </div>
                            <div class="col-md-5" style="padding: 0px">
                                <asp:Label ID="Label9" runat="server" Text="ItemDesc"></asp:Label>
                                <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="col-md-2" style="padding: 0px">
                                <asp:Label ID="Label26" runat="server" Text="Qty"></asp:Label>
                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                            </div>
                            <div style="margin-top: 14px">
                                <div class="col-md-3" style="padding: 0px">
                                    <div style="float: right">
                                        <asp:LinkButton ID="lnkAddInv" runat="server" class="button-blue" OnClientClick="return AddbuttonClick(); return false;" Text="Add"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkClearValue" runat="server" Text="Clear" class="button-blue" OnClientClick="fncClearEntervalue();return false;"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return ValidateSave()" OnClick="lnkSave_Click" Text="Save"></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text="Clear"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
