﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmInventoryParentChildSettings.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryParentChildSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 131px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 306px;
            max-width: 250px;
        }
        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            min-width: 111px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 201px;
            max-width: 209px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }
        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }
        .selectedCell
        {
            background-color: Green;
        }
        .unselectedCell
        {
            background-color: white;
        }
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        td
        {
            cursor: grab;
        }
        .selected_row
        {
            background-color: Teal;
            color: White;
        }
        
        .editableTable
        {
            border: solid 1px;
            width: 100%;
        }
        
        .editableTable td
        {
            border: solid 1px;
        }
        
        .editableTable .cellEditing
        {
            padding: 0;
        }
        
        .editableTable .cellEditing input[type=text]
        {
            width: 100%;
            border: 0;
            background-color: rgb(255,253,210);
        }
    </style>
    <script type="text/javascript">



        $(function () {
            SetAutoComplete();
            //$("[id*=lnkSave]").hide();
        });

        function fncfocusAddbtn(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }

        $(function () {

            $("[id*=grdChildItem]").on('click', 'td', function (e) {
                DisplayChildDetails($(this).closest("tr"));
                e.preventDefault();
            });

            //            $("[id*=grdChildItem] td").click(function () {
            //                DisplayChildDetails($(this).closest("tr"));
            //                e.preventDefault();
            //            });
            //            return false;
        });

        $(function () {
            $("[id*=grdBulkItem] td").click(function () {
                DisplayParentDetails($(this).closest("tr"));
            });
            return false;
        });

        //        $(function () {
        //            $("table tr:has(td)").css({ background: "ffffff" }).hover(
        //                        function () { $(this).css({ background: "#fa8170" }); }
        //                        , function () { $(this).css({ background: "#ffffff" }); }
        //                        );
        //        });

        $(function () {
            $("[id*=grdBulkItem] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdBulkItem] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });


        $(function () {
            $("[id*=grdChildItem] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdChildItem] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });

        // Cell editing in Gridview
        //        $(function () {
        //            $("td").dblclick(function () {
        //                var OriginalContent = $(this).text();

        //                $(this).addClass("cellEditing");
        //                $(this).html("<input type='text' value='" + OriginalContent + "' />");
        //                $(this).children().first().focus();

        //                $(this).children().first().keypress(function (e) {
        //                    if (e.which == 13) {
        //                        var newContent = $(this).val();
        //                        $(this).parent().text(newContent);
        //                        $(this).parent().removeClass("cellEditing");
        //                    }
        //                });

        //                $(this).children().first().blur(function () {
        //                    $(this).parent().text(OriginalContent);
        //                    $(this).parent().removeClass("cellEditing");
        //                });
        //            });
        //        });

        //On UpdatePanel Refresh.
        //        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //        if (prm != null) {
        //            prm.add_endRequest(function (sender, e) {
        //                if (sender._postBackSettings.panelsToUpdate != null) {
        //                    SetAutoComplete();
        //                    SetGridClickEvent();
        //                }
        //            });
        //        };


        //        $(document).ready(function () {
        //            $("#<%=grdChildItem.ClientID%> tr:has(td)").click(function () {
        //                $(this).fadeOut(1000, function () {
        //                    $(this).remove();
        //                });
        //            });
        //        });


        function SetAutoComplete() {

            $("[id$=txtChildCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryParentChildSettings.aspx/GetInventoryChildDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1],
                                    val2: item.split('|')[2],
                                    val3: item.split('|')[3],
                                    val4: item.split('|')[4],
                                    val5: item.split('|')[5]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //alert(i.item.val);
                    $('#<%=txtChildCode.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtCdescription.ClientID %>').val($.trim(i.item.val2));
                    $('#<%=txtCmrp.ClientID %>').val($.trim(i.item.val3));
                    $('#<%=txtCuom.ClientID %>').val($.trim(i.item.val4));
                    $('#<%=txtCweight.ClientID %>').val($.trim(i.item.val5));
                    $('#<%=txtCweight.ClientID %>').focus().select();
                    $('#<%=txtChildCode.ClientID %>').prop('disabled', true);

                    $("#hidenChildCode").val($.trim(i.item.val));

                    return false;
                },
                minLength: 1
            });
        }


        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtChildCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }

        function ClearTextBox() {
            $('#<%=txtChildCode.ClientID %>').val('');
            $('#<%=txtCdescription.ClientID %>').val('');
            $('#<%=txtCmrp.ClientID %>').val('');
            $('#<%=txtCuom.ClientID %>').val('');
            $('#<%=txtCweight.ClientID %>').val('');
            $("#hidenChildCode").val('');
            $('#<%=txtChildCode.ClientID %>').prop('disabled', false);
            $('#<%=txtChildCode.ClientID %>').focus().select();
            return false;
        }

        function validateforDelete() {
            try {

                if ($('#<%=txtParentCode.ClientID %>').val() == '') {
                    alert("Please select Parent ItemCode");
                    $('#<%=txtParentCode.ClientID %>').prop('disabled', false);
                    $('#<%=txtParentCode.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtChildCode.ClientID %>').val() == '') {

                    alert("Please select Child ItemCode");
                    $('#<%=txtChildCode.ClientID %>').focus().select();
                    $('#<%=txtChildCode.ClientID %>').prop('disabled', false);
                    $('#<%=txtChildCode.ClientID %>').focus().select();
                    return false;
                }

                //var trow = $("[id*=grdChildItem] tr:last-child").find('td').length;
                var rowCount = $("[id*=grdChildItem] tr:last").index() + 1;
                //alert(rowCount);
                if (rowCount == 2) {
                    $("[id*=grdChildItem] tr:last").after('<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
                }

                //deleterow();
                XmlGridValue();

                return true;
            }
            catch (err) {

                alert(err.message);
                console.log(err);
                return false;
            }
        }

        function AddremoveEmptyrow() {
            //            if ($("[id*=grdChildItem] tr:last-child").find('td').length = 1) {
            //                //Addemptyrow()
            //                $("[id*=grdChildItem] tr:last").after('<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
            //            }
            //            else {
            //                $("[id*=grdChildItem] tr").each(function () {
            //                    if (!$.trim($(this).text())) $(this).remove();
            //                });
            //            }
            $("[id*=grdChildItem] tr").each(function () {
                if (!$.trim($(this).text())) $(this).remove();
            });

            return false;
        }



        function saveValidate() {
            XmlGridValue();
            if ($("#hidenParentcode").val() == 'NO' || $("#hidenParentcode").val() == '') {
                ShowPopupMessageBox('Please Select Bulk Item..!');
                return false;
            }
        }
        function Clearall() {
            //debugger;
            $('input[type="text"]').val('');
            $("#hidenChildCode").val('');
            $("#hidenParentcode").val('');
            OnParentClick("", "Child");
            //$("[id*=grdChildItem] tr").not($("[id*=grdChildItem] td")).remove();
            //AddRow();
            $("[id*=lnkSave]").show();
            return false;
        }

        function AddbuttonClick() {
            try {

                if ($('#<%=txtParentCode.ClientID %>').val() == '') {

                    alert("Please check Parent ItemCode");
                    return false;
                }
                if ($('#<%=txtChildCode.ClientID %>').val() == '') {

                    alert("Please check Child ItemCode");
                    $('#<%=txtChildCode.ClientID %>').focus().select();
                    return false;
                }


                var checked = false;
                var txtitem = $('#<%=txtChildCode.ClientID %>').val();
                $("#<%=grdChildItem.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td.Itemcode").html();

                    //alert(gitem);

                    if ($.trim(gitem) == $.trim(txtitem)) {
                        alert("Item already Exists !");
                        checked = true;
                    }
                });

                var ResultArray = [];
                $('#<%=grdChildItem.ClientID %>').find('input:hidden').each(function () {
                    ResultArray.push($(this).val());
                    //alert($(this).val());
                });

                if ($.inArray(txtitem, ResultArray) != -1) {
                    // found it
                    //alert(ResultArray)
                    alert("Item already Exists !");
                    checked = true;
                }
                //alert(("#hidenParentcode").val());

                if (checked == false) {
                    AddRow();
                    $("[id*=lnkSave]").show();
                }


                ClearTextBox();
                return false;
            }
            catch (err) {
                alert(err.message);
                console.log(err);
                return false;
            }

        }

        function checkExistItem(itemcode) {

            $("#<%=grdChildItem.ClientID%> tr").each(function () {
                //alert($(this).find("td.Itemcode").html());
                if (!this.rowIndex) return;
                if ($(this).find("td.Itemcode").html() == itemcode)
                    return true;
            });
            //debugger;
            return false;
        }

        function AddRow1() {
            try {
                //Reference the GridView.
                var gridView = document.getElementById("<%=grdChildItem.ClientID %>");

                //Reference the TBODY tag.
                var tbody = gridView.getElementsByTagName("tbody")[0];

                //Reference the first row.
                var row = tbody.getElementsByTagName("tr")[1];

                //Check if row is dummy, if yes then remove.
                if (row.getElementsByTagName("td")[0].innerHTML.replace(/\s/g, '') == "") {
                    tbody.removeChild(row);
                }

                //Clone the reference first row.
                row = row.cloneNode(true);

                //Add the Name value to first cell.
                var txtChildCode = document.getElementById("<%=txtChildCode.ClientID %>"); // $("[id*=txtChildCode]").val();
                SetValue(row, 0, "Inventorycode", txtChildCode);

                var txtCdescription = document.getElementById("<%=txtCdescription.ClientID %>"); // $("[id*=txtCdescription]").val();
                SetValue(row, 1, "Description", txtCdescription);

                var txtCMrp = document.getElementById("<%=txtCmrp.ClientID %>");  //$("[id*=txtCmrp]").val();
                SetValue(row, 2, "Mrp", txtCMrp);

                var txtCUom = document.getElementById("<%=txtCuom.ClientID %>");  //$("[id*=txtCuom]").val();
                SetValue(row, 3, "Uom", txtCUom);

                var txtWeight = document.getElementById("<%=txtCweight.ClientID %>"); // $("[id*=txtCweight]").val();
                SetValue(row, 4, "weight", txtWeight);

                //Add the row to the GridView.
                tbody.appendChild(row);

                $('#<%=txtChildCode.ClientID %>').prop('disabled', false);
                $('#<%=txtChildCode.ClientID %>').focus().select();

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        };

        function SetValue(row, index, name, textbox) {

            try {
                //Reference the Cell and set the value.
                row.cells[index].innerHTML = textbox.value;

                //Create and add a Hidden Field to send value to server.
                var input = document.createElement("input");
                input.type = "hidden";
                input.name = name;
                input.value = textbox.value;
                row.cells[index].appendChild(input);

                //Clear the TextBox.
                textbox.value = "";

                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function DisplayParentDetails(row) {
            try {
                //var message = "";
                //message += "Id: " + $("td", row).eq(0).html();

                $("[id*=txtParentCode]").val($("td", row).eq(0).html());
                $("[id*=txtPdescription]").val($("td", row).eq(1).html());
                $("[id*=txtPmrp]").val($("td", row).eq(2).html());
                $("[id*=txtPuom]").val($("td", row).eq(3).html());
                $("[id*=txtPweight]").val($("td", row).eq(4).html());

                var invCode = $("[id*=txtParentCode]").val();
                $("#hidenParentcode").val(invCode);
                OnParentClick(invCode, "Child");
                $("[id*=lnkSave]").hide();
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }

        function DisplayChildDetails(row) {
            try {
                //var message = "";   
                //alert("child");
                //debugger;
                $("[id*=txtChildCode]").val($("td", row).eq(0).html());
                $("[id*=txtCdescription]").val($("td", row).eq(1).html());
                $("[id*=txtCmrp]").val($("td", row).eq(2).html());
                $("[id*=txtCuom]").val($("td", row).eq(3).html());
                $("[id*=txtCweight]").val($("td", row).eq(4).html());
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }

        function AddRow() {

            try {
                //debugger;
                var ChildCode = $("[id*=txtChildCode]").val();
                var Cdescription = $("[id*=txtCdescription]").val();
                var Cmrp = $("[id*=txtCmrp]").val();
                var Cuom = $("[id*=txtCuom]").val();
                var Cweight = $("[id*=txtCweight]").val();
                var ParentCode = $("[id*=txtParentCode]").val();

                if (ChildCode != "" && Cdescription != "" && Cmrp != "") {
                    var row = $("[id*=grdChildItem] tr:last").clone();
                    // $("[id*=grdChildItem] tr:last").remove(); 
                    $("td:nth-child(1)", row).html(ChildCode);
                    $("td:nth-child(2)", row).html(Cdescription);
                    $("td:nth-child(3)", row).html(Cmrp);
                    $("td:nth-child(4)", row).html(Cuom);
                    $("td:nth-child(5)", row).html(Cweight);
                    if (checkExistItem(ChildCode) == true) {
                        $("td:nth-child(6)", row).html("U");
                        // alert("update");
                    }
                    else {
                        $("td:nth-child(6)", row).html("S");
                        // alert("up");
                    }
                    $("[id*=grdChildItem] tbody").append(row);

                }

                $("[id*=txtChildCode]").val();
                $("[id*=txtCdescription]").val();
                $("[id*=txtCmrp]").val();
                $("[id*=txtCuom]").val();
                $("[id*=txtCweight]").val();


                // $(this).closest("tr").find('td:not(:empty):first').remove();   

                $("#<%=grdChildItem.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td").html();
                    if ($.trim(gitem) == '') {
                        $(this).remove();
                    }
                });

                XmlGridValue();
                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function XmlGridValue() {
            try {

                $("#HiddenNewitem").val('Y');
                var xml = '<NewDataSet>';
                $("#<%=grdChildItem.ClientID %> tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        //alert(cells.eq(0).text().trim());
                        if (cells.eq(0).text().trim() != '') {
                            xml += "<Table>";
                            xml += "<Inventorycode>" + cells.eq(0).text().trim() + "</Inventorycode>";
                            xml += "<Description>" + cells.eq(1).text().trim() + "</Description>";
                            xml += "<MRP>" + cells.eq(2).text().trim() + "</MRP>";
                            xml += "<Uom>" + cells.eq(3).text().trim() + "</Uom>";
                            xml += "<weight>" + cells.eq(4).text().trim() + "</weight>";

                            //                        for (var i = 1; i < cells.length; ++i) {
                            //                            xml += '<Data year="' + $("#grdChildItem").find("th").eq(i).text() + '" status="' + $("option:selected", cells.eq(i)).text() + '"/>';
                            //                        }

                            xml += "</Table>";
                        }
                    }
                });

                xml = xml + '</NewDataSet>'
                //alert(xml);
                $("#HiddenXmldata").val(escape(xml));
                //alert($("#HiddenXmldata").val());

                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        //        function OnChildItemSelect(sInvCode, sType) {
        //            $.ajax({
        //                type: "POST",
        //                url: "frmInventoryParentChildSettings.aspx/GetInventoryDetails",
        //                data: "{'InvCode':'" + sInvCode + "','Type':'" + sType + "'}",
        //                contentType: "application/json; charset=utf-8",
        //                dataType: "json",
        //                success: OnInventorySuccess,
        //                failure: function (response) {
        //                    alert(response.d);
        //                },
        //                error: function (response) {
        //                    alert(response.d);
        //                }
        //            });
        //        }

        //        function OnInventorySuccess(response) {
        //            try {

        //                $('#<%=txtChildCode.ClientID %>').val(response.d[0]);
        //                $('#<%=txtCdescription.ClientID %>').val(response.d[1]);
        //                $('#<%=txtCmrp.ClientID %>').val(response.d[2]);
        //                $('#<%=txtCuom.ClientID %>').val(response.d[3]);
        //                $('#<%=txtCweight.ClientID %>').val(response.d[4]); 
        //            }
        //            catch (err) {
        //                alert(err.Message);
        //                console.log(err);
        //            }
        //        }


        function OnParentClick(sInvCode, sType) {
            //debugger;
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryParentChildSettings.aspx/GetInventory")%>',
                data: "{'InvCode':'" + sInvCode + "','Type':'" + sType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }



        //C.CInventorycode, I.Description, I.MRP, I.Uom, I.weight, C.PInventorycode 
        function OnSuccess(response) {
            try {

                if (response.d == '<NewDataSet />')
                    response.d = '<NewDataSet> <Table> <Inventorycode>  </Inventorycode> <Description>  </Description> <Mrp>  </Mrp> <Uom>  </Uom> <Weight> </Weight> <Parentcode>  </Parentcode> </Table>  </NewDataSet>';

                var xmlDoc = $.parseXML(response.d);
                var xml = $(xmlDoc);
                var customers = xml.find("Table");

                var row = $("[id*=grdChildItem] tr:last-child").clone(true);
                //var row = $("#<%=grdChildItem.ClientID %> tr:last").prev("td").clone(true);
                $("[id*=grdChildItem] tr").not($("[id*=grdChildItem] tr:first-child")).remove();
                $.each(customers, function () {
                    var customer = $(this);
                    $("td", row).eq(0).html($(this).find("Inventorycode").text());
                    $("td", row).eq(1).html($(this).find("Description").text());
                    $("td", row).eq(2).html($(this).find("Mrp").text());
                    $("td", row).eq(3).html($(this).find("Uom").text());
                    $("td", row).eq(4).html($(this).find("Weight").text());
                    $("td", row).eq(5).html($(this).find("Parentcode").text());
                    $("[id*=grdChildItem]").append(row);
                    row = $("[id*=grdChildItem] tr:last-child").clone(true);
                });
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }

        function Addemptyrow() {
            try {

                debugger;
                var xmldata = '<NewDataSet> <Table> <Inventorycode>  </Inventorycode> <Description>  </Description> <Mrp>  </Mrp> <Uom>  </Uom> <Weight> </Weight> <Parentcode>  </Parentcode> </Table>  </NewDataSet>';
                var xmlDoc = $.parseXML(xmldata);
                var xml = $(xmlDoc);
                var customers = xml.find("Table");

                var row = $("[id*=grdChildItem] tr:last-child").clone(true);
                //$("[id*=grdChildItem] tr").not($("[id*=grdChildItem] tr:first-child")).remove();
                $.each(customers, function () {
                    var customer = $(this);
                    $("td", row).eq(0).html($(this).find("Inventorycode").text());
                    $("td", row).eq(1).html($(this).find("Description").text());
                    $("td", row).eq(2).html($(this).find("Mrp").text());
                    $("td", row).eq(3).html($(this).find("Uom").text());
                    $("td", row).eq(4).html($(this).find("Weight").text());
                    $("td", row).eq(5).html($(this).find("Parentcode").text());
                    $("[id*=grdChildItem]").append(row);
                    row = $("[id*=grdChildItem] tr:last-child").clone(true);
                });
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }

        function deleterow() {
            var invcode = $('#<%=txtChildCode.ClientID %>').val();
            $("[id*=grdChildItem]").find("tr:contains('" + invcode + "')").remove();
            //            $("[id*=grdChildItem]").fadeOut(1000, function () {
            //                $("[id*=grdChildItem]").find("tr:contains('" + invcode + "')").remove();
            //             });
            $('#<%=txtChildCode.ClientID %>').val('');
            $('#<%=txtCdescription.ClientID %>').val('');
            $('#<%=txtCmrp.ClientID %>').val('');
            $('#<%=txtCuom.ClientID %>').val('');
            $('#<%=txtCweight.ClientID %>').val('');
        }
        function Search_Gridview(strKey) {
            try {

                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdBulkItem.ClientID %>");
                var rowData;

                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;

                }
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    e.preventDefault();
                } 
        }
    };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function pageLoad() {
            try{
                
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'InventoryParentChildSettings');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "InventoryParentChildSettings";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" id="form1">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Parent Child Settings </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-pc">
            <div class="left-container-pc">
                <div class="left-container-header">
                    Bulk Item
                </div>
                <asp:Label ID="lbldesc" Text='<%$ Resources:LabelCaption,lblDesc %>' runat="server" />
                <asp:TextBox ID="txtSearch" CssClass="form-control-res" runat="server" onkeyup="Search_Gridview(this)" />
                <%--<div class="container-group-price-pc">--%>
                    <div class="GridDetails">

                        <%--    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                        <div class="grdLoad">
                            <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                        <th scope="col" style="padding:0px 39px 0px 26px">Inventorycode
                                        </th>
                                        <th scope="col" style="padding: 0px 236px 0px 113px;position:fixed;background:#4163e1">Description
                                        </th>
                                    </tr>
                                </thead>
                            </table>

                            <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height:370px;width:516px; background-color: aliceblue;">
                                <asp:GridView ID="grdBulkItem" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="false"
                                    Width="100%" ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn grdLoad">
                                    <PagerStyle CssClass="pshro_text" />
                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                      <AlternatingRowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    <Columns>
                                        <asp:BoundField HeaderText="InventoryCode" DataField="InventoryCode" ></asp:BoundField>
                                        <asp:BoundField HeaderText="Description" DataField="Description"></asp:BoundField>
                                        <asp:BoundField HeaderText="MRP" DataField="Mrp" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                        <asp:BoundField HeaderText="UOM" DataField="Uom" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                        <asp:BoundField HeaderText="Weight" DataField="Weight" ItemStyle-CssClass="hiddencol"
                                            HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        PageButtonCount="5" Position="Bottom" />
                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                    
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                </div>
            
            <div class="right-container-top-pc">
                <div class="right-container-top-header">
                    Mapped Inventory
                </div>
                <div class="right-container-top-detail">

                    <div class="GridDetails">
                        <div class="row">
                            <div class="grdLoad1">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Inventorycode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Mrp
                                            </th>
                                            <th scope="col">Uom
                                            </th>
                                            <th scope="col">Weight
                                            </th>
                                            <th scope="col">Parentcode
                                            </th>
                                        </tr>
                                    </thead>
                                </table>

                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll;height:100px; width:850px; background-color: aliceblue;">
                                    <asp:GridView ID="grdChildItem" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="false"
                                        ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn grdLoad" >
                                        <PagerStyle CssClass="pshro_text" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Inventorycode" DataField="Inventorycode"></asp:BoundField>
                                            <asp:BoundField HeaderText="Description" DataField="Description"></asp:BoundField>
                                            <asp:BoundField HeaderText="Mrp" DataField="Mrp" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Uom" DataField="Uom"></asp:BoundField>
                                            <asp:BoundField HeaderText="Weight" DataField="Weight" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Parentcode" DataField="Parentcode" ></asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            PageButtonCount="5" Position="Bottom" />
                                        <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-container-top-pc">
                <div class="right-container-top-header">
                    Parent Inventory
                </div>
                <div class="right-container-top-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpic" runat="server" Text='<%$ Resources:LabelCaption,lblItemcode %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtParentCode" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Description">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpdes" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPdescription" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpmrp" runat="server" Text='<%$ Resources:LabelCaption,lbl_mrp %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPmrp" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpuom" runat="server" Text='<%$ Resources:LabelCaption,lbl_Uom %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPuom" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpwgt" runat="server" Text='<%$ Resources:LabelCaption,lbl_weight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPweight" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-container-top-pc">
                <div class="right-container-bottom-header">
                    Child Inventory
                </div>
                <div class="right-container-bottom-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblitemcode %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtChildCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="control-group-left-Description">
                            <div class="label-left-pc">
                                <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbldesc %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCdescription" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_Mrp %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCmrp" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Uom %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCuom" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-pc">
                            <div class="label-left-pc">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_weight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCweight" runat="server" CssClass="form-control-res" onkeydown="return fncfocusAddbtn(event)"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClientClick="return AddbuttonClick()"
                            Text='<%$ Resources:LabelCaption,btnadd %>'> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                            Text='<%$ Resources:LabelCaption,btnclear %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="control-button">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClick="lnkSave_Click" OnClientClick="return saveValidate()"
                        Text='<%$ Resources:LabelCaption,btnsave %>'></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkPrint" runat="server" CssClass="button-red" Visible="False"
                        Text='<%$ Resources:LabelCaption,btn_Print %>'> Print </asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClearform" runat="server" CssClass="button-red" OnClientClick="return Clearall()"
                        Text='<%$ Resources:LabelCaption,btnclear %>'> </asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkDelet" runat="server" CssClass="button-red" OnClientClick="return validateforDelete()"
                        Text='<%$ Resources:LabelCaption,btn_delete %>'
                        OnClick="lnkDelet_Click"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidenChildCode" runat="server" ClientIDMode="Static" Value="NO" />
    <asp:HiddenField ID="hidenParentcode" runat="server" ClientIDMode="Static" Value="NO" />
    <asp:HiddenField ID="HiddenNewitem" runat="server" ClientIDMode="Static" Value="NO" />
    <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="NO" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
