﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmRepackingSchedule.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmRepackingSchedule" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<%@ Register TagPrefix="ups" TagName="ItemHistory" Src="~/UserControls/ItemHistory.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 90px;
            max-width: 90px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 40px;
            max-width: 40px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 45px;
            max-width: 45px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 56px;
            max-width: 56px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 55px;
            max-width: 55px;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 85px;
            max-width: 85px;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            min-width: 84px;
            max-width: 84px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {


        });
        function pageLoad() {
            try {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                $("#<%= txtEntryDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });

                $("#<%= txtFromDate.ClientID %>, #<%= txtToDate.ClientID %>, #<%= txtEntryDate.ClientID %>").on('keydown', function () {
                    return false;

                });
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    <script type="text/javascript" language="Javascript">

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //alert($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.find('input[id *= chkSingle]').prop('checked', true);
                tr.next().find('input[id *= txtNewQty]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function fncValidateCheckAll() {
            try {
                var gridtr = $("#<%= gvRepack.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    if ($("#<%=Check.ClientID %>").text() == "Select All") {

                        $("#<%=Check.ClientID %>").text("De-Select All");
                        $("#<%=gvRepack.ClientID %> [id*=chkSingle]").prop('checked', true);

                    }
                    else {

                        $("#<%=Check.ClientID %>").text("Select All");
                        $("#<%=gvRepack.ClientID %> [id*=chkSingle]").prop('checked', false);


                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateExportNoItems() {
            try {
                var gridtr = $("#<%= gvRepack.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems

                    fncShowAlertNoItemsMessage();
                    return false;

                }
                else {
                    return true;

                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateAllZero() {
            try {
                var gridtr = $("#<%= gvRepack.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    $("#<%=gvRepack.ClientID %> [id*=txtNewQty]").val('0.000');
                    //alert($("#<%=gvRepack.ClientID %> [id*=chkSingle]").is(":checked"));
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateSave() {
            try {
                fncShowConfirmSaveMessage();

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvRepack.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvRepack.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvRepack.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteConfirmmessage").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteConfirmmessage").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirmmessage").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirmmessage").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdate").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdate").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdate").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }

        function InvHistory_Button_Click() {
            InitializeDialogPay_Button();
            $("#dialog-ViewItemHistory").dialog("open");

        }
        function InitializeDialogPay_Button() {
            try {
                $("#dialog-ViewItemHistory").dialog({
                    appendTo: 'form:first',
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 460,
                    modal: true,

                    buttons: {

                    }
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        function clearForm() {
            //alert("Clear");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("select").val(0);
            $("select").trigger("liszt:updated");
            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }
        $(document).ready(function () {
            try {
                
                $("#ContentPlaceHolder1_searchFilterUserControl_txtBrand").bind('keydown', function (e) {
                    //alert(ControlID);
                    //alert(SetFocusID);
                    SetFocusID = 'searchFilterUserControl_txtMerchandise';
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13 || keyCode === 9) {
                $('#ContentPlaceHolder1_searchFilterUserControl_txtMerchandise').select();
            }
                });
                $("#ContentPlaceHolder1_searchFilterUserControl_txtShelf").bind('keydown', function (e) {
                    SetFocusID = 'searchFilterUserControl_txtItemCode';
                    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                    if (keyCode === 13 || keyCode === 9) {
                        $('#ContentPlaceHolder1_searchFilterUserControl_txtItemCode').select();
                    }
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });
        function fncSetValue() {
            try {
                debugger;
                alert(ControlID);
                if (ControlID == "#ContentPlaceHolder1_searchFilterUserControl_txtBrand") {
            $('#ContentPlaceHolder1_searchFilterUserControl_txtMerchandise').select();
        }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'RepackingSchedule');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "RepackingSchedule";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }
         function ValidateFetch() {
             if ($("#<%= txtReOrderDays.ClientID %>").val() == "") {
                 ShowPopupMessageBox("Reorderdays should not Empty");
                 return false;
             }
         }

     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                            <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="../Inventory/frmRepackingList.aspx">
                            <%=Resources.LabelCaption.lbl_RepackList%></a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active-page">
                            <%=Resources.LabelCaption.lbl_Repack_Schedule%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-price EnableScroll"  >
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Search Type
                            </div>
                        </div>
                        <div class="control-groupbarcode">
                            <div style="text-align: center">
                                <div>
                                    <div style="float: left">
                                        <asp:RadioButton ID="rbnBulk" runat="server" Text='Bulk' GroupName="barcode" />
                                    </div>
                                    <div class="control-group-split">
                                        <div class="control-group-left" style="width: 100%">
                                            <div class="label-left" style="width: 40%">
                                                <asp:Label ID="Label7" runat="server" Text="Bulk Item"></asp:Label>
                                            </div>
                                            <div class="label-right" style="width: 60%">
                                                <asp:DropDownList ID="ddlBulkItem" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="float: left; margin-left: 0%">
                                        <asp:RadioButton ID="rbnGeneral" runat="server" Text='General' Checked="True" GroupName="barcode" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                            EnableVendorDropDown="true"
                            EnableDepartmentDropDown="true"
                            EnableCategoryDropDown="true"
                            EnableBrandDropDown="true"
                            EnableMerchandiseDropDown="true"
                            EnableManufactureDropDown="true"
                            EnableFloorDropDown="true"
                            EnableSectionDropDown="true"
                            EnableBinDropDown="true"
                            EnableShelfDropDown="true"
                            EnableItemCodeTextBox="true"
                            EnableItemNameTextBox="true" />
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Compare Sales History
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 100%">
                                <div class="label-left" style="width: 39%">
                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 60%">
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 100%">
                                <div class="label-left" style="width: 39%">
                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 60%">
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="barcode-header">
                                Entry
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 100%">
                                <div class="label-left" style="width: 39%">
                                    <asp:Label ID="Label8" runat="server" Text="Entry Date"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 60%">
                                    <asp:TextBox ID="txtEntryDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 100%">
                                <div class="label-left" style="width: 39%">
                                    <asp:Label ID="Label9" runat="server" Text="Re-Ordering day's"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 60%">
                                    <asp:TextBox ID="txtReOrderDays" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" OnClientClick="return ValidateFetch();" OnClick="lnkLoadFilter_Click"
                                    Text='<%$ Resources:LabelCaption,btn_Fetch %>'><i class="icon-play" ></i></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick=" return clearForm()"
                                    Text='<%$ Resources:LabelCaption,btnClear %>'><i class="icon-play"></i></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                        style="height: 500px">
                        <div class="control-group-split">
                            <div class="control-group-left" style="width: 60%">
                                <div class="label-left" style="width: 30%">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblVendorName %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 60%">
                                    <asp:TextBox ID="txtVendorName" onMouseDown="return false" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split" style="width: 100%">
                            <div class="control-group-left" style="width: 40%">
                                <div class="label-left" style="width: 45%">
                                    <asp:Label ID="Label3" runat="server" Text="Re-Ordering Days's"></asp:Label>
                                </div>
                                <div class="label-right" style="width: 30%">
                                    <asp:TextBox ID="TextBox1" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right" style="width: 60%">
                                <div class="label-left" style="width: 20%">
                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                                </div>
                                <div class="label-right" style="width: 50%">
                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--<div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 700px">--%>
                        <div class="gridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="right-container-top-detail">
                                        <div class="grid">
                                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Select
                                                        </th>
                                                        <th scope="col">Location
                                                        </th>
                                                        <th scope="col">InventoryCode
                                                        </th>
                                                        <th scope="col">Item Name
                                                        </th>
                                                        <th scope="col">MRP
                                                        </th>
                                                        <th scope="col">Selling Price
                                                        </th>
                                                        <th scope="col">Cost
                                                        </th>
                                                        <th scope="col">Min Qty
                                                        </th>
                                                        <th scope="col">Sold Qty
                                                        </th>
                                                        <th scope="col">Qty On Hand
                                                        </th>
                                                        <th scope="col">Average Per Day
                                                        </th>
                                                        <th scope="col">Sugg Qty
                                                        </th>
                                                        <th scope="col">Order Qty
                                                        </th>
                                                        <th scope="col">ExcessDays
                                                        </th>
                                                        <th scope="col">No.Of Items Received
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div class="GridDetails" style="overflow-x: auto; overflow-y: auto; height: 400px; width: 986px; background-color: aliceblue;">
                                                <asp:GridView ID="gvRepack" runat="server"  AutoGenerateColumns="False" CssClass="pshro_GridDgn grdLoad"
                                                    ShowHeaderWhenEmpty="true" OnRowCommand="gvDisplay_OnRowCommand" ShowHeader="false"
                                                     EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                                                        <asp:TemplateField HeaderText="InventoryCode" ItemStyle-Width="75">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btnItemhis" ToolTip="Click to View Sales History" CommandName="sbtnItemhis" CommandArgument='<%# Eval("InventoryCode").ToString() %>'
                                                                    Text='<%# Eval("InventoryCode") %>'
                                                                    runat="server" Font-Bold="true" ForeColor="SeaGreen" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                                        <asp:BoundField DataField="MRP" HeaderText="MRP" DataFormatString="{0:n}"></asp:BoundField>
                                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" DataFormatString="{0:n}"></asp:BoundField>
                                                        <asp:BoundField DataField="UnitCost" HeaderText="Cost" DataFormatString="{0:n}" />
                                                        <asp:BoundField DataField="minimumQtylevel" HeaderText="Min Qty" />
                                                        <asp:BoundField DataField="QtySold" HeaderText="Sold Qty" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="QtyOnHand" HeaderText="Qty On Hand" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="PERDAY" HeaderText="Average Per Day" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="SuggQty" HeaderText="Sugg Qty" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="Reorderlevel" HeaderText="Order Qty" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="ExcessDays" HeaderText="ExcessDays" DataFormatString="{0:n2}" />
                                                        <asp:BoundField DataField="Noitems" HeaderText="No.Of Items Received" DataFormatString="{0:n2}" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- <ups:ItemHistory runat="server" ID="itemHistory" />--%>
    <%-- <div style="float: left; display: table">
        <div class="barcode-header" style="width: 100px">
            Item History
        </div>
        <div class="control-group-split">
            <div class="control-group-left" style="width: 100%">
                <div class="label-left" style="width: 20%">
                    <asp:LinkButton ID="btnBack" runat="server" class="button-blue" OnClick="lnkBack_Click">Back</asp:LinkButton>
                </div>
                <div class="label-right" style="width: 60%">
                    <div class="control-button">
                        <asp:Label ID="txtVItemCode" runat="server" Font-Bold="True"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="gridDetails grid-overflow" id="ItemHistory" runat="server" style="height: 160px">
            <asp:GridView ID="gvItemHistory" runat="server" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                OnSelectedIndexChanged="gvPO_SelectedIndexChanged" DataKeyNames="MNName" AutoGenerateSelectButton="True">
                <EmptyDataTemplate>
                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                </EmptyDataTemplate>
                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                <RowStyle CssClass="pshro_GridDgnStyle" />
                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                <PagerStyle CssClass="pshro_text" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <HeaderTemplate>
                            <asp:Label ID="Label18" Visible="false" runat="server" Text="Select"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="MNName" HeaderText="Month"></asp:BoundField>
                    <asp:BoundField DataField="WK1" HeaderText="1st Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK2" HeaderText="2nd Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK3" HeaderText="3rd Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK4" HeaderText="4th Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK5" HeaderText="5th Week" DataFormatString="{0:n}" />
                    <asp:BoundField DataField="WK6" HeaderText="6th Week" DataFormatString="{0:n}" />
                    <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:n}" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="gridDetails grid-overflow" id="ItemHistoryLoc" runat="server" style="height: 85px">
            <asp:GridView ID="gvItemHistoryLoc" runat="server" Width="100%" AutoGenerateColumns="False"
                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                <EmptyDataTemplate>
                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                </EmptyDataTemplate>
                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                <RowStyle CssClass="pshro_GridDgnStyle" />
                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                <PagerStyle CssClass="pshro_text" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <HeaderTemplate>
                            <asp:Label ID="Label18" Visible="false" runat="server" Text="Select"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Location" HeaderText="Location"></asp:BoundField>
                    <asp:BoundField DataField="MNName" HeaderText="Month"></asp:BoundField>
                    <asp:BoundField DataField="WK1" HeaderText="1st Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK2" HeaderText="2nd Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK3" HeaderText="3rd Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK4" HeaderText="4th Week" DataFormatString="{0:n}"></asp:BoundField>
                    <asp:BoundField DataField="WK5" HeaderText="5th Week" DataFormatString="{0:n}" />
                    <asp:BoundField DataField="WK6" HeaderText="6th Week" DataFormatString="{0:n}" />
                    <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:n}" />
                </Columns>
            </asp:GridView>
        </div>
    </div>--%>
    <div style="float: right; display: table">
        <div class="control-button">
            <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick="fncValidateSave();return false;"
                Text='<%$ Resources:LabelCaption,btnSave %>' Width="100px"></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                Text='<%$ Resources:LabelCaption,btn_delete %>' Width="100px"></asp:LinkButton>
        </div>
        <div class="control-button">
            <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
        </div>
    </div>
    <div id="dialog-ViewItemHistory" style="display: none" title="Item History">
        <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
            <ContentTemplate>
                <ups:ItemHistory runat="server" ID="itemHistory" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="modal-loader">
                <div class="center-loader">
                    <img alt="" src="../images/loading_spinner.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdate">
            <p>
                <%=Resources.LabelCaption.Alert_Select_Any%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'>
                </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="DeleteConfirmmessage">
            <p>
                <%=Resources.LabelCaption.Alert_DeleteSure%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkDelbtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="ConfirmSaveDialog">
            <p>
                <%=Resources.LabelCaption.Alert_Confirm_Save%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="StockUpdateSave">
            <p>
                <%=Resources.LabelCaption.Alert_Save%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="container-group-full" style="display: none">
        <div id="AlertNoItems">
            <p>
                <%=Resources.LabelCaption.Alert_No_Items%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk%>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
