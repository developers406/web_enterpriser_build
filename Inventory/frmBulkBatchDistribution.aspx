﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBulkBatchDistribution.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmBulkBatchDistribution" %>

<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            display: none;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 390px;
            max-width: 390px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            display: none;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display:none;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            display: none;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            display: none;
        }

        .Description_parent {
            max-width: 300px !important;
            min-width: 300px !important;
        }

        .parent_align {
            max-width: 130px !important;
            min-width: 130px !important;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 305px;
            max-width: 305px;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(7), .grdLoad1 th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(8), .grdLoad1 th:nth-child(8) {
            min-width: 116px;
            max-width: 116px;
        }

        .grdLoad1 td:nth-child(9), .grdLoad1 th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad1 td:nth-child(10), .grdLoad1 th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }


        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .selected_row {
            background-color: #A1DCF2;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }

        .Parent {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        .Child {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        #tags {
            bottom: 40px;
            position: fixed;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            display: none;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }

        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>

    <script type="text/javascript">



</script>
    <script type="text/javascript">

        function fncSetValue() {
            try {
                if (SearchTableName == "InventoryBatch") {
                    $('#<%=txtChildCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtCdescription.ClientID %>').val($.trim(Description));
                    fncGetInventoryCode($.trim(Code));
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function pageLoad() {

            try {
                pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                SetAutoComplete();
                $("#<%= txtChildCode.ClientID %>").focus();
                <%--$("#<%= txtMrp.ClientID %>").attr('readonly', true);--%>
                if ($("#<%= txtMrp.ClientID %>").val() != "Retain") {
                    $("#<%= txtExpiredDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                    return false;
                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        $(document).ready(function () {
            $("#<%= txtCost.ClientID %>").on("keypress", function (e) {
                if (e.keyCode == 13) {
                    if (parseFloat($("#<%= txtCost.ClientID %>").val()) > parseFloat($("#<%= txtMrp.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter Cost is Lessar than Mrp");
                        return false;
                    }
                    else {
                        $("#<%= txtSellPrice.ClientID %>").focus().select();
                        return false;
                    }

                }
            });
            $("#<%= txtSellPrice.ClientID %>").on("keypress", function (e) {
                if (e.keyCode == 13) {
                    if (parseFloat($("#<%= txtSellPrice.ClientID %>").val()) > parseFloat($("#<%= txtMrp.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter SellingPrice is Lessar than Mrp");
                        return false;
                    }
                    else if (parseFloat($("#<%= txtSellPrice.ClientID %>").val()) < parseFloat($("#<%= txtCost.ClientID %>").val())) {
                        ShowPopupMessageBox("Please Enter SellingPrice is Greater than or Equal to Cost ");
                        return false;
                    }
                    else {
                        $("#<%= txtBatchNo.ClientID %>").focus().select();
                        return false;
                    }
                }
            });
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible")) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        e.preventDefault();
                    }
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');

            return false;
        }

        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {

                fncToastInformation('<%=Resources.LabelCaption.Save_Barcodeprinting%>');

            }
            catch (err) {
                fncToastError(err.message);
                //console.log(err);
            }
        }


        ///Template
        function fncTemplateChange() {
            try {

                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Open Barcode Popup
        function ShowPopupBarcode() {

            dateFormat[0].selectedIndex = 1;
            dateFormat.trigger("liszt:updated");

            $("#divBarcode").dialog({
                appendTo: 'form:first',
                title: "Barcode Print",
                width: 500,
                modal: true

            });
            setTimeout(function () {
                expiredDate.select().focus();
            }, 50);
        }


        var pkdDate;
        var expiredDate;
        var dateFormat;
        var hideMRP;
        var hidePrice;
        var hidepkdDate;
        var hideexpDate;
        var hidecompany;
        var template;
        var size;
        var print;
        //Get Barcode User Comtrol
        $(function () {

            try {

                pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
                expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
                dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
                hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
                hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
                hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
                hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
                hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
                template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
                size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
                print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");


            }
            catch (err) {
                fncToastError(err.message);
            }

        });

        ///Change days and months
        function fncDateMonth() {
            try {

                if ($(DateRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Days)');
                }
                else if ($(MonthRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Months)');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Template
        function fncTemplateChange() {
            try {


                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        $(function () {
            $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if ($("#<%= txtBillDate.ClientID %>").val() === '') {
                $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });
        //function isNumberKey(evt) {

        //    var charCode = (evt.which) ? evt.which : evt.keyCode;
        //    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        //        return false;

        //    return true;
        //}

      <%--  $(function () {
            $('#<%=txtPCSQty.ClientID %>').focusout(function () {
                fncCalcTotalweight();
            });
        });--%>
        $(function () {
            $("[id*=grdDistribution]").on('click', 'td > img', function (e) {
                var r = ShowPopupMessageBox("Do you want to Delete?");
                if (r === false) {
                    return false;
                }

                var objIssuedQty = null;
                var objBalanceQty = null;

                $("[id*=grdBulkItem] tr").each(function () {

                    objIssuedQty = $(this).find(".LIssuedQty");
                    objBalanceQty = $(this).find(".LBalanceQty");
                    objIssuedQty.text("0.00");
                });

                $(this).closest("tr").remove();
                $('#<%=txtCdescription.ClientID %>').focus().select();
                return false;
            });

        });


        ///Barcode Validation
        function fncBarcodeValidation() {

            try {
                var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                //alert(totalRows);
                if (totalRows == 0) {
                    ShowPopupMessageBox("Add item to Print !");
                }
                else {
                    ShowPopupBarcode();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }




        ///Barcode Validation
        function fncBarcodeQtyValidation() {
            try {
                if (size.val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    return false;
                }
                else {
                    //fncGetGridValuesForSave();
                    $("#<%=lnkbtnPrint.ClientID %>").click();
                    return false;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function UpdateValidation() {
            //alert('test');
            //Show = Show + 'Already Exists';

            //if (Show != '') {
            //    ShowPopupMessageBox(Show);
            //    return false;
            //}
            try {

                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "YES": function () {
                            $(this).dialog("close");
                            $("#<%=lnkUpdate.ClientID %>").click();
                            return false;
                        },
                        "NO": function () {
                            $(this).dialog("close");
                            return false();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }
        function AddbtnValidation() {
            try {
                if ($('#<%=txtChildCode.ClientID %>').val() == '') {
                    ShowPopupMessageBox('Item Code should not be Empty !');
                    $('#<%=txtChildCode.ClientID %>').focus().select();
                    return false;
                }
                else if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {
                    ShowPopupMessageBox('Invalid Weight Qty !');
                    $('#<%=txtPCSQty.ClientID %>').focus().select();
                    return false;
                }
                else if ($('#<%=txtWeight.ClientID %>').val() <= 0) {
                    ShowPopupMessageBox('Invalid Weight Qty !');
                    $('#<%=txtWeight.ClientID %>').focus().select();
                    return false;
                }
                else if ($('#<%=txtMrp.ClientID %>').val() <= 0) {
                    ShowPopupMessageBox("Invalid MRP!");
                    return false;
                }
                else if ($('#<%=txtCost.ClientID %>').val() <= 0) {
                    ShowPopupMessageBox("Invalid Cost!");
                    return false;
                }
                else if ($('#<%=txtSellPrice.ClientID %>').val() <= 0) {
                    ShowPopupMessageBox("Invalid SellingPrice!");
                    return false;
                }
                else if (parseFloat($("#<%= txtMrp.ClientID %>").val()) < parseFloat($("#<%= txtCost.ClientID %>").val())) {
                    ShowPopupMessageBox("Please Enter Cost should be Lessar than Mrp");
                    return false;
                }
                else if (parseFloat($("#<%= txtMrp.ClientID %>").val()) < parseFloat($("#<%= txtSellPrice.ClientID %>").val())) {
                    ShowPopupMessageBox("Please Enter SellingPrice should be Lessar than Mrp");
                    return false;
                }
                else if (parseFloat($("#<%= txtCost.ClientID %>").val()) > parseFloat($("#<%= txtSellPrice.ClientID %>").val())) {
                    ShowPopupMessageBox("Please Enter SellingPrice should be Greater than or Equal to Cost ");
                    return false;
                }
                else if (parseFloat($("#<%= txtTotalWeight.ClientID %>").val()) < parseFloat($("#<%= txtPCSQty.ClientID %>").val())) {
                    ShowPopupMessageBox("Total Qty is Less than Stock. Stock is " + $("#<%= txtTotalWeight.ClientID %>").val());
                    return false;
                }
                else if ($("#HiddenAllowNegDist").val() == 'Y') {
                    var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                    var confirm_value = "";
                    $("#<%=grdBulkItem.ClientID%> tr").each(function () {

                        if (!this.rowIndex) return;
                        var gitem = $(this).find("td.Itemcode").html();
                        if ($.trim(gitem) == $.trim($('#<%=txtChildCode.ClientID %>').val())) {
                            confirm_value = document.createElement("INPUT");
                            confirm_value.type = "hidden";
                            confirm_value.name = "confirm_value";
                            if (confirm("Item Already exists!. Do you want to update Packed Qty?")) {
                                confirm_value.value = "Yes";

                            } else {
                                confirm_value.value = "No";
                            }
                            document.forms[0].appendChild(confirm_value);
                        }
                    });
                }
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }



        function Toast() {
            try {
                ShowPopupMessageBox("Saved Succesfully");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Get Calc Child Qty
        function fnctxtChildQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {
                        ShowPopupMessageBox('Invalid Weight Qty !');
                        $('#<%=txtPCSQty.ClientID %>').focus().select();
                        return false;
                    }
                    fncCalcTotalweight();
                    $('#<%=txtMrp.ClientID %>').focus().select();
                    return false;
                }

            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }

        function fncCalcTotalweight() {
            try {
                var ChildQty = $('#<%=txtPCSQty.ClientID%>').val();
                var ChildWeight = $('#<%=txtWeight.ClientID%>').val();
                var TotalQty;
                TotalQty = ChildWeight * ChildQty;
                $('#<%=txtTotalWeight.ClientID%>').val(TotalQty.toFixed(3));

            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }

        $(function () {
            $("[id*=grdDistribution] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdDistribution] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });


        function SetAutoComplete() {

            $("[id$=txtChildCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmBulkBatchDistribution.aspx/GetBulkDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",

                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valInvDet: item.split('|')[0],
                                    valCost: item.split('|')[1]
                                }
                            }))
                        },

                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                select: function (e, i) {
                    $('#<%=txtChildCode.ClientID %>').val($.trim(i.item.valInvDet.split('--')[0]));
                    $('#<%=txtCdescription.ClientID %>').val($.trim(i.item.valInvDet.split('--')[1]));
                    fncGetInventoryCode($.trim($.trim(i.item.valInvDet.split('--')[0])));
                    return false;
                },
                minLength: 1
            });

            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtChildCode'
                });
            });
        }




        function ClearTextBox() {
            try {
                $('#<%=txtChildCode.ClientID %>').val('');
                $('#<%=txtCdescription.ClientID %>').val('');
                $('#<%=txtCost.ClientID %>').val(0);
                $('#<%=txtSellPrice.ClientID %>').val(0);
                $('#<%=txtWeight.ClientID %>').val(0);
                $('#<%=txtPCSQty.ClientID %>').val(0);
                $('#<%=txtTotalWeight.ClientID %>').val(0);
                $('#<%=txtTotalWeight.ClientID %>').val(0);
                $('#<%=txtMrp.ClientID %>').val(0);
                $('#<%=txtBatchNo.ClientID %>').val('');
                if ($('#<%=hidRetain.ClientID %>').val() != "Retain")
                    $('#<%=txtExpiredDate.ClientID %>').datepicker("setDate", "0");
                else
                    $('#<%=txtExpiredDate.ClientID %>').val('');
                $("#hidenChildCode").val('');
                $("#HiddenMrp").val(0);
                $('#<%=txtChildCode.ClientID %>').removeAttr('disabled');
                $('#<%=txtChildCode.ClientID %>').focus().select();
                return false;
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }

        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {
                $(function () {
                    $("#barcodeprinting").html('<%=Resources.LabelCaption.Save_Barcodeprinting%>');
                    $("#barcodeprinting").dialog({
                        title: "Enterpriser Web",
                        buttons: {
                            Ok: function () {
                                $(this).dialog("destroy");
                                fncClear();
                            }
                        },
                        modal: true
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitializeBatchDetail() {
            try {
                $("#stkadj_batch").dialog({
                    titt: "Bulk Batch Details",
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetBatchno() {

            try {
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmBulkBatchDistribution.aspx/fncGetRetain_Batches")%>',
                    data: "{ 'Code': '" + $('#<%=txtChildCode.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try {
                var inputNumber = $('#<%=hidInvLength.ClientID %>').val();
                var regexp = /^[0-9]*$/;
                var numberCheck = regexp.exec(inputNumber);
                var objBulkBatch, row;
                objBulkBatch = jQuery.parseJSON(msg.d);
                $('#<%=txtChildCode.ClientID %>').attr('disabled', 'disabled');
                if (objBulkBatch.length > 0) {

                    if (objBulkBatch.length == 1 && numberCheck != null && ($('#<%=hidInvLength.ClientID %>').val().length == 15
                        || $('#<%=hidInvLength.ClientID %>').val().length == 12)) {
                        if (objBulkBatch[0]["BatchNo"] != "") {
                            $('#<%=txtChildCode.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                            $('#<%=txtBatchNo.ClientID %>').val($.trim(objBulkBatch[0]["BatchNo"]));
                            $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                            $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                            $('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));
                            $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));
                            $('#<%=txtPCSQty.ClientID %>').focus();
                        }
                        else if (objBulkBatch[0]["BatchNo"] == "") {
                            $('#<%=txtChildCode.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                            $('#<%=txtBatchNo.ClientID %>').val('');
                            $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                            $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                            $('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));
                            $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));

                            $('#<%=txtPCSQty.ClientID %>').focus();
                        }
                    }
                    else if (objBulkBatch[0]["BatchNo"] == "") {
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                        $('#<%=txtBatchNo.ClientID %>').val('');
                        $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                        $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                        $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                        $('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));
                        if (objBulkBatch[0]["Lqty"] != undefined)
                            $('#<%=txtTotalWeight.ClientID%>').val(parseFloat(parseFloat(objBulkBatch[0]["Lqty"].trim()) / 1000).toFixed(3));

                        $('#<%=txtPCSQty.ClientID %>').focus();
                    }
                    else {
                        tblBatchBody = $("#tblBatch tbody");
                        tblBatchBody.children().remove();
                        for (var i = 0; i < objBulkBatch.length; i++) {
                            row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                $.trim(objBulkBatch[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                $.trim(objBulkBatch[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" +
                                objBulkBatch[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                                objBulkBatch[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                                objBulkBatch[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                objBulkBatch[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                $.trim(objBulkBatch[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                                $.trim(objBulkBatch[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["AllowExpireDate"]) + "</td></tr>";

                            tblBatchBody.append(row);

                        }

                        fncInitializeBatchDetail();
                        tblBatchBody.children().dblclick(fncBatchRowClick);

                        tblBatchBody[0].setAttribute("style", "cursor:pointer");

                        tblBatchBody.children().on('mouseover', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "Yellow");
                        });

                        tblBatchBody.children().on('mouseout', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "white");
                        });

                        tblBatchBody.children().on('keydown', function () {
                            fncBatchKeyDown($(this), event);
                        });

                        $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                        $("#tblBatch tbody > tr").first().focus();
                    }
                }

                fncBatchDeleteCheck($.trim(objBulkBatch[0]["InventoryCode"]));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncBatchDeleteCheck(ItemCode) {
            try {
                var obj = {}
                obj.Inventorycode = ItemCode;
                $.ajax({
                    type: "POST",
                    url: "../Stock/frmStockAdjustmentEntry.aspx/fncGetInventoryDetail_BatchDelete",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBatchDeleteInventories(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchDeleteInventories(source) {
            try {
                MRPDEL = "";
                var obj = {};
                obj = jQuery.parseJSON(source.d);
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                        MRPDEL += obj[i]["MRP"] + ',';
                    }
                }
                //console.log(MRPDEL);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {
            try {
                setTimeout(function () {
                    $('#<%=hidExp.ClientID %>').val('');
                    $('#<%=txtChildCode.ClientID %>').val('');
                    $('#<%=hidExp.ClientID %>').val('');
                    $('#<%=txtBatchNo.ClientID %>').val($.trim($(rowObj).find('td[id*=tdBatchNo]').text()));
                    $('#<%=txtCost.ClientID %>').val($.trim($(rowObj).find('td[id*=tdCost]').text()));
                    $('#<%=txtTotalWeight.ClientID %>').val($.trim($(rowObj).find('td[id*=tdStock]').text()));
                    $('#<%=txtChildCode.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));
                    $('#<%=txtCdescription.ClientID %>').val($.trim($(rowObj).find('td[id*=tdDesc]').text()));
                    $('#<%=txtExpiredDate.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));
                    $('#<%=txtMrp.ClientID %>').val($.trim($(rowObj).find('td[id*=tdMRPNew]').text()));
                    $('#<%=txtSellPrice.ClientID %>').val($.trim($(rowObj).find('td[id*=tdSellingPrice]').text()));
                    $('#<%=txtWeight.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice1]').text()));
                    $("#stkadj_batch").dialog('close');
                    $('#<%=txtPCSQty.ClientID %>').focus().select();
                    $('#<%=hidExp.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));
                }, 50);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

    </script>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'DistributionBatch');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "DistributionBatch";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemCodeAdd, txtWQty) {
            try {
                var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (charCode == 112) {
                    fncShowSearchDialogCommon(event, Inventory, txtItemCodeAdd, txtWQty);
                    return false;
                }
                else if (charCode == 13) {
                    var ItemCode = $("#<%=txtChildCode.ClientID%>").val();
                    fncGetInventoryCode(ItemCode);
                    //fncBarcodeDetail(ItemCode);
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCode(ItemCode) {
            try {
                $('#<%=hidInvLength.ClientID %>').val(ItemCode);
                var obj = {};
                obj.code = ItemCode;
                obj.formName = 'BulkBatch';
                if (obj.code == "")
                    return;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "[]") {
                            ShowPopupMessageBox("Invalid ItemCode. Please Enter Valid Parent Bulk Item !.");
                            return false;
                        }
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fnBulkGridUpdate(ParentCode, ParentBatchNo, TotalItemWeight) {
            try {
                $("#<%=grdDistribution.ClientID %> tbody tr").each(function () {
                        if ($.trim(ParentCode) == $.trim($(this).find("td").eq(12).html()) && $.trim(ParentBatchNo) == $.trim($(this).find("td").eq(13).html()))
                            TotalItemWeight = parseFloat(TotalItemWeight) + parseFloat($(this).find("td").eq(9).html());
                    });


                $("#<%=grdBulkItem.ClientID %> tbody tr").each(function () {
                    if ($.trim(ParentCode) == $.trim($(this).find("td").eq(1).html()) && $.trim(ParentBatchNo) == $.trim($(this).find("td").eq(3).html())) {
                        if (parseFloat($(this).find('td input[id*="txtTakenQty"]').val()) < parseFloat(TotalItemWeight)) {
                            validation = false;
                        }
                        else {
                            $(this).find("td").eq(8).html(TotalItemWeight);
                            var Balqty = parseFloat($(this).find('td input[id*="txtTakenQty"]').val()) - parseFloat(TotalItemWeight);
                            $(this).find("td").eq(9).html(parseFloat(Balqty).toFixed(2));
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtPcs_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var rowobj = $(lnk).parent().parent();
                var sQty = row.cells[8].getElementsByTagName("input")[0].value;
                var sWight = row.cells[7].innerHTML;
                var ParentCode = row.cells[12].innerHTML;
                var ParentBatchNo = row.cells[13].innerHTML;
                if (sQty != "") {
                    var TotalWeight = 0, TotalItemWeight = 0;
                    TotalWeight = parseFloat(sQty * sWight).toFixed(2);
                    row.cells[9].innerHTML = TotalWeight; 
                    fnBulkGridUpdate(ParentCode, ParentBatchNo, TotalItemWeight);
                }
                if (validation == false) {
                    row.cells[9].innerHTML = 0;
                    row.cells[8].getElementsByTagName("input")[0].value = 0;
                    ShowPopupMessageBox("Assign Qty Greate than Taken Qty.");
                    TotalItemWeight = 0;
                    fnBulkGridUpdate(ParentCode, ParentBatchNo, TotalItemWeight); 
                    validation = true;
                    return false;
                }
                validation = true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        var validation = true;
        function txtPcs_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); //GET CURRENT ROW FOR UP AND DOWN KEY PRESS
            var row = lnk.parentNode.parentNode; //GET CURRENT ROW FOR LEFT AND RIGHT KEY PRESS
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;

            //if (charCode == 39) {
            //    row.cells[6].getElementsByTagName("input")[0].select();
            //    event.preventDefault();

            //}
            //if (charCode == 37) {
            //    var NextRowobj = rowobj;
            //    NextRowobj.find('td input[id*="txtBasicCost"]').select();
            //    event.preventDefault();
            //    return;

            //}
            if (charCode == 40 || charCode == 13) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="QtxQty"]').select();
                    return false;
                }
                else {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="QtxQty"]').select();
                    }
                    else
                        rowobj.siblings().first().find('td input[id*="QtxQty"]').select();
                    return false;
                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="QtxQty"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="QtxQty"]').select();
                }
            }
            return true;


        }
        function txtbarcode_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); //GET CURRENT ROW FOR UP AND DOWN KEY PRESS
            var row = lnk.parentNode.parentNode; //GET CURRENT ROW FOR LEFT AND RIGHT KEY PRESS
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
             
            if (charCode == 40 || charCode == 13) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtbarcode"]').select();
                    return false;
                }
                else {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtbarcode"]').select();
                    }
                    else
                        rowobj.siblings().first().find('td input[id*="txtbarcode"]').select();
                    return false;
                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtbarcode"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtbarcode"]').select();
                }
            }
            return true;


        }
        $(function () {
            $("#dialog-clear-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: "auto",
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close"); 
                        savestatus = true;
                        if (fncSave() == true) {
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        }
                    },
                    "No": function () {
                        $(this).dialog("close");
                        savestatus = false;
                    }
                }
            });
        });
        var savestatus = false;
        function fncSave() {
            if (savestatus == false) {
                $("#<%=grdBulkItem.ClientID %> tbody tr").each(function () {
                    if (parseFloat($.trim($(this).find("td").eq(8).html())) > 0) {
                        savestatus = true;
                        return false;
                    }
                });
                if (savestatus == true) {
                    $("#dialog-clear-confirm").dialog("open");
                    return false;
                }
            }
            var xmlchild = '<NewDataSetChild>';
            var xmlBulk = '<NewDataSetBulk>';
            var SNoChild = 0;
            $("#<%=grdDistribution.ClientID %> tbody tr").each(function () {
                if (parseFloat($.trim($(this).find("td").eq(9).html())) > 0) {
                    savestatus = true;
                    SNoChild = parseFloat(SNoChild) + 1;
                    xmlchild += "<TableChild>";
                    xmlchild += "<RowNo>" + SNoChild + "</RowNo>";
                    xmlchild += "<InventoryCode>" + $.trim($(this).find("td").eq(1).html()) + "</InventoryCode>";
                    xmlchild += "<Description>" + $.trim($(this).find("td").eq(2).html()) + "</Description>";
                    xmlchild += "<ItemType>" + 'CHILD' + "</ItemType>";
                    xmlchild += "<Weight>" + $.trim($(this).find("td").eq(7).html()) + "</Weight>";
                    xmlchild += "<Wqty>" + $(this).find('td input[id*="QtxQty"]').val() + "</Wqty>";
                    xmlchild += "<IssuedQty>" + '0' + "</IssuedQty>";
                    xmlchild += "<ReceivedQty>" + $.trim($(this).find("td").eq(9).html()) + "</ReceivedQty>";
                    xmlchild += "<TakenQty>" + '0' + "</TakenQty>";
                    xmlchild += "<AverageCost>" + $.trim($(this).find("td").eq(5).html()) + "</AverageCost>";
                    xmlchild += "<SellingPrice>" + $.trim($(this).find("td").eq(6).html()) + "</SellingPrice>";
                    xmlchild += "<BatchNo>" + $(this).find("td").eq(3).html() + "</BatchNo>";
                    xmlchild += "</TableChild>";
                }
                else {
                    savestatus = false;
                }
            });
            SNoChild = 0;
            $("#<%=grdBulkItem.ClientID %> tbody tr").each(function () {
                if (parseFloat($(this).find('td input[id*="txtTakenQty"]').val()) > 0 && parseFloat($.trim($(this).find("td").eq(8).html())) > 0) {
                    savestatus = true;
                    SNoChild = parseFloat(SNoChild) + 1;
                    xmlBulk += "<Tablebulk>";
                    xmlBulk += "<RowNo>" + SNoChild + "</RowNo>";
                    xmlBulk += "<InventoryCode>" + $.trim($(this).find("td").eq(1).html()) + "</InventoryCode>";
                    xmlBulk += "<Description>" + $.trim($(this).find("td").eq(2).html()) + "</Description>";
                    xmlBulk += "<ItemType>" + 'BULK' + "</ItemType>";
                    xmlBulk += "<weight>" + '1' + "</weight>";
                    xmlBulk += "<Wqty>" + $.trim($(this).find("td").eq(9).html()) + "</Wqty>";
                    xmlBulk += "<IssuedQty>" + $.trim($(this).find("td").eq(8).html()) + "</IssuedQty>";
                    xmlBulk += "<ReceivedQty>" + '0' + "</ReceivedQty>";
                    xmlBulk += "<TakenQty>" + $(this).find('td input[id*="txtTakenQty"]').val() + "</TakenQty>";
                    xmlBulk += "<AverageCost>" + $.trim($(this).find("td").eq(4).html()) + "</AverageCost>";
                    xmlBulk += "<SellingPrice>" + $.trim($(this).find("td").eq(5).html()) + "</SellingPrice>";
                    xmlBulk += "<Location>" + 'HQ' + "</Location>";
                    xmlBulk += "<BatchNo>" + $(this).find("td").eq(3).html() + "</BatchNo>";
                    xmlBulk += "</Tablebulk>";
                }
                else {
                    savestatus = false;
                }
            });

            if (savestatus == false) {
                ShowPopupMessageBox('Invlaid Issued Qty !.');
                return false;
            }
            xmlchild = xmlchild + '</NewDataSetChild>'
            xmlchild = escape(xmlchild);
            $('#<%=hidChild.ClientID %>').val(xmlchild);
            xmlBulk = xmlBulk + '</NewDataSetBulk>'
            xmlBulk = escape(xmlBulk);
            $('#<%=hidBulk.ClientID %>').val(xmlBulk);
            return true;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updtPnlgrdgrdDepartmentList" runat="Server">
        <ContentTemplate>
            <div class="main-container" id="form1">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="../Inventory/frmDistributionList.aspx">Distribution List</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page" id="DtCreation" runat="server"></li>
                        <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
                    </ul>
                </div>
                <div class="container-group-pc">
                    <asp:HiddenField ID="hidDtReturn" runat="server" Value="" />
                    <div class="right-container-top-distribution">

                        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Are you sure to Update this Item?</p>
                        </div>
                        <div class="right-container-bottom-detail">
                            <div class="control-group-split">
                                <div class="control-group-left-pc" style="margin-left: 20px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_partyCode %>'></asp:Label>
                                        <asp:DropDownList ID="PartyCodeDropdown" runat="server" CssClass="form-control-res" Width="100px"
                                            AutoPostBack="true" OnSelectedIndexChanged="PartyCodeDropdown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-left-Distribut-txt" style="margin-left: -57px;">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_name %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc" style="width: 180px">
                                        <asp:TextBox ID="txtPartyName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: -138px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblbillno %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: -21px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Trays %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtTrays" runat="server" Text="0" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: 2px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bags %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtBags" Text="0" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-Distb" style="margin-left: 2px">
                                    <div class="label-left-pc">
                                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_Emptybags %>'></asp:Label>
                                    </div>
                                    <div class="label-right-pc">
                                        <asp:TextBox ID="txtEmptyBag" Text="0" runat="server" CssClass="form-control-res"
                                            onkeypress="return isNumberKey(event)"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-right-Distb" style="margin-right: 24px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblBilldate %>'></asp:Label>
                                        <asp:TextBox ID="txtBillDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left-pc" style="margin-left: 5px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefrenceNo %>'></asp:Label>
                                        <asp:DropDownList ID="RefNoDropDown" runat="server" CssClass="form-control-res" AutoPostBack="true" Width="100px"
                                            OnSelectedIndexChanged="RefNoDropDown_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group-left-pc" style="margin-left: -63px">
                                    <div class="label-right-pc">
                                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_LotNumber %>'></asp:Label>
                                        <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-container-top-detail">
                    <div class="col-md-12  Parent">
                        Parent Item
                    </div>
                    <div class="GridDetails">
                        <div class="row">
                            <div class="grdLoad1">
                                <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">Inventorycode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">BatchNo
                                            </th>
                                            <th scope="col">MRP
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">QtyOnHand
                                            </th>
                                            <th scope="col">TakenQty
                                            </th>
                                            <th scope="col">IssuedQty
                                            </th>
                                            <th scope="col">BalanceQty
                                            </th>
                                        </tr>
                                    </thead>
                                </table>

                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; background-color: aliceblue; height: 135px;">
                                    <asp:GridView ID="grdBulkItem" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="false"
                                        OnRowDeleting="grdBulkItem_RowDeleting" OnRowDataBound="grdBulkItem_RowDataBound"  OnRowCommand="grdBulkItem_RowCommand"
                                        ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left">
                                        <PagerStyle CssClass="pshro_text" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <Columns>
                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                            <asp:BoundField HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"
                                                ItemStyle-CssClass="PInventorycode"></asp:BoundField>
                                            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="BatchNo" DataField="BatchNo" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="ParentCode"></asp:BoundField>
                                            <asp:BoundField HeaderText="MRP" DataField="MRP" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="QtyOnHand" DataField="QtyOnHand" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Taken Qty" ItemStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTakenQty" runat="server" Text='<%# Eval("TakenQty") %>'
                                                        onkeypress="return isNumberKey(event)" CssClass="txtTakenQty grid-textbox1" Style="background-color: yellow"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="IssuedQty" DataField="IssuedQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LIssuedQty"></asp:BoundField>
                                            <asp:BoundField HeaderText="BalanceQty" DataField="BalanceQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LBalanceQty"></asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            PageButtonCount="5" Position="Bottom" />
                                        <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-container-top-header">
                    Child Item
                </div>
                <div class="right-container-top-detail">
                    <div class="GridDetails">
                        <div class="row">
                            <div class="grdLoad">
                                <div class="GridDetails" id="divscroll" style="overflow-x: hidden; overflow-y: scroll; height: 150px; width: 1333px; background-color: aliceblue;">
                                    <asp:GridView ID="grdDistribution" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="true"
                                        ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left">
                                        <PagerStyle CssClass="pshro_text" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <Columns>
                                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="false" ButtonType="Button" />
                                            <asp:BoundField ItemStyle-CssClass="Itemcode" HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                            <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Batch" DataField="BatchNo" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:BoundField HeaderText="Mrp" DataField="Mrp" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Cost" DataField="Cost" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:BoundField HeaderText="Weight" DataField="Weight" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                            <asp:TemplateField HeaderText="PCS" ItemStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="QtxQty" runat="server" Text='<%# Eval("PCS") %>'
                                                        Style="text-align: right" onkeydown="return txtPcs_Keystroke(event,this);"
                                                        onchange="txtPcs_TextChanged(this); return false;" CssClass="grid-textbox2" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Total Qty PKD" DataField="PackedQty" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                            <asp:BoundField HeaderText="Expired Date" DataField="ExpiredDate" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Print Qty" ItemStyle-Width="100" ItemStyle-CssClass="hiddencol1">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtbarcode" runat="server" Text='<%# Eval("Barcode") %>' onkeydown="return txtbarcode_Keystroke(event,this);"
                                                        onkeypress="return isNumberKey(event)" CssClass="grid-textbox2" Style="background-color: whitesmoke"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="ParentCode" DataField="Parentcode" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="ParentCode hiddencol1"></asp:BoundField>
                                            <asp:BoundField HeaderText="ParentBatchNo" DataField="ParentBatchNo" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="ParentCode hiddencol1"></asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="pshro_GridNewHeaderCellCenter" BackColor="#3e4095" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            PageButtonCount="5" Position="Bottom" />
                                        <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                    </asp:GridView>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="right-container-bottom-detail">
                    <div class="control-group-split">
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtChildCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogInventory(event,'InventoryBatch',  'txtChildCode', 'txtPCSQty');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Description" style="width: 30% !important">
                            <div class="label-left-pc">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <div class="ui-widget">
                                    <asp:TextBox ID="txtCdescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                          <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label18" runat="server" Text="Batch No"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtBatchNo" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_weight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPCSQty" runat="server" MaxLength="12" CssClass="form-control-res"
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label19" runat="server" Text="Mrp"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtMrp" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_sellingPrice %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtSellPrice" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb-Batch">
                            <div class="label-left-pc">
                                <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalWeight %>'></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtTotalWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                                <asp:TextBox ID="txtSort" runat="server" Text="1" CssClass="display_none"></asp:TextBox>
                            </div>
                        </div>
                      
                        <div class="control-group-left-Distb display_none">
                            <div class="label-left-pc">
                                <asp:Label ID="Label20" runat="server" Text="Expired Date"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtExpiredDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClick="lnkAdd_Click"
                            Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick="return AddbtnValidation();"> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                            Text='<%$ Resources:LabelCaption,btnClear %>'> </asp:LinkButton>
                    </div>
                </div>

                <div class="control-button">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClientClick="return fncSave();" OnClick="lnkSave_Click"
                                    Text='<%$ Resources:LabelCaption,btnSave %>'> </asp:LinkButton>
                            </div>
                            <div class="control-button  ">
                                <asp:LinkButton ID="lnkClearform" runat="server" CssClass="button-red"
                                    Text="Close" PostBackUrl="~/Inventory/frmDistributionlist.aspx"></asp:LinkButton>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="control-button">
                <asp:LinkButton ID="lnkBarcode" runat="server" CssClass="button-red" OnClientClick="fncBarcodeValidation();"
                    Text='<%$ Resources:LabelCaption,Home_barcode %>'></asp:LinkButton>
            </div>

            <asp:HiddenField ID="hidenChildCode" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="hidenParentcode" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenMrp" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="NO" />
            <asp:HiddenField ID="HiddenAllowNegDist" runat="server" ClientIDMode="Static" Value="N" />
            <asp:Button ID="lnkbtnPrint" runat="server" OnClick="lnkbtnPrint_Click" class="hiddencol" />

            <div class="hiddencol">
                <div id="barcodeprinting" class="barcodesavedialog">
                </div>

            </div>
            <div id="divHiddenButton" style="display: none">
                <asp:LinkButton ID="lnkUpdate" runat="server" CssClass="button-red" OnClick="lnkUpdate_Click"> </asp:LinkButton>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divBarcode" style="display: none;">
        <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl" OnPrintButtonClientClick="return fncBarcodeQtyValidation();" />
    </div>
    <div class="hiddencol">
        <div id="stkadj_batch">
            <div class="Payment_fixed_headers BatchDetail">
                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th id="thBatchNo" scope="col">BatchNo
                            </th>
                            <th scope="col">Stock
                            </th>
                            <th scope="col">Cost
                            </th>
                            <th scope="col">MRP
                            </th>
                            <th scope="col">SellingPrice
                            </th>
                            <th scope="col">InventoryCode
                            </th>
                            <th scope="col">Description
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidRetain" runat="server" />
    <asp:HiddenField ID="hidExp" runat="server" />
    <asp:HiddenField ID="hidInvLength" runat="server" Value="" />
    <asp:HiddenField ID="hidChild" runat="server" Value="" />
    <asp:HiddenField ID="hidBulk" runat="server" Value="" />
    <div class="hidden">
        <div id="dialog-clear-confirm" title="Enterpriser">
            <p>
                <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>Wastage is Generated in Distribution.Do you want to Save ?
            </p>
        </div>
    </div>
</asp:Content>
