﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmDistributionList.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmDistributionList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 100px;
            max-width: 100px;
        }
        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }
        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 175px;
            max-width: 175px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 147px;
            max-width: 147px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 110px;
            max-width: 110px;
        }

        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .selected_row {
            background-color: #A1DCF2;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }
                
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">

       
        function pageLoad() {
            try {
                if ($('#<%=hidChildExport.ClientID%>').val() != "Y") { 
                    $('.grdLoad td:nth-child(2)').css("display", "none");
                    $('.grdLoad th:nth-child(2)').css("display", "none");

                    $('.grdLoad td:nth-child(1)').css("min-width", "130px");
                    $('.grdLoad th:nth-child(1)').css("min-width", "130px");
                    $('.grdLoad td:nth-child(3)').css("min-width", "130px");
                    $('.grdLoad th:nth-child(3)').css("min-width", "130px");
                    $('.grdLoad td:nth-child(10)').css("min-width", "150px");
                    $('.grdLoad th:nth-child(10)').css("min-width", "150px");

                } 
                 if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
               // $('#<%=lnkAddDistb.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkAddDistb.ClientID %>').css("display", "none");
             }
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "-1");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

            }
            catch (err) {
                alert(err.Message);
            }
        }

        function DateVAlidation() {
            try {
                return true;
                var FromDate = $("#<%= txtFromDate.ClientID %>").val();
                var ToDate = $("#<%= txtToDate.ClientID %>").val();

                if (FromDate > ToDate) {
                    ShowPopupMessageBox("Please Enter From Date is Lessthan or Equal to ToDate");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    
   <script type="text/javascript">
       var d1;
       var d2;
       var Opendate;
       var dur;
       function fncGetUrl() {
           fncSaveHelpVideoDetail('', '', 'DistributionList');
       }
       function fncOpenvideo() {

           document.getElementById("ifHelpVideo").src = HelpVideoUrl;

           var Mode = "DistributionList";
           var d = new Date($.now());
           Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
           d1 = new Date($.now()).getTime();



           $("#dialog-Open").dialog({
               autoOpen: true,
               resizable: false,
               height: "auto",
               width: 1093,
               modal: true,
               dialogClass: "no-close",
               buttons: {
                   Close: function () {
                       $(this).dialog("destroy");
                       var d = new Date($.now());
                       var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                       d2 = new Date($.now()).getTime();
                       var Diff = Math.floor((d2 - d1) / 1000);
                       //alert(Diff);
                       if (Diff >= 60) {
                           fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                       }

                   }
               }
           });
       }


   </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li class="active-page">Distribution List</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="top-purchase-container" style="padding: 10px;">
                <div class="top-purchase-left-container" style="width: 50%">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right" style="display:none">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_partycode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="PartyDropdown" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-right">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="top-purchase-right-container">
                    <div class="control-button" style="float: right; width: 54%">

                        <div class="col-md-1">
                            <asp:CheckBox ID="chReturn" runat="server" Text="Return" />
                        </div>


                        <%--<asp:RadioButtonList ID="rblRecent" runat="server">
                        <asp:ListItem Text="Recent" Value="Recent" />--%>
                        <%--<asp:ListItem Text="US" Value="us" />--%>
                        <%--  </asp:RadioButtonList>--%>
                        <%--<asp:RadioButton GroupName="MeasurementSystem" runat="server" Text="Recent" />
                            </div>--%>
                        <asp:LinkButton ID="lnkLoad" runat="server" class="button-blue" OnClick="lnkLoad_Click" Style="margin-left: 85px;" OnClientClick ="return DateVAlidation();"
                            Text='<%$ Resources:LabelCaption,btnLoadDistribution %>'></asp:LinkButton>
                        <asp:LinkButton ID="lnkRetDistb" runat="server" class="button-red" Style="margin-left: 5px;" OnClientClick ="return DateVAlidation();"
                            Text="Return Distribution" OnClick="lnkRetDistb_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkAddDistb" runat="server" class="button-blue" Style="margin-left: 5px;" OnClick="lnkAddDistb_Click"
                            Text='<%$ Resources:LabelCaption,btnAddDistribution %>' OnClientClick ="return DateVAlidation();"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>

        <div class="transfer-order-detail">
            <div class="GridDetails" style="margin-left: 4px">
                <div class="row">
                    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                        <%--    <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>

                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Print
                                                    </th>
                                                    <th scope="col">View
                                                    </th>
                                                    <th scope="col">BillNo
                                                    </th>
                                                    <th scope="col">Billdate
                                                    </th>
                                                    <th scope="col">DistributionNo
                                                    </th>
                                                    <th scope="col">Issued Qty
                                                    </th>
                                                    <th scope="col">Taken Qty
                                                    </th>
                                                    <th scope="col">Terminalcode
                                                    </th>
                                                    <th scope="col">Createuser
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 250px; background-color: aliceblue;">
                                            <asp:GridView ID="grdDistributionlist" runat="server" AutoGenerateColumns="true" ShowHeader="false"
                                                PageSize="14" ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgn" OnRowDataBound="grdDistributionlist_RowDataBound"
                                                OnRowDeleting="grdDistributionlist_RowDeleting" OnRowCommand="grdDistributionlist_RowCommand">
                                                <PagerStyle CssClass="pshro_text" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <Columns>
                                                    <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                                    <asp:CommandField HeaderText="Print" ShowSelectButton="True" ButtonType="Button" SelectText ="Print" />
                                                    <asp:CommandField HeaderText="View" ShowEditButton="True" ButtonType="Button" EditText ="View" />
                                                    <%--<asp:BoundField ItemStyle-CssClass="BillNo" HeaderText="BillNo" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Billdate" DataField="Billdate" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField HeaderText="DistributionNo" DataField="DistributionNo" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Issuedqty" DataField="Issuedqty" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Receivedqty" DataField="Receivedqty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Terminalcode" DataField="Terminalcode" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                    <asp:BoundField HeaderText="Createuser" DataField="Createuser" ItemStyle-HorizontalAlign="Right"></asp:BoundField>--%>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    PageButtonCount="5" Position="Bottom" />
                                                <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--   </ContentTemplate>

                            </asp:UpdatePanel>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidChildExport" runat="server" Value =""/>

</asp:Content>
