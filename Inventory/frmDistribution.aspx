﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"
    CodeBehind="frmDistribution.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmDistribution" %>

<%@ Register TagPrefix="ups" TagName="BarcodePrintUserControl" Src="~/UserControls/BarcodePrintUserControl.ascx" %>
<%@ Register TagPrefix="ups" TagName="ImageBarcodePrintUserControl" Src="~/UserControls/ImageBarcodePrintUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../BarcodeToolController/Scripts/modernizr-2.6.2.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/dom-to-image.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/dom-to-image.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/fabric.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/html2canvas.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/html2canvas.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/jspdf.debug.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/qrcode.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/qrious.min.js"></script>
    <script type="text/javascript" src="../js/BarcodeTemplateJs/jquery.classyqr.js"></script>
    <script type="text/javascript" src="../js/BarcodeTemplateJs/jquery.classyqr.min.js"></script>
    <%-- <script type="text/javascript" src="../js/BarcodeTemplateJs/qrcode.min.js"></script>--%>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/require.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/jquery-rotate.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/modernizr.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/zip.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/zip-ext.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/deflate.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/notify.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/notify.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/Common_BarcodePrinter.js"></script>
    <link href="../BarcodeToolController/Content/css/mystyle.css" rel="stylesheet" />
    <link type="text/css" href="../css/grnnote.css" rel="stylesheet" />
    <style type="text/css">
        /* vietnamese */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K8-C8QSw.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K9-C8QSw.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3Kz-C8.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwaPGR_p.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnruxElg4M.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnrtREl.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }



        /* cyrillic-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SkvzAbt.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SAvzAbt.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SgvzAbt.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_ScvzAbt.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* hebrew */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SYvzAbt.woff2) format('woff2');
            unicode-range: U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SsvzAbt.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SovzAbt.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SQvzA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7mlx17r.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7ilx17r.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7alxw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Candal';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/candal/v10/XoHn2YH6T7-t_8c9BhQI.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Copse';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/copse/v10/11hPGpDKz1rGb3dkFEk.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Orbitron';
            font-style: normal;
            font-weight: 500;
            src: url(https://fonts.gstatic.com/s/orbitron/v17/yMJMMIlzdpvBhQQL_SC3X9yhF25-T1ny_CmBoWgz.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Crimson Text';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/crimsontext/v11/wlp2gwHKFkZgtmSR3NB0oRJfbwhT.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* cyrillic-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjbYJwQj.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6Vj_YJwQj.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjTYJwQj.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjXYJwQj.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjvYJw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        .container_divlist1 {
            width: 22.4cm;
            background-color: #ffffff;
        }

        .container_divlist2 {
            text-align: left;
            background-color: #ffffff;
        }

        .fake_img {
            margin: 5px -1px;
            border-radius: 7px;
            margin-left: 5px;
            padding: 15px 0px 0px 15px;
        }

        .body {
            background: #F5F5F5;
        }

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 315px;
            max-width: 315px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 110px;
            max-width: 110px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display: none;
        }

        .Description_parent {
            max-width: 300px !important;
            min-width: 300px !important;
        }

        .parent_align {
            max-width: 130px !important;
            min-width: 130px !important;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 405px;
            max-width: 405px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(7), .grdLoad1 th:nth-child(7) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad1 td:nth-child(8), .grdLoad1 th:nth-child(8) {
            min-width: 116px;
            max-width: 116px;
        }


        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .selected_row {
            background-color: #A1DCF2;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }

        .Parent {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        .Child {
            padding: 0 0 0 10px;
            background-color: #004bc6;
            color: #fff;
            font-weight: bold;
            margin-bottom: 4px;
            margin-top: -15px;
        }

        #tags {
            bottom: 40px;
            position: fixed;
        }

        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold;
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }


        .Payment_fixed_headers th, .Payment_fixed_headers td {
            text-align: center;
            padding: 2px 2px;
            word-wrap: break-word;
            top: -31px;
            /* left: 228px; */
            width: 100px;
            /* border-color: black; */
            font-weight: 700;
        }
    </style>

    <script type="text/javascript">


        //var Templetedropdownctrl;
        //var PackedDatectrl;
        //var SizeTextBoxctrl;
        //var BestBeforeTextBoxctrl;
        //var DateRadioButton;
        //var MonthRadioButton;
        //var DateLable;


        function pageLoad() {

            try {
                pkdDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                expiredDate.datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                SetAutoComplete();
                if (localStorage.getItem('Template') != null) {
                    $('#ContentPlaceHolder1_barcodePrintUserControl_ddlTemplate').val(localStorage.getItem('Template'));
                    $("#ContentPlaceHolder1_barcodePrintUserControl_ddlTemplate").trigger("liszt:updated");
                    fncTemplateChange();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }


        //$(function () {
        //    SetAutoComplete();
        //});



        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');

            return false;
        }


        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {

                fncToastInformation('<%=Resources.LabelCaption.Save_Barcodeprinting%>');

            }
            catch (err) {
                fncToastError(err.message);
                //console.log(err);
            }
        }


        ///Template
        function fncTemplateChange() {
            try {

                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Open Barcode Popup
        function ShowPopupBarcode() {

            dateFormat[0].selectedIndex = 1;
            dateFormat.trigger("liszt:updated");
            if ("<%=ConfigurationManager.AppSettings["BarCode"].ToString() %>" == "A") {
                $("#divBarcode").dialog({
                    appendTo: 'form:first',
                    title: "Barcode Print",
                    width: 500,
                    modal: true

                });
            }
            else {
                $("#divBarcodeImage").dialog({
                    appendTo: 'form:first',
                    title: "Image Barcode Print",
                    width: 500,
                    modal: true
                });
            }
            setTimeout(function () {
                expiredDate.select().focus();
            }, 50);
        }


        var pkdDate;
        var expiredDate;
        var dateFormat;
        var hideMRP;
        var hidePrice;
        var hidepkdDate;
        var hideexpDate;
        var hidecompany;
        var template;
        var size;
        var print;
        //Get Barcode User Comtrol
        $(function () {

            try {
                if ("<%=ConfigurationManager.AppSettings["BarCode"].ToString() %>" == "A") {
                    pkdDate = $("#<%=barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.pakedDate).ClientID%>");
                    expiredDate = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.expiredDate).ClientID%>");
                    dateFormat = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.dateFormat).ClientID%>");
                    hideMRP = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideMRP).ClientID%>");
                    hidePrice = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidePrice).ClientID%>");
                    hidepkdDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidepkdDate).ClientID%>");
                    hideexpDate = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hideexpDate).ClientID%>");
                    hidecompany = $("#<%= barcodePrintUserControl.GetTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.hidecompany).ClientID%>");
                    template = $("#<%= barcodePrintUserControl.GetTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.template).ClientID%>");
                    size = $("#<%= barcodePrintUserControl.GetTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.size).ClientID%>");
                    print = $("#<%= barcodePrintUserControl.GetTemplateControl<LinkButton>(EnterpriserWebFinal.UserControls.BarcodePrintUserControl.TemplateControls.print).ClientID%>");
                }
                else {
                    pkdDate = $("#<%=imagebarcodePrintUserControl.GetImageTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.pakedDates).ClientID%>");
                    expiredDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.expiredDates).ClientID%>");
                    dateFormat = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.dateFormats).ClientID%>");
                    hideMRP = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hideMRPs).ClientID%>");
                    hidePrice = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidePrices).ClientID%>");
                    hidepkdDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidepkdDates).ClientID%>");
                    hideexpDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hideexpDates).ClientID%>");
                    hidecompany = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidecompanys).ClientID%>");
                    template = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.templates).ClientID%>");
                    size = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.sizes).ClientID%>");
                    print = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.printNames).ClientID%>");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        });

        ///Change days and months
        function fncDateMonth() {
            try {

                if ($(DateRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Days)');
                }
                else if ($(MonthRadioButton).is(':checked')) {
                    $(DateLable).text('Best Before (Months)');
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Template
        function fncTemplateChange() {
            try {
                var value;
                value = template.val();
                size.val(value.split('#')[1]);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }




        $(function () {
            $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if ($("#<%= txtBillDate.ClientID %>").val() === '') {
                $("#<%= txtBillDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        //$(function () {
        //    SetAutoComplete();
        //});

        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        $(function () {
            $('#<%=txtPCSQty.ClientID %>').focusout(function () {
                fncCalcTotalweight();
            });
        });


        $(function () {

            $("[id*=grdDistribution]").on('click', 'td > img', function (e) {
                var r = confirm("Do you want to Delete?");
                if (r === false) {
                    return false;
                }

                var objIssuedQty = null;
                var objBalanceQty = null;

                $("[id*=grdBulkItem] tr").each(function () {

                    objIssuedQty = $(this).find(".LIssuedQty");
                    objBalanceQty = $(this).find(".LBalanceQty");
                    objIssuedQty.text("0.00");
                });

                $(this).closest("tr").remove();
                $('#<%=txtCdescription.ClientID %>').focus().select();
                return false;
            });

        });


        ///Barcode Validation
        function fncBarcodeValidation() {

            try {
                var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                //alert(totalRows);
                if (totalRows == 0) {
                    ShowPopupMessageBox("Add item to Print !");
                }
                else {
                    ShowPopupBarcode();
                }

            }
            catch (err) {
                alert(err.message);
            }
        }




        ///Barcode Validation
        function fncBarcodeQtyValidation(type) {
            try {
                //$(".ui-resizable").hide();
                //$(".ui-front").hide();
                if (size.val() == "" && type == "Barcode") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    return false;
                }
                else if (($('#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName').val() == '' ||
                    $('#ContentPlaceHolder1_barcodePrintUserControl_ddlPrinterName').val() == '-- Select --') && type == "Barcode") {
                    ShowPopupMessageBox("Please Choose Printer Name");
                    return false;
                }
                else if (($('#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlPrinterNames').val() == '' ||
                    $('#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlPrinterNames').val() == '-- Select --') && type == "Image") {
                    ShowPopupMessageBox("Please Choose Printer Name");
                    return false;
                }
                else if (type == "Barcode") {
                    $("#<%=lnkbtnPrint.ClientID %>").click();
                    localStorage.setItem('Template', $('#ContentPlaceHolder1_barcodePrintUserControl_ddlTemplate').val());
                    return false;
                }
                else if (type == "Image") {
                    $(".container_divlist2").empty();
                    var PrinterStatus = "<%=ConfigurationManager.AppSettings["PrinterStatus"].ToString() %>";
                    localStorage.setItem('Template', $('#ContentPlaceHolder1_imagebarcodePrintUserControl_ddlTemplates').val());
                    fncImageBarcodeClick(PrinterStatus);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncImageBarcodeClick(PrinterStatus) {
            try {
                debugger
                var ddl = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.templates).ClientID%>").val();
                var PrinterName = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.printNames).ClientID%>").val();
                var pkdDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.pakedDates).ClientID%>").val();
                var expDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<TextBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.expiredDates).ClientID%>").val();
                var dateformat = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<DropDownList>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.dateFormats).ClientID%>").val();
                var pdate = pkdDate.replaceAll('/', '/');// + "T18:37:47";
                var edate = expDate.replaceAll('/', '/');// + "T18:37:47";
                var List = [];
                var listnames = [], Name = [];
                var TemplateList = "";
                var objlist;
                var objlistNew = [];
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx/GetAllBarcodeDetails")%>',
            data: "{ 'Code': '','BatchNo':'','Filename' :'" + ddl + "','NoLabels' :'','PkdDate' :'" + pdate + "','Expdate' :'" + edate + "','DateFormate' :'" + dateformat + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                debugger
                var HideMRP = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hideMRPs).ClientID%>")[0].checked ? 'Y' : 'N';
                        var HidePrice = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidePrices).ClientID%>")[0].checked ? 'Y' : 'N';
                        var HidePkdDate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidepkdDates).ClientID%>")[0].checked ? 'Y' : 'N';
                        var HideExpiredate = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hideexpDates).ClientID%>")[0].checked ? 'Y' : 'N';
                        var HideCompany = $("#<%= imagebarcodePrintUserControl.GetImageTemplateControl<CheckBox>(EnterpriserWebFinal.UserControls.ImageBarcodePrintUserControl.TemplateControls.hidecompanys).ClientID%>")[0].checked ? 'Y' : 'N';



                        for (var i = 0; i <= 12; i++) {
                            listnames.push(data.d[i]);
                        }

                        var rowObj = '', PritingCount = 0, SNO = 0;
                        $("#<%=grdDistribution.ClientID%> tr").each(function (index) {
                    if (index >= 1) {
                        rowObj = $(this);

                        PritingCount = rowObj.find('td input[id*="txtbarcode"]').val();
                        if (PritingCount != '' && parseFloat(PritingCount) > 0) {
                            objlist = {};
                            objlist.PritingCount = rowObj.find('td input[id*="txtbarcode"]').val();
                            objlist.Barcode = rowObj.find("td.Itemcode").html().trim();
                            objlist.FocQty = "0";
                            objlist.Inventorycode = rowObj.find("td.Itemcode").html().trim();
                            objlist.Description = rowObj.find("td.Description").html().trim();
                            objlist.SellingPrice = rowObj.find("td.SellingPrice").html().trim();
                            objlist.MRP = rowObj.find("td.Mrp").html().trim();
                            objlist.PKDDate = data.d[14][0].PKDDate;
                            objlist.EXPDate = data.d[14][0].EXPDate;
                            objlist.Discount = "0";
                            objlist.VendorCode = "";
                            objlist.VendorName = "";
                            objlist.SNO = (parseFloat(SNO) + 1).toString();
                            objlist.CompanyName = data.d[14][0].CompanyName;
                            objlistNew.push(objlist);
                        }
                    }
                });
                $.each(JSON.parse(JSON.stringify(objlistNew))[0], function (index, value) {
                    Name.push(index);
                });

                List.push(objlistNew);

                var ImageBarcodeDetails = data.d[13].split('|');
                TemplateList = ImageBarcodeDetails[2];

                var listimg = ObjectList(List, listnames, ddl, Name, TemplateList, HideMRP, HidePrice, HidePkdDate, HideExpiredate, HideCompany, ImageBarcodeDetails[9]);
                setTimeout(() => {
                    printerimagelist(listimg, ImageBarcodeDetails[5], ImageBarcodeDetails[6], ImageBarcodeDetails[4], ImageBarcodeDetails[1], ImageBarcodeDetails[8], PrinterStatus, PrinterName, "DB");
                }, 500);
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            },
            failure: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function QR_ImageAndReplaceQRCode(values) {
            debugger
            //var canvas = document.createElement('canvas');

            //canvas.id = "qr-code";
            //canvas.style.position = "absolute";
            //canvas.style.border = "1px solid";
            var base64 = [];
            var codecanvas = document.getElementById('qr-code'),
                qr = new QRious({
                    element: codecanvas,
                    size: 128,
                    value: values
                });
            codecanvas.style.display = "none";
            dataUrl = codecanvas.toDataURL(),
                imageFoo = document.createElement('img');
            imageFoo.src = dataUrl;
            base64 = imageFoo.src;
            console.log(base64);
            return base64;
        }
        function UpdateValidation() {
            //alert('test');
            //Show = Show + 'Already Exists';

            //if (Show != '') {
            //    ShowPopupMessageBox(Show);
            //    return false;
            //}
            try {

                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "YES": function () {
                            $(this).dialog("close");
                            $("#<%=lnkUpdate.ClientID %>").click();
                            return false;
                        },
                        "NO": function () {
                            $(this).dialog("close");
                            return false();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }
        function AddbtnValidation() {

            try {


                if ($('#<%=txtChildCode.ClientID %>').val() == '') {

                    alert('Item Code should not be Empty !');
                    $('#<%=txtChildCode.ClientID %>').focus().select();
                    return false;
                }

                if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {
                    alert('Invalid Weight Qty !');
                    $('#<%=txtPCSQty.ClientID %>').focus().select();

                    return false;
                }

                if ($('#<%=txtWeight.ClientID %>').val() <= 0) {
                    alert('Invalid Weight Qty !');
                    $('#<%=txtWeight.ClientID %>').focus().select();
                    return false;
                }

                if ($("#HiddenAllowNegDist").val() == 'Y') {

                        //alert('test');
                        <%-- var CheckStock = '';

                    $("[id*=grdBulkItem] tr").each(function () {                       

                        var dBalanceQty = $(this).find(".LBalanceQty").text();
                                            
                                            if (dBalanceQty != "")
                                            {
                                                //alert(dBalanceQty);
                                                //alert($('#<%=txtTotalWeight.ClientID %>').val());
                                                //var Balance = parseFloat(dBalanceQty);
                                                
                                                if (parseFloat($('#<%=txtTotalWeight.ClientID %>').val()) > parseFloat(dBalanceQty)) {

                                                    //alert(dBalanceQty);
                                                    CheckStock = 'less';

                                                  }
                                            }                      
                    });

                    if (CheckStock == 'less')
                    {
                        //ShowPopupMessageBox('Stock is Less !');
                        popUpObjectForSetFocusandOpen = $('#<%=txtPCSQty.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('Stock is Less !');
                        // $('#<%=txtPCSQty.ClientID %>').focus().select();
                        return false;
                    }--%>
                }
                    <%--       
                    var totalRows = 0;
                    totalRows = $("#<%=grdDistribution.ClientID %> tr").length;--%>
                    <%--if ($("#<%=grdDistribution.ClientID%> tr:has(td)").each(function () {
                         var gitem = $(this).find("td.Itemcode").html();
                        var gDes = $(this).find("td.Description").html();
                        if (($.trim(gitem) == $.trim($('#<%=txtChildCode.ClientID %>').val())) && ($.trim(gDes) == $.trim($('#<%=txtCdescription.ClientID %>').val()))) {
                            $(this).find("td.PCS").html() = $('#<%=txtPCSQty.ClientID %>').val();
                    }

                    });--%>

                var totalRows = $("#<%=grdDistribution.ClientID %> tr").length;
                var confirm_value = "";
                //alert(totalRows);
                $("#<%=grdDistribution.ClientID%> tr").each(function () {

                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td.Itemcode").html();
                    var mrp = $(this).find("td.Mrp").html();
                    if (($.trim(gitem) == $.trim($('#<%=txtChildCode.ClientID %>').val())) && ($.trim($('#HiddenMrp').val()) == mrp)) {
                        confirm_value = document.createElement("INPUT");
                        confirm_value.type = "hidden";
                        confirm_value.name = "confirm_value";
                        if (confirm("Item Already exists!. Do you want to update Packed Qty?")) {
                            confirm_value.value = "Yes";

                        } else {
                            confirm_value.value = "No";
                        }

                        document.forms[0].appendChild(confirm_value);
                    }
                    else if (confirm_value == null) {
                        confirm_value = "";
                    }
                });

                Confirm();


            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function Confirm() {
            //debugger;
            //var gitem = $(this).find("td:eq(1)");
        }

        //Get Calc Child Qty
        function fnctxtChildQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtPCSQty.ClientID %>').val() <= 0) {

                        alert('Invalid Weight Qty !');
                        $('#<%=txtPCSQty.ClientID %>').focus().select();

                        return false;
                    }



                    fncCalcTotalweight();



                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    return false;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncCalcTotalweight() {

            try {
                var ChildQty = $('#<%=txtPCSQty.ClientID%>').val();
                var ChildWeight = $('#<%=txtWeight.ClientID%>').val();
                var TotalQty;
                TotalQty = ChildWeight * ChildQty;
                $('#<%=txtTotalWeight.ClientID%>').val(TotalQty.toFixed(3));

            }
            catch (err) {
                alert(err.Message);
            }
        }


            //        $(function () {
            //            $("[id*=grdDistribution]").on('click', 'td > img', function (e) {

            //                var r = confirm("Do you want to Delete?");
            //                if (r === false) {
            //                    return false;
            //                }
            //                var rowCount = $("[id*=grdBulkItem] tr:last").index() + 1;
            //                if (rowCount == 2) {
            //                    $("[id*=grdBulkItem] tr:last").after('<tr class="pshro_GridDgnStyle"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img src="../images/delete.png" /></td></tr>');
            //                }
            //                $(this).closest("tr").remove();
            //                $('#<%=txtCdescription.ClientID %>').focus().select();
        //            });
        //            return false;
        //        });


        $(function () {
            $("[id*=grdDistribution] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdDistribution] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });


        function SetAutoComplete() {
            //debugger;

            $("[id$=txtCdescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmDistribution.aspx/GetBulkDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",

                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valInvDet: item.split('|')[0],
                                    valCost: item.split('|')[1],
                                    valSellPr: item.split('|')[2],
                                    valWeight: item.split('|')[3],
                                    valMrp: item.split('|')[4],
                                    valParentCode: item.split('|')[5],
                                    valBatch: item.split('|')[6]
                                }
                            }))
                        },

                        error: function (response) {
                            //alert(response.responseText);
                        },
                        failure: function (response) {
                            //alert(response.responseText);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                select: function (e, i) {

                    //alert(i.item.val);
                    $('#<%=txtChildCode.ClientID %>').val($.trim(i.item.valInvDet.split('--')[0]));
                    $('#<%=txtCdescription.ClientID %>').val($.trim(i.item.valInvDet.split('--')[1]));
                    $('#<%=txtCost.ClientID %>').val($.trim(i.item.valCost));
                    $('#<%=txtSellPrice.ClientID %>').val($.trim(i.item.valSellPr));
                    $('#<%=txtWeight.ClientID %>').val($.trim(i.item.valWeight));
                    $("#HidBatch").val($.trim(i.item.valBatch));
                    if ($("#HidBatch").val() == 'True') {
                        fncGetInventoryCode($.trim($.trim(i.item.valInvDet.split('--')[0])));
                    }
                    $('#<%=txtPCSQty.ClientID %>').focus().select();

                    $("#hidenChildCode").val($.trim(i.item.valInvDet.split('--')[0]));
                    $("#HiddenMrp").val($.trim(i.item.valMrp));
                    $("#hidenParentcode").val($.trim(i.item.valParentCode));
                    //alert($("#hidenParentcode").val());
                    return false;
                },
                minLength: 1
            });

            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtCdescription'
                });
            });
        }



        function fncBarcodeDetail(sItemCode) {
            //debugger;
            var isBatch, Code = '', Description, GST, Cost;
            $.ajax({
                url: '<%=ResolveUrl("~/Inventory/frmDistribution.aspx/GetItemDetails")%>',
                data: "{ 'ItemCode': '" + sItemCode + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //alert(data.d);
                    $.map(data.d, function (item) {
                        Code = item.split('|')[0],
                            valInvDet = item.split('|')[0],
                            valCost = item.split('|')[1],
                            valSellPr = item.split('|')[2],
                            valWeight = item.split('|')[3],
                            valMrp = item.split('|')[4],
                            valParentCode = item.split('|')[5],
                            valBatch = item.split('|')[6]
                    })


                    if (Code != '') {

                        $('#<%=txtChildCode.ClientID %>').val($.trim(valInvDet.split('--')[0]));
                        $('#<%=txtCdescription.ClientID %>').val($.trim(valInvDet.split('--')[1]));
                        $('#<%=txtCost.ClientID %>').val($.trim(valCost));
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(valSellPr));
                        $('#<%=txtWeight.ClientID %>').val($.trim(valWeight));
                        $("#HidBatch").val($.trim(i.item.valBatch));
                        if ($("#HidBatch").val() == 'True') {
                            fncGetInventoryCode($.trim($.trim(i.item.valInvDet.split('--')[0])));
                        }
                        $('#<%=txtPCSQty.ClientID %>').focus().select();

                        $("#hidenChildCode").val($.trim(valInvDet.split('--')[0]));
                        $("#HiddenMrp").val($.trim(valMrp));
                        $("#hidenParentcode").val($.trim(valParentCode));
                    }
                    else {
                        ClearTextBox();
                        popUpObjectForSetFocusandOpen = $('#<%=txtCdescription.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject('Invalid Code !');
                    }

                },
                error: function (response) {
                    // alert(response.responseText);
                },
                failure: function (response) {
                    // alert(response.responseText);
                }
            });
        }


        function fncInventorySearch(event) {

            try {

                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    if ($("#<%=txtCdescription.ClientID %>").val() != '') {
                        //alert($("#<%=txtCdescription.ClientID %>").val());
                        if (!isNaN($("#<%=txtCdescription.ClientID %>").val())) {
                            fncBarcodeDetail($("#<%=txtCdescription.ClientID %>").val());
                        }

                    }
                    //event.preventDefault();
                    //event.stopPropagation();
                    return false;
                }
                else if (keyCode == 220) {
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

       <%-- function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                
                    //event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtChildCode.ClientID %>").val());
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }--%>
        function fncGetInventoryCode(ItemCode) {
            try {
                $('#<%=Hidinv.ClientID %>').val(ItemCode);
                var obj = {};
                obj.code = ItemCode;
                obj.formName = 'ChildBatch';
                if (obj.code == "")
                    return;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "[]") {
                            ShowPopupMessageBox("Invalid ItemCode. Please Enter Valid Parent Bulk Item !.");
                            return false;
                        }
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try {
                var inputNumber = $('#<%=Hidinv.ClientID %>').val();
                var regexp = /^[0-9]*$/;
                var numberCheck = regexp.exec(inputNumber);
                var objBulkBatch, row;
                objBulkBatch = jQuery.parseJSON(msg.d);
                $('#<%=txtChildCode.ClientID %>').attr('disabled', 'disabled');
                if (objBulkBatch.length > 0) {

                    if (objBulkBatch.length == 1 && numberCheck != null && ($('#<%=Hidinv.ClientID %>').val().length == 15
                        || $('#<%=Hidinv.ClientID %>').val().length == 12)) {
                        if (objBulkBatch[0]["BatchNo"] != "") {
                            $('#<%=txtChildCode.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                            <%--$('#<%=txtBatchNo.ClientID %>').val($.trim(objBulkBatch[0]["BatchNo"]));--%>
                            $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                            $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                            <%--$('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));--%>
                            $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));
                            $('#<%=txtPCSQty.ClientID %>').focus();
                        }
                        else if (objBulkBatch[0]["BatchNo"] == "") {
                            $('#<%=txtChildCode.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                            <%--$('#<%=txtBatchNo.ClientID %>').val('');--%>
                            $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                            $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                            $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                            <%--$('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));--%>
                            $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));

                            $('#<%=txtPCSQty.ClientID %>').focus();
                        }
                    }
                    else if (objBulkBatch[0]["BatchNo"] == "") {
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["InventoryCode"]));
                        <%--$('#<%=txtBatchNo.ClientID %>').val('');--%>
                        $('#<%=txtCdescription.ClientID %>').val($.trim(objBulkBatch[0]["Description"]));
                        $('#<%=txtTotalWeight.ClientID %>').val($.trim(objBulkBatch[0]["Qty"]));
                        $('#<%=txtCost.ClientID %>').val($.trim(objBulkBatch[0]["Cost"]));
                        <%--$('#<%=txtMrp.ClientID %>').val($.trim(objBulkBatch[0]["MRP"]));--%>
                        $('#<%=txtSellPrice.ClientID %>').val($.trim(objBulkBatch[0]["SellingPrice"]));
                        if (objBulkBatch[0]["Lqty"] != undefined)
                            $('#<%=txtTotalWeight.ClientID%>').val(parseFloat(parseFloat(objBulkBatch[0]["Lqty"].trim()) / 1000).toFixed(3));

                        $('#<%=txtPCSQty.ClientID %>').focus();
                    }
                    else {
                        tblBatchBody = $("#tblBatch tbody");
                        tblBatchBody.children().remove();
                        for (var i = 0; i < objBulkBatch.length; i++) {
                            row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                $.trim(objBulkBatch[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                $.trim(objBulkBatch[i]["BatchNo"]) + "</td><td id='tdCost_" + i + "' >" +
                                objBulkBatch[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                                objBulkBatch[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                objBulkBatch[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                $.trim(objBulkBatch[i]["InventoryCode"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                                (objBulkBatch[i]["AllowExpireDate"]) + "</td></tr>";

                            tblBatchBody.append(row);

                        }

                        fncInitializeBatchDetail();
                        tblBatchBody.children().dblclick(fncBatchRowClick);

                        tblBatchBody[0].setAttribute("style", "cursor:pointer");

                        tblBatchBody.children().on('mouseover', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "Yellow");
                        });

                        tblBatchBody.children().on('mouseout', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "white");
                        });

                        tblBatchBody.children().on('keydown', function () {
                            fncBatchKeyDown($(this), event);
                        });

                        $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                        $("#tblBatch tbody > tr").first().focus();
                    }
                }

                fncBatchDeleteCheck($.trim(objBulkBatch[0]["InventoryCode"]));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {
            try {
                setTimeout(function () {
                    <%--$('#<%=hidExp.ClientID %>').val('');--%>
                    $('#<%=txtChildCode.ClientID %>').val('');
                    <%--$('#<%=hidExp.ClientID %>').val('');--%>
                    <%--$('#<%=txtBatchNo.ClientID %>').val($.trim($(rowObj).find('td[id*=tdBatchNo]').text()));--%>
                    $('#<%=txtCost.ClientID %>').val($.trim($(rowObj).find('td[id*=tdCost]').text()));
                    $('#<%=txtTotalWeight.ClientID %>').val($.trim($(rowObj).find('td[id*=tdStock]').text()));
                    $('#<%=txtChildCode.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));
                    $('#<%=txtCdescription.ClientID %>').val($('#<%=txtCdescription.ClientID %>').val());
                    <%--$('#<%=txtExpiredDate.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));--%>
               $('#HiddenMrp').val($.trim($(rowObj).find('td[id*=tdMRPNew]').text()));
                    
                    $('#<%=txtSellPrice.ClientID %>').val($.trim($(rowObj).find('td[id*=tdSellingPrice]').text()));
                    <%--$('#<%=txtWeight.ClientID %>').val($.trim($(rowObj).find('td[id*=tdWprice1]').text()));--%>
                    $("#stkadj_batch").dialog('close');
                    $('#<%=txtPCSQty.ClientID %>').focus().select();
                    <%--$('#<%=hidExp.ClientID %>').val($.trim($(rowObj).find('td[id*=tdExpiredDate]').text()));--%>
                }, 50);
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitializeBatchDetail() {
            try {
                $("#stkadj_batch").dialog({
                    titt: "Batch Details",
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchDeleteCheck(ItemCode) {
            try {
                var obj = {}
                obj.Inventorycode = ItemCode;
                $.ajax({
                    type: "POST",
                    url: "../Stock/frmStockAdjustmentEntry.aspx/fncGetInventoryDetail_BatchDelete",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBatchDeleteInventories(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncBatchDeleteInventories(source) {
            try {
                MRPDEL = "";
                var obj = {};
                obj = jQuery.parseJSON(source.d);
                if (obj.length > 0) {
                    for (var i = 0; i < obj.length; i++) {
                        MRPDEL += obj[i]["MRP"] + ',';
                    }
                }
                //console.log(MRPDEL);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function ClearTextBox() {
            //alert('test');
            try {
                $('#<%=txtChildCode.ClientID %>').val('');
                $('#<%=txtCdescription.ClientID %>').val('');
                $('#<%=txtCost.ClientID %>').val(0);
                $('#<%=txtSellPrice.ClientID %>').val(0);
                $('#<%=txtWeight.ClientID %>').val(0);
                $('#<%=txtPCSQty.ClientID %>').val(0);
                $('#<%=txtTotalWeight.ClientID %>').val(0);
                $("#hidenChildCode").val('');
                $("#HiddenMrp").val(0);
                $('#<%=txtCdescription.ClientID %>').focus().select();
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        //Show Popup After Save
        function fncShowBarcodeSuccessMessage() {
            try {
                $(function () {
                    $("#barcodeprinting").html('<%=Resources.LabelCaption.Save_Barcodeprinting%>');
                    $("#barcodeprinting").dialog({
                        title: "Enterpriser Web",
                        buttons: {
                            Ok: function () {
                                $(this).dialog("destroy");
                                fncClear();
                            }
                        },
                        modal: true
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        <%--function fncFocusGrid() {
            //alert('test');
            //debugger;
            ClearTextBox();
            try {

                $('#<%=txtCdescription.ClientID %>').focus();
                //$("html, body").animate({

                //    scrollTop: $("#divscroll").get(0).scrollHeight
                //}, 2000);
                //console.log("dxbgdfc");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>

        function fnchide() {
            try {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
                return true;
            }
            catch (er) {
                ShowPopupMessageBox(err.message);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                        $('#<%=lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

    </script>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'Distribution');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "Distribution";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:UpdatePanel ID="updtPnlgrdgrdDepartmentList" runat="Server">
        <ContentTemplate>

            <div class="main-container" id="form1">

                <div class="breadcrumbs">
                    <ul>
                        <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="../Inventory/frmDistributionList.aspx">Distribution List</a><i class="fa fa-angle-right"></i></li>
                        <li class="active-page" id="DtCreation" runat="server">Distribution Creation</li>
                        <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
                        <%-- <li class="active-page display_none" id ="DtReturn" runat >Distribution Return</li>--%>
                    </ul>
                </div>
                <div class="EnableScroll">
                    <div class="container-group-pc">
                        <asp:HiddenField ID="hidDtReturn" runat="server" Value="" />
                        <div class="right-container-top-distribution">

                            <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                                <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>Are you sure to Update this Item?</p>
                            </div>
                            <%--<div class="right-container-top-header">
                            Header Detail
                        </div>--%>
                            <div class="right-container-bottom-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left-pc" style="margin-left: 20px">
                                        <div class="label-right-pc">
                                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_partyCode %>'></asp:Label>
                                            <asp:DropDownList ID="PartyCodeDropdown" runat="server" CssClass="form-control-res" Width="100px"
                                                AutoPostBack="true" OnSelectedIndexChanged="PartyCodeDropdown_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-left-Distribut-txt" style="margin-left: -57px;">
                                        <div class="label-left-pc">
                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_name %>'></asp:Label>
                                        </div>
                                        <div class="label-right-pc" style="width: 180px">
                                            <asp:TextBox ID="txtPartyName" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left-Distb" style="margin-left: -138px">
                                        <div class="label-left-pc">
                                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lblbillno %>'></asp:Label>
                                        </div>
                                        <div class="label-right-pc">
                                            <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left-Distb" style="margin-left: -21px">
                                        <div class="label-left-pc">
                                            <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Trays %>'></asp:Label>
                                        </div>
                                        <div class="label-right-pc">
                                            <asp:TextBox ID="txtTrays" runat="server" Text="0" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left-Distb" style="margin-left: 2px">
                                        <div class="label-left-pc">
                                            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bags %>'></asp:Label>
                                        </div>
                                        <div class="label-right-pc">
                                            <asp:TextBox ID="txtBags" Text="0" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left-Distb" style="margin-left: 2px">
                                        <div class="label-left-pc">
                                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_Emptybags %>'></asp:Label>
                                        </div>
                                        <div class="label-right-pc">
                                            <asp:TextBox ID="txtEmptyBag" Text="0" runat="server" CssClass="form-control-res"
                                                onkeypress="return isNumberKey(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right-Distb" style="margin-right: 24px">
                                        <div class="label-right-pc">
                                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblBilldate %>'></asp:Label>
                                            <asp:TextBox ID="txtBillDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-left-pc" style="margin-left: 5px">
                                        <div class="label-right-pc">
                                            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefrenceNo %>'></asp:Label>
                                            <asp:DropDownList ID="RefNoDropDown" runat="server" CssClass="form-control-res" AutoPostBack="true" Width="100px"
                                                OnSelectedIndexChanged="RefNoDropDown_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-left-pc" style="margin-left: -63px">
                                        <div class="label-right-pc">
                                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_LotNumber %>'></asp:Label>
                                            <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <%-- <div class="control-group-left-pc" style="margin-left: 20px">
                            <div class="label-right-pc">
                                <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_RefrenceNo %>'></asp:Label>
                                <asp:DropDownList ID="RefNoDropDown" runat="server" CssClass="form-control-res" AutoPostBack="true"
                                    OnSelectedIndexChanged="RefNoDropDown_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-left-pc" style="margin-left: 10px">
                            <div class="label-right-pc">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_LotNumber %>'></asp:Label>
                                <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-container-top-detail">
                        <div class="col-md-12  Parent">
                            Parent Item
                        </div>
                        <div class="GridDetails">
                            <div class="row">
                                <%--    <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                                <div class="grdLoad1">
                                    <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Inventorycode
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Cost
                                                </th>
                                                <th scope="col">SellingPrice
                                                </th>
                                                <th scope="col">QtyOnHand
                                                </th>
                                                <th scope="col">TakenQty
                                                </th>
                                                <th scope="col">IssuedQty
                                                </th>
                                                <th scope="col">BalanceQty
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>

                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; background-color: aliceblue; height: 135px;">
                                        <asp:GridView ID="grdBulkItem" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="false"
                                            ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left">
                                            <PagerStyle CssClass="pshro_text" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <Columns>
                                                <asp:BoundField HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                                <asp:BoundField HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField HeaderText="Cost" DataField="Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField HeaderText="QtyOnHand" DataField="QtyOnHand" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <%--<asp:BoundField HeaderText="TakenQty" DataField="TakenQty"  ItemStyle-HorizontalAlign="Right"></asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="Taken Qty" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtTakenQty" runat="server" Text='<%# Eval("TakenQty") %>'
                                                            onkeypress="return isNumberKey(event)" CssClass="grid-textbox1" Style="background-color: yellow"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="IssuedQty" DataField="IssuedQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LIssuedQty"></asp:BoundField>
                                                <asp:BoundField HeaderText="BalanceQty" DataField="BalanceQty" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="LBalanceQty"></asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                PageButtonCount="5" Position="Bottom" />
                                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="control-group-split">
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpic" runat="server" Text="Inventory Code:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtParentCode" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Description">
                            <div class="label-left-pc">
                                <asp:Label ID="lblpdes" runat="server" Text="Description:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPdescription" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label8" runat="server" Text="Weight:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label9" runat="server" Text="Qty:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label10" runat="server" Text="Cost:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPcost" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label11" runat="server" Text="Selling Price:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPsellingPrice" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-left-Distb">
                            <div class="label-left-pc">
                                <asp:Label ID="Label12" runat="server" Text="Total Weight:"></asp:Label>
                            </div>
                            <div class="label-right-pc">
                                <asp:TextBox ID="txtPtotalWeight" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                    </div>--%>
                    <div class="right-container-top-header">
                        Child Item
                    </div>
                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                                <div class="grdLoad">
                                    <%--   <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">Inventorycode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Weight
                                            </th>
                                            <th scope="col">PCS
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">Total Qty PKD
                                            </th>
                                            <th scope="col">Mrp
                                            </th>
                                            <th scope="col">Barcode
                                            </th>
                                            <th scope="col">ParentCode
                                            </th>
                                        </tr>
                                    </thead>
                                </table>--%>
                                    <div class="GridDetails" id="divscroll" style="overflow-x: hidden; overflow-y: scroll; height: 150px; width: 1333px; background-color: aliceblue;">
                                        <asp:GridView ID="grdDistribution" runat="server" AutoGenerateColumns="false" PageSize="14" ShowHeader="true"
                                            ShowHeaderWhenEmpty="True" CssClass="pshro_GridDgnStyle tbl_left" OnRowDataBound="grdDistribution_RowDataBound" OnRowDeleting="grdDistribution_RowDeleting"
                                            OnRowCommand="grdDistribution_RowCommand">
                                            <PagerStyle CssClass="pshro_text" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <Columns>
                                                <%--<asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />--%>
                                                <%--<asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Delete" ID="Delete" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate> 
                                    <img src="../images/delete.png" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                                <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" ButtonType="Button" />
                                                <asp:BoundField ItemStyle-CssClass="Itemcode" HeaderText="Inventorycode" ItemStyle-HorizontalAlign="Left" DataField="Inventorycode"></asp:BoundField>
                                                <asp:BoundField ItemStyle-CssClass="Description" HeaderText="Description" DataField="Description" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField ItemStyle-CssClass="Weight right_align" HeaderText="Weight" DataField="Weight" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField HeaderText="PCS" ItemStyle-CssClass="PCS" DataField="PCS" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField HeaderText="Cost" DataField="Cost" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="Cost right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="SellingPrice" DataField="SellingPrice" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="SellingPrice right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="Total Qty PKD" DataField="PackedQty" ItemStyle-CssClass="PackedQty" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField HeaderText="Mrp" DataField="Mrp" ItemStyle-HorizontalAlign="Right" ItemStyle-CssClass="Mrp right_align"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Barcode" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtbarcode" runat="server" Text='<%# Eval("Barcode") %>'
                                                            onkeypress="return isNumberKey(event)" CssClass="grid-textbox2" Style="background-color: whitesmoke"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="ParentCode" DataField="Parentcode" HeaderStyle-CssClass="Parentcode hiddencol" ItemStyle-CssClass="hiddencol1"></asp:BoundField>
                                                <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox Text="Select All" ID="checkAll" runat="server" onclick="checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this)" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                            </Columns>
                                            <%--<EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>--%>
                                            <HeaderStyle CssClass="pshro_GridNewHeaderCellCenter" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                PageButtonCount="5" Position="Bottom" />
                                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="right-container-bottom-detail">
                        <div class="control-group-split">
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtChildCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-Description">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <div class="ui-widget">
                                        <asp:TextBox ID="txtCdescription" runat="server" CssClass="form-control-res" onkeydown="return fncInventorySearch(event)"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_weight %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtPCSQty" runat="server" CssClass="form-control-res" onkeydown="return fnctxtChildQty(event)"
                                        onkeypress="return isNumberKey(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_cost %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_sellingPrice %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtSellPrice" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-Distb">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_TotalWeight %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtTotalWeight" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    <asp:TextBox ID="txtSort" runat="server" Text="1" CssClass="display_none"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClick="lnkAdd_Click"
                                Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick=" return AddbtnValidation();"> </asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                                Text='<%$ Resources:LabelCaption,btnClear %>'> </asp:LinkButton>
                        </div>
                    </div>
                    <div class="control-button">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClick="lnkSave_Click" OnClientClick="return fnchide();"
                                        Text='<%$ Resources:LabelCaption,btnSave %>'> </asp:LinkButton>
                                </div>
                                <div class="control-button hiddencol ">
                                    <asp:LinkButton ID="lnkClearform" runat="server" CssClass="button-red"
                                        Text='<%$ Resources:LabelCaption,btnClear %>' OnClick="lnkClearform_Click"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkBarcode" runat="server" CssClass="button-red" OnClientClick="fncBarcodeValidation();"
                                        Text='<%$ Resources:LabelCaption,Home_barcode %>'></asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

                <asp:HiddenField ID="hidenChildCode" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:HiddenField ID="hidenParentcode" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:HiddenField ID="HiddenMrp" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:HiddenField ID="HiddenAllowNegDist" runat="server" ClientIDMode="Static" Value="N" />
                <asp:HiddenField ID="HidBatch" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:HiddenField ID="Hidinv" runat="server" ClientIDMode="Static" Value="NO" />
                <asp:Button ID="lnkbtnPrint" runat="server" OnClick="lnkbtnPrint_Click" class="hiddencol" />
                <div id="firstone" class="container_divlist1">
                    <div id="firstone1" class="container_divlist1">
                    </div>
                </div>
                <div id="header_div"></div>
                <div class="hiddencol">
                    <div id="barcodeprinting" class="barcodesavedialog">
                    </div>

                </div>
                <div id="divHiddenButton" style="display: none">
                    <asp:LinkButton ID="lnkUpdate" runat="server" CssClass="button-red" OnClick="lnkUpdate_Click"> </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divBarcode" style="display: none;">
        <ups:BarcodePrintUserControl runat="server" ID="barcodePrintUserControl" OnPrintButtonClientClick="return fncBarcodeQtyValidation('Barcode');" />
    </div>
    <div id="divboobalan">
        <div id="divBarcodeImage" style="display: none;">
            <ups:ImageBarcodePrintUserControl runat="server" ID="imagebarcodePrintUserControl" OnPrintButtonClientClick="fncBarcodeQtyValidation('Image');return false;" />
        </div>
    </div>
    <div class="hiddencol">
        <div id="stkadj_batch">
            <div class="Payment_fixed_headers BatchDetail">
                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th id="thBatchNo" scope="col">BatchNo
                            </th>
                            <%--<th scope="col">Stock
                            </th>--%>
                            <th scope="col">Cost
                            </th>
                            <th scope="col">MRP
                            </th>
                            <th scope="col">SellingPrice
                            </th>
                            <th scope="col">InventoryCode
                            </th>
                            <%--<th scope="col">Description
                            </th>--%>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <canvas id="qr-code"></canvas>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
