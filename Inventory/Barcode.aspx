﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="Barcode.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.Barcode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../BarcodeToolController/Scripts/modernizr-2.6.2.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/dom-to-image.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/dom-to-image.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/JsBarcode.all.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/fabric.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/html2canvas.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/html2canvas.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/jspdf.debug.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/qrcode.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/qrious.min.js"></script>
    <script type="text/javascript" src="../js/BarcodeTemplateJs/jquery.classyqr.js"></script>
    <script type="text/javascript" src="../js/BarcodeTemplateJs/jquery.classyqr.min.js"></script>
  <%--  <script type="text/javascript" src="../js/BarcodeTemplateJs/qrcode.min.js"></script>--%>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/require.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/jquery-rotate.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/modernizr.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/zip.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/zip-ext.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/deflate.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/notify.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/notify.min.js"></script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/Common_BarcodePrinter.js"></script>
    <link href="../BarcodeToolController/Content/css/mystyle.css" rel="stylesheet" />

    <style type="text/css">
        /* vietnamese */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K8-C8QSw.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K9-C8QSw.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3Kz-C8.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwaPGR_p.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnruxElg4M.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnrtREl.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        } 

        /* cyrillic-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SkvzAbt.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SAvzAbt.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SgvzAbt.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_ScvzAbt.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* hebrew */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SYvzAbt.woff2) format('woff2');
            unicode-range: U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SsvzAbt.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SovzAbt.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SQvzA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7mlx17r.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7ilx17r.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7alxw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Candal';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/candal/v10/XoHn2YH6T7-t_8c9BhQI.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Copse';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/copse/v10/11hPGpDKz1rGb3dkFEk.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Orbitron';
            font-style: normal;
            font-weight: 500;
            src: url(https://fonts.gstatic.com/s/orbitron/v17/yMJMMIlzdpvBhQQL_SC3X9yhF25-T1ny_CmBoWgz.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Crimson Text';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/crimsontext/v11/wlp2gwHKFkZgtmSR3NB0oRJfbwhT.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* cyrillic-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjbYJwQj.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6Vj_YJwQj.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjTYJwQj.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjXYJwQj.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjvYJw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        /* vietnamese */
        @font-face {
            font-family: 'Francois One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/francoisone/v15/_Xmr-H4zszafZw3A-KPSZut9zgiRi_Y.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Francois One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/francoisone/v15/_Xmr-H4zszafZw3A-KPSZut9zwiRi_Y.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Francois One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/francoisone/v15/_Xmr-H4zszafZw3A-KPSZut9wQiR.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }


        .container_divlist1 {
            width: 22.4cm;
            background-color: #ffffff;
        }

    .container_divlist2 {
        text-align: left;
        background-color: #ffffff;
    }

 
    .fake_img {
            margin: 5px 9px;
            border-radius: 7px;
            margin-left: 8px;
            padding: 15px 0px 0px 15px;
        }
        .gid_Itemsearch td:nth-child(1), .gid_Itemsearch th:nth-child(1) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(2), .gid_Itemsearch th:nth-child(2) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(3), .gid_Itemsearch th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .gid_Itemsearch td:nth-child(4), .gid_Itemsearch th:nth-child(4) {
            min-width: 460px;
            max-width: 460px;
        }

        .gid_Itemsearch td:nth-child(5), .gid_Itemsearch th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(6), .gid_Itemsearch th:nth-child(6) {
            min-width: 120px;
            max-width: 120px;
        }

        .gid_Itemsearch td:nth-child(7), .gid_Itemsearch th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }

        .body {
            background: #F5F5F5;
        }  
        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <script type="text/javascript">
        var row;
        //Page Load function
        var object = "";
        var canvas_id = "";
        var div_id = "";
        var imagebase64list = "";
        var chunk = [];

        function getdate() {
            var today = $('#<%=txtPkdData.ClientID%>').val();
            var d = {};
            d = today.split('/');
            var dd1 = d[0];
            var mm1 = d[1] - parseFloat(1);
            var yy1 = d[2];
            var newdate = mm1 + "/" + dd1 + "/" + yy1;
            var date = new Date(yy1, mm1, dd1);
            var newdate = new Date(date);
            var typeDate = $('#<%=txtBefore.ClientID%>').val();
            


            newdate.setDate(newdate.getDate() + parseFloat(typeDate));
            //   newdate.setDate(newdate.getMonth() - 1);
            var dd = ("0" + newdate.getDate()).slice(-2);
            var mm = newdate.getMonth() + 1;
            var y = newdate.getFullYear();
            var s = {};
            debugger
            if ($.trim(mm) != '10')
                if ($.trim(mm) != '11')
                    if ($.trim(mm) != '12')
                        mm = '0' + mm
            $('#<%=txtExpiredate.ClientID%>').val(dd + '/' + mm + '/' + y); 
            $('#<%=txtExpiredate.ClientID%>').attr("readonly", "readonly");
        }

        function pageLoad() {
            try {
                debugger
                
                $('#<%=txtInventoryCode.ClientID %>').focus();

                if ($("#<%=hiddatestatus.ClientID %>").val() == "") {
                    $("#<%= txtPkdData.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                    $("#<%= txtExpiredate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                    $('#<%= ddlDateformat.ClientID %>')[0].selectedIndex = 1;
                    $('#<%= ddlDateformat.ClientID %>').trigger("liszt:updated");
                }
                else {
                    $("#<%= txtPkdData.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker();
                    $("#<%= txtExpiredate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker();
                }

                $("#<%=hiddatestatus.ClientID %>").val('DataInitialize')


                // set the item in localStorage
                if (localStorage.getItem('Template') != null) {
                    $('#<%=ddlTemplate.ClientID %>').val(localStorage.getItem('Template'));
                    $("#<%=ddlTemplate.ClientID %>").trigger("liszt:updated");
                    fncTemplateChange();
                }
                if ($('#<%=hidExpiredDate.ClientID %>').val() != "")
                    $('#<%=txtExpiredate.ClientID %>').val($('#<%=hidExpiredDate.ClientID %>').val());
                else
                    $("#<%= txtExpiredate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                if ($('#<%=hidEAN.ClientID %>').val() == "N") {
                    $('#<%=chkEAN.ClientID %>').css("display", "none");
                }
                else {
                    $('#<%=chkEAN.ClientID %>').css("display", "block"); 
                    $("#<%=chkEAN.ClientID %>").css("margin-left", "97px");
                    $("#<%=chkEAN.ClientID %>").css("margin-top", "-16px");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);

            }
        }

        $(function () {
            //  SetAutoComplete();
        });

        //Show Popup Control        
        function fncShowPopUp() {
            try {
                DialogSearchKeyWord.val('');
                inventory.children().remove();
                inventory.trigger("liszt:updated");
                $("#inventoryDialog").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtInventoryCode.ClientID %>').val(itemcode);
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetInventoryCode(event) { 
            try {
                
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    $('#<%=txtInventoryCode.ClientID%>').attr("readonly", "readonly");

                    var inputNumber = $('#<%=txtInventoryCode.ClientID %>').val();
                    var regexp = /^[0-9]*$/;
                    var numberCheck = regexp.exec(inputNumber); 
                    $('#<%=hidEANo.ClientID%>').val($('#<%=txtInventoryCode.ClientID %>').val());//surya19052022
                    if (numberCheck != null && $('#<%=txtInventoryCode.ClientID %>').val().length == 15) {
                        fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
                        $('#<%=hidBatchNo.ClientID%>').val($('#<%=txtInventoryCode.ClientID%>').val().substring(7, 15));
                        $('#<%=txtInventoryCode.ClientID%>').val($('#<%=txtInventoryCode.ClientID%>').val().substring(0, 6));
                        Status = "Assigned";
                    }
                   <%-- else if (numberCheck != null && $('#<%=txtInventoryCode.ClientID %>').val().length == 12) {

                        fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
                        $('#<%=hidBatchNo.ClientID%>').val($('#<%=txtInventoryCode.ClientID%>').val().substring(7, 12));
                        $('#<%=txtInventoryCode.ClientID%>').val($('#<%=txtInventoryCode.ClientID%>').val().substring(0, 6));
                        Status = "Assigned";
                    }--%>
                    else if (numberCheck != null && $('#<%=txtInventoryCode.ClientID %>').val().length == 6) {
                        Status = "Unassigned";
                        fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));

                    }
                    else {
                        Status = "Unassigned";
                        fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));

                    }
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
                else if (keyCode == 35) {
                    $('#divGrid').show();
                    //fncItemSearch();by velu 31/12/2018
                    fncItemSearchForGrid();

                    // $('#tblItemSearch').find('input:first').focus();

                    return false;
                }
                if (keyCode == 112) {//by velu 22022019
                    fncShowSearchDialogCommon(event, 'Inventory', 'txtInventoryCode', '');
                    event.preventDefault();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //----------------------------------------------------------------------------------------//



        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $("#<%=txtInventoryCode.ClientID %>").val(Code);
                    $('#<%=txtInventoryCode.ClientID%>').attr("readonly", "readonly");
                    fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    return false;
                }
                else if (SearchTableName == "Vendor") {

                    $('#<%=txtVendor.ClientID %>').val(Description);
                    $('#<%=hidvendorcode.ClientID %>').val(Code);
                }
             <%--   else if (SearchTableName == "Serial") {

                    $('#<%=txtRemarks.ClientID %>').val(Description);
                    
                }--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get BatchStatus
        function fncInventoryCodeEnterkey_Master() {
            try {
                $('#<%=hidBatchNo.ClientID%>').val('');
                fncGetBatchStatus_Master($('#<%=txtInventoryCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        //Get Inventory Detail
        function fncGetBatchStatus() {
            try {

                fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        //Assign values to text box
        var Status = "Unassigned";  //sankar
        function fncAssignInventoryDetailToControls_Master(inventorydata) {
            try {
                debugger
                //alert(inventorydata[9]);
                $('#<%=txtDescription.ClientID%>').val(inventorydata[0]);
                $('#<%=hidvendorcode.ClientID %>').val(inventorydata[9]);
                $('#<%=txtVendor.ClientID %>').val(inventorydata[10]);
                $('#<%=txtBefore.ClientID %>').val(inventorydata[12]);
                getdate();


                if ($('#<%=hidisbatch.ClientID%>').val() == "1" && Status == "Unassigned") {
                    fncGetBatchDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());

                }
                else {
                    $('#<%=txtPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtMRP.ClientID%>').val(inventorydata[6]);
                }
                $('#<%=txtQty.ClientID%>').focus().select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost) {
            try {
       //  $('#<%=txtSize.ClientID%>').val(batchno);
                $('#<%=hidBatchNo.ClientID%>').val(batchno);
                $('#<%=txtMRP.ClientID%>').val(mrp);
                $('#<%=txtPrice.ClientID%>').val(sprice);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Clear All Controls
        function fncClearAll() {
            try {
                fncClear();
                // fncSubClear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }

        //Clear All controls
        function fncClear() {
            try {
                $('#<%=hidvendorcode.ClientID%>').val('');
                $('#<%=txtVendor.ClientID%>').val('');
                $('#<%=txtInventoryCode.ClientID%>').val('');
                $('#<%=txtDescription.ClientID%>').val('');
                $('#<%=txtPrice.ClientID%>').val('');
                $('#<%=txtMRP.ClientID%>').val('');
                $('#<%=txtDiscount.ClientID%>').val('');
                $('#<%=txtRemarks.ClientID%>').val('');
                $('#<%=txtQty.ClientID%>').val('0');
                $('#<%=txtBefore.ClientID%>').val('0')
                $('#<%=txtInventoryCode.ClientID%>').removeAttr("readonly");
                $('#<%=txtInventoryCode.ClientID%>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Apply initial values to control
        function fncSubClear() {
            try {

                $('#<%=ddlTemplate.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlTemplate.ClientID %>').trigger("liszt:updated");
                $("#<%= txtPkdData.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $('#<%=txtSize.ClientID %>').val('');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Show Popup After Save
        function fncShowSuccessMessage() {
            try {
                //InitializeDialog();
                //$("#barcodeprinting").dialog('open');
                fncToastInformation("Details Senting to Printer");
                fncClearAll();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#barcodeprinting").dialog({
                    title: "Barcode",
                    autoOpen: false,
                    resizable: false,
                    height: 130,
                    width: 300,
                    modal: true
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Close Success Dialog
        function fncCloseSaveDialog() {
            try {
                $("#barcodeprinting").dialog('close');
                fncClear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }


        //        }
        //        //Serial No Click
        //        function fncSNoClick() {
        //            fncDisableEnableSNoSupplier();
        //        }

        //Assign Cross value to text box
        function fncTemplateChange() {
            try {
                var value;
                value = $('#<%=ddlTemplate.ClientID %>').val();
                if (value != null)
                $('#<%=txtSize.ClientID %>').val(value.split('#')[1]);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncTemplateChanges() {
            try {
                if ($('#<%=hidImagebarcode.ClientID %>').val() == "N") {
                    fncTemplateChange();
                    return false;
                }
                var value;
                value = $('#<%=ddlTemplate.ClientID %>').val();

                if (value != null)
                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("Barcode.aspx/GetSelectBarcodeDetails")%>',
                        data: "{ 'Code': '" + value + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            debugger

                            $('#<%=txtSize.ClientID %>').val(data.d[0]);
                            $('#<%=txtSize.ClientID %>').attr('readonly', 'true');
                        }
                    });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Print Validation
        function fncPrintValidation() {
            try {
                //ddlTemplate
               // $('#<%=lnkbtnPrint.ClientID %>').css("display", "none");

                <%--if ($('#<%=txtSize.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    $('#<%=lnkbtnPrint.ClientID %>').css("display", "block");
                    return false;
                }--%>
                  if ($('#<%=txtInventoryCode.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Inventorycode%>');
                    $('#<%=lnkbtnPrint.ClientID %>').css("display", "block");
                    return false;
                }
                else if (parseInt($('#<%=txtQty.ClientID %>').val()) == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_Nolable%>');
                    $('#<%=lnkbtnPrint.ClientID %>').css("display", "block");
                    return false;
                } 
                else if ($('#<%=ddlPrinterName.ClientID %> option:selected').text() == "" || $('#<%=ddlPrinterName.ClientID %> option:selected').text() == "-- Select --") {
                    ShowPopupMessageBox("Please Enter Printer Name");
                    $('#<%=lnkbtnPrint.ClientID %>').css("display", "block");
                    return false;
                  }
                  else if ($('#<%=ddlTemplate.ClientID %> option:selected').text() == "" || $('#<%=ddlTemplate.ClientID %> option:selected').text() == "-- Select --")
                  {
                      ShowPopupMessageBox("Please select template Name");
                      $('#<%=lnkbtnPrint.ClientID %>').css("display", "block");
                      return false;
                  }
                else {
                    // set the item in localStorage 
                    localStorage.setItem('Template', $('#<%=ddlTemplate.ClientID %>').val());
                    if ($('#<%=hidImagebarcode.ClientID %>').val() == "N")
                        return true;
                    else {
                     $(".container_divlist2").empty(); 
                        var PrinterStatus = "<%=ConfigurationManager.AppSettings["PrinterStatus"].ToString() %>";
                        fncImageBarcodeClick(PrinterStatus);
                       // $("#secandone1").html("");
                        fncClear();
                        return false;
                    } 
                } 
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        } 
        ///Image Barcode Region  Vijay 20201227
        function fncImageBarcodeClick(PrinterStatus) {
    try {
        
        var txtPkdData = $('#<%=txtPkdData.ClientID %>').val().replaceAll('/', '/'); //+ "T18:37:47";
        var txtExpiredate = $('#<%=txtExpiredate.ClientID %>').val().replaceAll('/', '/'); //+ "T18:37:47";
        var ddl = $('#<%=ddlTemplate.ClientID %> option:selected').text();
        var PrinterName = $('#<%=ddlPrinterName.ClientID %> option:selected').text();
        var List = [];
        var listnames = [], Name = [];
        var TemplateList = "";
        var objlist = {};
        var EANstatus = $("#<%=chkEAN.ClientID%>").is(':checked');
        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("Barcode.aspx/GetAllBarcodeDetails")%>',
            data: "{ 'Code': '" + $('#<%=txtInventoryCode.ClientID %>').val() + "','BatchNo':'" + $('#<%=hidBatchNo.ClientID%>').val() + "','Filename' :'" + ddl + "','NoLabels' :'" + $('#<%=txtQty.ClientID %>').val() + "','PkdDate' :'" + txtPkdData + "','Expdate' :'" + txtExpiredate + "','DateFormate' :'" + $('#<%=ddlDateformat.ClientID %>').val() + "','Serial':'" + $('#<%=txtRemarks.ClientID %>').val() + "','status':'" + EANstatus + "','ENO':'" + $('#<%=hidEANo.ClientID%>').val() + "'}",
       
       
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: function (data) {
                  
                  var HideMRP = $('#<%=cbhidemrp.ClientID%>')[0].checked ? 'Y' : 'N';
               var HidePrice = $('#<%=cbHideprice.ClientID%>')[0].checked ? 'Y' : 'N';
                var HidePkdDate = $('#<%=cbhidePkddate.ClientID%>')[0].checked ? 'Y' : 'N';
                var HideExpiredate = $('#<%=cbexpiredate.ClientID%>')[0].checked ? 'Y' : 'N';
                var HideCompany = $('#<%=cbhidecompany.ClientID%>')[0].checked ? 'Y' : 'N';
                for (var i = 0; i <= 12; i++) {
                    listnames.push(data.d[i]);
                }
                $.each(JSON.parse(JSON.stringify(data.d[13]))[0], function (index, value) {
                    Name.push(index);
                });
                List.push(data.d[13]);
                // }
                var ImageBarcodeDetails = data.d[14].split('|');
                TemplateList = ImageBarcodeDetails[2];
                  debugger
                var listimg = ObjectList(List, listnames, ddl, Name, TemplateList, HideMRP, HidePrice, HidePkdDate, HideExpiredate, HideCompany, ImageBarcodeDetails[9]);
                setTimeout(() => {
                    printerimagelist(listimg, ImageBarcodeDetails[5], ImageBarcodeDetails[6], ImageBarcodeDetails[4], ImageBarcodeDetails[1], ImageBarcodeDetails[8], PrinterStatus, PrinterName,"BC");
                }, 500);
                fncTemplateChanges();
            },
              error: function (data) {
                  ShowPopupMessageBox(response.message);
              },
              failure: function (data) {
                  ShowPopupMessageBox(response.message);
              }
          });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
function QR_ImageAndReplaceQRCode(values) {

    //var canvas = document.createElement('canvas');

    //canvas.id = "qr-code";
    //canvas.style.position = "absolute";
    //canvas.style.border = "1px solid";
    var base64 = [];
    var codecanvas = document.getElementById('qr-code'),
 qr = new QRious({
     element: codecanvas,
     size: 128,
     value: values
 });
    codecanvas.style.display = "none";
    dataUrl = codecanvas.toDataURL(),
         imageFoo = document.createElement('img');
    imageFoo.src = dataUrl;
    base64 = imageFoo.src;
    console.log(base64);
    return base64;
}
<%--var i = 1;
function ObjectList(List, listname, ddl, Name, TemplateList) {
    var imglist = [];
    if (List.length > 0) {
        jQuery('#header_div').html('');
        $.each(List[0], function (key, valus) {
            var jsondata = listofarray(listname, JSON.parse(TemplateList), valus.Barcode, valus.Inventorycode, valus.Description, valus.SellingPrice, valus.MRP, valus.Discount, valus.VendorCode, valus.VendorName, valus.PritingCount, valus.PKDDate, valus.EXPDate, valus.SNO, valus.CompanyName, Name, key);
            var ele = '#' + jsondata;
            html2canvas($(ele), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL("image/png", 1.0);
                    imglist.push({ base64: img, Tcount: valus.PritingCount, Divid: ele });
                }
            });
        });
    }
    return imglist;
}
function printerimagelist(imglist, CrossWidth, CrossHeight, PaperRoleWidth, FileName, CrossCount, PrinterStatus) {
    var _objlist = [];
    var data = [];
    var paperrole = PaperRoleWidth * 2;
    var elem = document.getElementById('firstone');
    elem.style.width = paperrole + "cm";
    var elem1 = document.getElementById('firstone1');
    elem1.style.width = paperrole + "cm";
    $.each(imglist, function (key, valus) {
        $(valus.Divid).remove();
        _objlist.push({ TemplateImg: valus.base64, CrossWidth: CrossWidth, CrossHeight: CrossHeight, PaperRoleWidth: PaperRoleWidth, FileName: FileName, CrossCount: CrossCount, TotalCount: valus.Tcount });
    });
    $.each(_objlist, function (key, datalist) {
        for (var i = 0; i < datalist.TotalCount; i++) {
            data.push({
                TemplateImg: datalist.TemplateImg,
                CrossWidth: datalist.CrossWidth,
                CrossHeight: datalist.CrossHeight,
                PaperRoleWidth: datalist.PaperRoleWidth,
                FileName: datalist.FileName
            });
        }
    });
    images = "";
    while (data.length > 0) {
        var div_ids = "";
        var div = document.createElement("DIV");
        div.id = "secandone" + i++;
        div.className = 'container_divlist2';
        document.getElementById("firstone1").appendChild(div);
        div_ids = div.id;
        var CrossCounts = parseInt(CrossCount)
        chunk = data.splice(0, CrossCounts);
        chunk.forEach(function (entry, index) {
            var oImg = document.createElement("img");
            var height = entry.CrossHeight * 2;
            var width = entry.CrossWidth * 2;
            oImg.setAttribute('src', entry.TemplateImg);
            oImg.className = 'fake_img';
            oImg.id = 'Id_Imag'
            oImg.style.height = height + "cm";
            oImg.style.width = width + "cm";
            oImg.style["margin-top"] = "-15px";
            document.getElementById(div_ids).appendChild(oImg);
        });
        printer(div_ids, PrinterStatus);
    }
}
function listofarray(listname, object_Templates, Barcode, Inventorycode, Description, SellingPrice, MRP, Discount, VendorCode, VendorName, PritingCount, PKDDate, EXPDate, SNo, Company, listout, idcount) {

    canvas_id = "";
    div_id = "";
    jsondata = object_Templates;
    $.each(listout, function (key, values) {
        $.each(listname, function (key, names) {
            if (names == values) {
                var locallist = '#' + names;
                if (key == 0) {
                    findTextAndReplace(jsondata, locallist, PritingCount.replace(/\s/g, ''));
                }
                else if (key == 1) {
                   
                    var qrcode = ImageAndReplaceQRCode(Barcode.replace(/\s/g, ''));                                     
                    findTextAndReplace(jsondata, locallist, Barcode.replace(/\s/g, ''));
                   findImageAndReplace(jsondata, imagebase64list, ImageAndReplace(Barcode.replace(/\s/g, '')));
                    //findImageQRAndReplace(jsondata, imagebase64list, ImageAndReplaceQRCode(Barcode.replace(/\s/g, '')));
                }
                else if (key == 2) {
                    findTextAndReplace(jsondata, locallist, Inventorycode.replace(/\s/g, ''));
                }
                else if (key == 3) {
                    findTextAndReplace(jsondata, locallist, Description.replace(/\s/g, ''));
                }
                else if (key == 4) {
                    findTextAndReplace(jsondata, locallist, SellingPrice.replace(/\s/g, ''));
                }
                else if (key == 5) {
                    findTextAndReplace(jsondata, locallist, MRP.replace(/\s/g, ''));
                }
                else if (key == 6) {
                    findTextAndReplace(jsondata, locallist, PKDDate.replace(/\s/g, ''));
                }
                else if (key == 7) {
                    findTextAndReplace(jsondata, locallist, EXPDate.replace(/\s/g, ''));
                }
                else if (key == 8) {
                    findTextAndReplace(jsondata, locallist, Discount.replace(/\s/g, ''));
                }
                else if (key == 9) {
                    findTextAndReplace(jsondata, locallist, Company.replace(/\s/g, ''));
                }
                else if (key == 10) {
                    findTextAndReplace(jsondata, locallist, VendorCode.replace(/\s/g, ''));
                }
                else if (key == 11) {
                    findTextAndReplace(jsondata, locallist, VendorName.replace(/\s/g, ''));
                }
                else if (key == 12) {
                    findTextAndReplace(jsondata, locallist, SNo.replace(/\s/g, ''));
                }
            }
        });
    });
    var Div = document.createElement('div');
    Div.id = 'container-first' + idcount;
    Div.className = 'canvas-containerlist';
    document.getElementsByTagName('body')[0].appendChild(Div);
    div_id = 'container-first' + idcount;
    var canvasnew = document.createElement('canvas');
    canvasnew.id = "c" + idcount;
    canvasnew.width = 730;
    canvasnew.height = 480;
    document.getElementById(div_id).appendChild(canvasnew);
    canvas_id = "c" + idcount;
    var canvas = new fabric.Canvas(canvas_id);
    var jsonCanvas = JSON.stringify(jsondata);
    canvas.loadFromJSON(jsonCanvas, canvas.renderAll.bind(canvas));
    document.getElementById("header_div").appendChild(Div);
    return div_id;
}
function printer(div_ids, PrinterStatus) {
    var urls = "";
   // var PrinterStatus = "<%=ConfigurationManager.AppSettings["PrinterStatus"].ToString() %>";
    if (PrinterStatus == "PS_Local") {
        urls = '<%=ResolveUrl("Barcode.aspx/GetPrintBarcodeDetails")%>';
    }
    else if (PrinterStatus == "PS_Server") {
        urls = '<%=ResolveUrl("Barcode.aspx/SaveBarcodePrint")%>';
    }

    var elements = "#" + div_ids;
    html2canvas($(elements), {
        onrendered: function (canvas) {
            var img = canvas.toDataURL("image/png", 1.0);
            var base64result = img.split(',')[1];
            var jobName = "label";
            var zpl = $('#<%=ddlPrinterName.ClientID %> option:selected').text();
            $.ajax({
                type: "POST",
                url: urls,
                data: "{ 'printerName': '" + zpl + "' , 'base64Image' : '" + base64result + "', 'jobName' : '" + jobName + "'}",
                async: false,
                proccessData: false,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                cache: false,
                success: function (data) {
                    if (data.d == true) {
                        var id = "#" + div_ids;
                        $("#" + div_ids).remove();
                    }
                },
                error: function (jqXHR, exception) {
                    getErrorMessage(jqXHR, exception);
                }
            });
        }
    });
}
function findImageAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findImageAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            var obj_value = object["src"];
            if (obj_value != undefined) {
                const type = obj_value.split(';')[0].split('/')[1];
                if (type == "png") {
                    object["src"] = replacevalue;
                }
            }
        }
    }
}
function findImageQRAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findImageQRAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            object["src"] = replacevalue;
        }
    }
}
function findTextAndReplace(object, value, replacevalue) {
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findTextAndReplace(object[x], value, replacevalue);
        }
        if (object[x] == value) {
            object["text"] = replacevalue;
        }
    }
}
function ImageAndReplace(value) {
    var iDiv = document.createElement('div');
    iDiv.id = 'block';
    iDiv.className = 'block';
    iDiv.style = "display:none";
    document.getElementsByTagName('body')[0].appendChild(iDiv);
    var base64 = []
    var img = document.createElement("img");
    img.id = "rlp_imgbarcode"
    img.style = "display:none";
    document.getElementById("block").appendChild(img);
    JsBarcode("#rlp_imgbarcode", value, { format: "CODE128", lineColor: "#000000", width: 2, height: 50, fontSize: 75, displayValue: false, fontWeight: 'bold', background: "#FFFFFF" });
    base64 = $('#rlp_imgbarcode')[0].src;
    $('#rlp_imgbarcode').remove();
    return base64;
}--%>
        ///Image Barcode End
function ImageAndReplaceQRCode(value) {

    var qrbase64 = [];
   $.ajax({
        type: "POST",
        url: '<%=ResolveUrl("Barcode.aspx/GetQRCodeDetails")%>',
        data: "{ 'qrCode': '" + value + "'}",
        contentType: "application/json; charset=utf-8",       
        async: false,
        proccessData: false,
        dataType: "json",
        success: function (data) {           
            qrbase64 = data.d[0];
           
            return qrbase64;
        },
        error: function (jqXHR, exception) {
            getErrorMessage(jqXHR, exception);
        }
    });

  
    //var iDiv = document.createElement("div");
    //iDiv.id = "qrcode";
    //iDiv.className = 'block';
   
    //document.getElementsByTagName('body')[0].appendChild(iDiv);
    //var qrcode = new QRCode(document.getElementById("qrcode"), {
    //    text: value,
    //    width: 128,
    //    height: 128,
    //    colorDark: "#000000",
    //    colorLight: "#ffffff",
    //    correctLevel: QRCode.CorrectLevel.H
    //});
    //base64 = $("#qrcode img")[0].src;
    //iDiv.style = "display:none";
    //return base64;
}

        ///Inventory Search
        function SetAutoComplete() {
            $("[id$=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));

                    event.preventDefault();
                },
                select: function (e, i) {
                    //alert(i.item.val);

                    $('#<%=txtInventoryCode.ClientID%>').attr("readonly", "readonly");
                    $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));

                    var inputNumber = $('#<%=txtInventoryCode.ClientID %>').val();
                    var regexp = /^[0-9]*$/;
                    var numberCheck = regexp.exec(inputNumber);

                    if (numberCheck != null && $('#<%=txtInventoryCode.ClientID %>').val().length == 6) {
                        $('#<%=txtInventoryCode.ClientID%>').attr("readonly", "readonly");
                fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    }

                    return false;
                },
                minLength: 1
            });
        }

        function fncDailogItemSearch(event) {
            try {

                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;

                if (keyCode == 13) {

                    return false;
                }
                else if (keyCode == 40) {
                    if ($("#tblItemSearch tbody").children().length > 0) {
                        $("#tblItemSearch tbody > tr").first().focus();
                    }
                    return false;
                }
                else if (keyCode == 35) {
                    fncItemSearchForGrid();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }
        function fncItemSearchForGrid() {

            try {

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("Barcode.aspx/fncItemSearchForGrid")%>',
                    data: "{ 'Code': '" + $("#<%=txtInventoryCode.ClientID%>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncItemBindtoTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
                return false;
            }
            catch (err) {
                fncToastError(err.message);
            }

        }
        function fncItemBindtoTable(msg) {
            try {
                var objSearchData, tblSearchData, Rowno = 0;
                objSearchData = jQuery.parseJSON(msg.d);

                tblSearchData = $("#tblItemSearch tbody");
                tblSearchData.children().remove();

                if (objSearchData.length > 0) {
                    for (var i = 0; i < objSearchData.length; i++) {

                        row = "<tr tabindex = '" + i + "' onfocus = 'return fncMouseSearchesFocus(this);' onkeydown = 'return fncSearchTableKeyDowns(event, this);'>"
                            + "<td id='tdRowNo'>  " + $.trim(objSearchData[i]["SNo"]) + "</td>" +
                            "<td id='tdItemCode' > " + $.trim(objSearchData[i]["InventoryCOde"]) + "</td>" +
                            "<td id='tdBatchNo' > " + $.trim(objSearchData[i]["BatchNo"]) + "</td>" +
                            "<td id='tdDes'>" + $.trim(objSearchData[i]["Description"]) + "</td>" +
                            "<td id='tdBatchQty' style ='text-align:right !important'>" + $.trim(objSearchData[i]["BatchQty"]) + "</td>" +
                            "<td id='tdPrice' style ='text-align:right !important'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                            "<td id='tdRemarks' style ='display:none'>" + $.trim(objSearchData[i]["Remarks"]) + "</td>" +
                            "<td id='tdVendorCode' style ='display:none'>" + $.trim(objSearchData[i]["VendorCode"]) + "</td>" +
                            "<td id='tdCashDiscount'style ='display:none'>" + $.trim(objSearchData[i]["CashDiscount"]) + "</td>" +
                            "<td id='tdVendorName'style ='display:none'>" + $.trim(objSearchData[i]["VendorName"]) + "</td>" +
                            //"<td id='tdPrice'>" + $.trim(objSearchData[i]["SellingPrice"]) + "</td>" +
                            "<td id='tdMrp' style ='width:9%;text-align:right !important;'>" + $.trim(objSearchData[i]["MRP"]) + "</td></tr>";
                        tblSearchData.append(row);
                    }




                    //var scroll = tblSearchData[0].scrollHeight;
                    //tblSearchData.scrollTop(scroll);

                    tblSearchData.children().dblclick(fncSearchRowDoubleClick);
                    tblSearchData[0].setAttribute("style", "cursor:pointer");

                    tblSearchData.children().on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });

                    tblSearchData.children().on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });


                    $("tr[tabindex=0]").focus();

                    //$('table tr#tblItemSearch').find('input:first').focus();
                    //tblSearchData.children().on('keydown', function () {
                    //    fncBatchKeyDown($(this), event);
                    //});
                    //$("#tblItemSearch").on("keypress", function (e) {
                    //    var keyCode = (e.keyCode ? e.keyCode : e.which);
                    //    alert(keyCode);
                    //});

                    //$("#tblItemSearch tbody > tr").first().css("background-color", "Yellow");
                    //$("#tblItemSearch tbody > tr").first().focus();
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function fncMouseSearchesFocus(source) {
            try {
                var rowobj = $(source);
                rowobj.css("background-color", "#ADD8E6");
                rowobj.siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err);
            }
        }

        function fncSearchTableKeyDowns(evt, source) {
            try {
                var rowobj = $(source);
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                //Enter Key
                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }

                //Down Key
                else if (charCode == 40) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "#ADD8E6");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();
                        $('#<%=txtInventoryCode.ClientID %>').val($(NextRowobj).find("td").eq(3).html().trim());
                    }
                    return false;
                }
                //Up Key
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "#ADD8E6");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();
                        $('#<%=txtInventoryCode.ClientID %>').val($(prevrowobj).find("td").eq(3).html().trim());
                    }
                    else {
                        prevrowobj.css("background-color", "");
                        prevrowobj.siblings().css("background-color", "");
                        $("#search").focus();
                    }
                    return false;
                }
                return false;
            }
            catch (err) {
                fncToastError(err);
            }
        }

        function fncBatchKeyDown(rowobj, evt) { 
            //  alert('test');
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblItemSearch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblItemSearch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblItemSearch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblItemSearch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblItemSearch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSearchRowDoubleClick() {
            try {
                Itemcode = $.trim($(this).find("td").eq(1).text());
                $('#<%=txtInventoryCode.ClientID%>').val($.trim($(this).find("td").eq(1).text()));
                $('#<%=hidBatchNo.ClientID%>').val($.trim($(this).find("td").eq(2).text()));   //sankar      
                $('#<%=txtDescription.ClientID%>').val($.trim($(this).find('td[id*=tdDes]').text()));
                des = $.trim($(this).find('td[id*=tdPrice]').text());
                $('#<%=txtPrice.ClientID%>').val($.trim($(this).find('td[id*=tdPrice]').text()));
                $('#<%=txtMRP.ClientID%>').val($.trim($(this).find('td[id*=tdMrp]').text()));
                $('#<%=txtRemarks.ClientID%>').val($.trim($(this).find("td").eq(6).text()));
                $('#<%=txtVendor.ClientID%>').val($.trim($(this).find("td").eq(9).text()));
                $('#<%=txtDiscount.ClientID%>').val($.trim($(this).find("td").eq(8).text()));
                // $(this).parents("tr").remove();
                var tblSearchData = $("#tblItemSearch tbody");
                tblSearchData.children().remove();
                $('#divGrid').hide();
                $('#<%=txtQty.ClientID%>').focus();
                $('#<%=txtInventoryCode.ClientID%>').attr("readonly", "readonly");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        $(document).ready(function () {//velu 25022019
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            try {
                var charCode = (e.which) ? e.which : e.keyCode;
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (e.keyCode == 122) {
                    <%--$("#<%=lnkbtnPrint.ClientID%>").click();--%>
                    if ($('#<%=lnkbtnPrint.ClientID %>').is(":visible")) {
                        $('#<%=lnkbtnPrint.ClientID %>').attr("disabled", "disabled");
                        if (fncPrintValidation() == true)
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkbtnPrint', '');
                    }
                    //__doPostBack('ctl00$ContentPlaceHolder1$lnkbtnPrint', '');

                    e.preventDefault();
                }
                if (e.keyCode == 117) {
                    $("#<%=lnkbtnClear.ClientID%>").click();
                    e.preventDefault();
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        $(document).ready(function () {
            $("[id$=txtBefore]").bind('change', function () {
                debugger
                if (this.value.match(/[^a-zA-Z0-9-,]/g)) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBefore.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(/[^a-zA-Z0-9-,]/g, '');
                }
            });
            $(" #vendorNametooltip ").mouseenter(function () {
                var word = $('#<%=txtVendor.ClientID%>').val();
                $(" #vendorNametooltip ").attr('title', word);
            });
        });
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'Barcode');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "Barcode";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
            <li><a style="text-decoration: none;">Barcode</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">

                <%=Resources.LabelCaption.Home_Barcode%></li>
            <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="centerposition_barcode">
        <div class="barcodesplit_left">
            <div class="barcode_header">
                <asp:Label ID="lblitemdetail" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDetail %>'></asp:Label>
            </div>
            <div class="barcode_leftborder">
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblInventorycode" runat="server" Text='<%$ Resources:LabelCaption,lblInventoryCode %>'></asp:Label>
                        <asp:CheckBox ID="chkEAN" runat="server"/>
                    </div>
                    <div style="float: right">
                        
                        <asp:TextBox ID="txtInventoryCode" runat="server" onkeydown="return fncGetInventoryCode(event)"
                            CssClass="form-control"></asp:TextBox>
                        

                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"
                            onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_price %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" onkeydown="return false"
                            onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div id="vendorNametooltip" class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblVendor" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control" onkeydown="return false"
                            onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblDiscount" runat="server" Text='<%$ Resources:LabelCaption,lblDiscount %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control" onkeydown="return false"
                            onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblRemarks" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="lblLables" runat="server" Text='<%$ Resources:LabelCaption,lbl_NoOfLabels %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" onkeypress="return isNumberKey(event)"
                            oncopy="return false" onpaste="return false" oncut="return false">0</asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="tblBefore" runat="server" Text=' Best Before Days'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtBefore" runat="server" CssClass="form-control" onkeydown="return isNumberKeyWithDecimalNew(event)"
                            onblur="getdate();return false;">0</asp:TextBox>
                    </div>
                </div>
              <%--     <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label3" runat="server" Text=' SerialNo'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtSerial" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Serial', 'txtSerial', '');"></asp:TextBox>
                    </div>
                </div>--%>
            
            </div>
        </div>
        <div class="barcodesplit_right">
            <div class="barcode_header">
                <asp:Label ID="lblbarcodeoption" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcodeoption %>'></asp:Label>
            </div>
            <div class="barcode_rightborder">
                <%--<div class="control-groupbarcode">
                        <div style="text-align: center">--%>
                <%-- <div>
                                <div style="float: left">
                                    <asp:RadioButton ID="rbnDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'
                                        GroupName="barcode" Checked="True" onclick="fncDisableEnableSNoSupplier()" />
                                </div>
                                <div style="float: left; margin-left: 5%">
                                    <asp:RadioButton ID="rbnMonth" runat="server" Text='<%$ Resources:LabelCaption,lbl_Month %>'
                                        GroupName="barcode" onclick="fncDisableEnableSNoSupplier()" />
                                </div>
                                <div style="float: left; margin-left: 5%">
                                    <asp:RadioButton ID="rbnSNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_SNo %>'
                                        GroupName="barcode" onclick="fncDisableEnableSNoSupplier()" />
                                </div>
                                <div style="float: left; margin-left: 5%">
                                    <asp:TextBox ID="txtSNo" runat="server" CssClass="form-control" Style="width: 50px"
                                        Visible="true" Enabled="False"></asp:TextBox>
                                </div>
                                <div style="float: left; margin-left: 5%">
                                    <asp:RadioButton ID="rbnSupplier" runat="server" Text='<%$ Resources:LabelCaption,lbl_Supplier %>'
                                        GroupName="barcode" onclick="fncDisableEnableSNoSupplier()" />
                                </div>
                                <div style="float: left; margin-left: 5%">
                                    <asp:TextBox ID="txtSupp" runat="server" CssClass="form-control" Style="width: 50px"
                                        Visible="true" Enabled="False"></asp:TextBox>
                                </div>
                            </div>--%>
                <%-- </div>
                    </div>--%>
                <div class="barcode_PkdData">
                    <div class="barcode_date">
                        <%--<div style="float: left">
                                <asp:RadioButton ID="rbnMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'
                                    GroupName="Price" />
                            </div>
                            <div style="float: left; margin-left: 5%; width: 115px;">
                                <asp:RadioButton ID="rbnSprice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellingPrice %>'
                                    GroupName="Price" Checked="True" />
                            </div>--%>
                        <div class="barcode_date_left">
                            <asp:Label ID="lblPKDDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_PKDDate %>'></asp:Label>
                        </div>
                        <div class="barcode_date_right">
                            <asp:TextBox ID="txtPkdData" runat="server" onchange="getdate();return false;" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="barcode_date">
                        <div class="barcode_date_left">
                            <asp:Label ID="lblExpireDate" runat="server" Text='<%$ Resources:LabelCaption,lblExpiredDate %>'></asp:Label>
                        </div>
                        <div class="barcode_date_right">
                            <asp:TextBox ID="txtExpiredate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="barcode_date">
                        <div class="barcode_date_left1">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblDataformat %>'></asp:Label>
                        </div>
                        <div class="barcode_date_left2">
                            <asp:DropDownList ID="ddlDateformat" runat="server" Style="width: 100%">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="control-groupbarcode">
                    <div class="barcode_checkbox_hight">
                        <div class="barcode_checkbox1">
                            <asp:CheckBox ID="cbhidemrp" runat="server" />
                            <%=Resources.LabelCaption.lblhidemrp%>
                        </div>
                        <div class="barcode_checkbox1">
                            <asp:CheckBox ID="cbHideprice" runat="server" />
                            <%=Resources.LabelCaption.lblHideprice%>
                        </div>
                        <div class="barcode_checkbox2">
                            <asp:CheckBox ID="cbhidePkddate" runat="server" />
                            <%=Resources.LabelCaption.lblHidePkddate%>
                        </div>
                        <div class="barcode_checkbox">
                            <asp:CheckBox ID="cbexpiredate" runat="server" />
                            Hide Exp.Date
                        </div>
                        <div class="barcode_checkbox">
                            <asp:CheckBox ID="cbhidecompany" runat="server" />
                            <%=Resources.LabelCaption.lblHideCompany%>
                        </div>
                    </div>
                    <div class="barcode_template">
                        <div>
                            <div style="float: left">
                                <asp:Label ID="lblTemplate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Template %>'></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 5%">
                                <asp:DropDownList ID="ddlTemplate" runat="server" Style="width: 100px" onchange="fncTemplateChanges()">
                                </asp:DropDownList>
                            </div>
                            <div style="float: left; margin-left: 5%">
                                <asp:Label ID="lblSize" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 5%">
                                <asp:TextBox ID="txtSize" runat="server" CssClass="form-control" Style="width: 125px"
                                    onMouseDown="return false"></asp:TextBox>
                            </div>
                            <div style="float: left; margin-left: 5%">
                                <asp:Label ID="Label2" runat="server" Text="PrinterName"></asp:Label>
                            </div>
                            <div style="float: right;">
                                <asp:DropDownList ID="ddlPrinterName" runat="server" Style="width: 110px">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    
                    <div class="barcode_button" style="width: 45%;">
                        <div class="gid-control">
                            <asp:LinkButton ID="lnkbtnPrint" runat="server" class="button-blue" OnClick="lnkbtnPrint_Click"
                                OnClientClick="return fncPrintValidation()" Text="Print(F11)"></asp:LinkButton>
                        </div>
                        <div class="gid-control" style="margin-left: 5%">
                            <asp:LinkButton ID="lnkbtnClear" runat="server" class="button-blue" OnClientClick="return fncClearAll()"
                                Text="ClearAll(F6)"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <canvas id="qr-code"></canvas>
    </div>
      <div id="firstone" class="container_divlist1">
        <div id="firstone1" class="container_divlist1">
        </div>
    </div>
    <div id="header_div"></div>
    <div id="divGrid" class="display_none">
        <div id="divItemSearch " class="tbl_left" style="margin-left: 65px;">
            <%--<div class="float_left">
            <asp:Label ID="lblItemSearch" runat="server" Text="Search"></asp:Label>
        </div>--%>
            <%-- <div class="gidItem_Search">
            <asp:TextBox ID="txtItemSearch" onkeydown=" return fncDailogItemSearch(event);" runat="server"></asp:TextBox>
        </div>--%>
            <div class="Payment_fixed_headers gid_Itemsearch">
                <table id="tblItemSearch" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Batch No 
                            </th>
                            <th scope="col">Item Description
                            </th>
                            <th scope="col">Qty
                            </th>
                            <th scope="col">Selling Price
                            </th>
                            <th scope="col">MRP
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="dialog-Transferout">
        </div>
    </div>
    <div class="container-group-full">
        <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
        <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
        <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        <asp:HiddenField ID="hidBatchNo" runat="server" Value="" />
        <asp:HiddenField ID="hidExpiredDate" runat="server" Value="" />
        <div id="barcodeprinting" class="barcodesavedialog">
            <p>
                <%=Resources.LabelCaption.Save_Barcodeprinting%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>

        </div>
        <div class="display_none">
            <asp:Button ID="btnGidLoad" runat="server" OnClick="btnGidLoad_click" />
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidImagebarcode" runat="server" />
    <asp:HiddenField ID="hidEAN" runat="server" />
    <asp:HiddenField ID="hidEANo" runat="server" />
</asp:Content>
