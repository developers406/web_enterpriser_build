﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ImageBarcodeMaster.master" AutoEventWireup="true" CodeBehind="frmImageBarcodeTemplate.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmImageBarcodeTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K8-C8QSw.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3K9-C8QSw.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Anton';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/anton/v15/1Ptgg87LROyAm3Kz-C8.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwaPGR_p.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 700;
            src: url(https://fonts.gstatic.com/s/lato/v17/S6u9w4BMUTPHh6UVSwiPGQ.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnruxElg4M.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Hammersmith One';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/hammersmithone/v12/qWcyB624q4L_C4jGQ9IK0O_dFlnrtREl.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }



        /* cyrillic-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SkvzAbt.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SAvzAbt.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* greek-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SgvzAbt.woff2) format('woff2');
            unicode-range: U+1F00-1FFF;
        }
        /* greek */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_ScvzAbt.woff2) format('woff2');
            unicode-range: U+0370-03FF;
        }
        /* hebrew */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SYvzAbt.woff2) format('woff2');
            unicode-range: U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SsvzAbt.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SovzAbt.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cousine';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cousine/v16/d6lIkaiiRdih4SpP_SQvzA.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7mlx17r.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7ilx17r.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cabin';
            font-style: normal;
            font-weight: 400;
            font-stretch: 100%;
            src: url(https://fonts.gstatic.com/s/cabin/v18/u-4X0qWljRw-PfU81xCKCpdpbgZJl6XFpfEd7eA9BIxxkV2EH7alxw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Candal';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/candal/v10/XoHn2YH6T7-t_8c9BhQI.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* latin */
        @font-face {
            font-family: 'Copse';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/copse/v10/11hPGpDKz1rGb3dkFEk.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Orbitron';
            font-style: normal;
            font-weight: 500;
            src: url(https://fonts.gstatic.com/s/orbitron/v17/yMJMMIlzdpvBhQQL_SC3X9yhF25-T1ny_CmBoWgz.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* latin */
        @font-face {
            font-family: 'Crimson Text';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/crimsontext/v11/wlp2gwHKFkZgtmSR3NB0oRJfbwhT.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        /* cyrillic-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjbYJwQj.woff2) format('woff2');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }
        /* cyrillic */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6Vj_YJwQj.woff2) format('woff2');
            unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }
        /* vietnamese */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjTYJwQj.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjXYJwQj.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Cuprum';
            font-style: normal;
            font-weight: 400;
            src: url(https://fonts.gstatic.com/s/cuprum/v14/dg45_pLmvrkcOkBnKsOzXyGWTBcmg-X6VjvYJw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }



        body {
            background-color: #777 !important;
        }

        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

            .dropbtn:hover, .dropbtn:focus {
                background-color: #3e8e41;
            }

        #myInput {
            box-sizing: border-box;
            background-image: url('searchicon.png');
            background-position: 14px 12px;
            background-repeat: no-repeat;
            font-size: 16px;
            padding: 14px 20px 12px 45px;
            border: none;
            border-bottom: 1px solid #ddd;
        }

            #myInput:focus {
                outline: 3px solid #ddd;
            }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f6f6f6;
            min-width: 230px;
            overflow: auto;
            border: 1px solid #ddd;
            z-index: 1;
        }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

        .dropdown a:hover {
            background-color: #ddd;
        }

        .show {
            display: block;
        }

        .modals {
            overflow-y: unset !important;
            position: fixed !important;
            top: 0px !important;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1040 !important;
            display: none;
            overflow: auto !important;
            margin-left: -547px !important;
        }

        .win8-notif-button {
            min-width: 10%;
            display: block;
            margin-top: -3%;
            margin-left: 2%;
            float: right;
            font-weight: bold;
            margin-bottom: 2%;
        }
        /*--thank you pop ends here--*/
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script type="text/javascript">
        var imagebase64 = "";
        var clipboard = null;
        var Barcodestatus = "";
        $(function () {
            ddlSave();
            debugger
            $("#ddlsavelist").select2();
            var numberFields = document.querySelectorAll("input[type=number]"),

                len = numberFields.length,
                numberField = null;
            for (var i = 0; i < len; i++) {
                numberField = numberFields[i];
                numberField.onclick = function () {
                    this.setAttribute("step", ".1");
                };
                numberField.onkeyup = function (e) {
                    if (e.keyCode === 38 || e.keyCode === 40) {
                        this.setAttribute("step", ".01");
                    }
                };
            }
            $('.page-alert .close').click(function (e) {
                e.preventDefault();
                $(this).closest('.page-alert').slideUp();
            });
            var canvas = new fabric.Canvas('c', {
                selection: false
            });

            var gridGroup;
            var grid = 50;
            function addGrid() {
                if (gridGroup) return;
                var lines = [];
                for (var i = 0; i < (750 / grid); i++) {
                    lines.push(new fabric.Line([i * grid, 0, i * grid, 750], { stroke: '#ccc', selectable: false }));
                    lines.push(new fabric.Line([0, i * grid, 750, i * grid], { stroke: '#ccc', selectable: false }));
                }
                gridGroup = new fabric.Group(lines, {
                    selectable: false,
                    evented: false,
                    hoverCursor: 'auto',
                    left: -2,
                    top: -2,
                });
                gridGroup.addWithUpdate();
                canvas.add(gridGroup);
            }

            /* Hide/Show Grid */
            $(document).on('change', '#show_grid', function () {
                var grid = $('#show_grid')[0].checked ? 'Y' : 'N';
                if (grid == 'Y') {
                    addGrid();
                }
                else {
                    gridGroup && canvas.remove(gridGroup);
                    gridGroup = null;
                }
            });

            canvas.on('object:moving', function (options) {
                if (Math.round(options.target.left / grid * 2) % 2 == 0 &&
                    Math.round(options.target.top / grid * 2) % 2 == 0) {
                    options.target.set({
                        left: Math.round(options.target.left / grid) * grid,
                        top: Math.round(options.target.top / grid) * grid
                    }).setCoords();
                }
            });

            var map = { 17: false, 46: false, 67: false, 86: false };
            var copiedObject;
            var copiedObjects = new Array();
            $(document).keydown(function (e) {
                if (e.keyCode in map) {
                    map[e.keyCode] = true;
                    if (map[17] && map[67]) {
                        event.preventDefault();
                        if (canvas.getActiveGroup()) {
                            for (var i in canvas.getActiveGroup().objects) {
                                var object = fabric.util.object.clone(canvas.getActiveGroup().objects[i]);
                                object.set("top", object.top + 40);
                                object.set("left", object.left + 40);
                                copiedObjects[i] = object;
                            }
                        }
                        else if (canvas.getActiveObject()) {
                            var object = fabric.util.object.clone(canvas.getActiveObject());
                            object.set("top", object.top + 40);
                            object.set("left", object.left + 40);
                            copiedObject = object;
                            copiedObjects = new Array();
                        }
                    } else if (map[17] && map[86]) {
                        event.preventDefault();
                        if (copiedObjects.length > 0) {
                            for (var i in copiedObjects) {
                                canvas.add(copiedObjects[i]);
                            }
                        }
                        else if (copiedObject) {
                            canvas.add(copiedObject);
                        }
                    } else if (map[46]) {
                        if (canvas.getActiveGroup()) {
                            canvas.getActiveGroup().forEachObject(function (o) { canvas.remove(o) });
                            canvas.discardActiveGroup().renderAll();
                        } else {
                            canvas.remove(canvas.getActiveObject());
                        }
                    }
                }
            }).keyup(function (e) {
                if (e.keyCode in map) {
                    map[e.keyCode] = false;
                }
            });
            $("#lnbtndownload").click(function () {
                var link = document.createElement("a");
                link.href = "~/bin/Barcode_Template.xlsx";
                link.download = "Barcode_Template.xlsx";
                link.click();
                window.URL.revokeObjectURL(data);
                link.remove();
            });

            var olddll = "";
            $("#ddlsavelist").on("change", function () {
                var ddl = $('#ddlsavelist').val();
                if (ddl != "0") {
                    var object = canvas.toJSON();
                    if (object.objects.length > 0) {
                        $('#ignismyModalcreate').modal('show');
                        // $('#lblname').text(olddll);
                    }
                    else {

                        $.ajax({
                            url: '<%=ResolveUrl("frmImageBarcodeTemplate.aspx/SelectBarcodeDetails")%>',
                            type: "POST",
                            data: "{ 'FileName': '" + ddl + "' }",
                            datatype: "json",
                            traditional: true,
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                debugger
                                olddll = data.d[0].split('|')[1];
                                canvas.loadFromJSON(data.d[0].split('|')[2], canvas.renderAll.bind(canvas));
                                $("#btnsave").attr('value', 'Update');
                                $("#txt_save").val(data.d[0].split('|')[1]);//FileName
                                $('#txt_save').prop("readonly", true);
                                $("#txt_width").val(data.d[0].split('|')[5] * 10);//CrossWidth
                                $("#txt_height").val(data.d[0].split('|')[6] * 10);//CrossHeight
                                $("#txt_rolesize").val(data.d[0].split('|')[4] * 10);//PaperRoleWidth     
                                if (data.d[0].split('|')[3] == "true") {
                                    $('#show_status').prop('checked', true);
                                }
                                else {
                                    $('#show_status').prop('checked', false);
                                }
                                $("#ddl_cross").val(data.d[0].split('|')[13]);//CrossCount
                                Barcodestatus = data.d[0].split('|')[14];
                            },
                        });
                    }
                }
                else {
                    canvas.clear();
                    $('#ignismyModalcreate').modal('hide');
                    $("#btnsave").attr('value', 'Save');
                    $('#txt_save').prop("readonly", false);
                    $("#txt_width").val("");
                    $("#txt_height").val("");
                    $("#txt_rolesize").val("");
                    $("#txt_save").val("");
                    $("#ddl_cross").val("-- SELECT --");
                    $('#show_status').prop('checked', false);
                    $("#btnsave").attr('value', 'Save');
                    $('#txt_save').prop("readonly", false);
                }
            });

            $("#btnCYes").click(function () {
                var ddls = $('#ddlsavelist').val();
                $.ajax({
                    url: '<%=ResolveUrl("frmImageBarcodeTemplate.aspx/SelectBarcodeDetails")%>',
                    type: "POST",
                    data: "{ 'FileName': '" + ddls + "' }",
                    datatype: "json",
                    traditional: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        canvas.loadFromJSON(data.d[0].split('|')[2], canvas.renderAll.bind(canvas));
                        $("#btnsave").attr('value', 'Update');
                        $("#txt_save").val(data.d[0].split('|')[1]);//FileName
                        $('#txt_save').prop("readonly", true);
                        $("#txt_width").val(data.d[0].split('|')[5] * 10);//CrossWidth
                        $("#txt_height").val(data.d[0].split('|')[6] * 10);//CrossHeight
                        $("#txt_rolesize").val(data.d[0].split('|')[4] * 10);//PaperRoleWidth
                        if (data.d[0].split('|')[3] == "true") {
                            $('#show_status').prop('checked', true);
                        }
                        else {
                            $('#show_status').prop('checked', false);
                        }
                        $("#ddl_cross").val(data.d[0].split('|')[13]);//CrossCount
                        Barcodestatus = data.d[0].split('|')[14];
                        $('#ignismyModalcreate').modal('hide');
                    },
                });
            });

            $('#fontSize').change(
                function () {
                    $('#dText').css('fontSize', this.value + 'px');
                    canvas.getActiveObject().set("fontSize", this.value);
                    canvas.renderAll();
                });

            window.toggleBoldText = function () {
                var obj = canvas.getActiveObject();
                if (obj) {
                    if (obj.get('fontWeight') == 'normal') {
                        obj.set('fontWeight', 'bold');
                    } else {
                        obj.set('fontWeight', 'normal');
                    }
                    canvas.renderAll();
                } else {
                    $.notify("Please select text object to add font weight...!", "info");
                }
            }
            window.funclosed = function () {
                $('#noty-holder').hide();
            }

            $('#font').fontselect().change(function () {
                var font = $(this).val().replace(/\+/g, ' ');
                font = font.split(':');
                var font_style = font[0];
                if (font_style == '') {
                    $.notify("Please select a Font Style...!", "info");
                    return false;
                }
                var tObj = canvas.getActiveObject();
                if (tObj == undefined) {
                    $.notify("Please select a Text...!", "info");
                    return false;
                }
                tObj.set({
                    fontFamily: font_style
                });
                canvas.renderAll();
            });
            $('#fontslist').change(function () {
                debugger
                var font = $(this).val().replace(/\+/g, ' ');
                font = font.split(':');
                var font_style = font[0];
                if (font_style == '') {
                    $.notify("Please select a Font Style...!", "info");
                    return false;
                }
                var tObj = canvas.getActiveObject();
                if (tObj == undefined) {
                    $.notify("Please select a Text...!", "info");
                    return false;
                }
                tObj.set({
                    fontFamily: font_style
                });
                canvas.renderAll();
            });
            $('#btnclose').click(function () {
                $("#noty-holder").hide();
            });

            $('#zoomIn').click(function () {
                canvas.setZoom(canvas.getZoom() * 1.1);
            });

            $('#zoomOut').click(function () {
                canvas.setZoom(canvas.getZoom() / 1.1);
            });

            $('#goRight').click(function () {
                var units = 10;
                var delta = new fabric.Point(units, 0);
                canvas.relativePan(delta);
            });

            $('#goLeft').click(function () {
                var units = 10;
                var delta = new fabric.Point(-units, 0);
                canvas.relativePan(delta);
            });
            $('#goUp').click(function () {
                var units = 10;
                var delta = new fabric.Point(0, -units);
                canvas.relativePan(delta);
            });

            $('#goDown').click(function () {
                var units = 10;
                var delta = new fabric.Point(0, units);
                canvas.relativePan(delta);
            });

            var myFunc = function () {
                $('.newtag-Horizontal').trigger('click');
                $('.dbconnection-list').trigger('click');
            }
            window.onload = function () {
                setTimeout(myFunc, 100);
            }
            // Create the new tag
            $('.newtag-Horizontal').click(function () {

                if ($('#container-first')[0].attributes[2].value == "display: block;" && $('#center')[0].attributes[1].value == "display: block;") {
                    $('#container-first').css('display', 'none');
                    $('#center').css('display', 'none');
                    // $('.Horizontal').css('display', 'none');
                    $('#ddldiv').css('display', 'none');
                } else {
                    $("#container-first").width(750).height(500);
                    canvas.setHeight(499);
                    canvas.setWidth(749);
                    $('#container-first').css('display', 'block');
                    $('#center').css('display', 'block');
                    $('#ddldiv').css('display', 'block');
                    var object = canvas.toJSON();
                    if (object.objects.length > 0) {
                        $('#ignismyModal').modal('show');
                    }
                }
            });
            $('#btnrefresh').click(function () {
                var objs = canvas.getObjects().map(function (o) {
                    return o.set('active', true);
                });
                var group = new fabric.Group(objs, {
                    originX: 'center',
                    originY: 'center'
                });
                canvas.setActiveGroup(group.setCoords()).renderAll();
            });
            $('#btnDonotSave').click(function () {
                var Udatedata = $("#ddlsavelist option:selected").text();
                if (Udatedata == "-- SELECT --" || Udatedata == "") {
                    $('#ignismyModal').modal('hide');
                    canvas.clear();
                }
                else {
                    ddlSave();
                    $('#ignismyModal').modal('hide');
                    canvas.clear();
                }
                $("#txt_width").val("");
                $("#txt_height").val("");
                $("#txt_rolesize").val("");
                $("#txt_save").val("");
                $("#ddl_cross").val("-- SELECT --");
                $('#show_status').prop('checked', false);
                $("#btnsave").attr('value', 'Save');
                $('#txt_save').prop("readonly", false);
            });
            $('.newtag-Vertical').click(function () {
                if ($('#container-first')[0].attributes[2].value == "display: block;" && $('#center')[0].attributes[1].value == "display: block;") {
                    $('#container-first').css('display', 'none');
                    $('#center').css('display', 'none');
                    $('#ddldiv').css('display', 'none');
                } else {
                    $("#container-first").width(450).height(500);
                    canvas.setHeight(499);
                    canvas.setWidth(449);
                    $('#container-first').css('display', 'block');
                    $('#center').css('display', 'block');
                    $('#ddldiv').css('display', 'block');
                }
            });

            // Create the text
            $('#text').click(function () {
                $("#noty-holder").empty('');
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    var text = new fabric.IText('Enter Text!', {
                        left: 10,
                        top: 10,
                        fontFamily: $('#fontslist').val(),
                        fontWeight: 'bold'
                    });
                    canvas.add(text);
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }

            });

            // Delete Selected Object
            $('#btndelete').click(function () {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    var activeGroup = canvas.getActiveGroup();
                    if (activeGroup) {
                        var activeObjects = activeGroup.getObjects();
                        for (let i in activeObjects) {
                            canvas.remove(activeObjects[i]);
                        }
                        canvas.discardActiveGroup();
                        canvas.renderAll();
                    }
                    else {
                        canvas.getActiveObject().remove();
                    }
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            // Create the Barcode - base64
            $('.bar_code').click(function () {
                gridGroup && canvas.remove(gridGroup);
                gridGroup = null;
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    Barcodestatus = "Barcode";
                    var img = document.createElement("img");
                    img.id = "imgbarcode"
                    img.style = "display:none";
                    document.getElementById("container-first").appendChild(img);
                    JsBarcode("#imgbarcode", '???????????', { format: "CODE128", lineColor: "#000000", width: 2, height: 50, fontSize: 75, displayValue: false, fontWeight: 'bold', background: "#FFFFFF" });
                    imagebase64 = $('#imgbarcode')[0].src;
                    $('#imgbarcode').remove();
                    if (imagebase64 != "") {
                        fabric.Image.fromURL(imagebase64, function (myImg) {
                            var img1 = myImg.set({ left: 10, top: 10, width: 250, height: 100 });
                            canvas.add(img1);
                        });
                        canvas.renderAll();
                    }
                }
                else {

                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            $('#newtag-rs').click(function () {
                icon_text("Rs");
            });
            $('#newtag-rsdourt').click(function () {
                icon_text("Rs.");
            });
            $('#newtag-mrprs').click(function () {
                icon_text("MRP Rs");
            });
            $('#newtag-mrp').click(function () {
                icon_text("MRP");
            });
            $('#newtag-rsicon').click(function () {
                icon_text("₹");
            });
            $('#newtag-pdate').click(function () {
                icon_text("P.Date");
            });
            $('#newtag-pkddate').click(function () {
                icon_text("Pkd Date");
            });
            $('#newtag-edate').click(function () {
                icon_text("E.Date");
            });
            $('#newtag-expdate').click(function () {
                icon_text("Exp Date");
            });

            //Create the QRcode - base64
            $('.qr_code').click(function () {
                var imagebaseqr64 = "";
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    //
                    Barcodestatus = "Qrcode";
                    var codecanvas = document.getElementById('qr-code'),
                        qr = new QRious({
                            element: codecanvas,
                            size: 150,
                            value: "DEVELOPED BOOBALAN"
                        });
                    codecanvas.style.display = "none";
                    dataUrl = codecanvas.toDataURL(),
                        imageFoo = document.createElement('img');
                    imageFoo.src = dataUrl;
                    var div = document.createElement("div");
                    div.id = "qrcode"
                    div.style = "display:none";
                    document.getElementById("container-first").appendChild(div);
                    imagebaseqr64 = imageFoo.src;
                    if (imagebaseqr64 != "") {
                        fabric.Image.fromURL(imagebaseqr64, function (myImg) {
                            var img1 = myImg.set({ left: 0, top: 0, width: 128, height: 128 });
                            canvas.add(img1);
                        });
                        canvas.renderAll();
                    }
                }
                else {

                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });


            $(".txt_picture").click(function () {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    $("#upload-file").trigger('click');
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            $("#upload-file").on("change", function (e) {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var imgObj = new Image();
                        imgObj.src = event.target.result;
                        imgObj.onload = function () {
                            var image = new fabric.Image(imgObj);
                            image.set({ left: 8, top: 8, width: 115, height: 95 });
                            canvas.add(image);
                        }
                    }
                    reader.readAsDataURL(e.target.files[0]);
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            $('#btncopy').click(function () {
                if (canvas.getActiveObject()) {
                    if (!fabric.util.getKlass(canvas.getActiveObject().type).async) {
                        clipboard = canvas.getActiveObject().clone();
                    } else {
                        canvas.getActiveObject().clone(function (clone) {
                            clipboard = clone;
                        });
                    }
                }
                if (canvas.getActiveGroup()) {
                    canvas.getActiveGroup().clone(function (clone) {
                        clipboard = clone;
                    });
                }
            });

            $('#btnpaste').click(function () {
                if (clipboard) {
                    if (!fabric.util.getKlass(clipboard.type).async) {
                        var obj = clipboard.clone();
                        obj.setTop(obj.top += 10);
                        obj.setLeft(obj.left += 10);
                        canvas.add(obj);
                        canvas.setActiveObject(obj);
                        clipboard = obj;
                    } else {
                        clipboard.clone(function (clone) {
                            clone.setTop(clone.top += 10);
                            clone.setLeft(clone.left += 10);
                            clone.forEachObject(function (obj) {
                                canvas.add(obj);
                            });

                            canvas.deactivateAll();
                            if (clipboard.isType("group")) {
                                canvas.setActiveGroup(clone);
                            } else {
                                canvas.setActiveObject(clone);
                            }
                            clipboard = clone;
                        });
                    }
                }
                canvas.renderAll();
            });

            // Create the Download
            $('.download').click(function () {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    removeGrid();
                    var element = $("#container-first");//this div id.
                    var html2obj = html2canvas(element);
                    var queue = html2obj.parse();
                    var canvas = html2obj.render(queue);
                    var data = canvas.toDataURL('image/png');
                    var link = document.createElement('a');
                    document.body.appendChild(link);
                    link.href = data;
                    link.download = "Unipro-barcode-tool.png";
                    link.click();
                    window.URL.revokeObjectURL(data);
                    link.remove();
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            // Save the barcode design screen - canvas2json
            $('.save-data').click(function () {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    removeGrid();
                    var element = $("#container-first");//this div id.
                    var html2obj = html2canvas(element);
                    var queue = html2obj.parse();
                    var canvas = html2obj.render(queue);
                    var data = canvas.toDataURL('image/png');
                    $("#my_image").attr("src", data);
                    $('#myModal').modal('show');
                    $("#lblerrormessage").show();
                    //$('#myModal').css('display', 'block');
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            $('.save-design').click(function (o) {
                canvas.observe('mouse:down', function (e) { mousedown(e); });
                canvas.observe('mouse:move', function (e) { mousemove(e); });
                canvas.observe('mouse:up', function (e) { mouseup(e); });

                var started = false;
                var x = 0;
                var y = 0;
            });          
                $('#txt_save').bind('input', function () {
                    $(this).val(function (_, v) {
                        return v.replace(/\s+/g, '');
                    });
                });
            $("#btnsave").click(function () {
                var Udatedata = $("#ddlsavelist option:selected").text();
                if (Udatedata == "-- SELECT --" || Udatedata == "") {
                    Udatedata = "0";
                }
                var savevalue = $("#txt_save").val();
                var cwidth = $("#txt_width").val() / 10;
                var cheight = $("#txt_height").val() / 10;
                var rolesize = $("#txt_rolesize").val() / 10;
                var status = $('#show_status').prop('checked')
                var ddlcross = $("#ddl_cross").val();
                var rotutiontype = "Horizontal";
                var object = canvas.toJSON();
                var json = JSON.stringify(object);

                if (savevalue != "" && cwidth != "" && cheight != "" && rolesize != "" && ddlcross != "-1") {
                    $.ajax({
                        url: '<%=ResolveUrl("frmImageBarcodeTemplate.aspx/SaveJsonTemplate")%>',
                        data: "{ 'savevalue': '" + savevalue + "','jsonOfLog' :'" + json + "','cwidth' :'" + cwidth + "','cheight' :'" + cheight + "','rolesize' :'" + rolesize + "','status' :'" + status + "','cross' :'" + ddlcross + "','rotutiontype' :'" + rotutiontype + "','ddl' :'" + Udatedata + "','Barcodestatus' :'" + Barcodestatus + "'}",
                        type: "POST",
                        datatype: "json",
                        traditional: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            if (data.d == "True") {
                                if (data.d == "True") {
                                    var btnval = $("#btnsave").val();
                                    if (btnval == "Update") {
                                        ddlSave();
                                        $.notify("Update Successfully ...! ", "success");
                                        $("#btnsave").attr('value', 'Update');
                                    }
                                    else {
                                        ddlSave();
                                        $("#ddlsavelist option:selected").text(savevalue);
                                        $.notify("Save Successfully ...! ", "success");
                                    }
                                }
                            }
                            else
                                $.notify("Warning: Something Went Wrong...!", "error");
                        },
                    });
                } else {
                    if (savevalue != "") {
                        $("#lblerrormessage").text('');
                        $("#lblerrormessage").text("Please enter the file name...!");
                    }
                    else if (cwidth != "") {
                        $("#lblerrormessage").text('');
                        $("#lblerrormessage").text("Please enter the width...!");
                    }
                    else if (cheight != "") {
                        $("#lblerrormessage").text('');
                        $("#lblerrormessage").text("Please enter the height...!");
                    }
                    else if (rolesize != "") {
                        $("#lblerrormessage").text('');
                        $("#lblerrormessage").text("Please enter the role size...!");
                    }
                    else if (ddlcross != "") {
                        $("#lblerrormessage").text('');
                        $("#lblerrormessage").text("Please enter the cross...!");
                    }
                }
            });

            $('.dbconnection-list').click(function () {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    $.ajax({
                        url: '<%=ResolveUrl("frmImageBarcodeTemplate.aspx/GetBarcodeDetails")%>',
                        type: "POST",
                        datatype: "json",
                        traditional: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $("#dbname_list").empty();
                            $('#connection-list').css('display', 'block');
                            if (data.d != "") {
                                for (var i = 0; i < data.d.length; i++) {
                                    var listname = ""
                                    var name = data.d[i];
                                    listname = name.replace(/\s/g, '');
                                    $("#dbname_list").append("<li onClick='Objectlist(" + listname + ")' id=" + listname + ">" + name + "</li>");
                                }
                            }
                        },
                    });
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });

            $("#Print_ZPL").click(function () {

                var zpl = "ZDesigner ZD220-203dpi ZPL"
                var element = $("#container-first");
                var html2obj = html2canvas(element);
                var queue = html2obj.parse();
                var canvas = html2obj.render(queue);
                var result = canvas.toDataURL('image/png');
                var base64result = result.split(',')[1];
                let jobName = "label";
                $.ajax({
                    url: '@Url.Action("GetPrintBarcodeDetails", "Home")',
                    data: JSON.stringify({ printername: zpl, base64Image: base64result, jobName: jobName }),
                    type: "post",
                    async: false,
                    proccessData: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    cache: false,
                    success: function (data) {
                        if (data == true) {
                            $.notify("Successfully printed...! ", "success");
                        }
                        else {
                            $.notify("Failed printed...!", "error");
                        }
                    },
                });
            });

            $("#Print").click(function () {
                var ddl = $('#ddlsavelist').val();
                var object = "";
                var List = [];
                var listnames = [];
                var listout = "";
                $.ajax({
                    url: '@Url.Action("GetAllBarcodeDetails", "Home")',
                    data: { listname: ddl },
                    type: "GET",
                    datatype: "text",
                    traditional: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        data[0].forEach(function (entry) {
                            var listname = entry.NameList;
                            listnames.push(listname.replace(/\s/g, ''));
                        });
                        data[1].forEach(function (entry) {
                            List.push(entry);
                        });
                        ObjectList(List, listnames);
                    },
                });
            });
            function ObjectList(List, listname) {
                var object = "";
                object = canvas.toJSON();
                if (List.length > 0) {
                    listout = List.map((o) => { return Object.keys(o) }).reduce((prev, curr) => { return prev.concat(curr) }).filter((col, i, array) => { return array.indexOf(col) === i });
                    $.each(List, function (key, valus) {

                        $.each(listout, function (key, listout) {
                            $.each(listname, function (key, names) {
                                if (names == listout) {
                                    var locallist = '#' + names;
                                    if (key == 0) {
                                        findTextAndReplace(object, locallist, valus.ItemName.replace(/\s/g, ''));
                                    }
                                    else if (key == 1) {
                                        findTextAndReplace(object, locallist, valus.Qty.replace(/\s/g, ''));
                                    }
                                    else if (key == 2) {
                                        findTextAndReplace(object, locallist, valus.Barcode.replace(/\s/g, ''));
                                        findImageAndReplace(object, imagebase64, ImageAndReplace(valus.Barcode.replace(/\s/g, '')));
                                    }
                                }
                            });
                        });
                    });
                    var json = JSON.stringify(object);
                    canvas.loadFromJSON(json, canvas.renderAll.bind(canvas));
                }
            }

            function icon_text(val) {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    var text = new fabric.IText(val, {
                        left: 10,
                        top: 10,
                        fontFamily: $('#fontslist').val(),
                        fontWeight: 'bold'
                    });
                    canvas.add(text);
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            }

            function ddlSave() {
                $("#ddlsavelist").empty();
                $("#ddlsavelist").append("<option  value='0'>--Select--</option>");
                $.ajax({
                    url: '<%=ResolveUrl("frmImageBarcodeTemplate.aspx/GetBarcodeTemplateDetails") %>',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        debugger
                        $.each(data.d, function (i, d) {
                            $("#ddlsavelist").append("<option value=" + d + ">" + d + "</option>");
                        });
                        $("#ddlsavelist").css("display", "");
                    },
                });

            }

            /* Mousedown */
            function mousedown(e) {
                var mouse = canvas.getPointer(e.memo.e);
                started = true;
                x = mouse.x;
                y = mouse.y;

                var square = new fabric.Rect({
                    width: 0,
                    height: 0,
                    left: x,
                    top: y,
                    fill: '#000'
                });

                canvas.add(square);
                canvas.renderAll();
                canvas.setActiveObject(square);

            }

            /* Mousemove */
            function mousemove(e) {
                if (!started) {
                    return false;
                }

                var mouse = canvas.getPointer(e.memo.e);

                var w = Math.abs(mouse.x - x),
                    h = Math.abs(mouse.y - y);

                if (!w || !h) {
                    return false;
                }

                var square = canvas.getActiveObject();
                square.set('width', w).set('height', h);
                canvas.renderAll();
            }

            /* Mouseup */
            function mouseup(e) {
                if (started) {
                    started = false;
                }

                var square = canvas.getActiveObject();

                canvas.add(square);
                canvas.renderAll();
            }
            $('.horizontal').click(function (event) {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    canvas.add(new fabric.Line([50, 100, 200, 100], {
                        left: 10,
                        top: 10,
                        stroke: 'black',
                        perPixelTargetFind: false,
                        strokeWidth: 3
                    }));
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });
            $('.vertical').click(function (event) {
                if ($('#center')[0].attributes[1].value == "display: block;") {
                    canvas.add(new fabric.Line([50, 100, 50, 200], {
                        left: 50,
                        left: 50,
                        top: 50,
                        stroke: 'black',
                        perPixelTargetFind: false,
                        strokeWidth: 3
                    }));
                }
                else {
                    $.notify("Warning: Please open the designer tag...!", "warn");
                }
            });
            $("#btnsubmit").click(function () {
                var width = $('#myWidthtextbox').val();
                var height = $('#myHeighttextbox').val();
                if (width != "" && height != "") {
                    var elementwidth = document.getElementById('container-first').getBoundingClientRect().width;
                    var elementheight = document.getElementById('container-first').getBoundingClientRect().height;
                    width = parseInt(elementwidth),
                        height = parseInt(elementheight),
                        mmwidth = Math.floor(width * 0.264583);
                    mmheight = Math.floor(height * 0.264583);
                    $("#container-first").width(mmwidth).height(mmheight);
                    canvas.setDimensions({ width: mmwidth, height: mmheight });

                }
            });

            window.Objectlist = function (e) {
                var value = e.innerText;
                var text = new fabric.IText("#" + value, {
                    left: 10,
                    top: 10,
                    editable: false,
                    fontFamily: $('#fontslist').val(),
                    fontWeight: 'bold'
                });
                canvas.add(text);
            };

            function removeGrid() {
                //
                gridGroup && canvas.remove(gridGroup);
                gridGroup = null;
            }
        });
        function ImageAndReplace(value) {
            var base64 = []
            var img = document.createElement("img");
            img.id = "rlp_imgbarcode"
            img.style = "display:none";
            document.getElementById("container-first").appendChild(img);
            JsBarcode("#rlp_imgbarcode", value, { format: "CODE128", lineColor: "#000000", width: 2, height: 50, fontSize: 75, displayValue: false, fontWeight: 'bold', background: "#FFFFFF" });
            // JsBarcode("#rlp_imgbarcode", value);
            base64 = $('#rlp_imgbarcode')[0].src;
            $('#rlp_imgbarcode').remove();
            return base64;
        }
        function findImageAndReplace(object, value, replacevalue) {
            for (var x in object) {
                if (typeof object[x] == typeof {}) {
                    findImageAndReplace(object[x], value, replacevalue);
                }
                if (object[x] == value) {
                    object["src"] = replacevalue;
                }
            }
        }
        function findTextAndReplace(object, value, replacevalue) {
            for (var x in object) {
                if (typeof object[x] == typeof {}) {
                    findTextAndReplace(object[x], value, replacevalue);
                }
                if (object[x] == value) {
                    object["text"] = replacevalue;
                }
            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function minmax(value, min, max) {
            if (parseInt(value) < min || isNaN(parseInt(value)))
                return min;
            else if (parseInt(value) > max)
                return max;
            else return value;
        }
        function getErrorMessage(jqXHR, exception) {

            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
                $.notify(msg, "error");
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
                $.notify(msg, "error");
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
                $.notify(msg, "error");
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
                $.notify(msg, "error");
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
                $.notify(msg, "error");
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
                $.notify(msg, "error");
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                $.notify(msg, "error");
            }
            $('#post').html(msg);
        }

        function fnbtnCancel() {
            $('#ignismyModal').modal('hide');
        }
        function fnbtnNoback() {
            $('#ignismyModalback').modal('hide');
        }
        function fnbtnCNoback() {
            $('#ignismyModalcreate').modal('hide');
        }
        function fnbtnSave() {
            $('#ignismyModal').modal('hide');
            $('#myModal').modal('show');
        }

    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'InventoryImageBarcode');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "InventoryImageBarcode";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }
        function hideaction() {
            $("#container-first").width(750).height(500);
            canvas.setHeight(499);
            canvas.setWidth(749);
            $('#container-first').css('display', 'block');
            $('#center').css('display', 'block');
            $('#ddldiv').css('display', 'block');
        }


        function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        function filterFunction() {
            var input, filter, ul, li, a, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            div = document.getElementById("myDropdown");
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                txtValue = a[i].textContent || a[i].innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }
        $(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
    <script type="text/javascript" src="../BarcodeToolController/Scripts/qrious.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-10" style="margin-left: 100px;">

                    <div id="center" style="display: none;">
                        <a id="zoomIn" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span></a>
                        <a id="zoomOut" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-zoom-out" aria-hidden="true"></span></a>
                        <a id="goLeft" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span></a>
                        <a id="goRight" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span></a>
                        <a id="goUp" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-circle-arrow-up" aria-hidden="true"></span></a>
                        <a id="goDown" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-circle-arrow-down" aria-hidden="true"></span></a>
                        <a id="btndelete" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                        <a id="btnbold" onclick="toggleBoldText()" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-bold" aria-hidden="true"></span></a>
                        <%--      <a id="btnrefresh" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span></a>--%>
                        <a id="Print" style="display: none;" class="btn btn-success btn-circle btn-circle-sm m-1"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></a>
                        <%--<input id="font" hidden style="display: none;" type="text" />--%>
                        <select id="fontslist" class="selectpicker" style="width: 120px;" data-live-search="true">
                            <option style="font-family: Anton; font-weight: 400;" data-tokens="Anton">Anton</option>
                            <option style="font-family: Cabin; font-weight: 400;" data-tokens="Cabin">Cabin</option>
                            <option style="font-family: Orbitron; font-weight: 500;" data-tokens="Orbitron">Orbitron 500</option>
                            <option style="font-family: Hammersmith One; font-weight: 400;" data-tokens=" Hammersmith One">Hammersmith One</option>
                        </select>
                        <select id="fontSize" style="width: 48px; height: 25px; border-radius: 4px; display: none;">
                            <option value="10">10</option>
                            <option value="12">12</option>
                            <option value="14">14</option>
                            <option value="16">16</option>
                            <option value="18">18</option>
                            <option value="20">20</option>
                            <option value="22">22</option>
                            <option value="24">24</option>
                            <option value="26">26</option>
                            <option value="28">28</option>
                            <option value="30">30</option>
                            <option value="12">32</option>
                            <option value="14">34</option>
                            <option value="16">36</option>
                            <option value="18">38</option>
                            <option value="20">40</option>
                            <option value="22">42</option>
                            <option value="24">44</option>
                            <option value="26">46</option>
                            <option value="28">48</option>
                            <option value="30">50</option>
                        </select>
                        <span style="color: white; font-size: larger;">Grid :</span>
                        <input type="checkbox" style="margin-left: 10px;" id="show_grid" />
                    </div>
                    <div id="container-first" class="canvas-containerlist" style="display: none;">
                        <canvas id='c' class="datalist_get" width="749" height="499"></canvas>
                        <canvas id="qr-code"></canvas>
                    </div>


                </div>
                <div class="col-sm-2" style="margin-left: -22vh;">
                    <div id="connection-list" class="container" style="display: none;">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class="price-table pt-bg-blue">
                                <div>
                                    <span class="checkboxtext">Data Source</span>
                                </div>
                                <ul id="dbname_list"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" role="dialog" id="myModal">
            <div class="modal-dialog" style="top: 35px;">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Save/Update</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div style="margin-left: -19px;" class="col-lg-12">
                                    <div class="col-lg-3">
                                        <label for="recipient-name" class="form-label">File Name:</label>
                                    </div>
                                    <div class="col-lg-9">
                                        <input type="text" style="width: 371px;" id="txt_save" class="form-control">
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div style="margin-left: -19px;" class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <label for="recipient-name" class="form-label">Width(MM):</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="number" style="width: 132px;" id="txt_width" class="form-control" placeholder="Width(MM)" step=".01">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <label for="recipient-name" class="form-label">Height(MM):</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="number" style="width: 132px;" id="txt_height" class="form-control" placeholder="Height(MM)" step=".01">
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div style="margin-left: -19px;" class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <label for="recipient-name" class="form-label">Role Size Width(MM):</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="number" style="width: 132px;" id="txt_rolesize" class="form-control" placeholder="Role Size(MM)" step=".01">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="col-lg-6">
                                            <label for="recipient-name" class="form-label">Cross:</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select style="width: 132px;" id="ddl_cross" class="form-control" name="cross">
                                                <option value="-1">--Select--</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="col-lg-12">
                                    <div class="col-lg-12" style="margin-left: -31px; margin-top: 10px;">
                                        <div class="col-lg-6">
                                            <label for="recipient-name" class="form-label">Printer (Style):</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="col-lg-6">
                                                <label for="recipient-name" class="form-label">Status: </label>
                                            </div>
                                            <div class="col-lg-6">
                                                <input style="margin-left: 27px;" type="checkbox" id="show_status" />
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <section class="section">
                                        <div class="row">
                                            <div style="text-align: center;">
                                                <img style="border: groove;" width="130" height="130" id="my_image" src="../images/BarcodeTemplateImage/Boobalan.png" />
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer border-0">
                        <label id="lblerrormessage" style="color: red;"></label>
                        <input type="button" id="btnsave" class="btn btn-success" value="Save" data-dismiss="modal" />
                    </div>
                </div>
            </div>
        </div>

        <!--Model Popup starts-->
        <div id="ignismyModalscreate" class="container">
            <div class="row">
                <div class="modals fade" id="ignismyModalcreate" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="thank-you-pop">
                                    <p>
                                        Do you want to change this
                                        <label id="lblname"></label>
                                        ?
                                    </p>
                                </div>
                                <div class="thank-you-pop">
                                    <button type="button" id="btnCNo" onclick="return fnbtnCNoback();" class="win8-notif-button">No</button>
                                    <button type="button" id="btnCYes" onclick="return fnbtnCYes();" class="win8-notif-button">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Model Popup ends-->
        <!--Model Popup starts-->
        <div id="ignismyModalsback" class="container">
            <div class="row">
                <div class="modals fade" id="ignismyModalback" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="thank-you-pop">
                                    <p>Do you want a redirect barcode page?</p>
                                </div>
                                <div class="thank-you-pop">
                                    <button type="button" id="btnNo" onclick="return fnbtnNoback();" class="win8-notif-button">No</button>
                                    <button type="button" id="btnYes" onclick="return fnbtnYes();" class="win8-notif-button">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Model Popup ends-->
        <!--Model Popup starts-->
        <div id="ignismyModals" class="container">
            <div class="row">
                <div class="modals fade" id="ignismyModal" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="thank-you-pop">
                                    <p>Do you want to save change to Untitled?</p>
                                </div>
                                <div class="thank-you-pop">
                                    <button type="button" id="btnCancels" onclick="return fnbtnCancel();" class="win8-notif-button">Cancel</button>
                                    <button type="button" id="btnDonotSave" class="win8-notif-button">Don't Save</button>
                                    <button type="button" id="btnSave" onclick="return fnbtnSave();" class="win8-notif-button">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Model Popup ends-->



    </div>

</asp:Content>
