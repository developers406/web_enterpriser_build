﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPackageInventory.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmPackageInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 152px;
            max-width: 152px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 250px;
            max-width: 250px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }
        .btch_left{
            margin-left:20px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
              
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
     //----------------------------------------------- gvBatchDetails Select -----------------------------------------//

            $(function () {
                $("[id*=gvBatchDetails] td").click(function () {
                    DisplayBatch($(this).closest("tr"));
                });
                return false;
            });

            function DisplayBatch(row) {
                try {
                    if ( $.trim($("td", row).eq(0).text()) != "")
                    $("[id*=txtBatchNo]").val($("td", row).eq(0).text());
                }
                catch (err) {
                    alert(err.Message);
                    console.log(err);
                }
            }
        }
    </script>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtInventory.ClientID%>").value == "") {
                Show = Show + ' <%=Resources.LabelCaption.Alert_InvCode%>'
                document.getElementById("<%=txtInventory.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtQty.ClientID%>").value == "" || document.getElementById("<%=txtQty.ClientID%>").value == "0") {
                Show = Show + '\n Please Enter Qty';
                document.getElementById("<%=txtQty.ClientID %>").focus();
            }
            if ($("[id*=gvBatchDetails] tr").length > 1) {
                if (document.getElementById("<%=txtBatchNo.ClientID%>").value == "") {
                    Show = Show + '\n Please Choose Batch No';
                }
            }
            if (Show != '') {
                alert(Show);
                return false;
            }

            else {
                return true;
            }
        }

    </script>
    <script type="text/javascript">
        function ValidateFormUpdate() {
            var Show = '';

            if (document.getElementById("<%=ddlPackageInv.ClientID%>").value == "") {
                Show = Show + ' Please Select Package Inventory Code'
                document.getElementById("<%=ddlPackageInv.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                return false;
            }

            else {
                return true;
            }
        }
        function gridclear() {
            try {
                $("<%=gvPackageInv.ClientID%>").remove();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function clearForm() {
            $("[id*=gvPackageInv] tbody").remove();
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
           // gridclear();
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $("[id$=hfInvCode]").val(Code);
                    $("[id*=txtBatchNo]").val('');
                    $("[id$=txtQty]").val('0');
                    $("[id$=txtDescription]").val(Description);
                    //$('#<%=txtDescription.ClientID %>').val(Description);

                    $.ajax({

                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmPackageInventory.aspx/GetBatchDetail") %>',
                        data: "{'InvCode':'" + Code + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        failure: function (response) {
                            alert(response.d);
                        },
                        error: function (response) {
                            alert(response.d);
                        }
                    });

                    function OnSuccess(response) {
                        try {
                            debugger;
                            var xmlDoc = $.parseXML(response.d);
                            //alert(response.d)
                            var xml = $(xmlDoc);
                            var batch = xml.find("Table");
                            var row = $("[id*=gvBatchDetails] tr:last-child").clone(true);
                            $("[id*=gvBatchDetails] tr").not($("[id*=gvBatchDetails] tr:first-child")).remove();
                            $.each(batch, function () {
                                var batch = $(this); 
                                $("td", row).eq(0).html($(this).find("BatchNo").text());
                                $("td", row).eq(1).html($(this).find("Qty").text());
                                $("[id*=gvBatchDetails]").append(row);
                                row = $("[id*=gvBatchDetails] tr:last-child").clone(true);
                            });

                            if (response.d = "<NewDataSet/>") {
                                $("[id*=gvBatchDetails] tr:last").after('<tr><td></td><td></td></tr>');
                            }
                            $("[id*=gvBatchDetails] tr:first").remove();
                        }
                        catch (err) {
                            alert(err.Message);
                        }
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PackageInventory');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PackageInventory";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Package Inventory</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-group-pi">
                    <div class="container-control" style="padding: 0px">
                        <div class="top-container-pi">
                            <div class="top-container-pi-header">
                                Package Header
                            </div>
                            <div class="top-container-pi-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left" style="width: 60%">
                                        <div class="label-left" style="width: 27%">
                                            <asp:Label ID="Label4" runat="server" Text="Package Inv"></asp:Label>
                                        </div>
                                        <div class="label-right" style="width: 73%">
                                            <asp:DropDownList ID="ddlPackageInv" runat="server" CssClass="form-control-res" OnSelectedIndexChanged="ddlPackageInv_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="control-group-right" style="width: 38%">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text="Total Qty"></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtTotalQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="width: 16%">
                                        <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 84%">
                                        <asp:TextBox ID="txtPackageDesc" runat="server" CssClass="form-control-res" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="width: 16%">
                                        <asp:Label ID="Label1" runat="server" Text="Remarks"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 84%">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left" style="width: 16%">
                                        <asp:Label ID="Label5" runat="server" Text="Festival"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 84%">
                                        <asp:CheckBox ID="chkFestival" runat="server" />
                                    </div>
                                </div>


                                <div class="right-container-top-detail">
                                    <div class="GridDetails">
                                        <div class="row">
                                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                <div class="grdLoad">
                                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Remove
                                                                </th>
                                                                <th scope="col">Code
                                                                </th>
                                                                <th scope="col">Description
                                                                </th>
                                                                <th scope="col">Qty
                                                                </th>
                                                                <th scope="col">BatchNo
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <%--<div class="grid-overflow-height grdLoad" style="overflow: auto; height: 140px">--%>
                                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 110px; width: 870px; background-color: aliceblue;">
                                                        <asp:GridView ID="gvPackageInv" runat="server" AutoGenerateColumns="False" ShowHeader="false" ShowHeaderWhenEmpty="True"
                                                            OnRowDeleting="gvPackageInv_OnRowDeleting" OnRowDataBound="gvPackageInv_OnRowDataBound"
                                                            CssClass="pshro_GridDgn grdLoad">
                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                            <PagerStyle CssClass="pshro_text" />
                                                            <Columns>
                                                                <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Remove" />
                                                                <asp:BoundField DataField="InventoryCode" HeaderText="Code"></asp:BoundField>
                                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                                <asp:BoundField DataField="Qty" HeaderText="Qty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                                <asp:BoundField DataField="BatchNo" HeaderText="BatchNo" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                            </Columns>

                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bottom-container-pi">
                                    <div class="bottom-container-pi-header">
                                        Package Detail
                                    </div>
                                    <div class="bottom-container-pi-detail">
                                        <div class="bottom-container-pi-detail-left">
                                            <div class="control-group-single">
                                                <div class="label-left">
                                                    <asp:Label ID="Label6" runat="server" Text="Inventory"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtInventory" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtInventory', '');" CssClass="form-control-res"></asp:TextBox>
                                                    <asp:HiddenField ID="hfInvCode" runat="server" />
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="label-left">
                                                    <asp:Label ID="Label8" runat="server" Text="BatchNo"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="label-left">
                                                    <asp:Label ID="Label7" runat="server" Text="Description"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-group-single">
                                                <div class="label-left">
                                                    <asp:Label ID="Label9" runat="server" Text="Qty"></asp:Label>
                                                </div>
                                                <div class="label-right">
                                                    <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeywithDecimal(event)"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="control-container">
                                                <div class="control-button">
                                                    <asp:LinkButton ID="lnkAddToList" runat="server" class="button-blue" Text="Add to List"
                                                        OnClientClick="return ValidateForm();" OnClick="lnkAddToList_Click"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="bottom-container-pi-detail-right" >
                                            <div class="GridDetails" >
                                             
                                                    <%-- <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                                                    <div class="grdLoad">
                                                        <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">BatchNo
                                                                    </th>
                                                                    <th scope="col">Qty
                                                                    </th>
                                                            </thead>
                                                        </table>

                                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; width:303px; height: 115px; background-color: aliceblue;">
                                                            <asp:GridView ID="gvBatchDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false" CssClass="pshro_GridDgn grdLoad">

                                                                <EmptyDataTemplate>
                                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                                </EmptyDataTemplate>
                                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                                <PagerStyle CssClass="pshro_text" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="BatchNo" HeaderText="BatchNo" ></asp:BoundField>
                                                                    <asp:BoundField DataField="Qty" HeaderText="Qty"></asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                            
                                            </div></div>
                                        

                                        <div class="button-contol">
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkUpdate" runat="server" class="button-red" Text="Update" OnClientClick="return ValidateFormUpdate();"
                                                    OnClick="lnkUpdate_Click"></asp:LinkButton>
                                            </div>
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="button-red" Text="Delete" OnClick="lnkDeleteToList_Click"
                                                    OnClientClick="return ValidateFormUpdate();"></asp:LinkButton>
                                            </div>
                                            <div class="control-button">
                                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" Text="Clear" OnClientClick="clearForm();"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
