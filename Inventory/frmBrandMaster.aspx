﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmBrandMaster.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmBrandMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 110px;
            max-width: 110px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 110px;
            max-width: 110px;
            text-align: center;
        }
          .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 110px;
            max-width: 110px;
            text-align: center;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 180px;
            max-width: 180px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
           min-width: 135px;
            max-width: 135px;
      

        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
           
            display:none;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 135px;
            max-width: 135px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 295PX;
            max-width: 300PX;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display: none;
        }

        .Discount {
            margin-left: 438px;
            padding: 10px 10px 0px 10px;
            margin-top: -199px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
        .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        var divDis = '0';

        function ValidateForm() {

            if ($('#<%=txtBrandCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to save existing Brand");
                return false;
            }
            if (!($('#<%=txtBrandCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                ShowPopupMessageBox("You have no permission to save New Brand");
                return false;
            }
            var Show = '';
            var BrandCode = $.trim($('#<%=txtBrandCode.ClientID %>').val());         //22112022 musaraf
            var BrandName = $.trim($('#<%=txtBrandName.ClientID %>').val());

            if (BrandCode == "") {
                Show = Show + 'Please Enter Brand Code';
                document.getElementById("<%=txtBrandCode.ClientID %>").focus();
                //return false;
            }

            if (BrandName == "") {
                Show = Show + '<br />Please Enter Brand Name';
                document.getElementById("<%=txtBrandCode.ClientID %>").focus();
                //return false;
            }

           <%-- if (document.getElementById("<%=ddlCategory.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Category';
                document.getElementById("<%=txtBrandCode.ClientID %>").focus();
                //return false;
            }--%>
            //if ($("[id*=cblLocation] input:checked").length == 0) {
            //    Show = Show + '<br />Please Any One Location';
            //    $("[id*=cblLocation] input:checked").focus();
            //    //return false;
            //}
            if (divDis == "1") {
                if ($("[id*=cblLocation] input:checked").length == 0) {
                    Show = Show + '<br />Please Enter any one Location';
                }
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }


           <%-- if (document.getElementById("<%=txtBrandCode.ClientID%>").value == "") {
                ShowPopupMessageBox('Please Enter Brand Code');
                document.getElementById("<%=txtBrandCode.ClientID %>").focus();
                return false;
            }

            else if (document.getElementById("<%=txtBrandName.ClientID%>").value == "") {
                ShowPopupMessageBox('Please Enter Brand Name');
                document.getElementById("<%=txtBrandName.ClientID %>").focus();
                return false;
            }--%>
                //if ($("[id*=cblLocation] input:checked").length == 0) {
                //    Show = Show + ('<br />Please Select Any one Location');
                //    //return false;
                //}

            else {
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").prop("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").prop("checked", false);
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").prop("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").prop('checked', false);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });



        function Toast() {

            $('#<%=divDiscount.ClientID%>').show();
            divDis = '1';

        }
    </script>
    <script type="text/javascript">
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupBrand').dialog('close');
                window.parent.$('#popupBrand').remove();

            }
        });
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=lnkSave.ClientID %>').css("display", "none");
              }
              $(function () {
                  try {
                      $("[id*=cbSelectAll]").bind("click", function () {
                          if ($(this).is(":checked")) {
                              $("[id*=cblLocation] input").prop("checked", "checked");
                          } else {
                              $("[id*=cblLocation] input").prop("checked", false);
                          }
                      });
                      $("[id*=cblLocation] input").bind("click", function () {
                          if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                              $("[id*=cbSelectAll]").prop("checked", "checked");
                          } else {
                              $("[id*=cbSelectAll]").prop('checked', false);
                          }
                      });
                  }
                  catch (err) {
                      ShowPopupMessageBox(err.message);
                  }
              });
              $("select").chosen({ width: '100%' }); // width in px, %, em, etc

              $('#<%=txtBrandCode.ClientID %>').focus();
            $('#<%=txtBrandCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmBrandMaster.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtBrandCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {

                            $('#<%=txtBrandCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtBrandCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtBrandName.ClientID %>').val(strarray[1]);
                            $('#<%=txtDiscount.ClientID %>').val(strarray[2]);

                            $('#<%=ddlCategory.ClientID %>').val($.trim(strarray[3]));
                            $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                            $('#<%=txtRemarks.ClientID %>').val($.trim(strarray[4]));


                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtBrandName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmBrandMaster.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1],

                                }

                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               <%-- focus: function (event, i) {
                    $('#<%=txtBrandCode.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtBrandName.ClientID %>').val($.trim(i.item.label));
                    $('#<%=txtDiscount.ClientID %>').val($.trim(i.item.Discount));
                    $('#<%=txtRemarks.ClientID %>').val($.trim(i.item.Remarks));
                    $('#<%=ddlCategory.ClientID %>').val($.trim(i.item.Category));
                    $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                    event.preventDefault();
                },--%>
                 select: function (e, i) {

                     $('#<%=txtBrandName.ClientID %>').val($.trim(i.item.label));


                        return false;
                    },
                    minLength: 1
             });
                    $("[id$=txtSearch]").autocomplete({
                        source: function (request, response) {
                            $.ajax({
                                url: "frmBrandMaster.aspx/GetFilterValue",
                                data: "{ 'prefix': '" + request.term + "'}",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('-')[1],
                                            val: item.split('-')[1]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.responseText);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.responseText);
                                }
                            });
                        },

                        focus: function (event, i) {
                            $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        event.preventDefault();
                    },
                    select: function (e, i) {
                        $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                        return false;
                    },

                    minLength: 1
                });


                   

        }
        
                function Brand_Delete(source) {

                    if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                        ShowPopupMessageBox("You have no permission to Delete this Brand");
                        return false;
                    }
                    $("#dialog-confirm").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            "YES": function () {
                                $(this).dialog("close");
                                $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                                $("#<%= btnDelete.ClientID %>").click();
                            },
                            "NO": function () {
                                $(this).dialog("close");
                                returnfalse();
                            }
                        }
                    });

                }
                function disableFunctionKeys(e) {
                    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                        if (e.keyCode == 115) {
                            if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                                $('#<%= lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                             e.preventDefault();
                         }
                        <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                    }
          };
          //function param() {
          //    //alert('test');
          //    var objData;
          //    $.ajax({
          //        url: "frmBrandMaster.aspx/GetParam",
          //        //data: "{ 'prefix': '" + request.term + "'}",
          //        dataType: "json",
          //        type: "POST",
          //        contentType: "application/json; charset=utf-8",
          //        success: function (msg) {
          //            objData = jQuery.parseJSON(msg.d);
          //            //console.log(objData.TextileBranded);
          //           // debugger;
          //            if (objData.DiscountBasedOn == "N") {
          //                //alert('test');
          //                $("#divDiscount").css("display", "none");

        <%--for (var i = 0; i < objData.length; i++) {
                            //alert('vj');
                            if (($('#<%= hidParam.ClientID %>').val(objData[i]["TextileBranded"]))== "N") {
                                //hidParam
            
                               
                            }
                        }--%>
        //}
        //else {
        //    //$("#divDiscount").css("display", "none");
        //    alert('test');
        //    $("#divGRN").css("display", "block");
        //}

                 <%--   console.log($('#<%= hidParam.ClientID %>').val());
                    //objData = response.GetInitialCacheDataResult;--%>


        //$(document).prop('title', jQuery.parseJSON(objData).Table1[0]["Code"] + "-" + jQuery.parseJSON(objData).Table1[0]["Description"]);
        //if (jQuery.parseJSON(objData).Table.length > 0) {
        //    for (var i = 0; i < jQuery.parseJSON(objData).Table.length; i++) {
        //        $("#dllWareHouse").append($("<option></option>").val(jQuery.parseJSON(objData).Table[i]["Code"]).text(jQuery.parseJSON(objData).Table[i]["Description"].trim()));
        //    }
        //}

        //$("#ddlLocation").append($("<option></option>").val(jQuery.parseJSON(objData).Table1[0]["Code"]).text(jQuery.parseJSON(objData).Table1[0]["Code"].trim()));
        //        },
        //        error: function (response) {
        //            console.log(response.message);
        //        },
        //        failure: function (response) {
        //            console.log(response.message);
        //        }
        //    });
        //}
        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Brand");
                return false;
            }
            //alert('test');
            clearForm();
            $('#<%=rdDiscount.ClientID %>').find("input[value='SellingPrice']").prop("checked", true);
                    DisplayDetails($(source).closest("tr"));


                }

                function DisplayDetails(row) {
                    debugger;
                   <%-- $('#<%=hdfdstatus.ClientID %>').val("EDIT");--%>
                    $('#<%=txtBrandCode.ClientID %>').prop("disabled", true);
                    $('#<%=txtBrandCode.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtBrandName.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtDiscount.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=ddlCategory.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                    $('#<%=txtRemarks.ClientID %>').val(($("td", row).eq(7).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));

                    //  console.log($("td", row).eq(10).html());
                    //if ($("td", row).eq(9).html() == 'SellingPrice') {
                    //    $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
                    //}
                    //else if ($("td", row).eq(9).html() == 'Mrp') {
                    //    $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
                    //}

                    GetLocation();
                    //var checkboxList = $("[id*=cblLocation]");
                    //checkboxList.each(function () {
                    //    if ($(this).val() == $("td", row).eq(11).html()) {
                    //        $(this).prop("checked", true);
                    //    }
                    //});
                }
                function GetLocation() {
                    //alert('test');
                    var BrandCode = $('#<%=txtBrandCode.ClientID %>').val();
                    var dis = $('#<%=txtDiscount.ClientID %>').val();
                    //alert('test');
                    try {
                        $.ajax({
                            url: "frmBrandMaster.aspx/fncGetLoc",
                            data: "{ 'BrandCode': '" + BrandCode + "','dis': '" + dis + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (msg) {
                                objData = jQuery.parseJSON(msg.d);

                                // debugger;                                                    
                                if (objData.length > 0) {
                                    var objPrice = objData[0]["DiscountBasedOn"];
                                    if (objPrice == 'SellingPrice') {
                                        $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
                                    }
                                    else if (objPrice == 'Mrp') {
                                        $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
                                    }
                                    var valNew = objData[0]["LocationCode"].split(',');

                                    var data = valNew.slice(1, 10000);
                                    //console.log($("[id*=cblLocation] input"));
                                    $("[id*=cblLocation] input:checkbox").removeAttr("checked");
                                    var checkboxList = $("[id*=cblLocation]");
                                    var chk = checkboxList.slice(1, 10000);
                                    for (var i = 0; i <= data.length; i++) {


                                        checkboxList.each(function () {
                                            if ($(this).val() == data[i]) {
                                                $(this).prop("checked", true);
                                            }
                                        });
                                        // console.log(checkboxList.length);

                                    }
                                    if (chk.length == data.length) {
                                        $("[id*=cbSelectAll]").attr("checked", "checked");
                                    }
                                }


                            },
                            error: function (msg) {
                                console.log(msg.message);
                            },
                            failure: function (msg) {
                                console.log(msg.message);
                            }
                        });
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }
                $(document).ready(function () {
                    $(document).on('keydown', disableFunctionKeys);
                    //param();
                });
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").prop('checked', false);;
            $('#<%= ddlCategory.ClientID %>').val('');
            $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");

            return false;
        }
    </script>
    <script language="Javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BrandMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BrandMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Brand Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <asp:UpdatePanel ID="updtPnltopbrand" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />



                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:HiddenField ID="hdfdstatus" runat="server" />
                                <asp:Label ID="Label1" runat="server" Text="Brand Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrandCode" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Brand Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrandName" MaxLength="50" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Discount"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDiscount" Text="0" runat="server" MaxLength="18" CssClass="form-control" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Remarks"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRemarks" runat="server" Style="width: 230px !important" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Category"></asp:Label> 
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>

                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm();"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" Style="display: none" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>
                </div>



                <div class="col-md-4 Discount" runat="server" id="divDiscount">
                    <div class="container-group-small" style="width: 100%;">
                        <div class="container-control">
                            <div class="control-group-single-res">

                                <div class="terminal-detail-header">
                                    Discount Based On
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="col-md-8">
                                    <asp:RadioButtonList ID="rdDiscount" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                        <asp:ListItem Value="SellingPrice" Selected="True">Selling Price</asp:ListItem>

                                        <asp:ListItem style="margin-left: 71px" Value="Mrp">MRP</asp:ListItem>

                                    </asp:RadioButtonList>
                                    <%--<asp:RadioButton ID="rdSellingPrice" runat="server" Text="Selling Price" checked="true" />  
                                    <asp:RadioButton ID="rdMRP" runat="server" Text="MRP" Style="margin-left:71px" />  --%>
                                </div>

                            </div>

                            <div class="control-group-single-res">
                                <%--     <div class="label-left">
                                    <asp:Label ID="Label20" runat="server" Text="Location"></asp:Label>
                                </div>--%>
                                <div class="label-right">

                                    <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                </div>
                                <div class="control-group-single">
                                    <div style="overflow-y: auto; width: 75%; height: 90px">
                                        <asp:CheckBoxList ID="cblLocation" CssClass="cbLocation" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updtPnlgrdbrand" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="gridDetails">

                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Brand Name Search"></asp:TextBox>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                        OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>
                             <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                        </asp:Panel>
                    </div>
                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Delete
                                                </th>
                                                <th scope="col">Edit
                                                </th>
                                                 <th scope="col">Serial No
                                                </th>
                                                <th scope="col">Brand Code
                                                </th>
                                                <th scope="col">Brand Name
                                                </th>
                                                <th scope="col">Discount Percent
                                                </th>
                                                <th scope="col">Ref Cate Code
                                                </th>
                                                <th scope="col">Remarks
                                                </th>
                                                <th scope="col">Modify User
                                                </th>
                                                <th scope="col">Modify Date
                                                </th>
                                                <th scope="col">DiscountBasedon
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <%-- <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 205px; width: 1338px; background-color: aliceblue;">
                                        <asp:GridView ID="grdBrandList" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            CssClass="pshro_GridDgn" oncopy="return false" AllowPaging="false" PageSize="8"
                                            DataKeyNames="BrandCode">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Brand_Delete(this); return false;"
                                                            CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                            ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="RowNumber" HeaderText="Serial No"></asp:BoundField>
                                                <asp:BoundField DataField="BrandCode" HeaderText="Brand Code"></asp:BoundField>
                                                <asp:BoundField DataField="BrandName" HeaderText="Brand Name"></asp:BoundField>
                                                <asp:BoundField DataField="DiscountPercent" HeaderText="Discount Percent" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="RefCateCode" HeaderText="Ref Cate Code"></asp:BoundField>
                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                                <asp:BoundField DataField="ModifyUser" HeaderText="Modify User"></asp:BoundField>
                                                <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date"></asp:BoundField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ups:PaginationUserControl runat="server" ID="bradPaging" OnPaginationButtonClick="brandPaging_PaginationButtonClick" />
                </div>
                </div>




                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
                <asp:HiddenField ID="hidParam" runat="server" Value="N" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
