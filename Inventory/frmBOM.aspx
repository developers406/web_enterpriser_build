﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBOM.aspx.cs"
    Inherits="EnterpriserWebFinal.Inventory.frmBOM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 150px;
            max-width: 150px;
        }
        .grdLoad td:nth-child(1){
            text-align:center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 152px;
            max-width: 152px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 585px;
            max-width: 585px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }
        .grdLoad td:nth-child(4) {
            text-align:right !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 192px;
            max-width: 192px;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .btch_left {
            margin-left: 20px;
        }

        .top {
            margin-top: 5px;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }

        .ui-autocomplete {
            max-height: 200px !important;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 20px;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        var lastRowNo = 0;
        function pageLoad() {
             if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            //----------------------------------------------- gvBatchDetails Select -----------------------------------------// 
            $('#<%=txtInventory.ClientID %>').focus();
            fncInitializeBatchDetail();

            $('#<%=txtQty.ClientID %>').number(true, 2);
            $('#<%=txtWeight.ClientID %>').number(true, 2);
            SetAutoCompleteForItemSearch();
            $(function () {
                $("[id*=gvBatchDetails] td").click(function () {
                    DisplayBatch($(this).closest("tr"));
                });
                return false;
            });

            function DisplayBatch(row) {
                try {
                    if ($.trim($("td", row).eq(0).text()) != "")
                        $("[id*=txtBatchNo]").val($("td", row).eq(0).text());
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                    console.log(err);
                }
            }
        }
    </script>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';
            if (document.getElementById("<%=txtInventory.ClientID%>").value == "") {
                Show = Show + "Please Enter InventoryCode"
                document.getElementById("<%=txtInventory.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtQty.ClientID%>").value == "" || document.getElementById("<%=txtQty.ClientID%>").value == "0.00") {
                Show = Show + '\n Please Enter Qty';
                document.getElementById("<%=txtQty.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                //__doPostBack('ctl00$ContentPlaceHolder1$lnkAdd', '');
                fncTblBind();
                return false;
            }
        }

    </script>
    <script type="text/javascript">
        function ValidateFormUpdate() {
            var Show = '';

            if (document.getElementById("<%=ddlBOMInv.ClientID%>").value == "") {
                Show = Show + ' Please Select BOM Inventory Code'
                document.getElementById("<%=ddlBOMInv.ClientID %>").focus();
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                fncSave();
                __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
            }
        }

        function fncClear() {
            $('#<%=txtInventory.ClientID %>').val('');
            $('#<%=txtDescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtQty.ClientID %>').val('0');
            $('#<%=txtInventory.ClientID %>').focus();
        }
        function SetAutoCompleteForItemSearch() {
            $("[id$=txtInventory]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                focus: function (event, i) {
                    $('#<%=txtInventory.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtInventory.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    return false;
                },
                minLength: 1
            });
            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtInventory'
                });
            });
        }
        function fncGetInventoryCode() {
            try {
                var obj = {};
                obj.code = $('#<%=txtInventory.ClientID %>').val();
                obj.formName = 'StkAdj';
                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try {
                var objStk, row;
                var tblBatchBody = $("#tblBatch tbody");
                objStk = jQuery.parseJSON(msg.d);
                if (objStk.length > 0) {
                    $('#<%=txtInventory.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                    $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                    if($.trim(objStk[0]["AllowExpireDate"])=="NonBatch"){
                    $('#<%=txtInventory.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                    $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                    }
                else
                { 
                    tblBatchBody.children().remove();
                    for (var i = 0; i < objStk.length; i++) {
                        row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                            $.trim(objStk[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                            $.trim(objStk[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" + 
                            objStk[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                            objStk[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                            objStk[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                            objStk[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                            $.trim(objStk[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                            $.trim(objStk[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" + 
                           (objStk[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["AllowExpireDate"]) + "</td><td id='tdMRPDEL_ ' style ='display:none'" + i + "' >" +
                           (objStk[i]["MRPDEL"]) + "</td></tr>";
                        tblBatchBody.append(row);
                    } 
                        $("#Bom_Bacth").dialog("open");
                    }
                     
                    tblBatchBody.children().dblclick(fncBatchRowClick);

                    tblBatchBody[0].setAttribute("style", "cursor:pointer");

                    tblBatchBody.children().on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });

                    tblBatchBody.children().on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });

                    tblBatchBody.children().keypress(function (e) {
                        fncBatchKeyDown($(this), e);
                    });
                    $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                    $("#tblBatch tbody > tr").first().focus();

                }
                else {
                    $('#<%=txtQty.ClientID %>').focus().select();
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitializeBatchDetail() {
            try {
                $("#Bom_Bacth").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 250,
                    width: 633,
                    modal: false,
                    title: "Enterpriser Web",
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                            $('#<%=txtQty.ClientID %>').focus().select();
                           }
                       }
                   });
               }
               catch (err) {
                   ShowPopupMessageBox(err.message);
               }
           }
           function fncBatchRowClick() {
               try {
                   fncBatchInventoryValuesToTextBox($.trim($(this).find('td[id*=tdBatchNo]').text()));
                   return false;
               }
               catch (err) {
                   ShowPopupMessageBox(err.message);
               }
           }
           function fncBatchInventoryValuesToTextBox(Batch) {
               debugger;
               $('#<%=txtBatchNo.ClientID %>').val(Batch);
               $("#Bom_Bacth").dialog("close");
               //$('.ui-button').click();
               $('#<%=txtQty.ClientID %>').focus().select();
                return false;
            }
            function fncBatchKeyDown(rowobj, evt) {
                var rowobj, charCode;
                var scrollheight = 0;
                try {
                    //var rowobj = $(this);
                    var charCode = (evt.which) ? evt.which : evt.keyCode;

                    if (charCode == 13) {
                        rowobj.dblclick();
                        return false;
                    }
                    else if (charCode == 40) {

                        var NextRowobj = rowobj.next();
                        if (NextRowobj.length > 0) {
                            NextRowobj.css("background-color", "Yellow");
                            NextRowobj.siblings().css("background-color", "white");
                            NextRowobj.select().focus();

                            setTimeout(function () {
                                scrollheight = $("#tblBatch tbody").scrollTop();
                                if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                    $("#tblBatch tbody").scrollTop(0);
                                }
                                else if (parseFloat(scrollheight) > 10) {
                                    scrollheight = parseFloat(scrollheight) - 10;
                                    $("#tblBatch tbody").scrollTop(scrollheight);
                                }
                            }, 50);
                        }
                    }
                    else if (charCode == 38) {
                        var prevrowobj = rowobj.prev();
                        if (prevrowobj.length > 0) {
                            prevrowobj.css("background-color", "Yellow");
                            prevrowobj.siblings().css("background-color", "white");
                            prevrowobj.select().focus();

                            setTimeout(function () {
                                scrollheight = $("#tblBatch tbody").scrollTop();
                                if (parseFloat(scrollheight) > 3) {
                                    scrollheight = parseFloat(scrollheight) + 1;
                                    $("#tblBatch tbody").scrollTop(scrollheight);
                                }
                            }, 50);

                        }
                    }
                }
                catch (err) {
                    fncToastError(err.message);
                }
            }
            function disableFunctionKeys(e) {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) {
                        $('#<%=lnkSave.ClientID %>').click();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        $('#<%= lnkClearAll.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });


        function fncClearAll() {
            try {
                $("[id*=tblItemhistory] td").remove();
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                $(':checkbox, :radio').prop('checked', false);
                $("input").prop('checked', false);
                $('#<%= ddlBOMInv.ClientID %>').val('');
                $('#<%=ddlBOMInv.ClientID %>').trigger("liszt:updated");
                fncClear();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncTblBind() {
            var tblItem = $("#tblItemhistory tbody");
            try {
                lastRowNo = parseFloat(lastRowNo) + 1;
                var row = "<tr><td>" +
                                    "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                                    $('#<%=txtInventory.ClientID %>').val() + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                                $('#<%=txtDescription.ClientID %>').val() + "</td><td id='tdQty_" + lastRowNo + "' >" +
                                parseFloat($('#<%=txtQty.ClientID %>').val()).toFixed(2) + "</td><td id='tdBatch_" + lastRowNo + "' >" +
                                $('#<%=txtBatchNo.ClientID %>').val() + "</td></tr>";
                tblItem.append(row); 
                $('#tblItemhistory').css("display", "block");
                $('#tblItemhistory thead').css("display", "none");
                fncClear();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBindtbl(Json) {
            var tblValue = "";
            try {
                tblValue = jQuery.parseJSON(Json);
                var tblItem = $("#tblItemhistory tbody");
                lastRowNo = parseFloat(lastRowNo) + 1;
                if (tblValue.length > 0) {
                    tblItem.children().remove();
                    for (var i = 0; i < tblValue.length; i++) {
                        var row = "<tr><td>" +
                                           "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                                           tblValue[i]["InventoryCode"] + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                                            tblValue[i]["Description"] + "</td><td id='tdQty_" + lastRowNo + "' >" +
                                            parseFloat(tblValue[i]["Qty"]).toFixed(2) + "</td><td id='tdBatch_" + lastRowNo + "' >" +
                                            tblValue[i]["BatchNo"] + "</td></tr>";
                        tblItem.append(row); 
                    }
                    $('#tblItemhistory').css("display", "block");
                    $('#tblItemhistory thead').css("display", "none");
                    fncClear();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDeleteItem(rowobj) {
            $(rowobj).parents("tr").remove();
        }
        function fncSave() {
            try{
                 var xml = '<NewDataSet>';
                 $("#tblItemhistory tbody").children().each(function () {
              obj = $(this);

              if (parseFloat(obj.find('td[id*=tdQty]').text()) > 0) {

                  xml += "<BOMDetail>";
                  xml += "<InventoryCode>" + obj.find('td[id*=tdItemcode]').text() + "</InventoryCode>";
                  xml += "<Description>" + obj.find('td[id*=tdDesc]').text() + "</Description>";
                  xml += "<BatchNo>" + obj.find('td[id*=tdBatch]').text() + "</BatchNo>";
                  xml += "<Qty>" + obj.find('td[id*=tdQty]').text() + "</Qty>";
                  xml += "</BOMDetail>";
              }
          });
          xml = xml + '</NewDataSet>'
          xml = escape(xml);
           $('#<%=hidXML.ClientID %>').val(xml); 
            return xml;
            console.log(xml);
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'BOM');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "BOM";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">BOM Inventory</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="top-container-pi-header">
                    BOM Header
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label4" runat="server" Text="BOM Inv"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlBOMInv" runat="server" CssClass="form-control-res" OnSelectedIndexChanged="ddlBOMInv_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="col-lg-push-8"></div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtBOMDesc" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label6" runat="server" Text="Remarks"></asp:Label>
                    </div>
                    <div class="col-md-5">
                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-4">
                        <div class="col-md-3" style="margin-left: -5px">
                            <asp:Label ID="Label1" runat="server" Text="Package"></asp:Label>
                        </div>
                        <div class="col-md-3" style="margin-left: 3px">
                            <asp:TextBox ID="txtbomPack" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-3">
                            <asp:Label ID="Label3" runat="server" Text="Weight"></asp:Label>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtWeight" Text="0" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>

                <div class="right-container-top-detail">
                    <div class="GridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30 top">
                                <div class="grdLoad Payment_fixed_headers"> 
                                    <table id="tblItemhistoryHead" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Code
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Qty
                                                </th>
                                                <th scope="col">BatchNo
                                                </th>
                                            </tr>
                                        </thead> 
                                    </table>
                                    <%--<div class="grid-overflow-height grdLoad" style="overflow: auto; height: 140px">--%>
                                    <%--<div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 110px; width: 1238px; background-color: aliceblue;">
                                        <asp:GridView ID="gvBOMInv" runat="server" AutoGenerateColumns="False" ShowHeader="false" ShowHeaderWhenEmpty="True"
                                            OnRowDeleting="gvBOMInv_OnRowDeleting" OnRowDataBound="gvBOMInv_OnRowDataBound"
                                            CssClass="pshro_GridDgn grdLoad">
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Remove" />
                                                <asp:BoundField DataField="InventoryCode" HeaderText="Code"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                <asp:BoundField DataField="Qty" HeaderText="Qty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                            </Columns>

                                        </asp:GridView>
                                    </div>--%>
                                </div>
                                <div  class="grdLoad Payment_fixed_headers" style="height: 200px; width :93% !important;overflow-y:hidden;overflow-x:hidden;"> 
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="display_none">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Code
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Qty
                                                </th>
                                                <th scope="col">BatchNo
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-container-pi">
                    <div class="bottom-container-pi-header">
                        BOM Detail
                    </div>
                    <div id="divItemAddSection" runat="server">
                        <div class="stockAdjustment_itemcode" style="width: 12%">
                            <div>
                                <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtInventory" runat="server" CssClass="form-control-res" onchange="return fncGetInventoryCode(); return false;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_Description" style="width: 40%">
                            <div>
                                <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_itemcode">
                            <div>
                                <asp:Label ID="lblBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_itemcode">
                            <div class="spilt" align="center">
                                <asp:Label ID="lblQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div class="spilt">
                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res-right">0</asp:TextBox>
                            </div>
                        </div>

                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;"
                                Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick="return ValidateForm();"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='Clear'
                                OnClientClick="fncClear();return false;"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button">
                        </div>
                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Style="padding-left: 15px; padding-right: 15px;"
                                Text="Save(F4)" OnClientClick="return ValidateFormUpdate();" OnClick="lnkSave_Click"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button" style="margin-left: 25px">
                            <asp:LinkButton ID="lnkClearAll" runat="server" class="button-red"
                                OnClientClick="fncClearAll();return false;" Style="padding-left: 15px; padding-right: 15px;" Text='Clear(F6)'></asp:LinkButton>
                        </div>

                    </div>

                    <div class="hiddencol">
                        <div id="Bom_Bacth">
                            <div class="Payment_fixed_headers BatchDetail">
                                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th id="thBatchNo" scope="col">BatchNo
                                            </th>
                                            <th scope="col">Stock
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">MRP
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">InventoryCode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <asp:HiddenField ID="hidXML" runat="server" Value="2" />
                        <div class="display_none">
                            <button id="closeButton">Close Partial</button>
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>

