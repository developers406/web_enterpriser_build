﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInventoryMasterTextiles.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryMasterTextiles" %>

<%@ Register TagPrefix="ups" TagName="ItemHistory" Src="~/UserControls/ItemHistory.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        #grdAttributeValue {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #grdAttributeValuehead {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #grdAttributeValue td, #customers th {
            border: 1px solid #ddd;
            padding: 4px;
        }

        #grdAttributeValue tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        #grdAttributeValue tr:hover {
            background-color: #ddd;
        }

        #grdAttributeValuehead th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left;
            background-color: red;
            font-weight: bolder;
            color: white;
        }

        #grdAttributeValue th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left;
            background-color: red;
            font-weight: bolder;
            color: white;
        }

        /*Attribute Count Table */
        #grdAttributeOriginal {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeOriginal td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeOriginal tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdAttributeOriginal tr:hover {
                background-color: #ddd;
            }

            #grdAttributeOriginal th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: crimson;
                font-weight: bolder;
                color: white;
            }


        #grdModel {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdModel td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
                /*text-align:center;*/
            }

            #grdModel tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdModel tr:hover {
                background-color: #ddd;
            }

            #grdModel th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: rebeccapurple;
                font-weight: bolder;
                color: white;
            }

        #grdAttributeCount {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttributeCount td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttributeCount tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdAttributeCount tr:hover {
                background-color: #ddd;
            }

            #grdAttributeCount th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: orangered;
                font-weight: bolder;
                color: white;
            }
        /*Attribute Count Table END */

        #grdAttribute {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

            #grdAttribute td, #customers th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #grdAttribute tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #grdAttribute tr:hover {
                background-color: #ddd;
            }

            #grdAttribute th {
                padding-top: 4px;
                padding-bottom: 4px;
                text-align: left;
                background-color: forestgreen;
                font-weight: bolder;
                color: white;
            }

        .container {
            overflow: hidden;
        }

        .tab {
            float: left;
        }

        .tab-btn {
            margin: 50px;
        }

        /*button{display:block;margin-bottom: 20px;}
            tr{transition:all .25s ease-in-out}
            tr:hover{background-color: #ddd;}*/

        iframe {
            overflow: hidden;
        }

        .chzn-disabled {
            cursor: default;
            opacity: 0.9 !important;
        }

        .ui-autocomplete {
            z-index: 9999 !important;
        }

        #dialog-ViewItemHistory {
            width: 100% !important;
        }

        .container-inv-history {
            width: 100% !important;
        }

        .cust-pages {
            display: none !important;
        }

        .breakPrice td:nth-child(1), .breakPrice th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .breakPrice td:nth-child(2), .breakPrice th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .breakPrice td:nth-child(3), .breakPrice th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(4), .breakPrice th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(5), .breakPrice th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(6), .breakPrice th:nth-child(6) {
            min-width: 110px;
            max-width: 110px;
        }

        .breakPrice td:nth-child(7), .breakPrice th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .breakPrice td:nth-child(8), .breakPrice th:nth-child(8) {
            display: none;
        }

        .tbl_barcode td:nth-child(1), .tbl_barcode th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2) {
            min-width: 260px;
            max-width: 260px;
        }

        .newvendor td:nth-child(1), .newvendor th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .newvendor td:nth-child(1) {
            text-align: center !important;
        }

        .newvendor td:nth-child(2), .newvendor th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .newvendor td:nth-child(3), .newvendor th:nth-child(3) {
            min-width: 250px;
            max-width: 250px;
        }

        .newvendor td:nth-child(4), .newvendor th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .newvendor td:nth-child(4) {
            text-align: center !important;
        }

        .poList td:nth-child(1), .poList th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .poList td:nth-child(2), .poList th:nth-child(2) {
            min-width: 350px;
            max-width: 350px;
        }

        .poList td:nth-child(3), .poList th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .poList td:nth-child(4), .poList th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .poList td:nth-child(5), .poList th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(5) {
            text-align: right !important;
        }

        .poList td:nth-child(6), .poList th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(6) {
            text-align: right !important;
        }

        .poList td:nth-child(7), .poList th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(7) {
            text-align: right !important;
        }

        .poList td:nth-child(8), .poList th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(8) {
            text-align: right !important;
        }

        .poList td:nth-child(9), .poList th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(9) {
            text-align: right !important;
        }

        .poList td:nth-child(10), .poList th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .poList td:nth-child(10) {
            text-align: right !important;
        }

        .saleshistory td:nth-child(1), .saleshistory th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .saleshistory td:nth-child(1) {
            text-align: left !important;
        }

        .saleshistory td:nth-child(2), .saleshistory th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .saleshistory td:nth-child(2) {
            text-align: left !important;
        }

        .saleshistory td:nth-child(3), .saleshistory th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(4), .saleshistory th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(5), .saleshistory th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(6), .saleshistory th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(7), .saleshistory th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(8), .saleshistory th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(9), .saleshistory th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(10), .saleshistory th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(11), .saleshistory th:nth-child(11) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(12), .saleshistory th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(13), .saleshistory th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(14), .saleshistory th:nth-child(14) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(15), .saleshistory th:nth-child(15) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(16), .saleshistory th:nth-child(16) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(17), .saleshistory th:nth-child(17) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(18), .saleshistory th:nth-child(18) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(19), .saleshistory th:nth-child(19) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(21), .saleshistory th:nth-child(21) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(22), .saleshistory th:nth-child(22) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(23), .saleshistory th:nth-child(23) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(24), .saleshistory th:nth-child(24) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(25), .saleshistory th:nth-child(25) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(26), .saleshistory th:nth-child(26) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(27), .saleshistory th:nth-child(27) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(28), .saleshistory th:nth-child(28) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(29), .saleshistory th:nth-child(29) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(30), .saleshistory th:nth-child(30) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(31), .saleshistory th:nth-child(31) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(32), .saleshistory th:nth-child(32) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(33), .saleshistory th:nth-child(33) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(34), .saleshistory th:nth-child(34) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(35), .saleshistory th:nth-child(35) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(36), .saleshistory th:nth-child(36) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(37), .saleshistory th:nth-child(37) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(38), .saleshistory th:nth-child(38) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(39), .saleshistory th:nth-child(39) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(40), .saleshistory th:nth-child(40) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(41), .saleshistory th:nth-child(41) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(42), .saleshistory th:nth-child(42) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(43), .saleshistory th:nth-child(43) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(20), .saleshistory th:nth-child(20) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(44), .saleshistory th:nth-child(44) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(45), .saleshistory th:nth-child(45) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(46), .saleshistory th:nth-child(46) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(47), .saleshistory th:nth-child(47) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(48), .saleshistory th:nth-child(48) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(49), .saleshistory th:nth-child(49) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(50), .saleshistory th:nth-child(50) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(51), .saleshistory th:nth-child(51) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(52), .saleshistory th:nth-child(52) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(53), .saleshistory th:nth-child(53) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(54), .saleshistory th:nth-child(54) {
            min-width: 80px;
            max-width: 80px;
        }

        .saleshistory td:nth-child(55), .saleshistory th:nth-child(55) {
            min-width: 80px;
            max-width: 80px;
        }

        .childitems td:nth-child(1), .childitems th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .childitems td:nth-child(2), .childitems th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(3), .childitems th:nth-child(3) {
            min-width: 300px;
            max-width: 300px;
        }

        .childitems td:nth-child(4), .childitems th:nth-child(4) {
            min-width: 50px;
            max-width: 50px;
        }

        .childitems td:nth-child(5), .childitems th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(5) {
            text-align: right;
        }

        .childitems td:nth-child(6), .childitems th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .childitems td:nth-child(6) {
            text-align: right;
        }
    </style>
    <%-------------------------------------------- Common Declartion -----------------------------------------%>
    <script type="text/javascript">
        var ddlInputSGST;
        var ddlInputCGST;
        var ddlInputIGST;
        var ddlOutputSGST;
        var ddlOutputCGST;
        var ddlOutputIGST;
        var txtCESSPrc;
        var txtHSNCode;
        var txtMRP;
        var txtMRPMarkDown;
        var txtBasicCost;
        var txtDiscountPrc1;
        var txtDiscountPrc2;
        var txtDiscountPrc3;
        var txtDiscountAmt;
        var txtGrossCost;
        var txtInputSGSTPrc;
        var txtInputSGSTAmt;
        var txtInputCGSTPrc;
        var txtInputCGSTAmt;
        var txtInputCESSPrc;
        var txtInputCESSAmt;
        var txtNetCost;
        var txtMarginMRP;
        var txtMarginSP;
        var txtFixedMargin;
        var txtMRPAmt;
        var txtSPAmt;
        var txtProfitMRPPrc;
        var txtProfitSPPrc;
        var txtBasicSelling;
        var txtOutputSGSTPrc;
        var txtOutputSGSTAmt;
        var txtOutputCGSTPrc;
        var txtOutputCGSTAmt;
        var txtOutputCESSPrc;
        var txtOutputCESSAmt;
        var txtSellingPrice;
        var txtVatLiabilitySGSTPrc;
        var txtVatLiabilitySGSTAmt;
        var txtVatLiabilityCGSTPrc;
        var txtVatLiabilityCGSTAMt;
        var txtWholePrice1Popup;
        var txtWholePrice2Popup;
        var txtWholePrice3Popup;
        var txtMarginWPrice1;
        var txtMarginWPrice2;
        var txtMarginWPrice3;
        var txtMinimumQty;
        var txtMaxQty;
        var txtReOrderQty;
        var breakRowNo = 1;
        var breakRowPrice;
        var breakPriceBody;
        var AttributrClear = "";

        var NetSellingprice = 0;


    </script>
    <%-------------------------------------------- Page Load -----------------------------------------%>
    <script type="text/javascript">


        function LoadModel(show) {
            debugger;
            var Attr = [10];
            var AttrCode = [10];
            var ValCode = [10];

            var iCountOrnl = 0;
            var iCount = 1;
            var iCount1 = 0;
            var iCount2 = 0;
            var iCount3 = 0;
            var iCount4 = 0;
            var iCount5 = 0;
            var iCount6 = 0;
            var iCount7 = 0;
            var iCount8 = 0;
            var iCount9 = 0;
            var iCount10 = 0;


            var sDesc1 = '';
            var sDesc2 = '';
            var sDesc3 = '';
            var sDesc4 = '';
            var sDesc5 = '';
            var sDesc6 = '';
            var sDesc7 = '';
            var sDesc8 = '';
            var sDesc9 = '';
            var sDesc10 = '';

            var grdModels = document.getElementById("grdModel");
            var rowCount = 0;
            rowCount = $('table#grdAttributeCount tr:last').index()
            //if (AttributrClear == "1") {
            //    rowCount = rowCount - 1;
            //}
            var grdAttcount = document.getElementById("grdAttributeCount");
            var grdAttb = document.getElementById("grdAttribute");
            var grdAttbOrg = document.getElementById("grdAttributeOriginal");

            $('#grdModel').find("tr:gt(0)").remove();
            $('#grdAttributeOriginal').find("tr:gt(0)").remove();

            try {

                if (rowCount > 0) {

                    $('#grdAttributeCount tr').each(function () {
                        if (!this.rowIndex) return;
                        Attr[this.rowIndex - 1] = $(this).find("td").eq(3).html();
                    });

                    //var text = '';
                    //for (i = 0; i < Attr.length; i++) {
                    //    text += Attr[i] + "<br>";
                    //} 

                    if (rowCount === 1) {
                        $('#grdAttribute tr').each(function () {
                            if (!this.rowIndex) return;
                            sDesc1 = $(this).find("td").eq(4).html();// + '' + $(this).find("td").eq(4).html(); //$(this).find("td:last").html(); 
                            var newRow = grdModels.insertRow(grdModels.length),
                                cell1 = newRow.insertCell(0),
                                cell2 = newRow.insertCell(1);

                            cell2.style = "text-align:center";
                            // add values to the cells   
                            cell1.innerHTML = sDesc1;
                            cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";

                            var rowCountModel = $('table#grdModel tr:last').index();

                            var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                orgcell1 = newRowOrg.insertCell(0),
                                orgcell2 = newRowOrg.insertCell(1),
                                orgcell3 = newRowOrg.insertCell(2);
                            // add values to the cells   
                            orgcell1.innerHTML = rowCountModel;
                            orgcell2.innerHTML = $(this).find("td").eq(1).html();
                            orgcell3.innerHTML = $(this).find("td").eq(3).html();
                        });
                    }

                    if (rowCount === 2) {

                        for (iCount1 = 0; iCount1 < parseInt(Attr[0]) + 1; iCount1++) {
                            if (iCount1 != 0) {
                                var oCell1 = grdAttb.rows.item(iCount1).cells;
                                sDesc1 = oCell1[4].firstChild.data;// + ' ' + oCell1[4].firstChild.data;
                                AttrCode[0] = oCell1[1].firstChild.data;
                                ValCode[0] = oCell1[3].firstChild.data;
                                var Counts = parseInt(Attr[0]) + parseInt(Attr[1]);
                                //alert(Counts);
                                for (iCount2 = parseInt(Attr[0]) + 1; iCount2 < Counts + 1; iCount2++) {
                                    //alert(iCount2);
                                    var oCell2 = grdAttb.rows.item(iCount2).cells;
                                    sDesc2 = oCell2[4].firstChild.data;// + ' ' + oCell2[4].firstChild.data;
                                    //alert(sDesc1 + ' ' + sDesc2);
                                    AttrCode[1] = oCell2[1].firstChild.data;
                                    ValCode[1] = oCell2[3].firstChild.data;
                                    var newRow = grdModels.insertRow(grdModels.length),
                                        cell1 = newRow.insertCell(0),
                                        cell2 = newRow.insertCell(1);
                                    cell2.style = "text-align:center";
                                    cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";
                                    // add values to the cells  
                                    cell1.innerHTML = sDesc1 + ' ' + sDesc2;
                                    var rowCountModel = $('table#grdModel tr:last').index();
                                    for (i = 0; i <= 1; i++) {
                                        //text += AttrCode[i] + "<br>"; 
                                        //alert(AttrCode[i] + '  ' + ValCode[i]);
                                        var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                            orgcell1 = newRowOrg.insertCell(0),
                                            orgcell2 = newRowOrg.insertCell(1),
                                            orgcell3 = newRowOrg.insertCell(2);
                                        // add values to the cells   
                                        orgcell1.innerHTML = rowCountModel;
                                        orgcell2.innerHTML = AttrCode[i];
                                        orgcell3.innerHTML = ValCode[i];
                                    }

                                }
                            }

                            //var text = '';
                            //for (i = 0; i <= 1; i++) {
                            //    text += AttrCode[i] + "<br>";
                            //}
                            //alert(text);
                        }

                        //var index = $('yourtable th:contains("ID")').index();
                        //alert($('.selectedRow td:eq(' + index + ')').html()); 
                    }

                    if (rowCount === 3) {

                        for (iCount1 = 0; iCount1 < parseInt(Attr[0]) + 1; iCount1++) {
                            if (iCount1 != 0) {
                                var oCell1 = grdAttb.rows.item(iCount1).cells;
                                sDesc1 = oCell1[4].firstChild.data;// + ' ' + oCell1[4].firstChild.data;
                                AttrCode[0] = oCell1[1].firstChild.data;
                                ValCode[0] = oCell1[3].firstChild.data;
                                var Counts2 = parseInt(Attr[0]) + parseInt(Attr[1]);
                                //alert(Counts);
                                for (iCount2 = parseInt(Attr[0]) + 1; iCount2 < Counts2 + 1; iCount2++) {
                                    //alert(iCount2);
                                    var oCell2 = grdAttb.rows.item(iCount2).cells;
                                    sDesc2 = oCell2[4].firstChild.data;// + ' ' + oCell2[4].firstChild.data;
                                    //alert(sDesc1 + ' ' + sDesc2);
                                    AttrCode[1] = oCell2[1].firstChild.data;
                                    ValCode[1] = oCell2[3].firstChild.data;
                                    var Counts3 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]);
                                    //alert(Counts3);
                                    //For iCount3 = Attr(0) + Attr(1) + 1 To Attr(0) + Attr(1) + Attr(2)
                                    for (iCount3 = parseInt(Attr[0]) + parseInt(Attr[1]) + 1; iCount3 < Counts3 + 1; iCount3++) {
                                        //alert(iCount3);
                                        var oCell3 = grdAttb.rows.item(iCount3).cells;
                                        sDesc3 = oCell3[4].firstChild.data;// + ' ' + oCell3[4].firstChild.data;
                                        //alert(sDesc1 + ' ' + sDesc2);
                                        AttrCode[2] = oCell3[1].firstChild.data;
                                        ValCode[2] = oCell3[3].firstChild.data;
                                        var newRow = grdModels.insertRow(grdModels.length),
                                            cell1 = newRow.insertCell(0),
                                            cell2 = newRow.insertCell(1);
                                        cell2.style = "text-align:center";
                                        cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";
                                        // add values to the cells  
                                        cell1.innerHTML = sDesc1 + ' ' + sDesc2 + ' ' + sDesc3;
                                        var rowCountModel = $('table#grdModel tr:last').index();
                                        for (i = 0; i <= rowCount - 1; i++) {
                                            //text += AttrCode[i] + "<br>"; 
                                            //alert(AttrCode[i] + '  ' + ValCode[i]);
                                            var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                                orgcell1 = newRowOrg.insertCell(0),
                                                orgcell2 = newRowOrg.insertCell(1),
                                                orgcell3 = newRowOrg.insertCell(2);
                                            // add values to the cells   
                                            orgcell1.innerHTML = rowCountModel;
                                            orgcell2.innerHTML = AttrCode[i];
                                            orgcell3.innerHTML = ValCode[i];
                                        }
                                    }
                                }
                            }
                        }

                    }

                    if (rowCount === 4) {

                        for (iCount1 = 0; iCount1 < parseInt(Attr[0]) + 1; iCount1++) {
                            if (iCount1 != 0) {
                                var oCell1 = grdAttb.rows.item(iCount1).cells;
                                sDesc1 = oCell1[4].firstChild.data;// + ' ' + oCell1[4].firstChild.data;
                                AttrCode[0] = oCell1[1].firstChild.data;
                                ValCode[0] = oCell1[3].firstChild.data;
                                var Counts2 = parseInt(Attr[0]) + parseInt(Attr[1]);
                                //alert(Counts);
                                for (iCount2 = parseInt(Attr[0]) + 1; iCount2 < Counts2 + 1; iCount2++) {
                                    //alert(iCount2);
                                    var oCell2 = grdAttb.rows.item(iCount2).cells;
                                    sDesc2 = oCell2[4].firstChild.data;// + ' ' + oCell2[4].firstChild.data;
                                    //alert(sDesc1 + ' ' + sDesc2);
                                    AttrCode[1] = oCell2[1].firstChild.data;
                                    ValCode[1] = oCell2[3].firstChild.data;
                                    var Counts3 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]);
                                    //alert(Counts3);
                                    //For iCount3 = Attr(0) + Attr(1) + 1 To Attr(0) + Attr(1) + Attr(2)
                                    for (iCount3 = parseInt(Attr[0]) + parseInt(Attr[1]) + 1; iCount3 < Counts3 + 1; iCount3++) {
                                        //alert(iCount3);
                                        var oCell3 = grdAttb.rows.item(iCount3).cells;
                                        sDesc3 = oCell3[4].firstChild.data;// + ' ' + oCell3[4].firstChild.data;
                                        //alert(sDesc1 + ' ' + sDesc2);
                                        AttrCode[2] = oCell3[1].firstChild.data;
                                        ValCode[2] = oCell3[3].firstChild.data;
                                        var Counts4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]);
                                        //alert(Counts4);
                                        for (iCount4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + 1; iCount4 < Counts4 + 1; iCount4++) {
                                            var oCell4 = grdAttb.rows.item(iCount4).cells;
                                            sDesc4 = oCell4[4].firstChild.data;// + ' ' + oCell4[4].firstChild.data;
                                            //alert(sDesc1 + ' ' + sDesc2);
                                            AttrCode[3] = oCell4[1].firstChild.data;
                                            ValCode[3] = oCell4[3].firstChild.data;
                                            var newRow = grdModels.insertRow(grdModels.length),
                                                cell1 = newRow.insertCell(0),
                                                cell2 = newRow.insertCell(1);
                                            cell2.style = "text-align:center";
                                            cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";
                                            // add values to the cells  
                                            cell1.innerHTML = sDesc1 + ' ' + sDesc2 + ' ' + sDesc3 + ' ' + sDesc4;
                                            var rowCountModel = $('table#grdModel tr:last').index();
                                            for (i = 0; i <= rowCount - 1; i++) {
                                                //text += AttrCode[i] + "<br>"; 
                                                //alert(AttrCode[i] + '  ' + ValCode[i]);
                                                var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                                    orgcell1 = newRowOrg.insertCell(0),
                                                    orgcell2 = newRowOrg.insertCell(1),
                                                    orgcell3 = newRowOrg.insertCell(2);
                                                // add values to the cells   
                                                orgcell1.innerHTML = rowCountModel;
                                                orgcell2.innerHTML = AttrCode[i];
                                                orgcell3.innerHTML = ValCode[i];
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (rowCount === 5) {

                        for (iCount1 = 0; iCount1 < parseInt(Attr[0]) + 1; iCount1++) {
                            if (iCount1 != 0) {
                                var oCell1 = grdAttb.rows.item(iCount1).cells;
                                sDesc1 = oCell1[4].firstChild.data;// + ' ' + oCell1[4].firstChild.data;
                                AttrCode[0] = oCell1[1].firstChild.data;
                                ValCode[0] = oCell1[3].firstChild.data;
                                var Counts2 = parseInt(Attr[0]) + parseInt(Attr[1]);
                                //alert(Counts);
                                for (iCount2 = parseInt(Attr[0]) + 1; iCount2 < Counts2 + 1; iCount2++) {
                                    //alert(iCount2);
                                    var oCell2 = grdAttb.rows.item(iCount2).cells;
                                    sDesc2 = oCell2[4].firstChild.data;// + ' ' + oCell2[4].firstChild.data;
                                    //alert(sDesc1 + ' ' + sDesc2);
                                    AttrCode[1] = oCell2[1].firstChild.data;
                                    ValCode[1] = oCell2[3].firstChild.data;
                                    var Counts3 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]);
                                    //alert(Counts3);
                                    //For iCount3 = Attr(0) + Attr(1) + 1 To Attr(0) + Attr(1) + Attr(2)
                                    for (iCount3 = parseInt(Attr[0]) + parseInt(Attr[1]) + 1; iCount3 < Counts3 + 1; iCount3++) {
                                        //alert(iCount3);
                                        var oCell3 = grdAttb.rows.item(iCount3).cells;
                                        sDesc3 = oCell3[4].firstChild.data;// + ' ' + oCell3[4].firstChild.data;
                                        //alert(sDesc1 + ' ' + sDesc2);
                                        AttrCode[2] = oCell3[1].firstChild.data;
                                        ValCode[2] = oCell3[3].firstChild.data;
                                        var Counts4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]);
                                        //alert(Counts4);
                                        for (iCount4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + 1; iCount4 < Counts4 + 1; iCount4++) {
                                            var oCell4 = grdAttb.rows.item(iCount4).cells;
                                            sDesc4 = oCell4[4].firstChild.data;// + ' ' + oCell4[4].firstChild.data;
                                            //alert(sDesc1 + ' ' + sDesc2);
                                            AttrCode[3] = oCell4[1].firstChild.data;
                                            ValCode[3] = oCell4[3].firstChild.data;
                                            var Counts5 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + parseInt(Attr[4]);
                                            for (iCount5 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + 1; iCount5 < Counts5 + 1; iCount5++) {
                                                var oCell5 = grdAttb.rows.item(iCount5).cells;
                                                sDesc5 = oCell5[4].firstChild.data;// + ' ' + oCell5[4].firstChild.data;
                                                //alert(sDesc1 + ' ' + sDesc2);
                                                AttrCode[4] = oCell5[1].firstChild.data;
                                                ValCode[4] = oCell5[3].firstChild.data;
                                                var newRow = grdModels.insertRow(grdModels.length),
                                                    cell1 = newRow.insertCell(0),
                                                    cell2 = newRow.insertCell(1);
                                                cell2.style = "text-align:center";
                                                cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";
                                                // add values to the cells  
                                                cell1.innerHTML = sDesc1 + ' ' + sDesc2 + ' ' + sDesc3 + ' ' + sDesc4 + ' ' + sDesc5;
                                                var rowCountModel = $('table#grdModel tr:last').index();
                                                for (i = 0; i <= rowCount - 1; i++) {
                                                    //text += AttrCode[i] + "<br>"; 
                                                    //alert(AttrCode[i] + '  ' + ValCode[i]);
                                                    var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                                        orgcell1 = newRowOrg.insertCell(0),
                                                        orgcell2 = newRowOrg.insertCell(1),
                                                        orgcell3 = newRowOrg.insertCell(2);
                                                    // add values to the cells   
                                                    orgcell1.innerHTML = rowCountModel;
                                                    orgcell2.innerHTML = AttrCode[i];
                                                    orgcell3.innerHTML = ValCode[i];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (rowCount === 6) {

                        for (iCount1 = 0; iCount1 < parseInt(Attr[0]) + 1; iCount1++) {
                            if (iCount1 != 0) {
                                var oCell1 = grdAttb.rows.item(iCount1).cells;
                                sDesc1 = oCell1[4].firstChild.data;// + ' ' + oCell1[4].firstChild.data;
                                AttrCode[0] = oCell1[1].firstChild.data;
                                ValCode[0] = oCell1[3].firstChild.data;
                                var Counts2 = parseInt(Attr[0]) + parseInt(Attr[1]);
                                //alert(Counts);
                                for (iCount2 = parseInt(Attr[0]) + 1; iCount2 < Counts2 + 1; iCount2++) {
                                    //alert(iCount2);
                                    var oCell2 = grdAttb.rows.item(iCount2).cells;
                                    sDesc2 = oCell2[4].firstChild.data;// + ' ' + oCell2[4].firstChild.data;
                                    //alert(sDesc1 + ' ' + sDesc2);
                                    AttrCode[1] = oCell2[1].firstChild.data;
                                    ValCode[1] = oCell2[3].firstChild.data;
                                    var Counts3 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]);
                                    //alert(Counts3);
                                    //For iCount3 = Attr(0) + Attr(1) + 1 To Attr(0) + Attr(1) + Attr(2)
                                    for (iCount3 = parseInt(Attr[0]) + parseInt(Attr[1]) + 1; iCount3 < Counts3 + 1; iCount3++) {
                                        //alert(iCount3);
                                        var oCell3 = grdAttb.rows.item(iCount3).cells;
                                        sDesc3 = oCell3[4].firstChild.data;// + ' ' + oCell3[4].firstChild.data;
                                        //alert(sDesc1 + ' ' + sDesc2);
                                        AttrCode[2] = oCell3[1].firstChild.data;
                                        ValCode[2] = oCell3[3].firstChild.data;
                                        var Counts4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]);
                                        //alert(Counts4);
                                        for (iCount4 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + 1; iCount4 < Counts4 + 1; iCount4++) {
                                            var oCell4 = grdAttb.rows.item(iCount4).cells;
                                            sDesc4 = oCell4[4].firstChild.data;// + ' ' + oCell4[4].firstChild.data;
                                            //alert(sDesc1 + ' ' + sDesc2);
                                            AttrCode[3] = oCell4[1].firstChild.data;
                                            ValCode[3] = oCell4[3].firstChild.data;
                                            var Counts5 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + parseInt(Attr[4]);
                                            for (iCount5 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + 1; iCount5 < Counts5 + 1; iCount5++) {
                                                var oCell5 = grdAttb.rows.item(iCount5).cells;
                                                sDesc5 = oCell5[4].firstChild.data;// + ' ' + oCell5[4].firstChild.data;
                                                //alert(sDesc1 + ' ' + sDesc2);
                                                AttrCode[4] = oCell5[1].firstChild.data;
                                                ValCode[4] = oCell5[3].firstChild.data;
                                                var Counts6 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + parseInt(Attr[4]) + parseInt(Attr[5]);
                                                for (iCount6 = parseInt(Attr[0]) + parseInt(Attr[1]) + parseInt(Attr[2]) + parseInt(Attr[3]) + parseInt(Attr[4]) + 1; iCount6 < Counts6 + 1; iCount6++) {
                                                    var oCell6 = grdAttb.rows.item(iCount6).cells;
                                                    sDesc6 = oCell6[4].firstChild.data;// + ' ' + oCell6[4].firstChild.data;
                                                    //alert(sDesc1 + ' ' + sDesc2);
                                                    AttrCode[5] = oCell6[1].firstChild.data;
                                                    ValCode[5] = oCell6[3].firstChild.data;
                                                    var newRow = grdModels.insertRow(grdModels.length),
                                                        cell1 = newRow.insertCell(0),
                                                        cell2 = newRow.insertCell(1);
                                                    cell2.style = "text-align:center";
                                                    cell2.innerHTML = "<input type='checkbox' name='check-model' checked>";
                                                    // add values to the cells  
                                                    cell1.innerHTML = sDesc1 + ' ' + sDesc2 + ' ' + sDesc3 + ' ' + sDesc4 + ' ' + sDesc5 + ' ' + sDesc6;
                                                    var rowCountModel = $('table#grdModel tr:last').index();
                                                    for (i = 0; i <= rowCount - 1; i++) {
                                                        //text += AttrCode[i] + "<br>"; 
                                                        //alert(AttrCode[i] + '  ' + ValCode[i]);
                                                        var newRowOrg = grdAttbOrg.insertRow(grdAttbOrg.length),
                                                            orgcell1 = newRowOrg.insertCell(0),
                                                            orgcell2 = newRowOrg.insertCell(1),
                                                            orgcell3 = newRowOrg.insertCell(2);
                                                        // add values to the cells   
                                                        orgcell1.innerHTML = rowCountModel;
                                                        orgcell2.innerHTML = AttrCode[i];
                                                        orgcell3.innerHTML = ValCode[i];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    XmlGridValue($('#grdModel tr'), $('#hidengrdModel'), 'ItemName');
                    XmlGridValue($('#grdAttribute tr'), $('#hidengrdAttribute'), '');
                    XmlGridValue($('#grdAttributeCount tr'), $('#hidengrdAttributeCount'), '');
                    XmlGridValue($('#grdAttributeOriginal tr'), $('#hidengrdAttributeOriginal'), '');

                    if (show == 'show')
                        $("#divModelView").dialog("open");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

            return false;
        }


        function XmlGridValue(tableid, XmlID, val) {
            try {

                var xml = '<NewDataSet>';

                // $('#grdAttribute tr').each(function () {
                tableid.each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {

                        xml += "<Table>";
                        for (var j = 0; j < cells.length; ++j) {

                            if ($(this).parents('table:first').find('th').eq(j).text().indexOf('Select') != -1) {
                                //alert($("input:checkbox", cells.eq(j)).val());
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + $("input:checkbox", cells.eq(j)).is(':checked') + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }
                            else {
                                xml += '<' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>' + cells.eq(j).text().trim() + '</' + $(this).parents('table:first').find('th').eq(j).text().trim() + '>';
                            }

                            if (val == "ItemName") {
                                var Name = $('#<%=txtItemName.ClientID %>').val() + " " + cells.eq(j).text().trim();
                                if (ItemNameVlidateValue == "0")
                                    fncValidateItemName(Name);
                                else
                                    return false;
                            }
                        }
                        xml += "</Table>";
                    }
                });

                xml = xml + '</NewDataSet>'
                console.log(xml);
                //alert(xml);

                XmlID.val(escape(xml));
                //alert(XmlID.val());

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


        function tab1_To_tab2() {
            var table1 = document.getElementById("grdAttributeValue"),
                table2 = document.getElementById("grdAttribute"),
                checkboxes = document.getElementsByName("check-tab1");
            console.log("Val1 = " + checkboxes.length);

            var iAddedCount = 0
            var itemcheck = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {

                    $('#grdAttribute tr').each(function () {
                        var currentRow = $(this);
                        var AttribName = currentRow.find("td:eq(2)").text();
                        var ValueName = currentRow.find("td:eq(4)").text();
                        var selectedAttrib = $("[id$=ddlAttribute]").find('option:selected').text();
                        if ($('#<%=hidAttributeOrder.ClientID %>').val() == "Y") {
                            if ((AttribName == "2.TYPE") && (selectedAttrib == "1.CODE")) {
                                ShowPopupMessageBox("Attribute Order is Mismatch!");
                                itemcheck = true;
                            }
                            if ((AttribName == "3.SIZE") && (selectedAttrib == "1.CODE") || (AttribName == "3.SIZE") && (selectedAttrib == "2.TYPE")) {
                                ShowPopupMessageBox("Attribute Order is Mismatch!");
                                itemcheck = true;
                            }
                            if ((AttribName == "4.COLOR") && (selectedAttrib == "1.CODE") || (AttribName == "4.COLOR") && (selectedAttrib == "2.TYPE")
                                || (AttribName == "4.COLOR") && (selectedAttrib == "3.SIZE")) {
                                ShowPopupMessageBox("Attribute Order is Mismatch!");
                                itemcheck = true;
                            }
                        }
                        var selectedAttribValue = table1.rows[i + 1].cells[1].innerHTML;
                        if ($.trim(AttribName) == $.trim(selectedAttrib) && $.trim(ValueName) == $.trim(selectedAttribValue)) {
                            ShowPopupMessageBox('Already Exist  (' + AttribName + ' - ' + ValueName + ")");
                            itemcheck = true;
                        }
                    });
                }
            }

            if (itemcheck == false) {
                for (var i = 0; i < checkboxes.length; i++)
                    if (checkboxes[i].checked) {
                        // create new row and cells
                        var rowCount = $('table#grdAttribute tr:last').index() + 1;
                        var newRow = table2.insertRow(table2.length),
                            cell1 = newRow.insertCell(0),
                            cell2 = newRow.insertCell(1),
                            cell3 = newRow.insertCell(2),
                            cell4 = newRow.insertCell(3),
                            cell5 = newRow.insertCell(4),
                            cell6 = newRow.insertCell(5);

                        // add values to the cells 
                        cell1.className = "hiddencol";
                        cell2.className = "hiddencol";
                        cell2.id = "AttCode";
                        cell4.className = "hiddencol";
                        cell4.id = "ValueCode";
                        cell6.style = "text-align:center";


                        cell1.innerHTML = rowCount;
                        cell2.innerHTML = $("[id$=ddlAttribute]").val();
                        cell3.innerHTML = $("[id$=ddlAttribute]").find('option:selected').text();
                        cell4.innerHTML = table1.rows[i + 1].cells[0].innerHTML;
                        cell5.innerHTML = table1.rows[i + 1].cells[1].innerHTML;
                        cell6.innerHTML = "<input type='checkbox' name='check-tab2'>";
                        iAddedCount = iAddedCount + 1;
                        // remove the transfered rows from the first table [table1]
                        var index = table1.rows[i + 1].rowIndex;
                        table1.deleteRow(index);
                        // we have deleted some rows so the checkboxes.length have changed
                        // so we have to decrement the value of i
                        i--;
                        console.log(checkboxes.length);
                    }

                if (iAddedCount > 0) {
                    //alert($("[id$=ddlAttribute]").val());
                    //alert($("[id$=ddlAttribute]").find('option:selected').text());                
                    //alert('count' + iAddedCount)
                    var table1 = document.getElementById("grdAttributeCount");
                    var rowCount = $('table#grdAttributeCount tr:last').index() + 1;
                    //alert(rowCount);
                    var checkitem = false;
                    $('#grdAttributeCount tr').each(function () {
                        var currentRow = $(this);
                        var AttribName = currentRow.find("td:eq(2)").text();
                        var selectedAttribName = $("[id$=ddlAttribute]").find('option:selected').text();
                        if ($.trim(AttribName) == $.trim(selectedAttribName)) {
                            var NewCount = iAddedCount + parseFloat(currentRow.find("td:eq(3)").text());
                            currentRow.find("td:eq(3)").text(NewCount);
                            checkitem = true;
                        }
                    });

                    if (checkitem == false) {
                        // create new row and cells
                        var newRow = table1.insertRow(table1.length),
                            cell1 = newRow.insertCell(0),
                            cell2 = newRow.insertCell(1),
                            cell3 = newRow.insertCell(2),
                            cell4 = newRow.insertCell(3);

                        // add values to the cells
                        cell1.className = "hiddencol";
                        cell2.className = "hiddencol";
                        cell1.innerHTML = rowCount;
                        cell2.innerHTML = $("[id$=ddlAttribute]").val();
                        cell3.innerHTML = $("[id$=ddlAttribute]").find('option:selected').text();
                        cell4.innerHTML = iAddedCount;
                    }

                    $('#grdAttributeValue').find("tr:gt(0)").remove();
                }


            }

            return false;
        }

        var addSerialNumber = function () {
            var i = 0
            $('#grdAttributeValue tr').each(function (index) {
                $(this).find('td:nth-child(1)').html(index - 1 + 1);
            });
        };


        function tab2_To_tab1() {
            var table1 = document.getElementById("grdAttribute");
            checkboxes = document.getElementsByName("check-tab2");
            console.log("Val1 = " + checkboxes.length);
            for (var i = 0; i < checkboxes.length; i++)
                if (checkboxes[i].checked) {

                    $('#grdAttributeCount tr').each(function () {
                        if (!this.rowIndex) return;
                        var customerId = $(this).find("td").eq(2).html(); //$(this).find("td:last").html(); 
                        var Maxcount = $(this).find("td:last").html();
                        //alert(customerId + '--' + table1.rows[i + 1].cells[2].innerHTML);
                        if (table1.rows[i + 1].cells[2].innerHTML === customerId) {
                            Maxcount = Maxcount - 1;
                            $(this).find("td:last").html(Maxcount);
                            if (Maxcount == 0)
                                $(this).remove();
                        }
                    });

                    //$("#grdAttribute tr").each(function () {
                    //    if (!this.rowIndex) return;
                    //    var gitem = $(this).find("td.Itemcode").html();
                    //    var gBatchNo = $(this).find("td.BatchNo").html();
                    //    //alert(gitem); 
                    //});

                    //$('#' + table).find('tr#' + rowId).find('td:eq(colNum)').html(newValue);

                    // remove the transfered rows from the second table [table2]
                    var index = table1.rows[i + 1].rowIndex;
                    table1.deleteRow(index);
                    // we have deleted some rows so the checkboxes.length have changed
                    // so we have to decrement the value of i
                    i--;
                    console.log(checkboxes.length);
                }
        }

        function createatable() {

            var table1 = document.getElementById("grdAttributeValue"),
                checkboxes = document.getElementsByName("check-tab2");
            console.log("Val1 = " + checkboxes.length);

            for (var i = 0; i < checkboxes.length; i++)
                if (checkboxes[i].checked) {
                    // create new row and cells
                    var newRow = table1.insertRow(table1.length),
                        cell1 = newRow.insertCell(0),
                        cell2 = newRow.insertCell(1),
                        cell3 = newRow.insertCell(2),
                        cell4 = newRow.insertCell(3);

                    cell4.style = "text-align:center";
                    // add values to the cells
                    cell1.innerHTML = table2.rows[i + 1].cells[0].innerHTML;
                    cell2.innerHTML = table2.rows[i + 1].cells[1].innerHTML;
                    cell3.innerHTML = table2.rows[i + 1].cells[2].innerHTML;
                    cell4.innerHTML = "<input type='checkbox' name='check-tab1'>";

                    console.log(checkboxes.length);
                }
        }

        // Get Attributes
        function fncGetAttributes(mode, attributecode) {
            var obj = {};
            try {
                obj.mode = mode;
                obj.attributecode = attributecode;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncGetAttributesValue") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        funBindAttributes(jQuery.parseJSON(msg.d));
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function pageLoad() { 
            //Vijay 20210309
            $('#<%=grdStyleCode.ClientID %>').css('display', 'none');
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc      
            if ($('#<%=hidAttributeOrder.ClientID %>').val() == "Y") {
                $('#divSearchAttribute').css("display", "block");
                $('#btnClear').css("display", "block");
            }
            if ($(<%=txtUOMPurchase.ClientID%>).val() == "")
                $(<%=txtUOMPurchase.ClientID%>).val('PCS');
            if ($(<%=txtSalesUOM.ClientID%>).val() == "")
                $(<%=txtSalesUOM.ClientID%>).val('PCS');
            if ($(<%=txtStockType.ClientID%>).val() == "")
                $(<%=txtStockType.ClientID%>).val('Stock');
            if ($(<%=txtFloor.ClientID%>).val() == "")
                $(<%=txtFloor.ClientID%>).val('GF');
            $('#<%=ddlOrderBy.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlOrderBy.ClientID %>').next().remove();

            $('#<%=ddlMiniBy.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlMiniBy.ClientID %>').next().remove();

            $('#<%=ddlByReOrder.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlByReOrder.ClientID %>').next().remove();

            $('#<%=ddlBreakPriceType.ClientID %>').show().removeClass('chzn-done');
            $('#<%=ddlBreakPriceType.ClientID %>').next().remove();

            $('#<%=rbtMRP.ClientID %>').hide();
            $('#<%=rbnMRP2.ClientID %>').hide();


            $("[id$=ddlAttribute]").change(function () {
                var selectedVal = $("[id$=ddlAttribute]").val();
                //alert(selectedVal);
                fncGetAttributes('1', selectedVal);
            });


            //------------------------------------------- Get HSN Code ----------------------------------------//
            $("[id$=txtHSNCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/GetHsnCode") %>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                //==========================> After Select Value
                select: function (e, i) {
                    $('#<%=txtHSNCode.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtMRP.ClientID %>').select(); // work modified by saravanan 01-12-2017
                    return false;
                },
                focus: function (event, i) { /// work done by saravanan 01-12-2017
                    $('#<%=txtHSNCode.ClientID %>').val($.trim(i.item.val));
                    event.preventDefault();
                },

                minLength: 0
            });

            //-------------------------------------------- Select All --------------------------------------------//

            $('#<%=chkSelectAll.ClientID %>').change(function () {
                try {
                    $('#<%=chkExpiryDate.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSerialNo.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkAutoPO.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkPackage.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkLoyality.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSpecial.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkMemDis.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSeasonal.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkBatch.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkShelf.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkHideData.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkMarking.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkBarcodePrint.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkAllowCost.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=cbAdditionalCess.ClientID %>').prop('checked', $(this).prop("checked"));
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.Message);
                    }
                });


            $('#<%=chkSelectAllRight.ClientID %>').change(function () {
                try {
                    $('#<%=chkQuatation.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkGRN.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(this).prop("checked"));
                    $('#<%=chkEstimate.ClientID %>').prop('checked', $(this).prop("checked"));
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                }
            });


            //============================  Assign Variable for Simply =========================//

            ddlInputSGST = $('#<%=ddlInputSGST.ClientID %>');
            ddlInputCGST = $('#<%=ddlInputCGST.ClientID %>');
            ddlInputIGST = $('#<%=ddlInputIGST.ClientID %>');
            ddlOutputSGST = $('#<%=ddlOutputSGST.ClientID %>');
            ddlOutputCGST = $('#<%=ddlOutputCGST.ClientID %>');
            ddlOutputIGST = $('#<%=ddlOutputIGST.ClientID %>');
            txtCESSPrc = $('#<%=txtCESSPrc.ClientID %>');
            txtHSNCode = $('#<%=txtHSNCode.ClientID %>');
            txtMRP = $('#<%=txtMRP.ClientID %>');
            txtMRPMarkDown = $('#<%=txtMRPMarkDown.ClientID %>');
            txtBasicCost = $('#<%=txtBasicCost.ClientID %>');
            txtDiscountPrc1 = $('#<%=txtDiscountPrc1.ClientID %>');
            txtDiscountPrc2 = $('#<%=txtDiscountPrc2.ClientID %>');
            txtDiscountPrc3 = $('#<%=txtDiscountPrc3.ClientID %>');
            txtDiscountAmt = $('#<%=txtDiscountAmt.ClientID %>');
            txtGrossCost = $('#<%=txtGrossCost.ClientID %>');
            txtInputSGSTPrc = $('#<%=txtInputSGSTPrc.ClientID %>');
            txtInputSGSTAmt = $('#<%=txtInputSGSTAmt.ClientID %>');
            txtInputCGSTPrc = $('#<%=txtInputCGSTPrc.ClientID %>');
            txtInputCGSTAmt = $('#<%=txtInputCGSTAmt.ClientID %>');
            txtInputCESSPrc = $('#<%=txtInputCESSPrc.ClientID %>');
            txtInputCESSAmt = $('#<%=txtInputCESSAmt.ClientID %>');
            txtNetCost = $('#<%=txtNetCost.ClientID %>');
            txtMarginMRP = $('#<%=txtMarginMRP.ClientID %>');
            txtMarginSP = $('#<%=txtMarginSP.ClientID %>');
            txtFixedMargin = $('#<%=txtFixedMargin.ClientID %>');
            txtMRPAmt = $('#<%=txtMRPAmt.ClientID %>');
            txtSPAmt = $('#<%=txtSPAmt.ClientID %>');
            txtProfitMRPPrc = $('#<%=txtProfitMRPPrc.ClientID %>');
            txtProfitSPPrc = $('#<%=txtProfitSPPrc.ClientID %>');
            txtBasicSelling = $('#<%=txtBasicSelling.ClientID %>');
            txtOutputSGSTPrc = $('#<%=txtOutputSGSTPrc.ClientID %>');
            txtOutputSGSTAmt = $('#<%=txtOutputSGSTAmt.ClientID %>');
            txtOutputCGSTPrc = $('#<%=txtOutputCGSTPrc.ClientID %>');
            txtOutputCGSTAmt = $('#<%=txtOutputCGSTAmt.ClientID %>');
            txtOutputCESSPrc = $('#<%=txtOutputCESSPrc.ClientID %>');
            txtOutputCESSAmt = $('#<%=txtOutputCESSAmt.ClientID %>');
            txtSellingPrice = $('#<%=txtSellingPrice.ClientID %>');
            txtVatLiabilitySGSTPrc = $('#<%=txtVatLiabilitySGSTPrc.ClientID %>');
            txtVatLiabilitySGSTAmt = $('#<%=txtVatLiabilitySGSTAmt.ClientID %>');
            txtVatLiabilityCGSTPrc = $('#<%=txtVatLiabilityCGSTPrc.ClientID %>');
            txtVatLiabilityCGSTAMt = $('#<%=txtVatLiabilityCGSTAMt.ClientID %>');
            txtWholePrice1Popup = $('#<%=txtWholePrice1Popup.ClientID %>');
            txtWholePrice2Popup = $('#<%=txtWholePrice2Popup.ClientID %>');
            txtWholePrice3Popup = $('#<%=txtWholePrice3Popup.ClientID %>');
            txtMarginWPrice1 = $('#<%=txtMarginWPrice1.ClientID %>');
            txtMarginWPrice2 = $('#<%=txtMarginWPrice2.ClientID %>');
            txtMarginWPrice3 = $('#<%=txtMarginWPrice3.ClientID %>');
            txtMinimumQty = $('#<%=txtMinimumQty.ClientID %>');
            txtMaxQty = $('#<%=txtMaxQty.ClientID %>');
            txtReOrderQty = $('#<%=txtReOrderQty.ClientID %>');


            //============================  Dialog Open For Inventory Pricing =========================//

            $(function () {
                $("#divModelView").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: "auto",
                    //modal: true,
                    buttons: {
                        "OK": function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });


            $(function () {
                $("#dialog-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 350,
                    modal: true,
                    buttons: {
                        "Yes": function () {

                            $(this).dialog("close");

                            //==========================> Redirect Page to Price Change Batch

                            var InvCode = $('#<%=txtItemCode.ClientID %>').val();
                            var page = '<%=ResolveUrl("~/Merchandising/frmPriceChangeBatch.aspx") %>';
                            var page = page + "?InvCode=" + InvCode
                            var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                                autoOpen: false,
                                modal: true,
                                height: 645,
                                width: 1300,
                                title: "Price Change Batch"
                            });
                            $dialog.dialog('open');


                        },
                        No: function () {
                            $(this).dialog("close");



                            $("#dialog-InvPrice").dialog("open");
                            fncOpenDialogWithDetail();
                        }
                    }
                });
            });

            $(function () {
                $("#dialog-clear-confirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: "auto",
                    width: 250,
                    modal: true,
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");
                            fncClearPriceChange();
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    }
                });
            });

            /// work modified by saravanan 29-11-2017
            // $("#dialog-InvPrice").parent().appendTo($("form:first")); // For After Postback 
            $(document).ready(function () {
                fncinitializeinventoryCal();
                $("#dialog-InvPrice").parent().appendTo($("form:first"));
            });

            /// work modified by saravanan 29-11-2017
            ////Initialize Inventory Calculation Pricing
            function fncinitializeinventoryCal() {
                try {
                    $("#dialog-InvPrice").dialog({
                        appendTo: 'form:first',
                        autoOpen: false,
                        resizable: false,
                        height: "auto",
                        width: 1050,
                        modal: true,
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "blind",
                            duration: 1000
                        },
                        buttons: {
                            "Update": function () {
                                fncSaveInvPriceChange();
                                $('#<%=hidPriceEntered.ClientID %>').val("1");
                            },
                            Clear: function () {
                                $("#dialog-clear-confirm").dialog("open");
                                //$('#<%=hidPriceEntered.ClientID %>').val("0");
                            },
                            Close: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            //=============================================> Set Default Value in Price Change Dialog
            txtMRPMarkDown.val('0.00')

            //===================================== Price Change Calculation =====================================//

            //=============================>  Input IGST Change
            ddlInputIGST.change(function () {
                fncInputGSTValueChange();

            });

            /// Input GST Value Changed
            function fncInputGSTValueChange() {
                try {

                    var InputIGSTCode = ddlInputIGST.val();
                    var GSTObj, GSTType;

                    //=============================>  Output IGST Change
                    ddlOutputIGST.val(InputIGSTCode);
                    ddlOutputIGST.trigger("liszt:updated");

                    if (InputIGSTCode == "EIGST")
                        GSTType = "EIGST";
                    else
                        GSTType = "IGST";

                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                        data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + GSTType + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            //=============================>  Input SGST Change
                            GSTObj = jQuery.parseJSON(msg.d);

                            if (GSTObj.length > 0) {
                                for (var i = 0; i < GSTObj.length; i++) {
                                    if (GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                        //=============================>  Input SGST Change
                                        ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputSGST.trigger("liszt:updated");

                                        var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                        var InputSGSTPrc = array[1];
                                        if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                            txtInputSGSTPrc.val(InputSGSTPrc);
                                        }

                                        //=============================>  Output SGST Change
                                        ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputSGST.trigger("liszt:updated");

                                        var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                        var OutputSGSTPrc = array[1];
                                        if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                            txtOutputSGSTPrc.val(OutputSGSTPrc);
                                            fncCalcSelingPice();
                                        }

                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                        //=============================>  Input CGST Change
                                        ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputCGST.trigger("liszt:updated");

                                        var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                        var InputCGSTPrc = array[1];
                                        if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                            txtInputCGSTPrc.val(InputCGSTPrc);
                                        }

                                        //=============================>  Output CGST Change
                                        ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputCGST.trigger("liszt:updated");

                                        var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                        var OutputCGSTPrc = array[1];
                                        if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                            txtOutputCGSTPrc.val(OutputCGSTPrc);
                                        }
                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("S") != -1 || GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                        //=============================>  Input SGST Change
                                        ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputSGST.trigger("liszt:updated");

                                        var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                        var InputSGSTPrc = array[1];
                                        if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                            txtInputSGSTPrc.val(InputSGSTPrc);
                                        }

                                        //=============================>  Output SGST Change
                                        ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputSGST.trigger("liszt:updated");

                                        var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                        var OutputSGSTPrc = array[1];
                                        if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                            txtOutputSGSTPrc.val(OutputSGSTPrc);
                                            fncCalcSelingPice();
                                        }

                                    }
                                    else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 || GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                        //=============================>  Input CGST Change
                                        ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlInputCGST.trigger("liszt:updated");

                                        var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                        var InputCGSTPrc = array[1];
                                        if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                            txtInputCGSTPrc.val(InputCGSTPrc);
                                        }

                                        //=============================>  Output CGST Change
                                        ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                        ddlOutputCGST.trigger("liszt:updated");

                                        var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                        var OutputCGSTPrc = array[1];
                                        if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                            txtOutputCGSTPrc.val(OutputCGSTPrc);
                                        }
                                    }//


                                }

                                fncCalcSelingPice();
                            }//End

                            /// work done by saravanan 01-12-2017
                            $('#<%=txtCESSPrc.ClientID %>').select();

                        },
                        error: function (data) {
                            ShowPopupMessageBox('Something Went Wrong')
                        }
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err);
                }
            }


               <%-- $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                    data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + "C" + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        //=============================>  Input CGST Change
                        ddlInputCGST.val(msg.d)
                        ddlInputCGST.trigger("liszt:updated");

                        var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                        var InputCGSTPrc = array[1];
                        if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                            txtInputCGSTPrc.val(InputCGSTPrc);
                            fncCalcSelingPice();
                        }

                        //=============================>  Output CGST Change
                        ddlOutputCGST.val(msg.d)
                        ddlOutputCGST.trigger("liszt:updated");

                        var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                        var OutputCGSTPrc = array[1];
                        if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                            txtOutputCGSTPrc.val(OutputCGSTPrc);
                            fncCalcSelingPice();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });--%>
            //});


            //            //=============================>  Input SGST Change
            //            ddlInputSGST.change(function () {
            //                var InputSGSTPrc = ddlInputSGST.find("option:selected").text();
            //                if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
            //                    txtInputSGSTPrc.val(InputSGSTPrc);
            //                    fncCalcSelingPice();
            //                }
            //            });

            //            //=============================> Input CGST Change
            //            ddlInputCGST.change(function () {
            //                var InputCGSTPrc = ddlInputCGST.find("option:selected").text();
            //                if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
            //                    txtInputCGSTPrc.val(InputCGSTPrc);
            //                    fncCalcSelingPice();
            //                }
            //            });


            //=============================> CESS Focus Out
            txtCESSPrc.focusout(function () {
                var CessPrc = txtCESSPrc.val();
                if (CessPrc != '') {
                    txtCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                    txtInputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                    txtOutputCESSPrc.val(parseFloat(CessPrc).toFixed(2));
                    fncCalcSelingPice();
                }
            });


            //            //=============================>  Output SGST Change
            //            ddlOutputSGST.change(function () {
            //                var OutputSGSTPrc = ddlOutputSGST.find("option:selected").text();
            //                if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
            //                    txtOutputSGSTPrc.val(OutputSGSTPrc);
            //                    fncCalcSelingPice();
            //                }
            //            });

            //            //=============================> Output CGST Change
            //            ddlOutputCGST.change(function () {
            //                var OutputCGSTPrc = ddlOutputCGST.find("option:selected").text();
            //                if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
            //                    txtOutputCGSTPrc.val(OutputCGSTPrc);
            //                    fncCalcSelingPice();
            //                }
            //            });


            //===========================> MRP Enter Event
            <%-- $(txtMRP).live("keypress", function (e) {
                if (e.keyCode == 13) {
                    if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                        txtBasicCost.focus().select();
                    }
                    else {
                        txtMRPMarkDown.focus().select();
                    }
                }
            });--%>

            //=============================> MRP Focus Out
            txtMRP.focusout(function () {

                var MRP = txtMRP.val();
                if (MRP != '') {
                    txtMRP.val(parseFloat(MRP).toFixed(2));
                }

                if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                    var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
                    var SellingPrice = parseFloat(MRP) - parseFloat((MRP * FixedMargin) / 100)
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            });

            //=========================> Basic Cost Enter Event
            <%--$(txtBasicCost).live("keypress", function (e) {
                if (e.keyCode == 13) {
                    if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                        txtDiscountPrc1.focus().select();
                    }
                    else {
                        txtFixedMargin.focus().select();
                    }
                }
            });--%>

            //=============================> Basic Cost Focus Out
            txtBasicCost.focusout(function () {
                var BasicCost = txtBasicCost.val();
                if (BasicCost != '') {
                    txtBasicCost.val(parseFloat(BasicCost).toFixed(2));
                    fncCalcSelingPice();
                }
            });

            //=============================>  Discount PRC 1 Focus Out
            txtDiscountPrc1.focusout(function () {
                var DiscountPrc1 = txtDiscountPrc1.val();
                if (DiscountPrc1 != '') {
                    txtDiscountPrc1.val(parseFloat(DiscountPrc1).toFixed(2));
                    fncCalcSelingPice();
                }
            });

            //=============================>  Discount PRC 2 Focus Out 
            txtDiscountPrc2.focusout(function () {
                var DiscountPrc2 = txtDiscountPrc2.val();
                if (DiscountPrc2 != '') {
                    txtDiscountPrc2.val(parseFloat(DiscountPrc2).toFixed(2));
                    fncCalcSelingPice();
                }
            });

            //=============================>  Discount PRC 3 Focus Out 
            txtDiscountPrc3.focusout(function () {
                var DiscountPrc3 = txtDiscountPrc3.val();
                if (DiscountPrc3 != '') {
                    txtDiscountPrc3.val(parseFloat(DiscountPrc3).toFixed(2));
                    fncCalcSelingPice();
                }
            });

            //=============================>  Fixed Margin Focus Out
            txtFixedMargin.focusout(function () {
                var FixedMargin = txtFixedMargin.val();
                if (FixedMargin != '') {
                    //Always Mark Down
                    if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var SellingPrice = (MRP * FixedMargin) / 100
                        SellingPrice = parseFloat(MRP) - parseFloat(SellingPrice)
                        txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                        fncSellingPriceFocusOutCal();
                        //txtSellingPrice.focus(); /// work modified by saravanan 01-12-2017
                        //txtMRPMarkDown.focus();
                    }
                    else {
                        txtFixedMargin.val(parseFloat(FixedMargin).toFixed(2));
                        fncAfterMarginMRPSellingPrice();
                    }

                    // Assign Margin WholeSale Price
                    txtMarginWPrice1.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice2.val(parseFloat(FixedMargin).toFixed(2))
                    txtMarginWPrice3.val(parseFloat(FixedMargin).toFixed(2))
                }
            });

            //=============================>  Margin WholeSale 1 focus Out
            txtMarginWPrice1.focusout(function () {

                var MarginWPrice1 = txtMarginWPrice1.val();
                if (MarginWPrice1 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice1) == 0) {
                        $('#<%=txtWholePrice1Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var WholeSalePrice1 = (MRP * MarginWPrice1) / 100
                        WholeSalePrice1 = parseFloat(MRP) - parseFloat(WholeSalePrice1)
                        txtWholePrice1Popup.val(parseFloat(WholeSalePrice1).toFixed(2));
                    }
                    else {
                        txtMarginWPrice1.val(parseFloat(MarginWPrice1).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('1', MarginWPrice1);
                    }
                }
            });

            //=============================>  Margin WholeSale 2 focus Out
            txtMarginWPrice2.focusout(function () {
                var MarginWPrice2 = txtMarginWPrice2.val();
                if (MarginWPrice2 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice2) == 0) {
                        $('#<%=txtWholePrice2Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var MarginWPrice2 = (MRP * MarginWPrice2) / 100
                        MarginWPrice2 = parseFloat(MRP) - parseFloat(MarginWPrice2)
                        txtWholePrice2Popup.val(parseFloat(MarginWPrice2).toFixed(2));
                    }
                    else {
                        txtMarginWPrice2.val(parseFloat(MarginWPrice2).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('2', MarginWPrice2);
                    }
                }
            });

            //=============================>  Margin WholeSale 3 focus Out
            txtMarginWPrice3.focusout(function () {
                var MarginWPrice3 = txtMarginWPrice3.val();
                if (MarginWPrice3 != '') {
                    //Always Mark Down
                    if (parseFloat(MarginWPrice3) == 0) {
                        $('#<%=txtWholePrice3Popup.ClientID %>').val($('#<%=txtSellingPrice.ClientID %>').val());
                    }
                    else if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                        var MRP = txtMRP.val() == "" ? "0.00" : txtMRP.val();
                        var WholeSalePrice3 = (MRP * MarginWPrice3) / 100
                        WholeSalePrice3 = parseFloat(MRP) - parseFloat(WholeSalePrice3)
                        txtWholePrice3Popup.val(parseFloat(WholeSalePrice3).toFixed(2));
                    }
                    else {
                        txtMarginWPrice3.val(parseFloat(MarginWPrice3).toFixed(2));
                        fncAfterWSaleMarginSellingPrice('3', MarginWPrice3);
                    }
                }
            });


            //=============================>  Selling Price Focus Out
            txtSellingPrice.focusout(function () {

                fncSellingPriceFocusOutCal();

            });

            txtWholePrice1Popup.focusout(function () {
                fncArriveWpricePercentage("wprice1");
            });
            txtWholePrice2Popup.focusout(function () {
                fncArriveWpricePercentage("wprice2");
            });
            txtWholePrice3Popup.focusout(function () {
                fncArriveWpricePercentage("wprice3");
            });

            /// work modified by saravanan 16-02-2018
            function fncSellingPriceFocusOutCal() {
                try {

                    var SPMargin = 0;
                    var addCessAmt = 0;
                    /// Work done by saravanan 15-02-2018
                    if (parseFloat(NetSellingprice) == parseFloat(txtSellingPrice.val()) && parseFloat(txtFixedMargin.val()) != 0)
                        return;



                    var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
                    var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();
                    addCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
                    /// work modified by saravanan 2018-01-01
                    //var OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
                    //var OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();

                    var OutputSGSTLiabilityAmt, OutputCGSTLiabilityAmt;

                    var BasicSelling = SellingPrice;
                    var OutputSGSTAmt = '0';
                    var OutputCGSTAmt = '0';
                    var OutputCESSAmt = '0';

                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                    var OutputSGSTPrc = txtOutputSGSTPrc.val();
                    var OutputCSGSTPrc = txtOutputCGSTPrc.val();
                    var OutputCESSPrc = txtCESSPrc.val();
                    var TotalOutputGSTPrc = 0;

                    if (OutputSGSTPrc != '') {
                        TotalOutputGSTPrc = OutputSGSTPrc;
                    }
                    if (OutputCSGSTPrc != '') {
                        TotalOutputGSTPrc = parseFloat(TotalOutputGSTPrc) + parseFloat(OutputCSGSTPrc);
                    }
                    if (OutputCESSPrc != '') {
                        TotalOutputGSTPrc = parseFloat(TotalOutputGSTPrc) + parseFloat(OutputCESSPrc);
                    }

                    var TotalOutputGSTAmt = 0;
                    if (TotalOutputGSTPrc != '') {
                        SellingPrice = parseFloat(SellingPrice) - parseFloat(addCessAmt);
                        TotalOutputGSTAmt = ((SellingPrice * TotalOutputGSTPrc) / (parseFloat(TotalOutputGSTPrc) + parseFloat(100)));
                        if (TotalOutputGSTAmt != '') {
                            BasicSelling = parseFloat(SellingPrice) - parseFloat(TotalOutputGSTAmt);
                            txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2));
                            //Split Two Precentage
                            if (OutputSGSTPrc != '') {
                                OutputSGSTAmt = TotalOutputGSTAmt * OutputSGSTPrc / TotalOutputGSTPrc;
                                txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));
                            }
                            if (OutputCGSTAmt != '') {
                                OutputCGSTAmt = TotalOutputGSTAmt * OutputCSGSTPrc / TotalOutputGSTPrc;
                                txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));
                            }
                            if (OutputCESSAmt != '') {
                                OutputCESSAmt = TotalOutputGSTAmt * OutputCESSPrc / TotalOutputGSTPrc;
                                txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtBasicSelling.val(parseFloat(txtSellingPrice.val()).toFixed(2));
                    }

                    //==================> Tax Liability Calc
                    TaxLiabilityCalc();

                    OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
                    OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();

                   <%-- $('#<%=txtGSTLiability.ClientID %>').val(parseFloat(OutputSGSTLiabilityAmt) + parseFloat(OutputSGSTLiabilityAmt));
                    $('#<%=txtGSTLiabilityPer.ClientID %>').val(parseFloat($('#<%=txtVatLiabilitySGSTPrc.ClientID %>').val()) + parseFloat($('#<%=txtVatLiabilityCGSTPrc.ClientID %>').val()));--%>

                    //SellingPrice RoundOff based On ParamMaster
                    txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));

                    //===================> Margin MRP Amt  
                    var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

                    MRP = txtMRP.val();
                    GrossCost = txtGrossCost.val();
                    array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                    GSTPer = array[1];
                    GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
                    MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
                    MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);



                    var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
                    MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
                    txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

                    /// work done by saravanan 15-02-2015

                    MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
                    txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));

                    //===================> Margin Selling Price Prc   
                    SellingPrice = txtSellingPrice.val();
                    var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    var SPPrc1 = SPPrc / SellingPrice * 100
                    SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
                    txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

                    //===================> Margin Selling Price Amt 
                    var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
                    txtSPAmt.val(parseFloat(SPAmt).toFixed(2));


                    // Assign WholeSale Price
                    txtWholePrice1Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice2Popup.val(parseFloat(SellingPrice).toFixed(2))
                    txtWholePrice3Popup.val(parseFloat(SellingPrice).toFixed(2))



                    /// To Arrive SellingPrice Margin
                    if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                        if (parseFloat($('#<%=txtGrossCost.ClientID %>').val()) != 0) {
                            SPMargin = (parseFloat($('#<%=txtBasicSelling.ClientID %>').val()) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtFixedMargin.ClientID %>').val(SPMargin.toFixed(2));

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));
                        }
                    }



                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            /// work done by saravanan 16-02-2018
            // To Arrive Whole sale Margin
            function fncArriveWpricePercentage(mode) {
                try {

                    var SPMargin = 0, GSTAmt = 0, WPBasicAmt = 0;
                    var array, GSTPer = 0;

                    if ($('#<%=rbtCostPopup.ClientID %>').is(':checked') || $('#<%=hdfMarkDown.ClientID %>').val() == 'N') {

                            array = ddlOutputIGST.find("option:selected").text().trim().split("-");
                            GSTPer = array[1];



                            if (mode == "wprice1") {
                                GSTAmt = (parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice1Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());

                            $('#<%=txtMarginWPrice1.ClientID %>').val(SPMargin.toFixed(2));
                        }
                        else if (mode == "wprice2") {
                            GSTAmt = (parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice2Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice2.ClientID %>').val(SPMargin.toFixed(2));
                        }
                        else {
                            GSTAmt = (parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + parseFloat(100));
                            WPBasicAmt = parseFloat($('#<%=txtWholePrice3Popup.ClientID %>').val()) - parseFloat(GSTAmt);
                            SPMargin = (parseFloat(WPBasicAmt) - parseFloat($('#<%=txtGrossCost.ClientID %>').val())) * 100;
                            SPMargin = parseFloat(SPMargin) / parseFloat($('#<%=txtGrossCost.ClientID %>').val());
                            $('#<%=txtMarginWPrice3.ClientID %>').val(SPMargin.toFixed(2));
                        }
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }


            //=============================>  MRP PRC Focus Out
            txtMRPMarkDown.focusout(function () {
                ////bugger;
                txtMRPMarkDown.val(parseFloat(txtMRPMarkDown.val()).toFixed(2));
                var MRPMarkDown = parseFloat(txtMRPMarkDown.val()).toFixed(2);
                var MRP = txtMRP.val();
                var AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
            if (MRPMarkDown != '' && MRP != '') {
                var NetCost = parseFloat(MRP) * parseFloat(MRPMarkDown) / 100;
                NetCost = parseFloat(MRP).toFixed(2) - parseFloat(NetCost).toFixed(2);
                //NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                txtNetCost.val(parseFloat(NetCost).toFixed(2));
                var InputSGSTAmt = '0';
                var InputCGSTAmt = '0';
                var InputCESSAmt = '0';
                if (NetCost != '') {
                    var InputSGSTPrc = txtInputSGSTPrc.val();
                    var InputCGSTPrc = txtInputCGSTPrc.val();
                    var InputCESSPrc = txtInputCESSPrc.val();
                    var TotalInputGSTPrc = 0;
                    if (InputSGSTPrc != '') {
                        TotalInputGSTPrc = InputSGSTPrc;
                    }
                    if (InputCGSTPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCGSTPrc);
                    }
                    if (InputCESSPrc != '') {
                        TotalInputGSTPrc = parseFloat(TotalInputGSTPrc) + parseFloat(InputCESSPrc);
                    }

                    var TotalInputGSTAmt = 0;
                    var GrossCost = 0;

                    //NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                    //TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                    ////bugger;
                    if (TotalInputGSTPrc != '') {
                        NetCost = parseFloat(NetCost) - parseFloat(AddCessAmt);
                        TotalInputGSTAmt = ((NetCost * TotalInputGSTPrc) / (parseFloat(TotalInputGSTPrc) + parseFloat(100)));
                        if (TotalInputGSTAmt != '') {
                            //GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(TotalInputGSTAmt).toFixed(2);
                            //txtGrossCost.val(parseFloat(GrossCost).toFixed(2));
                            //Split Precentage
                            if (InputSGSTPrc != '') {
                                InputSGSTAmt = TotalInputGSTAmt * InputSGSTPrc / TotalInputGSTPrc;
                                txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                            }
                            if (InputCGSTPrc != '') {
                                InputCGSTAmt = TotalInputGSTAmt * InputCGSTPrc / TotalInputGSTPrc;
                                txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                            }
                            if (InputCESSAmt != '') {
                                InputCESSAmt = TotalInputGSTAmt * InputCESSPrc / TotalInputGSTPrc;
                                txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                            }
                        }
                    }
                    else {
                        txtGrossCost.val(parseFloat(NetCost).toFixed(2));
                    }

                    GrossCost = parseFloat(NetCost).toFixed(2) - parseFloat(InputSGSTAmt).toFixed(2) - parseFloat(InputCGSTAmt).toFixed(2) - parseFloat(InputCESSAmt);
                    //GrossCost = parseFloat(GrossCost) - parseFloat(AddCessAmt);
                    txtGrossCost.val(parseFloat(GrossCost).toFixed(2)); //GrossCost
                    txtBasicCost.val(parseFloat(GrossCost).toFixed(2)); //BasicCost                                               

                    //==================> Vat Liability Calc
                    TaxLiabilityCalc();

                }
            }

            fncCalcSelingPice();

        });

            //=============================> ReOrder Process
            //=============================>  Max Qty Focus Out
            txtMaxQty.focusout(function () {
                if (parseInt(txtMinimumQty.val()) > parseInt(txtMaxQty.val())) {
                    // work done by saravanan 2017-11-28
                    ShowPopupMessageBoxandFocustoObject('Max Qty should greater than Min qty');
                    popUpObjectForSetFocusandOpen = txtMaxQty;
                    //txtMaxQty.focus().select();
                    return;
                }
                if (txtMinimumQty.val() != '' && txtMaxQty.val() != '') {
                    txtReOrderQty.val(parseInt(txtMaxQty.val()) - parseInt(txtMinimumQty.val()))
                }
            });

            //============================> For Maintain Current Tab while postback
            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "InventoryPricing";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs ul li a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });

                // For Scrol down in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $("html, body").scrollTop($(document).height());
                })
            });

            //=============================> Set Width for Chosen Dropdown

            $("#<%=ddlInputSGST.ClientID %>, #<%=ddlInputSGST.ClientID %>_chzn, #<%=ddlInputSGST.ClientID %>_chzn > div, #<%=ddlInputSGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlInputCGST.ClientID %>, #<%=ddlInputCGST.ClientID %>_chzn, #<%=ddlInputCGST.ClientID %>_chzn > div, #<%=ddlInputCGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlInputIGST.ClientID %>, #<%=ddlInputIGST.ClientID %>_chzn, #<%=ddlInputIGST.ClientID %>_chzn > div, #<%=ddlInputIGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputSGST.ClientID %>, #<%=ddlOutputSGST.ClientID %>_chzn, #<%=ddlOutputSGST.ClientID %>_chzn > div, #<%=ddlOutputSGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputCGST.ClientID %>, #<%=ddlOutputCGST.ClientID %>_chzn, #<%=ddlOutputCGST.ClientID %>_chzn > div, #<%=ddlOutputCGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOutputIGST.ClientID %>, #<%=ddlOutputIGST.ClientID %>_chzn, #<%=ddlOutputIGST.ClientID %>_chzn > div, #<%=ddlOutputIGST.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlMiniBy.ClientID %>, #<%=ddlMiniBy.ClientID %>_chzn, #<%=ddlMiniBy.ClientID %>_chzn > div, #<%=ddlMiniBy.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlByReOrder.ClientID %>, #<%=ddlByReOrder.ClientID %>_chzn, #<%=ddlByReOrder.ClientID %>_chzn > div, #<%=ddlByReOrder.ClientID %>_chzn > div > div > input").css("width", '100%');
            $("#<%=ddlOrderBy.ClientID %>, #<%=ddlOrderBy.ClientID %>_chzn, #<%=ddlOrderBy.ClientID %>_chzn > div, #<%=ddlOrderBy.ClientID %>_chzn > div > div > input").css("width", '100%');

        }


        /// Add New Vendor
        function fncAddNewVendor(vendorcode, vendorname) {
            try {
                var tblNewVendorCreation, rowNo = 1, row;
                var status = true;
                tblNewVendorCreation = $("#tblNewVendorCreation tbody");


                /// save Multiple Vendor
                var obj = {};
                obj.mode = "Save";
                obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
                obj.vendorcode = vendorcode;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncSaveAndDeleteMultipleVendor") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "Success") {

                            if (tblNewVendorCreation.children().length == 0) {
                                row = "<tr>"
                                    + "<td id='tdRowNo' >" + rowNo + "</td>"
                                    + "<td id='tdVendorcode' >" + vendorcode + "</td>"
                                    + "<td id='tdVendorname' >" + vendorname + "</td>"
                                    + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                                    + "</tr>";
                            }
                            else {
                                tblNewVendorCreation.children().each(function () {

                                    if ($(this).find('td[id*=tdVendorcode]').text().trim() == vendorcode) {
                                        status = false;
                                        popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                                        ShowPopupMessageBoxandFocustoObject("Already vendor Added");
                                        return false;
                                    }

                                    rowNo = $(this).find('td[id*=tdRowNo]').text();
                                    rowNo = parseInt(rowNo) + 1;
                                    row = "<tr>"
                                        + "<td id='tdRowNo' >" + rowNo + "</td>"
                                        + "<td id='tdVendorcode' >" + vendorcode + "</td>"
                                        + "<td id='tdVendorname' >" + vendorname + "</td>"
                                        + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                                        + "</tr>";


                                });
                            }

                            if (status == true) {
                                tblNewVendorCreation.append(row);
                                tblNewVendorCreation.children().click(fncMultipleVendorRowClick);

                                popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('Vendor added successfully');
                            }



                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });

                $('#<%=txtNewVendorcode.ClientID %>').val("");
                //$('#<%=txtNewVendorcode.ClientID %>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// to show back ground color on click
        function fncMultipleVendorRowClick() {
            try {
                $(this).css("background-color", "#80b3ff");
                $(this).siblings().css("background-color", "white");

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDeleteMultipleVendor(source) {
            var rowNo = 1, rowObj, obj = {};;
            try {
                /// save Multiple Vendor
                rowObj = $(source).parent().parent();
                obj.mode = "Delete";
                obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
                obj.vendorcode = rowObj.find('td[id*=tdVendorcode]').text();
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncSaveAndDeleteMultipleVendor") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d == "Success") {
                            rowObj.remove();
                            $("#tblNewVendorCreation tbody").children().each(function () {
                                $(this).find('td[id*=tdRowNo]').text(rowNo)
                                rowNo = parent(rowNo) + 1;
                            });
                            popUpObjectForSetFocusandOpen = $('#<%=txtNewVendorcode.ClientID %>');
                            ShowPopupMessageBoxandFocustoObject('Vendor Deleted successfully');
                        }


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });

                $('#<%=txtNewVendorcode.ClientID %>').val("");


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <%--------------------------------------- End Page Load Script --------------------------------------%>
    <%--------------------------------------- Selling Price Calculation --------------------------------------%>
    <script type="text/javascript">



        function fncCalcSelingPice() {
            try {
                var BasicCost = txtBasicCost.val();
                var AddCessAmt = 0;
                if (BasicCost == '') {
                    return false;
                }

                txtGrossCost.val(parseFloat(BasicCost).toFixed(2));  // Assign to Gross Cost from Basic Cost Value
                AddCessAmt = $('#<%=txtAddCessAmt.ClientID %>').val();
                var GrossCost = txtGrossCost.val();
                if (GrossCost == '') {
                    return false;
                }

                //================> Discount 1
                txtDiscountAmt.val('0'); // Initial State 
                var DisPrc1 = txtDiscountPrc1.val();
                if (DisPrc1 != '') {
                    var DisAmt = (GrossCost * DisPrc1) / 100;
                    DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                    txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                    GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                }

                //================> Discount 2
                var DisPrc2 = txtDiscountPrc2.val();
                if (DisPrc2 != '') {
                    var DisAmt = (GrossCost * DisPrc2) / 100;

                    if (txtDiscountAmt.val() == '') {
                        txtDiscountAmt.val('0');
                    }
                    DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                    txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                    GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                }

                //================> Discount 3
                var DisPrc3 = txtDiscountPrc3.val();
                if (DisPrc3 != '') {
                    var DisAmt = (GrossCost * DisPrc3) / 100;

                    if (txtDiscountAmt.val() == '') {
                        txtDiscountAmt.val('0');
                    }
                    DisAmt = parseFloat(txtDiscountAmt.val()) + parseFloat(DisAmt);
                    txtDiscountAmt.val(parseFloat(DisAmt).toFixed(2));
                    GrossCost = parseFloat(txtGrossCost.val()) - parseFloat(DisAmt);
                }

                txtGrossCost.val(parseFloat(GrossCost).toFixed(2));  // Set After Discount Calculation

                //==================> Input SGST
                var InputSGSTPrc = txtInputSGSTPrc.val();
                if (InputSGSTPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputSGSTAmt = (GrossCost * InputSGSTPrc) / 100;
                        if (InputSGSTAmt == '') {
                            InputSGSTAmt = '0';
                        }
                        txtInputSGSTAmt.val(parseFloat(InputSGSTAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputCGSTAmt = txtInputCGSTAmt.val();
                        if (InputCGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }

                //==================> Input CGST
                var InputCGSTPrc = txtInputCGSTPrc.val();
                if (InputCGSTPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputCGSTAmt = (GrossCost * InputCGSTPrc) / 100;
                        if (InputCGSTAmt == '') {
                            InputCGSTAmt = '0';
                        }
                        txtInputCGSTAmt.val(parseFloat(InputCGSTAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputSGSTAmt = txtInputSGSTAmt.val();
                        if (InputSGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCGSTAmt) + parseFloat(InputSGSTAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCGSTAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }

                //==================> Input CESS
                var InputCESSPrc = txtInputCESSPrc.val();
                if (InputCESSPrc != '') {
                    var GrossCost = txtGrossCost.val();
                    if (GrossCost != '') {
                        var InputCESSAmt = (GrossCost * InputCESSPrc) / 100;
                        if (InputCESSAmt == '') {
                            InputCESSAmt = '0';
                        }
                        txtInputCESSAmt.val(parseFloat(InputCESSAmt).toFixed(2));
                        GrossCost = parseFloat(txtGrossCost.val());

                        var InputSGSTAmt = txtInputSGSTAmt.val();
                        var InputCGSTAmt = txtInputCGSTAmt.val();
                        if (InputSGSTAmt != '' && InputCGSTAmt != '') {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt) + parseFloat(InputCESSAmt);
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                        else {
                            var NetCost = parseFloat(GrossCost) + parseFloat(InputCESSAmt)
                            NetCost = parseFloat(NetCost) + parseFloat(AddCessAmt);
                            txtNetCost.val(parseFloat(NetCost).toFixed(2));
                        }
                    }
                }


                //==================> Based on Fixed Margin                
                var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
                txtFixedMargin.val(parseFloat(FixedMargin).toFixed(2))
                fncAfterMarginMRPSellingPrice();
            }
            catch (err) {
                ShowPopupMessageBox(err.Message);
            }
        }


        function fncAfterMarginMRPSellingPrice() {

            //==================> Selling Price CGST
            var FixedMargin = txtFixedMargin.val() == '' ? '0' : txtFixedMargin.val();
            var MRP = txtMRP.val() == '' ? '0' : txtMRP.val();
            var BasicCost = txtBasicCost.val() == '' ? '0' : txtBasicCost.val();
            var GrossCost = txtGrossCost.val() == '' ? '0' : txtGrossCost.val();
            var NetCost = txtNetCost.val() == '' ? '0' : txtNetCost.val();
            var InputSGSTPrc = txtInputSGSTPrc.val() == '' ? '0' : txtInputSGSTPrc.val();
            var InputCGSTPrc = txtInputCGSTPrc.val() == '' ? '0' : txtInputCGSTPrc.val();
            var InputCESSPrc = txtInputCESSPrc.val() == '' ? '0' : txtInputCESSPrc.val();
            var InputSGSTAmt = txtInputSGSTAmt.val() == '' ? '0' : txtInputSGSTAmt.val();
            var InputCGSTAmt = txtInputCGSTAmt.val() == '' ? '0' : txtInputCGSTAmt.val();
            var InputCESSAmt = txtInputCESSAmt.val() == '' ? '0' : txtInputCESSAmt.val();
            var SellingPrice = txtSellingPrice.val() == '' ? '0' : txtSellingPrice.val();
            var OutputSGSTLiabilityAmt = txtVatLiabilitySGSTAmt.val() == '' ? '0' : txtVatLiabilitySGSTAmt.val();
            var OutputCGSTLiabilityAmt = txtVatLiabilityCGSTAMt.val() == '' ? '0' : txtVatLiabilityCGSTAMt.val();
            var addCessAmt = $('#<%=txtAddCessAmt.ClientID%>').val();



            ////bugger;
            //===================> Margin MRP Prc          
            //var MarginMRPPrc = (parseFloat(MRP) - parseFloat(NetCost) + parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt) + parseFloat(InputCESSAmt))
            //var MRPVatLiablity = (MRP * (parseFloat(InputSGSTPrc) + parseFloat(InputCGSTPrc) + parseFloat(InputCESSPrc))) / (parseFloat(100) + parseFloat(InputSGSTPrc) + parseFloat(InputCGSTPrc) + parseFloat(InputCESSPrc))
            //var MRPVatLiablity1 = parseFloat(MRPVatLiablity) - (parseFloat(InputSGSTAmt) + parseFloat(InputCGSTAmt) + parseFloat(InputCESSAmt))
            //var MarginMRPPrc1 = parseFloat(MarginMRPPrc) - parseFloat(MRPVatLiablity1)
            //var MarginMRPPrc2 = (MarginMRPPrc1 / MRP * 100);
            //MarginMRPPrc2 = isNaN(MarginMRPPrc2) ? '0.00' : MarginMRPPrc2;
            //txtMarginMRP.val(parseFloat(MarginMRPPrc2).toFixed(2))

            /// work done by saravanan 15-02-2015

            var array, GSTPer, MRPGst, MarginMRPAmt;
            array = ddlOutputIGST.find("option:selected").text().trim().split("-");
            GSTPer = array[1];
            GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
            MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID%>').val());
            MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);

            var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
            MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
            txtMarginMRP.val(parseFloat(MarginMRPPrc).toFixed(2))

            //===================> Margin MRP Amt     
            //var MarginMRPAmt = parseFloat(MarginMRPPrc) - parseFloat(MRPVatLiablity1);
            //txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2))
            /// work done by saravanan 15-02-2015

            MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
            txtMRPAmt.val(parseFloat(MarginMRPAmt).toFixed(2));

            //===================> Margin Based Set Basic Selling 
            var FixedMarginAmt = (GrossCost * FixedMargin) / 100
            BasicSelling = parseFloat(GrossCost) + parseFloat(FixedMarginAmt)
            txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2))

            //===================> Output SGST
            var OutputSGSTPrc = txtOutputSGSTPrc.val();
            if (OutputSGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputSGSTAmt = (BasicSelling * OutputSGSTPrc) / 100;
                if (OutputSGSTAmt == '') {
                    OutputSGSTAmt = '0';
                }
                txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));

                var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputCGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }

            //==================> Output CGST
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputCGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                if (OutputCGSTAmt == '') {
                    OutputCGSTAmt = '0';
                }
                txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));

                var OutputSGSTAmt = txtOutputSGSTAmt.val();
                if (OutputSGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }

            //==================> Output CESS
            var OutputCESSPrc = txtOutputCESSPrc.val();
            if (OutputCESSPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCESSAmt = (BasicSelling * OutputCESSPrc) / 100;
                if (OutputCESSAmt == '') {
                    OutputCESSAmt = '0';
                }
                txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));

                var OutputSGSTAmt = txtOutputSGSTAmt.val();
                var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputSGSTAmt != '' && OutputCGSTAmt != '') {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt) + parseFloat(OutputCESSAmt);
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
                else {
                    var SellingPrice = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt)
                    SellingPrice = parseFloat(SellingPrice) + parseFloat(addCessAmt);
                    txtSellingPrice.val(parseFloat(SellingPrice).toFixed(2));
                }
            }


            TaxLiabilityCalc();
            //SellingPrice RoundOff based On ParamMaster
            txtSellingPrice.val(SellingPriceRoundOff(txtSellingPrice.val()));


            //===================> Margin Selling Price Prc          
            var SPPrc = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2);  //- parseFloat(OutputCGSTLiabilityAmt))
            var SPPrc1 = SPPrc / SellingPrice * 100
            SPPrc1 = isNaN(SPPrc1) ? '0.00' : SPPrc1;
            txtMarginSP.val(parseFloat(SPPrc1).toFixed(2))

            //===================> Margin Selling Price Amt 
            var SPAmt = (parseFloat(SellingPrice) - parseFloat(NetCost) - parseFloat($('#<%=txtGSTLiability.ClientID%>').val())).toFixed(2); //- parseFloat(OutputCGSTLiabilityAmt))
            txtSPAmt.val(parseFloat(SPAmt).toFixed(2));


            // Assign WholeSale Price
            txtWholePrice1Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
            txtWholePrice2Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
            txtWholePrice3Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))

            //==================> Tax Liability Calc

        }

        function fncAfterWSaleMarginSellingPrice(Mode, WholeSalePrice) {



            //==================> Selling Price CGST
            var FixedMargin = WholeSalePrice;
            var GrossCost = txtGrossCost.val();
            if (GrossCost == '') {
                return false;
            }
            var NetCost = txtNetCost.val();
            if (NetCost == '') {
                return false;
            }

            //Margin Based Set Basic Selling 
            var BasicSelling = (GrossCost * FixedMargin) / 100
            BasicSelling = parseFloat(GrossCost) + parseFloat(BasicSelling)
            //txtBasicSelling.val(parseFloat(BasicSelling).toFixed(2))
            var WholeSalePriceResult;

            //==================> Output SGST
            var OutputSGSTPrc = txtOutputSGSTPrc.val();
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputSGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputSGSTAmt = (BasicSelling * OutputSGSTPrc) / 100;
                if (OutputSGSTAmt == '') {
                    OutputSGSTAmt = '0';
                }
                //txtOutputSGSTAmt.val(parseFloat(OutputSGSTAmt).toFixed(2));

                //var OutputCGSTAmt = txtOutputCGSTAmt.val();
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                if (OutputCGSTAmt == '') {
                    OutputCGSTAmt = '0';
                }

                if (OutputCGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt);
                }
                else {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt)
                }
            }

            //==================> Output CGST
            var OutputCGSTPrc = txtOutputCGSTPrc.val();
            if (OutputCGSTPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCGSTAmt = (BasicSelling * OutputCGSTPrc) / 100;
                //if (OutputCGSTAmt == '') {
                //    OutputCGSTAmt = '0';
                //}
                //txtOutputCGSTAmt.val(parseFloat(OutputCGSTAmt).toFixed(2));

                //var OutputSGSTAmt = txtOutputSGSTAmt.val();
                if (OutputSGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt) + parseFloat(OutputSGSTAmt);
                }
                else {
                    WholeSalePrice = parseFloat(BasicSelling) + parseFloat(OutputCGSTAmt)
                }
            }

            //==================> Output CESS
            var OutputCESSPrc = txtOutputCESSPrc.val();
            if (OutputCESSPrc != '') {
                if (BasicSelling == '') {
                    return false;
                }
                var OutputCESSAmt = (BasicSelling * OutputCESSPrc) / 100;
                if (OutputCESSAmt == '') {
                    OutputCESSAmt = '0';
                }
                //txtOutputCESSAmt.val(parseFloat(OutputCESSAmt).toFixed(2));

                //var OutputSGSTAmt = txtOutputSGSTAmt.val();
                //var OutputCGSTAmt = txtOutputCGSTAmt.val();
                if (OutputSGSTAmt != '' && OutputCGSTAmt != '') {
                    WholeSalePriceResult = parseFloat(BasicSelling) + parseFloat(OutputSGSTAmt) + parseFloat(OutputCGSTAmt) + parseFloat(OutputCESSAmt);
                }
                else {
                    WholeSalePrice = parseFloat(BasicSelling) + parseFloat(OutputCESSAmt)
                }
            }

            // Assign WholeSale Price
            if (Mode == '1') {
                txtWholePrice1Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
            else if (Mode == '2') {
                txtWholePrice2Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
            else if (Mode == '3') {
                txtWholePrice3Popup.val(parseFloat(WholeSalePriceResult).toFixed(2))
            }
        }

        function TaxLiabilityCalc() {
            //==================> Vat Liability SGST
            var OutputSGST = txtOutputSGSTAmt.val();
            var InputSGST = txtInputSGSTAmt.val();
            if (OutputSGST != '' && InputSGST != '') {
                var VatLiabilitySGST = parseFloat(OutputSGST) - parseFloat(InputSGST);
                txtVatLiabilitySGSTAmt.val(parseFloat(VatLiabilitySGST).toFixed(2))
            }
            else {
                txtVatLiabilitySGSTAmt.val('0.00')
            }

            //==================> Vat Liability CGST
            var OutputCGST = txtOutputCGSTAmt.val();
            var InputCGST = txtInputCGSTAmt.val();
            if (OutputCGST != '' && InputCGST != '') {
                var VatLiabilityCGST = parseFloat(OutputCGST) - parseFloat(InputCGST);
                txtVatLiabilityCGSTAMt.val(parseFloat(VatLiabilityCGST).toFixed(2))
            }
            else {
                txtVatLiabilityCGSTAMt.val('0.00')
            }

            var SellingPrice = txtSellingPrice.val();
            if (SellingPrice != "") {
                debugger;
                var inputCessAmt = 0, outputCessAmt = 0, cessLiability = 0, liabilityPerc = 0, liabilityAmt = 0;
                //var VatLiabilitySGSTPrc = (txtVatLiabilitySGSTAmt.val() / SellingPrice) * 100
                //txtVatLiabilitySGSTPrc.val(parseFloat(VatLiabilitySGSTPrc).toFixed(2))
                //var VatLiabilityCGSTPrc = (txtVatLiabilityCGSTAMt.val() / SellingPrice) * 100
                //txtVatLiabilityCGSTPrc.val(parseFloat(VatLiabilityCGSTPrc).toFixed(2))

                inputCessAmt = $('#<%=txtInputCESSAmt.ClientID%>').val();
                outputCessAmt = $('#<%=txtOutputCESSAmt.ClientID%>').val();
                cessLiability = parseFloat(outputCessAmt) - parseFloat(inputCessAmt);

                /// work done by saravanan 16-02-2018
                liabilityAmt = parseFloat(txtVatLiabilitySGSTAmt.val()) + parseFloat(txtVatLiabilityCGSTAMt.val()) + parseFloat(cessLiability);
                $('#<%=txtGSTLiability.ClientID %>').val(liabilityAmt.toFixed(2));
                liabilityPerc = (parseFloat($('#<%=txtGSTLiability.ClientID %>').val()) / parseFloat(SellingPrice)) * 100
                $('#<%=txtGSTLiabilityPer.ClientID %>').val(liabilityPerc.toFixed(2));
            }
        }

        //work done by saravanan 2017-11-27

        ///Selling Price Round Off
        function SellingPriceRoundOff(SellingPrice) {

            var sellingprice = 0, roundOff = 0;
            sellingprice = SellingPrice;

            try {
                if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '1') //1 Ruppees RoundOff
                {
                    NetSellingprice = (Math.round(SellingPrice)).toFixed(2); //1 Ruppees RoundOff
                }
                else if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '50') {
                    NetSellingprice = (Math.round(SellingPrice * 2) / 2).toFixed(2); //50 Paise RoundOff
                }
                else if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == '25') {
                    NetSellingprice = (Math.round(SellingPrice * 4) / 4).toFixed(2); //25 Paise RoundOff
                }
                else {
                    NetSellingprice = SellingPrice;

                }

                roundOff = (parseFloat(NetSellingprice) - parseFloat(sellingprice)).toFixed(2);
                if (parseFloat(roundOff) > 0) {
                    roundOff = "+" + roundOff;
                }
                $('#<%=txtRoundOff.ClientID %>').val(roundOff);
                return NetSellingprice;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }


        }


        <%-- function SellingPriceRoundOff(SellingPrice) {
            if ($('#<%=hdfSellingPriceRoundOff.ClientID %>').val() == 'Y') {
                return (Math.round(SellingPrice * 2) / 2).toFixed(2); //50 Paise RoundOff
            }
            else {
                return (Math.round(SellingPrice * 4) / 4).toFixed(2); //25 Paise RoundOff
            }
        }--%>

    </script>
    <%------------------------------------------- Selling Price Calc Dialog Open -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenInvPriceChange() {

            //fncdecimal();
            fncFocusOutForNumberTextControl();
            if ($('#<%=txtItemCode.ClientID %>').val() == '') {
                ShowPopupMessageBox('Invalid Inventory Code');
                return false;
            }

            if ($('#<%=hfBatch.ClientID %>').val() == 'True') {
                $("#dialog-confirm").dialog("open");  // Open Popup
                return;
            }

            if ($("#dialog-InvPrice").dialog('isOpen') === true) {
                return;
            }

            //setTimeout(function () {
            //$("#dialog-InvPrice").parent().appendTo($("form:first")); // For After Postback 
            //}, 50);

            //fncinitializeinventoryCal();
            fncRemoveDisableAttForInputGST();
            $("#dialog-InvPrice").dialog("open");  // Open Popup
            fncOpenDialogWithDetail();



        }

        // Append Dailog 
        function fncAppendToFirstPriceDailog() {
            try {
                //fncinitializeinventoryCal();
                $("#dialog-InvPrice").parent().appendTo($("form:first")); // For After Postback 

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncRemoveDisableAttForInputGST() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() != "Liquor") {
                    setTimeout(function () {
                        $('#<%=ddlInputIGST.ClientID%>').removeAttr("disabled");
                        $('#<%=ddlInputIGST.ClientID%>').trigger("liszt:updated");
                        $('#<%=ddlInputIGST.ClientID%>').trigger("liszt:open");
                    }, 50);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <script type="text/javascript">

        /// Load Initial Data for Item History
        function fncInitialLoad() {
            try {
                fncOpenDialogWithDetail();
                fncBindBreakPriceQty();
                fncBindBarcode();
                fncDropdownEnterKeyVal();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncOpenDialogWithDetail() {

            //alert('fncOpenDialogWithDetail');
            var obj = {};

            if ($('#<%=rbtCost.ClientID %>').is(':checked')) {

                $('#<%=rbtCostPopup.ClientID %>').prop('checked', true);  // unchecked     
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', false);
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked

                $(txtMRPMarkDown).attr("disabled", "disabled");
                $(txtMRPMarkDown).val("0.00"); // work modified by saravanan 01-12-2017
                $(txtDiscountPrc1).removeAttr("disabled");
                $(txtDiscountPrc2).removeAttr("disabled");
                $(txtDiscountPrc3).removeAttr("disabled");
                $(txtDiscountAmt).removeAttr("disabled");

                $("#markup").css("background", "#4169e1");
                $("#markup").css("color", "white");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

                $("#PMD").css("background", "");
                $("#PMD").css("color", "");

            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked     
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', false);
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', true);  // checked

                $("#markdown").css("background", "#4169e1");
                $("#markdown").css("color", "white");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#PMD").css("background", "");
                $("#PMD").css("color", "");

                $('#<%=hdfMarkDown.ClientID %>').val('Y');

            }
            else {

                $('#<%=hdfMarkDown.ClientID %>').val('N');

                $(txtMRPMarkDown).removeAttr("disabled");
                $(txtDiscountPrc1).attr("disabled", "disabled");
                $(txtDiscountPrc2).attr("disabled", "disabled");
                $(txtDiscountPrc3).attr("disabled", "disabled");
                $(txtDiscountAmt).attr("disabled", "disabled");

                /// work done by saravanan 16-02-2018
                $('#<%=rbtCostPopup.ClientID %>').prop('checked', false);  // unchecked                      
                $('#<%=rbtMrpPopup.ClientID %>').prop('checked', false);  // checked
                $('#<%=rbtPartialMD.ClientID %>').prop('checked', true);


                $("#PMD").css("background", "#4169e1");
                $("#PMD").css("color", "white");

                $("#markup").css("background", "");
                $("#markup").css("color", "");

                $("#markdown").css("background", "");
                $("#markdown").css("color", "");

            }

            if ($('#<%=txtGSTGroup.ClientID %>').val() != '') {
                txtHSNCode = $('#<%=txtHSNCode.ClientID %>');
            txtHSNCode.val($('#<%=txtGSTGroup.ClientID %>').val());
            }

            obj.InvCode = $('#<%=txtItemCode.ClientID %>').val();
            obj.invStatus = $('#<%=hidinventoryStatus.ClientID %>').val();
            obj.priceEnt = $('#<%=hidPriceEntered.ClientID %>').val();

            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/GetInvPriceDetail") %>',
                //data: "{'InvCode':'" + $('#<%=txtItemCode.ClientID %>').val() + "'}",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    if ($('#<%=hidPriceEntered.ClientID %>').val() == "1")
                        return;



                    $('#<%=txtItemCodePopup.ClientID %>').val($('#<%=txtItemCode.ClientID %>').val());
                    $('#<%=txtItemNamePopup.ClientID %>').val($('#<%=txtItemName.ClientID %>').val());

                    //setTimeout(function(){
                    $('#<%=ddlInputSGST.ClientID %>').val(msg.d[0]);
                    $('#<%=ddlInputSGST.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlInputCGST.ClientID %>').val(msg.d[1]);
                    $('#<%=ddlInputCGST.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlInputIGST.ClientID %>').val(msg.d[2]);
                    $('#<%=ddlInputIGST.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlOutputSGST.ClientID %>').val(msg.d[3]);
                    $('#<%=ddlOutputSGST.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlOutputCGST.ClientID %>').val(msg.d[4]);
                    $('#<%=ddlOutputCGST.ClientID %>').trigger("liszt:updated");
                    $('#<%=ddlOutputIGST.ClientID %>').val(msg.d[5]);
                    $('#<%=ddlOutputIGST.ClientID %>').trigger("liszt:updated");
                    // }, 50);

                    if (msg.d[6] == 'C') {
                        $('#<%=rbtCostPopup.ClientID %>').prop('checked', $(this).prop("checked"));
                    }
                    else {
                        $('#<%=rbtMrpPopup.ClientID %>').prop('checked', $(this).prop("checked"));
                    }
                    $('#<%=txtMRP.ClientID %>').val(msg.d[7]);
                    $('#<%=txtBasicCost.ClientID %>').val(msg.d[8]);
                    $('#<%=txtDiscountPrc1.ClientID %>').val(msg.d[9]);
                    $('#<%=txtDiscountPrc2.ClientID %>').val(msg.d[10]);
                    $('#<%=txtDiscountPrc3.ClientID %>').val(msg.d[11]);
                    $('#<%=txtDiscountAmt.ClientID %>').val(msg.d[12]);
                    $('#<%=txtGrossCost.ClientID %>').val(msg.d[13]);
                    $('#<%=txtInputSGSTPrc.ClientID %>').val(msg.d[14]);
                    $('#<%=txtInputSGSTAmt.ClientID %>').val(msg.d[15]);
                    $('#<%=txtInputCGSTPrc.ClientID %>').val(msg.d[16]);
                    $('#<%=txtInputCGSTAmt.ClientID %>').val(msg.d[17]);
                    $('#<%=txtNetCost.ClientID %>').val(msg.d[18]);
                    $('#<%=txtMarginSP.ClientID %>').val(msg.d[19]);
                    $('#<%=txtSPAmt.ClientID %>').val(msg.d[20]);
                    $('#<%=txtFixedMargin.ClientID %>').val(msg.d[21]);
                    $('#<%=txtBasicSelling.ClientID %>').val(msg.d[22]);
                    $('#<%=txtOutputSGSTPrc.ClientID %>').val(msg.d[23]);
                    $('#<%=txtOutputSGSTAmt.ClientID %>').val(msg.d[24]);
                    $('#<%=txtOutputCGSTPrc.ClientID %>').val(msg.d[25]);
                    $('#<%=txtOutputCGSTAmt.ClientID %>').val(msg.d[26]);
                    $('#<%=txtSellingPrice.ClientID %>').val(msg.d[27]);
                    $('#<%=txtVatLiabilitySGSTPrc.ClientID %>').val(msg.d[28]);
                    $('#<%=txtVatLiabilitySGSTAmt.ClientID %>').val(msg.d[29]);
                    $('#<%=txtVatLiabilityCGSTPrc.ClientID %>').val(msg.d[30]);
                    $('#<%=txtVatLiabilityCGSTAMt.ClientID %>').val(msg.d[31]);
                    $('#<%=txtWholePrice1Popup.ClientID %>').val(msg.d[32]);
                    $('#<%=txtWholePrice2Popup.ClientID %>').val(msg.d[33]);
                    $('#<%=txtWholePrice3Popup.ClientID %>').val(msg.d[34]);
                    $('#<%=txtMarginWPrice1.ClientID %>').val(msg.d[35]);
                    $('#<%=txtMarginWPrice2.ClientID %>').val(msg.d[36]);
                    $('#<%=txtMarginWPrice3.ClientID %>').val(msg.d[37]);
                    $('#<%=txtCESSPrc.ClientID %>').val(msg.d[40]);
                    $('#<%=txtInputCESSPrc.ClientID %>').val(msg.d[40]);
                    $('#<%=txtInputCESSAmt.ClientID %>').val(msg.d[41]);
                    $('#<%=txtOutputCESSPrc.ClientID %>').val(msg.d[44]);
                    $('#<%=txtOutputCESSAmt.ClientID %>').val(msg.d[45]);
                    $('#<%=txtAddCessAmt.ClientID %>').val(msg.d[47]);

                    /// work done by saravanan 15-02-2018
                    $('#<%=txtGSTLiability.ClientID %>').val(parseFloat(msg.d[29]) + parseFloat(msg.d[31]));
                    $('#<%=txtGSTLiabilityPer.ClientID %>').val(parseFloat(msg.d[28]) + parseFloat(msg.d[30]));

                    fncDisableEXGST();
                <%--    if ($('#<%=ddlStockType.ClientID %>').find("option:selected").text().trim() == "Liquor") {

                        ddlInputIGST.append($("<option></option>").val('EIGST').html('EIGST 0 % - 0.00'));
                        $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");

                        $('#<%=ddlInputIGST.ClientID%>').val("EIGST");
                        $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                        fncInputGSTValueChange();

                    }
                    else {
                        $("select[id$=ddlInputIGST] option[value='EIGST']").remove();
                        $('#<%=ddlInputIGST.ClientID%>').removeAttr("disabled");
                        $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                    }--%>

                    //===================> Margin MRP Amt  
                    var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

                    MRP = $('#<%=txtMRP.ClientID %>').val();
                    GrossCost = $('#<%=txtGrossCost.ClientID %>').val();
                    array = $('#<%=ddlOutputIGST.ClientID %>').find("option:selected").text().trim().split("-");
                    GSTPer = array[1];
                    GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
                    MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID %>').val());
                    MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);

                    var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
                    MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
                    $('#<%=txtMarginMRP.ClientID %>').val(parseFloat(MarginMRPPrc).toFixed(2))

                    /// work done by saravanan 15-02-2015

                    MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
                    $('#<%=txtMRPAmt.ClientID %>').val(parseFloat(MarginMRPAmt).toFixed(2));


                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                }
            });

            ///Disable SellingPrice 
            if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {
                $('#<%=txtSellingPrice.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice1Popup.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice2Popup.ClientID %>').attr("disabled", "disabled");
                $('#<%=txtWholePrice3Popup.ClientID %>').attr("disabled", "disabled");

            }
            else {
                $('#<%=txtSellingPrice.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice1Popup.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice2Popup.ClientID %>').removeAttr("disabled");
                $('#<%=txtWholePrice3Popup.ClientID %>').removeAttr("disabled");
            }

            <%--  //===================> Margin MRP Amt  
            var array, GSTPer, MRPGst, MarginMRPAmt, MRP, GrossCost;

            MRP = $('#<%=txtMRP.ClientID %>').val();
            GrossCost = $('#<%=txtGrossCost.ClientID %>').val();
            array = $('#<%=ddlOutputIGST.ClientID %>').find("option:selected").text().trim().split("-");
            GSTPer = array[1];
            GSTPer = parseFloat(GSTPer) + parseFloat($('#<%=txtCESSPrc.ClientID%>').val());
            MRP = parseFloat(MRP) - parseFloat($('#<%=txtAddCessAmt.ClientID %>').val());
            MRPGst = (parseFloat(MRP) * parseFloat(GSTPer)) / (parseFloat(GSTPer) + 100);

            var MarginMRPPrc = (parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst)) / parseFloat(txtMRP.val()) * 100;
            MarginMRPPrc = isNaN(MarginMRPPrc) ? '0.00' : MarginMRPPrc;
            $('#<%=txtMarginMRP.ClientID %>').val(parseFloat(MarginMRPPrc).toFixed(2))

            /// work done by saravanan 15-02-2015

            MarginMRPAmt = parseFloat(MRP) - parseFloat(GrossCost) - parseFloat(MRPGst);
            $('#<%=txtMRPAmt.ClientID %>').val(parseFloat(MarginMRPAmt));--%>

             <%-- if ($('#<%=hidinventoryStatus.ClientID %>').val() == "New" && $('#<%=ddlStockType.ClientID %>').find("option:selected").text().trim() != "Liquor") {
                  setTimeout(function () {                      
                            $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:open");
                        }, 50);
                   }--%>

            return false;
        }

        function fncDisableEXGST() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() == "Liquor") {

                    ddlInputIGST.append($("<option></option>").val('EIGST').html('EIGST 0 % - 0.00'));
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");

                    $('#<%=ddlInputIGST.ClientID%>').val("EIGST");
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                    fncInputGSTValueChange();

                }
                else {
                    $("select[id$=ddlInputIGST] option[value='EIGST']").remove();
                    $('#<%=ddlInputIGST.ClientID%>').removeAttr("disabled");
                    $('#<%=ddlInputSGST.ClientID%>').trigger("liszt:updated");
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //function fncInputGSTOpen

        /// Input GST Value Changed
        function fncInputGSTValueChange() {
            try {

                var InputIGSTCode = ddlInputIGST.val();
                var GSTObj, GSTType;

                ddlInputIGST.val(InputIGSTCode);
                $('#<%=ddlInputIGST.ClientID%>').attr("disabled", "disabled");
                ddlInputIGST.trigger("liszt:updated");
                //=============================>  Output IGST Change
                ddlOutputIGST.val(InputIGSTCode);
                ddlOutputIGST.trigger("liszt:updated");

                if (InputIGSTCode == "EIGST")
                    GSTType = "EIGST";
                else
                    GSTType = "IGST";

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetGstDetail") %>',
                    data: "{'GstCode':'" + InputIGSTCode + "', 'GstType':'" + GSTType + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        //=============================>  Input SGST Change
                        GSTObj = jQuery.parseJSON(msg.d);

                        if (GSTObj.length > 0) {
                            for (var i = 0; i < GSTObj.length; i++) {
                                if (GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                    //=============================>  Input SGST Change
                                    ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputSGST.trigger("liszt:updated");

                                    var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                    var InputSGSTPrc = array[1];
                                    if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                        txtInputSGSTPrc.val(InputSGSTPrc);
                                    }

                                    //=============================>  Output SGST Change
                                    ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputSGST.trigger("liszt:updated");

                                    var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                    var OutputSGSTPrc = array[1];
                                    if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                        txtOutputSGSTPrc.val(OutputSGSTPrc);
                                        fncCalcSelingPice();
                                    }

                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                    //=============================>  Input CGST Change
                                    ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputCGST.trigger("liszt:updated");

                                    var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtInputCGSTPrc.val(InputCGSTPrc);
                                    }

                                    //=============================>  Output CGST Change
                                    ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputCGST.trigger("liszt:updated");

                                    var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                    var OutputCGSTPrc = array[1];
                                    if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                        txtOutputCGSTPrc.val(OutputCGSTPrc);
                                    }
                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("S") != -1 || GSTObj[i]["TaxCode"].indexOf("ES") != -1) {
                                    //=============================>  Input SGST Change
                                    ddlInputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputSGST.trigger("liszt:updated");

                                    var array = ddlInputSGST.find("option:selected").text().trim().split("-");
                                    var InputSGSTPrc = array[1];
                                    if (InputSGSTPrc != '' && InputSGSTPrc != '-- Select --') {
                                        txtInputSGSTPrc.val(InputSGSTPrc);
                                    }

                                    //=============================>  Output SGST Change
                                    ddlOutputSGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputSGST.trigger("liszt:updated");

                                    var array = ddlOutputSGST.find("option:selected").text().trim().split("-");
                                    var OutputSGSTPrc = array[1];
                                    if (OutputSGSTPrc != '' && OutputSGSTPrc != '-- Select --') {
                                        txtOutputSGSTPrc.val(OutputSGSTPrc);
                                        fncCalcSelingPice();
                                    }

                                }
                                else if (GSTObj[i]["TaxCode"].indexOf("C") != -1 || GSTObj[i]["TaxCode"].indexOf("EC") != -1) {
                                    //=============================>  Input CGST Change
                                    ddlInputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlInputCGST.trigger("liszt:updated");

                                    var array = ddlInputCGST.find("option:selected").text().trim().split("-");
                                    var InputCGSTPrc = array[1];
                                    if (InputCGSTPrc != '' && InputCGSTPrc != '-- Select --') {
                                        txtInputCGSTPrc.val(InputCGSTPrc);
                                    }

                                    //=============================>  Output CGST Change
                                    ddlOutputCGST.val(GSTObj[i]["TaxCode"])
                                    ddlOutputCGST.trigger("liszt:updated");

                                    var array = ddlOutputCGST.find("option:selected").text().trim().split("-");
                                    var OutputCGSTPrc = array[1];
                                    if (OutputCGSTPrc != '' && OutputCGSTPrc != '-- Select --') {
                                        txtOutputCGSTPrc.val(OutputCGSTPrc);
                                    }
                                }//


                            }

                            fncCalcSelingPice();
                        }//End

                        /// work done by saravanan 01-12-2017
                        $('#<%=txtCESSPrc.ClientID %>').select();

                    },
                    error: function (data) {
                        ShowPopupMessageBox('Something Went Wrong')
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err);
            }
        }

    </script>
    <%--------------------------------------- Selling Price Save --------------------------------------%>
    <script type="text/javascript">

        function fncSaveInvPriceChange() {
            ////bugger;
            //============================> Validations
            if (txtMRP.val() == '0.00') {
                popUpObjectForSetFocusandOpen = txtMRP;
                ShowPopupMessageBoxandFocustoObject('MRP Should Not be Empty!!!');
                return false;
            }
            if (ddlInputSGST.val() == '') {
                popUpObjectForSetFocusandOpen = ddlInputSGST;
                ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input SGST TaxCode');
                return false;
            }
            if (ddlInputCGST.val() == '') {
                popUpObjectForSetFocusandOpen = ddlInputCGST;
                ShowPopupMessageBoxandOpentoObject('Please Enter Valid Input CGST TaxCode');
                return false;
            }
            if (txtSellingPrice.val() == '0.00') {
                popUpObjectForSetFocusandOpen = txtSellingPrice;
                ShowPopupMessageBoxandFocustoObject('Invalid Selling Price');
                return false;
            }
            if (parseFloat(txtMRP.val()) < parseFloat(txtSellingPrice.val())) {
                popUpObjectForSetFocusandOpen = txtSellingPrice;
                ShowPopupMessageBoxandFocustoObject('Selling Price should be less than  MRP !!!');
                return false;
            }

            if (parseFloat(txtSellingPrice.val()) < parseFloat(txtNetCost.val())) {
                popUpObjectForSetFocusandOpen = txtSellingPrice;
                ShowPopupMessageBoxandFocustoObject('Selling Price Lower than NetCost!!!');
                return false;
            }
            if (parseFloat(txtWholePrice1Popup.val()) < parseFloat(txtNetCost.val())) {
                popUpObjectForSetFocusandOpen = txtWholePrice1Popup;
                ShowPopupMessageBoxandFocustoObject('Whole Sale Price 1 Lower than NetCost!!!');
                return false;
            }
            if (parseFloat(txtWholePrice2Popup.val()) < parseFloat(txtNetCost.val())) {
                popUpObjectForSetFocusandOpen = txtWholePrice2Popup;
                ShowPopupMessageBoxandFocustoObject('Whole Sale Price 2 Lower than NetCost!!!');
                return false;
            }
            if (parseFloat(txtWholePrice3Popup.val()) < parseFloat(txtNetCost.val())) {
                popUpObjectForSetFocusandOpen = txtWholePrice3Popup;
                ShowPopupMessageBoxandFocustoObject('Whole Sale Price 3 Lower than NetCost!!!');
                return false;
            }

            if ($('#<%=hidWpriceGreaterSP.ClientID %>').val() == "N") {
                if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice1Popup.val())) {
                    popUpObjectForSetFocusandOpen = txtWholePrice1Popup;
                    ShowPopupMessageBoxandFocustoObject('Whole Sale Price 1 should be less than  MRP !!!');
                    return false;
                }
                if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice2Popup.val())) {
                    popUpObjectForSetFocusandOpen = txtWholePrice2Popup;
                    ShowPopupMessageBoxandFocustoObject('Whole Sale Price 2 should be less than  MRP !!!');
                    return false;
                }
                if (parseFloat(txtMRP.val()) < parseFloat(txtWholePrice3Popup.val())) {
                    popUpObjectForSetFocusandOpen = txtWholePrictxtWholePrice3Popupe2Popup;
                    ShowPopupMessageBoxandFocustoObject('Whole Sale Price 3 should be less than  MRP !!!');
                    return false;
                }
            }

            var obj = {};
            obj.InventoryCode = $('#<%=txtItemCodePopup.ClientID %>').val();
            if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                obj.PricingType = 'SELLINGPRICE';
                obj.PurchaseBy = 'C';
                obj.MarginFixType = 'C';
            }
            else if ($('#<%=rbtMRP.ClientID %>').is(':checked')) {
                obj.PricingType = 'MRP';
                obj.PurchaseBy = 'M';
                obj.MarginFixType = 'M';
            }
            else {
                obj.PricingType = 'SELLINGPRICE';
                obj.PurchaseBy = 'M';
                obj.MarginFixType = 'C';
            }

            obj.HSNCode = txtHSNCode.val();
           <%-- if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                obj.PurchaseBy = 'C';
            }
            else {
                obj.PurchaseBy = 'M';
            }

              if ($('#<%=rbtCost.ClientID %>').is(':checked')) {
                
            }
            else {
                obj.MarginFixType = 'M';
            }--%>

            obj.MRP = txtMRP.val();
            obj.BasicCost = txtBasicCost.val();
            obj.DiscPer1 = txtDiscountPrc1.val();
            obj.DiscPrc2 = txtDiscountPrc2.val();
            obj.DiscPrc3 = txtDiscountPrc3.val();
            obj.DiscAmt = txtDiscountAmt.val();
            obj.GrossCost = txtGrossCost.val();
            obj.NetCost = txtNetCost.val();
            obj.MarginPer = '0';
            //obj.MarginAmt = txtFixedMargin.val();
            obj.MarginAmt = '0';
            obj.BasicSelling = txtBasicSelling.val();
            obj.NetSellingPrice = txtSellingPrice.val();
            obj.MFPer = txtMarginSP.val();
            obj.MFAmt = txtSPAmt.val();
            obj.EarnedMargin = txtFixedMargin.val();
            obj.WPrice1 = txtWholePrice1Popup.val() == null ? "0.00" : txtWholePrice1Popup.val();
            obj.WPRice2 = txtWholePrice2Popup.val() == null ? "0.00" : txtWholePrice2Popup.val();
            obj.WPrice3 = txtWholePrice3Popup.val() == null ? "0.00" : txtWholePrice3Popup.val();
            obj.MPWPrice1 = txtMarginWPrice1.val() == null ? "0.00" : txtMarginWPrice1.val();
            obj.MPWPrice2 = txtMarginWPrice2.val() == null ? "0.00" : txtMarginWPrice2.val();
            obj.MPWPrice3 = txtMarginWPrice3.val() == null ? "0.00" : txtMarginWPrice3.val();
            obj.MRPPurchase = txtMRPMarkDown.val() == "" ? "0.00" : txtMRPMarkDown.val();
            obj.ITaxCode1 = ddlInputSGST.val();
            obj.ITaxCode2 = ddlInputCGST.val();
            obj.ITaxCode3 = ddlInputIGST.val();
            obj.ITaxCode4 = '';
            obj.ITaxPer1 = txtInputSGSTPrc.val();
            obj.ITaxPer2 = txtInputCGSTPrc.val();
            obj.ITaxPer3 = parseFloat(txtInputSGSTPrc.val()) + parseFloat(txtInputCGSTPrc.val());
            obj.ITaxPer4 = txtInputCESSPrc.val();
            obj.ITaxAmt1 = txtInputSGSTAmt.val();
            obj.ITaxAmt2 = txtInputSGSTAmt.val();
            obj.ITaxAmt3 = parseFloat(txtInputSGSTAmt.val()) + parseFloat(txtInputSGSTAmt.val());
            obj.ITaxAmt4 = txtInputCESSAmt.val();
            obj.OTaxCode1 = ddlOutputSGST.val();
            obj.OTaxCode2 = ddlOutputCGST.val();
            obj.OTaxCode3 = ddlOutputIGST.val();
            obj.OTaxCode4 = '';
            obj.OTaxPer1 = txtOutputSGSTPrc.val();
            obj.OTaxPer2 = txtOutputCGSTPrc.val();
            obj.OTaxPer3 = parseFloat(txtOutputSGSTPrc.val()) + parseFloat(txtOutputCGSTPrc.val());
            obj.OTaxPer4 = txtOutputCESSPrc.val();
            obj.OTaxAmt1 = txtOutputCGSTAmt.val();
            obj.OTaxAmt2 = txtOutputSGSTAmt.val();
            obj.OTaxAmt3 = parseFloat(txtOutputCGSTAmt.val()) + parseFloat(txtOutputSGSTAmt.val());
            obj.OTaxAmt4 = txtOutputCESSAmt.val();
            obj.VatliabilityPer1 = txtVatLiabilitySGSTPrc.val();
            obj.VatliabilityPer2 = txtVatLiabilityCGSTPrc.val();
            obj.VatliabilityPer3 = '0';
            obj.VatliabilityAmt1 = txtVatLiabilitySGSTAmt.val();
            obj.VatliabilityAmt2 = txtVatLiabilityCGSTAMt.val();
            obj.VatliabilityAmt3 = '0';
            obj.ITaxAmt5 = $('#<%=txtAddCessAmt.ClientID %>').val();
            obj.OTaxAmt5 = $('#<%=txtAddCessAmt.ClientID %>').val();


            $('#<%=hidInvPriceDetail.ClientID %>').val(JSON.stringify(obj));
            //$("#dialog-InvPrice").dialog("destroy");
            $("#dialog-InvPrice").dialog("close");



            // Set Value                        
            $('#<%=txtFetchSellingPrice.ClientID %>').val(parseFloat(txtSellingPrice.val()).toFixed(2));
            $('#<%=lblMRP.ClientID %>').text(parseFloat(txtMRP.val()).toFixed(2));
            $('#<%=hdfMRP.ClientID %>').val(parseFloat(txtMRP.val()).toFixed(2));
            $('#<%=lblNetCost.ClientID %>').text(parseFloat(txtNetCost.val()).toFixed(2));
            $('#<%=hdfNetCost.ClientID %>').val(parseFloat(txtNetCost.val()).toFixed(2));
            $('#<%=txtWPrice1.ClientID %>').val(parseFloat(txtWholePrice1Popup.val() == null ? '0.00' : txtWholePrice1Popup.val()).toFixed(2));
            $('#<%=txtWPrice2.ClientID %>').val(parseFloat(txtWholePrice2Popup.val() == null ? '0.00' : txtWholePrice2Popup.val()).toFixed(2));
            $('#<%=txtWPrice3.ClientID %>').val(parseFloat(txtWholePrice3Popup.val() == null ? '0.00' : txtWholePrice3Popup.val()).toFixed(2));
            $('#<%=txtGSTGroup.ClientID %>').val(txtHSNCode.val());
            $('#<%=hdfBasicCost.ClientID %>').val($('#<%=txtBasicCost.ClientID %>').val());

            $('#<%=lnkSave.ClientID %>').focus();



            //alert(obj);
            //ShowPopupMessageBox(JSON.stringify(obj));
            //return false;

           <%-- $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/SaveInvPriceDetail") %>',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                    if (msg.d == "1") {
                        //alert("Price Change Update Successfully");
                        ShowPopupMessageBoxandFocustoObject("Price Change Update Successfully");

                        // Set Value                        
                        $('#<%=txtFetchSellingPrice.ClientID %>').val(parseFloat(txtSellingPrice.val()).toFixed(2));
                        $('#<%=lblMRP.ClientID %>').text(parseFloat(txtMRP.val()).toFixed(2));
                        $('#<%=hdfMRP.ClientID %>').val(parseFloat(txtMRP.val()).toFixed(2));
                        $('#<%=lblNetCost.ClientID %>').text(parseFloat(txtNetCost.val()).toFixed(2));
                        $('#<%=hdfNetCost.ClientID %>').val(parseFloat(txtNetCost.val()).toFixed(2));
                        $('#<%=txtWPrice1.ClientID %>').val(parseFloat(txtWholePrice1Popup.val() == null ? '0.00' : txtWholePrice1Popup.val()).toFixed(2));
                        $('#<%=txtWPrice2.ClientID %>').val(parseFloat(txtWholePrice2Popup.val() == null ? '0.00' : txtWholePrice2Popup.val()).toFixed(2));
                        $('#<%=txtWPrice3.ClientID %>').val(parseFloat(txtWholePrice3Popup.val() == null ? '0.00' : txtWholePrice3Popup.val()).toFixed(2));
                        $('#<%=txtGSTGroup.ClientID %>').val(txtHSNCode.val());
                        $('#<%=hdfBasicCost.ClientID %>').val($('#<%=txtBasicCost.ClientID %>').val());


                        $("#dialog-InvPrice").dialog("destroy");
                        popUpObjectForSetFocusandOpen = $('#<%=lnkSave.ClientID %>');

                    }
                    else {
                        ShowPopupMessageBox("Price Change Update Failed");
                    }
                },
                error: function (data) {
                    ShowPopupMessageBox('Something Went Wrong')
                }
            });--%>
        }

        //=====================> Clear Price Change Field
        function fncClearPriceChange() {
            $("#dialog-InvPrice").find("input[type=text]").val("0.00");
            $("#dialog-InvPrice").find("select").val(0);
            $("#dialog-InvPrice").find("select").trigger("liszt:updated");
            $('#<%=txtItemCodePopup.ClientID %>').val($('#<%=txtItemCode.ClientID %>').val());
            $('#<%=txtItemNamePopup.ClientID %>').val($('#<%=txtItemName.ClientID %>').val());
            $('#<%=hidPriceEntered.ClientID %>').val("0"); // work done by saravanan 01-12-2017
        }

    </script>
    <%------------------------------------------- Batch Grid Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenBatchDetail() {
            $("#dialog-batch").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 550,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- PR Req Grid Open Popup -------------------------------------------%>
    <script type="text/javascript">
        function fncOpenPRRequest() {
            $("#dialog-PRReq").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 850,
                modal: true,
                show: {
                    effect: "fade",
                    duration: 100
                },
                hide: {
                    effect: "fade",
                    duration: 500
                },
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
    </script>
    <%------------------------------------------- Image Preview -------------------------------------------%>
    <script type="text/javascript">
        function showpreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imgpreview.ClientID %>').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <%------------------------------------------- Inventory Child functions-------------------------------------------%>
    <script type="text/javascript">

        <%--  function AddbuttonClick() {
            try {

                if ($('#<%=txtItemCode.ClientID %>').val() == '') {

                    ShowPopupMessageBox("Please check Parent ItemCode");
                    return false;
                }
                if ($('#<%=txtChildCode.ClientID %>').val() == '') {

                    // work done by saravanan 2017-11-28
                    ShowPopupMessageBoxandFocustoObject("Please check Child ItemCode");
                    popUpObjectForSetFocusandOpen = $('#<%=txtChildCode.ClientID %>');
                    //$('#<%=txtChildCode.ClientID %>').focus().select();
                    return false;
                }


                var checked = false;
                var txtitem = $('#<%=txtChildCode.ClientID %>').val();
                $("#<%=grdChildItem.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td.Itemcode").html();

                    //ShowPopupMessageBox(gitem);

                    if ($.trim(gitem) == $.trim(txtitem)) {
                        ShowPopupMessageBox("Item already Exists !");
                        checked = true;
                    }
                });

                var ResultArray = [];
                $('#<%=grdChildItem.ClientID %>').find('input:hidden').each(function () {
                    ResultArray.push($(this).val());
                    //ShowPopupMessageBox($(this).val());
                });

                if ($.inArray(txtitem, ResultArray) != -1) {
                    // found it
                    //ShowPopupMessageBox(ResultArray)
                    ShowPopupMessageBox("Item already Exists !");
                    checked = true;
                }
                //ShowPopupMessageBox(("#hidenParentcode").val());

                if (checked == false)
                    AddRow();

                ClearTextBox();

                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
                return false;
            }

        }

        function ClearTextBox() {

            $("#divAdditem").find("input[type=text]").val('');
            $("#hidenChildCode").val('');
            $('#<%=txtChildCode.ClientID %>').prop('disabled', false);
            $('#<%=txtChildCode.ClientID %>').focus().select();
            return false;
        }--%>


        <%--        function AddRow() {

            try {
                ////bugger;

                //                $('#<%=txtChildCode.ClientID %>').val($.trim(i.item.val));
                //                $('#<%=txtCdescription.ClientID %>').val($.trim(i.item.val2));
                //                $('#<%=txtQtyonHand.ClientID %>').val($.trim(i.item.val3));
                //                $('#<%=txtCuom.ClientID %>').val($.trim(i.item.val4));
                //                $('#<%=txtCweight.ClientID %>').val($.trim(i.item.val5));
                //                $('#<%=txtSoldQty.ClientID %>').val($.trim(i.item.val6));
                //                $('#<%=txtCweight.ClientID %>').focus().select();
                //                $('#<%=txtChildCode.ClientID %>').prop('disabled', true);

                var ChildCode = $("[id*=txtChildCode]").val();
                var Cdescription = $("[id*=txtCdescription]").val();
                var cQOH = $("[id*=txtQtyonHand]").val();
                var Cuom = $("[id*=txtCuom]").val();
                var Cweight = $("[id*=txtCweight]").val();
                var cSoldQty = $("[id*=txtSoldQty]").val();
                var ParentCode = $("[id*=txtItemCode]").val();

                if (ChildCode != "" && Cdescription != "") {
                    var row = $("[id*=grdChildItem] tr:last").clone();
                    // $("[id*=grdChildItem] tr:last").remove(); 
                    $("td:nth-child(1)", row).html(ChildCode);
                    $("td:nth-child(2)", row).html(Cdescription);
                    $("td:nth-child(3)", row).html(Cuom);
                    $("td:nth-child(4)", row).html(Cweight);
                    $("td:nth-child(5)", row).html(cQOH);
                    $("td:nth-child(6)", row).html(cSoldQty);
                    $("td:nth-child(7)", row).html(ParentCode);
                    $("td:nth-child(8)", row).html('<img src="../images/delete.png" />');
                    $("[id*=grdChildItem] tbody").append(row);

                }

                //                $("[id*=txtChildCode]").val();
                //                $("[id*=txtCdescription]").val();
                //                $("[id*=txtQtyonHand]").val();
                //                $("[id*=txtCuom]").val();
                //                $("[id*=txtCweight]").val();


                // $(this).closest("tr").find('td:not(:empty):first').remove();   

                //                $("#<%=grdChildItem.ClientID%> tr").each(function () {
                //                    if (!this.rowIndex) return;
                //                    var gitem = $(this).find("td").html();
                //                    if ($.trim(gitem) == '') {
                //                        $(this).remove();
                //                    }
                //                });

                $("#<%=grdChildItem.ClientID%> tr").each(function () {
                    if (!$.trim($(this).text())) $(this).remove();
                });

                XmlGridValue();

                return false;

            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }--%>


        <%-- function fnctxtChildQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    if ($('#<%=txtCweight.ClientID %>').val() <= 0) {

                        /// work done by saravanan 201-11-28
                        ShowPopupMessageBoxandFocustoObject('Invalid Weight Qty !');
                        popUpObjectForSetFocusandOpen = $('#<%=txtCweight.ClientID %>');
                        //$('#<%=txtCweight.ClientID %>').focus().select();

                        return false;
                    }
                    $('#<%=btnChildAdd.ClientID %>').click();

                    return false;
                }
            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }--%>

        <%--   function XmlGridValue() {
            try {
                $("#HiddenNewitem").val('Y');
                var xml = '<NewDataSet>';
                $("#<%=grdChildItem.ClientID %> tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        //ShowPopupMessageBox(cells.eq(0).text().trim());
                        if (cells.eq(0).text().trim() != '') {
                            xml += "<Table>";
                            xml += "<Inventorycode>" + cells.eq(0).text().trim() + "</Inventorycode>";
                            xml += "<Description>" + cells.eq(1).text().trim() + "</Description>";
                            xml += "<Uom>" + cells.eq(2).text().trim() + "</Uom>";
                            xml += "<weight>" + cells.eq(3).text().trim() + "</weight>";
                            xml += "<QtyonHand>" + cells.eq(4).text().trim() + "</QtyonHand>";
                            xml += "<SoldQty>" + cells.eq(5).text().trim() + "</SoldQty>";
                            xml += "<ParantCode>" + cells.eq(6).text().trim() + "</ParantCode>";
                            xml += "</Table>";
                        }
                    }
                });

                xml = xml + '</NewDataSet>'
                //ShowPopupMessageBox(xml);
                $("#HiddenXmldata").val(escape(xml));
                //ShowPopupMessageBox($("#HiddenXmldata").val());

                return false;

            }
            catch (err) {
                return false;
                ShowPopupMessageBox(err.Message);
            }
        }--%>

    </script>
    <%------------------------------------------- Validation -------------------------------------------%>
    <script type="text/javascript">
        function Validate() {

            var obj = {}, status = false;
            try {
                ItemNameVlidateValue = 0;
                if (($('#<%=txtUOMPurchase.ClientID %>').val() == "KG" || $('#<%=txtSalesUOM.ClientID %>').val() == "KG") && $('#<%=chkBatch.ClientID %>').prop("checked")) {
                    ShowPopupMessageBox('Item has not saved,KG items only belongs to Non Batch items');
                    return false;
                }

                LoadModel('hide');


                obj.Categorycode = $('#<%=txtInvcategory.ClientID %>').val();
                obj.Departmentcode = $('#<%=txtDepartment.ClientID %>').val();
                obj.Brandcode = $('#<%=txtBrand.ClientID %>').val();
                obj.Floor = $('#<%=txtFloor.ClientID %>').val();
                obj.Vendorcode = $('#<%=txtVendor.ClientID %>').val();
                obj.warehouse = $('#<%=txtWareHouse.ClientID %>').val();
                obj.manufacture = $('#<%=txtManafacture.ClientID %>').val();
                obj.Merchandise = $('#<%=txtMerchandise.ClientID %>').val();
                obj.Itemtype = $('#<%=txtItemType.ClientID %>').val();
                obj.style = $('#<%=txtStyle.ClientID %>').val();
                obj.size = $('#<%=txtSize.ClientID %>').val();
                obj.Origin = $('#<%=txtOrigin.ClientID %>').val();
                obj.Subcategory = $('#<%=txtSubCategory.ClientID %>').val();
                obj.Bin = $('#<%=txtBin.ClientID %>').val();
                obj.package = $('#<%=txtPackage.ClientID %>').val();
                obj.companycode = $('#<%=txtCCode.ClientID %>').val();
                obj.Class = $('#<%=txtClass.ClientID %>').val();
                obj.Subclass = $('#<%=txtSubClass.ClientID %>').val();
                obj.Section = $('#<%=txtSection.ClientID %>').val();
                obj.Shelf = $('#<%=txtShelf.ClientID %>').val();

                if (fncCheckmandatoryEmpty() == false)
                    return false;

                else if (ItemNameVlidateValue == "1") {
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid red");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncMandatoryStatus") %>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            fncShowSaveErrorInfor(msg.d);
                        }
                        else if (ItemNameVlidateValue == "1") {
                            $('#<%=txtItemName.ClientID %>').css("border", "1px solid red");
                            return false;
                        }
                        else {
                            $('#<%=hidItemname.ClientID%>').val(escape($('#<%=txtItemName.ClientID%>').val()));
                            fncConverthtmlTabletoJSON();
                            fncConverttblbarcodetoJson();
                            fncConverttblMultiplevendortoJson();

                            if ($("#<%=hidSaveStatus.ClientID %>").val() == "0") {
                                $("#<%=hidSaveStatus.ClientID %>").val('1');
                                $("#<%=btnSave.ClientID%>").click();
                            }
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            //return status;
        }

        function fncCheckmandatoryEmpty() {
            try {
                var status = true;
                if ($('#<%=txtShortName.ClientID %>').val() == "") {
                    $('#<%=txtShortName.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtShortName.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtItemName.ClientID %>').val() == "") {
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtVendor.ClientID %>').val() == "") {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    $('#<%=txtVendor.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtFloor.ClientID %>').val() == "") {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtFloor.ClientID %>');
                    $('#<%=txtFloor.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtItemType.ClientID %>').val() == "") {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtItemType.ClientID %>');
                    $('#<%=txtItemType.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtBrand.ClientID %>').val() == "") {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtBrand.ClientID %>');
                    $('#<%=txtBrand.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtDepartment.ClientID %>').val() == "") {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID %>');
                    $('#<%=txtDepartment.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid #383838");
                }
                if ($('#<%=txtInvcategory.ClientID %>').val() == "") {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                    status = false;
                }
                else {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid #383838");
                }
                if (status == false) {
                    fncToastInformation("Please enter valid data for red color field");
                }
                else if ($('#<%=hidPriceEntered.ClientID %>').val() == "0") {
                    fncRemoveDisableAttForInputGST();
                    $("#dialog-InvPrice").dialog("open");
                    fncOpenDialogWithDetail();
                    return false;
                }

                return status;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// To show save error message
        function fncShowSaveErrorInfor(status) {
            try {


                if (status.indexOf("Shelf") != -1) {
                    $('#<%=txtShelf.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtShelf.ClientID %>');
                    $('#<%=txtSection.ClientID %>').select();
                }
                else {
                    $('#<%=txtShelf.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Section") != -1) {
                    $('#<%=txtSection.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtSection.ClientID %>');
                    $('#<%=txtSection.ClientID %>').select();
                }
                else {
                    $('#<%=txtSection.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Subclass") != -1) {
                    $('#<%=txtSubClass.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtSubClass.ClientID %>');
                    $('#<%=txtSubClass.ClientID %>').select();
                }
                else {
                    $('#<%=txtSubClass.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("class") != -1) {
                    $('#<%=txtClass.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtClass.ClientID %>');
                    $('#<%=txtClass.ClientID %>').select();
                }
                else {
                    $('#<%=txtClass.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Companycode") != -1) {
                    $('#<%=txtCCode.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtCCode.ClientID %>');
                    $('#<%=txtCCode.ClientID %>').select();
                }
                else {
                    $('#<%=txtCCode.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Package") != -1) {
                    $('#<%=txtPackage.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtPackage.ClientID %>');
                    $('#<%=txtPackage.ClientID %>').select();
                }
                else {
                    $('#<%=txtPackage.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Bin") != -1) {
                    $('#<%=txtBin.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtBin.ClientID %>');
                    $('#<%=txtBin.ClientID %>').select();
                }
                else {
                    $('#<%=txtBin.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("SubCategory") != -1) {
                    $('#<%=txtSubCategory.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtSubCategory.ClientID %>');
                    $('#<%=txtSubCategory.ClientID %>').select();
                }
                else {
                    $('#<%=txtSubCategory.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Origin") != -1) {
                    $('#<%=txtOrigin.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtOrigin.ClientID %>');
                    $('#<%=txtOrigin.ClientID %>').select();
                }
                else {
                    $('#<%=txtOrigin.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Size") != -1) {
                    $('#<%=txtSize.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtSize.ClientID %>');
                    $('#<%=txtSize.ClientID %>').select();
                }
                else {
                    $('#<%=txtSize.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Style") != -1) {
                    $('#<%=txtStyle.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtStyle.ClientID %>');
                    $('#<%=txtStyle.ClientID %>').select();
                }
                else {
                    $('#<%=txtStyle.ClientID %>').css("border", "1px solid #383838");
                }

                if (status.indexOf("Merchandise") != -1) {
                    $('#<%=txtMerchandise.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtMerchandise.ClientID %>');
                    $('#<%=txtMerchandise.ClientID %>').select();
                }
                else {
                    $('#<%=txtMerchandise.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Manufacture") != -1) {
                    $('#<%=txtManafacture.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtManafacture.ClientID %>');
                    $('#<%=txtManafacture.ClientID %>').select();
                }
                else {
                    $('#<%=txtManafacture.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Warehouse") != -1) {
                    $('#<%=txtWareHouse.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtWareHouse.ClientID %>');
                    $('#<%=txtWareHouse.ClientID %>').select();
                }
                else {
                    $('#<%=txtWareHouse.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Vendor") != -1) {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtVendor.ClientID %>');
                    $('#<%=txtVendor.ClientID %>').select();
                }
                else {
                    $('#<%=txtVendor.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Floor") != -1) {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtFloor.ClientID %>');
                    $('#<%=txtFloor.ClientID %>').select();
                }
                else {
                    $('#<%=txtFloor.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("ItemType") != -1) {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtItemType.ClientID %>');
                    $('#<%=txtItemType.ClientID %>').select();
                }
                else {
                    $('#<%=txtItemType.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Brand") != -1) {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtBrand.ClientID %>');
                    $('#<%=txtBrand.ClientID %>').select();
                }
                else {
                    $('#<%=txtBrand.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Department") != -1) {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID %>');
                    $('#<%=txtDepartment.ClientID %>').select();
                }
                else {
                    $('#<%=txtDepartment.ClientID %>').css("border", "1px solid #383838");
                }
                if (status.indexOf("Category") != -1) {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid red");
                    //popUpObjectForSetFocusandOpen = $('#<%=txtInvcategory.ClientID %>');
                    $('#<%=txtInvcategory.ClientID %>').select();
                }
                else {
                    $('#<%=txtInvcategory.ClientID %>').css("border", "1px solid #383838");
                }

                fncToastInformation("Please enter valid data for red color field");

            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }



        /// Set Focus to controller based on condition /// work done by saravanan 01-12-2017
        function fncInventoryKeyPress(evt, value) {
            try {

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    /// MRP Key Press
                    if (value == "MRP") {
                        if (($('#<%=rbtMrpPopup.ClientID %>').is(':checked')) || ($('#<%=rbtPartialMD.ClientID %>').is(':checked'))) {
                            $('#<%=txtMRPMarkDown.ClientID %>').select();
                        }
                        else {
                            $('#<%=txtBasicCost.ClientID %>').select();
                        }
                    }
                    else if (value == "SellingPrice") {
                        if ($('#<%=pnlWholeSalePrice.ClientID %>').is(":visible")) {
                            $('#<%=txtMarginWPrice1.ClientID %>').select();
                        }
                        else {
                            $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                        }
                    }
                    else if (value == "FixedMargin") {
                        if ($('#<%=hdfMarkDown.ClientID %>').val() == 'Y' && ($('#<%=rbtMrpPopup.ClientID %>').is(':checked'))) {

                            if ($('#<%=pnlWholeSalePrice.ClientID %>').is(":visible")) {
                                $('#<%=txtMarginWPrice1.ClientID %>').select();
                            }
                            else {
                                $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                            }


                            // Assign WholeSale Price
                            txtWholePrice1Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                            txtWholePrice2Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                            txtWholePrice3Popup.val(parseFloat(txtSellingPrice.val()).toFixed(2))
                        }
                        else {
                            $('#<%=txtSellingPrice.ClientID %>').select();
                        }



                    }
                    else if (value == "MRPMarkDown") {
                        $('#<%=txtFixedMargin.ClientID %>').select();
                    }
                    else if (value == "WPrice3") {
                        $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                    }
                    else if (value == "WPrice3Per") {
                        if ($('#<%=rbtMrpPopup.ClientID %>').prop('checked')) {
                            $("#dialog-InvPrice").parent().find("button:eq(1)").focus();
                        }
                        else {
                            $('#<%=txtWholePrice1Popup.ClientID %>').select();
                        }
                    }

                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        /// Work done by saravanan 02-12-2017
        function fncShowDuplicateInventory() {
            $(function () {
                setTimeout(function () {
                    $("#<%=hidSaveStatus.ClientID %>").val('0');
            $('#<%=hidDailogName.ClientID %>').val("Deplicatename");
            $('#<%=lnkDupNameNo.ClientID %>').focus();
        }, 50);

        $("#dialog-DuplicateInventoryName").dialog({
            title: "Enterpriser Web",
            modal: true
        });
    });
        };

        /// Work done by saravanan 02-12-2017
        function fncShowBreakPriceQty() {

            $(function () {

                if ($('#<%=chkBatch.ClientID %>').prop('checked')) {
            fncGetBatchDetail_Master($('#<%=txtBreakPriceItem.ClientID%>').val());
        }
        else {
            $('#<%=txtBreakMRP.ClientID%>').val($('#<%=txtMRP.ClientID%>').val());
            $('#<%=hidbrkNetCost.ClientID%>').val($('#<%=txtNetCost.ClientID%>').val());
            setTimeout(function () {
                $('#<%=txtBreakQty.ClientID%>').select();
            }, 50);

        }

        $("#div-BreakPriceQty").dialog({
            title: "Break Price Qty",
            width: "auto",
            modal: true
        });
    });
        };

    </script>
    <%------------------------------------------- Clear Form -------------------------------------------%>
    <script type="text/javascript">

        var shortcutKeyvalue = "";

        function clearFormInv() {

            try {
                var sItemCode = $('#<%=txtItemCode.ClientID %>').val();
                clearForm();
                //$("#<%=btnBarcodeclear.ClientID %>").click();
                $('#<%=txtItemCode.ClientID %>').val(sItemCode);
                //WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$ContentPlaceHolder1$lnkItemHistory", "", false, "", "frmInventoryMasterTextiles.aspx", false, true));
                $('#<%=txtItemCode.ClientID %>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        /// Bind Old Break Price to Table
        function fncBindBreakPriceQty() {

            var objBreakPrice;
            breakPriceBody = $("#tblBreakPriceQty tbody");
            try {
                objBreakPrice = jQuery.parseJSON($("#<%=hidBreakPriceQty.ClientID %>").val());

                if (objBreakPrice.length > 0) {
                    for (var i = 0; i < objBreakPrice.length; i++) {
                        breakRowPrice = "<tr>"
                            + "<td><img alt='Delete' src='../images/No.png' onclick='fncShowDeleteDialog(this);return false;' /></td>"
                            + "<td id='tdbrkRowNo_" + breakRowNo + "' >" + breakRowNo + "</td>"
                            + "<td id='tdbrkItemcode_" + breakRowNo + "' >" + objBreakPrice[i]["InventoryCode"] + "</td>"
                            + " <td id='tdbrkBatchNo_" + breakRowNo + "' >" + objBreakPrice[i]["BatchNo"] + "</td>"
                            + " <td id='tdbrkQty_" + breakRowNo + "' >" + objBreakPrice[i]["BreakQty"] + "</td>"
                            + "<td id='tdbrkPrice_" + breakRowNo + "' >" + objBreakPrice[i]["BreakPrice"] + "</td>"
                            + " <td>" + objBreakPrice[i]["BreakPriceType"] + "</td>"
                            + " <td>" + objBreakPrice[i]["NetCostValidation"] + "</td>"
                            + "</tr>";
                        breakPriceBody.append(breakRowPrice);
                        breakRowNo = parseInt(breakRowNo) + 1;
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        /// Bind Old  Barcode 
        function fncBindBarcode() {

            var tblBarcode, objBarcode, row;
            tblBarcode = $("#tblBarcode tbody");
            try {
                objBarcode = jQuery.parseJSON($("#<%=hidbarcode.ClientID %>").val());
                //console.log(objBarcode);
                tblBarcode.children().remove();
                if (objBarcode.length > 0) {
                    for (var i = 0; i < objBarcode.length; i++) {
                        row = "<tr><td> " + "<img alt='Delete' src='../images/No.png' onclick='fncDeleteBarcode(this);return false;' /></td>" +
                            "<td id='tdBarcode' >" + objBarcode[i]["BarCode"] + "</td></tr>";
                        tblBarcode.append(row);
                    }
                }
                //console.log(tblBarcode);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Add barcode work done by saravanan 19-02-2018
        function fncAddBarcode() {
            var objBarcode, status = true;
            try {
                tblBarcode = $("#tblBarcode tbody");

                if ($("#tblBarcode tbody").children().length > 0) {
                    $("#tblBarcode tbody").children().each(function () {
                        if ($(this).find('td[id*=tdBarcode]').text() == $('#<%=txtBarcode.ClientID %>').val()) {
                            status = false;
                            return;
                        }
                    });
                }

                if (status == false) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_alreadyadded%>');
                    $('#<%=txtBarcode.ClientID %>').val('');
                    return;
                }
                else if ($('#<%=txtBarcode.ClientID %>').val().trim() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('Please enter barcode');
                    return;
                }
                else {
                    var obj = {};
                    obj.mode = "Barcode";
                    obj.barcode = $('#<%=txtBarcode.ClientID %>').val().trim();
                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncBarcodeValidation") %>',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != "") {
                                popUpObjectForSetFocusandOpen = $('#<%=txtBarcode.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject("Duplicate barcode...Please check the inventorycode: " + msg.d.split(',')[0] + "(" + msg.d.split(',')[1] + ")  ");
                                $('#<%=txtBarcode.ClientID %>').val("");
                            }
                            else {
                                row = "<tr><td> " + "<img alt='Delete' src='../images/No.png' onclick='fncDeleteBarcode(this);return false;' /></td>" +
                                    "<td id='tdBarcode' >" + $('#<%=txtBarcode.ClientID %>').val() + "</td></tr>";
                                tblBarcode.append(row);
                                $('#<%=txtBarcode.ClientID %>').val('');
                            }

                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });
                }


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// delete barcode /// 
        function fncDeleteBarcode(source) {
            try {
                var rowObj, barcode;
                var barcodeObj = {};
                rowObj = $(source).parent().parent();

                if ($('#<%=hidDeletebarcode.ClientID %>').val() != "") {

                //barcodeObj.barcode = rowObj.find('td[id*=tdBarcode]').text().trim();

                barcode = $('#<%=hidDeletebarcode.ClientID %>').val();
                barcode = barcode + "{\"Barcode\":" + "\"" + rowObj.find('td[id*=tdBarcode]').text().trim() + "\"},";
            }
            else {
                barcode = "{\"Barcode\":" + "\"" + rowObj.find('td[id*=tdBarcode]').text().trim() + "\"},";
            }
            rowObj.remove();
            $('#<%=hidDeletebarcode.ClientID %>').val(barcode);


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost) {
            try {
                $('#<%=txtBreakPriceBatchNo.ClientID%>').val(batchno);
            $('#<%=txtBreakMRP.ClientID%>').val(mrp);
            $('#<%=hidbrkNetCost.ClientID%>').val(Basiccost);
            $('#<%=txtBreakQty.ClientID%>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        // Clear Break Price Detail
        function fncClearBreakPriceQty() {
            try {
                $('#<%=txtBreakPriceBatchNo.ClientID%>').val('');
            $('#<%=txtBreakQty.ClientID%>').val('');
            $('#<%=txtBreakPrice.ClientID%>').val('');
            $('#<%= ddlBreakPriceType.ClientID %>')[0].selectedIndex = 0;
            $('#<%= ddlBreakPriceType.ClientID %>').trigger("liszt:updated");

            if ($('#<%=chkBatch.ClientID %>').prop("checked")) {
                $('#<%=txtBreakMRP.ClientID%>').val('');
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Add Break Price Detail
        function fncAddBreakPriceQty() {
            try {
                var netCostVal = 1;
                if ($('#<%=chbrkNetCostVal.ClientID%>').prop("checked")) {
                netCostVal = 1;
            }
            else {
                netCostVal = 0;
            }



            if (fncBreakPriceValidation() == true) {

                breakPriceBody = $("#tblBreakPriceQty tbody");
                breakRowPrice = "<tr>"
                    + "<td><img alt='Delete' src='../images/No.png' onclick='fncShowDeleteDialog(this);return false;' /></td>"
                    + "<td id='tdbrkRowNo_" + breakRowNo + "' >" + breakRowNo + "</td>"
                    + "<td id='tdbrkItemcode_" + breakRowNo + "' >" + $('#<%=txtBreakPriceItem.ClientID%>').val() + "</td>"
                    + " <td id='tdbrkBatchNo_" + breakRowNo + "' >" + $('#<%=txtBreakPriceBatchNo.ClientID%>').val() + "</td>"
                    + " <td id='tdbrkQty_" + breakRowNo + "' >" + $('#<%=txtBreakQty.ClientID%>').val() + "</td>"
                    + "<td id='tdbrkPrice_" + breakRowNo + "' >" + $('#<%=txtBreakPrice.ClientID%>').val() + "</td>"
                    + " <td>" + $('#<%=ddlBreakPriceType.ClientID%>').val() + "</td> "
                        + " <td>" + netCostVal + "</td>"
                        + "</tr>";
                    breakPriceBody.append(breakRowPrice);
                    breakRowNo = parseInt(breakRowNo) + 1;
                    fncClearBreakPriceQty();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Break Price Validation
        function fncBreakPriceValidation() {
            try {

                var status = true;

                if ($('#<%=txtBreakQty.ClientID%>').val().trim() == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtBreakQty.ClientID%>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_enterbreakQty%>');
                status = false;
            }
            else if ($('#<%=txtBreakPrice.ClientID%>').val().trim() == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_enterbreakprice%>');
                status = false;
            }
            else if ($('#<%=txtBreakMRP.ClientID%>').val().trim() == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtBreakMRP.ClientID%>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_anyonebatch%>');
                status = false;
            }
            else if (parseFloat($('#<%=txtBreakPrice.ClientID%>').val()) > parseFloat($('#<%=txtBreakMRP.ClientID%>').val())) {
                popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_BreakPricelessMRP%>');
                status = false;
            }
            else if ($('#<%=chbrkNetCostVal.ClientID%>').prop("checked")) {
                if (parseFloat($('#<%=txtBreakPrice.ClientID%>').val()) < parseFloat($('#<%=hidbrkNetCost.ClientID%>').val())) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtBreakPrice.ClientID%>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.msg_BreakPricegreateNetCost%>');
                    status = false;
                }
            }

            $("#tblBreakPriceQty tbody").children().each(function () {

                if ($(this).find('td[id*=tdbrkItemcode]').text().trim() == $('#<%=txtBreakPriceItem.ClientID %>').val().trim() &&
            $(this).find('td[id*=tdbrkBatchNo]').text().trim() == $('#<%=txtBreakPriceBatchNo.ClientID %>').val().trim() &&
            parseFloat($(this).find('td[id*=tdbrkQty]').text()) == parseFloat($('#<%=txtBreakQty.ClientID %>').val())) {
            popUpObjectForSetFocusandOpen = $('#<%=txtBreakQty.ClientID%>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_alreadyadded%>');
            status = false;
        }
    });


                return status;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// delete Break Price Item
        function fncDeleteBrkItem(source) {
            try {

                $(source).parent().parent().remove();

                if ($("#tblBreakPriceQty tbody").children().length > 0) {
                    breakRowNo = 1;
                    $("#tblBreakPriceQty tbody").children().each(function () {
                        $(this).find('td[id*=tdbrkRowNo]').text(breakRowNo);
                        breakRowNo = breakRowNo + 1;
                    });


                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        /// show Break Price Qty Delete Dialog 
        function fncShowDeleteDialog(source) {

            var obj = {}, rowObj;
            $(function () {
                $("#delete-BreakPriceQty").html('<%=Resources.LabelCaption.msg_delete_com%>');
        $("#delete-BreakPriceQty").dialog({
            title: "Enterpriser Web",
            buttons: {
                Yes: function () {
                    $(this).dialog("destroy");

                    rowObj = $(source).parent().parent();

                    obj.inventorycode = rowObj.find('td[id*=tdbrkItemcode]').text().trim();
                    obj.breakQty = rowObj.find('td[id*=tdbrkQty]').text().trim();
                    obj.breakPrice = rowObj.find('td[id*=tdbrkPrice]').text().trim();
                    obj.batchNo = rowObj.find('td[id*=tdbrkBatchNo]').text().trim();
                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncDeleteBreakPrice") %>',
                        data: JSON.stringify(obj),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d == "1") {
                                fncDeleteBrkItem(source);
                                fncShowBreakPriceDeleteSuccess();
                            }

                        },
                        error: function (data) {
                            ShowPopupMessageBox(data.message);
                        }
                    });


                },
                No: function () {
                    $(this).dialog("destroy");
                }
            },
            modal: true
        });
    });
        };

        /// convert Html table to JSon Format
        function fncConverthtmlTabletoJSON() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {

                $.each($("#tblBreakPriceQty thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblBreakPriceQty tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidBreakPriceQty.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// convert html table to json(barcode)
        function fncConverttblbarcodetoJson() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {


                $.each($("#tblBarcode thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblBarcode tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidbarcode.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        /// convert html table to json(barcode)
        function fncConverttblMultiplevendortoJson() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {


                $.each($("#tblNewVendorCreation thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });

                $.each($("#tblNewVendorCreation tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $("#<%=hidMultipleVendor.ClientID %>").val(JSON.stringify(tableObj));



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowBreakPriceDeleteSuccess() {
            $(function () {
                $("#delete-BreakPriceQtySuccess").html('<%=Resources.LabelCaption.msg_success_com%>');
        $("#delete-BreakPriceQtySuccess").dialog({
            title: "Enterpriser Web",
            buttons: {
                Ok: function () {
                    $(this).dialog("destroy");
                }
            },
            modal: true
        });
    });
        };

        ///Function key set
        function disableFunctionKeys(e) {

            try {

                if (e.keyCode == 27) {
                    window.parent.jQuery('#popupInvMaster').dialog('close');
                    window.parent.$('#popupInvMaster').remove();
                    return;

                }

                if ($("#<%=hidDailogName.ClientID %>").val() == "Deplicatename") {
            if (e.keyCode == 37) {
                $("#<%=lnkDupNameYes.ClientID %>").focus();
            }
            else if (e.keyCode == 39) {
                $("#<%=lnkDupNameNo.ClientID %>").focus();
            }
        }
        else if ($("#<%=hidDailogName.ClientID %>").val() == "NewItemSave") {
            if (e.keyCode == 37) {
                $("#<%=lnkNewItemYes.ClientID %>").focus();
            }
            else if (e.keyCode == 39) {
                $("#<%=lnkNewItemNo.ClientID %>").focus();
            }
        }


        //hidDailogName

        var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

            if (e.keyCode == 115) {
                Validate();
                e.preventDefault();
            }
            else if (e.keyCode == 117) {
                clearFormInv();
                e.preventDefault();
            }
            else if (e.keyCode == 119) {
                WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$ContentPlaceHolder1$lnkItemHistory", "", false, "", "frmInventoryMasterTextilesView.aspx", false, true));
                e.preventDefault();
            }
            else if (e.keyCode == 118) {
                if (shortcutKeyvalue == "Category") {
                    page = '<%=ResolveUrl("~/Inventory/frmCategoryMaster.aspx") %>';
                $dialog = $('<div id="popupcategory" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Category",
                    closeOnEscape: true
                });
                $dialog.dialog('open');

            }
            else if (shortcutKeyvalue == "Department") {
                page = '<%=ResolveUrl("~/Inventory/frmDepartmentMaster.aspx") %>';
                $dialog = $('<div id="popupDepartment"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Department"

                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Brand") {
                page = '<%=ResolveUrl("~/Inventory/frmBrandMaster.aspx") %>';
                $dialog = $('<div id="popupBrand"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Brand"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "WareHouse") {
                page = '<%=ResolveUrl("~/Masters/frmWarehouse.aspx") %>';
                $dialog = $('<div id="popupWareHouse"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "WareHouse"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Manufacture") {
                page = '<%=ResolveUrl("~/Masters/frmManufacturer.aspx") %>';
                $dialog = $('<div id="popupManufacture"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Manufacture"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Merchandise") {
                page = '<%=ResolveUrl("~/Masters/frmMerchandise.aspx") %>';
                $dialog = $('<div id="popupMerchandise"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Merchandise"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "ItemType") {
                page = '<%=ResolveUrl("~/Masters/frmItemType.aspx") %>';
                $dialog = $('<div id="popupItemType"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "ItemType"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Style") {
                page = '<%=ResolveUrl("~/Masters/frmstyle.aspx") %>';
                $dialog = $('<div id="popupStyle"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Style"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Size") {
                page = '<%=ResolveUrl("~/Masters/frmSize.aspx") %>';
                $dialog = $('<div id="popupSize"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Size"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Origin") {
                page = '<%=ResolveUrl("~/Masters/frmOrigin.aspx") %>';
                $dialog = $('<div id="popupOrigin"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Origin"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "SubCategory") {
                page = '<%=ResolveUrl("~/Masters/frmsubCategory.aspx") %>';
                $dialog = $('<div id="popupSubCategory"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "SubCategory"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Floor") {
                page = '<%=ResolveUrl("~/Masters/frmFloor.aspx") %>';
                $dialog = $('<div id="popupFloor"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Floor"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Bin") {
                page = '<%=ResolveUrl("~/Masters/frmBin.aspx") %>';
                $dialog = $('<div id="popupBin"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Bin"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "CCode") {
                page = '<%=ResolveUrl("~/Inventory/frmCompanySettings.aspx") %>';
                $dialog = $('<div id="popupCCode"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Company Settings"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Vendor") {
                page = '<%=ResolveUrl("~/Masters/frmVendorMaster.aspx") %>';
                $dialog = $('<div id="popupVendor"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Vendor"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Class") {
                page = '<%=ResolveUrl("~/Masters/frmClass.aspx") %>';
                $dialog = $('<div id="popupClass"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Class"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "SubClass") {
                page = '<%=ResolveUrl("~/Masters/frmSubClass.aspx") %>';
                $dialog = $('<div id="popupSubClass"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "SubClass"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Section") {
                page = '<%=ResolveUrl("~/Masters/frmSection.aspx") %>';
                $dialog = $('<div id="popupSection"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Section"
                });
                $dialog.dialog('open');
            }
            else if (shortcutKeyvalue == "Shelf") {
                page = '<%=ResolveUrl("~//Masters/frmshelf.aspx") %>';
                $dialog = $('<div id="popupShelf"></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "Shelf"
                });
                $dialog.dialog('open');
            }


            e.preventDefault();
        }
        else if (e.keyCode == 114) {

            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                if (shortcutKeyvalue == "Vendor") {

                    /// save Multiple Vendor
                    var obj = {};
                    obj.mode = "GetVendorList";
                    obj.inventorycode = $('#<%=txtItemCode.ClientID %>').val().trim();
            obj.vendorcode = "";
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncSaveAndDeleteMultipleVendor") %>',
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    fncBindMultpleVendor(msg.d);
                    fncShowMultipleVendorCreation();
                },
                error: function (data) {
                    ShowPopupMessageBox(data.message);
                }
            });

                            }
                        }

                        shortcutKeyvalue = "";

                        e.preventDefault();
                        return false;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }


        }

        //// Bind Multiple Vendors
        function fncBindMultpleVendor(vendorlist) {
            var objMultipleVendor, row, tblNewVendorCreation;
            try {
                tblNewVendorCreation = $("#tblNewVendorCreation tbody");
                objMultipleVendor = jQuery.parseJSON(vendorlist);
                tblNewVendorCreation.children().remove();
                if (objMultipleVendor.length > 0) {
                    for (var i = 0; i < objMultipleVendor.length; i++) {
                        row = "<tr>"
                            + "<td id='tdRowNo' >" + objMultipleVendor[i]["RowNo"] + "</td>"
                            + "<td id='tdVendorcode' >" + objMultipleVendor[i]["VendorCode"] + "</td>"
                            + "<td id='tdVendorname' >" + objMultipleVendor[i]["VendorName"] + "</td>"
                            + "<td> " + "<img alt='Delete' src='../images/No.png' onclick='fncShowMultipleVendorDeleteDailog(this);return false;' /></td>"
                            + "</tr>";
                        tblNewVendorCreation.append(row);
                        tblNewVendorCreation.children().click(fncMultipleVendorRowClick);
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            fncdecimal();
            fncInventoryScreenChangeForDailog();
        });

        function fncInventoryScreenChangeForDailog() {
            try {
                if ($('#<%=hidInvOpenStatus.ClientID %>').val() == "dailog") {
                    fncHideMasterMenu();
                    $(".tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2)").css("min-width", "229px");
                    $(".tbl_barcode td:nth-child(2), .tbl_barcode th:nth-child(2)").css("max-width", "229px");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncdecimal() {
            $('#ContentPlaceHolder1_txtCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtMRP').number(true, 2);
            $('#txtNumericalDigits').number(true, 2);
            $('#ContentPlaceHolder1_txtMRPMarkDown').number(true, 2);
            $('#ContentPlaceHolder1_txtBasicCost').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc1').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc2').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountPrc3').number(true, 2);
            $('#ContentPlaceHolder1_txtDiscountAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtGrossCost').number(true, 2);
            $('#ContentPlaceHolder1_txtInputSGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtInputSGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtInputCESSAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtNetCost').number(true, 2);

            $('#ContentPlaceHolder1_txtFixedMargin').number(true, 2);
            $('#ContentPlaceHolder1_txtBasicSelling').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputSGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputSGSTAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtOutputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtOutputCGSTPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCGSTAmt').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSPrc').number(true, 2);
            $('#ContentPlaceHolder1_txtOutputCESSAmt').number(true, 2);

            $('#ContentPlaceHolder1_txtRoundOff').number(true, 2);
            $('#ContentPlaceHolder1_txtSellingPrice').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiabilityPer').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiability').number(true, 2);

            $('#ContentPlaceHolder1_txtGSTLiabilityPer').number(true, 2);
            $('#ContentPlaceHolder1_txtGSTLiability').number(true, 2);

            $('#<%=txtBreakQty.ClientID%>').number(true, 0);
    $('#<%=txtBreakPrice.ClientID%>').number(true, 2);
    $('#<%=txtBreakMRP.ClientID%>').number(true, 2);
    $('#<%=txtAddCessAmt.ClientID%>').number(true, 2);

            $('#ContentPlaceHolder1_txtWeight').number(true, 3);
            $('#ContentPlaceHolder1_txtWidth').number(true, 3);
            $('#ContentPlaceHolder1_txtHeight').number(true, 3);
            $('#ContentPlaceHolder1_txtLength').number(true, 3);

            function fncShowVendorSearchDialogMultiVendor(event, value) {
                try {
                    setTimeout(function () {
                        shortcutKeyvalue = "MultiVendor";
                    }, 50);
                    fncShowSearchDialogCommon(event, value);
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }
        }
        ///Show Item Search Table
        function fncShowVendorSearch() {
            try {
                $("#dialog-Vendor").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Vendor Search",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //// Short cut key function Option
        function fncToEnableShortcutKey(value) {
            try {

                if (value == "Category") {
                    shortcutKeyvalue = "Category";
                    $('#lblMasterOpen').text("Press F7 to Create New Category");
            //$('#<%=hidShortcutKey.ClientID%>').val("Category");
        }
        else if (value == "Department") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Department");
            shortcutKeyvalue = "Department";
            $('#lblMasterOpen').text("Press F7 to Create New Department");
        }
        else if (value == "Brand") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Brand");
            shortcutKeyvalue = "Brand";
            $('#lblMasterOpen').text("Press F7 to Create New Brand");
        }
        else if (value == "WareHouse") {
            //$('#<%=hidShortcutKey.ClientID%>').val("WareHouse");
            shortcutKeyvalue = "WareHouse";
            $('#lblMasterOpen').text("Press F7 to Create New WareHouse");
        }
        else if (value == "Manufacture") {
            $('#<%=hidShortcutKey.ClientID%>').val("Manufacture");
            $('#lblMasterOpen').text("Press F7 to Create New Manufacture");
            //shortcutKeyvalue = "Manufacture";
        }
        else if (value == "Merchandise") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Merchandise");
            shortcutKeyvalue = "Merchandise";
            $('#lblMasterOpen').text("Press F7 to Create New Merchandise");
        }
        else if (value == "ItemType") {
            //$('#<%=hidShortcutKey.ClientID%>').val("ItemType");
            shortcutKeyvalue = "ItemType";
            $('#lblMasterOpen').text("Press F7 to Create New Item Type");
        }
        else if (value == "Style") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Style");
            shortcutKeyvalue = "Style";
            $('#lblMasterOpen').text("Press F7 to Create New Style");
        }
        else if (value == "Size") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Size");
            shortcutKeyvalue = "Size";
            $('#lblMasterOpen').text("Press F7 to Create New Size");
        }
        else if (value == "Origin") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Origin");
            shortcutKeyvalue = "Origin";
        }
        else if (value == "SubCategory") {
            //$('#<%=hidShortcutKey.ClientID%>').val("SubCategory");
            shortcutKeyvalue = "SubCategory";
            $('#lblMasterOpen').text("Press F7 to Create New SubCategory");
        }
        else if (value == "Floor") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Floor");
            shortcutKeyvalue = "Floor";
            $('#lblMasterOpen').text("Press F7 to Create New Floor");
        }
        else if (value == "Bin") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Bin");
            shortcutKeyvalue = "Bin";
            $('#lblMasterOpen').text("Press F7 to Create New Bin");
        }
        else if (value == "CCode") {
            //$('#<%=hidShortcutKey.ClientID%>').val("CCode");
            shortcutKeyvalue = "CCode";
            $('#lblMasterOpen').text("Press F7 to Create New CCode");
        }
        else if (value == "Vendor") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Vendor");
            shortcutKeyvalue = "Vendor";
            $('#lblMasterOpen').text("Press F7 to Create New Vendor");
        }
        else if (value == "Class") {
            //$('#<%=hidShortcutKey.ClientID%>').val("Class");
            shortcutKeyvalue = "Class";
            $('#lblMasterOpen').text("Press F7 to Create New Class");
        }
        else if (value == "SubClass") {
            shortcutKeyvalue = "SubClass";
            $('#lblMasterOpen').text("Press F7 to Create New SubClass");
        }
        else if (value == "Section") {
            shortcutKeyvalue = "Section";
            $('#lblMasterOpen').text("Press F7 to Create New Section");
        }
        else if (value == "Shelf") {
            shortcutKeyvalue = "Shelf";
            $('#lblMasterOpen').text("Press F7 to Create New Shelf");
        }
        else if (value == "Itemname") {
            shortcutKeyvalue = "";
            $('#<%=txtItemName.ClientID%>').select();
                    $('#lblMasterOpen').text("");
                }

                else {
                    shortcutKeyvalue = "";
                    $('#lblMasterOpen').text("");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }


        function fncTextboxKeyDown(event, value) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    if (value == "brand") {
                        $('#<%=txtItemName.ClientID%>').val($('#<%=txtBrand.ClientID%>').val() + ' ' + $('#<%=txtInvcategory.ClientID%>').val());
                $('#<%=txtItemName.ClientID%>').select();
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowNewItemSavedDailog() {
            $(function () {
                $("#<%=hidSaveStatus.ClientID %>").val('0');
        $('#<%=hidDailogName.ClientID%>').val("NewItemSave");
        //$("#NewItemSave").html("Inventory Successfully Updated. \n Do You want to continue with existing values ?");
        $("#NewItemSave").dialog({
            title: "Enterpriser Web",
            modal: true
        });
    });
        };
        function fncShowItemEditDailog() {
            $(function () {
                $("#<%=hidSaveStatus.ClientID %>").val('0');
        $("#ItemEdit").html("Inventory Saved Successfully");
        $("#ItemEdit").dialog({
            title: "Enterpriser Web",
            buttons: {
                "Ok": function () {
                    $(this).dialog("destroy");
                    window.location.href = "frmInventoryMasterView.aspx";
                }
            },
            modal: true
        });
    });
        };

        function fncShowMultipleVendorDeleteDailog(source) {
            $(function () {
                $("#DeleteMultiplevendor").html("Do you want delete slected vendor?");
                $("#DeleteMultiplevendor").dialog({
                    title: "Enterpriser Web",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("destroy");
                            fncDeleteMultipleVendor(source);
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                        },
                    },
                    modal: true
                });
            });
        };

        $(document).ready(function () {
            $(" #vendorNametooltip ").mouseenter(function () {
                var word = $('#<%=hidVendorName.ClientID%>').val();
        $(" #vendorNametooltip ").attr('title', word);
    });
});

        /// Inventory code Key Function
        function fncInventorykeydown(keyCode) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    if ($('#<%=txtItemCode.ClientID%>').val().trim() == "") {
                    return false;
                }
                else if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
                    $('#<%=txtItemCode.ClientID%>').attr("readonly", "readonly");
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkEditItemCode', '');
                    }
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == "Edit") {
        $("#invhdr").text("Inventory History");
    }
    else {
        $("#userdetail").css("display", "none");
    }

});

        /// Go GRN Page
        function fncGoGRNPage(source) {
            var rowObj, grnNo = "", gaNo = "", billNo = "";
            var vendorcode = "", billType = "", locationCode = "";
            try {
                rowObj = $(source).closest("tr");
                grnNo = $(source).text().trim();
                billNo = $("td", rowObj).eq(1).html().replace(/&nbsp;/g, '');
                vendorcode = $("td", rowObj).eq(12).html().replace(/&nbsp;/g, '');
                billType = $("td", rowObj).eq(13).html().replace(/&nbsp;/g, '');
                locationCode = $("td", rowObj).eq(14).html().replace(/&nbsp;/g, '');

                var page = '<%=ResolveUrl("~/Purchase/GoodsAcknowledgmentNote.aspx") %>';
                var page = page + "?GRNNo=" + grnNo + "&GANo=" + gaNo;
                var page = page + "&BillNo=" + billNo + "&VendorCode=" + vendorcode;
                var page = page + "&Status=View&BillType=" + billType;
                var page = page + "&LoadPoQty=N" + "&locationcode=" + locationCode;
                var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 645,
                    width: 1300,
                    title: "VIEW GRN MAINTENANCE"
                });

                //alert(page);
                $dialog.dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///New Vendor Creation
        function fncShowMultipleVendorCreation() {
            try {
                $("#dialog-MultipleVendorCreation").dialog({
                    resizable: false,
                    height: "auto",
                    width: 500,
                    modal: true,
                    title: "Multiple Vendor",
                    appendTo: 'form:first'
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Mandatory Field validation
        function fncMandatoryval(event, value) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            try {
                if (keyCode == 13) {
                    $('#<%=txtBrand.ClientID%>').select();
            return false;
        }
        else if (keyCode == 118) {
            if (value == "Department") {
                page = '<%=ResolveUrl("~/Inventory/frmDepartmentMaster.aspx") %>';
                        $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                            autoOpen: false,
                            modal: true,
                            height: 645,
                            width: 1300,
                            title: "Department"
                        });

                    }
                    return false;
                }


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Check Mandatory feilds
        function fncToCheckMandatoryFeilds(value) {
            try {
                if (value == "Department") {
                    if ($('#<%=txtDepartment.ClientID%>').val() == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtDepartment.ClientID%>');
                        ShowPopupMessageBoxandFocustoObject("Please enter departmentcode");
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Bind Attributes
        function funBindAttributes(jsonObj) {

            try {

                if (jsonObj == null)
                    return;

                ////Creating table rows 
                var table1 = document.getElementById("grdAttributeValue");
                $('#grdAttributeValue').find("tr:gt(0)").remove();
                $(jsonObj).each(function (index, element) {
                    //alert('id: ' + element.ValueCode + ', name: ' + element.ValueName);
                    var newRow = table1.insertRow(table1.length),
                        cell1 = newRow.insertCell(0),
                        cell2 = newRow.insertCell(1),
                        cell3 = newRow.insertCell(2);

                    cell1.className = "hiddencol";
                    cell3.style = "text-align:center";
                    // add values to the cells  
                    cell1.innerHTML = element.ValueCode;
                    cell2.innerHTML = element.ValueName;
                    cell3.innerHTML = "<input type='checkbox' name='check-tab1'>";
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Bind Sales history
        function funBindSaleshistory(jsonObj) {
            var tableid = "Saleshistory";
            try {

                if (jsonObj == null)
                    return;

                var tblSalesHistory = document.getElementById(tableid);
                var SalestableContainer = document.getElementById('Saleshistory_id')

                if (tblSalesHistory) {
                    SalestableContainer.removeChild(tblSalesHistory);
                }

                tblSalesHistory = CreateTableFromJSON_Common(jsonObj, tableid);
                SalestableContainer.appendChild(tblSalesHistory);
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Bind Child Sales history
        function funBindChildSaleshistory(jsonObj) {
            var tableid = "childSaleshistory";
            try {

                if (jsonObj == null)
                    return;

                var tblChildSalesHistory = document.getElementById(tableid);
                var childSalestableContainer = document.getElementById('childSaleshistory_id')

                if (tblChildSalesHistory) {
                    childSalestableContainer.removeChild(tblChildSalesHistory);
                }

                tblChildSalesHistory = CreateTableFromJSON_Common(jsonObj, tableid);
                childSalestableContainer.appendChild(tblChildSalesHistory);
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        /// Get Item Sales history
        function fncGetSalesItemSaleshistory(invmode, mode, inventorycode) {
            var obj = {};
            try {
                obj.mode = mode;
                obj.inventorycode = inventorycode;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMasterTextiles.aspx/fncGetItemSaleshistory") %>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (invmode == "normal") {
                    funBindSaleshistory(jQuery.parseJSON(msg.d));
                }
                else {
                    funBindChildSaleshistory(jQuery.parseJSON(msg.d));
                }
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncChildSaleshistorycheckchanged() {
            try {

        //$('#<%=hidchildItem.ClientID%>').val($('#<%=txtItemCode.ClientID%>').val());

      <%--  if ($("#<%=rbnMonth.ClientID%>").is(':checked')) {
            fncGetSalesItemSaleshistory("child", "Month", $('#<%=hidchildItem.ClientID%>').val());
        }
        else if ($("#<%=rbnWeek.ClientID%>").is(':checked')) {
            fncGetSalesItemSaleshistory("child", "Week", $('#<%=hidchildItem.ClientID%>').val());
        }
        else {
            fncGetSalesItemSaleshistory("child", "Day", $('#<%=hidchildItem.ClientID%>').val());
        }--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Item sales History check changed
        function fncSaleshistorycheckchanged() {
            try {

                if ($("#<%=rbnSalesMonth.ClientID%>").is(':checked')) {
            fncGetSalesItemSaleshistory("normal", "Month", $('#<%=txtItemCode.ClientID%>').val());
        }
        else if ($("#<%=rbnSalesWeek.ClientID%>").is(':checked')) {
            fncGetSalesItemSaleshistory("normal", "Week", $('#<%=txtItemCode.ClientID%>').val());
        }
        else {
            fncGetSalesItemSaleshistory("normal", "Day", $('#<%=txtItemCode.ClientID%>').val());
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            fncCombinationkeypress();
            fncDisableAndEnableTab();
            fncTamilLanguage();


        });

        function fncTamilLanguage() {
            try {
                $("#<%=txtOtherLanguageName.ClientID%>").on('keydown', function (event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                $("#<%=txtOtherLanguageSName.ClientID%>").val($("#<%=txtOtherLanguageName.ClientID%>").val());
                $("#<%=txtOtherLanguageSName.ClientID%>").select();
                return false;
            }
            else if (event.which == 121) {
                $(this).toggleClass('tamil');
                return false;
            }
            if ($(this).hasClass('tamil')) {
                toggleKBMode(event);
            } else {
                return true;
            }
        });
        $("#<%=txtOtherLanguageName.ClientID%>").on('keypress', function (event) {
            if ($(this).hasClass('tamil')) {
                convertThis(event);
            }
        });

        $("#<%=txtOtherLanguageSName.ClientID%>").on('keydown', function (event) {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                $("#<%=txtRemarks.ClientID%>").select();
                return false;
            }

            if (event.which == 121) {
                $(this).toggleClass('tamil');
                return false;
            }
            if ($(this).hasClass('tamil')) {
                toggleKBMode(event);
            } else {
                return true;
            }
        });
        $("#<%=txtOtherLanguageSName.ClientID%>").on('keypress', function (event) {
                    if ($(this).hasClass('tamil')) {
                        convertThis(event);
                    }
                });

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        /// Disable and Enable Tab
        function fncDisableAndEnableTab() {
            try {
                if ($('#<%=hidinventoryStatus.ClientID%>').val() == "New") {

                    $('#liStockPurchase').addClass('disabled');
                    $('#liStockPurchase').not('.active').find('a').removeAttr("data-toggle");

                    $('#liSales').addClass('disabled');
                    $('#liSales').not('.active').find('a').removeAttr("data-toggle");

                    $('#liChildInventory').addClass('disabled');
                    $('#liChildInventory').not('.active').find('a').removeAttr("data-toggle");

                    $('#liPromotion').addClass('disabled');
                    $('#liPromotion').not('.active').find('a').removeAttr("data-toggle");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Combination key press
        function fncCombinationkeypress() {
            try {

                $.alt('1', function () {
                    $('#<%=rbtCost.ClientID %>').prop('checked', true);  // unchecked     
            $('#<%=rbtMRP.ClientID %>').prop('checked', false);
            $('#<%=rbnMRP2.ClientID %>').prop('checked', false);  // checked
        });
        $.alt('2', function () {
            $('#<%=rbtCost.ClientID %>').prop('checked', false);  // unchecked     
            $('#<%=rbtMRP.ClientID %>').prop('checked', true);
            $('#<%=rbnMRP2.ClientID %>').prop('checked', false);  // checked
        });
        $.alt('3', function () {
            $('#<%=rbtCost.ClientID %>').prop('checked', false);  // unchecked     
            $('#<%=rbtMRP.ClientID %>').prop('checked', false);
            $('#<%=rbnMRP2.ClientID %>').prop('checked', true);  // checked
        });

        $.alt('I', function () {


            $('#liInventoryPricing').addClass('active');
            $('#InventoryPricing').addClass('active');


            $('#liBreakPrice').removeClass('active');
            $('#BreakPrice').removeClass('active');

            $('#liActivationSettings').removeClass('active');
            $('#ActivationSettings').removeClass('active');

            $('#liStockPurchase').removeClass('active');
            $('#StockPurchase').removeClass('active');

            $('#liSales').removeClass('active');
            $('#Sales').removeClass('active');

            $('#liImage').removeClass('active');
            $('#Image').removeClass('active');

            $('#liPromotion').removeClass('active');
            $('#Promotion').removeClass('active');


            $('#liAttributes').removeClass('active');
            $('#Attributes').removeClass('active');

            $('#<%=imgPriceChange.ClientID %>').focus();
        });
        $.alt('O', function () {

           <%-- setTimeout(function () {
                $('#<%=txtMinimumQty.ClientID %>').select();
            }, 50);--%>

            $('#liBreakPrice').addClass('active');
            $('#BreakPrice').addClass('active');

            $('#liInventoryPricing').removeClass('active');
            $('#InventoryPricing').removeClass('active');

            $('#liActivationSettings').removeClass('active');
            $('#ActivationSettings').removeClass('active');

            $('#liStockPurchase').removeClass('active');
            $('#StockPurchase').removeClass('active');

            $('#liSales').removeClass('active');
            $('#Sales').removeClass('active');

            $('#liImage').removeClass('active');
            $('#Image').removeClass('active');

            $('#liPromotion').removeClass('active');
            $('#Promotion').removeClass('active');


            $('#liAttributes').removeClass('active');
            $('#Attributes').removeClass('active');

            $('#<%=txtMinimumQty.ClientID %>').select();

        });
        $.alt('A', function () {



            $('#liActivationSettings').addClass('active');
            $('#ActivationSettings').addClass('active');

            $('#liInventoryPricing').removeClass('active');
            $('#InventoryPricing').removeClass('active');

            $('#liBreakPrice').removeClass('active');
            $('#BreakPrice').removeClass('active');

            $('#liStockPurchase').removeClass('active');
            $('#StockPurchase').removeClass('active');

            $('#liSales').removeClass('active');
            $('#Sales').removeClass('active');

            $('#liImage').removeClass('active');
            $('#Image').removeClass('active');

            $('#liPromotion').removeClass('active');
            $('#Promotion').removeClass('active');


            $('#liAttributes').removeClass('active');
            $('#Attributes').removeClass('active');

            $('#<%=chkExpiryDate.ClientID %>').focus();
        });
        $.alt('T', function () {

            if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {

                //$('#<%=chkExpiryDate.ClientID %>').focus();

                $('#liStockPurchase').addClass('active');
                $('#StockPurchase').addClass('active');

                $('#liInventoryPricing').removeClass('active');
                $('#InventoryPricing').removeClass('active');

                $('#liBreakPrice').removeClass('active');
                $('#BreakPrice').removeClass('active');

                $('#liActivationSettings').removeClass('active');
                $('#ActivationSettings').removeClass('active');

                $('#liSales').removeClass('active');
                $('#Sales').removeClass('active');


                $('#liImage').removeClass('active');
                $('#Image').removeClass('active');

                $('#liPromotion').removeClass('active');
                $('#Promotion').removeClass('active');


                $('#liAttributes').removeClass('active');
                $('#Attributes').removeClass('active');
            }
        });
        $.alt('S', function () {
            if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {



                $('#liSales').addClass('active');
                $('#Sales').addClass('active');

                $('#liInventoryPricing').removeClass('active');
                $('#InventoryPricing').removeClass('active');

                $('#liBreakPrice').removeClass('active');
                $('#BreakPrice').removeClass('active');

                $('#liActivationSettings').removeClass('active');
                $('#ActivationSettings').removeClass('active');

                $('#liStockPurchase').removeClass('active');
                $('#StockPurchase').removeClass('active');

                $('#liImage').removeClass('active');
                $('#Image').removeClass('active');

                $('#liPromotion').removeClass('active');
                $('#Promotion').removeClass('active');

                $('#liAttributes').removeClass('active');
                $('#Attributes').removeClass('active');


                $('#<%=rbnSalesMonth.ClientID %>').focus();
            }
        });
        $.alt('C', function () {
            if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {

                //$('#<%=rbnSalesMonth.ClientID %>').focus();


                $('#liInventoryPricing').removeClass('active');
                $('#InventoryPricing').removeClass('active');

                $('#liBreakPrice').removeClass('active');
                $('#BreakPrice').removeClass('active');

                $('#liActivationSettings').removeClass('active');
                $('#ActivationSettings').removeClass('active');

                $('#liStockPurchase').removeClass('active');
                $('#StockPurchase').removeClass('active');

                $('#liSales').removeClass('active');
                $('#Sales').removeClass('active');

                $('#liImage').removeClass('active');
                $('#Image').removeClass('active');

                $('#liPromotion').removeClass('active');
                $('#Promotion').removeClass('active');

                $('#liAttributes').removeClass('active');
                $('#Attributes').removeClass('active');
            }
        });
        $.alt('M', function () {


            $('#liImage').addClass('active');
            $('#Image').addClass('active');

            $('#liInventoryPricing').removeClass('active');
            $('#InventoryPricing').removeClass('active');

            $('#liBreakPrice').removeClass('active');
            $('#BreakPrice').removeClass('active');

            $('#liActivationSettings').removeClass('active');
            $('#ActivationSettings').removeClass('active');

            $('#liStockPurchase').removeClass('active');
            $('#StockPurchase').removeClass('active');

            $('#liSales').removeClass('active');
            $('#Sales').removeClass('active');

            $('#liPromotion').removeClass('active');
            $('#Promotion').removeClass('active');

            $('#liAttributes').removeClass('active');
            $('#Attributes').removeClass('active');

            $('#<%=fuimage.ClientID %>').focus();
        });
        $.alt('P', function () {
            if ($("#<%=hidinventoryStatus.ClientID %>").val() == "Edit") {
                $('#liPromotion').addClass('active');
                $('#Promotion').addClass('active');

                $('#liInventoryPricing').removeClass('active');
                $('#InventoryPricing').removeClass('active');

                $('#liBreakPrice').removeClass('active');
                $('#BreakPrice').removeClass('active');

                $('#liActivationSettings').removeClass('active');
                $('#ActivationSettings').removeClass('active');

                $('#liStockPurchase').removeClass('active');
                $('#StockPurchase').removeClass('active');

                $('#liSales').removeClass('active');
                $('#Sales').removeClass('active');

                $('#liImage').removeClass('active');
                $('#Image').removeClass('active');
            }
        });

        shortcut.add("Alt+L", function () {

            if ($("#<%=hidLanguageChange.ClientID%>").val() == "N") {
                $("#<%=hidLanguageChange.ClientID%>").val("Y");
                $("#<%=txtOtherLanguageName.ClientID%>").css("display", "block");
                $("#<%=txtOtherLanguageSName.ClientID%>").css("display", "block");
                $("#<%=txtItemName.ClientID%>").css("display", "none");
                $("#<%=txtShortName.ClientID%>").css("display", "none");
                $("#<%=txtOtherLanguageName.ClientID%>").select();
            }
            else {
                $("#<%=hidLanguageChange.ClientID%>").val("N");
                $("#<%=txtOtherLanguageName.ClientID%>").css("display", "none");
                $("#<%=txtOtherLanguageSName.ClientID%>").css("display", "none");
                $("#<%=txtItemName.ClientID%>").css("display", "block");
                $("#<%=txtShortName.ClientID%>").css("display", "block");
                $("#<%=txtItemName.ClientID%>").select();
            }





        });



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get value for GA and GRN Report
        function fncChildRowClick(source) {
            try {
                $("#divchildSales").css("display", "block");
                $("#<%=hidchildItem.ClientID%>").val($.trim($(source).find('td span[id*="lblChildInventory"]').text()));

                $(source).css("background-color", "#80b3ff");
                $(source).siblings().each(function () {
                    $(this).css("background-color", "white");
                });

                fncChildSaleshistorycheckchanged();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        /// Break price Key down function
        function isNumberKeyWithDecimalNewforBreakprice(evt) {
            try {

                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 123 && $('#<%=chkBatch.ClientID%>').prop("checked")) {
            fncGetBatchDetail_Master($('#<%=txtBreakPriceItem.ClientID%>').val());
                    return false;
                }
                else if ((charCode > 95 && charCode < 106) || charCode == 37 || charCode == 39 || charCode == 190) {
                    return true;
                    evt.preventDefault();
                }
                else if ((charCode != 46 && charCode > 31)
                    && (charCode < 48 || charCode > 57) && charCode != 110) {
                    return false;
                    evt.preventDefault();
                }

                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDupNameYesClick() {
            try {
                $("#dialog-DuplicateInventoryName").dialog("destroy");
                $("#<%=btnInventorySave.ClientID %>").click();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDupNameNoClick() {
            try {
                $("#dialog-DuplicateInventoryName").dialog("destroy");
                $('#<%=txtItemName.ClientID %>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncNewItemYesClick() {
            try {
                fncInitialLoad();
                $("#NewItemSave").dialog("destroy");
                $('#<%=txtItemName.ClientID %>').select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncNewItemNoClick() {
            try {
                $("#NewItemSave").dialog("destroy");
                __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                //window.location.href = "frmInventoryMasterTextilesView.aspx";
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncInventoryStatus(inventoryStatus) {
            try {
                if (inventoryStatus == "InvalidInventory") {
                    $('#<%=txtItemCode.ClientID %>').removeAttr("readonly");
            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
            //ShowPopupMessageBoxandFocustoObject("Invalid inventorycode");
            __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
            setTimeout(function () {
                $('#<%=txtItemCode.ClientID %>').select();
            }, 50);
        }
        else {
            fncInitialLoad();
            $('#<%=txtItemName.ClientID %>').select();
                }



            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// block Special Charactor
        function fncBlockSpecialChar(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 219 || charCode == 221)
                    return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        // barcode key down event
        function fncBarcodeKeyDown(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    fncAddBarcode();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncImageButtonkeydown(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    fncOpenInvPriceChange();
                    fncDisableEXGST();
            //fncRemoveDisableAttForInputGST();
           <%-- setTimeout(function () {
            if ($("#dialog-InvPrice").dialog('isOpen') === false) {
                $('#<%=imgPriceChange.ClientID %>').focus();
            }
            }, 50);--%>

                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncCheckBoxKeyDown(evt, value) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (value == "chkExpiryDate") {
                    if (charCode == 40) {
                        $('#<%=chkSerialNo.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkSerialNo") {
            if (charCode == 40) {
                $('#<%=chkAutoPO.ClientID %>').focus();
            }
            else if (charCode == 38) {
                $('#<%=chkExpiryDate.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkAutoPO") {
            if (charCode == 40) {
                $('#<%=chkPackage.ClientID %>').focus();
        }
        else if (charCode == 38) {
            $('#<%=chkSerialNo.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkPackage") {
            if (charCode == 40) {
                $('#<%=chkLoyality.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkAutoPO.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkLoyality") {
            if (charCode == 40) {
                $('#<%=chkSpecial.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPackage.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkSpecial") {
            if (charCode == 40) {
                $('#<%=chkMemDis.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkLoyality.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkMemDis") {
            if (charCode == 40) {
                $('#<%=chkSeasonal.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSpecial.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkSeasonal") {
            if (charCode == 40) {
                if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=chkShelf.ClientID %>').focus();
        }
        else {
            $('#<%=chkBatch.ClientID %>').focus();
        }
    }
    else if (charCode == 38) {
        $('#<%=chkMemDis.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkBatch") {
            if (charCode == 40) {
                $('#<%=chkShelf.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSeasonal.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkShelf") {
            if (charCode == 40) {
                $('#<%=chkHideData.ClientID %>').focus();
    }
    else if (charCode == 38) {
        if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=chkSeasonal.ClientID %>').focus();
        }
        else {
            $('#<%=chkBatch.ClientID %>').focus();
                }
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkHideData") {
            if (charCode == 40) {
                $('#<%=chkMarking.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkShelf.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkMarking") {
            if (charCode == 40) {
                $('#<%=chkBarcodePrint.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkHideData.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkBarcodePrint") {
            if (charCode == 40) {
                $('#<%=chkAllowCost.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkMarking.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkAllowCost") {
            if (charCode == 40) {
                if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=txtExpiryDays.ClientID %>').select();
        }
        else {
            $('#<%=cbWeightbased.ClientID %>').focus();
        }

    }
    else if (charCode == 38) {
        $('#<%=chkBarcodePrint.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "cbWeightbased") {
            if (charCode == 40) {
                $('#<%=txtExpiryDays.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkAllowCost.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
<%--else if (value == "cbAdditinalCess") {
    if (charCode == 40) {
        $('#<%=txtExpiryDays.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=cbWeightbased.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}--%>
        else if (value == "txtExpiryDays") {
            if (charCode == 13) {
                $('#<%=txtRating.ClientID %>').select();
                return false;
            }

        }
        else if (value == "txtRating") {
            if (charCode == 13) {
                $('#<%=txtShelfQty.ClientID %>').select();
                return false;
            }
        }
        else if (value == "txtShelfQty") {
            if (charCode == 13) {
                $('#<%=chkSelectAll.ClientID %>').focus();
                return false;
            }
        }
        else if (value == "chkSelectAll") {
            if (charCode == 40) {
                $('#<%=chkPurchaseOrder.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=txtShelfQty.ClientID %>').select();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkPurchaseOrder") {
            if (charCode == 40) {
                $('#<%=chkGRN.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSelectAll.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkGRN") {
            if (charCode == 40) {
                $('#<%=chkPurchaseReturn.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPurchaseOrder.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkPurchaseReturn") {
            if (charCode == 40) {
                $('#<%=chkPointOfSale.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkGRN.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkPointOfSale") {
            if (charCode == 40) {
                $('#<%=chkSalesReturn.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPurchaseReturn.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkSalesReturn") {
            if (charCode == 40) {
                $('#<%=chkOrderBooking.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPointOfSale.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkOrderBooking") {
            if (charCode == 40) {
                $('#<%=chkEstimate.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSalesReturn.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkEstimate") {
            if (charCode == 40) {
                $('#<%=chkQuatation.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkOrderBooking.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkQuatation") {
            if (charCode == 40) {
                $('#<%=chkSelectAllRight.ClientID %>').focus();
            }
            if (charCode == 38) {
                $('#<%=chkEstimate.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
        }
        else if (value == "chkSelectAllRight") {
            if (charCode == 38) {
                $('#<%=chkQuatation.ClientID %>').focus();
                    }
                    else if (charCode == 13) {
                        return false;
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncOrderByChanged() {
            try {
                if ($('#<%=ddlOrderBy.ClientID %>').val() == "2") {
                    $('#divcycledays').css("display", "block");
                }
                else {
                    $('#divcycledays').css("display", "none");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        /// Set Focus to Object on Choosen Enter key press
        $(document).ready(function () {
            fncDropdownEnterKeyVal();

            if ($('#<%=hidInvStatus.ClientID %>').val() == "InvalidInventory") {
        fncToastInformation("Invalid Inventorycode");
    }

});

        function fncDropdownEnterKeyVal() {
            try {
        <%--$("#ContentPlaceHolder1_txtUOMPurchase").bind('keydown', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13) {
                $('#<%=txtSalesUOM.ClientID %>').select();
            }
        });
        $("#ContentPlaceHolder1_txtSalesUOM").bind('keydown', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13 || keyCode === 9) {
                setTimeout(function () {
                    $('#<%=ddlStockType.ClientID %>').trigger("liszt:open");
                }, 150);
            }
        });--%>
        <%--$("#<%=txtStockType.ClientID %>").bind('keydown', function (e) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode === 13) {
                debugger;
                $('#<%=txtStyle.ClientID %>').select();
            }
        });--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        /// Stock Type Cha
        function fncStockTypeChange() {
            try {
                if ($('#<%=txtStockType.ClientID %>').val() == "Liquor" || $('#<%=txtStockType.ClientID %>').val() == "Non Liquor") {


            if ($('#<%=hidPriceEntered.ClientID %>').val() == "1" || $('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                $('#<%=hidPriceEntered.ClientID %>').val('0');
                fncRemoveDisableAttForInputGST();
                $("#dialog-InvPrice").dialog("open");
                fncOpenDialogWithDetail();
                fncToastInformation("You changed Stock type.Please Re-Enter Price Detail");
            }
            else {
                setTimeout(function () {
                    $('#<%=txtStyle.ClientID %>').select();
                }, 50);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncNetCostCalonAddCessAmtChange() {
            //var netCost=0, AddCessAmt = 0;
            try {
                fncCalcSelingPice();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=hidVendorName.ClientID %>').val(Description);
                }
                if (SearchTableName == "Brand") {
                    $('#<%=txtItemName.ClientID%>').val($('#<%=txtBrand.ClientID%>').val() + ' ' + $('#<%=txtInvcategory.ClientID%>').val());
                }
                if (SearchTableName == "category") {
                    fncGetDepartmentForSelecetedCategory(Description);
                }
                if (SearchTableName == "WineShop") {
                    $('#<%=hdStoctTypeVal.ClientID %>').val(Code);
            $('#<%=txtStockType.ClientID %>').val(Description);
                    fncStockTypeChange();
                }
                if (SearchTableName == "Store") {
                    $('#<%=hdStoctTypeVal.ClientID %>').val(Code);
            $('#<%=txtStockType.ClientID %>').val(Description);
                    fncStockTypeChange();
                }
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemName.ClientID %>').val(Description);
            if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
                        $("[id$=txtItemCode]").val(Code);
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkEditItemCode', '')
                    }
                }
                $('#<%=txtItemName.ClientID %>').focusout(function () {
                    var ItemName = $('#<%=txtItemName.ClientID%>').val();
             if ($('#<%=hidinventoryStatus.ClientID %>').val() == "New") {
                 $('#<%=txtShortName.ClientID%>').val(ItemName.substring(0, 20));
             }
         });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetDepartmentForSelecetedCategory(CategoryCode) {
            try {

                var obj = {};
                obj.CategoryCode = CategoryCode.trim().replace("'", "''");
                //$data = { prefix: CategoryCode.replace("'", "''") };
                $.ajax({
                    type: "POST",
                    url: "frmInventoryMasterTextiles.aspx/GetDeptCode",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $('#<%=txtDepartment.ClientID %>').val(msg.d);
            },
            error: function (data) {
                ShowPopupMessageBox(data.message)
            }
        });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function checkPhoneKey(event) {
            debugger;
            var StockType = $('#<%=hdStoctType.ClientID %>').val();
            if (StockType == "WineShop") {
                fncShowSearchDialogCommon(event, 'WineShop', 'txtStockType', 'txtStyle');
            }
            else {
                fncShowSearchDialogCommon(event, 'Store', 'txtStockType', 'txtStyle');
            }
        }

        $(document).ready(function () {
            $("#search").on("keyup", function () {
                var value = $(this).val().toUpperCase();
                $("#grdAttributeValue tr").each(function (index) {
                    if (index >= 0) {
                        $row = $(this);
                        var id = $row.find("td:eq(1)").text();
                        if (id.indexOf(value) !== 0) {
                            $(this).hide();
                        }
                        else {
                            $(this).show();
                        }
                    }
                });
            });

            $('#SelectAll').click(function (e) {
                $('#grdAttribute').find('td input:checkbox').prop('checked', true);
            });

        });

        function clearAttributetbl() {
            //AttributrClear = ""; 
            //$('#grdAttribute').find("tr:gt(0)").remove();
            $('#SelectAll').click();
            var table1 = document.getElementById("grdAttribute");
            checkboxes = document.getElementsByName("check-tab2");
            for (var i = 0; i < checkboxes.length; i++)
                if (checkboxes[i].checked) {
                    $('#grdAttributeCount tr').each(function () {
                        if (!this.rowIndex) return;
                        var customerId = $(this).find("td").eq(2).html();
                        var Maxcount = $(this).find("td:last").html();
                        if (table1.rows[i + 1].cells[2].innerHTML === customerId) {
                            Maxcount = Maxcount - 1;
                            $(this).find("td:last").html(Maxcount);
                            if (Maxcount == 0)
                                $(this).remove();
                        }
                    });
                    var index = table1.rows[i + 1].rowIndex;
                    table1.deleteRow(index);
                    i--;
                    console.log(checkboxes.length);
                }
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            //alert(keyCode);
            if (charCode == 188 || charCode == 190 || charCode == 191)
                return false;
            if ($('#<%=hidinventoryStatus.ClientID%>').val() == 'Edit') {
                if (charCode == 112) {
                    fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
                }

            }
        }

        $(document).ready(function () {
            $("input[type=text]").keyup(function () {
                $(this).val($(this).val().toUpperCase());
            });
        });

        var ItemNameVlidateValue = 0;
        function fncValidateItemName(Description) {
            try {
                var obj = {}, objdata; var Itemname1;
                if (Description == "") {
                    obj.ItemName = $('#<%=txtItemName.ClientID %>').val().trim();
            Itemname1 = $('#<%=txtItemName.ClientID %>').val().trim();
        }
        else {
            obj.ItemName = Description;
            Itemname1 = Description;
        }

        $.ajax({
            type: "POST",
            url: '<%=ResolveUrl("frmInventoryMaster.aspx/fncGetInventoryDetailForValidation")%>',
            data: JSON.stringify(obj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                objdata = jQuery.parseJSON(msg.d);

                if (objdata.Table.length > 0) {
                    var Itemname2;
                    Itemname2 = objdata.Table[0]["Description"];
                    if (Itemname1 == Itemname2) {
                        if ($('#<%=hidinventoryStatus.ClientID%>').val() != 'Edit') {
                            $('#<%=txtItemName.ClientID %>').select().focus();
                            ShowPopupMessageBox('This Inventory Name Already Exists !! -' + Description);
                            $('#<%=txtItemName.ClientID %>').select().focus();
                            ItemNameVlidateValue = 1;
                        }
                    }
                    else {
                        $('#<%=txtShortName.ClientID%>').val(Itemname1.substring(0, 20));
                    }
                }
                else {
                    $('#<%=txtShortName.ClientID%>').val(Itemname1.substring(0, 20));
                    $('#<%=txtItemName.ClientID %>').css("border", "1px solid black");
                }
            },
            error: function (data) {
                ShowPopupMessageBox(data.message);
            }
        });


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'InventoryMasterTextiles');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "InventoryMasterTextiles";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

        function fncGetGidDetails(event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                if ($('#<%=txtSearch.ClientID %>').val().trim() != "") {
                    var obj = {};
                    obj.InvCode = $('#<%=txtItemCode.ClientID %>').val().trim();
                    obj.BatchNo = $('#<%=txtSearch.ClientID %>').val().trim();
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: 'frmInventoryMasterTextiles.aspx/Get_Data',
                        data: JSON.stringify(obj),
                        dataType: 'JSON',
                        success: function (response) {
                            $('#<%=gvStockBottom.ClientID %>').css('display', 'none');
                            $('#<%=grdStyleCode.ClientID %>').css('display', 'block');
                            $('#<%=grdStyleCode.ClientID %>').empty();
                            // $('#grd').append("<tr><th>Recognition_Type </th><th>Recognition_Number </th></tr>")
                            for (var i = 0; i < response.d.length; i++) {

                                $('#<%=grdStyleCode.ClientID %>').append("<tr><td class='text-left' style='width: 160px;'>" + response.d[i].Vendor_Type + "</td><td  class='text-left' style='width: 115px;'>" + response.d[i].DONo_Type + "</td><td class='text-left' style='width: 115px;'>" + response.d[i].DODate_Type + "</td><td class='text-left' style='width: 100px;'>" + response.d[i].WQty_Type + "</td><td class='text-left' style='width: 100px;'>" + response.d[i].LQty_Type + "</td><td class='text-left' style='width: 115px;'>" + response.d[i].FocQty_Type + "</td><td class='text-left' style='width: 125px;'>" + response.d[i].UnitCost_Type + "</td><td class='text-left' style='width: 115px;'>" + response.d[i].NetCost_Type + "</td><td class='text-left' style='width: 115px;'>" + response.d[i].MRP_Type + "</td><td class='text-left' style='width: 115px;'>" + response.d[i].SellingPrice_Type + "</td><td class='text-left' style='width: 150px;cursor: pointer;text-decoration: underline;' onclick='fncGoGRNPage(this); return false;'>" + response.d[i].GidNo_Type + "</td>><td  style='display: none;'>" + response.d[i].POReceiptNo_Type + "</td>><td style='display: none;'>" + response.d[i].vendorcode_Type + "</td>><td style='display: none;'>" + response.d[i].BillType_Type + "</td>><td style='display: none;'>" + response.d[i].LocationCode_Type + "</td></tr>")
                            };

                        },
                        error: function () {

                            alert("Error");
                        }
                    });
                   
                }
                else {
                    $('#<%=gvStockBottom.ClientID %>').css('display', 'block');
                    $('#<%=grdStyleCode.ClientID %>').css('display', 'none');
                }
                return false;
            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li id="invhdr" class="active-page">Inventory Master </li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="container-group-full">
                    <div class="top-container-im">
                        <div class="left-container">
                            <div class="left-container-header">
                                Product Detail
                            </div>
                            <div class="left-container-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtInvcategory" MaxLength="50" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category',  'txtInvcategory', 'txtDepartment');" onfocus="fncToEnableShortcutKey('Category');"></asp:TextBox>
                                            <%--  <asp:DropDownList ID="ddlCategory" runat="server" class="chosen-select" CssClass="chzn-select">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtDepartment" MaxLength="50" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department',  'txtDepartment', 'txtBrand');" onfocus="fncToEnableShortcutKey('Department');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemCode" MaxLength="20" runat="server" onkeydown=" return fncInventorykeydown(event);" CssClass="form-control-res"></asp:TextBox>
                                            <asp:HiddenField ID="hdfItemCode" runat="server" />
                                            <asp:HiddenField ID="hfBatch" runat="server" />
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBrand" runat="server" MaxLength="50" onkeydown="return fncShowSearchDialogCommon(event, 'Brand',  'txtBrand', 'txtItemName');" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('Brand');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label><span
                                            class="mandatory">*</span>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtItemName" runat="server" onkeypress=" return fncBlockSpecialChar(event);" onkeydown="return fncShowSearchDialogInventory(event, 'Inventory',  'txtItemName', 'txtShortName');" CssClass="form-control-res" MaxLength="50" onfocus="fncToEnableShortcutKey('Itemname');"></asp:TextBox>
                                        <asp:TextBox ID="txtOtherLanguageName" runat="server" CssClass="tamil form-control-res display_none" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShortName %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtShortName" runat="server" CssClass="form-control-res" onfocus="fncToEnableShortcutKey('');" MaxLength="20"></asp:TextBox>
                                        <asp:TextBox ID="txtOtherLanguageSName" runat="server" CssClass="tamil form-control-res display_none" MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single">
                                    <div class="label-left">
                                        <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtRemarks" runat="server" MaxLength="100" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWareHouse" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse',  'txtWareHouse', 'txtMerchandise');" onfocus="fncToEnableShortcutKey('WareHouse');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label11" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtMerchandise" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise',  'txtMerchandise', 'txtManafacture');" onfocus="fncToEnableShortcutKey('Merchandise');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtManafacture" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture',  'txtManafacture', 'txtItemType');" onfocus="fncToEnableShortcutKey('Manufacture');"></asp:TextBox>
                                            <%--  <asp:DropDownList ID="ddlManafacture" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemType %>'></asp:Label>
                                            <span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtItemType" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ItemType',  'txtItemType', 'txtUOMPurchase');" onfocus="fncToEnableShortcutKey('ItemType');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="left-container-detail" style="margin-top: 4px">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label8" runat="server" Text="UOM Purchase"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtUOMPurchase" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'UOM',  'txtUOMPurchase', 'txtSalesUOM');" CssClass="form-control-res"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlUOMPurchase" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label88" runat="server" Text="Sales UOM"></asp:Label><span class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSalesUOM" onkeydown="return fncShowSearchDialogCommon(event, 'UOM',  'txtSalesUOM', 'txtStockType');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlSalesUOM" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_StockType %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <%--<asp:DropDownList ID="ddlStockType" runat="server" CssClass="form-control-res" onchange="fncStockTypeChange();">
                                            </asp:DropDownList>--%>
                                            <asp:TextBox ID="txtStockType" runat="server" CssClass="form-control-res" onkeydown="return checkPhoneKey(event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-container-top">
                            <div class="right-container-top-header">
                                More Detail
                            </div>
                            <div class="right-container-top-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lbl_Style %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtStyle" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Style',  'txtStyle', 'txtSize');" onfocus="fncToEnableShortcutKey('Style');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlStyle" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_Package %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtPackage" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Package',  'txtPackage', 'txtCCode');" onfocus="fncToEnableShortcutKey('');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlPackage" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label18" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSize" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Size',  'txtSize', 'txtModelNo');" onfocus="fncToEnableShortcutKey('Size');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlSize" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label19" runat="server" Text='<%$ Resources:LabelCaption,lbl_CCode %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtCCode" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'CCode',  'txtCCode', 'txtVendor');" onfocus="fncToEnableShortcutKey('CCode');"></asp:TextBox>
                                            <%--   <asp:DropDownList ID="ddlCCode" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label21" runat="server" Text='<%$ Resources:LabelCaption,lbl_ModelNo %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtModelNo" runat="server" MaxLength="50" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label22" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label><span
                                                class="mandatory">*</span>
                                        </div>
                                        <div id="vendorNametooltip" class="label-right" title="">
                                            <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtClass');" onfocus="fncToEnableShortcutKey('Vendor');"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_Origin %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtOrigin" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Origin', 'txtOrigin', 'txtSubCategory');" onfocus="fncToEnableShortcutKey('Origin');"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label24" runat="server" Text='<%$ Resources:LabelCaption,lbl_Class %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtClass" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtSubClass');" onfocus="fncToEnableShortcutKey('Class');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label20" runat="server" Text='<%$ Resources:LabelCaption,lbl_SubCategory %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSubCategory" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtPackage');" onfocus="fncToEnableShortcutKey('SubCategory');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSubCategory" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label25" runat="server" Text='<%$ Resources:LabelCaption,lbl_SubClass %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSubClass" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'txtSubClass', 'txtFloor');" onfocus="fncToEnableShortcutKey('SubClass');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSubClass" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-container-bottom">
                            <div class="right-container-bottom-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label23" runat="server" Text='<%$ Resources:LabelCaption,lbl_Floor %>'></asp:Label>
                                            <span
                                                class="mandatory">*</span>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtFloor" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');" onfocus="fncToEnableShortcutKey('Floor');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label29" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtSection" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');" onfocus="fncToEnableShortcutKey('Section');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label26" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtBin" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');" onfocus="fncToEnableShortcutKey('Bin');"></asp:TextBox>
                                            <%-- <asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label32" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtShelf" runat="server" MaxLength="50" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtWeight');" onfocus="fncToEnableShortcutKey('Shelf');"></asp:TextBox>
                                            <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-container-bottom">
                            <div class="right-container-bottom-detail">
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label28" runat="server" Text='<%$ Resources:LabelCaption,lbl_Weight %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWeight" runat="server" Text="0.00" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label31" runat="server" Text='<%$ Resources:LabelCaption,lbl_Width %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtWidth" runat="server" Text="0.00" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group-split">
                                    <div class="control-group-left">
                                        <div class="label-left">
                                            <asp:Label ID="Label27" runat="server" Text='<%$ Resources:LabelCaption,lbl_Height %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtHeight" runat="server" Text="0.00" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="control-group-right">
                                        <div class="label-left">
                                            <asp:Label ID="Label30" runat="server" Text='<%$ Resources:LabelCaption,lbl_Length %>'></asp:Label>
                                        </div>
                                        <div class="label-right">
                                            <asp:TextBox ID="txtLength" runat="server" Text="0.00" MaxLength="10" onfocus="fncToEnableShortcutKey('');" CssClass="form-control-res-right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bottom-container display_block">
                        <div class="panel panel-default">
                            <div id="Tabs" role="tabpanel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs custnav custnav-im" role="tablist">
                                    <li id="liInventoryPricing"><a href="#InventoryPricing" aria-controls="InventoryPricing" role="tab" data-toggle="tab">Inventory Pricing </a></li>
                                    <li id="liBreakPrice"><a href="#BreakPrice" aria-controls="BreakPrice" role="tab" data-toggle="tab">Order
                                        & BreakPrice</a></li>
                                    <li id="liActivationSettings"><a href="#ActivationSettings" aria-controls="ActivationSettings" role="tab" data-toggle="tab">Activation Settings</a></li>
                                    <li id="liStockPurchase"><a href="#StockPurchase" aria-controls="StockPurchase" role="tab" data-toggle="tab">Stock & Purchase</a></li>
                                    <li id="liSales"><a href="#Sales" aria-controls="Sales" role="tab" data-toggle="tab">Sales</a></li>
                                    <li id="liImage"><a href="#Image" aria-controls="Image" role="tab" data-toggle="tab">Image</a></li>
                                    <li id="liPromotion"><a href="#Promotion" aria-controls="Promotion" role="tab" data-toggle="tab">Promotion</a></li>
                                    <li id="liAttributes"><a href="#Attributes" aria-controls="Attributes" role="tab" data-toggle="tab">Attributes</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                                    <div class="tab-pane active" role="tabpanel" id="InventoryPricing">
                                        <div class="inv-price">
                                            <div class="selling">
                                                <div class="selling-left">
                                                    <div class="selling-header">
                                                        Selling Details
                                                    </div>
                                                    <div class="selling-detail">
                                                        <div class="radio-top">
                                                            <div class="purchased">
                                                                <asp:Label ID="Label36" runat="server" Text="Purchased By"></asp:Label>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbtCost" runat="server" GroupName="grpPurchased" />
                                                                Cost
                                                                <br />
                                                                <span class="mark">MarkUp</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbtMRP" TabIndex="1" runat="server" GroupName="grpPurchased" />
                                                                MRP1
                                                                <br />
                                                                <span class="mark">MarkDown</span>
                                                            </div>
                                                            <div class="radio-price">
                                                                <asp:RadioButton ID="rbnMRP2" runat="server" GroupName="grpPurchased" />
                                                                MRP2
                                                                <br />
                                                                <span class="mark">PartialMarkDown</span>
                                                            </div>
                                                            <%--<div class="radio-price">
                                                        <asp:RadioButton ID="rbtMAmt" runat="server" GroupName="grpPurchased" />
                                                        M Amt
                                                    </div>--%>
                                                            <div class="avg-price">
                                                                <div class="label-left" style="width: 45%; float: left">
                                                                    <asp:Label ID="Label76" runat="server" Text="GST Group"></asp:Label>
                                                                </div>
                                                                <div class="label-right" style="width: 55%; float: right">
                                                                    <asp:TextBox ID="txtGSTGroup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="control-group-split">
                                                            <div class="control-group-left1">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label97" runat="server" Text="Selling Price"></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtFetchSellingPrice" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-middle" style="width: 60%">
                                                                <div class="label-left" style="width: 15%; margin-top: -8px">
                                                                    <asp:ImageButton ID="imgPriceChange" runat="server" ImageUrl="~/images/go_arrow.jpg"
                                                                        OnClientClick="fncOpenInvPriceChange(); return false;" onkeydown="return fncImageButtonkeydown(event);"
                                                                        ToolTip="Click to open selling detail" />
                                                                </div>
                                                                <div class="inv_tooltip">
                                                                    <span>Click arrow  to open selling detail </span>
                                                                </div>

                                                                <div class="label-right" style="width: 85%">
                                                                    <span style="color: Blue">MRP : </span>
                                                                    <asp:Label ID="lblMRP" runat="server" Text="0.00" Style="color: Blue"></asp:Label>
                                                                    <asp:HiddenField ID="hdfMRP" runat="server" />
                                                                    &nbsp; <span style="color: Blue">Net Cost : </span>
                                                                    <asp:Label ID="lblNetCost" runat="server" Text="0.00" Style="color: Blue"></asp:Label>
                                                                    <asp:HiddenField ID="hdfNetCost" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="selling-middle" style="width: 24.8%">
                                                    <div class="whole-price">
                                                        <div class="whole-price-header">
                                                            Wholesale Price
                                                        </div>
                                                        <div class="whole-price-detail">
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label37" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice1 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice1" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label38" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice2 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice2" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="control-group-single-res">
                                                                <div class="label-left">
                                                                    <asp:Label ID="Label39" runat="server" Text='<%$ Resources:LabelCaption,lbl_WPrice3 %>'></asp:Label>
                                                                </div>
                                                                <div class="label-right">
                                                                    <asp:TextBox ID="txtWPrice3" runat="server" CssClass="form-control-res-right"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="divBarcode" class="selling-right" runat="server">
                                                    <div class="barcode">
                                                        <div class="barcode-header">
                                                            Barcode & Shortcode
                                                        </div>
                                                        <div class="barcode-detail">

                                                            <div class="Payment_fixed_headers tbl_barcode">
                                                                <table id="tblBarcode" cellspacing="0" rules="all" border="1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col">Delete
                                                                            </th>
                                                                            <th scope="col">Barcode
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    </tbody>
                                                                    <tfoot>
                                                                    </tfoot>
                                                                </table>
                                                            </div>


                                                            <div class="control-group-split" style="padding: 4px 0">
                                                                <%--<asp:Panel ID="Panel1" runat="server" DefaultButton="lnkInsert">--%>
                                                                <div class="control-group-left" style="width: 64%">
                                                                    <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res" MaxLength="20" onkeydown=" return fncBarcodeKeyDown(event);"></asp:TextBox>
                                                                </div>
                                                                <div class="control-group-right" style="width: 34%">
                                                                    <%--<asp:Button ID="btnBarcodeInsert" Text="Insert" runat="server" OnClick="lnkInsert_Click" />--%>
                                                                    <asp:Button ID="lnkInsert" runat="server" class="button-blue-withoutanim" OnClientClick="fncAddBarcode();return false;" Text="Insert" Width="100%"></asp:Button>
                                                                </div>
                                                                <%--</asp:Panel>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <label id="lblMasterOpen" style="color: white; background: green; margin-left: 175px;"></label>
                                            </div>
                                            <div id="dialog-confirm" title="Enterpriser">
                                                <p>
                                                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>This is batch Item. Want to go BatchInfo ?
                                                </p>
                                            </div>
                                            <div id="dialog-clear-confirm" title="Enterpriser">
                                                <p>
                                                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>Do you want to Clear ?
                                                </p>
                                            </div>
                                            <div class="hiddencol">
                                                <div id="dialog-InvPrice" title="Selling Price Calculation">
                                                    <div class="dialog-inv">
                                                        <div class="dialog-inv-header">
                                                            <div class="control-group-split">
                                                                <div class="control-group-left" style="width: 30%">
                                                                    <div class="label-left" style="width: 40%">
                                                                        <asp:Label ID="Label87" runat="server" Text="Item Code"></asp:Label>
                                                                    </div>
                                                                    <div class="label-right" style="width: 60%">
                                                                        <asp:TextBox ID="txtItemCodePopup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group-right" style="width: 68%">
                                                                    <div class="label-left" style="width: 20%">
                                                                        <asp:Label ID="Label89" runat="server" Text="Item Name"></asp:Label>
                                                                    </div>
                                                                    <div class="label-right" style="width: 80%">
                                                                        <asp:TextBox ID="txtItemNamePopup" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-header">
                                                                Input Tax
                                                            </div>
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label92" runat="server" Text="IGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputIGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label90" runat="server" Text="SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputSGST" runat="server" CssClass="form-control-res" Width="100%">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-right1 inv_purchasebyPartial_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label91" runat="server" Text="CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlInputCGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-header">
                                                                Output Tax
                                                            </div>
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label95" runat="server" Text="IGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputIGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label93" runat="server" Text="SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputSGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-right1 inv_purchasebyPartial_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label94" runat="server" Text="CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:DropDownList ID="ddlOutputCGST" runat="server" CssClass="form-control-res">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-tax">
                                                            <div class="dialog-Inv-tax-detail">
                                                                <div class="control-group-split">
                                                                    <div class="control-group-left1 inv_purchaseby_width">
                                                                        <div class="grn_net_lbl3">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label96" runat="server" Text="CESS"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtCESSPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grn_net_lbl3 inv_paddingleft">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label106" runat="server" Text="A.C.Amt"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtAddCessAmt" runat="server" onchange="fncNetCostCalonAddCessAmtChange();" CssClass="form-control-res text-right" Text="0.00"></asp:TextBox>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="control-group-middle inv_purchaseby_width">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label112" runat="server" Text="HSN Code"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtHSNCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-right1 inv_purchasebyPartial_width inv_rbn_purchaseborder">
                                                                        <div class="label-left" style="width: 100%">
                                                                            <asp:RadioButton ID="rbtCostPopup" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                            <span id="markup" class="inv_rbn_purchaseby">Cost (MarkUp)  </span>&nbsp;
                                                                        <asp:RadioButton ID="rbtMrpPopup" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                            <span id="markdown" class="inv_rbn_purchaseby">MRP (MarkDown)  </span>&nbsp;
                                                                         <asp:RadioButton ID="rbtPartialMD" runat="server" GroupName="PurchaseBy" Enabled="false" />
                                                                            <span id="PMD" class="inv_rbn_purchaseby">MRP(PMD)  </span>
                                                                        </div>
                                                                        <div class="label-right" style="width: 0%">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="dialog-inv-set-price">
                                                            <div class="dialog-inv-container-set-price-header">
                                                                Selling Price
                                                            </div>
                                                            <div class="dialog-inv-container-set-price-detail">
                                                                <div class="set-price-left">
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label65" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right" style="width: 40%">
                                                                            <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res text-right" onkeydown="return isNumberKeyWithDecimalNew(event);" onkeypress=" return fncInventoryKeyPress(event,'MRP'); "></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtMRPMarkDown" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event);" onkeypress=" return fncInventoryKeyPress(event,'MRPMarkDown');"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label66" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicCost %>'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtBasicCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdfBasicCost" runat="server" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label67" runat="server" Text="Discount(%)"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc1" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc2" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 13%">
                                                                            <asp:TextBox ID="txtDiscountPrc3" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 21%">
                                                                            <asp:TextBox ID="txtDiscountAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label72" runat="server" Text='<%$ Resources:LabelCaption,lbl_GrossCost %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtGrossCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label73" runat="server" Text="Input SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputSGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputSGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label12" runat="server" Text="Input CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputCGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label113" runat="server" Text="CESS"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtInputCESSPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtInputCESSAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label74" runat="server" Text='<%$ Resources:LabelCaption,lbl_NetCost %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtNetCost" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="set-price-right">

                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label82" runat="server" Text='<%$ Resources:LabelCaption,lbl_SPMarginper %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtFixedMargin" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event);" onkeypress="return fncInventoryKeyPress(event,'FixedMargin');"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label75" runat="server" Text='<%$ Resources:LabelCaption,lbl_BasicSelling %>'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtBasicSelling" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label33" runat="server" Text="Output SGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputSGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputSGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label34" runat="server" Text="Output CGST"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputCGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label114" runat="server" Text="CESS"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtOutputCESSPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 8%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 32%">
                                                                            <asp:TextBox ID="txtOutputCESSAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label80" runat="server" Text='<%$ Resources:LabelCaption,lbl_RoundOff %>'></asp:Label>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtRoundOff" runat="server" CssClass="form-control-res text-right"
                                                                                Enabled="false" Text="0.00"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label78" runat="server" Text='NetSellingPrice'></asp:Label><span
                                                                                class="mandatory">*</span>
                                                                        </div>
                                                                        <div class="label-right">
                                                                            <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res text-right"
                                                                                onkeydown="return isNumberKeyWithDecimalNew(event);" onkeypress="return fncInventoryKeyPress(event,'SellingPrice');"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res ">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label81" runat="server" Text="Lia(GST+Cess)"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtGSTLiabilityPer" runat="server" Enabled="false" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtGSTLiability" runat="server" Enabled="false" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res display_none ">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label77" runat="server" Text="SGST Liability"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtVatLiabilitySGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtVatLiabilitySGSTAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-single-res display_none">
                                                                        <div class="label-left">
                                                                            <asp:Label ID="Label35" runat="server" Text="CGST Liability"></asp:Label>
                                                                        </div>
                                                                        <div class="label-right" style="width: 20%">
                                                                            <asp:TextBox ID="txtVatLiabilityCGSTPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                        <div class="label-right" style="width: 10%">
                                                                            %
                                                                        </div>
                                                                        <div class="label-right" style="width: 30%">
                                                                            <asp:TextBox ID="txtVatLiabilityCGSTAMt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="set-price-middle">
                                                                    <div class="inv_margin_border">
                                                                        <span class="inv_margin_hdr">Net Margin from MRP</span>
                                                                        <div class="display_table">
                                                                            <div class="float_left inv_margin_lbl">
                                                                                <asp:Label ID="Label40" runat="server" Text="Percentage(%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMarginMRP" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_margin_amt">
                                                                                <asp:Label ID="Label83" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMRPAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="inv_margin_border inv_netmarginearned">
                                                                        <span class="inv_margin_hdr">Net Margin from Sellingprice</span>
                                                                        <div class="display_table">
                                                                            <div class="float_left inv_margin_lbl">
                                                                                <asp:Label ID="Label79" runat="server" Text="Percentage(%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtMarginSP" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_margin_amt">
                                                                                <asp:Label ID="Label84" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_margin_txt">
                                                                                <asp:TextBox ID="txtSPAmt" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group-split" style="display: none">
                                                                        <div class="control-group-left1" style="width: 38%">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label85" runat="server" Text="Profit%"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtProfitMRPPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-middle" style="width: 39%">
                                                                            <div class="label-left">
                                                                                <asp:Label ID="Label86" runat="server" Text="Profit%"></asp:Label>
                                                                            </div>
                                                                            <div class="label-right">
                                                                                <asp:TextBox ID="txtProfitSPPrc" runat="server" CssClass="form-control-res text-right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <asp:Panel ID="pnlWholeSalePrice" CssClass="inv_whole_border" runat="server">
                                                                        <span class="inv_margin_hdr">Whole Sale Price</span>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:Label ID="Label101" runat="server" Text="Margin WPrice1 (%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtMarginWPrice1" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label98" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice1Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:Label ID="Label102" runat="server" Text="Margin WPrice2 (%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtMarginWPrice2" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label99" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice2Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="control-group-single-res">
                                                                            <div class="float_left inv_WPlbl">
                                                                                <asp:Label ID="Label103" runat="server" Text="Margin WPrice3 (%)"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtMarginWPrice3" runat="server" CssClass="form-control-res text_right" onkeydown="return isNumberKeyWithDecimalNew(event)" onkeypress=" return fncInventoryKeyPress(event,'WPrice3Per');"></asp:TextBox>
                                                                            </div>
                                                                            <div class="float_left inv_WPAmt">
                                                                                <asp:Label ID="Label100" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                            <div class="float_left inv_WPtxt ">
                                                                                <asp:TextBox ID="txtWholePrice3Popup" runat="server" CssClass="form-control-res text_right"
                                                                                    onkeydown="return isNumberKeyWithDecimalNew(event);" onkeypress="return fncInventoryKeyPress(event,'WPrice3');"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField ID="hdfSellingPriceRoundOff" runat="server" />
                                                    <asp:HiddenField ID="hdfMarkDown" runat="server" />
                                                    <asp:LinkButton ID="lnkSaveChangeMem" runat="server" class="button-blue" Text="Ok"
                                                        Visible="false"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="BreakPrice">
                                        <div class="breakprice">
                                            <div class="order-process order-process-width">
                                                <div style="margin: 0 10px 0px 40%">
                                                    <span style="color: Blue; font-weight: bold">Re-Order Process </span>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left order_txtwidth">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label41" runat="server" Text="Minimum Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtMinimumQty" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right" style="width: 38%">
                                                        <div class="label-left" style="width: 20%">
                                                            <asp:Label ID="Label42" runat="server" Text="By"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 80%">
                                                            <asp:DropDownList ID="ddlMiniBy" runat="server" CssClass="form-control-res">
                                                                <asp:ListItem Value="1">Demand Forecast</asp:ListItem>
                                                                <asp:ListItem Value="2">Manual</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split order_txtwidth1">
                                                    <div class="control-group-left" style="width: 60%">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label43" runat="server" Text="Max Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtMaxQty" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left order_txtwidth">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label45" runat="server" Text="ReOrder Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtReOrderQty" runat="server" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right" style="width: 38%">
                                                        <div class="label-left" style="width: 20%">
                                                            <asp:Label ID="Label46" runat="server" Text="By"></asp:Label>
                                                        </div>
                                                        <div class="label-right" style="width: 80%">
                                                            <asp:DropDownList ID="ddlByReOrder" runat="server" CssClass="form-control-res">
                                                                <asp:ListItem Value="1">Lead Time</asp:ListItem>
                                                                <asp:ListItem Value="2">Seasonal</asp:ListItem>
                                                                <asp:ListItem Value="3">Manual</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split order_txtwidth1">
                                                    <div class="control-group-left" style="width: 60%">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label47" runat="server" Text="Order By"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlOrderBy" runat="server" onchange="fncOrderByChanged();" CssClass="form-control-res">
                                                                <asp:ListItem Value="1">Order Qty</asp:ListItem>
                                                                <asp:ListItem Value="2">Cyc.Days</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="control-group-split">
                                                    <div id="divcycledays" class="control-group-left order_txtwidth display_none">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label105" runat="server" Text="Cycle Days"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtCycleDays" runat="server" MaxLength="4" CssClass="form-control-res" onkeydown="return isNumberKeyWithDecimalNew(event);"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-right breakprice_btn">
                                                        <asp:LinkButton ID="lnkbreakpriceQty" class="button-blue-withoutanim breakprice_btn1" Text='<%$ Resources:LabelCaption,lblBreakPriceQty %>'
                                                            OnClientClick="fncShowBreakPriceQty();return false;" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div id="divrepeater" class="poList">
                                                    <table id="tblga" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">S.No
                                                                </th>
                                                                <th scope="col">Supplier
                                                                </th>
                                                                <th scope="col">PO No
                                                                </th>
                                                                <th scope="col">PO Date
                                                                </th>
                                                                <th scope="col">W.Qty
                                                                </th>
                                                                <th scope="col">L.Qty
                                                                </th>
                                                                <th scope="col">F.Qty
                                                                </th>
                                                                <th scope="col">Net Cost
                                                                </th>
                                                                <th scope="col">MRP
                                                                </th>
                                                                <th scope="col">Sellingprice
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptrPoList" runat="server">
                                                            <HeaderTemplate>
                                                                <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNo") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblVendorname" runat="server" Text='<%# Eval("VendorName") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPono" runat="server" Text='<%# Eval("PoNo") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblPodate" runat="server" Text='<%# Eval("PODate") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblWQty" runat="server" Text='<%# Eval("WQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLQty" runat="server" Text='<%# Eval("LQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblFocQty" runat="server" Text='<%# Eval("FocQty") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblNetcost" runat="server" Text='<%# Eval("NetCost") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSellingprice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="ActivationSettings">
                                        <div class="activation">
                                            <div class="activation-left">
                                                <div class="inv_settinghdr">
                                                    <span>General Activation Settings </span>
                                                </div>
                                                <div class="activation-check-left activation-check-width">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkExpiryDate" onkeydown=" return fncCheckBoxKeyDown(event,'chkExpiryDate');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label104" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDate %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSerialNo" onkeydown="return fncCheckBoxKeyDown(event,'chkSerialNo');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label48" runat="server" Text='<%$ Resources:LabelCaption,lbl_SerialNoRequired %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkAutoPO" onkeydown="return fncCheckBoxKeyDown(event,'chkAutoPO');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label49" runat="server" Text='<%$ Resources:LabelCaption,lbl_AutoPOAllowed %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPackage" onkeydown="return fncCheckBoxKeyDown(event,'chkPackage');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label50" runat="server" Text='<%$ Resources:LabelCaption,lbl_PackageItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkLoyality" onkeydown="return fncCheckBoxKeyDown(event,'chkLoyality');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label51" runat="server" Text='<%$ Resources:LabelCaption,lbl_Loyality %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSpecial" onkeydown="return fncCheckBoxKeyDown(event,'chkSpecial');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label52" runat="server" Text='<%$ Resources:LabelCaption,lbl_Special %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkMemDis" onkeydown="return fncCheckBoxKeyDown(event,'chkMemDis');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label53" runat="server" Text='<%$ Resources:LabelCaption,lbl_MemDisAllowed %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSeasonal" onkeydown="return fncCheckBoxKeyDown(event,'chkSeasonal');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label54" runat="server" Text='<%$ Resources:LabelCaption,lbl_SeasonalItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right activation-check-width">
                                                    <%--<div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSeasonal" onkeydown="return fncCheckBoxKeyDown(event,'chkSeasonal');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label54" runat="server" Text='<%$ Resources:LabelCaption,lbl_SeasonalItem %>'></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkBatch" onkeydown="return fncCheckBoxKeyDown(event,'chkBatch');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label55" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batch %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkShelf" onkeydown="return fncCheckBoxKeyDown(event,'chkShelf');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label56" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfStorage %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkHideData" onkeydown="return fncCheckBoxKeyDown(event,'chkHideData');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label57" runat="server" Text='<%$ Resources:LabelCaption,lbl_HideDataOnBarcode %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkMarking" onkeydown="return fncCheckBoxKeyDown(event,'chkMarking');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label58" runat="server" Text='<%$ Resources:LabelCaption,lbl_MarkingItem %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkBarcodePrint" onkeydown="return fncCheckBoxKeyDown(event,'chkBarcodePrint');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label59" runat="server" Text='<%$ Resources:LabelCaption,lbl_NeedBarcodePrint %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkAllowCost" onkeydown="return fncCheckBoxKeyDown(event,'chkAllowCost');" CssClass="checkbox1" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label60" runat="server" Text='<%$ Resources:LabelCaption,lbl_AllowCostEdit %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbWeightbased" onkeydown="return fncCheckBoxKeyDown(event,'cbWeightbased');" CssClass="checkbox1" Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblWeightbased" runat="server" Text='Weight Based'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control display_none">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbAdditionalCess" onkeydown="return fncCheckBoxKeyDown(event,'cbAdditinalCess');" CssClass="checkbox1 " Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblAdditionalCess" runat="server" Text='Additional Cess'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right activation-check-width">
                                                    <%-- <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="cbAdditionalCess" onkeydown="return fncCheckBoxKeyDown(event,'cbAdditinalCess');" CssClass="checkbox1" Checked="false" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="lblAdditionalCess" runat="server" Text='Additional Cess'></asp:Label>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label44" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDays %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtExpiryDays" onkeydown="return fncCheckBoxKeyDown(event,'txtExpiryDays');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label116" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfQty %>'></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtShelfQty" onkeydown="return fncCheckBoxKeyDown(event,'txtShelfQty');" runat="server" MaxLength="5" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-left activation-check-width">
                                                        <div class="label-left">
                                                            <asp:Label ID="Label115" runat="server" Text="Rating"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtRating" onkeydown="return fncCheckBoxKeyDown(event,'txtRating');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                                                onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="float_left">
                                                        <div class="inv_selectall">
                                                            <asp:CheckBox ID="chkSelectAll" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAll');" runat="server" />
                                                        </div>
                                                        <div class="float_left">
                                                            <span style="color: Blue">Select All</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="activation-right">
                                                <div class="inv_settinghdr">
                                                    <span>Transaction Activation Settings </span>
                                                </div>
                                                <div class="activation-check-left">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPurchaseOrder" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseOrder');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkGRN" onkeydown="return fncCheckBoxKeyDown(event,'chkGRN');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label62" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNPurchase %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPurchaseReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseReturn');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label63" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseReturn %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkPointOfSale" onkeydown="return fncCheckBoxKeyDown(event,'chkPointOfSale');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label64" runat="server" Text='<%$ Resources:LabelCaption,lbl_POS %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="activation-check-right">
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkSalesReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkSalesReturn');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label68" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesReturn %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkOrderBooking" onkeydown="return fncCheckBoxKeyDown(event,'chkOrderBooking');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label69" runat="server" Text='<%$ Resources:LabelCaption,lbl_OrderBooking %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkEstimate" onkeydown="return fncCheckBoxKeyDown(event,'chkEstimate');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label70" runat="server" Text='<%$ Resources:LabelCaption,lbl_Estimate %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="checkbox-control">
                                                        <div class="checkbox-left">
                                                            <asp:CheckBox ID="chkQuatation" onkeydown="return fncCheckBoxKeyDown(event,'chkQuatation');" runat="server" />
                                                        </div>
                                                        <div class="checkbox-right">
                                                            <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lbl_Quatation %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-group-split">
                                                    <div class="control-group-right inv_tran_selectall">
                                                        <div class="inv_Tran_lblselectall">
                                                            <asp:CheckBox ID="chkSelectAllRight" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAllRight');" runat="server" />
                                                        </div>
                                                        <div class="label-left">
                                                            <span style="color: Blue">Select All</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="StockPurchase">
                                        <div style="height: 35px;" class="stockgrid-top">
                                            <table rules="all" border="1" id="tblgvStockTopdummy" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <%--style="font-family: Arial; font-size: 10pt; width: 600px; color: black; border-collapse: collapse; height: 100%;"--%>
                                                <tr>
                                                    <td style="text-align: center; width: 115px">Location Code</td>
                                                    <td style="text-align: center; width: 75px">L.Q.O.H</td>
                                                    <td style="text-align: center; width: 75px">W.Q.O.H</td>
                                                    <td style="text-align: center; width: 115px">Sold</td>
                                                    <td style="text-align: center; width: 115px">Min</td>
                                                    <td style="text-align: center; width: 115px">Max</td>
                                                    <td style="text-align: center; width: 115px">ReOrder</td>
                                                    <td style="text-align: center; width: 115px">Price</td>
                                                    <td style="text-align: center; width: 115px">Avg Cost</td>
                                                    <td style="text-align: center; width: 115px">MRP</td>
                                                    <td style="text-align: center; width: 115px">GST</td>
                                                    <td style="text-align: center; width: 139px">Cyc Days</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="stockgrid-top GridDetails">
                                            <asp:GridView ID="gvStockTop" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="75px" ItemStyle-CssClass="text-right" DataField="QtyOnHand" HeaderText="L.Q.O.H"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="75px" NullDisplayText="" HeaderText="W.Q.O.H"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="QtySold" HeaderText="Sold"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MINQTY" HeaderText="Min"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MAXQTY" HeaderText="Max"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="REORDERLEVEL" HeaderText="ReOrder"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="Price" HeaderText="Price"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="AvgCost" HeaderText="Avg Cost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="ITaxPer3" HeaderText="GST"></asp:BoundField>
                                                    <asp:BoundField DataField="REORDERDAYS" ItemStyle-CssClass="text-left" HeaderText="Cyc Days"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" onkeydown="return fncGetGidDetails(event);" placeholder="Enter Style Code To Search"
                                            onFocus="this.select()"></asp:TextBox>
                                        <%--Vijay -20210309--%>
                                        <div style="height: 35px;" class="stockgrid-top">
                                            <table rules="all" border="1" id="Table1" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <tr>
                                                    <td style="text-align: center; width: 160px">Supplier</td>
                                                    <td style="text-align: center; width: 113px">DO/INV</td>
                                                    <td style="text-align: center; width: 113px">DO/Inv Date</td>
                                                    <td style="text-align: center; width: 98px">WQty</td>
                                                    <td style="text-align: center; width: 98px">LQty</td>
                                                    <td style="text-align: center; width: 113px">FocQty</td>
                                                    <td style="text-align: center; width: 123px">Basic Cost</td>
                                                    <td style="text-align: center; width: 115px">Net Cost</td>
                                                    <td style="text-align: center; width: 115px">MRP</td>
                                                    <td style="text-align: center; width: 115px">Selling Price</td>
                                                    <td style="text-align: center; width: 148px">Gid No</td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div class="stockgrid-bottom GridDetails">

                                            <asp:GridView ID="grdStyleCode" runat="server"   CssClass="pshro_GridDgn" >
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" /> 
                                                 <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                        </asp:GridView> 
                                            <asp:GridView ID="gvStockBottom" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="160px" ItemStyle-CssClass="text-left" DataField="VendorName" HeaderText="Supplier"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="DONo" HeaderText="DO/INV"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="DODate" HeaderText="DO/Inv Date" DataFormatString="{0:dd-MM-yyyy}"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="100px" ItemStyle-CssClass="text-right" DataField="WQty" HeaderText="WQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="100px" ItemStyle-CssClass="text-right" DataField="LQty" HeaderText="LQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="FocQty" HeaderText="FocQty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="125px" ItemStyle-CssClass="text-right" DataField="UnitCost" HeaderText="Basic Cost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="NetCost" HeaderText="NetCost"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="GidNo" ItemStyle-CssClass="text-left" ItemStyle-Width="150">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkGidNo" runat="server" Text='<%# Eval("GidNo") %>' OnClientClick="fncGoGRNPage(this);return false;"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="POReceiptNo" HeaderText="GA No" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="vendorcode" HeaderText="Vendorcode" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="BillType" HeaderText="Billtype" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                    <asp:BoundField DataField="LocationCode" HeaderText="Location" HeaderStyle-CssClass="display_none" ItemStyle-CssClass="display_none"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>   
                                        </div>
                                        <div class="control-container">
                                            <div class="control-button">
                                                <asp:Button ID="btnPRReq" runat="server" class="button-blue" Text="PR Request" OnClick="btnPRReq_Click" />
                                            </div>
                                            <div class="control-button">
                                                <asp:Button ID="btnBatch" runat="server" class="button-blue" Text="Batch" OnClick="btnBatch_Click" />
                                            </div>
                                        </div>
                                        <div id="dialog-batch" title="Batch Detail" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="gvBatchPopup" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                                        <asp:BoundField DataField="BalanceQty" HeaderText="Q.O.H"></asp:BoundField>
                                                        <asp:BoundField DataField="InwardQty" HeaderText="Inward Qty"></asp:BoundField>
                                                        <asp:BoundField DataField="SoldQty" HeaderText="SoldQty"></asp:BoundField>
                                                        <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                        <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div id="dialog-PRReq" title="Purchase Return Request" style="display: none">
                                            <div class="grid-popup">
                                                <asp:GridView ID="gvPRReq" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true"
                                                    CssClass="pshro_GridDgn">
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                    </EmptyDataTemplate>
                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                    <PagerStyle CssClass="pshro_text" />
                                                    <Columns>
                                                        <asp:BoundField DataField="PRReqNo" HeaderText="PR Req No"></asp:BoundField>
                                                        <asp:BoundField DataField="ReqDate" HeaderText="Req Date"></asp:BoundField>
                                                        <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
                                                        <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                        <asp:BoundField DataField="PRReqQty" HeaderText="PR Req Qty"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Sales">
                                        <div class="Barcode_fixed_headers saleshistory" id="Saleshistory_id">
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesMonth" runat="server" Checked="true" Text="Month" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesWeek" runat="server" Text="Week" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <div class="saleshistory_inv">
                                            <asp:RadioButton ID="rbnSalesDay" runat="server" Text="Day" GroupName="sales" onchange="fncSaleshistorycheckchanged();" />
                                        </div>
                                        <%-- <div id="dialog-ViewItemHistory" class="container-center" title="Item History">
                                            <ups:ItemHistory runat="server" ID="itemHistory" />  OnSelectedIndexChanged="ddlAttribute_SelectedIndexChanged"
                                        </div>--%>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Attributes">
                                        <div class="container">
                                            <div class="col-md-2" style="float: left;">
                                                <asp:DropDownList ID="ddlAttribute" Width="150px" runat="server" CssClass="form-control-res">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <%--<table id="grdAttributeValue" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" class="hiddencol">ValueCode
                                                            </th>
                                                            <th scope="col">ValueNa 
                                                            </th>
                                                            <th scope="col">Select
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <%-- <asp:Repeater ID="rptrAttrValue" runat="server">
                                                        <HeaderTemplate>
                                                            <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr id="childRow">
                                                                <td class="hiddencol">
                                                                    <asp:Label ID="lblValueCode" runat="server" Text='<%# Eval("ValueCode") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblValueName" runat="server" Text='<%# Eval("ValueName") %>' />
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" name="check-tab1">
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                        </FooterTemplate>
                                                    </asp:Repeater>--%>
                                                <%-- <tfoot>
                                                    </tfoot>
                                                </table>--%>
                                                <div class="col-md-12 display_none" id="divSearchAttribute">
                                                    <input type="text" id="search" placeholder="search"></input>
                                                </div>
                                                <div class="col-md-12">
                                                    <table id="grdAttributeValuehead" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" class="hiddencol">ValueCode
                                                                </th>
                                                                <th scope="col" style="width: 55% !important">ValueName
                                                                </th>
                                                                <th scope="col">Select
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="col-md-12" style="overflow-y: visible; overflow-x: hidden; height: 150px">
                                                    <table id="grdAttributeValue" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                            </tr>
                                                        </thead>

                                                        <%-- <asp:Repeater ID="rptrAttrValue" runat="server">
                                                        <HeaderTemplate>
                                                            <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr id="childRow">
                                                                <td class="hiddencol">
                                                                    <asp:Label ID="lblValueCode" runat="server" Text='<%# Eval("ValueCode") %>' />
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblValueName" runat="server" Text='<%# Eval("ValueName") %>' />
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" name="check-tab1">
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                        </FooterTemplate>
                                                    </asp:Repeater>--%>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>

                                                </div>
                                            </div>



                                            <%--<div class="tab tab-btn">--%>
                                            <div class="col-md-2">
                                                <div class="tab tab-btn">
                                                    <button type="button" onclick="tab1_To_tab2();">>>>>></button>
                                                    <button type="button" style="margin-top: 15px;" onclick="tab2_To_tab1();"><<<<<</button>
                                                    <button type="button" id="btnClear" style="margin-top: 15px;" class="display_none" onclick="clearAttributetbl();">Clear </button>

                                                    <%-- <button type="button" onclick="LoadModel();">Model</button>--%>
                                                </div>
                                            </div>

                                            <%--<div class="tab">--%>
                                            <div class="col-md-3">
                                                <table id="grdAttribute" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" class="hiddencol">SNo
                                                            </th>
                                                            <th scope="col" class="hiddencol">AttributeCode
                                                            </th>
                                                            <th scope="col">AttributeName
                                                            </th>
                                                            <th scope="col" class="hiddencol">ValueCode
                                                            </th>
                                                            <th scope="col">ValueName
                                                            </th>
                                                            <th scope="col">Select
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="tab tab-btn">
                                                    <button type="button" onclick="LoadModel('show');">Model View</button>
                                                </div>
                                            </div>
                                            <div id="divAttrCount" class="col-md-6" style="display: none;">
                                                <table id="grdAttributeCount" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col" class="hiddencol">SNo
                                                            </th>
                                                            <th scope="col" class="hiddencol">AttributeCode
                                                            </th>
                                                            <th scope="col">AttributeName
                                                            </th>
                                                            <th scope="col">Count
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div id="divModelView" class="col-md-6" title="Attribute Name" style="display: none;">
                                                <table id="grdModel" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">ItemName
                                                            </th>
                                                            <th scope="col">Select
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div class="col-md-6" id="divAttriOriginal" style="display: none;">
                                                <table id="grdAttributeOriginal" cellspacing="0" rules="all" border="1">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">SNo
                                                            </th>
                                                            <th scope="col">AttributeCode
                                                            </th>
                                                            <th scope="col">ValueCode
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Image">
                                        <div class="control-group" style="margin: 0 auto">
                                            <asp:Image ID="imgpreview" runat="server" Height="130px" Width="250px" />
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSave" />
                                                </Triggers>
                                                <ContentTemplate>
                                                    <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="Promotion">
                                        <div class="promotiongrid-header">
                                            Promotion Details
                                        </div>
                                        <div style="height: 35px">
                                            <table rules="all" border="1" id="Table2" runat="server" class=" pshro_GridDgnHeaderCellCenter">
                                                <%--style="font-family: Arial; font-size: 10pt; width: 600px; color: black; border-collapse: collapse; height: 100%;"--%>
                                                <tr>
                                                    <td style="text-align: center; width: 115px">Promotion Code</td>
                                                    <td style="text-align: center; width: 128px">Promotion Desc</td>
                                                    <td style="text-align: center; width: 80px">Batch No</td>
                                                    <td style="text-align: center; width: 115px">Location</td>
                                                    <td style="text-align: center; width: 115px">Qty</td>
                                                    <td style="text-align: center; width: 115px">Sold Qty</td>
                                                    <td style="text-align: center; width: 115px">Initial Qty</td>
                                                    <td style="text-align: center; width: 115px">Promotion From Date</td>
                                                    <td style="text-align: center; width: 115px">Promotion To Date</td>
                                                    <td style="text-align: center; width: 115px">Discount Perc</td>
                                                    <td style="text-align: center; width: 115px">Create User</td>
                                                    <td style="text-align: center; width: 115px">Create Date</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="promotiongrid">
                                            <asp:GridView ID="gvPromotionGrid" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionCode" HeaderText="Promotion Code"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="130px" ItemStyle-CssClass="text-left" DataField="PromotionDescription" HeaderText="Promotion Desc"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="80px" ItemStyle-CssClass="text-left" DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="Location" HeaderText="Location"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="Qty" HeaderText="Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="SoldQty" HeaderText="Sold Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="InitialQty" HeaderText="Initial Qty"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionFromDate" HeaderText="Promotion From Date"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="PromotionToDate" HeaderText="Promotion To Date"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-right" DataField="DiscountPerc" HeaderText="Discount Perc"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="CreateUser" HeaderText="Create User"></asp:BoundField>
                                                    <asp:BoundField ItemStyle-Width="115px" ItemStyle-CssClass="text-left" DataField="CreateDate" HeaderText="Create Date"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="control-container">
                                            <div class="control-button">
                                                <asp:Button ID="btnLiveDetail" runat="server" class="button-blue" Text="Live Detail"
                                                    OnClick="btnLiveDetail_Click" />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <asp:HiddenField ID="TabName" runat="server" />
                            <asp:HiddenField ID="HiddenNewitem" runat="server" ClientIDMode="Static" Value="NO" />
                            <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hidengrdModel" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hidengrdAttribute" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hidengrdAttributeCount" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hidengrdAttributeOriginal" runat="server" ClientIDMode="Static" Value="" />
                            <asp:HiddenField ID="hdStoctTypeVal" runat="server" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updtInvSave" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton ID="lnkEditItemCode" runat="server" class="button-blue display_none" Text="ItemCode"
                    OnClick="lnkEditItemCode_Click"></asp:LinkButton>
                <div>
                    <div id="userdetail" class="float_left">
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Created Date: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblCreateddate"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Created User: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblCreateuser"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Modify Date: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblModifydate"></asp:Label>
                            </div>
                        </div>
                        <div class="Itemhistory_bottom_border">
                            <div class="item_history_bottom">
                                <span>Modify User: </span>
                            </div>
                            <div class="item_history_bottom">
                                <asp:Label runat="server" ID="lblModifyuser"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="Validate();return false;" Text='<%$ Resources:LabelCaption,btnSave %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClick="lnkClear_clik"
                                Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkItemHistory" runat="server" class="button-red" PostBackUrl="~/Inventory/frmInventoryMasterView.aspx"
                                Text="Item List(F8)"></asp:LinkButton>
                        </div>
                        <div class="hiddencol">
                            <asp:Button ID="btnBarcodeclear" runat="server" OnClick="btnBarcodeclear_Click" />
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="hiddencol">
        


        <div id="dialog-DuplicateInventoryName">
            <div>
                <asp:Label ID="lblGidSave" runat="server" Text="This Inventory name already exists.Do you want save duplicate name?"></asp:Label>
            </div>
            <div class="float_right">
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkDupNameYes" runat="server" Text="Yes" class="button-blue-withoutanim" OnClientClick="fncDupNameYesClick();return false;"></asp:LinkButton>
                </div>
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkDupNameNo" runat="server" Text="No" class="button-blue-withoutanim" OnClientClick="fncDupNameNoClick();return false;"></asp:LinkButton>
                </div>
            </div>

        </div>
        <div id="div-BreakPriceQty">
            <div class="breakpricehdr">
                <div class="breakpriceQty_lbl">
                    <asp:Label ID="lblBreakPriceItem" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                </div>
                <div class="breakpriceitem_lbl">
                    <asp:TextBox ID="txtBreakPriceItem" ReadOnly="true" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="breakpriceQty_lbl">
                    <asp:Label ID="lblBreakPriceDesc" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                </div>
                <div class="float_left">
                    <asp:TextBox ID="txtBreakPriceDesc" ReadOnly="true" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div class="breakpriceQty_lbl">
                    <asp:CheckBox ID="chbrkNetCostVal" runat="server" Checked="true" Text='<%$ Resources:LabelCaption,msg_NetCostVal %>' />
                </div>
            </div>
            <div class="Payment_fixed_headers breakPrice">
                <table id="tblBreakPriceQty" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">Delete
                            </th>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">Item Code
                            </th>
                            <th scope="col">Batch No
                            </th>
                            <th scope="col">Break Qty
                            </th>
                            <th scope="col">Break(Price/Perc) 
                            </th>
                            <th scope="col">Price Type
                            </th>
                            <th scope="col">NetCostValidation
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <div class="breakPrice_buttom">
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                    <asp:TextBox ID="txtBreakPriceBatchNo" ReadOnly="true" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_BreakQty %>'></asp:Label>
                    <asp:TextBox ID="txtBreakQty" MaxLength="4" onkeydown="return isNumberKeyWithDecimalNewforBreakprice(event);" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_BreakPrice %>'></asp:Label>
                    <asp:TextBox ID="txtBreakPrice" MaxLength="9" onkeydown="return isNumberKeyWithDecimalNewforBreakprice(event);" CssClass="form-control-res-right" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_txt">
                    <asp:Label ID="lblBreakMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    <asp:TextBox ID="txtBreakMRP" ReadOnly="true" CssClass="form-control-res" runat="server"></asp:TextBox>
                </div>
                <div class="breakPrice_ddl">
                    <asp:Label ID="lblPriceType" runat="server" Text='<%$ Resources:LabelCaption,lbl_PriceType %>'></asp:Label>
                    <asp:DropDownList ID="ddlBreakPriceType" runat="server" Style="width: 100%">
                        <asp:ListItem Value="UBP" Text="BreakPrice"> </asp:ListItem>
                        <asp:ListItem Value="UDA" Text="DiscountAmount"> </asp:ListItem>
                        <asp:ListItem Value="UDP" Text="DiscountPercentage"> </asp:ListItem>
                        <asp:ListItem Value="UW1" Text="WPRICE1"> </asp:ListItem>
                        <asp:ListItem Value="UW2" Text="WPRICE2"> </asp:ListItem>
                        <asp:ListItem Value="UW3" Text="WPRICE3"> </asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="breakPrice_Button">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkBreakPriceAdd" runat="server" class="button-blue-withoutanim"
                            Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick="fncAddBreakPriceQty();return false;"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkBreakPriceClear" runat="server" class="button-blue-withoutanim"
                            OnClientClick="fncClearBreakPriceQty();return false;" Text='<%$ Resources:LabelCaption,lnkClear %>'></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnInventorySave" runat="server" OnClick="btnInventorySave_Click" />
        <asp:HiddenField ID="hidinventoryStatus" runat="server" Value="New" />
        <asp:HiddenField ID="hidPriceEntered" runat="server" Value="0" />
        <asp:HiddenField ID="hidBreakPriceQty" runat="server" />
        <asp:HiddenField ID="hidbrkNetCost" runat="server" />
        <asp:HiddenField ID="hidMultipleVendor" runat="server" />
        <asp:HiddenField ID="hidShortcutKey" runat="server" />
        <asp:HiddenField ID="hidVendorName" runat="server" />
        <asp:HiddenField ID="hidSaleshistory" runat="server" />
        <asp:HiddenField ID="hidchildItem" runat="server" />
        <asp:HiddenField ID="hidInvPriceDetail" runat="server" />
        <asp:HiddenField ID="hidDailogName" runat="server" />
        <asp:HiddenField ID="hidInvStatus" runat="server" />
        <asp:HiddenField ID="hidDeletebarcode" runat="server" />
        <asp:HiddenField ID="hidInvOpenStatus" runat="server" />
        <asp:HiddenField ID="hidItemname" runat="server" />
        <asp:HiddenField ID="hidSaveStatus" runat="server" Value="0" />
        <asp:HiddenField ID="hidWpriceGreaterSP" runat="server" Value="N" />
        <asp:HiddenField ID="hidLanguageChange" runat="server" Value="N" />
        <asp:HiddenField ID="hdStoctType" runat="server" />
        <asp:UpdatePanel ID="upSave" runat="Server">
            <ContentTemplate>
                <asp:HiddenField ID="hidbarcode" runat="server" />
                <asp:Button ID="btnSave" runat="server" Text="Button" OnClick="lnkSave_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="delete-BreakPriceQty" style="display: none">
    </div>
    <div id="delete-BreakPriceQtySuccess" style="display: none">
    </div>
    <div id="NewItemSave" class="display_none">
        <div>
            <asp:Label ID="lblNewItem" runat="server" Text="Inventory Saved Successfully . Do You want to continue with existing values ?"></asp:Label>
        </div>
        <div class="float_right">
            <div class="inv_dailog_btn">
                <asp:LinkButton ID="lnkNewItemYes" runat="server" Text="Yes" class="button-blue-withoutanim" OnClientClick="fncNewItemYesClick();return false;"></asp:LinkButton>
            </div>
            <div class="inv_dailog_btn">
                <asp:LinkButton ID="lnkNewItemNo" runat="server" Text="No" class="button-blue-withoutanim" OnClientClick="fncNewItemNoClick();return false;"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div id="ItemEdit" class="display_none">
    </div>
    <div id="DeleteMultiplevendor" class="display_none">
    </div>

    <div id="dialog-MultipleVendorCreation" style="display: none">
        <div class="Payment_fixed_headers newvendor">
            <table id="tblNewVendorCreation" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">Vendor Code
                        </th>
                        <th scope="col">Vendor Name
                        </th>
                        <th scope="col">Delete
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="multiple_vendor">
            <div class="multiple_vendorlbl">
                <asp:Label ID="lblNewvendorcode" runat="server" Text="Vendor Code"></asp:Label>
            </div>
            <div class="multiple_vendorlbl">
                <asp:TextBox ID="txtNewVendorcode" runat="server" onfocus="fncToEnableShortcutKey('NewVendor');" onkeydown="return fncShowVendorSearchDialogMultiVendor(event,'NewVendor');" class="form-control-res"></asp:TextBox>
            </div>
        </div>

        <div class="display_none">
            <%--Vijay 20190214 Samrat--%>
            <asp:HiddenField ID="hidAttributeOrder" runat="server" />
            <input type="button" id="SelectAll" />
        </div>
    </div>

</asp:Content>
