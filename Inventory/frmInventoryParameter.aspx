﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmInventoryParameter.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryParameter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 350px;
            max-width: 350px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 161px;
            max-width: 161px;
        }

        .selectedCell {
            background-color: Green;
        }

        .unselectedCell {
            background-color: white;
        }

        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: grab;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .selected_row {
            background-color: #A1DCF2;
        }

        .editableTable {
            border: solid 1px;
            width: 100%;
        }

            .editableTable td {
                border: solid 1px;
            }

            .editableTable .cellEditing {
                padding: 0;
            }

                .editableTable .cellEditing input[type=text] {
                    width: 100%;
                    border: 0;
                    background-color: rgb(255,253,210);
                }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }
                if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                    $('#<%=lnkDelete.ClientID %>').css("display", "block");
                  }
                  else {
                      $('#<%=lnkDelete.ClientID %>').css("display", "none");
                  }
              }
              catch (err) {
                  ShowPopupMessageBox(err.message);
              }
          }
          $(function () {
              SetAutoComplete();
          });


          function reloadPage() {
              window.location.reload()
          }


          function isNumberKey(evt) {

              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                  return false;

              return true;
          }

          $(function () {

              $("[id*=grdBulkItem]").on('click', 'td', function (e) {
                  DisplayChildDetails($(this).closest("tr"));
                  e.preventDefault();
              });

          });

          $(function () {
              try {
                  $("[id*=grdBulkItem]").on('click', 'td > img', function (e) {

                      var r = confirm("Do you want to Delete?");
                      if (r === false) {
                          return false;
                      }
                      var rowCount = $("[id*=grdBulkItem] tr:last").index() + 1;
                      if (rowCount == 2) {
                          $("[id*=grdBulkItem] tr:last").after('<tr class="pshro_GridDgnStyle"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><img src="../images/delete.png" /></td></tr>');
                      }
                      $(this).closest("tr").remove();
                      $('#<%=txtdescription.ClientID %>').focus().select();
                });
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        });

        //        $(function () {
        //            $("#<%=grdBulkItem.ClientID%> tr:has(td)").click(function () {
        //                alert('delete');
        //                $(this).fadeOut(1000, function () {
        //                    $(this).remove();
        //                });
        //            });
        //        });

        function DisplayChildDetails(row) {
            try {
                //alert("child"); 
                $("[id*=txtInventoryCode]").val($("td", row).eq(0).html());
                $("[id*=txtdescription]").val($("td", row).eq(1).html());
                $("[id*=txtBWQuantity]").val($("td", row).eq(2).html());
                $("[id*=txtBWCost]").val($("td", row).eq(3).html());
                $("[id*=txtWastageQty]").val($("td", row).eq(4).html());
                $("[id*=txtAvailableQty]").val($("td", row).eq(5).html());
                $("[id*=txtAvailableCost]").val($("td", row).eq(6).html());
            }
            catch (err) {
                alert(err.Message);
                console.log(err);
            }
        }

        //Get Calc Bulk Qty
        function fncCalcBulkQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $('#<%=txtWastageQty.ClientID %>').focus().select();
                    fncCalc();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }

        //Get Calc Wastage Qty
        function fncCalcWastageQty(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $('#<%=lnkAdd.ClientID %>').focus().select();
                    fncCalc();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }

        function fncValidateSave() {
            try {

                //alert($("#HiddenXmldata").val());
                if ($("#HiddenXmldata").val() == '') {

                    ShowPopupMessageBox("Please add Any Bulk item to Proceed!");
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }

        function fncCalc() {
            try {
                var BWQty = $('#<%=txtBWQuantity.ClientID%>').val();

                var BWCost = $('#<%=txtBWCost.ClientID%>').val();
                var WastageQty = $('#<%=txtWastageQty.ClientID%>').val();
                var AWQty = 0;
                var AWCost = 0;

                if (BWQty > 0 && BWCost > 0) {
                    AWQty = BWQty - WastageQty;
                    AWCost = BWCost * AWQty;

                    $('#<%=txtAvailableQty.ClientID%>').val(AWQty.toFixed(2));
                    $('#<%=txtAvailableCost.ClientID%>').val(AWCost.toFixed(2));
                }

            }
            catch (err) {
                alert(err.Message);
            }

        }

        function AddbuttonClick() {
            try {

                if ($('#<%=txtInventoryCode.ClientID %>').val() == '') {

                    ShowPopupMessageBoxandFocustoObject("Sorry InventoryCode Can't be Empty");
                    popUpObjectForSetFocusandOpen = $('#<%=txtdescription.ClientID %>');
                    //$('#<%=txtInventoryCode.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtBWQuantity.ClientID %>').val() == '' || $('#<%=txtBWQuantity.ClientID %>').val() == 0) {

                    ShowPopupMessageBoxandFocustoObject("Sorry BW.Qty Can't be Zero");
                    popUpObjectForSetFocusandOpen = $('#<%=txtBWQuantity.ClientID %>');
                    //$('#<%=txtBWQuantity.ClientID %>').focus().select();
                    return false;
                }
                if ($('#<%=txtBWCost.ClientID %>').val() == '') {

                    ShowPopupMessageBoxandFocustoObject("Sorry BW.Cost Can't be Zero");
                    popUpObjectForSetFocusandOpen = $('#<%=txtBWCost.ClientID %>');
                    //$('#<%=txtBWCost.ClientID %>').focus().select();
                    return false;
                }

                var Qtyonhand = $("#HiddenQtyonHand").val();
                var BWQuantity = $('#<%=txtBWQuantity.ClientID %>').val();


                if (Qtyonhand < BWQuantity) {

                    ShowPopupMessageBoxandFocustoObject('QtyonHand < BWQty, QtyonHand = ' + Qtyonhand + ' Please Purchase Stock');
                    popUpObjectForSetFocusandOpen = $('#<%=txtBWQuantity.ClientID %>');
                    //$('#<%=txtBWQuantity.ClientID %>').focus().select();
                    return false;
                }

                var txtitem = $('#<%=txtInventoryCode.ClientID %>').val();
                fncCheckItemExist(txtitem);

                return false;
            }
            catch (err) {
                alert(err.message);
                console.log(err);
                return false;
            }

        }

        //        txtAvailableCost
        //        txtAvailableQty
        //        txtWastageQty
        //        txtBWCost
        //        txtBWQuantity
        //        txtdescription
        //        txtInventoryCode

        function deleterow() {
            try {
                var rowCount = $("[id*=grdBulkItem] tr:last").index() + 1;
                //alert(rowCount);
                if (rowCount == 2) {
                    $("[id*=grdBulkItem] tr:last").after('<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');
                }
                var invcode = $('#<%=txtInventoryCode.ClientID %>').val();
                $("[id*=grdBulkItem]").find("tr:contains('" + invcode + "')").remove();

                fncDeleteItem(invcode);

                ShowPopupMessageBoxandFocustoObject('Item Deleted..!')
                popUpObjectForSetFocusandOpen = $('#<%=txtdescription.ClientID %>');
                $("[id*=txtInventoryCode]").val('');
                $("[id*=txtdescription]").val('');
                $("[id*=txtBWQuantity]").val(0);
                $("[id*=txtBWCost]").val(0);
                $("[id*=txtWastageQty]").val(0);
                $("[id*=txtAvailableQty]").val(0);
                $("[id*=txtAvailableCost]").val(0);

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function AddRow() {
            //debugger;
            try {
                var InventoryCode = $("[id*=txtInventoryCode]").val();
                var Description = $("[id*=txtdescription]").val();
                var BWQty = $("[id*=txtBWQuantity]").val();
                var BWCost = $("[id*=txtBWCost]").val();
                var WastageQty = $("[id*=txtWastageQty]").val();
                var AvailableQty = $("[id*=txtAvailableQty]").val();
                var AvailableCost = $("[id*=txtAvailableCost]").val();

                if (InventoryCode != "" && BWQty != "" && BWCost != "") {
                    var row = $("[id*=grdBulkItem] tr:last").clone();
                    $("td:nth-child(1)", row).html(InventoryCode);
                    $("td:nth-child(2)", row).html(Description);
                    $("td:nth-child(3)", row).html(BWQty);
                    $("td:nth-child(4)", row).html(BWCost);
                    $("td:nth-child(5)", row).html(WastageQty);
                    $("td:nth-child(6)", row).html(AvailableQty);
                    $("td:nth-child(7)", row).html(AvailableCost);
                    //$("td:nth-child(8)", row).html('<img src="../images/delete.png" />');
                    $("[id*=grdBulkItem] tbody").append(row);
                }
                $("[id*=txtInventoryCode]").val();
                $("[id*=txtdescription]").val();
                $("[id*=txtBWQuantity]").val();
                $("[id*=txtBWCost]").val();
                $("[id*=txtWastageQty]").val();
                $("[id*=txtAvailableQty]").val();
                $("[id*=txtAvailableCost]").val();

                $("[id*=grdBulkItem] tr").each(function () {
                    if (!$.trim($(this).text())) $(this).remove();
                });
                XmlGridValue();
                return false;

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function XmlGridValue() {

            try {

                //$("#HiddenNewitem").val('Y');
                var xml = '<NewDataSet>';
                $("#<%=grdBulkItem.ClientID %> tr").each(function () {
                    var cells = $("td", this);
                    if (cells.length > 0) {
                        //alert(cells.eq(0).text().trim());
                        if (cells.eq(0).text().trim() != '') {
                            xml += "<Table>";
                            xml += "<Inventorycode>" + cells.eq(0).text().trim() + "</Inventorycode>";
                            xml += "<Description>" + cells.eq(1).text().trim() + "</Description>";
                            xml += "<BWQuantity>" + cells.eq(2).text().trim() + "</BWQuantity>";
                            xml += "<BWCost>" + cells.eq(3).text().trim() + "</BWCost>";
                            xml += "<WastageQty>" + cells.eq(4).text().trim() + "</WastageQty>";
                            xml += "<AvailableQty>" + cells.eq(5).text().trim() + "</AvailableQty>";
                            xml += "<AvailableCost>" + cells.eq(6).text().trim() + "</AvailableCost>";
                            xml += "</Table>";
                        }
                    }
                });

                xml = xml + '</NewDataSet>'
                //alert(xml);
                $("#HiddenXmldata").val(escape(xml));
                //alert($("#HiddenXmldata").val());

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function ClearTextBox() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $('#<%=txtdescription.ClientID %>').focus().select();
            return false;
        }


        function SetAutoComplete() {

            $("[id$=txtdescription]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryParameter.aspx/GetBulkDetails")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1],
                                    val2: item.split('|')[2],
                                    val3: item.split('|')[3],
                                    val4: item.split('|')[4]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    //alert(i.item.val);
                    $('#<%=txtInventoryCode.ClientID %>').val($.trim(i.item.val));
                    $('#<%=txtdescription.ClientID %>').val($.trim(i.item.val2));
                    $('#<%=txtBWCost.ClientID %>').val($.trim(i.item.val4));
                    $("#HiddenQtyonHand").val($.trim(i.item.val3));

                    $("[id*=txtWastageQty]").val(0);
                    $("[id*=txtAvailableQty]").val(0);
                    $("[id*=txtAvailableCost]").val(0);

                    //$('#<%=txtInventoryCode.ClientID %>').prop('disabled', true);
                    $('#<%=txtBWQuantity.ClientID %>').focus().select();
                    $("#hidenChildCode").val($.trim(i.item.val));


                    return false;
                },
                minLength: 2
            });
        }

        function Clearall() {
            //debugger;
            $('input[type="text"]').val('');
            $("#hidenExists").val('');
            $("#HiddenQtyonHand").val(0);
            $("#HiddenXmldata").val('');

            return false;
        }

        function fncDeleteItem(sInvCode) {
            //debugger;
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryParameter.aspx/CheckItemExist")%>',
                //data: "{'Itemcode':'" + sInvCode + "'}",
                data: "{'Itemcode':'" + sInvCode + "', 'Mode':'" + "Delete" + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert(response.d);
                    //if (response.d == '')
                    //    ShowPopupMessageBox('Item Deleted..!')
                },
                failure: function (response) {
                    ShowPopupMessageBox(response.d);
                },
                error: function (response) {
                    ShowPopupMessageBox(response.d);
                }
            });
        }


        function fncCheckItemExist(sInvCode) {
            // debugger;
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Inventory/frmInventoryParameter.aspx/CheckItemExist")%>',
                //data: "{'Itemcode':'" + sInvCode + "'}",
                data: "{'Itemcode':'" + sInvCode + "', 'Mode':'" + "Check" + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }



        function OnSuccess(response) {
            try {

                //debugger; 
                //alert(typeof response.d);
                var check = response.d;

                var checked = false;
                var txtitem = $('#<%=txtInventoryCode.ClientID %>').val();
                //alert(check);
                if (check != '') {
                    var r = confirm("Already have one Pending, Updating the Qty?");
                    if (r === false) {
                        return false;
                    }
                }

                $("#<%=grdBulkItem.ClientID%> tr").each(function () {
                    if (!this.rowIndex) return;
                    var gitem = $(this).find("td.Itemcode").html();
                    if ($.trim(gitem) == $.trim($('#<%=txtInventoryCode.ClientID %>').val())) {
                        ShowPopupMessageBox("Item already Exists !");
                        checked = true;
                    }
                });


                var ResultArray = [];
                $('#<%=grdBulkItem.ClientID %>').find('input:hidden').each(function () {
                    ResultArray.push($(this).val());
                    //alert($(this).val());
                });

                if ($.inArray(txtitem, ResultArray) != -1) {
                    // found it
                    //alert(ResultArray)
                    ShowPopupMessageBox("Item already Exists !");
                    checked = true;
                }

                if (checked == false)
                    AddRow();

                ClearTextBox();
            }
            catch (err) {
                console.log(err);
                alert(err.Message);
            }
        }

    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'InventoryParameter');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "InventoryParameter";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" id="form1">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Bulk Parameter</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-pc1">
            <div class="container-horiz-top-header1">
                <div class="right-container-top-header">
                    Bulk Items Parameter
                </div>
                <div class="right-container-top-detail">
                    <div class="GridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Inventorycode
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">BWqty
                                                </th>
                                                <th scope="col">BWcost
                                                </th>
                                                <th scope="col">WQty
                                                </th>
                                                <th scope="col">AWQty
                                                </th>
                                                <th scope="col">AWCost
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 280px; width: 1330px; background-color: aliceblue;">
                                        <%--<div class="grid-overflow-height grdLoad" id="HideFilter_GridOverFlow" runat="server">--%>
                                        <asp:GridView ID="grdBulkItem" runat="server" AutoGenerateColumns="false" PageSize="14"
                                            ShowHeaderWhenEmpty="True" ShowHeader="false" CssClass="pshro_GridDgnStyle tbl_left">
                                            <PagerStyle CssClass="pshro_text" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <Columns>
                                                <asp:BoundField ItemStyle-CssClass="Itemcode" HeaderText="Inventorycode" DataField="Inventorycode"></asp:BoundField>
                                                <asp:BoundField HeaderText="Description" DataField="Description"></asp:BoundField>
                                                <asp:BoundField HeaderText="BWqty" DataField="BWqty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="BWcost" DataField="BWcost" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="WQty" DataField="WQty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="AWQty" DataField="AWQty" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <asp:BoundField HeaderText="AWCost" DataField="AWCost" ItemStyle-CssClass="right_align"></asp:BoundField>
                                                <%--<asp:BoundField ItemStyle-CssClass="Delete" HeaderText="Delete" DataField="Delete"></asp:BoundField>--%>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                PageButtonCount="5" Position="Bottom" />
                                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            <RowStyle CssClass="" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-horiz-top-header1">
                    <div class="right-container-bottom-header">
                    </div>
                    <div class="right-container-bottom-detail">
                        <div class="control-group-split">
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lblitemcode %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtInventoryCode" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="control-group-left-Description-sm">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtdescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lbl_BWqty %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtBWQuantity" runat="server" Text="0" onkeydown="return fncCalcBulkQty(event)"
                                        onkeypress="return isNumberKey(event)" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_BWcost %>'></asp:Label>
                                </div>
                                <div class="label-right-small">
                                    <asp:TextBox ID="txtBWCost" runat="server" CssClass="form-control-res" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_Wastage %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtWastageQty" runat="server" Text="0" onkeydown="return fncCalcWastageQty(event)"
                                        onkeypress="return isNumberKey(event)" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_AWQty %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtAvailableQty" runat="server" Text="0" onkeypress="return isNumberKey(event)"
                                        CssClass="form-control-res" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left-small">
                                <div class="label-left-pc">
                                    <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_AWCostKg %>'></asp:Label>
                                </div>
                                <div class="label-right-pc">
                                    <asp:TextBox ID="txtAvailableCost" runat="server" Text="0" CssClass="form-control-res"
                                        Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-container">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkAdd" runat="server" CssClass="button-red" OnClientClick="return AddbuttonClick()"
                                Text='<%$ Resources:LabelCaption,btnadd %>'> </asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" CssClass="button-red" OnClientClick="return ClearTextBox()"
                                Text='<%$ Resources:LabelCaption,btnclear %>'> Clear </asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button-red" OnClientClick="return deleterow()"
                                Text='<%$ Resources:LabelCaption,btn_delete %>'> Delete </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <div class="control-button">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearform" runat="server" CssClass="button-red" OnClientClick="return reloadPage()"
                                    Text='<%$ Resources:LabelCaption,btn_refresh %>'> </asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" CssClass="button-red" OnClientClick="return fncValidateSave()"
                                    Text='<%$ Resources:LabelCaption,btnSave %>' OnClick="lnkSave_Click">  </asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:HiddenField ID="hidenChildCode" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hidenExists" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenQtyonHand" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="HiddenXmldata" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
