﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmBulkBarcode.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmBulkBarcode" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1)
        {
            min-width: 30px;
            max-width: 30px;
        }
        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2)
        {
            min-width: 40px;
            max-width: 40px;
        }
        
        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3)
        {
            min-width: 80px;
            max-width: 80px;
        }
        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4)
        {
            min-width: 180px;
            max-width: 180px;
        }
        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5)
        {
            min-width: 60px;
            max-width: 60px;
        }
        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6)
        {
            min-width: 60px;
            max-width: 60px;
        }
        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7)
        {
            min-width: 70px;
            max-width: 70px;
        }
        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8)
        {
            min-width: 70px;
            max-width: 70px;
        }
        
        .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9)
        {
            min-width: 120px;
            max-width: 120px;
        }
        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10)
        {
            min-width: 120px;
            max-width: 120px;
        }
        .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13)
        {
            display: none;
        }
        .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14)
        {
            display: none;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            $("#<%= txtPKDDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            fncPagingShowHide();
            $("#<%= txtPKDDate.ClientID %>").attr('readonly', true);

        }
        function controlEnter(obj, event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 13) {
                document.getElementById(obj).focus();
                return false;
            }
            else {
                return true;
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        //Best fore lable change
        function fncChangeBestbeforelabletext() {
            try {

                if ($('#<%=rbnDate.ClientID %>').is(':checked')) {
                    $('#<%=lblBestBefore.ClientID %>').text('<%=Resources.LabelCaption.lbl_BestBeforeDays%>');
                }
                else {
                    $('#<%=lblBestBefore.ClientID %>').text('<%=Resources.LabelCaption.lbl_BestBeforeMonths%>');
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Enable Supplier Text Box
        function fncEnablesupplier() {
            try {
                if ($('#<%=rbnSupplier.ClientID %>').is(':checked')) {
                    $('#<%=txtSupp.ClientID %>').prop('disabled', false);
                    $('#<%=txtSupp.ClientID %>').focus();
                }
            }
            catch (err) {
                alert(err.message)
            }
        }
        //Assign Cross value to text box
        function fncTemplateChange() {
            try {
                $('#<%=txtSize.ClientID %>').val($('#<%=ddlTemplate.ClientID %>').val());
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Clear All Control
        function fncClear() {
            try {
                $('#<%=txtQty.ClientID %>').val('0');
                $('#<%=txtBest.ClientID %>').val('0');
                $('#<%=txtBest.ClientID %>').val('0');
                $('#<%=txtSize.ClientID %>').val('');
                $('#<%=rbnSellingPrice.ClientID %>').attr("checked", "checked");
                $('#<%=rbnDate.ClientID %>').attr("checked", "checked");
                $('#<%=chkBatchNon.ClientID %>').removeAttr("checked");

                $("#tblbulkbarcode [id*=cbheader]").removeAttr("checked");
                $("#tblbulkbarcode [id*=BulkBarcoderow]").remove();

                $('#<%=ddlTemplate.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlTemplate.ClientID %>').trigger("liszt:updated");

                fncFilterClear();
                return false;
            }
            catch (err) {
                alert(err.message)
                return false;
            }
        }
        //Filter Clear()
        function fncFilterClear() {
            try {
                $('#<%=ddlVendor.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlDept.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlCategory.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlBrand.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlMerchandise.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlManufacture.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlFloor.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlSection.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlBin.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlShelf.ClientID %>')[0].selectedIndex = 0;
                $('#<%=ddlWarehouse.ClientID %>')[0].selectedIndex = 0;

                $('#<%=ddlVendor.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlDept.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlCategory.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlBrand.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlMerchandise.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlManufacture.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlFloor.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlSection.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlBin.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlShelf.ClientID %>').trigger("liszt:updated");
                $('#<%=ddlWarehouse.ClientID %>').trigger("liszt:updated");

                $('#<%=txtItemCode.ClientID %>').val('');
                $('#<%=txtItemName.ClientID %>').val('');

                return false;
            }
            catch (err) {
                alert(err.message);
                return false;
            }
        }
        //Header Row Click
        function fncrptrHeaderclick(source) {
            try {
                if (($(source).is(":checked"))) {
                    $("#tblbulkbarcode [id*=cbrptrrow]").attr("checked", "checked");
                }
                else {
                    $("#tblbulkbarcode [id*=cbrptrrow]").removeAttr("checked");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Repeater Row Click
        function fncrptrRowClick(source) {
            try {
                if ($("#tblbulkbarcode [id*=cbrptrrow]").length == $("#tblbulkbarcode [id*=cbrptrrow]:checked").length) {
                    $("#tblbulkbarcode [id*=cbheader]").attr("checked", "checked");

                }
                else {
                    $("#tblbulkbarcode [id*=cbheader]").removeAttr("checked");
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //grid Qty Focus out
        function fncQtyFocusOut(source) {
            try {
                var rowobj = $(source).parent().parent();
                if (rowobj.find('td input[id*="txtQty"]').val() == "" || parseInt(rowobj.find('td input[id*="txtQty"]').val()) == 0)
                    rowobj.find('td input[id*="txtQty"]').val('1');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncRatingfocusout(source) {
            try {
                var rowobj = $(source).parent().parent();
                if (rowobj.find('td input[id*="txtRating"]').val() == "")
                    rowobj.find('td input[id*="txtRating"]').val('0');
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowSuccessMessage() {
            try {
                InitializeDialog();
                $("#barcodeprinting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#barcodeprinting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 130,
                    width: 300,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Close Success Dialog
        function fncCloseSaveDialog() {
            try {
                $("#barcodeprinting").dialog('close');
                fncClear();
                return false;
            }
            catch (err) {
                alert(err.message);
                return false;
            }
        }
        //Validation Check before Print
        function fncPrintvalidation() {
            try {
                if ($('#<%=txtSize.ClientID %>').val() == "") {
                    alert('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
                    return false;
                }
                else if ($("#tblbulkbarcode [id*=BulkBarcoderow]").length == 0) {
                    alert('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false;
                }
                else if ($("#tblbulkbarcode [id*=cbrptrrow]:checked").length == 0) {
                    alert('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                    return false;
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Pagination Show and Hide
        function fncPagingShowHide() {
            try {
                //                alert('fncPagingShowHide');
                if ($("#spanpagestatus [id*=lblPageStatus]").html() != "Page 1 of 0" && $("#spanpagestatus [id*=lblPageStatus]").html() != "Page 1 of 1"
                && $("#tblbulkbarcode [id*=BulkBarcoderow]").length > 0) {
                    $('#rptpagination').show();
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source, id) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var rowobj = $(source).parent().parent();
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        if (id == "Qty") {
                            NextRowobj.find('td input[id*="txtQty"]').focus();
                            NextRowobj.find('td input[id*="txtQty"]').select();
                        }
                        else {
                            NextRowobj.find('td input[id*="txtRating"]').focus();
                            NextRowobj.find('td input[id*="txtRating"]').select();
                        }
                            
                    }
                    else {
                        if (id == "Qty") {
                            rowobj.siblings().first().find('td input[id*="txtQty"]').focus();
                            rowobj.siblings().first().find('td input[id*="txtQty"]').select();
                        }                            
                        else {
                            rowobj.siblings().first().find('td input[id*="txtRating"]').focus();
                            rowobj.siblings().first().find('td input[id*="txtRating"]').select();
                        }
                            
                    }
                }
                else if (charCode == 38) {
                    var rowobj = $(source).parent().parent();
                    var prevrowobj = rowobj.prev();

                    if (prevrowobj.length > 0) {
                        if (id == "Qty") {
                            prevrowobj.find('td input[id*="txtQty"]').focus();
                            prevrowobj.find('td input[id*="txtQty"]').select();
                        }                            
                        else {
                            prevrowobj.find('td input[id*="txtRating"]').focus();
                            prevrowobj.find('td input[id*="txtRating"]').select();
                        }
                            
                    }
                    else {
                        if (id == "Qty") {
                            rowobj.siblings().last().find('td input[id*="txtQty"]').focus();
                            rowobj.siblings().last().find('td input[id*="txtQty"]').select();
                        }
                        else {
                            rowobj.siblings().last().find('td input[id*="txtRating"]').focus();
                            rowobj.siblings().last().find('td input[id*="txtRating"]').select();
                        }                            
                    }
                }
            }
            catch (err) {
                alert(err.message);
            }
        }

        //Set Focus to Next Row of Qtytextbox
        function fncSetFocustoNextQty(evt, source) {
            fncSetFocustoNextRow(evt, source, "Qty")
        }
        //Set Focus to Next Row of Ratingtextbox
        function fncSetFocustoNextRating(evt, source) {
            fncSetFocustoNextRow(evt, source, "Rating")
        }
        //Assign 0 to txtQty and txtbestbefore
        function fnctxtQtyBestFocusOut() {
            if ($('#<%=txtQty.ClientID %>').val() == "")
                $('#<%=txtQty.ClientID %>').val('0');
            else if ($('#<%=txtBest.ClientID %>').val() == "")
                $('#<%=txtBest.ClientID %>').val('0');
        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'BulkBarcode');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "BulkBarcode";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="bulkbarcode_backgroundcolor">
        <div class="container-group-full">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration:none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration:none;">Barcode</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page">Bulk Barcode </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
        </div>
        <div class="container-group-full" style="margin-top: 1%">
            <div class="container3" style="width: 15%">
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblNoOfLabels" runat="server" Text='<%$ Resources:LabelCaption,lbl_NoOfLabels %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Style="width: 80px;
                            text-align: right" onkeypress="return isNumberKey(event)" MaxLength="4" onblur="fnctxtQtyBestFocusOut()">0</asp:TextBox>
                    </div>
                </div>
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblBestBefore" runat="server" Text='<%$ Resources:LabelCaption,lbl_BestBeforeDays %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtBest" runat="server" CssClass="form-control" Style="width: 80px;
                            text-align: right" onkeypress="return isNumberKey(event)" MaxLength="3" onblur="fnctxtQtyBestFocusOut()">0</asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="container3" style="margin-left: 15px; width: 100px">
                <div class="control-group" style="width: 100%">
                    <asp:RadioButton ID="rbnSellingPrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellingPrice %>'
                        GroupName="Price" Checked="True" />
                </div>
                <div class="control-group" style="width: 100px">
                    <asp:RadioButton ID="rbnMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'
                        GroupName="Price" Style="float: left" />
                </div>
            </div>
            <div class="container3">
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblPKDDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_PKDDate %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtPKDDate" runat="server" CssClass="form-control" Style="width: 90px"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:RadioButton ID="rbnSupplier" runat="server" Text='<%$ Resources:LabelCaption,lbl_Supplier %>'
                            onclick="fncEnablesupplier()" />
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtSupp" runat="server" CssClass="form-control" Style="width: 90px"
                            Enabled="False"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="container3" style="margin-left: 15px; width: 100px">
                <div class="control-group" style="width: 100%">
                    <asp:RadioButton ID="rbnDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'
                        GroupName="Date" Checked="True" onclick="fncChangeBestbeforelabletext()" />
                </div>
                <div class="control-group" style="width: 100%">
                    <asp:RadioButton ID="rbnMonth" runat="server" Text='<%$ Resources:LabelCaption,lbl_Month %>'
                        GroupName="Date" onclick="fncChangeBestbeforelabletext()" />
                </div>
            </div>
            <div class="container3" style="width: 11%">
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblHidePrice" runat="server" Text='<%$ Resources:LabelCaption,lbl_HidePrice %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:CheckBox ID="chkHidePrice" runat="server" />
                    </div>
                </div>
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblDate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Data %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:CheckBox ID="chkDate" runat="server" />
                    </div>
                </div>
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblHideCompanyName" runat="server" Text='<%$ Resources:LabelCaption,lbl_HideCompanyName %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:CheckBox ID="chkchkHideCompanyName" runat="server" />
                    </div>
                </div>
            </div>
            <div class="container3" style="margin-left: 15px; width: 12%">
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblTemplate" runat="server" Text='<%$ Resources:LabelCaption,lbl_Template %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="ddlTemplate" runat="server" Style="width: 100%" onchange="fncTemplateChange()">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblSize" runat="server" Text='<%$ Resources:LabelCaption,lbl_Size %>'></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:TextBox ID="txtSize" runat="server" CssClass="form-control" Style="width: 90px"
                            onMouseDown="return false"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="container3" style="margin-left: 15px; width: 5%">
                <div class="control-group" style="width: 100%">
                    <div style="float: left">
                        <asp:Label ID="lblBatch" runat="server" Text="Batch"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:CheckBox ID="chkBatchNon" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lblItemname %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single">
                    <div class="label-left">
                        <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_WareHouse %>'></asp:Label>
                    </div>
                    <div class="label-right1">
                        <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control-res">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-container">
                    <div class="control-button">
                        <asp:UpdatePanel runat="server" ID="upfetch">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkFetch" runat="server" class="button-red" OnClick="lnkFetch_Click"
                                    Text='<%$ Resources:LabelCaption,lbtn_Fetch %>'></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return fncFilterClear()"
                            Text='<%$ Resources:LabelCaption,lnkClear %>'></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <asp:UpdatePanel runat="server" ID="upbulkbarcode" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divrepeater" class="Barcode_fixed_headers">
                            <table id="tblbulkbarcode" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr>
                                        <th scope="col">
                                            <asp:CheckBox ID="cbheader" runat="server" onclick="fncrptrHeaderclick(this)" />
                                        </th>
                                        <th scope="col">
                                            S.No
                                        </th>
                                        <th scope="col">
                                            Itemcode
                                        </th>
                                        <th scope="col">
                                            Description
                                        </th>
                                        <th scope="col">
                                            Qty
                                        </th>
                                        <th scope="col">
                                            Batch No
                                        </th>
                                        <th scope="col">
                                            Selling Price
                                        </th>
                                        <th scope="col">
                                            MRP
                                        </th>
                                        <th scope="col">
                                            Department
                                        </th>
                                        <th scope="col">
                                            Category
                                        </th>
                                        <th scope="col">
                                            Brand
                                        </th>
                                        <th scope="col">
                                            Rating
                                        </th>
                                        <th scope="col">
                                            VendorCode
                                        </th>
                                        <th scope="col">
                                            VendorName
                                        </th>
                                    </tr>
                                </thead>
                                <asp:Repeater ID="rptrBulkBarcode" runat="server" OnItemDataBound="rptrBulkBarcode_ItemDataBound">
                                    <HeaderTemplate>
                                        <tbody id="BulkBarcodebody">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="BulkBarcoderow" runat="server">
                                            <td>
                                                <asp:CheckBox ID="cbrptrrow" runat="server" onclick="fncrptrRowClick(this)" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNumber") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblItemcode" runat="server" Text='<%# Eval("inventorycode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="barcode_Qty" Text='<%# Eval("Qty") %>'
                                                    onblur="fncQtyFocusOut(this)" MaxLength="4" onkeydown="fncSetFocustoNextQty(event,this)" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("BatchNo") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblSellingprice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDepartmentCode" runat="server" Text='<%# Eval("DepartmentCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCategoryCode" runat="server" Text='<%# Eval("CategoryCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblBrandCode" runat="server" Text='<%# Eval("BrandCode") %>' />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRating" runat="server" CssClass="barcode_Rating" Text='<%# Eval("Rating") %>'
                                                    MaxLength="1" onblur="fncRatingfocusout(this)" onkeydown="fncSetFocustoNextRating(event,this)" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblvendorcode" runat="server" Text='<%# Eval("VendorCode") %>' />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblvendorname" runat="server" Text='<%# Eval("VendorName") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr style="height: 370px" runat="server" id="emptyrow">
                                        <td colspan="14" class="repeater_td_align_center">
                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                                Text="No items to display" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div>
                <asp:UpdatePanel ID="upPaging" runat="server">
                    <ContentTemplate>
                        <div id="rptpagination" style="float: left;display:none">                            
                                <ups:PaginationUserControl runat="server" ID="barcodepaging" OnPaginationButtonClick="barcodepaging_PaginationButtonClick" />                            
                            <%--<div style="float: left">
                                <span>
                                    <asp:LinkButton ID="lnkFirst" runat="server" Text='<%$Resources:LabelCaption,lbl_First%>'
                                        OnCommand="lnkPageNavigation_Command" />
                                </span><span>
                                    <asp:LinkButton ID="lnkPrevious" Text='<%$ Resources:LabelCaption,lbl_Previous %>'
                                        runat="server" OnCommand="lnkPageNavigation_Command" />
                                </span>
                                <asp:Repeater ID="rptrPagination" runat="server" OnItemCommand="rptrPagination_ItemCommand">
                                    <ItemTemplate>
                                        <span id="spanpagination">
                                            <asp:LinkButton ID="lnkrptpagination" Text='<%# Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                                runat="server" CssClass='<%# Convert.ToBoolean(Eval("Enabled")) ? "pager_enabled" : "pager_disabled" %>'
                                                OnClientClick='<%# !Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'> ></asp:LinkButton>
                                        </span>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <span>
                                    <asp:LinkButton ID="lnkNext" Text='<%$ Resources:LabelCaption,lbl_Next %>' runat="server"
                                        OnCommand="lnkPageNavigation_Command" />
                                </span><span>
                                    <asp:LinkButton ID="lnkLast" Text='<%$ Resources:LabelCaption,lbl_Last %>' runat="server"
                                        OnCommand="lnkPageNavigation_Command" />
                                </span>
                            </div>
                            <div style="float: right" class="pagingborder_status">
                                <span id="spanpagestatus">
                                    <asp:Label ID="lblPageStatus" runat="server"></asp:Label>
                                </span>
                            </div>--%>
                        </div>
                        <div style="float: right; display: table" class="pagination">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_Click"
                                    OnClientClick="return fncPrintvalidation()" Text='<%$ Resources:LabelCaption,btn_Print %>'></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="return fncClear()"
                                    Text='<%$ Resources:LabelCaption,btnClearAll %>'></asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="barcodeprinting" class="barcodesavedialog">
            <p>
                <%=Resources.LabelCaption.Save_Barcodeprinting%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
