﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmInventoryMasterView.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmInventoryMasterView" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 40px;
            max-width: 40px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 270px;
            max-width: 270px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 140px;
            max-width: 140px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 155px;
            max-width: 155px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 78px;
            max-width: 78px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 89px;
            max-width: 89px;
        }
         .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 49px;
            max-width: 89px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                   <%-- $('#<%=lnkNew.ClientID %>').css("display", "initial");--%>
                 }
                 else {
                     $('#<%=lnkNew.ClientID %>').css("display", "none");
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
    </script>
    
       <script type="text/javascript">
               var d1;
               var d2;
               var Opendate;
               var dur;
               function fncGetUrl() {
                   fncSaveHelpVideoDetail('', '', 'InventoryMasterView');
               }
               function fncOpenvideo() {

                   document.getElementById("ifHelpVideo").src = HelpVideoUrl;

                   var Mode = "InventoryMasterView";
                   var d = new Date($.now());
                   Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                   d1 = new Date($.now()).getTime();



                   $("#dialog-Open").dialog({
                       autoOpen: true,
                       resizable: false,
                       height: "auto",
                       width: 1093,
                       modal: true,
                       dialogClass: "no-close",
                       buttons: {
                           Close: function () {
                               $(this).dialog("destroy");
                               var d = new Date($.now());
                               var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                               d2 = new Date($.now()).getTime();
                               var Diff = Math.floor((d2 - d1) / 1000);
                               //alert(Diff);
                               if (Diff >= 60) {
                                   fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                               }

                           }
                       }
                   });
               }


       </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View Inventory</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="updateInvGrid" runat="Server">
            <ContentTemplate>
                <div class="gridDetails" style="overflow-x:scroll">
                    <div class="grid-search">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"
                                        onFocus="this.select()"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'></asp:LinkButton>
                                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" onClick="lnkNew_Click"
                                        Text="New"></asp:LinkButton>
                                </asp:Panel>
                            </div>
                            <div class="col-md-3">
                                <asp:RadioButton ID="rdoStarWith" runat="server" Checked="true" GroupName="Search" Text="Start With"
                                    Class="radioboxlist" />
                                <asp:RadioButton ID="rdoContains" runat="server"   Class="radioboxlist"
                                    GroupName="Search" Text="Contains" />
                            </div>
                        </div>
                    </div>

                    <div class="container-group-pc1">
                        <div class="container-horiz-top-header1">
                            <div class="right-container-top-header">
                                Inventory History
                            </div>
                            <div class="right-container-top-detail">
                                <div class="GridDetails">
                                    <div class="row">
                                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                            <div class="grdLoad">
                                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Edit
                                                            </th>
                                                            <th scope="col">Item Code
                                                            </th>
                                                            <th scope="col">Item Name
                                                            </th>
                                                            <th scope="col">Department Code
                                                            </th>
                                                            <th scope="col">Brand Code
                                                            </th>
                                                            <th scope="col">Category Code
                                                            </th>
                                                            <th scope="col">Unit Cost
                                                            </th>
                                                            <th scope="col">Selling Price
                                                            </th>
                                                            <th scope="col">MRP
                                                            </th>
                                                            <th scope="col">Batch
                                                            </th>
                                                            <th scope="col">UOM
                                                            </th>
                                                            <th scope="col">VendorCode
                                                            </th>
                                                            <th scope="col">Stock
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 350px; width: 1372px; background-color: aliceblue;">
                                                    <asp:GridView ID="grdInventoryList" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                        DataKeyNames="InventoryCode" CssClass="pshro_GridDgn grdLoad">
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                        ToolTip="Click here to edit" OnClick="imgbtnEdit_Click" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                                            <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                                            <asp:BoundField DataField="DepartmentCode" HeaderText="Department Code" />
                                                            <asp:BoundField DataField="BrandCode" HeaderText="Brand Code"></asp:BoundField>
                                                            <asp:BoundField DataField="CategoryCode" HeaderText="Category Code"></asp:BoundField>
                                                            <asp:BoundField DataField="UnitCost" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                            <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                            <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Batch" HeaderText="Batch"></asp:BoundField>
                                                            <asp:BoundField DataField="UOM" HeaderText="UOM"></asp:BoundField>
                                                            <asp:BoundField DataField="VendorCode" HeaderText="VendorCode"></asp:BoundField>
                                                            <asp:BoundField DataField="QtyOnHand" HeaderText="Stock"></asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                                <ups:PaginationUserControl runat="server" ID="InvPaging" OnPaginationButtonClick="InvPaging_PaginationButtonClick" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updateInvGrid">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
