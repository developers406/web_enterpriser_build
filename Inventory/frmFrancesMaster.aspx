﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmFrancesMaster.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmFrancesMaster" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControlForDialogBox" Src="~/UserControls/SearchFilterUserControlForDialogBox.ascx" %>
<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .Franchise td:nth-child(1), .Franchise th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .Franchise td:nth-child(2), .Franchise th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .Franchise td:nth-child(3), .Franchise th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .Franchise td:nth-child(4), .Franchise th:nth-child(4) {
            min-width: 687px;
            max-width: 687px;
        }

        .Franchise td:nth-child(5), .Franchise th:nth-child(5) {
            min-width: 80px;
            max-width: 80px;
        }

        .Franchise td:nth-child(6), .Franchise th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .Franchise td:nth-child(7), .Franchise th:nth-child(7) {
            min-width: 90px;
            max-width: 90px;
        }
        .Franchise td:nth-child(7) {
            text-align:right;
        }

        .Franchise td:nth-child(8), .Franchise th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .Franchise td:nth-child(9), .Franchise th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }

        .Franchise td:nth-child(10), .Franchise th:nth-child(10) {
            min-width: 70px;
            max-width: 70px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>

    <script type="text/javascript">




        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" ) {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
             }
             else {
                 $('#<%=lnkSave.ClientID %>').css("display", "none");
             }
                $("select").chosen({ width: '100%' }); // width in px, %, em, etc
                //             $("select").show().removeClass('chzn-done');
                //$("select").next().remove();
                $('#<%=chkSelectAll.ClientID %>').change(function () {
                    fncGeneralActSettingChange(this);
                });
                $('#<%=chkSelectAllRight.ClientID %>').change(function () {
                    fncTranActSettingChange(this);
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        ///Function key set
        function disableFunctionKeys(e) {


            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 118) { /// F8
                    fncShowFiltration();
                    e.preventDefault();
                    //return false;
                }
                else if (e.keyCode == 115) /// F4
                {
                    if (fncSaveValidation() == true) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkOk', '');
                    }
                    e.preventDefault();
                    return false;
                }
                else if (e.keyCode == 116) /// F5
                {
                    fncShowLocationVendor();
                    e.preventDefault();
                    return false;
                    //if (fncSaveValidation() == true) {
                    //    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    //}
                }
                else if (e.keyCode == 123)///F12 
                {
                    __doPostBack('ctl00$ContentPlaceHolder1$searchFilterUserControlForDialogBox$lnkFilter', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 117)///F6
                {
                    clearForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 120) {
                    fncShowActivationSettings();
                    e.preventDefault();
                    return false;
                }

            }
        };

        function fncShowFiltration() {
            try {

                $(function () {
                    $("#dialog-Filtration").dialog({
                        appendTo: 'form:first',
                        title: "Enterpriser Web",
                        width: 400,
                        modal: true
                    });
                });

                if ($("#<%=rbnNew.ClientID%>").is(":checked")) {
                    $("#ContentPlaceHolder1_searchFilterUserControlForDialogBox_divLocation").css("display", "none");
                }
                else {
                    $("#ContentPlaceHolder1_searchFilterUserControlForDialogBox_divLocation").css("display", "block");
                }

               
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncHideFilter() {
            try {
                $("#dialog-Filtration").dialog("destroy");
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

       <%-- /// Disable and Enable Cancel Column
        function fncDisableEnableCancelCol() {
            try {
                if ($("#<%=rbnNew.ClientID%>").is(':checked')) {
                    $("#<%=Locven.ClientID%>").css("display", "block");

                }
                else {
                    $("#<%=Locven.ClientID%>").css("display", "none");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>

        /// Save Validation
        function fncSaveValidation() {
            try {

                if ($("#ContentPlaceHolder1_gvFranchise [id*=cbSelect]:checked").length == 0) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                    return false;
                }
                else if ($('#<%=lstLocation.ClientID %> option:selected').length == 0) {

                    //popUpObjectForSetFocusandOpen = $('#<%=lstLocation.ClientID %> option');
                    fncOpenDropdownlist($('#<%=lstLocation.ClientID %> option'));
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Loc%>');
                    return false;
                }
                else if ($('#<%=lstVendor.ClientID %> option:selected').length == 0) {
                    //popUpObjectForSetFocusandOpen = $('#<%=lstLocation.ClientID %> option');
                    fncOpenDropdownlist($('#<%=lstLocation.ClientID %> option'));
                    fncToastInformation('<%=Resources.LabelCaption.alert_selectanyonevendor%>');
                    return false;
                }


        return true;

    }
    catch (err) {
        fncToastError(err.message);
    }
}




//Focus Set to Next Row
function fncSetFocustoNextRow(evt, source, nxtRowVal, rightcolval, leftColval) {
    try {
        var rowobj = $(source).parent().parent();
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var verHight, minQty = 0, maxQty = 0, reOrderQty = 0;

        if (charCode == 13 || charCode == 40) {

            var NextRowobj = rowobj.next();
            fncSetFocustoObject(NextRowobj.find('td input[id*=' + nxtRowVal + ']'));

            if (nxtRowVal == "cbSelect" && parseInt($("td", rowobj).eq(0).text()) < 15) {
                $("#divFranchise").scrollTop(0);
            }
            else {
                verHight = $("#divFranchise").scrollTop();
                $("#divFranchise").scrollTop(verHight + 10);
            }



        }
        else if (charCode == 38) {
            var prevrowobj = rowobj.prev();
            //prevrowobj.find('td input[id*=' + nxtRowVal + ']').select();
            fncSetFocustoObject(prevrowobj.find('td input[id*=' + nxtRowVal + ']'));

            if (nxtRowVal == "cbSelect") {
                verHight = $("#divFranchise").scrollTop();
                $("#divFranchise").scrollTop(verHight - 10);
            }
        }
        else if (charCode == 39) {
            //rowobj.find('td input[id*=' + rightcolval + ']').select();
            fncSetFocustoObject(rowobj.find('td input[id*=' + rightcolval + ']'));
        }
        else if (charCode == 37) {
            //rowobj.find('td input[id*=' + leftColval + ']').select();
            fncSetFocustoObject(rowobj.find('td input[id*=' + leftColval + ']'));
        }
    }
    catch (err) {
        fncToastError(err.message);
    }
}

$(document).ready(function () {
    fncgridCheckboxdown();
});

function fncgridCheckboxdown() {
    try {
        $('[id*="ContentPlaceHolder1_gvFranchise_cbSelect_"]').on('keydown', function (e) {
            fncSetFocustoNextRow(e, this, 'cbSelect', 'txtMinQtyLevel', 'cbSelect');
        });

    }
    catch (err) {
        fncToastError(err.message);
    }
}



///Show Selected Row with color
function fncRowClick(source) {
    try {

        var obj;
        obj = $(source);

        $(obj).css("background-color", "#8080ff");
        $(obj).siblings().each(function () {
            $(this).css("background-color", "white");
        });
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

///Show Item Search Table
function fncShowActivationSettings() {
    try {
        $("#ActSettings").dialog({
            resizable: false,
            height: "auto",
            width: "auto",
            modal: true,
            title: "Activation Settings",
            appendTo: 'form:first'
        });
    }
    catch (err) {
        fncToastError(err.message);
    }
}


function fncCheckBoxKeyDown(evt, value) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    try {
        if (value == "chkExpiryDate") {
            if (charCode == 40) {
                $('#<%=chkSerialNo.ClientID %>').focus();
               }
               else if (charCode == 13) {
                   return false;
               }
           }
           else if (value == "chkSerialNo") {
               if (charCode == 40) {
                   $('#<%=chkAutoPO.ClientID %>').focus();
                }
                else if (charCode == 38) {
                    $('#<%=chkExpiryDate.ClientID %>').focus();
                }
                else if (charCode == 13) {
                    return false;
                }
        }
        else if (value == "chkAutoPO") {
            if (charCode == 40) {
                $('#<%=chkPackage.ClientID %>').focus();
            }
            else if (charCode == 38) {
                $('#<%=chkSerialNo.ClientID %>').focus();
            }
            else if (charCode == 13) {
                return false;
            }
    }
    else if (value == "chkPackage") {
        if (charCode == 40) {
            $('#<%=chkLoyality.ClientID %>').focus();
        }
        else if (charCode == 38) {
            $('#<%=chkAutoPO.ClientID %>').focus();
        }
        else if (charCode == 13) {
            return false;
        }
}
else if (value == "chkLoyality") {
    if (charCode == 40) {
        $('#<%=chkSpecial.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPackage.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSpecial") {
    if (charCode == 40) {
        $('#<%=chkMemDis.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkLoyality.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkMemDis") {
    if (charCode == 40) {
        $('#<%=chkSeasonal.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSpecial.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSeasonal") {
    if (charCode == 40) {
       <%-- if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=chkShelf.ClientID %>').focus();
        }
        else {
            $('#<%=chkBatch.ClientID %>').focus();
        }--%>
    }
    else if (charCode == 38) {
        $('#<%=chkMemDis.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkBatch") {
    if (charCode == 40) {
        $('#<%=chkShelf.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSeasonal.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkShelf") {
    if (charCode == 40) {
        $('#<%=chkHideData.ClientID %>').focus();
    }
    else if (charCode == 38) {
       <%-- if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=chkSeasonal.ClientID %>').focus();
        }
        else {
            $('#<%=chkBatch.ClientID %>').focus();
        }--%>
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkHideData") {
    if (charCode == 40) {
        $('#<%=chkMarking.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkShelf.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkMarking") {
    if (charCode == 40) {
        $('#<%=chkBarcodePrint.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkHideData.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkBarcodePrint") {
    if (charCode == 40) {
        $('#<%=chkAllowCost.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkMarking.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkAllowCost") {
    if (charCode == 40) {
        <%--if ($('#<%=hidinventoryStatus.ClientID %>').val() == "Edit") {
            $('#<%=txtExpiryDays.ClientID %>').select();
        }
        else {
            $('#<%=cbWeightbased.ClientID %>').focus();
        }--%>

    }
    else if (charCode == 38) {
        $('#<%=chkBarcodePrint.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "cbWeightbased") {
    if (charCode == 40) {
        $('#<%=txtExpiryDays.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkAllowCost.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "txtExpiryDays") {
    if (charCode == 13) {
        $('#<%=txtRating.ClientID %>').select();
        return false;
    }

}
else if (value == "txtRating") {
    if (charCode == 13) {
        $('#<%=txtShelfQty.ClientID %>').select();
        return false;
    }
}
else if (value == "txtShelfQty") {
    if (charCode == 13) {
        $('#<%=chkSelectAll.ClientID %>').focus();
        return false;
    }
}
else if (value == "chkSelectAll") {
    if (charCode == 40) {
        $('#<%=chkPurchaseOrder.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=txtShelfQty.ClientID %>').select();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkPurchaseOrder") {
    if (charCode == 40) {
        $('#<%=chkGRN.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSelectAll.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkGRN") {
    if (charCode == 40) {
        $('#<%=chkPurchaseReturn.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPurchaseOrder.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkPurchaseReturn") {
    if (charCode == 40) {
        $('#<%=chkPointOfSale.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkGRN.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkPointOfSale") {
    if (charCode == 40) {
        $('#<%=chkSalesReturn.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPurchaseReturn.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSalesReturn") {
    if (charCode == 40) {
        $('#<%=chkOrderBooking.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkPointOfSale.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkOrderBooking") {
    if (charCode == 40) {
        $('#<%=chkEstimate.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkSalesReturn.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkEstimate") {
    if (charCode == 40) {
        $('#<%=chkQuatation.ClientID %>').focus();
    }
    else if (charCode == 38) {
        $('#<%=chkOrderBooking.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkQuatation") {
    if (charCode == 40) {
        $('#<%=chkSelectAllRight.ClientID %>').focus();
    }
    if (charCode == 38) {
        $('#<%=chkEstimate.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
else if (value == "chkSelectAllRight") {
    if (charCode == 38) {
        $('#<%=chkQuatation.ClientID %>').focus();
    }
    else if (charCode == 13) {
        return false;
    }
}
}
    catch (err) {
        fncToastError(err.message);
    }
}

function fncGeneralActSettingChange(obj) {
    try {

        $('#<%=chkExpiryDate.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkSerialNo.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkAutoPO.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkPackage.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkLoyality.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkSpecial.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkMemDis.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkSeasonal.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkShelf.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkHideData.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkMarking.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkBarcodePrint.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkAllowCost.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=cbAdditionalCess.ClientID %>').prop('checked', $(obj).prop("checked"));
    }
    catch (err) {
        fncToastError(err.Message);
    }
}


function fncTranActSettingChange(obj) {
    try {
        $('#<%=chkQuatation.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkPurchaseOrder.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkGRN.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkPurchaseReturn.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkPointOfSale.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkSalesReturn.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkOrderBooking.ClientID %>').prop('checked', $(obj).prop("checked"));
        $('#<%=chkEstimate.ClientID %>').prop('checked', $(obj).prop("checked"));
    }
    catch (err) {
        fncToastError(err.Message);
    }
}

function fncShowLocationVendor() {
    try {

        $("#Locven").dialog({
            title: "Enterpriser Web",
            width: 870,
            modal: true,
            appendTo: 'form:first'
        });



    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncCancel() {
    try {
        $("#Locven").dialog("close");
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncCalculateReOrderLevel(source,obj,hdrclick) {    
    var minQty = 0, maxQty = 0, reOrderQty = 0, rowobj;
    try {

        if (hdrclick == "1") {
            rowobj = $(source);
        }
        else {
            rowobj = $(source).parent().parent();
        }
        
        minQty = rowobj.find('td input[id*="txtMinQtyLevel"]').val();
        maxQty = rowobj.find('td input[id*="txtMaxQtyLevel"]').val();
        reOrderQty = parseFloat(maxQty) - parseFloat(minQty);       
        if (reOrderQty < 0) {
            fncToastInformation("check entered value");
            fncSetFocustoObject(rowobj.find('td input[id*=' + obj + ']'));
            rowobj.find('td input[id*="cbSelect"]').prop("checked", false);
            return false;
        }
        else {
            rowobj.find('td input[id*="txtReOrderLevel"]').val(reOrderQty);            
            rowobj.find('td input[id*="cbSelect"]').prop("checked", true);
            return true;            
        }

    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncHdrclick(source)
{
    try
    {
        if ($(source).is(":checked")) {
            $("#ContentPlaceHolder1_gvFranchise tbody").children().each(function () {
                fncCalculateReOrderLevel(this, 'cbSelect','1');
            });
        }
        else {
            $("#ContentPlaceHolder1_gvFranchise tbody tr").find('td input[id*="cbSelect"]').prop("checked", false);
        }
    }
    catch (err)
    {
        fncToastError(err.message);
    }
}
function fncRowcbClick(source)
{
    try
    {
        if ($(source).is(":checked"))
        {
            fncCalculateReOrderLevel(source, 'cbSelect');
        }
    }
    catch (err)
    {
        fncToastError(err.message);
    }
}

    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'FrancesMaster');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "FrancesMaster";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory Configuration</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page uppercase" id="breadcrumbs_text">Inventory Configuration Master
                </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div>
            <div class=" control-button frances_rbn">
                <asp:UpdatePanel runat="server" ID="uprbn">
                    <ContentTemplate>
                        <asp:RadioButton ID="rbnNew" runat="server" CssClass="radioboxlist" Checked="true" AutoPostBack="true" OnCheckedChanged="rbnNew_checkChanged" Text='<%$ Resources:LabelCaption,lbl_New %>' GroupName="francesFetch" />
                        &nbsp
                <asp:RadioButton ID="rbnExisting" runat="server" CssClass="radioboxlist" AutoPostBack="true" OnCheckedChanged="rbnExisting_checkChanged" Text='<%$ Resources:LabelCaption,lbl_Existing %>' GroupName="francesFetch" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="float_right">
                <div class="control-button">
                    <asp:LinkButton ID="lnkActSettings" runat="server" class="button-blue" Text="Activation Settings (F9)" OnClientClick="fncShowActivationSettings();return false;"></asp:LinkButton>
                </div>
            </div>
        </div>
        <div id="divrepeater" class="over_flowhorizontal display_table">
            <table rules="all" border="1" id="tblPOSummary" runat="server" class="fixed_header Franchise">
                <tr>
                    <th>S.No</th>
                    <th> <asp:CheckBox ID="cbhdrSelect" runat="server" onclick="fncHdrclick(this);" /> </th>
                    <th>Item code</th>
                    <th>Description</th>
                    <th>MaxQtyLevel</th>
                    <th>MinQtyLevel</th>
                    <th>ReOrderLevel</th>
                    <th>Cycle Days</th>
                    <th>Shelf Qty</th>
                    <th>Rating</th>
                </tr>
            </table>
            <asp:UpdatePanel ID="uprptrfran" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divFranchise" class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 400px; width: 1355px;">
                        <asp:GridView ID="gvFranchise" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                            CssClass="pshro_GridDgn Franchise">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <Columns>
                                <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbSelect" runat="server" onclick="fncRowcbClick(this);" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="InventoryCode" HeaderText="InventoryCode"></asp:BoundField>
                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                <asp:TemplateField HeaderText="MaxQtyLevel">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMaxQtyLevel" runat="server" onchange="fncCalculateReOrderLevel(this,'txtMaxQtyLevel');" onkeydown="return fncSetFocustoNextRow(event,this,'txtMaxQtyLevel','txtMinQtyLevel','txtMaxQtyLevel');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("MaximumQtyLevel") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="MinQtyLevel">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMinQtyLevel" runat="server" onchange="fncCalculateReOrderLevel(this,'txtMinQtyLevel');" onkeydown="return fncSetFocustoNextRow(event,this,'txtMinQtyLevel','txtReOrderLevel','txtMaxQtyLevel');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("MinimumQtyLevel") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="ReOrderLevel" HeaderText="ReOrderLevel"></asp:BoundField>--%>
                                <asp:TemplateField HeaderText="ReOrderLevel">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtReOrderLevel" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'txtReOrderLevel','txtReOrderDays','txtMaxQtyLevel');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("ReOrderLevel") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cycle Days">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtReOrderDays" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'txtReOrderDays','txtShelfQty','txtReOrderLevel');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res" Text='<%# Eval("ReOrderDays") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Shelf Qty">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtShelfQty" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'txtShelfQty','txtRating','txtReOrderDays');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res-right" Text='<%# Eval("ReOrderDays") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rating">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtRating" runat="server" onkeydown="return fncSetFocustoNextRow(event,this,'txtRating','txtRating','txtShelfQty');" onkeypress="return isNumberKey(event);" CssClass="grn_rptr_textbox_width form-control-res-right" Text='<%# Eval("ReOrderDays") %>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div>
            <div class="float_left">
                <asp:UpdatePanel ID="upPagination" runat="server">
                    <ContentTemplate>
                        <ups:PaginationUserControl runat="server" ID="frances" OnPaginationButtonClick="frances_PaginationButtonClick" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="float_right">
                <div class="control-button">
                    <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Filter %>' OnClientClick="fncShowFiltration();return false;"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Text='Continue (F5)' OnClientClick="  fncShowLocationVendor();return false;"></asp:LinkButton>
                </div>
            </div>

        </div>

        <div class="hiddencol">
            <div id="ActSettings">
                <div class="activation">
                    <div class="activation-left franchise_ActWidth">
                        <div class="inv_settinghdr">
                            <span>General Activation Settings </span>
                        </div>
                        <div class="activation-check-left activation-check-width">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkExpiryDate" onkeydown=" return fncCheckBoxKeyDown(event,'chkExpiryDate');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label104" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDate %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSerialNo" onkeydown="return fncCheckBoxKeyDown(event,'chkSerialNo');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label48" runat="server" Text='<%$ Resources:LabelCaption,lbl_SerialNoRequired %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkAutoPO" onkeydown="return fncCheckBoxKeyDown(event,'chkAutoPO');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label49" runat="server" Text='<%$ Resources:LabelCaption,lbl_AutoPOAllowed %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPackage" onkeydown="return fncCheckBoxKeyDown(event,'chkPackage');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label50" runat="server" Text='<%$ Resources:LabelCaption,lbl_PackageItem %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkLoyality" onkeydown="return fncCheckBoxKeyDown(event,'chkLoyality');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label51" runat="server" Text='<%$ Resources:LabelCaption,lbl_Loyality %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSpecial" onkeydown="return fncCheckBoxKeyDown(event,'chkSpecial');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label52" runat="server" Text='<%$ Resources:LabelCaption,lbl_Special %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control" style="display:none">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkMemDis" onkeydown="return fncCheckBoxKeyDown(event,'chkMemDis');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label53" runat="server" Text='<%$ Resources:LabelCaption,lbl_MemDisAllowed %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="activation-check-right activation-check-width">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkShelf" onkeydown="return fncCheckBoxKeyDown(event,'chkShelf');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label56" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfStorage %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkHideData" onkeydown="return fncCheckBoxKeyDown(event,'chkHideData');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label57" runat="server" Text='<%$ Resources:LabelCaption,lbl_HideDataOnBarcode %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkMarking" onkeydown="return fncCheckBoxKeyDown(event,'chkMarking');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label58" runat="server" Text='<%$ Resources:LabelCaption,lbl_MarkingItem %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkBarcodePrint" onkeydown="return fncCheckBoxKeyDown(event,'chkBarcodePrint');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label59" runat="server" Text='<%$ Resources:LabelCaption,lbl_NeedBarcodePrint %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkAllowCost" onkeydown="return fncCheckBoxKeyDown(event,'chkAllowCost');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label60" runat="server" Text='<%$ Resources:LabelCaption,lbl_AllowCostEdit %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="cbWeightbased" onkeydown="return fncCheckBoxKeyDown(event,'cbWeightbased');" CssClass="checkbox1" Checked="false" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="lblWeightbased" runat="server" Text='Weight Based'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control" style="display:none">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSeasonal" onkeydown="return fncCheckBoxKeyDown(event,'chkSeasonal');" CssClass="checkbox1" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label54" runat="server" Text='<%$ Resources:LabelCaption,lbl_SeasonalItem %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control display_none">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="cbAdditionalCess" onkeydown="return fncCheckBoxKeyDown(event,'cbAdditinalCess');" CssClass="checkbox1 " Checked="false" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="lblAdditionalCess" runat="server" Text='Additional Cess'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="activation-check-right activation-check-width">
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left activation-check-width">
                                <div class="label-left">
                                    <asp:Label ID="Label44" runat="server" Text='<%$ Resources:LabelCaption,lbl_ExpiryDays %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtExpiryDays" onkeydown="return fncCheckBoxKeyDown(event,'txtExpiryDays');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                        onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left activation-check-width">
                                <div class="label-left">
                                    <asp:Label ID="Label116" runat="server" Text='<%$ Resources:LabelCaption,lbl_ShelfQty %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtShelfQty" onkeydown="return fncCheckBoxKeyDown(event,'txtShelfQty');" runat="server" MaxLength="5" CssClass="form-control-res" Width="80px"
                                        onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left activation-check-width" style="display:none">
                                <div class="label-left">
                                    <asp:Label ID="Label115" runat="server" Text="Rating"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtRating" onkeydown="return fncCheckBoxKeyDown(event,'txtRating');" MaxLength="5" runat="server" CssClass="form-control-res" Width="80px"
                                        onkeypress="return isNumberKeyWithDecimalNew(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="float_left">
                                <div class="inv_selectall">
                                    <asp:CheckBox ID="chkSelectAll" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAll');" runat="server" />
                                </div>
                                <div class="float_left">
                                    <span style="color: Blue">Select All</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="activation-right">
                        <div class="inv_settinghdr">
                            <span>Transaction Activation Settings </span>
                        </div>
                        <div class="activation-check-left">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPurchaseOrder" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseOrder');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseOrder %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkGRN" onkeydown="return fncCheckBoxKeyDown(event,'chkGRN');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label62" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNPurchase %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPurchaseReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkPurchaseReturn');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label63" runat="server" Text='<%$ Resources:LabelCaption,lbl_PurchaseReturn %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkPointOfSale" onkeydown="return fncCheckBoxKeyDown(event,'chkPointOfSale');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label64" runat="server" Text='<%$ Resources:LabelCaption,lbl_POS %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="activation-check-right">
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkSalesReturn" onkeydown="return fncCheckBoxKeyDown(event,'chkSalesReturn');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label68" runat="server" Text='<%$ Resources:LabelCaption,lbl_SalesReturn %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkOrderBooking" onkeydown="return fncCheckBoxKeyDown(event,'chkOrderBooking');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label69" runat="server" Text='<%$ Resources:LabelCaption,lbl_OrderBooking %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkEstimate" onkeydown="return fncCheckBoxKeyDown(event,'chkEstimate');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label70" runat="server" Text='<%$ Resources:LabelCaption,lbl_Estimate %>'></asp:Label>
                                </div>
                            </div>
                            <div class="checkbox-control">
                                <div class="checkbox-left">
                                    <asp:CheckBox ID="chkQuatation" onkeydown="return fncCheckBoxKeyDown(event,'chkQuatation');" runat="server" />
                                </div>
                                <div class="checkbox-right">
                                    <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lbl_Quatation %>'></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-right inv_tran_selectall">
                                <div class="inv_Tran_lblselectall">
                                    <asp:CheckBox ID="chkSelectAllRight" onkeydown="return fncCheckBoxKeyDown(event,'chkSelectAllRight');" runat="server" />
                                </div>
                                <div class="label-left">
                                    <span style="color: Blue">Select All</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="dialog-Filtration">
                <ups:SearchFilterUserControlForDialogBox runat="server" ID="searchFilterUserControlForDialogBox"                    
                    EnableVendorDropDown ="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    EnablelocationDropDown="true"
                    OnClearButtonClientClick="clearForm(); return false;"
                    OnFilterButtonClientClick="fncHideFilter();"
                    OnFilterButtonClick="lnkLoadFilter_Click" />
            </div>
            <div id="Locven">
                <div class="control-button">
                    <asp:Label ID="lblLocation" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
                </div>
                <div class="control-button">
                    <asp:ListBox ID="lstLocation" runat="server" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple"></asp:ListBox>
                </div>
                <div class="control-button">
                    <asp:Label ID="lblVendor" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                </div>
                <div class="control-button">
                    <asp:ListBox ID="lstVendor" runat="server" CssClass="form-control-res franchise_lst_ven" SelectionMode="Multiple"></asp:ListBox>
                </div>

                <div class="franchise_LocVen_width">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>' OnClientClick=" return fncSaveValidation();" OnClick="lnkSave_click"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkCancel" runat="server" class="button-blue" Text="Cancel" OnClientClick="fncCancel();return false;"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
