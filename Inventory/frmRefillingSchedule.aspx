﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmRefillingSchedule.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmRefillingSchedule" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControlForDialogBox" Src="~/UserControls/SearchFilterUserControlForDialogBox.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .RefillSchedule td:nth-child(1), .RefillSchedule th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .RefillSchedule td:nth-child(2), .RefillSchedule th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .RefillSchedule td:nth-child(3), .RefillSchedule th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(4), .RefillSchedule th:nth-child(4) {
            min-width: 537px;
            max-width: 537px;
        }

        .RefillSchedule td:nth-child(5), .RefillSchedule th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(6), .RefillSchedule th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(7), .RefillSchedule th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(8), .RefillSchedule th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(9), .RefillSchedule th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(10), .RefillSchedule th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(10) {
            text-align: right !important;
        }

        .RefillSchedule td:nth-child(11), .RefillSchedule th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(11) {
            text-align: right !important;
        }

        .RefillSchedule td:nth-child(12), .RefillSchedule th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(13), .RefillSchedule th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(14), .RefillSchedule th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(14) {
            text-align: right !important;
        }

        .RefillSchedule td:nth-child(15), .RefillSchedule th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .RefillSchedule td:nth-child(16), .RefillSchedule th:nth-child(16) {
            min-width: 200px;
            max-width: 200px;
        }

        .RefillSchedule td:nth-child(17), .RefillSchedule th:nth-child(17) {
            min-width: 200px;
            max-width: 200px;
        }

        .RefillSchedule td:nth-child(18), .RefillSchedule th:nth-child(18) {
            min-width: 200px;
            max-width: 200px;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }
                fncDecimal();
                setAutoComplete();
                $("#divItemcode").show();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncShowFiltration() {
            try {
                $("#divItemcode").hide();
                $('#<%=txtItemCode.ClientID %>').val('');
                $(function () {
                    $("#dialog-Filtration").dialog({
                        appendTo: 'form:first',
                        title: "Enterpriser Web",
                        width: 400,
                        modal: true
                    });
                });

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDecimal() {
            try {
                $('[id*="ContentPlaceHolder1_gvReFillSchedule_txtReFillQty_"]').number(true, 0);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }



        function fncReFillQtyCal(source, value) {

            try {
                obj = $(source).parent().parent();

                if (value == "cb") {
                    if (!obj.find('td input[id*="cbSelect"]').is(":checked")) {
                        return;
                    }
                }


                fncFillQtyVal(obj);


            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncFillQtyVal(obj) {
            var obj, reFillQty = 0, reqQty;
            try {
                reqQty = $("td", obj).eq(7).text();
                reFillQty = obj.find('td input[id*="txtReFillQty"]').val();
                if (parseFloat(reFillQty) == 0) {
                    obj.find('td input[id*="cbSelect"]').prop("checked", false);
                }
                else if (parseFloat(reFillQty) > parseFloat(reqQty)) {
                    fncToastInformation("ReFillQty must less then Or equal to Req.Qty");
                    obj.find('td input[id*="txtReFillQty"]').val("0");
                    obj.find('td input[id*="txtReFillQty"]').select();
                    obj.find('td input[id*="cbSelect"]').prop("checked", false);
                }
                else {
                    obj.find('td input[id*="cbSelect"]').prop("checked", true);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncMoveNextRow(source, evt) {
            var obj, charCode;
            try {
                obj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    obj.next().find('td input[id*="txtReFillQty"]').select();
                }
                else if (charCode == 38) {
                    obj.prev().find('td input[id*="txtReFillQty"]').select();
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSaveValidation() {
            var xml, obj;
            try {
                if ($("#ContentPlaceHolder1_gvReFillSchedule tbody tr").find('td input[id*="cbSelect"]:checked').length == 0) {
                    fncToastInformation("Select Any one row");
                    return false;
                }
                else {
                    xml = "<NewDataSet>"
                    $("#ContentPlaceHolder1_gvReFillSchedule tbody").children().each(function () {
                        obj = $(this);
                        if (obj.find('td input[id*="cbSelect"]').is(':checked')) {
                            xml += "<Table>";
                            xml += "<InventoryCode>" + $("td", obj).eq(2).text() + "</InventoryCode>";
                            xml += "<ReqQty>" + $("td", obj).eq(7).text() + "</ReqQty>";
                            xml += "<RefilledQty>" + obj.find('td input[id*="txtReFillQty"]').val() + "</RefilledQty>";
                            xml += "<LastReqQty>" + $("td", obj).eq(7).text() + "</LastReqQty>";
                            xml += "<LastReFilledQty>" + obj.find('td input[id*="txtReFillQty"]').val() + "</LastReFilledQty>";
                            xml += "<SoldQty>" + $("td", obj).eq(13).text() + "</SoldQty>";
                            xml += "</Table>";
                        }

                    });

                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    $('#<%=hidXml.ClientID %>').val(xml);

                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

         function fncPrintValidation() {
            var xml, obj;
            try {
                if ($("#ContentPlaceHolder1_gvReFillSchedule tbody tr").find('td input[id*="cbSelect"]:checked').length == 0) {
                    fncToastInformation("Select Any one row");
                    return false;
                }
                 else {
                    xml = "<NewDataSet>"
                    $("#ContentPlaceHolder1_gvReFillSchedule tbody").children().each(function () {
                        obj = $(this);
                        if (obj.find('td input[id*="cbSelect"]').is(':checked')) {
                            var Date = {};
                            Date = $("td", obj).eq(11).text().trim().split('-');
                            xml += "<Table>";
                            xml += "<InventoryCode>" + $("td", obj).eq(2).text() + "</InventoryCode>";
                            xml += "<Description>" + $("td", obj).eq(3).text() + "</Description>";
                            xml += "<warehouse>" + $("td", obj).eq(4).text() + "</warehouse>"; 
                            xml += "<ShelfQty>" + $("td", obj).eq(5).text() + "</ShelfQty>";
                            xml += "<CurSHQty>" + $("td", obj).eq(6).text() + "</CurSHQty>";
                            xml += "<ReqQty>" + $("td", obj).eq(7).text() + "</ReqQty>";
                            xml += "<RefilledQty>" + obj.find('td input[id*="txtReFillQty"]').val() + "</RefilledQty>";
                            xml += "<LastReqQty>" + $("td", obj).eq(9).text() + "</LastReqQty>";
                            xml += "<LastReFilledQty>" + $("td", obj).eq(10).text() + "</LastReFilledQty>";
                            xml += "<LastReFilledDate>" + Date[2] + '-' + Date[1] + '-' + Date[0] + "</LastReFilledDate>";
                            xml += "<SoldQty>" + $("td", obj).eq(13).text() + "</SoldQty>";
                            xml += "<CreateUser>" + $("td", obj).eq(14).text() + "</CreateUser>";
                           
                            xml += "</Table>";
                        }

                    });

                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    $('#<%=hidXml.ClientID %>').val(xml);

                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSelectAll(source) {
            try {

                if ($("#ContentPlaceHolder1_cbSelectAll").is(':checked')) {
                    $("#ContentPlaceHolder1_gvReFillSchedule tbody").children().each(function () {
                        fncFillQtyVal($(this));
                    });
                }
                else {
                    $("#ContentPlaceHolder1_gvReFillSchedule tbody tr").find('td input[id*="cbSelect"]').prop("checked", false);
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function setAutoComplete() {
            try {
                $("[id$=txtItemCode]").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "frmRefillingSchedule.aspx/fnGetItemcode",
                            data: "{ 'code': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('-')[0] + '-' + item.split('-')[1],
                                        val: item.split('-')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    open: function (event, ui) {
                        var $input = $(event.target);
                        var $results = $input.autocomplete("widget");
                        var scrollTop = $(window).scrollTop();
                        var top = $results.position().top;
                        var height = $results.outerHeight();
                        if (top + height > $(window).innerHeight() + scrollTop) {
                            newTop = top - height - $input.outerHeight();
                            if (newTop > scrollTop)
                                $results.css("top", newTop + "px");
                        }
                    },
                    focus: function (event, i) {
                        $('#<%=txtItemCode.ClientID %>').val(i.item.label.split('-')[0]);
                         event.preventDefault();
                     },
                     select: function (e, i) {
                         $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.label.split('-')[0]));
                    return false;
                },
                minLength: 1
                });
                $(window).scroll(function (event) {
                    $('.ui-autocomplete.ui-menu').position({
                        my: 'left bottom',
                        at: 'left top',
                        of: '#ContentPlaceHolder1_txtItemCode'
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function fncGetdData(msg) {
            try {
                var objData;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        <%--  function fncADDValidation() {
            try{
                if ($('#<%=txtItemCode.ClientID %>').val() == "") {
                    ShowPopupMessageBox("Please Enter Itemcode");
                    return false;
                } else  if ($('#<%=txtQty.ClientID %>').val() == "0") {
                    ShowPopupMessageBox("Please Enter Qty");
                    return false;
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }--%>

        function fncGetInventorycodePR(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    __doPostBack('ctl00$ContentPlaceHolder1$searchFilterUserControlForDialogBox$lnkFilter', '');
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'RefillingSchedule');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "RefillingSchedule";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Refilling Schedule</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="over_flowhorizontal">
                <table rules="all" border="1" id="tblTransfer" runat="server" class="fixed_header RefillSchedule">
                    <tr>
                        <th>S.No</th>
                        <th>
                            <asp:CheckBox ID="cbSelectAll" runat="server" onchange="fncSelectAll(this);" />
                        </th>
                        <th>Itemcode</th>
                        <th>Description</th>
                        <th>Warehouse</th>
                        <th>ShelfQty</th>
                        <th>Cur.ShelfQty</th>
                        <th>Req.Qty</th>
                        <th>Refill.Qty</th>
                        <th>LReq.Qty</th>
                        <th>LRefill.Qty</th>
                        <th>LRefill.Date</th>
                        <th>QtyOnHand</th>
                        <th>SoldQty</th>
                        <th>User</th>
                        <th>Department</th>
                        <th>Category</th>
                        <th>Brand</th>
                    </tr>
                </table>

                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 430px; width: 2455px; background-color: aliceblue;">
                    <asp:UpdatePanel runat="server" ID="upgrid" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvReFillSchedule" runat="server" ShowHeader="false"
                                AutoGenerateColumns="False" ShowHeaderWhenEmpty="false"
                                CssClass="RefillSchedule">
                                <%--  <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="Records Not Found"></asp:Label>
                                </EmptyDataTemplate>--%>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:BoundField DataField="RowNo" HeaderText="S.No"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbSelect" runat="server" onchange="fncReFillQtyCal(this,'cb');" onkeydown="return fncReturnfalse(event);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="InventoryCode"></asp:BoundField>
                                    <asp:BoundField DataField="Description"></asp:BoundField>
                                    <asp:BoundField DataField="warehouse"></asp:BoundField>
                                    <asp:BoundField DataField="ShelfQty"></asp:BoundField>
                                    <asp:BoundField DataField="CurSHQty"></asp:BoundField>
                                    <asp:BoundField DataField="ReqQty"></asp:BoundField>
                                    <asp:TemplateField HeaderText="ReFillQty">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReFillQty" runat="server" onchange="fncReFillQtyCal(this,'txt');" onkeydown="return fncMoveNextRow(this,event);" Style="margin: 2px;" CssClass="form-control-res-right" Text='<%#Eval("ReFillQty") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="LastReqQty"></asp:BoundField>
                                    <asp:BoundField DataField="LastReFilledQty"></asp:BoundField>
                                    <asp:BoundField DataField="LastReFilledDate"></asp:BoundField>
                                    <asp:BoundField DataField="QtyOnHand"></asp:BoundField>
                                    <asp:BoundField DataField="SoldQty"></asp:BoundField>
                                    <asp:BoundField DataField="CreateUser"></asp:BoundField>
                                    <asp:BoundField DataField="DepartmentCode"></asp:BoundField>
                                    <asp:BoundField DataField="CategoryCode"></asp:BoundField>
                                    <asp:BoundField DataField="BrandCode"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

            </div>
            <div class="container-group-price">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="col-md-5" style="margin-left: 10px" id="divItemcode">
                            <asp:Label ID="lblItemcode" runat="server" Text="ItemCode"></asp:Label>
                            <asp:TextBox ID="txtItemCode" runat="server" onkeydown="return fncGetInventorycodePR(event);"></asp:TextBox>
                        </div>
                        <%-- <div class ="col-md-4">
                        <asp:Label ID="Label1" runat="server" Text ="Qty"></asp:Label>
                        <asp:TextBox ID ="txtQty" runat="server" style ="text-align:right" Text ="0" ></asp:TextBox> 
                    </div>
                    <div class ="col-md-2">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text="Add"
                             OnClientClick="fncADDValidation();return;" OnClick ="lnkAdd_Click"></asp:LinkButton>
                        </div>--%>
                    </div>
                    <div class="transferOutSaveButton col-md-6">
                        <div style="float: right">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-blue" Text="Filter" OnClientClick="fncShowFiltration();return false;"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" Text="Save" OnClientClick="return fncSaveValidation();" OnClick="lnkSave_Click"></asp:LinkButton>
                            </div>
                              <div class="control-button">
                                <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" Text="Print" OnClick="lnkPrint_Click" OnClientClick="return fncPrintValidation();"></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="display_none">
            <asp:HiddenField ID="hidXml" runat="server" Value="" />
            <div id="dialog-Filtration">
                <ups:SearchFilterUserControlForDialogBox runat="server" ID="searchFilterUserControlForDialogBox"
                    EnableVendorDropDown="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    OnClearButtonClientClick="clearForm(); return false;"
                    OnFilterButtonClientClick="fncHideFilter();"
                    OnFilterButtonClick="lnkLoadFilter_Click" />
            </div>

        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
