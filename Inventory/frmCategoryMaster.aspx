﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCategoryMaster.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmCategoryMaster" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 66px;
            max-width: 66px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 66px;
            max-width: 66px;
            text-align: center !important;
        }
        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 66px;
            max-width: 66px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 204px;
            max-width: 204px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 159px;
            max-width: 160px;
           
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 160px;
            max-width: 160px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 165px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 173px;
            max-width: 173px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 164px;
            max-width: 350px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            display: none;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            display: none;
        }

        .Discount {
            margin-left: 438px;
            padding: 10px 10px 0px 10px;
            margin-top: -199px;
        }
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }
              .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">
        var divDis = '0';
        function ValidateForm() {
            try {
                var Show = '';
                var CategoryCode = $.trim($('#<%=txtCateCode.ClientID %>').val());         //22112022 musaraf
                var CategoryName = $.trim($('#<%=txtCateName.ClientID %>').val());

                if ($('#<%=txtCateCode.ClientID%>').is(':disabled') && $('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                    ShowPopupMessageBox("You have no permission to save existing Category");
                    return false;
                }
                if (!($('#<%=txtCateCode.ClientID%>').is(':disabled')) && $('#<%=hidSavebtn.ClientID%>').val() != "N1") {
                    ShowPopupMessageBox("You have no permission to save New Category");
                    return false;
                }
                if (CategoryCode == "") {
                    Show = Show + 'Please Enter Category Code';
                    document.getElementById("<%=txtCateCode.ClientID %>").focus();
                }

                if (CategoryName == "") {
                    Show = Show + '<br />  Please Enter Category Name';
                    document.getElementById("<%=txtCateName.ClientID %>").focus();
                }
                if (document.getElementById("<%=ddlDept.ClientID%>").value == "") {
                    Show = Show + '<br />  Please Enter Department Code';
                    document.getElementById("<%=txtCateName.ClientID %>").focus();
                }
                if (divDis == "1") {

                    if ($("[id*=cblLocation] input:checked").length == 0) {
                        console.log($("[id*=cblLocation] input:checked").length);
                        Show = Show + '<br />Please Enter any one Location';
                    }
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }

                else {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                    return true;
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Toast() {

            $('#<%=divDiscount.ClientID%>').show();
            divDis = '1';

        }
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });



    </script>
    <script type="text/javascript">


        function imgbtnEdit_ClientClick(source) {
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                ShowPopupMessageBox("You have no permission to Edit this Category");
                return false;
            }
            clearForm();
            $('#<%=rdDiscount.ClientID %>').find("input[value='SellingPrice']").prop("checked", true);
            DisplayDetails($(source).closest("tr"));

        }

        function DisplayDetails(row) {
            $('#<%=txtCateCode.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtCateCode.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtCateName.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtDiscount.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=txtRemarks.ClientID %>').val(($("td", row).eq(7).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlDept.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            $('#<%=ddlDept.ClientID %>').trigger("liszt:updated");
            if ($("td", row).eq(10).html() == 'SellingPrice') {
                $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
            }
            else if ($("td", row).eq(10).html() == 'Mrp') {
                $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
            }
            $('#<%=txtBin.ClientID %>').val(($("td", row).eq(10).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
            GetLocation();
                    <%--$('#<%=cblLocation.ClientID %>').val($("td", row).eq(8).html().replace(/&nbsp;/g, ''));--%>
            //var checkboxList = $("[id*=cblLocation]");
            //checkboxList.each(function () {
            //    if ($(this).val() == $("td", row).eq(10).html()) {
            //        $(this).prop("checked", true);
            //    }
            //});
        }
        function GetLocation() {
            //alert('test');
            var catCode = $('#<%=txtCateCode.ClientID %>').val();
            var dis = $('#<%=txtDiscount.ClientID %>').val();
            //alert('test');
            try {
                $.ajax({
                    url: "frmCategoryMaster.aspx/fncGetLoc",
                    data: "{ 'catCode': '" + catCode + "','dis': '" + dis + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        objData = jQuery.parseJSON(msg.d);

                        // debugger;                                                    
                        if (objData.length > 0) {
                            var objPrice = objData[0]["DiscountBasedOn"];
                            if (objPrice == 'SellingPrice') {
                                $('#ContentPlaceHolder1_rdDiscount_0').prop('checked', true);
                            }
                            else if (objPrice == 'Mrp') {
                                $('#ContentPlaceHolder1_rdDiscount_1').prop('checked', true);
                            }
                            var valNew = objData[0]["LocationCode"].split(',');

                            var data = valNew.slice(1, 10000);
                            //console.log($("[id*=cblLocation] input"));
                            $("[id*=cblLocation] input:checkbox").removeAttr("checked");
                            var checkboxList = $("[id*=cblLocation]");
                            var chk = checkboxList.slice(1, 10000);
                            for (var i = 0; i <= data.length; i++) {


                                checkboxList.each(function () {
                                    if ($(this).val() == data[i]) {
                                        $(this).prop("checked", true);
                                    }
                                });
                                // console.log(checkboxList.length);

                            }
                            if (chk.length == data.length) {
                                $("[id*=cbSelectAll]").attr("checked", "checked");
                            }
                        }


                    },
                    error: function (msg) {
                        console.log(msg.message);
                    },
                    failure: function (msg) {
                        console.log(msg.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1" || $('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $(function () {
                try {
                    $("[id*=cbSelectAll]").bind("click", function () {
                        if ($(this).is(":checked")) {
                            $("[id*=cblLocation] input").prop("checked", "checked");
                        } else {
                            $("[id*=cblLocation] input").prop("checked", false);
                        }
                    });
                    $("[id*=cblLocation] input").bind("click", function () {
                        if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                            $("[id*=cbSelectAll]").prop("checked", "checked");
                        } else {
                            $("[id*=cbSelectAll]").prop('checked', false);
                        }
                    });
                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            });
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc

            $('#<%=txtCateCode.ClientID %>').focus();
              $('#<%=txtCateCode.ClientID %>').focusout(function () {
                $.ajax({
                    url: "frmCategoryMaster.aspx/GetExistingCode",
                    data: "{ 'Code': '" + $("#<%=txtCateCode.ClientID%>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var temp = data.d;
                        var strarray = temp.split(',');

                        if (temp != "Null") {

                            $('#<%=txtCateCode.ClientID %>').prop("disabled", true);
                            $('#<%=txtCateCode.ClientID %>').val(strarray[0]);
                            $('#<%=txtCateName.ClientID %>').val(strarray[1]);
                            $('#<%=txtDiscount.ClientID %>').val(strarray[2]);
                            $('#<%=txtRemarks.ClientID %>').val(strarray[4]);
                            $('#<%=ddlDept.ClientID %>').val($.trim(strarray[3]));
                            $('#<%=ddlDept.ClientID %>').trigger("liszt:updated");
                        }

                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            });

            $("[id$=txtCateName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmCategoryMaster.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                minLength: 1
            });
            $("[id$=txtSearch]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "frmCategoryMaster.aspx/GetFilterValue",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[1],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },

                focus: function (event, i) {
                    $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtSearch.ClientID %>').val($.trim(i.item.label));
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkSearchGrid', '');

                    return false;
                },

                minLength: 1
            });

                //$('input[type=text]').bind('change', function () {
                //    if (this.value.match(SpecialChar)) {

                //        ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                //        this.value = this.value.replace(SpecialChar, '');
                //    }
                //});

            }

            function disableFunctionKeys(e) {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                    if (e.keyCode == 115) {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            $('#<%= lnkSave.ClientID %>').click();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
                       <%--  else if (e.keyCode == 119) {
                             $('#<%= lnkClose.ClientID %>').click();
            e.preventDefault();
        }--%>


                }
            };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
    </script>
    <script type="text/javascript">
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupcategory').dialog('close');
                window.parent.$('#popupcategory').remove();

            }
        });

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':checkbox, :radio').prop('checked', false);
            $("input").removeAttr('disabled');
            $('#<%= ddlDept.ClientID %>').val('');
            $('#<%=ddlDept.ClientID %>').trigger("liszt:updated");
        }

    </script>
    <script language="Javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 46 || charCode > 57))
                return false;
            return true;
        }
        function Category_Delete(source) {
            if ($('#<%=hidDeletebtn.ClientID%>').val() != "D1") {
                ShowPopupMessageBox("You have no permission to Delete this Category");
                return false;
            }
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnValue.ClientID %>').val($("td", $(source).closest("tr")).eq(3).html().replace(/&nbsp;/g, ''));

                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

        }

        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'CategoryMaster');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CategoryMaster";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container" style="overflow: hidden;">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Master</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Category Master </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li> 
            </ul>
        </div>
        <%-- <br />--%>

        <asp:UpdatePanel ID="upfrom" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnValue" Value="" runat="server" />
                <div class="container-group-small">
                    <div class="container-control">
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Category Code"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCateCode" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="Category Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCateName" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Discount"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDiscount" Text="0" MaxLength="18" runat="server" CssClass="form-control-res"
                                    onkeypress="return isNumberKey(event)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Remarks"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRemarks" MaxLength="50" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Department"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:DropDownList ID="ddlDept" runat="server" class="chzn-select" CssClass="chzn-select">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-single-res" id="divBin" runat="server" style="display: none">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Bin"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="button-contol">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                    OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save(F4)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClose" runat="server" class="button-red" Style="display: none" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-4 Discount" runat="server" id="divDiscount">
                    <div class="container-group-small" style="width: 100%;">
                        <div class="container-control">
                            <div class="control-group-single-res">

                                <div class="terminal-detail-header">
                                    Discount Based On
                                </div>
                            </div>
                            <div class="control-group-single-res">
                                <div class="col-md-8">
                                    <asp:RadioButtonList ID="rdDiscount" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                        <asp:ListItem Value="SellingPrice" Selected="True">Selling Price</asp:ListItem>

                                        <asp:ListItem style="margin-left: 71px" Value="Mrp">MRP</asp:ListItem>

                                    </asp:RadioButtonList>
                                    <%--<asp:RadioButton ID="rdSellingPrice" runat="server" Text="Selling Price" checked="true" />  
                                    <asp:RadioButton ID="rdMRP" runat="server" Text="MRP" Style="margin-left:71px" />  --%>
                                </div>

                            </div>

                            <div class="control-group-single-res">
                                <%--  <div class="label-left">
                                    <asp:Label ID="Label20" runat="server" Text="Location"></asp:Label>
                                </div>--%>
                                <div class="label-right">

                                    <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                </div>
                                <div class="control-group-single">
                                    <div style="overflow-y: auto; width: 75%; height: 90px">
                                        <asp:CheckBoxList ID="cblLocation" CssClass="cbLocation" runat="server">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="upgrid" runat="Server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="GridDetails">
                  
                        <div class="grid-search">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Category Name To Search"></asp:TextBox>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" Style="visibility: hidden"
                                    OnClick="lnkSearchGrid_Click"><i class="icon-play"></i>Search</asp:LinkButton>
                               <asp:label Id="lblcount" runat="server" style="font-weight: 900;"></asp:label>
                            </asp:Panel>
                        </div>
                    <%--<br />--%>

                    <div class="right-container-top-detail">
                        <div class="GridDetails">
                            <div class="row">
                                <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                    <div class="grdLoad">
                                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Delete
                                                    </th>
                                                    <th scope="col">Edit 
                                                    </th>
                                                     <th scope="col">Serial No 
                                                    </th>
                                                    <th scope="col">Category Code
                                                    </th>
                                                    <th scope="col">Category Name
                                                    </th>
                                                    <th scope="col">Discount Percent
                                                    </th>
                                                    <th scope="col">Ref Dept Code
                                                    </th>
                                                    <th scope="col">Remarks
                                                    </th>
                                                    <th scope="col">Modify User
                                                    </th>
                                                    <th scope="col">Modify Date
                                                    </th>
                                                    <th scope="col">Discount Basedon
                                                    </th>

                                                </tr>
                                            </thead>
                                        </table>
                                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 203px; width: 1320px; background-color: aliceblue;">
                                            <asp:GridView ID="grdCategoryMaster" runat="server" AllowSorting="true" AutoGenerateColumns="false"
                                                ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn grdLoad" oncopy="return false" AllowPaging="false"
                                                PageSize="10" DataKeyNames="CategoryCode" ShowHeader="false">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter " />
                                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png" ToolTip="Click here to Delete"
                                                                OnClientClick="return Category_Delete(this); return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="RowNumber" HeaderText="Serial No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="CategoryCode" HeaderText="Category Code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="CategoryName" HeaderText="Category Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="DiscountPercent" HeaderText="Discount Percent" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundField DataField="RefDeptCode" HeaderText="Ref Dept Code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyUser" HeaderText="Modify User" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modify Date" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                    <asp:BoundField DataField="Bin" HeaderText="Bin" ItemStyle-CssClass="hiddencol"
                                                        HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ups:PaginationUserControl runat="server" ID="categoryPaging" OnPaginationButtonClick="categoryPaging_PaginationButtonClick" />
                </div>
                <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
