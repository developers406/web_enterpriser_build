﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmExportImportPrice.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmExportImportPrice" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 65px;
            max-width: 65px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 103px;
            max-width: 103px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 105px;
            max-width: 105px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 65px;
            max-width: 65px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 85px;
            max-width: 85px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 85px;
            max-width: 85px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 40px;
            max-width: 40px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 45px;
            max-width: 45px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 43px;
            max-width: 43px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 50px;
            max-width: 50px;
        }
         .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
            min-width: 280px;
            max-width: 280px;
        }

        .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            min-width: 280px;
            max-width: 280px;
        }

        .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
            min-width: 325px;
            max-width: 325px;
        }

        .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
            min-width: 195px;
            max-width: 195px;
        }

        .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
            display: none;
        }
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript" language="Javascript">
        $(function () {
            $("#tabs").tabs();
        });
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=InkExport.ClientID %>').css("display", "block");
                    $('#<%=LinkButton5.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=InkExport.ClientID %>').css("display", "none");
                    $('#<%=LinkButton5.ClientID %>').css("display", "none");
                }
                if ($("#<%=TabName.ClientID %>").val() == "Import") {
                    $('#Repack').addClass('active');
                    $('#Regular').removeClass('active');
                }
                if ($('#<%=hidvisible.ClientID%>').val() == "N") {
                    $('#divvisible').css("display", "none");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "Regular";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs ul li a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });

            // For Scrol down in tab
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $("html, body").scrollTop($(document).height());
            })
        });

        //============================> For Maintain Current Tab while postback
        //$(function () {
        //    var tabName = $("[id*=TabName]").val() == "" ? $("[id*=TabName]").val() : "tabs-2";
        //    $('#tabs a[href="#' + tabName + '"]').tab('show');
        //    $("#tabs a").click(function () {
        //        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
        //    });
        //});
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //alert($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.find('input[id *= chkSingle]').prop('checked', true);
                tr.next().find('input[id *= txtNewQty]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function Fun_Check_All(isChecked) {
            $("#<%=chkShortDes.ClientID %>").prop('checked', isChecked);
            $("#<%=chkCategory.ClientID %>").prop('checked', isChecked);
            $("#<%=chkVendor.ClientID %>").prop('checked', isChecked);
            $("#<%=chkItemType.ClientID %>").prop('checked', isChecked);
            $("#<%=chkCost.ClientID %>").prop('checked', isChecked);
            $("#<%=chkDepartment.ClientID %>").prop('checked', isChecked);
            $("#<%=chkBrand.ClientID %>").prop('checked', isChecked);
            $("#<%=chkUom.ClientID %>").prop('checked', isChecked);
            $("#<%=chkMrp.ClientID %>").prop('checked', isChecked);
            $("#<%=chkSelling.ClientID %>").prop('checked', isChecked);


        }
        function fncValidateCheckAll() {

            try {

                if ($("#<%=Check.ClientID %>").text() == "Select All") {

                    $("#<%=Check.ClientID %>").text("De-Select All");
                    Fun_Check_All(true);

                }
                else {

                    $("#<%=Check.ClientID %>").text("Select All");
                    Fun_Check_All(false);
                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateExportNoItems() {
            try {
                var gridtr = $("#<%= gvExport.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems

                    fncShowAlertNoItemsMessage();
                    return false;

                }
                else {
                    
                    //InitializeDialogFormat();
                    //$("#ConfirmSaveDialogFormat").dialog('open');
                    return true;

                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncValidateAllZero() {
            try {
                var gridtr = $("#<%= gvExport.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    $("#<%=gvExport.ClientID %> [id*=txtNewQty]").val('0.000');
                    //alert($("#<%=gvExport.ClientID %> [id*=chkSingle]").is(":checked"));
                }


            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvImport.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    fncShowImportConfirmSaveMessage();
                    //alert($("#<%=gvExport.ClientID %> [id*=chkSingle]").is(":checked"));
                    <%--if ($("#<%=gvExport.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }--%>
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncShowImportConfirmSaveMessage() {
            try {
                InitializeDialogImportConfirmSave();
                $("#ImportConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialogImportConfirmSave() {
            try {
                $("#ImportConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseImportConfirmSave() {
            try {
                $("#ImportConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#StockUpdate").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#StockUpdate").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#StockUpdate").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }


        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialogFormat() {
            try {
                $("#ConfirmSaveDialogFormat").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 312,
                    modal: true,
                    title: "Export",
                    buttons: {
                        Ok: function () {
                            
                            $("#<%=btnok.ClientID %>").click();
                            //$(this).dialog('close');
                        }
                    }
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncCloseSaveDialogformat() {
            try {
                $("#ConfirmSaveDialogFormat").dialog('close');
                return true;
            }
            catch (err) {
                alert(err.message);
            }
        }
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'ExportImportPrice');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "ExportImportPrice";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        function clearForm() {
            //alert("Clear");
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("select").val(0);
            $("select").trigger("liszt:updated");
            //console.log(err);
            //            $(':checkbox, :radio').prop('checked', false);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Price Change Utility</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_Export_Import%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="bottom-purchase-container">
                <div id="Tabs" role="tabpanel">
                    <ul id="tablist" class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liRegular"><a href="#Regular" aria-controls="Regular" role="tab" data-toggle="tab">Export Price</a></li>
                        <li id="liRepack"><a href="#Repack" aria-controls="Repack" role="tab" data-toggle="tab">Import Price</a></li>
                    </ul>
                    <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                        <div class="tab-pane active" role="tabpanel" id="Regular">
                            <div class="gvSelectItemList" style="height: 470px; width: auto">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                                    <ContentTemplate>
                                        <div class="container-group-price">
                                            <div class="container-left-price" id="pnlFilter" runat="server">
                                                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                                                    EnableVendorDropDown="true"
                                                    EnableDepartmentDropDown="true"
                                                    EnableCategoryDropDown="true"
                                                    EnableSubCategoryDropDown="true"
                                                    EnableBrandDropDown="true"
                                                    EnableMerchandiseDropDown="true"
                                                    EnableManufactureDropDown="true"
                                                    EnableFloorDropDown="true"
                                                    EnableSectionDropDown="true"
                                                    EnableBinDropDown="true"
                                                    EnableShelfDropDown="true"
                                                    EnableWarehouseDropDown="true"
                                                    EnableItemCodeTextBox="true"
                                                    EnableItemNameTextBox="true"
                                                    EnableItemTypeDropDown="true"
                                                    EnableGRNNoTextBox="true"
                                                    EnableModelNoTextBox="false"
                                                    EnableFilterButton="true"
                                                    EnableClearButton="true"
                                                    
                                                    OnClearButtonClientClick="clearForm(); return false;"
                                                    OnFilterButtonClick="lnkLoadFilter_Click" />
                                            </div>
                                               
                                            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                                                style="height: 450px !important">
                                                <div class="control-group-split" style="width: 40%; float: left">
                                                    <div class="control-groupbarcode">
                                                        <div style="text-align: center">
                                                            <div>
                                             
                                                                <div style="float: left; margin-left: 10%">
                                                                    <asp:RadioButton ID="rbnBatch" runat="server" Text='<%$ Resources:LabelCaption,lbl_Batch %>'
                                                                        GroupName="barcode" Checked="True" />
                                                                </div>
                                                                <div style="float: left; margin-left: 10%">
                                                                    <asp:RadioButton ID="rbnNonBatch" runat="server" Text='<%$ Resources:LabelCaption,lbl_NonBatch %>'
                                                                        GroupName="barcode" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="col-md-5" style="margin-left: 31px;background:yellowgreen;margin-top: 9px;width:60%" id="divvisible">
                                             
                                                                <div class="col-md-4">
                                                                    <asp:RadioButton ID="rdncsv" runat="server" Text='.csv'
                                                                        GroupName="barcode1" Checked="True" /></div>
                                                                  
                                                                  <div class="col-md-4"> <asp:RadioButton ID="rdnxlsx" runat="server" Text='.xlsx'
                                                                        GroupName="barcode1" /></div> 
      <div class="col-md-4"> <asp:RadioButton ID="rdntxt" runat="server" Text='.txt'
                                                                        GroupName="barcode1" /></div>
                                                               
                                                                </div>
                                                </div>
                                              
                                                <div class="control-button">
                                                    <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                                                        Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                                                </div>
                                              
                                   
                                                <div class="control-group-split" style="width: 45%; float: right">
                                                     
                                                    <div class="control-group-left" style="width: 40%;">
                                                        <div class="label-right" style="width: 10%">
                                                            <asp:CheckBox ID="chkShortDes" runat="server" />
                                                        </div>
                                                        <div class="label-left" style="width: 55%">
                                                            <asp:Label ID="Label32" runat="server">Short Des</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkCategory" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label31" runat="server">Category</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkVendor" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label1" runat="server">Vendor</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 40%;">
                                                        <div class="label-right" style="width: 10%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkItemType" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 50%">
                                                            <asp:Label ID="Label3" runat="server">Item Type</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkCost" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label4" runat="server">Cost</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkDepartment" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label5" runat="server">Department</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 40%;">
                                                        <div class="label-right" style="width: 10%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkBrand" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label6" runat="server">Brand</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkUom" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label7" runat="server">UOM</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 27%;">
                                                        <div class="label-right" style="width: 15%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkMrp" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 40%">
                                                            <asp:Label ID="Label8" runat="server">MRP</asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="control-group-left" style="width: 40%;">
                                                        <div class="label-right" style="width: 10%">
                                                            <fieldset>
                                                                <asp:CheckBox ID="chkSelling" runat="server" />
                                                            </fieldset>
                                                        </div>
                                                        <div class="label-left" style="width: 50%">
                                                            <asp:Label ID="Label9" runat="server">Selling Price</asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--<div style="float: right; display: table">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="conditional">
                                <ContentTemplate>
                                    <div style="float: left">
                                        <div class="control-button">
                                            <asp:FileUpload ID="FileUpload1" runat="server" />
                                        </div>
                                    </div>
                                    <div style="float: right">
                                        <div class="control-button">
                                            <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" class="button-blue"
                                                Text="Import Data" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnImport" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>--%>
                                                <%--<div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                                                style="height: 410px">--%>

                                                <div class="right-container-top-detail">
                                                    <div class="GridDetails">
                                                        <div class="row">
                                                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                                <div class="grdLoad">
                                                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">Item Code
                                                                                </th>
                                                                                <th scope="col">Description
                                                                                </th>
                                                                                <th scope="col">ShortDescription
                                                                                </th>
                                                                                <th scope="col">BatchNo
                                                                                </th>
                                                                                <th scope="col">Department
                                                                                </th>
                                                                                <th scope="col">Category 
                                                                                </th>
                                                                                <th scope="col">Brand
                                                                                </th>
                                                                                <th scope="col">Vendor
                                                                                </th>
                                                                                <th scope="col">UOM
                                                                                </th>
                                                                                <th scope="col">Type
                                                                                </th>
                                                                                <th scope="col">MRP
                                                                                </th>
                                                                                <th scope="col">Cost
                                                                                </th>
                                                                                <th scope="col">S.Price
                                                                                </th>
                                                                                <th scope="col">ModelNo
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                    <div class="GridDetails" style="overflow-x: scroll; overflow-y: scroll; height: 320px; width:985px; background-color: aliceblue;">
                                                                        <asp:GridView ID="gvExport" runat="server" AutoGenerateColumns="False"
                                                                            ShowHeaderWhenEmpty="true" ShowHeader="false" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                                            <%--<EmptyDataTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                                        </EmptyDataTemplate>--%>
                                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                                            <PagerStyle CssClass="pshro_text" />
                                                                            <Columns>
                                                                                <%-- <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                                                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                                                <asp:BoundField DataField="ShortDescription" HeaderText="ShortDescription"></asp:BoundField>
                                                                                <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                                                                <asp:BoundField DataField="DepartmentCode" HeaderText="Department"></asp:BoundField>
                                                                                <asp:BoundField DataField="CategoryCode" HeaderText="Category"></asp:BoundField>
                                                                                <asp:BoundField DataField="BrandCode" HeaderText="Brand"></asp:BoundField>
                                                                                <asp:BoundField DataField="VendorName" HeaderText="Vendor"></asp:BoundField>
                                                                                <asp:BoundField DataField="UOM" HeaderText="UOM"></asp:BoundField>
                                                                                <asp:BoundField DataField="ItemType" HeaderText="Item Type" />
                                                                                <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-HorizontalAlign="Right" />
                                                                                <asp:BoundField DataField="Cost" HeaderText="Cost" ItemStyle-HorizontalAlign="Right" />
                                                                                <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right" />
                                                                                <asp:BoundField DataField="Model" HeaderText="ModelNO" ItemStyle-HorizontalAlign="Right" />
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div style="float: right; display: table;">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClick="lnkFilterOption_Click"
                                            Text='<%$ Resources:LabelCaption,btn_Hide_Filter %>'></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="InkExport" runat="server" class="button-blue" OnClick="btnExportToExcel_Click"
                                            OnClientClick="return fncValidateExportNoItems();" Text='<%$ Resources:LabelCaption,btn_Export %>'></asp:LinkButton>
                                    </div>
                                    <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>OnClick="btnExportToExcel_Click"--%>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="Repack">
                            <div class="GridDetails" style="height: 480px; width: auto">
                                <div style="float: left; display: table">
                                    <div class="control-button">
                                        <asp:LinkButton ID="LinkButton6" runat="server" class="button-blue" OnClick="btnExportImportToExcel_Click">Export Sample</asp:LinkButton>
                                    </div>
                                </div>
                                <div style="float: right; display: table">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="conditional">
                                        <ContentTemplate>
                                            <div style="float: left">
                                                <div class="control-button">
                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                </div>
                                            </div>
                                            <div style="float: right">
                                                <div class="control-button">
                                                    <asp:Button ID="btnImport" runat="server" OnClick="btnImport_Click" class="button-blue"
                                                        Text="Import Data" />
                                                </div>
                                            </div>
                                     
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnImport" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="right-container-top-detail">
                                    <div class="GridDetails">
                                        <div class="row">
                                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                                <div class="grdLoad1">
                                                    <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Location Code
                                                                </th>
                                                                <th scope="col">Inventory Code
                                                                </th>
                                                                <th scope="col">Description
                                                                </th>
                                                                <th scope="col">MRP
                                                                </th>
                                                                <th scope="col">Selling Price
                                                                </th>
                                                                <th scope="col">Barcode 
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 340px; width: 1300px; background-color: aliceblue;">
                                                        <%--<div class="gridDetails grid-overflow" id="Div1" runat="server" style="height: 420px">--%>
                                                        <asp:GridView ID="gvImport" runat="server" AutoGenerateColumns="False"
                                                            ShowHeaderWhenEmpty="true" ShowHeader="false" EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                                            <%--<EmptyDataTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                            </EmptyDataTemplate>--%>
                                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                            <PagerStyle CssClass="pshro_text" />
                                                            <Columns>
                                                                <%-- <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                                                <asp:BoundField DataField="Location Code" HeaderText="Location Code"></asp:BoundField>
                                                                <asp:BoundField DataField="Inventory Code" HeaderText="Inventory Code"></asp:BoundField>
                                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                                <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-HorizontalAlign="Right" />
                                                                <asp:BoundField DataField="Selling Price" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right" />
                                                                <asp:BoundField DataField="Barcode" HeaderText="Barcode" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-5">
                                        <label style="color: White; background-color: OrangeRed;">XXXX</label>
                                        <label style="font-weight: 700;">Sellingprice cannot be greater than Mrp</label>
                                    </div>

                                    <div class="col-md-1">
                                        <div class="control-button">
                                            <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="fncValidateSave();return false;">Save</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
                            </div>
                        </div>
                        <asp:HiddenField ID="TabName" runat="server" Value="" />
                    </div>
                </div>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div class="modal-loader">
                            <div class="center-loader">
                                <img alt="" src="../images/loading_spinner.gif" />
                            </div>
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <div class="container-group-full" style="display: none">
                    <div id="StockUpdate">
                        <p>
                            <%=Resources.LabelCaption.Alert_Select_Any%>
                        </p>
                        <div style="margin: auto; width: 100px">
                            <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                                Text='<%$ Resources:LabelCaption,lblOk %>'>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="container-group-full" style="display: none">
                    <div id="ConfirmSaveDialog">
                        <p>
                            <%=Resources.LabelCaption.Alert_Confirm_Save%>
                        </p>
                        <div style="width: 150px; margin: auto">
                            <div style="float: left">
                                <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                                    Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                                    Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                      
                    </div>

                   
               
                <div style="display:none">
                    <asp:Button ID="btnok" runat="server" OnClick="btnExportToExcel_Click" />
                    
                </div>
                    <div id="ImportConfirmSaveDialog" style="display:none">
                        <p>
                            <%=Resources.LabelCaption.Alert_Confirm_Save%>
                        </p>
                        <div style="width: 150px; margin: auto">
                            <div style="float: left">
                                <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return CloseImportConfirmSave()"
                                    Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkImportsave_click"> </asp:LinkButton>
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton8" runat="server" class="button-blue" OnClientClick="return CloseImportConfirmSave() "
                                    Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-group-full" style="display: none">
                    <div id="StockUpdateSave">
                        <p>
                            <%=Resources.LabelCaption.Alert_Save%>
                        </p>
                        <div style="margin: auto; width: 100px">
                            <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                                Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
                <div class="container-group-full" style="display: none">
                    <div id="AlertNoItems">
                        <p>
                            <%=Resources.LabelCaption.Alert_No_Items%>
                        </p>
                        <div style="margin: auto; width: 100px">
                            <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                                Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidvisible" runat="server" />
</asp:Content>
