﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInventoryGstChange.aspx.cs" EnableEventValidation="false"
    Inherits="EnterpriserWebFinal.Inventory.frmInventoryGstChange" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link type="text/css" href="../css/inventory_gst_change.css" rel="stylesheet" />
    <script type="text/javascript">
        function pageLoad() {
            try {
                $(document).on('keydown', disableFunctionKeys);
                $("#<%=txtExpectedDays.ClientID %>").val();
                $("#<%= txtExpectedDays.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                $("#<%= txtExpectedDays.ClientID %>").on('keydown', function () {
                    return false;
                });
                //$("#ExpectedDate").css("display", "none");
                $("#<%=chkExpected.ClientID %>").click(function () {
                    var checked = $(this).is(':checked');
                    if (checked == true) {
                        $("#ExpectedDate").css("display", "block");
                    }
                    else {
                        $("#ExpectedDate").css("display", "none");
                    }
                });
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                }
                if ($('#<%=hidWholesale.ClientID%>').val() != "Y") {
                    $('.grdLoad th:nth-child(11)').css("display", "none");
                    $('.grdLoad th:nth-child(12)').css("display", "none");
                    $('.grdLoad th:nth-child(13)').css("display", "none");
                    $('.grdLoad th:nth-child(14)').css("display", "none");
                    $('.grdLoad th:nth-child(15)').css("display", "none");
                    $('.grdLoad th:nth-child(16)').css("display", "none");
                    $('.grdLoad td:nth-child(11)').css("display", "none");
                    $('.grdLoad td:nth-child(12)').css("display", "none");
                    $('.grdLoad td:nth-child(13)').css("display", "none");
                    $('.grdLoad td:nth-child(14)').css("display", "none");
                    $('.grdLoad td:nth-child(15)').css("display", "none");
                    $('.grdLoad td:nth-child(16)').css("display", "none");
                    $('#<%=HideFilter_GridOverFlow.ClientID %>').css("width", "3365px");
                }

                var newOption = {};
                var option = '';
                $("#<%=ddlSort.ClientID %>").empty();
                $('#tblItemhistory th').each(function (e) {
                    var index = $(this).index();
                    var table = $(this).closest('table');
                    var val = table.find('.click th').eq(index).text();

                    if ($(this).is(":visible")) {
                        option += '<option value="' + val + '">' + val + '</option>';
                    }
                });
                $("#<%=ddlSort.ClientID %>").append(option);
                $("#<%=ddlSort.ClientID %>").trigger("liszt:updated");
                $("select").chosen({ width: '100%' });

                $('#lblUp').live('click', function (event) {
                    $('#lblDown').css("color", "");
                    $('#lblUp').css("color", "green");
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex; 
                    if (columnIndex > 0) {
                        var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                        if ($('#<%=hidWholesale.ClientID%>').val() == "Y") {
                            if (columnIndex == 1 || columnIndex == 5 || columnIndex == 6 || columnIndex == 7
                                     || columnIndex == 8 || columnIndex == 9 || columnIndex == 10 || columnIndex == 11
                                     || columnIndex == 12 || columnIndex == 13 || columnIndex == 14 || columnIndex == 15
                                     || columnIndex == 16 || columnIndex == 17) {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text());
                                    var nData = $.trim($(n).text());
                                    if (pData == "")
                                        pData = 0;
                                    if (nData == "")
                                        nData = 0;
                                    return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                                });
                            }
                            else {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text().toUpperCase());
                                    var nData = $.trim($(n).text().toUpperCase());
                                    return pData < nData ? -1 : 1;
                                });
                            }

                        }
                        else {
                            if (columnIndex == 1 || columnIndex == 5 || columnIndex == 6 || columnIndex == 7
                                     || columnIndex == 8 || columnIndex == 9 || columnIndex == 10 || columnIndex == 11) {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text());
                                    var nData = $.trim($(n).text());
                                    if (pData == "")
                                        pData = 0;
                                    if (nData == "")
                                        nData = 0;
                                    return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                                });
                            }
                            else {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text().toUpperCase());
                                    var nData = $.trim($(n).text().toUpperCase());
                                    return pData < nData ? -1 : 1;
                                });
                            }
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#<%=grdItemDetails.ClientID%>').append(row);
                        });
                    }
                    else {
                        $('#lblDown').css("color", "");
                        $('#lblUp').css("color", "");
                        ShowPopupMessageBox("Please select Altlest one Filter");
                        return false;
                    }


                });
                $('#lblDown').live('click', function (event) {
                    $('#lblDown').css("color", "green");
                    $('#lblUp').css("color", "");
                    var columnIndex = $('#<%=ddlSort.ClientID %>').get(0).selectedIndex;
                    var tdArray = $('#<%=grdItemDetails.ClientID%>').closest("table").find("tr td:nth-child(" + (columnIndex + 1) + ")");
                    if (columnIndex > 0) {
                        if ($('#<%=hidWholesale.ClientID%>').val() == "Y") {
                            if (columnIndex == 1 || columnIndex == 5 || columnIndex == 6 || columnIndex == 7
                                         || columnIndex == 8 || columnIndex == 9 || columnIndex == 10 || columnIndex == 11
                                         || columnIndex == 12 || columnIndex == 13 || columnIndex == 14 || columnIndex == 15
                                         || columnIndex == 16 || columnIndex == 17) {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text());
                                    var nData = $.trim($(n).text());
                                    if (pData == "")
                                        pData = 0;
                                    if (nData == "")
                                        nData = 0;
                                    return parseFloat(pData) > parseFloat(nData) ? -1 : 1;
                                });
                            }
                            else {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text().toUpperCase());
                                    var nData = $.trim($(n).text().toUpperCase());
                                    return pData > nData ? -1 : 1;
                                });
                            }
                        }
                        else {
                            if (columnIndex == 1 || columnIndex == 5 || columnIndex == 6 || columnIndex == 7
                                     || columnIndex == 8 || columnIndex == 9 || columnIndex == 10 || columnIndex == 11) {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text());
                                    var nData = $.trim($(n).text());
                                    if (pData == "")
                                        pData = 0;
                                    if (nData == "")
                                        nData = 0;
                                    return parseFloat(pData) < parseFloat(nData) ? -1 : 1;
                                });
                            }
                            else {
                                tdArray.sort(function (p, n) {
                                    var pData = $.trim($(p).text().toUpperCase());
                                    var nData = $.trim($(n).text().toUpperCase());
                                    return pData < nData ? -1 : 1;
                                });
                            }
                        }
                        tdArray.each(function () {
                            var row = $(this).parent();
                            $('#<%=grdItemDetails.ClientID%>').append(row);
                        });
                    }
                    else {
                        $('#lblDown').css("color", "");
                        $('#lblUp').css("color", "");
                        ShowPopupMessageBox("Please select Altlest one Filter");
                        return false;
                    }

                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keypress', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkUpdate.ClientID %>').is(":visible"))
                        fncUpdate();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) { 
                    clearForm();
                    e.preventDefault();
                }
                else if (e.keyCode == 118) {
                    alert('');
                    fncHideFilter();
                    e.preventDefault();
                }
                else if (e.keyCode == 112) {
                    e.preventDefault();
                }

            }
        };

        


        function fncHideFilter() {
            try {

                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
            $("[id*=pnlFilter]").hide();
            $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
            $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:100%');
            $("select").trigger("liszt:updated");
        }
        else {
            $("[id*=pnlFilter]").show();
            $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
            $("#<%=HideFilter_ContainerRight.ClientID%>").attr('style', 'width:74%');
            $("select").trigger("liszt:updated");
        }

        return false;
    }
    catch (err) {
        return false;
        alert(err.Message);
    }
}
function clearForm() {
    try {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("select").val(0);
        $("select").trigger("liszt:updated");
        $('#<%=grdItemDetails.ClientID%>').remove();
    }
    catch (err) {
        alert(err.Message);
        return false;
    }
}
function clearFormFilter() {
    try {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("select").val(0);
        $("select").trigger("liszt:updated");
    }
    catch (err) {
        ShowPopupMessageBox(err.Message);
        return false;
    }
}

function fncApplyGst() {
    try {
        if ($("#<%=grdItemDetails.ClientID %> tbody tr").length > 0) {
            $("#<%=grdItemDetails.ClientID %> tbody tr").each(function () {
                $(this).find("td").eq(4).html($('#<%=ddlIgst.ClientID %> option:selected').val());
            });
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.Message);
    }
}

function fncUpdate() {
    try {
        var check = "0";
        if ($("#<%=grdItemDetails.ClientID %> tbody tr").length > 0) {
            var xml = '<NewDataSet>';
            $("#<%=grdItemDetails.ClientID %> tbody tr").each(function () {
                        var obj = $(this);

                        if (obj.closest('tr').find('input[type="checkbox"]').is(':checked')) {
                            check = "1";
                            xml += "<Table>";
                            xml += "<Inventorycode>" + obj.find("td").eq(2).html().trim() + "</Inventorycode>";
                            xml += "<Description>" + obj.find("td").eq(3).html().trim().replace('>', ')').replace('<', '(') + "</Description>";
                            xml += "<Mrp>" + obj.find("td").eq(5).html().trim() + "</Mrp>";
                            xml += "<oldGST>" + obj.find("td").eq(32).html().trim() + "</oldGST>";
                            xml += "<NewGst>" + obj.find("td").eq(4).html().trim() + "</NewGst>";
                            xml += "<SellingPrice>" + obj.find("td").eq(9).html().trim() + "</SellingPrice>";
                            xml += "<NetCost>" + obj.find("td").eq(7).html().trim() + "</NetCost>";
                            xml += "</Table>";
                        }
                    });
                    xml = xml + '</NewDataSet>'
                    xml = escape(xml);
                    if (check == "1") {
                        $('#<%=GSTXmldata.ClientID %>').val(xml);
                        return true;
                    }
                    else {
                        ShowPopupMessageBox("Please Select any one Item");
                        return false;
                    }
                }
                else {
                    ShowPopupMessageBox("Please Enter valid Details");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function checkAll(objRef) {
            try {
                var GridView = objRef.parentNode.parentNode.parentNode;
                var inputList = GridView.getElementsByTagName("input");
                for (var i = 0; i < inputList.length; i++) {
                    var row = inputList[i].parentNode.parentNode;
                    if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                        if (objRef.checked) {
                            row.style.backgroundColor = "#959382";
                            inputList[i].checked = true;
                        }
                        else {
                            if (row.rowIndex % 2 == 0) {
                                row.style.backgroundColor = "#c4ddff";
                            }
                            else {
                                row.style.backgroundColor = "white";
                            }
                            inputList[i].checked = false;
                        }
                    }
                }

            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function Check_Click(objRef, val) {
            try {
                var row = objRef.parentNode.parentNode;
                if (val == 'row') {
                    objRef.checked = true;
                }
                if (objRef.checked) {
                    row.style.backgroundColor = "#acb3a4";
                }
                else {
                    if (row.rowIndex % 2 == 0) {
                        row.style.backgroundColor = "#c4ddff";
                    }
                    else {
                        row.style.backgroundColor = "white";
                    }
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncVenItemRowdblClk(rowObj) {
            try {
                fncOpenItemhistory($.trim(rowObj));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        /// Open Item History
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function SaveMsg() {
            try {
                clearForm();
                ShowPopupMessageBox("Saved Successfully");
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

         function fncSelectUnselectrow(event) {
            pricevalidationstatus = "All";
            try {
                if (($(event).is(":checked"))) {
                    $("#<%=grdItemDetails.ClientID %> tbody tr").each(function () {
                        $(this).find('td input[id*="CheckBox1"]').attr("checked", "checked");
                    }); 
                }
                else { 
                    $("#<%=grdItemDetails.ClientID%> input[id*='CheckBox1']:checkbox").removeAttr("checked");

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'InventoryGSTChange');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "InventoryGSTChange";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Inventory</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">GST Change</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                    EnableVendorDropDown="true"
                    EnableDepartmentDropDown="true"
                    EnableCategoryDropDown="true"
                    EnableSubCategoryDropDown="true"
                    EnableBrandDropDown="true"
                    EnableClassDropDown="true"
                    EnableSubClassDropDown="true"
                    EnableMerchandiseDropDown="true"
                    EnableManufactureDropDown="true"
                    EnableFloorDropDown="true"
                    EnableSectionDropDown="true"
                    EnableBinDropDown="true"
                    EnableShelfDropDown="true"
                    EnableWarehouseDropDown="true"
                    EnableItemTypeDropDown="false"
                    EnablePriceTextBox="true"
                    EnableItemCodeTextBox="true"
                    EnableItemNameTextBox="true"
                    EnableFilterButton="true"
                    EnableClearButton="true"
                    EnableGstDropDown="true"
                    OnClearButtonClientClick="clearFormFilter(); return false;"
                    OnFilterButtonClick="lnkLoadFilter_Click" />

            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <div class="col-md-12 sort_by">
                    <div class="col-md-3 sort_by">
                        <div class="col-md-4">
                            <label id="lblGST" style="margin-top: 5px;">GST %</label>
                        </div>
                        <div class="col-md-8">
                            <asp:DropDownList runat="server" Width="100%" ID="ddlIgst"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top: 5px;display:none;">
                        <asp:CheckBox ID="chkExpected" runat="server" Checked ="true" Text="Expected Date" Class="radioboxlistgreen" />
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-4" id="ExpectedDate" style="margin-top: 5px;">
                            <asp:TextBox ID="txtExpectedDays" Width="100%" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-8">
                            <asp:Button ID="lnkAdd" runat="server" class="button-red" Text="Apply" OnClientClick="fncApplyGst();return false;" />
                        </div>
                    </div>
                    <div class="col-md-3 sort_by">
                        <div class="col-md-3">
                            <label id="lblSortby" style="margin-top: 5px;">Sort by:</label>
                        </div>
                        <div class="col-md-7">
                            <asp:DropDownList runat="server" Width="100%" ID="ddlSort"></asp:DropDownList>
                        </div>
                        <div class="col-md-1">
                            <label id="lblUp" style="font-size: large; color: green; cursor: pointer; font-weight: bold;">&#8657</label>
                        </div>
                        <div class="col-md-1">
                            <label id="lblDown" style="font-size: large; cursor: pointer; font-weight: bold;">&#8659</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="grdLoad">
                        <div class="row" style="overflow: auto;">
                            <table id="tblItemhistory" cellspacing="0" rules="all" border="1">
                                <thead>
                                    <tr class="click" style="background-color: #4163e1; color: white;">
                                        <th class="fixed-side" scope="col">
                                            <asp:CheckBox ID="checkAll" onclick="fncSelectUnselectrow(this)" runat="server"  />
                                        </th>
                                        <th class="fixed-side" scope="col">S.No</th>
                                        <th class="fixed-side" scope="col">ItemCode
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                        <th scope="col">GST
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th scope="col">M.D Per
                                        </th>
                                        <th scope="col">NetCost
                                        </th>
                                        <th scope="col">E.Margin
                                        </th>
                                        <th scope="col">S.Price
                                        </th>
                                        <th scope="col">Wprice1
                                        </th>
                                        <th scope="col">Wprice2
                                        </th>
                                        <th scope="col">Wprice3
                                        </th>
                                        <th scope="col">MPWprice1
                                        </th>
                                        <th scope="col">MPWprice2
                                        </th>
                                        <th scope="col">MPWprice3
                                        </th>
                                        <th scope="col">QtyonHand
                                        </th>
                                        <th scope="col">UnitCost
                                        </th>
                                        <th scope="col">Itemtype
                                        </th>
                                        <th scope="col">Department
                                        </th>
                                        <th scope="col">Category 
                                        </th>
                                        <th scope="col">Brand
                                        </th>
                                        <th scope="col">Vendor
                                        </th>
                                        <th scope="col">Merchandise 
                                        </th>
                                        <th scope="col">S.Category 
                                        </th>
                                        <th scope="col">Manufacturer
                                        </th>
                                        <th scope="col">WareHouse
                                        </th>
                                        <th scope="col">Origin
                                        </th>
                                        <th scope="col">Floor
                                        </th>
                                        <th scope="col">FSection
                                        </th>
                                        <th scope="col">BinNo
                                        </th>
                                        <th scope="col">Shelf
                                        </th>
                                        <th scope="col">OldGst
                                        </th>
                                        <th scope="col">Status
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div id="HideFilter_GridOverFlow" runat="server" style="overflow-x: hidden; overflow-y: scroll; height: 380px; width: 3825px;">
                                <asp:GridView ID="grdItemDetails" runat="server" AutoGenerateColumns="true" ShowHeader="false" OnRowDataBound="grdItemDetails_RowDataBound"
                                    ShowHeaderWhenEmpty="True">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <PagerStyle CssClass="pshro_text" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <Columns>
                                        <asp:TemplateField ItemStyle-CssClass="FrozenCell">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" onclick="Check_Click(this,'click')" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        PageButtonCount="5" Position="Bottom" />
                                    <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-bottom-invchange">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilterOption" runat="server" OnClientClick=" return fncHideFilter()"
                            class="button-blue" Text="Hide Filter"> </asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClick="lnkUpdate_Click" OnClientClick="return fncUpdate();"
                            Text="Update"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="clearForm();return false;"
                            Text="Clear"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkView" runat="server" class="button-red" PostBackUrl="~/Inventory/frmInventoryGstChangeEdit.aspx"
                            Text="View"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="hiddencol">
                <asp:HiddenField ID="hidSavebtn" runat="server" />
                <asp:HiddenField ID="hidDeletebtn" runat="server" />
                <asp:HiddenField ID="hidEditbtn" runat="server" />
                <asp:HiddenField ID="hidViewbtn" runat="server" />
                <asp:HiddenField ID="hidWholesale" runat="server" />
                <asp:HiddenField ID="GSTXmldata" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
