﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmWatchPriceList.aspx.cs" Inherits="EnterpriserWebFinal.Inventory.frmWatchPriceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .grdBody td {
            padding: 3px;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 310px;
            max-width: 310px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td {
            padding: 2px;
        }

        .cssGrid {
            overflow-x: hidden;
            overflow-y: scroll;
            height: 375px;
            width: 1390px;
            background-color: aliceblue;
        }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            try { 
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
                if ($('#<%=hidWholeSale.ClientID%>').val() == "N") {
                    $('.grdLoad td:nth-child(10)').css("display", "none");
                    $('.grdLoad th:nth-child(10)').css("display", "none");
                    $('.grdLoad td:nth-child(11)').css("display", "none");
                    $('.grdLoad th:nth-child(11)').css("display", "none");
                    $('.grdLoad td:nth-child(12)').css("display", "none");
                    $('.grdLoad th:nth-child(12)').css("display", "none");
                    $('.cssGrid').css("width", "1165");
                    $('.fixed_headers').css("width", "72%");
                }
                else {
                    $('.fixed_headers').css("width", "91%");
                    $('.grdLoad').css({
                        'width': '990px',
                        'height': '400px',
                        'overflow-x': 'scroll',
                        'overflow-y': 'hidden'
                    });
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            var functionKeys = new Array(40, 38, 13, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '100%',
                        'margin-left': '0'
                    });

                    $("select").trigger("liszt:updated"); 
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '74%',
                        'margin-left': '0% !important'
                    });  
                    $("select").trigger("liszt:updated"); 
                }
                return false;
            }
            catch (err) {
                alert(err.Message);
            }
        }

        function clearForm() {
            try {
                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtItemName.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtFloor.ClientID %>').val('');
                $('#<%= txtShelf.ClientID %>').val('');
                $('#<%= txtBin.ClientID %>').val('');
                $('#<%= txtMerchandise.ClientID %>').val('');
                $('#<%= txtManufacture.ClientID %>').val('');
                $('#<%= txtSection.ClientID %>').val('');
                $(':checkbox, :radio').prop('checked', false);
                $("select").trigger("liszt:updated");
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
        }

        function fncVenItemRowdblClk(itemcode) {
            try {
                fncOpenItemhistory($.trim(itemcode));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }

        function fncSelectUnselectrow(event) {
            pricevalidationstatus = "All";
            try {
                if (($(event).is(":checked"))) {
                    $("#<%=grdPricewatch.ClientID %> tbody tr").each(function () {
                        $(this).find('td input[id*="chkSingle"]').attr("checked", "checked");
                    }); 
                }
                else { 
                    $("#<%=grdPricewatch.ClientID%> input[id*='chkSingle']:checkbox").removeAttr("checked");

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblNewSell_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1; 
                var sSellingPrice = row.cells[7].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                if (sSellingPrice == "" || parseFloat(sSellingPrice) <= 0) {
                    row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                    return ;
                }
                if (sSellingPrice != "") {
                    if (parseFloat(sSellingPrice) > parseFloat(sMRP)) 
                    {
                        row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML; 
                        row.cells[0].getElementsByTagName("input")[0].checked = false; 
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to Sellinprice", "#ContentPlaceHolder1_grdPricewatch_txtNewSelling_" + rowIndex);

                        return;
                    } 
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;    
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblNewSell_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); 
            var row = lnk.parentNode.parentNode; 
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtNewSelling"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                row.cells[6].find('td input[id*="txtNewSelling"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtNewSelling"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtNewSelling"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtNewSelling"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtNewSelling"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtNewSelling"]').select();
                }
            }
            return true;


        }

         function fncSaveValidation() {
             if ($(".cbLocation").find("input:checked").length == 0) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Loc%>');
                    return false;
             }
             else {
                 return true;
             }
        }
    </script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'WatchPriceList');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "WatchPriceList";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Price Change Utility</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Price Watch List </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">  
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="control-group-price-header">
                            Filtertions
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtMerchandise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchandise', 'txtManufacture');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" OnClick="lnkLoadFilter_Click"
                                    Text='<%$ Resources:LabelCaption,btn_Filter %>'></i></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm(); return false;"
                                    Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <asp:UpdatePanel ID="UpdatePanel2" runat="Server"> 
                    <ContentTemplate>
                        <div class="gridDetails">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="control-group-price-header">
                                        Locations
                                    </div>
                                    <div class="control-group-single">
                                        <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                    </div>
                                    <div class="control-group-single">
                                        <div style="overflow-y: auto; width: 260px; height: 40px; margin-bottom: 10px">
                                            <asp:CheckBoxList ID="cblLocation" runat="server" CssClass="cbLocation">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9"></div>
                            </div>
                            <div class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: hidden; height: 370px; background-color: aliceblue;"
                                id="HideFilter_GridOverFlow" runat="server">
                                <div class="grdLoad">
                                    <div class="col-md-12">
                                        <table id="tblGrd" cellspacing="0" rules="all" border="1" class="fixed_headers">
                                            <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <asp:CheckBox ID="cbProfilterheader"  onclick="fncSelectUnselectrow(this)" runat="server" /></th>
                                                    <th scope="col">S.No</th>
                                                    <th scope="col">ItemCode</th>
                                                    <th scope="col">Description</th>
                                                    <th scope="col">Batch</th>
                                                    <th scope="col">MRP</th>
                                                    <th scope="col">Curr.Price</th>
                                                    <th scope="col">New Price</th>
                                                    <th scope="col">Net Cost</th>
                                                    <th scope="col">WPrice1</th>
                                                    <th scope="col">WPrice2</th>
                                                    <th scope="col">WPrice3</th>
                                                    <th scope="col">Basic Cost</th>
                                                    <th scope="col">Gross Cost</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class=" col-md-12 cssGrid" id="divGrd2" runat="server">
                                        <asp:GridView ID="grdPricewatch" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true"
                                            OnRowDataBound="grdItemDetails_RowDataBound">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSingle" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNo" HeaderText="SNO" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Item Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="BatchNo" HeaderText="Batch No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Mrp" HeaderText="MRP" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:TemplateField HeaderText="New SellingPrice">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lnlNewSelling" runat="server" Text='<%# Eval("SellingPrice") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtNewSelling" runat="server" Text='<%# Eval("SellingPrice") %>' onkeyup="return txtlblNewSell_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus ="this.select();"
                                                            CssClass="grid-textbox" onchange="txtlblNewSell_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Netcost" HeaderText="Net Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="WPrice1" HeaderText="W1Price" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="WPrice2" HeaderText="W2Price"></asp:BoundField>
                                                <asp:BoundField DataField="WPrice3" HeaderText="W3Price" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="BasicCost" HeaderText="Basic cost"></asp:BoundField>
                                                <asp:BoundField DataField="Grosscost" HeaderText="Gross cost" ItemStyle-HorizontalAlign="Right" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <div class="col-md-7">
                            </div>
                            <div class="col-md-5">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Update %>'
                                       OnClientClick ="return fncSaveValidation();"  OnClick ="lnkUpdate_Click"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>' OnClick ="lnkClear_CLick"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClose" runat="server" PostBackUrl="../Masters/frmMain.aspx" class="button-blue" Text="Close"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidWholeSale" runat="server" />
</asp:Content>
