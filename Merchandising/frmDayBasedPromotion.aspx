﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmDayBasedPromotion.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmDayBasedPromotion" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 190px;
            max-width: 190px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 520px;
            max-width: 520px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(4) {
            text-align: center;
        }

        .grdLoad td:nth-child(3) {
            text-align: right;
        }

        .radioclass {
            font-weight: 100;
        }

        .textboxPromotion {
            width: 80% !important;
        }

        .clockpicker {
            text-align: center;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'DayBasedPromotion');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "DayBasedPromotion";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.min.js"></script>
    <script type="text/javascript">
        var itmcode;
        var vendor;
        var dept;
        var cat;
        var sub;
        var brand;
        var shelf;
        var floor;
        var merchendise;
        var manufacture;
        var secton;
        var bin;
        var barcode;
        var batchno;
        var grnno;
        var row;
        var selectedValue;
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }
                if ($('#<%=hidEdit.ClientID%>').val() != "") {
                    $('#<%=divActive.ClientID%>').show();
                    if ($("#<%= txtFromDate.ClientID %>").val() == "")
                        $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                    if ($("#<%= txtToDate.ClientID %>").val() == "")
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                } else {
                    $('#<%=divTime.ClientID%>').hide();
                    if ($("#<%= txtFromDate.ClientID %>").val() == "")
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                    if ($("#<%= txtToDate.ClientID %>").val() == "")
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                }
                $("select").chosen({ width: '100%' });
                $('#<%=txtDiscount.ClientID %>').number(true, 2);
                $(".numberDay").number(true, 2);
                $("[id*=chIntime]").click(function () {
                    if ($(this).is(":checked")) {
                        $('#<%=divTime.ClientID%>').show();
                    } else {
                        $('#<%=divTime.ClientID%>').hide();
                    }
                });
                $("#<%= txtFromtime.ClientID %>").timepicker({
                    'timeFormat': 'H:i',
                    'minTime': '05:00',
                    'maxTime': '00:00',
                    'showDuration': true
                });
                $("#<%= txtTotime.ClientID %>").timepicker({
                    'timeFormat': 'H:i',
                    'minTime': '05:00',
                    'maxTime': '00:00',
                    'showDuration': true
                });
                $("#<%= txtTotime.ClientID %>").focusout(function () {
                    Compare();

                });
                $("#<%= txtFromtime.ClientID %>").focusout(function () {
                    Compare();

                });
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
                SetQualifyingItemAutoComplete();
                $("#<%= gvDayBasedPr.ClientID %> input[type=text]").focusout(function () {
                    if ($(this).val() > 100.00) {
                        $(this).val('100.00');
                    }
                });
                $("#<%= txtDiscount.ClientID %>").focusout(function () {
                    if ($(this).val() > 100.00) {
                        $(this).val('100.00');
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function Compare() {
            var strStartTime = $("#<%= txtFromtime.ClientID %>").val();
            var strEndTime = $("#<%= txtTotime.ClientID %>").val();
            var startTime = new Date().setHours(GetHours(strStartTime), GetMinutes(strStartTime), 0);
            var endTime = new Date(startTime)
            endTime = endTime.setHours(GetHours(strEndTime), GetMinutes(strEndTime), 0);
            if (startTime > endTime) {
                $("#<%= txtTotime.ClientID %>").val("");
                ShowPopupMessageBox("Start Time is greater than end time");
            }
            if (startTime == endTime) {
                $("#<%= txtTotime.ClientID %>").val("");
                ShowPopupMessageBox("Start Time equals end time");
            }
        }
        function GetHours(d) {
            var h = parseInt(d.split(':')[0]);
            //if (d.split(':')[1].split(' ')[1] == "pm") {
            //    h = h + 12;
            //}
            return h;
        }
        function GetMinutes(d) {
            return parseInt(d.split(':')[1].split(' ')[0]);
        }
        function Clearall() {
            try {
                $("select").chosen({ width: '100%' });
                $('#gvDayBasedPr').remove();
                $("#<%=gvDayBasedPr.ClientID%>").remove();
                $("#<%=txtFromtime.ClientID%>").val("");
                $("#<%=txtPromoDescription.ClientID%>").val("");
                $("#<%=txtDiscount.ClientID%>").val("");
                $("#<%=txtTotime.ClientID%>").val("");
                $("#<%=txtFromtime.ClientID%>").val("");
                $("#<%=txtPromotionCode.ClientID%>").val("");
                $('#<%=lstDays.ClientID %>').find("option").attr("selected", false);
                clearForm();
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function ClearForm() {
            //$(':checkbox, :radio').prop('checked', false);
            //$("input").prop('checked', false);
            $("#ContentPlaceHolder1_searchFilterUserControl_txtBarcode").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtItemCode").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtItemName").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtVendor").val("");
            ;//$("#ContentPlaceHolder1_searchFilterUserControl_ddlVendor").trigger("liszt:updated"); //by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtDepartment").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlDept").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtCategory").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlCategory").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtBrand").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlBrand").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtSubCategory").val("-- Select --");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlSubCategory").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtMerchandise").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlMerchandise").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtManufacture").val("-- Select --");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlManufacture").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtFloor").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlFloor").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtSection").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlSection").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtBin").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlBin").trigger("liszt:updated");//by velu29122018 ##
            $("#ContentPlaceHolder1_searchFilterUserControl_txtShelf").val("");
            //$("#ContentPlaceHolder1_searchFilterUserControl_ddlShelf").trigger("liszt:updated");//by velu29122018 ##

            return false;
        }
        function fncFilterValidation() {
            debugger;
            itmcode = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val()
            barcode = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BarcodeTextBox).ClientID %>").val();
            vendor = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.VendorDropDown).ClientID %>").val();
            dept = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DepartmentDropDown).ClientID %>").val();
            cat = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.CategoryDropDown).ClientID %>").val();
            sub = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.SubCategoryDropDown).ClientID %>").val();
            brand = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BrandDropDown).ClientID %>").val();
            floor = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.FloorDropDown).ClientID %>").val();
            shelf = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ShelfDropDown).ClientID %>").val();
            merchendise = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.MerchandiseDropDown).ClientID %>").val();
            manufacture = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ManufactureDropDown).ClientID %>").val();
            secton = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.SectionDropDown).ClientID %>").val();
            bin = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BinDropDown).ClientID %>").val();
            try {
                if ((itmcode == "") && (barcode == "") && (vendor == "") && (dept == "") && (cat == "")
                && (sub == "") && (brand == "") && (floor == "") && (shelf == "") && (merchendise == "") && (manufacture == "")
                && (secton == "") && (bin == "")) {
                    ShowPopupMessageBox("Please select Atleast one Filter");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function SetQualifyingItemAutoComplete() {
            debugger;
            try {
                $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });

                $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    open: function (event, ui) {
                        var $input = $(event.target);
                        var $results = $input.autocomplete("widget");
                        var scrollTop = $(window).scrollTop();
                        var top = $results.position().top;
                        var height = $results.outerHeight();
                        if (top + height > $(window).innerHeight() + scrollTop) {
                            newTop = top - height - $input.outerHeight();
                            if (newTop > scrollTop)
                                $results.css("top", newTop + "px");
                        }
                    },
                    focus: function (event, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });
                $(window).scroll(function (event) {
                    $('.ui-autocomplete.ui-menu').position({
                        my: 'left bottom',
                        at: 'left top',
                        of: '#ContentPlaceHolder1_searchFilterUserControl_txtItemCode'
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncApplyAllZero() {
            try {
                var gridtr = $("#<%= gvDayBasedPr.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    ShowPopupMessageBox("Please select Atleast one Filter");
                }
                else {
                    var Discount = $('#<%=txtDiscount.ClientID %>').val();
                    if (Discount == '') {
                        ShowPopupMessageBox("Enter Discount (%)");
                    }
                    else {
                        $("#<%=gvDayBasedPr.ClientID %> [id*=txtgrdDiscount]").val(Number.parseFloat(Discount).toFixed(2));
                    }

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncSelectAllClick(source) {
            var totalqty;
            //  debugger;
            try {
                if ($(source).is(":checked")) {
                    $("#ContentPlaceHolder1_gvDayBasedPr tr").find('td input[id*="cbSelect"]').prop("checked", true);
                }
                else {
                    $("#ContentPlaceHolder1_gvDayBasedPr tbody tr").find('td input[id*="cbSelect"]').prop("checked", false);
                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function tabE(obj, e) {
            var e = (typeof event != 'undefined') ? window.event : e;// IE : Moz
            if (e.keyCode == 13) { // 9 for Tab and 13 for enter
                var ele = document.forms[0].elements;
                for (var i = 0; i < ele.length; i++) {
                    var q = (i == ele.length - 1) ? 0 : i + 1;
                    if (obj == ele[i]) {
                        //focus TextBox on next row
                        ele[q].focus();
                        break
                    }
                }
                e.returnValue = false;
                if (typeof event == 'undefined')
                    e.preventDefault();
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {

                //alert($(evt.target).attr('id'));
                var tr = $(evt.target).closest('tr');
                tr.find('input[id *= cbSelect]').prop('checked', true);
                tr.next().find('input[id *= txtgrdDiscount]').focus();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function fncValidation() {
            try {
                var Show = '';
                if ($('#<%=txtPromoDescription.ClientID %>').val() == "") {
                    Show = Show + 'Please Enter Description';
                    $('#<%=txtPromoDescription.ClientID %>').focus();
                }
                if ($("[id*=cblLocation] input:checked").length == 0) {
                    Show = Show + '<br />Please Enter any one Location';
                }
                if ($('#<%=lstDays.ClientID %>').val() == null) {
                    Show = Show + '<br />Please Select Any one Day';
                    $('#<%=lstDays.ClientID %>').focus();
                }

                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(er.message);
            }
        }
        function fncShowSuccess() {
            try {
                ShowPopupMessageBox("Day Based Promotion Saved Successfully");
                Clearall();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAlertDiscount() {
            try {
                ShowAlert("Please Enter Discount on Selected Rows");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $("#<%= gvDayBasedPr.ClientID %> input[type=text]").focusout(function () {
                if ($(this).val() > 100.00) {
                    $(this).val('100.00');
                }
            });
        });



    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div style="background: #e6f2ff" id="divMain" runat="Server">
                <div class="container-group-full">
                    <div class="breadcrumbs" style="background: #e6f2ff">
                        <ul>
                            <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                                <%=Resources.LabelCaption.lblHome%></a>  <i class="fa fa-angle-right"></i></li>
                            <li>Merchandising
                        <i class="fa fa-angle-right"></i></li>

                            <li class="active-page">Day Based Promotions</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                        </ul>
                        <script src="../js/jsPromotionMnt.js" type="text/javascript"></script>
                    </div>
                </div>
                <div class="container-group-price">
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                            EnableVendorDropDown="true"
                            EnableDepartmentDropDown="true"
                            EnableCategoryDropDown="true"
                            EnableSubCategoryDropDown="true"
                            EnableBrandDropDown="true"
                            EnableClassDropDown="false"
                            EnableSubClassDropDown="false"
                            EnableMerchandiseDropDown="true"
                            EnableManufactureDropDown="true"
                            EnableFloorDropDown="true"
                            EnableSectionDropDown="true"
                            EnableBinDropDown="true"
                            EnableShelfDropDown="true"
                            EnableBarcodeTextBox="true"
                            EnableGRNNoTextBox="false"
                            EnableBatchNoTextBox="false"
                            EnableWarehouseDropDown="false"
                            EnableItemTypeDropDown="false"
                            EnablePriceTextBox="false"
                            EnableItemNameTextBox="true"
                            EnableItemCodeTextBox="true" />
                        <div style="width: 100%">
                            <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                <div class="control-button">
                                    <div class="label-right" style="width: 100%">
                                        <asp:LinkButton ID="lnkFetch" runat="server" class="button-red" OnClientClick="return fncFilterValidation();" OnClick="lnkLoadFilter_Click"><i class="icon-play"></i>Fetch</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="control-button">
                                    <div class="label-right" style="width: 100%">
                                        <asp:LinkButton ID="lnkclearFilter" runat="server" class="button-red" OnClientClick="return ClearForm();"><i class="icon-play"></i>Clear</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                    <div class="container-group-full" style="background-color: aliceblue" id="divPageload">
                        <div class="centerposition" style="width: 98%; background-color: antiquewhite;">
                            <asp:Panel runat="server" ID="pnlHeader" BorderColor="Black" BorderStyle="Solid"
                                Height="140px" Width="100%" Style="float: left; padding: 5px" BorderWidth="1px">
                                <div class="Split3Position" id="divDes" style="width: 32% !important">
                                    <div class="floatleft" style="width: 30%">
                                        <asp:Label ID="lblPromotionCode" runat="server" Text="Pr.Code"></asp:Label>
                                    </div>
                                    <div class="floatleft">
                                        <asp:TextBox ID="txtPromotionCode" Style="width: 100% !important" runat="server" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div class="floatleft" style="width: 30%">
                                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                                    </div>
                                    <div class="floatleft">
                                        <asp:TextBox ID="txtPromoDescription" TextMode="MultiLine" MaxLength="100" runat="server" Style="width: 200% !important" CssClass="form-control-res"></asp:TextBox>
                                    </div>
                                    <div style="padding: 0px; width: 222% !important; background-color: #faebd7 !important;">
                                        <div class="control-group-split">
                                            <div class="control-group-left">
                                                <div class="label-left" style="width: 20%;">
                                                    <asp:Label ID="lblday" runat="server" Text="Days"></asp:Label>
                                                </div>
                                                <div class="label-right" style="margin-left: 25px;">
                                                    <asp:ListBox ID="lstDays" runat="server" Style="width: 630px !important; margin-left: 25px !important;" CssClass="form-control-res franchise_lst_loc" SelectionMode="Multiple">
                                                        <asp:ListItem Text="Sunday" Value="1" />
                                                        <asp:ListItem Text="Monday" Value="2" />
                                                        <asp:ListItem Text="Tuesday" Value="3" />
                                                        <asp:ListItem Text="Wednesday" Value="4" />
                                                        <asp:ListItem Text="Thursday" Value="5" />
                                                        <asp:ListItem Text="Friday" Value="6" />
                                                        <asp:ListItem Text="Saturday" Value="7" />
                                                    </asp:ListBox>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="floatleft" style="width: 30%">
                                            <asp:Label ID="lblBasis" runat="server" Text="Discount"></asp:Label>
                                        </div>
                                        <div class="floatleft">
                                            <asp:TextBox ID="txtDiscount" runat="server" Style="text-align: right" onkeypress="return isNumberKey(event)" Text="0" MaxLength="6" CssClass="form-control-res textboxPromotion"></asp:TextBox>
                                        </div>
                                        <asp:LinkButton ID="lnkAdd" Style="margin-left: -18px; width: 72px;"
                                            runat="server" class="button-green" OnClientClick="fncApplyAllZero();return false;"
                                            Text='Apply'></asp:LinkButton>
                                    </div>
                                    <div class="col-md-12" style="margin-left: 241px; margin-top: -25px; display: none" runat="server" id="divActive">
                                        <div class="floatleft" style="width: 30%; margin-left: 71px;">
                                            <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Active"></asp:Label>
                                        </div>
                                        <div class="floatleft">
                                            <asp:CheckBox ID="chActive" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="Split3Position" id="divDate" style="width: 25% !important; margin-left: 79px;">

                                    <div class="floatleft" style="width: 30%">
                                        <asp:Label ID="lblFromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                    </div>
                                    <div class="floatleft">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res textboxPromotion"></asp:TextBox>
                                    </div>
                                    <div class="floatleft" style="width: 30%">
                                        <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                    </div>
                                    <div class="floatleft">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res textboxPromotion"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="Split3Position" style="width: 15% !important">

                                    <div class="floatleft">
                                        <asp:CheckBox ID="chIntime" runat="server" Text="In Time" />
                                    </div>
                                    <div class="floatleft" id="divTime" runat="server">
                                        <div class="floatleft">
                                            <asp:Label ID="Label1" runat="server" Text="From"></asp:Label>
                                        </div>
                                        <div class="floatleft" style="margin-left: 3px;">
                                            <asp:TextBox ID="txtFromtime" placeholder="--:--" runat="server" Style="width: 60px !important" CssClass="form-control-res clockpicker"></asp:TextBox>
                                        </div>
                                        <div class="floatleft">
                                            <asp:Label ID="Label2" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div class="floatleft" style="margin-left: 3px;">
                                            <asp:TextBox ID="txtTotime" placeholder="--:--" Style="width: 60px !important" runat="server" CssClass="form-control-res clockpicker"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="Split3Position" id="divLocation" style="width: 13% !important; margin-left: 55px;">
                                    <div class="control-group-single">
                                        <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                    </div>
                                    <div class="control-group-single">
                                        <div style="overflow-y: auto; width: 75%; height: 40px; background-color: azure;">
                                            <asp:CheckBoxList ID="cblLocation" CssClass="cbLocation" runat="server">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="grdLoad" style="margin-top: 5px">
                            <table id="tblDays" cellspacing="0" rules="all" border="1" class="fixed_header">
                                <thead>
                                    <tr>
                                        <th scope="col">Item Code
                                        </th>
                                        <th scope="col">Description
                                        </th>
                                        <th scope="col">Discount Percentage
                                        </th>
                                        <th scope="col">Select
                                    <asp:CheckBox ID="chSelectAllGrid" runat="server" onclick="fncSelectAllClick(this)" AutoPostBack="false" />
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                                <ContentTemplate>
                                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                                        style="height: 337px; width: 990px; overflow-x: hidden; overflow-y: scroll;">
                                        <asp:GridView ID="gvDayBasedPr" runat="server" AutoGenerateColumns="False" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                                            ShowHeader="false" CssClass="pshro_GridDgn grdLoad">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Discount" ItemStyle-Width="65">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtgrdDiscount" MaxLength="6" runat="server" Text='<%# Bind("Discount") %>' Style="background-color: #f9f2f5; border: #4163e1;"
                                                            onkeypress="return isNumberKey(event)" CssClass="grid-textbox numberDay"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>

                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="control-button" style="float: right;">
                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return fncValidation();" OnClick="lnkSave_Click"
                            Text='Save'> </asp:LinkButton>
                        <asp:LinkButton ID="lnkView" Style="margin-left: 10px" runat="server" class="button-blue" OnClick="lnkView_Click"
                            Text='View'> </asp:LinkButton>
                        <asp:LinkButton ID="lnkClear" Style="margin-left: 10px" runat="server" class="button-blue" OnClientClick="return Clearall()"
                            Text='Clear'></asp:LinkButton>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hidEdit" ClientIDMode="Static" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
