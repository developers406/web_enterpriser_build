﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    EnableEventValidation="false" CodeBehind="frmPromotionPrice.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmPromotionPrice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../js/jsCommon.js" type="text/javascript"></script>
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .BulkPromotion td:nth-child(1), .BulkPromotion th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .BulkPromotion td:nth-child(2), .BulkPromotion th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .BulkPromotion td:nth-child(3), .BulkPromotion th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .BulkPromotion td:nth-child(4), .BulkPromotion th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .BulkPromotion td:nth-child(5), .BulkPromotion th:nth-child(5) {
            min-width: 350px;
            max-width: 350px;
        }

        .BulkPromotion td:nth-child(6) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(6), .BulkPromotion th:nth-child(6) {
            min-width: 90px;
            max-width: 90px;
        }

        .BulkPromotion td:nth-child(7) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(7), .BulkPromotion th:nth-child(7) {
            min-width: 90px;
            max-width: 90px;
        }

        .BulkPromotion td:nth-child(8) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(8), .BulkPromotion th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
        }

        .BulkPromotion td:nth-child(9), .BulkPromotion th:nth-child(9) {
            min-width: 90px;
            max-width: 90px;
        }

        .BulkPromotion td:nth-child(10), .BulkPromotion th:nth-child(10) {
            min-width: 90px;
            max-width: 90px;
        }

        .BulkPromotion td:nth-child(11) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(11), .BulkPromotion th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
        }

        .BulkPromotion td:nth-child(12) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(12), .BulkPromotion th:nth-child(12) {
            min-width: 70px;
            max-width: 70px;
        }

        .BulkPromotion td:nth-child(13) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(13), .BulkPromotion th:nth-child(13) {
            min-width: 70px;
            max-width: 70px;
        }

        .BulkPromotion td:nth-child(14) {
            text-align: right !important;
        }

        .BulkPromotion td:nth-child(14), .BulkPromotion th:nth-child(14) {
            min-width: 70px;
            max-width: 70px;
        }

        .BulkPromotion td:nth-child(15), .BulkPromotion th:nth-child(15) {
            min-width: 150px;
            max-width: 150px;
        }

        .BulkPromotion td:nth-child(16), .BulkPromotion th:nth-child(16) {
            min-width: 150px;
            max-width: 150px;
        }

        .BulkPromotion td:nth-child(17), .BulkPromotion th:nth-child(17) {
            min-width: 150px;
            max-width: 150px;
        }

        .body {
            background: #F5F5F5 !important;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
            text-align: left;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
            text-align: left;
        }

        .BatchDetail {
            padding: 2px;
        }

        .ui-dialog-titlebar-close {      /*05122022 musaraf*/
            visibility: visible;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 100px;
            max-width: 100px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 325px;
            max-width: 325px;
            text-align: left;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 135px;
            max-width: 135px;
            text-align: left;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 135px;
            max-width: 135px;
            text-align: left;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
            text-align: right;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            min-width: 120px;
            max-width: 120px;
            text-align: right;
        }

        .grdLoad td:nth-child(18), .grdLoad th:nth-child(18) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        }
        .grdLoad td:nth-child(19), .grdLoad th:nth-child(19) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
        } 
         .grdLoad td,th{
             padding:5px !important;
         }
    </style>
    <script type="text/javascript">



        //function pageLoad() {
        //    try {
        //        SetItemAutoComplete();
        //        SetItemAutoCompleteBulkPromotion();
        //    }
        //    catch (err) {
        //        fncToastError(err.message);
        //    }
        //}

        //Document.ready(function () {
        //    SetItemAutoComplete();
        //    SetItemAutoCompleteBulkPromotion();
        //});

        //$(function () {
        //    SetItemAutoComplete();
        //    SetItemAutoCompleteBulkPromotion();
        //});

        ///Inventory Search For Item


       <%-- function SetItemAutoComplete() {
            $("[id$=txtItemCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.message);
                        },
                        failure: function (response) {
                            fncToastError(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtItemCode.ClientID %>').focus();
                    return false;
                },
                minLength: 1
            });
        }--%>



        ///Inventory Search For BulkPromotionItem
        function SetItemAutoCompleteBulkPromotion() {
            $("[id$=txtitemcodefilteration]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtitemcodefilteration.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtitemcodefilteration.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtitemcodefilteration.ClientID %>').focus();
                    return false;
                },
                minLength: 1
            });
        }

        $(function () {
            $("#RevokeOption").dialog({
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true
            });
        });

        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $('#divMsg').hide();
            //alert($("#spanpagestatus [id*=lblPageStatus]").html());
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            $("#<%= txtItemCode.ClientID %>").focus();
            if ($("#<%= hidAllowPromotion.ClientID %>").val() == "Block") {
                $("#<%= ChkAllow.ClientID %>").attr("disabled", true);
                $('#divMsg').hide();
            }
            else {
                $("#<%= ChkAllow.ClientID %>").removeAttr("disabled");
                $("#<%= ChkAllow.ClientID %>").attr("checked", "checked");
                $('#divMsg').show();
            }
            $("#<%= txtFrom.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
            $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txttodatefilteration.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
            //Promotion Filter Date time Picker
            if ($("#<%= txtfromdate.ClientID %>").val() =="")
                $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            if ($("#<%= txttodatefilteration.ClientID %>").val() == "")
            $("#<%= txttodatefilteration.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
           
            //alert(currentDatesqlformat_Master);
            //fncShowSaveNumber();

            //            $('#tblpromotionfilter').Scrollable({
            //                ScrollHeight: 100
            //            });

            $("#promotionsave").dialog({
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true
            });

            if ($("#<%=hidshowrepeater.ClientID %>").val() == "1") {
                fncPromotionfiltershow();
            }
            else {
                fncPromotionfilterhide();
            }

            //SetItemAutoComplete();
            SetItemAutoCompleteBulkPromotion();
        }
        function Clear() {
            $("#<%=txtItemCode.ClientID %>").val('');
            $("#<%=txtDescription.ClientID %>").val('');
            $("#<%=txtDepartment.ClientID %>").val('');
            $("#<%=txtCategory.ClientID %>").val('');
            $("#<%=txtBrand.ClientID %>").val('');
            $("#<%=txtCost.ClientID %>").val('');
            $("#<%=txtPrice.ClientID %>").val('');
            $("#<%=txtMRP.ClientID %>").val('');
            $("#<%=txtPPrice.ClientID %>").val('');
            $("#<%=txtDiscountPerc.ClientID %>").val('');
            $("#<%=txtEligibleQty.ClientID %>").val('1');
            $("#<%=hidisbatch.ClientID %>").val('');
            $("#<%=hidbatchno.ClientID %>").val('');
            $("#<%=txtProQty.ClientID %>").val('0');
            $("[id*=cbSelectAll]").removeAttr("checked");
            $("[id*=cblLocation] input").removeAttr("checked");
            $("[id*=rbnSellingpricewise]").prop("checked", true);
            $("[id*=tblpromotionfilter]").hide();
            $('#<%= txtDepartmentfilteration.ClientID %>').val('');
            $('#<%= txtCategoryfilteration.ClientID %>').val('');
            $('#<%= txtBrandfilteration.ClientID %>').val('');
            $('#<%= txtVendorfilteration.ClientID %>').val('');
            $("#RevokeOption").dialog('close');
            $("#<%= txtFrom.ClientID %>").val('');
            $("#<%= txtToDate.ClientID %>").val('');
            $("#<%= txttodatefilteration.ClientID %>").val('');
            $("#<%= txtfromdate.ClientID %>").val('');
            $('#<%=lnkAdd.ClientID %>').css("display", "block");
            $("#<%= txtFrom.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
            //Promotion Filter Date time Picker
            $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txttodatefilteration.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
            $("#<%=txtItemCode.ClientID %>").focus();
            return false;
        }

        //Promotion Filter Clear()
        function fncPromotionFilterClear() {
            try {
                $("#<%=txtpromotionprice.ClientID %>").val('0');
                $("#<%=txtpromotiondisc.ClientID %>").val('0');
                $("#<%=txteligibleqtyfilteration.ClientID %>").val('0');
                $("#<%=txtitemcodefilteration.ClientID %>").val('');
                $("#<%=txtBProQty.ClientID %>").val('0');
                $('#<%= txtDepartmentfilteration.ClientID %>').val('');
                $('#<%= txtCategoryfilteration.ClientID %>').val('');
                $('#<%= txtBrandfilteration.ClientID %>').val('');
                $('#<%= txtVendorfilteration.ClientID %>').val('');
              

                $('#<%= txtDepartmentfilteration.ClientID %>')[0].selectedIndex = 0;
                $('#<%= txtCategoryfilteration.ClientID %>')[0].selectedIndex = 0;
                $('#<%= txtBrandfilteration.ClientID %>')[0].selectedIndex = 0;
                $('#<%= txtVendorfilteration.ClientID %>')[0].selectedIndex = 0;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                        //fncPromotionSearch($("#<%=txtItemCode.ClientID %>").val(), "Prommotion Search");
                    fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }

                else if (keyCode == 112) {
                    //SetItemAutoComplete();
                    fncShowSearchDialogCommon(event, 'Inventory', 'txtItemCode', '');
                    //$(this).attr('autocomplete', 'off');                        
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInventoryCodeEnterkey_Master() {
            try {
                PromotionMNT = "1";
                fncGetBatchStatus_Master($('#<%=txtItemCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        //Data Assign to TextBox
        function fncDataAssigntoTextbox() {
            try {
                $.ajax({
                    type: "POST",
                    //                    url: "/WebService.asmx/fncGetInventoryDetail",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail")%>',
                    data: "{'inventorycode':'" + $("#<%=txtItemCode.ClientID %>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $("#<%=txtDescription.ClientID %>").val(msg.d[0]);
                        $("#<%=txtDepartment.ClientID %>").val(msg.d[1]);
                        $("#<%=txtCategory.ClientID %>").val(msg.d[2]);
                        $("#<%=txtBrand.ClientID %>").val(msg.d[3]);
                        $("#<%=txtDiscountPerc.ClientID %>").val('0.00');

                        //hidisbatch
                        if ($("#<%=hidisbatch.ClientID %>").val() == "1") {
                            $("#<%=txtCost.ClientID %>").val($("#<%=batchbasiccost.ClientID %>").val());
                            $("#<%=txtPrice.ClientID %>").val($("#<%=batchselprice.ClientID %>").val());
                            $("#<%=txtMRP.ClientID %>").val($("#<%=batchmrp.ClientID %>").val());
                            $("#<%=txtPPrice.ClientID %>").val($("#<%=batchselprice.ClientID %>").val());
                        }
                        else if ($("#<%=hidisbatch.ClientID %>").val() == "0") {
                            $("#<%=txtCost.ClientID %>").val(msg.d[4]);
                            $("#<%=txtPrice.ClientID %>").val(msg.d[5]);
                            $("#<%=txtMRP.ClientID %>").val(msg.d[6]);
                            $("#<%=txtPPrice.ClientID %>").val(msg.d[5]);
                        }

                        $("#<%=txtFrom.ClientID %>").focus();
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                            //alert('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Check Item in Promotion List
        function fncCheckIteminPromotionlist() {
            try {

                var obj = {};
                obj.Inventorycode = $("#<%=txtItemCode.ClientID %>").val();
                obj.BatchNo = $('#<%=hidbatchno.ClientID%>').val();
                    //                alert(obj);

                    //                alert($("#<%=txtItemCode.ClientID %>").val());
                $.ajax({
                    type: "POST",
                    url: "frmPromotionPrice.aspx/fncCheckAvailableinPromotionlist",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == 'Already') {
                            $("#RevokeOption").dialog('open');
                        }
                        else if (msg.d == 'New') {
                            fncDataAssigntoTextbox();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(err.message);
                        console.log(data);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //Select All Check Box in CheckBox List
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

        //        //Select and unselect all Promotion filter Record
        //        $(function () {
        //            try {
        //                $("#tblpromotionfilter [id*=cbProfilterheader]").click(function () {
        //                    alert('checked');
        //                    if ($(this).is(":checked")) {
        //                        $("#tblpromotionfilter [id*=cbProfilterrow]").attr("checked", "checked");
        //                    } else {
        //                        $("#tblpromotionfilter [id*=cbProfilterrow]").removeAttr("checked");
        //                    }
        //                });
        //                $("#tblpromotionfilter [id*=cbProfilterrow]").click(function () {
        //                    if ($("#tblpromotionfilter [id*=cbProfilterrow]").length == $("#tblpromotionfilter [id*=cbProfilterrow]:checked").length) {
        //                        $("#tblpromotionfilter [id*=cbProfilterheader]").attr("checked", "checked");
        //                    } else {
        //                        $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
        //                    }
        //                });
        //            }
        //            catch (err) {
        //                ShowPopupMessageBox(err.message);
        //            }
        //        });

        //Show Popup Control        
        /*function fncShowPopUp() {
            try {
                //alert('fncShowPopUp');
                DialogSearchKeyWord.val('');
                inventory.children().remove();
                inventory.trigger("liszt:updated");
                $("#inventoryDialog").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }*/
        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtItemCode.ClientID %>').val(itemcode);
                $('#<%=txtItemCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Discount Discount Percentage Calculation
        function fncDiscountPerCal(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    if (parseFloat($('#<%=txtPPrice.ClientID %>').val()) < parseFloat($('#<%=txtCost.ClientID %>').val())) {

                        popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>');
                            //ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_PromotionPrice%>');
                        $("#dialog-Alreadypromotion-confirm").dialog("open");
                            // $('#<%=txtDiscountPerc.ClientID %>').focus().select();

                    }
                    else if (parseFloat($('#<%=txtPPrice.ClientID %>').val()) >= parseFloat($('#<%=txtPrice.ClientID %>').val())) {
                        $('#<%=txtDiscountPerc.ClientID %>').val("0.00");
                        popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>');
                        fncToastInformation('<%=Resources.LabelCaption.Alert_Promotionpricelessthensellingprice%>');
                        $('#<%=txtPPrice.ClientID %>').val($('#<%=txtPrice.ClientID %>').val());
                        $('#<%=txtPPrice.ClientID %>').select();
                    }
                    else {
                        var Discountvalue = 100 - (parseFloat($('#<%=txtPPrice.ClientID %>').val()) / parseFloat($('#<%=txtPrice.ClientID %>').val())) * 100;
                        $('#<%=txtDiscountPerc.ClientID %>').val(Discountvalue.toFixed(2));
                        if (parseInt(Discountvalue) > 0)
                            $('#<%=txtEligibleQty.ClientID %>').select();
                        else
                            $('#<%=txtDiscountPerc.ClientID %>').focus().select();

                    }
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }
        function fncDiscountcal() {
            try {
                if (parseFloat($('#<%=txtPPrice.ClientID %>').val()) >= parseFloat($('#<%=txtPrice.ClientID %>').val())) {

                        <%--popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_PromotionPrice%>');

                        $('#<%=txtPPrice.ClientID %>').val($('#<%=txtPrice.ClientID %>').val());--%>
                    // $("#dialog-Alreadypromotion-confirm").dialog("open");

                    $('#<%=txtDiscountPerc.ClientID %>').val("0.00");
                    popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>');
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Promotionpricelessthensellingprice%>');
                    $('#<%=txtPPrice.ClientID %>').val($('#<%=txtPrice.ClientID %>').val());
                    $('#<%=txtPPrice.ClientID %>').select();
                }
                else {
                    var Discountvalue = 100 - (parseFloat($('#<%=txtPPrice.ClientID %>').val()) / parseFloat($('#<%=txtPrice.ClientID %>').val())) * 100;
                    $('#<%=txtDiscountPerc.ClientID %>').val(Discountvalue.toFixed(2));

                    if (parseInt(Discountvalue) > 0)
                        $('#<%=txtEligibleQty.ClientID %>').select();
                    else
                        $('#<%=txtDiscountPerc.ClientID %>').select();

                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }
        //Promotion Price Calculation
        function fncPromotionPriceCal(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    //alert('fncPromotionPriceCalEnterkey');
                    var PromotionPrice = parseFloat($('#<%=txtPrice.ClientID %>').val()) - parseFloat($('#<%=txtPrice.ClientID %>').val()) * (parseFloat($('#<%=txtDiscountPerc.ClientID %>').val()) / 100);

                    if (PromotionPrice < parseFloat($('#<%=txtCost.ClientID %>').val())) {

                            <%--<%--   popUpObjectForSetFocusandOpen = $('#<%=txtDiscountPerc.ClientID %>');
                          ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_PromotionPrice%>');--%>

                        $('#<%=txtPPrice.ClientID %>').val(PromotionPrice);
                            //$('#<%=txtDiscountPerc.ClientID %>').val('0.00'); 
                        $("#dialog-Alreadypromotion-confirm").dialog("open");
                        $('#<%=txtProQty.ClientID %>').select();


                    }
                    else {
                        $('#<%=txtPPrice.ClientID %>').val(PromotionPrice.toFixed(2));
                        $('#<%=txtProQty.ClientID %>').select();
                    }
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }



        function fncPromotionPriceCalmouse() {
            try {
                if (parseFloat($('#<%=txtDiscountPerc.ClientID %>').val()) > 100) {
                    $('#<%=txtDiscountPerc.ClientID %>').val('100');
                }
                var PromotionPrice = parseFloat($('#<%=txtPrice.ClientID %>').val()) - parseFloat($('#<%=txtPrice.ClientID %>').val()) * (parseFloat($('#<%=txtDiscountPerc.ClientID %>').val()) / 100);

                if (PromotionPrice < parseFloat($('#<%=txtCost.ClientID %>').val())) {

                            <%--    popUpObjectForSetFocusandOpen = $('#<%=txtDiscountPerc.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_PromotionPrice%>');--%>


                    $('#<%=txtPPrice.ClientID %>').val(PromotionPrice);
                      // $('#<%=txtDiscountPerc.ClientID %>').val('0.00');
                    $("#dialog-Alreadypromotion-confirm").dialog("open");

                }
                else {
                    $('#<%=txtPPrice.ClientID %>').val(PromotionPrice.toFixed(2));
                    $('#<%=txtProQty.ClientID %>').select();
                }
                return false;

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //Promotion Add Validation
        function fncAddvalidation() {
            var frmDate, toDate;
            var status = true;
            try {

                $('#<%=lnkAdd.ClientID %>').css("display", "none");

                frmDate = $('#<%=txtfromdate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

                $("#<%=grdPromotion.ClientID %>  tr").each(function () {
                    if ($('#<%=txtItemCode.ClientID %>').val().trim() == $(this).find("td.ItemcodeCss").text().trim() &&
                        $('#<%=hidbatchno.ClientID %>').val().trim() == $(this).find("td.BatchNoCss").text().trim()) {
                        fncToastInformation('<%=Resources.LabelCaption.Alert_AddedList%>');
                        Clear();
                        return false;
                    }
                });

                if ($('#<%=txtItemCode.ClientID %>').val() == "") {
                            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                    fncToastInformation('<%=Resources.LabelCaption.Alert_ItemCode%>');
                    $('#<%=txtItemCode.ClientID %>').select();
                    status = false;
                }
                else if ($('#<%=txtMRP.ClientID %>').val() == "" || parseFloat($('#<%=txtMRP.ClientID %>').val()) == 0) {
                        //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                    fncToastInformation("MRP is invalid");
                    $('#<%=txtItemCode.ClientID %>').select();
                    status = false;
                }
                else if ($('#<%=txtCost.ClientID %>').val() == "" || parseFloat($('#<%=txtCost.ClientID %>').val()) == 0) {
                            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                    fncToastInformation("Netcost is invalid");
                    $('#<%=txtItemCode.ClientID %>').select();
                    status = false;
                }

                if ($('#<%=txtPrice.ClientID %>').val() == "" || parseFloat($('#<%=txtPrice.ClientID %>').val()) == 0) {
                            //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                    fncToastInformation("Selling Price is invalid");
                    $('#<%=txtItemCode.ClientID %>').select();
                    status = false;
                }
                   <%-- else if (parseFloat($('#<%=txtPPrice.ClientID %>').val()) < parseFloat($('#<%=txtCost.ClientID %>').val())) {
                        //popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                        //fncToastError('<%=Resources.LabelCaption.Alert_PromotionPrice%>');
                        $("#dialog-Alreadypromotion-confirm").dialog("open");
                    }--%>
                else if (parseFloat($('#<%=txtPPrice.ClientID %>').val()) > parseFloat($('#<%=txtPrice.ClientID %>').val())) {
                //popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>')
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Promotionpricelessthensellingprice%>');
                    $('#<%=txtPPrice.ClientID %>').select();
                    status = false;
                }

                else if (parseFloat($('#<%=txtDiscountPerc.ClientID %>').val()) <= 0) {
                             //popUpObjectForSetFocusandOpen = $('#<%=txtPPrice.ClientID %>')
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Promotionpricelessthensellingprice%>');
                    $('#<%=txtPPrice.ClientID %>').select();
                    status = false;
                }
                else if (new Date(frmDate) < new Date(currentDatesqlformat_Master)) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Promotionfromdate%>');
                    $('#<%=txtFrom.ClientID %>').select();
                    status = false;
                }
                else if (new Date(toDate) < new Date(frmDate)) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Promotiontodatevalidation%>');
                    $('#<%=txtToDate.ClientID %>').select();
                    status = false;
                }
                if (status == false) {
                    $('#<%=lnkAdd.ClientID %>').css("display", "block");
                }
                return status;

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //Set Focus to To Date
        function fncSetFocustodate(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {

            //alert('fncSetFocustodate');
            //fncSetfocus($('#<%=txtToDate.ClientID%>'));
            //evt.preventDefault();
            //evt.stopPropagation();
            //$('#<%=txtToDate.ClientID%>').datepicker("option", "showOn ", "button");
            //$('#<%=txtToDate.ClientID%>').focus();
            //fncSetfocus($('divtxtToDate'));
            //divtxtToDate
            //$('#<%=txtToDate.ClientID%>').datepicker("show");


                    setTimeout(function () {
                //$('#<%=txtToDate.ClientID%>').datepicker("option", "showOn ", "focus");
                        $('#<%=txtToDate.ClientID%>').datepicker("show");
                        $('#<%=txtToDate.ClientID%>').focus();
                    }, 10);


                    //evt.preventDefault();
                    //evt.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                console.log(err);
                ShowPopupMessageBox(err.message);
            }
        }
//        function dateInFocus(source) {
//            try {
//                //alert(source);
//               // $('#<%=txtToDate.ClientID%>').datepicker("open");
        //            } catch (err) {
        //                console.log(err);
        //            }
        //        }
        //Set Focus to Promotion Price
        function fncSetFocusPromotionprice() {
            try {
                $('#<%=txtPPrice.ClientID%>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Check Batch or Not
        function fncGetBatchStatus() {
            try {

                if ($('#<%=hidisbatch.ClientID%>').val() == "1") {
                    $('#<%=txtFrom.ClientID%>').datepicker("hide");
                    //alert('fromdateclose');
                    fncGetBatchDetail_Master($('#<%=txtItemCode.ClientID%>').val());
                }
                else if ($('#<%=hidisbatch.ClientID%>').val() == "0") {
                    fncDataAssigntoTextbox();
                }
                $('#<%=hidUom.ClientID%>').val(Uom);
                
                //fncCheckIteminPromotionlist();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Assign Batch values to text box
        var Batchno_Master, Batchno_Master, mrp_Master, sprice_Master, expmonth_Master, expyear_Master, Basiccost_Master, stock_Master, netcost_Master;
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock, netcost) {
            try {
                Batchno_Master = batchno;
                mrp_Master = mrp;
                sprice_Master = sprice;
                expmonth_Master = expmonth;
                expyear_Master = expyear;
                Basiccost_Master = Basiccost;
                stock_Master = stock;
                netcost_Master = netcost;
                fncPromotionSearch($("#<%=txtItemCode.ClientID %>").val(), batchno, "Prommotion Search");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Revoke Promotion Item
        function fncRevokePromotionItem() {
            try {
                //alert('fncRevokePromotionItem');
                var obj = {};
                obj.inventorycode = $("#<%=txtItemCode.ClientID %>").val();
                obj.currentDate = currentDatesqlformat_Master;
                $.ajax({
                    type: "POST",
                    url: "frmPromotionPrice.aspx/fncRevokePromotionItem",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "ItemRevoked") {
                            //alert('fncRevokePromotionItemSuccess');
                            $("#RevokeOption").dialog('close');
                            Clear();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(err.message);
                        //alert('fncRevokePromotionItemfail');
                        console.log(data);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }
        //Show Popup After Save
        function fncShowSuccessMessage() {
            try {
                //alert('fncShowSaveNumber');
                $('#divCount').hide();
                $('#divMsg').hide();
                $("#promotionsave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        //Save Validation
        function fncSaveValidation() {
            try {

                if ($("#<%=hidshowrepeater.ClientID %>").val() == "1") {
                    //alert($(".cbLocation").find("input:checked").length);
                    if ($("#tblpromotionfilter [id*=promotionrow]").length == 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                        return false;
                    }
                    else if ($(".cbLocation").find("input:checked").length == 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Loc%>');
                        return false;
                    }
                    else if ($("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length == 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                        return false;
                    }
                }
                else if ($("#<%=hidshowrepeater.ClientID %>").val() == "0") {
                    //alert($(".cbLocation").find("input:checked").length);
                    if ($(".cbLocation").find("input:checked").length == 0) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Loc%>');
                        return false;
                    }
                    else if ($("#<%=grdPromotion.ClientID %>").length == 0) {

                        popUpObjectForSetFocusandOpen = $('#<%=txtItemCode.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alter_AddItemtogrid%>');
                        return false;
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //UnSelect Location List
        function fncUnSelectLocationList() {
            try {
                $("[id*=cbSelectAll]").removeAttr("checked");
                $("[id*=cblLocation] input").removeAttr("checked");
                $("#promotionsave").dialog('close');

                fncPromotionFilterClear();

                $("#tblpromotionfilter [id*=promotionrow]").remove();

                if ($("#<%=hidshowrepeater.ClientID %>").val() == "1")
                    return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Show and Hide Promotion Filter
        function fncShowandHidePromotionFilter() {
            try {
                //alert('fncShowandHidePromotionFilter');
                if ($('#divpromotionfilter:visible').length == 0) {
                    fncPromotionfiltershow();
                    return false;
                }
                else {
                //                    $('#divpromotionfilter').hide();
                //                    //$('#divrepeater').hide();
                //                    $('#<%=divrepeater.ClientID%>').hide();
                //                    $('#divPromotion').show();
                //                    $("#<%=hidshowrepeater.ClientID %>").val('0');
                    fncPromotionfilterhide();
                    return false;
                }

            }
            catch (err) {
                //console.log(err);
                ShowPopupMessageBox(err.message);
            }
        }

        //Promotion Hide
        function fncPromotionfilterhide() {
            try {
                fncPromotionFilterClear();
                $("#tblpromotionfilter [id*=promotionrow]").remove();
                $('#divpromotionfilter').hide();
                $('#rptpagination').hide();
                $('#<%=divrepeater.ClientID%>').hide();
                $('#divpromotionprice').hide();
                $('#divPromotion').show();
                $('#divsinglepromotioncontent').show();
                $('#divsubClear').show();
                $("#<%=divAllow.ClientID %>").hide();
                $('#divCount').hide();
                $('#divMsg').hide();
                //divpromotionprice
                $("#<%=hidshowrepeater.ClientID %>").val('0');

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Promotion filter show
        function fncPromotionfiltershow() {
            try {

                if ($("#spanpagestatus [id*=lblPageStatus]").html() != "Page 1 of 0" && $("#spanpagestatus [id*=lblPageStatus]").html() != "Page 1 of 1"
                    && $("#tblpromotionfilter [id*=promotionrow]").length > 0) {
                    $('#rptpagination').hide();
                }

                $('#<%=emptyrow.ClientID%>').show();
                $('#divpromotionfilter').show();
                $("#<%=divAllow.ClientID %>").show();
                $('#divCount').show();
                $('#<%=divrepeater.ClientID%>').show();
                $('#divpromotionprice').show();
                $('#divPromotion').hide();
                $('#divsinglepromotioncontent').hide();
                $('#divsubClear').hide();
                if ($("#<%= hidAllowPromotion.ClientID %>").val() != "Block") {
                    $("#<%= ChkAllow.ClientID %>").removeAttr("disabled");
                    $('#divMsg').show();
                }

                //divsubClear
                //divsinglepromotioncontent
                //divPromotion
                $("#<%=hidshowrepeater.ClientID %>").val('1');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Promotion Filter validation
        function fncProfiltervalidation() {
            try {



                if ($("#tblpromotionfilter [id*=promotionrow]").length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');

                    return false;
                }
                else if ($("#<%=txtpromotionprice.ClientID %>").val() == "0" && $("#<%=txtpromotiondisc.ClientID %>").val() == "0") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_ProPriceandDiscount%>');

                    return false;
                }
                else if (new Date($('#<%=txtfromdate.ClientID %>').val()) < new Date(currentDateformat_Master)) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtfromdate.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_Promotionfromdate%>');
                    return false;
                }
                else if (new Date($('#<%=txttodatefilteration.ClientID %>').val()) < new Date($('#<%=txtfromdate.ClientID %>').val())) {

                    popUpObjectForSetFocusandOpen = $('#<%=txttodatefilteration.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_Promotiontodatevalidation%>');


                    return false;
                }
                else {

                    $("#tblpromotionfilter [id*=promotionrow]").each(function () {
                        var promotionprice = 0;
                        //$(this).find('td span[id*="lblreProPrice"]').html('10');
                        if ($("#<%=rbnSellingpricewise.ClientID %>").prop("checked")) {
                            var sellingprice = $(this).find('td span[id*="lblrePrice"]').html();
                            if (parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) > 0) {
                                promotionprice = parseFloat(sellingprice) - parseFloat(sellingprice) * parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) / 100;
                            }
                            else
                                   <%-- promotionprice = $("#<%=txtpromotionprice.ClientID %>").val(); Vijay--%>
                                promotionprice = parseFloat(sellingprice) - parseFloat($("#<%=txtpromotionprice.ClientID %>").val());
                        }
                        else {
                            var mrp = $(this).find('td span[id*="lblreMRP"]').html();
                            if (parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) > 0)
                                promotionprice = parseFloat(mrp) - parseFloat(mrp) * parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) / 100;
                            else
                                    // promotionprice = $("#<%=txtpromotionprice.ClientID %>").val();
                                promotionprice = parseFloat(mrp) - parseFloat($("#<%=txtpromotionprice.ClientID %>").val());

                        }

                        $(this).find('td input[id*="hidProPrice"]').val(promotionprice);
                        $(this).find('td span[id*="lblreProPrice"]').html(promotionprice);

                        $(this).find('td input[id*="hidproDiscPerc"]').val($("#<%=txtpromotiondisc.ClientID %>").val());
                        $(this).find('td span[id*="lblreDiscPerc"]').html($("#<%=txtpromotiondisc.ClientID %>").val());

                        $(this).find('td input[id*="hidProQty"]').val($("#<%=txtBProQty.ClientID %>").val());
                        $(this).find('td span[id*="lblProQty"]').html($("#<%=txtBProQty.ClientID %>").val());

                        $(this).find('td input[id*="hidEqty"]').val($("#<%=txteligibleqtyfilteration.ClientID %>").val());
                        $(this).find('td span[id*="lblreEQty"]').html($("#<%=txteligibleqtyfilteration.ClientID %>").val());

                        $(this).find('td input[id*="hidfromdate"]').val($("#<%=txtfromdate.ClientID %>").val());
                        $(this).find('td span[id*="lblreFromDate"]').html($("#<%=txtfromdate.ClientID %>").val());

                        $(this).find('td input[id*="hidtodate"]').val($("#<%=txttodatefilteration.ClientID %>").val());
                        $(this).find('td span[id*="lblreToDate"]').html($("#<%=txttodatefilteration.ClientID %>").val());

                    });

                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //To Clear Promotion filter Price
        function fncPromotionDiscountFocusout() {
            try {
                if (parseFloat($('#<%=txtpromotiondisc.ClientID %>').val()) > 100) {
                    $('#<%=txtpromotiondisc.ClientID %>').val('100');
                }
                if (parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) > 0) {
                    $("#<%=txtpromotionprice.ClientID %>").val(0);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Promotion Price Validation
        pricevalidationstatus = "All";
        function fncPromotionPriceValidationcheckboxclick(event) {
            try {
                $(event).css("readonly", "readonly");
                if (($(event).is(":checked"))) {
                    var rowobj = $(event).parent().parent();
                    //alert(rowobj.find('td input[id*="hidProPrice"]').val());
                    var promotionprice = rowobj.find('td span[id*="lblreProPrice"]').html();
                    var basiccost = rowobj.find('td span[id*="lblreCost"]').html();
                    var price = rowobj.find('td span[id*="lblrePrice"]').html();
                    var itemcode = rowobj.find('td span[id*="lblreItemCode"]').html().trim();

                    if (parseFloat(promotionprice) < parseFloat(basiccost)) {
                        pricevalidationstatus = "All";
                        fncInitialize($(event));
                //  ShowPopupMessageBox(itemcode + '<%=Resources.LabelCaption.Alert_Promotionwithinventorycode%>');

                        $(event).removeAttr("checked");

                        //if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                        //    $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                        //}
                    }
                    else if (parseFloat(promotionprice) > parseFloat(price)) {

                        ShowPopupMessageBox(itemcode + '<%=Resources.LabelCaption.Alert_Promotionwithprice%>');
                        $(event).removeAttr("checked");

                        if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                        }
                    }
                    else {
                        if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length == $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").attr("checked", "checked");
                        }
                    }
                }
                else {
                    if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                        $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                    }
                }
                $(event).css("readonly", false);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function fncInitialize(event) {

            $("#dialog-BasicCost-Validation").dialog({
                resizable: false,
                height: "auto",
                width: 250,
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        if (pricevalidationstatus == "All") {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").attr("checked", "checked");
                            $("#tblpromotionfilter [id*=cbreProfilterrow]").attr("checked", "checked");
                        }
                        else {
                            $(event).attr("checked", "checked");
                        }
                    },
                    "No": function () {
                        $(this).dialog("close");
                        if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                        }
                    }
                }
            });
        }
        function fncPromotionPriceValidationSinglecheckboxclick(event) {
            try {
                $(event).css("readonly", "readonly");
                if (($(event).is(":checked"))) {
                    var rowobj = $(event).parent().parent();
                    //alert(rowobj.find('td input[id*="hidProPrice"]').val());
                    var promotionprice = rowobj.find('td span[id*="lblreProPrice"]').html();
                    var basiccost = rowobj.find('td span[id*="lblreCost"]').html();
                    var price = rowobj.find('td span[id*="lblrePrice"]').html();
                    var itemcode = rowobj.find('td span[id*="lblreItemCode"]').html().trim();

                    if (parseFloat(promotionprice) < parseFloat(basiccost)) {
                        pricevalidationstatus = "Single";

                        fncInitialize($(event));

                //  ShowPopupMessageBox(itemcode + '<%=Resources.LabelCaption.Alert_Promotionwithinventorycode%>');

                        $(event).removeAttr("checked");

                        //if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                        //    $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                        //}
                    }
                    else if (parseFloat(promotionprice) > parseFloat(price)) {

                        ShowPopupMessageBox(itemcode + '<%=Resources.LabelCaption.Alert_Promotionwithprice%>');
                        $(event).removeAttr("checked");

                        if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                        }
                    }
                    else {
                        if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length == $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                            $("#tblpromotionfilter [id*=cbProfilterheader]").attr("checked", "checked");
                        }
                    }
                }
                else {
                    if ($("#tblpromotionfilter [id*=cbreProfilterrow]").length != $("#tblpromotionfilter [id*=cbreProfilterrow]:checked").length) {
                        $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr("checked");
                    }
                }
                $(event).css("readonly", false);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }

        //Select and UnSelect Promotion Filter row
        function fncSelectUnselectPromotionrow(event) {
            pricevalidationstatus = "All";
            try {
                if (($(event).is(":checked"))) {
                    $("#tblpromotionfilter [id*=promotionrow]").each(function () {
                        $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                        var objprorow = $(this).find('td input[id*="cbreProfilterrow"]');
                        fncPromotionPriceValidationcheckboxclick(objprorow);
                    });
                    //                    $("#tblpromotionfilter [id*=cbreProfilterrow]").attr("checked", "checked");
                }
                else {
                    $("#tblpromotionfilter [id*=cbreProfilterrow]").removeAttr("checked");

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //set Focus to Promotion filter to date
        function fncSetfocusprofiltertodate(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {

                    setTimeout(function () {
                //$('#<%=txtToDate.ClientID%>').datepicker("option", "showOn ", "focus");
                        $('#<%=txttodatefilteration.ClientID%>').datepicker("show");
                        $('#<%=txttodatefilteration.ClientID%>').focus();
                    }, 10);

            //                    $("#<%=txttodatefilteration.ClientID %>").focus();
                    return false;
                }
                else {
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //set focus to promotion filter price
        function fncSetfocustoPrice(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    if (parseFloat($("#<%=txtpromotionprice.ClientID %>").val()) == 0 && parseFloat($("#<%=txtpromotiondisc.ClientID %>").val()) == 0) {
                        $("#<%=txtpromotionprice.ClientID %>").focus();
                    }
                }
                //else {
                //    return false;
                //}  //Vijay
            }
            catch (err) {
                ShowPopupMessageBox(err.message);

            }
        }

        //Set focus to Eligible Qty
        function fncPromotionpriceEnterkeyvalidation(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    if (parseFloat($("#<%=txtpromotionprice.ClientID %>").val()) == 0)
                        $("#<%=txtpromotiondisc.ClientID %>").focus();
                    else
                        $("#<%=txteligibleqtyfilteration.ClientID %>").select();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Set focus to Apply Button
        function fncSetFocustoApplyButton(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    $("#<%=lnkapply.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Set focus to Eligible Qty
        function fncSetfocustoEligibleQty(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    $("#<%=txtBProQty.ClientID %>").focus();
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncPromotionChange(evt) {

            try {
                if ($('#<%=rbnHourlyPro.ClientID%>').is(':checked')) {
                    $("#proTime").css("display", "block");
                }
                else {
                    $("#proTime").css("display", "none");
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(function () {
            $('#<%=txtFrmTime.ClientID%>').timepicker({ 'timeFormat': 'H:i:s' });
            $('#<%=txtToTime.ClientID%>').timepicker({ 'timeFormat': 'H:i:s' });
            fncDecimal();
        });

        function fncCheckPromotionMode(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            try {
                if (charCode == 13) {
                    if ($('#<%=rbnNormalPro.ClientID%>').is(':checked')) {
                        $('#<%=lnkAdd.ClientID%>').focus();
                    }
                    else {
                        $('#<%=txtFrmTime.ClientID%>').select();
                    }
                    return false;
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtCost.ClientID%>').number(true, 2);
                $('#<%=txtPrice.ClientID%>').number(true, 2);
                $('#<%=txtMRP.ClientID%>').number(true, 2);
                $('#<%=txtPPrice.ClientID%>').number(true, 2);
                $('#<%=txtDiscountPerc.ClientID%>').number(true, 2);
                $('#<%=txtProQty.ClientID%>').number(true, 0);
                $('#<%=txtEligibleQty.ClientID%>').number(true, 0);
                $('#<%=txtpromotionprice.ClientID%>').number(true, 2);
                $('#<%=txtpromotiondisc.ClientID%>').number(true, 2);
                $('#<%=txtBProQty.ClientID%>').number(true, 0);
                $('#<%=txteligibleqtyfilteration.ClientID%>').number(true, 0);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //------------------------------------------------------------------------------------------------
        function checkPhoneKey(event) {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (event.keyCode != 27) {
                fncShowSearchDialogCommon(event, 'Inventory', 'txtItemCode', 'txtItemCode');
            }
            else {
                //fncGetInventoryCode(event);
            }
            //return false;
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    if (ControlID != "txtitemcodefilteration") {
                        event.preventDefault();
                            // fncPromotionSearch($("#<%=txtItemCode.ClientID %>").val(), "Prommotion Search");
                        fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                        event.preventDefault();
                        event.stopPropagation();
                        return false;
                    }
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtDescription.ClientID %>').val($.trim(Description));

                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        $(function () {
            $("#dialog-Alreadypromotion-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 250,
                modal: true,
                open: function () {
                    $(this).parent().find('button:nth-child(1)').focus().select();
                },
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        var Discountvalue = 100 - (parseFloat($('#<%=txtPPrice.ClientID %>').val()) / parseFloat($('#<%=txtPrice.ClientID %>').val())) * 100;
                        $('#<%=txtDiscountPerc.ClientID %>').val(Discountvalue.toFixed(2));
                        $('#<%=txtProQty.ClientID %>').select();
                        status = true;
                    },
                    "No": function () {
                        $(this).dialog("close");
                        fncToastError('<%=Resources.LabelCaption.Alert_PromotionPrice%>');
                        status = false;
                        $('#<%=txtDiscountPerc.ClientID %>').val("0.00");
                        $('#<%=txtPPrice.ClientID %>').val($('#<%=txtPrice.ClientID %>').val());
                        $('#<%=txtPPrice.ClientID %>').select();
                    }
                }
            });


        });


        function fncPromotionSearch(ItemCode, BatchNo, mode) {
            try {
                $.ajax({
                    type: "POST",
                    url: "frmPromotionPrice.aspx/fncGetInventorycodeinPromotion",
                    data: "{'inventorycode':'" + $.trim(ItemCode) + "','batchno':'" + BatchNo + "','mode':'" + mode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (mode == "Prommotion Search")
                            fncAssignValuesTable(msg);
                    },
                    error: function (data) {
                        alert('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }


        function fncAssignValuesTable(msg) {
            try {
                var value = jQuery.parseJSON(msg.d);
                if (value != null) {
                    var tblPromotionBody = $("#tblPromotion tbody");
                    tblPromotionBody.children().remove();
                    for (var i = 0; i < value.length; i++) {
                        row = "<tr id='PromotionRow_" + i + "' tabindex='" + i + "' ><td id='tdSNo_" + i + "' > " +
                            $.trim(value[i]["SNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                            $.trim(value[i]["BatchNo"]) + "</td><td id='tdPromotionFromDate_" + i + "' >" +
                            $.trim(value[i]["PromotionFromDate"]) + "</td><td id='tdPromotionTodate_" + i + "' >" +
                            $.trim(value[i]["PromotionTodate"]) + "</td><td id='tdPromotionPrice_" + i + "' >" +
                            (value[i]["PromotionPrice"]).toFixed(2) + "</td><td id='tdLocation_" + i + "' >" +
                            $.trim(value[i]["Location"]) + "</td></tr>";
                        tblPromotionBody.append(row);
                    }
                    $('#lblInventoryCode').text("ItemCode: " + $.trim(value[0]["InventoryCode"]));
                    $('#lblPromoDescription').text("Description: " + $.trim(value[0]["Description"]));
                    fncInitializePromotionDetail($.trim(value[0]["InventoryCode"]), $.trim(value[0]["BatchNo"]));

                    $('#<%=hidbatchno.ClientID%>').val(Batchno_Master);
                    $('#<%=batchmrp.ClientID%>').val(mrp_Master);
                    $('#<%=batchselprice.ClientID%>').val(sprice_Master);
                    $('#<%=batchbasiccost.ClientID%>').val(netcost_Master);
                    Batchno_Master = '';
                    mrp_Master = '';
                    sprice_Master = '';
                    expmonth_Master = '';
                    expyear_Master = '';
                    Basiccost_Master = '';
                    stock_Master = '';
                    netcost_Master = '';
                }

                else {

                    $('#<%=hidbatchno.ClientID%>').val(Batchno_Master);
                    $('#<%=batchmrp.ClientID%>').val(mrp_Master);
                    $('#<%=batchselprice.ClientID%>').val(sprice_Master);
                    $('#<%=batchbasiccost.ClientID%>').val(netcost_Master);
                            //$('#<%=hidbatchno.ClientID%>').val('');
                    //fncCheckIteminPromotionlist();
                    fncDataAssigntoTextbox();
                            //fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncInitializePromotionDetail(ItemCode, BatchNo) {
            try {
                $("#Promotion_Details").dialog({
                    resizable: false,
                    height: 320,
                    width: 633,
                    modal: true,
                    title: "Already Promotions are Available for this Item",
                    buttons: {
                        "Yes": function () {
                            $(this).dialog("close");

                            fncDataAssigntoTextbox();
                                    //fncPromotionSearch(ItemCode,BatchNo, "Promotion Revoke");
                                    //fncGetInventoryCode_Master($("#<%=txtItemCode.ClientID %>"));
                        },
                        "No": function () {
                            Clear();
                            $(this).dialog("close");
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var charCode = (e.which) ? e.which : e.keyCode;
                if (charCode == 27) {
                    if ($("#Promotion_Details").is(":visible")) {
                        $("#Promotion_Details").dialog("close");
                    }
                    e.preventDefault();
                }
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $("#<%=ChkAllow.ClientID %>").click(function () {
                var checked = $(this).is(':checked');
                if (checked == false) {
                    $("#tblpromotionfilter [id*=promotionrow]").each(function () {
                        // $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                        if ($(this).find('td input[id*="cbreProfilterrow"]').is(':checked')) {
                            var promo = $(this).find('td input[id*="hidParmcode"]').val();
                            if (promo == "1") {
                                $(this).find('td input[id*="cbreProfilterrow"]').removeAttr('checked');
                            }
                            $("#tblpromotionfilter [id*=cbProfilterheader]").removeAttr('checked');
                        }

                    });
                }
                else {
                    $("#tblpromotionfilter [id*=promotionrow]").each(function () {
                        if (!($(this).find('td input[id*="cbreProfilterrow"]').is(':checked'))) {
                            var promo = $(this).find('td input[id*="hidParmcode"]').val();
                            if (promo == "1") {
                                $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                            }
                            $("#tblpromotionfilter [id*=cbProfilterheader]").attr("checked", "checked");
                        }

                    });
                }
            });
        });

        function dept_Delete(source) {
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $(source).remove();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });
        }
   var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PromotionPrice');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PromotionPrice";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="background: #e6f2ff; padding: 0px">
        <div class="container-group-full">
            <div class="breadcrumbs">
                &nbsp;<ul>
                    <li>&nbsp;<a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Promotions</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page">Promotion Price Change</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
        </div>
        <div class="container-group-full">
            <div style="width: 20%; margin-left: 1%; float: left">
                <div class="control-group-single">
                    <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                </div>
                <div class="control-group-single">
                    <div style="overflow-y: auto; width: 260px; height: 40px; margin-bottom: 10px">
                        <asp:CheckBoxList ID="cblLocation" runat="server" CssClass="cbLocation">
                        </asp:CheckBoxList>
                    </div>
                </div>
                <div class="control-group-single" id="divAllow" runat="server">
                    <asp:CheckBox ID="ChkAllow" runat="server" CssClass="cbLocation" Text="Overwrite Existing Promotion"></asp:CheckBox>
                </div>
            </div>
            <div id="divpromotionfilter" style="width: 78%; float: left; display: none">
                <asp:UpdatePanel ID="upfetch" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                            <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                        </div>
                        <div style="float: left; width: 13%">
                            <div class="spilt">
                                <asp:RadioButton ID="rbnSellingpricewise" runat="server" GroupName="Status" Checked="True" />
                                <%=Resources.LabelCaption.lbl_Sellingpricewise%>
                            </div>
                            <div class="spilt">
                                <asp:RadioButton ID="rbnMRPwise" runat="server" GroupName="Status" />
                                <%=Resources.LabelCaption.lbl_MRPWise%>
                            </div>
                        </div>
                        <div class="Promo_Data">
                            <div class="display_table">
                                <div class="float_left" style="width: 30%">
                                    <asp:Label ID="lblfromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                                </div>
                                <div class="float_left" style="width: 70%">
                                    <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control-res" onkeydown="return fncSetfocusprofiltertodate(event)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="display_table" style="margin-top: 2px;">
                                <div class="float_left" style="width: 30%">
                                    <asp:Label ID="lbltodatefilteration" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                                </div>
                                <div class="float_left" style="width: 70%">
                                    <asp:TextBox ID="txttodatefilteration" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncSetfocustoPrice(event)"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="Promo_Data" style="margin-left: 5px">
                            <div class="floatleft" style="width: 30%">
                                <asp:Label ID="lbldepartmentfilteration" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 70%">
                                <%--<asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtDepartmentfilteration" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartmentfilteration', 'txtCategoryfilteration');"></asp:TextBox>
                            </div>
                            <div class="floatleft" style="width: 30%">
                                <asp:Label ID="lblcategoryfilteration" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 70%">
                                <%--<asp:DropDownList ID="ddlCategory" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtCategoryfilteration" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategoryfilteration', 'txtBrandfilteration');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="Promo_Data" style="margin-left: 5px">
                            <div class="floatleft" style="width: 30%">
                                <asp:Label ID="lblbrandfilteration" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 70%">
                                <%--<asp:DropDownList ID="ddlbrand" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtBrandfilteration" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrandfilteration', 'txtVendorfilteration');"></asp:TextBox>
                            </div>
                            <div class="floatleft" style="width: 30%">
                                <asp:Label ID="lblvendorfilteration" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 70%">
                                <%--<asp:DropDownList ID="ddlvendor" runat="server" Style="width: 100%">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVendorfilteration" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendorfilteration', 'txtitemcodefilteration');"></asp:TextBox>
                            </div>
                        </div>
                        <div style="margin-left: 5px; width: 18%; float: left">
                            <div class="floatleft" style="width: 32%; margin-left: 2px">
                                <asp:Label ID="lblItemcodefilteration" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 65%">
                                <asp:TextBox ID="txtitemcodefilteration" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtitemcodefilteration', 'txtSubcategory');" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="floatleft" style="width: 32%; margin-left: 2px">
                                <asp:Label ID="lblSubcategory" runat="server" Text="Sub Category"></asp:Label>
                            </div>
                            <div class="floatleft" style="width: 65%">
                                <asp:TextBox ID="txtSubcategory" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory',  'txtSubcategory', 'lnkFetch');" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="container-group-full">
            <div style="margin-left: 21%;" class="display_none" id="divpromotionprice">
                <div style="width: 15%; float: left">
                    <div class="floatleft" style="width: 45%; margin-left: 2px">
                        <asp:Label ID="lblpromotionprice" runat="server" Text='<%$ Resources:LabelCaption,lbl_ProPrice %>'></asp:Label>
                    </div>
                    <div class="floatleft" style="width: 45%">
                        <asp:TextBox ID="txtpromotionprice" runat="server" CssClass="form-control-res-right" onFocus="this.select()"
                            onkeydown="return fncPromotionpriceEnterkeyvalidation(event)">0</asp:TextBox>
                    </div>
                </div>
                <div style="width: 15%; float: left">
                    <div class="floatleft" style="width: 45%; margin-left: 2px">
                        <asp:Label ID="lblpromotiondisc" runat="server" Text='<%$ Resources:LabelCaption,lbl_Disc %>'></asp:Label>
                    </div>
                    <div class="floatleft" style="width: 45%">
                        <asp:TextBox ID="txtpromotiondisc" runat="server" CssClass="form-control-res-right" onFocus="this.select()" onblur="fncPromotionDiscountFocusout()"
                            onkeydown="fncSetfocustoEligibleQty(event)">0</asp:TextBox>
                    </div>
                </div>
                <div style="width: 15%; float: left">
                    <div class="floatleft" style="width: 45%; margin-left: 2px">
                        <asp:Label ID="lblProQty" runat="server" Text='Promo Qty'></asp:Label>
                    </div>
                    <div class="floatleft" style="width: 45%">
                        <asp:TextBox ID="txtBProQty" runat="server" onFocus="this.select()" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                    </div>
                </div>
                <div style="width: 15%; float: left">
                    <div class="floatleft" style="width: 45%; margin-left: 2px">
                        <asp:Label ID="lbleligibleqty" runat="server" Text='<%$ Resources:LabelCaption,lbl_eligibleQty %>'></asp:Label>
                    </div>
                    <div class="floatleft" style="width: 45%">
                        <asp:TextBox ID="txteligibleqtyfilteration" onFocus="this.select()" runat="server" Text="0" CssClass="form-control-res-right"
                            onkeydown="fncSetFocustoApplyButton(event)"></asp:TextBox>
                    </div>
                </div>
                <div style="width: 5%; margin-left: 25px; float: left">
                    <asp:LinkButton ID="lnkapply" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lnk_apply %>'
                        OnClientClick="fncProfiltervalidation();return false;"></asp:LinkButton>
                </div>
                <div style="width: 5%; margin-left: 25px; float: left">
                    <asp:LinkButton ID="lnkProfilterClear" runat="server" class="button-blue" OnClientClick="fncPromotionFilterClear();return false"
                        Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                </div>
                <div style="width: 5%; margin-left: 25px; float: left" class="display_none">
                    <asp:LinkButton ID="lnkExit" runat="server" class="button-blue" OnClientClick="return fncShowandHidePromotionFilter()"
                        Text='<%$ Resources:LabelCaption,lbtn_exit %>'></asp:LinkButton>
                </div>
                <div class="control-button" style="width: 5%; margin-left: 25px; float: left; margin-top: -1px;">
                    <%--<asp:UpdatePanel ID="upfetch" runat="server" UpdateMode="Always">
                            <ContentTemplate>--%>
                    <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbtn_Fetch %>'
                        OnClick="lnkFetch_Click">
                    </asp:LinkButton>
                    <%-- </ContentTemplate>
                        </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
        <div class="container-group-full">
            <asp:UpdatePanel ID="upgvPromotion" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divPromotion" style="display: block">
                        <div   class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: hidden; height: 330px; background-color: aliceblue;"
                                        id="HideFilter_GridOverFlow" runat="server">
                            <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                            <div class="grdLoad">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Delete
                                            </th>
                                            <th scope="col">ItemCode
                                            </th>
                                            <th scope="col">BatchNo
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">Department
                                            </th>
                                            <th scope="col">Category
                                            </th>
                                            <th scope="col">Brand
                                            </th>
                                            <th scope="col">Qty
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">S.Price
                                            </th>
                                            <th scope="col">FromDate
                                            </th>
                                            <th scope="col">ToDate
                                            </th>
                                            <th scope="col">Pro.Price    
                                            </th>
                                            <th scope="col">DiscPerc
                                            </th>
                                            <th scope="col">PromotionCode
                                            </th>
                                            <th scope="col">PromotionDesc
                                            </th>
                                            <th scope="col">EQty
                                            </th>
                                            <th scope="col">FromTime
                                            </th>
                                            <th scope="col">ToTime
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                                <div style="overflow-x: hidden; overflow-y: scroll;  background-color: aliceblue;width:158%;height:300px;">
                                    <asp:GridView ID="grdPromotion" runat="server" AutoGenerateColumns="False" CssClass="pshro_GridDgn"
                                        Theme="Aqua" KeyFieldName="ItemCode" EnableCallBacks="False" ShowHeader="false">
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                        <PagerStyle CssClass="pshro_text" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/No.png" OnClick="btnRemove_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ItemCode" HeaderText="ItemCode"></asp:BoundField>
                                            <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                            <asp:BoundField DataField="Department" HeaderText="Department"></asp:BoundField>
                                            <asp:BoundField DataField="Category" HeaderText="Category"></asp:BoundField>
                                            <asp:BoundField DataField="Brand" HeaderText="Brand"></asp:BoundField>
                                            <asp:BoundField DataField="Qty" HeaderText="Qty"></asp:BoundField>
                                            <asp:BoundField DataField="Cost" HeaderText="Cost"></asp:BoundField>
                                            <asp:BoundField DataField="Price" HeaderText="S.Price"></asp:BoundField>
                                            <asp:BoundField DataField="FromDate" HeaderText="FromDate"></asp:BoundField>
                                            <asp:BoundField DataField="ToDate" HeaderText="ToDate"></asp:BoundField>
                                            <asp:BoundField DataField="ProPrice" HeaderText="Pro.Price"></asp:BoundField>
                                            <asp:BoundField DataField="DiscPerc" HeaderText="DiscPerc"></asp:BoundField>
                                            <asp:BoundField DataField="PromotionCode" HeaderText="PromotionCode"></asp:BoundField>
                                            <asp:BoundField DataField="PromotionDesc" HeaderText="PromotionDesc"></asp:BoundField>
                                            <asp:BoundField DataField="EQty" HeaderText="EQty"></asp:BoundField>
                                            <asp:BoundField DataField="FromTime" HeaderText="FromTime"></asp:BoundField>
                                            <asp:BoundField DataField="ToTime" HeaderText="ToTime"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="uprepeaterprogress" runat="server">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel ID="uprepeater" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divrepeater" class="fixed_headers BulkPromotion" runat="server" style="display: none">
                        <table id="tblpromotionfilter" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">
                                        <asp:CheckBox ID="cbProfilterheader" onclick="fncSelectUnselectPromotionrow(this)"
                                            runat="server" />
                                    </th>

                                    <th scope="col">ItemCode
                                    </th>
                                    <th scope="col">BatchNo
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">Cost
                                    </th>
                                    <th scope="col">Price
                                    </th>
                                    <th scope="col">MRP
                                    </th>
                                    <th scope="col">FromDate
                                    </th>
                                    <th scope="col">ToDate
                                    </th>
                                    <th scope="col">ProPrice
                                    </th>
                                    <th scope="col">DiscPerc
                                    </th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">EQty</th>
                                    <th scope="col">Department
                                    </th>
                                    <th scope="col">Category
                                    </th>
                                    <th scope="col">Brand
                                    </th>
                                </tr>
                            </thead>
                            <div>
                            </div>
                            <asp:Repeater ID="rptPromotionfilteration" runat="server" OnItemDataBound="rptPromotionfilteration_ItemDataBound">
                                <HeaderTemplate>
                                    <tbody id="promotionfilterbody">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="promotionrow" runat="server">
                                        <td>
                                            <asp:Label ID="lblRowNo" runat="server" Text='<%# Eval("RowNumber") %>' />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbreProfilterrow" runat="server" onclick="fncPromotionPriceValidationSinglecheckboxclick(this)"
                                                Checked='<%# Eval("selection") %>' />
                                        </td>

                                        <td id="rptItemcode" runat="server">
                                            <asp:Label ID="lblreItemCode" runat="server" Text='<%# Eval("InventoryCOde") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreBatchNo" runat="server" Text='<%# Eval("BatchNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>

                                        <td>
                                            <asp:Label ID="lblreCost" runat="server" Text='<%# Eval("UnitCost") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblrePrice" CssClass="reprice" runat="server" Text='<%# Eval("Sellingprice") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreFromDate" runat="server" Text='<%# Eval("FromDate") %>' />
                                            <asp:HiddenField ID="hidfromdate" runat="server" Value='<%#Eval("FromDate")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreToDate" runat="server" Text='<%# Eval("ToDate") %>' />
                                            <asp:HiddenField ID="hidtodate" runat="server" Value='<%#Eval("ToDate")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreProPrice" runat="server" Text='<%# Eval("ProPrice") %>' />
                                            <asp:HiddenField ID="hidProPrice" runat="server" Value='<%#Eval("ProPrice")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreDiscPerc" runat="server" Text='<%# Eval("DiscPerc") %>' />
                                            <asp:HiddenField ID="hidproDiscPerc" runat="server" Value='<%#Eval("DiscPerc")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblProQty" runat="server" Text='<%# Eval("EQty") %>' />
                                            <asp:HiddenField ID="hidProQty" runat="server" Value='<%#Eval("EQty")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreEQty" runat="server" Text='<%# Eval("EQty") %>' />
                                            <asp:HiddenField ID="hidEqty" runat="server" Value='<%#Eval("EQty")%>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreDepartment" runat="server" Text='<%# Eval("Departmentcode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreCategory" runat="server" Text='<%# Eval("categorycode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblreBrand" runat="server" Text='<%# Eval("brandcode") %>' />
                                        </td>
                                        <td style="display: none;" id="Promcode" runat="server">
                                            <asp:HiddenField ID="hidParmcode" runat="server" Value='<%# Eval("PromoCode") %>' />
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                                <tr style="height: 320px" runat="server" id="emptyrow">
                                    <td colspan="16" class="repeater_td_align_center">
                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                            Text="No items to display" />
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div id="divMsg" style="margin-left: 105px; margin-top: 5px;">
                        <label style="background-color: #34d361; color: #FFFFFF;">XXXX</label>
                        <label id="lblPromotionAvailable" runat="server">Already Promotions are Available for this items</label>
                    </div>
                    <div id="rptpagination" style="display: none">
                        <asp:Panel ID="pnlrepeater" runat="server" CssClass="pagingborder">
                            <div style="float: left">
                                <span>
                                    <asp:LinkButton ID="lnkFirst" Text='<%$ Resources:LabelCaption,lbl_First %>' runat="server"
                                        OnCommand="lnkPageNavigation_Command" />
                                </span><span>
                                    <asp:LinkButton ID="lnkPrevious" Text='<%$ Resources:LabelCaption,lbl_Previous %>'
                                        runat="server" OnCommand="lnkPageNavigation_Command" />
                                </span>
                                <asp:Repeater ID="rptrPagination" runat="server" OnItemCommand="rptrPagination_ItemCommand">
                                    <ItemTemplate>
                                        <span id="spanpagination">
                                            <asp:LinkButton ID="lnkrptpagination" Text='<%# Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                                runat="server" CssClass='<%# Convert.ToBoolean(Eval("Enabled")) ? "pager_enabled" : "pager_disabled" %>'
                                                OnClientClick='<%# !Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'> ></asp:LinkButton>
                                        </span>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <span>
                                    <asp:LinkButton ID="lnkNext" Text='<%$ Resources:LabelCaption,lbl_Next %>' runat="server"
                                        OnCommand="lnkPageNavigation_Command" />
                                </span><span>
                                    <asp:LinkButton ID="lnkLast" Text='<%$ Resources:LabelCaption,lbl_Last %>' runat="server"
                                        OnCommand="lnkPageNavigation_Command" />
                                </span>
                            </div>
                            <div style="float: right" class="pagingborder_status">
                                <span id="spanpagestatus">
                                    <asp:Label ID="lblPageStatus" runat="server"></asp:Label>
                                </span>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="divsinglepromotioncontent" class="container-group-full" style="margin-top: 10px; display: block">
            <div style="width: 98%">
                <div class="container1">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown="return fncGetInventoryCode(event)"></asp:TextBox><%--onkeydown="return fncGetInventoryCode(event)"--%>
                    </div>
                </div>
                <%-- <div class="container5">
                    <img id="imgsearch" src="../images/SearchImage.png" onclick="return fncShowPopUp()"
                        height="20px" width="20px" alt="search" />
                </div>--%>
                <div class="container3">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container1">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblDepartment" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container1">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblCategory" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container1">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblBrand" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblCost" runat="server" Text="Net Cost"></asp:Label><%--'<%$ Resources:LabelCaption,lbl_cost %>'--%>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtCost" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblPrice" runat="server" Text="S.Price"></asp:Label>
                        <%--'<%$ Resources:LabelCaption,lbl_price %>'--%>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblMRP" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtMRP" runat="server" CssClass="form-control-res" Enabled="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container1" style="width: 6%">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblFrom" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtFrom" runat="server" CssClass="form-control-res" onkeydown="return fncSetFocustodate(event)"></asp:TextBox>
                    </div>
                </div>
                <div class="container1" style="width: 6%">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center" id='divtxtToDate'>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblProPrice" runat="server" Text="Pro Price"></asp:Label><%--'<%$ Resources:LabelCaption,lbl_ProPrice %>'--%>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtPPrice" runat="server" CssClass="form-control-res" onkeypress="return isNumberKeywithDecimal(event)"
                            onkeydown="return fncDiscountPerCal(event);" onchange="return fncDiscountcal();"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblDisc" runat="server" Text='<%$ Resources:LabelCaption,lbl_Discperc %>'></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtDiscountPerc" runat="server" CssClass="form-control-res" Style="text-align: right"
                            onkeypress="return isNumberKeywithDecimal(event)" onkeydown="return fncPromotionPriceCal(event)" onchange="return fncPromotionPriceCalmouse()"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="lblQty" runat="server" Text="Pro Qty"></asp:Label>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtProQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                            onkeypress="return isNumberKey(event)" Text="0" ReadOnly="false"></asp:TextBox>
                    </div>
                </div>
                <div class="container2">
                    <div class="spilt" align="center">
                        <asp:Label ID="ligibleQty" runat="server" Text="Elg Qty"></asp:Label><%--'<%$ Resources:LabelCaption,lbl_EQty %>'--%>
                    </div>
                    <div class="spilt" align="center">
                        <asp:TextBox ID="txtEligibleQty" runat="server" CssClass="form-control-res" Style="text-align: right"
                            onkeypress="return isNumberKey(event);" onkeydown="return fncCheckPromotionMode(event);">1</asp:TextBox>
                    </div>
                </div>
                <div class="control-button" style="margin: 20px">
                    <asp:UpdatePanel ID="upadd" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" OnClick="lnkAdd_Click"
                                OnClientClick="return fncAddvalidation()" Text='<%$ Resources:LabelCaption,btnAdd %>'></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="margin-top: 10px">
            <div style="width: 98%">
                <div style="float: left; width: 60%;">
                    <div class="Pro_border">
                        <div class="float_left" style="padding-right: 5px; padding-left: 5px;">
                            <asp:RadioButton ID="rbnNormalPro" runat="server" Checked="true" Text="Normal Promotion" onchange="fncPromotionChange();" GroupName="Promotion" />
                        </div>
                        <div class="float_left">
                            <asp:RadioButton ID="rbnHourlyPro" runat="server" Text="Hourly Promotion" onchange="fncPromotionChange();" GroupName="Promotion" />
                        </div>
                    </div>
                    <div id="proTime" class="Pro_time">
                        <div class="promotion_time" style="padding-left: 5px;">
                            <asp:Label ID="lblTime" runat="server" Text="From Time"></asp:Label>
                        </div>
                        <div class="promotion_time">
                            <asp:TextBox ID="txtFrmTime" Text="0" runat="server" onkeypress="return isNumberKey(event);" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="promotion_time" style="padding-left: 5px;">
                            <asp:Label ID="lblFromTime" runat="server" Text="To Time"></asp:Label>
                        </div>
                        <div class="promotion_time">
                            <asp:TextBox ID="txtToTime" Text="0" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event);"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div id="divCount">
                    <asp:Label ID="lblRow" runat="server"></asp:Label>
                </div>
                <div style="margin-right: 18px; float: right; width: 30%;">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkPromotionfilteration" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbtn_promotionfilteration %>'
                            OnClientClick="return fncShowandHidePromotionFilter()"></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return fncSaveValidation()"
                                    OnClick="lnkSave_Click" Text='Save'></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnksubClear" runat="server" class="button-blue" OnClientClick="return Clear()"
                            Text='Clear'></asp:LinkButton>
                    </div>


                    <%--<div class="control-button">
                    <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" OnClick="lnkClose_Click">Close</asp:LinkButton>
                </div>--%>
                </div>
            </div>
        </div>
        <div>
            <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
            <asp:HiddenField ID="hidbatchno" runat="server" Value="" />
            <asp:HiddenField ID="batchbasiccost" runat="server" Value="0" />
            <asp:HiddenField ID="batchmrp" runat="server" Value="0" />
            <asp:HiddenField ID="batchselprice" runat="server" Value="0" />
            <asp:HiddenField ID="hidshowrepeater" runat="server" Value="0" />
            <asp:HiddenField ID="hidAllowPromotion" runat="server" Value="Block" />
        </div>
        <div id="RevokeOption">
            <p>
                <%=Resources.LabelCaption.Already_InPromotion%>
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return fncRevokePromotionItem()"
                        Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div id="promotionsave">
            <p>
                <%--<%=Resources.LabelCaption.Save_PromotionPriceChange%>--%>
                Promomtion Set Successfully
            </p>
            <div style="margin: auto; width: 100px">
                <asp:UpdatePanel ID="upok" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lblOk %>'
                            OnClick="lnkbtnOk_Click" OnClientClick="fncUnSelectLocationList()"> </asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="hiddencol">
            <div id="dialog-MessageBoxProPrice" style="display: none">
            </div>

            <div id="dialog-Alreadypromotion-confirm" title="Enterpriser">
                <p>
                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>Promomtion Price is Less than NetCost.Do you want to Continue?
                </p>
            </div>
            <div id="dialog-BasicCost-Validation" title="Enterpriser">
                <p>
                    <span class="ui-icon ui-icon-alert" style="float: left; margin: 1px 5px 20px 0;"></span>Promomtion Price is Less than NetCost.Do you want to Continue?
                </p>
            </div>
        </div>
        <div class="hiddencol">
            <div id="Promotion_Details">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <label id="lblInventoryCode" style="font-weight: 800;"></label>
                    </div>
                    <div class="col-md-9">
                        <label id="lblPromoDescription" style="font-weight: 800;"></label>
                    </div>
                </div>
                <div class="Payment_fixed_headers BatchDetail">
                    <table id="tblPromotion" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th scope="col">BatchNo     
                                </th>
                                <th scope="col">From Date
                                </th>
                                <th scope="col">To Date
                                </th>
                                <th scope="col">Promotion Price
                                </th>
                                <th scope="col">Location
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <p>
                        <span class="ui-icon ui-icon-alert" style="float: left; margin: 10px 5px 20px 0;"></span>
                        <label id="lblAlert" style="margin-top: 10px; font-weight: 800;">Do you want to Edit this Promotion after revoking this Promotion?</label>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidUom" runat="server" />
</asp:Content>
