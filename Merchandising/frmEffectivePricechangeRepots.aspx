﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmEffectivePricechangeRepots.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmEffectivePricechangeRepots" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
      
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }



        .gid_LocBatchInfoAndModification td:nth-child(1), .gid_LocBatchInfoAndModification th:nth-child(1) {
            min-width: 59px;
            max-width: 59px;
            text-align: center;
        }

        .gid_LocBatchInfoAndModification td:nth-child(2), .gid_LocBatchInfoAndModification th:nth-child(2) {
           min-width: 59px;
            max-width: 59px;
            text-align: center;
        }

        .gid_LocBatchInfoAndModification td:nth-child(3), .gid_LocBatchInfoAndModification th:nth-child(3) {
           min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(4), .gid_LocBatchInfoAndModification th:nth-child(4) {
            min-width: 250px;
            max-width: 250px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(5), .gid_LocBatchInfoAndModification th:nth-child(5) {
           min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(6), .gid_LocBatchInfoAndModification th:nth-child(6) {
           min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(7), .gid_LocBatchInfoAndModification th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(8), .gid_LocBatchInfoAndModification th:nth-child(8) {
             min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(9), .gid_LocBatchInfoAndModification th:nth-child(9) {
            min-width: 70px;
            max-width: 70px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(10), .gid_LocBatchInfoAndModification th:nth-child(10) {
            min-width: 70px;
            max-width: 70px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(11), .gid_LocBatchInfoAndModification th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }
        .gid_LocBatchInfoAndModification td:nth-child(12), .gid_LocBatchInfoAndModification th:nth-child(12) {
            min-width: 150px;
            max-width: 150px;
        }
       
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'EffectivePriceChangeReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "EffectivePriceChangeReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>



    <script type="text/javascript">
        function pageLoad() {


        }

        function fncPriceChange(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowobj = $(lnk).parent().parent();
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False  
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncChangeSellingQty(lnk, obj, evt, nextCol, prvCol) { //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var row = lnk.parentNode.parentNode;
                var rowobj = $(lnk).parent().parent();

                if (charCode == 13) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="' + obj + '"]').select();
                    }
                    else {
                        rowobj.siblings().first().find('td input[id*="' + obj + '"]').select();
                    }
                    return false;
                }
                else if (charCode == 39) {
                    setTimeout(function () {
                        rowobj.find('td input[id*="' + nextCol + '"]').select();
                    }, 10);
                }
                else if (charCode == 37) {
                    setTimeout(function () {
                        rowobj.find('td input[id*="' + prvCol + '"]').select();
                    }, 10);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            fncDecimal();
        });

        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupBatchInfo').dialog('close');
                window.parent.$('#popupBatchInfo').remove();
            }
        });

        function disableFunctionKeys(e) {
            try {
                debugger;
                var functionKeys = new Array(13, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {

                        e.preventDefault();
                        return false;
                    }
                    else if (e.keyCode == 117) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClearAll', '');
                        fncToastInformation("Cleared");
                        e.preventDefault();
                        return false;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function fncDecimal() {
            try {
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_0"]').select();
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice1_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice2_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice3_"]').number(true, 2);

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        var validation = false;
        function txtSellingPrice_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;

                if (sSellingPrice != "") {
                    if (parseFloat(sSellingPrice) > parseFloat(sMRP)) {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("Selling Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }

                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice1_TextChanged(lnk) {
            try {

                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sWholeSalePrice1 = row.cells[7].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice1 != "") {
                    if (parseFloat(sWholeSalePrice1) > parseFloat(sMRP)) {
                        row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("WPrice1 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice1) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false;  
                    //    validation = true;
                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //} 
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;
                row.cells[8].getElementsByTagName("input")[0].select();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice2_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sWholeSalePrice2 = row.cells[8].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice2 != "") {
                    if (parseFloat(sWholeSalePrice2) > parseFloat(sMRP)) {
                        row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("WPrice2 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice2_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice2) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; 
                    //    validation = true;
                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //} 
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;
                row.cells[9].getElementsByTagName("input")[0].select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice3_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sWholeSalePrice3 = row.cells[9].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice3 != "") {
                    if (parseFloat(sWholeSalePrice3) > parseFloat(sMRP)) {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;

                        Confirm("WPrice3 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice3_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice3) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; 

                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice3_" + rowIndex);

                    //    return;
                    //} 
                }
                row.cells[0].getElementsByTagName("input")[0].checked = true;
                row.cells[6].getElementsByTagName("input")[0].select();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        function txtSellingPrice_Keystroke(event, lnk) {
            debugger;
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[7].getElementsByTagName("input")[0].select();
                    event.preventDefault();
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault();
            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtSellingPrice"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtSellingPrice"]').select();
                }
            }
            return false;
        }

        function txtlblWPrice1_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;

            if (charCode == 13) {
                if (validation == false) {
                    row.cells[8].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice1"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice1"]').select();
                }
            }
            return false;
        }

        function txtlblWPrice2_Keystroke(event, lnk) {
            debugger;
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[9].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice2"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice2"]').select();
                }
            }
            return false;


        }
          function Class_Delete(source) {            
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        $('#<%=hdnInventory.ClientID %>').val($("td", $(source).closest("tr")).eq(2).html().replace(/&nbsp;/g, ''));
                        $('#<%=hdnBatch.ClientID %>').val($("td", $(source).closest("tr")).eq(4).html().replace(/&nbsp;/g, ''));
                          $('#<%=hdnLocation.ClientID %>').val($("td", $(source).closest("tr")).eq(10).html().replace(/&nbsp;/g, ''));
                        $("#<%= btnDelete.ClientID %>").click();
                    },
                    "NO": function () {
                        $(this).dialog("close");
                        returnfalse();
                    }
                }
            });

          }
         function fncSetValue() {
        try {
            if (SearchTableName == "Inventory") {
                $('#<%=txtInventory.ClientID %>').val($.trim(Code)); 
            }
            if (SearchTableName == "Location") {
                $('#<%=txtLocation.ClientID %>').val($.trim(Code)); 
            }
            if (SearchTableName == "BatchNo") {
                $('#<%=txtBatchNo.ClientID %>').val($.trim(Code)); 
            }   
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
        function clearForm() {
                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    $(':checkbox, :radio').prop('checked', false);
                    $('#<%=txtInventory.ClientID %>').focus();
                    return false;
                }
        function txtlblWPrice3_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[6].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice3"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice3"]').select();
                }
            }
            return false;
        }
        function Confirm(Mgs, object) {
            popUpObjectForSetFocusandOpen = $(object);
            ShowPopupMessageBoxandFocustoObject(Mgs);
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="../Merchandising/frmPriceChangeEffectiveDate.aspx">PriceChange Effective</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">PriceChange Report</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">        
        <ContentTemplate>
            <div class="main-container">
                <div class="container-group-full">
                    <div class="container-left-price" id="pnlFilter" runat="server">

                        <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
                            <ContentTemplate>

                                <div class="control-group-price-header">
                                    Filtertions
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtInventory" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtInventory', 'txtLocation');" CssClass="form-control" Style="width: 100%"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label5" runat="server" Text="Location"></asp:Label>
                                    </div>
                                    <div class="label-right">
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtBatchNo');"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-single-res">
                                    <div class="label-left">
                                        <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
                                    </div>
                                    <div class="label-right">
                                  <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15"  CssClass="form-control-res"></asp:TextBox>
                                   </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                        <asp:UpdatePanel ID="UpdatePanel4" runat="Server">
                            <ContentTemplate>
                                <div class="control-container">
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Filter %>'
                                            OnClick="lnkFilter_Click"></asp:LinkButton>
                                    </div>
                                    <div class="control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm()"
                                            Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="over_flowhorizontal">
                        <table id="tblLocList" cellspacing="0" rules="all" border="1" class="fixed_header gid_LocBatchInfoAndModification">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Delete
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Name
                                    </th>
                                    <th scope="col">Batch No
                                    </th>
                                    <th scope="col">Old S.Price
                                    </th>
                                    <th scope="col">S.Price
                                    </th>                                 
                                    <th runat="server" id="thWPricedet1" scope="col">WPrice1
                                    </th>
                                    <th runat="server" id="thWPricedet2" scope="col">WPrice2
                                    </th>
                                    <th runat="server" id="thWPricedet3" scope="col">WPrice3
                                    </th>
                                    <th scope="col">LocationCode
                                    </th>
                                    <th scope="col">EffectiveDate
                                    </th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <div id="gvOutDet" runat="server" class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 420px; width: 1270px; background-color: aliceblue;">
                            <asp:GridView ID="gvOutletDetails" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" CssClass="gid_LocBatchInfoAndModification" ShowHeader="false" OnRowDataBound="gvOutletDetails_DataBound">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="" />
                                <Columns>
                                     <asp:BoundField DataField="RowNo" HeaderText="SNo"></asp:BoundField>
                                  <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnDelete" runat="server" OnClientClick=" return Class_Delete(this); return false;"
                                                                CommandName="Select" ImageUrl="~/images/No.png" ToolTip="Click here to Delete" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                   <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                     <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                   <asp:BoundField DataField="BatchNo" HeaderText="Batch No" />
                                    <asp:BoundField DataField="OldSellingPrice" ItemStyle-CssClass="left_align" HeaderText="OldSellingPrice"></asp:BoundField>
                                     <asp:BoundField DataField="SellingPrice" ItemStyle-CssClass="left_align" HeaderText="SellingPrice"></asp:BoundField>
                                    <asp:BoundField DataField="WPrice1" ItemStyle-CssClass="left_align" HeaderText="WPrice1"></asp:BoundField>
                                    <asp:BoundField DataField="WPrice2" ItemStyle-CssClass="left_align" HeaderText="WPrice2"></asp:BoundField>
                                    <asp:BoundField DataField="WPrice3" ItemStyle-CssClass="right_align" HeaderText="WPrice3"></asp:BoundField>
                                     <asp:BoundField DataField="LocationCode" ItemStyle-CssClass="left_align" HeaderText="LocationCode"></asp:BoundField>                                                                   
                                    <asp:BoundField DataField="EffectiveDate" ItemStyle-CssClass="right_align" HeaderText="EffectiveDate"></asp:BoundField>                                    
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                                       
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>   
         <div class="hiddencol">

                    <asp:Button ID="btnDelete" runat="server" OnClick="imgbtnDelete_Click" />

                </div>
     <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                    <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
                </div>
                <asp:HiddenField ID="hdnInventory" Value="" runat="server" />
     <asp:HiddenField ID="hdnBatch" Value="" runat="server" />
     <asp:HiddenField ID="hdnLocation" Value="" runat="server" />
        <div class="display_none">
            <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
            </asp:UpdatePanel>
            <asp:HiddenField ID="hidItemcode" runat="server" Value="" />
            <asp:HiddenField ID="hidBatchNo" runat="server" Value="" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>    
</asp:Content>
