﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCustomerTypePromotion.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmCustomerTypePromotion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">

            .Free_Header_Text {
    border: 1px solid black;
    padding: 5px;
    width: 167%;
}
            .qualifyingItem td:nth-child(1), .qualifyingItem th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

            .qualifyingItem td:nth-child(2), .qualifyingItem th:nth-child(2) {
                min-width: 45px;
                max-width: 45px;
            }

            .qualifyingItem td:nth-child(3), .qualifyingItem th:nth-child(3) {
                min-width: 80px;
                max-width: 80px;
            }

            .qualifyingItem td:nth-child(4), .qualifyingItem th:nth-child(4) {
                 min-width: 258px;
                 max-width: 258px;
            }

            .qualifyingItem td:nth-child(5), .qualifyingItem th:nth-child(5) {
                min-width: 100px;
                max-width: 100px;
            }

            .qualifyingItem td:nth-child(6), .qualifyingItem th:nth-child(6) {
                min-width: 100px;
                max-width: 100px;
                
            }


            .freeItem td:nth-child(1), .freeItem th:nth-child(1) {
                min-width: 100%;
                max-width: 100%;
            }

            .freeItem td:nth-child(2), .freeItem th:nth-child(2) {
                min-width: 100%;
                max-width: 100%;
            }

        .freeItem td:nth-child(3), .freeItem th:nth-child(3) {
            min-width: 100%;
            max-width: 100%;
        }

        .freeItem td:nth-child(4), .freeItem th:nth-child(4) {
            min-width: 100%;
            max-width: 100%;
        }

        .freeItem td:nth-child(5), .freeItem th:nth-child(5) {
            min-width: 100%;
            max-width: 100%;
        }

        .freeItem td:nth-child(5), .freeItem th:nth-child(5) {
            min-width: 100%;
            max-width: 100%;
        }

        .freeItem td:nth-child(6), .freeItem th:nth-child(6) {
            min-width: 100%;
            max-width: 100%;
        }

        .freeItem td:nth-child(7), .freeItem th:nth-child(7) {
            min-width: 100%;
            max-width: 100%;
        }
        .Payment_fixed_headers th, .Payment_fixed_headers td {
            text-align: center;
            padding: 2px 2px;
            word-wrap: break-word;
            top: -31px;
            /* left: 228px; */
            width: 100px;
            /* border-color: black; */
            font-weight: 700;
        }
            </style>
     <script type="text/javascript">
         $(document).ready(function (e) {
             $('#<% =txtPromotionPrice.ClientID %>').keydown(function (e) {
                  if (e.keyCode == 13) {
                      $('#<%=txtMaximumLimit.ClientID %>').focus();
                }
            });
         });
         $(document).ready(function (e) {
             $('#<% =txtMaximumLimit.ClientID %>').keydown(function (e) {
                 if (e.keyCode == 13) {
                     $('#<%=lnkAdd.ClientID %>').focus();
                 }
             });
         });
         var QualifyingLastRowNo, freeItemLastRowNo, mode;
         function pageLoad() {
            <%-- $("#<%=txtCustomerType.ClientID %>").focus();--%>
            mode = "";
            QualifyingLastRowNo = 0;
            freeItemLastRowNo = 0;

            $("#<%=txtfrmDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%=txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "1");
            //fncLoadFreeItemForView();
            //fncDecimal();
         }
         function fncSetValue() {
             try {
                 var obj = {}, objInventory;
                  obj.Itemcode = Code;
                 $.ajax({
                     type: "POST",
                     url: '<%=ResolveUrl("~/Merchandising/frmCustomerTypePromotion.aspx/GetItemDetailsForCustomerPromotion")%>',
                     data: JSON.stringify(obj),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (msg) {

                         objInventory = jQuery.parseJSON(msg.d);
                         $('#<%=hidBatch.ClientID%>').val(objInventory.Table[0]["Batch"]);
                         
                         //console.log(objInventory);
                         if (objInventory.Table.length > 0) {
                             if (objInventory.Table[0]["Batch"] == true) {
                                 fncGetInventoryCode(objInventory.Table[0]["InventoryCode"].trim());
                             }
                             else {
                                 $('#<%=txtSellingPrice.ClientID%>').val(objInventory.Table[0]["NetSellingPrice"]);
                                 $('#<%=txtItemdesc.ClientID%>').val(objInventory.Table[0]["Description"]);
                                 $('#ContentPlaceHolder1_txtItemCode').attr("disabled", "disabled");
                                 $('#<%=txtPromotionPrice.ClientID %>').focus();
                             }
                         }

                     }
                 });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncGetInventoryCode(ItemCode) {
             try {
                 $('#<%=Hidinv.ClientID %>').val(ItemCode);
                var obj = {};
                obj.code = ItemCode;
                if (obj.code == "")
                    return;
                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/Merchandising/frmCustomerTypePromotion.aspx/fncGetInventoryDetailBatch")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "[]") {
                            ShowPopupMessageBox("Invalid ItemCode. Please Enter Valid Parent Bulk Item !.");
                            return false;
                        }
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncAssignValuestoTextBox(msg) {
             try {
                 var inputNumber = $('#<%=Hidinv.ClientID %>').val();
                 var regexp = /^[0-9]*$/;
                 var numberCheck = regexp.exec(inputNumber);
                 var objBatch, row;
                 objBatch = jQuery.parseJSON(msg.d);
                 if (objBatch.length > 0) {

                     if (objBatch.length == 1 && numberCheck != null && ($('#<%=Hidinv.ClientID %>').val().length == 15
                         || $('#<%=Hidinv.ClientID %>').val().length == 12)) {
                         if (objBatch[0]["BatchNo"] != "") {
                             $('#<%=txtItemCode.ClientID %>').val($.trim(objBatch[0]["InventoryCode"]));
                             $('#<%=txtBatchNo.ClientID %>').val($.trim(objBatch[0]["BatchNo"]));
                             $('#<%=txtSellingPrice.ClientID %>').val($.trim(objBatch[0]["SellingPrice"]));
                             $('#<%=txtItemdesc.ClientID %>').val($.trim(objBatch[0]["Description"]))
                           <%-- $('#<%=txtPCSQty.ClientID %>').focus();--%>
                         }
                     }
                         else {
                             tblBatchBody = $("#tblBatch tbody");
                             tblBatchBody.children().remove();
                             for (var i = 0; i < objBatch.length; i++) {
                                 row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                     $.trim(objBatch[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                     $.trim(objBatch[i]["BatchNo"]) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                     objBatch[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                     $.trim(objBatch[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                                     $.trim(objBatch[i]["Description"]) + "</td></tr>";

                                 tblBatchBody.append(row);

                             }

                             fncInitializeBatchDetail();
                             tblBatchBody.children().dblclick(fncBatchRowClick);

                             tblBatchBody[0].setAttribute("style", "cursor:pointer");

                             tblBatchBody.children().on('mouseover', function (evt) {
                                 var rowobj = $(this);
                                 rowobj.css("background-color", "Yellow");
                             });

                             tblBatchBody.children().on('mouseout', function (evt) {
                                 var rowobj = $(this);
                                 rowobj.css("background-color", "white");
                             });

                             tblBatchBody.children().on('keydown', function () {
                                 fncBatchKeyDown($(this), event);
                             });

                             $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                             $("#tblBatch tbody > tr").first().focus();
                         }

                     fncBatchDeleteCheck($.trim(objBatch[0]["InventoryCode"]));
                 }
             }
             
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncBatchKeyDown(rowobj, evt) {
             var rowobj, charCode;
             var scrollheight = 0;
             try {
                 var charCode = (evt.which) ? evt.which : evt.keyCode;

                 if (charCode == 13) {
                     rowobj.dblclick();
                     return false;
                 }
                 else if (charCode == 40) {

                     var NextRowobj = rowobj.next();
                     if (NextRowobj.length > 0) {
                         NextRowobj.css("background-color", "Yellow");
                         NextRowobj.siblings().css("background-color", "white");
                         NextRowobj.select().focus();

                         setTimeout(function () {
                             scrollheight = $("#tblBatch tbody").scrollTop();
                             if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                 $("#tblBatch tbody").scrollTop(0);
                             }
                             else if (parseFloat(scrollheight) > 10) {
                                 scrollheight = parseFloat(scrollheight) - 10;
                                 $("#tblBatch tbody").scrollTop(scrollheight);
                             }
                         }, 50);
                     }
                 }
                 else if (charCode == 38) {
                     var prevrowobj = rowobj.prev();
                     if (prevrowobj.length > 0) {
                         prevrowobj.css("background-color", "Yellow");
                         prevrowobj.siblings().css("background-color", "white");
                         prevrowobj.select().focus();

                         setTimeout(function () {
                             scrollheight = $("#tblBatch tbody").scrollTop();
                             if (parseFloat(scrollheight) > 3) {
                                 scrollheight = parseFloat(scrollheight) + 1;
                                 $("#tblBatch tbody").scrollTop(scrollheight);
                             }
                         }, 50);

                     }
                 }
             }
             catch (err) {
                 fncToastError(err.message);
             }
         }

         function fncBatchRowClick() {
             try {
                 fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }

         function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) {
             try {
                 setTimeout(function () {
                     $('#<%=txtItemCode.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));
                     $('#<%=txtBatchNo.ClientID %>').val($.trim($(rowObj).find('td[id*=tdBatchNo]').text()));
                     $('#<%=txtItemdesc.ClientID %>').val($.trim($(rowObj).find('td[id*=tdDesc]').text()));
                     $('#ContentPlaceHolder1_txtItemCode').attr("disabled", "disabled");
                     $('#HiddenMrp').val($.trim($(rowObj).find('td[id*=tdMRPNew]').text()));
                    $('#<%=txtSellingPrice.ClientID %>').val($.trim($(rowObj).find('td[id*=tdSellingPrice]').text()));
                    $("#stkadj_batch").dialog('close');
               $('#<%=txtPromotionPrice.ClientID %>').focus();
                }, 50);
                 return false;
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncInitializeBatchDetail() {
             try {
                 $("#stkadj_batch").dialog({
                     titt: "Batch Details",
                     resizable: false,
                     height: "auto",
                     width: "auto",
                     modal: true
                 });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncBatchDeleteCheck(ItemCode) {
             try {
                 var obj = {}
                 obj.Inventorycode = ItemCode;
                 $.ajax({
                     type: "POST",
                     url: "../Stock/frmStockAdjustmentEntry.aspx/fncGetInventoryDetail_BatchDelete",
                     data: JSON.stringify(obj),
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (msg) {
                         fncBatchDeleteInventories(msg);
                     },
                     error: function (data) {
                         ShowPopupMessageBox(data.message);
                     }
                 });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }


         function fncBatchDeleteInventories(source) {
             try {
                 MRPDEL = "";
                 var obj = {};
                 obj = jQuery.parseJSON(source.d);
                 if (obj.length > 0) {
                     for (var i = 0; i < obj.length; i++) {
                         MRPDEL += obj[i]["MRP"] + ',';
                     }
                 }
                 //console.log(MRPDEL);
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncQItemClear() {
             //alert('test');
             try {
                 $('#<%=txtItemCode.ClientID %>').val('');
                $('#<%=txtPromotionPrice.ClientID %>').val(0.00);
                $('#<%=txtMaximumLimit.ClientID %>').val(0);
                 $('#<%=txtBatchNo.ClientID %>').val('');
                 $('#<%=txtItemdesc.ClientID %>').val('');
                 $('#<%=txtSellingPrice.ClientID %>').val(0.00);
                 $('#<%=txtItemCode.ClientID %>').removeAttr("disabled");
                 return false;
             }
             catch (err) {
                 return false;
                 alert(err.Message);
             }
         }
         function fncQItemAddValidation() {
             try {
                 var status = true;
                 var obj = {};
                 obj.itemcode = $("#<%=txtItemCode.ClientID %>").val();
                obj.BatchNo = $("#<%=txtBatchNo.ClientID %>").val();
                 obj.FromDate = $("#<%=txtfrmDate.ClientID %>").val();
                 obj.ToDate = $("#<%=txtToDate.ClientID %>").val();
                if (obj.itemcode == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtItemCode.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_EntItemcode%>');
                    return false;
                }
                else if ($("#<%=hidBatch.ClientID %>").val() == "1" && $("#<%=txtBatchNo.ClientID %>").val() == "") {
                    mode = "QItem";
                    fncGetBatchDetail_Master($('#<%=txtItemCode.ClientID %>').val());
                    return false;
                }
                else if ($("#<%=txtMaximumLimit.ClientID %>").val() == "" || parseFloat($("#<%=txtMaximumLimit.ClientID %>").val()) == 0) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtMaximumLimit.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Qty%>');
                    return false;
                }
                else if ($("#<%=txtPromotionPrice.ClientID %>").val() == "" || parseFloat($("#<%=txtPromotionPrice.ClientID %>").val()) == 0) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtPromotionPrice.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Amount%>');
                    return false;
                }
                else if ($("#tblQualifyingItem tbody").children().length > 0) {
                    $("#tblQualifyingItem tbody").children().each(function () {
                        if ($(this).find('td[id*=tdQItemCode]').text() == obj.itemcode && $(this).find('td[id*=tdQBatchNo]').text() == obj.BatchNo) {
                            status = false;
                            return;
                        }
                    });
                }

                if (status == false) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_alreadyadded%>');
                    fncQItemClear();
                    return;
                }



                $.ajax({
                    type: "POST",
                    url: "frmCustomerTypePromotion.aspx/fncQItemAvailableOrNot",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "available") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.CustomerWisePromotion%>');
                            fncQItemClear();
                        }
                        else {
                            fncAddQualifyingItem();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }

         function fncAddQualifyingItem() {
             try {
                 var buyQty, tblQualifyingItem;

                 tblQualifyingItem = $("#tblQualifyingItem tbody");
                 QualifyingLastRowNo = parseInt(QualifyingLastRowNo) + 1;
                 row = "<tr><td id='tdRowNo_" + QualifyingLastRowNo + "' > " +
                     QualifyingLastRowNo + "</td><td> " +
                     "<img alt='Delete' src='../images/No.png' onclick='fncDeleteQualifyingItem(this);return false;' /></td><td id='tdItemCode_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtItemCode.ClientID %>').val() + "</td><td id='tdItemDesc_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtItemdesc.ClientID %>').val() + "</td><td id='tdBatchNo_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtBatchNo.ClientID %>').val() + "</td><td id='tdMaximumLimitQty_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtMaximumLimit.ClientID %>').val() + "</td><td id='tdPromotionPrice_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtPromotionPrice.ClientID %>').val() + "</td><td id='tdSellingPrice_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtSellingPrice.ClientID %>').val()+ "</td><td id='tdCustomerType_" + QualifyingLastRowNo + "' >" +
                     $('#<%=ddlCustomerType.ClientID %>').val() +"</td><td id='tdValidFromDate_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtfrmDate.ClientID %>').val()+ "</td><td id='tdValidToDate_" + QualifyingLastRowNo + "' >" +
                     $('#<%=txtToDate.ClientID %>').val() +"</td></tr>";
            tblQualifyingItem.append(row);
            fncQItemClear();

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
         function fncDeleteQualifyingItem(source) {
             try {
                 var RowNo = 0, buyQty = 0;
                 $(source).parent().parent().remove();
                 if ($("#tblQualifyingItem tbody").children().length > 0) {
                     $("#tblQualifyingItem tbody").children().each(function () {
                         $(this).find('td[id*=tdRowNo]').text(RowNo + 1);
                         //buyQty = buyQty + parseInt($(this).find('td[id*=tdLimitQty]').text());
                     });
                     QualifyingLastRowNo = RowNo + 1;
                     $('#<%=txtMaximumLimit.ClientID %>').val(buyQty);
                }
                else {
                    QualifyingLastRowNo = 0;
                    $('#<%=txtMaximumLimit.ClientID %>').val('0');
                    }

                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
         }
         function fncSaveCustomerWisePromotion() {
             try {

                 if (fncSaveValidation() == false) {
                     return false;
                 }

                 var rowGroupCode, rowQItem, rowFItem;
                 var frmDate, toDate, desc;

                 frmDate = $('#<%=txtfrmDate.ClientID %>').val();
        frmDate = frmDate.split("/").reverse().join("-");

        toDate = $('#<%=txtToDate.ClientID %>').val();
        toDate = toDate.split("/").reverse().join("-");
        //Qualifying Item xml   
        rowQItem = "<table>";
                 $("#tblQualifyingItem tbody").children().each(function () {
                     rowQItem = rowQItem + "<CustomerWisePromtion CustomerType='" + $('#<%=ddlCustomerType.ClientID %>').val().trim() + "'";
            rowQItem = rowQItem + " FromDate='" + $.trim($(this).find('td[id*=tdValidFromDate]').text()) + "'";
            rowQItem = rowQItem + " Todate='" + $.trim($(this).find('td[id*=tdValidToDate]').text()) + "'";
            rowQItem = rowQItem + " ItemCode='" + $.trim($(this).find('td[id*=tdItemCode]').text()) + "'";
            rowQItem = rowQItem + " Description='" + $.trim($(this).find('td[id*=ItemDesc]').text()) + "'";
            rowQItem = rowQItem + " BatchNo='" + $.trim($(this).find('td[id*=tdBatchNo]').text()) + "'";
            rowQItem = rowQItem + " MaximumLimitQty='" + $.trim($(this).find('td[id*=tdMaximumLimitQty]').text()) + "'";
            rowQItem = rowQItem + " PromtionPrice='" + $.trim($(this).find('td[id*=tdPromotionPrice]').text()) + "'";
            rowQItem = rowQItem + " SellingPrice='" + $.trim($(this).find('td[id*=tdSellingPrice]').text()) + "'";
                     rowQItem = rowQItem + "></CustomerWisePromtion>";
                });
                 rowQItem = rowQItem + "</table>";

                 $('#<%=hidCustomerWisePromotion.ClientID %>').val(escape(rowQItem));
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncSaveValidation() {
             try {
                 var frmDate, toDate;
                 frmDate = $('#<%=txtfrmDate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

                if ($("#tblQualifyingItem tbody").children().length == 0 ) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false
                }
                else if (new Date(frmDate) < new Date(currentDatesqlformat_Master)) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_frmDate%>');
                    return false;
                }
                else if (new Date(toDate) < new Date(frmDate)) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_freeitemtoDate%>');
                    return false;
                }

             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
         function fncClearAll() {
             try {
                 
                  $("#tblQualifyingItem tbody").children().remove();
                 

              }
              catch (err) {
                  ShowPopupMessageBox(err.message);
              }
          }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Merchandising/frmCustomerTypePromotionView.aspx">CustomerWisePromotion List</a></li>
                <li class="active-page">CustomerWisePromotion</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
    <div class="freeItemEntry_header_left">
    <div class="Free_Header_Text">
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblCustomerType" runat="server" Text='CustomerType'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:DropDownList ID="ddlCustomerType" runat="server" CssClass="form-control-res" Width="100%">
                        </asp:DropDownList>
                                <%--<asp:TextBox ID="txtCustomerType" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblfrmDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtfrmDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                      <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblItemCode" runat="server" Text='ItemCode' ></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory', 'txtItemCode', 'txtItemName');"></asp:TextBox>
                            </div>
                        </div>
                    <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="Label3" runat="server" Text='ItemDesc' ></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtItemdesc" runat="server" CssClass="form-control-res" Enabled="false" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblBatchNo" runat="server" Text='BatchNo'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res-right" Enabled="false" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblSellingPrice" runat="server" Text='SellingPrice'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtSellingPrice" runat="server" CssClass="form-control-res-right" Enabled="false">0.00</asp:TextBox>
                            </div>
                            </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="Label1" runat="server" Text='PromotionPrice' CssClass="form-control-res-middle"></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtPromotionPrice" runat="server" CssClass="form-control-res-right" onFocus ="this.select()" >0.00</asp:TextBox>
                            </div>
                             </div>
                       <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="Label2" runat="server" Text='Eligibile Qty'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtMaximumLimit" runat="server" CssClass="form-control-res-right" onFocus ="this.select()" >0</asp:TextBox>
                            </div>
                           </div>
        <div class="display_table">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                    OnClientClick="fncQItemAddValidation();return false;"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                    OnClientClick="fncQItemClear();return false;" ></asp:LinkButton>
                            </div>
                        </div>
                        </div>
                    </div>
    <div class="Payment_fixed_headers qualifyingItem">
                        <table id="tblQualifyingItem" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Delete
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">BatchNo
                                    </th>
                                    <th scope="col">Eligibile Qty
                                    </th>
                                    <th scope="col">PromtionPrice
                                    </th>
                                    <th scope="col">SellingPrice
                                    </th>
                                    <th scope="col">CustomerType
                                    </th>
                                    <th scope="col">Valid From
                                    </th>
                                    <th scope="col">Valid To
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
    
     <div class="FreeItemSave">
                <div class="control-button">
                    <asp:UpdatePanel ID="upSave" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lnkSave %>'
                                OnClientClick="return fncSaveCustomerWisePromotion();" OnClick="lnkSave_Click" ></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="control-button">
                    <asp:UpdatePanel ID="close" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red"  Text='Close' OnClick="lnkClose_Click" ></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

    <asp:HiddenField ID="Hidinv" runat="server" ClientIDMode="Static" Value="NO" />
      <asp:HiddenField ID="hidBatch" runat="server" ClientIDMode="Static" Value="NO" />
    <asp:HiddenField ID="hidCustomerWisePromotion" runat="server" ClientIDMode="Static" Value="NO" />
        <div class="hiddencol">
        <div id="stkadj_batch">
            <div class="Payment_fixed_headers BatchDetail">
                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th id="thBatchNo" scope="col">BatchNo
                            </th>
                            <%--<th scope="col">Stock
                            </th>--%>
                            <%--<th scope="col">Cost
                            </th>
                            <th scope="col">MRP
                            </th>--%>
                            <th scope="col">SellingPrice
                            </th>
                            <th scope="col">InventoryCode
                            </th>
                            <th scope="col">Description
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
