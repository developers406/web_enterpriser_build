﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMixMatchEntry.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmMixMatchEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(1) {
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 152px;
            max-width: 152px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 505px;
            max-width: 505px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(4) {
            text-align: right !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 192px;
            max-width: 192px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
        }

        .grdLoad td:nth-child(6) {
            text-align: right !important;
        }

        .right_align {
            text-align: right !important;
            padding-right: 5px !important;
        }

        .btch_left {
            margin-left: 20px;
        }

        .top {
            margin-top: 5px;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }

        .ui-autocomplete {
            max-height: 200px !important;
            overflow-y: auto;
            overflow-x: hidden;
            padding-right: 20px;
        }
    </style>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;

        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'MixMatchEntry');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "MixMatchEntry";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        function fncdatepicker() {
            try {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
               // $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "1");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var lastRowNo = 0;
        function pageLoad() {
            if ($('#<%=hivgroupcode.ClientID%>').val() != "") {
                fncgetItempromotiondetails();
            }
            fncdatepicker();
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $("select").chosen({ width: '100%' });
            $('#<%=txtInventory.ClientID %>').focus();
            fncInitializeBatchDetail();

            $('#<%=txtQty.ClientID %>').number(true, 2);
            $('#<%=txtBuyQty.ClientID %>').number(true, 2);
            $('#<%=txtPrice.ClientID %>').number(true, 2);
            SetAutoCompleteForItemSearch();
            $(function () {
                $("[id*=gvBatchDetails] td").click(function () {
                    DisplayBatch($(this).closest("tr"));
                });
                return false;
            });

            function DisplayBatch(row) {
                try {
                    if ($.trim($("td", row).eq(0).text()) != "")
                        $("[id*=txtBatchNo]").val($("td", row).eq(0).text());
                }
                catch (err) {
                    ShowPopupMessageBox(err.Message);
                    console.log(err);
                }
            }
        }

        function ValidateForm() {
            status = true;
            if (document.getElementById("<%=txtInventory.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtInventory.ClientID %>');
                ShowPopupMessageBoxandOpentoObject("Please Enter Inventorycode");
                return false;
            }
            if (document.getElementById("<%=txtDescription.ClientID%>").value == "") {
                popUpObjectForSetFocusandOpen = $('#<%=txtInventory.ClientID %>');
                ShowPopupMessageBoxandOpentoObject("Enter Valid item");
                return false;
            }
            else if (document.getElementById("<%=txtQty.ClientID%>").value == "" || document.getElementById("<%=txtQty.ClientID%>").value == "0.00") {
                popUpObjectForSetFocusandOpen = $('#<%=txtQty.ClientID %>');
                ShowPopupMessageBoxandOpentoObject("Please Enter Qty");
                return false;
            }

            else {

                if ($("#tblItemhistory tbody").length > 0) {
                    $("#tblItemhistory tbody").children().each(function () {
                        var itemCode = $(this).find('td[id*=tdItemcode]').text();
                        var batchNo = $(this).find('td[id*=tdBatch]').text();
                        if (itemCode == $('#<%=txtInventory.ClientID %>').val() && batchNo == $('#<%=txtBatchNo.ClientID %>').val()) {
                            $(this).find('td[id*=tdQty]').text(parseFloat(parseFloat($(this).find('td[id*=tdQty]').text()) + parseFloat($('#<%=txtQty.ClientID %>').val())).toFixed(2));
                           // $('#<%=txtBuyQty.ClientID %>').val(parseFloat(parseFloat($('#<%=txtBuyQty.ClientID %>').val()) + parseFloat($('#<%=txtQty.ClientID %>').val())).toFixed(2));
                            status = false;
                        }
                    });
                }
                if (status == "false") {
                    fncClear();
                    return false;
                }
                else {
                    fncgetItempromotiondetailscheck();
                    // fncTblBind();

                }
                return false;
            }
        }

    </script>
    <script type="text/javascript">
        function ValidateFormUpdate() {
            var Show = '';

            if (document.getElementById("<%=txtGroupCode.ClientID%>").value == "") {
                Show = Show + 'Group Code is Empty'
            }
            if (document.getElementById("<%=txtMixDescription.ClientID%>").value == "") {
                Show = Show + 'Please Enter Description'
            }

            if (document.getElementById("<%=txtPrice.ClientID%>").value == "" || parseFloat(document.getElementById("<%=txtPrice.ClientID%>").value) <= 0) {
                Show = Show + '<br />Please Enter Price'
            }
            if (document.getElementById("tblItemhistory").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length <= 0) {
                Show = Show + '<br />Please Enter MixMatch Detail'
            }
            if (document.getElementById("<%=txtBuyQty.ClientID%>").value == "" || parseFloat(document.getElementById("<%=txtBuyQty.ClientID%>").value) <= 0) {
                Show = Show + '<br />Please Enter Buy Qty'
            }

            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                fncSave();
                return true;//__doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
            }
        }

        function fncClear() {
            $('#<%=txtInventory.ClientID %>').val('');
            $('#<%=txtDescription.ClientID %>').val('');
            $('#<%=txtBatchNo.ClientID %>').val('');
            $('#<%=txtQty.ClientID %>').val('1');
            $('#<%=txtSPrice.ClientID %>').val('');
            $('#<%=txtInventory.ClientID %>').removeAttr("disabled");
            $('#<%=txtInventory.ClientID %>').focus();
        }
        function fncgetItempromotiondetailscheck() {//sankar
            status = true;
            var itemCode = $('#<%=txtInventory.ClientID %>').val();//$('#<%=txtInventory.ClientID %>').val();
            var batchNo = $('#<%=txtBatchNo.ClientID %>').val()
            $.ajax({
                url: '<%=ResolveUrl("~/Merchandising/frmMixMatchEntry.aspx/fncGetItemPromotion")%>',
                data: "{ 'groupcode': '" + "" + "','invcode': '" + itemCode + "','batch': '" + batchNo + "','mode': '" + "CHECK" + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    //console.log(response)
                    if (response.d != "[]") {
                        popUpObjectForSetFocusandOpen = $('#<%=txtInventory.ClientID %>');
                        ShowPopupMessageBoxandOpentoObject("This Item Allready In Another Promotion");
                        fncClear();
                        return false;
                    }
                    else {
                        fncTblBind();
                    }

                },
                error: function (response) {
                    console.log(response.message);
                },
                failure: function (response) {
                    console.log(response.message);
                }
            });
        }

        function fncgetItempromotiondetails() {//sankar
            var checked_radio = $("[id*=rdoDateFilter] input:checked");
            var value = checked_radio.val();

            var groupcode = $('#<%=hivgroupcode.ClientID%>').val();

            $.ajax({
                url: '<%=ResolveUrl("~/Merchandising/frmMixMatchEntry.aspx/fncGetItemPromotion")%>',
                data: "{ 'groupcode': '" + groupcode + "','invcode': '" + "" + "','batch': '" + "" + "','mode': '" + "GET" + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    //console.log(response.fncGetSelectedReportResult);
                    var objData = jQuery.parseJSON(response.d);
                    // console.log(objData);
                    fncBindtbl(objData);

                },
                error: function (response) {
                    console.log(response.message);
                    //   $('#loading-indicator').hide();
                },
                failure: function (response) {
                    console.log(response.message);
                    //  $('#loading-indicator').hide();
                }
            });
        }
        function fncLoadItemWiseSalesReport(D2) {

        }

        function SetAutoCompleteForItemSearch() {
            $("[id$=txtInventory]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                focus: function (event, i) {
                    $('#<%=txtInventory.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtInventory.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    return false;
                },
                minLength: 1
            });
            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtInventory'
                });
            });
        }
        function fncGetInventoryCode() {
            try {
                var obj = {};
                obj.code = $('#<%=txtInventory.ClientID %>').val();
                obj.formName = 'StkAdj';
                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncAssignValuestoTextBox(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try {
                var objStk, row;
                var tblBatchBody = $("#tblBatch tbody");
                objStk = jQuery.parseJSON(msg.d);

                if (objStk.length > 0) {
                    $('#<%=txtInventory.ClientID %>').attr('disabled', 'disabled');
                    $('#<%=txtInventory.ClientID %>').val($.trim(objStk[0]["InventoryCode"]));
                    $('#<%=txtDescription.ClientID %>').val($.trim(objStk[0]["Description"]));
                    if ($.trim(objStk[0]["BatchNo"]) == "") {
                        $('#<%=txtSPrice.ClientID %>').val($.trim(objStk[0]["SellingPrice"]));
                        // $('#<%=txtQty.ClientID %>').focus();
                    }
                    else {
                        tblBatchBody.children().remove();
                        for (var i = 0; i < objStk.length; i++) {
                            row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                                $.trim(objStk[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                                $.trim(objStk[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" +
                                objStk[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                                objStk[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                                objStk[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                                objStk[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                                $.trim(objStk[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                                $.trim(objStk[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["AllowExpireDate"]) + "</td><td id='tdMRPDEL_ ' style ='display:none'" + i + "' >" +
                                (objStk[i]["MRPDEL"]) + "</td></tr>";
                            tblBatchBody.append(row);
                        }
                        $("#Bom_Bacth").dialog("open");


                        tblBatchBody.children().dblclick(fncBatchRowClick);

                        tblBatchBody[0].setAttribute("style", "cursor:pointer");

                        tblBatchBody.children().on('mouseover', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "Yellow");
                        });

                        tblBatchBody.children().on('mouseout', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "white");
                        });

                        tblBatchBody.children().keypress(function (e) {
                            fncBatchKeyDown($(this), e);
                        });
                        $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                        $("#tblBatch tbody > tr").first().focus();
                    }
                }
                else {
                    $('#<%=txtInventory.ClientID %>').val('');
                }
                 //   $('#<%=txtQty.ClientID %>').focus().select();

                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitializeBatchDetail() {
            try {
                $("#Bom_Bacth").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 250,
                    width: 633,
                    modal: false,
                    title: "Enterpriser Web",
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                            $('#<%=txtQty.ClientID %>').focus().select();
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox($.trim($(this).find('td[id*=tdBatchNo]').text()), $.trim($(this).find('td[id*=tdSellingPrice]').text()));
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchInventoryValuesToTextBox(Batch, Sprice) {
            $('#<%=txtBatchNo.ClientID %>').val(Batch);
            $('#<%=txtSPrice.ClientID %>').val(Sprice);
            $("#Bom_Bacth").dialog("close");
            //$('.ui-button').click();
            $('#<%=txtQty.ClientID %>').focus().select();
            return false;
        }
        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
                else if (e.keyCode == 115) {
                    $('#<%=lnkSave.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClearAll.ClientID %>').click();
                    e.preventDefault();
                }
            }
        };

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            $("[id$=txtInventory]").bind('change', function () {
                if (this.value.match(SpecialChar)) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtInventory.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(SpecialChar, '');
                }
            });
            $("[id$=txtMixDescription]").bind('change', function () {
                if (this.value.match(SpecialChar)) {
                    popUpObjectForSetFocusandOpen = $('#<%=txtMixDescription.ClientID %>');
                    ShowPopupMessageBoxandOpentoObject("Special Characters are not Allowed : " + this.value);
                    this.value = this.value.replace(SpecialChar, '');
                }
            });
        });

        function fncClearAll() {
            try {

                $("[id*=tblItemhistory] td").remove();
                $(':checkbox, :radio').prop('checked', false);
                $("input").prop('checked', false);
                $('#<%=txtMixDescription.ClientID %>').val('');
                $('#<%=txtPrice.ClientID %>').val('0');
                $('#<%=txtBuyQty.ClientID %>').val('0');
                $('#<%=txtSPrice.ClientID %>').val('0');
                fncClear();
                window.location.href = "../Merchandising/frmMixMatchEntry.aspx?";
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Location_Delete(source) {
           
            $("#dialog-confirm").dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    "YES": function () {
                        $(this).dialog("close");
                        fncClearAll();
                        },
                        "NO": function () {
                            $(this).dialog("close");
                            return false();
                        }
                    }
                });

        }
        var Qty = 0;
        function fncTblBind() {
            try {
                var tblItem = $("#tblItemhistory tbody");
                lastRowNo = parseFloat(lastRowNo) + 1;
                var row = "<tr><td>" +
                    "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                    $('#<%=txtInventory.ClientID %>').val() + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                    $('#<%=txtDescription.ClientID %>').val() + "</td><td id='tdQty_" + lastRowNo + "' >" +
                    parseFloat($('#<%=txtQty.ClientID %>').val()).toFixed(2) + "</td><td id='tdBatch_" + lastRowNo + "' >" +
                    $('#<%=txtBatchNo.ClientID %>').val() + "</td><td id='tdSPrice_" + lastRowNo + "' >" +
                    $('#<%=txtSPrice.ClientID %>').val() + "</td></tr>";
                tblItem.append(row);
                $('#tblItemhistory').css("display", "block");
                $('#tblItemhistory thead').css("display", "none");
                Qty = parseFloat(Qty) + parseFloat($('#<%=txtQty.ClientID %>').val());
              //  $('#<%=txtBuyQty.ClientID %>').val(Qty);
                fncClear();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBindtbl(Json) {
            var tblValue = "";
            try {
                tblValue = Json;//jQuery.parseJSON(Json);
                var tblItem = $("#tblItemhistory tbody");
                lastRowNo = parseFloat(lastRowNo) + 1;
                if (tblValue.length > 0) {

                    tblItem.children().remove();
                    for (var i = 0; i < tblValue.length; i++) {
                        var row = "<tr><td>" +
                            "<img alt='Delete' src='../images/No.png' onclick='fncDeleteItem(this);return false;' /></td><td id='tdItemcode_" + lastRowNo + "' >" +
                            tblValue[i]["Inventorycode"] + "</td><td id='tdDesc_" + lastRowNo + "' >" +
                            tblValue[i]["Descriptiondetail"] + "</td><td id='tdQty_" + lastRowNo + "' >" +
                            parseFloat(tblValue[i]["Qty"]).toFixed(2) + "</td><td id='tdBatch_" + lastRowNo + "' >" +
                            (tblValue[i]["BatchNo"]) + "</td><td id='tdSPrice_" + lastRowNo + "' >" +
                            parseFloat(tblValue[i]["SellingPrice"]).toFixed(2) + "</td></tr>";
                        tblItem.append(row);
                    }
                    $('#tblItemhistory').css("display", "block");
                    $('#tblItemhistory thead').css("display", "none");

                    $('#<%=txtGroupCode.ClientID %>').val(tblValue[0]["Groupcode"]);
                    $('#<%=txtMixDescription.ClientID %>').val(tblValue[0]["Descriptionheader"]);
                    $('#<%=txtBuyQty.ClientID %>').val(tblValue[0]["BuyQty"]);
                    $('#<%=txtPrice.ClientID %>').val(tblValue[0]["Price"]);
                    $('#<%=txtFromDate.ClientID %>').val(tblValue[0]["FromDate"]);
                    $('#<%=txtToDate.ClientID %>').val(tblValue[0]["Todate"]);
                    if ($.trim(tblValue[0]["Status"]) == "Y") {
                        $("#<%=chkDeactive.ClientID%>").attr("checked", true);
                        //$("#<%=chkDeactive.ClientID%>").attr("disabled", true);
                    }
                    fncClear();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDeleteItem(rowobj) {
            var Qty = 0;
            $(rowobj).parents("tr").remove();
            $("#tblItemhistory tbody").children().each(function () {
                obj = $(this);
                Qty = parseFloat(Qty) + parseFloat(obj.find('td[id*=tdQty]').text());
            });
          //  $('#<%=txtBuyQty.ClientID %>').val(Qty);
        }
        function fncSave() {
            try {
                var xml = '<NewDataSet>';
                $("#tblItemhistory tbody").children().each(function () {
                    obj = $(this);

                    if (parseFloat(obj.find('td[id*=tdQty]').text()) > 0) {

                        xml += "<MixDetail>";
                        xml += "<InventoryCode>" + obj.find('td[id*=tdItemcode]').text() + "</InventoryCode>";
                        xml += "<Description>" + obj.find('td[id*=tdDesc]').text() + "</Description>";
                        xml += "<BatchNo>" + obj.find('td[id*=tdBatch]').text() + "</BatchNo>";
                        xml += "<Qty>" + obj.find('td[id*=tdQty]').text() + "</Qty>";
                        xml += "<SPrice>" + obj.find('td[id*=tdSPrice]').text() + "</SPrice>";
                        xml += "</MixDetail>";
                    }
                });
                xml = xml + '</NewDataSet>'
                xml = escape(xml);
                $('#<%=hidXML.ClientID %>').val(xml);
                return xml;
                console.log(xml);
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
            }
            if (charCode == 13) {
                fncGetInventoryCode();
                return false;
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    fncGetInventoryCode();
                }
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncclearmsg()  {
            $("#dialog-clear-confirm").dialog({
                autoOpen: false,
                resizable: false,
                height: "auto",
                width: 250,
                modal: true,
                buttons: {
                    "Yes": function () {
                        $(this).dialog("close");
                        $("*[name$='grpPurchased']").removeAttr("disabled");
                        fncClearAllsa();
                    },
                    No: function () {
                        $(this).dialog("close");
                    }
                }
            });
        };

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="../Merchandising/frmMixMatchView.aspx">MixMatch View</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">MixMax Entry</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="top-container-pi-header">
                    MixMatch Header
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label4" runat="server" Text="Group Code"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtGroupCode" runat="server" CssClass="form-control-res">
                        </asp:TextBox>
                    </div>
                    <div class="col-lg-push-8"></div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtMixDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-8">
                    </div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-8">
                    </div>
                </div>
                <div class="col-md-12 top">
                    <div class="col-md-1">
                        <asp:Label ID="Label1" runat="server" Text="Buy Qty"></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtBuyQty" runat="server" CssClass="form-control-res">0.00</asp:TextBox>
                    </div>
                    <div class="col-md-1">
                        <asp:Label ID="Label3" runat="server" Text="Price"></asp:Label>
                    </div>
                    <div class="col-md-1">
                        <asp:TextBox ID="txtPrice" Style="text-align: right;" Text="0" onFocus="this.select();" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div class="col-md-8">
                        <asp:CheckBox ID="chkDeactive" runat="server" Class="radioboxlist" Text="Activate"></asp:CheckBox>
                    </div>
                </div>

                <div class="right-container-top-detail">
                    <div class="GridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30 top">
                                <div class="grdLoad Payment_fixed_headers">
                                    <table id="tblItemhistoryHead" cellspacing="0" rules="all" border="1">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Code
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Qty
                                                </th>
                                                <th scope="col">BatchNo
                                                </th>
                                                <th scope="col">S.Price
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="grdLoad Payment_fixed_headers" style="height: 200px; width: 1300px !important; overflow-y: hidden; overflow-x: hidden;">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="display_none">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Code
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Qty
                                                </th>
                                                <th scope="col">BatchNo
                                                </th>
                                                <th scope="col">S.Price
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-container-pi">
                    <div class="bottom-container-pi-header">
                        MixMatch Detail
                    </div>
                    <div id="divItemAddSection" runat="server">
                        <div class="stockAdjustment_itemcode" style="width: 12%">
                            <div>
                                <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtInventory" runat="server" CssClass="form-control-res"
                                    onchange="return fncGetInventoryCode(); return false;" onkeydown="return fncShowSearchDialogInventory(event,'Inventory',  'txtInventory', 'txtQty');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_Description" style="width: 20%">
                            <div>
                                <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemDesc %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_itemcode">
                            <div>
                                <asp:Label ID="Label7" runat="server" Text="S.Price"></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtSPrice" runat="server" CssClass="form-control-res" Style="text-align: right;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_itemcode">
                            <div>
                                <asp:Label ID="lblBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="stockAdjustment_itemcode">
                            <div class="spilt" align="center">
                                <asp:Label ID="lblQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div class="spilt">
                                <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res-right">1</asp:TextBox>
                            </div>
                        </div>

                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;"
                                Text='<%$ Resources:LabelCaption,btnAdd %>' OnClientClick="return ValidateForm();"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Style="padding-left: 15px; padding-right: 15px;" Text='Clear'
                                OnClientClick="fncClear();return false;"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button">
                        </div>
                        <div class="stockAdjustment_save_button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Style="padding-left: 15px; padding-right: 15px;"
                                Text="Save" OnClientClick="return ValidateFormUpdate();" OnClick="lnkSave_Click"></asp:LinkButton>
                        </div>
                        <div class="stockAdjustment_save_button" style="margin-left: 25px">
                            <asp:LinkButton ID="lnkClearAll" runat="server" class="button-red"
                               OnClientClick="Location_Delete(this);return false;" Style="padding-left: 15px; padding-right: 15px;" Text='Clear'></asp:LinkButton>
                        </div>                         
                    </div>
                     <div id="dialog-confirm" style="display: none;" title="Enterpriser Web">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; display: none; margin: 12px 12px 20px 0;"></span> Are you sure to clear this item?</p>
                    </div>
                    <div class="hiddencol">
                        <div id="Bom_Bacth">
                            <div class="Payment_fixed_headers BatchDetail">
                                <table id="tblBatch" cellspacing="0" rules="all" border="1">
                                    <thead>
                                        <tr>
                                            <th scope="col">S.No
                                            </th>
                                            <th id="thBatchNo" scope="col">BatchNo
                                            </th>
                                            <th scope="col">Stock
                                            </th>
                                            <th scope="col">Cost
                                            </th>
                                            <th scope="col">MRP
                                            </th>
                                            <th scope="col">SellingPrice
                                            </th>
                                            <th scope="col">InventoryCode
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <asp:HiddenField ID="hidXML" runat="server" Value="2" />
                        <div class="display_none">
                            <button id="closeButton">Close Partial</button>
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hivgroupcode" runat="server" />
</asp:Content>
