﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmQuickPriceChange.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmQuickPriceChange" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .BatchDetail td:nth-child(7), .BatchDetail th:nth-child(7) {
            display: none;
        }

        .BatchDetail td:nth-child(8), .BatchDetail th:nth-child(8) {
            display: none;
        }
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .grdQuickPriceChange td:nth-child(1), .grdQuickPriceChange th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdQuickPriceChange td:nth-child(2), .grdQuickPriceChange th:nth-child(2) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdQuickPriceChange td:nth-child(3), .grdQuickPriceChange th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdQuickPriceChange td:nth-child(4), .grdQuickPriceChange th:nth-child(4) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdQuickPriceChange td:nth-child(5), .grdQuickPriceChange th:nth-child(5) {
            min-width: 370px;
            max-width: 370px;
        }

        .grdQuickPriceChange td:nth-child(6), .grdQuickPriceChange th:nth-child(6) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdQuickPriceChange td:nth-child(7), .grdQuickPriceChange th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdQuickPriceChange td:nth-child(8), .grdQuickPriceChange th:nth-child(8) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdQuickPriceChange td:nth-child(9), .grdQuickPriceChange th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdQuickPriceChange td:nth-child(10), .grdQuickPriceChange th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(11), .grdQuickPriceChange th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(12), .grdQuickPriceChange th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(13), .grdQuickPriceChange th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(14), .grdQuickPriceChange th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(15), .grdQuickPriceChange th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdQuickPriceChange td:nth-child(16), .grdQuickPriceChange th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdRepeak td:nth-child(1), .grdRepeak th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdRepeak td:nth-child(2), .grdRepeak th:nth-child(2) {
            min-width: 65px;
            max-width: 65px;
        }

        .grdRepeak td:nth-child(3), .grdRepeak th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(4), .grdRepeak th:nth-child(4) {
            min-width: 310px;
            max-width: 310px;
        }

        .grdRepeak td:nth-child(5), .grdRepeak th:nth-child(5) {
            min-width: 50px;
            max-width: 50px;
        }

        .grdRepeak td:nth-child(6), .grdRepeak th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(7), .grdRepeak th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(8), .grdRepeak th:nth-child(8) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(9), .grdRepeak th:nth-child(9) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdRepeak td:nth-child(10), .grdRepeak th:nth-child(10) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(11), .grdRepeak th:nth-child(11) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(12), .grdRepeak th:nth-child(12) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(13), .grdRepeak th:nth-child(13) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(14), .grdRepeak th:nth-child(14) {
            min-width: 75px;
            max-width: 75px;
        }

        .grdRepeak td:nth-child(15), .grdRepeak th:nth-child(15) {
            min-width: 95px;
            max-width: 95px;
            padding-right: 20px;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 220px;
            max-width: 220px;
        }


        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 105px;
            max-width: 105px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 105px;
            max-width: 105px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 110px;
            max-width: 110px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 90px;
            max-width: 90px;
        }

        .grdLoad th {
            padding-top: 4px;
            padding-bottom: 4px;
            text-align: left !important;
            background-color: indigo;
            font-weight: bolder;
            color: white;
        }

        .grdLoad td:nth-child(3) {
            text-align: right;
        }

        .radioclass {
            font-weight: 100;
        }

        .textboxPromotion {
            width: 80% !important;
        }

        .clockpicker {
            text-align: center;
        }

        #tblFilter tr:hover {
            background-color: pink;
        }

        #tblFilter {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            /*width: 100%;*/
            height: auto;
        }

            #tblFilter td, #tblStyleCode th {
                border: 1px solid #ddd;
                padding: 4px;
            }

            #tblFilter tr:nth-child(even) {
                background-color: #f2f2f2;
                min-width: 20px;
            }

        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite; /* Safari */
            animation: spin 2s linear infinite;
        }

        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .wrapper {
            position: relative;
        }

        .loading:before {
            content: '';
            display: block;
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 100;
            background: rgba(0,0,0,0.3) center center no-repeat url('data:image/gif;base64,R0lGODlhFAAUAKEAAO7u7lpaWgAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQBCgACACwAAAAAFAAUAAACQZRvoIDtu1wLQUAlqKTVxqwhXIiBnDg6Y4eyx4lKW5XK7wrLeK3vbq8J2W4T4e1nMhpWrZCTt3xKZ8kgsggdJmUFACH5BAEKAAIALAcAAAALAAcAAAIUVB6ii7jajgCAuUmtovxtXnmdUAAAIfkEAQoAAgAsDQACAAcACwAAAhRUIpmHy/3gUVQAQO9NetuugCFWAAAh+QQBCgACACwNAAcABwALAAACE5QVcZjKbVo6ck2AF95m5/6BSwEAIfkEAQoAAgAsBwANAAsABwAAAhOUH3kr6QaAcSrGWe1VQl+mMUIBACH5BAEKAAIALAIADQALAAcAAAIUlICmh7ncTAgqijkruDiv7n2YUAAAIfkEAQoAAgAsAAAHAAcACwAAAhQUIGmHyedehIoqFXLKfPOAaZdWAAAh+QQFCgACACwAAAIABwALAA');
        }
    </style>

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'QuickPriceChange');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "QuickPriceChange";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        var itmcode;
        var vendor;
        var dept;
        var cat;
        var sub;
        var brand;
        var shelf;
        var floor;
        var merchendise;
        var manufacture;
        var secton;
        var bin;
        var barcode;
        var batchno;
        var grnno;
        var row;
        var Location;
        var selectedValue;
        var Name;
        var sellinggreaterthenMRP = "false";
        var txtSellingPrice_Regular = $('#<%=txtSellingPrice_Regular.ClientID %>');
        var txtMRP_Regular = $('#<%=txtMRP_Regular.ClientID %>');
        var txtCost_Regular = $('#<%=txtCost_Regular.ClientID %>');
        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupQuickPriceChange').dialog('close');
                window.parent.$('#popupQuickPriceChange').remove();
            }
        });

        $(document).ready(function () {
            $("select").chosen({ width: '100%' });
            fncSetNumericVal();
            if ($('#<%=hidTab.ClientID %>').val() == "Y") {
                $("#liRepackBatch").show();
                $("#divPriceChangeAllBatch").show();
            }
            else {
                $("#liRepackBatch").hide();
                $("#divPriceChangeAllBatch").hide();
            }
        });
       <%-- function Hide() {
            if ($('#<%=hdBcastvalue.ClientID %>').val() != "Y") {
                //$("#divGidNo").hide();
                $('#ContentPlaceHolder1_divGidNo').hide();
                //$("[id*=divGidNo]").show();
            }
        }--%>
        function pageLoad() {
            try {
                 <%--if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                     $('#<%=lnkSave.ClientID %>').css("display", "block"); 
                     $('#<%=lnkUpdate_Regular.ClientID %>').css("display", "block");
                     $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                    $('#<%=lnkUpdate_Regular.ClientID %>').css("display", "none");
                    $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                }--%>
                $("select").chosen({ width: '100%' });
                fncSetNumericVal();
                SetQualifyingItemAutoComplete();
                if ($('#<%=hidTab.ClientID %>').val() == "Y") {
                    $("#liRepackBatch").show();
                    $("#divPriceChangeAllBatch").show();
                }
                else {
                    $("#liRepackBatch").hide();
                    $("#divPriceChangeAllBatch").hide();
                }
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }
        function fnchideforRepack() {
            try {
                $('#liRepack').addClass('active');
                $('#Repack').addClass('active');
                $('#liRegular').removeClass('active');
                $('#Regular').removeClass('active');
                $("[id*=TabName]").val("Repack");
                $('.breadcrumbs').hide();

                $("[id*=Mode]").val("PopUp");
                fnctabhide();
                fncHideMasterMenu();
                $('#<%=txtNewSellingPrice.ClientID%>').focus();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fnctabhide() {
            $('#tablist').hide();
        }
        function Check_all_Regular() {
            try {

                if ((parseFloat($('#<%=txtMRP_Regular.ClientID %>').val())) < (parseFloat($('#<%=txtSellingPrice_Regular.ClientID %>').val()))) {
                    fncConfirm("Mrp should be greater than selling price", $('#<%=txtMRP_Regular.ClientID %>'));
                }
                else if ((parseFloat($('#<%=txtSellingPrice_Regular.ClientID %>').val())) < (parseFloat($('#<%=txtCost_Regular.ClientID %>').val()))) {
                    fncConfirm("selling Price should be greater than Cost price", $('#<%=txtMRP_Regular.ClientID %>'));
                }
                else {
                    //  $('#<%=cbSelectRHdr.ClientID %>').prop('checked', true);
                    $("#tblQuickPriceChange_Regular [id*=gaRepackRow_Regular]").each(function () {
                        rowObj = $(this);
                        // rowObj.find('td input[id*="chkSelect_Regular"]').prop('checked', true);
                        rowObj.find('td input[id*="lblMRP_Regular"]').val((parseFloat($('#<%=txtMRP_Regular.ClientID %>').val())).toFixed(2));
                        rowObj.find('td input[id*="lblSUGPrice_Regular"]').val((parseFloat($('#<%=txtSellingPrice_Regular.ClientID %>').val())).toFixed(2));

                        rowObj.find('td input[id*="txtRWprice1"]').val((parseFloat($('#<%=txtRWprice1.ClientID %>').val())).toFixed(2));
                        rowObj.find('td input[id*="txtRWprice2"]').val((parseFloat($('#<%=txtRWprice2.ClientID %>').val())).toFixed(2));
                        rowObj.find('td input[id*="txtRWprice3"]').val((parseFloat($('#<%=txtRWprice3.ClientID %>').val())).toFixed(2));

                        //rowObj.find('td input[id*="lblMRP_Regular"]').number(true, 2);
                        //rowObj.find('td input[id*="lblSUGPrice_Regular"]').number(true, 2);
                        //rowObj.find('td input[id*="txtRWprice1"]').number(true, 2);
                        //rowObj.find('td input[id*="txtRWprice2"]').number(true, 2);
                        //rowObj.find('td input[id*="txtRWprice3"]').number(true, 2);
                    });
                }



            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSetNumericValForRegular() {
            var rowObj;
            try {
                $("#tblQuickPriceChange_Regular [id*=gaRepackRow_Regular]").each(function () {
                    rowObj = $(this);
                    rowObj.find('td input[id*="lblMRP_Regular"]').number(true, 2);
                    rowObj.find('td input[id*="lblSUGPrice_Regular"]').number(true, 2);
                    rowObj.find('td input[id*="txtRWprice1"]').number(true, 2);
                    rowObj.find('td input[id*="txtRWprice2"]').number(true, 2);
                    rowObj.find('td input[id*="txtRWprice3"]').number(true, 2);
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncRepackingValue() {
            var txtNewSellingPrice = $('#<%=txtNewSellingPrice.ClientID %>').val();
            var txtNetCost = $('#<%=txtNetCost.ClientID %>').val();
            var txtNewCost = $('#<%=txtNewCost.ClientID %>').val();
            var txtNewMRP = $('#<%=txtNewMRP.ClientID %>').val();
            if (txtNewSellingPrice == "") {
                fncConfirm("Please Enter New Selling Price", $('#<%=txtNewSellingPrice.ClientID %>'));
                return
            }
            if ($("[id*=hdRepackCostParam]").val() == "Y") {
                if (txtNetCost == "") { // dinesh
                    fncConfirm("Please Enter NetCost", $('#<%=txtNetCost.ClientID %>'));
                    return
                }
                if (txtNewCost == "") {
                    fncConfirm("Please Enter New Cost", $('#<%=txtNewCost.ClientID %>'));
                    return
                }
                if (txtNewMRP == "" && $('#<%=hidBatchNo.ClientID %>').val() == "") {
                    fncConfirm("Please Enter New MRP", $('#<%=txtNewMRP.ClientID %>'));
                    return
                }
                else {
                    //txtNewMRP = $('#<%=txtMrp.ClientID %>').val()
                }
                if (parseFloat(txtNewSellingPrice) < parseFloat(txtNewCost)) {
                    fncConfirm("Selling price must not be lesser than Cost", $('#<%=txtNewSellingPrice.ClientID %>'));
                    return
                }
                if (txtNewMRP != "" && parseFloat(txtNewMRP) < parseFloat(txtNewCost)) {
                    fncConfirm("Mrp must not be lesser than Cost", $('#<%=txtNewMRP.ClientID %>'));
                    return
                }
            } //
            var txtWprice1 = $('#<%=txtWPrice1.ClientID %>').val();
            var txtWprice2 = $('#<%=txtWPrice2.ClientID %>').val();
            var txtWprice3 = $('#<%=txtWPrice3.ClientID %>').val();

            var txtgrdNewSellingPrice = 0, txtgrdWprice1 = 0, txtgrdWprice2 = 0, txtgrdWprice3 = 0, txtgrdOldMRPPrice = 0
            txtgrdNetCost = 0, txtgrdNewCost = 0, txtgrdNewMRP = 0;

            try {
                $("#tblQuickPriceChange [id*=gaRepackRow]").each(function () {
                    rowObj = $(this);
                    txtgrdNewSellingPrice = parseFloat(txtNewSellingPrice) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    if (parseFloat(txtgrdNewSellingPrice) <= 0) {
                        //rowObj.remove();
                        ShowPopupMessageBox("Weight is cannot be zero");
                        return false;
                    }
                    txtgrdOldMRPPrice = parseFloat(rowObj.find('td  span[id*="lblOldMRP"]').text());
                    txtgrdWprice1 = parseFloat(txtWprice1) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    txtgrdWprice2 = parseFloat(txtWprice2) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    txtgrdWprice3 = parseFloat(txtWprice3) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    rowObj.find('td input[id*="txtNewSelling"]').number(true, 2);
                    rowObj.find('td input[id*="txtNewSelling"]').val(txtgrdNewSellingPrice);
                    //PriceCheckandset_Repacking(rowObj, txtgrdNewSellingPrice, txtgrdOldMRPPrice);
                    rowObj.find('td input[id*="txtWPrice1"]').number(true, 2);
                    rowObj.find('td input[id*="txtWPrice1"]').val(txtgrdWprice1);
                    rowObj.find('td input[id*="txtWPrice2"]').number(true, 2);
                    rowObj.find('td input[id*="txtWPrice2"]').val(txtgrdWprice2);
                    rowObj.find('td input[id*="txtWPrice3"]').number(true, 2);
                    rowObj.find('td input[id*="txtWPrice3"]').val(txtgrdWprice3);

                    txtgrdNetCost = parseFloat(txtNetCost) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    txtgrdNewCost = parseFloat(txtNewCost) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    txtgrdNewMRP = parseFloat(txtNewMRP) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    //alert(txtgrdNetCost, txtgrdNewCost, txtgrdNewMRP);

                    rowObj.find('td input[id*="txtgridNetCost"]').number(true, 2); // dinesh
                    rowObj.find('td input[id*="txtgridNetCost"]').val(txtgrdNetCost);
                    rowObj.find('td input[id*="txtgridNewCost"]').number(true, 2);
                    rowObj.find('td input[id*="txtgridNewCost"]').val(txtgrdNewCost);
                    rowObj.find('td input[id*="txtgridNewMRP"]').number(true, 2);
                    rowObj.find('td input[id*="txtgridNewMRP"]').val(txtgrdNewMRP);
                    NewCostMrp_Repacking(rowObj, txtgrdNetCost, txtgrdNewCost, txtgrdNewMRP, txtgrdNewSellingPrice);

                    if (txtgrdNewSellingPrice > txtgrdNewMRP) {
                        sellinggreaterthenMRP = "true";
                    }

                    //sellinggreaterthenMRP

                });

                if (sellinggreaterthenMRP == "true") {
                    $('#Mgs').show().css("background-color", "#ff4c4c").html("Selling Price must be less than MRP");
                }
                else {
                    $('#Mgs').hide();
                }



            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncConfirm(Mgs, object) {
            $(object).select();
            fncToastError(Mgs);
        }


        //Focus Set to Next Row
        function fncSetFocustoNextRow(evt, source, curcell) {
            var verHight;
            try {
                var rowobj = $(source).parent().parent();
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13 || charCode == 40) {
                    var NextRowobj = rowobj.next();
                    //alert('td input[id*="' + curcell + '"]');
                    fncSetFocustoObject(NextRowobj.find('td input[id*="' + curcell + '"]'));
                    //return false;
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    fncSetFocustoObject(prevrowobj.find('td input[id*="' + curcell + '"]'));
                    //return false;
                }

                if (charCode == 13) {
                    verHight = $("#tblQuickPriceChange_Regular").scrollTop();
                    $("#tblQuickPriceChange_Regular").scrollTop(verHight + 10);
                    return false;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncRegularCheckChanged(source) {
            try {
                $("#tblQuickPriceChange_Regular tbody tr").find('td input[id*="chkSelect_Regular"]').prop("checked", $(source).prop("checked"));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUpdateClick() {
            try {
                var staus = true;
                if ($("#tblQuickPriceChange [id*=gaRepackRow]").children().length > 0) {
                    $("#tblQuickPriceChange [id*=gaRepackRow]").each(function () {
                        var rowObj = $(this);
                        var txtgrdNewSellingPrice = parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                        if (parseFloat(txtgrdNewSellingPrice) <= 0) {
                            //rowObj.remove();
                            staus = false;
                            ShowPopupMessageBox("Weight is cannot be zero");
                            return false;
                        }
                    });
                    if (staus == true) {
                        $('#<%=hidBatchNo.ClientID %>').val('');
                        $('#<%=btnUpdate.ClientID %>').click();

                    }
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncRegularUpdateClick() {
            try {
                $('#<%=btnRegularUpdate.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function ClearForm() {
            //$(':checkbox, :radio').prop('checked', false);
            //$("input").prop('checked', false);
            $("#ContentPlaceHolder1_searchFilterUserControl_txtBarcode").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtItemCode").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtItemName").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtVendor").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtVendor").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_txtDept").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlDept").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlCategory").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlCategory").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlBrand").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlBrand").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlSubCategory").val("-- Select --");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlSubCategory").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlMerchandise").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlMerchandise").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlManufacture").val("-- Select --");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlManufacture").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlFloor").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlFloor").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlSection").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlSection").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlBin").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlBin").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlShelf").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlShelf").trigger("liszt:updated");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlLocation").val("");
            $("#ContentPlaceHolder1_searchFilterUserControl_ddlLocation").trigger("liszt:updated");
            $("#ContentPlaceHolder1_txtGidNo").val("");
            $("#ContentPlaceHolder1_txtGidNo").trigger("liszt:updated");
            return false;
        }
        function fncFilterValidation() {
            $("#tblFilter tbody tr").remove();
            itmcode = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val()
            barcode = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BarcodeTextBox).ClientID %>").val();
            vendor = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.VendorDropDown).ClientID %>").val();
            dept = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.DepartmentDropDown).ClientID %>").val();
            cat = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.CategoryDropDown).ClientID %>").val();
            sub = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.SubCategoryDropDown).ClientID %>").val();
            brand = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BrandDropDown).ClientID %>").val();
            floor = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.FloorDropDown).ClientID %>").val();
            shelf = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ShelfDropDown).ClientID %>").val();
            merchendise = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.MerchandiseDropDown).ClientID %>").val();
            manufacture = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ManufactureDropDown).ClientID %>").val();
            secton = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.SectionDropDown).ClientID %>").val();
            bin = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.BinDropDown).ClientID %>").val();
            Location = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.LocationDropDown).ClientID %>").val();
            Name = $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val();
            GidNo = $("#<%=txtGidNo.ClientID %>").val();


            try {
                if ((itmcode == "") && (barcode == "") && (vendor == "") && (dept == "") && (cat == "")
                    && (sub == "") && (brand == "") && (floor == "") && (shelf == "") && (merchendise == "") && (manufacture == "")
                    && (secton == "") && (bin == "") && (Location == "") && (Name == "") && (GidNo == "")) {
                    ShowPopupMessageBox("Please select Atleast one Filter");
                    return false;
                }
                else {
                    fncTableBind();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function SetQualifyingItemAutoComplete() {
            debugger;
            try {
                $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemCodeTextBox).ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });

                $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    open: function (event, ui) {
                        var $input = $(event.target);
                        var $results = $input.autocomplete("widget");
                        var scrollTop = $(window).scrollTop();
                        var top = $results.position().top;
                        var height = $results.outerHeight();
                        if (top + height > $(window).innerHeight() + scrollTop) {
                            newTop = top - height - $input.outerHeight();
                            if (newTop > scrollTop)
                                $results.css("top", newTop + "px");
                        }
                    },
                    focus: function (event, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= searchFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.SearchFilterUserControl.FilterControls.ItemNameTextBox).ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });
                $(window).scroll(function (event) {
                    $('.ui-autocomplete.ui-menu').position({
                        my: 'left bottom',
                        at: 'left top',
                        of: '#ContentPlaceHolder1_searchFilterUserControl_txtItemCode'
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function isNumberKey_SP(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            //if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode >= 96 && charCode <= 105)) {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }
        function isNumberKey_w1(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            //if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode >= 96 && charCode <= 105)) {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;
        }
        function isNumberKey_w2(event) {

            var charCode = (event.which) ? event.which : event.keyCode;
            //if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode >= 96 && charCode <= 105)) {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                event.preventDefault();
                return false;
            }
            return true;


        }
        function fncTableBind() {
            var obj = {};
            var vitmcode;
            var vvendor;
            var vdept;
            var vcat;
            var vsub;
            var vbrand;
            var vshelf;
            var vfloor;
            var vmerchendise;
            var vmanufacture;
            var vsecton;
            var vbin;
            var vbarcode;
            var vLocation
            var vName;
            var vGidNo;
            obj.vitmcode = itmcode; obj.vvendor = vendor; obj.vdept = dept; obj.vcat = cat; obj.vsub = sub; obj.vbrand = brand;
            obj.vshelf = shelf; obj.vfloor = floor; obj.vmerchendise = merchendise; obj.vmanufacture = manufacture; obj.vsecton = secton; obj.vbin = bin;
            obj.vbarcode = barcode, obj.vLocation = Location, obj.vName = Name, obj.vGidNo = GidNo;
            try {
                $.ajax({
                    url: "frmQuickPriceChange.aspx/fncTableValue",
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        fnctblbidvalues(data);
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.message);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.message);
                    }

                });
                return false;
            }
            catch (err) {
                ShowAlert(err.message);
            }

        }
        function fnctblbidvalues(msg) {
            try {
                if (msg.d == 'Fail') {
                    ShowPopupMessageBox('Please Select Valid Filter');
                    return false;
                }
                else {
                    var objdata = $.parseJSON(msg.d);
                    var tblFilter = $("#tblFilter tbody");
                    tblFilter.children().remove();
                    if (objdata.length > 0) {
                        if ($('#<%=hdBcastvalue.ClientID %>').val() != "Y") {
                            for (var i = 0; i < objdata.length; i++) {
                                var row = "<tr><td>" +
                                    objdata[i]["InventoryCode"] + "</td><td>" +
                                    objdata[i]["Description"] + "</td><td>" +
                                    objdata[i]["MRP"].toFixed(2) + "</td><td>" +
                                    objdata[i]["Basiccost"].toFixed(2) + "</td><td>" +
                                    // "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["MRP"].toFixed(2) + "' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return number(event,this);'  id='txtMRP_" + i + "'</td><td>" +
                                    // "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["Basiccost"].toFixed(2) + "' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return number(event,this);'  id='txtBasiccost_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["SellingPrice"].toFixed(2) + "' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return number(event,this);'  id='txtSPrice_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice1"].toFixed(2) + "' onkeypress='return isNumberKey_w1(event,this);' onkeydown ='return numberW1(event,this);' id='txtW1Price_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice2"].toFixed(2) + "' onkeypress='return isNumberKey_w2(event,this);' onkeydown ='return numberW2(event,this);' id='txtW2Price_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice3"].toFixed(2) + "' onkeypress='return isNumberKey(event,this);' onkeydown ='return numberW3(event,this);' id='txtW3Price_" + i + "'</td><td>" +
                                    "<input type='checkbox' style ='margin-left: 40px;' id='cbSelect_" + i + "'</td></tr>";
                                tblFilter.append(row);
                            }
                            $("#txtSPrice_0").focus().select();
                        }
                        else {
                            for (var i = 0; i < objdata.length; i++) {
                                var row = "<tr><td>" +
                                    objdata[i]["InventoryCode"] + "</td><td>" +
                                    objdata[i]["Description"] + "</td><td>" +
                                    //objdata[i]["MRP"].toFixed(2) + "</td><td>" +
                                    "<input type='textbox'  style ='width: 100%;text-align: right;' value ='" + objdata[i]["MRP"].toFixed(2) + "' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return numberMRP(event,this);'  id='txtMRP_" + i + "'</td><td>" +
                                    "<input type='textbox'  style ='width: 100%;text-align: right;' value ='" + objdata[i]["Basiccost"].toFixed(2) + "' onchange='return fncValitation(event,this);' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return numberBcost(event,this);' id='txtBasiccost_" + i + "'</td><td>" +
                                    "<input type='textbox'   style ='width: 100%;text-align: right;' value ='" + objdata[i]["SellingPrice"].toFixed(2) + "' onchange='return fncValitation(event,this);' onkeypress='return isNumberKey_SP(event,this);' onkeydown ='return number(event,this);'  id='txtSPrice_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice1"].toFixed(2) + "'  onchange='return fncValitation(event,this);' onkeypress='return isNumberKey_w1(event,this);' onkeydown ='return numberW1(event,this);' id='txtW1Price_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice2"].toFixed(2) + "'  onchange='return fncValitation(event,this);' onkeypress='return isNumberKey_w2(event,this);' onkeydown ='return numberW2(event,this);'  id='txtW2Price_" + i + "'</td><td>" +
                                    "<input type='textbox' style ='width: 100%;text-align: right;' value ='" + objdata[i]["WPrice3"].toFixed(2) + "'  onchange='return fncValitation(event,this);' onkeypress='return isNumberKey(event,this);' onkeydown ='return numberW3(event,this);' id='txtW3Price_" + i + "'</td><td>" +
                                    "<input type='checkbox' style ='margin-left: 40px;' id='cbSelect_" + i + "'</td></tr>";
                                tblFilter.append(row);
                            }
                            $("#txtMRP_0").focus().select();
                        }

                    }
                    return false;
                }

            }
            catch (err) {
                ShowAlert(err.msg);
            }
        }

        function fncSetNumericVal() {
            var rowObj;
            try {
                $("#tblFilter tbody tr").each(function () {
                    rowObj = $(this);
                    rowObj.find('td input[id*="txtSPrice"]').number(true, 2);
                    rowObj.find('td input[id*="txtW1Price"]').number(true, 2);
                    rowObj.find('td input[id*="txtW2Price"]').number(true, 2);
                    rowObj.find('td input[id*="txtW3Price"]').number(true, 2);
                });
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncValitation(eve, source) {
            var status = true;
            var tr = $(source).parent().parent();
            var MRP = tr.find('td input[id*="txtMRP"]').val();
            var Basiccost = tr.find('td input[id*="txtBasiccost"]').val();
            var SPrice = tr.find('td input[id*="txtSPrice"]').val();
            var W1Price = tr.find('td input[id*="txtW1Price"]').val();
            var txtW2Price = tr.find('td input[id*="txtW2Price"]').val();
            var txtW3Price = tr.find('td input[id*="txtW3Price"]').val();

            if (parseFloat(Basiccost) > parseFloat(MRP)) {
                fncToastInformation('MRP Should be Greater than BasicCost');
                tr.find(' td input[id*="txtBasiccost"]').focus().select();
                status = false;
            }
            if (parseFloat(SPrice) > parseFloat(MRP)) {
                fncToastInformation('MRP Must Greater than SPrice');
                tr.find('input[id*=txtSPrice]').focus().select();
                status = false;
            }

            if (parseFloat(W1Price) > parseFloat(MRP)) {
                fncToastInformation('MRP Must Greater than W1Price');
                tr.find('input[id*=txtW1Price]').focus().select();
                status = false;
            }

            if (parseFloat(txtW2Price) > parseFloat(MRP)) {
                fncToastInformation('MRP Must Greater than W2Price');
                tr.find('input[id*=txtW2Price]').focus().select();
                status = false;
            }

            if (parseFloat(txtW3Price) > parseFloat(MRP)) {
                fncToastInformation('MRP Must Greater than W3Price');
                tr.find('input[id*=txtW3Price]').focus().select();
                status = false;
            }
            return status;
        }
        function numberMRP(eve, source) {
            debugger;
            var keyCode, NextRowobj;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            //var MRP = tr.find('td input[id*="txtMRP"]').val();
            //var Basiccost = tr.find('td input[id*="txtBasiccost"]').val();
            //if (parseFloat(Basiccost) >= parseFloat(MRP)) {
            //    fncToastInformation('MRP Must Greater than BasicCost');
            //    return false;
            //}

            if (charCode == "40") {
                NextRowobj = tr.next();
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMRP"]'));
                tr.find('input[id*=cbSelect]').prop('checked', true);
                return false;
            }
            if (charCode == "37") {
                NextRowobj = tr;
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW3Price"]'));
                tr.find('input[id*=cbSelect]').prop('checked', false);
                return false;
            }
            if (charCode == "38") {
                NextRowobj = tr.prev();
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMRP"]'));
                tr.find('input[id*=cbSelect]').prop('checked', false);
                return false;
            }
            if (charCode == "39") {
                NextRowobj = tr;
                fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBasiccost"]'));
                return false;
            }

        }
        function numberBcost(eve, source) {
            var keyCode, NextRowobj;
            var status = false;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            status = fncValitation(eve, source);
            if (status) {
                if (charCode == "40") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBasiccost"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;

                }
                if (charCode == "37") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMRP"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;

                }
                if (charCode == "38") {

                    NextRowobj = tr.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBasiccost"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;

                }
                if (charCode == "39") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSPrice"]'));
                    return false;

                }
            }

        }
        function number(eve, source) {
            var keyCode, NextRowobj;
            var status = false;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            status = fncValitation(eve, source);
            if (status) {
                if (charCode == "40") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSPrice"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;
                }
                if (charCode == "37") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtBasiccost"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;
                }
                if (charCode == "38") {

                    NextRowobj = tr.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSPrice"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;
                }
                if (charCode == "39") {
                    NextRowobj = tr;

                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW1Price"]'));
                    return false;
                }
            }
        }
        function numberW1(eve, source) {
            var keyCode, NextRowobj;
            var status = false;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            status = fncValitation(eve, source);
            if (status) {
                if (charCode == "40") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW1Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;
                }
                if (charCode == "37") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtSPrice"]'));
                    return false;
                }
                if (charCode == "38") {

                    NextRowobj = tr.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW1Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;
                }
                if (charCode == "39") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW2Price"]'));
                    return false;
                }
            }
        }
        function numberW2(eve, source) {
            var keyCode, NextRowobj;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            var status = false;
            status = fncValitation(eve, source);
            if (status) {
                if (charCode == "40") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW2Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;
                }
                if (charCode == "37") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW1Price"]'));
                    return false;
                }
                if (charCode == "38") {

                    NextRowobj = tr.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW2Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;
                }
                if (charCode == "39") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW3Price"]'));
                    return false;
                }
            }
        }
        function numberW3(eve, source) {
            var keyCode, NextRowobj;
            var charCode = (eve.which) ? eve.which : eve.keyCode;
            var tr = $(source).parent().parent();
            var status = false;
            status = fncValitation(eve, source);
            if (status) {
                if (charCode == "40") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW3Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;
                }
                if (charCode == "37") {

                    NextRowobj = tr;
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW2Price"]'));
                    return false;
                }
                if (charCode == "38") {

                    NextRowobj = tr.prev();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtW3Price"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', false);
                    return false;
                }
                if (charCode == "39") {

                    NextRowobj = tr.next();
                    fncSetSelectAndFocus(NextRowobj.find('td input[id*="txtMRP"]'));
                    tr.find('input[id*=cbSelect]').prop('checked', true);
                    return false;
                }
            }
        }
        function fncSetSelectAndFocus(obj) {
            try {

                setTimeout(function () {
                    obj.focus().select();
                }, 10);

                fncRowClickSp($(obj).parent().parent());

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncRowClickSp(source) {
            try {

                var obj;
                obj = $(source);
                //$(obj).css("background-color", "#80b3ff");
                //$(obj).siblings().css("background-color", "white");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncSelectAllClick(source) {
            var totalqty;
            //  debugger;
            try {
                if ($(source).is(":checked")) {
                    $("#tblFilter tr").find('td input[id*="cbSelect"]').prop("checked", true);
                }
                else {
                    $("#tblFilter tbody tr").find('td input[id*="cbSelect"]').prop("checked", false);
                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function isNumberKey_SP(evt, source) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            var tr = $(source).parent().parent();
            if (charCode == 13) {
                tr.find('input[id*=txtW1Price]').focus().select();
                return false;
            }
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKey_w1(evt, source) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                var tr = $(source).parent().parent();
                tr.find('input[id*=txtW2Price]').focus().select();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKey_w2(evt, source) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                var tr = $(source).parent().parent();
                tr.find('input[id*=txtW3Price]').focus().select();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function isNumberKey(evt, source) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                var tr = $(source).parent().parent();
                tr.find('input[id*=cbSelect]').prop('checked', true);
                tr.next().find('input[id*=txtSPrice]').focus().select();
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        function fncSave() {
            try {
                var tbody = $("#tblFilter tbody");

                if (tbody.children().length == 0) {
                    ShowAlert("Please Select Atleast one Filter");
                    return false;
                }
                else {
                    var SNo = 0;
                    var rowJob = "<table>";
                    var sCheck = 0;
                    $("#tblFilter tbody").children().each(function () {
                        sCheck = parseFloat(sCheck) + 1;
                        if ($(this).find('td input[id*="cbSelect"]').is(":checked") == true) {
                            SNo = parseFloat(SNo) + 1;
                            rowJob = rowJob + "<QuicBatch SNO='" + SNo + "' ";
                            rowJob = rowJob + "Inventory='" + $(this).find('td').eq(0).html() + "' ";
                            rowJob = rowJob + "Description='" + $(this).find('td').eq(1).html() + "' ";
                            // rowJob = rowJob + "MRP='" + parseInt($(this).find('td').eq(2).html()) + "' ";
                            rowJob = rowJob + "MRP='" + $(this).find('td input[id*="txtMRP"]').val() + "' ";
                            rowJob = rowJob + "Basiccost='" + $(this).find('td input[id*="txtBasiccost"]').val() + "' ";
                            rowJob = rowJob + "SPrice='" + $(this).find('td input[id*="txtSPrice"]').val() + "' ";
                            rowJob = rowJob + "W1Price='" + $(this).find('td input[id*="txtW1Price"]').val() + "' ";
                            rowJob = rowJob + "W2Price='" + $(this).find('td input[id*="txtW2Price"]').val() + "' ";
                            rowJob = rowJob + "W3Price='" + $(this).find('td input[id*="txtW3Price"]').val() + "' ";
                            rowJob = rowJob + "></QuicBatch>";
                        }
                    });
                    rowJob = rowJob + "</table>";
                    console.log(rowJob);
                    fncXml(escape(rowJob), Location);
                }
                return false;
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }
        function fncXml(source, Location) {
            try {
                $.ajax({
                    url: "frmQuickPriceChange.aspx/fncSave",
                    data: "{ 'sCode': '" + source + "','hidLocation': '" + Location + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        fncSuccesss(data.d);
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }
        function fncSuccesss(msh) {
            if (msh == "True") {
                ClearForm();
                Clearall();
                ShowPopupMessageBox("Saved Successfully");
                return false;
            }
            if (msh == "Select") {
                ShowPopupMessageBox("Please Select Atleast one Row");
                return false;
            }
        }
        function Clearall() {
            try {
                $("#tblFilter tbody tr").remove();
                ClearForm();
                return false;
            }
            catch (err) {
                ShowAlert(err.message);
            }
        }

        //$(document).ready(function () {
        //    $(document).on('keydown', disableFunctionKeys);
        //});
        //function disableFunctionKeys(e) {
        //    var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
        //    if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

        //        if (e.keyCode == 112) {
        //            e.preventDefault();
        //        }
        //    }
        //};

        function fncShowSearchDialogInventory(event, Inventory, txtItemName, txtShortName) {
            var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (charCode == 112) {
                fncShowSearchDialogCommon(event, Inventory, txtItemName, txtShortName);
            }
        }

        //Vijay 20210524 -Batch Wise Repack Item
        function fncBatchWiseRepackItem() {
            try {
                fncGetInventoryCode();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncGetInventoryCode() {
            try {
                var obj = {};
                obj.code = $('#<%=txtInventory.ClientID %>').val();
                obj.formName = 'StkAdj';

                if (obj.code == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventoryDetail_BasedonBarcode")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        fncAssignValuestoTextBox(msg);

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }

                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncAssignValuestoTextBox(msg) {
            try { 
                var objStk, row;
                objStk = jQuery.parseJSON(msg.d);
                if (objStk.length > 0) {
                    tblBatchBody = $("#tblBatch tbody");
                    tblBatchBody.children().remove();
                    for (var i = 0; i < objStk.length; i++) {
                        row = "<tr id='BatchRow_" + i + "' tabindex='" + i + "' ><td id='tdRowNo_" + i + "' > " +
                            $.trim(objStk[i]["RowNo"]) + "</td><td id='tdBatchNo_" + i + "' >" +
                            $.trim(objStk[i]["BatchNo"]) + "</td><td id='tdStock_" + i + "' >" +
                            objStk[i]["Qty"].toFixed(2) + "</td><td id='tdCost_" + i + "' >" +
                            objStk[i]["Cost"].toFixed(2) + "</td><td id='tdMRPNew_" + i + "' >" +
                            objStk[i]["MRP"].toFixed(2) + "</td><td id='tdSellingPrice_" + i + "' >" +
                            objStk[i]["SellingPrice"].toFixed(2) + "</td><td id='tdInventory_" + i + "' >" +
                            $.trim(objStk[i]["InventoryCode"]) + "</td><td id='tdDesc_" + i + "' >" +
                            $.trim(objStk[i]["Description"]) + "</td><td id='tdWprice1_'  style ='display:none'" + i + "' >" +
                            (objStk[i]["WPrice1"]).toFixed(2) + "</td><td id='tdWprice2_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["WPrice2"]).toFixed(2) + "</td><td id='tdWprice3_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["WPrice3"]).toFixed(2) + "</td><td id='tdExpDate_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["ExpiredDate"]) + "</td><td id='tdExpMonth_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["ExpMonth"]) + "</td><td id='tdExpYear_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["ExpYear"]) + "</td><td id='tdAllowExpDate_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["AllowExpireDate"]) + "</td><td id='tdNetCost_ ' style ='display:none'" + i + "' >" +
                            (objStk[i]["AverageCost"]) + "</td></tr>";

                        tblBatchBody.append(row);

                        fncInitializeBatchDetail();
                        tblBatchBody.children().dblclick(fncBatchRowClick);

                        tblBatchBody[0].setAttribute("style", "cursor:pointer");

                        tblBatchBody.children().on('mouseover', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "Yellow");
                        });

                        tblBatchBody.children().on('mouseout', function (evt) {
                            var rowobj = $(this);
                            rowobj.css("background-color", "white");
                        });

                        tblBatchBody.children().on('keydown', function () {
                            fncBatchKeyDown($(this), event);
                        });

                        $("#tblBatch tbody > tr").first().css("background-color", "Yellow");
                        $("#tblBatch tbody > tr").first().focus();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchRowClick() {
            try {
                fncBatchInventoryValuesToTextBox(this, $.trim($(this).find('td[id*=tdBatchNo]').text()));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBatchInventoryValuesToTextBox(rowObj, BatchNo) { 
            var stock;
            try {
               <%-- $('#<%=txtInventory.ClientID %>').val($.trim($(rowObj).find('td[id*=tdInventory]').text()));--%>
                $('#<%=hidBatchNo.ClientID %>').val(BatchNo);
                 
                $("#stkadj_batch").dialog('close'); 

                __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        }

        function fncBatchKeyDown(rowobj, evt) {
            var rowobj, charCode;
            var scrollheight = 0;
            try {
                //var rowobj = $(this);
                var charCode = (evt.which) ? evt.which : evt.keyCode;

                if (charCode == 13) {
                    rowobj.dblclick();
                    return false;
                }
                else if (charCode == 40) {

                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.css("background-color", "Yellow");
                        NextRowobj.siblings().css("background-color", "white");
                        NextRowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseInt(NextRowobj.find('td[id*=tdRowNo]').text()) == 2) {
                                $("#tblBatch tbody").scrollTop(0);
                            }
                            else if (parseFloat(scrollheight) > 10) {
                                scrollheight = parseFloat(scrollheight) - 10;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);
                    }
                }
                else if (charCode == 38) {
                    var prevrowobj = rowobj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.css("background-color", "Yellow");
                        prevrowobj.siblings().css("background-color", "white");
                        prevrowobj.select().focus();

                        setTimeout(function () {
                            scrollheight = $("#tblBatch tbody").scrollTop();
                            if (parseFloat(scrollheight) > 3) {
                                scrollheight = parseFloat(scrollheight) + 1;
                                $("#tblBatch tbody").scrollTop(scrollheight);
                            }
                        }, 50);

                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        ///Initialize Batch Detail
        function fncInitializeBatchDetail() {
            try {
                $("#stkadj_batch").dialog({
                    resizable: false,
                    height: 'auto',
                    width: 633,
                    modal: true,
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close"); 
                        }
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncCLearRepack() {
            $('#<%=hidBatchNo.ClientID %>').val('');
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Quick Price Change</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <div id="Tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul id="tablist" class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liRegular"><a href="#Regular" aria-controls="Regular" role="tab" data-toggle="tab">Regular</a></li>
                        <li id="liRepack"><a href="#Repack" aria-controls="Repack" role="tab" data-toggle="tab">Repack</a></li>
                        <li id="liRepackBatch" runat="server"><a href="#Batch" aria-controls="Repack" role="tab" data-toggle="tab">PriceChange all Batchs</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                        <div class="tab-pane active" role="tabpanel" id="Regular">
                            <div id="divMainContainer_Regular">

                                <div class="container-group-small_new">
                                    <center>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-3">

                                            <div class="container-control">
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label10" runat="server" Text="Inventory Code"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtInvertoryCode_Regular" runat="server" MaxLength="20"
                                                            CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommonOpen(event, 'Regular');"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label11" runat="server" Text="MRP"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtMRP_Regular" Text="0" Style="text-align: right;" runat="server" MaxLength="18" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label12" runat="server" Text="Cost"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCost_Regular" Text="0" Style="text-align: right;" runat="server" MaxLength="18" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label18" runat="server" Text="Discount"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDiscount_Regular" Text="0" Style="text-align: right;" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-4">

                                            <div class="container-control">


                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label13" runat="server" Text="Description"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDescription_Regular" runat="server" MaxLength="400" CssClass="form-control-res"></asp:TextBox>

                                                    </div>

                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label14" runat="server" Text="Selling Price"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtSellingPrice_Regular" Text="0" Style="text-align: right;" runat="server" MaxLength="18" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label16" runat="server" Text="Packing"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtPacking_Regular" runat="server" MaxLength="50" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label15" runat="server" Text="WareHouse"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtWareHouse_Regular" runat="server" MaxLength="15" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left display_none" style="text-align: left">
                                                        <asp:Label ID="Label17" runat="server" Text="Batch"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2 display_none">
                                                        <asp:CheckBox ID="chkBatch_Regular" runat="server" />
                                                    </div>
                                                    <div class="col-md-5">
                                                        <%--<button type="button" class="button-blue" runat="server" id="lnkHQPrice_Regular" onClientclick=" Check_all_Regular();" >HQ Price</button>--%>
                                                        <asp:LinkButton ID="lnkHQPriceRegular" runat="server" CssClass="button-blue" OnClientClick="Check_all_Regular();return false;" Text="Apply HQ Price"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="col-md-3">
                                            <div class="container-control">
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="lblRWprice1" runat="server" Text="Wprice1"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtRWprice1" runat="server" MaxLength="20" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="lblRWprice2" runat="server" Text="Wprice2"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtRWprice2" runat="server" MaxLength="20" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left">
                                                        <asp:Label ID="lblRWprice3" runat="server" Text="Wprice3"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtRWprice3" runat="server" MaxLength="20" Text="0" CssClass="form-control-res-right"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divrepeater_Regular" class="Barcode_fixed_headers grdQuickPriceChange">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table id="tblQuickPriceChange_Regular" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">S.No
                                                                </th>
                                                                <th scope="col">
                                                                    <asp:CheckBox ID="cbSelectRHdr" runat="server" onclick="fncRegularCheckChanged(this);" />
                                                                </th>
                                                                <th scope="col">Location
                                                                </th>
                                                                <th scope="col">Item Code
                                                                </th>
                                                                <th scope="col">Description
                                                                </th>
                                                                <th scope="col">Qty   
                                                                </th>
                                                                <th scope="col">MRP
                                                                </th>
                                                                <th scope="col">SUG.Price</th>
                                                                <th id="thRWprice1" runat="server" scope="col">WPrice1</th>
                                                                <th id="thRWprice2" runat="server" scope="col">WPrice2</th>
                                                                <th id="thRWprice3" runat="server" scope="col">WPrice3</th>
                                                                <th scope="col">SUG.Date
                                                                </th>
                                                                <th scope="col">LOC.Price
                                                                </th>
                                                                <th scope="col">LOC.PriceDate
                                                                </th>
                                                                <th scope="col">Qty In Hand
                                                                </th>
                                                                <th scope="col">LastSell.Price 
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                        <asp:Repeater ID="rptrQuickPriceChange_Regular" runat="server" OnItemDataBound="rptrQuickPriceChangeRegular_ItemDataBound">
                                                            <HeaderTemplate>

                                                                <tbody style="height: 230px;">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr id="gaRepackRow_Regular" onclick="fncRowClick(this,event);" tabindex='<%#(Container.ItemIndex)%>' runat="server">
                                                                    <td>
                                                                        <asp:Label ID="lblSNo_Regular" runat="server" Text='<%# Eval("SNO") %>' />
                                                                    </td>
                                                                    <td style="text-align: center;">
                                                                        <asp:CheckBox ID="chkSelect_Regular" runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLocationCode_Regular" runat="server" Text='<%# Eval("LocationCode") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblInventoryCode_Regular" runat="server" Text='<%# Eval("InventoryCode") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblDescription_Regular" runat="server" Text='<%# Eval("Description") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblQty_Regular" runat="server" Text='<%# Eval("Qtyonhand") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="lblMRP_Regular" BackColor="AliceBlue"
                                                                            Style="text-align: right;" BorderStyle="None" runat="server" Text='<%# Eval("MRP") %>' CssClass="form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="lblSUGPrice_Regular" onkeydown=" return fncSetFocustoNextRow(event,this,'lblSUGPrice_Regular');"
                                                                            runat="server" Text='<%# Eval("SUGPrice") %>' CssClass="form-control-res-right border_nonewith_aliceblue"></asp:TextBox>
                                                                    </td>
                                                                    <td id="tdRWPrice1" runat="server">
                                                                        <asp:TextBox ID="txtRWprice1" onkeydown=" return fncSetFocustoNextRow(event,this,'txtRWprice1');"
                                                                            runat="server" Text='<%# Eval("Wprice1") %>' CssClass="form-control-res-right border_nonewith_aliceblue"></asp:TextBox>
                                                                    </td>
                                                                    <td id="tdRWPrice2" runat="server">
                                                                        <asp:TextBox ID="txtRWprice2" onkeydown=" return fncSetFocustoNextRow(event,this,'txtRWprice2');" runat="server"
                                                                            Text='<%# Eval("Wprice2") %>' CssClass="form-control-res-right border_nonewith_aliceblue"></asp:TextBox>
                                                                    </td>
                                                                    <td id="tdRWPrice3" runat="server">
                                                                        <asp:TextBox ID="txtRWprice3" onkeydown=" return fncSetFocustoNextRow(event,this,'txtRWprice3');" runat="server"
                                                                            Text='<%# Eval("Wprice3") %>' CssClass="form-control-res-right border_nonewith_aliceblue"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSUGDate_Regular" runat="server" Text='<%# Eval("SUGDate") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblLOCPrice_Regular" runat="server" Text='<%# Eval("LOCPrice") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLOCPriceDate_Regular" runat="server" Text='<%# Eval("LOCPriceDate") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblQtyInHand_Regular" runat="server" Text='<%# Eval("Qtyonhand") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblLastsPrice_Regular" runat="server" Style="padding-right: 20px;" Text='<%# Eval("SUGPrice") %>' />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                            </FooterTemplate>
                                                        </asp:Repeater>

                                                        <tfoot>
                                                        </tfoot>
                                                    </table>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </center>
                                    <%-- <div class="button-contol">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClear_Regular" runat="server" class="button-blue" OnClick="lnkClear_Regular_Click"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkUpdate_Regular" runat="server" OnClick="lnkUpdate_Regular_Click" class="button-blue"><i class="icon-play" ></i>Update(F8)</asp:LinkButton>
                                        </div>
                                    </div>--%>
                                </div>

                            </div>
                            <div class="button-contol">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear_Regular" runat="server" class="button-blue" OnClick="lnkClear_Regular_Click"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate_Regular" OnClientClick="fncRegularUpdateClick();return false;" runat="server" class="button-blue"><i class="icon-play" ></i>Update(F8)</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="Repack">
                            <div id="divMainContainer">

                                <div class="container-group-small_new">
                                    <center>
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-3">

                                            <div class="container-control">
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label2" runat="server" Text="Inventory Code"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtInventory" runat="server" MaxLength="20" CssClass="form-control-res"
                                                            onkeydown="return fncShowSearchDialogCommonOpen(event, 'Inventory');"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="lblCost" runat="server" Text="Cost"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtCost" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label5" Style="text-align: right;" runat="server" Text="MRP"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="text-align: left">
                                                        <asp:TextBox ID="txtMrp" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-md-3">
                                            <div class="container-control">
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="lblNetCost" runat="server" Text="Net Cost"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="text-align: left">
                                                        <asp:TextBox ID="txtNetCost" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="lblNewCost" runat="server" Text="New Cost"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="text-align: left">
                                                        <asp:TextBox ID="txtNewCost" Text="0" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="lblNewMRP" runat="server" Text="New MRP"></asp:Label>
                                                    </div>
                                                    <div class="label-right" style="text-align: left">
                                                        <asp:TextBox ID="txtNewMRP" Text="0" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="container-control">


                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label3" runat="server" Text="Description"></asp:Label>
                                                    </div>
                                                    <div class="label-right">
                                                        <asp:TextBox ID="txtDescription" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>

                                                    </div>

                                                </div>
                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label4" runat="server" Text="Selling Price"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtSellingPrice" Text="0" Style="text-align: right;" runat="server" onkeydown="return isNumberKeyWithDecimalNew(event)" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblWprice1" runat="server" Text="W.Price1"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtWPrice1" Text="0" Style="text-align: right;" runat="server" onkeydown="return isNumberKeyWithDecimalNew(event)" MaxLength="9" CssClass="form-control-res"></asp:TextBox>

                                                    </div>
                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <asp:Label ID="Label7" runat="server" Text="New Selling Price"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtNewSellingPrice" Text="0" Style="text-align: right;" runat="server"
                                                            onkeydown="return isNumberKeyWithDecimalNew(event)" MaxLength="18" CssClass="form-control-res"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblWprice2" runat="server" Text="W.Price2"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtWPrice2" Text="0" Style="text-align: right;" runat="server"
                                                            onkeydown="return isNumberKeyWithDecimalNew(event)" MaxLength="9" CssClass="form-control-res"></asp:TextBox>
                                                    </div>

                                                </div>

                                                <div class="control-group-single-res">
                                                    <div class="label-left" style="text-align: left">
                                                        <%--<button type="button" class="button-blue" id="btRepackingapply">Apply</button>--%>
                                                        <asp:LinkButton ID="lnkRepackingapply" class="button-blue" OnClientClick="fncRepackingValue();return false;" runat="server" Text="Apply"></asp:LinkButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-md-3">
                                                        <asp:Label ID="lblWprice3" runat="server" Text="W.Price3"></asp:Label>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtWPrice3" Text="0" Style="text-align: right;" runat="server" MaxLength="9" CssClass="form-control-res"></asp:TextBox>

                                                    </div>

                                                </div>
                                            </div>



                                        </div>
                                        <div class="col-md-2">
                                        </div>
                                        <div id="divrepeater" class="Barcode_fixed_headers grdRepeak">
                                            <asp:UpdatePanel ID="uprptQuickPriceChangeGRD" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table id="tblQuickPriceChange" cellspacing="0" rules="all" border="1">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">S.No
                                                                </th>
                                                                <th scope="col">Location
                                                                </th>
                                                                <th scope="col">Item Code
                                                                </th>
                                                                <th scope="col">Description
                                                                </th>
                                                                <th scope="col">UOM
                                                                </th>
                                                                <th scope="col">Weight
                                                                </th>
                                                                <th runat="server" id="thNewCost" scope="col">New Cost
                                                                </th>
                                                                <th runat="server" id="thNewMRP" scope="col">New MRP
                                                                </th>
                                                                <th scope="col">New Selling
                                                                </th>
                                                                <th runat="server" id="thWPrice1" scope="col">WPrice1
                                                                </th>
                                                                <th runat="server" id="thWPrice2" scope="col">WPrice2
                                                                </th>
                                                                <th runat="server" id="thWPrice3" scope="col">WPrice3
                                                                </th>
                                                                <th scope="col">Old MRP
                                                                </th>
                                                                <th scope="col">Old Cost
                                                                </th>
                                                                <th scope="col">Old Selling
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptrQuickPriceChange" runat="server" OnItemDataBound="rptrQuickPriceChange_ItemDataBound">
                                                            <HeaderTemplate>
                                                                <tbody id="BulkBarcodebody" style="height: 230px;">
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr id="gaRepackRow" tabindex='<%#(Container.ItemIndex)%>' runat="server">
                                                                    <td>
                                                                        <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("SNO") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLocationCode" runat="server" Text='<%# Eval("LocationCode") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblInventoryCode" runat="server" Text='<%# Eval("InventoryCode") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblUOM" runat="server" Text='<%# Eval("UOM") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblWeight" runat="server" Text='<%# Eval("Weight") %>' />
                                                                    </td>
                                                                    <td runat="server" id="tdNewCost" style="text-align: right;">
                                                                        <asp:TextBox ID="txtgridNewCost" Style="text-align: right;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return txtgridNewCost_change(event,this); " CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td runat="server" id="tdNewMRP" style="text-align: right;">
                                                                        <asp:TextBox ID="txtgridNewMRP" Style="text-align: right;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return txtgridNewMRP_change(event,this); " CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtNewSelling" Style="text-align: right;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return txtNewSelling_change(event,this); " CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td runat="server" id="tdWPrice1" style="text-align: right;">
                                                                        <asp:TextBox ID="txtWPrice1" Style="text-align: right;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return txtWPrice1_change(event,this);" CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td runat="server" id="tdWPrice2" style="text-align: right;">
                                                                        <asp:TextBox ID="txtWPrice2" Style="text-align: right;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown=" return txtWPrice2_change(event,this);" CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td runat="server" id="tdWPrice3" style="text-align: right;">
                                                                        <asp:TextBox ID="txtWPrice3" Style="text-align: right; padding-right: 10px;" runat="server" onfocus="this.select();" keypress="return isNumberKeyWithDecimalNew(event)" onkeydown="return txtWPrice3_change(event,this);" CssClass="grn_rptr_textbox_width form-control-res"></asp:TextBox>
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblOldMRP" runat="server" Text='<%# Eval("MRP") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblUnitCost" runat="server" Text='<%# Eval("UnitCost") %>' />
                                                                    </td>
                                                                    <td style="text-align: right;">
                                                                        <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("Price") %>' />
                                                                    </td>
                                                                     <td style="display:none;">
                                                                        <asp:Label ID="lblBatch" runat="server" Text='<%# Eval("BatchNo") %>' />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <tfoot>
                                                        </tfoot>
                                                    </table>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                    </center>
                                    <div id="Mgs" style="padding: 5px; margin-top: 5px; margin-left: 70px; display: none; width: 30%"></div>
                                </div>

                            </div>
                            <div class="button-contol">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick ="return fncCLearRepack();" OnClick="lnkClear_Click"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" OnClientClick="fncUpdateClick();return false;" class="button-blue"><i class="icon-play" ></i>Update(F8)</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" role="tabpanel" id="Batch">
                            <div id="divMainContainerBatch">
                                <div class="container-group-price">
                                    <div class="container-left-price" id="pnlFilter" runat="server">
                                        <ups:SearchFilterUserControl runat="server" ID="searchFilterUserControl"
                                            EnablelocationDropDown="true"
                                            EnableVendorDropDown="true"
                                            EnableDepartmentDropDown="true"
                                            EnableCategoryDropDown="true"
                                            EnableSubCategoryDropDown="true"
                                            EnableBrandDropDown="true"
                                            EnableClassDropDown="false"
                                            EnableSubClassDropDown="false"
                                            EnableMerchandiseDropDown="true"
                                            EnableManufactureDropDown="true"
                                            EnableFloorDropDown="true"
                                            EnableSectionDropDown="true"
                                            EnableBinDropDown="true"
                                            EnableShelfDropDown="true"
                                            EnableBarcodeTextBox="true"
                                            EnableGRNNoTextBox="false"
                                            EnableBatchNoTextBox="false"
                                            EnableWarehouseDropDown="false"
                                            EnableItemTypeDropDown="false"
                                            EnablePriceTextBox="false"
                                            EnableItemNameTextBox="true"
                                            EnableItemCodeTextBox="true" />
                                        <div class="control-group-single-res" runat="server" id="divGidNo">
                                            <div class="label-left">
                                                <asp:Label ID="lblGidNo" runat="server" Text="Gid No"></asp:Label>
                                            </div>
                                            <div class="label-right">
                                                <asp:TextBox ID="txtGidNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'GidNo',  'txtGidNo', '');" MaxLength="15" CssClass="form-control" Style="width: 100%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="width: 100%">
                                            <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                                                <div class="control-button">
                                                    <div class="label-right" style="width: 100%">
                                                        <asp:LinkButton ID="lnkFetch" runat="server" class="button-red" OnClientClick="return fncFilterValidation();"><i class="icon-play"></i>Fetch(F4)</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="control-button">
                                                    <div class="label-right" style="width: 100%">
                                                        <asp:LinkButton ID="lnkclearFilter" runat="server" class="button-red" OnClientClick="return ClearForm();"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">

                                        <table class="grdLoad" cellspacing="0" rules="all" border="1">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Item Code
                                                    </th>
                                                    <th scope="col">Description
                                                    </th>
                                                    <th scope="col">Mrp
                                                    </th>
                                                    <th scope="col">B.cost
                                                    </th>
                                                    <th scope="col">S.Price
                                                    </th>
                                                    <th scope="col">W1Price
                                                    </th>
                                                    <th scope="col">W2Price
                                                    </th>
                                                    <th scope="col">W3Price
                                                    </th>
                                                    <th scope="col">Select
                                                        <asp:CheckBox ID="chSelectAllGrid" runat="server" onclick="fncSelectAllClick(this)" AutoPostBack="false" />
                                                    </th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <div style="overflow: auto; height: 400px;">
                                            <table id="tblFilter" class="grdLoad" cellspacing="0" rules="all" border="1">
                                                <thead>
                                                    <tr class="hiddencol">
                                                        <th scope="col">Item Code
                                                        </th>
                                                        <th scope="col">Description
                                                        </th>
                                                        <th scope="col">Mrp
                                                        </th>
                                                        <th scope="col">S.Price
                                                        </th>
                                                        <th scope="col">W1Price
                                                        </th>
                                                        <th scope="col">W2Price
                                                        </th>
                                                        <th scope="col">W3Price
                                                        </th>
                                                        <th scope="col">Select
                                                        <asp:CheckBox ID="CheckBox1" runat="server" onclick="fncSelectAllClick(this)" AutoPostBack="false" />
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="control-button" id="divPriceChangeAllBatch" runat="server" style="float: right;">
                                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClientClick="return  fncSave();"
                                    Text='Save(F8)'> </asp:LinkButton>
                                <asp:LinkButton ID="lnktblclear" Style="margin-left: 10px" runat="server" class="button-blue" OnClientClick="return Clearall()"
                                    Text='Clear(F7)'></asp:LinkButton>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="hiddencol">
                    <asp:Button ID="Button1" runat="server" />
                    <asp:Button ID="btInventorySearch" runat="server" OnClick="InventorySearch_Click" />
                    <asp:Button ID="btInventorySearch_Regular" runat="server" OnClick="InventorySearch_Regular_Click" />
                    <asp:HiddenField ID="TabName" runat="server" />
                    <asp:HiddenField ID="Mode" runat="server" />
                    <asp:HiddenField ID="hdnOldPrice_Regular" runat="server" />
                    <asp:HiddenField ID="hdRepackCostParam" runat="server" />
                    <asp:HiddenField ID="hdWpriceParam" runat="server" />
                    <asp:HiddenField ID="hdBcastvalue" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="hiddencol">
            <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                <ContentTemplate>

                    <asp:Button ID="btnUpdate" runat="server" OnClick="lnkUpdate_Click" />
                    <asp:Button ID="btnRegularUpdate" runat="server" OnClick="lnkUpdate_Regular_Click" />

                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hidTab" runat="server" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />

            <div id="stkadj_batch">
                <div class="Payment_fixed_headers BatchDetail">
                    <table id="tblBatch" cellspacing="0" rules="all" border="1">
                        <thead>
                            <tr>
                                <th scope="col">S.No
                                </th>
                                <th id="thBatchNo" scope="col">BatchNo
                                </th>
                                <th scope="col">Stock
                                </th>
                                <th scope="col">Cost
                                </th>
                                <th scope="col">MRP
                                </th>
                                <th scope="col">SellingPrice
                                </th>
                                <th scope="col">InventoryCode
                                </th>
                                <th scope="col">Description
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function fncRowClick(source, e) {
            try {

                var obj;
                obj = $(source);
                var target = e.target;
                if (target.tagName == 'INPUT' || target.cellIndex == 10) {
                    return false;
                }
                var inv = $(obj).find('td  span[id*="lblInventoryCode_Regular"]').text();
                var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
                var page = page + "?Status=dailog&InvCode=" + inv;
                var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 630,
                    width: 1200,
                    title: "Inventory Master"
                });
                $dialog.dialog('open');


            }
            catch (err) {
                fncToastError(err.message);
            }
        }
       <%-- function fncBatch(inv) {

            var page = '<%=ResolveUrl("~/Merchandising/frmPriceChangeBatch.aspx") %>';
            var page = page + "?InvCode=" + inv
            var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 645,
                width: 1300,
                title: "Price Change Batch"
            });
            $dialog.dialog('open');
        }--%>
        function fncBatch(inv) { // dinesh
            sBatchNo = "";
            var page = '<%=ResolveUrl("~/Merchandising/frmBatchInfoAndModification.aspx") %>';
            var page = page + "?ItemCode=" + inv + "&BatchNo=" + sBatchNo
            var $dialog = $('<div id="popupBatchInfo" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 650,
                width: 1300,
                title: "Batch Information / Modification",
                closeOnEscape: true
            });
            $dialog.dialog('open');
        }
        function pageLoad() {

            //$("#btRepackingapply").click(function () {
            //    fncRepackingValue();
            //});
            $("select").chosen({ width: '100%' });
            fncDecimal();
            //$("#lnkHQPrice_Regular").click(function () {
            //    Check_all_Regular();
            //});

            var rowObj;
            $("#tblQuickPriceChange_Regular [id*=gaRepackRow_Regular]").each(function () {
                rowObj = $(this);
                rowObj.find('td input[id*="lblMRP_Regular"]').attr("disabled", "disabled");
            });
            $('#<%=txtSellingPrice_Regular.ClientID %>').number(true, 2);
            $('#<%=txtMRP_Regular.ClientID %>').number(true, 2);
            $('#<%=txtCost_Regular.ClientID %>').number(true, 2);
            $('<%=txtNewSellingPrice.ClientID%>').number(true, 2);
            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "Regular";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs ul li a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });

                // For Scrol down in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $("html, body").scrollTop($(document).height());
                })
            });
            $("[id$=txtDescription]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx/GetFilterValue") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.responseText);
                        },
                        failure: function (response) {
                            fncToastError(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id$=txtInventory]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtDescription]").val(i.item.desc);


                    $("[id$=txtInventory]").val(i.item.val);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '')

                    return false;
                },

                // minLength: 1
            });

            $("[id$=txtInventory]").autocomplete({ // dinesh
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.responseText);
                        },
                        failure: function (response) {
                            fncToastError(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id$=txtInventory]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtDescription]").val(i.item.desc);
                    $("[id$=txtInventory]").val(i.item.val);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '')

                    return false;
                },
            });

            $("[id$=txtInventory]").keydown(function (e) { // dinesh
                if (e.keyCode == 13) {
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '')
                    e.preventDefault();
                }
            });

            $("[id$=txtDescription_Regular]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx/GetFilterValue_Regular") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.responseText);
                        },
                        failure: function (response) {
                            fncToastError(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id$=txtInvertoryCode_Regular]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtDescription_Regular]").val(i.item.desc);


                    $("[id$=txtInvertoryCode_Regular]").val(i.item.val);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch_Regular', '')

                    return false;
                },

                // minLength: 1
            });

            $("[id$=txtInvertoryCode_Regular]").autocomplete({ // dinesh
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx/GetFilterValue_Regular") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.responseText);
                        },
                        failure: function (response) {
                            fncToastError(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id$=txtInvertoryCode_Regular]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtInvertoryCode_Regular]").val(i.item.desc);
                    $("[id$=txtInvertoryCode_Regular]").val(i.item.val);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch_Regular', '')

                    return false;
                },
            });

            $("[id$=txtInvertoryCode_Regular]").keydown(function (e) { // dinesh
                if (e.keyCode == 13) {
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch_Regular', '')
                    e.preventDefault();
                }
            });

            $("[id$=txtDiscount_Regular]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("~/Merchandising/frmQuickPriceChange.aspx/GetFilterValue_Discount") %>',
                        //data: "{ 'prefix': '" + request.term + "'}",
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            fncToastError(response.responseText);
                        },
                        failure: function (response) {
                            fncToastError(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id$=txtDiscount_Regular]").val(i.item.desc);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {

                    $("[id$=txtDiscount_Regular]").val(i.item.desc);

                    return false;
                },

                // minLength: 1
            });
        }

        function fncDecimal() {
            try {
                $('#<%=txtNewSellingPrice.ClientID%>').number(true, 2);
                $('#<%=txtSellingPrice.ClientID%>').number(true, 2);
                $('#<%=txtWPrice1.ClientID%>').number(true, 2);
                $('#<%=txtWPrice2.ClientID%>').number(true, 2);
                $('#<%=txtWPrice3.ClientID%>').number(true, 2);
                $('#<%=txtCost.ClientID%>').number(true, 2);
                $('#<%=txtMrp.ClientID%>').number(true, 2);
                $('#<%=txtNewCost.ClientID%>').number(true, 2);
                $('#<%=txtNewMRP.ClientID%>').number(true, 2);
                $('#<%=txtNetCost.ClientID%>').number(true, 2);
                $('#<%=txtRWprice1.ClientID%>').number(true, 2);
                $('#<%=txtRWprice2.ClientID%>').number(true, 2);
                $('#<%=txtRWprice3.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function txtgridNewCost_change(event, data) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtWPrice3"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtgridNewMRP"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {

                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtgridNewCost"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtgridNewCost"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtgridNewCost"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtgridNewCost"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function txtgridNewMRP_change(event, data) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtgridNewMRP"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtNewSelling"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtgridNewCost"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {

                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtgridNewMRP"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtgridNewMRP"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtgridNewMRP"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtgridNewMRP"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function txtNewSelling_change(event, data) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    txtgrdNewSellingPrice = parseFloat(data.value) * parseFloat(rowObj.find('td  span[id*="lblWeight"]').text());
                    txtgrdOldMRPPrice = parseFloat(rowObj.find('td  span[id*="lblOldMRP"]').text());
                    PriceCheckandset_Repacking(rowObj, txtgrdNewSellingPrice, txtgrdOldMRPPrice);
                    console.log();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtNewSelling"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtgridNewMRP"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {

                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtNewSelling"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtNewSelling"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtNewSelling"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtNewSelling"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function txtWPrice1_change(event, data) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtWPrice1"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtWPrice2"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtNewSelling"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {
                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtWPrice1"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtWPrice1"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtWPrice1"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtWPrice1"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function txtWPrice2_change(event, data) {
            try {
                debugger;
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtWPrice2"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {

                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtWPrice2"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtWPrice2"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtWPrice2"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtWPrice2"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function txtWPrice3_change(event, data) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                rowObj = $(data).parent().parent();
                if (keyCode == 13) {
                    rowObj = $(data).parent().parent();
                    var NextRowobj = rowObj.next();
                    NextRowobj.find('td input[id*="txtWPrice3"]').select();
                    return false;
                }
                if (keyCode == 39) {
                    rowObj.find('td input[id*="txtgridNewCost"]').select();
                    event.preventDefault();
                }
                if (keyCode == 37) {
                    rowObj.find('td input[id*="txtWPrice2"]').select();
                    event.preventDefault();
                }
                if (keyCode == 40) {

                    var NextRowobj = rowObj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtWPrice3"]').select();
                    }
                    else {
                        rowObj.siblings().first().find('td input[id*="txtWPrice3"]').select();
                    }
                }

                if (keyCode == 38) {
                    var prevrowobj = rowObj.prev();
                    if (prevrowobj.length > 0) {
                        prevrowobj.find('td input[id*="txtWPrice3"]').select();
                    }
                    else {
                        rowObj.siblings().last().find('td input[id*="txtWPrice3"]').select();
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function NewCostMrp_Repacking(rowObj, grdNetCost, grdNewCost, grdNewMRP, grdNewSellingPrice) { // dinesh
            try {

                if (grdNewCost > grdNewMRP) {
                    rowObj.find('td input[id*="txtgridNewMRP"]').css('background-color', '‎#ff4c4c');
                }
                if (grdNewSellingPrice <= grdNewMRP) {
                    rowObj.find('td input[id*="txtNewSelling"]').css('background-color', '‎#90EE90');
                }
                else {
                    rowObj.find('td input[id*="txtNewSelling"]').css('background-color', '‎#ff4c4c');
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function disableFunctionKeys(e) {

            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
                if (e.keyCode == 119) {
                    if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Regular") {

                        if ($('#<%=lnkUpdate_Regular.ClientID %>').is(":visible"))
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkUpdate_Regular', '')
                        e.preventDefault();
                    } else if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Repack") {
                        if ($('#<%=lnkUpdate.ClientID %>').is(":visible"))
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkUpdate', '');

                        e.preventDefault();
                    } else if ($("[id*=TabName]").val() != "" || $("[id*=TabName]").val() == "Batch") {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            fncSave();
                    }
                }
                else if (e.keyCode == 117) {
                    if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Regular") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClear_Regular', '');
                        fncToastInformation("Cleared");
                        e.preventDefault();
                    } else if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Repack") {

                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');

                        fncToastInformation("Cleared");
                        e.preventDefault();

                    } else if ($("[id*=TabName]").val() != "" || $("[id*=TabName]").val() == "Batch") {
                        ClearForm();
                    }
                }
                else if (e.keyCode == 115) {
                    if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Batch") {
                        fncFilterValidation();
                    }
                }
                else if (e.keyCode == 118) {
                    if ($("[id*=TabName]").val() == "" || $("[id*=TabName]").val() == "Batch") {
                        Clearall();
                    }
                }

            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function fncSetValue() {
            try {
                $('#txtSearch').val('');
                if (SearchTableName == "Inventory" && varType == 'Inventory') {
                    $("[id$=txtDescription]").val(Description);
                    $("[id$=txtInventory]").val(Code);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                }
                if (SearchTableName == "Inventory" && varType == 'Regular') {
                    $("[id$=txtDescription_Regular]").val(Description);


                    $("[id$=txtInvertoryCode_Regular]").val(Code);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch_Regular', '')
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        var varType;
        function fncShowSearchDialogCommonOpen(event, data) {
            try {
                var charCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (charCode == 112) {
                    if (data == 'Regular') {
                        varType = 'Regular';
                        fncShowSearchDialogCommon(event, 'Inventory', 'txtInventory', '');
                    }
                    if (data == 'Inventory') {
                        varType = 'Inventory';
                        fncShowSearchDialogCommon(event, 'Inventory', 'txtInventory', '');
                    }
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
    <asp:HiddenField ID ="hidBatchNo" runat="server" />
</asp:Content>
