﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMixMatchView.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmMixMatchView" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


        .no-close .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <link type="text/css" href="../css/mixmaxqty.css" rel="stylesheet" />

    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'MixMatchView');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "MixMatchView";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

        function Pr_Edit(source) {
            try {

                $('#<%=hidPromotioncode.ClientID %>').val($("td", $(source).closest("tr")).eq(1).html().replace(/&nbsp;/g, ''));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">MixMatch</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">View MixMatch</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="updateInvGrid" runat="Server">
            <ContentTemplate>
                <div class="gridDetails">
                    <div style="float: left">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" PostBackUrl="../Merchandising/frmMixMatchEntry.aspx"
                                Text='Add'></asp:LinkButton>
                        </div>
                        <div class="control-button" style="display: none">
                            <asp:LinkButton ID="lnkDeactivate" runat="server" class="button-blue"
                                Text='Deactivate'></asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" PostBackUrl="../MISDashboard/frmMisDashBoard.aspx"
                                Text='Close'></asp:LinkButton>
                        </div>
                    </div>
                    <div class="grid-search">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"
                                        onFocus="this.select()"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" OnClick="lnkSearchGrid_Click"
                                Text='<%$ Resources:LabelCaption,btn_Search %>'></asp:LinkButton>
                                    <asp:CheckBox ID="chkDeactive" runat="server" Class="radioboxlist" Text="Deactivate"></asp:CheckBox>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>

                    <div class="container-group-pc1">
                        <div class="container-horiz-top-header1">
                            <div class="right-container-top-header">
                                Mix and Max Items
                            </div>
                            <div class="right-container-top-detail">
                                <div class="GridDetails">
                                    <div class="row">
                                        <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                            <div class="grdLoad">
                                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">S.No
                                                            </th>
                                                            <th scope="col">Group Code
                                                            </th>
                                                            <th scope="col">Description
                                                            </th>
                                                            <th scope="col">Buy Qty
                                                            </th>
                                                            <th scope="col">Price
                                                            </th>
                                                            <th scope="col">Valid From
                                                            </th>
                                                            <th scope="col">Valid To
                                                            </th>
                                                            <th scope="col">Status
                                                            </th>
                                                            <th scope="col">Modify
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 270px; width: 1450px; background-color: aliceblue;">
                                                    <asp:GridView ID="grdMixmatchList" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false"
                                                        DataKeyNames="Groupcode" CssClass="pshro_GridDgn grdLoad">
                                                        <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                            <asp:BoundField DataField="RowNumber" HeaderText="Item Code"></asp:BoundField>
                                                            <asp:BoundField DataField="Groupcode" HeaderText="Item Name"></asp:BoundField>
                                                            <asp:BoundField DataField="Description" HeaderText="Department Code" />
                                                            <asp:BoundField DataField="BuyQty" HeaderText="Brand Code"></asp:BoundField>
                                                            <asp:BoundField DataField="Price" HeaderText="Category Code"></asp:BoundField>                                                            
                                                            <asp:BoundField DataField="FromDate" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                            <asp:BoundField DataField="Todate" HeaderText="Unit Cost" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                              <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                                                            <asp:TemplateField HeaderText="Modify">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgbtnModify" runat="server" ImageUrl="~/images/edit-icon.png"
                                                                        ToolTip="Click here to Modify" OnClientClick="return Pr_Edit(this);"
                                                                        OnClick="lnkModify_Click" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>
                                                <ups:PaginationUserControl runat="server" ID="InvPaging" OnPaginationButtonClick="MixPaging_PaginationButtonClick" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updateInvGrid">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidPromotioncode" runat="server" />
</asp:Content>
