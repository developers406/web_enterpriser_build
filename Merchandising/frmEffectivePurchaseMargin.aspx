﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmEffectivePurchaseMargin.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmEffectivePurchaseMargin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .grdBody td {
            padding: 3px;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            display: none;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 465px;
            max-width: 465px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 65px;
            max-width: 65px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 65px;
            max-width: 65px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 85px;
            max-width: 85px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            display: none;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            display: none;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            display: none;
        }

        .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            display: none;
        }

        .grdLoad td {
            padding: 2px;
        }

        .cssGrid {
            overflow-x: hidden;
            overflow-y: scroll;
            height: 375px;
            width: 135%;
            background-color: aliceblue;
        }

        .left {
            text-align: right !important;
        }
    </style>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'EffectivePurchaseMargin');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "EffectivePurchaseMargin";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


    <script type="text/javascript">
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                    $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function pageLoad() {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
                $("#<%= txtEfectiveDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
                $("#<%= txtEfectiveDate.ClientID %>").on('keydown', function () {
                    return false;
                });
                $('#<%=txtDis1.ClientID %>').number(true, 2);
                $('#<%=txtDis2.ClientID %>').number(true, 2);
                $('#<%=txtDis3.ClientID %>').number(true, 2);
                $("#<%=rdoMarkDown.ClientID %>").click(function () {
                    $('#<%=lblDis.ClientID %>').text('Dis %');
                    $('#<%=lblDis2.ClientID %>').text('Add.Dis %');
                    $('#<%=lblDis3.ClientID %>').text(' ');
                    $('#<%=txtDis3.ClientID %>').attr("disabled", "disabled");
                });
                $("#<%=rdoMarkUp.ClientID %>").click(function () {
                    $('#<%=lblDis.ClientID %>').text('Dis1 %');
                    $('#<%=lblDis2.ClientID %>').text('Dis2 %');
                    $('#<%=lblDis3.ClientID %>').text('Dis3 %');
                    $('#<%=txtDis3.ClientID %>').removeAttr("disabled");
                });
                if ($("#<%=rdoMarkDown.ClientID %>").is(':checked')) {
                    $('#<%=txtDis3.ClientID %>').attr("disabled", "disabled");
                    $('#<%=lblDis.ClientID %>').text('Dis %');
                    $('#<%=lblDis2.ClientID %>').text('Add.Dis %');
                    $('#<%=lblDis3.ClientID %>').text(' ');
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncAddItem() {
            try {
                var Show = '';

                if ($("#<%=grdEffectivepurMargin.ClientID %>").length == 0) {
                      Show = Show + '<%=Resources.LabelCaption.Alert_RecordNotFound%>';
                  }
                  if ((parseFloat($("#<%=txtDis1.ClientID %>").val()) <= 0 || $("#<%=txtDis1.ClientID %>").val() == "")
                      && (parseFloat($("#<%=txtDis2.ClientID %>").val()) <= 0 || $("#<%=txtDis2.ClientID %>").val() == "")
                    && (parseFloat($("#<%=txtDis3.ClientID %>").val()) <= 0 || $("#<%=txtDis3.ClientID %>").val() == "")) {
                    Show = Show + '<br />Please Enter Atleast one Discount Percentage';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    $("#<%=grdEffectivepurMargin.ClientID %> tbody tr").each(function () {
                        var Disper = 0;
                        var BasicCost = $(this).find('td input[id*="txtbasicCost"]').val();
                        var Mrp = $(this).find('td input[id*="txtMrp"]').val();
                        var SGST = $(this).find("td").eq(13).html();
                        var CGST = $(this).find("td").eq(14).html();
                        var Cess = $(this).find("td").eq(15).html();
                        $(this).find('td input[id*="txtDis1"]').val(parseFloat($("#<%=txtDis1.ClientID %>").val()).toFixed(2));
                        $(this).find('td input[id*="txtDis2"]').val(parseFloat($("#<%=txtDis2.ClientID %>").val()).toFixed(2));
                        $(this).find('td input[id*="txtDis3"]').val(parseFloat($("#<%=txtDis3.ClientID %>").val()).toFixed(2));
                        var txtDiscountPrc1 = $(this).find('td input[id*="txtDis1"]').val().trim();
                        var txtDiscountPrc2 = $(this).find('td input[id*="txtDis2"]').val().trim();
                        var txtDiscountPrc3 = $(this).find('td input[id*="txtDis3"]').val().trim();
                        if ($(this).find("td").eq(16).html().trim() == "CS" || $(this).find("td").eq(16).html().trim() == "C") {
                            if (parseFloat(BasicCost) > 0) {
                                var TotDiscAmt;
                                var DiscountedBasicCost;
                                var DiscAmt1 = (BasicCost * parseFloat(txtDiscountPrc1).toFixed(2) / 100).toFixed(2);
                                TotDiscAmt = DiscAmt1;
                                DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(DiscAmt1)).toFixed(2)
                                var DiscAmt2 = (DiscountedBasicCost * parseFloat(txtDiscountPrc2).toFixed(2) / 100).toFixed(2);
                                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                                DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                                var DiscAmt3 = (DiscountedBasicCost * parseFloat(txtDiscountPrc3).toFixed(2) / 100).toFixed(2);
                                TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                                var GrossCost = (parseFloat(BasicCost).toFixed(2) - TotDiscAmt).toFixed(2);

                                var CGSTAmt = (GrossCost * parseFloat(CGST) / 100).toFixed(2);
                                var SGSTAmt = CGSTAmt;
                                var CESSAmt = (GrossCost * parseFloat(Cess).toFixed(2) / 100).toFixed(2);

                                var NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
                                    parseFloat(CESSAmt) + parseFloat('0')).toFixed(2);

                                $(this).find('td input[id*="txtGrossCost"]').val(parseFloat(GrossCost).toFixed(2));
                                $(this).find('td input[id*="txtNetCost"]').val(parseFloat(NetCost).toFixed(2));

                            }
                        }
                        else {

                            if (parseFloat(Mrp) > 0) {
                                var MRP1_MDP = parseFloat(txtDiscountPrc1).toFixed(2);
                                var MRP1_MDAP = parseFloat(txtDiscountPrc2).toFixed(2);
                                var NetCost = parseFloat(Mrp - Mrp * MRP1_MDP / 100).toFixed(2);
                                NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2);


                                var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat('0').toFixed(2);
                                var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)))).toFixed(2);

                                var SGSTAmt;
                                var CGSTAmt;
                                var CESSAmt;
                                if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) == 0) {
                                    SGSTAmt = 0;
                                    CGSTAmt = 0;
                                    CESSAmt = 0;
                                }
                                else {
                                    SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) * parseFloat(SGST)).toFixed(2);
                                    CGSTAmt = SGSTAmt;
                                    CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                                }

                                var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                                if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                                    GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                                    CESSAmt = 0.00;
                                }
                                $(this).find('td input[id*="txtbasicCost"]').val(parseFloat(GrossCost).toFixed(2));//BasicCost==GrossCost
                                $(this).find('td input[id*="txtGrossCost"]').val(parseFloat(GrossCost).toFixed(2));
                                $(this).find('td input[id*="txtNetCost"]').val(parseFloat(NetCost).toFixed(2));
                            }
                        }
                    });
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

        $(document).ready(function () {
            //$("[id*=grdEffectivepurMargin] tr").live('click', function (event) {
            //    $("[id*=grdEffectivepurMargin]").closest('tr').css("background-color", "");
            //    $(this).closest('tr').css("background-color", "#acb3a4");
            //});
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            var functionKeys = new Array(40, 38, 13, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '100%',
                        'margin-left': '0'
                    });
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("1330px");
                    $("#<%=divGrd2.ClientID %>").width("90%");
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '74%',
                        'margin-left': '0% !important'
                    });
                    $("#<%=HideFilter_GridOverFlow.ClientID%>").width("1000px");
                    $("select").trigger("liszt:updated");
                    $("#<%=divGrd2.ClientID %>").width("123%");
                }
                return false;
            }
            catch (err) {
                alert(err.Message);
            }
        }

        function clearForm() {
            try {
                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtItemName.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtFloor.ClientID %>').val('');
                $('#<%= txtShelf.ClientID %>').val('');
                $('#<%= txtBin.ClientID %>').val('');
                $('#<%= txtMerchandise.ClientID %>').val('');
                $('#<%= txtManufacture.ClientID %>').val('');
                $('#<%= txtSection.ClientID %>').val('');
                $(':checkbox, :radio').prop('checked', false);
                $("select").trigger("liszt:updated");
                $('#<%= rdoMarkUp.ClientID %>').prop('checked', true);
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
        }

        function fncVenItemRowdblClk(itemcode) {
            try {
                fncOpenItemhistory($.trim(itemcode));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function fncOpenItemhistory(itemcode) {
            var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
            page = page + "?InvCode=" + itemcode + "&Status=dailog";
            var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 700,
                width: 1250,
                title: "Inventory History"
            });
            $dialog.dialog('open');
        }
        function txtbasicCost_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtbasicCost"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                rowobj.find('td input[id*="txtDis1"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtMrp"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtbasicCost"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtbasicCost"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtbasicCost"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtbasicCost"]').select();
                }
            }
            return true;
        }

        function txtDis1_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtDis1"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                rowobj.find('td input[id*="txtDis2"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtbasicCost"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDis1"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDis1"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDis1"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDis1"]').select();
                }
            }
            return true;
        }

        function txtDis2_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtDis2"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                rowobj.find('td input[id*="txtDis3"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtDis1"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDis2"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDis2"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDis2"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDis2"]').select();
                }
            }
            return true;
        }

        function txtDis3_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtDis3"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                rowobj.find('td input[id*="txtMrp"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtDis2"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtDis3"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtDis3"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtDis3"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtDis3"]').select();
                }
            }
            return true;
        }

        function txtMrp_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var NextRow = row.NextRow + 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="txtMrp"]').select();
                        return;
                    }
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                var NextRowobj = rowobj;
                rowobj.find('td input[id*="txtbasicCost"]').select();
                event.preventDefault();

            }
            if (charCode == 37) {
                var NextRowobj = rowobj;
                NextRowobj.find('td input[id*="txtDis3"]').select();
                event.preventDefault();
                return;

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtMrp"]').select();
                    return;
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtMrp"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtMrp"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtMrp"]').select();
                }
            }
            return true;
        }

        function txtbasicCost_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sBasicCost = row.cells[6].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sStatus = row.cells[16].innerHTML;
                if ($.trim(sStatus.charAt(0)) == "M") {
                    row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                    validation = true;
                    ShowPopupMessageBox("This is MarkDown Item.You cannot edit this.", "#ContentPlaceHolder1_grdEffectivepurMargin_txtbasicCost_" + rowIndex);

                    return;
                } else if (sBasicCost == "" || parseFloat(sBasicCost) <= 0) {
                    row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                    return;
                }
                else if (sBasicCost != "") {
                    if (parseFloat(sBasicCost) > parseFloat(sMRP)) {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to Basic Cost", "#ContentPlaceHolder1_grdEffectivepurMargin_txtbasicCost_" + rowIndex);

                        return;
                    }

                    else {
                        fncGetNetCost_MarkUp(lnk);
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtDis1_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sDis = row.cells[7].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sStatus = row.cells[16].innerHTML;
                if (sDis == "" || parseFloat(sDis) <= 0) {
                    row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                    return;
                } else if ($.trim(sStatus.charAt(0)) == "M") {
                    fncGetNetCost_MarkDown(lnk);
                }
                else {
                    fncGetNetCost_MarkUp(lnk);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtDis2_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sDis = row.cells[8].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sStatus = row.cells[16].innerHTML;
                if (sDis == "" || parseFloat(sDis) <= 0) {
                    row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;
                    return;
                } else if ($.trim(sStatus.charAt(0)) == "M") {
                    fncGetNetCost_MarkDown(lnk);
                }
                else {
                    fncGetNetCost_MarkUp(lnk);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtDis3_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sBasicCost = row.cells[9].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sStatus = row.cells[16].innerHTML;
                if (sBasicCost == "" || parseFloat(sBasicCost) <= 0) {
                    row.cells[9].getElementsByTagName("input")[0].value = row.cells[9].getElementsByTagName("span")[0].innerHTML;
                    return;
                }
                else if ($.trim(sStatus.charAt(0)) == "M") {
                    validation = true;
                    ShowPopupMessageBox("This is MarkDown Item.You cannot edit this.", "#ContentPlaceHolder1_grdEffectivepurMargin_txtDis_" + rowIndex);
                    return;
                }
                else {
                    fncGetNetCost_MarkUp(lnk);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtMrp_TextChanged(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sMRP = row.cells[5].getElementsByTagName("input")[0].value;
                var sSell = row.cells[12].innerHTML;
                var sStatus = row.cells[16].innerHTML;
                if ($.trim(sStatus.charAt(0)) == "C") {
                    row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;
                    validation = true;
                    ShowPopupMessageBox("This is MarkUp Item.You cannot edit this.", "#ContentPlaceHolder1_grdEffectivepurMargin_txtMrp_" + rowIndex);

                    return;
                }
                else if (sMRP == "" || parseFloat(sMRP) <= 0) {
                    row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;
                    return;
                }
                else if (sMRP != "") {
                    if (parseFloat(sSell) > parseFloat(sMRP)) {
                        row.cells[5].getElementsByTagName("input")[0].value = row.cells[5].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        ShowPopupMessageBox("MRP Should be Greater than or Equal to Basic Cost", "#ContentPlaceHolder1_grdEffectivepurMargin_txtMrp_" + rowIndex);
                        return;
                    }

                    else {
                        fncGetNetCost_MarkDown(lnk);
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetNetCost_MarkUp(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var Disper = 0;
                var BasicCost = row.cells[6].getElementsByTagName("input")[0].value;
                var Mrp = row.cells[5].getElementsByTagName("input")[0].value;
                var SGST = row.cells[13].innerHTML;
                var CGST = row.cells[14].innerHTML;
                var Cess = row.cells[15].innerHTML;
                var txtDiscountPrc1 = row.cells[7].getElementsByTagName("input")[0].value;
                var txtDiscountPrc2 = row.cells[8].getElementsByTagName("input")[0].value;
                var txtDiscountPrc3 = row.cells[9].getElementsByTagName("input")[0].value;

                if (parseFloat(BasicCost) > 0) {
                    var TotDiscAmt;
                    var DiscountedBasicCost;
                    var DiscAmt1 = (BasicCost * parseFloat(txtDiscountPrc1).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = DiscAmt1;
                    DiscountedBasicCost = (parseFloat(BasicCost) - parseFloat(DiscAmt1)).toFixed(2)
                    var DiscAmt2 = (DiscountedBasicCost * parseFloat(txtDiscountPrc2).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt2);
                    DiscountedBasicCost = parseFloat(DiscountedBasicCost) - parseFloat(DiscAmt2);
                    var DiscAmt3 = (DiscountedBasicCost * parseFloat(txtDiscountPrc3).toFixed(2) / 100).toFixed(2);
                    TotDiscAmt = parseFloat(TotDiscAmt) + parseFloat(DiscAmt3);

                    var GrossCost = (parseFloat(BasicCost).toFixed(2) - TotDiscAmt).toFixed(2);

                    var CGSTAmt = (GrossCost * parseFloat(CGST) / 100).toFixed(2);
                    var SGSTAmt = CGSTAmt;
                    var CESSAmt = (GrossCost * parseFloat(Cess).toFixed(2) / 100).toFixed(2);

                    var NetCost = parseFloat(parseFloat(GrossCost) + parseFloat(CGSTAmt) + parseFloat(SGSTAmt) +
                        parseFloat(CESSAmt) + parseFloat('0')).toFixed(2);

                    row.cells[10].getElementsByTagName("input")[0].value = parseFloat(GrossCost).toFixed(2);
                    row.cells[11].getElementsByTagName("input")[0].value = parseFloat(NetCost).toFixed(2);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncGetNetCost_MarkDown(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var Disper = 0;
                var BasicCost = row.cells[6].getElementsByTagName("input")[0].value;
                var Mrp = row.cells[5].getElementsByTagName("input")[0].value;
                var SGST = row.cells[13].innerHTML;
                var CGST = row.cells[14].innerHTML;
                var Cess = row.cells[15].innerHTML;
                var txtDiscountPrc1 = row.cells[7].getElementsByTagName("input")[0].value;
                var txtDiscountPrc2 = row.cells[8].getElementsByTagName("input")[0].value;
                var txtDiscountPrc3 = row.cells[9].getElementsByTagName("input")[0].value;

                if (parseFloat(Mrp) > 0) {
                    var MRP1_MDP = parseFloat(txtDiscountPrc1).toFixed(2);
                    var MRP1_MDAP = parseFloat(txtDiscountPrc2).toFixed(2);
                    var NetCost = parseFloat(Mrp - Mrp * MRP1_MDP / 100).toFixed(2);
                    NetCost = NetCost - parseFloat(NetCost * MRP1_MDAP / 100).toFixed(2);


                    var NetCost1 = parseFloat(NetCost).toFixed(2) - parseFloat('0').toFixed(2);
                    var NetCost2 = parseFloat((NetCost1 * (parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) / (parseFloat(100) + parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)))).toFixed(2);

                    var SGSTAmt;
                    var CGSTAmt;
                    var CESSAmt;
                    if ((parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) == 0) {
                        SGSTAmt = 0;
                        CGSTAmt = 0;
                        CESSAmt = 0;
                    }
                    else {
                        SGSTAmt = (parseFloat(NetCost2) / (parseFloat(CGST) + parseFloat(SGST) + parseFloat(Cess)) * parseFloat(SGST)).toFixed(2);
                        CGSTAmt = SGSTAmt;
                        CESSAmt = (parseFloat(NetCost2) - parseFloat(CGSTAmt) - parseFloat(SGSTAmt)).toFixed(2);
                    }

                    var GrossCost = (parseFloat(parseFloat(NetCost1) - parseFloat(SGSTAmt) - parseFloat(CGSTAmt) - parseFloat(CESSAmt))).toFixed(2);

                    if (parseFloat(CESSAmt).toFixed(2) == '-0.01' || parseFloat(CESSAmt).toFixed(2) == '0.01') {
                        GrossCost = parseFloat(GrossCost) + parseFloat(CESSAmt);
                        CESSAmt = 0.00;
                    }
                    row.cells[6].getElementsByTagName("input")[0].value = parseFloat(GrossCost).toFixed(2);//BasicCost==GrossCost
                    row.cells[10].getElementsByTagName("input")[0].value = parseFloat(GrossCost).toFixed(2);
                    row.cells[11].getElementsByTagName("input")[0].value = parseFloat(NetCost).toFixed(2);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSelectUnselectrow(event) {
            pricevalidationstatus = "All";
            try {
                if (($(event).is(":checked"))) {
                    $("#<%=grdEffectivepurMargin.ClientID %> tbody tr").each(function () {
                        $(this).find('td input[id*="chkSingle"]').attr("checked", "checked");
                    });
                }
                else {
                    $("#<%=grdEffectivepurMargin.ClientID%> input[id*='chkSingle']:checkbox").removeAttr("checked");
                }
                $("select").trigger("liszt:updated");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDis2EnterTraversal(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var item = $('#<%=txtItemCode.ClientID%>').val();
                if (charCode == 13) {
                    if ($('#<%=txtDis3.ClientID%>').is(':disabled')) {
                        $('#<%=lnkAdd.ClientID%>').focus();
                    }
                    else {
                        $('#<%=txtDis3.ClientID%>').select();
                    }
                    return false;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSaveValidation() {
            if ($(".cbLocation").find("input:checked").length == 0) {
                fncToastInformation('<%=Resources.LabelCaption.Alert_Loc%>');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Effective Purchase Margin</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="container-left-price" id="pnlFilter" runat="server">
                        <div class="control-group-price-header">
                            Filtertions
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtMerchandise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchandise', 'txtManufacture');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-price-header">
                            Pricing Methods
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:RadioButton ID="rdoMarkUp" runat="server" Checked="true" Text="Mark Up" GroupName="Order"></asp:RadioButton>
                            </div>
                            <div class="label-right">
                                <asp:RadioButton ID="rdoMarkDown" runat="server" Text="Mark Down" GroupName="Order"></asp:RadioButton>
                            </div>
                        </div>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" OnClick="lnkFilter_Click"
                                    Text='<%$ Resources:LabelCaption,btn_Filter %>'></i></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm(); return false;"
                                    Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                    <ContentTemplate>
                        <div class="gridDetails">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="control-group-price-header">
                                        Locations
                                    </div>
                                    <div class="control-group-single">
                                        <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                                    </div>
                                    <div class="control-group-single">
                                        <div style="overflow-y: auto; width: 260px; height: 40px; margin-bottom: 10px">
                                            <asp:CheckBoxList ID="cblLocation" runat="server" CssClass="cbLocation">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-3" style="margin-top: 50px;">

                                    <div class="col-md-12">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="txtEfectiveDate" Width="100%" CssClass="form-control-res" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" style="margin-top: 50px;">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <asp:Label ID="lblDis" runat="server" Text="Dis1 %"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="lblDis2" runat="server" Text="Dis2 %"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="lblDis3" runat="server" Text="Dis3 %"></asp:Label>
                                        </div>
                                        <div class="col-md-3">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtDis1" onFocus="this.select();" Width="100%" runat="server" Text="0" CssClass="form-control-res left"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtDis2" onFocus="this.select();" onkeydown="return fncDis2EnterTraversal(event);" Width="100%" runat="server" Text="0" CssClass="form-control-res left"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3" id="divDis3">
                                            <asp:TextBox ID="txtDis3" Width="100%" runat="server" onFocus="this.select();" Text="0" CssClass="form-control-res left"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="lnkAdd" class="button-green" OnClientClick="fncAddItem();return false;" runat="server" Text="Apply"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="GridDetails col-md-12" style="overflow-x: scroll; overflow-y: hidden; height: 375px; background-color: aliceblue;"
                                id="HideFilter_GridOverFlow" runat="server">
                                <div class="grdLoad">
                                    <div class="col-md-12">
                                        <table id="tblGrd" cellspacing="0" rules="all" border="1" class="fixed_headers">
                                            <thead>
                                                <tr>
                                                    <th scope="col">
                                                        <asp:CheckBox ID="cbProfilterheader" onclick="fncSelectUnselectrow(this)" runat="server" /></th>
                                                    <th scope="col">S.No</th>
                                                    <th scope="col">ItemCode</th>
                                                    <th scope="col">Description</th>
                                                    <th scope="col">Batch</th>
                                                    <th scope="col">MRP</th>
                                                    <th scope="col">B.Cost</th>
                                                    <th scope="col">Dis 1</th>
                                                    <th scope="col">Dis 2</th>
                                                    <th scope="col">Dis 3</th>
                                                    <th scope="col">G.Cost</th>
                                                    <th scope="col">Net Cost</th>
                                                    <th scope="col">S.Price</th>
                                                    <th scope="col">SGST</th>
                                                    <th scope="col">CGST</th>
                                                    <th scope="col">Cess</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class=" col-md-12 cssGrid" id="divGrd2" runat="server">
                                        <asp:GridView ID="grdEffectivepurMargin" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            ShowHeaderWhenEmpty="true" OnRowDataBound="grdItemDetails_RowDataBound">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                            <PagerStyle CssClass="pshro_text" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSingle" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RowNo" HeaderText="SNO" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Item Name" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:BoundField DataField="BatchNo" HeaderText="Batch No" ItemStyle-HorizontalAlign="Left"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Basic Cost">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMrp" runat="server" Text='<%# Eval("Mrp") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtMrp" runat="server" Text='<%# Eval("Mrp") %>' onkeyup="return txtMrp_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select();"
                                                            CssClass="grid-textbox textboxOrder" onchange="txtMrp_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Basic Cost">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBasicCost" runat="server" Text='<%# Eval("BasicCost") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtbasicCost" runat="server" Text='<%# Eval("BasicCost") %>' onkeyup="return txtbasicCost_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select();"
                                                            CssClass="grid-textbox textboxOrder" onchange="txtbasicCost_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dis 1">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDis1" runat="server" Text='<%# Eval("Dis1") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtDis1" runat="server" Text='<%# Eval("Dis1") %>' onkeyup="return txtDis1_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select();"
                                                            CssClass="grid-textbox" onchange="txtDis1_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dis 2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDis2" runat="server" Text='<%# Eval("Dis2") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtDis2" runat="server" Text='<%# Eval("Dis2") %>' onkeyup="return txtDis2_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select();"
                                                            CssClass="grid-textbox" onchange="txtDis2_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dis 3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDis3" runat="server" Text='<%# Eval("Dis3") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtDis3" runat="server" Text='<%# Eval("Dis3") %>' onkeyup="return txtDis3_Keystroke(event,this);" onkeydown="return isNumberKeyWithDecimalNew(event)" onFocus="this.select();"
                                                            CssClass="grid-textbox" onchange="txtDis3_TextChanged(this);" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dis 3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGrossCost" runat="server" Text='<%# Eval("GrossCost") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtGrossCost" runat="server" Text='<%# Eval("GrossCost") %>'
                                                            CssClass="grid-textbox" onmousedown="return false;" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dis 3">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblNetCost" runat="server" Text='<%# Eval("Netcost") %>' CssClass="hiddencol"></asp:Label>
                                                        <asp:TextBox ID="txtNetCost" runat="server" Text='<%# Eval("Netcost") %>'
                                                            CssClass="grid-textbox" onmousedown="return false" Style="text-align: right"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Sellingprice" HeaderText="Selling price" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ITaxPer1" HeaderText="SGST" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ITaxPer2" HeaderText="CGST" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="ITaxPer4" HeaderText="Cess" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="Status" HeaderText="Cess" ItemStyle-HorizontalAlign="Right" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <div class="col-md-7">
                            </div>
                            <div class="col-md-5">
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btn_Update %>' OnClientClick="return fncSaveValidation();" OnClick="lnkUpdate_Click"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>' OnClick="lnkClear_CLick"></asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClose" runat="server" PostBackUrl="../Masters/frmMain.aspx" class="button-blue" Text="Close"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
    <asp:HiddenField ID="hidWholeSale" runat="server" />
</asp:Content>
