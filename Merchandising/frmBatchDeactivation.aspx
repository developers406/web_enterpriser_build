﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBatchDeactivation.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmBatchDeactivation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


        .batchDeactivation_tbl td:nth-child(1), .batchDeactivation_tbl th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .batchDeactivation_tbl td:nth-child(2), .batchDeactivation_tbl th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
        }

        .batchDeactivation_tbl td:nth-child(3), .batchDeactivation_tbl th:nth-child(3) {
            min-width: 300px;
            max-width: 300px;
        }

        .batchDeactivation_tbl td:nth-child(4), .batchDeactivation_tbl th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .batchDeactivation_tbl td:nth-child(5), .batchDeactivation_tbl th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .batchDeactivation_tbl td:nth-child(5) {
            text-align: right !important;
        }

        .batchDeactivation_tbl td:nth-child(6), .batchDeactivation_tbl th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .batchDeactivation_tbl td:nth-child(6) {
            text-align: right !important;
        }

        .batchDeactivation_tbl td:nth-child(7), .batchDeactivation_tbl th:nth-child(7) {
            min-width: 50px;
            max-width: 50px;
        }

        .batchDeactivation_tbl td:nth-child(7) {
            text-align: center !important;
        }
    </style>
    
   <script type="text/javascript">
       var d1;
       var d2;
       var Opendate;
       var dur;
       function fncGetUrl() {
           fncSaveHelpVideoDetail('', '', 'BatchDeactivation');
       }
       function fncOpenvideo() {

           document.getElementById("ifHelpVideo").src = HelpVideoUrl;

           var Mode = "BatchDeactivation";
           var d = new Date($.now());
           Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
           d1 = new Date($.now()).getTime();



           $("#dialog-Open").dialog({
               autoOpen: true,
               resizable: false,
               height: "auto",
               width: 1093,
               modal: true,
               dialogClass: "no-close",
               buttons: {
                   Close: function () {
                       $(this).dialog("destroy");
                       var d = new Date($.now());
                       var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                       d2 = new Date($.now()).getTime();
                       var Diff = Math.floor((d2 - d1) / 1000);
                       //alert(Diff);
                       if (Diff >= 60) {
                           fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                       }

                   }
               }
           });
       }


   </script>
    <script type="text/javascript">
        var Itemcode = '';
        $(document).ready(function () {
            fncGetItemname();
        });

        function pageLoad() {
            try {
                $('#navigation').hide();
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkSave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkSave.ClientID %>').css("display", "none");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncInitLoad() {

            try {
                fncBatchDetail($('#<%=txtItemcode.ClientID %>').val());
                Itemcode = $('#<%=txtItemcode.ClientID %>').val();
            }
            catch (err) {
                ShowPopupMessageBox(err.message());
            }
        }

        //------------------------------------------- Get Item Name ----------------------------------------//

        function fncGetItemname() {
            try {
                $("[id$=txtDesc]").autocomplete({
                    source: function (request, response) {
                        var obj = {};
                        if ($('#<%=cbshowDeActiveBatch.ClientID %>').is(":checked")) {
                            obj.mode = "DeActiveSearch";
                        }
                        else {
                            obj.mode = "ActiveSearch";
                        }

                        obj.prefix = escape(request.term);
                        $.ajax({
                            url: '<%=ResolveUrl("frmBatchDeactivation.aspx/GetFilterValue") %>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('/')[0],
                                        val: item.split('/')[1],
                                        desc: item.split('/')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.responseText);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtDesc.ClientID %>').val(i.item.desc);
                        event.preventDefault();
                    },
                    //==========================> After Select Value
                    select: function (e, i) {
                        $('#<%=txtItemcode.ClientID %>').val(i.item.val);
                        $('#<%=txtDesc.ClientID %>').val(i.item.desc);
                        fncBatchDetail(i.item.val)
                        return false;
                    },

                    minLength: 2
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBatchDetail(Itemcode) {
            var obj = {};
            try {
                if ($('#<%=cbshowDeActiveBatch.ClientID %>').is(":checked")) {
                    obj.mode = "GetDeActive";
                }
                else {
                    obj.mode = "GetActive";
                }
                obj.Itemcode = escape(Itemcode);
                $.ajax({
                    url: '<%=ResolveUrl("frmBatchDeactivation.aspx/fncGetBatchDetail") %>',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        fncBindBatchtable(jQuery.parseJSON(msg.d), '1');
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.responseText);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncBindBatchtable(tblObj, mode) {
            var tblBody, row;
            if (mode == '2') {
                tblObj = jQuery.parseJSON(tblObj);
            }
            try {
                tblBody = $("#tblBatchActivation tbody");
                tblBody.children().remove();
                if (tblObj.length > 0) {
                    $('#<%=txtDesc.ClientID %>').val(tblObj[0]["Description"]);
                    for (var i = 0; i < tblObj.length; i++) {
                        row = "<tr>"
                    + "<td id='tdRowNo_" + i + "' >" + tblObj[i]["RowNo"] + "</td>"
                    + "<td id='tdItemcode_" + i + "' >" + tblObj[i]["InventoryCode"] + "</td>"
                    + "<td id='tdDesc_" + i + "' >" + tblObj[i]["Description"] + "</td>"
                    + " <td id='tdBatchNo_" + i + "' >" + tblObj[i]["BatchNo"] + "</td>"
                    + " <td id='tdMRP_" + i + "' >" + tblObj[i]["MRP"].toFixed() + "</td>"
                    + " <td id='tdBalQty_" + i + "' >" + tblObj[i]["BalanceQty"].toFixed() + "</td>"
                    + "<td><input id='cbSelect_" + i + "' type='checkbox' onclick='fncrptrRowClick(this);' /></td>"
                    + "</tr>";
                        tblBody.append(row);
                    }
                    $("#tblBatchActivation [id*=cbAllSelect]").focus();
                }
                else {
                    setTimeout(function () {
                        popUpObjectForSetFocusandOpen = $('#<%=txtDesc.ClientID %>');
                        ShowPopupMessageBoxandFocustoObject("Record not found");
                    });
                    fncClear();
                }
                if ($('#<%=hidSelect.ClientID %>').val() != "") {
                    fncSelectCheckbox();
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSelectCheckbox() {
            try {
                var tblBody = $("#tblBatchActivation");
                var select = {};
                var hidSplit = "";
                hidSplit = $('#<%=hidSelect.ClientID %>').val();
            select = hidSplit.split(',');
            for (var i = 0; i < select.length; i++) {
                var MyRows = $('table#tblBatchActivation').find('tbody').find('tr');
                for (var j = 0; j < MyRows.length; j++) {
                    var MyIndexValue = $(MyRows[j]).find('td:eq(3)').html();
                    if (select[i] == MyIndexValue)
                        $(MyRows[j]).find('td input[id*=cbSelect]').prop("checked", true);
                }
            }
            $("#tblBatchActivation [id*=cbAllSelect]").focus();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    //Header Row Click
    function fncrptrHeaderclick(source) {
        try {

            if (($(source).is(":checked"))) {
                $("#tblBatchActivation tbody").children().each(function () {
                    $(this).find('td [id*=cbSelect]').attr("checked", "checked");
                    var rowObj = $(this).find('td [id*=cbSelect]');
                    fncrptrRowClick(rowObj);
                });
            }
            else {
                $("#tblBatchActivation [id*=cbSelect]").removeAttr("checked");
            }

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Repeater Row Click
    function fncrptrRowClick(source) {
        try {

            if (($(source).is(":checked"))) {
                var rowobj = $(source).parent().parent();

                //var qty = rowobj.find('td[id*=tdBalQty]').text();

                //if (parseFloat(qty) != 0) {
                //    popUpObjectForSetFocusandOpen = rowobj;
                //    ShowPopupMessageBoxandFocustoObject("Zero stock batches only can be deactivated.Please update these batches to zero.");
                //    $(source).removeAttr("checked");
                //    $("#tblBatchActivation [id*=cbAllSelect]").removeAttr("checked");
                //}
                //else
                if ($("#tblBatchActivation tbody").children().length == $("#tblBatchActivation [id*=cbSelect]:checked").length) {
                    $("#tblBatchActivation [id*=cbAllSelect]").attr("checked", "checked");
                }
                else if ($("#tblBatchActivation tbody").children().length != $("#tblBatchActivation [id*=cbSelect]:checked").length) {
                    $("#tblBatchActivation [id*=cbAllSelect]").removeAttr("checked");
                }
            }
            else if ($("#tblBatchActivation tbody").children().length != $("#tblBatchActivation [id*=cbSelect]:checked").length) {
                $("#tblBatchActivation [id*=cbAllSelect]").removeAttr("checked");
            }

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
            console.log(err);
        }
    }

    function fncClearAll() {
        try {
            $('#<%=txtItemcode.ClientID %>').val('');
                $('#<%=txtDesc.ClientID %>').val('');
                $("#tblBatchActivation [id*=cbAllSelect]").removeAttr("checked");
                fncClear();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncClear() {
            try {
                $("#tblBatchActivation tbody").children().remove();
                $('#<%=txtDesc.ClientID %>').focus();
                                    $('#<%=cbshowDeActiveBatch.ClientID %>').removeAttr("checked");
                                    return false;
                                }
                                catch (err) {
                                    ShowPopupMessageBox(err.message);
                                }
                            }




                            /// To Form JSON Format;
                            function fncToFormJSONFormat() {
                                var jsonObj = "", mode, status = false;;

                                try {
                                    if ($('#<%=cbshowDeActiveBatch.ClientID %>').is(":checked")) {
                            mode = "1";
                        }
                        else {
                            mode = "1";
                        }

                        if ($("#tblBatchActivation tbody").children().length > 0) {//surya280122 Replacing Double Quotes
                            $("#tblBatchActivation tbody").children().each(function () {
                                if ($(this).find('td input[id*=cbSelect]').is(":checked")) {
                                    jsonObj = jsonObj + "{\"BatchNo\":" + "\"" + $(this).find('td[id*=tdBatchNo]').text().trim() + "\",";
                                    jsonObj = jsonObj + "\"InventoryCode\":" + "\"" + $(this).find('td[id*=tdItemcode]').text().trim() + "\",";
                                    jsonObj = jsonObj + "\"Description\":" + "\"" + $(this).find('td[id*=tdDesc]').text().trim().replace(/"/g, '') + "\",";
                                    jsonObj = jsonObj + "\"BalanceQty\":" + "\"" + $(this).find('td[id*=tdBalQty]').text().trim() + "\",";
                                    jsonObj = jsonObj + "\"MRP\":" + "\"" + $(this).find('td[id*=tdMRP]').text().trim() + "\",";
                                    jsonObj = jsonObj + "\"Value\":" + "\"" + mode + "\"},";
                                }
                            });

                            jsonObj = jsonObj.substr(0, jsonObj.length - 1);

                            $('#<%=hidBatchDetail.ClientID %>').val(jsonObj);
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        }

                        status = true;
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }

                    return status;
                }

                function fncSaveSuccessMassage() {
                    try {
                      if ($('#<%=txtItemcode.ClientID %>').val() == "")
                            $('#<%=txtItemcode.ClientID %>').val(Itemcode);
                         popUpObjectForSetFocusandOpen = $('#<%=txtDesc.ClientID %>');
                       // fncToastError("Updated successfully");
                       Itemcode = '';
                       fncClear();
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                function fncChangeSelectMode() {
                    try {
                        if ($('#<%=cbshowDeActiveBatch.ClientID %>').is(":checked")) {
                            $('#<%=lnkSave.ClientID %>').text('Activate Batch(F4)');
                            fncBatchDetail($('#<%=txtItemcode.ClientID %>').val());
                        }
                        else {
                            $('#<%=lnkSave.ClientID %>').text('DeActivate Batch(F4)');
                            fncBatchDetail($('#<%=txtItemcode.ClientID %>').val());
                        }
                    }
                    catch (err) {
                        ShowPopupMessageBox(err.message);
                    }
                }

                function fncItemcodekeydown(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    try {
                        if (charCode == 13) {
                            fncBatchDetail($('#<%=txtItemcode.ClientID %>').val());
                    fncChangeSelectMode();
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncYesClick_master() {
            try {
                $("#saveConfirmation_master").dialog("destroy");
                fncToFormJSONFormat();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncCallSavefunction() {
            try {
                if ($("#tblBatchActivation tbody").children().length == $("#tblBatchActivation [id*=cbSelect]:checked").length) {
                    if ($('#<%=cbshowDeActiveBatch.ClientID %>').is(":checked")) {
                        fncShowSaveConfirmation_master("You have selected all Batches of this Item to Activate. Press YES to Confirm");
                    }
                    else {
                        fncShowSaveConfirmation_master("You have selected all Batches of this Item to Activate. Press YES to Confirm");
                    }
                }
                else if ($("#tblBatchActivation [id*=cbSelect]:checked").length > 0) {
                    fncToFormJSONFormat();
                }
                else {
                    popUpObjectForSetFocusandOpen = $("#tblBatchActivation [id*=cbAllSelect]");
                    ShowPopupMessageBoxandFocustoObject("select any row");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {

                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if ($('#<%=lnkSave.ClientID %>').is(":visible"))
                            fncCallSavefunction();
                        e.preventDefault();
                    }
                    else if (e.keyCode == 117) {
                        fncClearAll();
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 'auto',
                    width: 'auto',
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Merchandising/frmPriceChangeBatch.aspx">Price Change Batch
                </a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Batch Deactivation</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div>
            <div class="batchDeactivation_lbl">
                <asp:Label ID="lblItemcode" runat="server" Text="Item Code"></asp:Label>
            </div>
            <div class="batchDeactivation_lbl">
                <asp:TextBox ID="txtItemcode" runat="server" class="form-control-res" onkeydown=" return fncItemcodekeydown(event);"></asp:TextBox>
            </div>
            <div class="batchDeactivation_lbl">
                <asp:Label ID="lblDesc" runat="server" Text="Description"></asp:Label>
            </div>
            <div class="batchDeactivation_lbl">
                <asp:TextBox ID="txtDesc" runat="server" class="form-control-res"></asp:TextBox>
            </div>
            <div class="batchDeactivation_lbl">
                <asp:CheckBox ID="cbshowDeActiveBatch" runat="server" onchange=" return fncChangeSelectMode();" Text="Show the DeActive Batch" />
            </div>
        </div>
        <div class="Payment_fixed_headers batchDeactivation_tbl">
            <table id="tblBatchActivation" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">Item Code
                        </th>
                        <th scope="col">Description
                        </th>
                        <th scope="col">Batch No
                        </th>
                        <th scope="col">MRP
                        </th>
                        <th scope="col">TotalQty
                        </th>
                        <th scope="col">
                            <input id="cbAllSelect" type="checkbox" onclick="fncrptrHeaderclick(this);" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div>
            <div class="batchDeactivation_lbl">
                <asp:LinkButton ID="lnkSave" runat="server" Text="Deactivate Batch(F4)" class="button-blue" OnClick="lnkSave_click" OnClientClick="fncCallSavefunction();return false;"></asp:LinkButton>
            </div>
            <div class="batchDeactivation_lbl">
                <asp:LinkButton ID="lnkClear" runat="server" Text="Clear(F6)" class="button-blue" OnClientClick=" return fncClearAll();"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidBatchDetail" runat="server" />

        <%--   <div id="saveConfirmation_master" class="display_none">
            <div>
                <asp:Label ID="lblSavemsg" runat="server"></asp:Label>
            </div>
            <div class="float_right">
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkNewItemYes" runat="server" Text="Yes" class="button-blue-withoutanim" OnClientClick="fncYesClick();return false;"></asp:LinkButton>
                </div>
                <div class="inv_dailog_btn">
                    <asp:LinkButton ID="lnkNewItemNo" runat="server" Text="No" class="button-blue-withoutanim" OnClientClick="fncNoClick();return false;"></asp:LinkButton>
                </div>
            </div>
        </div>--%>
        <div id="ConfirmSaveDialog">
            <p>
                Total Quantity Must Be Zero.Do you want Update?
            </p>
            <div style="width: 150px; margin: auto">
                <div style="float: left">
                    <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                        Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                </div>
                <div style="float: right">
                    <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                        Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidSelect" runat="server" Value="" />
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
