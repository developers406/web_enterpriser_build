﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmBulkBatchDeactivation.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmBulkBatchDeactivation" %>

<%@ Register TagPrefix="ups" TagName="SearchFilterUserControl" Src="~/UserControls/SearchFilterUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 190px;
            max-width: 950px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 277px;
            max-width: 277px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 130px;
            max-width: 130px;
            text-align: right;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 130px;
            max-width: 130px;
            text-align: right;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 115px;
            max-width: 115px;
            text-align: center;
        }

        /*.grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
          
        }*/
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BulkBatchDeactivation');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BulkBatchDeactivation";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        var itmcode;
        var vendor;
        var dept;
        var cat;
        var sub;
        var brand;
        var shelf;
        var floor;
        var merchendise;
        var manufacture;
        var secton;
        var bin;
        var barcode;
        var batchno;
        var grnno;
        var row;
        var selectedValue;

        function pageLoad() {
            try {
                if (InventoryHistory == "T") {
                    InventoryHistory = "F";
                    $('#navigation').hide();
                }
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkDeactive.ClientID %>').css("display", "block");
                    $('#<%=lnkActive.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkDeactive.ClientID %>').css("display", "none");
                    $('#<%=lnkActive.ClientID %>').css("display", "none");
                }
                $("select").chosen({ width: '100%' });
                SetQualifyingItemAutoComplete();
                $('#divActive').hide();
                $("#<%=txtRetain.ClientID%>").val('1');
                if ($("#<%=hidStockUpdate.ClientID%>").val() != "") {
                    if ($("#<%=hidStockUpdate.ClientID%>").val() == "D") {
                        $("#ContentPlaceHolder1_lnkDeactive").css("display", "block");
                        $("#ContentPlaceHolder1_lnkActive").css("display", "none");
                    }
                    else {
                        $("#ContentPlaceHolder1_lnkDeactive").css("display", "none");
                        $("#ContentPlaceHolder1_lnkActive").css("display", "block");
                    }
                }
                if($("#<%=rdActivation.ClientID %> input:checked").val() == 'Deactive') {
                    $("#ContentPlaceHolder1_lnkDeactive").css("display", "block");
                    $("#ContentPlaceHolder1_lnkActive").css("display", "none");
                }
                else if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Active') {
                    $("#ContentPlaceHolder1_lnkDeactive").css("display", "none");
                    $("#ContentPlaceHolder1_lnkActive").css("display", "block");
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=hidVendor.ClientID %>').val(Code);
                    $('#<%=txtVendor.ClientID %>').val(Description);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function SetQualifyingItemAutoComplete() {

            try {
                $("#<%= txtItemCode.ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {

                        $("#<%= txtItemCode.ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= txtItemName.ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= txtItemCode.ClientID %>").val($.trim(i.item.valitemcode));
                        $("#<%= txtItemName.ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });

                $("#<%= txtItemName.ClientID %>").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1],
                                        valName: item.split('|')[2]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    open: function (event, ui) {
                        var $input = $(event.target);
                        var $results = $input.autocomplete("widget");
                        var scrollTop = $(window).scrollTop();
                        var top = $results.position().top;
                        var height = $results.outerHeight();
                        if (top + height > $(window).innerHeight() + scrollTop) {
                            newTop = top - height - $input.outerHeight();
                            if (newTop > scrollTop)
                                $results.css("top", newTop + "px");
                        }
                    },
                    focus: function (event, i) {

                        $("#<%= txtItemName.ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>
                        event.preventDefault();
                    },
                    select: function (e, i) {

                        $("#<%= txtItemName.ClientID %>").val($.trim(i.item.valName));
                        <%--$("#<%=txtDescription.ClientID%>").val(i.item.valName);--%>

                        return false;
                    },
                    minLength: 1
                });
                $(window).scroll(function (event) {
                    $('.ui-autocomplete.ui-menu').position({
                        my: 'left bottom',
                        at: 'left top',
                        of: '#ContentPlaceHolder1_searchFilterUserControl_txtItemCode'
                    });
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncFetchValidation() {
            itmcode = $("#<%= txtItemCode.ClientID %>").val()
            barcode = $("#<%= txtBarcode.ClientID %>").val();
            grnno = $("#<%= txtGRNNo.ClientID %>").val();
            batchno = $("#<%= txtBatchNo.ClientID %>").val();
            vendor = $("#<%= hidVendor.ClientID %>").val();
            dept = $("#<%= txtDepartment.ClientID %>").val();
            cat = $("#<%= txtCategory.ClientID %>").val();
            sub = $("#<%= txtSubCategory.ClientID %>").val();
            brand = $("#<%= txtBrand.ClientID %>").val();
            floor = $("#<%= txtFloor.ClientID %>").val();
            shelf = $("#<%= txtShelf.ClientID %>").val();
            merchendise = $("#<%= txtMerchandise.ClientID %>").val();
            manufacture = $("#<%= txtManufacture.ClientID %>").val();
            secton = $("#<%= txtSection.ClientID %>").val();
            bin = $("#<%= txtBin.ClientID %>").val();
            try {
                if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Deactive') {
                    if (parseFloat($("#<%=txtRetain.ClientID%>").val()) == 0 || $("#<%=txtRetain.ClientID%>").val() == "") {
                        ShowPopupMessageBox("Please Enter Atleast '1' in Retain");
                        return false;
                    }
                }
                else if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Active') {
                    if ($("#<%=txtActiveRetain.ClientID%>").val() <= 0 || $("#<%=txtActiveRetain.ClientID%>").val() == "") {
                        ShowPopupMessageBox("Please Enter Activate Batches Count");
                        return false;
                    }
                }
            if ($("#<%=hidStockUpdate.ClientID%>").val() == "") {
                    if ((itmcode == "") && (barcode == "") && (grnno == "") && (batchno == "") && (vendor == "") && (dept == "") && (cat == "")
                        && (sub == "") && (brand == "") && (floor == "") && (shelf == "") && (merchendise == "") && (manufacture == "")
                        && (secton == "") && (bin == "")) {
                        ShowPopupMessageBox("Please select Atleast one Filter");
                        return false;
                    }
                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $("[id*=rdActivation] input").live("click", function () {

            selectedValue = $(this).val();
            var selectedText = $(this).next().html();
            if (selectedValue == "Deactive") {
                $("#ContentPlaceHolder1_lnkDeactive").css("display", "block");
                $("#ContentPlaceHolder1_lnkActive").css("display", "none");
                $('#divDeactive').show();
                $('#divActive').hide();
            }
            else if (selectedValue == "Active") {
                $("#ContentPlaceHolder1_lnkDeactive").css("display", "none");
                $("#ContentPlaceHolder1_lnkActive").css("display", "block");
                $('#divActive').show();
                $('#divDeactive').hide();
            }
        });

        function fncSelectAllClick(source) {
            var totalqty;
            //  
            try {
                if ($(source).is(":checked")) {
                    $("#ContentPlaceHolder1_gvBulkDeactivation tbody tr").find('td input[id*="cbSelect"]').prop("checked", true);
                }
                else {
                    $("#ContentPlaceHolder1_gvBulkDeactivation tbody tr").find('td input[id*="cbSelect"]').prop("checked", false);
                }
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function clearForm() {
            // $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("input").removeAttr('disabled');
            $("#ContentPlaceHolder1_rdActivation_0").prop('disabled', false);
            $("#ContentPlaceHolder1_rdActivation_1").prop('disabled', false);
            $("#<%=rdActivation.ClientID%>").prop("checked", true);
            $("#<%= txtItemCode.ClientID %>").val('')
            $("#<%= txtBarcode.ClientID %>").val('');
            $("#<%= txtGRNNo.ClientID %>").val('');
            $("#<%= txtBatchNo.ClientID %>").val('');
            $("#<%= txtVendor.ClientID %>").val('');
            $("#<%= txtDepartment.ClientID %>").val('');
            $("#<%= txtCategory.ClientID %>").val('');
            $("#<%= txtSubCategory.ClientID %>").val('');
            $("#<%= txtBrand.ClientID %>").val('');
            $("#<%= txtFloor.ClientID %>").val('');
            $("#<%= txtShelf.ClientID %>").val('');
            $("#<%= txtMerchandise.ClientID %>").val('');
            $("#<%= txtManufacture.ClientID %>").val('');
            $("#<%= txtSection.ClientID %>").val('');
            $("#<%= txtBin.ClientID %>").val('');
            $("select").chosen({ width: '100%' });
            return false;
        }


        function Clearall() {
            try {
                $("select").chosen({ width: '100%' });
                $("#<%=txtRetain.ClientID%>").val(1);
                $("#<%=gvBulkDeactivation.ClientID%>").remove();
                $("#ContentPlaceHolder1_rdActivation_0").prop('disabled', false);
                $("#ContentPlaceHolder1_rdActivation_1").prop('disabled', false);
                $("#<%=cbSelectAll.ClientID%>").prop("checked", false);
                $("#ContentPlaceHolder1_lnkDeactive").show();
                $("#ContentPlaceHolder1_lnkActive").hide();
                $("#<%=rdActivation.ClientID%>").prop("checked", true);
                $("#<%= txtItemCode.ClientID %>").val('')
                $("#<%= txtBarcode.ClientID %>").val('');
                $("#<%= txtGRNNo.ClientID %>").val('');
                $("#<%= txtBatchNo.ClientID %>").val('');
                $("#<%= txtVendor.ClientID %>").val('');
                $("#<%= txtDepartment.ClientID %>").val('');
                $("#<%= txtCategory.ClientID %>").val('');
                $("#<%= txtSubCategory.ClientID %>").val('');
                $("#<%= txtBrand.ClientID %>").val('');
                $("#<%= txtFloor.ClientID %>").val('');
                $("#<%= txtShelf.ClientID %>").val('');
                $("#<%= txtMerchandise.ClientID %>").val('');
                $("#<%= txtManufacture.ClientID %>").val('');
                $("#<%= txtSection.ClientID %>").val('');
                $("#<%= txtBin.ClientID %>").val('');
                clearForm();
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Toast(source) {
            if (source == "D") {
                clearForm();
                ShowPopupMessageBox("Deactivated Succssfully");
            }
            else if (source == "A") {
                clearForm();
                ShowPopupMessageBox("Activated Successfully");
            }
            else if (source == "W") {
                clearForm();
                ShowPopupMessageBox("Please Select Atleast one Row");
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncDeactivateValidation() {

            try {
                itmcode = $("#<%= txtItemCode.ClientID %>").val()
                barcode = $("#<%= txtBarcode.ClientID %>").val();
                grnno = $("#<%= txtGRNNo.ClientID %>").val();
                batchno = $("#<%= txtBatchNo.ClientID %>").val();
                vendor = $("#<%= hidVendor.ClientID %>").val();
                dept = $("#<%= txtDepartment.ClientID %>").val();
                cat = $("#<%= txtCategory.ClientID %>").val();
                sub = $("#<%= txtSubCategory.ClientID %>").val();
                brand = $("#<%= txtBrand.ClientID %>").val();
                floor = $("#<%= txtFloor.ClientID %>").val();
                shelf = $("#<%= txtShelf.ClientID %>").val();
                merchendise = $("#<%= txtMerchandise.ClientID %>").val();
                manufacture = $("#<%= txtManufacture.ClientID %>").val();
                secton = $("#<%= txtSection.ClientID %>").val();
                bin = $("#<%= txtBin.ClientID %>").val();
                if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Deactive') {
                    if ($("#<%=txtRetain.ClientID%>").val() == 1 || $("#<%=txtRetain.ClientID%>").val() == "") {
                        ShowPopupMessageBox("Please Enter Atleast '1' in Retain");
                        return false;
                    }
                }
                else if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Active') {
                    if ($("#<%=txtActiveRetain.ClientID%>").val() <= 0 || $("#<%=txtActiveRetain.ClientID%>").val() == "") {
                        ShowPopupMessageBox("Please Enter Activate Batches Count");
                        return false;
                    }
                }
            if ($("#<%=hidStockUpdate.ClientID%>").val() == "") {
                    if ((itmcode == "") && (barcode == "") && (grnno == "") && (batchno == "") && (vendor == "") && (dept == "") && (cat == "")
                        && (sub == "") && (brand == "") && (floor == "") && (shelf == "") && (merchendise == "") && (manufacture == "")
                        && (secton == "") && (bin == "")) {
                        ShowPopupMessageBox("Please select Atleast one Filter");
                        return false;
                    }
                } else
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkDeactive', '');
                return true;
            }

            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function disableFunctionKeys(e) {

            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 112) {
                    e.preventDefault();
                }
                if (e.keyCode == 115) {

                    if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Active') {
                        if ($('#<%=lnkActive.ClientID %>').is(":visible"))
                            $('#<%=lnkActive.ClientID %>').click();
                        e.preventDefault();
                    }
                    else if ($("#<%=rdActivation.ClientID %> input:checked").val() == 'Deactive') {
                        if ($('#<%=lnkDeactive.ClientID %>').is(":visible"))
                            $('#<%=lnkDeactive.ClientID %>').click();
                        e.preventDefault();
                    }

            }


            else if (e.keyCode == 117) {
                $('#<%=lnkClear.ClientID %>').click();
                e.preventDefault();
            }
    }
}
function fncShowConfirmSaveMessage() {
    try {
        InitializeDialogConfirmSave();
        $("#ConfirmSaveDialog").dialog('open');
    }
    catch (err) {
        alert(err.message);
        console.log(err);
    }
}
function InitializeDialogConfirmSave() {
    try {
        $("#ConfirmSaveDialog").dialog({
            autoOpen: false,
            resizable: false,
            height: 150,
            width: 370,
            modal: true
        });
    }
    catch (err) {
        alert(err.message);
    }
}
$(document).ready(function () {
    $(document).on('keydown', disableFunctionKeys);
});

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Merchandising/frmPriceChangeBatch.aspx">Price Change Batch</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Bulk Batch Deactivation</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="container-left-price" id="pnlFilter" runat="server">
                    <div class="control-group-price-header">
                        Filtertions
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label2" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtMerchandise');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label11" runat="server" Text="SubCategory"></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchandise');"></asp:TextBox>
                        </div>
                    </div>

                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchandise', 'txtManufacture');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                            <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtBarcode');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcode %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-single-res">
                        <div class="label-left">
                            <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
                        </div>
                        <div class="label-right">
                            <asp:TextBox ID="txtGRNNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>

                    <div style="width: 100%">

                        <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                            <div class="control-button">
                                <div class="label-right" style="width: 100%">
                                    <asp:LinkButton ID="lnkFetch" runat="server" class="button-blue" OnClick="lnkFetch_Click" OnClientClick="return fncFetchValidation();"><i class="icon-play"></i>Fetch</asp:LinkButton>
                                </div>
                            </div>
                            <div class="control-button">
                                <div class="label-right" style="width: 100%">
                                    <asp:LinkButton ID="lnkclear1" runat="server" class="button-blue" OnClientClick="return Clearall();"><i class="icon-play"></i>Clear</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
            <ContentTemplate>
                <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                    <div class="grdLoad">
                        <table id="tblBulkBatch" cellspacing="0" rules="all" border="1" class="fixed_header">
                            <thead>
                                <tr>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">Batch No
                                    </th>
                                    <th scope="col">MRP
                                    </th>
                                    <th scope="col">TotalQty
                                    </th>
                                    <th scope="col">Select
                                    <asp:CheckBox ID="cbSelectAll" runat="server" onclick="fncSelectAllClick(this)" AutoPostBack="false" />
                                    </th>

                                </tr>
                            </thead>
                        </table>
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 350px; width: 990px; overflow-x: hidden; overflow-y: scroll;">
                            <asp:GridView ID="gvBulkDeactivation" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" ShowHeader="false" CssClass="pshro_GridDgn grdLoad">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="Bottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>

                                    <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="BalanceQty" HeaderText="BalanceQty"></asp:BoundField>
                                    <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                    <div class="container-group-small" style="margin-top: 10px; width: 200px;">
                        <div class="container-control">
                            <div class="control-group-split" style="width: 265px;">
                                <div class="control-group-left" style="width: 100%;" id="divDeactive">
                                    <div class="label-left" style="width: 75%;">
                                        <asp:Label ID="Label1" runat="server" Text="Latest Batches to be Retained" Font-Bold="true"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 25%;">
                                        <asp:TextBox ID="txtRetain" runat="server"
                                            CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group-left" id="divActive" style="width: 100%;">
                                    <div class="label-left" style="width: 75%;">
                                        <asp:Label ID="Label18" runat="server" Text="Show Latest Batches Count" Font-Bold="true"></asp:Label>
                                    </div>
                                    <div class="label-right" style="width: 25%;">
                                        <asp:TextBox ID="txtActiveRetain" runat="server"
                                            CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="margin-top: 5px">
                                <asp:RadioButtonList ID="rdActivation" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Deactive" Selected="True">Deactivate</asp:ListItem>
                                    <asp:ListItem style="margin-left: 71px" Value="Active">Activate</asp:ListItem>
                                </asp:RadioButtonList>
                                <%--  <div class="col-md-4">
                                    <asp:CheckBox ID="chDeactive" runat="server" Text="Deactive" />
                                </div>
                                <div class="col-md-4">
                                    <asp:CheckBox ID="chActive" runat="server" Text="Active" />--%>
                                <%--<div class="col-md-8">
                                    <asp:RadioButtonList ID="rdDiscount" runat="server" RepeatDirection="Horizontal" AutoPostBack="false">
                                        <asp:ListItem Value="SellingPrice" Selected="True">Selling Price</asp:ListItem>

                                        <asp:ListItem style="margin-left: 71px" Value="Mrp">MRP</asp:ListItem>

                                    </asp:RadioButtonList>
                                    <%--<asp:RadioButton ID="rdSellingPrice" runat="server" Text="Selling Price" checked="true" />  
                                    <asp:RadioButton ID="rdMRP" runat="server" Text="MRP" Style="margin-left:71px" />  --%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-group-full" style="display: none">
                    <div id="ConfirmSaveDialog">
                        <p>
                            Total Quantity Must Be Zero.Do you want to Update?
                        </p>
                        <div style="width: 150px; margin: auto">
                            <div style="float: left">
                                <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                                    Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                            </div>
                            <div style="float: right">
                                <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                                    Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
            <ContentTemplate>
                <div class="button-contol" style="margin-top: -50px">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkDeactive" runat="server" class="button-red"
                            OnClientClick="return fncDeactivateValidation()" OnClick="lnkDeActivate_Click">
                    <i class="icon-play"></i>Deactivate Batch(F4)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkActive" Style="display: none;" runat="server" class="button-red" OnClientClick="return fncDeactivateValidation()"
                            OnClick="lnkDeActivate_Click"><i class="icon-play"></i>Activate Batch(F4)</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return Clearall()"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:HiddenField ID="hidenCheckActive" runat="server" ClientIDMode="Static" Value="" />
        <asp:HiddenField ID="hidStockUpdate" runat="server" Value="" />
        <asp:HiddenField ID="hidVendor" runat="server" />
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>

