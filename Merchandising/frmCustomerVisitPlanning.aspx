﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmCustomerVisitPlanning.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmCustomerVisitPlanning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    body
    {
        font-family: Arial;
        font-size: 10pt;
    }
    .GridPager a, .GridPager span
    {
        display: block;
        height: 20px;
        width: 20px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
    }
    .GridPager a
    {
        background-color: #f5f5f5;
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }
</style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'CustomerVisitPlanning');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "CustomerVisitPlanning";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


    <script type="text/javascript">
        function pageLoad() {
            $("select").show().removeClass('chzn-done');
            $("select").next().remove();
            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");;
            $("[id$=txtCustomerName]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/Masters/frmCustomerMasterView.aspx/loadCustomer",
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('-')[0],
                                    val: item.split('-')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $('#<%=hdForCustCode.ClientID %>').val(i.item.val);
                    $("[id$=txtCustomerName]").val(i.item.label);
                },

                minLength: 1
            });
           
        }
         function imgbtnEdit_ClientClick(source) {

                DisplayDetails($(source).closest("tr"));

            }
         function DisplayDetails(row) {
            
             if ($("td", row).eq(9).html().substring(0, 10).replace(/&nbsp;/g, '') == 'CL')
             {
                 ShowPopupMessageBox("Status Closed");
                 return false;
             }
              var strarray = $("td", row).eq(4).html().substring(0, 10).replace(/&nbsp;/g, '').split(':');
              var tddate = $("td", row).eq(7).html().substring(0, 10).replace(/&nbsp;/g, '');
             
              var fromdate = new Date();
              var todate = new Date(tddate);
              if (todate > fromdate) {
                 
              } else {
                  ShowPopupMessageBox("Pervious Can't Be edited");
                  return false;
              }
              <%--$('#<%=hdForEdit.ClientID %>').val(($("td", row).eq(2).html()));--%>
              $('#<%=txtDate.ClientID %>').datepicker("setDate", $("td", row).eq(3).html().substring(0, 10).replace(/&nbsp;/g, '')).datepicker({ dateFormat: "dd/mm/yy" });
            $('#<%=ddlhour.ClientID %>').val(strarray[0]);
            $('#<%=ddlmin.ClientID %>').val(strarray[1]);
            $('#<%=txtCustomerName.ClientID %>').val($("td", row).eq(5).html().replace(/&nbsp;/g, ''));
            $('#<%=txtRemark.ClientID %>').val($("td", row).eq(6).html().replace(/&nbsp;/g, ''));
             
              $('#<%=hdStatus.ClientID %>').val($("td", row).eq(9).html().substring(0, 10).replace(/&nbsp;/g, '')); 
             $('#<%=hdForCustCode.ClientID %>').val($("td", row).eq(8).html().substring(0, 10).replace(/&nbsp;/g, '')); 
            
         }
        function clearForm_new() {
            $("#<%=txtCustomerName.ClientID %>").val("");
            $("#<%=txtRemark.ClientID %>").val("");
            $('#<%=hdForEdit.ClientID %>').val("");
            $('#<%=txtDate.ClientID %>').focus();
         }
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=ddlSalesMan.ClientID%>").value == 0) {
                Show = Show + 'Please Select Sales Man';
                document.getElementById("<%=ddlSalesMan.ClientID %>").focus();
            }

            if (document.getElementById("<%=txtCustomerName.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Customer Name';
                document.getElementById("<%=txtCustomerName.ClientID %>").focus();
            }
            if (document.getElementById("<%=txtRemark.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Remark';
                document.getElementById("<%=txtRemark.ClientID %>").focus();
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }

            else {
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Customer Visit Planning</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
       <asp:UpdatePanel ID="upfrom" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hdForEdit"  runat="server" />
                    <asp:HiddenField ID="hdForCustCode" Value="" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="hdStatus" Value="" ClientIDMode="Static" runat="server" />
                <div class="col-md-3">
                    <div class="control-group-single-res" style="padding: 9px;">
                        <div class="label-left">
                            <asp:Label ID="Label7" runat="server" Text="Sales Man Name"></asp:Label> 
                            
                        </div>
                        <div class="label-right">
                           
                            <asp:DropDownList ID="ddlSalesMan" AutoPostBack="True"   runat="server" OnSelectedIndexChanged="ddlSalesMan_SelectedIndexChanged" CssClass="form-control-res">
                               
                            </asp:DropDownList>
                            
                            
                               
                        </div>
                         
                    </div>

                </div>
                  <div class="col-md-3">
                    <div class="control-group-single-res" style="padding: 9px;">
                        <div class="label-left">
                          
                            
                        </div>
                        <div class="label-right">
                           
                           <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" Checked="true"  OnCheckedChanged="ChckedChanged" Text="Today"/>  
                            
                               
                        </div>
                         
                    </div>

                </div>

                <%-- <div class="col-md-3">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkNew" runat="server" class="button-red">New</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkEdit" runat="server" class="button-red" OnClientClick="clearForm()">Edit</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClose" runat="server" Style="display: none" class="button-red" OnClientClick="Close()"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                        </div>
                    </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="upgrid" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>


                
                    <div class="GridDetails">
                        <div class="row">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div class="grdLoad">
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: hidden; height: 320px; width: 1313px; background-color: aliceblue;">
                                        <asp:GridView ID="gvCustomerVisitPlanning" runat="server" AutoGenerateColumns="False" style="height:auto" ShowHeaderWhenEmpty="true" ShowHeader="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="10"
                                            oncopy="return false" DataKeyNames="CustomerCode" CssClass="pshro_GridDgn grdLoad " AllowPaging="true">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Record"></asp:Label>
                                            </EmptyDataTemplate>
                                            <PagerStyle CssClass = "GridPager" />
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                            <Columns>
                                                 <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgbtnEdit" runat="server" OnClientClick="imgbtnEdit_ClientClick(this);return false;"
                                                                ImageUrl="~/images/edit-icon.png" ToolTip="Click here to edit" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                 <asp:BoundField DataField="RowNumber" HeaderText="SNo"></asp:BoundField>
                                                <asp:BoundField DataField="SNo" HeaderText="SNo" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                <asp:BoundField DataField="VisitDate" HeaderText="Schedule Date"></asp:BoundField>
                                                <asp:BoundField DataField="VisitTime" HeaderText="Schedule Time"></asp:BoundField>
                                                <asp:BoundField DataField="CustomerName" HeaderText="Customer"></asp:BoundField>
                                                <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                                <asp:BoundField DataField="tempVisitDate" HeaderText="tempVisitDate" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                <asp:BoundField DataField="CustomerCode" HeaderText="CustomerName" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                <div class="container-group-small" style="width: 100%">
                    <div class="container-control">

                        <div class="col-md-2">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Date"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Time"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right" style="padding-top: 2px;">
                                <div class="col-md-6">
                                <asp:DropDownList ID="ddlhour" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="07">07</asp:ListItem>
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">13</asp:ListItem>
                                    <asp:ListItem Value="14">14</asp:ListItem>
                                    <asp:ListItem Value="15">15</asp:ListItem>
                                    <asp:ListItem Value="16">16</asp:ListItem>
                                    <asp:ListItem Value="17">17</asp:ListItem>
                                    <asp:ListItem Value="18">18</asp:ListItem>
                                    <asp:ListItem Value="19">19</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="21">21</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                                 <div class="col-md-6">
                                <asp:DropDownList ID="ddlmin" runat="server" CssClass="form-control-res">
                                    <asp:ListItem Value="00">00</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="15">15</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="30">30</asp:ListItem>
                                    <asp:ListItem Value="40">40</asp:ListItem>
                                    <asp:ListItem Value="45">45</asp:ListItem>
                                    <asp:ListItem Value="50">50</asp:ListItem>
                                    <asp:ListItem Value="55">55</asp:ListItem>
                                </asp:DropDownList>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Customer Name"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Remark"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="button-contol" style="margin-left: 175px">

                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm_new()"><i class="icon-play"></i>Clear</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" OnClientClick="return ValidateForm()" class="button-red"><i class="icon-play"></i>Add</asp:LinkButton>
                        </div>
                    </div>
                </div>
                </div>
                <div class="hiddencol">
                    
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
