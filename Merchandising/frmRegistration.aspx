﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmRegistration.aspx.cs" EnableEventValidation="false" Inherits="EnterpriserWebFinal.Merchandising.frmRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        

    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';


            if (document.getElementById("<%=txtCORPID.ClientID%>").value == "") {
                Show = Show + 'Please Enter CORPID';
            }
            if (document.getElementById("<%=txtUSERID.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter USERID';
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                //__doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                return true;
            }
        }

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':input').attr('disabled', false);
            $("#<%=txtAGGRID.ClientID %>").focus();
            return false;
        }

        function Toast(val) {
            ShowPopupMessageBox(val.slice(0, -1));
            clearForm();
            return false;
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible")) {
                        if (ValidateForm() == true) {
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        }
                    }
                    e.preventDefault();
                }
                else if (e.keyCode == 117) {
                    $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function pageLoad() {
            try{
                $('#<%= lnkSelfApproval.ClientID %>').css('visibility', 'visible');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSelfApproval() {

            var url = "https://cibnext.icicibank.com/corp/AuthenticationController?FORMSGROUP_ID__=AuthenticationFG&__START_TRAN_FLAG__=Y&FG_BUTTONS__=LOAD&ACTION.LOAD=Y&AuthenticationFG.LOGIN_FLAG=1&BANK_ID=ICI&LANGUAGE_ID=001&AGGR_ID=" + $('#<%= hidaggrId.ClientID %>').val().trim() + "&USER_ID=&ORG_ID="; 
            window.open(url, '_blank'); 
        }
    </script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'Registration');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "Registration";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">ICICI</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Registration</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="container-group-small" style="margin-top: 10px;">
            <div class="whole-price-header">
                Registration
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <img src="../images/icici_bank _logo_.jpg" style="width: 100%;" /><%--style ="background-image: url('../images/icici_bank_logo.jpg');"--%>
                </div>
                <div class="col-md-8"></div>
            </div>
            <div class="container-control">
                <div class="control-group-single-res display_none">
                    <div class="label-left">
                        <asp:HiddenField ID="hdfdstatus" runat="server" />
                        <asp:Label ID="Label1" runat="server" Text="AGGRID"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtAGGRID" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res">
                    <div class="label-left">
                        <asp:Label ID="Label2" runat="server" Text="CORPID"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtCORPID" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res">
                    <div class="label-left">
                        <asp:Label ID="Label3" runat="server" Text="USERID"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtUSERID" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res display_none">
                    <div class="label-left">
                        <asp:Label ID="Label4" runat="server" Text="AGGRNAME"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtAGGRNAME" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res display_none">
                    <div class="label-left">
                        <asp:Label ID="Label7" runat="server" Text="URN"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtURN" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res">
                    <div class="label-left">
                        <asp:Label ID="Label5" runat="server" Text="ALIASID"></asp:Label>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtAliasId" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div class="button-contol">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                        OnClientClick="return ValidateForm();"><i class="icon-play"></i>Register(F4)</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkSelfApproval" runat="server" class="button-red" OnClientClick ="fncSelfApproval();return false;" ><i class="icon-play"></i>SelfApprove</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm();"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID ="hidaggrId" runat ="server" Value ="" />
</asp:Content>
