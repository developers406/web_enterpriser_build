﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLinkAccount.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmLinkAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 70px;
            max-width: 70px;
            text-align: center !important;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 200px;
            max-width: 200px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 160px;
            max-width: 160px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
          display:none;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 160px;
            max-width: 160px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 160px;
            max-width: 160px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            display:none;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 120px;
            max-width: 120px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 120px;
            max-width: 120px;
            text-align: center !important;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'LinkAccount');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "LinkAccount";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtAGGRID.ClientID%>").value == "") {
                Show = Show + 'Please Enter AGGRID';
            }
            if (document.getElementById("<%=txtCORPID.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter CORPID';
            }
            if (document.getElementById("<%=txtURN.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter URN';
            }
            if (document.getElementById("<%=txtUSERID.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter USERID';
            }
            if (document.getElementById("<%=txtAccountNo.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter ACCOUNT NUMBER';
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                return true;
            }
        }

        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $(':input').attr('disabled', false);
            $(':input').attr('checked', false);
            $("#<%=txtAGGRID.ClientID %>").focus();
            return false;
        }

        function Toast(val) { 
            ShowPopupMessageBox(val);  
            clearForm();
            return false;
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible")) {
                         if (ValidateForm() == true) {
                             __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                         }
                     }
                     e.preventDefault();
                 }
                 else if (e.keyCode == 117) {
                     $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function imgbtnEdit_ClientClick(source) {
            //alert('test');
            if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                 ShowPopupMessageBox("You have no permission to Edit this Account");
                 return false;
             }
             clearForm();
             DisplayDetails($(source).closest("tr"));
        }

        function DisplayDetails(row) {  
            $('#<%=txtAGGRID.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtAccountNo.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtCORPID.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtURN.ClientID %>').attr("disabled", "disabled");
            $('#<%=txtUSERID.ClientID %>').attr("disabled", "disabled"); 
            $('#<%=hidEditValue.ClientID %>').val('1');

                    $('#<%=txtAGGRID.ClientID %>').val(($("td", row).eq(3).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtAccountNo.ClientID %>').val(($("td", row).eq(2).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtCORPID.ClientID %>').val(($("td", row).eq(4).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtURN.ClientID %>').val(($("td", row).eq(6).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
                    $('#<%=txtUSERID.ClientID %>').val(($("td", row).eq(5).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&'));
             
            if (($("td", row).eq(8).html().replace(/&nbsp;/g, '')).replace(/&amp;/g, '&') == "1")
                $('#<%=chkPrimary.ClientID %>').attr('checked', true);
            else
                $('#<%=chkPrimary.ClientID %>').attr('checked', false);

                }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">ICICI</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Link Account</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <asp:UpdatePanel ID="updtPnlgrdgrdDepartmentList" UpdateMode="Conditional" runat="Server">
            <ContentTemplate>
                <div class="container-group-small" style="margin-top: 10px;">
                    <div class="whole-price-header">
                        Link Account
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <img src="../images/icici_bank _logo_.jpg" style="width: 100%;" /><%--style ="background-image: url('../images/icici_bank_logo.jpg');"--%>
                        </div>
                        <div class="col-md-8"></div>
                    </div>
                    <div class="container-control">
                        <div class="control-group-single-res display_none">
                            <div class="label-left"> 
                                <asp:Label ID="Label1" runat="server" Text="AGGRID"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAGGRID" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" Text="CORPID"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCORPID" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="USERID"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtUSERID" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res display_none">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="URN"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtURN" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="ACCOUNT NUMBER"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtAccountNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:Label ID="Label5" runat="server" Text="PRIMARY"></asp:Label><span class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:CheckBox ID="chkPrimary" runat="server"></asp:CheckBox>
                            </div>
                        </div>
                    </div>

                    <div class="button-contol">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkSave_Click"
                                OnClientClick="return ValidateForm();"><i class="icon-play"></i>Link Account(F4)</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm();"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="grdLoad">
                        <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                            <thead>
                                <tr>
                                    <th scope="col">Edit
                                    </th>
                                    <th scope="col">Bank Name 
                                    </th>
                                    <th scope="col">Account Number
                                    </th>
                                    <th scope="col">Aggr Id
                                    </th>
                                    <th scope="col">Corp Id
                                    </th>
                                    <th scope="col">User Id 
                                    </th>
                                    <th scope="col">Urn
                                    </th>
                                    <th scope="col">Create Date
                                    </th>
                                    <th scope="col">Primary
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1010px; background-color: aliceblue;">
                            <asp:GridView ID="grdStatement" runat="server" AllowSorting="true" ShowHeaderWhenEmpty="true" oncopy="return false"
                                AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClientClick="imgbtnEdit_ClientClick(this);return false;" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidEditValue" runat="server" Value ="" />
    </div>
</asp:Content>
