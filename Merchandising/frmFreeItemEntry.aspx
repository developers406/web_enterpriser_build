﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmFreeItemEntry.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmFreeItemEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
           .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .qualifyingItem td:nth-child(1), .qualifyingItem th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .qualifyingItem td:nth-child(2), .qualifyingItem th:nth-child(2) {
            min-width: 45px;
            max-width: 45px;
        }

        .qualifyingItem td:nth-child(3), .qualifyingItem th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .qualifyingItem td:nth-child(4), .qualifyingItem th:nth-child(4) {
            min-width: 258px;
            max-width: 258px;
        }

        .qualifyingItem td:nth-child(5), .qualifyingItem th:nth-child(5) {
            min-width: 55px;
            max-width: 55px;
        }

        .qualifyingItem td:nth-child(6), .qualifyingItem th:nth-child(6) {
            min-width: 50px;
            max-width: 50px;
        }


        .freeItem td:nth-child(1), .freeItem th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .freeItem td:nth-child(2), .freeItem th:nth-child(2) {
            min-width: 45px;
            max-width: 45px;
        }

        .freeItem td:nth-child(3), .freeItem th:nth-child(3) {
            min-width: 80px;
            max-width: 80px;
        }

        .freeItem td:nth-child(4), .freeItem th:nth-child(4) {
            min-width: 297px;
            max-width: 297px;
        }

        .freeItem td:nth-child(5), .freeItem th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .freeItem td:nth-child(5), .freeItem th:nth-child(5) {
            min-width: 55px;
            max-width: 55px;
        }

        .freeItem td:nth-child(6), .freeItem th:nth-child(6) {
            min-width: 55px;
            max-width: 55px;
        }

        .freeItem td:nth-child(7), .freeItem th:nth-child(7) {
            min-width: 55px;
            max-width: 55px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'FreeItemEntry');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "FreeItemEntry";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>
    <script type="text/javascript">
        var QualifyingLastRowNo, freeItemLastRowNo, mode;
        function pageLoad() {
            $("#<%=txtGroupCode.ClientID %>").focus();
            mode = "";
            QualifyingLastRowNo = 0;
            freeItemLastRowNo = 0;

            $("#<%=txtfrmDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%=txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "1");
            fncLoadFreeItemForView();
            fncDecimal();
        }

        $(function () {
            SetQualifyingItemAutoComplete();
            SetFreeItemAutoComplete();
        });

        ///View Free Item List
        function fncLoadFreeItemForView() {
            try {
                var obj = {};

                if ($("#<%=txtGroupCode.ClientID %>").val() == "") {
                    return;
                }
                $("#<%=lnkSave.ClientID %>").css("visibility", "hidden");
                obj.groupcode = $("#<%=txtGroupCode.ClientID %>").val();
                obj.mode = $("#<%=hidMode.ClientID %>").val();

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("frmFreeItemEntry.aspx/fncGetFreeItemList")%>',
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        LoadFreeItemData(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Load Free Item Data
        function LoadFreeItemData(msg) {
            try {
                var objFreeItem, QualifyingItemBody, FreeItemBody;
                objFreeItem = jQuery.parseJSON(msg.d);

                QualifyingItemBody = $("#tblQualifyingItem tbody");
                FreeItemBody = $("#tblFreeItem tbody");

                QualifyingItemBody.children().remove();
                FreeItemBody.children().remove();

                if (objFreeItem.Table.length > 0) {
                    $("#<%=txtDesc.ClientID %>").val(objFreeItem.Table[0]["Description"]);
                    $("#<%=txtfrmDate.ClientID %>").val(objFreeItem.Table[0]["ValidFrom"]);
                    $("#<%=txtToDate.ClientID %>").val(objFreeItem.Table[0]["ValidTo"]);
                    $("#<%=txtBuyQty.ClientID %>").val(objFreeItem.Table[0]["BuyQty"]);
                    $("#<%=txtFreeQty.ClientID %>").val(objFreeItem.Table[0]["OfferedQty"]);
                }

                ///Create Quqlify Item
                if (objFreeItem.Table1.length > 0) {
                    for (var i = 0; i < objFreeItem.Table1.length; i++) {
                        row = "<tr><td>" +
                        objFreeItem.Table1[i]["RowNo"] + "</td><td> " +
                        "<img alt='Delete' type='image' src='../images/No.png' /></td><td >" +
                        objFreeItem.Table1[i]["ItemCode"] + "</td><td >" +
                        objFreeItem.Table1[i]["ItemDesc"] + "</td><td >" +
                        objFreeItem.Table1[i]["BatchNo"] + "</td><td  >" +
                        objFreeItem.Table1[i]["BuyQty"] + "</td></tr>";
                        QualifyingItemBody.append(row);
                    }
                }

                ///Create Free Item List
                if (objFreeItem.Table2.length > 0) {
                    for (var i = 0; i < objFreeItem.Table1.length; i++) {
                        row = "<tr><td>" +
                        objFreeItem.Table2[i]["RowNo"] + "</td><td> " +
                        "<img alt='Delete' type='image' src='../images/No.png' /></td><td >" +
                        objFreeItem.Table2[i]["ItemCode"] + "</td><td >" +
                        objFreeItem.Table2[i]["ItemDesc"] + "</td><td >" +
                        objFreeItem.Table2[i]["BatchNo"] + "</td><td  >" +
                        objFreeItem.Table2[i]["FreeQty"] + "</td><td  >" +
                        objFreeItem.Table2[i]["InputQty"] + "</td></tr>";
                        FreeItemBody.append(row);
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Inventory Search For Qualifying Item
        function SetQualifyingItemAutoComplete() {
            $("[id$=txtQItemCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtQItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtQItemCode.ClientID %>').val($.trim(i.item.valitemcode));
                    $('#<%=txtQDesc.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtQItemCode.ClientID %>').focus();
                    return false;
                },
                minLength: 1
            });
        }

        ///Inventory Search For Free Item 
        function SetFreeItemAutoComplete() {
            $("[id$=txtFItemcode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                focus: function (event, i) {
                    $('#<%=txtFItemcode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtFDesc.ClientID %>').val($.trim(i.item.valName));
                    $('#<%=txtFItemcode.ClientID %>').focus();
                    return false;
                },
                minLength: 1
            });
        }


        //Get Inventory code
        function fncGetInventoryCode(value) {
            try {

                var itemCode;
                if (value == "QItem") {
                    mode = "QItem";
                    itemCode = $('#<%=txtQItemCode.ClientID %>').val();
                }
                else {
                    mode = "FItem";
                    itemCode = $('#<%=txtFItemcode.ClientID %>').val();
                }

                if (itemCode == "")
                    return;

                $.ajax({
                    type: "POST",
                    url: '<%=ResolveUrl("~/WebService.asmx/fncComGetInventorycode")%>',
                    data: "{'inventorycode':'" + itemCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            if (value == "QItem")
                                $('#<%=txtQItemCode.ClientID %>').val(msg.d);
                            else if (value == "FItem")
                                $('#<%=txtFItemcode.ClientID %>').val(msg.d);

                        fncGetBatchStatus_Master(msg.d, $('#<%=hidBatch.ClientID %>'));
                        }
                        else {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                            if (mode == "QItem")
                                fncQItemClear();
                            else if (mode == "FItem")
                                fncFItemClear();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_InvalidItemcode%>');
                    }
                });


                }
                catch (err) {
                    ShowPopupMessageBox(err.message);
                }
            }

            ///Check Batch or Non Batch
            function fncGetBatchStatus() {
                try {

                    if (mode == "QItem")
                        fncGetInventoryDetail_Master($('#<%=txtQItemCode.ClientID %>').val());
                else if (mode == "FItem")
                    fncGetInventoryDetail_Master($('#<%=txtFItemcode.ClientID %>').val());
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    function fncAssignInventoryDetailToControls_Master(inventorydata) {
        try {
            if ($('#<%=hidBatch.ClientID %>').val() == "1") {
                    if (mode == "QItem")
                        fncGetBatchDetail_Master($('#<%=txtQItemCode.ClientID %>').val());
                    else if (mode == "FItem")
                        fncGetBatchDetail_Master($('#<%=txtFItemcode.ClientID %>').val());
            }


            if (mode == "QItem") {
                $('#<%=txtQDesc.ClientID%>').val(inventorydata[0]);
                    $('#<%=txtQQty.ClientID %>').focus();
                }
                else if (mode == "FItem") {
                    $('#<%=txtFDesc.ClientID%>').val(inventorydata[0]);
                    $('#<%=txtFQty.ClientID %>').focus();
                }


        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Get Batch No
    function fncAssignbatchvaluestotextbox(batchNo) {
        try {
            if (mode == "QItem") {
                $('#<%=txtQBatchNo.ClientID %>').val(batchNo);
                    $('#<%=txtQQty.ClientID %>').focus();
                }
                else if (mode == "FItem") {
                    $('#<%=txtFBatchNo.ClientID %>').val(batchNo);
                    $('#<%=txtFQty.ClientID %>').focus();
                }

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    ///Add Qualifying Item
    function fncAddQualifyingItem() {
        try {
            var buyQty, tblQualifyingItem;

            tblQualifyingItem = $("#tblQualifyingItem tbody");
            QualifyingLastRowNo = parseInt(QualifyingLastRowNo) + 1;
            row = "<tr><td id='tdRowNo_" + QualifyingLastRowNo + "' > " +
                QualifyingLastRowNo + "</td><td> " +
               "<img alt='Delete' src='../images/No.png' onclick='fncDeleteQualifyingItem(this);return false;' /></td><td id='tdQItemCode_" + QualifyingLastRowNo + "' >" +
               $('#<%=txtQItemCode.ClientID %>').val() + "</td><td id='tdQDesc_" + QualifyingLastRowNo + "' >" +
                   $('#<%=txtQDesc.ClientID %>').val() + "</td><td id='tdQBatchNo_" + QualifyingLastRowNo + "' >" +
                   $('#<%=txtQBatchNo.ClientID %>').val() + "</td><td id='tdQQty_" + QualifyingLastRowNo + "' >" +
                   $('#<%=txtQQty.ClientID %>').val() + "</td></tr>";
                tblQualifyingItem.append(row);
                buyQty = parseInt($('#<%=txtBuyQty.ClientID %>').val()) + parseInt($('#<%=txtQQty.ClientID %>').val());
                $('#<%=txtBuyQty.ClientID %>').val(buyQty);
                fncQItemClear();

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Qualifying Item Clear
        function fncQItemClear() {
            try {
                $('#<%=txtQItemCode.ClientID %>').val('');
                $('#<%=txtQDesc.ClientID %>').val('');
                $('#<%=txtQBatchNo.ClientID %>').val('');
                $('#<%=txtQQty.ClientID %>').val('');
                $('#<%=txtQItemCode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Delete Qualifying Item
        function fncDeleteQualifyingItem(source) {
            try {
                var RowNo = 0, buyQty = 0;
                $(source).parent().parent().remove();
                if ($("#tblQualifyingItem tbody").children().length > 0) {
                    $("#tblQualifyingItem tbody").children().each(function () {
                        $(this).find('td[id*=tdRowNo]').text(RowNo + 1);
                        buyQty = buyQty + parseInt($(this).find('td[id*=tdQQty]').text());
                    });
                    QualifyingLastRowNo = RowNo + 1;
                    $('#<%=txtBuyQty.ClientID %>').val(buyQty);
                }
                else {
                    QualifyingLastRowNo = 0;
                    $('#<%=txtBuyQty.ClientID %>').val('0');
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Add Free Item
        function fncAddFreeItem() {
            try {
                var tblFreeItemBody, freeQty, status = true;
                if ($('#<%=txtFItemcode.ClientID %>').val() == "") {
                    popUpObjectForSetFocusandOpen = $('#<%=txtFItemcode.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_EntItemcode%>');

                    return false;
                }
                if ($("#<%=hidBatch.ClientID %>").val() == "1" && $("#<%=txtFBatchNo.ClientID %>").val() == "") {
                    mode = "FItem";
                    fncGetBatchDetail_Master($('#<%=txtFItemcode.ClientID %>').val());
                    return false;
                }
                if ($("#<%=txtFQty.ClientID %>").val() == "" || parseFloat($("#<%=txtFQty.ClientID %>").val()) == 0) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtFQty.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Qty%>');
                    return false;
                }
                else if ($("#<%=txtLimitQty.ClientID %>").val() == "" || parseFloat($("#<%=txtLimitQty.ClientID %>").val()) < 0) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtLimitQty.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_limitQty%>');
                    return false;
                }
                <%--else if (parseFloat($("#<%=txtLimitQty.ClientID %>").val()) < parseFloat($("#<%=txtFQty.ClientID %>").val())) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtLimitQty.ClientID %>");
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_limitgreaterthenQty%>');
                    return false;
                }--%>
                else if ($("#tblFreeItem tbody").children().length > 0) {
                    $("#tblFreeItem tbody").children().each(function () {
                        if ($(this).find('td[id*=tdFItemCode]').text() == $('#<%=txtFItemcode.ClientID %>').val() &&
                    $(this).find('td[id*=tdFBatchNo]').text() == $('#<%=txtFBatchNo.ClientID %>').val()) {
                            status = false;
                            return;
                        }
                    });
                }

    if (status == false) {
        ShowPopupMessageBox('<%=Resources.LabelCaption.alert_alreadyadded%>');
                    fncFItemClear();
                    return;
                }


                tblFreeItemBody = $("#tblFreeItem tbody");
                freeItemLastRowNo = parseInt(freeItemLastRowNo) + 1;
                row = "<tr><td id='tdRowNo_" + freeItemLastRowNo + "' > " +
                    freeItemLastRowNo + "</td><td> " +
                   "<img alt='Delete' src='../images/No.png' onclick='fncDeleteFreeItem(this);return false;' /></td><td id='tdFItemcode_" + freeItemLastRowNo + "' >" +
                   $('#<%=txtFItemcode.ClientID %>').val() + "</td><td id='tdFDesc_" + freeItemLastRowNo + "' >" +
                   $('#<%=txtFDesc.ClientID %>').val() + "</td><td id='tdFBatchNo_" + freeItemLastRowNo + "' >" +
                   $('#<%=txtFBatchNo.ClientID %>').val() + "</td><td id='tdFQty_" + freeItemLastRowNo + "' >" +
                   $('#<%=txtFQty.ClientID %>').val() + "</td><td id='tdFLimitQty_" + freeItemLastRowNo + "' >" +
                   $('#<%=txtLimitQty.ClientID %>').val() + "</td></tr>";
                tblFreeItemBody.append(row);
                freeQty = parseInt($('#<%=txtFreeQty.ClientID %>').val()) + parseInt($('#<%=txtFQty.ClientID %>').val());
                $('#<%=txtFreeQty.ClientID %>').val(freeQty);
                fncFItemClear();


            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Free Item Clear
        function fncFItemClear() {
            try {
                $('#<%=txtFItemcode.ClientID %>').val('');
                $('#<%=txtFDesc.ClientID %>').val('');
                $('#<%=txtFBatchNo.ClientID %>').val('');
                $('#<%=txtFQty.ClientID %>').val('');
                $('#<%=txtLimitQty.ClientID %>').val('');
                $('#<%=txtFItemcode.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Delete Free Item
        function fncDeleteFreeItem(source) {
            try {
                var RowNo = 0, freeQty = 0;
                $(source).parent().parent().remove();
                if ($("#tblFreeItem tbody").children().length > 0) {
                    $("#tblFreeItem tbody").children().each(function () {
                        $(this).find('td[id*=tdRowNo]').text(RowNo + 1);
                        freeQty = freeQty + parseInt($(this).find('td[id*=tdFQty]').text());
                    });
                    freeItemLastRowNo = RowNo + 1;
                    $('#<%=txtFreeQty.ClientID %>').val(freeQty);
                }
                else {
                    freeItemLastRowNo = 0;
                    $('#<%=txtFreeQty.ClientID %>').val('0');
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Select All Check Box in CheckBox List
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

        ///Save Validation
        function fncSaveValidation() {
            try {
                var frmDate, toDate;
                frmDate = $('#<%=txtfrmDate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

                if ($("#tblQualifyingItem tbody").children().length == 0 || $("#tblFreeItem tbody").children().length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false
                }
                else if ($('#<%=txtGroupCode.ClientID %>').val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_GroupCode%>');
                    return false
                }
                else if (new Date(frmDate) < new Date(currentDatesqlformat_Master)) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_frmDate%>');
                    return false;
                }
                else if (new Date(toDate) < new Date(frmDate)) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.alert_freeitemtoDate%>');
                    return false;
                }
                else if ($("[id*=cblLocation] input:checked").length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Loc%>');
                        return false;
                    }

}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}

///Save Free Item
function fncSaveFreeItem() {
    try {

        if (fncSaveValidation() == false) {
            return false;
        }

        var rowGroupCode, rowQItem, rowFItem;
        var frmDate, toDate, desc ;

        frmDate = $('#<%=txtfrmDate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

                rowGroupCode = "<table>";
                $("[id*=cblLocation] input:checked").each(
                function () {
                    this.value;
                    //Free Item Group xml        
			desc = escape($('#<%=txtDesc.ClientID %>').val() );            
                    rowGroupCode = rowGroupCode + "<FGroupCode Groupcode='" + $('#<%=txtGroupCode.ClientID %>').val() + "'";
			rowGroupCode = rowGroupCode + " Desc='" +desc + "'";
                    rowGroupCode = rowGroupCode + " ValidFrom='" + frmDate + "'";
                    rowGroupCode = rowGroupCode + " ValidTo='" + toDate + "' ";
                    rowGroupCode = rowGroupCode + " BuyQty='" + $('#<%=txtBuyQty.ClientID %>').val() + "'";
                    rowGroupCode = rowGroupCode + " OfferedQty='" + $('#<%=txtFreeQty.ClientID %>').val() + "'";
                    rowGroupCode = rowGroupCode + " Location='" + this.value + "' ";
                    rowGroupCode = rowGroupCode + "></FGroupCode>";
                });
                rowGroupCode = rowGroupCode + "</table>";

                //Qualifying Item xml   
                rowQItem = "<table>";
                $("#tblQualifyingItem tbody").children().each(function () {
			desc = escape($.trim($(this).find('td[id*=tdQDesc]').text()));
                    rowQItem = rowQItem + "<QItem GroupCode='" + $('#<%=txtGroupCode.ClientID %>').val() + "'";
                    rowQItem = rowQItem + " ItemCode='" + $.trim($(this).find('td[id*=tdQItemCode]').text()) + "'";
                    rowQItem = rowQItem + " ItemDesc='" + desc  + "'";
                    rowQItem = rowQItem + " BuyQty='" + $.trim($(this).find('td[id*=tdQQty]').text()) + "' ";
                    rowQItem = rowQItem + " BatchNo='" + $.trim($(this).find('td[id*=tdQBatchNo]').text()) + "'";
                    rowQItem = rowQItem + "></QItem>";
                });
                rowQItem = rowQItem + "</table>";

                //Free Item xml  
                rowFItem = "<table>";
                $("#tblFreeItem tbody").children().each(function () {
		desc = escape($.trim($(this).find('td[id*=tdFDesc]').text()));
                    rowFItem = rowFItem + "<FItem GroupCode='" + $('#<%=txtGroupCode.ClientID %>').val() + "'";
                    rowFItem = rowFItem + " ItemCode='" + $.trim($(this).find('td[id*=tdFItemcode]').text()) + "'";
                    rowFItem = rowFItem + " ItemDesc='" + desc  + "'";
                    rowFItem = rowFItem + " FreeQty='" + $.trim($(this).find('td[id*=tdFQty]').text()) + "' ";
                    rowFItem = rowFItem + " BatchNo='" + $.trim($(this).find('td[id*=tdFBatchNo]').text()) + "'";
                    rowFItem = rowFItem + " InputQty='" + $.trim($(this).find('td[id*=tdFLimitQty]').text()) + "'";
                    rowFItem = rowFItem + "></FItem>";
                });
                rowFItem = rowFItem + "</table>";

                $('#<%=hidGroupcode.ClientID %>').val(escape(rowGroupCode));
                $('#<%=hidQItem.ClientID %>').val(escape(rowQItem));
                $('#<%=hidFItem.ClientID %>').val(escape(rowFItem));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Check Qualifying Item Available Or Not
        function fncQItemAddValidation() {
            try {
                var status = true;
                var obj = {};
                obj.itemcode = $("#<%=txtQItemCode.ClientID %>").val();
                obj.BatchNo = $("#<%=txtQBatchNo.ClientID %>").val();

                if (obj.itemcode == "") {
                    popUpObjectForSetFocusandOpen = $("#<%=txtQItemCode.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_EntItemcode%>');
                    return false;
                }
                else if ($("#<%=hidBatch.ClientID %>").val() == "1" && $("#<%=txtQBatchNo.ClientID %>").val() == "") {
                    mode = "QItem";
                    fncGetBatchDetail_Master($('#<%=txtQItemCode.ClientID %>').val());
                    return false;
                }
                else if ($("#<%=txtQQty.ClientID %>").val() == "" || parseFloat($("#<%=txtQQty.ClientID %>").val()) == 0) {
                    popUpObjectForSetFocusandOpen = $("#<%=txtQQty.ClientID %>");
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.alert_Qty%>');
                    return false;
                }
                else if ($("#tblQualifyingItem tbody").children().length > 0) {
                    $("#tblQualifyingItem tbody").children().each(function () {
                        if ($(this).find('td[id*=tdQItemCode]').text() == obj.itemcode && $(this).find('td[id*=tdQBatchNo]').text() == obj.BatchNo) {
                            status = false;
                            return;
                        }
                    });
                }

        if (status == false) {
            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_alreadyadded%>');
                    fncQItemClear();
                    return;
                }



                $.ajax({
                    type: "POST",
                    url: "frmFreeItemEntry.aspx/fncQItemAvailableOrNot",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d == "available") {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_QualifyItem%>');
                            fncQItemClear();
                        }
                        else {
                            fncAddQualifyingItem();
                        }
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Clear All
        function fncClearAll() {
            try {
                $("#<%=txtGroupCode.ClientID %>").val('');
                $("#<%=txtDesc.ClientID %>").val('');
                $("#<%=txtBuyQty.ClientID %>").val('0');
                $("#<%=txtFreeQty.ClientID %>").val('0');
                $("#tblQualifyingItem tbody").children().remove();
                $("#tblFreeItem tbody").children().remove();
                $("[id*=cbSelectAll]").removeAttr("checked");
                $("[id*=cblLocation] input").removeAttr("checked");

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncDecimal()
        {
            try
            {
                $("#<%=txtFreeMode.ClientID%>").number(true, 0);
            }
            catch (err)
            {
                fncToastError(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="FreeItemBackground_color ">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Merchandising/frmFreeItemView.aspx">Free Items Group</a></li>
                <li class="active-page">Free Items </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="supplierNote-group-full">
            <div class="display_table">
                <div class="freeItemEntry_header_left">
                    <div class="Free_Header_Text">
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblGroupCode" runat="server" Text='<%$ Resources:LabelCaption,lbl_GroupCode %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtGroupCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblDesc" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblfrmDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtfrmDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblBuyQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_BuyQty %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtBuyQty" runat="server" CssClass="form-control-res-right" Enabled="false" >0</asp:TextBox>
                            </div>
                        </div>
                        <div class="display_table">
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="lblfreeQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_FreeQty %>'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtFreeQty" runat="server" CssClass="form-control-res-right" Enabled="false" >0</asp:TextBox>
                            </div>
                            <div class="freeItem_header_lbl">
                                <asp:Label ID="Label1" runat="server" Text='Free Mode'></asp:Label>
                            </div>
                            <div class="freeItem_header_txt">
                                <asp:TextBox ID="txtFreeMode" runat="server" CssClass="form-control-res-right" onFocus ="this.select()" >0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="freeItemEntry_header_right">
                    <div class="freeItem_cbList">
                        <div class="free_selectAll">
                            <asp:CheckBox ID="cbSelectAll" runat="server" Text="Select All" />
                        </div>
                        <div>
                            <asp:CheckBoxList ID="cblLocation" runat="server">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="qualifying_item">
                    <div class="Free_Item_header_text qItem_len">
                        <asp:Label ID="lblQualifyingItem" runat="server" Text='<%$ Resources:LabelCaption,lbl_QualifyingHdr %>'></asp:Label>
                    </div>
                    <div class="Payment_fixed_headers qualifyingItem">
                        <table id="tblQualifyingItem" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Delete
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">BatchNo
                                    </th>
                                    <th scope="col">Qty
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblQItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtQItemCode" runat="server" CssClass="form-control-res" onblur="fncGetInventoryCode('QItem'); return false;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="freeItem_gDesc">
                            <div>
                                <asp:Label ID="lblQDesc" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtQDesc" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtQBatchNo" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblQQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtQQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="qualifyingAddButton">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                    OnClientClick="fncQItemAddValidation();return false;"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                    OnClientClick="fncQItemClear();return false;"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="free_item">
                    <div class="Free_Item_header_text freeItem_len">
                        <asp:Label ID="lblFreeItem" runat="server" Text='<%$ Resources:LabelCaption,lbl_FreeItemHdr %>'></asp:Label>
                    </div>
                    <div class="Payment_fixed_headers freeItem">
                        <table id="tblFreeItem" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                    <th scope="col">S.No
                                    </th>
                                    <th scope="col">Delete
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">BatchNo
                                    </th>
                                    <th scope="col">Qty
                                    </th>
                                    <th scope="col">LimitQty
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                    <div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblFItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFItemcode" runat="server" CssClass="form-control-res" onblur="fncGetInventoryCode('FItem');return false; "></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingDesc">
                            <div>
                                <asp:Label ID="lblFDesc" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFDesc" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblFBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFBatchNo" runat="server" CssClass="form-control-res" onMouseDown="return false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblFQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="qualifyingItem_lbl">
                            <div>
                                <asp:Label ID="lblLimitQty" runat="server" Text='<%$ Resources:LabelCaption,lbl_LimitQty %>'></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="txtLimitQty" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="freeItemAddButton">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAddFreeItem" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                    OnClientClick="fncAddFreeItem();return false;"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFreeClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                    OnClientClick="fncFItemClear();return false;"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="FreeItemSave">
                <div class="control-button">
                    <asp:UpdatePanel ID="upSave" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkSave" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,lnkSave %>'
                                OnClientClick="return fncSaveFreeItem();" OnClick="lnkSave_Click"></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="control-button">
                    <asp:UpdatePanel ID="close" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="lnkClose" runat="server" class="button-red" OnClick="lnkClose_Click" Text='Close'></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="hiddencol">
                <asp:HiddenField ID="hidBatch" runat="server" />
                <asp:HiddenField ID="hidGroupcode" runat="server" />
                <asp:HiddenField ID="hidQItem" runat="server" />
                <asp:HiddenField ID="hidFItem" runat="server" />
                <asp:HiddenField ID="hidMode" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
