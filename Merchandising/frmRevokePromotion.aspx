﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmRevokePromotion.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmRevokePromotion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .duplicate {
            background-color: yellow;
        }

        .BatchDetail td:nth-child(1), .BatchDetail th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
            text-align: left;
        }

        .BatchDetail td:nth-child(2), .BatchDetail th:nth-child(2) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(3), .BatchDetail th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(4), .BatchDetail th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
            text-align: left;
        }

        .BatchDetail td:nth-child(5), .BatchDetail th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: right !important;
        }

        .BatchDetail td:nth-child(6), .BatchDetail th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
            text-align: left;
        }

        .BatchDetail {
            padding: 2px;
        }

        .fixed_headers td:nth-child(1), .fixed_headers th:nth-child(1) {
            min-width: 57px;
            max-width: 57px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(2), .fixed_headers th:nth-child(2) {
            min-width: 78px;
            max-width: 78px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(3), .fixed_headers th:nth-child(3) {
            min-width: 75px;
            max-width: 75px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(4), .fixed_headers th:nth-child(4) {
            min-width: 266px;
            max-width: 266px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(5), .fixed_headers th:nth-child(5) {
            min-width: 150px;
            max-width: 150px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(6), .fixed_headers th:nth-child(6) {
            min-width: 150px;
            max-width: 150px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(7), .fixed_headers th:nth-child(7) {
            min-width: 155px;
            max-width: 155px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(8), .fixed_headers th:nth-child(8) {
            min-width: 80px;
            max-width: 80px;
            text-align: right !important;
        }

        .fixed_headers td:nth-child(9), .fixed_headers th:nth-child(9) {
            min-width: 79px;
            max-width: 79px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(10), .fixed_headers th:nth-child(10) {
            min-width: 83px;
            max-width: 83px;
            text-align: left !important;
        }

        .fixed_headers td:nth-child(11), .fixed_headers th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
            text-align: right !important;
        }

        .fixed_headers td:nth-child(12), .fixed_headers th:nth-child(12) {
            min-width: 90px;
            max-width: 90px;
            text-align: center !important;
        }

        .fixed_headers td:nth-child(13), .fixed_headers th:nth-child(13) {
               min-width: 90px;
            max-width: 90px;
            text-align: center !important;
        }

        .fixed_headers td:nth-child(14), .fixed_headers th:nth-child(14) {
            display: none;
        }

        .fixed_headers td:nth-child(15), .fixed_headers th:nth-child(15) {
            display: none;
        }

        .fixed_headers td:nth-child(16), .fixed_headers th:nth-child(16) {
            display: none;
        }
         .fixed_headers td:nth-child(17), .fixed_headers th:nth-child(17) {
            display: none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'StockAgingReport');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "StockAgingReport";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);

        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        //Page Load function
        function pageLoad() {
            try {
                if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnksave.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnksave.ClientID %>').css("display", "none");
                }
                $("#tblrevokepromotion tr").dblclick(function () {
                    var value = $(this).find('td input[id*="hidParmcode"]').val();
                    if (value == "1") {
                        fncAssignValuesTable($(this).closest("tr"));
                        $("#Promotion_Details").dialog({
                            resizable: false,
                            height: 150,
                            width: 633,
                            modal: true,
                            title: "Already Promotions are Available for this Item"
                        });
                    }

                });

                $('#divMsg').hide();
                $("select").chosen({ width: '100%' });
               <%-- //$("#<%= ChkAllow.ClientID %>").attr("disabled", true);--%>
                if ($("#<%= hidAllowPromotion.ClientID %>").val() == "Block") {
                    $('#divMsg').hide();
                }
                else {
                    <%--//$("#<%= ChkAllow.ClientID %>").removeAttr("disabled");
                   // $("#<%= ChkAllow.ClientID %>").attr("checked", "checked");--%>
                    $('#divMsg').show();
                }
                fncHighlightButton();


                if ($("#<%=hidfromdatestatus.ClientID %>").val() == "") {
                    $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                    $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                }
                else {
                    $("#<%= txtfromdate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker();
                    $("#<%= txttodate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker();
                }

                $("#<%=hidfromdatestatus.ClientID %>").val('DataInitialize')

                //fnchideshowrptrfooter();
                fncrptrRowClick();
                if ($('#<%=hidMode.ClientID %>').val() == "Revoke") {
                    fncCheck();
                }
                <%-- $("#<%=ChkAllow.ClientID %>").click(function () {
            var checked = $(this).is(':checked');
            if (checked == false) {
                $("#tblrevokepromotion [id*=promotionrow]").each(function () {
                    // $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                    if ($(this).find('td input[id*="cbreProfilterrow"]').is(':checked')) {cbreProfilterrow
                        var promo = $(this).find('td input[id*="hidParmcode"]').val();
                        if (promo == "1") {
                            $(this).find('td input[id*="cbreProfilterrow"]').removeAttr('checked');
                        }
                        $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr('checked');
                    }

                });
            }
            else {
                $("#tblrevokepromotion [id*=promotionrow]").each(function () {
                    if (!($(this).find('td input[id*="cbreProfilterrow"]').is(':checked'))) {
                        var promo = $(this).find('td input[id*="hidParmcode"]').val();
                        if (promo == "1") {
                            $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                        }
                        //$("#tblrevokepromotion [id*=cbProfilterheader]").attr("checked", "checked");
                    }

                });
            }
        });--%>
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///High light Button Selection 
        function fncHighlightButton() {
            try {
                if ($('#<%=hidMode.ClientID%>').val() == "Live") {
                    $('#<%=lnkviewlivepromotin.ClientID%>').css("background-color", "#26c538 !important");
                    $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.lbl_revoke%>');
                }
                else if ($('#<%=hidMode.ClientID%>').val() == "Expired") {
                    $('#<%=lnkviewexpirepromotion.ClientID%>').css("background-color", "#26c538 !important");
                    $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.lbl_revoke%>');
                }
                else {
                    $('#<%=lnkrevokepromotion.ClientID%>').css("background-color", "#26c538 !important");
                    $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.btnSave%>');
                }


        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //To show Inventory Search
    function fncShowPopUp() {
        try {
            //alert('fncShowPopUp');
            DialogSearchKeyWord.val('');
            inventory.children().remove();
            inventory.trigger("liszt:updated");
            $("#inventoryDialog").dialog('open');
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
        return false;
    }
    //Get Inventory Code DropDownList
    function setItemcode(itemcode) {
        try {
            $('#<%=txtItemCode.ClientID %>').val(itemcode);
            $('#<%=txtItemCode.ClientID %>').focus();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Clear all Controls
    function fncClearAll() {
        try {
            $("#tblrevokepromotion [id*=promotionrow]").remove();

            $('#<%=txtDepartment.ClientID %>').val('');
            $('#<%=txtCategory.ClientID %>').val('');
            $('#<%=txtBrand.ClientID %>').val('');
            $('#<%=txtVendor.ClientID %>').val('');
            $('#<%=txtLocation.ClientID %>').val('');

            $('#<%=txtItemCode.ClientID %>').val('');
            $("[id*=cbAllRecord]").removeAttr("checked");
            return false;
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
            return false;
        }
    }
    //repeater footer hide and show
    function fnchideshowrptrfooter() {
        try {
            if ($("#tblrevokepromotion [id*=promotionrow]").length > 0)
                $('#<%=emptyrow.ClientID%>').hide();
            else
                $('#<%=emptyrow.ClientID%>').show();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    //get from and To date
    //        function fncgetfromdatetadate() {
    //            try {
    //                //alert('fncgetfromdatetadate');


    //            }
    //            catch (err) {
    //                ShowPopupMessageBox(err.message);
    //            }
    //        }
    //disable save button
    function fncviewexpirebuttonclick() {
        try {
            //$('#savebutton').hide();
            //fncgetfromdatetadate();
            $('#<%=hidstatus.ClientID%>').val('live');
            $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.lbl_revoke%>');
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    //save button change to revoke mode
    function fncviewlivepromotionclick() {
        try {
            $('#<%=hidstatus.ClientID%>').val('live');
            $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.lbl_revoke%>');
            //$('#savebutton').show();
            //fncgetfromdatetadate();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Revoke button change to Save mode
    function fncrevokepromotionclick() {
        try {
           <%--// $("#<%= ChkAllow.ClientID %>").attr("checked", "checked");--%>
            //  $('#divMsg').show()
            $('#<%=hidstatus.ClientID%>').val('revoke');
            $('#<%=lnksave.ClientID%>').text('<%=Resources.LabelCaption.btnSave%>');
            return true;
            //$('#savebutton').show();
            //fncgetfromdatetadate();
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Show Popup After Save
    function fncShowSuccessMessage() {
        try {
            InitializeDialog();
            $("#Promotionupdate").dialog('open');
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
            console.log(err);
        }
    }
    //Close Save Dialog
    function fncCloseSaveDialog() {
        try {
            //$("#Promotionupdate").dialog('close');
            //fncClearAll();
            if ($("#<%=hidCheckBox.ClientID %>").val() == "1")//
            {
                $("#<%=cbAllRecord.ClientID %>").attr("checked", "checked");
            }
            else {
                $("#<%=cbAllRecord.ClientID %>").removeAttr("checked");
            }

            $("#<%=btnSave.ClientID %>").click();
            return false;
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    //Save Dialog Initialation
    function InitializeDialog() {
        try {
            $("#Promotionupdate").html('<%=Resources.LabelCaption.Save_RevokePromotionUpdate%>');
            $("#Promotionupdate").dialog({
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true,
                buttons: {
                    Ok: function () {
                        $(this).dialog("destroy");
                        fncCloseSaveDialog();
                    }
                }
            });
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    //Save Validation
    function fncSaveValidation() {
        try {

            if ($("#tblrevokepromotion [id*=promotionrow]").length == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');

                return false;
            }
            else if ($("#tblrevokepromotion [id*=cbreProfilterrow]:checked").length == 0) {
                ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Selctonerow%>');

                    return false;
                }


            if ($('#<%=lnksave.ClientID %>').text() == "Save") {
                if (new Date($('#<%=txtfromdate.ClientID %>').val()) < new Date(currentDateformat_Master)) {

                    popUpObjectForSetFocusandOpen = $('#<%=txtfromdate.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_Promotionfromdate%>');
                    return false;
                }
                else if (new Date($('#<%=txttodate.ClientID %>').val()) < new Date($('#<%=txtfromdate.ClientID %>').val())) {
                    popUpObjectForSetFocusandOpen = $('#<%=txttodate.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_Promotiontodatevalidation%>');
                    return false;
                }
        }
    }
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
//Repeater Row Click
var val = '0';
function fncrptrRowClick(source) {
    try {
        var rowobj = $(source).parent().parent();
        var promo = rowobj.find('td input[id*="hidParmcode"]').val();
        var code = $.trim(rowobj.find('td span[id*="lblinventory"]').html());
        var batch = $.trim(rowobj.find('td span[id*="lblBatchNo"]').html());
        var lable = $.trim(rowobj.find('td input[id*="txtName"]').val());
        if ($('#<%=hidMode.ClientID %>').val() == "Revoke") {
                $('#tblrevokepromotion [id*=promotionrow]').each(function () {
                    var Itemcode = $.trim($(this).find('td span[id*="lblinventory"]').html());
                    var Itembatch = $.trim($(this).find('td span[id*="lblBatchNo"]').html());
                    var name = $.trim($(this).find('td input[id*="txtName"]').val());

                    if (lable != name) {
                        if (code == Itemcode && Itembatch == batch && $(this).find('input[type="checkbox"]').is(':checked')) {
                            rowobj.find('td input[id*="cbreProfilterrow"]').removeAttr("checked");
                            $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr('checked');
                            ShowPopupMessageBox("Already same promotion is checked");
                            return false;
                        }
                    }

                });
            }
            //if (val == '1') {
            //    $("#tblrevokepromotion [id*=cbreProfilterrow]").removeAttr("checked");
            //    ShowPopupMessageBox("Already one promotion is checked");
            //    val = '0';
            //    return false;
            //}
            if (promo == "1") {
                $("#tblrevokepromotion [id*=cbreProfilterrow]").removeAttr("checked");
                $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr('checked');
                ShowPopupMessageBox("Already promtion is Available for this item.");
                return false;
            }

            else if ($("#tblrevokepromotion [id*=cbreProfilterrow]").length == 0) {
                $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr("checked");
            }
            else if ($("#tblrevokepromotion [id*=cbreProfilterrow]").length == $("#tblrevokepromotion [id*=cbreProfilterrow]:checked").length) {
                $("#tblrevokepromotion [id*=cbProfilterheader]").attr("checked", "checked");

            }
            else {
                $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr("checked");
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }


    //$(document).ready(function () {
    //    $('#tblrevokepromotion').find('input[type="checkbox"]:checked').each(function () {
    //        var Itemcode = $(this).find('td span[id*="InventoryCode"]').html();
    //        var Itembatch = $(this).find('td span[id*="BatchNo"]').html();
    //        if ($("#tblrevokepromotion [id*=cbreProfilterrow]").is(":checked") && Itemcode == code && Itembatch == batch) {
    //            $("#tblrevokepromotion [id*=cbreProfilterrow]").removeAttr("checked");
    //            ShowPopupMessageBox("Already promtion is Available for this item.");
    //            return false;
    //        }
    //    });
    //});
    //Header Row Click
    function fncrptrHeaderclick(source) {
        try {
            if (($(source).is(":checked"))) {
                $("#tblrevokepromotion [id*=promotionrow]").each(function () {
                    var promo = $(this).find('td input[id*="hidParmcode"]').val();
                    var lable = $.trim($(this).find('td input[id*="txtName"]').val());
                    var code = $.trim($(this).find('td span[id*="lblinventory"]').html());
                    var batch = $.trim($(this).find('td span[id*="lblBatchNo"]').html());
                    if ($('#<%=hidMode.ClientID %>').val() == "Revoke") {
                        $('#tblrevokepromotion [id*=promotionrow]').each(function () {
                            var Itemcode = $.trim($(this).find('td span[id*="lblinventory"]').html());
                            var Itembatch = $.trim($(this).find('td span[id*="lblBatchNo"]').html());
                            var name = $.trim($(this).find('td input[id*="txtName"]').val());

                            if (lable != name) {
                                if (code == Itemcode && Itembatch == batch && $(this).find('input[type="checkbox"]').is(':checked')) {
                                    $(this).find('td input[id*="cbreProfilterrow"]').removeAttr("checked");
                                    $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr('checked');
                                    ShowPopupMessageBox("Already one promotion is checked");
                                }
                            }

                        });
                    }
                    if (promo == "1") {
                        $(this).find('td input[id*="cbreProfilterrow"]').removeAttr("checked");
                        $("#tblrevokepromotion [id*=cbProfilterheader]").removeAttr('checked');
                        ShowPopupMessageBox("Already promtion is Available for this item.");
                    }
                    else {
                        $(this).find('td input[id*="cbreProfilterrow"]').attr("checked", "checked");
                    }
                });
            }
            else {
                $("#tblrevokepromotion [id*=cbreProfilterrow]").removeAttr("checked");
            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    function fncSetValue() {
        try {

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    function fncAssignValuesTable(msg) {
        try {
            var row = msg;
            if (msg != null) {
                var tblPromotionBody = $("#tblPromotion tbody");
                tblPromotionBody.children().remove();
                $('#lblInventoryCode').text("ItemCode: " + $.trim($("td", row).eq(1).text()));
                $('#lblPromoDescription').text("Description: " + $.trim($("td", row).eq(3).text()));
                row = "<tr><td>" + "1" + "</td>" +
                    "<td>" + $.trim($("td", row).eq(2).text()) + "</td>" +
                    "<td>" + $.trim($("td", row).eq(13).text()) + "</td>" +
                    "<td>" + $.trim($("td", row).eq(14).text()) + "</td>" +
                    "<td>" + $.trim($("td", row).eq(15).text()) + "</td>" +
                    "<td>" + $.trim($("td", row).eq(0).text()) + "</td></tr>";
                tblPromotionBody.append(row);

            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }

    function fncCheck() {
        var arr = [];
        var arr2 = [];
        var val = '0';
        var tr = $('#tblrevokepromotion tbody tr');
        var td = '';
        var row = 0;
        $('#tblrevokepromotion [id*=promotionrow]').each(function () {
            var Itemcode = $.trim($(this).find('td span[id*="lblinventory"]').html());
            var Itembatch = $.trim($(this).find('td span[id*="lblBatchNo"]').html());
            row = row + 1;

            td = "<td><input style ='display:none' type='text' id='txtName' value='" + row + "' ></input></td>";

            $(td).insertAfter($(this).find('td').eq(15));

            if (arr.indexOf(Itembatch) == -1 && arr2.indexOf(Itemcode) == -1) {
                arr.push(Itembatch);
            }
            else {
                //td = "<td><input style ='display:none' type='text' id='txtName' value='0'></input></td>";
                $(td).insertAfter($(this).find('td').eq(15));
                // $(this).addClass("duplicate");
            }
        });
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="background: #e6f2ff">
        <div class="container-group-full">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Promotions</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page">
                        <%=Resources.LabelCaption.Alert_lblRevokePromotion%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
            <div class="container-group-full">
                <div class="revokepromotionbutton">
                    <asp:UpdatePanel ID="upviewbutton" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkviewexpirepromotion" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_viewexpiredpromotion %>'
                                    OnClick="lnkviewexpirepromotion_Click" OnClientClick="fncviewexpirebuttonclick()"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkviewlivepromotin" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_viewlivepromotion %>'
                                    OnClick="lnkviewlivepromotin_Click" OnClientClick="fncviewlivepromotionclick()"> </asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkrevokepromotion" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_revokepromotion %>'
                                    OnClick="lnkrevokepromotion_Click" OnClientClick="return fncrevokepromotionclick();"> </asp:LinkButton>
                            </div>
                            <asp:HiddenField ID="hidMode" runat="server" Value="" />
                            <asp:HiddenField ID="hidCheckBox" runat="server" Value="0" />
                            <asp:HiddenField ID="hidAllowPromotion" runat="server" Value="Block" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="container-group-full">
                <asp:UpdatePanel ID="upfilter" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="revokepromotion_filteration">
                            <div class="revokepromotion_dropdown">
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lbldepartment" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <%--<asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                                </div>
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lblcategory" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <%--<asp:DropDownList ID="ddlCategory" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="revokepromotion_dropdown">
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lblbran" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <%--<asp:DropDownList ID="ddlbrand" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtVendor');"></asp:TextBox>
                                </div>
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lblvendor" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <%--<asp:DropDownList ID="ddlvendor" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtItemCode');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="revokepromotion_dropdownwithsearch">
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lblitemcode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <asp:TextBox ID="txtItemCode" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtLocation');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                                <%--<div class="container5">
                                    <img id="imgsearch" src="../images/SearchImage.png" onclick="return fncShowPopUp()"
                                        height="20px" width="20px" alt="search" />
                                </div>--%>
                                <div class="revokepromotion_dropdown_left">
                                    <asp:Label ID="lbllocation" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label>
                                </div>
                                <div class="revokepromotion_dropdown_right">
                                    <%--<asp:DropDownList ID="ddllocation" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="revokepromotion_date">
                            <div class="revokepromotion_dropdown_left">
                                <asp:Label ID="lblfromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                            </div>
                            <div class="revokepromotion_dropdown_right">
                                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                            <div class="revokepromotion_dropdown_left">
                                <asp:Label ID="lbltodatefilteration" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                            </div>
                            <div class="revokepromotion_dropdown_right">
                                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="revokepromotion_date">
                            <div class="control-group-single">
                                <asp:CheckBox ID="cbAllRecord" runat="server" Text='<%$ Resources:LabelCaption,cbGetAllRecord %>' />
                            </div>
                            <%--<div class="control-group-single" id="divAllow" runat="server">
                                <asp:CheckBox ID="ChkAllow" runat="server" CssClass="cbLocation" Text="Overwrite Existing Promotion"></asp:CheckBox>
                            </div>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="divrepeater" class="fixed_headers" runat="server">
                <asp:UpdatePanel ID="uprepeater" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table id="tblrevokepromotion" cellspacing="0" rules="all" border="1">
                            <thead>
                                <tr>
                                   <th scope="col">S.No</th>
                                    <th scope="col">Location
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">BatchNo
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">Department
                                    </th>
                                    <th scope="col">Category
                                    </th>
                                    <th scope="col">Brand
                                    </th>
                                    <th scope="col">S.price
                                    </th>
                                    <th scope="col">FromDate
                                    </th>
                                    <th scope="col">ToDate
                                    </th>
                                    <th scope="col">ProPrice
                                    </th>
                                    <th scope="col">
                                        <asp:CheckBox ID="cbProfilterheader" runat="server" onclick="fncrptrHeaderclick(this)" />
                                    </th>
                                    <th scope="col">PromoCode
                                    </th>
                                    <th scope="col">FromDate
                                    </th>
                                    <th scope="col">TOdate
                                    </th>
                                    <th scope="col">CurrProPrice
                                    </th>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptrRevokepromotion" runat="server" OnItemDataBound="rptrRevokepromotion_ItemDataBound">
                                <HeaderTemplate>
                                    <tbody id="promotionfilterbody">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr id="promotionrow">
                                        <td>
                                            <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNumber") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbllocation" runat="server" Text='<%# Eval("Location") %>' />
                                        </td>
                                        <td id="rptItemcode" runat="server">
                                            <asp:Label ID="lblinventory" runat="server" Text='<%# Eval("InventoryCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("BatchNo") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDepartmentCode" runat="server" Text='<%# Eval("DepartmentCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCategoryCode" runat="server" Text='<%# Eval("CategoryCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBrandCode" runat="server" Text='<%# Eval("BrandCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPromotionFromDate" CssClass="reprice" runat="server" Text='<%# Eval("PromotionFromDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPromotionToDate" runat="server" Text='<%# Eval("PromotionToDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblpromotionprice" runat="server" Text='<%# Eval("PromotionPrice") %>' />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="cbreProfilterrow" runat="server" onclick="fncrptrRowClick(this)" />
                                        </td>
                                        <td style="display: none;" id="Promcode" runat="server">
                                            <asp:HiddenField ID="hidParmcode" runat="server" Value='<%# Eval("PromoCode") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFromdate" runat="server" Text='<%# Eval("CurrentPromotionFromDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltodate" runat="server" Text='<%# Eval("CurrentPromotionToDate") %>' />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCurrentPrprice" runat="server" Text='<%# Eval("CurrentPrice") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                </FooterTemplate>
                            </asp:Repeater>
                            <tfoot>
                                <tr style="height: 360px" runat="server" id="emptyrow">
                                    <td colspan="10" class="repeater_td_align_center">
                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                            Text="No items to display" />
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="rptpagination" class="container-group-full">
                <div class="revokepromotion_pagingborder" style="display: none">
                    <div style="float: left">
                        <span>
                            <asp:LinkButton ID="lnkFirst" Text='<%$ Resources:LabelCaption,lbl_First %>' runat="server" />
                        </span><span>
                            <asp:LinkButton ID="lnkPrevious" Text='<%$ Resources:LabelCaption,lbl_Previous %>'
                                runat="server" />
                        </span>
                        <asp:Repeater ID="rptrPagination" runat="server">
                            <ItemTemplate>
                                <span id="spanpagination">
                                    <asp:LinkButton ID="lnkrptpagination" Text='<%# Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                        runat="server" CssClass='<%# Convert.ToBoolean(Eval("Enabled")) ? "pager_enabled" : "pager_disabled" %>'
                                        OnClientClick='<%# !Convert.ToBoolean(Eval("Enabled")) ? "return false;" : "" %>'> ></asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                        <span>
                            <asp:LinkButton ID="lnkNext" Text='<%$ Resources:LabelCaption,lbl_Next %>' runat="server" />
                        </span><span>
                            <asp:LinkButton ID="lnkLast" Text='<%$ Resources:LabelCaption,lbl_Last %>' runat="server" />
                        </span>
                    </div>
                    <div style="float: right" class="pagingborder_status">
                        <span id="spanpagestatus">
                            <asp:Label ID="lblPageStatus" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
                <div class="revokepromotion_save">
                    <asp:UpdatePanel runat="server" ID="upReProSave" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkclear" runat="server" class="button-blue" OnClientClick="return fncClearAll()"
                                    Text='<%$ Resources:LabelCaption,btnClearAll %>'></asp:LinkButton>
                            </div>
                            <div id="savebutton" class="control-button">
                                <asp:LinkButton ID="lnksave" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_revoke %>'
                                    OnClick="lnksave_Click" OnClientClick="return fncSaveValidation()"></asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <asp:HiddenField ID="hidfromdatestatus" runat="server" Value="" />
                <asp:HiddenField ID="hidstatus" runat="server" Value="" />
                <asp:HiddenField ID="hidrptrtstatus" runat="server" />

                <div id="Promotionupdate">
                    <%-- <p>
                        <%=Resources.LabelCaption.Save_RevokePromotionUpdate%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>
    <div id="divMsg" style="margin-top: 5px;">
        <label style="background-color: #f44336; color: #FFFFFF;">XXXX</label>
        <label id="lblPromotionAvailable" runat="server">Already Promotions are Available for this items(Double Click)</label>
        <label style="background-color: #34d361; color: #FFFFFF;">XXXX</label>
        <label id="Label1" runat="server">There is no current promotion  for this items</label>
    </div>
    <div class="hiddencol">
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" />
    </div>
    <div class="hiddencol">
        <div id="Promotion_Details">
            <div class="col-md-12">
                <div class="col-md-3">
                    <label id="lblInventoryCode" style="font-weight: 800;"></label>
                </div>
                <div class="col-md-9">
                    <label id="lblPromoDescription" style="font-weight: 800;"></label>
                </div>
            </div>
            <div class="Payment_fixed_headers BatchDetail" style="height: 52px; overflow: hidden">
                <table id="tblPromotion" cellspacing="0" rules="all" border="1">
                    <thead>
                        <tr>
                            <th scope="col">S.No
                            </th>
                            <th scope="col">BatchNo     
                            </th>
                            <th scope="col">From Date
                            </th>
                            <th scope="col">To Date
                            </th>
                            <th scope="col">Promotion Price
                            </th>
                            <th scope="col">Location
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
