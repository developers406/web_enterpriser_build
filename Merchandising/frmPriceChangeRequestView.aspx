﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPriceChangeRequestView.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmPriceChangeRequestView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .hiddencol {
            display: none;
        }

        .control-group-single .label-left {
            width: 40%;
        }

        .control-group-single .label-right {
            width: 60%;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PriceChangeRequestView');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PriceChangeRequestView";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript" language="Javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=LinkButton6.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=LinkButton6.ClientID %>').css("display", "none");
            }
            if ($('#<%=hidViewbtn.ClientID%>').val() == "V1") {
                $('#<%=LinkButton8.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=LinkButton8.ClientID %>').css("display", "none");
              }
              if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                $('#<%=LinkButton9.ClientID %>').css("display", "block");
              }
              else {
                  $('#<%=LinkButton9.ClientID %>').css("display", "none");
              }

          }

          //        $(document).ready(function () {
          //           
          //        });


          function fncValidateDelete() {
              try {
                  var gridtr = $("#<%= gvPriceChange.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage1();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete1() {
            try {
                // alert("sdfg");
                $("#DisplayDelete").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //alert("message");
                InitializeDialogDelete1();
                $("#DisplayDelete").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncCloseDeleteDialog() {
            try {
                $("#DisplayDelete").dialog('close');


            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmDeleteMessage1() {
            try {
                InitializeDialogDeletes();
                $("#DeleteConfirm").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDeletes() {
            try {
                $("#DeleteConfirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirm").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirm").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }


        function EnableOrDisableDropdown(element, isEnable) {
            //alert(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncValidateSave() {
            try {
                var gridtr = $("#<%= gvPriceChange.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmSaveMessage();
                    } else {
                        fncShowMessage();
                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }


        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return false;
            }
            catch (err) {
                alert(err.message);
            }
        }

    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");


        }
        function fncClear() {



        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Price Change Utility</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_PriceChangeRequest%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="control-group-split">
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton6" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                        OnClick="lnkAdd_Click" Width="100px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton8" runat="server" class="button-blue" OnClick="lnkBtnView_Click"
                        Text='<%$ Resources:LabelCaption,btn_View %>' Width="100px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton9" runat="server" class="button-blue" OnClick="lnkBtnDelete_Click"
                        Text='<%$ Resources:LabelCaption,btn_delete %>' Width="100px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton10" runat="server" class="button-blue" OnClick="lnkBtnPrint_Click"
                        Text='<%$ Resources:LabelCaption,btn_Print %>' Width="100px"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="LinkButton11" runat="server" class="button-blue" OnClick="lnkRefresh_Click"
                        Text='<%$ Resources:LabelCaption,btn_refresh %>' Width="100px"></asp:LinkButton>
                </div>
                <div class="control-group-split" style="width: 65%; float: left">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Outlet Name"></asp:Label>
                            </div>
                            <div class="label-right" id="ddl">
                                <asp:DropDownList ID="ddlOutlet" runat="server" OnSelectedIndexChanged="ddlOutlet_SelectedIndexChanged"
                                    AutoPostBack="True" CssClass="form-control-res">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 46%;">
                            <div class="label-right" style="width: 10%">
                                <asp:CheckBox ID="chkProcess" runat="server" AutoPostBack="True" OnCheckedChanged="chkLinked_CheckedChanged" />
                            </div>
                            <div class="label-left" style="width: 55%">
                                <asp:Label ID="Label32" runat="server" Text='<%$ Resources:LabelCaption,lbl_Processed %>'></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                        style="height: 400px">
                        <asp:GridView ID="gvPriceChange" runat="server" Width="100%" AutoGenerateColumns="False"
                            ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" AlternatingRowStyle-BackColor="#c4ddff"
                            CssClass="pshro_GridDgn" EmptyDataRowStyle-CssClass="Emptyidclassforselector"
                            OnSelectedIndexChanged="gvPO_SelectedIndexChanged" DataKeyNames="PriceChangeReqNo"
                            AutoGenerateSelectButton="True">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                            <PagerStyle CssClass="pshro_text" />
                            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:BoundField DataField="PriceChangeReqNo" HeaderText="Request No"></asp:BoundField>
                                <asp:BoundField DataField="ReqDate" HeaderText="Request Date"></asp:BoundField>
                                <asp:BoundField DataField="LocationCode" HeaderText="From Location"></asp:BoundField>
                                <asp:BoundField DataField="createUser" HeaderText="Created By"></asp:BoundField>
                                <asp:BoundField DataField="createDate" HeaderText="Created On"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <div class="modal-loader">
                        <div class="center-loader">
                            <img alt="" src="../images/loading_spinner.gif" />
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="container-group-full" style="display: none">
                <div id="SelectAny">
                    <p>
                        <%=Resources.LabelCaption.Alert_Select_Any%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DisplayDelete">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="DeleteConfirm">
                    <p>
                        <%=Resources.LabelCaption.Alert_DeleteSure%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                                Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="ConfirmSaveDialog">
                    <p>
                        <%=Resources.LabelCaption.Alert_Confirm_Save%>
                    </p>
                    <div style="width: 150px; margin: auto">
                        <div style="float: left">
                            <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                                Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                        </div>
                        <div style="float: right">
                            <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                                Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdatePosting">
                    <p>
                        <%=Resources.LabelCaption.Alert_Delete%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="StockUpdateSave">
                    <p>
                        <%=Resources.LabelCaption.Alert_Save%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full" style="display: none">
                <div id="AlertNoItems">
                    <p>
                        <%=Resources.LabelCaption.Alert_No_Items%>
                    </p>
                    <div style="margin: auto; width: 100px">
                        <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                            Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-group-full">
                <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
                <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
                <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
                <asp:HiddenField ID="hidSavebtn" runat="server" />
                <asp:HiddenField ID="hidDeletebtn" runat="server" />
                <asp:HiddenField ID="hidEditbtn" runat="server" />
                <asp:HiddenField ID="hidViewbtn" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
