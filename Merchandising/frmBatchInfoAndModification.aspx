﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmBatchInfoAndModification.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmBatchInfoAndModification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .gid_BatchInfoAndModification td:nth-child(1), .gid_BatchInfoAndModification th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .gid_BatchInfoAndModification td:nth-child(2), .gid_BatchInfoAndModification th:nth-child(2) {
            min-width: 70px;
            max-width: 70px;
        }

        .gid_BatchInfoAndModification td:nth-child(3), .gid_BatchInfoAndModification th:nth-child(3) {
            min-width: 350px;
            max-width: 350px;
        }

        .gid_BatchInfoAndModification td:nth-child(4), .gid_BatchInfoAndModification th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .gid_BatchInfoAndModification td:nth-child(5), .gid_BatchInfoAndModification th:nth-child(5) {
            min-width: 75px;
            max-width: 75px;
        }

        .gid_BatchInfoAndModification td:nth-child(6), .gid_BatchInfoAndModification th:nth-child(6) {
            min-width: 75px;
            max-width: 75px;
        }

        .gid_BatchInfoAndModification td:nth-child(7), .gid_BatchInfoAndModification th:nth-child(7) {
            min-width: 75px;
            max-width: 75px;
        }

        .gid_BatchInfoAndModification td:nth-child(8), .gid_BatchInfoAndModification th:nth-child(8) {
            min-width: 75px;
            max-width: 75px;
        }

        .gid_BatchInfoAndModification td:nth-child(9), .gid_BatchInfoAndModification th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_BatchInfoAndModification td:nth-child(10), .gid_BatchInfoAndModification th:nth-child(10) {
            min-width: 75px;
            max-width: 75px;
        }

        .gid_BatchInfoAndModification td:nth-child(11), .gid_BatchInfoAndModification th:nth-child(11) {
            min-width: 90px;
            max-width: 90px;
        }

        .gid_BatchInfoAndModification td:nth-child(12), .gid_BatchInfoAndModification th:nth-child(12) {
            min-width: 80px;
            max-width: 80px;
        }

        .left_align {
            text-align: left;
            padding-left: 5px;
        }

        .right_align {
            text-align: right;
            padding-right: 5px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(1), .gid_LocBatchInfoAndModification th:nth-child(1) {
            min-width: 59px;
            max-width: 59px;
            text-align: center;
        }

        .gid_LocBatchInfoAndModification td:nth-child(2), .gid_LocBatchInfoAndModification th:nth-child(2) {
            min-width: 90px;
            max-width: 90px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(3), .gid_LocBatchInfoAndModification th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(4), .gid_LocBatchInfoAndModification th:nth-child(4) {
            min-width: 90px;
            max-width: 90px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(5), .gid_LocBatchInfoAndModification th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(6), .gid_LocBatchInfoAndModification th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(7), .gid_LocBatchInfoAndModification th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(8), .gid_LocBatchInfoAndModification th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(9), .gid_LocBatchInfoAndModification th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(10), .gid_LocBatchInfoAndModification th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(11), .gid_LocBatchInfoAndModification th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(12), .gid_LocBatchInfoAndModification th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(13), .gid_LocBatchInfoAndModification th:nth-child(13) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(14), .gid_LocBatchInfoAndModification th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LocBatchInfoAndModification td:nth-child(15), .gid_LocBatchInfoAndModification th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BatchInfoAndModification');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BatchInfoAndModification";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
        function pageLoad() {

            $('#<%=chkAllLoc.ClientID %>').click(
              function () {

                  //$("input[type='CheckBox']").attr('checked', $('#<%=chkAllLoc.ClientID %>').is(':checked'));
                  if (($('#<%=chkAllLoc.ClientID %>').is(":checked"))) { // dinesh
                      $("#ContentPlaceHolder1_chkListLoc input[type='CheckBox']").attr("checked", "checked");
                  }
                  else {
                      $("#ContentPlaceHolder1_chkListLoc input[type='CheckBox']").removeAttr("checked");
                  }

              });
        }

        function fncPriceChange(lnk) {
            try {
                var row = lnk.parentNode.parentNode;
                var rowobj = $(lnk).parent().parent();
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False  
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncChangeSellingQty(lnk, obj, evt, nextCol, prvCol) { //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                var row = lnk.parentNode.parentNode;
                var rowobj = $(lnk).parent().parent();

                if (charCode == 13) {
                    var NextRowobj = rowobj.next();
                    if (NextRowobj.length > 0) {
                        NextRowobj.find('td input[id*="' + obj + '"]').select();
                    }
                    else {
                        rowobj.siblings().first().find('td input[id*="' + obj + '"]').select();
                    }
                    return false;
                }
                else if (charCode == 39) {
                    setTimeout(function () {
                        rowobj.find('td input[id*="' + nextCol + '"]').select();
                    }, 10);
                }
                else if (charCode == 37) {
                    setTimeout(function () {
                        rowobj.find('td input[id*="' + prvCol + '"]').select();
                    }, 10);
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncUpdate() {
            try {
                if ($(".cbLocation").find("input:checked").length == 0) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Loc%>');
                    return false;
                }
                else
                $('#<%=btnUpdate.ClientID %>').click();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
            fncDecimal();
        });

        $(document).keyup(function (e) {

            if (e.keyCode == 27) {
                window.parent.jQuery('#popupBatchInfo').dialog('close');
                window.parent.$('#popupBatchInfo').remove();
            }
        });

        function disableFunctionKeys(e) {
            try {
                debugger;
                var functionKeys = new Array(13,112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        fncUpdate();
                       <%-- $('#<%=btnUpdate.ClientID %>').click();--%>
                        e.preventDefault();
                        return false;
                    }
                    else if (e.keyCode == 117) {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClearAll', '');
                        fncToastInformation("Cleared");
                        e.preventDefault();
                        return false;
                    }
                    else {
                        e.preventDefault();
                        return false;
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }

        }

        function fncDecimal() {
            try {
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_0"]').select();
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice1_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice2_"]').number(true, 2);
                $('[id*="ContentPlaceHolder1_gvHeadQuatersDetails_txtWPrice3_"]').number(true, 2);

            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        var validation = false; 
        function txtSellingPrice_TextChanged(lnk) { 
            try { 
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sNetCost = row.cells[4].innerHTML;
               
                if (sSellingPrice != "") {
                    if (parseFloat(sSellingPrice) > parseFloat(sMRP)) 
                    {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML; 
                        row.cells[0].getElementsByTagName("input")[0].checked = false; 
                        validation = true;
                        Confirm("Selling Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }
                    else if (parseFloat(sSellingPrice) < parseFloat(sNetCost))
                    {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("Selling Price must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }
                    
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice1_TextChanged(lnk) {   
            try {

                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1; 
                var sWholeSalePrice1 = row.cells[7].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sNetCost = row.cells[4].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice1 != "") {
                    if (parseFloat(sWholeSalePrice1) > parseFloat(sMRP))  
                    {
                        row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;  
                        validation = true;
                        Confirm("WPrice1 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                        return;
                    }
                    else if (parseFloat(sSellingPrice) < parseFloat(sNetCost)) {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("Selling Price must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }
                    //if (parseFloat(sWholeSalePrice1) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[7].getElementsByTagName("input")[0].value = row.cells[7].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false;  
                    //    validation = true;
                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //} 
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;  
                row.cells[8].getElementsByTagName("input")[0].select(); 

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice2_TextChanged(lnk) { 
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1; 
                var sWholeSalePrice2 = row.cells[8].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sNetCost = row.cells[4].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice2 != "") {
                    if (parseFloat(sWholeSalePrice2) > parseFloat(sMRP)) 
                    {
                        row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; 
                        validation = true;
                        Confirm("WPrice2 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice2_" + rowIndex);

                        return false;
                    }
                    else if (parseFloat(sSellingPrice) < parseFloat(sNetCost)) {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("Selling Price must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }
                    //if (parseFloat(sWholeSalePrice2) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; 
                    //    validation = true;
                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //} 
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true;  
                row.cells[9].getElementsByTagName("input")[0].select();  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            } 
        }

        function txtlblWPrice3_TextChanged(lnk) { 
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1; 
                var sWholeSalePrice3 = row.cells[9].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[5].innerHTML;
                var sNetCost = row.cells[4].innerHTML;
                var sSellingPrice = row.cells[6].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice3 != "") {
                    if (parseFloat(sWholeSalePrice3) > parseFloat(sMRP)) 
                    {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;  

                        Confirm("WPrice3 Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice3_" + rowIndex);

                        return false;
                    }
                    else if (parseFloat(sSellingPrice) < parseFloat(sNetCost)) {
                        row.cells[6].getElementsByTagName("input")[0].value = row.cells[6].getElementsByTagName("span")[0].innerHTML;
                        row.cells[0].getElementsByTagName("input")[0].checked = false;
                        validation = true;
                        Confirm("Selling Price must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtSellingPrice_" + rowIndex);

                        return false;
                    }
                    //if (parseFloat(sWholeSalePrice3) > parseFloat(sSellingPrice))  
                    //{
                    //    row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; 

                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_gvHeadQuatersDetails_txtlblWPrice3_" + rowIndex);

                    //    return;
                    //} 
                }
                row.cells[0].getElementsByTagName("input")[0].checked = true;  
                row.cells[6].getElementsByTagName("input")[0].select();  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        function txtSellingPrice_Keystroke(event, lnk) {
            debugger;
            var rowobj = $(lnk).parent().parent();  
            var row = lnk.parentNode.parentNode;  
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[7].getElementsByTagName("input")[0].select();
                    event.preventDefault();
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault(); 
            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtSellingPrice"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtSellingPrice"]').select();
                }
            }
            return false; 
        }

        function txtlblWPrice1_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;

            if (charCode == 13) {
                if (validation == false) {
                    row.cells[8].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice1"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice1"]').select();
                }
            }
            return false;
        }

        function txtlblWPrice2_Keystroke(event, lnk) {
            debugger;
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[9].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[7].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice2"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice2"]').select();
                }
            }
            return false;


        }
        function txtlblWPrice3_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode; 
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[6].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[6].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice3"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice3"]').select();
                }
            }
            return false; 
        }
        function Confirm(Mgs, object) { 
            popUpObjectForSetFocusandOpen = $(object); 
            ShowPopupMessageBoxandFocustoObject(Mgs);  
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
        <ContentTemplate>
            <div class="main-container">
                  <div class="breadcrumbs">
            <ul><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul></div>
                <div class="container-group-full">
                    <div class="top-batch-container" style="display: block">
                        <div class="left-batch-Loccontainer" style="overflow: auto; height: 115px; width: 250px;">
                            <div class="control-group-price-header">
                                Available Locations
                            </div>
                            <asp:CheckBox ID="chkAllLoc" runat="server" Text=" Select All" /><br />
                            <asp:CheckBoxList ID="chkListLoc" CssClass="cbLocation" runat="server">
                            </asp:CheckBoxList>
                        </div>
                        <div class="over_flowhorizontal">
                            <table id="tblBatchList" cellspacing="0" rules="all" border="1" class="fixed_header gid_BatchInfoAndModification">
                                <thead>
                                    <tr>
                                        <th scope="col">Select
                                        </th>
                                        <th scope="col">Item Code
                                        </th>
                                        <th scope="col">Name
                                        </th>
                                        <th scope="col">Batch No
                                        </th>
                                        <th scope="col">Cost
                                        </th>
                                        <th scope="col">MRP
                                        </th>
                                        <th scope="col">Sug Price
                                        </th>
                                        <th runat="server" id="thWPricehdr1" scope="col">WPrice1
                                        </th>
                                        <th runat="server" id="thWPricehdr2" scope="col">WPrice2
                                        </th>
                                        <th runat="server" id="thWPricehdr3" scope="col">WPrice3
                                        </th>
                                        <th scope="col">Old Sug Price
                                        </th>
                                        <th scope="col">Sug Date
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <div id="gvdhdQuatDet" runat="server" class="gridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 100px; width: 1195px; background-color: aliceblue;">
                                <asp:GridView ID="gvHeadQuatersDetails" runat="server" AutoGenerateColumns="False"
                                    ShowHeaderWhenEmpty="true" CssClass="gid_BatchInfoAndModification" ShowHeader="false" OnRowDataBound="gvHeadQuatersDetails_DataBound">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSingle" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Inventorycode" ItemStyle-CssClass="left_align" HeaderText="Item Code"></asp:BoundField>
                                        <asp:BoundField DataField="Description" ItemStyle-CssClass="left_align" HeaderText="Name"></asp:BoundField>
                                        <asp:BoundField DataField="BatchNo" ItemStyle-CssClass="left_align" HeaderText="Batch No"></asp:BoundField>
                                        <asp:BoundField DataField="Basiccost" ItemStyle-CssClass="right_align" HeaderText="Cost"></asp:BoundField>
                                        <asp:BoundField DataField="MRP" ItemStyle-CssClass="right_align" HeaderText="MRP"></asp:BoundField>
                                        <%--<asp:TemplateField HeaderText="Sug Price" ControlStyle-BackColor="#ff7f7f">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSellingPrice" runat="server" Text='<%# Eval("Sellingprice") %>' Style="text-align: right; padding-right: 5px;" onkeypress="return isNumberKeywithDecimal(event)"
                                                    onkeydown="return fncChangeSellingQty(this,'txtSellingPrice',event,'txtWPrice1','txtSellingPrice')" onchange="fncPriceChange(this);"
                                                    CssClass="grid-textbox"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="txtWPrice1" ControlStyle-BackColor="#ff7f7f">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWPrice1" runat="server" Text='<%# Eval("Wprice1") %>' Style="text-align: right; padding-right: 5px;" onkeypress="return isNumberKeywithDecimal(event)"
                                                    onkeydown="return fncChangeSellingQty(this,'txtWPrice1',event,'txtWPrice2','txtSellingPrice')" onchange="fncPriceChange(this);"
                                                    CssClass="grid-textbox"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="WPrice2" ControlStyle-BackColor="#ff7f7f">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWPrice2" runat="server" Text='<%# Eval("Wprice2") %>' Style="text-align: right; padding-right: 5px;" onkeypress="return isNumberKeywithDecimal(event)"
                                                    onkeydown="return fncChangeSellingQty(this,'txtWPrice2',event,'txtWPrice3','txtWPrice1')" onchange="fncPriceChange(this);"
                                                    CssClass="grid-textbox"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="WPrice3" ControlStyle-BackColor="#ff7f7f">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWPrice3" runat="server" Text='<%# Eval("Wprice3") %>' Style="text-align: right; padding-right: 5px;" onkeypress="return isNumberKeywithDecimal(event)"
                                                    onkeydown="return fncChangeSellingQty(this,'txtWPrice3',event,'txtWPrice3','txtWPrice2')" onchange="fncPriceChange(this);"
                                                    CssClass="grid-textbox"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Selling Price">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>'
                                                CssClass="hiddencol"></asp:Label>
                                            <asp:TextBox ID="txtSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>'
                                                onkeydown="return isNumberKeyWithDecimalNew(event)" onkeyup="return txtSellingPrice_Keystroke(event,this);" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                onchange="return txtSellingPrice_TextChanged(this)" CssClass="grid-textbox"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField> 
                                    <%--<asp:BoundField DataField="WPrice1" HeaderText="WPrice1"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="WPrice1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWPrice1" runat="server" Text='<%# Eval("WPrice1") %>' CssClass="hiddencol"></asp:Label>
                                            <asp:TextBox ID="txtlblWPrice1" runat="server" Text='<%# Eval("WPrice1") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                CssClass="grid-textbox" onkeyup="return txtlblWPrice1_Keystroke(event,this);" onchange="return txtlblWPrice1_TextChanged(this)"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>   
                                    <%--<asp:BoundField DataField="WPrice2" HeaderText="WPrice2"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="WPrice2">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWPrice2" runat="server" Text='<%# Eval("WPrice2") %>' CssClass="hiddencol"></asp:Label>
                                            <asp:TextBox ID="txtlblWPrice2" runat="server" Text='<%# Eval("WPrice2") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                CssClass="grid-textbox" onkeyup="return txtlblWPrice2_Keystroke(event,this)" onchange="return txtlblWPrice2_TextChanged(this)"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="WPrice3" HeaderText="WPrice3"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="WPrice3">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWPrice3" runat="server" Text='<%# Eval("WPrice3") %>' CssClass="hiddencol"></asp:Label>
                                            <asp:TextBox ID="txtlblWPrice3" runat="server" Text='<%# Eval("WPrice3") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                CssClass="grid-textbox" onkeyup="return txtlblWPrice3_Keystroke(event,this)" onchange="return txtlblWPrice3_TextChanged(this)"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:BoundField DataField="Sellingprice" ItemStyle-CssClass="right_align" HeaderText="Old Sug Price"></asp:BoundField>
                                        <asp:BoundField DataField="SuggSellingpricedate" ItemStyle-CssClass="left_align" HeaderText="Sug Date"></asp:BoundField>
                                        <asp:BoundField DataField="OWPrice1" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice1"></asp:BoundField>
                                        <asp:BoundField DataField="OWPrice2" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice2"></asp:BoundField>
                                        <asp:BoundField DataField="OWPrice3" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice3"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                    </div>
                    <div class="over_flowhorizontal">
                        <table id="tblLocList" cellspacing="0" rules="all" border="1" class="fixed_header gid_LocBatchInfoAndModification">
                            <thead>
                                <tr>
                                    <th scope="col">Loc
                                    </th>
                                    <th scope="col">Item Code
                                    </th>
                                    <th scope="col">Name
                                    </th>
                                    <th scope="col">Batch No
                                    </th>
                                    <th scope="col">Cost
                                    </th>
                                    <th scope="col">MRP
                                    </th>
                                    <th scope="col">Sug Price
                                    </th>
                                    <th runat="server" id="thWPricedet1" scope="col">WPrice1
                                    </th>
                                    <th runat="server" id="thWPricedet2" scope="col">WPrice2
                                    </th>
                                    <th runat="server" id="thWPricedet3" scope="col">WPrice3
                                    </th>
                                    <th scope="col">Sug Date
                                    </th>
                                    <th scope="col">Loc Price
                                    </th>
                                    <th scope="col">Loc Date
                                    </th>
                                    <th scope="col">Qty
                                    </th>
                                    <th scope="col">Old Sug Date
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <div id="gvOutDet" runat="server" class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 300px; width: 1558px; background-color: aliceblue;">
                            <asp:GridView ID="gvOutletDetails" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" CssClass="gid_LocBatchInfoAndModification" ShowHeader="false" OnRowDataBound="gvOutletDetails_DataBound">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="" />
                                <Columns>
                                    <asp:BoundField DataField="locationcode" ItemStyle-CssClass="left_align" HeaderText="Loc"></asp:BoundField>
                                    <asp:BoundField DataField="InventoryCode" ItemStyle-CssClass="left_align" HeaderText="Item Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" ItemStyle-CssClass="left_align" HeaderText="Name"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" ItemStyle-CssClass="left_align" HeaderText="Batch No"></asp:BoundField>
                                    <asp:BoundField DataField="UnitCost" ItemStyle-CssClass="right_align" HeaderText="Cost"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" ItemStyle-CssClass="right_align" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="SugSellingPrice" ItemStyle-CssClass="right_align" HeaderText="Sug Price"></asp:BoundField>
                                    <asp:BoundField DataField="Wprice1" ItemStyle-CssClass="right_align" HeaderText="WPrice1"></asp:BoundField>
                                    <asp:BoundField DataField="Wprice2" ItemStyle-CssClass="right_align" HeaderText="WPrice2"></asp:BoundField>
                                    <asp:BoundField DataField="Wprice3" ItemStyle-CssClass="right_align" HeaderText="WPrice3"></asp:BoundField>
                                    <asp:BoundField DataField="SugSellingPriceDate" ItemStyle-CssClass="left_align" HeaderText="Sug Date"></asp:BoundField>
                                    <asp:BoundField DataField="OutletSellingPrice" ItemStyle-CssClass="right_align" HeaderText="Loc Price"></asp:BoundField>
                                    <asp:BoundField DataField="OutletSellingPriceDate" ItemStyle-CssClass="left_align" HeaderText="Loc Date"></asp:BoundField>
                                    <asp:BoundField DataField="Qtyonhand" ItemStyle-CssClass="right_align" HeaderText="Qty"></asp:BoundField>
                                    <asp:BoundField DataField="SugSellingPriceDate" ItemStyle-CssClass="left_align" HeaderText="Old Sug Date"></asp:BoundField>
                                    <asp:BoundField DataField="OWPrice1" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice1"></asp:BoundField>
                                    <asp:BoundField DataField="OWPrice2" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice2"></asp:BoundField>
                                    <asp:BoundField DataField="OWPrice3" ItemStyle-CssClass="left_align; hiddencol"
                                                    HeaderStyle-CssClass="hiddencol" HeaderText="OWPrice3"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                    <div style="float: right; display: table">
                        <div class="control-button">
                            <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" OnClientClick="fncUpdate();return false;" Text='update (F4)'></asp:LinkButton>
                        </div>
                        <div class="control-button display_none">
                            <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue">Print</asp:LinkButton>
                        </div>
                        <div class="control-button">
                            <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue"
                                Text='<%$ Resources:LabelCaption,btnClear %>' OnClick="lnkClearAll_click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div>
        <div class="display_none">
            <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                <ContentTemplate>
                    <asp:Button ID="btnUpdate" runat="server" OnClick="lnkUpdate_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="hidItemcode" runat="server" Value="" />
            <asp:HiddenField ID="hidBatchNo" runat="server" Value="" />
        </div>

    </div>
</asp:Content>
