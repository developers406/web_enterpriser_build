﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPriceChangeRequest.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmPriceChangeRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .Barcode_fixed_headers td:nth-child(1), .Barcode_fixed_headers th:nth-child(1)
        {
            min-width: 50px;
            max-width: 50px;
        }
        .Barcode_fixed_headers td:nth-child(2), .Barcode_fixed_headers th:nth-child(2)
        {
            min-width: 100px;
            max-width: 100px;
        }
        
        .Barcode_fixed_headers td:nth-child(3), .Barcode_fixed_headers th:nth-child(3)
        {
            min-width: 350px;
            max-width: 350px;
        }
        .Barcode_fixed_headers td:nth-child(4), .Barcode_fixed_headers th:nth-child(4)
        {
            min-width: 150px;
            max-width: 150px;
        }
        .Barcode_fixed_headers td:nth-child(5), .Barcode_fixed_headers th:nth-child(5)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(6), .Barcode_fixed_headers th:nth-child(6)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(7), .Barcode_fixed_headers th:nth-child(7)
        {
            min-width: 130px;
            max-width: 130px;
        }
        .Barcode_fixed_headers td:nth-child(8), .Barcode_fixed_headers th:nth-child(8)
        {
            min-width: 130px;
            max-width: 130px;
        }
        
        .Barcode_fixed_headers td:nth-child(9), .Barcode_fixed_headers th:nth-child(9)
        {
            min-width: 300px;
            max-width: 300px;
        }
        .Barcode_fixed_headers td:nth-child(10), .Barcode_fixed_headers th:nth-child(10)
        {
            min-width: 320px;
            max-width: 320px;
        }
        .Barcode_fixed_headers td:nth-child(11), .Barcode_fixed_headers th:nth-child(11)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(12), .Barcode_fixed_headers th:nth-child(12)
        {
            min-width: 100px;
            max-width: 100px;
        }
        .Barcode_fixed_headers td:nth-child(13), .Barcode_fixed_headers th:nth-child(13)
        {
            display: none;
        }
        .Barcode_fixed_headers td:nth-child(14), .Barcode_fixed_headers th:nth-child(14)
        {
            display: none;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PriceChangeRequest');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PriceChangeRequest";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>


    <script type="text/javascript">

        $(function () {
            //$("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true });


            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtFromDate.ClientID %>").on('keydown', function () {
                return false;

            });

        });
    </script>
    <script type="text/javascript" language="Javascript">
        function pageLoad() {

        }
        
        function fncValidateAdd() {
            try {
                debugger;
                var itemcode = $('#<%=txtInventoryCode.ClientID %>').val();
                var itemdesc = $('#<%=txtItemNameAdd.ClientID %>').val();
                var ComPrice = $('#<%=txtCompetitorPrice.ClientID%>').val();
                if (itemcode == '' && itemdesc == '' && ComPrice == '') {
                    alert("Please Enter Details");
                    return false;
                }
                else if (ComPrice == '') {
                    alert("Please Enter Competitor Price");
                    $('#<%=txtCompetitorPrice.ClientID%>').focus();
                    return false;

                } else {
                    return true;
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function fncComPriceFocus(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {

                    $("#<%=lnkAdd.ClientID %>").focus();
                }
            }
            catch (err) {
                alert(err.message);
            }
        }
        //Get Inventory Code DropDownList
        function setItemcode(itemcode) {
            try {
                $('#<%=txtInventoryCode.ClientID %>').val(itemcode);
                $('#<%=txtInventoryCode.ClientID %>').focus();
            }
            catch (err) {
                alert(err.Message);
            }
        }
        //Get Inventory code
        function fncGetInventoryCode(event) {
            try {
                var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
                if (keyCode == 13) {
                    event.preventDefault();
                    fncGetInventoryCode_Master($("#<%=txtInventoryCode.ClientID %>"));
                    event.preventDefault();
                    event.stopPropagation();
                    return false;
                }
            }
            catch (err) {
                alert(err.Message);
            }
        }
        //Get BatchStatus
        function fncInventoryCodeEnterkey_Master() {
            try {
                fncGetBatchStatus_Master($('#<%=txtInventoryCode.ClientID%>').val(), $('#<%=hidisbatch.ClientID%>'));
            }
            catch (err) {
                alert(err.Message);
            }

        }

        //Get Inventory Detail
        function fncGetBatchStatus() {
            try {
                fncGetInventoryDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
            }
            catch (err) {
                alert(err.Message);
            }
        }

        //Assign values to text box
        function fncAssignInventoryDetailToControls_Master(inventorydata) {
            try {
                //alert(inventorydata[9]);
                $('#<%=txtItemNameAdd.ClientID%>').val(inventorydata[0]);
                $('#<%=hidvendorcode.ClientID %>').val(inventorydata[9]);


                if ($('#<%=hidisbatch.ClientID%>').val() == "1") {
                    fncGetBatchDetail_Master($('#<%=txtInventoryCode.ClientID%>').val());
                }
                else {
                    $('#<%=txtSellingPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtPomPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtCompetitorPrice.ClientID%>').val(inventorydata[5]);
                    $('#<%=txtMrp.ClientID%>').val(inventorydata[6]);
                    $('#<%=txtCompetitorPrice.ClientID%>').focus();

                }

            }
            catch (err) {
                alert(err.message);
            }
        }

        //Assign Batch values to text box
        function fncAssignbatchvaluestotextbox(batchno, mrp, sprice, expmonth, expyear, Basiccost, stock) {
            try {

                $('#<%=txtMrp.ClientID%>').val(mrp);
                $('#<%=txtSellingPrice.ClientID%>').val(sprice);
                $('#<%=txtPomPrice.ClientID%>').val(sprice);
                $('#<%=txtCompetitorPrice.ClientID%>').val(sprice);
                $('#<%=txtBatchno.ClientID%>').val(batchno);
                $('#<%=txtCompetitorPrice.ClientID%>').focus();

            }
            catch (err) {
                alert(err.Messagek);
            }
        }
        //        $(document).ready(function () {
        //           
        //        });


        function fncValidateDelete() {
            try {
                var gridtr = $("#<%= gvPriceChange.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();
                }
                else {
                    //alert($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked"));
                    if ($("#<%=gvPriceChange.ClientID %> [id*=chkSingle]").is(":checked") == '1') {

                        fncShowConfirmDeleteMessage1();
                    } else {
                        fncShowMessage();
                    }
                }

            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete1() {
            try {
                // alert("sdfg");
                $("#DisplayDelete").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessDeleteMessage() {
            try {
                //alert("message");
                InitializeDialogDelete1();
                $("#DisplayDelete").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }

        function fncCloseDeleteDialog() {
            try {
                $("#DisplayDelete").dialog('close');
                return true;

            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowConfirmDeleteMessage1() {
            try {
                InitializeDialogDeletes();
                $("#DeleteConfirm").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDeletes() {
            try {
                $("#DeleteConfirm").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteConfirm").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteConfirm").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }


        function EnableOrDisableDropdown(element, isEnable) {
            //alert(element[0]);
            //console.log(element);
            element[0].selectedIndex = 0;
            element.attr("disabled", isEnable);
            element.trigger("liszt:updated");

        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function fncShowAlertNoItemsMessage() {
            try {
                InitializeDialogAlertNoItems();
                $("#AlertNoItems").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseAlertNoItemsDialog() {
            try {
                $("#AlertNoItems").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialogAlertNoItems() {
            try {
                $("#AlertNoItems").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncValidateSave() {
            try {
                debugger;

                var gridtr = $("#<%= gvPriceChange.ClientID %> tbody tr").not('[class=pshro_GridDgnHeaderCellCenter],[class=Emptyidclassforselector]');
                var location = $("#<%= ddlLocationCode.ClientID %>");
                if (gridtr.length == '0') {
                    //AlertNoItems
                    fncShowAlertNoItemsMessage();

                } else {
                    if (location[0].selectedIndex == 0) {

                        debugger;
                        alert("Please Select Location");
                    }
                    else {
                        fncShowConfirmSaveMessage();

                    }
                }
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }


        //Show Popup After Save
        function fncShowMessage() {
            try {
                InitializeDialog();
                $("#SelectAny").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseDialog() {
            try {
                $("#SelectAny").dialog('close');
                return false;

            }
            catch (err) {
                alert(err.message);
            }
        }
        //Save Dialog Initialation
        function InitializeDialog() {
            try {
                $("#SelectAny").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmSaveMessage() {
            try {
                InitializeDialogConfirmSave();
                $("#ConfirmSaveDialog").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmSave() {
            try {
                $("#ConfirmSaveDialog").dialog('close');
            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncShowConfirmDeleteMessage() {
            try {
                InitializeDialogDelete();
                $("#DeleteStockUpdatePosting").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        function InitializeDialogDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function CloseConfirmDelete() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close');

            }
            catch (err) {
                alert(err.message);
            }
        }
        function Clear() {
            try {
                $("#DeleteStockUpdatePosting").dialog('close'); return false;
            }
            catch (err) {
                alert(err.message);
            }
        }
        function InitializeDialog1() {
            try {
                $("#StockUpdateSave").dialog({
                    autoOpen: false,
                    resizable: false,
                    height: 150,
                    width: 370,
                    modal: true
                });
            }
            catch (err) {
                alert(err.message);
            }
        }
        function fncShowSuccessMessage() {
            try {
                InitializeDialog1();
                $("#StockUpdateSave").dialog('open');
            }
            catch (err) {
                alert(err.message);
                console.log(err);
            }
        }
        //Close Save Dialog
        function fncCloseSaveDialog() {
            try {
                $("#StockUpdateSave").dialog('close');
                return true;
            }
            catch (err) {
                alert(err.message);
            }
        } 
        
    </script>
    <script type="text/javascript">
        function clearForm() {
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            //$(':checkbox, :radio').prop('checked', false);
            $("select").val(0);
            $("select").trigger("liszt:updated");


        }
        function fncClear() {
            $('#<%=txtInventoryCode.ClientID%>').val('');
            $('#<%=txtItemNameAdd.ClientID%>').val('');
            $('#<%=txtMrp.ClientID%>').val('');
            $('#<%=txtSellingPrice.ClientID%>').val('');
            $('#<%=txtPomPrice.ClientID%>').val('');
            $('#<%=txtCompetitorPrice.ClientID%>').val('');
            $('#<%=txtBatchno.ClientID%>').val('');
            $('#<%=txtRemarks.ClientID%>').val('');

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                 <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Price Change Utility</a><i class="fa fa-angle-right"></i></li>
                <li><a href="../Merchandising/frmPriceChangeRequestView.aspx">
                    <%=Resources.LabelCaption.lbl_PriceChangeRequest%></a> <i class="fa fa-angle-right">
                    </i></li>
                <li class="active-page">
                    <%=Resources.LabelCaption.lbl_PriceChangeRequestAdd%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>

            </ul>
        </div>
        <div class="container-group-full">
            <div class="top-container-im">
                <div class="container-control">
                    <div class="left-container-header">
                        Price Change Request
                    </div>
                    <div class="left-container-detail">
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lblPriceChangeReqCode %>'></asp:Label><span
                                        class="mandatory">*</span>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtPriceChangeRequestNo" onMouseDown="return false" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblRequestdate %>'></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="control-group-split">
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="LocationCodek" runat="server" Text='<%$ Resources:LabelCaption,lbl_location %>'></asp:Label> 
                                </div>
                                <div class="label-right">
                                    <asp:DropDownList ID="ddlLocationCode" runat="server" CssClass="form-control-res">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="control-group-right">
                                <div class="label-left">
                                    <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_CreatedBy %>'></asp:Label> 
                                </div>
                               <div class="label-right">
                                    <asp:TextBox ID="txtCreatedBy" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-left">
                                <div class="label-left">
                                    <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label><span
                                      ></span>
                                </div>
                               <div class="label-right">
                                    <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                    style="height: 350px">
                   <asp:GridView ID="gvPriceChange" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeaderWhenEmpty="true" RowStyle-BackColor="#edf1fe" OnRowDeleting="gv_OnRowDeleting"
                                OnRowDataBound="gv_OnRowDataBound" AlternatingRowStyle-BackColor="#c4ddff" CssClass="pshro_GridDgn"
                                EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Button" DeleteText="Remove" />
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label18" runat="server" Visible="false" Text="Select"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSingle" Visible="false" runat="server" Width="40px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Inventorycode" HeaderText="Code"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Item Description"></asp:BoundField>
                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                    <asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>
                                    <asp:BoundField DataField="PromotionPrice" HeaderText="Prom.Price"></asp:BoundField>
                                    <asp:BoundField DataField="CompetitorPrice" HeaderText="Competitor Price"></asp:BoundField>
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks"></asp:BoundField>
                                    
                                </Columns>
                            </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottom-purchase-container-add">
            <div class="bottom-purchase-container-add-Text">
                <div class="control-group-split">
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label61" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 5px">
                            <asp:TextBox ID="txtInventoryCode" runat="server" onkeydown="return fncGetInventoryCode(event)"
                                CssClass="form-control" Style="width: 100px"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 3%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label14" runat="server" Text=" "></asp:Label>
                        </div>
                        <div class="control-group-left" style="width: 3%">
                            <div class="label-right" style="width: 99%; margin-left: 1%">
                                <div class="container5">
                                    <%-- <asp:ImageButton ID="btnImage" runat="server" ImageUrl="../images/SearchImage.png"
                                            OnClick="btnImage_Click" Height="16px" Width="16px" Visible="False" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 20%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label71" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtItemNameAdd" runat="server" CssClass="form-control-res" onMouseDown="return false"
                                Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_SellingPrice %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtSellingPrice" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_PromotionPrice %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtPomPrice" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lbl_CompetitorPrice %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtCompetitorPrice" onkeypress="return isNumberKey(event)" onkeydown="fncComPriceFocus(event)"
                                runat="server" CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="lblBatchno" runat="server" Text='<%$ Resources:LabelCaption,lbl_BatchNo %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtBatchno" runat="server" onMouseDown="return false" CssClass="form-control-res"
                                Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 6%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label161" runat="server" Text='<%$ Resources:LabelCaption,lbl_MRP %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtMrp" runat="server" onkeypress="return isNumberKey(event)" onMouseDown="return false"
                                CssClass="form-control-res" Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-group-left" style="width: 8%">
                        <div class="label-left" style="width: 100%">
                            <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Remarks %>'></asp:Label>
                        </div>
                        <div class="label-right" style="width: 99%; margin-left: 1%">
                            <asp:TextBox ID="txtRemarks" runat="server"   CssClass="form-control-res"
                                Font-Bold="true"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%">
                        <div class="button-contol" style="float: right; padding-top: 0px; margin: 0px; clear: none;">
                            <div class="control-button">
                                <div class="label-left" style="width: 100%">
                                    <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                        OnClick="lnkAdd_Click" OnClientClick="return fncValidateAdd();"><i class="icon-play"></i></asp:LinkButton>
                                </div>
                            </div>
                            <%--<div class="control-button">
                            <asp:LinkButton ID="lnkDelete" runat="server" class="button-red-small"><i class="icon-play"></i>Delete</asp:LinkButton>
                        </div>--%>
                            <div class="control-button">
                                <div class="label-right" style="width: 100%">
                                    <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>'
                                        OnClientClick="clearForm();"><i class="icon-play"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="float: right; display: table">
            <div class="control-button">
                <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnSave %>'
                    OnClientClick="fncValidateSave();return false;" Width="150px"></asp:LinkButton>
            </div>
            <div class="control-button">
                <asp:LinkButton ID="LinkButton9" runat="server" class="button-blue" OnClick="lnkBtnDelete_Click"
                    Text='<%$ Resources:LabelCaption,btn_delete %>' Width="150px"></asp:LinkButton>
            </div>
            <%--<div class="control-button">
                        <asp:LinkButton ID="Check" runat="server" class="button-blue" OnClientClick="fncValidateCheckAll();return false;"
                            Text='<%$ Resources:LabelCaption,btn_SelectAll %>'></asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="Delete" runat="server" class="button-blue" OnClientClick="fncValidateDelete();return false;"
                            Text="Delete"></asp:LinkButton>
                    </div>--%>
            <%--<div class="control-button">
                                <asp:LinkButton ID="CheckAll" runat="server" class="button-blue" OnClick="lnkCheckAll" >Select All</asp:LinkButton>
                            </div>--%>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <div class="container-group-full" style="display: none">
            <div id="SelectAny">
                <p>
                    <%=Resources.LabelCaption.Alert_Select_Any%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="lnkbtnOk" runat="server" class="button-blue" OnClientClick="return fncCloseDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="DisplayDelete">
                <p>
                    <%=Resources.LabelCaption.Alert_Delete%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton7" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="DeleteConfirm">
                <p>
                    <%=Resources.LabelCaption.Alert_DeleteSure%>
                </p>
                <div style="width: 150px; margin: auto">
                    <div style="float: left">
                        <asp:LinkButton ID="lnkYes" runat="server" class="button-blue" OnClientClick="return CloseConfirmDelete()"
                            Text='<%$ Resources:LabelCaption,lblYes %>'> </asp:LinkButton>
                    </div>
                    <div style="float: right">
                        <asp:LinkButton ID="lnkNo" runat="server" class="button-blue" OnClientClick="return Clear()"
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="ConfirmSaveDialog">
                <p>
                    <%=Resources.LabelCaption.Alert_Confirm_Save%>
                </p>
                <div style="width: 150px; margin: auto">
                    <div style="float: left">
                        <asp:LinkButton ID="LinkButton3" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave()"
                            Text='<%$ Resources:LabelCaption,lblYes %>' OnClick="lnkConfirmSavebtnOk_Click"> </asp:LinkButton>
                    </div>
                    <div style="float: right">
                        <asp:LinkButton ID="LinkButton4" runat="server" class="button-blue" OnClientClick="return CloseConfirmSave() "
                            Text='<%$ Resources:LabelCaption,lblNo %>'> </asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="StockUpdatePosting">
                <p>
                    <%=Resources.LabelCaption.Alert_Delete%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue" OnClientClick="return fncCloseDeleteDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="StockUpdateSave">
                <p>
                    <%=Resources.LabelCaption.Alert_Save%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton2" runat="server" class="button-blue" OnClientClick="return fncCloseSaveDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk %>' OnClick="lnkbtnOk_Click"> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="display: none">
            <div id="AlertNoItems">
                <p>
                    <%=Resources.LabelCaption.Alert_No_Items%>
                </p>
                <div style="margin: auto; width: 100px">
                    <asp:LinkButton ID="LinkButton5" runat="server" class="button-blue" OnClientClick="return fncCloseAlertNoItemsDialog()"
                        Text='<%$ Resources:LabelCaption,lblOk%>'> </asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="container-group-full">
            <asp:HiddenField ID="hidisbatch" runat="server" Value="" />
            <asp:HiddenField ID="hiddatestatus" runat="server" Value="" />
            <asp:HiddenField ID="hidvendorcode" runat="server" Value="" />
        </div>
        <%--<div id="divrepeater" class="Barcode_fixed_headers">
            <table id="tblbulkbarcode" cellspacing="0" rules="all" border="1" width="100%">
                <thead>
                    <tr>
                       
                        <th scope="col">
                            S.No
                        </th>
                        <th scope="col">
                            Itemcode
                        </th>
                        <th scope="col">
                            Description
                        </th>
                        
                        <th scope="col">
                            Batch No
                        </th>
                        <th scope="col">
                            Selling Price
                        </th>
                        <th scope="col">
                            MRP
                        </th>
                            <th scope="col">
                            Promotion Price
                        </th>
                        <th scope="col">
                            CompetitorPrice
                        </th>
                        <th scope="col">
                            Remarks
                        </th>
                        
                    </tr>
                </thead>
                <asp:Repeater ID="rptrBulkBarcode" runat="server"  >
                    <HeaderTemplate>
                        <tbody id="BulkBarcodebody">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="BulkBarcoderow" runat="server">
                           
                            <td>
                                <asp:Label ID="lblSNo" runat="server" Text='<%# Eval("RowNumber") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblItemcode" runat="server" Text='<%# Eval("inventorycode") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtQty" runat="server" CssClass="barcode_Qty" Text='<%# Eval("Qty") %>'
                                    onblur="fncQtyFocusOut(this)" MaxLength="4" onkeydown="fncSetFocustoNextQty(event,this)" />
                            </td>
                            <td>
                                <asp:Label ID="lblBatchNo" runat="server" Text='<%# Eval("BatchNo") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSellingprice" runat="server" Text='<%# Eval("SellingPrice") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblMRP" runat="server" Text='<%# Eval("MRP") %>' />
                            </td>
                             <td>
                                <asp:Label ID="lblPromotionPrice" runat="server" Text='<%# Eval("PromotionPrice") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblCompeditorPrice" runat="server" Text='<%# Eval("CompetotorPrice") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' />
                            </td>
                             
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                    </FooterTemplate>
                </asp:Repeater>
                <tfoot>
                    <tr style="height: 370px" runat="server" id="emptyrow">
                        <td colspan="11" class="repeater_td_align_center">
                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>'
                                Text="No items to display" />
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>--%>
    </div>
</asp:Content>
