﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPromotionMnt.aspx.cs" EnableEventValidation="false" Inherits="EnterpriserWebFinal.Merchandising.frmPromotionMnt" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .body {
            background: #e6f2ff !important;
        }

        .buyqty {
            margin-left: -293px;
            width: 200px;
        }

        .getqty {
            margin-left: 145px;
            width: 200px;
            margin-top: -21px;
        }
    </style>
     <style type="text/css">
         .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
             min-width: 120px;
             max-width: 120px;
             text-align: center;
         }

         .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
             min-width: 150px;
             max-width: 150px;
             text-align: left;
         }

         .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
             min-width: 150px;
             max-width: 150px;
             text-align: left;
         }

         .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
             min-width: 140px;
             max-width: 140px;
             text-align: right;
         }

         .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
             min-width: 140px;
             max-width: 140px;
             text-align: right;
         }

         .grdLoad1 td:nth-child(1), .grdLoad1 th:nth-child(1) {
             min-width: 90px;
             max-width: 90px;
             text-align: center;
         }

         .grdLoad1 td:nth-child(2), .grdLoad1 th:nth-child(2) {
            display:none;
         }

         .grdLoad1 td:nth-child(3), .grdLoad1 th:nth-child(3) {
             min-width: 120px;
             max-width: 120px;
             text-align: left;
         }

         .grdLoad1 td:nth-child(4), .grdLoad1 th:nth-child(4) {
             min-width: 120px;
             max-width: 120px;
             text-align: left;
         }

         .grdLoad1 td:nth-child(5), .grdLoad1 th:nth-child(5) {
             min-width: 120px;
             max-width: 120px;
             text-align: left;
         }

         .grdLoad1 td:nth-child(6), .grdLoad1 th:nth-child(6) {
             min-width: 120px;
             max-width: 120px;
             text-align: right;
         }

         .grdLoad1 td:nth-child(7), .grdLoad1 th:nth-child(7) {
             min-width: 120px;
             max-width: 120px;
             text-align: right;
         }
     </style>
    <script src="../js/jsCommon.js" type="text/javascript"></script>
    <script type="text/javascript">
   var d1;
       var d2;
       var Opendate;
       var dur;
       function fncGetUrl() {
           fncSaveHelpVideoDetail('', '', 'PromotionMnt');
       }
       function fncOpenvideo() {

           document.getElementById("ifHelpVideo").src = HelpVideoUrl;

           var Mode = "PromotionMnt";
           var d = new Date($.now());
           Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
           d1 = new Date($.now()).getTime();



           $("#dialog-Open").dialog({
               autoOpen: true,
               resizable: false,
               height: "auto",
               width: 1093,
               modal: true,
               dialogClass: "no-close",
               buttons: {
                   Close: function () {
                       $(this).dialog("destroy");
                       var d = new Date($.now());
                       var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                       d2 = new Date($.now()).getTime();
                       var Diff = Math.floor((d2 - d1) / 1000);
                       //alert(Diff);
                       if (Diff >= 60) {
                           fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                       }

                   }
               }
           });
       }
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        $(function () {
            SetItemAutoComplete();
            fncDecimal();
        });


        ///Inventory Search 
        function SetItemAutoComplete() {
            $("[id$=txtItemcode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/WebService.asmx/fncGetInventorycode")%>',
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valitemcode: item.split('|')[1],
                                    valName: item.split('|')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.message);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.message);
                        }
                    });
                },
                open: function (event, ui) {
                    var $input = $(event.target);
                    var $results = $input.autocomplete("widget");
                    var scrollTop = $(window).scrollTop();
                    var top = $results.position().top;
                    var height = $results.outerHeight();
                    if (top + height > $(window).innerHeight() + scrollTop) {
                        newTop = top - height - $input.outerHeight();
                        if (newTop > scrollTop)
                            $results.css("top", newTop + "px");
                    }
                },
                focus: function (event, i) {
                    $('#<%=txtItemcode.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtDescription.ClientID %>').val($.trim(i.item.valName));
                    fncItemFocusOut();
                    //$('#<%=txtQty.ClientID %>').focus();
                    //controlEnter($('#<%=txtDescription.ClientID %>'), 13, true);
                    return false;
                },
                minLength: 1
            });

            $(window).scroll(function (event) {
                $('.ui-autocomplete.ui-menu').position({
                    my: 'left bottom',
                    at: 'left top',
                    of: '#ContentPlaceHolder1_txtItemcode'
                });
            });
        }


        ///Disable and Enable         
        function DisableSlabPanel(isDisabled) {
            $("#<%=txtRangeFrom.ClientID%>").attr("disabled", isDisabled);
            $("#<%=txtRangeTo.ClientID%>").attr("disabled", isDisabled);
            $("#<%=txtDiscountAdd.ClientID%>").attr("disabled", isDisabled);
            $('#divValue').css('margin-top', '0px');

            if ($('#<%= ddlType.ClientID %>').val() == "Free Item") {
                $("#<%=cbPercentage.ClientID%>").attr("disabled", true);
                $("#<%=chkSPrice.ClientID%>").attr("disabled", true);
                $("#<%=chkMrp.ClientID%>").attr("disabled", true);
                $("#<%=txtBillValue.ClientID%>").attr("disabled", false);
                $("#<%=txtDiscount.ClientID%>").attr("disabled", true);
                $("#<%=txtDiscount.ClientID%>").val("0.00");
                $("#<%=cbPercentage.ClientID%>").attr("checked", false);
                $("#<%=chkSPrice.ClientID%>").attr("checked", false);
                $("#<%=chkMrp.ClientID%>").attr("checked", false);
                if (!$('#<%=lnkRangeAdd.ClientID %>').is(":visible"))
                    $('#divValue').css('margin-top', '10px');
            }
            else if ($('#<%= ddlType.ClientID %>').val() == "Discount") {
                $("#<%=cbPercentage.ClientID%>").attr("disabled", false);
                $("#<%=chkSPrice.ClientID%>").attr("disabled", false);
                $("#<%=chkMrp.ClientID%>").attr("disabled", false);
                $("#<%=chkSPrice.ClientID%>").attr("checked", true);
                $("#<%=txtBillValue.ClientID%>").attr("disabled", false);
                $("#<%=txtDiscount.ClientID%>").attr("disabled", false);
                $('#divValue').css('margin-top', '0px');
            }
            else {
                $("#<%=cbPercentage.ClientID%>").attr("disabled", true);
                $("#<%=chkSPrice.ClientID%>").attr("disabled", true);
                $("#<%=chkMrp.ClientID%>").attr("disabled", true);
                $("#<%=txtBillValue.ClientID%>").attr("disabled", false);
                $("#<%=txtDiscount.ClientID%>").attr("disabled", true);
                $("#<%=txtDiscount.ClientID%>").val("0.00");
                $("#<%=cbPercentage.ClientID%>").attr("checked", false);
                $("#<%=chkSPrice.ClientID%>").attr("checked", false);
                $("#<%=chkMrp.ClientID%>").attr("checked", false);
                if (!$('#<%=lnkRangeAdd.ClientID %>').is(":visible"))
                    $('#divValue').css('margin-top', '10px');
            }
        }

        ///Disable and Enable 
        function DisableFreeItemPanel(isDisabled) {
            try {
                // alert("DisableFreeItemPanel");
                $("#<%=txtItemcode.ClientID%>").attr("disabled", isDisabled);
                $("#<%=ddlBactno.ClientID%>").attr("disabled", isDisabled);
                $('#<%=ddlBactno.ClientID %>').trigger("liszt:updated");
                $("#<%=txtDescription.ClientID%>").attr("disabled", isDisabled);
                $("#<%=txtQty.ClientID%>").attr("disabled", isDisabled);
                $("#<%=txtDisFreeItem.ClientID%>").attr("disabled", isDisabled);
                $('#imgsearch').prop('disabled', isDisabled);
                if ($('#<%= ddlType.ClientID %>').val() == "RangeDis") {
                    $('#<%= txtQty.ClientID %>').val('1');
                $("#<%=txtQty.ClientID%>").attr("disabled", true);
                $("#<%=txtDisFreeItem.ClientID%>").attr("disabled", isDisabled);
            }
            else {
                $("#<%=txtDisFreeItem.ClientID%>").attr("disabled", true);
                $('#<%= txtQty.ClientID %>').val('0');
                $("#<%=txtQty.ClientID%>").attr("disabled", isDisabled);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncdatepickermodify() {
            try {
                //alert('test');
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncdatepicker() {
            try {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function pageLoad() {
        //$("select").chosen({ width: '100%' }); // width in px, %, em, etc
        //alert('<%=this.IsPostBack%>');
        //            $("#<%=txtPromotionCode.ClientID%> input").attr("disabled", true);
            //fnSaveandModify();
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" });
            if ($('#<%= ddlType.ClientID %>').val() != "RangeDis")
                $("#<%=txtDisFreeItem.ClientID%>").attr("disabled", true);

            $("#<%=txtPromotionCode.ClientID%>").attr("disabled", true);
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkSave.ClientID %>').css("display", "block");
        }
        else {
            $('#<%=lnkSave.ClientID %>').css("display", "none");
            }
            $('#<%=hidOldPrCode.ClientID %>').val($('#<%=txtPromotionCode.ClientID %>').val());

            fncNumberFormat();
            $("#promotionsave").dialog({
                autoOpen: false,
                resizable: false,
                height: 150,
                width: 370,
                modal: true
            });

            try {
                $('#imgsearch').prop('disabled', true);
                load();
            }
            catch (err) {
                err.Message;
            }
        }

        function fncLocationClick() {
            try {
                $("[id*=cblLocation] input").attr("checked", "checked");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Show Popup After Save
        function fncShowSaveNumber() {
            try {
                $("#promotionsave").dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //        function fncSaveOk() {
        //            try {
        //                //$("#promotionsave").dialog('close');
        //                window.location.replace('frmPromotionMnt.aspx');             
        //            }
        //            catch (err) {
        //                ShowPopupMessageBox(err.message);
        //            }
        //        }
        //Select All Check Box in CheckBox List
        $(function () {
            try {
                $("[id*=cbSelectAll]").bind("click", function () {
                    if ($(this).is(":checked")) {
                        $("[id*=cblLocation] input").attr("checked", "checked");
                    } else {
                        $("[id*=cblLocation] input").removeAttr("checked");
                    }
                });
                $("[id*=cblLocation] input").bind("click", function () {
                    if ($("[id*=cblLocation] input:checked").length == $("[id*=cblLocation] input").length) {
                        $("[id*=cbSelectAll]").attr("checked", "checked");
                    } else {
                        $("[id*=cbSelectAll]").removeAttr("checked");
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        });

    //        //Drown Dowp Focus Change
    //        $('#ContentPlaceHolder1_ddlBactno_chzn').on('onkeydown', function () {
    //            alert(01);
    //            $('#<%=txtQty.ClientID %>').focus();
        //        });
        //        
        function fncBatchChange() {
            try {

                $('#<%=txtQty.ClientID %>').focus();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncBasisChange() {
            fncAdjustText("Basis");
        }
        function fncTypeChange() {
            fncAdjustText("Type");
        }
        function fncModeChange() {
            fncAdjustText("Mode");
        }

        function fncAdjustText(Code) {

            var BillValue = "Bill Value";
            var Discount = "Discount";
            var Fixed = "Fixed";
            var Quantity = "Quantity";
            var FreeItem = "Free Item";
            var Quantity = "Quantity";
            var Slab = "Slab";
            var RangeDis = "RangeDis";

            try {
                var BasisVal = $('#<%= ddlBasis.ClientID %>').val();
                var TypeVal = $('#<%= ddlType.ClientID %>').val();
                var ModeVal = $('#<%= ddlMode.ClientID %>').val();

                var ProRows = $("#ContentPlaceHolder1_aspxPromotion_DXMainTable tr.dxgvDataRow_Aqua").length;
                var FreeRows = $("#ContentPlaceHolder1_aspxgvFreeItem_DXMainTable tr.dxgvDataRow_Aqua").length;

                if (FreeRows > 0 || ProRows > 0) {
                    if (Code == "Type") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_PromotionGrid%>');

                        if (TypeVal == Discount)
                            $('#<%= ddlType.ClientID %>')[0].selectedIndex = 1;
                        else
                            $('#<%= ddlType.ClientID %>')[0].selectedIndex = 0;

                        $('#<%= ddlType.ClientID %>').trigger("liszt:updated");

                        return false;
                    }
                    else if (Code == "Mode") {
                        ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_PromotionGrid%>');

                        if (ModeVal == Fixed)
                            $('#<%= ddlMode.ClientID %>')[0].selectedIndex = 1;
                        else
                            $('#<%= ddlMode.ClientID %>')[0].selectedIndex = 0;

                        $('#<%= ddlMode.ClientID %>').trigger("liszt:updated");

                        return false;
                    }
                }


                //Fixed Calculation
                if (BasisVal == BillValue && TypeVal == Discount && ModeVal == Fixed) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQDisFixRF();
                    return false;
                }
                else if (BasisVal == Quantity && TypeVal == Discount && ModeVal == Fixed) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblQuantity%>');
                    fncBQDisFixRF();
                    return false;
                }
                else if (BasisVal == BillValue && TypeVal == FreeItem && ModeVal == Fixed) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQFreeFixRF();
                    return false;
                }
                else if (BasisVal == Quantity && TypeVal == FreeItem && ModeVal == Fixed) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblQuantity%>');
                    fncBQFreeFixRF();
                    return false;
                }
                //Slab Calculation
                else if (BasisVal == BillValue && TypeVal == Discount && ModeVal == Slab) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQDisSlab();
                    return false;
                }
                else if (BasisVal == Quantity && TypeVal == Discount && ModeVal == Slab) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblQuantity%>');
                    fncBQDisSlab();
                    return false;
                }
                else if (BasisVal == BillValue && TypeVal == FreeItem && ModeVal == Slab) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQFreeSlab();
                    return false;
                }
                else if (BasisVal == Quantity && TypeVal == FreeItem && ModeVal == Slab) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblQuantity%>');
                    fncBQFreeSlab();
                    return false;
                }
                else if (BasisVal == BillValue && TypeVal == RangeDis && ModeVal == Slab) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQFreeSlab();
                    return false;
                }
                else if (BasisVal == BillValue && TypeVal == RangeDis && ModeVal == Fixed) {
                    $('#<%=lblBillValue.ClientID%>').html('<%=Resources.LabelCaption.lblBillValue%>');
                    fncBQFreeFixRF();
                    return false;
                }
                else if (BasisVal == Quantity && TypeVal == RangeDis) {
                    $('#<%= ddlBasis.ClientID %>')[0].selectedIndex = 0;
                    $('#<%= ddlBasis.ClientID %>').trigger("liszt:updated");
                    ShowPopupMessageBox("This Promotion only applied on Bill Value");
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        /// Disable and Enable Discount/Fixed
        function fncBQDisFixRF() {
            try {
                //Range Clear
                RangeClear();
                //Enable
                $('#<%=txtDiscount.ClientID %>').removeAttr("disabled");
                //Disable 
                $('#<%=lnkRangeAdd.ClientID %>').css("display", "block");
                DisableSlabPanel(true);
                DisableFreeItemPanel(true);

                $("#<%=ddlBactno.ClientID%>").attr("disabled", true);
                $('#<%= txtBillValue.ClientID %>').focus();

                fncDisableandEnable('fixed');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
    //document.getElementById("<%=txtBillValue.ClientID %>").focus();
            return false;
        }
        /// Disable and Enable Free/Fixed
        function fncBQFreeFixRF() {
            try {
                //Range Clear
                RangeClear();
                //ToEnable 
                DisableFreeItemPanel(false);
                $('#<%=ddlBactno.ClientID %>').trigger("liszt:updated");
        //$('#<%=ddlBactno.ClientID %>').removeAttr("disabled");
                //ToDisable
                $('#<%=txtDiscount.ClientID %>').attr("disabled", "disabled");
                $('#<%=lnkRangeAdd.ClientID %>').css("display", "block");
                DisableSlabPanel(true);
                $('#<%= txtBillValue.ClientID %>').focus();
                fncDisableandEnable('fixed');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
    //document.getElementById("<%=txtBillValue.ClientID %>").focus();
            return false;
        }



        /// Disable and Enable Discount/Slab
        function fncBQDisSlab() {
            try {
                fncFixClear();
                $('#<%=lnkRangeAdd.ClientID %>').css("display", "block");
                DisableSlabPanel(false);
                DisableFreeItemPanel(true);
                $('#<%= txtRangeFrom.ClientID %>').focus();
                fncDisableandEnable('slab');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

            return false;
        }
        function fncBQFreeSlab() {
            try {
                //Fixed Value Clear
                fncFixClear();
                //Enable
                $('#<%=txtDiscount.ClientID %>').removeAttr("disabled");
                $('#<%=lnkRangeAdd.ClientID %>').css("display", "none");
                DisableSlabPanel(false);
                DisableFreeItemPanel(false);
                //Disable   
                $('#<%=txtDiscountAdd.ClientID %>').attr("disabled", true);
                $('#<%= txtRangeFrom.ClientID %>').focus();

                fncDisableandEnable('slab');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
            return false;
        }

        function fncDisableandEnable(Satus) {
            try {
                if (Satus == "slab") {
                    //Disable 
                    $('#<%= txtBillValue.ClientID %>').attr("disabled", true);
                    $('#<%= txtDiscount.ClientID %>').attr("disabled", true);
                    //Enabled
                    $('#<%= txtRangeFrom.ClientID %>').attr("disabled", false);
                    $('#<%= txtRangeTo.ClientID %>').attr("disabled", false);
            //$('#<%= txtDiscountAdd.ClientID %>').attr("disabled", false);
                }
                else {
                    //Disable 
                    if ($('#<%= ddlType.ClientID %>').val() == "Free Item" || $('#<%= ddlType.ClientID %>').val() == "RangeDis") {
                        $('#<%= txtBillValue.ClientID %>').attr("disabled", false);
                $('#<%= txtDiscount.ClientID %>').attr("disabled", true);
                $('#<%= txtBillValue.ClientID %>').focus();
            }
            else {
                $('#<%= txtBillValue.ClientID %>').attr("disabled", false);
                $('#<%= txtDiscount.ClientID %>').attr("disabled", false);
                    }
                    //Enabled
                    $('#<%= txtRangeFrom.ClientID %>').attr("disabled", true);
                    $('#<%= txtRangeTo.ClientID %>').attr("disabled", true);
                    $('#<%= txtDiscountAdd.ClientID %>').attr("disabled", true);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        function fncFreeItemAddVal(e) {
            try {
                //fncCheckJavaScript();
                var ProRows = $("#ContentPlaceHolder1_aspxPromotion_DXMainTable tr.dxgvDataRow_Aqua").length;
                $("#hidFreeadd").val('Y');
                var min = "-";
                if ($('#<%= ddlMode.ClientID %>').val() == "Fixed") {
                    if ($('#<%= txtBillValue.ClientID %>').val() == "") {
                        if ($('#<%=lblBillValue.ClientID%>').text() == '<%=Resources.LabelCaption.lblBillValue%>') {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_EnterBillValue%>');
                    return false;
                }
                else {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_EnterQty%>');
                            return false;
                        }

                    }
                }
                else {
                    if ($('#<%= txtRangeFrom.ClientID %>').val() == "" || $('#<%= txtRangeTo.ClientID %>').val() == "")
                        if (ProRows == 0) {
                            ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_EnterRangeValue%>');
                            return false;
                        }
                }

                if ($('#<%= txtItemcode.ClientID %>').val() == "") {

                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_ItemCode%>');
            popUpObjectForSetFocusandOpen = $('#<%= txtItemcode.ClientID %>');
            return false;
        }
        else if ($('#<%= txtQty.ClientID %>').val() == "") {
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterQty%>');
            popUpObjectForSetFocusandOpen = $('#<%= txtQty.ClientID %>');
            return false;
        }
        else if ($('#<%= ddlType.ClientID %>').val() == "RangeDis" && ($('#<%= txtDisFreeItem.ClientID %>').val().trim() == "" ||
            parseFloat($('#<%= txtDisFreeItem.ClientID %>').val()) <= 0)) {
            ShowPopupMessageBoxandFocustoObject("Please Enter Valid Discount Percentage");
            popUpObjectForSetFocusandOpen = $('#<%= txtDisFreeItem.ClientID %>');
            return false;
        }
        else {
            var BillRange = $('#<%= txtBillValue.ClientID %>').val();
            BillRange = BillRange + min + $('#<%= txtBillValue.ClientID %>').val();
            var Itemcode = $('#<%= txtItemcode.ClientID %>').val();

            $("#ContentPlaceHolder1_aspxgvFreeItem_DXMainTable tr.dxgvDataRow_Aqua").each(function () {
                if (BillRange == $(this).find("td.Rangecss").html() && Itemcode == $(this).find("td.ItemCodecss").html()) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_AddedList%>');
                    FreeItemClear();
                    $("#hidFreeadd").val('N');
                    return false;
                }
            });
                }
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }

        function fncDiscAddValidation(e) {
            try {
                //fncCheckJavaScript();
                //                $("#hidDiscAdd").val('Y');
                var Status = "Y";
                if ($('#<%= txtRangeFrom.ClientID %>').val() == "" || $('#<%= txtRangeFrom.ClientID %>').val() == 0) {
                    popUpObjectForSetFocusandOpen = $('#<%= txtRangeFrom.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterRangeFrom%>');
                    Status = "N";
                }
                else if ($('#<%= txtRangeTo.ClientID %>').val() == "" || $('#<%= txtRangeTo.ClientID %>').val() == 0) {

                    popUpObjectForSetFocusandOpen = $('#<%= txtRangeTo.ClientID %>');
                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterRangeTo%>');
                    Status = "N";
                }
                else if (($('#<%= ddlType.ClientID %>').val() != "Free Item" && $('#<%= ddlMode.ClientID %>').val() != "Slab") || $('#<%= ddlType.ClientID %>').val() == "Discount") {
                    if ($('#<%= txtDiscountAdd.ClientID %>').val() == "" || $('#<%= txtDiscountAdd.ClientID %>').val() == 0) {

                        popUpObjectForSetFocusandOpen = $('#<%= txtDiscountAdd.ClientID %>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterDiscValue%>');
                        Status = "N";
                    }
                }
                if ($('#cbPercentage').prop('checked') == false) {
                    if (parseInt($('#<%= txtRangeFrom.ClientID %>').val()) < parseInt($('#<%= txtDiscountAdd.ClientID %>').val())) {

                popUpObjectForSetFocusandOpen = $('#<%= txtDiscountAdd.ClientID %>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_DiscAmtFrmRang%>');

                        Status = "N";
                    }
                }

                if ($("#ContentPlaceHolder1_aspxPromotion_DXMainTable tr.dxgvDataRow_Aqua").length > 0) {
                    var frmvalue = $('#<%= txtRangeFrom.ClientID %>').val();
            var ToValue = $('#<%= txtRangeTo.ClientID %>').val();
            var status;

            $("#ContentPlaceHolder1_aspxPromotion_DXMainTable tr.dxgvDataRow_Aqua").each(function () {
                if (frmvalue == $(this).find("td.ValueFromcss").html() && ToValue == $(this).find("td.ValueTocss").html()) {

                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_AddedList%>');
                RangeClear();
                Status = "N";
            }
        });
                }

                if (Status == "Y")
                    return true;
                else
                    return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
        }
<%--        function fnSaveandModify() {
            try{
                if($('#<%=txtBillValue.ClientID %>').attr("readonly") == true || $('#<%=txtDiscount.ClientID %>').attr("readonly") == true){
                    __doPostBack('ContentPlaceHolder1_lnkSave','OnClick')
                  
                }
                else{
                    fncSaveValidation(); 
                }
        }
        catch(err){
            ShowPopupMessageBox(err.message);
        
        }--%>

        /// Save Validation
        function fncSaveValidation() {
            var frmDate, toDate;
            var BillValue = "Bill Value";
            var Quantity = "Quantity";
            var Discount = "Discount";
            var Fixed = "Fixed";
            var Quantity = "Quantity";
            var FreeItem = "Free Item";
            var Slab = "Slab";
            var Zero = 0;
            var Hun = 100;

            var ProRows = $("#ContentPlaceHolder1_aspxPromotion_DXMainTable tr.dxgvDataRow_Aqua").length;
            var FreeRows = $("#ContentPlaceHolder1_aspxgvFreeItem_DXMainTable tr.dxgvDataRow_Aqua").length;


            try {
                frmDate = $('#<%=txtFromDate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

                //fncCheckJavaScript();
                if ($('#<%= ddlBasis.ClientID %>').val() == "Quantity") {
                    if ($('#<%=txtDepartment.ClientID %>').val() == "" && $('#<%=txtCategory.ClientID %>').val() == "" && $('#<%=txtBrand.ClientID %>').val() == "" &&
                        $('#<%=txtSubCategory.ClientID %>').val() == "" && $('#<%=txtClass.ClientID %>').val() == "" && $('#<%=txtSubClass.ClientID %>').val() == "") {
                        fncToastInformation("Select any One Filtration");
                        return false;
                    }
                }

                if ($('#<%= txtFromDate.ClientID %>').val() == "") {
                    //popUpObjectForSetFocusandOpen = $('#<%= txtFromDate.ClientID %>');
                    $('#<%= txtFromDate.ClientID %>').select();
                    fncToastInformation('<%=Resources.LabelCaption.Alert_FromDate%>');
                    return false;
                }
                else if ($('#<%= txtToDate.ClientID %>').val() == "") {
                    //popUpObjectForSetFocusandOpen = $('#<%= txtToDate.ClientID %>');
                    $('#<%= txtToDate.ClientID %>').select();
                    fncToastInformation('<%=Resources.LabelCaption.Alert_ToDate%>');
                    return false;
                }

                else if (new Date(frmDate) < new Date(currentDatesqlformat_Master)) {
                    fncToastInformation('<%=Resources.LabelCaption.alert_fromdategreaterthencurDate%>');
                    return false;
                }
                else if (new Date(toDate) < new Date(frmDate)) {
                    fncToastInformation('<%=Resources.LabelCaption.alert_TodategreaterthenFromDate%>');
                    return false;
                }
                else if ($(".cbLocation").find("input:checked").length == 0) {
                    fncToastInformation('<%=Resources.LabelCaption.Alert_Loc%>');
                    return false;
                }
                else if ($('#<%= txtPromoDescription.ClientID %>').val() == "") {
                    //popUpObjectForSetFocusandOpen = $('#<%= txtPromoDescription.ClientID %>');
                    $('#<%= txtPromoDescription.ClientID %>').select();
                    fncToastInformation('<%=Resources.LabelCaption.Alert_ProDec%>');
                    return false;
                }

                if ($('#<%= hidQueryString.ClientID %>').val() == "") {
                    //Discount and Validation
                    var ddlBasis = $("[id*=ddlBasis]");
                    var ddlType = $("[id*=ddlType]");
                    var ddlMode = $("[id*=ddlMode]");


                    if (ddlBasis.val() == BillValue) {
                        if (ddlType.val() == Discount && ddlMode.val() == Fixed) {

                            if ($('#<%= txtBillValue.ClientID %>').val() == "" || $('#<%= txtBillValue.ClientID %>').val() == Zero) {
                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterBillValue%>');
                                return false;
                            }

                            else if ($('#<%= txtDiscount.ClientID %>').val() == "" || $('#<%= txtDiscount.ClientID %>').val() == Zero) {
                                popUpObjectForSetFocusandOpen = $('#<%= txtDiscount.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterDisc%>');
                                return false;
                            }

                            else if ($('#<%= cbPercentage.ClientID %>').is(':checked')) {
                                if ($('#<%= txtDiscount.ClientID %>').val() > Hun) {
                                    popUpObjectForSetFocusandOpen = $('#<%= txtDiscount.ClientID %>');
                                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterDisc%>');
                                    return false;
                                }
                                else if (!($('#<%= chkSPrice.ClientID %>').is(':checked')) && !($('#<%= chkMrp.ClientID %>').is(':checked'))) {
                                    popUpObjectForSetFocusandOpen = $('#<%= chkSPrice.ClientID %>');
                                    ShowPopupMessageBoxandFocustoObject("Please Choose Selling Price or Mrp");
                                    return false;
                                }


                            }
                            else if ($('#<%= cbPercentage.ClientID %>').is(':checked') && $('#<%= ddlBasis.ClientID %>').val() != Quantity) {
                                if ($('#<%= txtBillValue.ClientID %>').val() < $('#<%= txtDiscountAdd.ClientID %>').val()) {

                                    popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                                    ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_DiscAmt%>');

                                    return false;
                                }
                            }
                        }
                        else if (ddlType.val() == FreeItem && ddlMode.val() == Fixed) {
                            if ($('#<%= txtBillValue.ClientID %>').val() == "" || $('#<%= txtBillValue.ClientID %>').val() == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterBillValue%>');
                                return false;
                            }
                            else if (aspxgvFreeItem.GetVisibleRowsOnPage == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtItemcode.ClientID %>')
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterFreeItemDetail%>');
                                return false;
                            }
                        }
                        else if (ddlType.val() == Discount && ddlMode.val() == Slab) {
                            if (ProRows == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterProSlabDet%>');
                                return false;
                            }
                        }
                        else if (ddlType.val() == FreeItem && ddlMode.val() == Slab) {
                            if (FreeRows == 0) {
                                popUpObjectForSetFocusandOpen = $('#<%= txtItemcode.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterProSlabDet%>');
                                return false;
                            }
                        }
                    }
                    else {
                        if (ddlType.val() == Discount && ddlMode.val() == Fixed) {
                            if ($('#<%= txtBillValue.ClientID %>').val() == "" || $('#<%= txtBillValue.ClientID %>').val() == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterQty%>');
                                return false;
                            }
                            else if ($('#<%= txtDiscount.ClientID %>').val() == "" || $('#<%= txtDiscount.ClientID %>').val() == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtDiscount.ClientID %>');
                                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterDisc%>');
                                return false;
                            }
                            else if ($('#<%= cbPercentage.ClientID %>').is(':checked')) {
                                if ($('#<%= txtBillValue.ClientID %>').val() > Hun) {

                                    popUpObjectForSetFocusandOpen = $('#<%= txtDiscount.ClientID %>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterDisc%>');

                                    return false;
                                }
                            }
                            else if (!$('#<%= cbPercentage.ClientID %>').is(':checked') && $('#<%= ddlBasis.ClientID %>').val() != Quantity) {
                                if ($('#<%= txtBillValue.ClientID %>').val() < $('#<%= txtDiscountAdd.ClientID %>').val()) {

                                    popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
                ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_DiscAmt%>');

                                    return false;
                                }
                            }
                        }
                        else if (ddlType.val() == FreeItem && ddlMode.val() == Fixed) {
                            if ($('#<%= txtBillValue.ClientID %>').val() == "" || $('#<%= txtBillValue.ClientID %>').val() == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterQty%>');

            return false;
        }
        else if (FreeRows == 0) {

            popUpObjectForSetFocusandOpen = $('#<%= txtItemcode.ClientID %>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterFreeItemDetail%>');
                                return false;
                            }
                        }
                        else if (ddlType.val() == Discount && ddlMode.val() == Slab) {
                            if (ProRows == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtBillValue.ClientID %>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterProSlabDet%>');

                                return false;
                            }
                        }
                        else if (ddlType.val() == FreeItem && ddlMode.val() == Slab) {
                            if (FreeRows == Zero) {

                                popUpObjectForSetFocusandOpen = $('#<%= txtItemcode.ClientID %>');
            ShowPopupMessageBoxandFocustoObject('<%=Resources.LabelCaption.Alert_EnterProSlabDet%>');
                                return false;
                            }
                        }
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
            return true;
        }

        //Qty Focus Out
        function fncQtyOut() {
            try {
                var ddlBactno = $('#<%= ddlBactno.ClientID %>').val();
                $("#hidBatchNo").val(ddlBactno);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function ClearAll() {
            try {
        //alert(document.getElementById("<%=txtPromoDescription.ClientID%>"));
                $('#<%= txtPromoDescription.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtSubCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtClass.ClientID %>').val('');
                $('#<%= txtSubClass.ClientID %>').val('');
                $('#<%= ddlBasis.ClientID %>').val('');
                $('#<%= ddlType.ClientID %>').val('');
                $('#<%= ddlMode.ClientID %>').val('');
                $('#<%= txtRangeFrom.ClientID %>').val('0');
                $('#<%= txtRangeTo.ClientID %>').val('0');
                $('#<%= txtDiscountAdd.ClientID %>').val('0');
                $('#<%= txtBillValue.ClientID %>').val('0');
                $('#<%= txtDiscount.ClientID %>').val('0');
                $('#<%= txtBuyQty.ClientID %>').val('0');
                $('#<%= txtGetQty.ClientID %>').val('0');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
            return false;


        }
        //FreeItem Clear
        function FreeItemClear() {
            try {
                $("select[id$=ddlBactno] > option").remove();
                $('#<%=ddlBactno.ClientID %>').trigger("liszt:updated");
                $('#<%= txtItemcode.ClientID %>').val('');
        //$('#<%= ddlBactno.ClientID %>').val('');
                $('#<%= txtDescription.ClientID %>').val('');
                $("#<%=txtDisFreeItem.ClientID%>").val('0');
                if ($('#<%= ddlType.ClientID %>').val() == "RangeDis") {
                    $('#<%= txtQty.ClientID %>').val('1');
            $("#<%=txtQty.ClientID%>").attr("disabled", true);
        }
        else {
            $('#<%= txtQty.ClientID %>').val('0');
            $("#<%=txtQty.ClientID%>").attr("disabled", false);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }
            return false;

    //            document.getElementById("<%=txtItemcode.ClientID%>").value = "";
    //            document.getElementById("<%=ddlBactno.ClientID%>").value = "";
    //            document.getElementById("<%=txtDescription.ClientID%>").value = "";
    //            document.getElementById("<%=txtQty.ClientID%>").value = "";
        }
        //Range Clear
        function RangeClear() {

            try {
                $('#<%= txtRangeFrom.ClientID %>').val('');
                $('#<%= txtRangeTo.ClientID %>').val('')
                $('#<%= txtDiscountAdd.ClientID %>').val('0');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                return false;
            }

            //            document.getElementById("<%=txtRangeFrom.ClientID%>").value = "";
            //            document.getElementById("<%=txtRangeTo.ClientID%>").value = "";
            //            document.getElementById("<%=txtDiscountAdd.ClientID%>").value = "0";
        }
        //Fixed Clear        
        function fncFixClear() {
            try {
                $('#<%= txtBillValue.ClientID %>').val('');
                $('#<%= txtDiscount.ClientID %>').val('');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Juery For Get Inventory Record
        function fncGetInventory(evt) {
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    var sInvCode = $('#<%=txtItemcode.ClientID%>').val();
                    $('#<%=txtQty.ClientID%>').focus();
                    fncInventoryDetail(sInvCode);
                    return false;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Get Inventory 
        function fncInventoryDetail(sInvCode) {
            try {
                $.ajax({
                    type: "POST",
                    url: "frmPromotionMnt.aspx/GetInventory",
                    data: "{'InvCode':'" + sInvCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $('#<%=txtItemcode.ClientID %>').val(msg.d[0]);
                        $('#<%=txtDescription.ClientID %>').val(msg.d[1]);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(err.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        //BatchNo Assign To Drop Down List(ddlBatchNo)
        function fncItemFocusOut() {
            try {
                var sInvCode = $('#<%=txtItemcode.ClientID%>').val();
                $.ajax({
                    type: "POST",
                    url: "frmPromotionMnt.aspx/GetBatchNo",
                    data: "{'sInvCode':'" + sInvCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        //var ddlBactno = $("[id*=ddlBactno]");
                        var ddlBactno = $('#<%=ddlBactno.ClientID%>');
                        //ddlBactno.empty().append('<option selected="selected" Value="0">Please select</option>');                        
                        $("select[id$=ddlBactno] > option").remove();
                        $.each(msg.d, function () {
                            ddlBactno.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });

                        <%--if ($('#<%=ddlBactno.ClientID %>').children('option').length > 1) {
                            $('#<%=ddlBactno.ClientID %>').trigger("liszt:open");                            
                        }
                        else {
                            $('#<%=txtQty.ClientID%>').focus();
                        }--%>
                        //$('#<%=ddlBactno.ClientID %>').trigger("liszt:open");                            
                        $('#<%=ddlBactno.ClientID %>').trigger("liszt:updated");
                        if ($('#<%= ddlType.ClientID %>').val() != "RangeDis")
                            $('#<%=txtQty.ClientID%>').select();
                        else
                            $('#<%=txtDisFreeItem.ClientID%>').select();


                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function load() {
            try {
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncDecimal() {
            try {
                $('#<%=txtRangeFrom.ClientID%>').number(true, 2);
                $('#<%=txtRangeTo.ClientID%>').number(true, 2);
                $('#<%=txtBillValue.ClientID%>').number(true, 2);
                $('#<%=txtDiscount.ClientID%>').number(true, 2);
                $('#<%=txtDiscountAdd.ClientID%>').number(true, 2);
                $('#<%=txtQty.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function funcPromocode() {
            try {
                var objData;
                var Content;
                //alert('test');
                $.ajax({
                    url: "frmPromotionMnt.aspx/fncGetPromoCode",
                    data: "{ 'prefix': '" + $("#<%=txtPromotionCode.ClientID %>").val() + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        objData = jQuery.parseJSON(msg.d);
                        //console.log(objData);
                        if (objData.length > 0) {
                            $('#<%=txtPromotionCode.ClientID%>').val(objData[0]["PrCode"]);
                           <%-- $('#<%=divBuyQty.ClientID%>').val(substr(0, 2)) + objData[0]["No"];
                           var from = $('#<%=divBuyQty.ClientID %>').val().substring(0, 2);
                           console.log(from);
                           var quantity = parseFloat(from) + objData[0]["No"];
                           $('#<%=divBuyQty.ClientID %>').val(quantity);--%>
                        }
                    },
                    error: function (msg) {
                        console.log(msg.message);
                    },
                    failure: function (msg) {
                        console.log(msg.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(function () {
            $("[id*=chBuyQty]").click(function () {
                if ($(this).is(":checked")) {
                    $('#<%=divBuyQty.ClientID%>').show();
                    $('#divPromotionCriteria').hide();
                    $('#divBillValue').hide();
                    $('#divGrid').hide();
                    $('#divAdd').hide();
                    $('#divItemcode').hide();
                    $('#divBatchNo').hide();
                    $('#divSave').hide();
                    $('#divDescription').hide();
                    $('#divQuantity').hide();
                    $('#divDes').show();
                    $('#divDate').show();
                    $('#divLocation').show();
                    $('#divFilteration').show();
                    $('#divPageload').show();
                    $('#divBuyQtyText').show();
                    $('#divSaveBuyQty').show();
                    $('#divGetQtyText').show();
                    funcPromocode();
                }
                else {
                    $('#<%=txtPromotionCode.ClientID%>').val($('#<%=hidOldPrCode.ClientID%>').val());
                    $('#<%=divBuyQty.ClientID%>').show();
                    $('#divPromotionCriteria').show();
                    $('#divBillValue').show();
                    $('#divGrid').show();
                    $('#divAdd').show();
                    $('#divItemcode').show();
                    $('#divBatchNo').show();
                    $('#divSave').show();
                    $('#divDescription').show();
                    $('#divQuantity').show();
                    $('#divDes').show();
                    $('#divDate').show();
                    $('#divLocation').show();
                    $('#divFilteration').show();
                    $('#divPageload').show();
                    $('#divBuyQtyText').hide();
                    $('#divSaveBuyQty').hide();
                    $('#divGetQtyText').hide();

                }
            });
        });

        function fncNumberFormat() {
            try {
                $("#<%=txtBuyQty.ClientID %>").number(true, 2);
                $("#<%=txtGetQty.ClientID %>").number(true, 2);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function clearForm() {
            $(':checkbox, :radio').prop('checked', false);
            $('#<%= txtPromoDescription.ClientID %>').val('');
            $('#<%= txtDepartment.ClientID %>').val('');
            $('#<%= txtCategory.ClientID %>').val('');
            $('#<%= txtSubCategory.ClientID %>').val('');
            $('#<%= txtBrand.ClientID %>').val('');
            $('#<%= txtClass.ClientID %>').val('');
            $('#<%= txtSubClass.ClientID %>').val('');
            $('#<%=txtPromoDescription.ClientID%>').val('');
        }
        function back() {
            try {
                window.location.href = "<%=ResolveUrl("~/frmPromotionMnt.aspx")%>";
                return false;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function ValidateDate() {
            // alert('test');
            var frmDate, toDate;
            try {
                //debugger;

            <%--var startDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
              var endDate = document.getElementById("<%=txtToDate.ClientID%>").value;


              if (startDate == "") {
                  ShowPopupMessageBox("Please Enter Start Date");
                  return false;
              }
              else if (endDate == "") {
                  ShowPopupMessageBox("Please Enter End Date");
                  return false;
              }
              else if (startDate > endDate) {
                  ShowPopupMessageBox("Please Enter End date should be greater than or equal to Start date");
                  return false;
              }--%>
                var startDate = document.getElementById("<%=txtFromDate.ClientID%>").value;
                var endDate = document.getElementById("<%=txtToDate.ClientID%>").value;

                if ((Date.parse(endDate) <= Date.parse(startDate))) {
                    ShowPopupMessageBox("Please Enter End date should be greater than or equal to Start date");
                    return false;
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncValidation() {
            try {
                if (("<%=txtGetQty.ClientID%>").val() > ("<%=txtBuyQty.ClientID%>").val()) {
                    ShowPopupMessageBox("Please Enter Get Quantity lesser than Buy Quantity");
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        $(function () {

            $("#<%=txtBillValue.ClientID %>").keypress(function (e) {

                if (e.keyCode == 13) {
                    if ($('#<%=txtDiscount.ClientID%>').is(':disabled')) {
                        $('#<%=txtItemcode.ClientID%>').focus();
                    }
                    else {
                        $('#<%=txtDiscount.ClientID%>').select();
                    }
                }

            });

        });


        function Pro_Delete(source) {
            $(source).parent().parent().remove();
        }
        function Free_Delete(source) {
            $(source).parent().parent().remove();
        }
    </script>
    <%-- <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
        });
    </script>--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-group-full">
            <div class="breadcrumbs" style="background: #e6f2ff">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                        <%=Resources.LabelCaption.lblHome%></a>  <i class="fa fa-angle-right"></i></li>
                    <li>Merchandising
                        <i class="fa fa-angle-right"></i></li>
                    <li>
                        <i class="fa fa-angle-right"></i></li>
                    <li>
                        <a href="../Merchandising/frmPromotion.aspx">Promotion</a>
                        <i class="fa fa-angle-right"></i></li>
                    <li class="active-page">
                        <%=Resources.LabelCaption.lblPromotionCreation%></li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul>
                <script src="../js/jsPromotionMnt.js" type="text/javascript"></script>
            </div>
        </div>
    <div style="background: #e6f2ff;display: block;" class="centerposition EnableScroll" id="divMain" runat="Server">
        
        <div class="container-group-full" id="divPageload">
            <div class="centerposition" style="width: 98%">
                <asp:Panel runat="server" ID="pnlHeader" BorderColor="Black" BorderStyle="Solid"
                    Height="86px" Width="100%" Style="float: left; padding: 5px" BorderWidth="1px">
                    <div class="Split3Position" id="divDes">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblPromotionCode" runat="server" Text='<%$ Resources:LabelCaption,lblPromotioCode %>'></asp:Label>
                        </div>
                        <asp:UpdatePanel ID="UpPromotionCode" runat="Server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="floatleft">
                                    <asp:TextBox ID="txtPromotionCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblDescription" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtPromoDescription" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div id="divBuyQty" runat="server">
                            <div class="floatleft" style="width: 30%">
                                <asp:Label ID="Label1" runat="server" Text='BuyQty'></asp:Label>
                            </div>
                            <div class="floatleft">
                                <asp:CheckBox ID="chBuyQty" runat="server" /><%--AutoPostBack="true" OnCheckedChanged="lnk_loadPromotionCode"--%>
                            </div>
                        </div>
                    </div>
                    <div class="Split3Position" id="divDate">

                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblFromDate" runat="server" Text='<%$ Resources:LabelCaption,lblFromDate %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblToDate" runat="server" Text='<%$ Resources:LabelCaption,lblToDate %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>

                    </div>
                    <div class="Split3Position" style="float: right" id="divLocation">
                        <div class="control-group-single">
                            <asp:CheckBox ID="cbSelectAll" runat="server" Text='<%$ Resources:LabelCaption,cbSelectAll %>' />
                        </div>
                        <div class="control-group-single">
                            <div style="overflow-y: auto; width: 75%; height: 40px">
                                <asp:CheckBoxList ID="cblLocation" CssClass="cbLocation" runat="server">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-group-full" id="divFilteration">
            <div class="centerposition" style="width: 98%">
                <div class="control-group-single" style="color: Purple; background: #e6ffff">
                    <asp:Label ID="lblFilteration" runat="server" Text='<%$ Resources:LabelCaption,lblFilteration %>'
                        Font-Bold="true" Font-Size="15px"></asp:Label>
                </div>
                <asp:Panel ID="pnlFilteration" runat="server" BorderStyle="solid" BorderColor="Black"
                    Height="75px" Style="padding: 5px" BorderWidth="1px">
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblDepartment" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtDepartment" runat="server"
                                CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtBrand');"></asp:TextBox>
                        </div>
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblBrand" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlBrand" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtCategory');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblCategory" runat="server" Text='<%$ Resources:LabelCaption,lblCategory %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlCategory" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtClass');"></asp:TextBox>
                        </div>
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblClass" runat="server" Text='<%$ Resources:LabelCaption,lblClass %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlClass" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtSubCategory');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblSubCategory" runat="server" Text='<%$ Resources:LabelCaption,lblSubCategory %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlSubCategory" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtSubClass');"></asp:TextBox>
                        </div>
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblSubClass" runat="server" Text='<%$ Resources:LabelCaption,lblSubClass %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <%--<asp:DropDownList ID="ddlSubClass" runat="server" Style="width: 200px">
                            </asp:DropDownList>--%>
                            <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'txtSubClass', '');"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-group-full" id="divPromotionCriteria">
            <div class="centerposition" style="width: 98%">
                <div class="control-group-single" style="color: Purple; background: #e6ffff">
                    <asp:Label ID="lblPromotionCritiria" runat="server" Text='<%$ Resources:LabelCaption,lblPromotionCritiria %>'
                        Font-Bold="true" Font-Size="15px"></asp:Label>
                </div>
                <asp:Panel ID="pnlPromotionCritiria" runat="server" BorderStyle="solid" BorderColor="Black"
                    Height="45px" Style="padding: 5px" BorderWidth="1px">
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblBasis" runat="server" Text='<%$ Resources:LabelCaption,lblBasis %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:DropDownList ID="ddlBasis" runat="server" Style="width: 100%" onchange="fncBasisChange()">
                                <asp:ListItem Text="Bill Value" Value="Bill Value"></asp:ListItem>
                                <asp:ListItem Text="Quantity" Value="Quantity"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblType" runat="server" Text='<%$ Resources:LabelCaption,lblType %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:DropDownList ID="ddlType" runat="server" Style="width: 100%" onchange="fncTypeChange()">
                                <asp:ListItem Text="Discount" Value="Discount"></asp:ListItem>
                                <asp:ListItem Text="Free Item" Value="Free Item"></asp:ListItem>
                                <asp:ListItem Text="Range Discount Items" Value="RangeDis"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblMode" runat="server" Text='<%$ Resources:LabelCaption,lblMode %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:DropDownList ID="ddlMode" runat="server" Style="width: 100%" onchange="fncModeChange()">
                                <asp:ListItem Text="Fixed" Value="Fixed"></asp:ListItem>
                                <asp:ListItem Text="Slab" Value="Slab"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-group-full" id="divBillValue">
            <div class="centerposition" style="width: 98%">
                <div class="control-group-single" style="color: Purple; background: #e6ffff">
                    <asp:Label ID="lblBillValue" runat="server" Text='<%$ Resources:LabelCaption,lblBillValue %>'
                        Font-Bold="true" Font-Size="15px"></asp:Label>
                </div>
                <asp:Panel ID="pnlRange" runat="server" BorderStyle="solid" BorderColor="Black" Height="75px"
                    Style="padding: 5px" BorderWidth="1px">
                    <div class="col-md-12" runat="server">
                        <div class="col-md-4">
                            <div class="col-md-6" style="width: 30%">
                                <asp:Label ID="lblRangeFrom" runat="server" Text='<%$ Resources:LabelCaption,lblRangeFrom %>'></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtRangeFrom" runat="server" CssClass="form-control-res-right"
                                    Enabled="False" onfocus="this.select();" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-6" style="width: 30%">
                                <asp:Label ID="lblrangeto" runat="server" Text='<%$ Resources:LabelCaption,lblRangeTo %>'></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtRangeTo" runat="server" CssClass="form-control-res-right" onfocus="this.select();" Text="0"
                                    Enabled="False"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-6" style="width: 30%">
                                <asp:Label ID="lblDiscount" runat="server" Text='<%$ Resources:LabelCaption,lblDiscount %>'></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtDiscountAdd" runat="server" CssClass="form-control-res-right" onfocus="this.select();" Text="0"
                                    Enabled="False"></asp:TextBox>
                                <%--<asp:HiddenField ID="hidDiscAdd" ClientIDMode="Static" runat="server" Value="" />--%>
                                <%--<asp:Button ID="btnDiscAdd" runat="server" OnClick="btnDiscAdd_Click1" Style="display: none" />--%>
                            </div>
                            <div class="floatleft" style="width: 50px; margin-left: 10px">
                                <asp:UpdatePanel ID="upDiscAdd" runat="Server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lnkRangeAdd" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnAdd %>'
                                            OnClick="lnkRangeAdd_Click" OnClientClick="return fncDiscAddValidation(this);"></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="divValue">
                        <div class="col-md-4">
                            <div class="col-md-6" style="width: 30%">
                                <asp:Label ID="lblFixedValue" runat="server" Text='<%$ Resources:LabelCaption,lblValueQty %>'></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtBillValue" runat="server" CssClass="form-control-res-right" onfocus="this.select();" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-6" style="width: 30%">
                                <asp:Label ID="lblFDiscount" runat="server" Text='<%$ Resources:LabelCaption,lblDiscount %>'></asp:Label>
                            </div>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control-res-right" onfocus="this.select();" Text="0"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-4">
                                <div class="floatleft">
                                    <asp:Label ID="lblPercentage" runat="server" Text='<%$ Resources:LabelCaption,lblPercentage %>'></asp:Label>
                                </div>
                                <div class="floatleft" style="float: right;">
                                    <asp:CheckBox ID="cbPercentage" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-4" style="display: none;">
                                <div class="floatleft">
                                    <asp:Label ID="Label5" runat="server" Text="S.Price"></asp:Label>
                                </div>
                                <div class="floatleft">
                                    <asp:RadioButton ID="chkSPrice" GroupName="BasedOn" Checked="true" runat="server" />
                                </div>
                            </div>
                            <div class="col-md-4" style="display: none;">
                                <div class="floatleft">
                                    <asp:Label ID="Label4" runat="server" Text="MRP"></asp:Label>
                                </div>
                                <div class="floatleft">
                                    <asp:RadioButton ID="chkMrp" GroupName="BasedOn" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-group-full">
            <div class="centerposition" style="width: 98%">
                <%--<asp:Panel ID="pnlFixedValue" runat="server">
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblFixedValue" runat="server" Text='<%$ Resources:LabelCaption,lblValueQty %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtBillValue" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Split3Position">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="lblFDiscount" runat="server" Text='<%$ Resources:LabelCaption,lblDiscount %>'></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control-res" onkeypress="return isNumberKey(event)"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>--%>
                <%--<div class="Split3Position">
                    <div class="floatleft" style="width: 30%">
                        <asp:Label ID="lblPercentage" runat="server" Text='<%$ Resources:LabelCaption,lblPercentage %>'></asp:Label>
                    </div>
                    <div class="floatleft">
                        <asp:CheckBox ID="cbPercentage" runat="server" />
                    </div>
                </div>--%>
            </div>
        </div>
        <div class="container-group-full" style="margin-top: 1%" id="divGrid">
            <div class="centerposition" style="width: 98%">
                <div class="col-md-6">
                    <asp:UpdatePanel ID="upPromotion" runat="Server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="grdLoad">
                                    <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Value From
                                                </th>
                                                <th scope="col">Value To
                                                </th>
                                                <th scope="col">Discount
                                                </th> 
                                                <th scope="col">Per(%)
                                                </th> 
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; background-color: aliceblue;">
                                        <asp:GridView ID="grdPromotion" runat="server" AllowSorting="true"
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" oncopy="return false" 
                                            AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad" DataKeyNames="ValueFrom">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" /> 
                                            <Columns>
                                                <asp:TemplateField HeaderText="Remove">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/images/No.png"
                                                            ToolTip="Click here to Delete" OnClick ="ibtnRemoveQI_Click"/>
                                                         <%--OnClientClick="Pro_Delete(this);  return false;"--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:BoundField DataField="ValueFrom" HeaderText="ValueFrom"></asp:BoundField>
                                                <asp:BoundField DataField="ValueTo" HeaderText="ValueTo"></asp:BoundField>
                                                <asp:BoundField DataField="Discount" HeaderText="Discount"   />
                                                <asp:BoundField DataField="Perc" HeaderText="Perc" ></asp:BoundField> 
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
               <div class="col-md-6"> 
                    <asp:UpdatePanel ID="upgvFreeItem" runat="Server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="grdLoad1">
                                    <table id="tblItemhistory1" cellspacing="0" rules="all" border="1" class="fixed_header">
                                        <thead>
                                            <tr>
                                                <th scope="col">Remove
                                                </th>
                                                <th scope="col">Range
                                                </th>
                                                <th scope="col">Item Code
                                                </th>
                                                <th scope="col">Description
                                                </th>
                                                <th scope="col">Batch No
                                                </th> 
                                                <th scope="col">Qty
                                                </th> 
                                                <th scope="col">Dis %
                                                </th> 
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px;   background-color: aliceblue;">
                                        <asp:GridView ID="grdFreeItem" runat="server" AllowSorting="true" 
                                            AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" oncopy="return false"
                                            AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad1" DataKeyNames="ItemCode">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" /> 
                                            <Columns>
                                                <asp:TemplateField HeaderText="Remove">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete1" runat="server" ImageUrl="~/images/No.png"
                                                            ToolTip="Click here to Delete"  OnClick ="btnRemoveFI_Click" />
                                                        <%--OnClientClick="Free_Delete(this);  return false;"--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:BoundField DataField="Range" HeaderText="Range"></asp:BoundField>
                                                <asp:BoundField DataField="ItemCode" HeaderText="ItemCode"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                                <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"/>
                                                <asp:BoundField DataField="Qty" HeaderText="Qty" ></asp:BoundField> 
                                                <asp:BoundField DataField="Dis" HeaderText="Dis" ></asp:BoundField> 
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="container-group-full" style="margin-top: 1%">
            <div class="centerposition" style="width: 100%">
                <div style="width: 100%">
                    <%--<asp:Panel ID="pnltxthide" runat="server" Enabled="true">--%>
                    <div class="container1" id="divItemcode">
                        <div class="spilt" align="center">
                            <asp:Label ID="lblItemCode" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                        </div>
                        <div class="spilt">
                            <asp:TextBox ID="txtItemcode" runat="server" CssClass="form-control-res" onkeydown=" return fncGetInventory(event);"
                                onblur="fncItemFocusOut()" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <%-- <div class="container5">
                        <input id="imgsearch" type="image" src="../images/SearchImage.png" onclick="return fncShowPopUp()"
                            height="20px" width="20px" name="button" />--%>
                    <%--<img alt="" id="imgsearch" src="../images/SearchImage.png" onclick="return fncShowPopUp()"
                            height="16px" width="16px" />--%>
                    <%-- <asp:ImageButton ID="btnImage" PostBackUrl="" runat="server" ImageUrl=""
                            Height="16px" Width="16px" OnClientClick="return fncShowPopUp()" />--%>
                    <%--</div>--%>
                    <div class="container1" id="divBatchNo">
                        <div class="spilt" align="center">
                            <asp:Label ID="lblBatchNo" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
                        </div>
                        <div class="spilt">
                            <asp:DropDownList ID="ddlBactno" runat="server" Style="width: 100%" onchange="return fncBatchChange()"
                                Enabled="False">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hidBatchNo" ClientIDMode="Static" Value="N" runat="server" />
                        </div>
                    </div>
                    <div class="container1" style="width: 30%" id="divDescription">
                        <div class="spilt" align="center">
                            <asp:Label ID="lblDescriptio" runat="server" Text='<%$ Resources:LabelCaption,lblDesc %>'></asp:Label>
                        </div>
                        <div class="spilt">
                            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control-res" Enabled="False"
                                onkeydown="return false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="container1" id="divDisPer">
                        <div class="spilt" align="center">
                            <asp:Label ID="Label6" runat="server" Text="Discount"></asp:Label>
                        </div>
                        <div class="spilt">
                            <asp:TextBox ID="txtDisFreeItem" onblur="fncQtyOut()" runat="server" CssClass="form-control-res-right" Text="0" onFocus="this.select()"></asp:TextBox>
                        </div>
                    </div>
                    <div class="container1" id="divQuantity">
                        <div class="spilt" align="center">
                            <asp:Label ID="lblQty" runat="server" Text='<%$ Resources:LabelCaption,lblQty %>'></asp:Label>
                        </div>
                        <div class="spilt">
                            <asp:TextBox ID="txtQty" runat="server" CssClass="form-control-res-right" Text="0" onFocus="this.select()"
                                onblur="fncQtyOut()" Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                    <div class="display_none" id="divBuyQtyText">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="Label2" runat="server" Text="BuyQty"></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtBuyQty" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control-res buyqty"></asp:TextBox>
                        </div>
                    </div>
                    <div class="display_none" id="divGetQtyText">
                        <div class="floatleft" style="width: 30%">
                            <asp:Label ID="Label3" runat="server" Text="GetQty"></asp:Label>
                        </div>
                        <div class="floatleft">
                            <asp:TextBox ID="txtGetQty" runat="server" onkeypress="return isNumberKey(event)" CssClass="form-control-res buyqty"></asp:TextBox>
                        </div>
                    </div>

                    <div style="float: right; margin-top: 1%">
                        <asp:Panel ID="plButton" runat="server">
                            <div class="control-button" id="divAdd">
                                <asp:UpdatePanel ID="upAdd" runat="Server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lnkAdd" runat="server" class="button-blue" OnClick="lnkAdd_Click"
                                            OnClientClick="return fncFreeItemAddVal(this)" Text='<%$ Resources:LabelCaption,btnAdd %>'> </asp:LinkButton>
                                        <asp:HiddenField ID="hidFreeadd" ClientIDMode="Static" runat="server" Value="N" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="control-button display_none" id="divBack">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" class="button-blue"
                                            OnClientClick="return back()" Text="Back"> </asp:LinkButton>
                                        <%--<asp:HiddenField ID="HiddenField1" ClientIDMode="Static" runat="server" Value="N" />--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClientClick="return FreeItemClear()"
                                    Text='Clear'></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClick="lnkbtnOk_Click"
                                    Text='<%$ Resources:LabelCaption,btnClearAll %>'></asp:LinkButton>
                            </div>
                            <div class="control-button display_none" id="divSaveBuyQty">
                                <asp:LinkButton ID="lnkBuy" runat="server" class="button-blue" OnClick="lnkBuyQtySave_Click" OnClientClick="return fncValidation();"
                                    Text='Save'> </asp:LinkButton>
                                <%----%>
                            </div>
                            <div class="control-button" id="divSave">
                                <asp:UpdatePanel ID="upSave" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_Click" OnClientClick="return fncSaveValidation();"
                                            Text='Save'> </asp:LinkButton>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" OnClick="lnkClose_Click"
                                    Text='Close'></asp:LinkButton>
                                <%--<asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" OnClientClick="fncCheckJavaScript()" />--%>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
        <div id="promotionsave">
            <p>
                <%=Resources.LabelCaption.SavePro%>
                <%=txtPromotionCode.Text%>
                <%=Resources.LabelCaption.SaveProUpdate%>
                <%--Resources.LabelCaption.SavePro + txtPromotionCode.Text + Resources.LabelCaption.SaveProUpdate--%>
            </p>
            <div style="margin: auto; width: 100px">
                <asp:LinkButton ID="lnkbtnOk" runat="server" OnClick="lnkbtnOk_Click" class="button-blue"
                    Text='<%$ Resources:LabelCaption,lblOk %>'> </asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <div id="dialog-MessageBoxProMnt" style="display: none">
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
            <asp:HiddenField ID="hidOldPrCode" runat="server" />
            <asp:HiddenField ID="hidFreeItemDis" runat="server" />
            <asp:HiddenField ID="hidQueryString" runat="server" Value="" />
        </div>
    </div>
</asp:Content>
