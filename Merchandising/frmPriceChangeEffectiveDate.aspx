﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="frmPriceChangeEffectiveDate.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmPriceChangeEffectiveDate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .gid_LastPurchase td:nth-child(1), .gid_LastPurchase th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
        }

        .gid_LastPurchase td:nth-child(2), .gid_LastPurchase th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
        }

        .gid_LastPurchase td:nth-child(3), .gid_LastPurchase th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(3) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(4), .gid_LastPurchase th:nth-child(4) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(4) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(5), .gid_LastPurchase th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(6), .gid_LastPurchase th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(6) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(7), .gid_LastPurchase th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(8), .gid_LastPurchase th:nth-child(8) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(8) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(9), .gid_LastPurchase th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(9) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(10), .gid_LastPurchase th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(10) {
            text-align: right !important;
        }

        .gid_LastPurchase td:nth-child(11), .gid_LastPurchase th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(12), .gid_LastPurchase th:nth-child(12) {
            min-width: 100px;
            max-width: 100px;
        }

        .gid_LastPurchase td:nth-child(13), .gid_LastPurchase th:nth-child(13) {
            display: none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) { 
             display: none;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 50px;
            max-width: 50px;
            text-align: center;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 70px;
            max-width: 70px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 245px;
            max-width: 245px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 60px;
            max-width: 60px;
            text-align: left !important;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 55px;
            max-width: 55px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 75px;
            max-width: 75px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 70px;
            max-width: 70px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 60px;
            max-width: 60px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            display: none;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            display: none;
        }

        .grdLoad td:nth-child(17), .grdLoad th:nth-child(17) {
            display: none;
        }

        .grdLoad td:nth-child(18), .grdLoad th:nth-child(18) {
            display: none;
        }

        .grdLoad td:nth-child(19), .grdLoad th:nth-child(19) {
            display: none;
        }

        .grdLoad td:nth-child(20), .grdLoad th:nth-child(20) {
            display: none;
        }

        .grdLoad td:nth-child(21), .grdLoad th:nth-child(21) {
            min-width: 95px;
            max-width: 95px;
        }
        .grdLoad td:nth-child(22), .grdLoad th:nth-child(22) {
            min-width: 95px;
            max-width: 95px;
           display: none;
        }

        .grdLoad td {
            padding: 2px;
        }

        .GridChange {
            height: 275px;
            width: 1110px;
            overflow-x: hidden;
        }
    </style>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'PriceChangeEffectiveDate');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "PriceChangeEffectiveDate";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtEffectiveDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true, minDate: "0" }).datepicker("setDate", "0");
        });
    </script>
    <script type="text/javascript">
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkUpdate.ClientID %>').css("display", "block");
                $('#<%=lnkBulkBatch.ClientID %>').css("display", "block");
                $('#<%=lnkDeactivatebatch.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkUpdate.ClientID %>').css("display", "none");
                $('#<%=lnkBulkBatch.ClientID %>').css("display", "none");
                $('#<%=lnkDeactivatebatch.ClientID %>').css("display", "none");
            }
            $('#<%=txtDis.ClientID%>').number(true, 2);
            if ($('#<%=hidWholeSale.ClientID%>').val() == "N") {
                $('.grdLoad td:nth-child(12)').css("display", "none");
                $('.grdLoad th:nth-child(12)').css("display", "none");
                $('.grdLoad td:nth-child(10)').css("display", "none");
                $('.grdLoad th:nth-child(10)').css("display", "none");
                $('.grdLoad td:nth-child(11)').css("display", "none");
                $('.grdLoad th:nth-child(11)').css("display", "none");
                $('.cssGrid').css("width", "990");
                $('.GridChange').css("width", "990");
                $('.fixed_headers').css("width", "80%");
            }
            else {
                $('.fixed_headers').css("width", "91%");
                $('.grdLoad').css({
                    'width': '990px',
                    'height': '320px',
                    'overflow-x': 'scroll',
                    'overflow-y': 'hidden'
                });
            }

            $('#<%=chkAllLoc.ClientID %>').click(
             function () {

                 //$("input[type='CheckBox']").attr('checked', $('#<%=chkAllLoc.ClientID %>').is(':checked'));
                 if (($('#<%=chkAllLoc.ClientID %>').is(":checked"))) { // dinesh
                     $("#ContentPlaceHolder1_chkListLoc input[type='CheckBox']").attr("checked", "checked");
                 }
                 else {
                     $("#ContentPlaceHolder1_chkListLoc input[type='CheckBox']").removeAttr("checked");
                 }

             });
        }
    </script>
    <script type="text/javascript">
        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '100%',
                        'margin-left': '0'
                    });
                    $('.grdLoad').css({
                        'width': '1200px',
                        'height': '320px',
                        'overflow-x': 'hidden',
                        'overflow-y': 'hidden'
                    });
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '74%',
                        'margin-left': '1%'
                    });
                    if ($('#<%=hidWholeSale.ClientID%>').val() != "N") {
                        $('.grdLoad').css({
                            'width': '990px',
                            'height': '320px',
                            'overflow-x': 'scroll',
                            'overflow-y': 'hidden'
                        });
                    }
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

    </script>
    <script type="text/javascript">
     <%-- function fncOpenFrame(sItemCode, sBatchNo) {
            var page = '<%=ResolveUrl("~/Merchandising/frmEffectivePricechangeRepots.aspx") %>';
            var page = page + "?ItemCode=" + sItemCode + "&BatchNo=" + sBatchNo
            var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                autoOpen: false,
                modal: true,
                height: 590,
                width: 1200,
                title: "Effective Price Information / Modification",
                buttons: [
                         {
                             text: "Close",
                             click: function () {
                                 $(this).dialog("close");
                             }
                         }
                ]
            });
            $dialog.dialog('open');
        }--%>

        /// Open DeActivate Batch
        function fncOpenDeativateBatch() {
            try {
                var page = '<%=ResolveUrl("~/Merchandising/frmBatchDeactivation.aspx") %>';
                var page = page + "?ItemCode=" + $("#<%=hidItemcode.ClientID%>").val();
                var $dialog = $('<div></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
                    autoOpen: false,
                    modal: true,
                    height: 630,
                    width: 1200,
                    title: "Batch Deactivation",
                    buttons: [
                        {
                            text: "Close",
                            click: function () {
                                $(this).dialog("close");
                            }
                        }
                    ]
                });
                $dialog.dialog('open');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

            return false;
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function disableFunctionKeys(e) {
            try {
                var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
                if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                    if (e.keyCode == 115) {
                        if ($('#<%=lnkUpdate.ClientID %>').is(":visible"))
                            __doPostBack('ctl00$ContentPlaceHolder1$lnkUpdate', '');
                        e.preventDefault();
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function Confirm(Mgs, object) {
            console.log(object);

            popUpObjectForSetFocusandOpen = $(object);

            ShowPopupMessageBoxandFocustoObject(Mgs);

        }
    </script>

    <script type="text/javascript">
        var validation = false; //ENTERKEY PASSING THROUGHT ROWS
        function txtSellingPrice_TextChanged(lnk) { //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sSellingPrice = row.cells[8].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[7].innerHTML;
                var sNetCost = row.cells[13].innerHTML;
                //var sNetCost = row.cells[9].innerHTML;
                if (sSellingPrice != "") {
                    if (parseFloat(sSellingPrice) > parseFloat(sMRP)) // SellingPrice > MRP
                    {
                        row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        Confirm("Selling Price must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtSellingPrice_" + rowIndex);

                        return;
                    }
                    if (parseFloat(sSellingPrice) < parseFloat(sNetCost)) {
                        row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                        validation = true;
                        Confirm("Selling Price must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_grdBatchPriceChange_txtSellingPrice_" + rowIndex);

                        return;
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False
                //row.cells[8].getElementsByTagName("input")[0].select(); // Focus to Next Row  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice1_TextChanged(lnk) {  //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {

                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sWholeSalePrice1 = row.cells[9].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[7].innerHTML;
                var sNetCost = row.cells[13].innerHTML;
                var sSellingPrice = row.cells[8].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice1 != "") {
                    if (parseFloat(sWholeSalePrice1) > parseFloat(sMRP)) // WholeSalePrice1 > MRP
                    {
                        row.cells[9].getElementsByTagName("input")[0].value = row.cells[9].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        Confirm("Wprice1 must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice1_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice1) > parseFloat(sSellingPrice)) // Selling Price > WholeSalePrice
                    //{
                    //    row.cells[8].getElementsByTagName("input")[0].value = row.cells[8].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                    //    validation = true;
                    //    Confirm("Wprice1 must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //}
                    if (parseFloat(sWholeSalePrice1) < parseFloat(sNetCost)) // WholeSalePrice1 > NetCost
                    {
                        row.cells[9].getElementsByTagName("input")[0].value = row.cells[9].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                        validation = true;
                        Confirm("Wprice1 must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice1_" + rowIndex);

                        return;
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False
                row.cells[10].getElementsByTagName("input")[0].select(); // Focus to Next Row  

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function txtlblWPrice2_TextChanged(lnk) { //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sWholeSalePrice2 = row.cells[10].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[7].innerHTML;
                var sNetCost = row.cells[13].innerHTML;
                var sSellingPrice = row.cells[8].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice2 != "") {
                    if (parseFloat(sWholeSalePrice2) > parseFloat(sMRP)) // WholeSalePrice2 > MRP
                    {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                        validation = true;
                        Confirm("Wprice2 must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice2_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice2) > parseFloat(sSellingPrice)) // Selling Price > WholeSalePrice
                    //{
                    //    row.cells[9].getElementsByTagName("input")[0].value = row.cells[9].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False
                    //    validation = true;
                    //    Confirm("Wprice2 must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice1_" + rowIndex);

                    //    return;
                    //}
                    if (parseFloat(sWholeSalePrice2) < parseFloat(sNetCost)) // WholeSalePrice1 > NetCost
                    {
                        row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    
                        validation = true;
                        Confirm("Wprice2 must be Greater than or Equal to Net Cost", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice2_" + rowIndex);

                        return;
                    }
                }
                validation = true;
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False
                row.cells[11].getElementsByTagName("input")[0].select(); // Focus to Next Row  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        function txtlblWPrice3_TextChanged(lnk) { //CHIDAMBARAM 13-03-2018 GRIDVIEW VALIDATION OF SELLING,W1,W2,W3,NETCODE AND FOCUS OF TEXT BOX
            try {
                var row = lnk.parentNode.parentNode;
                var rowIndex = row.rowIndex - 1;// Row Index
                var sWholeSalePrice3 = row.cells[11].getElementsByTagName("input")[0].value;
                var sMRP = row.cells[7].innerHTML;
                var sNetCost = row.cells[13].innerHTML;
                var sSellingPrice = row.cells[8].getElementsByTagName("input")[0].value;
                if (sWholeSalePrice3 != "") {
                    if (parseFloat(sWholeSalePrice3) > parseFloat(sMRP)) // WholeSalePrice2 > MRP
                    {
                        row.cells[11].getElementsByTagName("input")[0].value = row.cells[11].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False

                        Confirm("Wprice3 must be Lessar than or Equal to MRP", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice3_" + rowIndex);

                        return;
                    }
                    //if (parseFloat(sWholeSalePrice3) > parseFloat(sSellingPrice)) // Selling Price > WholeSalePrice
                    //{
                    //    row.cells[10].getElementsByTagName("input")[0].value = row.cells[10].getElementsByTagName("span")[0].innerHTML;//ReAssign Value if Wrong
                    //    row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False

                    //    Confirm("Please check WPrice1 Lessthan/Equalto MRP and WPrice1 Greaterthan NetCost", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice3_" + rowIndex);

                    //    return;
                    //}
                    if (parseFloat(sWholeSalePrice3) < parseFloat(sNetCost)) // WholeSalePrice1 > NetCost
                    {
                        row.cells[11].getElementsByTagName("input")[0].value = row.cells[11].getElementsByTagName("span")[0].innerHTML;  //ReAssign Value if Wrong
                        row.cells[0].getElementsByTagName("input")[0].checked = false; //Check Box False    

                        Confirm("Wprice2 must be Lessar than or Equal to Net Cost", "#ContentPlaceHolder1_grdBatchPriceChange_txtlblWPrice3_" + rowIndex);

                        return;
                    }
                }
                row.cells[0].getElementsByTagName("input")[0].checked = true; //Check Box False
                row.cells[8].getElementsByTagName("input")[0].select(); // Focus to Next Row  
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }

        }

        function txtSellingPrice_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent(); //GET CURRENT ROW FOR UP AND DOWN KEY PRESS
            var row = lnk.parentNode.parentNode; //GET CURRENT ROW FOR LEFT AND RIGHT KEY PRESS
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[9].getElementsByTagName("input")[0].select();
                    event.preventDefault();
                }
                else {
                    validation = false;
                }
            }
            if (charCode == 39) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[11].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtSellingPrice"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtSellingPrice"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtSellingPrice"]').select();
                }
            }
            return true;


        }

        function txtlblWPrice1_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;

            if (charCode == 13) {
                if (validation == false) {
                    row.cells[10].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[10].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice1"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice1"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice1"]').select();
                }
            }
            return true;


        }

        function txtlblWPrice2_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode == 13) {
                if (validation == false) {
                    row.cells[11].getElementsByTagName("input")[0].select();

                    event.preventDefault();
                }
                else {
                    validation = false;
                }

            }
            if (charCode == 39) {
                row.cells[11].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[9].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice2"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice2"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice2"]').select();
                }
            }
            return true;


        }
        function txtlblWPrice3_Keystroke(event, lnk) {
            var rowobj = $(lnk).parent().parent();
            var row = lnk.parentNode.parentNode;
            var rowIndex = row.rowIndex - 1;
            var charCode = (event.which) ? event.which : event.keyCode;


            if (charCode == 39) {
                row.cells[8].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 37) {
                row.cells[10].getElementsByTagName("input")[0].select();
                event.preventDefault();

            }
            if (charCode == 40) {

                var NextRowobj = rowobj.next();
                if (NextRowobj.length > 0) {
                    NextRowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    rowobj.siblings().first().find('td input[id*="txtlblWPrice3"]').select();

                }
            }

            if (charCode == 38) {
                var prevrowobj = rowobj.prev();
                if (prevrowobj.length > 0) {
                    prevrowobj.find('td input[id*="txtlblWPrice3"]').select();
                }
                else {
                    row.siblings().last().find('td input[id*="txtlblWPrice3"]').select();
                }
            }
            return true;
        }

        var glInvCode, glBatchNo, glNetCost = 0;
        var glSelectedRow;
        function fncShowMultiUOMPriceList(itemcode, batchno, NetCost) {
            var obj = {};
            try {
                //source = $(source);
                //glSelectedRow = source;
                obj.itemcode = itemcode;//$("td", source).eq(3).text();
                obj.batchno = batchno;//$("td", source).eq(5).text();

                glInvCode = itemcode;//$("td", source).eq(3).text();
                glBatchNo = batchno;//$("td", source).eq(5).text();
                glNetCost = NetCost;//$("td", source).eq(13).text();


                //if (source.find('td input[id*="chkSingle"]').is(":checked")) {
                //    fncBindMultipleUOMExisitingRecord();
                //    fncShowMultipUOM(glInvCode);
                //}
                //else {
                $.ajax({
                    type: "POST",
                    url: "frmPriceChangeEffectiveDate.aspx/fncGetMultiUOMDetail",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncBindMultipleUOM($.parseJSON(msg.d));
                        fncShowMultipUOM(glInvCode);
                    },
                    error: function (data) {
                        fncToastError(data.message);
                        return false;
                    }
                });


            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindMultipleUOM(uomObj) {
            var uomObj, row, rowNo = 0;
            var mulUOMBody;
            try {

                mulUOMBody = $("#tblMultipleUOM tbody");
                mulUOMBody.children().remove();
                if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                    for (var i = 0; i < uomObj.length; i++) {
                        rowNo = parseInt(rowNo) + 1;

                        row = "<tr>"
                            + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                            + "<td id='tdUOMCode_" + rowNo + "' >" + uomObj[i]["UOMcode"] + "</td>"
                                               + "<td id='tdMRP_" + rowNo + "' >" + parseFloat(uomObj[i]["MRP"]).toFixed(2) + "</td>"
                                               + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(glNetCost * uomObj[i]["ConvQty"]).toFixed(2) + "</td>"
                                               + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='" + uomObj[i]["SPricePer"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                                               + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='" + uomObj[i]["Sellingprice"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                                               //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat(uomObj[i]["Sellingprice"]).toFixed(2) + "</td>"
                                               //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                        + "<td  ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                        + "<td ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                        //+ "<td><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer2"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                        //+ "<td ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice2"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                        // + "<td ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer3"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                        //+ "<td><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice3"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                                               + "<td id='tdUOMconv_" + rowNo + "' >" + uomObj[i]["ConvQty"] + "</td>"
                                               + "</tr>";
                        mulUOMBody.append(row);

                        $("#txtSellingPer_" + rowNo + "").number(true, 4);
                        $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                        //$("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                        //$("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                        $("#txtSPrice_" + rowNo + "").number(true, 2);
                        $("#txtSellingPer_" + rowNo + "").select();
                        $("#txtWPrice1_" + rowNo + "").number(true, 2);
                        //$("#txtWPrice2_" + rowNo + "").number(true, 2);
                        //$("#txtWPrice3_" + rowNo + "").number(true, 2);
                    }
                }
                else {
                    for (var i = 0; i < uomObj.length; i++) {
                        rowNo = parseInt(rowNo) + 1;

                        row = "<tr>"
                            + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                            + "<td id='tdUOMCode_" + rowNo + "' >" + uomObj[i]["UOMcode"] + "</td>"
                                               + "<td id='tdMRP_" + rowNo + "' >" + parseFloat(uomObj[i]["MRP"]).toFixed(2) + "</td>"
                                               + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(glNetCost * uomObj[i]["ConvQty"]).toFixed(2) + "</td>"
                                               + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='" + uomObj[i]["SPricePer"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                                               + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='" + uomObj[i]["Sellingprice"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                                               //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat(uomObj[i]["Sellingprice"]).toFixed(2) + "</td>"
                                               //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                        + "<td  ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                        + "<td ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice1\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                        + "<td><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer2"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                        + "<td ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice2"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                         + "<td ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='" + uomObj[i]["WPricePer3"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                        + "<td><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + uomObj[i]["WPrice3"] + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                                               + "<td id='tdUOMconv_" + rowNo + "' >" + uomObj[i]["ConvQty"] + "</td>"
                                               + "</tr>";
                        mulUOMBody.append(row);

                        $("#txtSellingPer_" + rowNo + "").number(true, 4);
                        $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                        $("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                        $("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                        $("#txtSPrice_" + rowNo + "").number(true, 2);
                        $("#txtSellingPer_" + rowNo + "").select();
                        $("#txtWPrice1_" + rowNo + "").number(true, 2);
                        $("#txtWPrice2_" + rowNo + "").number(true, 2);
                        $("#txtWPrice3_" + rowNo + "").number(true, 2);
                    }
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncBindMultipleUOMExisitingRecord() {
            var uomObj, row, rowNo = 0;
            var mulUOMBody, xmlObj;
            try {

                mulUOMBody = $("#tblMultipleUOM tbody");
                mulUOMBody.children().remove();
                if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                    $("#tblMultipleUOMSave tbody").children().each(function () {// your outer tag of xml
                        if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode && $(this).find('td[id*="tdBatachNo"]').text() == glBatchNo) {
                            rowNo = parseInt(rowNo) + 1;
                            row = "<tr>"
                                + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                                + "<td id='tdUOMCode_" + rowNo + "' >" + $(this).find('td[id*="tdUOMcode"]').text() + "</td>"

                                + "<td id='tdMRP_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdMRP"]').text()).toFixed(2) + "</td>"
                                + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(glNetCost * uomObj[i]["ConvQty"]).toFixed(2) + "</td>"
                                + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='" + $(this).find('td[id*="tdSellingPricePer"]').text() + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                                + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='" + $(this).find('td[id*="tdSellingPricePer"]').text() + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                                 //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                        + "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                        + "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                        //+ "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer2"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                        //+ "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice2"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                        // + "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer3"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                        //+ "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice3"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                        //+ "<td id='tdWPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdWPrice"]').text()).toFixed(2) + "</td>"

                                + "<td id='tdUOMconv_" + rowNo + "' >" + $(this).find('td[id*="tdUOMconv"]').text() + "</td>"
                                + "</tr>";
                            mulUOMBody.append(row);
                            $("#txtSellingPer_" + rowNo + "").number(true, 4);
                            $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                            //$("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                            //$("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                            $("#txtSPrice_" + rowNo + "").number(true, 2);

                            $("#txtSellingPer_" + rowNo + "").select();
                            $("#txtWPrice1_" + rowNo + "").number(true, 2);
                            //$("#txtWPrice2_" + rowNo + "").number(true, 2);
                            //$("#txtWPrice3_" + rowNo + "").number(true, 2);
                        }
                    });
                }
                else {
                    $("#tblMultipleUOMSave tbody").children().each(function () {// your outer tag of xml
                        if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode && $(this).find('td[id*="tdBatachNo"]').text() == glBatchNo) {
                            rowNo = parseInt(rowNo) + 1;
                            row = "<tr>"
                                + "<td id='tdRowNo_" + rowNo + "' >" + rowNo + "</td>"
                                + "<td id='tdUOMCode_" + rowNo + "' >" + $(this).find('td[id*="tdUOMcode"]').text() + "</td>"

                            + "<td id='tdMRP_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdMRP"]').text()).toFixed(2) + "</td>"
                            + "<td id='tdNetCost_" + rowNo + "' >" + parseFloat(glNetCost * uomObj[i]["ConvQty"]).toFixed(2) + "</td>"
                            + "<td id='tdSellingPricePer_" + rowNo + "' ><input type='text' id='txtSellingPer_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"SellingPer\");' Class='form-control-res-right' value='" + $(this).find('td[id*="tdSellingPricePer"]').text() + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPricePer\");' /></td>"
                            + "<td id='tdSPrice_" + rowNo + "' ><input type='text' id='txtSPrice_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"Selling\");' Class='form-control-res-right' value='" + $(this).find('td[id*="tdSellingPricePer"]').text() + "' onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"SPrice\");' /></td>"
                             //+ "<td id='tdSPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdSPrice"]').text()).toFixed(2) + "</td>"
                    + "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer1\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer1\");' /></td>"
                    + "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice1_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice1\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice1\");' /></td>"
                    + "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer2\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer2"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer2\");' /></td>"
                    + "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice2_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice2\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice2"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice2\");' /></td>"
                     + "<td id='tdWholeSalePricePer_" + rowNo + "' ><input type='text' id='txtWholeSalePricePer3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPricePer3\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPricePer3"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPricePer3\");' /></td>"
                    + "<td id='tdWPrice_" + rowNo + "' ><input type='text' id='txtWPrice3_" + rowNo + "' onkeydown='return fncMultiUOMTraversal(this,event,\"WPrice3\");' Class='form-control-res-right' value='" + $(this).find('td[id*="WPrice3"]').text() + "'  onchange='fncArriveSellingpriceAndWpriceForMulUOM(this,\"WPrice3\");' /></td>"
                    //+ "<td id='tdWPrice_" + rowNo + "' >" + parseFloat($(this).find('td[id*="tdWPrice"]').text()).toFixed(2) + "</td>"

                            + "<td id='tdUOMconv_" + rowNo + "' >" + $(this).find('td[id*="tdUOMconv"]').text() + "</td>"
                            + "</tr>";
                            mulUOMBody.append(row);
                            $("#txtSellingPer_" + rowNo + "").number(true, 4);
                            $("#txtWholeSalePricePer1_" + rowNo + "").number(true, 4);
                            $("#txtWholeSalePricePer2_" + rowNo + "").number(true, 4);
                            $("#txtWholeSalePricePer3_" + rowNo + "").number(true, 4);
                            $("#txtSPrice_" + rowNo + "").number(true, 2);

                            $("#txtSellingPer_" + rowNo + "").select();
                            $("#txtWPrice1_" + rowNo + "").number(true, 2);
                            $("#txtWPrice2_" + rowNo + "").number(true, 2);
                            $("#txtWPrice3_" + rowNo + "").number(true, 2);
                        }
                    });
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }


        function fncMultiUOMtoXmlformat() {
            var tblUOMBodySave, row, rowNo = 0;
            try {

                if (uomStatus == false) {
                    fncToastInformation("Please Check Entered Values.");
                    return false;
                }

                tblUOMBodySave = $("#tblMultipleUOMSave tbody");
                /// Delete Exisiting UOM
                tblUOMBodySave.children().each(function () {
                    if ($(this).find('td[id*="tdInventorycode"]').text() == glInvCode && $(this).find('td[id*="tdBatachNo"]').text() == glBatchNo) {
                        $(this).remove();
                    }
                });

                if ($('#<%=hidWholeSaleWprice1Only.ClientID %>').val() == "Y") {
                    $("#tblMultipleUOM tbody").children().each(function () {
                        rowNo = parseInt(rowNo) + 1;
                        var obj = $(this);
                        row = "<tr>"
                               + "<td id='tdInventorycode_" + rowNo + "' >" + glInvCode + "</td>"
                               + "<td id='tdBatachNo_" + rowNo + "' >" + glBatchNo + "</td>"
                                                  + "<td id='tdUOMcode_" + rowNo + "' >" + obj.find('td[id*="tdUOMCode"]').text().trim() + "</td>"
                                                  + "<td id='tdMRP_" + rowNo + "' >" + obj.find('td[id*="tdMRP"]').text().trim() + "</td>"
                                                  //+ "<td id='tdNetCost_" + rowNo + "' >" + obj.find('td[id*="tdNetCost"]').text().trim() + "</td>"
                                                  + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj.find('td input[id*="txtSellingPer"]').val().trim() + "</td>"
                                                  + "<td id='tdSPrice_" + rowNo + "' >" + obj.find('td input[id*="txtSPrice"]').val().trim() + "</td>"
                                                 + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice1_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                               + "<td id='tdUOMconv_" + rowNo + "' >" + obj.find('td[id*="tdUOMconv"]').text().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                               + "<td >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"

                                                  + "</tr>";
                        tblUOMBodySave.append(row);

                    });
                }
                else {
                    $("#tblMultipleUOM tbody").children().each(function () {
                        rowNo = parseInt(rowNo) + 1;
                        var obj = $(this);
                        row = "<tr>"
                               + "<td id='tdInventorycode_" + rowNo + "' >" + glInvCode + "</td>"
                               + "<td id='tdBatachNo_" + rowNo + "' >" + glBatchNo + "</td>"
                                                  + "<td id='tdUOMcode_" + rowNo + "' >" + obj.find('td[id*="tdUOMCode"]').text().trim() + "</td>"
                                                  + "<td id='tdMRP_" + rowNo + "' >" + obj.find('td[id*="tdMRP"]').text().trim() + "</td>"
                                                  //+ "<td id='tdNetCost_" + rowNo + "' >" + obj.find('td[id*="tdNetCost"]').text().trim() + "</td>"
                                                  + "<td id='tdSellingPricePer_" + rowNo + "' >" + obj.find('td input[id*="txtSellingPer"]').val().trim() + "</td>"
                                                  + "<td id='tdSPrice_" + rowNo + "' >" + obj.find('td input[id*="txtSPrice"]').val().trim() + "</td>"
                                                 + "<td id='tdWholeSalePricePer1_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer1"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice1_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice1"]').val().trim() + "</td>"
                                               + "<td id='tdUOMconv_" + rowNo + "' >" + obj.find('td[id*="tdUOMconv"]').text().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer2_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer2"]').val().trim() + "</td>"
                                              + "<td id='tdWPrice2_" + rowNo + "' >" + obj.find('td input[id*="txtWPrice2"]').val().trim() + "</td>"
                                               + "<td id='tdWholeSalePricePer3_" + rowNo + "' >" + obj.find('td input[id*="txtWholeSalePricePer3"]').val().trim() + "</td>"
                                               + "<td >" + obj.find('td input[id*="txtWPrice3"]').val().trim() + "</td>"

                                              + "</tr>";
                        tblUOMBodySave.append(row);

                    });
                }

                $("#divMultipleUOM").dialog("destroy");
                glSelectedRow.find('td input[id*="chkSingle"]').prop("checked", true);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncMultiUOMTraversal(source, evt, value) {
            var obj;
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                obj = $(source).parent().parent();
                if (charCode == 13) {
                    if (value == "SellingPer") {
                        obj.find('td input[id*="txtSPrice"]').select();
                    }
                    else if (value == "WPricePer1") {
                        obj.find('td input[id*="txtWPrice1"]').select();
                    }
                    else if (value == "WPrice1") {
                        obj.find('td input[id*="txtWholeSalePricePer2"]').select();
                    }
                    else if (value == "WPricePer2") {
                        obj.find('td input[id*="txtWPrice2"]').select();
                    }
                    else if (value == "WPrice2") {
                        obj.find('td input[id*="txtWholeSalePricePer3"]').select();
                    }

                    else if (value == "txtWholeSalePricePer1") {
                        if (obj.next().length > 0) {
                            obj.next().find('td input[id*="txtWholeSalePricePer2"]').select();
                        }
                        else {
                            $('#<%=lnkOk.ClientID %>').focus();
                        }
                    }
                    else if (value == "Selling") {
                        obj.find('td input[id*="txtWholeSalePricePer1"]').select();
                    }
                    else if (value == "WPrice1") {
                        if (obj.next().length > 0) {
                            obj.next().find('td input[id*="WPrice2"]').select();
                        }

                    }
                    else if (value == "WPrice2") {
                        if (obj.next().length > 0) {
                            obj.next().find('td input[id*="WPrice3"]').select();
                        }
                        else {
                            $('#<%=lnkOk.ClientID %>').focus();
                        }
                    }
                    else if (value == "WPrice3") {
                        if (obj.next().length > 0) {
                            obj.next().find('td input[id*="txtSellingPer"]').select();
                        }
                        else {
                            $('#<%=lnkOk.ClientID %>').focus();
                        }
                    }
                    else if (value == "WPricePer3") {
                        obj.find('td input[id*="txtWPrice3"]').select();

                    }

}

}
    catch (err) {
        fncToastError(err.message);
    }
}

       <%-- function fncMultiUOMTraversal(source, evt, value) {
            var obj;
            try {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                obj = $(source).parent().parent();
                if (charCode == 13) {
                    if (value == "SellingPer") {
                        obj.find('td input[id*="txtWPricePer"]').select();
                    }
                    else {
                        if (obj.next().length > 0) {
                            obj.next().find('td input[id*="txtSellingPer"]').select();
                        }
                        else {
                            $('#<%=lnkOk.ClientID %>').focus();
                    }
                }
            }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }--%>

        function fncShowMultipUOM(itemcode) {
            try {

                if ($('#<%=hidMultiUOM.ClientID %>').val() == "Y") {
                    $("#divMultipleUOM").dialog({
                        resizable: false,
                        height: "auto",
                        width: "auto",
                        modal: true,
                        title: itemcode
                    });
                }
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        var uomStatus = true;
        function fncArriveSellingpriceAndWpriceForMulUOM(sorce, value) {
            var per = 0, sellingprice = 0, mrp = 0;
            var rowObj, discAmt = 0, discPer = 0, netCost = 0;
            var wprice = 0;
            var wprice1 = 0; wprice2 = 0; wprice3 = 0;
            try {

                rowObj = $(sorce).parent().parent();
                netCost = rowObj.find('td[id*="tdNetCost"]').text();
                if (value == "SPricePer") {
                    per = rowObj.find('td input[id*="txtSellingPer"]').val();
                    sellingprice = rowObj.find('td[id*="tdMRP"]').text() - (rowObj.find('td[id*="tdMRP"]').text() * per / 100);
                    rowObj.find('td input[id*="txtSPrice"]').val(sellingprice.toFixed(2));

                    /// WPrice Calculation
                    //fncArriveSellingpriceAndWpriceForMulUOM(sorce, "WPricePer1");
                }
                else if (value == "WPricePer") {
                    per = rowObj.find('td input[id*="txtWholeSalePricePer"]').val();
                    sellingprice = rowObj.find('td input[id*="txtSPrice"]').val() - (rowObj.find('td input[id*="txtSPrice"]').val() * per / 100);
                    rowObj.find('td input[id*="txtWPrice"]').val(sellingprice.toFixed(2));
                }
                else if (value == "WPricePer1") {
                    per = rowObj.find('td input[id*="txtWholeSalePricePer1"]').val();
                    sellingprice = rowObj.find('td input[id*="txtSPrice"]').val() - (rowObj.find('td input[id*="txtSPrice"]').val() * per / 100);
                    rowObj.find('td input[id*="txtWPrice1"]').val(sellingprice.toFixed(2));
                }
                else if (value == "WPricePer2") {
                    per = rowObj.find('td input[id*="txtWholeSalePricePer2"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice1"]').val() - (rowObj.find('td input[id*="txtWPrice1"]').val() * per / 100);
                    rowObj.find('td input[id*="txtWPrice2"]').val(sellingprice.toFixed(2));
                }
                else if (value == "WPricePer3") {
                    per = rowObj.find('td input[id*="txtWholeSalePricePer3"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice2"]').val() - (rowObj.find('td input[id*="txtWPrice2"]').val() * per / 100);
                    rowObj.find('td input[id*="txtWPrice3"]').val(sellingprice.toFixed(2));
                }
                else if (value == "SPrice") {
                    mrp = rowObj.find('td[id*="tdMRP"]').text();
                    sellingprice = rowObj.find('td input[id*="txtSPrice"]').val();

                    discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                    discPer = discAmt * 100 / mrp;
                    rowObj.find('td input[id*="txtSellingPer"]').val(discPer.toFixed(4));

                    /// Wprice Calculation
                    fncArriveSellingpriceAndWpriceForMulUOM(sorce, "WPricePer");
                }
                else if (value == "WPrice") {
                    mrp = rowObj.find('td input[id*="txtSPrice"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice"]').val();
                    //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                    discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                    discPer = discAmt * 100 / mrp;
                    rowObj.find('td input[id*="txtWholeSalePricePer"]').val(discPer.toFixed(4));
                }

                else if (value == "WPrice1") {
                    mrp = rowObj.find('td input[id*="txtSPrice"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice1"]').val();
                    //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                    discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                    discPer = discAmt * 100 / mrp;
                    rowObj.find('td input[id*="txtWholeSalePricePer1"]').val(discPer.toFixed(4));
                }
                else if (value == "WPrice2") {
                    mrp = rowObj.find('td input[id*="WPrice1"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice2"]').val();
                    //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                    discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                    discPer = discAmt * 100 / mrp;
                    rowObj.find('td input[id*="txtWholeSalePricePer2"]').val(discPer.toFixed(4));
                }
                else if (value == "WPrice3") {
                    mrp = rowObj.find('td input[id*="WPrice2"]').val();
                    sellingprice = rowObj.find('td input[id*="txtWPrice3"]').val();
                    //netCost = rowObj.find('td[id*="tdNetCost"]').text();

                    discAmt = parseFloat(mrp) - parseFloat(sellingprice);
                    discPer = discAmt * 100 / mrp;
                    rowObj.find('td input[id*="txtWholeSalePricePer3"]').val(discPer.toFixed(4));
                }
                //// Validation Part////
                sellingprice = rowObj.find('td input[id*="txtSPrice"]').val();
                wprice = rowObj.find('td input[id*="txtWPrice1"]').val();
                mrp = rowObj.find('td[id*="tdMRP"]').text();
                wprice1 = rowObj.find('td input[id*="txtWPrice1"]').val();
                wprice2 = rowObj.find('td input[id*="txtWPrice2"]').val();
                wprice3 = rowObj.find('td input[id*="txtWPrice3"]').val();
                if (parseFloat(sellingprice) > parseFloat(mrp)) {
                    fncToastInformation("Selling Price must less then Or equal MRP. ");
                    rowObj.find('td input[id*="txtSPrice"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(sellingprice) < parseFloat(netCost)) {
                    fncToastInformation("Selling Price must greater then  Netcost. ");
                    rowObj.find('td input[id*="txtSPrice"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice1) > parseFloat(sellingprice)) {
                    fncToastInformation("WPrice1 must less then Or equal Selling Price. ");
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice2) > parseFloat(wprice1)) {
                    fncToastInformation("WPrice2 must less then Or equal wprice1. ");
                    rowObj.find('td input[id*="txtWPrice2"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice3) > parseFloat(wprice1)) {
                    fncToastInformation("WPrice3 must less then Or equal wprice1. ");
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice3) > parseFloat(wprice2)) {
                    fncToastInformation("WPrice3 must less then Or equal wprice2. ");
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice2) > parseFloat(sellingprice)) {
                    fncToastInformation("WPrice1 must less then Or equal Selling Price. ");
                    rowObj.find('td input[id*="txtWPrice2"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice3) > parseFloat(sellingprice)) {
                    fncToastInformation("WPrice1 must less then Or equal Selling Price. ");
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice1) < parseFloat(netCost)) {
                    fncToastInformation("WPrice1 must greater then Netcost. ");
                    rowObj.find('td input[id*="txtWPrice1"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice2) < parseFloat(netCost)) {
                    fncToastInformation("WPrice2 must greater then Netcost. ");
                    rowObj.find('td input[id*="txtWPrice2"]').select();
                    uomStatus = false;
                    return;
                }
                else if (parseFloat(wprice3) < parseFloat(netCost)) {
                    fncToastInformation("WPrice3 must greater then Netcost. ");
                    rowObj.find('td input[id*="txtWPrice3"]').select();
                    uomStatus = false;
                    return;
                }
                else {
                    uomStatus = true;
                }

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        //function fncArriveSellingpriceAndWpriceForMulUOM(sorce, value) {
        //    var per = 0, sellingprice = 0;
        //    var rowObj;

        //    try {
        //        per = $(sorce).val();
        //        rowObj = $(sorce).parent().parent();
        //        if (value == "SPricePer") {

        //            sellingprice = rowObj.find('td[id*="tdMRP"]').text() - (rowObj.find('td[id*="tdMRP"]').text() * per / 100);
        //            rowObj.find('td[id*="tdSPrice"]').text(sellingprice.toFixed(2));
        //        }
        //        else {
        //            sellingprice = rowObj.find('td[id*="tdSPrice"]').text() - (rowObj.find('td[id*="tdSPrice"]').text() * per / 100);
        //            rowObj.find('td[id*="tdWPrice"]').text(sellingprice.toFixed(2));
        //        }
        //    }
        //    catch (err) {
        //        fncToastError(err.message);
        //    }
        //}

        function fncGetMultiUOMForSave() {
            var tableObj = { myrows: [] }, rowValue = [], i = 0;
            try {
                  if ($(".cbLocation").find("input:checked").length == 0) {
                      fncToastInformation('Please Select Anyone Location ');
               return false;
            }

                $.each($("#tblMultipleUOMSave thead th"), function () {
                    rowValue[i++] = $(this).text().trim();
                });


                $.each($("#tblMultipleUOMSave tbody tr"), function () {
                    var $row = $(this), rowObj = {};
                    i = 0;
                    $.each($("td", $row), function () {
                        var $col = $(this);
                        rowObj[rowValue[i]] = $col.text().trim();
                        i++;
                    });

                    tableObj.myrows.push(rowObj);
                });

                $('#<%=hidMultiUOMSave.ClientID %>').val(JSON.stringify(tableObj));
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        //---------------------------------------------------------------------------------------
        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=hidVendor.ClientID %>').val(Code);
                    $('#<%=txtVendor.ClientID %>').val(Description);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }


        function AddDisPer() {
            try {

                var Show = '';

                if ($("#<%=grdBatchPriceChange.ClientID %>").length == 0) {
                    Show = Show + '<%=Resources.LabelCaption.Alert_RecordNotFound%>';
                }
                if (parseFloat($("#<%=txtDis.ClientID %>").val()) <= 0 || $("#<%=txtDis.ClientID %>").val() == "") {
                    Show = Show + '<br />Please Enter Discount Percentage';
                }
                if (Show != '') {
                    ShowPopupMessageBox(Show);
                    return false;
                }
                else {
                    $("#<%=grdBatchPriceChange.ClientID %> tbody tr").each(function () {
                        //$(this).find('td span[id*="lblreProPrice"]').html('10');
                        var Disper = 0;
                        if ($("#<%=rdoDown.ClientID %>").prop("checked")) {
                            var sellingprice = $(this).find('td span[id*="lblSellingPrice"]').html();
                            var netcost = $(this).find("td").eq(13).html();
                            var mrp = $(this).find("td").eq(7).html();
                            Disper = parseFloat(sellingprice) - parseFloat(sellingprice) * parseFloat($("#<%=txtDis.ClientID %>").val()) / 100;

                            $(this).find('td input[id*="txtSellingPrice"]').val(parseFloat(Disper).toFixed(2));
                            sSellingPrice = $(this).find('td input[id*="txtSellingPrice"]').val();

                            if (sSellingPrice != "") {
                                if (parseFloat(sSellingPrice) > parseFloat(mrp)) {
                                    $(this).find('td input[id*="txtSellingPrice"]').val(sellingprice);
                                    ShowPopupMessageBox("Selling Price must be Lessar than or Equal to MRP");
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", false);
                                }
                                else if (parseFloat(sSellingPrice) < parseFloat(netcost)) {
                                    $(this).find('td input[id*="txtSellingPrice"]').val(sellingprice);
                                    ShowPopupMessageBox("Selling Price must be Greater than or Equal to Net Cost");
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", false);
                                }
                                else {
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", true);
                                    $(this).find('td input[id*="txtSellingPrice"]').css('background-color', '#1343d0');
                                    $(this).find('td input[id*="txtSellingPrice"]').css('color', 'white');
                                }
                            }
                        }
                        else if ($("#<%=rdoUp.ClientID %>").prop("checked")) {
                            var sellingprice = $(this).find('td span[id*="lblSellingPrice"]').html();
                            var netcost = $(this).find("td").eq(13).html();
                            var mrp = $(this).find("td").eq(7).html();
                            Disper = parseFloat(sellingprice) + parseFloat(sellingprice) * parseFloat($("#<%=txtDis.ClientID %>").val()) / 100;

                            $(this).find('td input[id*="txtSellingPrice"]').val(parseFloat(Disper).toFixed());
                            sSellingPrice = $(this).find('td input[id*="txtSellingPrice"]').val();

                            if (sSellingPrice != "") {
                                if (parseFloat(sSellingPrice) > parseFloat(mrp)) {
                                    $(this).find('td input[id*="txtSellingPrice"]').val(sellingprice);
                                    ShowPopupMessageBox("Selling Price must be Lessar than or Equal to MRP");
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", false);
                                }
                                else if (parseFloat(sSellingPrice) < parseFloat(netcost)) {
                                    $(this).find('td input[id*="txtSellingPrice"]').val(sellingprice);
                                    ShowPopupMessageBox("Selling Price must be Greater than or Equal to Net Cost");
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", false);
                                }
                                else {
                                    //$(this).find('td input[id*="chkSingle"]').prop("checked", true);
                                    $(this).find('td input[id*="txtSellingPrice"]').css('background-color', '#1343d0');
                                    $(this).find('td input[id*="txtSellingPrice"]').css('color', 'white');
                                }
                            }
                        }
                    });


            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }


    function fncVenItemRowdblClk(rowObj) {
        try {
            //rowObj = $(rowObj).html();
            fncOpenItemhistory(rowObj);
        }
        catch (err) {
            fncToastError(err.message);
        }
    }
    /// Open Item History
    function fncOpenItemhistory(itemcode) {
        var page = '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx") %>';
        page = page + "?InvCode=" + itemcode + "&Status=dailog";
        var $dialog = $('<div id="popupInvMaster" ></div>').html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>').dialog({
            autoOpen: false,
            modal: true,
            height: 700,
            width: 1250,
            title: "Inventory History"
        });
        $dialog.dialog('open');
    }

    function fncSelectUnselectrow(event) {
        pricevalidationstatus = "All";
        try {
            if (($(event).is(":checked"))) {
                $("#<%=grdBatchPriceChange.ClientID %> tbody tr").each(function () {
                    $(this).find('td input[id*="chkSingle"]').attr("checked", "checked");
                });
                //                    $("#tblpromotionfilter [id*=cbreProfilterrow]").attr("checked", "checked");
            }
            else {
                //$("#<%=grdBatchPriceChange.ClientID %>  [id*=cbreProfilterrow]").removeAttr("checked");
                $("#<%=grdBatchPriceChange.ClientID%> input[id*='chkSingle']:checkbox").removeAttr("checked");

            }
        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    function fncRowdblClk(source) {
        source = $(source);
        glSelectedRow = source;
    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container">
        <div class="breadcrumbs" id="breadcrumbs" runat="server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <%--                <li><a style="text-decoration: none;">Price Change Utility</a><i class="fa fa-angle-right"></i></li>--%>
                <li class="active-page">Price Change With Effective Date </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="container-group-price">



            <div class="container-left-price" id="pnlFilter" runat="server">

                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                    <ContentTemplate>

                        <div class="control-group-price-header">
                            Filtertions
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtMerchandise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text='<%$ Resources:LabelCaption,lbl_Merchandise %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlMerchandise" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtMerchandise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchandise', 'txtManufacture');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text='<%$ Resources:LabelCaption,lbl_Manufacture %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlManufacture" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text='<%$ Resources:LabelCaption,lblFloor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlFloor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text='<%$ Resources:LabelCaption,lbl_Section %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text='<%$ Resources:LabelCaption,lbl_Bin %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlBin" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text='<%$ Resources:LabelCaption,lbl_Shelf %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlShelf" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtBarcode');"></asp:TextBox>
                            </div>
                        </div>
                       <%-- <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text='<%$ Resources:LabelCaption,lbl_Barcode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBarcode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label14" runat="server" Text='<%$ Resources:LabelCaption,lblBatchNo %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBatchNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text='<%$ Resources:LabelCaption,lbl_ItemName %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res" style="display:none">
                            <div class="label-left">
                                <asp:Label ID="Label17" runat="server" Text='<%$ Resources:LabelCaption,lbl_GRNNo %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtGRNNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="UpdatePanel4" runat="Server">
                    <ContentTemplate>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" Text='<%$ Resources:LabelCaption,btn_Filter %>'
                                    OnClick="lnkFilter_Click"></asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClick="lnkClear_Click"
                                    Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <div class="price-qty-Deactivebatch" style="display: none">
                    <asp:LinkButton ID="lnkBulkBatch" runat="server" Style="margin-left: -932px;" class="button-blue" Text='Bulk Batch Activate and Deactivate'
                        OnClick="lnkBulkBatch_Click"></asp:LinkButton>
                </div>
                <div class="left-batch-Loccontainer" style="overflow: auto; height: 115px; width: 250px;">
                    <div class="control-group-price-header">
                        Available Locations
                    </div>
                    <asp:CheckBox ID="chkAllLoc" runat="server" Text=" Select All" /><br />
                    <asp:CheckBoxList ID="chkListLoc" CssClass="cbLocation" runat="server">
                    </asp:CheckBoxList>
                </div>
                <div class="price-qty-Deactivebatch" style="display: none">
                    <asp:LinkButton ID="lnkDeactivatebatch" runat="server" class="button-blue" Text='Deactivate and Activate Batch'
                        OnClientClick=" return fncOpenDeativateBatch();"></asp:LinkButton>
                </div>
                <div class="price-qty" style="margin-right: 505px">
                    <asp:RadioButton ID="rbtAllQty" runat="server" Text="All Qty" GroupName="Qty" OnCheckedChanged="rbtAllQty_CheckedChanged"
                        Checked="true" AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtGreaterQty" runat="server" Text="Qty>0" GroupName="Qty" OnCheckedChanged="rbtGreaterQty_CheckedChanged"
                                AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtLessQty" runat="server" Text="Qty<=0" GroupName="Qty" OnCheckedChanged="rbtLessQty_CheckedChanged"
                                AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                </div>
               
                <div class="col-md-12" style="width: 70%">
                    <div class="col-md-7">
                    </div>
                    <div class="col-md-5 sort_by" style="width: 62.666667%; top: -39px; margin-left: 286px;">
                        <div class="col-md-5">
                            <div class="control-group-single-res">
                                <div class="label-left">
                                    <asp:Label ID="lblDiscount" runat="server" Text="S.Price %"></asp:Label>
                                </div>

                                <div class="label-right">
                                    <asp:TextBox ID="txtDis" runat="server" Text="0" onkeydown="return isNumberKeyWithDecimalNew(event)" Style="text-align: right" class="form-control-res textboxPromotion" Width="100%" onFocus="this.select();"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <asp:RadioButton ID="rdoUp" runat="server" GroupName="Calculation" Text="UP" Style="margin-top: 10px;"></asp:RadioButton>
                        </div>
                        <div class="col-md-2">
                            <asp:RadioButton ID="rdoDown" runat="server" Checked="true" GroupName="Calculation" Text="Down" Style="margin-top: 10px;"></asp:RadioButton>
                        </div>
                        <div class="col-md-3">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkAdd" runat="server" OnClientClick="AddDisPer();return false;" class="button-blue">Apply</asp:LinkButton>
                            </div>
                        </div>
                    </div>  
                <div class="col-md-7">                              
                    <div class="col-md-3">  
                        <asp:Label ID="Label11" runat="server" Text="Effective Date"></asp:Label>
                    </div>
                       <div class="col-md-4">
                        <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>  
                 </div>  
                    
                </div>  
                           
                <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
                    <ContentTemplate>
                        <div class="gridDetails">
                            <div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">
                                <div id="Div1" runat="server">
                                    <div class="grdLoad">
                                        <div class="row">
                                            <table id="tblGrd" cellspacing="0" rules="all" border="1" class="fixed_headers">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">
                                                            <asp:CheckBox ID="cbProfilterheader" onclick="fncSelectUnselectrow(this)" runat="server" />
                                                        </th>
                                                        <th scope="col">Price Change</th>
                                                        <th scope="col">S.No</th>
                                                        <th scope="col">ItemCode</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Batch No</th>
                                                        <th scope="col">Qty</th>
                                                        <th scope="col">MRP</th>
                                                        <th scope="col">SellingPrice</th>
                                                        <th scope="col">WPrice1</th>
                                                        <th scope="col">WPrice2</th>
                                                        <th scope="col">WPrice3</th>
                                                        <th scope="col">OldS.Pirce</th>
                                                        <th scope="col">Net Cost</th>
                                                        <th scope="col">Basic Cost</th>
                                                        <th scope="col">Gross Cost</th>
                                                        <th scope="col">NetSellingPrice</th>
                                                        <th scope="col">HWP1</th>
                                                        <th scope="col">HWP2</th>
                                                        <th scope="col">HWP3</th>
                                                        <th scope="col">Create Date</th>
                                                         <th style="display:none" scope="col">Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div class="GridDetails cssGrid">
                                                <div class="gridDetails grid-overflow GridChange" id="HideFilter_GridOverFlow" runat="server">
                                                    <asp:GridView ID="grdBatchPriceChange" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" OnRowDataBound="grdBatchPriceChange_RowDataBound" >
                                                         <EmptyDataTemplate>
                                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                        </EmptyDataTemplate>
                                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                            NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                        <PagerStyle CssClass="pshro_text" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Select"></asp:Label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSingle" runat="server" />
                                                                    <%--AutoPostBack="true" OnCheckedChanged="OnCheckedChanged"--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:ButtonField ButtonType="Button" HeaderText="Price Change in Outlet" Text="Price Change"
                                                                CommandName="PriceChange" />
                                                            <asp:BoundField DataField="RowNo" HeaderText="SNo"></asp:BoundField>
                                                            <asp:BoundField DataField="InventoryCode" HeaderText="Item Code"></asp:BoundField>
                                                            <asp:BoundField DataField="Description" HeaderText="Item Name"></asp:BoundField>
                                                            <asp:BoundField DataField="BatchNo" HeaderText="Batch No" />
                                                            <asp:BoundField DataField="BalanceQty" HeaderText="Qty"></asp:BoundField>
                                                            <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                            <%--<asp:BoundField DataField="SellingPrice" HeaderText="Selling Price"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="Selling Price">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>'
                                                                        CssClass="hiddencol"></asp:Label>
                                                                    <asp:TextBox ID="txtSellingPrice" runat="server" Text='<%# Eval("SellingPrice") %>'
                                                                        onkeydown="return isNumberKeyWithDecimalNew(event)" Style="text-align: right" onkeyup="return txtSellingPrice_Keystroke(event,this);" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                                        onchange="return txtSellingPrice_TextChanged(this)" CssClass="grid-textbox"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:BoundField DataField="WPrice1" HeaderText="WPrice1"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="WPrice1">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWPrice1" runat="server" Text='<%# Eval("WPrice1") %>' CssClass="hiddencol"></asp:Label>
                                                                    <asp:TextBox ID="txtlblWPrice1" runat="server" Style="text-align: right" Text='<%# Eval("WPrice1") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                                        CssClass="grid-textbox" onkeyup="return txtlblWPrice1_Keystroke(event,this);" onchange="return txtlblWPrice1_TextChanged(this)"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:BoundField DataField="WPrice2" HeaderText="WPrice2"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="WPrice2">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWPrice2" runat="server" Text='<%# Eval("WPrice2") %>' CssClass="hiddencol"></asp:Label>
                                                                    <asp:TextBox ID="txtlblWPrice2" runat="server" Style="text-align: right" Text='<%# Eval("WPrice2") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                                        CssClass="grid-textbox" onkeyup="return txtlblWPrice2_Keystroke(event,this)" onchange="return txtlblWPrice2_TextChanged(this)"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:BoundField DataField="WPrice3" HeaderText="WPrice3"></asp:BoundField>--%>
                                                            <asp:TemplateField HeaderText="WPrice3">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWPrice3" runat="server" Text='<%# Eval("WPrice3") %>' CssClass="hiddencol"></asp:Label>
                                                                    <asp:TextBox ID="txtlblWPrice3" runat="server" Style="text-align: right" Text='<%# Eval("WPrice3") %>' onkeydown="return isNumberKeyWithDecimalNew(event)" onfocus="this.select()" ondragstart="return false;" ondrop="return false;"
                                                                        CssClass="grid-textbox" onkeyup="return txtlblWPrice3_Keystroke(event,this)" onchange="return txtlblWPrice3_TextChanged(this)"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="OldSellingPrice" HeaderText="Old S.Price"></asp:BoundField>
                                                            <asp:BoundField DataField="Basiccost" HeaderText="Net cost"></asp:BoundField>
                                                            <asp:BoundField DataField="UnitCost" HeaderText="Unit Cost"></asp:BoundField>
                                                            <asp:BoundField DataField="Grosscost" HeaderText="Gross cost"></asp:BoundField>
                                                            <asp:BoundField DataField="SellingPrice" HeaderText="SellingPrice" ItemStyle-CssClass="hiddencol"
                                                                HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                            <asp:BoundField DataField="HWP1" HeaderText="Old WPrice1" ItemStyle-CssClass="hiddencol"
                                                                HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                            <asp:BoundField DataField="HWP2" HeaderText="Old WPrice2" ItemStyle-CssClass="hiddencol"
                                                                HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                            <asp:BoundField DataField="HWP3" HeaderText="Old WPrice3" ItemStyle-CssClass="hiddencol"
                                                                HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                            <asp:BoundField DataField="CreateDate" HeaderText="Create Date"></asp:BoundField>
                                                            <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
                    <ContentTemplate>

                        <div class="col-md-12">
                            <div class="col-md-8">
                               <%-- <label style="color: white; background-color: #1343d0">XXXX</label>
                                <label style="font-weight: 700;">Sellingprice percentage is updated</label>--%>
                          <div class="control-button ">
                    <asp:LinkButton ID="lnkDeletePrice" runat="server"  PostBackUrl="../Merchandising/frmEffectivePricechangeRepots.aspx" class="button-blue">Delete Effective Price</asp:LinkButton>
                        </div>      
                            </div>
                            <div class="col-md-4">                                                           
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter</asp:LinkButton>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkUpdate" runat="server" class="button-blue" Text='Update(F4)'
                                        OnClick="lnkUpdate_Click" OnClientClick="return fncGetMultiUOMForSave();"></asp:LinkButton>
                                </div>
                                <%-- <div class="control-button">
                                <asp:LinkButton ID="lnkPrint" runat="server" class="button-blue" OnClick="lnkPrint_click" Text='Print(F11)'></asp:LinkButton>
                            </div>--%>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue"
                                        OnClick="lnkClear_Click" Text='<%$ Resources:LabelCaption,btnClear %>'></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidItemcode" runat="server" />
        <asp:HiddenField ID="hidPricecheck" runat="server" />
        <asp:HiddenField ID="hidMultiUOM" runat="server" Value="N" />
        <asp:HiddenField ID="hidWholeSaleWprice1Only" runat="server" />
        <asp:HiddenField ID="hidMultiUOMSave" runat="server" Value="" />

    </div>
    <div id="divMultipleUOM" class="display_none">
        <div class="Payment_fixed_headers gid_LastPurchase">
            <table id="tblMultipleUOM" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">UOMCode</th>
                        <th scope="col">MRP</th>
                        <th scope="col">Net Cost</th>
                        <th scope="col">Selling(%)</th>
                        <th scope="col">Selling Price</th>
                        <th scope="col">WPricePer1</th>
                        <th scope="col">WPrice1</th>
                        <th id="thRMUOMWprice2Per" runat="server" scope="col">WPricePer2   
                        </th>
                        <th id="thRMUOMWprice2" runat="server" scope="col">WPrice2
                        </th>
                        <th id="thRMUOMWprice3Per" runat="server" scope="col">WPricePer3
                        </th>
                        <th id="thRMUOMWprice3" runat="server" scope="col">WPrice3
                        </th>
                        <th scope="col">UOMConv</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="float_right">
            <asp:LinkButton ID="lnkOk" runat="server" class="ui-button ui-corner-all ui-widget" Text='Ok'
                OnClientClick="fncMultiUOMtoXmlformat();return false;"></asp:LinkButton>
        </div>
    </div>
    <div class="display_none">
        <div class="Payment_fixed_headers gid_LastPurchase">
            <table id="tblMultipleUOMSave" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">Inventorycode
                        </th>
                        <th scope="col">BatachNo</th>
                        <th scope="col">UOMcode</th>
                        <th scope="col">MRP</th>
                        <th scope="col">SPricePer</th>
                        <th scope="col">Sellingprice</th>
                        <th scope="col">WPricePer1</th>
                        <th scope="col">WPrice1</th>
                        <th scope="col">UOMConv</th>
                        <th id="thRMUOMWprice2Persave" runat="server" scope="col">WPricePer2   
                        </th>
                        <th id="thRMUOMWprice2save" runat="server" scope="col">WPrice2
                        </th>
                        <th id="thRMUOMWprice3Persave" runat="server" scope="col">WPricePer3
                        </th>
                        <th id="thRMUOMWprice3save" runat="server" scope="col">WPrice3
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hidVendor" runat="server" />
    <asp:HiddenField ID="hidWholeSale" runat="server" />
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
