﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmClearingSales.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmClearingSales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 30px;
            max-width: 30px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 45px;
            max-width: 45px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 70px;
            max-width: 70px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(10), .grdLoad th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(11), .grdLoad th:nth-child(11) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(12), .grdLoad th:nth-child(12) {
            min-width: 60px;
            max-width: 60px;
        }

        .grdLoad td:nth-child(13), .grdLoad th:nth-child(13) {
            min-width: 80px;
            max-width: 80px;
        }

        .grdLoad td:nth-child(14), .grdLoad th:nth-child(14) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(15), .grdLoad th:nth-child(15) {
            min-width: 100px;
            max-width: 100px;
        }

        .grdLoad td:nth-child(16), .grdLoad th:nth-child(16) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'ClearingSales');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "ClearingSales";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">
        function pageLoad() {
            fncDecimal();
            $(function () {
                var Tabname = '';
                if ($("[id*=hdNav]").val().includes("All"))
                    $("[id*=hdNav]").val('All');
                else
                    $("[id*=hdNav]").val('New');
                Tabname = $("[id*=hdNav]").val() != "" ? $("[id*=hdNav]").val() : "New";
                $('#Tabs a[href="#' + Tabname + '"]').tab('show');
                $("#Tabs ul li a").click(function () {
                    $("[id*=hdNav]").val($(this).attr("href").replace("#", ""));
                });
                SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                SetDefaultDates($("#<%= txtPkd.ClientID %>"), $("#<%= txtBestBefore.ClientID %>"));
                if (($('#<%=chkSelect.ClientID %>').is(":checked"))) {
                    $("#<%=grdLoad.ClientID%> input[id*='chkSingle']:checkbox").attr("checked", true);
                }
                else {
                    $("#<%=grdLoad.ClientID%> input[id*='chkSingle']:checkbox").attr("checked", false);
                }
                // For Scrol down in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $("html, body").scrollTop($(document).height());
                })
            });
            //$(function () {
            //    $("[id*=grdBatch]").on('dblclick', 'td', function (e) {
            //        DisplayChildDetails($(this).closest("tr"));
            //        $("#dialog-Batch").dialog('close')
            //        e.preventDefault();
            //    });
            //});
            $('#<%=chkSelect.ClientID %>').click(// dinesh
              function () {
                  if (($('#<%=chkSelect.ClientID %>').is(":checked"))) {
                      $("#<%=grdLoad.ClientID%> input[id*='chkSingle']:checkbox").attr("checked", true);
                  }
                  else {
                      $("#<%=grdLoad.ClientID%> input[id*='chkSingle']:checkbox").attr("checked", false);
                  }
              });
              $('#<%=chkLoad.ClientID %>').click(
          function () {
              __doPostBack('ctl00$ContentPlaceHolder1$lnkFetch', '');
          });
            $('#<%=chkDeactivate.ClientID %>').click(
              function () {
                  <%--if (($('#<%=chkDeactivate.ClientID %>').is(":checked"))) {
                      //$("#ContentPlaceHolder1_lnkDelete").attr("disabled", "disabled");
                      $('#ContentPlaceHolder1_lnkDelete').prop("disabled", true);
                      //$("#ContentPlaceHolder1_lnkPrint").attr("disabled", "disabled");
                  } else {
                      //$("#ContentPlaceHolder1_lnkDelete").removeAttr("disabled");
                      //$("#ContentPlaceHolder1_lnkPrint").removeAttr("disabled");
                      $('#ContentPlaceHolder1_lnkDelete').prop("disabled", false);
                  }--%>
                  __doPostBack('ctl00$ContentPlaceHolder1$lnkFetch', '');
              });

            <%--$("[id*=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmClearingSales.aspx/GetInventoryCode") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtInventoryCode]").val(i.item.val);
                    $("[id*=txtDescription]").val(i.item.desc);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtInventoryCode]").val(i.item.val);
                    $("[id*=txtDescription]").val(i.item.desc);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                    return false;
                },
            });--%>

            $("[id$=txtInventoryCode]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("frmClearingSales.aspx/GetInventoryCode") %>',
                        <%--url: '<%=ResolveUrl("~/Managements/frmCreditInvoice.aspx/GetInventoryDetails")%>',--%>
                        data: "{ 'prefix': '" + request.term + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    valCode: item.split('|')[1],
                                    valDescription: item.split('|')[2],
                                    valCost: item.split('|')[3],
                                    valTax: item.split('|')[4],
                                    valCess: item.split('|')[5],
                                    valBatch: item.split('|')[6]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtInventoryCode]").val(i.item.valCode);
                    $("[id*=txtDescription]").val(i.item.valDescription);
                    $("#HidenIsBatch").val($.trim(i.item.valBatch));
                    $("#HiddenItemCode").val($.trim(i.item.valCode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtInventoryCode]").val(i.item.valCode);
                    $("[id*=txtDescription]").val(i.item.valDescription);
                    $("#HidenIsBatch").val($.trim(i.item.valBatch));
                    $("#HiddenItemCode").val($.trim(i.item.valCode));
                    if ($("#HidenIsBatch").val() == 'True') {
                        fncGetBatchDetails(i.item.valCode);
                    }
                    else {
                        __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                    }
                    return false;
                },
                minLength: 3
            });

           <%-- $("[id*=txtTemplate]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmClearingSales.aspx/GetTemplate") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtTemplate]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtTemplate]").val(i.item.val);
                    return false;
                },
            });--%>

            $("[id*=txtItemCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmClearingSales.aspx/GetInventory") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    return false;
                },
            });

            $("[id$=txtInventoryCode]").keydown(function (e) { // dinesh
                if (e.keyCode == 13) {
                    if ($("#HidenIsBatch").val() == 'True') {
                        fncGetBatchDetails($("#HiddenItemCode").val());
                    }
                    else {
                        __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                    }
                    e.preventDefault();
                    return;
                }
            });

            $(document).ready(function () {
                $(document).on('keydown', disableFunctionKeys);
            });
            function disableFunctionKeys(e) {
                try {
                    if (e.keyCode == 113 && $("[id*=hdNav]").val() == "New") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClear', '');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 114 && $("[id*=hdNav]").val() == "All") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkFetch', '');
                        e.preventDefault();
                        return;
                    }
                        //else if (e.keyCode == 115 && $("[id*=hdNav]").val() == "New") {
                        //    console.log("Key");
                        //    __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                        //    e.preventDefault();
                        //    return;
                        //}
                        //else if (e.keyCode == 116 && $("[id*=hdNav]").val() == "All") {
                        //    __doPostBack('ctl00$ContentPlaceHolder1$lnkPrint', '');
                        //    e.preventDefault();
                        //    return;
                        //}
                    else if (e.keyCode == 117 && $("[id*=hdNav]").val() == "All") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkDelete', '');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 119 && $("[id*=hdNav]").val() == "New") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClose', '');
                        e.preventDefault();
                        return;
                    }
                    else if (e.keyCode == 120 && $("[id*=hdNav]").val() == "All") {
                        __doPostBack('ctl00$ContentPlaceHolder1$lnkClose', '');
                        e.preventDefault();
                        return;
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.message)
                }
            }
            //Get Batch Detail              
            function fncGetBatchDetails(inventorycode) {
                try {
                    //alert('fncGetBatchDetail_Master');
                    $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("~/WebService.asmx/DataTableToJSONWithJSONNet")%>',
                        data: "{'inventorycode':'" + inventorycode + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            tableCreateforbatchdetails(jQuery.parseJSON(msg.d));
                            $("#batchdetail").dialog('open');
                        },
                        error: function (data) {
                            alert(data);
                        }
                    });
                }
                catch (err) {
                    alert(err.Message);
                }
            }
            //Create Batch Detail table Dynamically
            function tableCreateforbatchdetails(jsonobj) {
                try {
                    var tableid = "dataGridBatchTable_master";
                    var tblbatchdetail = document.getElementById(tableid);
                    var batchDetailtableContainer = document.getElementById('BatchDetailtableContainer')
                    if (tblbatchdetail) {
                        batchDetailtableContainer.removeChild(tblbatchdetail);
                    }
                    tblbatchdetail = CreateTableFromJSON(jsonobj, tableid);
                    batchDetailtableContainer.appendChild(tblbatchdetail);
                    $("tbody.dataGridBatchTable_master_tablebody > tr").first().css("background-color", "Yellow");
                    $("tbody.dataGridBatchTable_master_tablebody > tr").on('click', function (event) {
                        //alert(event.currenttarget);
                        var batchNo = $(this).closest("tr").find('td.BatchNo').html();
                        var mrp = $(this).closest("tr").find('td.MRP').html();
                        var sellingprice = $(this).closest("tr").find('td.SellingPrice').html();
                        var expMonth = $(this).closest("tr").find('td.ExpMonth').html();
                        var expYear = $(this).closest("tr").find('td.ExpYear').html();
                        var basiccost = $(this).closest("tr").find('td.Basiccost').html();
                        var stock = $(this).closest("tr").find('td.Stock').html();
                        var netCost = $(this).closest("tr").find('td.NetCost').html();
                        //alert(basiccost);
                        $("#batchdetail").dialog('close');
                        fncAssignbatchvaluestotextboxes(batchNo, mrp, sellingprice, expMonth, expYear, basiccost, stock, netCost);
                    });
                    $("tbody.dataGridBatchTable_master_tablebody > tr").on('mouseover', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "Yellow");
                    });
                    $("tbody.dataGridBatchTable_master_tablebody > tr").on('mouseout', function (evt) {
                        var rowobj = $(this);
                        rowobj.css("background-color", "white");
                    });
                    $("tbody.dataGridBatchTable_master_tablebody > tr").on('keydown', function (evt) {
                        var rowobj = $(this);
                        var charCode = (evt.which) ? evt.which : evt.keyCode;

                        if (charCode == 13) {
                            rowobj.click();
                            return false;
                        }
                        else if (charCode == 40) {
                            var NextRowobj = rowobj.next();
                            if (NextRowobj.length > 0) {
                                NextRowobj.css("background-color", "Yellow");
                                NextRowobj.siblings().css("background-color", "white");
                                NextRowobj.select().focus();
                            }
                            else {
                                rowobj.siblings().css("background-color", "white");
                                rowobj.css("background-color", "white");
                                rowobj.siblings().first().css("background-color", "Yellow");
                                rowobj.siblings().first().select().focus();
                            }
                        }
                        else if (charCode == 38) {
                            var prevrowobj = rowobj.prev();
                            if (prevrowobj.length > 0) {
                                prevrowobj.css("background-color", "Yellow");
                                prevrowobj.siblings().css("background-color", "white");
                                prevrowobj.select().focus();
                            }
                            else {
                                rowobj.siblings().css("background-color", "white");
                                rowobj.css("background-color", "white");
                                rowobj.siblings().last().css("background-color", "Yellow");
                                rowobj.siblings().last().select().focus();
                            }
                        }

                    });
                }
                catch (err) {
                    alert(err.Message);
                }
            }
            //Assign Batch values to text box
            function fncAssignbatchvaluestotextboxes(batchno, mrp, sprice, expmonth, expyear, Basiccost) {
                try {
                    $("[id*=txtBatchNo]").val(batchno);
                    $("[id*=txtActualSelling]").val(sprice);
                    $("[id*=txtCost]").val(Basiccost);
                    //$("[id*=txtStock]").val(Basiccost);
                    __doPostBack('ctl00$ContentPlaceHolder1$btInventorySearch', '');
                }
                catch (err) {
                    alert(err.message);
                }
            }
        }
    </script>
    <script type="text/javascript">

        //Show Popup After Save
        function fncShowSuccessMessage() {
            try {
                fncToastInformation("Details Senting to Printer");
                //fncClearAll();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
                console.log(err);
            }
        }
        function SetDefaultDate(FromDate, Todate) {
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }
        function SetDefaultDates(PkdDate, BpDate) {
            PkdDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            BpDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            if (PkdDate.val() === '') {
                PkdDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            if (BpDate.val() === '') {
                BpDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }
        function fncDecimal() {
            try {
                $('#<%=txtActualSelling.ClientID%>').number(true, 2);
                $('#<%=txtCost.ClientID%>').number(true, 2);
                $('#<%=txtComm.ClientID%>').number(true, 2);
                $('#<%=txtNewSelling.ClientID%>').number(true, 2);
                $('#<%=txtQty.ClientID%>').number(true, 0);
                $('#<%=txtStock.ClientID%>').number(true, 2);
            }
            catch (err) {
                fncToastError(err.message);
            }
        }
        function ValidateForm() {
            var Show = '';
            if (document.getElementById("<%=txtInventoryCode.ClientID%>").value == "") {
                Show = Show + 'Please Enter Inventory Code';
                document.getElementById("<%=txtInventoryCode.ClientID %>").focus();
            }
            else if (document.getElementById("<%=txtNewSelling.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter New SellingPrice';
                document.getElementById("<%=txtNewSelling.ClientID %>").focus();
            }
            else if (document.getElementById("<%=txtQty.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Qty';
                document.getElementById("<%=txtQty.ClientID %>").focus();
            }
            else if (document.getElementById("<%=txtComm.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter Salesman Commission';
                document.getElementById("<%=txtComm.ClientID %>").focus();
            }
    if (Show != '') {
        ShowPopupMessageBox(Show);
        return false;
    }
    else {
        $("input").removeAttr('disabled');
        return true;
    }
}
function fncgetClearingSalesNo() {
    var obj;
    $('#<%=hdClearingSalesNo.ClientID%>').val('');
    $("#ContentPlaceHolder1_grdLoad tbody").children().each(function () {
        obj = $(this);
        if (obj.find('td span input[id*="chkSingle"]').is(":checked")) {
            if ($('#<%=hdClearingSalesNo.ClientID%>').val() != "") {
                $('#<%=hdClearingSalesNo.ClientID%>').val($('#<%=hdClearingSalesNo.ClientID%>').val() + ',' + $("td", obj).eq(4).text());
            }
            else {
                $('#<%=hdClearingSalesNo.ClientID%>').val($("td", obj).eq(4).text());
            }
        }
    });
}
function fncgetPrintSalesNo() {
    var obj;
    $('#<%=hdClearingSalesNo.ClientID%>').val('');
    $("#ContentPlaceHolder1_grdLoad tbody").children().each(function () {
        obj = $(this);
        if (obj.find('td span input[id*="chkSingle"]').is(":checked")) {
            if ($('#<%=hdClearingSalesNo.ClientID%>').val() != "") {
                $('#<%=hdClearingSalesNo.ClientID%>').val($('#<%=hdClearingSalesNo.ClientID%>').val() + ',' + $("td", obj).eq(4).text());
            }
            else {
                $('#<%=hdClearingSalesNo.ClientID%>').val($("td", obj).eq(4).text());
            }
        }
    });
}
//Print Validation
function fncPrintValidation() {
    try {
        if ($('#<%=ddSize.ClientID %>').val() == "") {
            ShowPopupMessageBox('Please Selct Size');
            return false;
        }
      <%--  else if ($('#<%=txtTemplate.ClientID %>').val() == "") {
            ShowPopupMessageBox('<%=Resources.LabelCaption.alert_BarcodeTemplate%>');
            return false;
        }--%>
        else {
            fncgetPrintSalesNo();
            fncgetPrintSalesQty();
        }
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }
}
function fncgetPrintSalesQty() {
    $('#<%=hdClearingSalesQty.ClientID%>').val('');
            $("#<%=grdLoad.ClientID %> input[name*='txtBarcQty']").each(function (index) {
                $('#<%=hdClearingSalesQty.ClientID%>').val($('#<%=hdClearingSalesQty.ClientID%>').val() + ',' + $.trim($(this).val()));
    });
}
//function DisplayChildDetails(row) {
//    try {
//        $("[id*=txtBatchNo]").val($("td", row).eq(0).html());
//        $("[id*=txtActualSelling]").val($("td", row).eq(2).html());
//        $("[id*=txtStock]").val($("td", row).eq(3).html());
//        $("[id*=txtCost]").val($("td", row).eq(6).html());
//    }
//    catch (err) {
//        alert(err.Message);
//        console.log(err);
//    }
//}
//function fncOpenBatch() {
//    $("#dialog-Batch").dialog({
//        autoOpen: true,
//        resizable: false,
//        height: "auto",
//        width: 850,
//        modal: true,
//        show: {
//            effect: "fade",
//            duration: 100
//        },
//        hide: {
//            effect: "fade",
//            duration: 500
//        }
//        ,
//        buttons: {
//            Exit: function () {
//                $(this).dialog("destroy");
//            }
//        }
//    });
//    //fncChangeGridColor();
//}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Clearing Sales</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <br />
        <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="server">
            <ContentTemplate>
                <div id="Tabs" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul id="tablist" class="nav nav-tabs custnav custnav-im" role="tablist">
                        <li id="liNew"><a href="#New" aria-controls="New" role="tab" data-toggle="tab">Add Clearing Sales</a></li>
                        <li id="liAll"><a href="#All" aria-controls="All" role="tab" data-toggle="tab">All Clearing Sales Item</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content" style="padding-top: 5px; overflow-y: scroll; height: 500px">
                        <div class="tab-pane active" role="tabpanel" id="New">
                            <div id="divMainContainer">
                                <div class="container-group-small_new">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-6">
                                        <div class="container-control">
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblItemcode" runat="server" Text="Inventory Code"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtInventoryCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtDescription" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblBatchNo" runat="server" Text="BatchNo"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtBatchNo" runat="server" onMouseDown="return false" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblActualSelling" runat="server" Text="Actual Selling"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtActualSelling" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblCost" runat="server" Text="Cost"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtCost" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-5">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblStock" runat="server" Text="Stock in Hand"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtStock" runat="server" onMouseDown="return false" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="control-group-single-res" style="padding-top: 15px">
                                                <div class="col-md-12">
                                                    <div class="col-md-4">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblNewSelling" runat="server" Text="New Selling  "></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtNewSelling" Text="0" Style="text-align: right;" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtQty" Text="0" Style="text-align: right;" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblComm" runat="server" Text="Comm %"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtComm" Text="0" Style="text-align: right;" runat="server" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 button-contol" style="padding-top: 20px">
                                    <div class="col-md-5">
                                    </div>
                                    <div class="col-md-1 control-button">
                                        <asp:LinkButton ID="lnkClear" runat="server" class="button-blue" OnClick="lnkClear_Click"><i class="icon-play"></i>Clear (F2)</asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 control-button">
                                        <asp:LinkButton ID="lnkNewClose" runat="server" class="button-blue" OnClick="lnkClose_Click"><i class="icon-play"></i>Close(F8)</asp:LinkButton>
                                    </div>
                                    <asp:UpdatePanel ID="updtPnlSave" UpdateMode="Conditional" runat="Server">
                                        <ContentTemplate>
                                            <div class="col-md-1 control-button">
                                                <asp:LinkButton ID="lnkSave" runat="server" class="button-blue" OnClick="lnkSave_Click" OnClientClick="return ValidateForm()"><i class="icon-play"></i>Save</asp:LinkButton>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <%--<asp:UpdatePanel ID="updtPnlBatch" UpdateMode="Conditional" runat="Server">
                                <ContentTemplate>
                                    <div id="dialog-Batch" style="display: none">
                                        <div class="grid-popup">
                                            <asp:GridView ID="grdBatch" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" OnRowDataBound="grdBatch_RowDataBound"
                                                CssClass="pshro_GridDgn">
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                                </EmptyDataTemplate>
                                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                <PagerStyle CssClass="pshro_text" />
                                                <Columns>
                                                    <asp:BoundField DataField="BatchNo" HeaderText="Batch No"></asp:BoundField>
                                                    <asp:BoundField DataField="MRP" HeaderText="MRP"></asp:BoundField>
                                                    <asp:BoundField DataField="SellingPrice" HeaderText="S.Price"></asp:BoundField>
                                                    <asp:BoundField DataField="BalanceQty" HeaderText="Stock"></asp:BoundField>
                                                    <asp:BoundField DataField="ExpMonth" HeaderText="ExpMonth"></asp:BoundField>
                                                    <asp:BoundField DataField="ExpYear" HeaderText="ExpYear"></asp:BoundField>
                                                    <asp:BoundField DataField="Basiccost" HeaderText="Basiccost" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="All">
                            <div id="divAllContainer">
                                <div class="container-group-small_new">
                                    <%--<div class="col-md-1">
                                    </div>--%>
                                    <div class="col-md-12" style="padding-left: 20px">
                                        <div class="container-control">
                                            <div class="control-group-single-res" style="padding-top: 10px">
                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblInventory" runat="server" Text="Inventory Code"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtItemCode" runat="server" MaxLength="20" CssClass="form-control-res"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="label-left form_bootstyle">
                                                            <asp:Label ID="lblFromDate" runat="server" Text="From"></asp:Label>
                                                        </div>
                                                        <div class="label-left form_bootstyle">
                                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkDeactivate" runat="server" Checked="false" Text="DeActivated" />
                                                        <%--OnCheckedChanged="chkDeactivate_CheckedChanged"--%>
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:LinkButton ID="lnkFetch" runat="server" OnClick="lnkFetch_Click" class="button-blue"><i class="icon-play"></i>Fetch(F3)</asp:LinkButton>
                                                        <%-- OnClientClick="return ValidateForm()"--%>
                                                    </div>
                                                    <div class="col-md-1">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkSelect" runat="server" Checked="false" Text="SelectAll" />
                                                        <%--onchange="fncGetAllRecords();"--%>
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkLoad" runat="server" Checked="false" Text="LoadQty" />
                                                        <%--OnCheckedChanged="chkLoad_CheckedChanged"--%>
                                                    </div>
                                                    <div class="col-md-3" style="padding-right: 10px">
                                                        <div class="label-left form_bootstyle">
                                                            <asp:Label ID="lblToDate" runat="server" Text="To"></asp:Label>
                                                        </div>
                                                        <div class="label-left form_bootstyle">
                                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-top: 10px">
                                                    <asp:UpdatePanel ID="updtPnlGrid1" UpdateMode="Conditional" runat="Server">
                                                        <ContentTemplate>
                                                            <script type="text/javascript">
                                                                jQuery(".main-table").clone(true).appendTo("#<%= tblHead.ClientID %>").addClass('clone');
                                                            </script>
                                                            <table rules="all" border="1" id="tblHead" runat="server" class="grdLoad fixed_header">
                                                                <tr>
                                                                    <td style="text-align: center"></td>
                                                                    <td style="text-align: center">Select</td>
                                                                    <td style="text-align: center">Barc.Code</td>
                                                                    <td style="text-align: center">InputQty</td>
                                                                    <td style="text-align: left">C Barcode</td>
                                                                    <td style="text-align: left">ItemCode</td>
                                                                    <td style="text-align: left">Batch No</td>
                                                                    <td style="text-align: left">ItemName</td>
                                                                    <td style="text-align: right">S Price</td>
                                                                    <td style="text-align: right">Actual Selling</td>
                                                                    <td style="text-align: right">MRP</td>
                                                                    <td style="text-align: center">SoldQty</td>
                                                                    <td style="text-align: right">Commission</td>
                                                                    <td style="text-align: left">CreateBy</td>
                                                                    <td style="text-align: left">CreateDate</td>
                                                                </tr>
                                                            </table>
                                                            <div class="GridDetails" id="gvItemLoad" style="overflow-x: hidden; overflow-y: scroll; height: 280px; width: 1325px; background-color: aliceblue;">
                                                                <asp:GridView ID="grdLoad" runat="server" AutoGenerateColumns="False"
                                                                    ShowHeaderWhenEmpty="false" CssClass="pshro_GridDgn grdLoad myGrid" ShowHeader="false">
                                                                    <%--OnRowDataBound="grdBatch_RowDataBound"--%>
                                                                    <EmptyDataTemplate>
                                                                        <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                                                    </EmptyDataTemplate>
                                                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                                                    <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                                                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                                                    <PagerStyle CssClass="pshro_text" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="SNo" ItemStyle-CssClass="text-center" />
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblSelect" DataField="Select" runat="server" Text="Select" ItemStyle-CssClass="text-center"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSingle" DataField="Select" runat="server" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                        </asp:TemplateField>
                                                                        <%--<asp:BoundField DataField="Select" HeaderText="Select" />--%>
                                                                        <%--<asp:BoundField DataField="BarcQty" HeaderText="BarC.Qty" ItemStyle-CssClass="text-center"></asp:BoundField>--%>
                                                                        <asp:TemplateField HeaderText="BarcQty">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtBarcQty" runat="server" Text='<%# Eval("BarcQty") %>' Style="text-align: center; padding-right: 5px;" onkeypress="return isNumberKey(event)" CssClass="grid-textbox"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <%--<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />--%>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="InputQty" HeaderText="InputQty" ItemStyle-CssClass="text-center"></asp:BoundField>
                                                                        <asp:BoundField DataField="ClearingBarcode" HeaderText="CBarcode" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                        <asp:BoundField DataField="Inventorycode" HeaderText="ItemCode" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                        <asp:BoundField DataField="BatchNo" HeaderText="Batch No" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                        <asp:BoundField DataField="Description" HeaderText="ItemName" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                        <asp:BoundField DataField="Price" HeaderText="S Price" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                                        <asp:BoundField DataField="Actualselling" HeaderText="Actual Selling" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                                        <asp:BoundField DataField="MRP" HeaderText="MRP" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                                        <asp:BoundField DataField="SoldQty" HeaderText="SoldQty" ItemStyle-CssClass="text-center"></asp:BoundField>
                                                                        <asp:BoundField DataField="Commission" HeaderText="Commission" ItemStyle-CssClass="text-right"></asp:BoundField>
                                                                        <asp:BoundField DataField="CreateUser" HeaderText="CreateBy" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                        <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" ItemStyle-CssClass="text-left"></asp:BoundField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="col-md-12" style="padding-top: 10px">
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblBestBefore" runat="server" Text="Best Before"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtBestBefore" runat="server" CssClass="form_textbox"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 control-button" style="padding-left: 10px">
                                                        <asp:CheckBox ID="chkMrp" runat="server" Checked="false" Text="Hide MRP" />
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkHidePrice" runat="server" Checked="false" Text="Hide Price" />
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkHideCompany" runat="server" Checked="false" Text="Hide Company Name" />
                                                    </div>
                                                    <div class="col-md-2 control-button" style="padding-left: 21px">
                                                        <asp:LinkButton ID="lnkPrint" runat="server" Width="170px" OnClick="lnkPrint_Click" OnClientClick="return fncPrintValidation()" class="button-blue"><i class="icon-play"></i>Print Barcode</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-top: 5px">
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left">
                                                            <asp:Label ID="lblPkd" runat="server" Text="PKD Date:"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:TextBox ID="txtPkd" runat="server" CssClass="form_textbox"></asp:TextBox>
                                                            <%--<asp:TextBox ID="txtPkd" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" style="margin-left: 10px">
                                                        <%--border-style: solid;--%>
                                                        <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <asp:RadioButton ID="rdoDate" Checked="true" runat="server" Text="Date" GroupName="Date" Class="radioboxlist" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:RadioButton ID="rdoMonth" runat="server" Text="Month" GroupName="Date" Class="radioboxlist" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 control-button">
                                                        <asp:CheckBox ID="chkDate" runat="server" Checked="false" Text="Hide PackedDate" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:CheckBox ID="chkExpiryDate" runat="server" Checked="false" Text="Hide ExpiryDate" />
                                                    </div>
                                                    <div class="col-md-2 control-button" style="margin-left: 20px">
                                                        <asp:LinkButton ID="lnkDelete" runat="server" Width="170px" OnClientClick="fncgetClearingSalesNo()" OnClick="lnkDelete_Click" class="button-blue"><i class="icon-play"></i>Delete (F6)</asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="padding-top: 5px">
                                                    <div class="col-md-3">
                                                        <div class="col-md-12">
                                                            <%--border-style: solid; --%>
                                                            <div class="col-md-4">
                                                                <asp:RadioButton ID="rdoSelling" Checked="true" runat="server" Text="S.Price" GroupName="Price" Class="radioboxlist" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <asp:RadioButton ID="rdoMrp" runat="server" Text="MRP" GroupName="Price" Class="radioboxlist" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left; padding-left: 15px">
                                                            <asp:Label ID="lblTemplate" runat="server" Text="Template"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                             <asp:DropDownList ID="ddTemplate" runat="server" Style="width: 100%"></asp:DropDownList>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-2">
                                                        <div class="label-left" style="text-align: left;">
                                                            <asp:Label ID="lblSize" runat="server" Text="Size"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddSize" runat="server" Style="width: 100%"></asp:DropDownList>
                                                            <%--<asp:TextBox ID="TextBox2" runat="server" CssClass="form-control-res"></asp:TextBox>--%>
                                                        </div>
                                                    </div>
                                                      <div class="col-md-3">
                                                        <div class="label-left" style="text-align: left; padding-left: 15px">
                                                            <asp:Label ID="Label1" runat="server" Text="PrinterName"></asp:Label>
                                                        </div>
                                                        <div class="label-right">
                                                            <asp:DropDownList ID="ddlPrinterName" runat="server" Style="width: 100%"></asp:DropDownList>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-1 control-button" style="padding-left: 20px;">
                                                        <asp:LinkButton ID="lnkClose" Width="170px" runat="server" OnClick="lnkClose_Click" class="button-blue"><i class="icon-play"></i>Close (F9)</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="col-md-1"></div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="hiddencol">
                    <asp:Button ID="btInventorySearch" runat="server" OnClick="InventorySearch_Click" />
                    <%--<asp:Button ID="btnEmptyValueCheck" runat="server" OnClientClick="fncRepackingValue();return false;" />--%>
                    <asp:HiddenField ID="hdNav" runat="server" />
                    <asp:HiddenField ID="hdClearingSalesNo" runat="server" />
                    <asp:HiddenField ID="hdClearingSalesQty" runat="server" />
                    <asp:HiddenField ID="HidenIsBatch" runat="server" ClientIDMode="Static" Value="False" />
                    <asp:HiddenField ID="HiddenItemCode" runat="server" ClientIDMode="Static" Value="" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
