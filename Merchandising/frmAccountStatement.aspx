﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAccountStatement.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmAccountStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
           .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 200px;
            max-width: 200px;
            text-align: right !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 200px;
            max-width: 200px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 120px;
            max-width: 120px;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AccountStatement');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AccountStatement";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>
    <script type="text/javascript">
        function fncDateFormat() {
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
        }
        function pageLoad() {
            try {
                fncDateFormat();
                if ($("#<% =hidBalance.ClientID%>").val() != "") {
                    $("#<% =lblBalance.ClientID%>").text("View Balance");
                    $("#<% =lblBalance.ClientID%>").css("background-color", "pink");
                    $("#<% =lblBalance.ClientID%>").hover(function () {
                        $(this).text($("#<% =hidBalance.ClientID%>").val() + " Rs");
                        $(this).css("background-color", "yellow");
                    }, function () {
                        $(this).text("View Balance");
                        $(this).css("background-color", "pink");
                    });
                }
                else {
                    $("#<% =lblBalance.ClientID%>").text('0');
                }

            }
            catch (e) {
                ShowPopupMessageBox(e.message);
            }
        }
        function ValidateForm() {
            var Show = '';

            if (document.getElementById("<%=txtFromDate.ClientID%>").value == "") {
                Show = 'Please Enter From Date';
            }
            if (document.getElementById("<%=txtToDate.ClientID%>").value == "") {
                Show = Show + '<br />Please Enter To Date';
            }
            if (Show != '') {
                ShowPopupMessageBox(Show);
                return false;
            }
            else {
                return true;
                $("#<% =btnStateMent.ClientID%>").click();
            }
        }

        function clearForm() {
            $("#<% =lblBalance.ClientID%>").text('0');
            $("#<% =grdStatement.ClientID%>").remove();
            $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $("#<%=txtFromDate.ClientID %>").focus();
             $("#<% =lblBalance.ClientID%>").css("background-color", "");
            fncDateFormat();
            return false;
        }

        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 115) {
                    if ($('#<%=lnkSave.ClientID %>').is(":visible")) {
                         if (ValidateForm() == true) {
                             __doPostBack('ctl00$ContentPlaceHolder1$lnkSave', '');
                         }
                     }
                     e.preventDefault();
                 }
                 else if (e.keyCode == 117) {
                     $('#<%= lnkClear.ClientID %>').click();
                        e.preventDefault();
                    }
            }
        };
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

        function Toast(msg) {
            ShowPopupMessageBox(msg);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container" style="overflow: hidden">
        <div id="breadcrumbs" class="breadcrumbs" runat="Server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Management</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">ICICI</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Account Statement & Balance Enquiry</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-small" style="margin-top: 10px;">
            <div class="whole-price-header">
                Account Statement & Balance Enquiry
            </div>

            <div class="col-md-12">
                <div class="col-md-4">
                    <img src="../images/icici_bank _logo_.jpg" style="width: 100%;" /><%--style ="background-image: url('../images/icici_bank_logo.jpg');"--%>
                </div>
                <div class="col-md-8"></div>
            </div>
            <div class="control-group-single-res" style="margin-top: 10px;">
                <div class="label-left">
                    <asp:LinkButton ID="lnkBalance" runat="server" class="button-blue" OnClick="lnkBalance_Click"><i class="icon-play"></i>Balance</asp:LinkButton>
                </div>
                <div class="label-right">
                    <asp:Label ID="lblBalance" runat="server" Style="font-weight: 900; font-size: 18px;" Text="0"></asp:Label>
                </div>
            </div>
            <div class="container-control">
                <div class="control-group-single-res">
                    <div class="label-left">
                        <asp:Label ID="Label3" runat="server" Text="From Date"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group-single-res">
                    <div class="label-left">
                        <asp:Label ID="Label4" runat="server" Text="To Date"></asp:Label><span class="mandatory">*</span>
                    </div>
                    <div class="label-right">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="button-contol">
                <div class="control-button">
                    <asp:LinkButton ID="lnkSave" runat="server" class="button-red" OnClick="lnkStatement_Click"
                        OnClientClick="return ValidateForm();"><i class="icon-play"></i>Statement(F4)</asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm();"><i class="icon-play"></i>Clear(F6)</asp:LinkButton>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="grdLoad">
                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                    <thead>
                        <tr>
                            <th scope="col">Date 
                            </th>
                            <th scope="col">Amount
                            </th>
                            <th scope="col">Cheque Number
                            </th>
                            <th scope="col">Txn Date
                            </th>
                            <th scope="col">Remarks 
                            </th>
                            <th scope="col">Transaction ID
                            </th>
                            <th scope="col">Type
                            </th>
                        </tr>
                    </thead>
                </table>
                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 176px; width: 1338px; background-color: aliceblue;">
                    <asp:GridView ID="grdStatement" runat="server" AllowSorting="true"
                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" oncopy="return false"
                        AllowPaging="True" PageSize="10" ShowHeader="false" CssClass="pshro_GridDgn grdLoad">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle tbl_left" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative tbl_left" />
                        <Columns>
                            <asp:BoundField DataField="VALUEDATE" HeaderText="VALUE DATE"></asp:BoundField>
                            <asp:BoundField DataField="AMOUNT" HeaderText="AMOUNT"></asp:BoundField>
                            <asp:BoundField DataField="CHEQUENO" HeaderText="CHEQUE NO"></asp:BoundField>
                            <asp:BoundField DataField="TXNDATE" HeaderText="TXNDATE"></asp:BoundField>
                            <asp:BoundField DataField="REMARKS" HeaderText="REMARKS"></asp:BoundField>
                            <asp:BoundField DataField="TRANSACTIONID" HeaderText="TRANSACTION ID"></asp:BoundField>
                            <asp:BoundField DataField="TYPE" HeaderText="TYPE"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="button-contol">
            <div class="control-button">
                <asp:LinkButton ID="lnkExport" runat="server" class="button-red" OnClick="lnkExport_Click"><i class="icon-play"></i>Export</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidBalance" runat="server" Value="" />
    <div class ="display_none">
        <asp:Button ID ="btnStateMent" runat ="server" OnClick ="lnkStatement_Click" />
    </div>
</asp:Content>
