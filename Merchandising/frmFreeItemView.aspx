﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmFreeItemView.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmFreeItemView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .FreeItem_View td:nth-child(1), .FreeItem_View th:nth-child(1) {
            min-width: 80px;
            max-width: 80px;
        }

        .FreeItem_View td:nth-child(2), .FreeItem_View th:nth-child(2) {
            min-width: 80px;
            max-width: 80px;
        }

        .FreeItem_View td:nth-child(3), .FreeItem_View th:nth-child(3) {
            min-width: 100px;
            max-width: 100px;
        }

        .FreeItem_View td:nth-child(4), .FreeItem_View th:nth-child(4) {
            min-width: 200px;
            max-width: 200px;
        }

        .FreeItem_View td:nth-child(5), .FreeItem_View th:nth-child(5) {
            min-width: 250px;
            max-width: 250px;
        }

        .FreeItem_View td:nth-child(6), .FreeItem_View th:nth-child(6) {
            min-width: 135px;
            max-width: 135px;
        }

        .FreeItem_View td:nth-child(7), .FreeItem_View th:nth-child(7) {
            min-width: 135px;
            max-width: 135px;
        }

        .FreeItem_View td:nth-child(8), .FreeItem_View th:nth-child(8) {
            min-width: 110px;
            max-width: 110px;
        }

        .FreeItem_View td:nth-child(9), .FreeItem_View th:nth-child(9) {
            min-width: 110px;
            max-width: 110px;
        }

        .FreeItem_View td:nth-child(10), .FreeItem_View th:nth-child(10) {
            min-width: 100px;
            max-width: 100px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'FreeItemView');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "FreeItemView";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">
        ///Page Load
        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };
        function pageLoad() {
            try {
                 if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkNew.ClientID %>').css("display", "block");
                    $('#<%=lnkReToLive.ClientID %>').css("display", "block");
                    $('#<%=lnkRevokeGroup.ClientID %>').css("display", "block"); 
                }
                else {
                    $('#<%=lnkNew.ClientID %>').css("display", "none"); 
                    $('#<%=lnkReToLive.ClientID %>').css("display", "none"); 
                    $('#<%=lnkRevokeGroup.ClientID %>').css("display", "none"); 
                }
                fncGetLiveItem();
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //        ///Refresh Click
        //        function fncRefreshClick() {
        //            try {

        //                <%--fncGetFreeItemData('Refresh', $("#<%=ddlGroupcode.ClientID %>").val())--%>
        //            }
        //            catch (err) {
        //                ShowPopupMessageBox(err.message);
        //            }
        //        }
        //Get Free Item Data
        function fncGetFreeItemData(mode, groupcode) {
            try {
                var obj = {};
                obj.mode = mode;
                obj.groupcode = groupcode;
                $.ajax({
                    type: "POST",
                    url: "frmFreeItemView.aspx/fncGetFreeItemData",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        fncCreateFreeItemsTable(msg);
                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        ///Create Free Items Table
        function fncCreateFreeItemsTable(msg) {
            try {

                var objFreeItemAll, tblFreeItemsBody, row;
                objSupplierNote = jQuery.parseJSON(msg.d);

                tblFreeItemsBody = $("#tblFreeItems tbody");
                tblFreeItemsBody.children().remove();
                for (var i = 0; i < objSupplierNote.length; i++) {
                    row = "<tr><td>" +
                        $.trim(objSupplierNote[i]["RowNo"]) + "</td><td>" +
                        "<button type='button' onclick='fncViewFreeItem(this);' >View</button>" + "</td><td id='tdLocationcode_" + i + "' >" +
                        $.trim(objSupplierNote[i]["Location"]) + "</td><td id='tdGroupcode_" + i + "' >" +
                        $.trim(objSupplierNote[i]["GroupCode"]) + "</td><td>" +
                        $.trim(objSupplierNote[i]["Description"]) + "</td><td >" +
                        $.trim(objSupplierNote[i]["ValidFrom"]) + "</td><td>" +
                        $.trim(objSupplierNote[i]["ValidTo"]) + "</td><td> " +
                        $.trim(objSupplierNote[i]["BuyQty"].toFixed(2)) + "</td><td> " +
                        $.trim(objSupplierNote[i]["OfferedQty"].toFixed(2)) + "</td><td id='tdcheckbox_" + i + "' >" +
                        "<input type='checkbox' id='tdcheckbox_" + i + "' onclick='fncCheckBoxRowClick(this);' />" +
                        "</td></tr>";
                    tblFreeItemsBody.append(row);
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Get Live Free Item
        function fncGetLiveItem() {
            try {
                fncGetFreeItemData('LiveItem', $("#<%=txtGroupcode.ClientID %>").val());
                $('#Revok_button').show();
                $('#Live_button').hide();
                $("#<%=hidMode.ClientID %>").val('Live');

                //                $('#<%=lnkRevokeGroup.ClientID %>').css("visibility", "visible");
                //                $('#<%=lnkReToLive.ClientID %>').css("visibility", "hidden");
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //Get Revoked Free Item
        function fncGetRevokedFreeItem() {
            try {
                fncGetFreeItemData('RevokedItem', $("#<%=txtGroupcode.ClientID %>").val());

                //                $('#<%=lnkRevokeGroup.ClientID %>').css("visibility", "hidden");
                //                $('#<%=lnkReToLive.ClientID %>').css("visibility", "visible");

                $('#Revok_button').hide();
                $('#Live_button').show();
                $("#<%=hidMode.ClientID %>").val('Revoke');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //select All
        function fncSelectAllClick(source) {
            try {

                if (($(source).is(":checked"))) {
                    $("#tblFreeItems [id*=tdcheckbox]").attr("checked", "checked");
                }
                else {
                    $("#tblFreeItems [id*=tdcheckbox]").removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        //table Row CheckBox Click
        function fncCheckBoxRowClick(source) {
            try {
                if ($("#tblFreeItems tbody").children().length == $("#tblFreeItems [id*=tdcheckbox]:checked").length) {
                    $("#tblFreeItems [id*=cbSelectAll]").attr("checked", "checked");
                }
                else {
                    $("#tblFreeItems [id*=cbSelectAll]").removeAttr("checked");
                }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSaveValidation() {
            try {
                if ($("#tblFreeItems tbody").children().length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_RecordNotFound%>');
                    return false;
                }
                else if ($("#tblFreeItems [id*=tdcheckbox]:checked").length == 0) {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.Alert_Selctonerow%>');
                        return false;
                    }
                return true;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Revoke Free Item
        function fncLiveAndRevoke(mode) {
            try {
                var freeItem, rowObj;
                if (fncSaveValidation() == false)
                    return;

                freeItem = "<table>"
                $("#tblFreeItems [id*=tdcheckbox]:checked").each(function () {
                    rowObj = $(this).parent().parent();
                    freeItem = freeItem + "<FreeItem Groupcode='" + $.trim(rowObj.find('td[id*=tdGroupcode]').text()) + "'";
                    freeItem = freeItem + " Locationcode='" + $.trim(rowObj.find('td[id*=tdLocationcode]').text()) + "'";
                    freeItem = freeItem + "></FreeItem>";
                });
                freeItem = freeItem + "</table>";

                fncSaveFreeItem(mode, freeItem);

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///Save Revoke to Live and Live to Revoke
        function fncSaveFreeItem(mode, xmlfile) {
            try {
                var obj = {};
                obj.mode = mode;
                obj.xmlFreeItem = escape(xmlfile);
                $.ajax({
                    type: "POST",
                    url: "frmFreeItemView.aspx/fncSaveFreeItem",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != "") {
                            if (mode == "RevokeToLive") {
                                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_RevoketoLive%>');
                                fncGetRevokedFreeItem();
                            }
                            else if (mode == "LiveToRevoke") {
                                ShowPopupMessageBox('<%=Resources.LabelCaption.alert_LiveToRevoke%>');
                                    fncGetLiveItem();
                                }

                        }

                    },
                    error: function (data) {
                        ShowPopupMessageBox(data.message);
                    }
                });
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        ///View To view Free Item List
        function fncViewFreeItem(source) {
            try {
                var rowObj;
                if ($('#<%=hidViewbtn.ClientID%>').val() == "V1") {
                    rowObj = $(source).parent().parent();
                    $('#<%=hidGroupCode.ClientID %>').val(rowObj.find('td[id*=tdGroupcode]').text());
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkNew', '');
                }
                else {
                    ShowPopupMessageBox("You have permission to view this promotion");
                    return false;
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        //Clear Group Code
        function fncClearGroup() {
            try {
                $('#<%=hidGroupCode.ClientID %>').val('');
                $("#<%=hidMode.ClientID %>").val('');
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        function fncSetValue() {
            try {
                //fncVendorChange();//by velu 29122018
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
            <li class="active-page">Free Items Group </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <div class="supplierNote-group-full">
        <div>
            <div class="float_left">
                <div class="float_left">
                    <asp:Label ID="lblGroupCode" runat="server" Text='<%$ Resources:LabelCaption,lbl_GroupCode %>'></asp:Label>
                </div>
                <div class="freeItem_groupCode">
                    <%--<asp:DropDownList ID="ddlGroupcode" runat="server" Style="width: 100%" onchange="fncVendorChange()">
                    </asp:DropDownList>--%>
                    <asp:TextBox ID="txtGroupcode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'GroupCode', 'txtGroupcode', '');"></asp:TextBox>
                </div>
            </div>
            <div class="freeItemView_button">
                <div class="control-button">
                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_New %>'
                        OnClick="lnkNew_Click" OnClientClick="fncClearGroup();"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkLiveItem" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_livefreeItem %>'
                        OnClientClick="fncGetLiveItem();return false;"></asp:LinkButton>
                </div>
                <div class="control-button">
                    <asp:LinkButton ID="lnkRevokeItem" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_revokefreeItem %>'
                        OnClientClick="fncGetRevokedFreeItem();return false;"></asp:LinkButton>
                </div>
                <%--<div class="control-button">
                    <asp:LinkButton ID="lnkRefresh" runat="server" class="button-blue" OnClientClick="fncRefreshClick();return false;"
                        Text='<%$ Resources:LabelCaption,btn_refresh %>'></asp:LinkButton>
                </div>--%>
            </div>
        </div>
        <div class="Payment_fixed_headers FreeItem_View">
            <table id="tblFreeItems" cellspacing="0" rules="all" border="1">
                <thead>
                    <tr>
                        <th scope="col">S.No
                        </th>
                        <th scope="col">View
                        </th>
                        <th scope="col">Location
                        </th>
                        <th scope="col">Group Code
                        </th>
                        <th scope="col">Description
                        </th>
                        <th scope="col">Valid From Date
                        </th>
                        <th scope="col">Valid To Date
                        </th>
                        <th scope="col">Buy Qty
                        </th>
                        <th scope="col">Free Qty
                        </th>
                        <th scope="col">
                            <asp:CheckBox ID="cbSelectAll" runat="server" onclick="fncSelectAllClick(this);" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="freeItemView_button">
            <div id="Live_button" class="control-button">
                <asp:LinkButton ID="lnkReToLive" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_liveRevokedGroup %>'
                    OnClientClick="fncLiveAndRevoke('RevokeToLive');return false;"></asp:LinkButton>
            </div>
            <div id="Revok_button" class="control-button">
                <asp:LinkButton ID="lnkRevokeGroup" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,lbl_RevokeGroup %>'
                    OnClientClick="fncLiveAndRevoke('LiveToRevoke');return false;"></asp:LinkButton>
            </div>
        </div>
        <div class="hiddencol">
            <asp:HiddenField ID="hidGroupCode" runat="server" />
            <asp:HiddenField ID="hidMode" runat="server" />
            <asp:HiddenField ID="hidSavebtn" runat="server" />
            <asp:HiddenField ID="hidDeletebtn" runat="server" />
            <asp:HiddenField ID="hidEditbtn" runat="server" />
            <asp:HiddenField ID="hidViewbtn" runat="server" />
        </div>
    </div>
</asp:Content>
