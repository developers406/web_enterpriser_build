﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmDayBasedPromotionEdit.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmDayBasedPromotionEdit" %>

<%@ Register TagPrefix="ups" TagName="PaginationUserControl" Src="~/UserControls/PaginationUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 190px;
            max-width: 190px;
        }

        .grdLoad td:nth-child(2) {
            text-align: left !important;
            padding: 3px !important;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 460PX;
            max-width: 460PX;
        }

        .grdLoad td:nth-child(3) {
            text-align: left !important;
            padding: 3px !important;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(4) {
            text-align: left !important;
            padding: 3px !important;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 130px;
            max-width: 130px;
        }

        .grdLoad td:nth-child(5) {
            text-align: left !important;
            padding: 3px !important;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 260px;
            max-width: 260px;
        }

        .grdLoad td:nth-child(6) {
            text-align: left !important;
            padding: 3px !important;
        }
    </style>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'DayBasedPromotionEdit');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "DayBasedPromotionEdit";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>


    <script type ="text/javascript">
        function pageLoad() {
              if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                    $('#<%=lnkBack.ClientID %>').css("display", "block");
                }
                else {
                    $('#<%=lnkBack.ClientID %>').css("display", "none");
                }
            $("select").chosen({ width: '100%' });
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divMain" runat="Server">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">
                    <%=Resources.LabelCaption.lblHome%></a>  <i class="fa fa-angle-right"></i></li>
                <li>Merchandising <i class="fa fa-angle-right"></i></li>
                <li class="active-page">Day Based Promotions View</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
            <script src="../js/jsPromotionMnt.js" type="text/javascript"></script>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
                <div style="margin-left: 20px;">
                    <div class="grid-search">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkSearchGrid">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="TextboxSize" placeholder="Enter Search Text"></asp:TextBox>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkSearchGrid" runat="server" class="button-blue" Width="100px" OnClick="lnkSearchGrid_Click"
                                OnClientClick="return Validate()">Search</asp:LinkButton>
                            <asp:LinkButton ID="lnkBack" runat="server" class="button-blue" Width="100px" PostBackUrl="~/Merchandising/frmDayBasedPromotion.aspx">New</asp:LinkButton>
                            <div>
                                <div class="float_left" style="margin-left: 130px;">
                    <asp:Label ID="Label1" runat="server" Text="FromDate"></asp:Label>
                </div>

                <div class="float_left" style="margin-left: 10px;">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" BackColor="White"></asp:TextBox>
                </div>
                <div class="control-group-single-res">
                    <div class="float_left" style="margin-left: 565px;">
                        <asp:Label ID="Label2" runat="server" Text="ToDate"></asp:Label>
                    </div> 
                    <div style ="float:left;margin-right: 15px;">
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" Style ="margin-left: 27px;width: 87%;" BackColor="White"></asp:TextBox>
                    </div>
                     <asp:LinkButton ID="lnkfilter" runat="server" class="button-blue" style ="margin-top: -5px;" OnClick ="lnkDatefilter_Click">Filter</asp:LinkButton>
                </div>
                                <div class="float_right" style ="margin-right: 30px;margin-top: -30px;"> 
                                    <asp:DropDownList ID="ddlFilter" style ="width:120px;" runat ="server" AutoPostBack="True" OnSelectedIndexChanged ="chFilter_Click">
                                        <asp:ListItem Text ="All" Value ="A"></asp:ListItem>
                                        <asp:ListItem Text ="Active" Value ="Ac"></asp:ListItem>
                                        <asp:ListItem Text ="Deactive" Value ="D"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="grdLoad" style="margin-top: 5px">
                        <table id="tblDays" cellspacing="0" rules="all" border="1" class="fixed_header">
                            <thead>
                                <tr>
                                    <th scope="col">Edit
                                    </th>
                                    <th scope="col">Promotion Code
                                    </th>
                                    <th scope="col">Description
                                    </th>
                                    <th scope="col">FromDate
                                    </th>
                                    <th scope="col">ToDate
                                    </th>
                                    <th scope="col">Promotion Days 
                                    </th>
                                </tr>
                            </thead>
                        </table>
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 351px; width: 1318px; overflow-x: hidden; overflow-y: scroll;">
                            <asp:GridView ID="gvDayPrEdit" runat="server" Width="100%" AutoGenerateColumns="False" ShowHeader="false"
                                ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" oncopy="return false" DataKeyNames="PromotionCode">
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" runat="server" ImageUrl="~/images/edit-icon.png"
                                                ToolTip="Click here to edit" OnClick="lnkImage_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PromotionCode" HeaderText="Promotion Code"></asp:BoundField>
                                    <asp:BoundField DataField="PromotionDescription" HeaderText="Promotion Description"></asp:BoundField>
                                    <asp:BoundField DataField="FromDate" HeaderText="FromDate" />
                                    <asp:BoundField DataField="ToDate" HeaderText="ToDate"></asp:BoundField>
                                    <asp:BoundField DataField="ProDays" HeaderText="FromTime"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <ups:PaginationUserControl runat="server" ID="DayBasedPromo" OnPaginationButtonClick="DayBasedPro_PaginationButtonClick" />
    </div>
    <asp:HiddenField ID="hidSavebtn" runat="server" />
    <asp:HiddenField ID="hidDeletebtn" runat="server" />
    <asp:HiddenField ID="hidEditbtn" runat="server" />
    <asp:HiddenField ID="hidViewbtn" runat="server" />
</asp:Content>
