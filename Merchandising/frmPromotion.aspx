﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmPromotion.aspx.cs" Inherits="EnterpriserWebFinal.Merchandising.frmPromotion" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        } 

        .no-close .ui-dialog-titlebar-close {
            display: none;
        }

        .grdLoad td:nth-child(1), .grdLoad th:nth-child(1) {
            min-width: 150px;
            max-width: 150px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(2), .grdLoad th:nth-child(2) {
            min-width: 385px;
            max-width: 385px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(3), .grdLoad th:nth-child(3) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(4), .grdLoad th:nth-child(4) {
            min-width: 120px;
            max-width: 120px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(5), .grdLoad th:nth-child(5) {
            min-width: 135px;
            max-width: 135px;
            text-align: center;
        }

        .grdLoad td:nth-child(6), .grdLoad th:nth-child(6) {
            min-width: 135px;
            max-width: 135px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(7), .grdLoad th:nth-child(7) {
            min-width: 135px;
            max-width: 135px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(8), .grdLoad th:nth-child(8) {
            min-width: 135px;
            max-width: 135px;
            text-align: left;
            padding: 0px 3px 0px 3px;
        }

        .grdLoad td:nth-child(9), .grdLoad th:nth-child(9) {
            min-width: 60px;
            max-width: 60px;
            text-align: center;
        }

    </style>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'Promotion');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "Promotion";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime(); 

            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }
        function pageLoad() {
            if ($('#<%=hidSavebtn.ClientID%>').val() == "N1") {
                $('#<%=lnkNew.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkNew.ClientID %>').css("display", "none");
            }
            if ($('#<%=hidEditbtn.ClientID%>').val() == "E1") {
                $('#<%=lnkModify.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkModify.ClientID %>').css("display", "none");
            }
            if ($('#<%=hidDeletebtn.ClientID%>').val() == "D1") {
                $('#<%=lnkDeActive.ClientID %>').css("display", "block");
            }
            else {
                $('#<%=lnkDeActive.ClientID %>').css("display", "none");
            }

            $("select").chosen({ width: '100%' }); // width in px, %, em, etc     


            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "1");
        }
        function OnGetRowValues(values) {
            $("#<%=txtFromDate.ClientID %>").val(values[0]);
            $("#<%=txtToDate.ClientID %>").val(values[1]);
            $("#<%=hidPromotioncode.ClientID %>").val(values[2]);
        }

        function fncActiveAndDeActive() {
            try {
                if ($("#<%=hidPromotioncode.ClientID %>").val() == "") {
                    ShowPopupMessageBox('<%=Resources.LabelCaption.msg_SelectProCode%>');
                    return false;
                }

            }
            catch (err) {
                alert(err.message);
            }
        }

        function fncModifyVal() {
            var frmDate, toDate;
            try {
                frmDate = $('#<%=txtFromDate.ClientID %>').val();
                frmDate = frmDate.split("/").reverse().join("-");

                toDate = $('#<%=txtToDate.ClientID %>').val();
                toDate = toDate.split("/").reverse().join("-");

<%--                if (new Date(frmDate) < new Date(currentDatesqlformat_Master)) {
                    fncToastInformation('<%=Resources.LabelCaption.alert_fromdategreaterthencurDate%>');
                    return false;
                }
                else if (new Date(toDate) < new Date(frmDate)) {
                    fncToastInformation('<%=Resources.LabelCaption.alert_TodategreaterthenFromDate%>');
                    return false;
                }--%>
                <%--else if ($("#<%=hidPromotioncode.ClientID %>").val() == "") {
                    fncToastInformation('<%=Resources.LabelCaption.msg_SelectProCode%>');
                    return false;
                }--%>
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function Pr_Edit(source) {
            try {
                if ($('#<%=hidEditbtn.ClientID%>').val() != "E1") {
                    ShowPopupMessageBox("You have no permission to Edit this Permission");
                    return false;
                }
                $('#<%=hidPromotioncode.ClientID %>').val($("td", $(source).closest("tr")).eq(0).html().replace(/&nbsp;/g, ''));
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
            $("[id*=grdPromotion] tr").live('click', function (event) {
                $("[id*=grdPromotion]").closest('tr').css("background-color", "");
                $(this).closest('tr').css("background-color", "#acb3a4");
                $("#<%=hidPromotioncode.ClientID %>").val($("td", $(this).closest("tr")).eq(0).html().replace(/&nbsp;/g, ''))
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="background: #e6f2ff">
        <div class="container-group-full">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                    </li>
                    <li><a style="text-decoration: none;">Merchandising</a><i class="fa fa-angle-right"></i></li>
                    <li><a style="text-decoration: none;">Promotions</a><i class="fa fa-angle-right"></i></li>
                    <li class="active-page">Promotion View </li>
                    <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
                </ul>
            </div>
        </div>
        <div class="container-group-full EnableScroll" style="overflow-x: scroll;">
            <div class="centerposition">
                <div style="width: 920px">
                    <div style="float: left">
                        <div class="control-button1">
                            <asp:UpdatePanel ID="upMode" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlMode" runat="server" Style="width: 100px;" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlMode_SelectedIndexChanged">
                                        <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                        <asp:ListItem Text="DeActive" Value="DeActive"></asp:ListItem>
                                        <%--<asp:ListItem Text="All" Value="All"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="padding-left: 150px">
                        <div class="control-button">
                            <asp:Label ID="lblFromDate" runat="server" Text="FromDate"></asp:Label>
                        </div>
                        <div class="control-button">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                        <div class="control-button">
                            <asp:Label ID="lblToDate" runat="server" Text="ToDate"></asp:Label>
                        </div>
                        <div class="control-button">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                        </div>
                    </div>
                    <div class="control-button" style="float: right">
                        <%--dinesh--%>
                        <asp:LinkButton ID="lnkClearSales" runat="server" class="button-blue" OnClick="lnkClearSales_Click">Clearing Sales</asp:LinkButton>
                    </div>
                </div>
                <asp:UpdatePanel ID="upnlPromotionSum" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <div class="row">
                            <%--<div class="col-xs-12 fw_light m_bottom_45 m_xs_bottom_30">--%>
                            <div class="grdLoad">
                                <table id="tblItemhistory" cellspacing="0" rules="all" border="1" class="fixed_header">
                                    <thead>
                                        <tr>
                                            <th scope="col">Promotion Code
                                            </th>
                                            <th scope="col">Description
                                            </th>
                                            <th scope="col">From Date
                                            </th>
                                            <th scope="col">To Date
                                            </th>
                                            <th scope="col">Status
                                            </th>
                                            <th scope="col">Basis
                                            </th>
                                            <th scope="col">Type
                                            </th>
                                            <th scope="col">Mode
                                            </th>
                                            <th scope="col">Modify
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                                <div class="GridDetails" style="overflow-x: hidden; overflow-y: scroll; height: 435px; width: 1395px; background-color: aliceblue;">
                                    <asp:GridView ID="grdPromotion" runat="server"
                                        AutoGenerateColumns="False" ShowHeaderWhenEmpty="false"
                                        ShowHeader="false" CssClass="pshro_GridDgn grdLoad"
                                        DataKeyNames="PromotionCode">
                                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                        <RowStyle CssClass="pshro_GridDgnStyle" />
                                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                        </EmptyDataTemplate>

                                        <Columns>
                                            <asp:BoundField DataField="PromotionCode" HeaderText="PromotionCode"></asp:BoundField>
                                            <asp:BoundField DataField="PromotionDescription" HeaderText="PromotionDescription"></asp:BoundField>
                                            <asp:BoundField DataField="FromDate" HeaderText="From Date" />
                                            <asp:BoundField DataField="ToDate" HeaderText="ToDate"></asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="Status" />
                                            <asp:BoundField DataField="PromotionBasis" HeaderText="PromotionBasis"></asp:BoundField>
                                            <asp:BoundField DataField="PromotionType" HeaderText="PromotionType"></asp:BoundField>
                                            <asp:BoundField DataField="PromotionMode" HeaderText="PromotionMode"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Modify">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnModify" runat="server" ImageUrl="~/images/edit-icon.png"
                                                        ToolTip="Click here to Modify" OnClientClick="return Pr_Edit(this);"
                                                        OnClick="lnkModify_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div>
                    <div class="rightalign">
                        <asp:UpdatePanel runat="server" ID="upButton">
                            <ContentTemplate>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkNew" runat="server" class="button-blue" OnClick="lnkNew_Click">New</asp:LinkButton>
                                </div>
                                <div class="control-button" style="display: none;">
                                    <asp:LinkButton ID="lnkModify" runat="server" class="button-blue" OnClick="lnkModify_Click">Modify</asp:LinkButton>
                                    <%--OnClientClick=" return fncModifyVal(); "--%>
                                </div>
                                <div class="control-button">
                                    <asp:LinkButton ID="lnkDeActive" runat="server" class="button-blue" OnClick="lnkDeActive_Click" Text='DeActive'
                                        OnClientClick=" return fncActiveAndDeActive(); "></asp:LinkButton>
                                    <asp:HiddenField ID="hidPromotioncode" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hiddencol">
        <asp:HiddenField ID="hidSavebtn" runat="server" />
        <asp:HiddenField ID="hidDeletebtn" runat="server" />
        <asp:HiddenField ID="hidEditbtn" runat="server" />
        <asp:HiddenField ID="hidViewbtn" runat="server" />
    </div>
</asp:Content>
