﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPRReqReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmPRReqReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PRReqReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PRReqReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">

        function pageLoad() {

            shortcut.add("Ctrl+A", function () {
            $('#breadcrumbs_text').removeClass('lowercase');
            $('#breadcrumbs_text').addClass('uppercase');
            $("#<%=hidValBilltype.ClientID%>").val('A');

        });
        shortcut.add("Ctrl+B", function () {
            $('#breadcrumbs_text').removeClass('uppercase');
            $('#breadcrumbs_text').addClass('lowercase');
            $("#<%=hidValBilltype.ClientID%>").val('B');
        });
              shortcut.add("Ctrl+C", function () { 
            $('#breadcrumbs_text').removeClass('uppercase');
            $('#breadcrumbs_text').removeClass('lowercase');
            $('#breadcrumbs_text').text('Purchase Return Request Report');
            $("#<%=hidValBilltype.ClientID%>").val('C');
        });

            $("#<%=txtReturnNo.ClientID%>").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.Code = escape(request.term);
                    obj.Type = 'ReturnNo';
                    $.ajax({
                        url: '<%=ResolveUrl("frmPRReqReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[1] + '-' + item.split('/')[0],
                                    val: item.split('/')[0]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
               focus: function (event, i) {
                   $("[id*=txtReturnNo]").val(i.item.val);
                   event.preventDefault();
               },
               //==========================> After Select Value
               select: function (e, i) {
                   $("[id*=txtReturnNo]").val(i.item.val);
                   return false;
               },
               minLength: 1
           });

            $("#<%=txtReturnType.ClientID%>").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.Code = escape(request.term);
                    obj.Type = 'ReturnType';
                    $.ajax({
                        url: '<%=ResolveUrl("frmPRReqReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[1] + '-' + item.split('/')[0],
                                    val: item.split('/')[0]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtReturnType]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtReturnType]").val(i.item.val);
                    return false;
                },
                minLength: 1
            });

            $("#<%=txtTrantype.ClientID%>").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.Code = escape(request.term);
                    obj.Type = 'Trantype';
                    $.ajax({
                        url: '<%=ResolveUrl("frmPRReqReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[1] + '-' + item.split('/')[0],
                                    val: item.split('/')[0]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtTrantype]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtTrantype]").val(i.item.val);
                    return false;
                },
                minLength: 1
            });
            if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'DatewiseSummary' || $("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'Unclaimed') {
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_divReturnNo').show();
                $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                $('#ContentPlaceHolder1_divReturnType').show();
                $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                $('#ContentPlaceHolder1_divTrantype').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                clearForm();
            } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PurchaseReturnStatus') {
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_divReturnNo').show();
                $('#ContentPlaceHolder1_divPRPaymentDetails').show();
                $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                $('#ContentPlaceHolder1_divReturnType').show();
                $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                $('#ContentPlaceHolder1_divTrantype').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();

                clearForm();
            } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'DatewiseDetail') {
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').show();
                 $('#ContentPlaceHolder1_divReturnNo').show();
                 $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                 $('#ContentPlaceHolder1_divReturnType').show();
                 $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                 $('#ContentPlaceHolder1_divTrantype').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').show();
                 $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                 clearForm();
             } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'Claimed') {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_divReturnNo').show();
                    $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                    $('#ContentPlaceHolder1_divTrantype').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
             }
             else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRSummary') {
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                 $('#ContentPlaceHolder1_divReturnNo').hide();
                 $('#ContentPlaceHolder1_divReturnNoDelete').show();
                 $('#ContentPlaceHolder1_divReturnType').show();
                 $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                 $('#ContentPlaceHolder1_divTrantype').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                 $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                 clearForm();
             }
        else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRRSummary') {
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_divReturnNo').hide();
            $('#ContentPlaceHolder1_divPRNumberDelete').show();
            $('#ContentPlaceHolder1_divReturnNoDelete').hide();
            $('#ContentPlaceHolder1_divReturnType').show();
            $('#ContentPlaceHolder1_divTrantype').hide();
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
            $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                 clearForm();
             }
             else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRDetail' ) {
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                 $('#ContentPlaceHolder1_divReturnNo').hide();
                 $('#ContentPlaceHolder1_divReturnNoDelete').show();
                 $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                 $('#ContentPlaceHolder1_divReturnType').show();
                 $('#ContentPlaceHolder1_divTrantype').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                 $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                 clearForm();
             }
             else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRRDetail') {
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                 $('#ContentPlaceHolder1_divReturnNo').hide();
                 $('#ContentPlaceHolder1_divPRNumberDelete').show();
                 $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                 $('#ContentPlaceHolder1_divReturnType').show();
                 $('#ContentPlaceHolder1_divTrantype').hide();
                 $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                 $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                 clearForm();
             }

            $('.ziehharmonika').ziehharmonika({
                highlander: true,
                collapsible: true,
                prefix: '★'//'♫'//,
                //collapseIcons: {
                //    opened: ' ',
                //    closed: ' '
                //}
            });
            SetDefaultDate($("#<%= txtFromDatePRR.ClientID %>"), $("#<%= txtToDatePRR.ClientID %>"));



    $('#<%=chkBydatePRR.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDatePRR]").removeAttr("disabled");
                    $("[id*=txtToDatePRR]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDatePRR]").attr("disabled", "disabled");
                    $("[id*=txtToDatePRR]").attr("disabled", "disabled");
                }
            });

        }

      <%--  $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            try {
                if (e.ctrlKey && e.keyCode == 65) {
                    $('#<%=hidValBilltype.ClientID%>').val('A');
                          alert('A');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 66) {
                        $('#<%=hidValBilltype.ClientID%>').val('B');
                      alert('B');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 67) {
                        $('#<%=hidValBilltype.ClientID%>').val('C');
                        alert('c');
                            e.preventDefault();
                            return;
                        }
            }
            catch (err) {
                ShowPopupMessageBox(err.message)
            }
        } //--%>
        $(function () {
            SetDefaultDate($("#<%= txtFromDatePRR.ClientID %>"), $("#<%= txtToDatePRR.ClientID %>"));
        });


        function SetDefaultDate(FromDate, Todate) {
            FromDate.datepicker('destroy');
            Todate.datepicker('destroy');
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }

        function SetManualDate(FromDate, Todate) {
            FromDate.datepicker('destroy');
            Todate.datepicker('destroy');
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }


        $(document).ready(function () {

            $("input:radio").change(function () {
                if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'DatewiseSummary' || $("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'Unclaimed') {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_divReturnNo').show();
                    $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divTrantype').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                    $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
                } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PurchaseReturnStatus') {
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_divReturnNo').show();
                $('#ContentPlaceHolder1_divPRPaymentDetails').show();
                $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                $('#ContentPlaceHolder1_divReturnType').show();
                $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                $('#ContentPlaceHolder1_divTrantype').hide();
                $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                clearForm();            
                } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'DatewiseDetail') {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_divReturnNo').show();
                    $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                    $('#ContentPlaceHolder1_divTrantype').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').show();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
                } else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'Claimed') {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_divReturnNo').hide();
                    $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divTrantype').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                    $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
                }
                else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRSummary' ) {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_divReturnNo').hide();
                    $('#ContentPlaceHolder1_divReturnNoDelete').show();
                    $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divTrantype').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();

                }
                else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRRSummary') {
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_divReturnNo').hide();
                    $('#ContentPlaceHolder1_divPRNumberDelete').show();
                    $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                    $('#ContentPlaceHolder1_divReturnType').show();
                    $('#ContentPlaceHolder1_divTrantype').hide();
                    $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                    $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    //clearForm();
                }
                 else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRDetail' ) {
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').show();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').show();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                     $('#ContentPlaceHolder1_divReturnNo').hide();
                     $('#ContentPlaceHolder1_divReturnNoDelete').show();
                     $('#ContentPlaceHolder1_divPRNumberDelete').hide();
                     $('#ContentPlaceHolder1_divReturnType').show();
                     $('#ContentPlaceHolder1_divTrantype').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                     $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
                }
                 else if ($("#<%=rdoPRRReport.ClientID%>").find(":checked").val() == 'PRRDetail') {
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divVendorDropDown').show();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divLocation').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divDepartmentDropDown').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divCategoryDropDown').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divSubCategoryDropDown').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divBrandDropDown').hide();
                     $('#ContentPlaceHolder1_divReturnNo').hide();
                     $('#ContentPlaceHolder1_divPRNumberDelete').show();
                     $('#ContentPlaceHolder1_divReturnNoDelete').hide();
                     $('#ContentPlaceHolder1_divReturnType').show();
                     $('#ContentPlaceHolder1_divTrantype').hide();
                     $('#ContentPlaceHolder1_PRRReportFilterUserControl_divAgent').hide();
                     $('#ContentPlaceHolder1_divPRPaymentDetails').hide();
                    clearForm();
                }
            });
        });

    function clearForm() {
        try {
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtLocation').val('');
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtVendor').val('');
            $('#ContentPlaceHolder1_txtReturnNo').val('');
            $('#ContentPlaceHolder1_txtReturnNoDelete').val('');
            $('#ContentPlaceHolder1_txtReturnType').val('');
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtDepartment').val('');
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtCategory').val('');
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtBrand').val('');
            $('#ContentPlaceHolder1_txtTrantype').val('');
            $('#ContentPlaceHolder1_PRRReportFilterUserControl_txtAgent').val('');
            $('#ContentPlaceHolder1_txtPRNumberDelete').val('');
            $('#ContentPlaceHolder1_txtReturnNoDelete').val('');
            
            $("select").val(0);
            $("select").trigger("liszt:updated");
        }
        catch (err) {
            alert(err.Message);
            return false;
        }
        return false;
    }


    function fncSetValue() { 
        try {
            if (SearchTableName == "Agent") { 
                    $('#<%=hidAgent.ClientID %>').val($.trim(Code));
                }

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page" id ="breadcrumbs_text">PURCHASE RETURN REQUEST REPORT</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <div style="overflow: visible; display: block;" runat="server" id="DivPRR">
                <h3>Purchase Return Request Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoPRRReport" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="DatewiseSummary">Date Wise Purchase Return Summary</asp:ListItem>
                                <asp:ListItem Value="DatewiseDetail">Date Wise Purchase Return Detail</asp:ListItem>                                 
                                <asp:ListItem Value="Unclaimed"> Purchase Return request - Unclaimed</asp:ListItem>
                                <asp:ListItem Value="Claimed"> Purchase Return request - Claimed</asp:ListItem>
                                  <asp:ListItem Value="PRSummary"> Purchase Return Delete Summary</asp:ListItem>
                                <asp:ListItem Value="PRDetail"> Purchase Return Delete Detail</asp:ListItem>
                                 <asp:ListItem Value="PRRSummary"> Purchase Return Req LOg Summary</asp:ListItem>
                                <asp:ListItem Value="PRRDetail"> Purchase Return Req LOg Detail</asp:ListItem>
                                 <asp:ListItem Value="PurchaseReturnStatus">Purchase Return Status Detail</asp:ListItem>
                                <%-- Vijay--%>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="PRRReportFilterUserControl"
                                EnablelocationDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableBrandDropDown="true"
                                EnableInventoryTextBox="false"
                                EnableClassDropDown="false"
                                EnableWarehouseDropDown="false"
                                EnableSubClassDropDown="false"
                                EnableFloorDropDown="false"
                                EnableCompanyDropDown="false"
                                EnableManufactureDropDown="false"
                                EnableSectionDropDown="false"
                                EnableSubCategoryDropDown="true"
                                EnableMerchandiseDropDown="false"
                                EnableVendorDropDown="true" 
                                EnableAgentTextBox ="true" />

                            <div class="col-md-12 form_bootstyle" runat="server" id="divReturnNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label19" runat="server" Text="Return No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtReturnNo"  MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'ReturnNo',  'txtReturnNo', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="divReturnNoDelete">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label3" runat="server" Text="Return No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtReturnNoDelete"  MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'ReturnNoDelete',  'txtReturnNoDelete', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="divPRNumberDelete">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label4" runat="server" Text="Return No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtPRNumberDelete"  MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'PRNumberDelete',  'txtPRNumberDelete', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-12 form_bootstyle" runat="server" id="divReturnType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label20" runat="server" Text="Return Type"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtReturnType" MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'ReturnType',  'txtReturnType', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divTrantype">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label22" runat="server" Text="Tran Type"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtTrantype" MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'TranType',  'txtTrantype', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="divPRPaymentDetails">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label5" runat="server" Text="Payment Status"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtPRPaymentDetails" MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'PRPaymentDetails',  'txtPRPaymentDetails', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDatePRR" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDatePRR" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-5 form_bootstyle">
                                        <asp:CheckBox ID="chkBydatePRR" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-2">
                                    </div>

                                    <asp:UpdatePanel ID="updpnlRefresh2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="col-md-5">
                                                <asp:LinkButton ID="lnkLoadPRRReport" runat="server" class="btn btn-danger" Font-Bold="true"
                                                    OnClick="lnkClick_lnkLoadPRRReport">Load Report </asp:LinkButton>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidValBilltype" runat="server" Value="A"/>
        <asp:HiddenField ID="hidAgent" runat="server" />
    </div>
</asp:Content>
