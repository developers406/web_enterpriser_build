﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmPurchaseReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmPurchaseReport" %>


<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <style type="text/css">
        .chzn-drop {
            width: 100% !important;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'PurchaseReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "PurchaseReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">




        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });

       <%-- $(document).ready(function () { //sankar  
        $("#<%=chKCheckDate.ClientID%>").click(function () { 
          if (this.checked) { 
               $("#<%= txtFromDateVendor.ClientID %>").prop('disabled', false);
               $("#<%= txtToDateVendor.ClientID %>").prop('disabled', false);                          
            }
       <%-- else {
                    $("#<%= txtFromDateVendor.ClientID %>").prop('disabled', true);
               $("#<%= txtToDateVendor.ClientID %>").prop('disabled', true);
                }                 --%>
      //     });
      //});--%>
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };


        function pageLoad() {
            visiblePaymentfilter();
            //$("[id*=POGroupBy]").show();
            //$("[id*=POGroupBy1]").show();
            //$("[id*=POGroupBy2]").hide();
        
            var checkedvalue = $("#<%=rdoListPurchaseOrder.ClientID%>").find(":checked").val();
            if (checkedvalue == 'List') {
                $("[id*=POGroupBy]").hide();
                $("[id*=POGroupBy1]").hide();
                $("[id*=POGroupBy2]").show();
            }
            else if (checkedvalue == 'Pending') {
                $("[id*=POGroupBy]").show();
                $("[id*=POGroupBy1]").show();
                $("[id*=POGroupBy2]").hide();
            }
            else if (checkedvalue == 'GADeleteLog') {
                $("[id*=POGroupBy]").show();
                $("[id*=POGroupBy1]").show();
                $("[id*=POGroupBy2]").hide();
            }
            else if (checkedvalue == 'Closed') {
                $("[id*=POGroupBy]").hide();
                $("[id*=POGroupBy1]").hide();
                $("[id*=POGroupBy2]").hide();
            }
            else if (checkedvalue == 'Cancelled') {
                $("[id*=POGroupBy]").hide();
                $("[id*=POGroupBy1]").show();
                $("[id*=POGroupBy2]").hide();
            }
            else if (checkedvalue == 'CancelledSummary' || checkedvalue == 'CancelledDetail') {
                $("[id*=POGroupBy]").hide();
                $("[id*=POGroupBy1]").show();
                $("[id*=POGroupBy2]").hide();
            }
            else if (checkedvalue == 'Userwise') {
                $("[id*=POGroupBy]").hide();
                $("[id*=POGroupBy1]").hide();
                $("[id*=POGroupBy2]").hide();
            }
            
            $('#<%= txtVendorPo.ClientID %>').val('');
            $('#<%= txtPoNo.ClientID %>').val('');
            $('#<%= txtUserPo.ClientID %>').val('');


            var checkvalue = $("#<%=rdoListPurchase.ClientID%>").find(":checked").val();
            if (checkvalue == 'Pending') {
                SetGroupGRN('H', 'S', 'H', 'S');
                SetGRNFilter("P");
            }
           else if (checkvalue == 'GADeleteLog') {
                SetGroupGRN('H', 'S', 'H', 'S');
                SetGRNFilter("P");
            }
            else if (checkvalue == 'DateWiseS') {
                SetGroupGRN('H', 'S', 'S');
                SetGRNFilter("DS");
            }
            else if (checkvalue == 'DateWiseD') {
                SetGroupGRN('H', 'S', 'S');
                SetGRNFilter("DD");
            }
            else if (checkvalue == 'Department') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("D");
            }
            else if (checkvalue == 'Category') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("C");
            }
            else if (checkvalue == 'SubCategory') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("C");
            }
            else if (checkvalue == 'Brand') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("B");
            }
            else if (checkvalue == 'Class') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("CL");
            }
            else if (checkvalue == 'Vendor') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("V");
            }
            else if (checkvalue == 'Discount') {
                SetGroupGRN('S', 'S', 'H');
                SetGRNFilter("DI");
            }
            else if (checkvalue == 'Inventory') {
                SetGroupGRN('H', 'S', 'H');
                SetGRNFilter("I");
            }
            else if (checkvalue == 'PurchaseReturnS') {
                SetGroupGRN('H', 'H', 'H');
                SetGRNFilter("PS");
            }
            else if (checkvalue == 'PurchaseReturnD') {
                SetGroupGRN('H', 'H', 'H');
                SetGRNFilter("PD");
            }
            else if (checkvalue == 'GRNPoComparison' ) {
                SetGroupGRN('H', 'H', 'H');
                SetGRNFilter("PD");
            }
            else if (checkvalue == 'Hold') {
                SetGroupGRN('H', 'S', 'S', 'H');
                SetGRNFilter("H");
            }
            else if (checkvalue == 'CancelledSummary' || checkvalue == 'CancelledDetail') {
                SetGroupGRN('H', 'S', 'H');
                SetGRNFilter("CN");
            }
            else if (checkvalue == 'GRNCount' || checkvalue == 'WBConv') {
                SetGroupGRN('H', 'S', 'H');
                SetGRNFilter("G");
            }
            else if (checkvalue == 'CostChange') {
                SetGroupGRN('H', 'S', 'H');
                SetGRNFilter("CC");
            }          
             
            // dinesh
            //if (checkvalue == 'Department' || checkvalue == 'Category' || checkvalue == 'Brand' || checkvalue == 'Vendor' || checkvalue == 'Inventory' || checkvalue == 'Class') {
            //$("[id*=GrpfilterPurchaseReport]").show();
            <%--$('#<%= lstCategory.ClientID %>').val('');
                $('#<%=lstCategory.ClientID %>').trigger("liszt:updated");
                $('#<%= lstDepartment.ClientID %>').val('');
                $('#<%=lstDepartment.ClientID %>').trigger("liszt:updated");
                $('#<%= lstBrand.ClientID %>').val('');
                $('#<%=lstBrand.ClientID %>').trigger("liszt:updated");
                $('#<%= lstVendor.ClientID %>').val('');
                $('#<%=lstVendor.ClientID %>').trigger("liszt:updated");--%>
            //}
            //else {
            //    //$("[id*=GrpfilterPurchaseReport]").hide();
            //}
            //
           <%-- $("[id*=txtItemCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmPurchaseReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    return false;
                },
                // minLength: 1
            });--%>
         <%--   $("[id*=txtGidNo]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmPurchaseReport.aspx/GetFilterValues") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtGidNo]").val(i.item.label);
                    return false;
                },
                 minLength: 0
            });--%>


            $('.ziehharmonika').ziehharmonika({
                highlander: true,
                collapsible: true,
                prefix: '★'//'♫'//,
                //collapseIcons: {
                //    opened: ' ',
                //    closed: ' '
                //}
            });

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'}); 
            debugger;
            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked');
            var BydatevaluePurchase = $("#<%=chkBydatePurchase.ClientID%>").is(':checked');
            var BydatevalueVendor = $("#<%=chkBydateVendor.ClientID%>").is(':checked');
            var BydatevaluePayment = $("#<%=chkBydatePayment.ClientID%>").is(':checked');              

            SetDateDisable(Bydatevalue, $("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"))
            //SetDateDisable(BydatevaluePurchase, $("#<%= txtFromDatePurchase.ClientID %>"), $("#<%= txtToDatePurchase.ClientID %>"))
            SetDateDisable(BydatevalueVendor, $("#<%= txtFromDateVendor.ClientID %>"), $("#<%= txtToDateVendor.ClientID %>"))
            SetDateDisable(BydatevaluePayment, $("#<%= txtFromDatePayment.ClientID %>"), $("#<%= txtToDatePayment.ClientID %>"))
           

            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePurchase.ClientID %>"), $("#<%= txtToDatePurchase.ClientID %>"));
            SetDefaultDateAllow($("#<%= txtFromDateVendor.ClientID %>"), $("#<%= txtToDateVendor.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePayment.ClientID %>"), $("#<%= txtToDatePayment.ClientID %>"));

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'D')
                }
            });

            $('#<%=chkBydatePayment.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDatePayment.ClientID %>"), $("#<%= txtToDatePayment.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDatePayment.ClientID %>"), $("#<%= txtToDatePayment.ClientID %>"), 'D')
                }
            });

            $('#<%=chkBydateVendor.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDateVendor.ClientID %>"), $("#<%= txtToDateVendor.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDateVendor.ClientID %>"), $("#<%= txtToDateVendor.ClientID %>"), 'D')
                }
            });
            
            $('#<%=chkBydatePurchase.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDatePurchase.ClientID %>"), $("#<%= txtToDatePurchase.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDatePurchase.ClientID %>"), $("#<%= txtToDatePurchase.ClientID %>"), 'D')
                }
            });

            if ($('#<%=hdnReportType.ClientID%>').val() == "C") { // dinesh
                $("[id$=POTopHead]").text('PURCHASE ORDER REPORT');
                $("[id$=GRNTopHead]").text('PURCHASE(GRN) REPORT');
                $("[id$=VendorTopHead]").text('VENDOR OUTSTANDING');
            }
            else if ($('#<%=hdnReportType.ClientID%>').val() == "B") {
                $("[id$=POTopHead]").text('purchase order report');
                $("[id$=GRNTopHead]").text('purchase(GRN) report');
                $("[id$=VendorTopHead]").text('vendor outstanding');
            }
            else {
                $("[id$=POTopHead]").text('Purchase Order Report');
                $("[id$=GRNTopHead]").text('Purchase(GRN) Report');
                $("[id$=VendorTopHead]").text('Vendor OutStanding');
            }

            $(document).ready(function () {
                $(document).on('keydown', disableFunctionKeys);
            });
            function disableFunctionKeys(e) {
                try {
                    if (e.ctrlKey && e.keyCode == 65) {
                        $('#<%=hdnReportType.ClientID%>').val('');
                        $("[id$=POTopHead]").text('Purchase Order Report');
                        $("[id$=GRNTopHead]").text('Purchase(GRN) Report');
                        $("[id$=VendorTopHead]").text('Vendor OutStanding');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 66) {
                        $('#<%=hdnReportType.ClientID%>').val('B');
                        $("[id$=POTopHead]").text('purchase order report');
                        $("[id$=GRNTopHead]").text('purchase(GRN) report');
                        $("[id$=VendorTopHead]").text('vendor outstanding');
                        e.preventDefault();
                        return;
                    }
                    else if (e.ctrlKey && e.keyCode == 67) {
                        $('#<%=hdnReportType.ClientID%>').val('C');
                        $("[id$=POTopHead]").text('PURCHASE ORDER REPORT');
                        $("[id$=GRNTopHead]").text('PURCHASE(GRN) REPORT');
                        $("[id$=VendorTopHead]").text('VENDOR OUTSTANDING');
                        e.preventDefault();
                        return;
                    }
                }
                catch (err) {
                    ShowPopupMessageBox(err.message)
                }
            } //


          
               // alert('test');
                $("[id*=divVendorPay]").show();
                $("[id*=divPaymentNo]").hide();
                $("[id*=divlocationFilter]").show();
                $("[id*=divPayType]").hide();
                $("[id*=divDebitno]").hide();
            
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Vendor') {
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").show();
                    $("[id*=divlocationFilter]").hide();
                    $("[id*=divPayType]").show(); 
                    $("[id*=divDebitno]").hide();
                    $("[id*=divVendorPayFilter").show();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Transactions') {
                $("[id*=divVendorPay]").show();
                $("[id*=divPaymentNo]").hide();
                $("[id*=divlocationFilter]").show();
                $("[id*=divPayType]").hide();
                $("[id*=divDebitno]").hide();
                $("[id*=chByDate]").show(); 
                $("[id*=chBasedon]").hide();
                $("[id*=divVendorPayFilter").hide();
                $("[id*=divReasonCode]").hide();
                $("[id*=divPRPaymentDetails").hide();
                $("[id*=chChkDate]").hide();
                }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Outstanding') {
                $("[id*=divVendorPay]").show();
                $("[id*=divPaymentNo]").hide();
                $("[id*=divlocationFilter]").show();
                $("[id*=divPayType]").hide();
                $("[id*=divDebitno]").hide();
                $("[id*=chByDate]").hide();
                $("[id*=chBasedon]").show();
                $("[id*=divVendorPayFilter").hide();
                $("#<%=chCreditDays.ClientID%>").prop("checked", false);
                $('#ContentPlaceHolder1_txtFromDateVendor').attr("disabled", "disabled");
                $('#ContentPlaceHolder1_txtToDateVendor').attr("disabled", "disabled");
                $("[id*=divReasonCode]").hide();
                $("[id*=divPRPaymentDetails").hide();
                $("[id*=chChkDate]").hide();
            }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Cash') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                     $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'UnIssued') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").show();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Issued') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").show();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Credit') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divReasonCode]").show(); 
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divPRPaymentDetails").show();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Debit') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divReasonCode]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divPRPaymentDetails").show();
                    $("[id*=chChkDate]").hide();
                }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DeletedDetail'|| $("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DeletedSummary') {
                    // alert('test');
                $("[id*=divVendorPay]").show();
                $("[id*=divReasonCode]").hide();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divPRPaymentDetails").hide();
                $("[id*=chChkDate]").hide();
            }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DebitCreditLog') {
                    // alert('test');
                $("[id*=divVendorPay]").show();
                $("[id*=divReasonCode]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divPRPaymentDetails").hide();
                $("[id*=chChkDate]").hide();
                }
            if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'OutstandingView')
            {
            $("[id*=divVendorPay]").show();
                $("[id*=divPaymentNo]").hide();
                $("[id*=divlocationFilter]").show();
                $("[id*=divPayType]").hide();
                $("[id*=divDebitno]").hide();
                $("[id*=chByDate]").hide();
                $("[id*=chBasedon]").show();
                $("[id*=divVendorPayFilter").show();
                $("#<%=chCreditDays.ClientID%>").prop("checked", false);
                $('#ContentPlaceHolder1_txtFromDateVendor').attr("enable", "enable");
                $('#ContentPlaceHolder1_txtToDateVendor').attr("enable", "enable");
                $("[id*=divReasonCode]").hide();
                $("[id*=divPRPaymentDetails").hide();
                $("[id*=chChkDate]").hide();
            }
        }
        if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'VendorOutstandingDateWise') {
            $("[id*=divVendorPay]").show();
            $("[id*=divReasonCode]").hide();
            $("[id*=divPaymentNo]").hide();
            $("[id*=divlocationFilter]").hide();
            $("[id*=divPayType]").hide();
            $("[id*=divDebitno]").hide();
            $("[id*=chByDate]").hide();
            $("[id*=chBasedon]").hide();
            $("[id*=divVendorPayFilter").hide();
            $("[id*=divPRPaymentDetails").hide();
            $("[id*=chChkDate]").hide();
            //FromDate.attr("disabled", "disabled");
            //Todate.attr("disabled", "disabled");
            $('#ContentPlaceHolder1_txtFromDateVendor').attr("disable", "disable");
            $('#ContentPlaceHolder1_txtToDateVendor').attr("disable", "disable");
            //$('#Label7').hide(); 
        }
    

        function fncVendorFilterClear() {
            try {
                $('#ContentPlaceHolder1_txtPRPaymentDetails').val('');
                $('#ContentPlaceHolder1_txtReasonCode').val('');
                $('#ContentPlaceHolder1_txtrptVendorDropBack').val('');
                //$('#ContentPlaceHolder1_rptVendorDropBack').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtDropDownLocation').val('');
                //$('#ContentPlaceHolder1_DropDownLocation').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtPaymentNo').val('');
                //$('#ContentPlaceHolder1_DropDownPaymentNo').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtDropPaymentType').val('');
                //$('#ContentPlaceHolder1_DropDownPaymentType').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtDebitNoteno').val('');
                //$('#ContentPlaceHolder1_DropDowndDebitNoteno').trigger("liszt:updated");
              <%--  $("#<%=txtFromDateVendor.ClientID%>").val('');.datepicker();
                $("#<%=txtToDateVendor.ClientID%>").val('');--%>
                $('#ContentPlaceHolder1_txtFromDateVendor').datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");;
                $('#ContentPlaceHolder1_txtToDateVendor').datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");;
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
        $(document).ready(function () {
           $('#<%=chCreditDays.ClientID%>').change(function () {
                if (this.checked) {
                    $('#ContentPlaceHolder1_txtFromDateVendor').removeAttr("disabled");
                    $("[id*=rdoSummery]").attr("disabled", "disabled");
                    $("[id*=rdoDetail]").attr("disabled", "disabled");
                } else {
                    $('#ContentPlaceHolder1_txtFromDateVendor').attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_txtToDateVendor').attr("disabled", "disabled");
                    $("[id*=rdoSummery]").removeAttr("disabled");
                    $("[id*=rdoDetail]").removeAttr("disabled");
                }
                
            });
        });
        $(document).ready(function () {
            $('#<%=rdoVendorOutStand.ClientID %> input').click(function () {
                fncVendorFilterClear();
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Outstanding') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").hide();
                    $("[id*=chBasedon]").show();
                    $("[id*=divVendorPayFilter").hide();
                    $('#ContentPlaceHolder1_txtFromDateVendor').attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_txtToDateVendor').attr("disabled", "disabled");
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();

                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Transactions') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Vendor') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").show();
                    $("[id*=divlocationFilter]").hide();
                    $("[id*=divPayType]").show();
                    $("[id*=divDebitno]").hide();
                    $("[id*=divVendorPayFilter").show();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                  
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Cash') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'UnIssued') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").show();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Issued') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").show();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Credit') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").show();
                    $("[id*=divPRPaymentDetails").show();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'Debit') {
                    // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").show();
                    $("[id*=divPRPaymentDetails").show();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DeletedDetail'|| $("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DeletedSummary') {
                 // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'DebitCreditLog') {
                 // alert('test');
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").show();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").show();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").show();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'OutstandingView')
                {
                $("[id*=divVendorPay]").show();
                $("[id*=divPaymentNo]").hide();
                $("[id*=divlocationFilter]").show();
                $("[id*=divPayType]").hide();
                $("[id*=divDebitno]").hide();
                $("[id*=chByDate]").hide();
                $("[id*=chBasedon]").show();
                $("[id*=divVendorPayFilter").show();
                //$("#<%=chCreditDays.ClientID%>").prop("checked", true);
                    $('#ContentPlaceHolder1_txtFromDateVendor').attr("enable", "enable");
                    $('#ContentPlaceHolder1_txtToDateVendor').attr("enable", "enable");
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                }
                if ($("#<%=rdoVendorOutStand.ClientID%>").find(":checked").val() == 'VendorOutstandingDateWise') {
                    $("[id*=divVendorPay]").show();
                    $("[id*=divPaymentNo]").hide();
                    $("[id*=divlocationFilter]").hide();
                    $("[id*=divPayType]").hide();
                    $("[id*=divDebitno]").hide();
                    $("[id*=chByDate]").hide();
                    $("[id*=chBasedon]").hide();
                    $("[id*=divVendorPayFilter").hide();
                    $("[id*=divReasonCode]").hide();
                    $("[id*=divPRPaymentDetails").hide();
                    $("[id*=chChkDate]").hide();
                    //FromDate.attr("disabled", "disabled");
                    //Todate.attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_txtFromDateVendor').attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_txtToDateVendor').attr("disabled", "disabled");
                    //$('#Label7').hide();
                }
            });
        });
        //var check = true;
        function SetDateDisable(check, FromDate, Todate) {
            if (!check) {
                FromDate.attr("disabled", "disabled");
                Todate.attr("disabled", "disabled");
            }
        }

        function visiblePaymentfilter() { // dinesh
            $("#ContentPlaceHolder1_PaymentReportFilterUserControl_txtLocation").css("width", "100%");
            $("#ContentPlaceHolder1_PaymentReportFilterUserControl_txtVendor").css("width", "100%");
            $("#ContentPlaceHolder1_PaymentReportFilterUserControl_txtTerminal").css("width", "100%");
            $("#ContentPlaceHolder1_txtPaymentType").css("width", "100%");
            $("#ContentPlaceHolder1_txtPaymentSubType").css("width", "100%");
            $("[id*=divLocation]").show();

            //$("#chzn-drop").css("width", "100%");
            //$("#ContentPlaceHolder1_ddPaymentSubType_chzn").css("width", "100%");
        }

        $(document).ready(function () {

            $('#<%= rdoListPurchaseOrder.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoListPurchaseOrder.ClientID %> input:radio:checked").val();
                if (checkvalue == 'List') {
                    $("[id*=POGroupBy]").hide();
                    $("[id*=POGroupBy1]").hide();
                    $("[id*=POGroupBy2]").show();
                }
                else if (checkvalue == 'Pending') {
                    $("[id*=POGroupBy]").show();
                    $("[id*=POGroupBy1]").show();
                    $("[id*=POGroupBy2]").hide();
                }
                else if (checkvalue == 'GADeleteLog') {
                    $("[id*=POGroupBy]").show();
                    $("[id*=POGroupBy1]").show();
                    $("[id*=POGroupBy2]").hide();
                }
                else if (checkvalue == 'Closed') {
                    $("[id*=POGroupBy]").hide();
                    $("[id*=POGroupBy1]").hide();
                    $("[id*=POGroupBy2]").hide();
                }
                else if (checkvalue == 'Cancelled') {
                    $("[id*=POGroupBy]").hide();
                    $("[id*=POGroupBy1]").show();
                    $("[id*=POGroupBy2]").hide();
                }
                else if (checkvalue == 'CancelledSummary' || checkvalue == 'CancelledDetail') {
                    $("[id*=POGroupBy]").hide();
                    $("[id*=POGroupBy1]").show();
                    $("[id*=POGroupBy2]").hide();
                }
                else if (checkvalue == 'Userwise') {
                    $("[id*=POGroupBy]").hide();
                    $("[id*=POGroupBy1]").hide();
                    $("[id*=POGroupBy2]").hide();
                }

                
            $('#<%= txtVendorPo.ClientID %>').val('');
            $('#<%= txtPoNo.ClientID %>').val('');
            $('#<%= txtUserPo.ClientID %>').val('');

            });


            $('#<%= rdoListPurchase.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoListPurchase.ClientID %> input:radio:checked").val();
                if (checkvalue == 'Pending') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("P");
                }
                else if (checkvalue == 'GADeleteLog') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("P");
                }
                else if (checkvalue == 'DateWiseS') {
                    SetGroupGRN('H', 'S', 'S');
                    SetGRNFilter("DS");
                }
                else if (checkvalue == 'DateWiseD') {
                    SetGroupGRN('H', 'S', 'S');
                    SetGRNFilter("DD");
                }
                else if (checkvalue == 'Department') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("D");
                }
                else if (checkvalue == 'Category') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("C");
                }
                else if (checkvalue == 'SubCategory') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("C");
                }
                else if (checkvalue == 'Brand') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("B");
                }
                else if (checkvalue == 'Class') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("CL");
                }
                else if (checkvalue == 'Vendor') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("V");
                }
                else if (checkvalue == 'Discount') {
                    SetGroupGRN('S', 'S', 'H');
                    SetGRNFilter("DI");
                }
                else if (checkvalue == 'Inventory') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("I");
                }
                else if (checkvalue == 'PurchaseReturnS') {
                    SetGroupGRN('H', 'H', 'H');
                    SetGRNFilter("PS");
                }
                else if (checkvalue == 'PurchaseReturnD') {
                    SetGroupGRN('H', 'H', 'H');
                    SetGRNFilter("PD");
                }
                else if (checkvalue == 'GRNPoComparison') {
                    SetGroupGRN('H', 'H', 'H');
                    SetGRNFilter("PD");
                }
                else if (checkvalue == 'Hold') {
                    SetGroupGRN('H', 'S', 'S', 'H');
                    SetGRNFilter("H");
                }
                else if (checkvalue == 'CancelledSummary' || checkvalue == 'CancelledDetail') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("CN");
                }
                else if (checkvalue == 'GRNCount' || checkvalue == 'WBConv') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("G");
                }
                else if (checkvalue == 'CostChange') {
                    SetGroupGRN('H', 'S', 'H');
                    SetGRNFilter("CC");
                }
          
              
                ClearGRNFilter();
            });

            $('#<%= rdoListPaymentReciept.ClientID %> input').click(function () {
                //alert();
                $('#ContentPlaceHolder1_PaymentReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlLocation').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlLocation').val('');

                $('#ContentPlaceHolder1_PaymentReportFilterUserControl_txtVendor').val('');
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlVendor').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlVendor').val('');

                $('#ContentPlaceHolder1_PaymentReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlTerminal').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_PaymentReportFilterUserControl_ddlTerminal').val('');

                $('#ContentPlaceHolder1_txtPaymentType').val('');
                //$('#ContentPlaceHolder1_ddPaymentType').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_ddPaymentType').val('');

                $('#ContentPlaceHolder1_txtPaymentSubType').val('');
                //$('#ContentPlaceHolder1_ddPaymentSubType').trigger("liszt:updated");
                //$('#ContentPlaceHolder1_ddPaymentSubType').val('');
            });

        });

        function ClearGRNFilter() {
            $('#<%= txtLocation.ClientID %>').val('');
            //$('#ContentPlaceHolder1_ddLocation').val('');
            //$('#ContentPlaceHolder1_ddLocation').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddLocation').val('');

            //$('#ContentPlaceHolder1_ddVendor').val('');
            //$('#ContentPlaceHolder1_ddVendor').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddVendor').val('');
            $('#<%= txtVendor.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddDepartment').val('');
            //$('#ContentPlaceHolder1_ddDepartment').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddDepartment').val('');
            $('#<%= txtDepartment.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddBrand').val('');
            //$('#ContentPlaceHolder1_ddBrand').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddBrand').val('');
            $('#<%= txtBrand.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddCategory').val('');
            //$('#ContentPlaceHolder1_ddCategory').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddCategory').val('');
            $('#<%= txtCategory.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddSubCategory').val('');
            //$('#ContentPlaceHolder1_ddSubCategory').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddSubCategory').val('');
            $('#<%= txtSubCategory.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddClass').val('');
            //$('#ContentPlaceHolder1_ddClass').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddClass').val('');
            $('#<%= txtClass.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddSubClass').val('');
            //$('#ContentPlaceHolder1_ddSubClass').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddSubClass').val('');
            $('#<%= txtSubClass.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddFloor').val('');
            //$('#ContentPlaceHolder1_ddFloor').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddFloor').val('');
            $('#<%= txtFloor.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddUser').val('');
            //$('#ContentPlaceHolder1_ddUser').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddUser').val('');
            $('#<%= txtUser.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddManufacture').val('');
            //$('#ContentPlaceHolder1_ddManufacture').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddManufacture').val('');
            $('#<%= txtManufacture.ClientID %>').val('');

            //$('#ContentPlaceHolder1_ddTax').val('');
            //$('#ContentPlaceHolder1_ddTax').trigger("liszt:updated");
            //$('#ContentPlaceHolder1_ddTax').val('');
            $('#<%= txtTax.ClientID %>').val('');

            $('#<%= txtItemCode.ClientID %>').val('');
            $('#<%= txtGidNo.ClientID %>').val('');


        }
        function SetGRNFilter(Filter) {
            if (Filter == 'P') { // GA Pending Wise
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide();
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").hide();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'DS' || Filter == 'DD' || Filter == 'PS' || Filter == 'PD') { // Date Summary and Detail Wise
                // Tax and GID No Extra add
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide();
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                if (Filter == 'PS' || Filter == 'PD')
                    $("[id*=divTax]").hide();
                else
                    $("[id*=divTax]").show();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'D' || Filter == 'B' || Filter == 'C' || Filter == 'SC') { // Department,Category,Brand Wise,Sub Cat.,Vendor
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").show();
                $("[id*=divCategory]").show();
                $("[id*=divBrand]").show(); // Tax and GID No,Sub Catagory,Class,SubClass, Manufac.,Floor Extra add
                $("[id*=divSubCategory]").show();
                $("[id*=divClass]").show();
                $("[id*=divSubClass]").show();
                $("[id*=divManufacture]").show();
                $("[id*=divFloor]").show();
                $("[id*=divTax]").show();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'V') { // Department,Category,Brand Wise,Sub Cat.,Vendor
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").show();
                $("[id*=divCategory]").show();
                $("[id*=divBrand]").show();
                $("[id*=divSubCategory]").show();
                $("[id*=divClass]").show();
                $("[id*=divSubClass]").show();
                $("[id*=divManufacture]").show();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").show();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'CN' || Filter == 'G') { // User
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide();
                $("[id*=divSubCategory]").hide(); // GIDNo ,USER
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").show();
            }
            else if (Filter == 'CL') { // Class Wise
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide(); // GID No,Class,SubClass,Floor Extra add
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").show();
                $("[id*=divSubClass]").show();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").show();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'I') { // Inventory Wise
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide(); // GID No,Inventory,Floor Extra add
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").show();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").show();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'H') { // Hide Wise
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide();
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").hide();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'DI') { // Vendor Wise Scheme Discount
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").show();
                $("[id*=divDepartment]").show();
                $("[id*=divCategory]").show();
                $("[id*=divBrand]").show(); // GIDNo,Tax,Manufac.
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").show();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").show();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").show();
                $("[id*=divUser]").hide();
            }
            else if (Filter == 'CC') { // Vendor Wise Scheme Discount
                $("[id*=GrpfilterPurchaseReport]").show();
                $("[id*=divLocationGRN]").show();
                $("[id*=divVendor]").hide();
                $("[id*=divDepartment]").show();
                $("[id*=divCategory]").show();
                $("[id*=divBrand]").show(); // GIDNo,Tax,Manufac.
                $("[id*=divSubCategory]").show();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").show();
                $("[id*=divGIDNo]").hide();
                $("[id*=divUser]").hide();
            }
            else {
                $("[id*=GrpfilterPurchaseReport]").hide();
                $("[id*=divLocationGRN]").hide();
                $("[id*=divVendor]").hide();
                $("[id*=divDepartment]").hide();
                $("[id*=divCategory]").hide();
                $("[id*=divBrand]").hide();
                $("[id*=divSubCategory]").hide();
                $("[id*=divClass]").hide();
                $("[id*=divSubClass]").hide();
                $("[id*=divManufacture]").hide();
                $("[id*=divFloor]").hide();
                $("[id*=divTax]").hide();
                $("[id*=divItemcode]").hide();
                $("[id*=divGIDNo]").hide();
                $("[id*=divUser]").hide();
            }
        }

        function SetGroupGRN(vGroup, vGroup1, ChkinvDate, chkGrnDate) {

            //alert(vGroup);
            if (vGroup == "S")
                $("[id*=GRNGroup]").show();
            else
                $("[id*=GRNGroup]").hide();

            if (vGroup1 == "S")
                $("[id*=GRNGroup1]").show();
            else
                $("[id*=GRNGroup1]").hide();

            if (ChkinvDate == "H") {
                $("[id*=chkInvDate]").attr("disabled", "disabled");
                $("[id*=chkInvDate]").attr("checked", false);
                $("[id*=chkGrnDate]").attr("checked", true);
                //document.getElementById("<%=chkInvDate.ClientID%>").style.display = 'none'; 
            }
            else {
                $("[id*=chkInvDate]").removeAttr("disabled");
                $("[id*=chkInvDate]").attr("checked", true);
                $("[id*=chkGrnDate]").attr("checked", false);
                //document.getElementById("<%=chkInvDate.ClientID%>").style.display = 'block';                
            }
            //aalayam
            if (chkGrnDate == "H") {
                $("[id*=chkGrnDate]").attr("disabled", "disabled");
                $("[id*=chkGrnDate]").attr("checked", false);
                $("[id*=chkInvDate]").attr("checked", true);
            }
            else {
                $("[id*=chkGrnDate]").removeAttr("disabled");
                $("[id*=chkGrnDate]").attr("checked", true);
                $("[id*=chkInvDate]").attr("checked", false);
            }
            //
        }

        $(function () {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePurchase.ClientID %>"), $("#<%= txtToDatePurchase.ClientID %>"));
            SetDefaultDateAllow($("#<%= txtFromDateVendor.ClientID %>"), $("#<%= txtToDateVendor.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePayment.ClientID %>"), $("#<%= txtToDatePayment.ClientID %>"));
        });


        function SetDisable(FromDate, Todate, sflag) {
            if (sflag == "D") {
                FromDate.attr("disabled", "disabled");
                Todate.attr("disabled", "disabled");
            }
            else {
                FromDate.removeAttr("disabled");
                Todate.removeAttr("disabled");
            }
        }
        function SetDefaultDate(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }           
        }

         function SetDefaultDateAllow(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true,  });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true,  }).datepicker("setDate", "0");
            }           
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Vendor") {
                    $('#<%=hidVendorName.ClientID %>').val($.trim(Code));
                    $('#<%=txtVendorPo.ClientID %>').val($.trim(Description));
                }
                
             }
            catch (err)
            {
                ShowPopupMessageBox(err.message);
            }
        }
      
 
        
 

    </script>
    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        /*h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }*/

        input:checked + label {
            color: white;
            background: red;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Purchase Reports</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika EnableScroll" >

            <div style="overflow: visible; display: block;" runat="server" id="DivPurchaseOrder">
                <h3 id="POTopHead">Purchase Order Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <asp:RadioButtonList ID="rdoListPurchaseOrder" runat="server" Class="radioboxlist">
                            <asp:ListItem Selected="True" Value="List"> PO List</asp:ListItem>
                            <asp:ListItem Value="Pending">PO Pending List</asp:ListItem>
                            <asp:ListItem Value="Closed">PO Closed List</asp:ListItem>
                            <asp:ListItem Value="Cancelled">PO Cancelled List</asp:ListItem>
                            <asp:ListItem Value="Userwise">User Wise PO Count</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>


                    <div class="col-md-4 form_bootstyle_border" id="divPoFilteraton">
                        <div class="barcode-header">
                            Filterations
                        </div>
                        <asp:Panel ID="Panel1" runat="server">
                            <div class="control-group" id="divVendorPO">
                                <div style="float: left">
                                    <asp:Label ID="Label9" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="rptVendorDropBack" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtVendorPo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendorPo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divPONoPO">
                                <div style="float: left">
                                    <asp:Label ID="Label10" runat="server" Text="PO No"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="DropDownPaymentNo" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtPoNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PoNo', 'txtPoNo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divUserPo">
                                <div style="float: left">
                                    <asp:Label ID="Label13" runat="server" Text="User"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtUserPo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'User', 'txtUserPo', '');"></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="col-md-5 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="POGroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="chkPending" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="COMPLETE PENDING" Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="chkPartial" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="PARTIAL PENDING" />
                            </div>
                        </div>
                        <div id="POGroupBy1" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="chkSummary" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="SUMMARY" Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="chkDetail" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                            </div>
                        </div>
                        <div id="POGroupBy2" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkApproved" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu2" Text="APPROVED" Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkUnApproved" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu2" Text="UNAPPROVED" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="RadioButton1" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu2" Text="BOTH" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-2">
                                <%--<asp:LinkButton ID="lnkPOClearSearch" runat="server" class="btn btn-primary" Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                <asp:LinkButton ID="lnkPOReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkPOReport_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivPurchase">
                <h3 id="GRNTopHead">Purchase(GRN) Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <asp:RadioButtonList ID="rdoListPurchase" runat="server" Class="radioboxlist">
                            <asp:ListItem Selected="True" Value="Pending"> GA Pending List</asp:ListItem>
                            <asp:ListItem Value="DateWiseS">Date Wise Purchase Summary</asp:ListItem>
                            <asp:ListItem Value="DateWiseD">Date Wise Purchase Detail</asp:ListItem>
                            <asp:ListItem Value="Department">Department Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="Category">Category Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="SubCategory">Sub Category Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="Brand">Brand Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="Class">Class Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="Vendor">Vendor Wise Purchase</asp:ListItem>
                            <asp:ListItem Value="Discount">Vendor Wise Scheme discount</asp:ListItem>
                            <asp:ListItem Value="Inventory">Inventory Wise GRN</asp:ListItem>
                            <asp:ListItem Value="Hold"> GRN Hold (pending ) List</asp:ListItem>
                            <asp:ListItem Value="CancelledSummary">Cancelled GRN List Summary</asp:ListItem>
                            <asp:ListItem Value="CancelledDetail">Cancelled GRN List Detail</asp:ListItem>
                            <asp:ListItem Value="GRNCount"> User Wise GRN Count</asp:ListItem>
                            <asp:ListItem Value="PurchaseReturnS">Date Wise Purchase Return Summary</asp:ListItem>
                            <asp:ListItem Value="PurchaseReturnD">Date Wise Purchase Return Detail</asp:ListItem>
                            <asp:ListItem Value="CostChange">Cost Change</asp:ListItem>
                            <asp:ListItem Value="GRNPoComparison">GRN Po Comparison</asp:ListItem>
                            <asp:ListItem Value="GADeleteLog"> GA Delete Log</asp:ListItem>
                            <asp:ListItem Value="WBConv">Weight Based Conversion</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <%--dinesh--%>
                    <div id="GrpfilterPurchaseReport" runat="server" class="col-md-4 form_bootstyle_border" style="margin-right: 50px;">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12 form_bootstyle" runat="server" id="divItemcode">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblItemCode" runat="server" Text="Item Code"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtItemCode" MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divItemcode">
                                <div class="label-left">
                                    <asp:Label ID="lblItemCode" runat="server" Text="Item Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemCode" MaxLength="15" runat="server" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>--%>
                            <%--<div class="control-group-single-res" runat="server" id="divGIDNo">
                                <div class="label-left">
                                    <asp:Label ID="lblGidNo" runat="server" Text="GID No"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtGidNo" MaxLength="50" runat="server" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divGIDNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblGidNo" runat="server" Text="GID No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtGidNo" MaxLength="50" onkeydown="return fncShowSearchDialogCommon(event, 'GidNo',  'txtGidNo', '');" runat="server" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divLocationGRN">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddLocation" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divLocationGRN">
                                <div class="label-left">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstLocation" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divDepartment">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddDepartment" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divDepartment">
                                <div class="label-left">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstDepartment" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divCategory">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddCategory" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divCategory">
                                <div class="label-left">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstCategory" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divSubCategory">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblSubCategory" runat="server" Text="Sub Category"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddSubCategory" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divSubCategory">
                                <div class="label-left">
                                    <asp:Label ID="lblSubCategory" runat="server" Text="Sub Category"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="txtSubCategory" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBrand">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBrand" runat="server" Text="Brand"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddBrand" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divBrand">
                                <div class="label-left">
                                    <asp:Label ID="lblBrand" runat="server" Text="Brand"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstBrand" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divVendor">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblVendor" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divVendor">
                                <div class="label-left">
                                    <asp:Label ID="lblVendor" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstVendor" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>  --%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divClass">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddClass" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divClass">
                                <div class="label-left">
                                    <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="txtClass" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divSubClass">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblSubClass" runat="server" Text="Sub Class"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddSubClass" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'txtSubClass', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divSubClass">
                                <div class="label-left">
                                    <asp:Label ID="lblSubClass" runat="server" Text="Sub Class"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstSubClass" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divTax">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblTax" runat="server" Text="Tax"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddTax" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtTax" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'TaxMaster', 'txtTax', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%-- <div class="control-group-single-res" runat="server" id="divTax">
                                <div class="label-left">
                                    <asp:Label ID="lblTax" runat="server" Text="Tax"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="txtTax" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divManufacture">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblManufacture" runat="server" Text="Manufacture"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%-- <asp:DropDownList ID="ddManufacture" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manufacture', 'txtManufacture', '');"></asp:TextBox>
                                </div>
                            </div>
                            <%--<div class="control-group-single-res" runat="server" id="divManufacture">
                                <div class="label-left">
                                    <asp:Label ID="lblManufacture" runat="server" Text="Manufacture"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="txtManufacture" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>--%>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divFloor">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblFloor" runat="server" Text="Floor"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddFloor" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divUser">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblUser" runat="server" Text="User"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddUser" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtUser" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'User', 'txtUser', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- dinesh--%>
                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label6" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDatePurchase" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label7" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDatePurchase" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GRNGroup" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoGroupDate" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="DATE" Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoGroupMonth" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="MONTH" />
                            </div>
                        </div>
                        <div id="GRNGroup1" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkInvDate" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="INVOICE DATE" Checked="true" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkGrnDate" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="GRN DATE" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydatePurchase" Visible="false" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%-- <asp:LinkButton ID="lnkClearPurchase" runat="server" class="btn btn-primary"
                                    Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkPurchaseReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkPurchaseReport_Click">Load Report </asp:LinkButton>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivVendorOutstands">
                <h3 id="VendorTopHead">Vendor OutStanding</h3>
                <div class="row">
                    <div class="col-md-4">
                        <div>
                            <asp:RadioButtonList ID="rdoVendorOutStand" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Transactions">Vendor Transactions</asp:ListItem>
                                <asp:ListItem Value="Outstanding" style="display: none !important">Vendor Outstanding</asp:ListItem>
                                <asp:ListItem Value="OutstandingView">Vendor Outstanding</asp:ListItem>
                                <asp:ListItem Value="Vendor">Vendor Payments</asp:ListItem>
                                <asp:ListItem Value="Cash">Cash Payments</asp:ListItem>
                                <asp:ListItem Value="UnIssued">Cheque Payment-Prepared (UnIssued)</asp:ListItem>
                                <asp:ListItem Value="Issued">Cheque Payment-Delivered (Issued)</asp:ListItem>
                                <asp:ListItem Value="Credit">Credit Note List</asp:ListItem>
                                <asp:ListItem Value="Debit">Debit Note List</asp:ListItem>
                                <asp:ListItem Value="DeletedDetail">Deleted Payments-Detail</asp:ListItem>
                                <asp:ListItem Value="DeletedSummary">Deleted Payments-Summary</asp:ListItem>
                                <asp:ListItem Value="DebitCreditLog">Debit/Credit Delete-Log</asp:ListItem>
                                <asp:ListItem Value="VendorOutstandingDateWise">VendorOutstandingDateWise</asp:ListItem>

                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4" id="divfilter" runat="server">
                        <div class="barcode-header">
                            Filteration
                        </div>
                        <asp:Panel ID="PanelFilter" runat="server">
                            <div class="control-group" id="divVendorPay">
                                <div style="float: left">
                                    <asp:Label ID="Label11" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="rptVendorDropBack" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtrptVendorDropBack" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtrptVendorDropBack', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divPaymentNo">
                                <div style="float: left">
                                    <asp:Label ID="Label29" runat="server" Text="Payment No"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="DropDownPaymentNo" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtPaymentNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PaymentNo', 'txtPaymentNo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divlocationFilter">
                                <div style="float: left">
                                    <asp:Label ID="Label51" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <%--<asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control">
                                        </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtDropDownLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtDropDownLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="margin-bottom: 20px" id="divPayType">
                                <div style="float: left">
                                    <asp:Label ID="Label30" runat="server" Text="Payment Type"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtDropPaymentType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'PaymentType', 'txtDropPaymentType', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: -20px" id="divDebitno">
                                <div style="float: left">
                                    <asp:Label ID="Label12" runat="server" Text="Debit NoteNo"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtDebitNoteno" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'DebitNoteNo', 'txtDebitNoteno', '');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group" id="divReasonCode">
                                <div style="float: left">
                                    <asp:Label ID="Label15" runat="server" Text="Reason Code"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtReasonCode" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'ReturnType', 'txtReasonCode', '');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group" id="divPRPaymentDetails">
                                <div style="float: left">
                                    <asp:Label ID="Label14" runat="server" Text="Payment Status"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtPRPaymentDetails" MaxLength="15" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'PRPaymentDetails',  'txtPRPaymentDetails', '');" CssClass="form-control-res" Style="width: 100%"></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div id="divVendorDate" class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateVendor" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateVendor" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="divVendorPayFilter" class="col-md-12" style="margin-top: 15px; display: none;">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoSummery" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="SUMMARY" Checked="true" />
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoDetail" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-3 form_bootstyle" id="chByDate">
                                        <asp:CheckBox ID="chkBydateVendor" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-6 form_bootstyle display_none" id="chBasedon">
                                        <asp:CheckBox ID="chCreditDays" runat="server" Text="Based on Credit Days" Class="radioboxlist" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-5 form_bootstyle" id="chChkDate">
                                        <asp:CheckBox ID="chKCheckDate" runat="server" Text="Check Date" Class="radioboxlist" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-2">
                                        <asp:LinkButton ID="lnkVendorOsReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkVendorOsReport_Click">Load Report </asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivPaymentReceipt">
                <h3>Payment & Receipts</h3>
                <div class="row">

                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoListPaymentReciept" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Inventory">Payments</asp:ListItem>
                                <asp:ListItem Value="Batch">Receipts</asp:ListItem>
                                <asp:ListItem Value="Department">Payment & Receipts</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDatePayment" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label3" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDatePayment" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-3 form_bootstyle">
                                        <asp:CheckBox ID="chkBydatePayment" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-5">
                                        <%-- <asp:LinkButton ID="lnkPaymentClear" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:LinkButton ID="lnkPaymentReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkPaymentReport_Click">Load Report </asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" runat="server" id="divLocation" style="display: none;">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="PaymentReportFilterUserControl"
                                EnablelocationDropDown="true"
                                EnableVendorDropDown="true"
                                EnableTerminalDropDown="true" />

                            <div class="col-md-12 form_bootstyle" runat="server" id="divPaymentType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblPaymentType" runat="server" Text="PaymentType"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddPaymentType" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Payment', 'txtPaymentType', '');"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-12 form_bootstyle" runat="server" id="divPaymentSubType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblPaymentSubType" runat="server" Text="PaymentSubType"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddPaymentSubType" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtPaymentSubType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Receipts', 'txtPaymentSubType', '');"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
            <asp:HiddenField ID="hdnReportType" runat="server" Value="" />
            <asp:HiddenField ID="hidVendorName" runat="server" Value="" />
        </div>
    </div>
</asp:Content>
