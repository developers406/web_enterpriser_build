﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmLocWiseItemSales.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmLocWiseItemSales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <script type="text/javascript">       
       
        function pageLoad() {
            $("select").chosen({ width: '100%' }); // width in px, %, em, etc
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
        }
    </script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'LocationWiseItemSale');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "LocationWiseItemSale";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">
       

        function fncHideFilter() {
            try {
                if ($('#<%=lnkFilterOption.ClientID%>').html() == "Hide Filter(F9)") {
                    $("[id*=pnlFilter]").hide();
                    $('#<%=lnkFilterOption.ClientID%>').html("Show Filter(F9)");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '100%',
                        'margin-left': '0'
                    });
                    $("select").trigger("liszt:updated");
                }
                else {
                    $("[id*=pnlFilter]").show();
                    $('#<%=lnkFilterOption.ClientID%>').html("Hide Filter(F9)");
                    $("#<%=HideFilter_ContainerRight.ClientID%>").css({
                        'width': '74%',
                        'margin-left': '1%'
                    });
                    $("select").trigger("liszt:updated");
                }

                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncClear() {
            try {
                $('input[type="text"]').val('');
                $('#<%= txtItemCode.ClientID %>').val('');
                $('#<%= txtLocation.ClientID %>').val('');
                $('#<%= txtDepartment.ClientID %>').val('');
                $('#<%= txtCategory.ClientID %>').val('');
                $('#<%= txtBrand.ClientID %>').val('');
                $('#<%= txtVendor.ClientID %>').val('');
                SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                __doPostBack('ctl00$ContentPlaceHolder1$lnkClose', '');
                return false;
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {
                if (e.keyCode == 117) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkClearAll.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 118) {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkFilter', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 119) {
                    __doPostBack('ctl00$ContentPlaceHolder1$lnkExport', '');
                    e.preventDefault();
                }
                else if (e.keyCode == 120) {
                    $("input").removeAttr('disabled');
                    $('#<%= lnkFilterOption.ClientID %>').click();
                    e.preventDefault();
                }
                else if (e.keyCode == 112) {
                    e.preventDefault();
                }
        }
    };

    $(document).ready(function () {
        $(document).on('keydown', disableFunctionKeys);
    });

    function SetDefaultDate(FromDate, Todate) {
        FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        if (FromDate.val() === '') {
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
        }
        if (Todate.val() === '') {
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
        }
    }
    function fncSetValue() {
        try {

        }
        catch (err) {
            ShowPopupMessageBox(err.message);
        }
    }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="main-container">
        <div class="breadcrumbs" id="breadcrumbs" runat="server">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Location Wise Item Sales</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Location Wise Item Sales </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                    <ContentTemplate>

                        <div class="control-group-price-header">
                            Filtertions
                        </div>
                        <%--<div class="col-md-12">
                            <div class="col-md-6 form_bootstyle">--%>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="lblLocation" runat="server" Text='<%$ Resources:LabelCaption,lblLocation %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text='<%$ Resources:LabelCaption,lblDepartment %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlDept" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text='<%$ Resources:LabelCaption,lbl_Category %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text='<%$ Resources:LabelCaption,lblBrand %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtVendor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text='<%$ Resources:LabelCaption,lbl_Vendor %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res">
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text='<%$ Resources:LabelCaption,lblItemCode %>'></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', '');" CssClass="form-control-res" MaxLength="30"></asp:TextBox>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>


                <asp:UpdatePanel ID="UpdatePanel4" runat="Server">
                    <ContentTemplate>
                        <div class="control-container">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="button-red"
                                    OnClick="lnkFilter_Click">Filter(F7)</asp:LinkButton>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server">
                <%--<div class="price-qty">
                    <asp:RadioButton ID="rbtAllQty" runat="server" Text="All Qty" GroupName="Qty" OnCheckedChanged="rbtAllQty_CheckedChanged"
                        Checked="true" AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtGreaterQty" runat="server" Text="Qty>0" GroupName="Qty" OnCheckedChanged="rbtGreaterQty_CheckedChanged"
                                AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtLessQty" runat="server" Text="Qty<=0" GroupName="Qty" OnCheckedChanged="rbtLessQty_CheckedChanged"
                                AutoPostBack="true" />
                    &nbsp;&nbsp;&nbsp;
                </div>--%>

                <asp:UpdatePanel ID="UpdatePanel2" runat="Server" style="padding-top: 10px;">
                    <ContentTemplate>
                        <div class="gridDetails grid-overflow" id="HideFilter_GridOverFlow" runat="server"
                            style="height: 380px">
                            <asp:GridView ID="grdLocItemprice" runat="server" AutoGenerateColumns="true"
                                ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn"
                                OnRowDataBound="grdLocItemprice_RowDataBound"
                                >
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                </EmptyDataTemplate>
                                <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                <RowStyle CssClass="pshro_GridDgnStyle" />
                                <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                    NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                <PagerStyle CssClass="pshro_text" />
                                <Columns>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="UpdatePanel3" runat="Server">
                    <ContentTemplate>

                        <div style="float: right; display: table">
                            <div class="control-button">
                                <asp:LinkButton ID="lnkFilterOption" runat="server" class="button-blue" OnClientClick="return fncHideFilter()">Hide Filter(F9)</asp:LinkButton>
                            </div>
                            <div class="control-button">
                                <asp:LinkButton ID="lnkClearAll" runat="server" class="button-blue" OnClientClick="return fncClear()">Clear(F6)</asp:LinkButton>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="control-button">
                    <asp:LinkButton ID="lnkExport" runat="server" class="button-blue" OnClick="lnkExport_Click">Excel Export(F8)</asp:LinkButton>
                </div>
            </div>

        </div>

        <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
            <ProgressTemplate>
                <div class="modal-loader">
                    <div class="center-loader">
                        <img alt="" src="../images/loading_spinner.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    </div>
    <div class="control-button" style="display: none">
        <asp:LinkButton ID="lnkClose" runat="server" class="button-blue" Text='<%$ Resources:LabelCaption,btnClear %>' OnClick="lnkClose_Click"></asp:LinkButton>
    </div>

</asp:Content>

<%--RowCellStyle ="grdLocItemprice_RowCellStyle"--%>

<%--OnRowDataBound="grdLocItemprice_RowDataBound" OnRowCommand="grdLocItemprice_RowCommand"--%>
