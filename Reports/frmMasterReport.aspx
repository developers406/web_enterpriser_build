﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMasterReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmMasterReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
    }

    </style>

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'MasterReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "MasterReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <script type="text/javascript">

        var ctrltxtInventory;

        function pageLoad() {

            //SetAutoComplete();

            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked');
             
            if (!Bydatevalue) { 
                $("[id*=divfilter]").attr("disabled", "disabled");
                $("[id*=txtFromDate]").attr("disabled", "disabled");
                $("[id*=txtToDate]").attr("disabled", "disabled");
            }

             $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=divfilter]").attr("disabled", "disabled");
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=divfilter]").removeAttr("disabled");
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });
            //$('.ziehharmonika').ziehharmonika({
            //    collapsible: false,
            //    arrow: true,
            //    scroll: true,
            //    prefix: '★',//'♫'//,
            //    collapseIcons: {
            //        opened: ' ',
            //        closed: ' '
            //    }
            //});

            $('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'}); 

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

           

        }


        function clearForm() {

            try {

                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }


        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


<%--        $(function () {

            try {

                $('#<%=chkBydate.ClientID%>').change(function () {
                    if (this.checked) {
                        $("[id*=divfilter]").attr("disabled", "disabled");
                        $("[id*=txtFromDate]").removeAttr("disabled");
                        $("[id*=txtToDate]").removeAttr("disabled");
                    }
                    else {
                        $("[id*=divfilter]").removeAttr("disabled");
                        $("[id*=txtFromDate]").attr("disabled", "disabled");
                        $("[id*=txtToDate]").attr("disabled", "disabled");
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        });--%>


        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }


    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Masters Report</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <h3>Inventory Masters Report</h3>
            <div style="overflow: visible; display: block;">
                <div class="row">
                    <div class="col-md-8">
                        <div>
                            <asp:RadioButtonList ID="rdoMasterReport" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Department">Department</asp:ListItem>
                                <asp:ListItem Value="Category">Category</asp:ListItem>
                                <asp:ListItem Value="SubCategory">SubCategory</asp:ListItem>
                                <asp:ListItem Value="Brand">Brand</asp:ListItem>
                                <asp:ListItem Value="Floor">Floor</asp:ListItem>
                                <asp:ListItem Value="Section">Section</asp:ListItem>
                                <asp:ListItem Value="Vendor">Vendor</asp:ListItem>
                                <asp:ListItem Value="Location">Location</asp:ListItem>
                                <asp:ListItem Value="Warehouse">Warehouse</asp:ListItem>
                                <asp:ListItem Value="Terminal">Terminal</asp:ListItem>
                                <asp:ListItem Value="Class">Class</asp:ListItem>
                                <asp:ListItem Value="SubClass">Sub Class</asp:ListItem>
                                <asp:ListItem Value="PlantGroup">Product Group</asp:ListItem> 
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px">
                                    <div class="col-md-4 form_bootstyle">
                                        <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                     <div class="col-md-4">
                                        <asp:CheckBox ID="chkuser" runat="server" Text="By User" Class="radioboxlist" Font-Bold="True" />
                                    </div>                                   
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                                        <%--<asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
