﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmStockAgingReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmStockAgingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'StockAgingReport');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "StockAgingReport";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">
        function pageLoad() {
            try {
                fncTabClick();
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function fncInitail() {
            try {
                $("#divAttributeName").hide(); //css('dispaly', 'block');
                $("#divAttributevalue").hide(); //css('dispaly', 'none');
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncTabClick() {
            try {
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    var target = $(e.target).attr("href") // activated tab                    
                    $('#<%=hidSelection.ClientID %>').val(target);

                    if ($("#<%=hidSelection.ClientID%>").val() == "#StockReport") {
                        $('#divStkType').css("display", "block");
                    }
                    else if ($("#<%=hidSelection.ClientID%>").val() == "#PurchaseReport") {
                        $('#divStkType').css("display", "none");
                    }
                    else if ($("#<%=hidSelection.ClientID%>").val() == "#SalesReport") {
                        $('#divStkType').css("display", "none");
                    }
                });
        }
        catch (err) {
            fncToastError(err.message);
        }
    }

                            function fncGroupNameChange(value) {
                                try {
                                    $('#<%=hidGroupbyName.ClientID%>').val($('#<%=txtInvMaster.ClientID%>').val());


                                    if ($('#<%=txtInvMaster.ClientID%>').val() == "AttributeName") {
                                        $("#divAttributeName").show(); //css('dispaly', 'block');
                                        $("#divAttributevalue").hide(); //css('dispaly', 'none');
                                    }
                                    else if ($('#<%=txtInvMaster.ClientID%>').val() == "ValueName") {
                                        $("#divAttributeName").hide(); //css('dispaly', 'block');
                                        $("#divAttributevalue").show(); //css('dispaly', 'none');                    
                                    }

                                }
                                catch (err) {
                                    fncToastError(err.message);
                                }
                            }

                            function fncGetAttributeFilterValue() {
                                try {
                                    if ($('#divAttributeName:visible').length == 1) {
                                        $('#<%=hidAttrFilterBy.ClientID%>').val($('#<%=txtAttributeName.ClientID%>').val());
                                    }
                                    else if ($('#divAttributevalue:visible').length == 1) {
                                        $('#<%=hidAttrFilterBy.ClientID%>').val($('#<%=txtAttributeValue.ClientID%>').val());
                                    }
                            }
                            catch (err) {
                                fncToastError(err.message);
                            }
                            }

        //----------------------------------------------------------------------------------------------------------------------------------------------
        function fncSetValue() {
            try {
                if (SearchTableName == "InvMaster") {
            fncGroupNameChange('InventoryMaster');
        }
                
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <div class="breadcrumbs">
            <ul><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul></div></div>
    <div id="divItemSearchTextiles">
        <div class="container welltextiles" style="margin-top: 10px;">
            <div style="border: solid; margin-bottom: 10px;">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="container welltextiles" style="width: 100%;">
                            <div class="col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Vendor</label>
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                            <div class="col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Department</label>
                                <asp:TextBox ID="txtDepartment" onkeydown="return fncShowSearchDialogCommon(event, 'Department',  'txtDepartment', 'txtCategory');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Category</label>
                                <asp:TextBox ID="txtCategory" onkeydown="return fncShowSearchDialogCommon(event, 'category',  'txtCategory', 'txtClass');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Class</label>
                                <asp:TextBox ID="txtClass" onkeydown="return fncShowSearchDialogCommon(event, 'Class',  'txtClass', 'txtBrand');" CssClass="col-md-11 " runat="server"></asp:TextBox>

                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Brand</label>
                                <asp:TextBox ID="txtBrand" onkeydown="return fncShowSearchDialogCommon(event, 'Brand',  'txtBrand', 'txtItemSearchT');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label for="staticEmail" class="col-md-12 col-form-label">Item Code</label>
                                <asp:TextBox ID="txtItemSearchT" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemSearchT', 'txtStyle');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label id="lblStyle" class="col-md-12 col-form-label">Style </label>
                                <asp:TextBox ID="txtStyle" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtStyle', 'txtLocationcode');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                                <label id="lblLocationcode" class="col-md-12 col-form-label">Location code</label>
                                <asp:TextBox ID="txtLocationcode" onkeydown="return fncShowSearchDialogCommon(event, 'Location',  'txtLocationcode', 'txtInvMaster');" CssClass="col-md-11 " runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-8">
                                <div class="col-md-4">
                                    <asp:Label ID="lblInventoryMaster" runat="server" Text="Group By" CssClass="col-md-12"></asp:Label>
                                    <%--<asp:DropDownList ID="ddlInvMaster" runat="server" class="chosen-select" CssClass="col-md-11" onchange="fncGroupNameChange('InventoryMaster');">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtInvMaster" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'InvMaster', 'txtInvMaster', 'txtAttributeName');"></asp:TextBox>
                                </div>
                                <div class="col-md-4" id="divAttributeName">
                                    <asp:Label ID="lblAttributeName" runat="server" Text="Filter By Attribute Name" CssClass="col-md-12"></asp:Label>
                                    <%--<asp:DropDownList ID="ddlAttributeName" runat="server" class="chosen-select" CssClass="col-md-11">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtAttributeName" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'AttrName', 'txtAttributeName', '');"></asp:TextBox>
                                </div>
                                <div class="col-md-4" id="divAttributevalue">
                                    <asp:Label ID="lblAttributeValue" runat="server" Text="Filter By Attribute Value" CssClass="col-md-12"></asp:Label>
                                    <%--<asp:DropDownList ID="ddlAttributeValue" runat="server" class="chosen-select" CssClass="col-md-11">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtAttributeValue" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'AttrVal', 'txtAttributeValue', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="container welltextiles" style="width: 100%;">
                            <div class="barcode-header" style="background-color: #c6b700">
                                Attributes
                            </div>
                            <div style="height: auto; border: solid; overflow-x: visible; overflow-y: hidden; height: 215px;">
                                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                                    ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid"
                                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                    <RowStyle CssClass="pshro_GridDgnStyle" />
                                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                                    <PagerStyle CssClass="pshro_text" />
                                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                    <Columns>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-9">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <asp:UpdatePanel ID="upbtn" UpdateMode="always" runat="Server">
                    <ContentTemplate>
                        <asp:LinkButton ID="lnkbtClearFilter" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Clear"
                            OnClick="lnkbtClearFilter_click"></asp:LinkButton>
                        <asp:LinkButton ID="lnkLoad" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Load"
                            OnClientClick="fncGetAttributeFilterValue();" OnClick="lnkLoad_click"></asp:LinkButton>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hidSelection" runat="server" Value="#StockReport" />
        <asp:HiddenField ID="hidGroupbyName" runat="server" Value="Description" />
        <asp:HiddenField ID="hidAttrFilterBy" runat="server" Value="" />
    </div>
</asp:Content>
