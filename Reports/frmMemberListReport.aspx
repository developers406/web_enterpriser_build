﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmMemberListReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmMemberListReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'MemberListReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "MemberListReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>

    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>

    <script type="text/javascript">

        var ctrltxtInventory;

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {

            //$("[id*=GroupByLevel]").hide();
            //$("[id*=GroupByCompare]").hide();
            //$("[id*=GroupByVisitor]").show();
            //$('.ziehharmonika').ziehharmonika({
            //    collapsible: false,
            //    arrow: true,
            //    scroll: true,
            //    prefix: '★',//'♫'//,
            //    collapseIcons: {
            //        opened: ' ',
            //        closed: ' '
            //    }
            //});

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'});

            var checkvalueed = $("#<%=rdoMemberList.ClientID%>").find(":checked").val();
            if (checkvalueed == 'MemberList') {
                $("[id*=GroupByLevel]").hide();
                $("[id*=GroupByCompare]").hide();
                $("[id*=GroupByVisitor]").show();
                
            }
            else if (checkvalueed == 'Level') {
                $("[id*=GroupByLevel]").show();
                $("[id*=GroupByCompare]").hide();
                $("[id*=GroupByVisitor]").hide();
               
            }
            else if (checkvalueed == 'Comparison') {
                $("[id*=GroupByLevel]").hide();
                $("[id*=GroupByCompare]").show();
                $("[id*=GroupByVisitor]").hide();
              
            }
            else {
                $("[id*=GroupByLevel]").hide();
                $("[id*=GroupByCompare]").hide();
                $("[id*=GroupByVisitor]").hide();
                 
            }
            if (checkvalueed == 'MemberList')
            {
                $('#ContentPlaceHolder1_divMemberType').show();
                $('#ContentPlaceHolder1_divMember').show();
                $('#ContentPlaceHolder1_divIncomeGroup').hide();
                $('#ContentPlaceHolder1_divInvoiceNo').hide();
                $('#ContentPlaceHolder1_divZone').hide();
                $('#ContentPlaceHolder1_divArea').show();
                $('#ContentPlaceHolder1_divMobileNo').show();
            }
            if (checkvalueed == 'Points' || checkvalueed == 'Adjustment' || checkvalueed == 'Summary' || checkvalueed == 'Detail' || checkvalueed == 'Items' || checkvalueed == 'Issued' || checkvalueed == 'UnIssued')//aalayam 03052018
             {
                $('#ContentPlaceHolder1_divMemberType').show();
                $('#ContentPlaceHolder1_divMember').show();
                $('#ContentPlaceHolder1_divIncomeGroup').hide();
                $('#ContentPlaceHolder1_divInvoiceNo').hide();
                $('#ContentPlaceHolder1_divZone').hide();
                $('#ContentPlaceHolder1_divArea').hide();
                $('#ContentPlaceHolder1_txtArea').val("");
                $('#ContentPlaceHolder1_divMobileNo').show();
                //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
            }
            if (checkvalueed == 'Zone')
            {
                $('#ContentPlaceHolder1_divMemberType').show();
                $('#ContentPlaceHolder1_divMember').show();
                $('#ContentPlaceHolder1_divIncomeGroup').hide();
                $('#ContentPlaceHolder1_divInvoiceNo').hide();
                $('#ContentPlaceHolder1_divZone').show();
                $('#ContentPlaceHolder1_divArea').hide();
                $('#ContentPlaceHolder1_txtArea').val("");
                $('#ContentPlaceHolder1_divMobileNo').show();
                //$('#ContentPlaceHolder1_txtArea').trigger("liszt:updated");
 
            }
            if (checkvalueed == 'Income')
            {
                $('#ContentPlaceHolder1_divMemberType').show();
                $('#ContentPlaceHolder1_divMember').show();
                $('#ContentPlaceHolder1_divIncomeGroup').show();
                $('#ContentPlaceHolder1_divInvoiceNo').hide();
                $('#ContentPlaceHolder1_divZone').hide();
                $('#ContentPlaceHolder1_txtArea').val("");
                $('#ContentPlaceHolder1_divMobileNo').show();
                //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
       
            }
            if (checkvalueed == 'Level' || checkvalueed == 'Comparison')
            {
                $('#ContentPlaceHolder1_divMemberType').hide();
                $('#ContentPlaceHolder1_divMember').show();
                $('#ContentPlaceHolder1_divIncomeGroup').hide();
                $('#ContentPlaceHolder1_divInvoiceNo').hide();
                $('#ContentPlaceHolder1_divZone').hide();
                $('#ContentPlaceHolder1_divArea').hide();
                $('#ContentPlaceHolder1_txtArea').val("");
                $('#ContentPlaceHolder1_divMobileNo').show();
                //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
            }
            if (checkvalueed == 'Invoice')
            {
                $('#ContentPlaceHolder1_divMemberType').hide();
                $('#ContentPlaceHolder1_divMember').hide();
                $('#ContentPlaceHolder1_divIncomeGroup').hide();
                $('#ContentPlaceHolder1_divInvoiceNo').show();
                $('#ContentPlaceHolder1_divZone').hide();
                $('#ContentPlaceHolder1_divArea').hide();
                $('#ContentPlaceHolder1_txtArea').val("");
                $('#ContentPlaceHolder1_divMobileNo').hide();
                //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
            }
            if (checkvalueed == 'Count')
                $("[id*=GrpfilterMemberlist]").hide();

            else
                $("[id*=GrpfilterMemberlist]").show();


            if (checkvalueed == 'Points')
            {
                $("[id*=txtFromDate]").attr("disabled", "disabled");
                $("[id*=txtToDate]").attr("disabled", "disabled");
                $("[id*=chkBydate]").attr("disabled", "disabled");
              
            }
            else
            {
                $("[id*=txtFromDate]").removeAttr("disabled");
                $("[id*=txtToDate]").removeAttr("disabled");
                $("[id*=chkBydate]").removeAttr("disabled");
              
            }//


            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=divfilter]").attr("disabled", "disabled");
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=divfilter]").removeAttr("disabled");
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });


        }

        function clearForm() {

            try {
                $('input[type="text"]').val('');
                $("[id$=txtInventory]").val('');
                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }

        $(document).ready(function () {


            $('#<%= rdoMemberList.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoMemberList.ClientID %> input:radio:checked").val();
                if (checkvalue == 'MemberList') {
                    $("[id*=GroupByLevel]").hide();
                    $("[id*=GroupByCompare]").hide();
                    $("[id*=GroupByVisitor]").show();
                     
                }
                else if (checkvalue == 'Level') {
                    $("[id*=GroupByLevel]").show();
                    $("[id*=GroupByCompare]").hide();
                    $("[id*=GroupByVisitor]").hide();
                    
                }
                else if (checkvalue == 'Comparison') {
                    $("[id*=GroupByLevel]").hide();
                    $("[id*=GroupByCompare]").show();
                    $("[id*=GroupByVisitor]").hide();
 
                }
                else {
                    $("[id*=GroupByLevel]").hide();
                    $("[id*=GroupByCompare]").hide();
                    $("[id*=GroupByVisitor]").hide();
                        
                }
                if (checkvalue == 'MemberList') {
                    $('#ContentPlaceHolder1_divMemberType').show();
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divIncomeGroup').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divArea').show();
                    $('#ContentPlaceHolder1_divMobileNo').show();
                    
                }
                if ( checkvalue == 'Points' || checkvalue == 'Adjustment' || checkvalue == 'Summary' || checkvalue == 'Detail' || checkvalue == 'Items' || checkvalue == 'Issued' || checkvalue == 'UnIssued')//aalayam 03052018
                {
                    $('#ContentPlaceHolder1_divMemberType').show();
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divIncomeGroup').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divArea').hide();
                    $('#ContentPlaceHolder1_txtArea').val("");
                    $('#ContentPlaceHolder1_divMobileNo').show();
                    //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");

                }
                if (checkvalue == 'Zone') {
                    $('#ContentPlaceHolder1_divMemberType').show();
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divIncomeGroup').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divZone').show();
                    $('#ContentPlaceHolder1_divArea').hide();
                    $('#ContentPlaceHolder1_txtArea').val("");
                    $('#ContentPlaceHolder1_divMobileNo').show();
                    //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
                }

                if (checkvalue == 'Income') {
                    $('#ContentPlaceHolder1_divMemberType').show();
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divIncomeGroup').show();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divArea').hide();
                    $('#ContentPlaceHolder1_txtArea').val("");
                    $('#ContentPlaceHolder1_divMobileNo').show();
                    //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
                }

                if (checkvalue == 'Level' || checkvalue == 'Comparison') {
                    $('#ContentPlaceHolder1_divMemberType').hide();
                    $('#ContentPlaceHolder1_divMember').show();
                    $('#ContentPlaceHolder1_divMobileNo').show();
                    $('#ContentPlaceHolder1_divIncomeGroup').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').hide();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divArea').hide();
                    $('#ContentPlaceHolder1_txtArea').val("");
                    //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
                }
                if (checkvalue == 'Invoice') {
                    $('#ContentPlaceHolder1_divMemberType').hide();
                    $('#ContentPlaceHolder1_divMember').hide();
                    $('#ContentPlaceHolder1_divIncomeGroup').hide();
                    $('#ContentPlaceHolder1_divInvoiceNo').show();
                    $('#ContentPlaceHolder1_divZone').hide();
                    $('#ContentPlaceHolder1_divArea').hide();
                    $('#ContentPlaceHolder1_txtArea').val("");
                    $('#ContentPlaceHolder1_divMobileNo').hide();
                    //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated"); 
                }
                if (checkvalue == 'Count')
                    $("[id*=GrpfilterMemberlist]").hide();

                else
                    $("[id*=GrpfilterMemberlist]").show();

                $('#ContentPlaceHolder1_txtMemberType').val('');
                //$('#ContentPlaceHolder1_ddlMemberType').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtMember').val('');
                //$('#ContentPlaceHolder1_ddlMember').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtIncomeGroup').val('');
                //$('#ContentPlaceHolder1_ddlIncomeGroup').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtInvoiceNo').val('');
                //$('#ContentPlaceHolder1_ddlInvoiceNo').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtZone').val('');
                //$('#ContentPlaceHolder1_ddlZone').trigger("liszt:updated");
                $('#ContentPlaceHolder1_txtArea').val("");
                //$('#ContentPlaceHolder1_ddlArea').trigger("liszt:updated");
                if (checkvalue == 'Points') {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                    $("[id*=chkBydate]").attr("disabled", "disabled");
                  
                }
                else {
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                    $("[id*=chkBydate]").removeAttr("disabled");
                  
                }

            });
        });

        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


        $(function () {

            try {

                $('#<%=chkBydate.ClientID%>').change(function () {
                    if (this.checked) {
                        $("[id*=txtFromDate]").removeAttr("disabled");
                        $("[id*=txtToDate]").removeAttr("disabled");
                    }
                    else {
                        $("[id*=txtFromDate]").attr("disabled", "disabled");
                        $("[id*=txtToDate]").attr("disabled", "disabled");
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        });


        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
        function fncSetValue() {
            try {
                if (SearchTableName == "Member") {
                    $('#ContentPlaceHolder1_txtMember').val($.trim(Code)); 
        }
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Member List Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <h3>Member List Reports</h3>
            <div style="overflow: visible; display: block;">
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoMemberList" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="MemberList">MemberList</asp:ListItem>
                                <asp:ListItem Value="Income">MemberList By Income</asp:ListItem>
                                <asp:ListItem Value="Points">Member Points</asp:ListItem>
                                <asp:ListItem Value="Adjustment">Member Points Adjustment</asp:ListItem>
                                <asp:ListItem Value="Summary">Member Sales Summary</asp:ListItem>
                                <asp:ListItem Value="Detail">Member Sales Detail</asp:ListItem>
                                <asp:ListItem Value="Items">Member Sales with Items</asp:ListItem>
                             <%--   <asp:ListItem Value="Issued">Gift Voucher - Issued</asp:ListItem>
                                <asp:ListItem Value="UnIssued">Gift Voucher - Un Issued</asp:ListItem>--%>
                                <asp:ListItem Value="Zone">Member Sales - Zone</asp:ListItem>
                                <asp:ListItem Value="Level">Member Sales - Level</asp:ListItem>
                                <asp:ListItem Value="Count">Member Count</asp:ListItem>
                                <asp:ListItem Value="Invoice">Member Change by Invoice</asp:ListItem>
                                <asp:ListItem Value="Comparison">Member Sales - Comparison</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                     <div id="GrpfilterMemberlist" class="col-md-4 form_bootstyle_border" style="margin-right: 55px">
                        <%--aalayam 02052018 --%>
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12 form_bootstyle" runat="server" id="divMember">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label19" runat="server" Text="Member"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlMember" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtMember" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Member', 'txtMember', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divMemberType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label6" runat="server" Text="MemberType"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlMemberType" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtMemberType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'MemberType', 'txtMemberType', '');"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="divMobileNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label8" runat="server" Text="MobileNumber"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">                                  
                                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'MobileNumber', 'txtMobileNo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divIncomeGroup">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label1" runat="server" Text="Income"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlIncomeGroup" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtIncomeGroup" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'IncomeGroup', 'txtIncomeGroup', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divInvoiceNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label2" runat="server" Text="InvoiceNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlInvoiceNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'InvoiceNo', 'txtInvoiceNo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divZone">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label3" runat="server" Text="Zone"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlZone" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtZone" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Zone', 'txtZone', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divArea">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label7" runat="server" Text="Area"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlArea" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtArea" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'AreaCode', 'txtArea', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 15px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="GroupByLevel" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkHigh" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="HIGH"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkMedium" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="MEDIUM" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkLow" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="LOW" />
                            </div>
                        </div>
                        <div id="GroupByCompare" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkyear" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="YEAR"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkMonth" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="MONTH" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="chkWeek" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="WEEK" />
                            </div>
                        </div>
                        <div id="GroupByVisitor" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-8 form_bootstyle">
                                <asp:CheckBox ID="chkNonVisitor" Class="radioboxlistgreen" runat="server" Text="NONE VISITOR" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-5">
                                <%--   <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
