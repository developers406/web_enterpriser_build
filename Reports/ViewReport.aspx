﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="ViewReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.ViewReport" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

    <script type="text/javascript" language="Javascript"> 
        $(function () {
            $('#divImage').click(function () {
                alert($("#rptName").val());
            });
            if ($.session.get('height') != null && $.session.get('height') != "") {
                var height = $.session.get('height');
                height = parseFloat(height) - 50;
                $('.EnableScroll').css('height', '' + height + "px" + '');
            }
        });
    </script>
    <script type="text/javascript" src="../js/print.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="navigation" style="display: block; max-width: 100%; width: 100%; height: 50px; margin-top: 4px;">
        <div id="divImage" class="container-fluid">
            <img id="imgRpt" src="../images/CompanyLogo.png" style="display: block; max-width: 100%; width: 240px; height: 50px; margin-top: 4px;" />
        </div>
    </div>
    <div class="GridDetails EnableScroll" id="dvReport" style="width: 100%; margin-top: 10px;">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" EnableDatabaseLogonPrompt="False"
            GroupTreeImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/tree/"
            ToolbarImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/toolbar/"
            DisplayGroupTree="False" Width="100%" BestFitPage="True" />
    </div>
    <asp:HiddenField ID="rptName" runat="server" ClientIDMode="Static" Value="" />

</asp:Content>
