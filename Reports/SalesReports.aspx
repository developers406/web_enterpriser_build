﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="SalesReports.aspx.cs" Inherits="EnterpriserWebFinal.Reports.SalesReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        table
        {
            width: 100%;
        }
        .chkBydate label
        {
            position: relative;
            top: -2px;
        }
    </style>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'SaleReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "SaleReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript" language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <%-- <link href="css/jquery-ui-dtp.css" rel="stylesheet" type="text/css" />
    <script src="js/datetimepicker/jquery-1.8.2.js" type="text/javascript"></script>
    <script src="js/datetimepicker/jquery-ui.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).click(function () {

            //            $(".rg").change(function () {

            //                if ($("#r1").attr("checked")) {
            //                    $('#r1edit:input').removeAttr('disabled');
            //                }
            //                else {
            //                    $('#r1edit:input').attr('disabled', 'disabled');
            //                }
            //            });


            $("input:radio").change(function () {
                //$("#<%=divSalesReport.ClientID %>").toggle("slow"); 


                //                var Rdochecked = $(this).next().text();
                //                if (Rdochecked == "Voucher Sales" || Rdochecked == "SalesMan Commission" || Rdochecked == "Floor Wise Sales") {

                //                    $("[id*=divSalesMoreSD]").show();
                //                    $("[id*=divDateGroups]").show(); 
                //                }
                //                else {

                //                    $("[id*=divSalesMoreSD]").hide();
                //                    $("[id*=divDateGroups]").hide(); 
                //                }


                if ($("#<%=rdoBatchWiseInvlist.ClientID %>").is(':checked')) {                     
                    $("[id*=divBatchcount]").show();
                }
                else {
                    $("[id*=divBatchcount]").hide();
                }

                if ($("#<%=chkInventoryList.ClientID %>").is(':checked')) {

                    $("#complete").css("display", "block");
                }
                else {
                    $("#complete").css("display", "none");
                }

                if ($("#<%=chkCompleteInfo.ClientID %>").is(':checked')) {

                    $("#subDiv").css("display", "block");
                }
                else {
                    $("#subDiv").css("display", "none");
                }

                // $("#chkCompleteInfo input[type='radio']:checked").val();

            });
        });

        function pageLoad() {

            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            //alert($("div:visible").text());
            $("[id*=divSalesMoreSD]").hide();
            $("[id*=divDateGroups]").hide();
            $("[id*=divfilter]").hide();
            $("[id*=lnkFilter]").show();
            $("[id*=divBatchcount]").hide();
            $("[id*=divCRDueSD]").hide();

            // $("#mydate").datepicker({ dateFormat: "yy-mm-dd" }).datepicker("setDate", "0");
            // $("#complete").css("display", "none");
            // $("#<%=chkCompleteInfo.ClientID %>").css("display", "none");


            var Rdochecked = $('input[type="radio"]:checked').next().text();

            if (Rdochecked.indexOf("Voucher-") != -1 || Rdochecked.indexOf("SalesMan-") != -1 || Rdochecked.indexOf("Floor Wise") != -1) {
                //if (Rdochecked == "Voucher SalesSUMMARYDATE" || Rdochecked == "SalesMan CommissionSUMMARYDATE" || Rdochecked == "Floor Wise SalesSUMMARYDATE") {                
                $("[id*=divSalesMoreSD]").show();
                $("[id*=divDateGroups]").show();
                $("[id*=lnkFilter]").hide();
            }
            else {
                //alert(Rdochecked);
                $("[id*=divSalesMoreSD]").hide();
                $("[id*=divDateGroups]").hide();
                $("[id*=lnkFilter]").hide();
            }

//            rdoBatchWiseInvlist
            //alert(Rdochecked);

            if (Rdochecked.indexOf("Department") != -1 || Rdochecked.indexOf("Inventory List") != -1) {
                $("[id*=lnkFilter]").show();
            }


            if (Rdochecked.indexOf("Outstandings") != -1 || Rdochecked.indexOf("Customer-") != -1) {
                $("[id*=divCRDueSD]").show();
            }
            else {
                $("[id*=divCRDueSD]").hide();
            }

            if ($("#<%=chkInventoryList.ClientID %>").is(':checked')) {
                $("[id*=complete]").show();
            }
            else {
                $("[id*=complete]").hide();
            }

            if ($('#<%=chkBydate.ClientID%>').is(':checked') == false) {
                $("[id*=txtfromDate]").attr("disabled", "disabled");
                $("[id*=txttoDate]").attr("disabled", "disabled");
            }

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtfromDate]").removeAttr("disabled");
                    $("[id*=txttoDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtfromDate]").attr("disabled", "disabled");
                    $("[id*=txttoDate]").attr("disabled", "disabled");
                }
            });
        }


        $(function () {
            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtfromDate.ClientID %>").val() === '') {
                $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txttoDate.ClientID %>").val() === '') {
                $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });


        function showFilter() {

            if ($("[id*=divfilter]").is(':hidden')) {
                $("[id*=divfilter]").show();
            }
            else {
                $("[id*=divfilter]").hide();
            }

            return false;
        }

        function Clear() {
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("[id*=divfilter]").hide();
            $("[id*=lnkFilter]").show();
            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            return false;
        }



        function CheckChangeSalesMore(Type, isDate) {

            $("[id*=lnkFilter]").hide();
            //            alert(isDate);
            if (isDate == "Yes") {

                $("[id*=divDateFilter]").show();
            }
            else {
                //alert(isDate);
                $("[id*=divDateFilter]").hide();
            }
        }

        function CheckChange(Type, isDate) { 

            Clear();

            //            alert(isDate + '-' + Type);
            if (isDate == "Yes") {

                $("[id*=divDateFilter]").show();
            }
            else if (isDate == "No") {
                //alert(isDate);
                $("[id*=divDateFilter]").hide();
            }

            if (Type == "Sales") {
                $("[id*=lnkFilter]").show();
                $("[id*=rptBrandDrop]").attr("disabled", "disabled");
                $("[id*=rptCatDrop]").attr("disabled", "disabled");
                $("[id*=rptDeptDrop]").attr("disabled", "disabled");
                $("[id*=rptInvDrop]").attr("disabled", "disabled");
                $("[id*=rptVendorDrop]").attr("disabled", "disabled");
                $("[id*=DropDownCashier]").removeAttr("disabled");
                $("[id*=DropDownTerminal]").removeAttr("disabled");
            }
            else if (Type == "Inventory") {
                $("[id*=lnkFilter]").show();
                $("[id*=rptBrandDrop]").removeAttr("disabled");
                $("[id*=rptCatDrop]").removeAttr("disabled");
                $("[id*=rptDeptDrop]").removeAttr("disabled");
                $("[id*=rptInvDrop]").removeAttr("disabled");
                $("[id*=rptVendorDrop]").removeAttr("disabled");
                $("[id*=DropDownCashier]").attr("disabled", "disabled");
                $("[id*=DropDownTerminal]").attr("disabled", "disabled");
            }
            else if (Type == "More") {

                $("[id*=lnkFilter]").hide();
                $("[id*=divfilter]").hide();
                if (isDate == "Yes") {

                    $("[id*=divSalesMoreSD]").show();
                    $("[id*=divDateGroups]").show();
                }
                else if (isDate == "No") {

                    $("[id*=divSalesMoreSD]").hide();
                    $("[id*=divDateGroups]").hide();
                }
                else if (isDate == "YN") {

                    $("[id*=divSalesMoreSD]").hide();
                    $("[id*=divDateGroups]").show();
                }
            }
            else if (Type == "Credit") {

                $("[id*=lnkFilter]").hide();
                $("[id*=divfilter]").hide();

                if (isDate == "Yes") {

                    $("[id*=divCRDueSD]").show();
                }
                else {
                    $("[id*=divCRDueSD]").hide();
                }
            }

        }

        function ValidateForm() {

            var Show = '';
            if ($("#<%= txtfromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtfromDate.ClientID %>").focus();
            }

            if ($("#<%= txttoDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txttoDate.ClientID %>").focus();
            }

            if ($('#<%=rdoBatchWiseInvlist.ClientID%>').is(':checked') == true) {
                if ($('#<%=txtBatchCount.ClientID%>').val() == "") {
                    Show = Show + '\n  Please Choose Batch count!';
                    $("#<%=txtBatchCount.ClientID %>").focus();
                }
            }

            if (Show != '') {
                alert(Show);
                //JSalerts(Show);
                return false;
            }

            else {

                var StartDate = $("#<%= txtfromDate.ClientID %>").val();
                var EndDate = $("#<%= txttoDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                $("#HiddenfromDate").val($("#<%= txtfromDate.ClientID %>").val());
                $("#HiddentoDate").val($("#<%= txttoDate.ClientID %>").val());
                return true;
            }
        }

    </script>
    <%--  <script type="text/javascript">

        $(function () {
            $("[id*=lnkClearSearch]").click(function () {
               
               $('#<%=txtfromDate.ClientID %>').val('');
               $('#<%=txttoDate.ClientID %>').val('');
            }
        });
          
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration:none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration:none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page" id="ReportTitle" runat="server">Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-report" style="width: 100%" style="margin-top: 10px">
            <div class="container-left" id="divInventorylist" runat="server" visible="false"
                style="margin-top: 10px">
                <div class="barcode-header">
                    Inventory List Reports
                </div>
                <asp:Panel ID="Panel1" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="chkInventoryList" runat="server" GroupName="RegularMenu" Checked="true"
                                    onclick="CheckChange('Inventory', 'Yes')" />
                                <asp:Label ID="Label7" runat="server" Checked="true" Text="Inventory List"></asp:Label>
                                <%--<p style="display: none">testing 1...2...3...</p>--%>
                            </div>
                        </div>
                        <div id="complete" style="float: right; margin-left: 15%">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkCompleteInfo" runat="server" GroupName="SubMenu" Text="Complete Info"
                                        Font-Bold="True" />
                                </div>
                            </div>
                            <div class="control-group" id="subDiv" style="float: left; border: 1px solid; padding: 5px 5px 0 5px;
                                width: 165px; margin-left: 5%">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkItemWise" runat="server" GroupName="SubMenu1" Text="ItemWise"
                                        Checked="true" Font-Bold="True" />
                                    &nbsp;&nbsp;
                                </div>
                                <div style="float: left">
                                    <asp:RadioButton ID="chkBatchwise" runat="server" GroupName="SubMenu1" Text="Batchwise"
                                        Font-Bold="True" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkMarginnotSet" runat="server" GroupName="SubMenu" Text="Margin Not Set"
                                        Font-Bold="True" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="chkDepartmantWise" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory','Yes')" />
                            <asp:Label ID="Label8" runat="server" Text="Department Wise Inventory List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="chkCategoryWise" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label9" runat="server" Text="Category Wise Inventory List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="chkBrandWise" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory','Yes')" />
                            <asp:Label ID="Label10" runat="server" Text="Brand Wise Inventory List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="chkVendorWise" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label12" runat="server" Text="Vendor Wise Inventory List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="Chkbarcodelist" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label13" runat="server" Text="Barcode list"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoBreakqty" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label31" runat="server" Text="List of breakQty items"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoDeactiveitem" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label32" runat="server" Text="Deactivated items list"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoDeactiveBatch" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label33" runat="server" Text="Deactivated Batches list"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMinqty" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label34" runat="server" Text="Min qty alert"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMaxQty" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label35" runat="server" Text="Max qty alert"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoitemChangelog" runat="server" GroupName="RegularMenu" onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label36" runat="server" Text="ItemName Change Log"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdoPriceChange" runat="server" GroupName="RegularMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label21" runat="server" Text="Price Change log"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoBatchWiseInvlist" runat="server" GroupName="RegularMenu"
                                onclick="CheckChange('Inventory', 'Yes')" />
                            <asp:Label ID="Label37" runat="server" Text="Batchwise inventory list"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" id="divBatchcount" runat="server" style="margin-bottom: 25px">
                        <div style="float: left">
                            <asp:TextBox ID="txtBatchCount" runat="server" CssClass="TextboxSize-Reports" onkeypress="return isNumberKey(event)"
                                ForeColor="Black"></asp:TextBox>
                            <asp:Label ID="Label38" runat="server" Text="Batch Count" ForeColor="#FF3300"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divSalesReport" runat="server" style="margin-top: 10px">
                <div class="barcode-header">
                    Sales Reports
                </div>
                <asp:Panel ID="Panel2" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="RdDepSales" runat="server" GroupName="salesMenu" Checked="true"
                                    onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label26" runat="server" Text="Department wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdCatSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label27" runat="server" Text="Category wise Sales"></asp:Label>
                                <asp:CheckBox ID="chkGST" runat="server" Text="GST" ForeColor="#FF3300" Visible="false" />
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdBrandSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label28" runat="server" Text="Brand wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdClassSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label5" runat="server" Text="Class wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdShelf" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label43" runat="server" Text="Shelf wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdVendorSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                                <asp:Label ID="Label39" runat="server" Text="Vendor wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdinvoiceSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','No')" />
                                <asp:Label ID="Label11" runat="server" Checked="true" Text="Invoice Wise sales Summary"></asp:Label>
                                <%--<p style="display: none">testing 1...2...3...</p>--%>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdinvSalesDetail" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','No')" />
                            <asp:Label ID="Label14" runat="server" Text="Invoice Wise sales Detail"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RditemwiseSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                            <asp:Label ID="Label15" runat="server" Text="Item Wise sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdpaymodeWiseSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label16" runat="server" Text="Paymode Wise Sales"></asp:Label>
                        </div>
                    </div>
                    <%-- <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdInvoicePaymodesales" runat="server" GroupName="salesMenu" />
                            <asp:Label ID="Label17" runat="server" Text="Invoice Wise Paymode Sales"></asp:Label>
                        </div>
                    </div>--%>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdTotalSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label18" runat="server" Text="Total Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdHourlySales" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label40" runat="server" Text="Hourly Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdSessionRpt" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label45" runat="server" Text="Session Report On"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdTerminalWiseSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label19" runat="server" Text="Terminal Wise Sales Detail"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdCashierPaymode" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label20" runat="server" Text="Cashier and Paymode wise Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdSellPriceNonBatch" runat="server" GroupName="salesMenu" onclick="CheckChange('Sales','Yes')" />
                            <asp:Label ID="Label44" runat="server" Text="Selling Price Wise NonBatch Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="RdBatchSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                            <asp:Label ID="Label41" runat="server" Text="Batch Wise Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" style="margin-bottom: 18px">
                        <div style="float: left">
                            <asp:RadioButton ID="RdNonBatchSales" runat="server" GroupName="salesMenu" onclick="CheckChange('Inventory','Yes')" />
                            <asp:Label ID="Label42" runat="server" Text="Non Batch Wise Sales"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" id="divDateFilter" style="float: left; border: 1px solid;
                        padding: 5px 5px 0 5px; width: 70%" runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="Radiodate" runat="server" GroupName="Groupby" Text="DATE" Checked="true"
                                Font-Bold="True" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="RadioMonth" runat="server" GroupName="Groupby" Text="MONTH"
                                Font-Bold="True" />
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="RadioNone" runat="server" GroupName="Groupby" Text="NONE" Font-Bold="True" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divSalesAudit" runat="server">
                <div class="barcode-header">
                    Sales Audit Reports
                </div>
                <asp:Panel ID="Panel11" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBillCancelLog" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    Checked="True" OnCheckedChanged="rdoBillCancelLog_CheckedChanged" />
                                <asp:Label ID="Label119" runat="server" Checked="true" Text="Bill Cancel Log"></asp:Label>
                                <asp:CheckBox ID="chkDeptWise" runat="server" Text="Department Wise" ForeColor="#FF3300"
                                    Visible="false" />
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBillDiscLog" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label120" runat="server" Text="Bill Discount Log"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPaymodeChangeLog" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label123" runat="server" Text="Paymode Change Log"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBillClearLog" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label122" runat="server" Text="Bill Clear Log"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoItemCancelLog" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label124" runat="server" Text="ItemCancel Log(Delete item from Grid)"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDBbackup" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label128" runat="server" Text="DB Backup"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoModulewise" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label129" runat="server" Text="Module Wise Reports"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBrandDiscSales" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    OnCheckedChanged="rdoBrandDiscSales_CheckedChanged" />
                                <asp:Label ID="Label131" runat="server" Text="Brand wise Sales discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDeptDiscSales" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    OnCheckedChanged="rdoDeptDiscSales_CheckedChanged" />
                                <asp:Label ID="Label132" runat="server" Text="Department wise Sales discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCategDiscSales" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    OnCheckedChanged="rdoCategDiscSales_CheckedChanged" />
                                <asp:Label ID="Label133" runat="server" Text="Category wise Sales discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBreakPriceDiscSales" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label134" runat="server" Text="Break Price Sales discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPriceChngDiscSales" runat="server" GroupName="RegularMenu"
                                    AutoPostBack="True" OnCheckedChanged="rdoPriceChngDiscSales_CheckedChanged" />
                                <asp:Label ID="Label135" runat="server" Text="Pricechange Sales discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPromotDisc" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    OnCheckedChanged="rdoPromotDisc_CheckedChanged" />
                                <asp:Label ID="Label136" runat="server" Text="Promotion wise Discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCleardiscount" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label4" runat="server" Text="Clearing Sales Discount"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHoldBill" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                    OnCheckedChanged="rdoHoldBill_CheckedChanged" />
                                <asp:Label ID="Label137" runat="server" Text="Hold bills"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPOSActLog" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label138" runat="server" Text="POS Activity Log"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="divSADT" visible="false" style="float: left; border: 1px solid;
                            margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoSAdtSumm" runat="server" Text="SUMMARY"
                                    Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoSAdtDet" runat="server" Text="DETAIL" Font-Bold="True"
                                    Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divPreSales" runat="server" style="margin-top: 10px;">
                <div class="barcode-header">
                    Pre-Sales Reports
                </div>
                <asp:Panel ID="Panel4" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesEstPending" runat="server" GroupName="Presales" Checked="true" />
                                <asp:Label ID="Label46" runat="server" Text="Sales Estimation- Pending"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesEstClosed" runat="server" GroupName="Presales" />
                                <asp:Label ID="Label47" runat="server" Text="Sales Estimation- Closed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesEstCancel" runat="server" GroupName="Presales" />
                                <asp:Label ID="Label48" runat="server" Text="Sales Estimation- Cancel"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesOrderPending" runat="server" GroupName="Presales" />
                                <asp:Label ID="Label49" runat="server" Text="Sales Order - Pending"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesOrderClosed" runat="server" GroupName="Presales" />
                                <asp:Label ID="Label50" runat="server" Text="Sales Order - Closed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 5px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesOrderCancel" runat="server" GroupName="Presales" />
                                <asp:Label ID="Label51" runat="server" Text="Sales Order - Cancel"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="divPreSalesSumDet" style="float: left; border: 1px solid;
                            margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="chkPreSumm" runat="server" Text="SUMMARY"
                                    Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="chkPreDet" runat="server" Text="DETAIL" Font-Bold="True"
                                    Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divSalesMore" runat="server" style="margin-top: 10px;">
                <div class="barcode-header">
                    Sales - More Reports
                </div>
                <asp:Panel ID="Panel6" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoVoucherSales" runat="server" GroupName="Creditsales" Checked="true"
                                    onclick="CheckChange('More','Yes')" />
                                <asp:Label ID="Label64" runat="server" Text="Voucher- Sales"></asp:Label>
                                <%--<asp:RadioButton ID="RadioButton2" runat="server" GroupName="Level2"  Text="Paid" ForeColor="#FF3300" />
                                <asp:RadioButton ID="RadioButton3" runat="server" GroupName="Level2" Text="UnPaid" ForeColor="#FF3300" />
                                <asp:RadioButton ID="RadioButton4" runat="server" GroupName="Level2" Checked="true" Text="All" ForeColor="#FF3300" />--%>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoVouchSalesDenom" runat="server" GroupName="Creditsales" onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label65" runat="server" Text="Voucher Sales Denomination"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCreditCardSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label66" runat="server" Text="CreditCard Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHomeDelSalesPending" runat="server" GroupName="Creditsales"
                                    onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label67" runat="server" Text="Home Delivery Sales - Pending"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHomeDelSalesclosed" runat="server" GroupName="Creditsales"
                                    onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label68" runat="server" Text="Home Delivery Sales - Closed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHomeDelProcessdetail" runat="server" GroupName="Creditsales"
                                    onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label69" runat="server" Text="Home Delivery Process Detail"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPackageInv" runat="server" GroupName="Creditsales" onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label70" runat="server" Text="Package Inventory Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCashierSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label71" runat="server" Text="Cashier wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesmanSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','No')" />
                                <asp:Label ID="Label72" runat="server" Text="Salesman wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSalesmanCommis" runat="server" GroupName="Creditsales" onclick="CheckChange('More','Yes')" />
                                <asp:Label ID="Label73" runat="server" Text="SalesMan- Commission"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdofloorSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','Yes')" />
                                <asp:Label ID="Label74" runat="server" Text="Floor Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoChequeSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','YN')" />
                                <asp:Label ID="Label76" runat="server" Text="Cheque Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoIMEISales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','YN')" />
                                <asp:Label ID="Label77" runat="server" Text="IMEI Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 5px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHelperSales" runat="server" GroupName="Creditsales" onclick="CheckChange('More','Yes')" />
                                <asp:Label ID="Label75" runat="server" Text="Helper Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="divSalesMoreSD" style="float: left; border: 1px solid;
                            margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoSOthSumm" runat="server" Text="SUMMARY"
                                    Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoSOthDet" runat="server" Text="DETAIL" Font-Bold="True"
                                    Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group" id="divDateGroups" style="float: left; border: 1px solid;
                        padding: 5px 5px 0 5px; width: 70%" runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoSOthDate" runat="server" GroupName="Groupby" Text="DATE"
                                Checked="true" Font-Bold="True" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoSOthMonth" runat="server" GroupName="Groupby" Text="MONTH"
                                Font-Bold="True" />
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton ID="rdoSOthNone" runat="server" GroupName="Groupby" Text="NONE"
                                Font-Bold="True" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divCreditDueSales" runat="server" style="margin-top: 10px;">
                <div class="barcode-header">
                    Credit & Due Sales Reports
                </div>
                <asp:Panel ID="Panel5" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCreditSales" runat="server" Checked="true" GroupName="Creditsales"
                                    onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label52" runat="server" Text="Credit Sales Report"></asp:Label>
                                <asp:RadioButton ID="chkPaid" runat="server" GroupName="Level2" Text="Paid" ForeColor="#FF3300"
                                    Visible="false" />
                                <asp:RadioButton ID="chkUnPaid" runat="server" GroupName="Level2" Text="UnPaid" ForeColor="#FF3300"
                                    Visible="false" />
                                <asp:RadioButton ID="chkAll" runat="server" GroupName="Level2" Checked="true" Text="All"
                                    Visible="false" ForeColor="#FF3300" />
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCustOutstands" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','Yes')" />
                                <asp:Label ID="Label53" runat="server" Text="Customer Outstandings"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCRLmitExceed" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label54" runat="server" Text="Credit Limit Exceed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCRDayExceed" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label55" runat="server" Text="Credit Days Exceed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCustomerReceipt" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','Yes')" />
                                <asp:Label ID="Label56" runat="server" Text="Customer- Receipt"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCustReceiptDelete" runat="server" GroupName="Creditsales"
                                    onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label58" runat="server" Text="Customer Receipt Delete"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCustDueSales" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label59" runat="server" Text="Customer wise Due Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDuePending" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label60" runat="server" Text="Due Sales Pending"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDuePartialPending" runat="server" GroupName="Creditsales"
                                    onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label61" runat="server" Text="Due Sales Partialy Pending"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDueClosed" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label62" runat="server" Text="Due Sales Closed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDueReceipt" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label63" runat="server" Text="Due Receipt"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 5px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCRsaleZone" runat="server" GroupName="Creditsales" onclick="CheckChange('Credit','No')" />
                                <asp:Label ID="Label57" runat="server" Text="Credit Sales Zone"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="divCRDueSD" style="float: left; border: 1px solid;
                            margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoCRdueSumm" runat="server" Text="SUMMARY"
                                    Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="rdoCRdueDet" runat="server" Text="DETAIL"
                                    Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divSettlement" runat="server" style="margin-top: 10px">
                <div class="barcode-header">
                    Settlement Reports
                </div>
                <asp:Panel ID="Panel3" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="RdoSettleTeminalwise" runat="server" Checked="true" GroupName="SettlementMenu"
                                    onclick="CheckChange('Sales','Yes')" />
                                <asp:Label ID="Label22" runat="server" Text="Terminal Wise"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdoSettleCashierwise" runat="server" GroupName="SettlementMenu"
                                    onclick="CheckChange('Sales','Yes')" />
                                <asp:Label ID="Label25" runat="server" Text="Cashier wise"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdoDenomination" runat="server" GroupName="SettlementMenu" onclick="CheckChange('Sales','Yes')" />
                                <asp:Label ID="Label24" runat="server" Text="Denomination Wise"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdoSettleTerminalCashier" runat="server" GroupName="SettlementMenu"
                                    Visible="false" AutoPostBack="True" OnCheckedChanged="RdoSettleTerminalCashier_CheckedChanged" />
                                <asp:Label ID="Label23" runat="server" Text="Terminal and Cashier wise" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divpricechange" runat="server">
                <div class="barcode-header">
                    Price Change Reports
                </div>
                <asp:Panel ID="Panelpricechange" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <%--   <asp:RadioButton ID="RdoPriceChange" runat="server" GroupName="priceMenu" onclick="CheckChange('Sales','Yes')" />
                                <asp:Label ID="Label21" runat="server" Text="Price Change log"></asp:Label>--%>
                                <%--<p style="display: none">testing 1...2...3...</p>--%>
                            </div>
                        </div>
                        <%--   <div class="control-group" >
                        <div style="float: left">
                            <asp:RadioButton ID="RdoPOSpriceChange" runat="server" GroupName="priceMenu" />
                            <asp:Label ID="lblPriceCh" runat="server" Text="POS-Price Change log"></asp:Label>
                        </div>--%>
                    </div>
            </div>
            </asp:Panel>
        </div>
        <div class="container-right" id="divfilter" runat="server" style="margin-top: 10px;
            display: none;">
            <div class="barcode-header">
                Filteration
            </div>
            <asp:Panel ID="PanelFilter" runat="server">
                <div class="control-group" style="margin-top: 10px">
                    <div style="float: left">
                        <asp:Label ID="Label17" runat="server" Text="Inventory" Visible="false"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptInvDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label2" runat="server" Text="Department"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptDeptDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptCatDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label3" runat="server" Text="Brand"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptBrandDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label6" runat="server" Text="Vendor"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="rptVendorDrop" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:Label ID="Label29" runat="server" Text="Terminal"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="DropDownTerminal" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group" style="margin-bottom: 10px">
                    <div style="float: left">
                        <asp:Label ID="Label30" runat="server" Text="Cashier"></asp:Label>
                    </div>
                    <div style="float: right">
                        <asp:DropDownList ID="DropDownCashier" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="container-right" id="div1" runat="server" style="margin-top: 10px; width: auto;
            border: 1px solid #d8d9d8; width: 435px; height: 200px">
            <div class="barcode-header">
                Date filter
            </div>
            <asp:Panel ID="Panel7" runat="server">
                <div class="control-group" style="margin-top: 5px; margin-left: 15px">
                    <div style="float: left">
                        <%--<dx:ASPxDateEdit ID="txtfromDate" runat="server" EditFormatString="yyyy-MM-dd" Font-Bold="True"
                            Font-Size="Small" NullText="From Date" Enabled="False">
                        </dx:ASPxDateEdit>--%>
                        <asp:Label ID="Label78" runat="server" Text="From Date" Font-Bold="True"></asp:Label>
                        <asp:TextBox ID="txtfromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div style="float: left; margin-left: 5px">
                        <%-- <dx:ASPxDateEdit ID="txttoDate" runat="server" EditFormatString="yyyy-MM-dd" Font-Bold="True"
                            Font-Size="Small" NullText="To Date" Enabled="False">
                        </dx:ASPxDateEdit>--%>
                        <asp:Label ID="Label79" runat="server" Text="To Date" Font-Bold="True"></asp:Label>
                        <asp:TextBox ID="txttoDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                    </div>
                    <div style="float: left; margin-top: 10px">
                        <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Style="margin-top: 20px"
                            Font-Bold="True" />
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                        <ContentTemplate>
                            <div style="float: right; margin-top: 20px">
                                <asp:LinkButton ID="lnkFilter" runat="server" class="btn btn-danger" Width="100px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" OnClientClick="return showFilter()">Filter</asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" OnClick="lnkLoadReport_Click"
                                    OnClientClick="return ValidateForm()">Load Report</asp:LinkButton>&nbsp;&nbsp;
                                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                                    Style="box-shadow: 3px 3px grey;" Font-Bold="true" OnClientClick=" return Clear()">Clear</asp:LinkButton>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
        </div>
        <%--        <div class="grid-search" style="display: table; width: 100%">
            <div style="float: left; margin-top: 6px; width: 500px;">
                <asp:Label ID="Label4" runat="server" Text="Date filter"></asp:Label>&nbsp;&nbsp;
                <asp:TextBox ID="txtfromDate" runat="server" CssClass="TextboxSize-Reports" placeholder="From Date"
                    ForeColor="Black" Enabled="False"></asp:TextBox>&nbsp;&nbsp;
                <asp:TextBox ID="txttoDate" runat="server" CssClass="TextboxSize-Reports" placeholder="To Date"
                    ForeColor="Black" Enabled="False"></asp:TextBox>&nbsp;&nbsp;
                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" AutoPostBack="true" OnCheckedChanged="chkBydate_CheckedChanged"
                    Font-Bold="True" />
            </div>
            <div style="float: right">
                <asp:LinkButton ID="lnkFilter" runat="server" class="btn btn-danger" Width="100px"
                    Font-Bold="true" OnClick="lnkfilter_Click">Filter</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                    Font-Bold="true" OnClick="lnkLoadReport_Click" OnClientClick="return ValidateForm()">Load Report</asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                    Font-Bold="true" OnClick="lnkClearSearch_Click">Clear</asp:LinkButton>
            </div>
        </div>--%>
    </div>
    </div>
    <asp:HiddenField ID="HiddenfromDate" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="HiddentoDate" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>
