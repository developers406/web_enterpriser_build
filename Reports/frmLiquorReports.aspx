﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmLiquorReports.aspx.cs"
    Inherits="EnterpriserWebFinal.Reports.frmLiquorReports" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }


        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'LiquorReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "LiquorReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
    <script type="text/javascript">
        function pageLoad() {
            $("#ContentPlaceHolder1_lstCategory_chzn").css("width", "100%");
            $("#ContentPlaceHolder1_lstDepartment_chzn").css("width", "100%");
            //$("[id*=GrpfilterLiquorSales]").hide();
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
           <%-- $("[id*=txtItemCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmLiquorReports.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    return false;
                },
                // minLength: 1
            });--%>
            var vSales = $("[id*=DivLiquorReports]").is(':visible');
            $('#<%= rdoLiquorSalesReport.ClientID %> input').click(function () {
                var checkedvalue = $("#<%=rdoLiquorSalesReport.ClientID%>").find(":checked").val();
                if (vSales) {
                    
                    fncEnableAndDisableFilter(checkedvalue);
                    $('#<%= lstCategory.ClientID %>').val('');
                    $('#<%=lstCategory.ClientID %>').trigger("liszt:updated");

                    $('#<%= lstDepartment.ClientID %>').val('');
                    $('#<%=lstDepartment.ClientID %>').trigger("liszt:updated");

                    $('#<%= txtItemCode.ClientID %>').val('');

                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");

                }
            });
        }
        $(function () {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
        });

        function fncEnableAndDisableFilter(checkedvalue) {
            try {
                
                if (checkedvalue == 'Sales' || checkedvalue == 'Purchase') {
                    $("[id*=GrpfilterLiquorSales]").show();
                    $("[id*=divDepartment]").hide();
                    $("[id*=divCategory]").hide();
                    $("[id*=divItemcode]").hide();
                    $("[id*=divRadioList]").hide();
                    $("[id*=divLocation]").show();
                }
                else if (checkedvalue == 'ItemWiseSales') {
                    $("[id*=GrpfilterLiquorSales]").show();
                    $("[id*=divDepartment]").hide();
                    $("[id*=divCategory]").show();
                    $("[id*=divDepartment]").hide();
                    $("[id*=divItemcode]").hide();
                    $("[id*=divRadioList]").hide();
                }
                else if (checkedvalue == 'PurchaseAndSales') {
                    $('#<%=rdoPurchaseSalesFilter.ClientID %>').find("input[value='Report']").prop("checked", true);
                        $("[id*=GrpfilterLiquorSales]").show();
                        $("[id*=divDepartment]").hide();
                        $("[id*=divCategory]").hide();
                        $("[id*=divItemcode]").hide();
                        $("[id*=divRadioList]").show();
                    }
                    else if (checkedvalue == 'StockBalanceQtyWise' || checkedvalue == 'StockBalanceWeightWise') {
                        $("[id*=GrpfilterLiquorSales]").show();
                        $("[id*=divDepartment]").show();
                        $("[id*=divCategory]").show();
                        $("[id*=divItemcode]").show();
                        $("[id*=divRadioList]").hide();
                    }
                    else {
                        $("[id*=GrpfilterLiquorSales]").hide();
                        $("[id*=divDepartment]").hide();
                        $("[id*=divCategory]").hide();
                        $("[id*=divItemcode]").hide();
                        $("[id*=divRadioList]").hide();
                    }
        }
        catch (err) {
            fncToastError(err.message)
        }
    }

    function SetDefaultDate(FromDate, Todate) {
        FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
        if (FromDate.val() === '') {
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
        }
        if (Todate.val() === '') {
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
        }
    }

    function clearForm() {
        $("[id*=GrpfilterLiquorSales]").hide();
        $('#<%=rdoLiquorSalesReport.ClientID %>').find("input[value='Sales']").prop("checked", true);
            $('#<%=rdoPurchaseSalesFilter.ClientID %>').find("input[value='Report']").prop("checked", true);
            $("input").removeAttr('disabled');
            $('#<%= lstCategory.ClientID %>').val('');
            $('#<%=lstCategory.ClientID %>').trigger("liszt:updated");
            $('#<%= txtItemCode.ClientID %>').val('');
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            return false;
    }

        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Liquor Reports</a><i class="fa fa-angle-right"></i></li>
                <li id="ReportTitle" runat="server" class="active-page">Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>

        <div class="ziehharmonika">
            <div style="overflow: visible; display: block;" runat="server" id="DivLiquorReports">
                <h3>Liquor Reports</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoLiquorSalesReport" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Sales">Daily Sales</asp:ListItem>
                                <asp:ListItem Value="ItemWiseSales">Item Wise Sales</asp:ListItem>
                                <asp:ListItem Value="Purchase">Purchase</asp:ListItem>
                                <asp:ListItem Value="PurchaseAndSales">Purchase And Sales</asp:ListItem>
                                <%--<asp:ListItem Value="PurchaseAndSalesDynamic">Purchase And Sales Dynamic</asp:ListItem>--%>
                                <asp:ListItem Value="StockBalanceQtyWise">Stock Balance Qty Wise</asp:ListItem>
                                <asp:ListItem Value="StockBalanceWeightWise">Stock Balance Liter Wise</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-4 form_bootstyle">
                                <%--<asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />--%>
                            </div>

                            <div class="col-md-1">
                            </div>

                            <div class="col-md-7">
                                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger"
                                    Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger"
                                    Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                            </div>

                        </div>
                    </div>

                    <div id="GrpfilterLiquorSales" runat="server" class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">

                            <div class="control-group-single-res" runat="server" id="divLocation">
                                <div class="label-left">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="full_width">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divDepartment">
                                <div class="label-left">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstDepartment" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divCategory">
                                <div class="label-left">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstCategory" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>

                            <div class="control-group-single-res" runat="server" id="divItemcode">
                                <div class="label-left">
                                    <asp:Label ID="lblItemCode" runat="server" Text="Item Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemCode" MaxLength="15" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', '');" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>

                            <div class="control-group-single-res" runat="server" id="divRadioList">
                                <div>
                                    <asp:RadioButtonList ID="rdoPurchaseSalesFilter" runat="server" Class="radioboxlist">
                                        <asp:ListItem Selected="True" Value="Report">Range 1</asp:ListItem>
                                        <asp:ListItem Value="LocReport">Range 2</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
