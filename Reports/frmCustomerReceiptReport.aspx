﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmCustomerReceiptReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmCustomerReceiptReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'CustomerReceiptReport');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "CustomerReceiptReport";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">
        var txtCustomerCode;
        function pageLoad() {

            $("[id*=divCustomercode]").show();
            $("[id*=divMembertype]").hide();
            $("[id*=divLocation]").hide();
            $("[id*=DivSumDet]").show();

            var checkvalue = $("#<%= rdoCustomerReceiptCredit.ClientID %> input:radio:checked").val();
                if (checkvalue == 'CreditReceipt') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditOutstanding') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").show();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'Creditsales') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditsalesReceipt') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditCheque') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'Aging') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }
                else if (checkvalue == 'DueReceipt') {
                    $("[id*=divCustomercode]").hide();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }
                else if (checkvalue == 'ReceiptDelete') {
                    $("[id*=divCustomercode]").hide();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }

        }
        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });

        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txtFromDate.ClientID %>").focus();
            }

            if ($("#<%= txtToDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose from date';
                $("#<%= txtToDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                //JSalerts(Show);
                return false;
            }

            else {
                var StartDate = $("#<%= txtFromDate.ClientID %>").val();
                var EndDate = $("#<%= txtToDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }
                return true;
            }
        }
        function Clearall() {

            $("select").val(0);
            $("select").trigger("liszt:updated");
            $('input[type="text"]').val('');
            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                return false;
            }

            $(document).ready(function () {

                $('#<%= rdoCustomerReceiptCredit.ClientID %> input').click(function () {

                var checkvalue = $("#<%= rdoCustomerReceiptCredit.ClientID %> input:radio:checked").val();
                if (checkvalue == 'CreditReceipt') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditOutstanding') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").show();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'Creditsales') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditsalesReceipt') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").show();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'CreditCheque') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").show();
                }
                else if (checkvalue == 'Aging') {
                    $("[id*=divCustomercode]").show();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }
                else if (checkvalue == 'DueReceipt') {
                    $("[id*=divCustomercode]").hide();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }
                else if (checkvalue == 'ReceiptDelete') {
                    $("[id*=divCustomercode]").hide();
                    $("[id*=divMembertype]").hide();
                    $("[id*=divLocation]").hide();
                    $("[id*=DivSumDet]").hide();
                }

                $('#ContentPlaceHolder1_CustomerReportFilterUserControl_txtLocation').val('');//aalayam 04052018
                //$('#ContentPlaceHolder1_CustomerReportFilterUserControl_ddlLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_CustomerReportFilterUserControl_txtMemberType').val('');
                //$('#ContentPlaceHolder1_CustomerReportFilterUserControl_ddlMemberType').trigger("liszt:updated");
                
                $('#ContentPlaceHolder1_CustomerReportFilterUserControl_txtCustomer').val('');
                //$('#ContentPlaceHolder1_CustomerReportFilterUserControl_txtCustomer').trigger("liszt:updated");//

                });             

        });

    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <div class="main-container">
        <div class="breadcrumbs">
            <ul> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul></div></div>
             

    <div class="ziehharmonika">
        <div style="overflow: visible; display: block;" runat="server" id="DivVoucherSettlement">
            <h3>Customer Receipt Reports</h3>
            <div class="row">
                <div class="col-md-3" style="margin-left: 20px">
                    <div>
                        <asp:RadioButtonList ID="rdoCustomerReceiptCredit" runat="server" Class="radioboxlist">
                            <asp:ListItem Selected="True" Value="CreditReceipt">Credit Customer Receipt</asp:ListItem>
                            <asp:ListItem Value="CreditOutstanding">Credit Customer Outstanding</asp:ListItem>
                            <asp:ListItem Value="Creditsales">Credit Sales</asp:ListItem>
                           <%-- <asp:ListItem Value="CreditsalesReceipt">Credit Sales and Receipt</asp:ListItem>--%>
                            <asp:ListItem Value="CreditCheque">Credit-Cheque Payment</asp:ListItem>
                            <asp:ListItem Value="Aging">Customer-Wise Aging</asp:ListItem>
                            <asp:ListItem Value="DueReceipt">Due Receipt</asp:ListItem>
                            <asp:ListItem Value="ReceiptDelete">Receipt Delete</asp:ListItem>
                            <%--<asp:ListItem Value="Outletsales">Vehicle-Wise Outlet Sales</asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                    <div class="barcode-header">
                        Date Filter
                    </div>
                    <div class="small-box bg-aqua">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="col-md-6 form_bootstyle">
                                    <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                </div>
                                <div class="col-md-6 form_bootstyle">
                                    <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                </div>
                            </div>
                           
                            <div id="DivSumDet" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                                <div class="col-md-6 form_bootstyle">
                                    <asp:RadioButton ID="rdoSummary" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Summary" Checked="true" />
                                </div>
                                <div class="col-md-6 form_bootstyle">
                                    <asp:RadioButton ID="rdoDetail" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Detail" />
                                </div>
                            </div>
                             <div class="col-md-12" style="margin-top: 15px">
                                <div class="col-md-3 form_bootstyle">
                                    <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlistgreen" Checked="true" Font-Bold="True" />
                                </div>
                                <div class="col-md-5">
                                   <%--<asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary" Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                </div>
                                <div class="col-md-2">
                                     <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 form_bootstyle_border">
                    <div class="report-header-filter">
                        Filtrations
                    </div>
                    <div class="small-box bg-aqua">
                        <ups:ReportFilterUserControl runat="server" ID="CustomerReportFilterUserControl"
                            EnablelocationDropDown="true"
                            EnableCustomerTextbox="true"
                            EnableMemberTypeDropDown="true" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidVendorName" runat="server" Value="" />
</asp:Content>
