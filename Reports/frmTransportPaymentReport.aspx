﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmTransportPaymentReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmTransportPaymentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1197px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'TransportPaymentReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "TransportPaymentReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>



    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>

    <script type="text/javascript">
        function pageLoad() {   
            fncDatePicker();
        }
        function fncDatePicker() {
            if ($('#<%=txtFromDate.ClientID %>').val() == "")
                $('#<%=txtFromDate.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" }).datepicker("setDate", "0");
            else
                $('#<%=txtFromDate.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });

            if ($('#<%=txtToDate.ClientID %>').val() == "")
                $('#<%=txtToDate.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" }).datepicker("setDate", "0");
            else
                $('#<%=txtToDate.ClientID %>').datepicker({ dateFormat: "dd-mm-yy" });

        }
        function fncSetValue() {
            if (SearchTableName == "TransportPay") {
                var splitDescription = Description.split('-');
                $('#<%=hidTransportCode.ClientID %>').val($.trim(splitDescription[2]));
            }
            else if (SearchTableName == "Vendor") {
                $('#<%=txtSupplier.ClientID %>').val(Description);
                $('#<%=hidSupplierCode.ClientID %>').val(Code);
            }
        }

        function fncClear() {
            $(':input').val('');
            fncDatePicker();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transport Payment Reports</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <div style="overflow: visible; display: block;" runat="server">
                <h3 id="VendorTopHead">Transport Payment Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div>
                            <asp:RadioButtonList ID="rdoGoods" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Summary">Goods Receive Summary</asp:ListItem>
                                <asp:ListItem Value="Supplier">Goods Receive Supplier Wise</asp:ListItem>
                                <asp:ListItem Value="Package">Goods Receive Package Wise</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-5" id="divfilter" runat="server">
                        <div class="barcode-header">
                            Filteration
                        </div>
                        <asp:Panel ID="PanelFilter" runat="server">
                            <div class="control-group" id="divTRansport">
                                <div style="float: left">
                                    <asp:Label ID="Label11" runat="server" Text="Transport"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtTransport" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'TransportPay', 'txtTransport', 'txtSupplier');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divSupplier">
                                <div style="float: left">
                                    <asp:Label ID="Label29" runat="server" Text="Supplier"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtSupplier" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtSupplier', 'txtpaType');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" id="divPaytype">
                                <div style="float: left">
                                    <asp:Label ID="Label51" runat="server" Text="Pay Type"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtpaType" runat="server" CssClass="form-control-res"
                                        onkeydown="return fncShowSearchDialogCommon(event, 'PayTypeTran', 'txtpaType', 'txtLRNo');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="margin-bottom: 20px" id="divLrNo">
                                <div style="float: left">
                                    <asp:Label ID="Label30" runat="server" Text="LR No"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtLRNo" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group" style="margin-top: -20px" id="divLRDate">
                                <div style="float: left">
                                    <asp:Label ID="Label12" runat="server" Text="LR Date"></asp:Label>
                                </div>
                                <div style="float: right">
                                    <asp:TextBox ID="txtLRDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div id="divVendorDate" class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label8" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div id="divVendorPayFilter" class="col-md-12" style="margin-top: 15px; display: none;">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoSummery" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="SUMMARY" Checked="true" />
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:RadioButton ID="rdoDetail" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu1" Text="DETAIL" />
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-4 form_bootstyle" id="chByDate">
                                        <asp:CheckBox ID="chkBydateVendor" runat="server" Text="By Date" Class="radioboxlist"
                                            Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-4 form_bootstyle" id="chBasedon">
                                        <asp:CheckBox ID="chLrDate" runat="server" Text="LR Date" Class="radioboxlist" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="button-contol">
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkAdd" runat="server" class="button-red" OnClick="lnkReport_Click" ><i class="icon-play"></i>Load</asp:LinkButton>
                                        </div>
                                        <div class="control-button">
                                            <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="fncClear();return false;">
                            <i class="icon-play"></i>Clear</asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidSupplierCode" runat="server" />
        <asp:HiddenField ID="hidTransportCode" runat="server" />
    </div>
</asp:Content>
