﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmLocationWiseSales.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmLocationWiseSales" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }


    </style>

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'LocationWiseSale');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "LocationWiseSale";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">

        var ctrltxtInventory;

        function pageLoad() {

            $("[id*=divFilter]").hide();

            //$('.ziehharmonika').ziehharmonika({
            //    collapsible: false,
            //    arrow: true,
            //    scroll: true,
            //    prefix: '★',//'♫'//,
            //    collapseIcons: {
            //        opened: ' ',
            //        closed: ' '
            //    }
            //});

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'});

            var checkvalued = $("#<%=rdoLocationWise.ClientID%>").find(":checked").val();

            if (checkvalued == 'Location') {
                $("[id*=GroupBy]").show();
                $("[id*=chkBydate]").next().show();
                $("[id*=chkBydate]").show();
            }
            else if (checkvalued == 'Department') {
                $("[id*=GroupBy]").show();
                $("[id*=chkBydate]").show();
                $("[id*=chkBydate]").next().show();
            }
            else if (checkvalued == 'Category') {
                $("[id*=GroupBy]").show();
                $("[id*=chkBydate]").hide();
                $("[id*=chkBydate]").next().hide();
            }
            else if (checkvalued == 'Brand') {
                $("[id*=GroupBy]").show();
                $("[id*=chkBydate]").hide();
                $("[id*=chkBydate]").next().hide();
            }
            else if (checkvalued == 'Class') {
                $("[id*=GroupBy]").show();
                $("[id*=chkBydate]").next().hide();
            }
            else if (checkvalued == 'Item') {
                $("[id*=GroupBy]").hide();
                $("[id*=chkBydate]").hide();
                $("[id*=chkBydate]").next().hide();
            }
            else if (checkvalued == 'Totalsales') {
                $("[id*=GroupBy]").hide();
                $("[id*=chkBydate]").show();
                $("[id*=chkBydate]").next().show(); 
            }
            else if (checkvalued == 'LocationwiseStockSales') {
                $("[id*=GroupBy]").hide();
                $("[id*=chkBydate]").show();
                $("[id*=chkBydate]").next().show(); 
            }
            else {
                $("[id*=GroupBy]").hide();
                $("[id*=chkBydate]").hide();
                $("[id*=chkBydate]").next().hide();
            }

            if (checkvalued == 'Location' || checkvalued == 'Totalsales' || checkvalued == 'Department' || checkvalued == 'Category' || checkvalued == 'Brand' || checkvalued == 'Class')//aalayam 03052018
            {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                $('#divLocation').show();
                $('#ContentPlaceHolder1_divStock').hide();
            }
            else if (checkvalued == 'LocationwiseStockSales')//sankar17092019
            {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                $('#divLocation').show();
                $('#ContentPlaceHolder1_divStock').show();
            }
            else {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_divBatchNo').show();
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                $('#divLocation').show();
                $('#ContentPlaceHolder1_divStock').hide();
            }//

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=divfilter]").attr("disabled", "disabled");
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=divfilter]").removeAttr("disabled");
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });


        }

        function clearForm() {

            try {
                $('input[type="text"]').val('');
                $("[id$=txtInventory]").val('');
                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }

        $(document).ready(function () {


            $('#<%= rdoLocationWise.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoLocationWise.ClientID %> input:radio:checked").val();
                if (checkvalue == 'Location') {
                    $("[id*=GroupBy]").show();
                    $("[id*=chkBydate]").next().show();
                    $("[id*=chkBydate]").show();
                }
                else if (checkvalue == 'Department') {
                    $("[id*=GroupBy]").show();
                    $("[id*=chkBydate]").show();
                    $("[id*=chkBydate]").next().show();
                }
                else if (checkvalue == 'Category') {
                    $("[id*=GroupBy]").show();
                    //$("[id*=chkBydate]").hide();
                    //$("[id*=chkBydate]").next().hide();
                }
                else if (checkvalue == 'Brand') {
                    $("[id*=GroupBy]").show();
                    //$("[id*=chkBydate]").hide();
                    //$("[id*=chkBydate]").next().hide();
                }
                else if (checkvalue == 'Class') {
                    $("[id*=GroupBy]").show();
                    //$("[id*=chkBydate]").next().hide();
                }
                else if (checkvalue == 'Item') {
                    $("[id*=GroupBy]").hide();
                    //$("[id*=chkBydate]").hide();
                    //$("[id*=chkBydate]").next().hide();
                }
                else if (checkvalue == 'Totalsales') {
                    $("[id*=GroupBy]").hide();
                    //$("[id*=chkBydate]").hide();
                    //$("[id*=chkBydate]").next().hide();LocationwiseStockSales
                }
                else if (checkvalue == 'LocationwiseStockSales') {
                    $("[id*=GroupBy]").hide();
                    //$("[id*=chkBydate]").hide();
                    //$("[id*=chkBydate]").next().hide();
                }

                if (checkvalue == 'Location' || checkvalue == 'Totalsales' || checkvalue == 'Totalsales' || checkvalue == 'Department' || checkvalue == 'Category' || checkvalue == 'Brand' || checkvalue == 'Class')//aalayam 03052018
                {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                    $('#divLocation').show();
                    $('#ContentPlaceHolder1_divStock').hide();
                }
                else if (checkvalue == 'LocationwiseStockSales')//sankar17092019
                {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                    $('#divLocation').show();
                    $('#ContentPlaceHolder1_divStock').show();
                }
                else {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    //$('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocationDropDown').show();
                    $('#divLocation').show();
                    $('#ContentPlaceHolder1_divStock').hide();
                }//
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtInventory').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_txtInventory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtDepartment').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlDept').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtCategory').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlCategory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtBrand').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlBrand').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtVendor').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlVendor').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtLocation').val('');
                //$('#ContentPlaceHolder1_ddLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_txtBatchNo').val('');
            });
        });

        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


        $(function () {

            try {

                $('#<%=chkBydate.ClientID%>').change(function () {
                    if (this.checked) {
                        $("[id*=divfilter]").attr("disabled", "disabled");
                        $("[id*=txtFromDate]").removeAttr("disabled");
                        $("[id*=txtToDate]").removeAttr("disabled");
                    }
                    else {
                        $("[id*=divfilter]").removeAttr("disabled");
                        $("[id*=txtFromDate]").attr("disabled", "disabled");
                        $("[id*=txtToDate]").attr("disabled", "disabled");
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        });

        function showFilter() {

            if ($("[id*=divFilter]").is(':hidden')) {
                $("[id*=divFilter]").show();
            }
            else {
                $("[id*=divFilter]").hide();
            }

            return false;

        }
        function JSalerts(Show) {
            swal({
                title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }

        function DateCheck() {
            var StartDate = $("#<%= txtFromDate.ClientID %>").val();
            var EndDate = $("#<%= txtToDate.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Location Wise Sales Reports </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <h3>Location Wise Sales Reports</h3>
            <div style="overflow: visible; display: block;">
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoLocationWise" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Location">Location Wise Sales</asp:ListItem>
                                <asp:ListItem Value="Department">Department wise Sales</asp:ListItem>
                                <asp:ListItem Value="Category">Category wise Sales</asp:ListItem>
                                <asp:ListItem Value="Brand">Brand wise Sales</asp:ListItem>
                                <asp:ListItem Value="Class">Class wise Sales</asp:ListItem>
                                <asp:ListItem Value="Item">Item wise Sales</asp:ListItem>
                                <asp:ListItem Value="Totalsales">Location wise Sales - All Values</asp:ListItem>
                                <asp:ListItem Value="LocationwiseStockSales">Location wise Stock & Sales Report</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="GroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoDate" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Date wise"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoMonth" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Month wise" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:RadioButton ID="rdoNone" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="None" />
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                                <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                            </div>
                            <div class="col-md-4">
                                <%-- <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12 form_bootstyle" runat="server" id="divLocation">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                         <div class="col-md-12 form_bootstyle" runat="server"  id="divStock" >
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label90" runat="server" Text="Group By"></asp:Label>
                                </div>
                                   <div class="col-md-8 form_bootstyle">
                                    <asp:DropDownList ID="ddlReport" runat="server" CssClass="form-control-res" Width="100%">                                      
                                         <asp:ListItem value="Item">Item wise </asp:ListItem>
                                      <asp:ListItem value="Dept">Dept wise </asp:ListItem>
                                     <asp:ListItem value="Catg">Catg Wise </asp:ListItem>
                                        <asp:ListItem value="Sub">Sub Catg Wise</asp:ListItem>
                                     <asp:ListItem value="Brand"> Brand Wise</asp:ListItem>
                                      <asp:ListItem value="Class">Class Wise</asp:ListItem>
                                     <asp:ListItem value="Vendor">Vendor Wise</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <ups:ReportFilterUserControl runat="server" ID="SalesReportFilterUserControl"
                                EnablelocationDropDown="false"
                                EnableVendorDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableBrandDropDown="true"
                                EnableInventoryTextBox="true" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBatchNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddBinNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
