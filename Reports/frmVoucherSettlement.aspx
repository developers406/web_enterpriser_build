﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmVoucherSettlement.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmVoucherSettlement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
       <script type="text/javascript">
           var d1;
           var d2;
           var Opendate;
           var dur;
           function fncGetUrl() {
               fncSaveHelpVideoDetail('', '', 'VoucherSettlement');
           }
           function fncOpenvideo() {

               document.getElementById("ifHelpVideo").src = HelpVideoUrl;

               var Mode = "VoucherSettlement";
               var d = new Date($.now());
               Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
               d1 = new Date($.now()).getTime();



               $("#dialog-Open").dialog({
                   autoOpen: true,
                   resizable: false,
                   height: "auto",
                   width: 1093,
                   modal: true,
                   dialogClass: "no-close",
                   buttons: {
                       Close: function () {
                           $(this).dialog("destroy");
                           var d = new Date($.now());
                           var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                           d2 = new Date($.now()).getTime();
                           var Diff = Math.floor((d2 - d1) / 1000);
                           //alert(Diff);
                           if (Diff >= 60) {
                               fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                           }

                       }
                   }
               });
           }


       </script>


    <script type="text/javascript">

        function pageLoad() {
            $('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }

        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>")
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>
    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Voucher Settlement </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <h3>Voucher Settlement Reports</h3>
            <div style="overflow: visible; display: block;">
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <asp:RadioButtonList ID="rdoVoucherSettlement" CssClass="radioboxlist" runat="server">
                            <asp:ListItem Selected="True" Value="DateWise">Date Wise Settlement</asp:ListItem>
                            <asp:ListItem Value="Consolidated">Consolidated Settlement</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-5" style="margin-left: 20px">
                  
                    </div>
                    <div class="col-md-3 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="GroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:Label ID="Label1" runat="server" Text="Location"></asp:Label>
                                <%--<asp:DropDownList ID="LocationDropdown" runat="server" CssClass="form-control">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtVoucher');"></asp:TextBox>
                            </div>                            
                        </div>
                        <div id="GroupByvendor" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:Label ID="Label3" runat="server" Text="Voucher Type"></asp:Label>
                                <%--<asp:DropDownList ID="VoucherDropdown" runat="server" CssClass="form-control">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVoucher" runat="server" CssClass="form-control" onkeydown="return fncShowSearchDialogCommon(event, 'Voucher', 'txtVoucher', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                            </div>
                            <div class="col-md-4">
                                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px" Text='<%$ Resources:LabelCaption,btnClear %>'
                                Font-Bold="true"></asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px" Text='<%$ Resources:LabelCaption,btn_report %>'
                                Font-Bold="true" OnClick="lnkLoadReport_Click"> </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</asp:Content>
