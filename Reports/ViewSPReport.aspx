﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" 
    CodeBehind="ViewSPReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.ViewSPReport" %>

<%--<%@ Page  Title="" Language="C#" AutoEventWireup="true"  EnableEventValidation="true"  CodeFile="View.aspx.cs" Inherits="View" %>--%>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
     <style type="text/css" media="print">
    @page 
    {
        size:  auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }

    html
    {
        background-color: #FFFFFF; 
        margin: 0px;  /* this affects the margin on the html before sending to printer */
        page-break-after: avoid;
        page-break-before: avoid;
    }

    body
    {
        background-color: #FFFFFF; 
        margin: 0px; /* margin you want for the content */
        page-break-after: avoid;
        page-break-before: avoid;
    }  
 .GridDetails:last-child {
     page-break-after: auto;
}
    </style>
    <script type="text/javascript" language="Javascript">
       
        $(function () {
            $('#divImage').click(function () {
                alert($("#rptName").val());
            }); 
            if ($.session.get('height') != null && $.session.get('height') != "") {
                var height = $.session.get('height');
                height = parseFloat(height) - 50;
                $('.EnableScroll').css('height', '' + height + "px" + '');
            }
        });
        function fncPrintClick() {
            $('.crtoolbar').css('display', 'none'); 
            $('#footerview').css('display', 'none');
            $('#btnPrint').css('display', 'none'); 
            window.print(); 
            $('.crtoolbar').css('display', 'block'); 
            $('#footerview').css('display', 'block');
            $('#btnPrint').css('display', 'block');
         }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="navigation" style="display: block; max-width: 100%; width: 100%; height: 50px; margin-top: 4px;">
        <div id="divImage" class="container-fluid">
            <img id="imgRpt" src="../images/CompanyLogo.png" style="display: block; max-width: 100%; width: 240px; height: 50px; margin-top: 4px;" />
        </div> 
    </div>
     <%--<input type="button" value="Print" id="btnPrint" style="margin: 10px 0px -10px 0px; border: 1px solid #3b6f7b!important;" onclick="fncPrintClick();return false;" /> --%>
   <div class="GridDetails EnableScroll" id="dvReport" style="width: 100%;margin-top: 10px;"> 
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" Width="100%">
            <ContentTemplate>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" EnableDatabaseLogonPrompt="False"
                    GroupTreeImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/tree/"
                    ToolbarImagesFolderUrl="/aspnet_client/system_web/2_0_50727/CrystalReportWebFormViewer4/images/toolbar/"
                    DisplayGroupTree="False" Width="100%" BestFitPage="True" />
                <asp:Label ID="Label5" runat="server" Text="Label" Visible="False"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="CrystalReportViewer1" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:HiddenField ID="rptName" runat="server" ClientIDMode="Static" Value="" /> 
</asp:Content>
