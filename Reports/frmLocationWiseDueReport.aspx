﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmLocationWiseDueReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmLocationWiseDueReport" %>

<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
      <script type="text/javascript">
          var d1;
          var d2;
          var Opendate;
          var dur;
          function fncGetUrl() {
              fncSaveHelpVideoDetail('', '', 'LocationWiseDueReport');
          }
          function fncOpenvideo() {

              document.getElementById("ifHelpVideo").src = HelpVideoUrl;

              var Mode = "LocationWiseDueReport";
              var d = new Date($.now());
              Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
              d1 = new Date($.now()).getTime();



              $("#dialog-Open").dialog({
                  autoOpen: true,
                  resizable: false,
                  height: "auto",
                  width: 1093,
                  modal: true,
                  dialogClass: "no-close",
                  buttons: {
                      Close: function () {
                          $(this).dialog("destroy");
                          var d = new Date($.now());
                          var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                          d2 = new Date($.now()).getTime();
                          var Diff = Math.floor((d2 - d1) / 1000);
                          //alert(Diff);
                          if (Diff >= 60) {
                              fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                          }

                      }
                  }
              });
          }


      </script>


    <script type="text/javascript">

        var ctrltxtInventory;

        function pageLoad() {

            //$("[id*=divFilter]").hide();

            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

              var checkedvalue = $("#<%=rdoLocationWise.ClientID%>").find(":checked").val();
            ClearFilter();


            ClearTextBoxFilter();
                if (checkedvalue == "POandGIDLog"  || checkedvalue == "SettlementDiscrepancyReport" || checkedvalue == "VoucharClaimReceipt") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').hide();
                }
                else if (checkedvalue == "ConsolidatedSettlementLocationWise") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else if (checkedvalue == "DueReceipt" || checkedvalue == "PriceChangeReportPOS" || checkedvalue == "DiscountReportPOS") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#GroupBy').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                   
                }
                else if (checkedvalue == "ConsolidatedSettlementDateWise") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else {
                    $('#Filteration').show();
                    $('#SalesReportFilterUserControl').show();
                    $('#PaymentReceipt').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divPaymentType').show();
                    $('#GroupBy').hide();
                     
                }

            $("input:radio").change(function () {
                ClearFilter();
                ClearTextBoxFilter()
                var checkedvalue = $("#<%=rdoLocationWise.ClientID%>").find(":checked").val();

                if (checkedvalue == "POandGIDLog" || checkedvalue == "SettlementDiscrepancyReport" || checkedvalue == "VoucharClaimReceipt") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').hide();
                }
                else if (checkedvalue == "ConsolidatedSettlementLocationWise") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else if (checkedvalue == "DueReceipt" || checkedvalue == "PriceChangeReportPOS" || checkedvalue == "DiscountReportPOS") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#GroupBy').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                   
                }
                else if (checkedvalue == "ConsolidatedSettlementDateWise") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else {
                    $('#Filteration').show();
                    $('#SalesReportFilterUserControl').show();
                    $('#PaymentReceipt').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divPaymentType').show();
                    $('#GroupBy').hide();
                }
            });

            //$("input:radio").change(function () {
            //    var radioValue = $("input[name='rbtPayment']:checked").val();
            //    var radioValue =$("input[name='rbtPayment']:checked").val();
            //    console.log(radioValue);
            //});

        }

        function LoadFilter()
        {
            ClearTextBoxFilter();
           
                $("input:radio").change(function () {
                var checkedvalue = $("#<%=rdoLocationWise.ClientID%>").find(":checked").val();

                if (checkedvalue == "POandGIDLog"  || checkedvalue == "SettlementDiscrepancyReport" || checkedvalue == "VoucharClaimReceipt") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').hide();
                }
                else if (checkedvalue == "ConsolidatedSettlementLocationWise") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else if (checkedvalue == "DueReceipt" || checkedvalue == "PriceChangeReportPOS" || checkedvalue == "DiscountReportPOS") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#GroupBy').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                   
                }
                else if (checkedvalue == "ConsolidatedSettlementDateWise") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else {
                    $('#Filteration').show();
                    $('#SalesReportFilterUserControl').show();
                    $('#PaymentReceipt').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divPaymentType').show();
                    $('#GroupBy').hide();
                }
            });

        }

        function ClearTextBoxFilter()
        {
            $('#ContentPlaceHolder1_txtLocation').val('');
            $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtVendor').val('');
            $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtTerminal').val('');
            $('#ContentPlaceHolder1_txtPaymentType').val('');
        }

        function ClearFilter()
        {
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $('input:checkbox').removeAttr('checked');
            $('input:checkbox').removeAttr('checked');
        }
        function clearForm() {
            try {
                 
                $('input[type="text"]').val('');

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                 $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                 if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                     $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                     $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
                $("select").val(0);
                $("select").trigger("liszt:updated");
                $('input:checkbox').removeAttr('checked');
                $('input:checkbox').removeAttr('checked');
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
            return false;
        }


        $(function () {
            try {


                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txtToDate.ClientID %>").val() === '') {
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
        catch (err) {
            alert(err.Message);
        }
    });




        function showFilter() {

            if ($("[id*=divFilter]").is(':hidden')) {
                $("[id*=divFilter]").show();
            }
            else {
                $("[id*=divFilter]").hide();
            }

            return false;

        }
        function JSalerts(Show) {
            swal({
                title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }

        function DateCheck() {
            var StartDate = $("#<%= txtFromDate.ClientID %>").val();
            var EndDate = $("#<%= txtToDate.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        $(document).ready(function () {

            $('#<%= rbtPaymentandReceipt.ClientID %>').on('change', function () {
                $('#divChkVendor').hide();
            });

            $('#<%= rbtReceipt.ClientID %>').on('change', function () {
                $('#divChkVendor').hide();
            });
            $('#<%= rbtPayment.ClientID %>').on('change', function () {
                $('#divChkVendor').show();
            });

            $("input:radio").change(function () {
                var checkedvalue = $("#<%=rdoLocationWise.ClientID%>").find(":checked").val();

                if (checkedvalue == "POandGIDLog"|| checkedvalue == "PriceChangeReportPOS" || checkedvalue == "DiscountReportPOS" || checkedvalue == "SettlementDiscrepancyReport" || checkedvalue == "VoucharClaimReceipt") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').hide();
                }
                else if (checkedvalue == "ConsolidatedSettlementLocationWise") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else if (checkedvalue == "DueReceipt") {
                    $('#Filteration').show();
                    $('#txtLocation').show();
                    $('#GroupBy').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_divPaymentType').hide();
                    $('#PaymentReceipt').hide();
                   
                }
                else if (checkedvalue == "ConsolidatedSettlementDateWise") {
                    $('#Filteration').hide();
                    $('#PaymentReceipt').hide();
                    $('#GroupBy').show();
                }
                else {
                    $('#Filteration').show();
                    $('#SalesReportFilterUserControl').show();
                    $('#PaymentReceipt').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_divPaymentType').show();
                    $('#GroupBy').show();
                }
            });

        });




        <%-- $('#<%= rbtPaymentandReciept.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rbtPaymentandReciept.ClientID %> input:radio:checked").val();
                   if (checkvalue == 'Payment') {
                       $('#ContentPlaceHolder1_chkVendor').show();
                }
                else if ($(this).is(":not(:checked)")) {
                    $('#ContentPlaceHolder1_chkVendor').hide();
                }
            });--%>
       

    </script>



    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Location Due Reports </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="ziehharmonika">
            <h3>Location Due Reports</h3>
            <div style="overflow: visible; display: block;">
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoLocationWise" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="PaymentReceipt">Payment Receipt</asp:ListItem>
                                <asp:ListItem Value="ConsolidatedSettlementLocationWise">Consolidated Settlement LocationWise</asp:ListItem>
                                <asp:ListItem Value="ConsolidatedSettlementDateWise">Consolidated Settlement DateWise</asp:ListItem>
                                <asp:ListItem Value="POandGIDLog">PO and GID Cost Change log</asp:ListItem>
                                <asp:ListItem Value="DueReceipt">Due Receipt</asp:ListItem>
                                <asp:ListItem Value="PriceChangeReportPOS">Price Change Report in POS</asp:ListItem>
                                <asp:ListItem Value="DiscountReportPOS">Discount Report in POS</asp:ListItem>
                                <asp:ListItem Value="SettlementDiscrepancyReport">Settlement Discrepancy Report</asp:ListItem>
                                <asp:ListItem Value="VoucharClaimReceipt">Voucher Claim and Receipt</asp:ListItem>

                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="GroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">

                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoSummary"    Checked="True" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Summary" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="rdoDetail" Class="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Detail" />
                            </div>
                        </div>
                        <div id="PaymentReceipt" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">

                            <div class="col-md-6">
                                <asp:RadioButton ID="rbtPayment"   Checked="True"  Class="radioboxlistgreen" runat="server" GroupName="PaymentReceipt" Text="Payment" />
                            </div>
                            <div class="col-md-6">
                                <asp:RadioButton ID="rbtReceipt" Class="radioboxlistgreen" runat="server" GroupName="PaymentReceipt" Text="Receipt" />
                            </div>

                            <div class="col-md-6 ">
                                <asp:RadioButton ID="rbtPaymentandReceipt" Class="radioboxlistgreen" runat="server" GroupName="PaymentReceipt" Text="PaymentandReceipt" />
                            </div>
                            <div class="col-md-6" id="divChkVendor">
                                <asp:CheckBox ID="chkVendor" Class="radioboxlistgreen" runat="server" GroupName="PaymentReceipt" Text="Vendor" />
                            </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px">
                            <div class="col-md-3 form_bootstyle">
                            </div>
                            <div class="col-md-4">
                                <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                    Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>
                            </div>
                            <div class="col-md-3">
                                <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report </asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" id="Filteration">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12 form_bootstyle" runat="server" id="divLocation">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                            <ups:ReportFilterUserControl runat="server" ID="SalesReportFilterUserControl"
                                EnablelocationDropDown="false"
                                EnableTerminalDropDown="true"
                                EnableVendorDropDown="true" />
                            <div class="col-md-12 form_bootstyle" runat="server" id="divPaymentType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label1" runat="server" Text="PaymentType"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtPaymentType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Payment', 'txtPaymentType', '');"></asp:TextBox>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
