﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAgingReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmAgingReport1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link type="text/css" href="../css/autopo.css" rel="stylesheet" />
    <style type="text/css">
        .grdLoadNew td:nth-child(1), .grdLoadNew th:nth-child(1) {
            min-width: 50px;
            max-width: 50px;
            text-align: center !important;
        }

        .grdLoadNew td:nth-child(2), .grdLoadNew th:nth-child(2) {
            min-width: 150px;
            max-width: 150px;
            text-align: left !important;
        }

        .grdLoadNew td:nth-child(3), .grdLoadNew th:nth-child(3) {
            min-width: 425px;
            max-width: 425px;
            text-align: left !important;
        }

        .grdLoadNew td:nth-child(4), .grdLoadNew th:nth-child(4) {
           min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .grdLoadNew td:nth-child(5), .grdLoadNew th:nth-child(5) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .grdLoadNew td:nth-child(6), .grdLoadNew th:nth-child(6) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }

        .grdLoadNew td:nth-child(7), .grdLoadNew th:nth-child(7) {
            min-width: 100px;
            max-width: 100px;
            text-align: center !important;
        }
         
    </style>
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AutoPO');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AutoPO";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }

    </script>
    <script type="text/javascript">
        $(function () {
            $("#spLeftArrow").removeClass('ui-icon-circle-arrow-s');
            $("#spLeftArrow").addClass('ui-icon-circle-arrow-e');
            var icons = {
                header: "ui-icon-circle-arrow-e",
                activeHeader: "ui-icon-circle-arrow-s"
            };
            $("#accordion").accordion({
                collapsible: true,
                icons: icons
            });
            $("#toggle").button().on("click", function () {
                if ($("#accordion").accordion("option", "icons")) {
                    $("#accordion").accordion("option", "icons", null);
                } else {
                    $("#accordion").accordion("option", "icons", icons);
                }
            });

            $("#spLeftArrow").removeClass('ui-icon-circle-arrow-s');
            $("#spLeftArrow").addClass('ui-icon-circle-arrow-e');
             
        });
        function fncSetValue() {
            try {
                if (SearchTableName == "Inventory") {
                    $('#<%=txtItemCode.ClientID %>').val($.trim(Code));
                     $('#<%=txtItemName.ClientID %>').val($.trim(Description));
                 }
             }
             catch (err) {
                 ShowPopupMessageBox(err.message);
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Stock</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Textile Aging Report</li>
                <li><i class="fa fa-question-circle" style="cursor: pointer; font-size: 19px;" onclick="fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-price">
            <div class="container-left-price" id="pnlFilter" runat="server" style="width: 22% !important">
                <div id="accordion">
                    <h1>Filtration 
                            <span id="spLeftArrow" onclick="return fncHideFilter();" title="Click to Hide Filter" style="margin-left: 115px;" class="ui-accordion-header-icon ui-icon ui-icon-circle-arrow-e"></span></h1>
                    <div>
                        <div class="control-group-single-res FilterSearch display_none">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="Location"></asp:Label>

                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', 'txtVendor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label3" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtSubCategory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSubCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubCategory', 'txtSubCategory', 'txtMerchendise');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Merchendise"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtMerchendise" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Merchandise', 'txtMerchendise', 'txtManufacture');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="Manufature"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtManufacture" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Manafacture', 'txtManufacture', 'txtFloor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label20" runat="server" Text="Floor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFloor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Floor', 'txtFloor', 'txtSection');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtBin');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text="Bin"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtBin" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBin', 'txtShelf');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Shelf"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtShelf" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Shelf', 'txtShelf', 'txtClass');"></asp:TextBox>
                            </div>
                        </div>

                        
                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label14" runat="server" Text="Class"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtItemCode');"></asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label16" runat="server" Text="Item Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', 'txtItemName');">
                                </asp:TextBox>
                            </div>
                        </div>

                        <div class="control-group-single-res FilterSearch">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="Item Name"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control-res">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-single-res FilterSearch display_none" >
                            <div class="label-left">
                                <asp:Label ID="Label15" runat="server" Text="Style Code"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtStyleCode" runat="server" CssClass="form-control-res"
                                    onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtStyleCode', 'lnkFilter');">
                                </asp:TextBox>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="control-container RadioButtonFilter">
                    <div class="control-button">
                        <asp:LinkButton ID="lnkFilter" runat="server" class="button-red" OnClick ="lnkFetch_Click" ><i class="icon-play" ></i>Load Data</asp:LinkButton>
                    </div>
                    <div class="control-button">
                        <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="return clearForm();"><i class="icon-play"></i>Clear</asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="container-right-price" id="HideFilter_ContainerRight" runat="server"
                style="width: 78% !important; margin-left: 0% !important">

                <div class="GridDetails col-md-12" style="overflow-x: hidden; overflow-y: hidden;  background-color: aliceblue;"
                    id="HideFilter_GridOverFlow" runat="server">
                    <div class="grdLoadNew">
                        <table rules="all" id="tblHead" class="Payment_fixed_headers">
                            <thead>
                                <tr class="click">
                                    <th>S.No</th>
                                    <th>ItemCode</th>
                                    <th>Description</th> 
                                    <th>0-30</th>
                                    <th>30-60</th>
                                    <th>60-90</th>
                                    <th>90 Above</th>
                                </tr>
                            </thead>
                        </table>
                         <div   id="divGrd2" runat="server" style ="overflow-x: hidden;overflow-y: scroll;height:400px;">
                        <asp:GridView ID="grdItemDetails" runat="server" AutoGenerateColumns="False"
                            PageSize="14" ShowHeaderWhenEmpty="True" ShowHeader="false"  >

                            <PagerStyle CssClass="pshro_text" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                            <Columns>
                                <asp:BoundField DataField="RowNo" HeaderText="RowNo"></asp:BoundField>
                                <asp:BoundField DataField="InventoryCode" HeaderText="Item Code" ItemStyle-CssClass="left_align"></asp:BoundField>
                                <asp:BoundField DataField="Description" HeaderText="Item Name" ItemStyle-CssClass="left_align"></asp:BoundField> 
                                <asp:BoundField DataField="0-30" HeaderText="0-30" ItemStyle-CssClass="left_align" />
                                <asp:BoundField DataField="30-60" HeaderText="30-60" ItemStyle-CssClass="left_align"></asp:BoundField>
                                <asp:BoundField DataField="60-90" HeaderText="60-90" ItemStyle-CssClass="left_align"></asp:BoundField>
                                <asp:BoundField DataField="90Above" HeaderText="90 Above" ItemStyle-CssClass="left_align"></asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                PageButtonCount="5" Position="Bottom" />
                            <PagerStyle BackColor="AliceBlue" Height="30px" VerticalAlign="Bottom" HorizontalAlign="Center" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                        </asp:GridView>
                             </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
