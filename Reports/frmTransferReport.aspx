﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="frmTransferReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmTransferReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'TransferReport');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "TransferReport";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
       
            SetAutoComplete();
            var checkTransferin = $("#<%=rdoTransferIn.ClientID%>").find(":checked").val();
            if (checkTransferin == 'TransferIn' || checkTransferin == 'PartialTransfer') {
                $("table[id$=rdoTransferOut] input:radio:checked").removeAttr("checked");
            }
            var checkvalue = $("#<%= rdoTransferOut.ClientID %> input:radio:checked").val();
            if (checkvalue == 'TransferOut') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'Section') {
                $("[id*=GroupBy]").hide();
               
            }
            else if (checkvalue == 'Vehicle') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'W-O') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'O-W') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'O-O') {
                $("[id*=GroupBy]").hide();
            }
            else if (checkvalue == 'Revoked') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'NonTransfer') {
                $("[id*=GroupBy]").show();
            }
            else if (checkvalue == 'Itemwise') {
                $("[id*=GroupBy]").hide();
            }
            else if (checkvalue == 'ClassWise') {
                $("[id*=GroupBy]").hide();
            }
            else if (checkvalue == 'SubClassWise') {
                $("[id*=GroupBy]").hide();
            }
            else if (checkvalue == 'TransferOutLog') {
                $("[id*=GroupBy]").show();
            }

            var checkedvalue = $("#<%= rdoTransferIn.ClientID %> input:radio:checked").val();
            if (checkedvalue == 'TransferIn') {
                $("[id*=GroupBy]").show();
            }
            else if (checkedvalue == 'PartialTransfer') {
                $("[id*=GroupBy]").show();
            }

        }

        function SetAutoComplete() {

            $("[id$=txtInventory]").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<%=ResolveUrl("~/Reports/frmLocationWiseSales.aspx/GetInventoryDetails")%>',
                            data: "{ 'prefix': '" + request.term + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valCode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                alert(response.responseText);
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    },
                    select: function (e, i) {

                        $("[id$=txtInventory]").val(i.item.valCode)

                        //                    $("#HidenIsBatch").val($.trim(i.item.valBatch)); 

                        //                    if ($("#HidenIsBatch").val() == 'True') {
                        //                        fncGetBatchDetail_Master(i.item.valCode);
                        //                    }
                        //                    else {
                        //                        AddRow($.trim(i.item.valCode), $.trim(i.item.valDescription), $.trim(i.item.valCost), 1, $.trim(i.item.valTax), '');
                        //                    }

                        return false;
                    },
                    minLength: 0
                });

            }


            function clearForm() {

                try {
                    $('input[type="text"]').val('');
                    $("[id$=txtInventory]").val('');
                    $("select").val(0);
                    $("select").trigger("liszt:updated");
                }
                catch (err) {

                    alert(err.Message);
                    return false;
                }

                return false;
            }

        $(document).ready(function () {
            
                $('#<%= rdoTransferOut.ClientID %> input').click(function () {

                $('#<%= rdoTransferIn.ClientID %> input').removeAttr('checked');
                $("#<%= hiddReportType.ClientID %>").val('Out');
                var checkvalue = $("#<%= rdoTransferOut.ClientID %> input:radio:checked").val();
                if (checkvalue == 'TransferOut') {
                    $("[id*=GroupBy]").show();

                }
                else if (checkvalue == 'Section') {
                    $("[id*=GroupBy]").hide();
               
                }
                else if (checkvalue == 'Vehicle') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'W-O') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'O-W') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'O-O') {
                    $("[id*=GroupBy]").hide();
                }
                else if (checkvalue == 'Revoked') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'NonTransfer') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'Itemwise') {
                    $("[id*=GroupBy]").hide();
                }
                else if (checkvalue == 'ClassWise') {
                    $("[id*=GroupBy]").hide();
                }
                else if (checkvalue == 'SubClassWise') {
                    $("[id*=GroupBy]").hide();
                }
                else if (checkvalue == 'TransferOutLog') {
                    $("[id*=GroupBy]").show();
                }
            });

            $('#<%= rdoTransferIn.ClientID %> input').click(function () {

                $('#<%= rdoTransferOut.ClientID %> input').removeAttr('checked');
                $("#<%= hiddReportType.ClientID %>").val('In');
                var checkvalue = $("#<%= rdoTransferIn.ClientID %> input:radio:checked").val();
                if (checkvalue == 'TransferIn') {
                    $("[id*=GroupBy]").show();
                }
                else if (checkvalue == 'PartialTransfer') {
                    $("[id*=GroupBy]").show();
                }
            });
        });

        $(function () {

            try {

                $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
                $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

                if ($("#<%= txtFromDate.ClientID %>").val() === '') {
                    $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }

                if ($("#<%= txtToDate.ClientID %>").val() === '') {
                    $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
                }
            }
            catch (err) {
                alert(err.Message);
            }
        });


        function showFilter() {

            if ($("[id*=divFilter]").is(':hidden')) {
                $("[id*=divFilter]").show();
            }
            else {
                $("[id*=divFilter]").hide();
            }

            return false;

        }
        function JSalerts(Show) {
            swal({
                title: "Please fill the details!",
                text: Show,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                cancelButtonText: "I am not sure!",
                closeOnConfirm: true,
                closeOnCancel: true
            });
        }

        function DateCheck() {
            var StartDate = $("#<%= txtFromDate.ClientID %>").val();
            var EndDate = $("#<%= txtToDate.ClientID %>").val();
            var eDate = new Date(EndDate);
            var sDate = new Date(StartDate);
            if (StartDate != '' && EndDate != '' && sDate > eDate) {
                alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                return false;
            }
        }

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

        function fncSetValue() {
            try {
                $('#<%=hidVendorName.ClientID %>').val(Description);
            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <style type="text/css">
        input:checked + label {
            color: white;
            background: red;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Transfer Reports </li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-full">
            <div class="container-transfer">
                <div class="container-Transfer-header">
                    <div class="control-group-split">
                        <div class="control-group-left" style="width: 50%">
                            <div class="label-left">
                                <asp:Label ID="Label29" runat="server" Text="From Date" CssClass="label-invoice"></asp:Label><span
                                    class="mandatory">*</span>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right" style="width: 48%">
                            <div class="label-left">
                                <asp:Label ID="Label31" runat="server" CssClass="label-invoice" Text="To Date"></asp:Label>
                            </div>
                            <div class="label-right" style="color: Black">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-Transfer-header">
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label1" runat="server" Text="From Location" CssClass="label-invoice"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlFromLoc" runat="server" CssClass="form-control-res">
                                    <%-- <asp:ListItem Value="" Text=""></asp:ListItem>
                                    <asp:ListItem Value="HQ" Text="HQ"></asp:ListItem>
                                    <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtFromLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtFromLocation', 'txtToLocation');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label2" runat="server" CssClass="label-invoice" Text="To Location"></asp:Label>
                            </div>
                            <div class="label-right" style="color: Black">
                                <%--<asp:DropDownList ID="ddlToLoc" runat="server" CssClass="form-control-res">
                                    <%-- <asp:ListItem Value="" Text=""></asp:ListItem>
                                    <asp:ListItem Value="HQ" Text="HQ"></asp:ListItem>
                                    <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtToLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtToLocation', 'txtDepartment');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-container-detail">
                <div class="left-top-container">
                    <div class="control-group-split">
                        <div class="container-Transfer-left">
                            <div class="barcode-header-Transfer">
                                Transfer Out Reports
                            </div>
                            <asp:RadioButtonList ID="rdoTransferOut" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="TransferOut">Transfer Out</asp:ListItem>
                                <asp:ListItem Value="Section">Section wise Transfer Out</asp:ListItem>
                                <asp:ListItem Value="Vehicle">Vehicle wise Transfer Out</asp:ListItem>
                                <%--<asp:ListItem Value="W-O"> WareHouse To Outlet</asp:ListItem>
                                <asp:ListItem Value="O-W">Outlet To WareHouse</asp:ListItem>
                                <asp:ListItem Value="O-O">Outlet To Outlet</asp:ListItem>--%>
                                <asp:ListItem Value="Revoked">Revoked Transfer</asp:ListItem>
                                <asp:ListItem Value="NonTransfer">Not Transfered Inventory</asp:ListItem>
                                <asp:ListItem Value="Itemwise">Item Wise TransferOut</asp:ListItem>
                                <asp:ListItem Value="ClassWise">Class Wise TransferOut</asp:ListItem>
                                <asp:ListItem Value="SubClassWise">SubClass Wise TransferOut</asp:ListItem>
                                <asp:ListItem Value="TransferOutLog">Transfer Out Log</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="container-Transfer-right">
                            <div class="barcode-header-Transfer">
                                Transfer In Reports
                            </div>
                            <asp:RadioButtonList ID="rdoTransferIn" runat="server" Class="radioboxlist">
                                <asp:ListItem Value="TransferIn">Transfer In</asp:ListItem>
                                <asp:ListItem Value="PartialTransfer">Partially Transfer In </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="container-Transfer-right" style="margin-top: 175px;">
                            <div class="control-group" id="GroupBy" style="float: left; border: 1px solid; padding: 5px 5px 0 5px; width: 100%"
                                runat="server">
                                <div style="float: left; margin-left: 20px">
                                    <asp:RadioButton GroupName="Groupby" ID="rdoSumm" runat="server" Text="SUMMARY" Font-Bold="True" Class="radioboxlist"
                                        Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                    &nbsp;&nbsp;
                                </div>
                                <div style="float: left; margin-left: 20px" id="hdDetail">
                                    <asp:RadioButton GroupName="Groupby" ID="rdoDetail" runat="server" Text="DETAIL" Font-Bold="True" Class="radioboxlist"
                                        Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-top-container-Transfer">
                    <div class="barcode-header-Transfer">
                        Report Filtrations
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label11" runat="server" Text="Department"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Department', 'txtDepartment', 'txtWareHouse');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label9" runat="server" Text="WareHouse"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlWareHouse" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtWareHouse" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'WareHouse', 'txtWareHouse', 'txtCategory');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label10" runat="server" Text="Category"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtCategory" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'category', 'txtCategory', 'txtTransferType');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label8" runat="server" Text="Transfer Type"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlTransferType" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtTransferType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'TransferType', 'txtTransferType', 'txtBrand');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label13" runat="server" Text="Brand"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Brand', 'txtBrand', 'txtVendor');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label14" runat="server" Text="Vendor"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtSection');"></asp:TextBox>
                                <asp:HiddenField ID="hidVendorName" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label6" runat="server" Text="Section"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtSection" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Section', 'txtSection', 'txtVehicle');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label7" runat="server" Text="Vehicle"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlVehicle" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtVehicle" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vehicle', 'txtVehicle', 'txtClass');"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label5" runat="server" Text="Class"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Class', 'txtClass', 'txtInventory');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                                <asp:Label ID="Label4" runat="server" Text="ItemSearch"></asp:Label>
                            </div>
                            <div class="label-right">
                                <asp:TextBox ID="txtInventory" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtInventory', 'txtSubClass');" CssClass="form-control-res"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                            <div class="label-left">
                                <asp:Label ID="Label12" runat="server" Text="SubClass"></asp:Label>
                            </div>
                            <div class="label-right">
                                <%--<asp:DropDownList ID="ddlSubClass" runat="server" CssClass="form-control-res">
                                </asp:DropDownList>--%>
                                <asp:TextBox ID="txtSubClass" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'SubClass', 'txtSubClass', '');"></asp:TextBox>
                            </div>
                        </div>
                        <div class="control-group-right">
                            <div class="label-left">
                            </div>
                            <div class="label-right">
                            </div>
                        </div>
                    </div>
                    <div class="control-group-split">
                        <div class="control-group-left">
                        </div>
                        <div class="control-group-right">
                            <asp:LinkButton ID="lnkLoadReport" runat="server" class="btn btn-danger" Width="121px"
                                Font-Bold="true" OnClick="lnkLoadReport_Click">Load Report</asp:LinkButton>
                            <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-danger" Width="100px"
                                Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>
                        </div>
                    </div>
                    <asp:HiddenField ID="hiddReportType" Value="Out" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
