﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmSettlementReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmSettlementReport" %>


<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>

    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'SettlementReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "SettlementReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>

    <script type="text/javascript">

        var ctrltxtInventory;
        var CurrentDate;

        function pageLoad() {
            createDate();

            //$("[id*=GroupByLevel]").hide();
            //$("[id*=GroupByCompare]").hide();
            //$("[id*=GroupByVisitor]").show();
            //$('.ziehharmonika').ziehharmonika({
            //    collapsible: false,
            //    arrow: true,
            //    scroll: true,
            //    prefix: '★',//'♫'//,
            //    collapseIcons: {
            //        opened: ' ',
            //        closed: ' '
            //    }
            //});

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'});


            //$('#<%= rdolistPromotionMasterFilter.ClientID %> input').click(function () {
            var chkrdo = $("#<%= rdolistPromotionMasterFilter.ClientID %> input:radio:checked").val();
            if (chkrdo == 'Revoke') {
                $('#<%=hdRevokePromotion.ClientID %>').val('Revoke');
            }
            else {
                $('#<%=hdRevokePromotion.ClientID %>').val('');
            }
            if (chkrdo == 'Expired') {
                $('#<%=hdExpiredPromotion.ClientID %>').val('Expired');
            }
            else {
                $('#<%=hdExpiredPromotion.ClientID %>').val('');
            }
            if (chkrdo == 'Current') {

                $('#<%=txtProMasterFromDate.ClientID %>').val(CurrentDate);
                $('#<%=txtProMasterTodate.ClientID %>').val(CurrentDate);
                $('#ContentPlaceHolder1_txtProMasterFromDate').attr("disabled", "disabled");
                $('#ContentPlaceHolder1_txtProMasterFromDate').attr("disabled", "disabled");
                $('#<%=txtProMasterFromDate.ClientID %>').attr("disabled", true);
                    $('#<%=txtProMasterTodate.ClientID %>').attr("disabled", true);
                $('#<%=hdrdoCurrent.ClientID %>').val('Current');
            }
            else {
                $('#<%=hdrdoCurrent.ClientID %>').val('');
                $('#<%=txtProMasterFromDate.ClientID %>').attr("disabled", false);
                $('#<%=txtProMasterTodate.ClientID %>').attr("disabled", false);
            }
            // });
            var checkedvalue = $("#<%=rdoPromotion.ClientID%>").find(":checked").val();
            if (checkedvalue == 'Discount') {
                $("[id*=GroupByPromo]").show();
                $("[id*=divPromotionMaster]").hide();
            }
            else if (checkedvalue == 'Free') {
                $("[id*=GroupByPromo]").show();
                $("[id*=divPromotionMaster]").hide();
            }
            else if (checkedvalue == 'PromotionMaster') {

                $("[id*=divPromotionMaster]").show();
                $("[id*=GroupByPromo]").hide();
            }
            else {
                $("[id*=GroupByPromo]").hide();
                $("[id*=divPromotionMaster]").hide();
            }
            if (checkedvalue == 'Inventory' || checkedvalue == 'Department' || checkedvalue == 'Category' || checkedvalue == 'Brand')//aalayam 03052018
            {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_divchkBydatePromo').show();
                $('#ContentPlaceHolder1_divPromotionDate').show();
                $('#ContentPlaceHolder1_divProMasterDate').hide();
            }
            if (checkedvalue == 'Issued' || checkedvalue == 'Used' || checkedvalue == 'Unused') {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_divchkBydatePromo').show();
                $('#ContentPlaceHolder1_divPromotionDate').show();
                $('#ContentPlaceHolder1_divProMasterDate').hide();
            }

            if (checkedvalue == 'Discount' || checkedvalue == 'Free') {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').show();
                $('#ContentPlaceHolder1_divchkBydatePromo').show();
                $('#ContentPlaceHolder1_divPromotionDate').show();
                $('#ContentPlaceHolder1_divProMasterDate').hide();

            }
            if (checkedvalue == 'Duplicate') {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_divchkBydatePromo').show();
                $('#ContentPlaceHolder1_divPromotionDate').show();
                $('#ContentPlaceHolder1_divProMasterDate').hide();
            }
            if (checkedvalue == 'PromotionMaster') {
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_divchkBydatePromo').hide();
                $('#ContentPlaceHolder1_divPromotionDate').hide();
                $('#ContentPlaceHolder1_divProMasterDate').show();
                $('#ContentPlaceHolder1_txtPromotionExpired').number(true, 0);
            }
            if (checkedvalue == 'Item' || checkedvalue == 'Expired') {
                $("[id*=GrpfilterPromotionSales]").hide();
                $("[id*=divProMasterDate]").hide(); 
            }
            else
                $("[id*=GrpfilterPromotionSales]").show();


            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"));
            SetDefaultDate($("#<%= txtProMasterFromDate.ClientID %>"), $("#<%= txtProMasterTodate.ClientID %>"));

            $('#<%=chkBydateVC.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"), 'D')
                }
            });

            $('#<%=chkBydatePromo.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"), 'D')
                }
            });

            $('#<%=chkByDateSet.ClientID%>').change(function () {
                if (this.checked) {
                    SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'E')
                }
                else {
                    SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'D')
                }
            });


        }

        function clearForm() {

            try {
                $('input[type="text"]').val('');
                $("[id$=txtInventory]").val('');
                $("select").val(0);
                $("select").trigger("liszt:updated");
            }
            catch (err) {

                alert(err.Message);
                return false;
            }

            return false;
        }
        $(document).ready(function () {

        });

        $(document).ready(function () {
            $('#<%= rdolistPromotionMasterFilter.ClientID %> input').click(function () {
                var chkrdo = $("#<%= rdolistPromotionMasterFilter.ClientID %> input:radio:checked").val();
                if (chkrdo == 'Revoke') {
                    $('#<%=hdRevokePromotion.ClientID %>').val('Revoke');
                }
                else {
                    $('#<%=hdRevokePromotion.ClientID %>').val('');
                }
                if (chkrdo == 'Expired') {
                    $('#<%=hdExpiredPromotion.ClientID %>').val('Expired');
                }
                else {
                    $('#<%=hdExpiredPromotion.ClientID %>').val('');
                }
                if (chkrdo == 'Current') {

                    $('#<%=txtProMasterFromDate.ClientID %>').val(CurrentDate);
                    $('#<%=txtProMasterTodate.ClientID %>').val(CurrentDate);
                    //$('#ContentPlaceHolder1_txtFromDatePromo').attr("disabled", "disabled");
                    //$('#ContentPlaceHolder1_txtToDatePromo').attr("disabled", "disabled");
                    $('#<%=txtProMasterFromDate.ClientID %>').attr("disabled", true);
                    $('#<%=txtProMasterTodate.ClientID %>').attr("disabled", true);
                    $('#<%=hdrdoCurrent.ClientID %>').val('Current');
                }
                else {
                    $('#<%=hdrdoCurrent.ClientID %>').val('');
                    $('#<%=txtProMasterFromDate.ClientID %>').attr("disabled", false);
                    $('#<%=txtProMasterTodate.ClientID %>').attr("disabled", false);
                }

            });
            $('#<%= rdoPromotion.ClientID %> input').click(function () {
                var checkvalue = $("#<%= rdoPromotion.ClientID %> input:radio:checked").val();

                if (checkvalue == 'Discount') {
                    $("[id*=GroupByPromo]").show();
                    $("[id*=divPromotionMaster]").hide();
                }
                else if (checkvalue == 'Free') {
                    $("[id*=GroupByPromo]").show();
                    $("[id*=divPromotionMaster]").hide();
                }
                else if (checkvalue == 'PromotionMaster') {
                    $("[id*=divPromotionMaster]").show();
                    $("[id*=GroupByPromo]").hide();
                }
                else {
                    $("[id*=GroupByPromo]").hide();
                    $("[id*=divPromotionMaster]").hide();
                }
                if (checkvalue == 'Inventory' || checkvalue == 'Department' || checkvalue == 'Category' || checkvalue == 'Brand')//aalayam 03052018
                {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divchkBydatePromo').show();
                    $('#ContentPlaceHolder1_divPromotionDate').show();
                    $('#ContentPlaceHolder1_divProMasterDate').hide();
                }
                if (checkvalue == 'Issued' || checkvalue == 'Used' || checkvalue == 'Unused') {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divchkBydatePromo').show();
                    $('#ContentPlaceHolder1_divPromotionDate').show();
                    $('#ContentPlaceHolder1_divProMasterDate').hide();
                }

                if (checkvalue == 'Discount' || checkvalue == 'Free') {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_divchkBydatePromo').show();
                    $('#ContentPlaceHolder1_divPromotionDate').show();
                    $('#ContentPlaceHolder1_divProMasterDate').hide();

                }
                if (checkvalue == 'Duplicate') {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divchkBydatePromo').show();
                    $('#ContentPlaceHolder1_divPromotionDate').show();
                    $('#ContentPlaceHolder1_divProMasterDate').hide();
                }
                if (checkvalue == 'PromotionMaster') {
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divCashier').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divTerminal').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_SalesReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_divchkBydatePromo').hide();
                    $('#ContentPlaceHolder1_divPromotionDate').hide();
                    $('#ContentPlaceHolder1_divProMasterDate').show();
                    $('#ContentPlaceHolder1_txtPromotionExpired').number(true, 0);
                }
                if (checkvalue == 'Item' || checkvalue == 'Expired')
                    $("[id*=GrpfilterPromotionSales]").hide();
                else
                    $("[id*=GrpfilterPromotionSales]").show();

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtInventory').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_txtInventory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtLocation').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlLocation').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtDepartment').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlDept').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtCategory').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlCategory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtSubCategory').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlSubCategory').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtBrand').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlBrand').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtVendor').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlVendor').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtCashier').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlCashier').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtTerminal').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlTerminal').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtClass').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlClass').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SalesReportFilterUserControl_txtSubClass').val('');
                //$('#ContentPlaceHolder1_SalesReportFilterUserControl_ddlSubClass').trigger("liszt:updated");//
            });
            $('#<%= rdoSettlemnt.ClientID %> input').click(function () //aalayam 04052018
            {
                var checkvalue = $("#<%= rdoSettlemnt.ClientID %> input:radio:checked").val();

                $('#ContentPlaceHolder1_SettleReportfilterusercontrol_txtCashier').val('');
                //$('#ContentPlaceHolder1_SettleReportfilterusercontrol_ddlCashier').trigger("liszt:updated");

                $('#ContentPlaceHolder1_SettleReportfilterusercontrol_txtTerminal').val(''); 
                //$('#ContentPlaceHolder1_SettleReportfilterusercontrol_ddlTerminal').trigger("liszt:updated");
                if (checkvalue == "TerminalCash") {
                    $('#divFilteration').hide();
                }
                else {
                    $('#divFilteration').show();
                }
            });//Vijay 20190226 -Add Terminal and Cashier Wise Report
        });


        $(function () {

            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"));
            SetDefaultDate($("#<%= txtProMasterFromDate.ClientID %>"), $("#<%= txtProMasterTodate.ClientID %>"));

        });

        function SetDisable(FromDate, Todate, sflag) {
            if (sflag == "D") {
                FromDate.attr("disabled", "disabled");
                Todate.attr("disabled", "disabled");
            }
            else {
                FromDate.removeAttr("disabled");
                Todate.removeAttr("disabled");
            }
        }


        $(function () {

            try {

                $('#<%=chkBydateVC.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDateVC.ClientID %>"), $("#<%= txtToDateVC.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkByDateSet.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"), 'D')
                    }
                });

                $('#<%=chkBydatePromo.ClientID%>').change(function () {
                    if (this.checked) {
                        SetDisable($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"), 'E')
                    }
                    else {
                        SetDisable($("#<%= txtFromDatePromo.ClientID %>"), $("#<%= txtToDatePromo.ClientID %>"), 'D')
                    }
                });

            }
            catch (err) {
                alert(err.Message);
            }
        });
        function createDate() {
            CurrentDate = $.datepicker.formatDate("dd-mm-yy", new Date());
            //var date = new Date(),
            //    yr = date.getFullYear(),
            //    month = date.getMonth() + 1,
            //    day = date.getDate(),
            //    todayDate = day + '-' + month + '-' + yr;

        }

        function SetDefaultDate(FromDate, Todate) {

            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        }
        function fncProMasterValidation() {
            debugger;
            try {
                var fromdate = new Date($('#<%=txtProMasterFromDate.ClientID %>').val());
                var todate = new Date($('#<%=txtProMasterTodate.ClientID %>').val());
                if (fromdate > todate) {
                            alert('Please Enter From Date Must be lesser than Todate');
                            $("#<%= txtProMasterFromDate.ClientID %>").focus();
                                return false;
                            }
                var checkvalue = $("#<%= rdoPromotion.ClientID %> input:radio:checked").val();
                if (checkvalue == 'PromotionMaster') {
                    
                        
                            var chkrdo = $("#<%= rdolistPromotionMasterFilter.ClientID %> input:radio:checked").val();
                        if (chkrdo == 'Expired') {


                            if (todate >= CurrentDate) {
                                alert('Invalid To Date. Please Choose previous Date');
                                $("#<%= txtProMasterTodate.ClientID %>").focus();
                        return false;
                    }
                    else {
                        return true;
                    }

                }
            }
        }
        catch (err) {
            return false;
            alert(err.Message);
        }
    }

    function ValidateForm() {
        try {
            var Show = '';
            if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }

                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }

                if (Show != '') {
                    alert(Show);
                    return false;
                }

                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }

    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 150px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }

        .Promotion_textbox {
            width: 25%;
            height: 24px;
            padding: 3px 4px 0px 4px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #000000;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #d8d9d8;
            border-radius: 5px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            -webkit-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            -moz-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
            -o-transition: all border-color ease-in-out .15s, box-shadow ease-in-out .15s ease-out;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li><a style="text-decoration: none;">Sales Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Settlement Reports</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>


        <div class="ziehharmonika">

            <div style="overflow: visible; display: block;" runat="server" id="DivVoucherSettlement">
                <h3>Voucher Settlement Reports</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoVoucherSettlement" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Date">Date Wise Settlement</asp:ListItem>
                                <asp:ListItem Value="Consolidated">Consolidated Settlement</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateVC" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateVC" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-3 form_bootstyle">
                                        <asp:CheckBox ID="chkBydateVC" runat="server" Text="By Date" Class="radioboxlistgreen" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-5">
                                        <%--<asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:LinkButton ID="lnkLoadReportVC" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadReportVC_Click">Load Report </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="VouchersReportFilterUserControl"
                                EnablelocationDropDown="true"
                                EnableVoucherDropDown="true" />
                        </div>
                    </div>
                </div>
            </div>

            <div style="overflow: visible; display: block;" runat="server" id="DivSettlement">
                <h3>Settlement Reports</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoSettlemnt" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Terminal">Terminal Wise</asp:ListItem>
                                <asp:ListItem Value="Cashier">Cashier wise</asp:ListItem>
                                <asp:ListItem Value="Denomination">Denomination Wise</asp:ListItem>
                                <asp:ListItem Value="TerminalCash">Terminal and Cashier Wise</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-3 form_bootstyle">
                                        <asp:CheckBox ID="chkByDateSet" runat="server" Text="By Date" Class="radioboxlistgreen" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-5">
                                        <%--<asp:LinkButton ID="lnkClearSet" runat="server" class="btn btn-primary" Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:LinkButton ID="lnkSettlement" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkSettlement_Click">Load Report </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 form_bootstyle_border" id="divFilteration">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="SettleReportfilterusercontrol"
                                EnableTerminalDropDown="true"
                                EnableCashierDropDown="true" />
                        </div>
                    </div>
                </div>
            </div>


            <div style="overflow: visible; display: block;" runat="server" id="DivPromotion">
                <h3>Promotion Report</h3>
                <div class="row">
                    <div class="col-md-3">
                        <asp:RadioButtonList ID="rdoPromotion" runat="server" Class="radioboxlist">
                            <asp:ListItem Selected="True" Value="Inventory">Inventory Wise Sales</asp:ListItem>
                            <asp:ListItem Value="Department">Department Wise Sales</asp:ListItem>
                            <asp:ListItem Value="Category">Category Wise Sales</asp:ListItem>
                            <asp:ListItem Value="Brand">Brand Wise Sales</asp:ListItem>
                            <asp:ListItem Value="Item">Free Item sales</asp:ListItem>
                            <asp:ListItem Value="Issued">Coupon Issued List</asp:ListItem>
                            <asp:ListItem Value="Used">Discount Coupon Used List</asp:ListItem>
                            <asp:ListItem Value="Expired">Discount Coupon Expired List</asp:ListItem>
                            <asp:ListItem Value="Unused">Coupon Unused List</asp:ListItem>
                            <asp:ListItem Value="Duplicate">Duplicate Coupon Reprint</asp:ListItem>
                            <asp:ListItem Value="Discount">Discount Promotion List</asp:ListItem>
                            <asp:ListItem Value="Free">Free Promotion List</asp:ListItem>
                            <asp:ListItem Value="PromotionMaster">Promotion Master</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div id="GrpfilterPromotionSales" class="col-md-4 form_bootstyle_border">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="SalesReportFilterUserControl"
                                EnableVendorDropDown="true"
                                EnablelocationDropDown="true"
                                EnableCashierDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableTerminalDropDown="true"
                                EnableClassDropDown="true"
                                EnableInventoryTextBox="true"
                                EnableBrandDropDown="true"
                                EnableSubCategoryDropDown="true"
                                EnableSubClassDropDown="true" />
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-left: 40px;">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12" runat="server" id="divPromotionDate">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label3" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDatePromo" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label6" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDatePromo" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" runat="server" id="divProMasterDate">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label9" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtProMasterFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label10" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtProMasterTodate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="divPromotionMaster" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                                        <asp:RadioButtonList ID="rdolistPromotionMasterFilter" runat="server" Class="radioboxlist" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="Current">Current Date</asp:ListItem>
                                            <asp:ListItem Value="Revoke">Revoke</asp:ListItem>
                                            <asp:ListItem Value="Expired">Expired</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <%--<div class="col-md-4 form_bootstyle">
                                            <asp:RadioButton ID="rdoCurrent" Class="radioboxlistgreen" runat="server" GroupName="PromotionMasterFilter" Text="Current"
                                                Checked="true" />
                                        </div>
                                        <div class="col-md-4 form_bootstyle">
                                            <asp:RadioButton ID="rdoRevoke" Class="radioboxlistgreen" runat="server" GroupName="PromotionMasterFilter" Text="Revoke" />
                                        </div>
                                        <div class="col-md-4 form_bootstyle">
                                            <asp:RadioButton ID="rdoExpired" Class="radioboxlistgreen" runat="server" GroupName="PromotionMasterFilter" Text="Expired" />
                                        </div>--%>
                                        <asp:Label ID="Label7" runat="server" Text="Promotion Expired After"></asp:Label>
                                        <asp:TextBox ID="txtPromotionExpired" runat="server" MaxLength="4" CssClass="Promotion_textbox"></asp:TextBox>
                                        <asp:Label ID="Label8" runat="server" Text="Days"></asp:Label>
                                    </div>


                                </div>
                            </div>
                            <div class="col-md-12" style="margin-top: 15px">
                                <div id="divchkBydatePromo" runat="server" class="col-md-3 form_bootstyle">
                                    <asp:CheckBox ID="chkBydatePromo" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                </div>
                                <div class="col-md-5">
                                    <%--<asp:LinkButton ID="lnkPromoClearSearch" runat="server" class="btn btn-primary" Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                </div>
                                <div class="col-md-2">
                                    <asp:LinkButton ID="lnkPromotionReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClientClick="return fncProMasterValidation();" OnClick="lnkPromotionReport_Click">Load Report </asp:LinkButton>
                                </div>
                            </div>
                        </div>


                        <div id="GroupByPromo" class="col-md-12 form_bootstyle_border" style="margin-top: 15px; display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkPromActive" Class="radioboxlistgreen" runat="server" Text="IsACTIVE" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
            <asp:HiddenField ID="hdrdoCurrent" runat="server" Value="" />
            <asp:HiddenField ID="hdRevokePromotion" runat="server" Value="" />
              <asp:HiddenField ID="hdExpiredPromotion" runat="server" Value="" />
        </div>
    </div>

</asp:Content>
