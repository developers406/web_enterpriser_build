﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmAttributeReports.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmAttributeReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'AttributeReport');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "AttributeReport";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>


    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {
            try {
                fncInitiateFilter();
                fncTabClick();
                fncSetActiveTab();


                $("#<%=txtStyle.ClientID%>").attr("disabled", true);

            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncLoadInitialRecord() {
            try {
                $("#<%= txtfrmDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
                $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            catch (err) {
                fncToastError(err.message);
            }
        }

        function fncSetActiveTab() {
            try {

                if ($("#<%=hidSelection.ClientID%>").val() == "#StockReport") {
                    $('#liStockReport').addClass('active');
                    $('#StockReport').addClass('active');

                    $('#liPurchaseReport').removeClass('active');
                    $('#PurchaseReport').removeClass('active');

                    $('#liSalesReport').removeClass('active');
                    $('#SalesReport').removeClass('active');

                }
                else if ($("#<%=hidSelection.ClientID%>").val() == "#PurchaseReport") {
                    $('#liStockReport').removeClass('active');
                    $('#StockReport').removeClass('active');

                    $('#liPurchaseReport').addClass('active');
                    $('#PurchaseReport').addClass('active');

                    $('#liSalesReport').removeClass('active');
                    $('#SalesReport').removeClass('active');

                }
                else if ($("#<%=hidSelection.ClientID%>").val() == "#SalesReport") {
                    $('#liStockReport').removeClass('active');
                    $('#StockReport').removeClass('active');

                    $('#liPurchaseReport').removeClass('active');
                    $('#PurchaseReport').removeClass('active');

                    $('#liSalesReport').addClass('active');
                    $('#SalesReport').addClass('active');

                }

        }
        catch (err) {
            fncToastError(err.message);
        }
    }

    function fncTabClick() {
        try {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href") // activated tab                    
                $('#<%=hidSelection.ClientID %>').val(target);

                if ($("#<%=hidSelection.ClientID%>").val() == "#StockReport") {
                    $('#divStkType').css("display", "block");
                }
                else if ($("#<%=hidSelection.ClientID%>").val() == "#PurchaseReport") {
                    $('#divStkType').css("display", "none");
                }
                else if ($("#<%=hidSelection.ClientID%>").val() == "#SalesReport") {
                    $('#divStkType').css("display", "none");
                }
            });
    }
    catch (err) {
        fncToastError(err.message);
    }
}

function fncInitiateFilter() {
    try {
        //// AutoComplete Search for Department
        $("[id$=txtDepartment]").autocomplete({
            source: function (request, response) {
                var obj = {};
                obj.searchData = request.term.replace("'", "''");
                obj.mode = "Department";

                $.ajax({
                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                    data: JSON.stringify(obj),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                valitemcode: item.split('|')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        ShowPopupMessageBox(response.message);
                    },
                    failure: function (response) {
                        ShowPopupMessageBox(response.message);
                    }
                });
            },
            focus: function (event, i) {
                $('#<%=txtDepartment.ClientID %>').val($.trim(i.item.valitemcode));
                event.preventDefault();
            },
            select: function (e, i) {
                $('#<%=txtDepartment.ClientID %>').val($.trim(i.item.valitemcode));
                return false;
            },
            minLength: 1
        });

            $("[id$=txtCategory]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.searchData = request.term.replace("'", "''");
                    obj.mode = "category";

                    $.ajax({
                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                            data: JSON.stringify(obj),
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        label: item.split('|')[0],
                                        valitemcode: item.split('|')[1]
                                    }
                                }))
                            },
                            error: function (response) {
                                ShowPopupMessageBox(response.message);
                            },
                            failure: function (response) {
                                ShowPopupMessageBox(response.message);
                            }
                        });
                    },
                    focus: function (event, i) {
                        $('#<%=txtCategory.ClientID %>').val($.trim(i.item.valitemcode));
                    event.preventDefault();
                },
                select: function (e, i) {
                    $('#<%=txtCategory.ClientID %>').val($.trim(i.item.valitemcode));
                        return false;
                    },
                    minLength: 1
                });

                    $("[id$=txtClass]").autocomplete({
                        source: function (request, response) {
                            var obj = {};
                            obj.searchData = request.term.replace("'", "''");
                            obj.mode = "Class";

                            $.ajax({
                                url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                                data: JSON.stringify(obj),
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    response($.map(data.d, function (item) {
                                        return {
                                            label: item.split('|')[0],
                                            valitemcode: item.split('|')[1]
                                        }
                                    }))
                                },
                                error: function (response) {
                                    ShowPopupMessageBox(response.message);
                                },
                                failure: function (response) {
                                    ShowPopupMessageBox(response.message);
                                }
                            });
                        },
                        focus: function (event, i) {
                            $('#<%=txtClass.ClientID %>').val($.trim(i.item.valitemcode));
                            event.preventDefault();
                        },
                        select: function (e, i) {
                            $('#<%=txtClass.ClientID %>').val($.trim(i.item.valitemcode));
                            return false;
                        },
                        minLength: 1
                    });
                        $("[id$=txtBrand]").autocomplete({
                            source: function (request, response) {
                                var obj = {};
                                obj.searchData = request.term.replace("'", "''");
                                obj.mode = "Brand";

                                $.ajax({
                                    url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/fncCommonSearch")%>',
                                    data: JSON.stringify(obj),
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    success: function (data) {
                                        response($.map(data.d, function (item) {
                                            return {
                                                label: item.split('|')[0],
                                                valitemcode: item.split('|')[1]
                                            }
                                        }))
                                    },
                                    error: function (response) {
                                        ShowPopupMessageBox(response.message);
                                    },
                                    failure: function (response) {
                                        ShowPopupMessageBox(response.message);
                                    }
                                });
                            },
                            focus: function (event, i) {
                                $('#<%=txtBrand.ClientID %>').val($.trim(i.item.valitemcode));
                                event.preventDefault();
                            },
                            select: function (e, i) {
                                $('#<%=txtBrand.ClientID %>').val($.trim(i.item.valitemcode));
                                return false;
                            },
                            minLength: 1
                        });

                            $("[id$=txtItemSearchT]").autocomplete({
                                source: function (request, response) {
                                    var obj = {};
                                    obj.prefix = escape(request.term);
                                    $.ajax({
                                        url: '<%=ResolveUrl("~/Inventory/frmInventoryMaster.aspx/GetFilterValue") %>',
                                        data: JSON.stringify(obj),
                                        dataType: "json",
                                        type: "POST",
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            response($.map(data.d, function (item) {
                                                return {
                                                    label: item.split('/')[0],
                                                    val: item.split('/')[1],
                                                    desc: item.split('/')[2]
                                                }
                                            }))
                                        },
                                        error: function (response) {
                                            ShowPopupMessageBox(response.responseText);
                                        },
                                        failure: function (response) {
                                            ShowPopupMessageBox(response.responseText);
                                        }
                                    });
                                },
                                focus: function (event, i) {
                                    $("[id$=txtItemSearchT]").val(i.item.desc);
                                    event.preventDefault();
                                },
                                //==========================> After Select Value
                                select: function (e, i) {
                                    $("[id$=txtItemSearchT]").val(i.item.val);
                                    return false;
                                },

                                minLength: 2
                            });
                                    

                                $("[id$=txtLocationcode]").autocomplete({
                                    source: function (request, response) {
                                        var obj = {};
                                        obj.searchData = request.term.replace("'", "''");
                                        obj.mode = "Location";

                                        $.ajax({
                                            url: '<%=ResolveUrl("frmAttributeReports.aspx/fncCommonSearch")%>',
                                            data: JSON.stringify(obj),
                                            dataType: "json",
                                            type: "POST",
                                            contentType: "application/json; charset=utf-8",
                                            success: function (data) {
                                                response($.map(data.d, function (item) {
                                                    return {
                                                        label: item.split('|')[0],
                                                        valitemcode: item.split('|')[1]
                                                    }
                                                }))
                                            },
                                            error: function (response) {
                                                fncToastError(response.message);
                                            },
                                            failure: function (response) {
                                                fncToastError(response.message);
                                            }
                                        });
                                    },
                                    focus: function (event, i) {
                                        $('#<%=txtLocationcode.ClientID %>').val($.trim(i.item.valitemcode));
                                        event.preventDefault();
                                    },
                                    select: function (e, i) {
                                        $('#<%=txtLocationcode.ClientID %>').val($.trim(i.item.valitemcode));
                                        return false;
                                    },
                                    minLength: 1
                                });

                                }
                                catch (err) {
                                    fncToastError(err.message);
                                }
                            }                           
                            function fncStockReportClick() {
                                try {
                                    var checkedvalue = $("#<%=rdoStockReport.ClientID%>").find(":checked").val();
                                    if (checkedvalue == "Batchwisestk" || checkedvalue == "stockAging") {
                                        $("#<%=txtStyle.ClientID%>").attr("disabled", false);
                                    }
                                    else {
                                        $("#<%=txtStyle.ClientID%>").attr("disabled", true);
                                    }
                                }
                                catch (err) {
                                    fncToastError(err.message);
                                }
                            }
 //----------------------------------------------------------------------------------------------------------------------------------------------
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }

        function fncShowSearchDialogInventory(event, BatchNo, txtStyle, txtLocationcode) {

        try {
            var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
            if (keyCode == 112) {
                fncShowSearchDialogCommon(event, BatchNo, 'txtStyle', 'txtLocationcode');
                event.preventDefault();
            }
            else if (keyCode == 13 && $("#<%=txtStyle.ClientID %>").val() == '') {
                $("#<%=txtStyle.ClientID %>").focus();
                event.preventDefault();
            }
            else if (keyCode == 13) {
                event.preventDefault();
                fncGetInventoryCode_Master($("#<%=txtStyle.ClientID %>"));
                event.preventDefault();
                event.stopPropagation();
                return false;
            }        
            
}
    catch (err) {
        ShowPopupMessageBox(err.message);
    }

         }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
                </ul></div></div>
             
    <div id="divItemSearchTextiles">
        <div class="container welltextiles" style="margin-top: 10px;">
            <div style="border: solid; margin-bottom: 10px;">
                <div class="panel panel-default">
                    <div id="Tabs" role="tabpanel">
                        <ul class="nav nav-tabs custnav custnav-im" role="tablist">
                            <li id="liStockReport" class="active attribute_tabwidth"><a href="#StockReport" aria-controls="StockReport" role="tab" data-toggle="tab">Stock Report</a></li>
                            <li id="liPurchaseReport" class="attribute_tabwidth"><a href="#PurchaseReport" aria-controls="PurchaseReport" role="tab" data-toggle="tab">Purchase Report</a></li>
                            <li id="liSalesReport" class="attribute_tabwidth"><a href="#SalesReport" aria-controls="SalesReport" role="tab" data-toggle="tab">Sales Report </a></li>
                        </ul>
                        <div class="tab-content" style="padding-top: 5px; overflow: auto; height: auto">
                            <div class="tab-pane active" role="tabpanel" id="StockReport">
                                <div class="col-md-12">
                                    <asp:RadioButtonList ID="rdoStockReport" runat="server" onclick="fncStockReportClick();" Class="radioboxlist" RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Selected="True" Value="Inventory"> Inventory Stock List</asp:ListItem>
                                        <asp:ListItem Value="Batchwisestk">Batch Wise Stock</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="PurchaseReport">
                                <asp:RadioButtonList ID="rbnPurchaseReport" runat="server" Class="radioboxlist" RepeatDirection="Horizontal" RepeatLayout="Table">
                                    <asp:ListItem Selected="True" Value="ItemWisePurchaseReport"> Item Wise Purchase Report</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="SalesReport">
                                <div class="col-md-12">
                                    <asp:RadioButtonList ID="rbnSalesReport" runat="server" Class="radioboxlist" RepeatDirection="Horizontal" RepeatLayout="Table">
                                        <asp:ListItem Selected="True" Value="ItemWiseSalesReport"> Item Wise Sales Report</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container welltextiles" id="divFilter">
            <div class="col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Vendor</label>
                 <asp:TextBox ID="txtVendor" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Vendor', 'txtVendor', 'txtDepartment');"></asp:TextBox>
            </div>
            <div class="col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Department</label>
                <asp:TextBox ID="txtDepartment" onkeydown="return fncShowSearchDialogCommon(event, 'Department',  'txtDepartment', 'txtCategory');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Category</label>
                <asp:TextBox ID="txtCategory" onkeydown="return fncShowSearchDialogCommon(event, 'category',  'txtCategory', 'txtClass');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Class</label>
                <asp:TextBox ID="txtClass" onkeydown="return fncShowSearchDialogCommon(event, 'Class',  'txtClass', 'txtBrand');" CssClass="col-md-11 " runat="server"></asp:TextBox>

            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Brand</label>
                <asp:TextBox ID="txtBrand" onkeydown="return fncShowSearchDialogCommon(event, 'Brand',  'txtBrand', 'txtItemSearchT');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label for="staticEmail" class="col-md-12 col-form-label">Item Code</label>
                <asp:TextBox ID="txtItemSearchT" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemSearchT', 'txtStyle');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label id="lblStyle" class="col-md-12 col-form-label">Style </label>
                <asp:TextBox ID="txtStyle" onkeydown="return fncShowSearchDialogInventory(event, 'BatchNo',  'txtStyle', 'txtLocationcode');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px;">
                <label id="lblLocationcode" class="col-md-12 col-form-label">Location code</label>
                <asp:TextBox ID="txtLocationcode" onkeydown="return fncShowSearchDialogCommon(event, 'Location',  'txtLocationcode', 'ddlInvMaster');" CssClass="col-md-11 " runat="server"></asp:TextBox>
            </div>
            <div class="col-md-6" id="divDateStklst">
                <div class="form-group row col-md-4" style="margin-bottom: 5px;">
                    <div class="col-md-6">
                        <label id="lblfrmDate" class="col-md-12 col-form-label">From Date</label>
                        <asp:TextBox ID="txtfrmDate" CssClass="col-md-10 " runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label id="lbltoDate" class="col-md-12 col-form-label">To Date</label>
                        <asp:TextBox ID="txttoDate" CssClass="col-md-10 " runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group row col-md-8" style="margin-bottom: 5px;">
                    <div id="divStkType">
                        <label id="lblStockType" class="col-md-12 col-form-label">Stock Type</label>
                        <div class="col-md-12">
                            <asp:ListBox ID="lstStocktype" runat="server" CssClass="form-control-res" Style="width: 370px;" SelectionMode="Multiple">
                                <asp:ListItem Value="1">Positive</asp:ListItem>
                                <asp:ListItem Value="2">Negative</asp:ListItem>
                                <asp:ListItem Value="3">Zero</asp:ListItem>
                            </asp:ListBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row col-md-2" style="margin-bottom: 5px; margin-top: 20px;">
                <asp:LinkButton ID="lnkbtClearFilter" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Clear" OnClick="lnkbtClearFilter_click"></asp:LinkButton>
                <asp:LinkButton ID="lnkLoad" runat="server" class="ui-button ui-corner-all ui-widget" Style="float: right" Text="Load" OnClick="lnkLoad_click"></asp:LinkButton>
            </div>
        </div>
        <div class="container welltextiles" id="divAttribute">
            <div class="barcode-header" style="background-color: #c6b700">
                Attributes
            </div>
            <div style="height: auto; border: solid; overflow-x: visible; overflow-y: hidden; height: 215px;">
                <asp:GridView ID="grdAttribute" runat="server" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" CssClass="fixed_header_Empty_grid"
                    EmptyDataRowStyle-CssClass="Emptyidclassforselector">
                    <EmptyDataTemplate>
                        <asp:Label ID="Label2" runat="server" Text="No Records Found"></asp:Label>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                    <RowStyle CssClass="pshro_GridDgnStyle" />
                    <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />
                    <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                        NextPageText="Next" PreviousPageText="Previous" Position="TopAndBottom" />
                    <PagerStyle CssClass="pshro_text" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <div class="col-md-2">
            </div>
            <div class="col-md-9">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                </div>
            </div>
        </div>
    </div>
    <div class="display_none">
        <asp:HiddenField ID="hidSelection" runat="server" Value="#StockReport" />
        
    </div>
</asp:Content>
