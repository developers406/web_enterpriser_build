﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true"
    CodeBehind="BackOfficeReports.aspx.cs" Inherits="EnterpriserWebFinal.Reports.BackOfficeReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
        
    .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        table
        {
            width: 100%;
        }
        .chkBydateBack label
        {
            position: relative;
            top: -2px;
        }
    </style>
    <%--   <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
            color: Black;
        }
        .main_menu, .main_menu:hover
        {
            width: 100px;
            background-color: #fff;
            color: #333;
            text-align: center;
            height: 30px;
            line-height: 30px;
            margin-right: 5px;
            display: inline-block;
        }
        .main_menu:hover
        {
            background-color: #ccc;
        }
        .level_menu, .level_menu:hover
        {
            width: 110px;
            background-color: #fff;
            color: #333;
            text-align: center;
            height: 30px;
            line-height: 30px;
            margin-top: 5px;
        }
        .level_menu:hover
        {
            background-color: #ccc;
        }
        .selected, .selected:hover
        {
            background-color: #A6A6A6 !important;
            color: #fff;
        }
        .level2
        {
            background-color: #fff;
        }
    </style>--%>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'BackOfficeReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "BackOfficeReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>

    <script type="text/javascript">

        function pageLoad() {
            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            var Rdochecked = $('input[type="radio"]:checked').next().text();

            if (Rdochecked.indexOf("Voucher-") != -1 || Rdochecked.indexOf("SalesMan-") != -1 || Rdochecked.indexOf("Floor Wise") != -1) {
                //if (Rdochecked == "Voucher SalesSUMMARYDATE" || Rdochecked == "SalesMan CommissionSUMMARYDATE" || Rdochecked == "Floor Wise SalesSUMMARYDATE") {                
                $("[id*=divSalesMoreSD]").show();
                $("[id*=divDateGroups]").show();
                $("[id*=lnkFilter]").hide();
            }
            else {
                //alert(Rdochecked);
                $("[id*=divSalesMoreSD]").hide();
                $("[id*=divDateGroups]").hide();
                $("[id*=lnkFilter]").hide();
            }


            if ($('#<%=chkBydateBack.ClientID%>').is(':checked') == false) {
                $("[id*=txtfromDate]").attr("disabled", "disabled");
                $("[id*=txttoDate]").attr("disabled", "disabled");
            }

            $('#<%=chkBydateBack.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtfromDate]").removeAttr("disabled");
                    $("[id*=txttoDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtfromDate]").attr("disabled", "disabled");
                    $("[id*=txttoDate]").attr("disabled", "disabled");
                }
            });

        }


        function showFilter() {

            if ($("[id*=divfilter]").is(':hidden')) {
                $("[id*=divfilter]").show();
            }
            else {
                $("[id*=divfilter]").hide();
            }

            return false;
        }

        function Clear() {
            $("select").val(0);
            $("select").trigger("liszt:updated");
            $("[id*=divfilter]").hide();
            $("[id*=lnkFilter]").show();
            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");

        }


        function CheckChange(Type, isDate) {


            Clear();

            //alert(isDate + '-' + Type);
            if (isDate == "Yes") {

                $("[id*=divDateFilter]").show();
            }
            else if (isDate == "No") {
                //alert(isDate);
                $("[id*=divDateFilter]").hide();
            }

            if (Type == "Sales") {
                $("[id*=lnkFilter]").show();
                $("[id*=rptBrandDrop]").attr("disabled", "disabled");
                $("[id*=rptCatDrop]").attr("disabled", "disabled");
                $("[id*=rptDeptDrop]").attr("disabled", "disabled");
                $("[id*=rptInvDrop]").attr("disabled", "disabled");
                $("[id*=rptVendorDrop]").attr("disabled", "disabled");
                $("[id*=DropDownCashier]").removeAttr("disabled");
                $("[id*=DropDownTerminal]").removeAttr("disabled");
            }
            else if (Type == "Inventory") {
                $("[id*=lnkFilter]").show();
                $("[id*=rptBrandDrop]").removeAttr("disabled");
                $("[id*=rptCatDrop]").removeAttr("disabled");
                $("[id*=rptDeptDrop]").removeAttr("disabled");
                $("[id*=rptInvDrop]").removeAttr("disabled");
                $("[id*=rptVendorDrop]").removeAttr("disabled");
                $("[id*=DropDownCashier]").attr("disabled", "disabled");
                $("[id*=DropDownTerminal]").attr("disabled", "disabled");
            }
            else if (Type == "More") {

                $("[id*=lnkFilter]").hide();
                $("[id*=divfilter]").hide();
                if (isDate == "Yes") {

                    $("[id*=divSalesMoreSD]").show();
                    $("[id*=divDateGroups]").show();
                }
                else if (isDate == "No") {

                    $("[id*=divSalesMoreSD]").hide();
                    $("[id*=divDateGroups]").hide();
                }
                else if (isDate == "YN") {

                    $("[id*=divSalesMoreSD]").hide();
                    $("[id*=divDateGroups]").show();
                }
            }
            else if (Type == "Credit") {
                $("[id*=lnkFilter]").hide();
                $("[id*=divfilter]").hide();

                if (isDate == "Yes") {

                    $("[id*=divCRDueSD]").show();
                }
                else {
                    $("[id*=divCRDueSD]").hide();
                }

            }

        }

        $(function () {
            $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });

            if ($("#<%= txtfromDate.ClientID %>").val() === '') {
                $("#<%= txtfromDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }

            if ($("#<%= txttoDate.ClientID %>").val() === '') {
                $("#<%= txttoDate.ClientID %>").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
        });

        //        function ValidateForm() {
        //            var Show = '';
        //            
        //         
        //            if (document.getElementById("<%=txttoDate.ClientID%>").value == "") {
        //                Show = Show + '\n  Choose To date';
        //                document.getElementById("<%=txttoDate.ClientID %>").focus();
        //            }

        //            if (document.getElementById("<%=txtfromDate.ClientID%>").value == "") {
        //                Show = Show + '\n  Choose from date';
        //                document.getElementById("<%=txtfromDate.ClientID %>").focus();
        //            }
        //            //            }


        //            if (Show != '') {
        //                alert(Show);
        //                return false;
        //            }

        //            else {

        //                $("#HiddenfromDate").val($("#<%= txtfromDate.ClientID %>").val());
        //                $("#HiddentoDate").val($("#<%= txttoDate.ClientID %>").val());

        //                return true;
        //            }
        //        }

        function ValidateForm() {
            var Show = '';
            if ($("#<%= txtfromDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose From date';
                $("#<%= txtfromDate.ClientID %>").focus();
            }

            if ($("#<%= txttoDate.ClientID %>").val() == "") {
                Show = Show + '\n  Choose To date';
                $("#<%= txttoDate.ClientID %>").focus();
            }

            if (Show != '') {
                alert(Show);
                //JSalerts(Show);
                return false;
            }

            else {
                var StartDate = $("#<%= txtfromDate.ClientID %>").val();
                var EndDate = $("#<%= txttoDate.ClientID %>").val();
                var eDate = new Date(EndDate);
                var sDate = new Date(StartDate);
                if (StartDate != '' && EndDate != '' && sDate > eDate) {
                    alert("Please ensure that the End Date is greater than or equal to the Start Date.");
                    //JSalerts("Please ensure that the End Date is greater than or equal to the Start Date.");
                    return false;
                }

                $("#HiddenfromDate").val($("#<%= txtfromDate.ClientID %>").val());
                $("#HiddentoDate").val($("#<%= txttoDate.ClientID %>").val());
                return true;
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="main-container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
               <li class="active-page" id="BackOfficeReportTitle" runat="server"> Reports </li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
            </ul>
        </div>
        <div class="container-group-report" style="float: left; margin-top: 10px; border:0px;">
            <%--<asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" DataSourceID="XmlDataSource1"
                OnMenuItemDataBound="OnMenuItemDataBound">
                <LevelMenuItemStyles>
                    <asp:MenuItemStyle CssClass="main_menu" />
                    <asp:MenuItemStyle CssClass="level_menu" />
                </LevelMenuItemStyles>
                <StaticSelectedStyle CssClass="selected" />
                <DataBindings>
                    <asp:MenuItemBinding DataMember="Menu" TextField="Text" ValueField="Value" NavigateUrlField="Url" />
                    <asp:MenuItemBinding DataMember="SubMenu" TextField="Text" ValueField="Value" NavigateUrlField="Url" />
                </DataBindings>
            </asp:Menu>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/Menus.xml" XPath="/Menus/Menu">
            </asp:XmlDataSource>--%>
            <div class="container-left" id="divInventoryMaster" runat="server">
                <div class="barcode-header">
                    Inventory Master Reports
                </div>
                <asp:Panel ID="Panel1" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDepartment" runat="server" GroupName="RegularMenu" Checked="True" />
                                <asp:Label ID="Label7" runat="server" Checked="true" Text="Department"></asp:Label>
                                <%--<p style="display: none">testing 1...2...3...</p>--%>
                            </div>
                        </div>
                        <%--    <div id="complete" style="float: right; margin-left: 15%">
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkCompleteInfo" runat="server" GroupName="SubMenu" Text="Complete Info"
                                        Font-Bold="True" />
                                </div>
                            </div>
                            <div class="control-group" id="subDiv" style="float: left; border: 1px solid; padding: 5px 5px 0 5px;
                                width: 165px; margin-left: 5%">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkItemWise" runat="server" GroupName="SubMenu1" Text="ItemWise"
                                        Checked="true" Font-Bold="True" />
                                    &nbsp;&nbsp;
                                </div>
                                <div style="float: left">
                                    <asp:RadioButton ID="chkBatchwise" runat="server" GroupName="SubMenu1" Text="Batchwise"
                                        Font-Bold="True" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkMarginnotSet" runat="server" GroupName="SubMenu" Text="Margin Not Set"
                                        Font-Bold="True" />
                                </div>
                            </div>
                        </div>
                    </div>--%>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoCategory" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label8" runat="server" Text="Category"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSubcategory" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label9" runat="server" Text="SubCategory"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoBrand" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label10" runat="server" Text="Brand"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoVendor" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label12" runat="server" Text="Vendor"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoLocation" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label13" runat="server" Text="Location"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoWarehouse" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label31" runat="server" Text="Warehouse"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoTerminal" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label32" runat="server" Text="Terminal"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoFloor" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label33" runat="server" Text="Floor"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 25px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoSection" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label34" runat="server" Text="Section"></asp:Label>
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divPromotion" runat="server">
                <div class="barcode-header">
                    Promotion Reports
                </div>
                <asp:Panel ID="Panel10" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoInvenPromotionSales" runat="server" GroupName="RegularMenu"
                                    Checked="True" />
                                <asp:Label ID="Label117" runat="server" Checked="true" Text="Inventory Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPromotionDept" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label107" runat="server" Text="Department Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPromotionCat" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label108" runat="server" Text="Category Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPromotionBrand" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label109" runat="server" Text="Brand Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoPromitionfreeItem" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label110" runat="server" Text="Free Item sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDiscCouponIssued" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label111" runat="server" Text="Coupon Issued List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDiscCouponUsed" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label112" runat="server" Text="Discount Coupon Used List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDiscCouponExpired" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label113" runat="server" Text="Discount Coupon Expired List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDiscCouponUnusedList" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label114" runat="server" Text="Coupon Unused List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDuplicateCouponReprint" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label115" runat="server" Text="Duplicate Coupon Reprint"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDiscPromoList" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label118" runat="server" Text="Discount Promotion List"></asp:Label>
                                <asp:CheckBox ID="chkPromActive" runat="server" Text="isActive" ForeColor="#FF3300" />
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 25px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoFreePromoList" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label116" runat="server" Text="Free Promotion List"></asp:Label>
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divPayReceipt" runat="server">
                <div class="barcode-header">
                    Payment & Receipts Reports
                </div>
                <asp:Panel ID="Panel5" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="RdPayments" runat="server" GroupName="RegularMenu" Checked="True" />
                                <asp:Label ID="Label63" runat="server" Checked="true" Text="Payments"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdReceipts" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label64" runat="server" Text="Receipts"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" style="margin-bottom: 25px">
                            <div style="float: left">
                                <asp:RadioButton ID="RdPayReceipt" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label65" runat="server" Text="Payment & Receipts"></asp:Label>
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divMemberDetails" runat="server">
                <div class="barcode-header">
                    Member Details Report
                </div>
                <asp:Panel ID="Panel8" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMemberDetail" runat="server" GroupName="RegularMenu" Checked="True" />
                                <asp:Label ID="Label4" runat="server" Checked="true" Text="Member Detail"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMembDetJoinDate" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label76" runat="server" Text="Member Detail Join Date"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMemberPoints" runat="server" GroupName="RegularMenu" AutoPostBack="True" />
                                <asp:Label ID="Label77" runat="server" Text="Member List with Points"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMemberTrans" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label78" runat="server" Text="Member Transactions"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMemberPointAdj" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label79" runat="server" Text="Member Point Adjustment"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoAgeGroup" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label80" runat="server" Text="Age Group"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoIncomeGroup" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label81" runat="server" Text="Income Group"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMembListByAge" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label82" runat="server" Text="Member List By AgeGroup"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMembListByIncome" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label83" runat="server" Text="Member List By Income"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoGiftIssued" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label84" runat="server" Text="Gift Issued"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoUnclaimedGift" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label85" runat="server" Text="Unclaimed Gift"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoMembOutstanding" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label86" runat="server" Text="Member Outstanding"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoDayWisesales" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label87" runat="server" Text="Day Wise Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoHourlySales" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label88" runat="server" Text="Hourly Sales"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoIBCmembTran" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label89" runat="server" Text="IBC Member Transaction"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="rdoAvgPurReceipt" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label90" runat="server" Text="Average.Purchase / Receipt"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="div5" style="float: left; border: 1px solid; padding: 5px 5px 0 5px;
                            width: 100%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="rdoActive" runat="server" Text="ACTIVE" Checked="true" GroupName="SRGroup"
                                    Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="rdoInactive" runat="server" Text="IN ACTIVE" Font-Bold="True"
                                    GroupName="SRGroup" Font-Overline="False" Font-Size="X-Small" ForeColor="Green" />
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="rdoAllMB" runat="server" Text="ALL" Font-Bold="True" GroupName="SRGroup"
                                    Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                            <div class="control-group" id="div4" visible="false" style="float: left; border: 1px solid;
                                margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                                <div style="float: left; margin-left: 25px">
                                    <asp:RadioButton GroupName="PurCH" ID="RadioButton11" runat="server" Text="SUMMARY"
                                        Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                    &nbsp;&nbsp;
                                </div>
                                <div style="float: left; margin-left: 25px">
                                    <asp:RadioButton GroupName="PurCH" ID="RadioButton12" runat="server" Text="DETAIL"
                                        Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                </div>
                            </div>
                        </div>
                </asp:Panel>
            </div>
            <div class="container-left" id="divSalesReturn" runat="server">
                <div class="barcode-header">
                    Sales Return
                </div>
                <asp:Panel ID="Panel6" runat="server">
                    <div style="display: table">
                        <div class="control-group" style="margin-top: 10px">
                            <div style="float: left">
                                <asp:RadioButton ID="RdSalesRefundAll" runat="server" GroupName="RegularMenu" Checked="True" />
                                <asp:Label ID="Label66" runat="server" Checked="true" Text="Sales Refund-ALL"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdSalesRefundCash" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label67" runat="server" Text="Sales Refund-CASH"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdSalesRefdExcVoch" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label68" runat="server" Text="Sales Refund-Exchange Voucher"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdExVouchUnUsed" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label69" runat="server" Text="Exchange Voucher - UnUsed"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdExVouchUsed" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label70" runat="server" Text="Exchange Voucher - Used"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdExVouchExpired" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label71" runat="server" Text="Exchange Voucher - Expired"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdTradeUnused" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label72" runat="server" Text="Trade - UnUsed List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdTradeused" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label73" runat="server" Text="Trade - Used List"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdTradeDeleted" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label75" runat="server" Text="Trade - Deleted"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group">
                            <div style="float: left">
                                <asp:RadioButton ID="RdExchange" runat="server" GroupName="RegularMenu" />
                                <asp:Label ID="Label74" runat="server" Text="Exchange Items"></asp:Label>
                            </div>
                        </div>
                        <div class="control-group" id="divSalesRetDetSum" visible="false" style="float: left;
                            border: 1px solid; margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="RdoSalRetSumm" runat="server" Text="SUMMARY"
                                    Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton GroupName="PurCH" ID="RdoSalRetDet" runat="server" Text="DETAIL"
                                    Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                        <div class="control-group" id="div3" style="float: left; border: 1px solid; padding: 5px 5px 0 5px;
                            width: 100%" runat="server">
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="RdoCashSales" runat="server" Text="CASH SALES" Checked="true"
                                    GroupName="SRGroup" Font-Bold="True" Font-Overline="False" Font-Size="X-Small"
                                    ForeColor="Red" />
                                &nbsp;&nbsp;
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="RdoCreditSales" runat="server" Text="CREDIT SALES" Font-Bold="True"
                                    GroupName="SRGroup" Font-Overline="False" Font-Size="X-Small" ForeColor="Green" />
                            </div>
                            <div style="float: left; margin-left: 25px">
                                <asp:RadioButton ID="RdoSalesRetAll" runat="server" Text="ALL" Font-Bold="True" GroupName="SRGroup"
                                    Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            </div>
                        </div>
                </asp:Panel>
            </div>
        </div>
        <div class="container-left" id="divVendorPay" runat="server" style="margin-top: 10px">
            <div class="barcode-header">
                Vendor Payments
            </div>
            <asp:Panel ID="Panel4" runat="server" Font-Bold="true">
                <div style="display: table">
                    <div class="control-group" style="margin-top: 10px">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoVendorTrans" runat="server" GroupName="RegularMenu" Checked="True" />
                            <asp:Label ID="Label54" runat="server" Checked="true" Text="Vendor Transactions"></asp:Label>
                            <%--<p style="display: none">testing 1...2...3...</p>--%>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoVendorOutstand" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label55" runat="server" Text="Vendor Outstanding"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoVendorPay" runat="server" GroupName="RegularMenu" AutoPostBack="True"
                                OnCheckedChanged="rdoVendorPay_CheckedChanged" />
                            <asp:Label ID="Label56" runat="server" Text="Vendor Payments"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoCashPay" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label57" runat="server" Text="Cash Payments"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoCheckPayPrepared" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label58" runat="server" Text="Check Payment-Prepared(UnIssued)"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoCheckPayDelivered" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label59" runat="server" Text="Check Payment-Delivered(Issued)"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoCrediNote" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label60" runat="server" Text="Credit Note List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoDebitNote" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label61" runat="server" Text="Debit Note List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoDeletedPay" runat="server" GroupName="RegularMenu" />
                            <asp:Label ID="Label62" runat="server" Text="Deleted Payments"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" id="divDetSum" visible="false" style="float: left; border: 1px solid;
                        margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="PurCH" ID="rdoVendorSumm" runat="server" Text="SUMMARY"
                                Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="PurCH" ID="rdoVendorDetl" runat="server" Text="DETAIL"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                    </div>
            </asp:Panel>
        </div>
        <div class="container-left" id="divMemberList" runat="server" style="margin-top: 10px;">
            <div class="barcode-header">
                Member List Reports
            </div>
            <asp:Panel ID="Panel9" runat="server" Font-Bold="true">
                <div style="display: table">
                    <div class="control-group" style="margin-top: 10px">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMemberList" runat="server" GroupName="MemberMenu" Checked="True" />
                            <asp:Label ID="Label91" runat="server" Text="MemberList"></asp:Label>
                            <asp:CheckBox ID="chkNonVisitor" runat="server" Text="Non Visitor" ForeColor="#FF3300" />
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMemberListByIncome" runat="server" GroupName="MemberMenu" />
                            <asp:Label ID="Label92" runat="server" Text="Batch Wise Stock"></asp:Label>&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMemberListPoint" runat="server" GroupName="MemberMenu" />
                            <asp:Label ID="Label93" runat="server" Text="Member Points"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoMemberListPointAdj" runat="server" GroupName="MemberMenu" />
                            <asp:Label ID="Label94" runat="server" Checked="true" Text="Category wise Stock"></asp:Label>
                            <%--<p style="display: none">testing 1...2...3...</p>--%>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberSalesSumm" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label95" runat="server" Text="Member Sales Summary"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberSalesDet" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label96" runat="server" Text="Member Sales Detail"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMembSalesItem" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label97" runat="server" Text="Member Sales with Items"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoGiftVouchIssued" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label98" runat="server" Text="Gift Voucher - Issued"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoGiftVouchUnIssued" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label99" runat="server" Text="Gift Voucher - Un Issued"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberSalesZone" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label100" runat="server" Text="Member Sales - Zone"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberSalesLevel" runat="server" GroupName="MemberMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoMemberSalesLevel_CheckedChanged" />
                        <asp:Label ID="Label101" runat="server" Text="Member Sales - Level"></asp:Label>&nbsp;&nbsp;
                        <asp:RadioButton ID="chkHigh" runat="server" GroupName="Level" Checked="true" Text="High"
                            ForeColor="#FF3300" />
                        <asp:RadioButton ID="chkMedium" runat="server" GroupName="Level" Text="Mid" ForeColor="#FF3300" />
                        <asp:RadioButton ID="chkLow" runat="server" GroupName="Level" Text="Low" ForeColor="#FF3300" />
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberCount" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label102" runat="server" Text="Member Count"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberChngInvc" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label103" runat="server" Text="Member Change by Invoice "></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoVatExpmtSumm" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label104" runat="server" Text="VAT Exemption - Summary"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoVatExpmtdet" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label105" runat="server" Text="VAT Exemption - Detail"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-bottom: 18px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoMemberSalesCompare" runat="server" GroupName="MemberMenu" />
                        <asp:Label ID="Label106" runat="server" Text="Member Sales - Comparison"></asp:Label>
                        <asp:RadioButton ID="chkyear" runat="server" GroupName="Level2" Checked="true" Text="Year"
                            ForeColor="#FF3300" />
                        <asp:RadioButton ID="chkMonth" runat="server" GroupName="Level2" Text="Month" ForeColor="#FF3300" />
                        <asp:RadioButton ID="chkWeek" runat="server" GroupName="Level2" Text="Week" ForeColor="#FF3300" />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="container-left" id="divStockReports" runat="server" style="margin-top: 10px;">
            <div class="barcode-header">
                Stock Reports
            </div>
            <asp:Panel ID="Panel2" runat="server" Font-Bold="true">
                <div style="display: table">
                    <div class="control-group" style="margin-top: 10px">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoInvStockList" runat="server" GroupName="salesMenu" Checked="True" />
                            <asp:Label ID="Label26" runat="server" Text="Inventory Stock List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoBatchwiseStock" runat="server" GroupName="salesMenu" AutoPostBack="True"
                                OnCheckedChanged="rdoBatchwiseStock_CheckedChanged" />
                            <asp:Label ID="Label27" runat="server" Text="Batch Wise Stock"></asp:Label>&nbsp;&nbsp;
                            <asp:CheckBox ID="chkBatchonly" runat="server" Text="ONLY BATCH ITEM" ForeColor="#FF3300"
                                Visible="False" />
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoDeptStock" runat="server" GroupName="salesMenu" />
                            <asp:Label ID="Label28" runat="server" Text="Department Wise Stock"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoCategoryStock" runat="server" GroupName="salesMenu" />
                            <asp:Label ID="Label11" runat="server" Checked="true" Text="Category wise Stock"></asp:Label>
                            <%--<p style="display: none">testing 1...2...3...</p>--%>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoBrandstock" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label14" runat="server" Text="Brand Wise Stock"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoStockAdjSummary" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label15" runat="server" Text="Stock Adjustment - Summary"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoStockAdjDetail" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label16" runat="server" Text="Stock Adjustment - Detail"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoStockUpdate" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label18" runat="server" Text="Stock Update"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoStockAdjCancel" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label19" runat="server" Text="Stock Adjustment - Cancelled"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoNonBatchStock" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label20" runat="server" Text="Non Batch Wise Stock"></asp:Label>
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoLocStock" runat="server" GroupName="salesMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoLocStock_CheckedChanged" />
                        <asp:Label ID="Label35" runat="server" Text="Location Wise Stock"></asp:Label>&nbsp;&nbsp;
                        <asp:CheckBox ID="chkDntShowZeroQty" runat="server" Text="DONT SHOW ZERO STOCK" ForeColor="#FF3300"
                            Visible="False" />
                    </div>
                </div>
                <div class="control-group">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoStockVAT" runat="server" GroupName="salesMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoStockVAT_CheckedChanged" />
                        <asp:Label ID="Label36" runat="server" Text="Stock List By VAT"></asp:Label>&nbsp;&nbsp;
                        <asp:CheckBox ID="chkDeptVAt" runat="server" Text="DEPT WISE VAT" ForeColor="#FF3300"
                            Visible="False" />
                    </div>
                </div>
                <div class="control-group" style="margin-bottom: 18px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoNonMovItem" runat="server" GroupName="salesMenu" />
                        <asp:Label ID="Label37" runat="server" Text="Non Moving Item"></asp:Label>
                    </div>
                </div>
                <div class="control-group" id="divDateFilter" style="float: left; border: 1px solid;
                    padding: 5px 5px 0 5px; width: 100%" runat="server">
                    <div style="float: left; margin-left: 25px">
                        <asp:CheckBox ID="chkPositive" runat="server" Text="POSITIVE STOCK" Checked="true"
                            Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        &nbsp;&nbsp;
                    </div>
                    <div style="float: left; margin-left: 25px">
                        <asp:CheckBox ID="chkNegative" runat="server" Text="NEGATIVE STOCK" Font-Bold="True"
                            Font-Overline="False" Font-Size="X-Small" ForeColor="Green" />
                    </div>
                    <div style="float: left; margin-left: 25px">
                        <asp:CheckBox ID="chkZerostock" runat="server" Text="ZERO STOCK" Font-Bold="True"
                            Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <div class="container-left" id="divPurchaseOrder" runat="server" style="margin-top: 10px">
            <div class="barcode-header">
                Purchase Order
            </div>
            <asp:Panel ID="pnlPurchaseorder" runat="server" Font-Bold="true">
                <div style="display: table">
                    <div class="control-group" style="margin-top: 10px">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoPoList" runat="server" GroupName="Purmenu" AutoPostBack="True"
                                Checked="True" OnCheckedChanged="rdoPoList_CheckedChanged1" />
                            <asp:Label ID="Label21" runat="server" Text="PO List"></asp:Label>
                            <%--<p style="display: none">testing 1...2...3...</p>--%>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoPOpending" runat="server" GroupName="Purmenu" AutoPostBack="True"
                                OnCheckedChanged="rdoPOpending_CheckedChanged" />
                            <asp:Label ID="lblPriceCh" runat="server" Text="PO Pending List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoPOclosed" runat="server" GroupName="Purmenu" AutoPostBack="True"
                                OnCheckedChanged="rdoPOclosed_CheckedChanged" />
                            <asp:Label ID="Label38" runat="server" Text="PO Closed List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoPOCancel" runat="server" GroupName="Purmenu" AutoPostBack="True"
                                OnCheckedChanged="rdoPOCancel_CheckedChanged" />
                            <asp:Label ID="Label39" runat="server" Text="PO Canceled List"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" style="margin-bottom: 18px">
                        <div style="float: left">
                            <asp:RadioButton ID="rdoPOCount" runat="server" GroupName="Purmenu" AutoPostBack="True"
                                OnCheckedChanged="rdoPOCount_CheckedChanged" />
                            <asp:Label ID="Label40" runat="server" Text="User Wise PO Count"></asp:Label>
                        </div>
                    </div>
                    <div class="control-group" id="divPurFilter" style="float: left; border: 1px solid;
                        margin-bottom: 20px; padding: 5px 5px 0 5px; width: 100%" runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="Pur" ID="rdoApproved" runat="server" Text="APPROVED"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="Pur" ID="rdoUnApproved" runat="server" Text="UNAPPROVED"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="Pur" ID="rdoBoth" runat="server" Text="BOTH" Font-Bold="True"
                                Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                        <div style="float: left;">
                            <asp:RadioButton GroupName="Pur" ID="rdoPending" runat="server" Text="COMPLETE PENDING"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" Checked="true" Visible="false"
                                ForeColor="Red" />
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="Pur" ID="rdoPartPending" runat="server" Text="PARTIAL PENDING"
                                Font-Bold="True" Font-Overline="False" Font-Size="X-Small" Visible="false" ForeColor="Red" />
                        </div>
                    </div>
                    <div class="control-group" id="divSummary" visible="false" style="float: left; border: 1px solid;
                        margin-bottom: 10px; padding: 5px 5px 0 5px; width: 70%" runat="server">
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="PurCH" ID="rdoSummary" runat="server" Text="SUMMARY"
                                Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                            &nbsp;&nbsp;
                        </div>
                        <div style="float: left; margin-left: 25px">
                            <asp:RadioButton GroupName="PurCH" ID="rdoDetail" runat="server" Text="DETAIL" Font-Bold="True"
                                Font-Overline="False" Font-Size="X-Small" ForeColor="Red" />
                        </div>
                    </div>
            </asp:Panel>
        </div>
    </div>
    <div class="container-left" id="divPurchase" runat="server" style="margin-top: 10px">
        <div class="barcode-header">
            Purchase(GRN) Reports
        </div>
        <asp:Panel ID="Panel3" runat="server">
            <div style="display: table">
                <div class="control-group" style="margin-top: 10px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoGAPending" runat="server" AutoPostBack="True" GroupName="PurchaseMenu"
                            Checked="True" OnCheckedChanged="rdoGAPending_CheckedChanged" />
                        <asp:Label ID="Label22" runat="server" Checked="true" Text="GA Pending List" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoDatePurSummary" runat="server" AutoPostBack="True" GroupName="PurchaseMenu"
                            OnCheckedChanged="rdoDatePurSummary_CheckedChanged" />
                        <asp:Label ID="Label25" runat="server" Text="Date Wise Purchase Summary" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoDatePurDetail" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoDatePurDetail_CheckedChanged" />
                        <asp:Label ID="Label24" runat="server" Text="Date Wise Purchase Detail" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoDeptPur" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoDeptPur_CheckedChanged" />
                        <asp:Label ID="Label23" runat="server" Text="Department Wise Purchase" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoCatPur" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoCatPur_CheckedChanged" />
                        <asp:Label ID="Label41" runat="server" Text="Category Wise Purchase" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoBrandPur" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoBrandPur_CheckedChanged" />
                        <asp:Label ID="Label42" runat="server" Text="Brand Wise Purchase" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoClassPur" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoClassPur_CheckedChanged" />
                        <asp:Label ID="Label43" runat="server" Text="Class Wise Purchase" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoVendorPur" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoVendorPur_CheckedChanged" />
                        <asp:Label ID="Label45" runat="server" Text="Vendor Wise Purchase" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoVendorScheDisc" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoVendorScheDisc_CheckedChanged" />
                        <asp:Label ID="Label52" runat="server" Text="Vendor Wise Scheme discount" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoInvGRN" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoInvGRN_CheckedChanged" />
                        <asp:Label ID="Label44" runat="server" Text="Inventory Wise GRN" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoHoldGRN" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoHoldGRN_CheckedChanged" />
                        <asp:Label ID="Label47" runat="server" Text="GRN Hold (pending ) List" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoCancelGRN" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoCancelGRN_CheckedChanged" />
                        <asp:Label ID="Label46" runat="server" Text="Cancelled GRN List" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 2px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoGRNcount" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoGRNcount_CheckedChanged" />
                        <asp:Label ID="Label48" runat="server" Text="User Wise GRN Count" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-top: 4px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoPurRetSumm" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoPurRetSumm_CheckedChanged" />
                        <asp:Label ID="Label49" runat="server" Text="Date Wise Purchase Return Summary" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" style="margin-bottom: 18px; margin-top: 4px">
                    <div style="float: left">
                        <asp:RadioButton ID="rdoPurRetDetail" runat="server" GroupName="PurchaseMenu" AutoPostBack="True"
                            OnCheckedChanged="rdoPurRetDetail_CheckedChanged" />
                        <asp:Label ID="Label50" runat="server" Text="Date Wise Purchase Return Detail" Font-Bold="True"></asp:Label>
                    </div>
                </div>
                <div class="control-group" id="divGroup" visible="false" style="float: left; border: 1px solid;
                    margin-bottom: 10px; padding: 5px 5px 0 5px; width: 80%" runat="server">
                    <div style="float: left;">
                        <asp:Label ID="Label53" runat="server" Text="Group By" Font-Bold="True"></asp:Label>
                    </div>
                    <%-- <div style="float: left; margin-left: 25px">
                            <asp:CheckBoxList ID="chkListDateGroup" runat="Server" RepeatDirection="Horizontal" >
                                <asp:ListItem Text="Date" Value="Date"></asp:ListItem>
                                <asp:ListItem Text="Month" Value="Month"></asp:ListItem> 
                            </asp:CheckBoxList>
                        </div>--%>
                    <div style="float: left; margin-left: 25px">
                        <asp:RadioButton GroupName="Groupfilter" ID="rdoGroupDate" runat="server" Text="DATE"
                            Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" Checked="True" />
                        &nbsp;&nbsp;
                    </div>
                    <div style="float: left; margin-left: 25px">
                        <asp:RadioButton GroupName="Groupfilter" ID="rdoGroupMonth" runat="server" Text="MONTH"
                            Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Green" />
                    </div>
                </div>
                <div class="control-group" id="divDateGroup" style="float: left; border: 1px solid;
                    margin-bottom: 3px; padding: 5px 5px 0 5px; width: 80%" runat="server">
                    <div style="float: left; margin-left: 25px">
                        <asp:CheckBox GroupName="GroupDate" ID="rdoInvDate" runat="server" Text="INVOICE DATE"
                            Font-Bold="True" Checked="true" Font-Overline="False" Font-Size="X-Small" ForeColor="Green"
                            AutoPostBack="true" OnCheckedChanged="rdoInvDate_CheckedChanged" />
                        &nbsp;&nbsp;
                    </div>
                    <div style="float: left; margin-left: 25px">
                        <asp:CheckBox GroupName="GroupDate" ID="rdoGRNdate" runat="server" Text="GRN DATE"
                            Font-Bold="True" Font-Overline="False" Font-Size="X-Small" ForeColor="Red" AutoPostBack="true"
                            OnCheckedChanged="rdoGRNdate_CheckedChanged" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="container-right" id="divfilter" runat="server" style="margin-top: 10px">
        <div class="barcode-header">
            Filteration
        </div>
        <asp:Panel ID="PanelFilter" runat="server">
            <div class="control-group" style="margin-top: 10px">
                <div style="float: left">
                    <asp:Label ID="Label17" runat="server" Text="Inventory" Visible="false"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="rptInvDropBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label2" runat="server" Text="Department"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="rptDeptDropBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label1" runat="server" Text="Category"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="rptCatDropBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label3" runat="server" Text="Brand"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="rptBrandDropBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label6" runat="server" Text="Vendor"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="rptVendorDropBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label29" runat="server" Text="Terminal"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="DropDownTerminalBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group">
                <div style="float: left">
                    <asp:Label ID="Label51" runat="server" Text="Location"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="DropDownLocation" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="control-group" style="margin-bottom: 20px">
                <div style="float: left">
                    <asp:Label ID="Label30" runat="server" Text="Cashier"></asp:Label>
                </div>
                <div style="float: right">
                    <asp:DropDownList ID="DropDownCashierBack" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="container-right" id="div1" runat="server" style="margin-top: 10px; border: 1px solid #d8d9d8;
        width: 435px; height: 200px">
        <div class="barcode-header">
            Date filter
        </div>
        <asp:Panel ID="Panel7" runat="server">
            <div class="control-group" style="margin-top: 5px; margin-left: 15px">
                <div style="float: left">
                    <%--<dx:ASPxDateEdit ID="txtfromDateBack" runat="server" EditFormatString="yyyy-MM-dd"
                            Font-Bold="True" Font-Size="Small" NullText="From Date" Enabled="False">
                        </dx:ASPxDateEdit>--%>
                    <asp:Label ID="Label5" runat="server" Text="From Date" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="txtfromDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div style="float: left; margin-left: 5px">
                    <%-- <dx:ASPxDateEdit ID="txttoDateBack" runat="server" EditFormatString="yyyy-MM-dd"
                            Font-Bold="True" Font-Size="Small" NullText="To Date" Enabled="False">
                        </dx:ASPxDateEdit>--%>
                    <asp:Label ID="Label119" runat="server" Text="To Date" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="txttoDate" runat="server" CssClass="form-control-res"></asp:TextBox>
                </div>
                <div style="float: left; margin-top: 10px">
                    <asp:CheckBox ID="chkBydateBack" runat="server" Text="By Date" Style="margin-top: 20px"
                        Font-Bold="True" />
                </div>
                <div style="float: right; margin-top: 20px">
                    <asp:LinkButton ID="lnkFilterBack" runat="server" class="btn btn-danger" Width="100px"
                        Font-Bold="true" OnClientClick="return showFilter()">Filter</asp:LinkButton>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkLoadReportBack" runat="server" class="btn btn-danger" Width="121px"
                        Font-Bold="true" OnClientClick="return ValidateForm()" OnClick="lnkLoadReportBack_Click">Load Report</asp:LinkButton>&nbsp;&nbsp;
                    <asp:LinkButton ID="lnkClearSearchBack" runat="server" class="btn btn-danger" Width="100px"
                        Font-Bold="true" OnClientClick=" return Clear()">Clear</asp:LinkButton>
                </div>
            </div>
        </asp:Panel>
    </div>
    </div> </div>
    <asp:HiddenField ID="HiddenfromDate" runat="server" ClientIDMode="Static" Value="0" />
    <asp:HiddenField ID="HiddentoDate" runat="server" ClientIDMode="Static" Value="0" />
</asp:Content>
