﻿<%@ Page Title="Branch Stock Movement Report" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="StockMovementBranchwise.aspx.cs" Inherits="EnterpriserWebFinal.Reports.StockMovementBranchwise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <style type="text/css"> 
         .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .LocationNamePostion
        {
            text-align:left;
            padding-left:20px;
        }
    </style>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'StockMovementBranchwise');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "StockMovementBranchwise";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         }


     </script>
<script type="text/javascript">
    var reportTimer = 0;
    function pageLoad() {
        $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });
        $("select").chosen({ width: '100%' });
    }
    function clearForm() {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("input").removeAttr('disabled');

        $("select").val(0);
        $("select").trigger("liszt:updated");

        //$("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy" }).datepicker("setDate", "0");
        $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });
        $("select").chosen({ width: '100%' });
    }
    function LoadReport() {
        //alert($("#<%= txtDate.ClientID %>").datepicker("getDate"));
        $('#statusMessage').text('Loading report....');
        $("#<%= divFilterData.ClientID %>").hide();
        $("#<%= divSummary.ClientID %>").hide();
        $("#divBranchDetails").hide();
        $("#<%= lnkExportToExcel.ClientID %>").hide();

        $("#statusMessageTimer").show();
        $("#statusMessageTimer").text(0);

        reportTimer = 0;
        setInterval(function () {
            reportTimer++;
            $("#statusMessageTimer").text(reportTimer);
        }, 3000);

    }

    //    $(document).ready(function () {
    //        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

    //            $("#<%= txtDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });

    //            $("select").chosen({ width: '100%' });
    //        });
    //    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="#">Management</a> <i class="fa fa-angle-right">
            </i></li>
            <li class="active-page">Branch Stock Movement Report</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="Server">
    <Triggers><asp:PostBackTrigger ControlID="lnkExportToExcel" /></Triggers>
    <ContentTemplate>
    <div class="container-group-small">
        <div class="container-control">                
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblGroupBy" runat="server" Text="Group By"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:DropDownList runat="server" ID="ddlGroupBy" CssClass="form-control-res">
                            <asp:ListItem Text="Inventory Items" Value="" />
                            <asp:ListItem Text="Category" Value="C" />
                            <asp:ListItem Text="Brand" Value="B" />
                            <asp:ListItem Text="Department" Value="D" />
                        </asp:DropDownList>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblLocation" runat="server" Text="Branch"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:DropDownList runat="server" ID="ddlLocation" CssClass="form-control-res">
                            <asp:ListItem Text="All Branches" Value="" />
                           <%-- <asp:ListItem Text="JAYAM SUPER MARKET" Value="JSM" />
                            <asp:ListItem Text="SRI GOPAL GREENS" Value="SGG" />
                            <asp:ListItem Text="SVG RETAIL SHOP" Value="SVGR" />

                            <%--<asp:ListItem Text="AMBATTUR O.T" Value="AMOT" />
                            <asp:ListItem Text="Besant Nagar" Value="BN" />
                            <asp:ListItem Text="KOYAMBEDU" Value="CMBT" />
                            <asp:ListItem Text="COLLECTOR NAGAR" Value="COL" />
                            <asp:ListItem Text="CENTRAL RAILWAY  STATION" Value="CRS" />
                            <asp:ListItem Text="RES MOGAPPAIRWEST" Value="GRMW" />
                            <asp:ListItem Text="BLUE STAR" Value="GSBS" />
                            <asp:ListItem Text="CHINTHAMANI" Value="GSCM" />
                            <asp:ListItem Text="SELAIYUR" Value="GSCR" />
                            <asp:ListItem Text="KOLATHUR" Value="GSKT" />
                            <asp:ListItem Text="GANGA NOLAMBUR OLD" Value="GSMW2" />
                            <asp:ListItem Text="GANGA NOLAMBUR NEW" Value="GSN" />
                            <asp:ListItem Text="PALLAVARAM" Value="GSP" />
                            <asp:ListItem Text="SCHOOL ROAD" Value="GSSR" />
                            <asp:ListItem Text="HEAD OFFICE" Value="GST" />
                            <asp:ListItem Text="VELACHERY" Value="GSV" />
                            <asp:ListItem Text="VASALARAWAKKAM" Value="GSVW" />
                            <asp:ListItem Text="WAREHOUSE" Value="GSWH" />
                            <asp:ListItem Text="GANGAVEG-R-KICTHEN" Value="GVRK" />
                            <asp:ListItem Text="HQ" Value="HQ" />
                            <asp:ListItem Text="KUMAR R" Value="KMR" />
                            <asp:ListItem Text="KNR" Value="KNR" />
                            <asp:ListItem Text="MEDAVAKKAM" Value="MDK" />
                            <asp:ListItem Text="MTH" Value="MTH" />
                            <asp:ListItem Text="Navalur" Value="NAV" />
                            <asp:ListItem Text="Ambattur OT2" Value="OT2" />
                            <asp:ListItem Text="PADUR" Value="PDR" />
                            <asp:ListItem Text="Purasawalkam-1" Value="PW1" />
                            <asp:ListItem Text="Purasawalkam-2" Value="PW2" />
                            <asp:ListItem Text="RED HILLS" Value="RHS" />
                            <asp:ListItem Text="RAMAPURAM" Value="RPM" />
                            <asp:ListItem Text="TMS" Value="TMS" />
                            <asp:ListItem Text="T NAGAR" Value="TNR" />
                            <asp:ListItem Text="TVSFACTORY" Value="TVSF" />
                            <asp:ListItem Text="VENKATESWARA BOLI STALL" Value="VBS" />
                            <asp:ListItem Text="VELACHERY NEW" Value="VC1" />
                            <asp:ListItem Text="VEG RESTAURENT MOGAPPAIR EAST" Value="VRMW" />--%>

                        </asp:DropDownList>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtDate" runat="server" CssClass="form-control-res" onkeydown="return false;"></asp:TextBox>
                </div>
            </div>               
        </div>
        <div style="float:left">
        <div class="control-button">
            <asp:LinkButton ID="lnkExportToExcel" runat="server" class="button-blue" OnClick="lnkExportToExcel_Click"
                Text='<%$ Resources:LabelCaption,btn_Export %>'></asp:LinkButton>
        </div>
        </div>
        <div style="float:right">
        <div class="button-contol">
            <div class="control-button">
                <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-red" OnClick="lnkLoadReport_Click" 
                OnClientClick="LoadReport()">Load Report</asp:LinkButton>
            </div>
           <%-- <div class="control-button">
                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm();return false;"
                    ><i class="icon-play"></i>Clear</asp:LinkButton>
            </div>--%>
        </div>
        </div>        
    </div>
    <span style="color:Red" id="statusMessage"></span><span id="statusMessageTimer"  style="color:Red;display:none;"></span>
   <div style="margin:10px 0 10px 0;font-weight:bold;color:#0E72B5" id="divFilterData" runat="server" visible="false">
       <span style="color:Black">Filter:</span>
       <asp:Label ID="lblGroupBySelected" Text="" runat="server" /> |        
       <asp:Label ID="lblBranchSelected" Text="" runat="server" /> |
       <asp:Label ID="lblDateSelected" Text="" runat="server" />
    </div>
    <a name = "Summary"></a>
    <div class="gridDetails" id="divSummary" runat="server" style="background-color:#f3f3f3" visible="false">
        <div style="margin:10px 0 10px 0;font-weight:bold;">
            Branch-wise Movement Summary
        </div>
        <asp:GridView ID="gvBranchTransactionSummary" runat="server" AllowSorting="false" AutoGenerateColumns="false"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" AllowPaging="false" OnRowDataBound="gvBranchTransactionSummary_RowDataBound">
            <EmptyDataTemplate>
                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
            <RowStyle CssClass="pshro_GridDgnStyle" />
            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />                        
            <Columns>
            <asp:BoundField DataField="Sl.No" HeaderText="Sl.No"></asp:BoundField>
            <asp:BoundField DataField="LocationCode" HeaderText="LocationCode"></asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFormatString="#{0}" DataNavigateUrlFields="LocationCode" DataTextField="LocationName" HeaderText="LocationName" HeaderStyle-CssClass="LocationNamePostion" ItemStyle-CssClass="LocationNamePostion" />
            <asp:BoundField DataField="LastSyncAt" HeaderText="LastSyncAt"></asp:BoundField>
            <asp:BoundField DataField="TransferInQty" HeaderText="TransferInQty"></asp:BoundField>
            <asp:BoundField DataField="TransferOutQty" HeaderText="TransferOutQty"></asp:BoundField>
            <asp:BoundField DataField="SalesQty" HeaderText="SalesQty"></asp:BoundField>
            <asp:BoundField DataField="AdjQty" HeaderText="AdjQty"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="gridDetails" id="divBranchDetails">
        <asp:Repeater runat="server" ID="rptBranchDetails" OnItemDataBound="rptBranchDetails_OnItemDataBound">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                <div style="margin:12px 0 2px 10px;font-weight:bold;height:20px;width:98%;">
                    <div style="float:left">
                        <a name="<%# Eval("LocationCode")%>"></a>
                        <%# Eval("LocationName")%> <span>(</span>
                        <span>Transfer-In Qty : </span><asp:Label ID="lblTransferInQty" Text="0" runat="server" />
                        <span> | Transfer-Out Qty : </span><asp:Label ID="lblTransferOutQty" Text="0" runat="server" />
                        <span> | SalesQty : </span><asp:Label ID="lblSalesQty" Text="0" runat="server" />
                        <span> | AdjQty : </span><asp:Label ID="lblAdjQty" Text="0" runat="server" />
                        <span>)</span>
                        <span style="color:<%# GetSyncDateColor(Eval("LastSyncAt").ToString())%>"> Last Sync : <%# Eval("LastSyncAt")%></span>
                    </div>
                    <div style="float:right">
                    <%--<span style="font-weight:bold;"><a href="#Summary">Top</a></span>--%>
                        <a href="#Summary" style="font-weight:bold;">Goto Summary <span class="glyphicon glyphicon-upload" aria-hidden="true"></span></a>
                    </div>                    
                </div>
                <div style="margin:0 10px 10px 10px;max-height:400px;overflow:auto;">
                    <asp:GridView ID="gvBranchTransaction" runat="server" AllowSorting="false" AutoGenerateColumns="true"
                        ShowHeaderWhenEmpty="true" OnRowDataBound="gvBranchTransaction_OnRowDataBound"
                        CssClass="pshro_GridDgn" AllowPaging="false">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />                        
                        <Columns>                       
                            <%--<asp:BoundField DataField="InventoryCode" HeaderText="Inventory Code"></asp:BoundField>
                            <asp:BoundField DataField="BatchNo" HeaderText="BatchNo"></asp:BoundField>
                            <asp:BoundField DataField="ItemDesc" HeaderText="Description"></asp:BoundField>--%>
                           <%-- <asp:BoundField DataField="CategoryCode" HeaderText="CategoryCode"></asp:BoundField>--%>
                            <%--<asp:BoundField DataField="CategoryName" HeaderText="CategoryName"></asp:BoundField>--%>
                            <%--<asp:BoundField DataField="BrandCode" HeaderText="BrandCode"></asp:BoundField>--%>
                            <%--<asp:BoundField DataField="BrandName" HeaderText="BrandName"></asp:BoundField>--%>
                            <%--<asp:BoundField DataField="DeptCode" HeaderText="DeptCode"></asp:BoundField>--%>
                            <%--<asp:BoundField DataField="DeptName" HeaderText="DeptName"></asp:BoundField>
                            <asp:BoundField DataField="TransferQty" HeaderText="TransferQty"></asp:BoundField>
                            <asp:BoundField DataField="SalesQty" HeaderText="SalesQty"></asp:BoundField>
                            <asp:BoundField DataField="AdjQty" HeaderText="AdjQty"></asp:BoundField>--%>
                                                        
                            <%--<asp:BoundField DataField="Active" HeaderText="Active" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol"></asp:BoundField>--%>
                        </Columns>
                    </asp:GridView>
                </div>
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
        </asp:Repeater>                                        
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
