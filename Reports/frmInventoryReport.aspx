﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="frmInventoryReport.aspx.cs" Inherits="EnterpriserWebFinal.Reports.frmInventoryReport" %>


<%@ Register TagPrefix="ups" TagName="ReportFilterUserControl" Src="~/UserControls/ReportFilterUserControl.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

    </style>
    <link href="../css/ziehharmonika.css" rel="stylesheet" type="text/css" />
    <%--------------------------------- js file  ------------------------------------%>
    <script src="../js/ChosenDropdown/ziehharmonika.js" type="text/javascript"></script>
    
     <script type="text/javascript">
         var d1;
         var d2;
         var Opendate;
         var dur;
         function fncGetUrl() {
             fncSaveHelpVideoDetail('', '', 'InventoryReport');
         }
         function fncOpenvideo() {

             document.getElementById("ifHelpVideo").src = HelpVideoUrl;

             var Mode = "InventoryReport";
             var d = new Date($.now());
             Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
             d1 = new Date($.now()).getTime();



             $("#dialog-Open").dialog({
                 autoOpen: true,
                 resizable: false,
                 height: "auto",
                 width: 1093,
                 modal: true,
                 dialogClass: "no-close",
                 buttons: {
                     Close: function () {
                         $(this).dialog("destroy");
                         var d = new Date($.now());
                         var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                         d2 = new Date($.now()).getTime();
                         var Diff = Math.floor((d2 - d1) / 1000);
                         //alert(Diff);
                         if (Diff >= 60) {
                             fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                         }

                     }
                 }
             });
         } 
     </script> 
    <script type="text/javascript">

        $(document).ready(function () {
            $(document).on('keydown', disableFunctionKeys);
        });
        function disableFunctionKeys(e) {
            var functionKeys = new Array(112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123);
            if (functionKeys.indexOf(e.keyCode) > -1 || functionKeys.indexOf(e.which) > -1) {

                if (e.keyCode == 112) {
                    e.preventDefault();
                }
            }
        };

        function pageLoad() {

   


            $("[id*=divBatchcount]").hide();
            $("[id*=txtItemCode]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmInventoryReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    event.preventDefault();
                },
                //==========================> After Select Value
                select: function (e, i) {
                    $("[id*=txtItemCode]").val(i.item.val);
                    return false;
                },
                // minLength: 1
            });

            $("[id*=txtDistributionNo]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmInventoryReport.aspx/GetDistributionNo") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtDistributionNo]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtDistributionNo]").val(i.item.val);
                    return false;
                },
            });

            $("[id*=txtBillNo]").autocomplete({
                source: function (request, response) {
                    var obj = {};
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmInventoryReport.aspx/GetBillNo") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=txtBillNo]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=txtBillNo]").val(i.item.val);
                    return false;
                },
            });

            if ($("#<%=chkCompleteInfo.ClientID %>").is(':checked')) {
                $("#subDiv").css("display", "block");
            }
            else {
                $("#subDiv").css("display", "none");
                $("[id*=chkItemWise]").attr("checked", false);
                $("[id*=chkBatchwise]").attr("checked", false);
            }
            if ($("#<%=chkInventoryList.ClientID %>").is(':checked')) {
                $("#complete").css("display", "block");
            }
            else {
                $("#complete").css("display", "none");
                $("[id*=chkCompleteInfo]").attr("checked", false);
                $("[id*=chkMarginnotSet]").attr("checked", false);
                if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked') || $("#<%=rdoParantChild.ClientID%>").is(':checked')) { // dinesh
                    $("[id*=ContentPlaceHolder1_chkActive]").attr("checked", false);
                    $("[id*=ContentPlaceHolder1_chkActive]").attr("disabled", "disabled");
                }
                else {
                    $("[id*=ContentPlaceHolder1_chkActive]").removeAttr("disabled");
                }
                if ($("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked')) {
                    $("[id*=chkBydate]").attr("checked", true);
                    $("[id*=chkBydate]").attr("disabled", "disabled");
                    }
                    else {
                    $("[id*=chkBydate]").removeAttr("disabled");
                    }
            }
            if ($("#<%=rdoExpired.ClientID%>").is(':checked')) { // dinesh
                $("[id*=ContentPlaceHolder1_chkVendor]").removeAttr("disabled");
                $("[id*=ContentPlaceHolder1_chkVendor]").attr("checked", false);
                 $("#<%=txtToDate.ClientID%>").datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            else {
                $("[id*=ContentPlaceHolder1_chkVendor]").attr("checked", false);
                $("[id*=ContentPlaceHolder1_chkVendor]").attr("disabled", "disabled");
            }
            var Bydatevalue = $("#<%=chkBydate.ClientID%>").is(':checked');
            var Bydatevaluestock = $("#<%=chkBydateStock.ClientID%>").is(':checked');

            if (!Bydatevalue) {
                $("[id*=txtFromDate]").attr("disabled", "disabled");
                $("[id*=txtToDate]").attr("disabled", "disabled");
            }
            else {
                $("[id*=txtFromDate]").removeAttr("disabled");
                $("[id*=txtToDate]").removeAttr("disabled");
            }

            if (!Bydatevaluestock) {
                $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                $("[id*=txtToDateStock]").attr("disabled", "disabled");
            }
            else {
                $("[id*=txtFromDateStock]").removeAttr("disabled");
                $("[id*=txtToDateStock]").removeAttr("disabled");
            }

            // dinesh
            if ($("#<%=chkInventoryList.ClientID%>").is(':checked') || $("#<%=chkDepartmantWise.ClientID%>").is(':checked') || $("#<%=chkCategoryWise.ClientID%>").is(':checked') || $("#<%=chkSubCategoryWise.ClientID%>").is(':checked') || $("#<%=rdoVendorWise.ClientID%>").is(':checked') || $("#<%=chkBrandWise.ClientID%>").is(':checked') || $("#<%=Chkbarcodelist.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked') || $("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoBreakqty.ClientID%>").is(':checked') || $("#<%=rdoDeactiveitem.ClientID%>").is(':checked') || $("#<%=rdoDeactiveBatch.ClientID%>").is(':checked') || $("#<%=rdoitemChangelog.ClientID%>").is(':checked') || $("#<%=RdoPriceChange.ClientID%>").is(':checked') || $("#<%=rdoBatchWiseInvlist.ClientID%>").is(':checked') || $("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoExpired.ClientID%>").is(':checked') || $("#<%=rdoLessNetCost.ClientID%>").is(':checked') || $("#<%=rdoMUomWprice.ClientID%>").is(':checked') || $("#<%=rdoUomMPriceChage.ClientID%>").is(':checked') || $("#<%=rdoInvMov.ClientID%>").is(':checked')
                || $("#<%=rdoInvVal.ClientID%>").is(':checked') || $("#<%=rdoParantChild.ClientID%>").is(':checked')) {
                //clearForm();
                $("[id*=GrpInventoryListReportFilter]").show();
              
                if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked')) { 
                    $("[id*=divLocation]").show();
                    $("[id*=divItemcode]").hide();
                    $("[id*=divDepartment]").hide();
                    $("[id*=divCategory]").hide();
                    $("[id*=divSubCategory]").hide();
                    $("[id*=divBrand]").hide();
                    $("[id*=divVendor]").hide();
                    $("[id*=divClass]").hide();
                    $("[id*=divSubClass]").hide();
                    $("[id*=divFloor]").hide();
                    $("[id*=divSection]").hide();
                }
                else if ($("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked')) { 
                        $("[id*=divItemcode]").show();
                        $("[id*=divLocation]").hide();
                        $("[id*=divDepartment]").show();
                        $("[id*=divCategory]").show();
                        $("[id*=divSubCategory]").show();
                        $("[id*=divBrand]").show();
                        $("[id*=divVendor]").show();
                        $("[id*=divClass]").hide();
                        $("[id*=divSubClass]").hide();
                        $("[id*=divFloor]").show();
                        $("[id*=divSection]").show();
                    }
                else {
                    $("[id*=divItemcode]").show();
                    $("[id*=divLocation]").hide();                   
                    $("[id*=divDepartment]").show();
                    $("[id*=divCategory]").show();
                    $("[id*=divSubCategory]").show();
                    $("[id*=divBrand]").show();
                    $("[id*=divVendor]").show();
                    $("[id*=divClass]").show();
                    $("[id*=divSubClass]").show();
                    $("[id*=divFloor]").show();
                    $("[id*=divSection]").show();
                }
                 if ($("#<%=hidMultiComany.ClientID%>").val() == "Y") {
                    if ($("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked')) {
                        $("[id*=divLocation]").show();
                        $('#ContentPlaceHolder1_ReportFilterUserControl_txtLocation').val($("#<%=hidMinMaxLoc.ClientID%>").val()); 
                    }
                }
            }
            else {
                $("[id*=GrpInventoryListReportFilter]").hide();
            }

            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Inventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Department' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Category' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Brand' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Location' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum') {
                $("[id*=chkBydateStock]").attr("Enabled", "Enabled");
                $("[id*=chkBydateStock]").attr("checked", false);
                $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                $("[id*=txtToDateStock]").attr("disabled", "disabled");
            }
            else {
                $("[id*=chkBydateStock]").removeAttr("disabled");
                $("[id*=chkBydateStock]").attr("checked", true);
                $("[id*=txtFromDateStock]").removeAttr("disabled");
                $("[id*=txtToDateStock]").removeAttr("disabled");
            }

            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Summary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Detail' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Cancel' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Update' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum' ) {
                $("[id*=GroupBy]").hide();
            }
            else {
                $("[id*=GroupBy]").show();
            }
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch') {
                $("[id*=GroupByBatch]").show();
            }
            else {
                $("[id*=GroupByBatch]").hide();
            }

            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
                $('#ContentPlaceHolder1_divBinNo').show();
            }
            else {
                $('#ContentPlaceHolder1_divBinNo').hide();
            }

            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Inventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch')
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                else
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').show();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch')
                    $('#ContentPlaceHolder1_divBatchNo').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                $("[id*=GroupBy]").hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Department' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Category' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'SubCategory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Brand' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Summary')//aalayam 19042018
            {
                $('#ContentPlaceHolder1_divStockAdjNo').show();
                $('#ContentPlaceHolder1_divStockAdjType').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Detail') {
                $('#ContentPlaceHolder1_divStockAdjNo').show();
                $('#ContentPlaceHolder1_divStockAdjType').show();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divBatchNo').show();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Cancel') {
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').show();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').show();
            }//
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Update') {
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockTakeId').show();
                $('#ContentPlaceHolder1_divStockAdjType').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divBatchNo').show();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Location') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $("[id*=GroupBy]").hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonMoving') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').show();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }

            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DateWise') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $("[id*=GroupBy]").hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySales'
       || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySummury') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divBinNo').show();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $("[id*=GroupBy]").hide();
                $("[id*=chkBydateStock]").attr("disabled", "disabled");
                $("[id*=chkBydateStock]").attr("checked", false);
                $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                $("[id*=txtToDateStock]").attr("disabled", "disabled");
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'StyleCode') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                $('#ContentPlaceHolder1_divBatchNo').show();
                $("[id*=GroupBy]").hide();
            }
            else {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                $('#ContentPlaceHolder1_divStockAdjNo').hide();
                $('#ContentPlaceHolder1_divStockAdjType').hide();
                $('#ContentPlaceHolder1_divStockTakeId').hide();
                $('#ContentPlaceHolder1_divBatchNo').hide();
                $('#ContentPlaceHolder1_divBillNoDelete').hide();
                $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                $("[id*=GroupBy]").hide();
                $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            }
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Distribution' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();          
        }
        else
         $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
        $('#ContentPlaceHolder1_divStockAdjNo').hide();
        $('#ContentPlaceHolder1_divStockAdjType').hide();
        $('#ContentPlaceHolder1_divStockTakeId').hide();
        $('#ContentPlaceHolder1_divDistributionNo').show();
        $('#ContentPlaceHolder1_divBillNo').show();
        $('#ContentPlaceHolder1_divBatchNo').hide();
        $('#ContentPlaceHolder1_divBillNoDelete').hide();
        $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
        $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        $("[id*=GroupBy]").hide();
        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_divDistributionNo').hide();
            $('#ContentPlaceHolder1_divBillNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').show();
            $('#ContentPlaceHolder1_divDistributionNoDelete').show();
        }
    }
    else {
        $('#ContentPlaceHolder1_divDistributionNo').hide();
        $('#ContentPlaceHolder1_divBillNo').hide();
    }

    if ($("#<%=HiddenLocationCheck.ClientID%>").val() == 'HQ') { //11042018
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonMoving') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
        }
    }
    else {
        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
    }

    $('#<%= rdoStockReport.ClientID %> input').click(function () {
                var checkedstockvalue = $("#<%=rdoStockReport.ClientID%>").find(":checked").val();
        $('#ContentPlaceHolder1_txtDistributionNo').val('');
        $('#ContentPlaceHolder1_txtDistributionNoDelete').val('');
        $('#ContentPlaceHolder1_txtBillNo').val('');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtInventory').val('');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtDepartment').val('');
        $('#ContentPlaceHolder1_txtBatchNo').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlDept').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtCategory').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlCategory').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtBrand').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlBrand').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtVendor').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlVendor').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtClass').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlClass').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtSubClass').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlSubClass').trigger("liszt:updated");
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtWarehouse').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlWarehouse').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtFloor').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlFloor').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtCompany').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlCompany').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtManufacture').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlManufacture').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtSection').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlSection').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtSubCategory').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlSubCategory').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtLocation').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlLocation').trigger('liszt:updated');
        $('#ContentPlaceHolder1_txtStockAdjNo').val('');
        //$('#ContentPlaceHolder1_ddlStockAdjNo').trigger('liszt:updated');
        $('#ContentPlaceHolder1_txtStockAdjType').val('');
        //$('#ContentPlaceHolder1_ddlStockAdjType').trigger('liszt:updated');
        $('#ContentPlaceHolder1_txtStockTakeId').val('');
        //$('#ContentPlaceHolder1_ddlStockTakeId').trigger('liszt:updated');
        $('#ContentPlaceHolder1_StockReportFilterUserControl_txtMerchandise').val('');
        //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlMerchandise').trigger('liszt:updated');
        $('#ContentPlaceHolder1_txtBinNo').val('');
        //$('#ContentPlaceHolder1_ddBinNo').trigger('liszt:updated');
        //                    
        if (checkedstockvalue == 'Inventory' || checkedstockvalue == 'Batch' || checkedstockvalue == 'Department' || checkedstockvalue == 'Category' || checkedstockvalue == 'SubCategory' || checkedstockvalue == 'Brand' || checkedstockvalue == 'NonBatch' || checkedstockvalue == 'Location' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
            $("[id*=chkBydateStock]").attr("disabled", "disabled");
            $("[id*=chkBydateStock]").attr("checked", false);
            $("[id*=txtFromDateStock]").attr("disabled", "disabled");
            $("[id*=txtToDateStock]").attr("disabled", "disabled");
        }
        else {
            $("[id*=chkBydateStock]").removeAttr("disabled");
            $("[id*=chkBydateStock]").attr("checked", true);
            $("[id*=txtFromDateStock]").removeAttr("disabled");
            $("[id*=txtToDateStock]").removeAttr("disabled");
        }
        if ($("#<%=HiddenLocationCheck.ClientID%>").val() == 'HQ') { //11042018
            if (checkedstockvalue == 'Batch' || checkedstockvalue == 'NonMoving') {
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                //$('#ContentPlaceHolder1_StockReportFilterUserControl_Label5').hide();
                //$('#ContentPlaceHolder1_StockReportFilterUserControl_ddlLocation_chzn').val('');
                //$('##ContentPlaceHolder1_StockReportFilterUserControl_ddlLocation_chzn').trigger("liszt:updated");
            }
        }
        else {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
        }
        if (checkedstockvalue == 'Inventory')//aalayam 18042018
        {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
        }
        if (checkedstockvalue == 'Summary' || checkedstockvalue == 'Detail' || checkedstockvalue == 'Cancel' || checkedstockvalue == 'Update') {
            $("[id*=GroupBy]").hide();
        }
        else {
            $("[id*=GroupBy]").show();
        }
        if (checkedstockvalue == 'Batch') {
            $("[id*=GroupByBatch]").show();
        }
        else {
            $("[id*=GroupByBatch]").hide();
        }

        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
            $('#ContentPlaceHolder1_divBinNo').show();
        }
        else {
            $('#ContentPlaceHolder1_divBinNo').hide();
        }

        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Inventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch')
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            else
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_divBatchNo').show();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch')
                $('#ContentPlaceHolder1_divBatchNo').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $("[id*=GroupBy]").hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Department' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Category' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'SubCategory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Brand' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').show();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySales'
            || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySummury') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divBinNo').show();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $("[id*=GroupBy]").hide();
            $("[id*=chkBydateStock]").attr("disabled", "disabled");
            $("[id*=chkBydateStock]").attr("checked", false);
            $("[id*=txtFromDateStock]").attr("disabled", "disabled");
            $("[id*=txtToDateStock]").attr("disabled", "disabled");
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Summary')  //aalayam 19042018
        {
            $('#ContentPlaceHolder1_divStockAdjNo').show();
            $('#ContentPlaceHolder1_divStockAdjType').show();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Detail') {
            $('#ContentPlaceHolder1_divStockAdjNo').show();
            $('#ContentPlaceHolder1_divStockAdjType').show();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').show();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Cancel') {
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').show();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').show();
        }//
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Update') {
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockTakeId').show();
            $('#ContentPlaceHolder1_divStockAdjType').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').show();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Location') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $("[id*=GroupBy]").hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonMoving') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').show();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DateWise') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $("[id*=GroupBy]").hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }
        else {
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
        }

        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Distribution' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail'
            || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail')
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            else
                $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
            $('#ContentPlaceHolder1_divBatchNo').hide();
            $('#ContentPlaceHolder1_divStockAdjNo').hide();
            $('#ContentPlaceHolder1_divStockAdjType').hide();
            $('#ContentPlaceHolder1_divStockTakeId').hide();
            $('#ContentPlaceHolder1_divDistributionNo').show();
            $('#ContentPlaceHolder1_divBillNo').show();
            $('#ContentPlaceHolder1_divBillNoDelete').hide();
            $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
            $("[id*=GroupBy]").hide();
            $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
            if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_divDistributionNo').hide();
            $('#ContentPlaceHolder1_divBillNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').show();
            $('#ContentPlaceHolder1_divDistributionNoDelete').show();
        }
        }
        else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'StyleCode') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_divBatchNo').show();
            $("[id*=GroupBy]").hide();
        }
        else {
            $('#ContentPlaceHolder1_divDistributionNo').hide();
            $('#ContentPlaceHolder1_divBillNo').hide();
        }

        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Location') {
            $('#GroupBy').hide();
            $('#divZeroQty').show();
        }
        else {
            $('#divZeroQty').hide();
        }
        if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() != 'StyleCode') {
            $('#ContentPlaceHolder1_lblBatchNo').text('Batch No');
        }
    });
            //
            // SetAutoComplete();

    $('.ziehharmonika').ziehharmonika({
        highlander: true,
        collapsible: true,
        prefix: '★'//'♫'//,
        //collapseIcons: {
        //    opened: ' ',
        //    closed: ' '
        //}
    });

            //$('.ziehharmonika h3:eq(0)').ziehharmonika('open', true);
            //$("select").chosen({ width: '100%'}); 

    SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
    SetDefaultDate($("#<%= txtFromDateStock.ClientID %>"), $("#<%= txtToDateStock.ClientID %>"));


            $('#<%=chkBydate.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
            });

            $('#<%=chkBydateStock.ClientID%>').change(function () {
                if (this.checked) {
                    $("[id*=txtFromDateStock]").removeAttr("disabled");
                    $("[id*=txtToDateStock]").removeAttr("disabled");
                }
                else {
                    $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                    $("[id*=txtToDateStock]").attr("disabled", "disabled");
                }
            });

            if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoExpired.ClientID%>").is(':checked')) { // dinesh
                SetManualDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            }
            else {
                SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            }
        }

        <%--function SetAutoComplete() {
            ctrltxtInventory = $("#<%= StockReportFilterUserControl.GetFilterControl<TextBox>(EnterpriserWebFinal.UserControls.ReportFilterUserControl.FilterControls.InventoryTextBox).ClientID %>");
            ctrltxtInventory.autocomplete({
                source: function (request, response) {
                    var obj = {};
                    //obj.prefix = request.term.replace("'", "''");
                    obj.prefix = escape(request.term);
                    $.ajax({
                        url: '<%=ResolveUrl("frmInventoryReport.aspx/GetFilterValue") %>',
                        data: JSON.stringify(obj),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1],
                                    desc: item.split('/')[2]
                                }
                            }))
                        },
                        error: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        },
                        failure: function (response) {
                            ShowPopupMessageBox(response.responseText);
                        }
                    });
                },
                focus: function (event, i) {
                    $("[id*=HidenInventoryName]").val(i.item.label);
                    $("[id$=txtInventory]").val(i.item.val);
                    event.preventDefault();
                },
                select: function (e, i) {
                    $("[id*=HidenInventoryName]").val(i.item.label);
                    $("[id$=txtInventory]").val(i.item.val);
                    return false;
                },
            });
        }--%>


        $(function () {
            SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
            SetDefaultDate($("#<%= txtFromDateStock.ClientID %>"), $("#<%= txtToDateStock.ClientID %>"));
        });


        function SetDefaultDate(FromDate, Todate) {
            FromDate.datepicker('destroy');
            Todate.datepicker('destroy');
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "0" }).datepicker("setDate", "0");
            }
            if ($("#<%=rdoExpired.ClientID%>").is(':checked')) {
                Todate.datepicker('destroy');
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }

        function SetManualDate(FromDate, Todate) {
            FromDate.datepicker('destroy');
            Todate.datepicker('destroy');
            FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true });
            if (FromDate.val() === '') {
                FromDate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
            if (Todate.val() === '') {
                Todate.datepicker({ dateFormat: "dd-mm-yy", changeMonth: true, changeYear: true, showButtonPanel: true }).datepicker("setDate", "0");
            }
        }


        $(document).ready(function () {

            $("input:radio").change(function () {

                if ($("#<%=rdoBatchWiseInvlist.ClientID %>").is(':checked')) {
                    $("[id*=divBatchcount]").show();
                }
                else {
                    $("[id*=divBatchcount]").hide();
                }

                if ($("#<%=chkInventoryList.ClientID %>").is(':checked')) {
                    $("#complete").css("display", "block");
                }
                else {
                    $("#complete").css("display", "none");
                    $("[id*=chkCompleteInfo]").attr("checked", false);
                    $("[id*=chkMarginnotSet]").attr("checked", false);

                    if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked') || $("#<%=rdoParantChild.ClientID%>").is(':checked')) { // dinesh
                        $("[id*=ContentPlaceHolder1_chkActive]").attr("checked", false);
                        $("[id*=ContentPlaceHolder1_chkActive]").attr("disabled", "disabled");
                    } 
                    else {
                        $("[id*=ContentPlaceHolder1_chkActive]").removeAttr("disabled");
                    }
                    if ($("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked')) { 
                        $("[id*=chkBydate]").attr("checked", true);
                        $("[id*=chkBydate]").attr("disabled", "disabled");
                    }
                    else {
                        $("[id*=chkBydate]").removeAttr("disabled");
                    }
                }
                if ($("#<%=rdoExpired.ClientID%>").is(':checked')) { // dinesh
                    $("[id*=ContentPlaceHolder1_chkVendor]").removeAttr("disabled");
                    $("[id*=ContentPlaceHolder1_chkVendor]").attr("checked", false);
                    
                }
                else {
                    $("[id*=ContentPlaceHolder1_chkVendor]").attr("checked", false);
                    $("[id*=ContentPlaceHolder1_chkVendor]").attr("disabled", "disabled");
                }

                if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoExpired.ClientID%>").is(':checked')) { // dinesh
                    SetManualDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                }
                else {
                    SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                }

                if ($("#<%=chkCompleteInfo.ClientID %>").is(':checked')) {
                    $("#subDiv").css("display", "block");
                }
                else {
                    $("#subDiv").css("display", "none");
                    $("[id*=chkItemWise]").attr("checked", false);
                    $("[id*=chkBatchwise]").attr("checked", false);
                }
                // dinesh
                if ($("#<%=chkInventoryList.ClientID%>").is(':checked') || $("#<%=chkDepartmantWise.ClientID%>").is(':checked') || $("#<%=chkCategoryWise.ClientID%>").is(':checked') || $("#<%=chkSubCategoryWise.ClientID%>").is(':checked') || $("#<%=rdoVendorWise.ClientID%>").is(':checked') || $("#<%=chkBrandWise.ClientID%>").is(':checked') || $("#<%=Chkbarcodelist.ClientID%>").is(':checked') || $("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked') || $("#<%=rdoBreakqty.ClientID%>").is(':checked') || $("#<%=rdoDeactiveitem.ClientID%>").is(':checked') || $("#<%=rdoDeactiveBatch.ClientID%>").is(':checked') || $("#<%=rdoitemChangelog.ClientID%>").is(':checked') || $("#<%=RdoPriceChange.ClientID%>").is(':checked') || $("#<%=rdoBatchWiseInvlist.ClientID%>").is(':checked') || $("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoExpired.ClientID%>").is(':checked') || $("#<%=rdoLessNetCost.ClientID%>").is(':checked') || $("#<%=rdoMUomWprice.ClientID%>").is(':checked') || $("#<%=rdoUomMPriceChage.ClientID%>").is(':checked') || $("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked')
                    || $("#<%=rdoParantChild.ClientID%>").is(':checked')) {
                    //clearForm();
                    $("[id*=GrpInventoryListReportFilter]").show(); 
                    
                    if ($("#<%=rdoPromotion.ClientID%>").is(':checked') || $("#<%=rdoPromotionPrice.ClientID%>").is(':checked') || $("#<%=rdoPromotionFree.ClientID%>").is(':checked')) {
                                              
                        $("[id*=divLocation]").show();
                        $("[id*=divItemcode]").hide();
                        $("[id*=divDepartment]").hide();
                        $("[id*=divCategory]").hide();
                        $("[id*=divSubCategory]").hide();
                        $("[id*=divBrand]").hide();
                        $("[id*=divVendor]").hide();
                        $("[id*=divClass]").hide();
                        $("[id*=divSubClass]").hide();
                        $("[id*=divFloor]").hide();
                        $("[id*=divSection]").hide();
                    }
                    else if ($("#<%=rdoInvMov.ClientID%>").is(':checked')|| $("#<%=rdoInvVal.ClientID%>").is(':checked')) { 
                        $("[id*=divItemcode]").show();
                        $("[id*=divLocation]").hide();
                        $("[id*=divDepartment]").show();
                        $("[id*=divCategory]").show();
                        $("[id*=divSubCategory]").show();
                        $("[id*=divBrand]").show();
                        $("[id*=divVendor]").show();
                        $("[id*=divClass]").hide();
                        $("[id*=divSubClass]").hide();
                        $("[id*=divFloor]").show();
                        $("[id*=divSection]").show();
                    }
                     
                    else {
                        $("[id*=divItemcode]").show();
                        $("[id*=divLocation]").hide();                       
                        $("[id*=divDepartment]").show();
                        $("[id*=divCategory]").show();
                        $("[id*=divSubCategory]").show();
                        $("[id*=divBrand]").show();
                        $("[id*=divVendor]").show();
                        $("[id*=divClass]").show();
                        $("[id*=divSubClass]").show();
                        $("[id*=divFloor]").show();
                        $("[id*=divSection]").show();
                    }
                    $('#<%=txtItemCode.ClientID%>').val('');
                    $('#<%= lstCategory.ClientID %>').val('');
                    $('#<%=lstCategory.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstSubCategory.ClientID %>').val('');
                    $('#<%=lstSubCategory.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstDepartment.ClientID %>').val('');
                    $('#<%=lstDepartment.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstBrand.ClientID %>').val('');
                    $('#<%=lstBrand.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstVendor.ClientID %>').val('');
                    $('#<%=lstVendor.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstClass.ClientID %>').val(''); //aalayam 18042018
                    $('#<%=lstClass.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstFloor.ClientID %>').val('');
                    $('#<%=lstFloor.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstSubClass.ClientID %>').val('');
                    $('#<%=lstSubClass.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstSection.ClientID %>').val('');
                    $('#<%=lstSection.ClientID %>').trigger("liszt:updated");//
                    $('#ContentPlaceHolder1_txtLocation').val(''); // dinesh
                    //$('#ContentPlaceHolder1_ddLocation').trigger("liszt:updated");
                }
                else {
                    $("[id*=GrpInventoryListReportFilter]").hide();
                    $('#<%= lstCategory.ClientID %>').val('');
                    $('#<%=lstCategory.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstDepartment.ClientID %>').val('');
                    $('#<%=lstDepartment.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstBrand.ClientID %>').val('');
                    $('#<%=lstBrand.ClientID %>').trigger("liszt:updated");
                    $('#<%= lstVendor.ClientID %>').val('');
                    $('#<%=lstVendor.ClientID %>').trigger("liszt:updated");
                    $('#ContentPlaceHolder1_txtLocation').val('');
                    //$('#ContentPlaceHolder1_ddLocation').trigger("liszt:updated");
                }

                if (($("#<%=chkInventoryList.ClientID%>").is(':checked') && ($("#<%=chkCompleteInfo.ClientID%>").is(':checked') || $("#<%=chkMarginnotSet.ClientID%>").is(':checked'))) || $("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked') || $("#<%=rdoBatchWiseInvlist.ClientID%>").is(':checked') || $("#<%=rdoLessNetCost.ClientID%>").is(':checked') || $("#<%=rdoMUomWprice.ClientID%>").is(':checked'))//17042018
                {
                    $("[id*=chkBydate]").attr("checked", false);
                    $("[id*=chkBydate]").attr("disabled", "disabled");
                 
                    $("[id*=txtFromDate]").attr("disabled", "disabled");
                    $("[id*=txtToDate]").attr("disabled", "disabled");
                }
                else if(!$("#<%=rdoInvMov.ClientID%>").is(':checked') || $("#<%=rdoInvVal.ClientID%>").is(':checked')) {
                    $("[id*=chkBydate]").removeAttr("disabled");
                    $("[id*=chkBydate]").attr("checked", true);
                    $("[id*=txtFromDate]").removeAttr("disabled");
                    $("[id*=txtToDate]").removeAttr("disabled");
                }//
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Summary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Detail' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Cancel' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Update' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum')//aalayam 18042018
                {
                    $("[id*=GroupBy]").hide();
                    $("[id*=GroupByBatch]").show();
                }
                else {
                    $("[id*=GroupBy]").show();
                    $("[id*=GroupByBatch]").show();
                }//
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch') {
                    $("[id*=GroupByBatch]").show();
                }
                else {
                    $("[id*=GroupByBatch]").hide();
                }
                if ($("#<%=hidMultiComany.ClientID%>").val() == "Y") {
                    if ($("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked')) {
                        $("[id*=divLocation]").show();
                        $('#ContentPlaceHolder1_ReportFilterUserControl_txtLocation').val($("#<%=hidMinMaxLoc.ClientID%>").val()); 
                    }
                }
                var checkedstockvalue = $("#<%=rdoStockReport.ClientID%>").find(":checked").val();
                if (checkedstockvalue == 'Inventory' || checkedstockvalue == 'Batch' || checkedstockvalue == 'Department' || checkedstockvalue == 'Category' || checkedstockvalue == 'SubCategory' || checkedstockvalue == 'Brand' || checkedstockvalue == 'NonBatch' || checkedstockvalue == 'Location' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
                    $("[id*=chkBydateStock]").attr("disabled", "disabled");
                    $("[id*=chkBydateStock]").attr("checked", false);
                    $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                    $("[id*=txtToDateStock]").attr("disabled", "disabled");
                }
                else {
                    $("[id*=chkBydateStock]").removeAttr("disabled");
                    $("[id*=chkBydateStock]").attr("checked", true);
                    $("[id*=txtFromDateStock]").removeAttr("disabled");
                    $("[id*=txtToDateStock]").removeAttr("disabled");
                }

                if ($("#<%=HiddenLocationCheck.ClientID%>").val() == 'HQ') { //11042018
                    if (checkedstockvalue == 'Batch' || checkedstockvalue == 'NonMoving') {
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    }
                    else {
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    }
                }
                else {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                }
                if (checkedstockvalue == 'Inventory') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                }

                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
                    $('#ContentPlaceHolder1_divBinNo').show();
                }
                else {
                    $('#ContentPlaceHolder1_divBinNo').hide();
                }

                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Inventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Batch')
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    else
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                    if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonBatch')
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinSummary' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinDetail') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                    $("[id*=GroupBy]").hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Department' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Category' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'SubCategory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Brand' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Minimum') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Summary') {
                    $('#ContentPlaceHolder1_divStockAdjNo').show();
                    $('#ContentPlaceHolder1_divStockAdjType').show();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjNo').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjType').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Detail') {
                    $('#ContentPlaceHolder1_divStockAdjNo').show();
                    $('#ContentPlaceHolder1_divStockAdjType').show();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjNo').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjType').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Cancel') {
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').show();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjNo').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjType').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').show();
                }//
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Update') {
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').show();
                    $('#ContentPlaceHolder1_divStockAdjType').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divStockAdjType').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Location') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'NonMoving') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DateWise') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                } else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Refilling') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventory' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySales'
            || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'BinWiseInventorySummury') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').show();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divBinNo').show();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $("[id*=GroupBy]").hide();
                    $("[id*=chkBydateStock]").attr("disabled", "disabled");
                    $("[id*=chkBydateStock]").attr("checked", false);
                    $("[id*=txtFromDateStock]").attr("disabled", "disabled");
                    $("[id*=txtToDateStock]").attr("disabled", "disabled");
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }
                else {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                }

                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'Distribution' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail' || $("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
                    if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDetail')
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    else
                        $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divBrandDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divLocation').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divDepartmentDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubCategoryDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSubClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divVendorDropDown').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divClass').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divWarehouse').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divFloor').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divCompany').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divManufacture').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divSection').hide();
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divMerchandise').hide();
                    $('#ContentPlaceHolder1_divBatchNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjNo').hide();
                    $('#ContentPlaceHolder1_divStockAdjType').hide();
                    $('#ContentPlaceHolder1_divStockTakeId').hide();
                    $('#ContentPlaceHolder1_divDistributionNo').show();
                    $('#ContentPlaceHolder1_divBillNo').show();
                    $('#ContentPlaceHolder1_divBillNoDelete').hide();
                    $('#ContentPlaceHolder1_divDistributionNoDelete').hide();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_divStkAdjNoDelete').hide();
                    if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'DistributionDelete') {
            $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
            $('#ContentPlaceHolder1_divDistributionNo').hide();
            $('#ContentPlaceHolder1_divBillNo').hide();
            $('#ContentPlaceHolder1_divBillNoDelete').show();
            $('#ContentPlaceHolder1_divDistributionNoDelete').show();
        }
                }
                else if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() == 'StyleCode') {
                    $('#ContentPlaceHolder1_StockReportFilterUserControl_divInventory').show();
                    $('#ContentPlaceHolder1_divBatchNo').show();
                    $("[id*=GroupBy]").hide();
                    $('#ContentPlaceHolder1_lblBatchNo').text('Style Code');
                }
                else {
                    $('#ContentPlaceHolder1_divDistributionNo').hide();
                    $('#ContentPlaceHolder1_divBillNo').hide();
                }
                if ($("#<%=SortByCode.ClientID%>").is(':checked') || $("#<%=SortByName.ClientID%>").is(':checked')) {
                    $("[id*=ChkBatchOnly]").attr("disabled", "disabled");
                    $("[id*=ChkBatchOnly]").attr("checked", false);
                }
                else {
                    $("[id*=ChkBatchOnly]").removeAttr("disabled");
                    $("[id*=ChkBatchOnly]").attr("checked", true);
                }
                if ($("#<%=rdoStockReport.ClientID%>").find(":checked").val() != 'StyleCode') { 
                    $('#ContentPlaceHolder1_lblBatchNo').text('Batch No');
                }
            });
        });

        function clearForm() {
            try {
                if ($("#<%=rdoMinqty.ClientID%>").is(':checked') || $("#<%=rdoMaxQty.ClientID%>").is(':checked')) {
                    return false;
                }
                else {


                    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
                    //$("select").val(0);
                    //$("select").trigger("liszt:updated");
                    SetDefaultDate($("#<%= txtFromDate.ClientID %>"), $("#<%= txtToDate.ClientID %>"));
                    SetDefaultDate($("#<%= txtFromDateStock.ClientID %>"), $("#<%= txtToDateStock.ClientID %>"));
                }
            }
            catch (err) {
                alert(err.Message);
                return false;
            }
            return false;
        }

        function ValidateForm() {
            try {
                var Show = '';
                if ($("#<%= txtFromDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose To date';
                    $("#<%= txtFromDate.ClientID %>").focus();
                }
                if ($("#<%= txtToDate.ClientID %>").val() == "") {
                    Show = Show + '\n  Choose from date';
                    $("#<%= txtToDate.ClientID %>").focus();
                }
                if (Show != '') {
                    alert(Show);
                    return false;
                }
                else {
                    DateCheck();
                    return true;
                }
            }
            catch (err) {
                return false;
                alert(err.Message);
            }
        }
        function fncSetValue() {
            try {

            }
            catch (err) {
                ShowPopupMessageBox(err.message);
            }
        }
    </script>

    <style type="text/css">
        body {
            font-family: 'Roboto', 'Verdana';
            background-color: #fafafa;
        }

        .chzn-container .chzn-results {
            max-height: 100px;
        }

        .container {
            width: 100%;
        }

        h3 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto',Verdana;
            background: url("../images/ui-bg_gloss-wave_55_000000_500x100.png") repeat-x scroll 50% 50%;
        }

        h4 {
            font-weight: normal;
            font-size: 16px;
            font-family: 'Roboto', 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }

        input:checked + label {
            color: white;
            background: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">
        <div class="breadcrumbs">
            <ul>
                <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
                </li>
                <li><a style="text-decoration: none;">Reports</a><i class="fa fa-angle-right"></i></li>
                <li class="active-page">Inventory Report</li><li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>

            </ul>
        </div>
        <div class="ziehharmonika EnableScroll"  >

            <div style="overflow: visible; display: block;" runat="server" id="DivInv">
                <h3>Inventory List Report</h3>
                <div class="row">

                    <div class="col-md-3" style="margin-right: 40px;">
                        <div>
                            <div style="display: table">
                                <div class="control-group">
                                    <div style="float: left">
                                        <asp:RadioButton ID="chkInventoryList" runat="server" Class="radioboxlist" GroupName="RegularMenu" Checked="true" Text="Inventory List" />
                                        <%-- <p style="display: none">testing 1...2...3...</p>--%>
                                    </div>
                                </div>
                                <div id="complete" style="margin-left: 15%;">
                                    <div class="control-group">
                                        <div style="float: left">
                                            <asp:RadioButton ID="chkCompleteInfo" Class="radioboxlistgreen" runat="server" GroupName="SubMenu" Text="Complete Info"
                                                Font-Bold="True" />
                                        </div>
                                    </div>
                                    <div class="control-group" id="subDiv" style="float: left; padding: 5px 5px 0 5px; width: 165px; margin-left: 5%; display: none">
                                        <div style="float: left">
                                            <asp:RadioButton ID="chkItemWise" Class="radioboxlistgreen" runat="server" GroupName="SubMenu1" Text="ItemWise"
                                                Checked="true" Font-Bold="True" />
                                            &nbsp;&nbsp;
                                        </div>
                                        <div style="float: left">
                                            <asp:RadioButton ID="chkBatchwise" Class="radioboxlistgreen" runat="server" GroupName="SubMenu1" Text="Batchwise"
                                                Font-Bold="True" />
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div style="float: left">
                                            <asp:RadioButton ID="chkMarginnotSet" Class="radioboxlistgreen" runat="server" GroupName="SubMenu" Text="Margin Not Set"
                                                Font-Bold="True" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkDepartmantWise" runat="server" GroupName="RegularMenu" Class="radioboxlist" Text="Department Wise Inventory List" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkCategoryWise" runat="server" Text="Category Wise Inventory List" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group" style="display:none">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkSubCategoryWise" runat="server" Text="SubCategory Wise Inventory List" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="chkBrandWise" runat="server" Text="Brand Wise Inventory List" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoVendorWise" runat="server" Text="Vendor Wise Inventory List" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="Chkbarcodelist" Text="Barcode list" runat="server" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoBreakqty" Text="List of breakQty items" runat="server" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoDeactiveitem" runat="server" Text="Deactivated items list" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoDeactiveBatch" runat="server" Text="Deactivated Batches list" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoMinqty" runat="server" Text="Min qty alert" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoMaxQty" runat="server" Text="Max qty alert" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoitemChangelog" runat="server" Text="ItemName Change Log" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="RdoPriceChange" runat="server" Text="Price Change log" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoBatchWiseInvlist" Text="Batchwise inventory list" runat="server" GroupName="RegularMenu" Class="radioboxlist" />

                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoPromotion" runat="server" Text="Active Promotion List" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoPromotionPrice" runat="server" Text="Active Promotion PriceList" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoPromotionFree" runat="server" Text="Active Promotion FreeItemDetail" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoExpired" runat="server" Text="Expire Items(Getting Expiration)" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoLessNetCost" runat="server" Text="List of Items - SP less than Net Cost" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                             <div class="control-group" id="divInvMov" runat="server" >
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoInvMov"  runat="server" Text="Inventory Movement" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                                  <div style="float: left">
                                    <asp:RadioButton ID="rdoInvVal"  runat="server" Text="Inventory Movement Value" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoMUomWprice" Visible="false" runat="server" Text="MultiUom Wprice List" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoUomMPriceChage" Visible="false" runat="server" Text="MultiUom Price Change List" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoBatchExpired" Visible="false" runat="server" Text="Batch Item Expiry List" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                            <div class="control-group" id="divBatchcount" runat="server" style="margin-bottom: 25px">
                                <div style="float: left">
                                    <asp:TextBox ID="txtBatchCount" runat="server" CssClass="TextboxSize-Reports" onkeypress="return isNumberKey(event)"
                                        ForeColor="Black"></asp:TextBox>
                                    <asp:Label ID="Label38" runat="server" Text="Batch Count" ForeColor="#FF3300"></asp:Label>
                                </div>
                            </div>
                             <div class="control-group">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoParantChild"  runat="server" Text="Parant Child Detail" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                              <div class="control-group" style="display:none">
                                <div style="float: left">
                                    <asp:RadioButton ID="rdoPlantProduction"  runat="server" Text="Plant Production" GroupName="RegularMenu" Class="radioboxlist" />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="GrpInventoryListReportFilter" runat="server" class="col-md-4 form_bootstyle_border" style="margin-right: 25px;">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                          <div class="small-box bg-aqua">
                         <ups:ReportFilterUserControl runat="server" ID="ReportFilterUserControl"
                                EnableVendorDropDown="true"
                                EnablelocationDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableBrandDropDown="true"
                                EnableInventoryTextBox="true"
                                EnableClassDropDown="true"
                                EnableWarehouseDropDown="false"
                                EnableSubClassDropDown="true"
                                EnableFloorDropDown="true"
                                EnableCompanyDropDown="false"
                                EnableManufactureDropDown="false"
                                EnableSectionDropDown="true"
                                EnableSubCategoryDropDown="true"
                                EnableMerchandiseDropDown="false" />
                        </div>
                        <div class="small-box bg-aqua" style="display:none">

                            <div class="col-md-12 form_bootstyle" runat="server" id="divLocation">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddLocation" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Location', 'txtLocation', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divItemcode">
                                <div class="label-left">
                                    <asp:Label ID="lblItemCode" runat="server" Text="Item Code"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:TextBox ID="txtItemCode" onkeydown="return fncShowSearchDialogCommon(event, 'Inventory',  'txtItemCode', '');" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divDepartment">
                                <div class="label-left">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstDepartment" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divCategory">
                                <div class="label-left">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstCategory" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divSubCategory">
                                <%--aalayam 16042018--%>
                                <div class="label-left">
                                    <asp:Label ID="lblSubCategory" runat="server" Text="SubCategory"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstSubCategory" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divBrand">
                                <div class="label-left">
                                    <asp:Label ID="lblBrand" runat="server" Text="Brand"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstBrand" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divVendor">
                                <div class="label-left">
                                    <asp:Label ID="lblVendor" runat="server" Text="Vendor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstVendor" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divClass">
                                <%--aalayam 16042018--%>
                                <div class="label-left">
                                    <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstClass" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divSubClass">
                                <%--aalayam 16042018--%>
                                <div class="label-left">
                                    <asp:Label ID="lblSubClass" runat="server" Text="SubClass"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstSubClass" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divFloor">
                                <%--aalayam 16042018--%>
                                <div class="label-left">
                                    <asp:Label ID="lblFloor" runat="server" Text="Floor"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstFloor" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                            <div class="control-group-single-res" runat="server" id="divSection">
                                <%--aalayam 16042018--%>
                                <div class="label-left">
                                    <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label>
                                </div>
                                <div class="label-right">
                                    <asp:ListBox ID="lstSection" CssClass="form-control-res" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label4" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label5" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-5 form_bootstyle">
                                        <asp:CheckBox ID="chkBydate" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-2">
                                    </div>

                                    <asp:UpdatePanel ID="updpnlRefresh1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <%-- aalayam--%>
                                            <div class="col-md-5">
                                                <%-- <asp:LinkButton ID="lnkClearSearch" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                                <asp:LinkButton ID="lnkInvListReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkInvListReport_Click">Load Report </asp:LinkButton>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkActive" Class="radioboxlistgreen" runat="server" Text="IsACTIVE" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="chkVendor" Class="radioboxlistgreen" runat="server" Text="VendorWise" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div style="overflow: visible; display: block;" runat="server" id="DivStock">
                <h3>Stock Report</h3>
                <div class="row">
                    <div class="col-md-3" style="margin-left: 20px">
                        <div>
                            <asp:RadioButtonList ID="rdoStockReport" runat="server" Class="radioboxlist">
                                <asp:ListItem Selected="True" Value="Inventory"> Inventory Stock List</asp:ListItem>
                                <asp:ListItem Value="Batch">Batch Wise Stock</asp:ListItem>
                                <asp:ListItem Value="Department"> Department Wise Stock</asp:ListItem>
                                <asp:ListItem Value="Category"> Category Wise Stock</asp:ListItem>
                                <asp:ListItem Value="SubCategory"> Sub Category Wise Stock</asp:ListItem>
                                <asp:ListItem Value="Brand">Brand wise Stock</asp:ListItem>
                                <asp:ListItem Value="Summary">Stock Adjustment-Summary</asp:ListItem>
                                <asp:ListItem Value="Detail">Stock Adjustment-Detail</asp:ListItem>
                                <asp:ListItem Value="Cancel">Stock Adjustment-Cancelled</asp:ListItem>
                                <asp:ListItem Value="Update">Stock Update</asp:ListItem>
                                <asp:ListItem Value="NonBatch">Non Batch Wise Stock</asp:ListItem>
                                <asp:ListItem Value="Location">Location Wise Stock</asp:ListItem>
                                <asp:ListItem Value="NonMoving">Non Moving Item</asp:ListItem>
                              <%-- <asp:ListItem Value="DateWise">DateWise Stock</asp:ListItem>  --%>
                                <asp:ListItem Value="Distribution">Distribution - Summary</asp:ListItem>
                                <asp:ListItem Value="DistributionDetail">Distribution - Detailed</asp:ListItem>
                                <asp:ListItem Value="DistributionDelete">Distribution - Deleted</asp:ListItem>
                                <asp:ListItem Value="BinSummary">Bin Based Refilling Qty Summary</asp:ListItem>
                                <asp:ListItem Value="BinDetail">Bin Based Refilling Qty Detail</asp:ListItem>
                                <asp:ListItem Value="Refilling">Refilling Report</asp:ListItem>
                           <%--     <asp:ListItem Value="BinWiseInventory">Bin Wise Inventory Stock List Detail</asp:ListItem>
                                <asp:ListItem Value="BinWiseInventorySummury">Bin Wise Inventory Stock List Summury</asp:ListItem>
                                <asp:ListItem Value="BinWiseInventorySales">Bin Wise Inventory Stock List Sales</asp:ListItem>--%>
                                <asp:ListItem Value="Minimum">Minimum Reorder Level Qty</asp:ListItem> 
                                <asp:ListItem Value="StyleCode">Style Code Report</asp:ListItem> 
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border" style="margin-right: 45px">
                        <div class="report-header-filter">
                            Filtrations
                        </div>
                        <div class="small-box bg-aqua">
                            <ups:ReportFilterUserControl runat="server" ID="StockReportFilterUserControl"
                                EnableVendorDropDown="true"
                                EnablelocationDropDown="true"
                                EnableDepartmentDropDown="true"
                                EnableCategoryDropDown="true"
                                EnableBrandDropDown="true"
                                EnableInventoryTextBox="true"
                                EnableClassDropDown="true"
                                EnableWarehouseDropDown="true"
                                EnableSubClassDropDown="true"
                                EnableFloorDropDown="true"
                                EnableCompanyDropDown="true"
                                EnableManufactureDropDown="true"
                                EnableSectionDropDown="true"
                                EnableSubCategoryDropDown="true"
                                EnableMerchandiseDropDown="true" />

                            <div class="col-md-12 form_bootstyle" runat="server" id="divBatchNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBatchNo" runat="server" Text="Batch No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddBinNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtBatchNo" runat="server" onkeydown="return fncShowSearchDialogCommon(event, 'BatchNo',  'txtBatchNo', '');" MaxLength="15" runat="server" CssClass="form-control-res"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divStockAdjNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label19" runat="server" Text="StockAdjNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlStockAdjNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtStockAdjNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'StkAdjNo', 'txtStockAdjNo', '');"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-12 form_bootstyle" runat="server" id="divStkAdjNoDelete">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label7" runat="server" Text="StockAdj No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlStockAdjNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtStkAdjNoDelete" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'StkAdjNoDelete', 'txtStkAdjNoDelete', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divStockAdjType">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label20" runat="server" Text="StockAdjType"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlStockAdjType" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtStockAdjType" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'StkAdjTypeCode', 'txtStockAdjType', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divStockTakeId">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label22" runat="server" Text="StockTakeId"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddlStockTakeId" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtStockTakeId" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'StockTakeId', 'txtStockTakeId', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divDistributionNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label3" runat="server" Text="DistributionNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtDistributionNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'DistributionNo', 'txtDistributionNo', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divDistributionNoDelete">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblDistributionNo" runat="server" Text="DistributionNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtDistributionNoDelete" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'DistributionNoDelete', 'txtDistributionNoDelete', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBillNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBillNo" runat="server" Text="BillNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'BillNo', 'txtBillNo', '');"></asp:TextBox>


                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBillNoDelete">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="Label6" runat="server" Text="BillNo"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <asp:TextBox ID="txtBillNoDelete" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'BillNoDelete', 'txtBillNoDelete', '');"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-12 form_bootstyle" runat="server" id="divBinNo">
                                <div class="col-md-4 form_bootstyle">
                                    <asp:Label ID="lblBinNo" runat="server" Text="Bin No"></asp:Label>
                                </div>
                                <div class="col-md-8 form_bootstyle">
                                    <%--<asp:DropDownList ID="ddBinNo" runat="server" Style="width: 100%">
                                    </asp:DropDownList>--%>
                                    <asp:TextBox ID="txtBinNo" runat="server" CssClass="form-control-res" onkeydown="return fncShowSearchDialogCommon(event, 'Bin', 'txtBinNo', '');"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 form_bootstyle_border">
                        <div class="barcode-header">
                            Date Filter
                        </div>
                        <div class="small-box bg-aqua">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label1" runat="server" Text="From Date"></asp:Label>
                                        <asp:TextBox ID="txtFromDateStock" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                    <div class="col-md-6 form_bootstyle">
                                        <asp:Label ID="Label2" runat="server" Text="To Date"></asp:Label>
                                        <asp:TextBox ID="txtToDateStock" runat="server" CssClass="form_textbox"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top: 15px">
                                    <div class="col-md-5 form_bootstyle">
                                        <asp:CheckBox ID="chkBydateStock" runat="server" Text="By Date" Class="radioboxlist" Checked="true" Font-Bold="True" />
                                    </div>
                                    <div class="col-md-2">
                                    </div>

                                    <asp:UpdatePanel ID="updpnlRefresh2" runat="server" UpdateMode="Conditional">
                                        <%-- aalayam--%>
                                        <ContentTemplate>
                                            <div class="col-md-5">
                                                <%--    <asp:LinkButton ID="lnkClearStock" runat="server" class="btn btn-primary"
                                            Font-Bold="true" OnClientClick="return clearForm()">Clear</asp:LinkButton>--%>
                                                <asp:LinkButton ID="lnkLoadStockReport" runat="server" class="btn btn-danger" Font-Bold="true" OnClick="lnkLoadStockReport_Click">Load Report </asp:LinkButton>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </div>
                            </div>
                        </div>
                        <div id="GroupBy" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <div class="col-md-4 form_bootstyle">
                                <asp:CheckBox ID="chkPositive" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Positive"
                                    Checked="true" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:CheckBox ID="chkNegative" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Negative" />
                            </div>
                            <div class="col-md-4 form_bootstyle">
                                <asp:CheckBox ID="chkZerostock" Class="radioboxlist" runat="server" GroupName="RegularMenu" Text="Zero Stock" />
                            </div>
                        </div>
                        <div id="divZeroQty" style="display: none;">
                            <div class="col-md-6 form_bootstyle">
                                <asp:CheckBox ID="cbDotShowZeroQty" Class="radioboxlist" runat="server" Text="Dont Show Zero Qty"
                                    Checked="true" />
                            </div>
                        </div>


                        <div id="GroupByBatchOnly" class="col-md-12 form_bootstyle_border" style="margin-top: 15px;">
                            <%--aalayam--%>
                            <div class="col-md-8 form_bootstyle">
                                <asp:CheckBox ID="ChkBatchOnly" Class="radioboxlistgreen" runat="server" Text="Batch Only" />
                            </div>
                        </div>

                        <div id="GroupByBatch" class="col-md-12 form_bootstyle_border" style="margin-top: 10px;">
                            <div class="col-md-5 form_bootstyle">
                                <asp:RadioButton ID="BatchGrp" CssClass="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Grouped" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="SortByCode" CssClass="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Sort By Code" />
                            </div>
                            <div class="col-md-6 form_bootstyle">
                                <asp:RadioButton ID="SortByName" CssClass="radioboxlistgreen" runat="server" GroupName="RegularMenu" Text="Sort By Name" />
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <asp:HiddenField ID="HidenInventoryName" runat="server" Value="" />
            <asp:HiddenField ID="HiddenLocationCheck" runat="server" Value="" />
            <asp:HiddenField ID="hidMultiComany" runat="server" Value="" />
            <asp:HiddenField ID="hidMinMaxLoc" runat="server" Value="" />

        </div>
    </div>

</asp:Content>
