﻿<%@ Page Title="Branchwise Sales Details" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeBehind="SalesDetailsBranchwise.aspx.cs" Inherits="EnterpriserWebFinal.Reports.SalesDetailsBranchwise" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css"> 
          .fa-question-circle {
            color: royalblue;
            display: block;
            position: absolute;
            left: 1386px;
            font-size: 15px;
            padding: 0px 80px;
            font-weight: bold
        }


    .no-close .ui-dialog-titlebar-close{
            display:none;
        }

        .LocationNamePostion
        {
            text-align:left;
            padding-left:20px;
        }
    </style>
    
    <script type="text/javascript">
        var d1;
        var d2;
        var Opendate;
        var dur;
        function fncGetUrl() {
            fncSaveHelpVideoDetail('', '', 'SaleDetailBranchwise');
        }
        function fncOpenvideo() {

            document.getElementById("ifHelpVideo").src = HelpVideoUrl;

            var Mode = "SaleDetailBranchwise";
            var d = new Date($.now());
            Opendate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
            d1 = new Date($.now()).getTime();



            $("#dialog-Open").dialog({
                autoOpen: true,
                resizable: false,
                height: "auto",
                width: 1093,
                modal: true,
                dialogClass: "no-close",
                buttons: {
                    Close: function () {
                        $(this).dialog("destroy");
                        var d = new Date($.now());
                        var ClosedDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                        d2 = new Date($.now()).getTime();
                        var Diff = Math.floor((d2 - d1) / 1000);
                        //alert(Diff);
                        if (Diff >= 60) {
                            fncSaveHelpVideoDetail(Opendate, ClosedDate, Mode);
                        }

                    }
                }
            });
        }


    </script>

<script type="text/javascript">
    var reportTimer = 0;
    function pageLoad() {        
        $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });
        $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });

        $("select").chosen({ width: '100%' });
    }
    function clearForm() {
        $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
        $(':checkbox, :radio').prop('checked', false);
        $("input").removeAttr('disabled');

        $("select").val(0);
        $("select").trigger("liszt:updated");
                
        $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });
        $("#<%= txtToDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });

        $("select").chosen({ width: '100%' });
    }
    function LoadReport() {
       
        $('#statusMessage').text('Loading report....');
        $("#<%= divFilterData.ClientID %>").hide();
        $("#<%= divSummary.ClientID %>").hide();
        $("#divBranchDetails").hide();
        $("#<%= lnkExportToExcel.ClientID %>").hide();

        $("#statusMessageTimer").show();
        $("#statusMessageTimer").text(0);

        reportTimer = 0;
        setInterval(function () {
            reportTimer++;
            $("#statusMessageTimer").text(reportTimer);
        }, 3000);

    }

    function BindTaxToggle() {

        $('span[id*=spanTaxDetailRight]').on("click", function () {

            var colspan = 4; //For Summary grid
            if ($(this).closest("table").attr('id').indexOf("rptBranchDetails_gvBranchSales") >= 0) {
                colspan = 2;
            }
            $(this).closest("tr").after("<tr><td colspan = '" + colspan + "'></td><td colspan = '999'>" + $(this).next().next().html() + "</td></tr>")

            $(this).next().show();
            $(this).hide();
        });

        $("span[id*=spanTaxDetailDown]").on("click", function () {            

            $(this).prev().show();
            $(this).hide();

            $(this).closest("tr").next().remove();
        });
    }


    $(document).ready(function () {
        


    });
    //    $(document).ready(function () {
    //        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

    //            $("#<%= txtFromDate.ClientID %>").datepicker({ dateFormat: "dd/mm/yy", maxDate: "0" });

    //            $("select").chosen({ width: '100%' });
    //        });
    //    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="breadcrumbs">
        <ul>
            <li><a href="../MISDashboard/frmMisDashBoard.aspx">Home</a> <i class="fa fa-angle-right"></i>
            </li>
            <li><a href="#">Management</a> <i class="fa fa-angle-right">
            </i></li>
            <li class="active-page">Branch-Wise Sales</li> <li><i class="fa fa-question-circle" style="cursor:pointer;font-size: 19px;" onclick= "fncGetUrl(); return false;"></i></li>
        </ul>
    </div>
    <asp:UpdatePanel ID="upfrom" UpdateMode="Always" runat="Server">
    <Triggers><asp:PostBackTrigger ControlID="lnkExportToExcel" /></Triggers>
    <ContentTemplate>
    <div class="container-group-small">
        <div class="container-control">                
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblGroupBy" runat="server" Text="Group By"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:DropDownList runat="server" ID="ddlGroupBy" CssClass="form-control-res">
                            <asp:ListItem Text="Tax Rate" Value="" />
                            <asp:ListItem Text="Inventory Item" Value="I" />
                            <asp:ListItem Text="Category" Value="C" />
                            <asp:ListItem Text="Brand" Value="B" />
                            <asp:ListItem Text="Department" Value="D" />
                        </asp:DropDownList>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblLocation" runat="server" Text="Branch"></asp:Label>
                </div>
                <div class="label-right">
                        <asp:DropDownList runat="server" ID="ddlLocation" CssClass="form-control-res">                           
                        </asp:DropDownList>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control-res" onkeydown="return false;"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control-res" onkeydown="return false;"></asp:TextBox>
                </div>
            </div>
            <div class="control-group-single-res">
                <div class="label-left">
                    <asp:Label ID="Label2" runat="server" Text="Summary Only"></asp:Label>
                </div>
                <div class="label-right">
                    <asp:CheckBox ID="chkSummaryOnly" runat="server" Checked="true" />
                </div>
            </div>
        </div>
        <div style="float:left">
        <div class="control-button">
            <asp:LinkButton ID="lnkExportToExcel" runat="server" class="button-blue" OnClick="lnkExportToExcel_Click"
                Text='<%$ Resources:LabelCaption,btn_Export %>'></asp:LinkButton>
        </div>
        </div>
        <div style="float:right">
        <div class="button-contol">
            <div class="control-button">
                <asp:LinkButton ID="lnkLoadReport" runat="server" class="button-red" OnClick="lnkLoadReport_Click" 
                OnClientClick="LoadReport()">Load Report</asp:LinkButton>
            </div>
           <%-- <div class="control-button">
                <asp:LinkButton ID="lnkClear" runat="server" class="button-red" OnClientClick="clearForm();return false;"
                    ><i class="icon-play"></i>Clear</asp:LinkButton>
            </div>--%>
        </div>
        </div>        
    </div>
    <span style="color:Red" id="statusMessage"></span><span id="statusMessageTimer"  style="color:Red;display:none;"></span>
   <div style="margin:10px 0 10px 0;font-weight:bold;color:#0E72B5" id="divFilterData" runat="server" visible="false">
       <span style="color:Black">Filter:</span>
       <asp:Label ID="lblGroupBySelected" Text="" runat="server" /> |        
       <asp:Label ID="lblBranchSelected" Text="" runat="server" /> |
       Between <asp:Label ID="lblFromDateSelected" Text="" runat="server" /> and 
       <asp:Label ID="lblToDateSelected" Text="" runat="server" />
    </div>
    <a name = "Summary"></a>
    <div class="gridDetails" id="divSummary" runat="server" style="background-color:#f3f3f3" visible="false">
        <div style="margin:10px 0 10px 0;font-weight:bold;">
            Branch-wise Sales Summary
        </div>
        <asp:GridView ID="gvBranchSalesSummary" runat="server" AllowSorting="false" AutoGenerateColumns="false"
                        ShowHeaderWhenEmpty="true" CssClass="pshro_GridDgn" AllowPaging="false" OnRowDataBound="gvBranchSalesSummary_RowDataBound">
            <EmptyDataTemplate>
                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
            </EmptyDataTemplate>
            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
            <RowStyle CssClass="pshro_GridDgnStyle" />
            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />                        
            <Columns>
            <asp:BoundField DataField="Sl.No" HeaderText="Sl.No"></asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>                    
                   <%-- <div id="divTaxDetailToggle">--%>
                        <span id="spanTaxDetailRight" style="cursor: pointer;"  class="glyphicon glyphicon-chevron-right" aria-hidden="true" runat="server"></span>
                        <span id="spanTaxDetailDown" style="cursor: pointer;display:none;"  class="glyphicon glyphicon-chevron-down" aria-hidden="true" runat="server"></span>
                    <%--</div>--%>
                    <asp:Panel ID="pnlTaxDrillDown" runat="server" Style="display: none">
                        <asp:GridView ID="gvTaxDrillDown" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn">
                            <EmptyDataTemplate>
                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                            <RowStyle CssClass="pshro_GridDgnStyle" />
                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />     
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="127" HeaderStyle-Width="127">
                                    <HeaderTemplate>
                                        <%# SelectedGroupBy %>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("DrillDownField") %>
                                    </ItemTemplate>                                    
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="DrillDownField" HeaderText="" ItemStyle-Width="127" HeaderStyle-Width="127"/>--%>
                                <asp:BoundField DataField="TaxableValue" HeaderText="Taxable Value"></asp:BoundField>
                                <asp:BoundField DataField="SGSTAmt" HeaderText="SGST"></asp:BoundField>
                                <asp:BoundField DataField="CGSTAmt" HeaderText="CGST"></asp:BoundField>
                                <asp:BoundField DataField="TotalTaxAmt" HeaderText="TotalTax"></asp:BoundField>
                                <asp:BoundField DataField="TotalAmt" HeaderText="Total Value"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="LocationCode" HeaderText="Location Code"></asp:BoundField>
            <asp:HyperLinkField DataNavigateUrlFormatString="#{0}" DataNavigateUrlFields="LocationCode" DataTextField="LocationName" HeaderText="Location Name" HeaderStyle-CssClass="LocationNamePostion" ItemStyle-CssClass="LocationNamePostion" />
            <asp:BoundField DataField="LastSyncDate" HeaderText="LastSyncAt" ItemStyle-Width="127" HeaderStyle-Width="127"></asp:BoundField>
            <asp:BoundField DataField="TaxableValue" HeaderText="Taxable Value"></asp:BoundField>
            <asp:BoundField DataField="SGSTAmt" HeaderText="SGST"></asp:BoundField>
            <asp:BoundField DataField="CGSTAmt" HeaderText="CGST"></asp:BoundField>            
            <asp:BoundField DataField="TotalTaxAmt" HeaderText="TotalTax"></asp:BoundField>
            <asp:BoundField DataField="TotalAmt" HeaderText="Total Value"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="gridDetails" id="divBranchDetails">
        <asp:Repeater runat="server" ID="rptBranchDetails" OnItemDataBound="rptBranchDetails_OnItemDataBound">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>
                <div style="margin:12px 0 2px 10px;font-weight:bold;height:20px;width:98%;">
                    <div style="float:left">
                        <a name="<%# Eval("LocationCode")%>"></a>
                        <%# Eval("LocationName")%> <span>(</span>
                        <span>Taxable Value : </span> <%# Eval("TaxableValue")%>
                        <span> | SGST : </span> <%# Eval("SGSTAmt")%>
                        <span> | CGST : </span> <%# Eval("CGSTAmt")%>
                        <span> | TotalTax : </span> <%# Eval("TotalTaxAmt")%>
                        <span> | TotalAmt : </span> <%# Eval("TotalAmt")%>
                        <span>)</span>
                        <span style="color:<%# GetSyncDateColor(Eval("LastSyncDate").ToString()) %>"> Last Sync : <%# Eval("LastSyncDate")%></span>
                    </div>
                    <div style="float:right">
                    <%--<span style="font-weight:bold;"><a href="#Summary">Top</a></span>--%>
                        <a href="#Summary" style="font-weight:bold;">Goto Summary <span class="glyphicon glyphicon-upload" aria-hidden="true"></span></a>
                    </div>                    
                </div>
                <div style="margin:0 10px 10px 10px;max-height:400px;overflow:auto;">
                    <asp:GridView ID="gvBranchSales" runat="server" AllowSorting="false" AutoGenerateColumns="false"
                        ShowHeaderWhenEmpty="true" OnRowDataBound="gvBranchSales_OnRowDataBound"
                        CssClass="pshro_GridDgn" AllowPaging="false">
                        <EmptyDataTemplate>
                            <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                        </EmptyDataTemplate>
                        <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                        <RowStyle CssClass="pshro_GridDgnStyle" />
                        <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />                        
                        <Columns>      
                            <asp:BoundField DataField="Sl.No" HeaderText="Sl.No"></asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>                                       
                                    <span id="spanTaxDetailRight" style="cursor: pointer;"  class="glyphicon glyphicon-chevron-right" aria-hidden="true" runat="server"></span>
                                    <span id="spanTaxDetailDown" style="cursor: pointer;display:none;"  class="glyphicon glyphicon-chevron-down" aria-hidden="true" runat="server"></span>                   
                                    <asp:Panel ID="pnlTaxDrillDown" runat="server" Style="display: none">
                                        <asp:GridView ID="gvTaxDrillDown" runat="server" AutoGenerateColumns="false" CssClass="pshro_GridDgn">
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label3" runat="server" Text="No Records Found"></asp:Label>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="pshro_GridDgnHeaderCellCenter" />
                                            <RowStyle CssClass="pshro_GridDgnStyle" />
                                            <AlternatingRowStyle CssClass="pshro_GridDgnStyle_Alternative" />     
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="127" HeaderStyle-Width="127">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label1" Text="<%# SelectedGroupBy %>" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDrillDownField" runat="server" ><%# Eval("DrillDownField") %></asp:Label>
                                                    </ItemTemplate>                                    
                                                </asp:TemplateField>
                                                <%--<asp:BoundField DataField="TaxRate" HeaderText="Tax Rate" ItemStyle-Width="127" HeaderStyle-Width="127"/>--%>
                                                <asp:BoundField DataField="TaxableValue" HeaderText="Taxable Value"></asp:BoundField>
                                                <asp:BoundField DataField="SGSTAmt" HeaderText="SGST"></asp:BoundField>
                                                <asp:BoundField DataField="CGSTAmt" HeaderText="CGST"></asp:BoundField>
                                                <asp:BoundField DataField="TotalTaxAmt" HeaderText="TotalTax"></asp:BoundField>
                                                <asp:BoundField DataField="TotalAmt" HeaderText="Total Value"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>                                         
                            <asp:BoundField DataField="TransactionDate" HeaderText="Sales Date" ItemStyle-Width="127" HeaderStyle-Width="127"></asp:BoundField>
                            <asp:BoundField DataField="TaxableValue" HeaderText="Taxable Value"></asp:BoundField>
                            <asp:BoundField DataField="SGSTAmt" HeaderText="SGST"></asp:BoundField>
                            <asp:BoundField DataField="CGSTAmt" HeaderText="CGST"></asp:BoundField>            
                            <asp:BoundField DataField="TotalTaxAmt" HeaderText="TotalTax"></asp:BoundField>
                            <asp:BoundField DataField="TotalAmt" HeaderText="Total Value"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
        </asp:Repeater>                                        
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
